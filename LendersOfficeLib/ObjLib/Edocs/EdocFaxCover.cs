﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text.pdf;
using System.IO;
using iTextSharp.text;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Common;

namespace EDocs
{
    public enum E_FaxCoverType
    {
        EDocs = 0,
        ConsumerPortalRequest = 1,
        ObsoleteEdocs = 2,
        PDFProcessor = 3
    }



    public class EdocFaxCover
    {
        // POD class to deal with all the new data going into cover sheet generation.
        public class FaxCoverData
        {
            public E_FaxCoverType Type {get; set;}
            public long ConsumerRequestId {get; set;}
            public Guid BrokerId { get; set; }
            public Guid LoanId {get; set;}
            public Guid AppId {get; set;}
            public int DocTypeId {get; set;}
            public string LoanNumber { get; set; }
            public string Description {get; set;}
            public string ApplicationDescription { get; set; }
            public string LenderName {get; set;}
            public string DocTypeDescription {get; set;}
            public string FaxNumber {get; set;}
            public byte[] DocumentToAttach {get; set;}
        }

        public static bool IsBarcodeType(E_FaxCoverType type, string data)
        {
            if (type != E_FaxCoverType.PDFProcessor)
            {
                throw new NotSupportedException(type.ToString());
            }

            return data.StartsWith("LQB:"); 
        }

        public static FaxCoverData ReadBarcode(string barcode)
        {
            if (barcode.StartsWith("LQB:") == false)
            {
                throw new NotSupportedException();
            }

            string decryptedData = EncryptionHelper.Decrypt(barcode.Substring(4));
            string[] parts = decryptedData.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);

            if (parts.Length < 3)
            {
                throw CBaseException.GenericException("Invalid barcode.");
            }

            FaxCoverData data = new FaxCoverData()
            {
                Type = E_FaxCoverType.PDFProcessor,
                LoanId = new Guid(parts[0]),
                AppId = new Guid(parts[1]),
                DocTypeId = int.Parse(parts[2])  ,
                Description = ""
               
            };

            if (parts.Length == 4)
            {
                data.Description = parts[3];
            }

            return data;
        }

        // 02/08/12 mf. OPM 77831.  When doing the batch coversheets for all doc types
        // build the pdf from scratch using itext library instead of filling
        // out the form of the embedded resource pdf.  Otherwise clients
        // with many doc types will generate Out of Memory exception.
        public static byte[] GenerateBarcodePackagePdf(FaxCoverData[] coverData)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                Document doc = new Document(PageSize.LETTER);
                try
                {
                    PdfWriter writer = PdfWriter.GetInstance(doc, stream);
                    doc.Open();

                    foreach (FaxCoverData cover in coverData)
                    {
                        doc.NewPage();
                        WritePackagePage(writer, cover);

                        if (cover.DocumentToAttach != null && cover.DocumentToAttach.Length != 0)
                        {
                            PdfReader inputPdf = new PdfReader(cover.DocumentToAttach, System.Text.UTF8Encoding.UTF8.GetBytes(ConstAppDavid.PdfMasterPassword));
                            
                            PdfImportedPage page;
                            PdfContentByte cb = writer.DirectContent;
                            for (int i = 1; i <= inputPdf.NumberOfPages; i++)
                            {
                                doc.NewPage();
                                page = writer.GetImportedPage(inputPdf, i);
                                cb.AddTemplate(page, 0, 0);
                            }

                            if (inputPdf != null)
                                inputPdf.Close();
                        }

                    }
                }
                finally
                {
                    doc.Close();
                }
                
                return stream.ToArray();
            }
        }

        private static void WritePackagePage(PdfWriter writer, FaxCoverData data)
        {
            
            float firstCol_x = 75;
            float firstColData_x = firstCol_x + 80;
            float secondCol_x = firstCol_x + 220;
            float secondColData_x = secondCol_x + 80;
            float lineEnd_x = 550;
            
            float y_pos = 675;

            PdfContentByte cb = writer.DirectContent;

            // Title
            cb.BeginText();
            cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, false), 26);

            WriteText( data.Type == E_FaxCoverType.PDFProcessor ? "Barcode Cover Sheet" : data.LenderName,
                     cb,  firstCol_x, y_pos  );
            cb.EndText();

            // Horizontal line
            y_pos -= 10;
            cb.SetLineWidth(0.5f);
            cb.MoveTo(firstCol_x, y_pos);
            cb.LineTo(lineEnd_x, y_pos);
            cb.Stroke();

            // Data
            if (data.Type != E_FaxCoverType.PDFProcessor)
            {
                firstColData_x = firstCol_x + 70;
                secondCol_x = firstCol_x + 250;
                secondColData_x = secondCol_x + 80;

                y_pos -= 35;
                cb.BeginText();
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false), 28);
                cb.SetColorFill(GrayColor.GRAY);
                WriteText("Fax Cover", cb, firstCol_x, y_pos);
                cb.EndText();

                y_pos -= 25;
                cb.BeginText();
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false), 10);
                cb.SetColorFill(GrayColor.BLACK);
                WriteText("To:", cb, firstCol_x, y_pos);
                WriteText(data.LenderName, cb, firstColData_x - 15, y_pos);

                WriteText("Fax:", cb, secondCol_x, y_pos);
                WriteText(data.FaxNumber, cb, secondColData_x, y_pos);

                y_pos -= 25;
                WriteText("Doc Type:", cb, firstCol_x, y_pos);
                WriteText(data.DocTypeDescription, cb, firstColData_x - 15, y_pos);

                WriteText("Loan Number:", cb, secondCol_x, y_pos);
                WriteText(data.LoanNumber, cb, secondColData_x, y_pos);

                y_pos -= 25;
                WriteText("Application:", cb, firstCol_x, y_pos);
                WriteText(data.ApplicationDescription, cb, firstColData_x - 15, y_pos);

                cb.EndText();
            }
            else
            {
                y_pos -= 20;
                cb.BeginText();
                cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false), 12);

                WriteText("Loan Number:", cb, firstCol_x, y_pos);
                WriteText(data.LoanNumber, cb, firstColData_x, y_pos);

                WriteText("Application:", cb, secondCol_x, y_pos);
                WriteText(data.ApplicationDescription, cb, secondColData_x, y_pos);

                y_pos -= 25;
                WriteText("Doc Type:", cb, firstCol_x, y_pos);
                WriteText(data.DocTypeDescription, cb, firstColData_x, y_pos);

                y_pos -= 25;
                WriteText("Description:", cb, firstCol_x, y_pos);
                WriteText(data.Description, cb, firstColData_x, y_pos);

                cb.EndText();
            }
            
            // Horizontal line
            y_pos -= 50;
            cb.SetLineWidth(0.5f);
            cb.MoveTo(firstCol_x, y_pos);
            cb.LineTo(lineEnd_x, y_pos);
            cb.Stroke();

            // Warning Text
            y_pos -= 15;
            cb.BeginText();
            cb.SetFontAndSize(BaseFont.CreateFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, false), 12);
            WriteText("PLEASE DO NOT WRITE ON THIS COVER SHEET", cb, firstColData_x, y_pos);
            y_pos -= 200;
            WriteText("PLEASE DO NOT WRITE ON THE BARCODE BELOW", cb, firstColData_x, y_pos);
            cb.EndText();

            // Barcode
            y_pos -= 200;

            if (data.Type == E_FaxCoverType.EDocs)
            {
                BarcodePDF417 codePDF = new BarcodePDF417();
                codePDF.SetText(
                    BuildCompositeStringForEDocs(
                                        data.LoanId,
                                        data.AppId,
                                        data.DocTypeId,
                                        data.Description));
                Image barcodeImage = Image.GetInstance(codePDF.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White), BaseColor.BLACK);
                barcodeImage.SetAbsolutePosition(firstCol_x, y_pos);
                barcodeImage.ScalePercent(225, 225);
                cb.AddImage(barcodeImage);
            }
            else if (data.Type == E_FaxCoverType.ObsoleteEdocs)
            {
                BarcodePDF417 codePDF = new BarcodePDF417();
                codePDF.SetText(
                    BuildCompositeStringForObsoleteEDocs(
                                        data.LoanId,
                                        data.DocTypeId,
                                        data.Description));
                Image barcodeImage = Image.GetInstance(codePDF.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White), BaseColor.BLACK);
                barcodeImage.SetAbsolutePosition(firstCol_x, y_pos);
                barcodeImage.ScalePercent(225, 225);
                cb.AddImage(barcodeImage);
            }
            else if (data.Type == E_FaxCoverType.ConsumerPortalRequest)
            {
                BarcodePDF417 codePDF = new BarcodePDF417();
                codePDF.SetText(BuildCompositeStringForConsumerPortalRequest(data.ConsumerRequestId, data.BrokerId));
                Image barcodeImage = Image.GetInstance(codePDF.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White), BaseColor.BLACK);
                barcodeImage.SetAbsolutePosition(firstCol_x, y_pos);
                barcodeImage.ScalePercent(225, 225);
                cb.AddImage(barcodeImage);
            }
            else if (data.Type == E_FaxCoverType.PDFProcessor)
            {
                Barcode128 codePDf = new Barcode128();
                codePDf.Code = data.DocTypeId.ToString();
                codePDf.ChecksumText = false;
                codePDf.BarHeight = 50;
                codePDf.GenerateChecksum = false;
                codePDf.CodeType = Barcode.CODE128;

                Image image128 = codePDf.CreateImageWithBarcode(cb, BaseColor.BLACK, BaseColor.BLACK);
                image128.SetAbsolutePosition(firstCol_x + 200, y_pos);
                image128.ScalePercent(225, 225);

                cb.AddImage(image128);
                //Image barcodedImage = Image.GetInstance(codePDf.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.Black), BaseColor.BLACK);
                //barcodedImage.SetAbsolutePosition(firstCol_x+200, y_pos);
                //cb.AddImage(barcodedImage);
            }
            else
            {
                throw new UnhandledEnumException(data.Type);
            }
        }

        private static void WriteText(string text, PdfContentByte cB, float x, float y)
        {
            cB.ShowTextAligned(PdfContentByte.ALIGN_LEFT, text, x, y, 0);
        }

        public static string BuildPdfProcessorBarcodes(Guid loanId, Guid appId, int docTypeId, string description)
        {
            string compositeData = String.Format("{0}|{1}|{2}|{3}",
             loanId.ToString(),
             appId.ToString(),
             docTypeId.ToString(),
             description
             );

            // Encrypting because this piece of data as a barcode on a piece
            // of paper will have a life outside our system.
            return "LQB:"+LendersOffice.Common.EncryptionHelper.Encrypt(compositeData);
        }

        // This is only public for debug/test purposes, should have better mechanism
        public static string BuildCompositeStringForObsoleteEDocs(Guid loanId, int docTypeId, string description)
        {
            string compositeData = String.Format("{0}|{1}|{2}",
                loanId.ToString(),
                docTypeId.ToString(),
                description
                );

            // Encrypting because this piece of data as a barcode on a piece
            // of paper will have a life outside our system.
            return LendersOffice.Common.EncryptionHelper.Encrypt(compositeData);
        }

        // This is only public for debug/test purposes, should have better mechanism
        public static string BuildCompositeStringForEDocs(Guid loanId, Guid appId, int docTypeId, string description)
        {
            string compositeData = String.Format("{0}|{1}|{2}|{3}",
                loanId.ToString(),
                appId.ToString(),
                docTypeId.ToString(),
                description
                );

            // Encrypting because this piece of data as a barcode on a piece
            // of paper will have a life outside our system.
            return LendersOffice.Common.EncryptionHelper.Encrypt(compositeData);
        }

        // This is only public for debug/test purposes, should have better mechanism
        public static string BuildCompositeStringForConsumerPortalRequest(long cpRequestId, Guid brokerId)
        {
            string compositeData = string.Format("{0}|{1}|{2}|{2}",
                cpRequestId.ToString(),
                brokerId,
                string.Empty);

            // Encrypting because this piece of data as a barcode on a piece
            // of paper will have a life outside our system.
            return LendersOffice.Common.EncryptionHelper.Encrypt(compositeData);
        }

        public static byte[] GetPageRange(byte[] pdfFile, int startPage, int endPage)
        {

            PdfReader reader = new PdfReader(pdfFile);
            try
            {
                if (reader.NumberOfPages == 1) return pdfFile;

                reader.SelectPages(startPage.ToString() + "-" + endPage.ToString());

                int pages = reader.NumberOfPages;

                Document document = new Document();

                using (MemoryStream memStream = new MemoryStream())
                {

                    PdfCopy copy = new PdfCopy(document, memStream);
                    try
                    {
                        document.Open();

                        for (int i = 0; i < pages; )
                        {
                            ++i;
                            copy.AddPage(copy.GetImportedPage(reader, i));
                        }
                        document.Close();
                        return memStream.ToArray();
                    }
                    finally
                    {
                        copy.Close();
                    }
                }
            }
            finally
            {
                reader.Close();
            }
        }
    }
}
