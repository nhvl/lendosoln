﻿using System;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;
using System.Collections;
using LendersOffice.Security;
using System.Collections.Generic;
using System.Threading;
using System.Data;
using System.Runtime.Serialization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Caching;

namespace EDocs
{
    
    public class EDocumentDocType
    {
        public const string BORROWER_UPLOAD_DOCTYPE = "BORROWER UPLOAD";
        public const string PARTNER_UPLOAD_DOCTYPE = "INTEGRATION UPLOAD";
        public const string ESIGNED_UPLOAD_DOCTYPE = "ESIGNED UPLOAD";
        public const string UNCLASSIFIED_DOCTYPE = "UNCLASSIFIED";
        private bool m_InLoAdmin;

        private Guid m_BrokerId
        {
            get
            {
                AbstractUserPrincipal broker = Thread.CurrentPrincipal as AbstractUserPrincipal;

                if (broker == null)
                {
                    return Guid.Empty;
                }

                return broker.BrokerId;
            }
        }

        public void AddDocType(string docTypeName, int folderId, Guid brokerId, int classificationId, int? esignTargetDocTypeId)
        {
            SqlParameter[] parameters = {
                new SqlParameter("@DocTypeName", docTypeName.ToUpper().TrimWhitespaceAndBOM()),
                new SqlParameter("@FolderId", folderId),
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@ClassificationId", classificationId),
                new SqlParameter("@EsignTargetDocTypeId", esignTargetDocTypeId)
            };
            PerformOperation(brokerId, "EDOCS_DocTypeAdd", parameters);
        }

        public void AddDocType(string docTypeName, int folderId, Guid brokerId)
        {
            SqlParameter[] parameters = {
                new SqlParameter("@DocTypeName", docTypeName.ToUpper().TrimWhitespaceAndBOM()),
                new SqlParameter("@FolderId", folderId),
                new SqlParameter("@BrokerId", brokerId)
            };
            PerformOperation(brokerId, "EDOCS_DocTypeAdd", parameters);
        }

        // db - Needed to be called from LoAdmin for consumer portal features migration
        public static int AddDocType(string docTypeName, Guid brokerId, CStoredProcedureExec exce)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@DocTypeName", docTypeName.ToUpper().TrimWhitespaceAndBOM()));
            parameters.Add(new SqlParameter("@BrokerId", brokerId));


            string procedureName = "INTERNAL_USE_ONLY_DocTypeAdd";
            using (DbDataReader dr = StoredProcedureHelper.ExecuteReader(brokerId, procedureName + "CheckName", parameters))
            {
                if (dr.HasRows)
                {
                    throw new DuplicateNameSelectedException(ErrorMessages.EDocs.DocTypeNameInUse);
                }
            }

            parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@DocTypeName", docTypeName.ToUpper().TrimWhitespaceAndBOM()));
            parameters.Add(new SqlParameter("@BrokerId", brokerId));

            SqlParameter idParam = new SqlParameter("@DocumentId", -1);
            idParam.Direction = ParameterDirection.Output;
            parameters.Add(idParam);
            
            exce.ExecuteNonQuery(procedureName, 3, parameters.ToArray());
            return (int)idParam.Value;
        }

        public void DeleteDocType(int DocTypeId)
        {
            Guid brokerId = Guid.Empty;

            if (!m_InLoAdmin)
            {
                brokerId = m_BrokerId;
            }

            DeleteDocType(DocTypeId, brokerId);
        }

        public void DeleteDocType(int DocTypeId, Guid brokerId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@DocTypeId", DocTypeId));

            if (brokerId != Guid.Empty)
            {
                parameters.Add(new SqlParameter("@BrokerId", brokerId));
            }

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "EDOCS_DeleteDocType", 3, parameters);
        }

        /// <summary>
        /// Updates or adds a new Document Type Mapping from a source doc type to the destination ESign doc type.
        /// </summary>
        /// <param name="SourceDocTypeId">The source document type ID.</param>
        /// <param name="ESignDocTypeId">The destination document type ID for documents returned from ESign.</param>
        public static void SaveEsignDocumentTypeMapping(int SourceDocTypeId, int? ESignDocTypeId, Guid brokerId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@SourceDocTypeId", SourceDocTypeId));
            parameters.Add(new SqlParameter("@EsignDocTypeId", ESignDocTypeId));

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "EDOCS_DOCUMENT_TYPE_MAPPING_AddOrUpdate", 3, parameters);
        }

        /// <summary>
        /// Retrieves all the doctypes the user has access to. Sorted by Folder -> DocTYpeName 
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public static IEnumerable<DocType> GetDocTypesByBroker(Guid brokerId, E_EnforceFolderPermissions enforceFolderPermissionsT)
        {
            bool enforcePermissions = true;
            switch(enforceFolderPermissionsT)
            {
                case E_EnforceFolderPermissions.False:
                    enforcePermissions = false;
                    break;
                case E_EnforceFolderPermissions.True:
                    enforcePermissions = true;
                    break;
                default:
                    throw new UnhandledEnumException(enforceFolderPermissionsT);
            }

            return GetDocTypesByBroker(brokerId, enforcePermissions);
        }

        public static IEnumerable<DocType> GetDocTypesByBroker(Guid brokerId, bool EnforcePermissions)
        {
            if (HttpContext.Current == null)
            {
                return GetDocTypesByBrokerImpl(brokerId).Where(p => !EnforcePermissions || p.Folder.CanRead()).OrderBy(p => p.FolderAndDocTypeName);
            }

            string key = String.Concat("GetDocTypesByBroker_", brokerId.ToString("N"));
            
            IEnumerable<DocType> docTypes;
            if (false == HttpContext.Current.Items.Contains(key))
            {
                docTypes = GetDocTypesByBrokerImpl(brokerId);
                HttpContext.Current.Items.Add(key, docTypes);
            }
            else
            {
                docTypes = (IEnumerable<DocType>)HttpContext.Current.Items[key];
            }

            return docTypes.Where(p => !EnforcePermissions || p.Folder.CanRead()).OrderBy(p => p.FolderAndDocTypeName);
        }

        /// <summary>
        /// Lists all doc types in broker
        /// </summary>
        /// <param name="brokerId">Broker id</param>
        /// <param name="EnforcePermissions">False if you don't want to enforce folder permissions; use only if you have to</param>
        /// <returns></returns>
        private static IEnumerable<DocType> GetDocTypesByBrokerImpl(Guid brokerId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId)
                                        };

            List<DocType> docTypes = new List<DocType>();
            Dictionary<int, EDocumentFolder> folders = new Dictionary<int,EDocumentFolder>();
            Dictionary<int, List<EDocRoles>> rolesByFolder = new Dictionary<int,List<EDocRoles>>();

            using (DbDataReader dr = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_GetDocTypesByBroker", parameters))
            {
                //read the folders and permissions first
                while (dr.Read())
                {
                    int folderId = (int)dr["FolderId"];
                    string folderName = (string)dr["FolderName"];
                   
                    EDocumentFolder folder;
                    List<EDocRoles> roles;
                    if (!folders.TryGetValue(folderId, out folder))
                    {
                        folder = new EDocumentFolder(brokerId, folderId, folderName);
                        folders.Add(folderId, folder);
                        rolesByFolder.Add(folderId, new List<EDocRoles>());
                    }

                    roles = rolesByFolder[folderId];

                    if (DBNull.Value != dr["roleid"])
                    {
                        bool ispmluser = (bool)dr["ispmluser"];
                        Guid roleid = (Guid)dr["roleid"];

                        EDocRoles role = new EDocRoles();
                        role.RoleId = roleid;
                        role.IsPmlUser = ispmluser; 
                        roles.Add(role);
                    }
                }

                foreach (var item in rolesByFolder)
                {
                    EDocumentFolder folder = folders[item.Key];
                    folder.SetFolderRoles(item.Value);
                }

                if (dr.NextResult())
                {
                    while (dr.Read())
                    {
                        int folderId = (int)dr["FolderId"];
                        EDocumentFolder folder = folders[folderId];
                    

                        DocType dt = FromReader(dr, folder);
                        docTypes.Add(dt);
                    }
                }
            }

            return docTypes;
        }

        public static Dictionary<string, string> GetDocTypesNamesByClassificationName(Guid brokerId, E_EnforceFolderPermissions enforcePermissions = E_EnforceFolderPermissions.True)
        {
            return GetDocTypesByClassification(brokerId, enforcePermissions).ToDictionary(a => a.Key.Name, a => a.Value.DocTypeName, StringComparer.OrdinalIgnoreCase);
        }

        public static Dictionary<string, DocType> GetDocTypesByClassificationName(Guid brokerId, E_EnforceFolderPermissions enforcePermissions = E_EnforceFolderPermissions.True)
        {
            return GetDocTypesByClassification(brokerId, enforcePermissions).ToDictionary(a => a.Key.Name, a => a.Value, StringComparer.OrdinalIgnoreCase);
        }

        public static Dictionary<EdocClassification, DocType> GetDocTypesByClassification(Guid brokerId, E_EnforceFolderPermissions enforcePermissions = E_EnforceFolderPermissions.True)
        {
            return GetDocTypesByBroker(brokerId, enforcePermissions)
                    .Where(a => a.EDocClassification.Id != EdocClassification.NoClassification.Id)
                    .ToDictionary(a => a.EDocClassification, a => a);
        }

        private void PerformOperation(Guid brokerId, string procedureName, IEnumerable<SqlParameter> parameters)
        {
            using (DbDataReader dr = StoredProcedureHelper.ExecuteReader(brokerId, procedureName + "CheckName", parameters.ToList()))
            {
                if (dr.HasRows)
                    throw new DuplicateNameSelectedException(ErrorMessages.EDocs.DocTypeNameInUse);
            }

            StoredProcedureHelper.ExecuteNonQuery(brokerId, procedureName, 3, parameters.ToList());
        }


        
        public EDocumentDocType()
        {
            AbstractUserPrincipal broker = Thread.CurrentPrincipal as AbstractUserPrincipal;
            
            if(broker != null && broker.Type == "I")
                m_InLoAdmin = true;
            else
                m_InLoAdmin = false;
        }

        public static DocType GetDocTypeById(Guid brokerId, int docTypeId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@DocTypeId", docTypeId)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_GetDocTypesById", parameters))
            {
                if (reader.Read())
                {
                    return FromReader(brokerId, reader);
                }
            }

            throw new DocTypeNotFoundException("Doc type " + docTypeId + " not found.");
        }

        /// <summary>
        /// Gets the document type corresponding to the given document type ID.
        /// </summary>
        /// <param name="brokerId">The broker's ID, only for choosing which database to query. Doesn't </param>
        /// <returns>An object conaining information about the document type, or null if no document types were found with the given ID.</returns>
        public static DocType GetNullableDocTypeById(Guid brokerId, int docTypeId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@DocTypeId", docTypeId)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_GetDocTypesById", parameters))
            {
                if (reader.Read())
                {
                    return FromReader(brokerId, reader);
                }
            }

            return null;
        }

        internal static DocType FromReader(DbDataReader dr, EDocumentFolder folder)
        {
            return new DocType()
            {
                Folder = folder,
                DocTypeName = dr["DocTypeName"].ToString(),
                DocTypeId = dr["DocTypeId"].ToString(),
                EDocClassification = EdocClassification.Retrieve(dr["ClassificationId"] as int?),
                DefaultPage = (E_AutoSavePage)Convert.ToInt32(dr["DefaultForPageId"]),
                ESignTargetDocTypeId = dr.GetNullableInt("ESignTargetDocTypeId")
            };
        }

        private static DocType FromReader(Guid brokerId, DbDataReader dr)
        {
            var dt = FromReader(dr, null);
            dt.Folder = new EDocumentFolder(brokerId, (int)dr["FolderId"]);
            return dt;
        }

        public static IEnumerable<DocType> GetStackOrderedDocTypesByBroker(LendersOffice.Admin.BrokerDB broker)
        {
            IEnumerable<DocType> sortedDocTypes, docTypes;
            IEnumerable<int> sortedDocTypeIDs;
            IDictionary<int, DocType> docTypeByID;
            HashSet<int> docTypeIDs;

            sortedDocTypeIDs = broker.EdocsStackOrderList.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Distinct().Select((s)=> Int32.Parse(s)).ToList(); // may want to not use Distinct.
            docTypes = GetDocTypesByBroker(broker.BrokerID, E_EnforceFolderPermissions.True).ToList();
            docTypeByID = docTypes.ToDictionary((docType) => Int32.Parse(docType.DocTypeId));
            docTypeIDs = new HashSet<int>(sortedDocTypeIDs);

            IEnumerable<DocType> missingDocTypes = (from d in docTypes.Where((d) => !docTypeIDs.Contains(Int32.Parse(d.DocTypeId)))
                                                          orderby d.Folder.FolderNm, d.DocTypeName
                                                          select d).ToList();
            sortedDocTypes = sortedDocTypeIDs.Where((i) => docTypeByID.ContainsKey(i)).Select((p) => docTypeByID[p]).ToList();
            return sortedDocTypes.Concat(missingDocTypes).ToList();
        }

        private static int GetOrCreateId(Guid BrokerId, string doctypeName, Func<Guid, int> GetBrokerFolderId)
        {
            bool isDeleted;
            int result = DocType.GetDocTypeIdFromName(BrokerId, doctypeName, out isDeleted);

            //If it already exists, just return it (-1 is it doesn't exist)
            if (result != -1) return result;

            //Otherwise, it doesn't exist so we need to create it
            int folderId = GetBrokerFolderId(BrokerId);

            EDocumentDocType temp = new EDocumentDocType();
            temp.AddDocType(doctypeName, folderId, BrokerId);

            return DocType.GetDocTypeIdFromName(BrokerId, doctypeName, out isDeleted);
        }

        /// <summary>
        /// Given the brokerId, it will attempt to retrieve the broker's 'Unclassified" folder and doctype.  If it does not exist,
        /// it will create it, and return that doctype.
        /// </summary>
        /// <param name="brokerId">The Broker Id.</param>
        /// <returns>The doctype id.</returns>
        public static int GetOrCreateUnclassifiedDocType(Guid brokerId)
        {
            return GetOrCreateDocType(brokerId, UNCLASSIFIED_DOCTYPE, EDocumentFolder.UNCLASSIFIED_FOLDERNAME);
        }

        private static int GetOrCreateUnclassifiedFolderId(Guid BrokerId)
        {
            return GetOrCreateFolder(BrokerId, EDocumentFolder.UNCLASSIFIED_FOLDERNAME);
        }

        private static int GetOrCreateGeneratedFolderId(Guid BrokerId)
        {
            return GetOrCreateFolder(BrokerId, EDocumentFolder.GENERATED_FOLDERNAME);
        }

        private static int GetOrCreateFolder(Guid BrokerId, string FolderName)
        {
            if (!EDocumentFolder.FolderExists(BrokerId, FolderName))
            {
                return EDocumentFolder.CreateAllRolesFolderAndSave(BrokerId, FolderName);
            }
            else
            {
                return EDocumentFolder.GetFolderId(BrokerId, FolderName);
            }
        }

        public static int GetOrCreateBorrowerUpload(Guid BrokerId)
        {
            return GetOrCreateId(BrokerId, BORROWER_UPLOAD_DOCTYPE, GetOrCreateUnclassifiedFolderId);
        }

        public static int GetOrCreatePartnerUpload(Guid BrokerId)
        {
            return GetOrCreateId(BrokerId, PARTNER_UPLOAD_DOCTYPE, GetOrCreateUnclassifiedFolderId);
        }

        public static int GetOrCreatePackageDocType(Guid brokerId, string packageName)
        {
            return GetOrCreateId(brokerId, packageName.ToUpper(), GetOrCreateGeneratedFolderId);
        }

        public static int GetOrCreateDocType(Guid BrokerId, string DocTypeName, string FolderName)
        {
            return GetOrCreateId(BrokerId, DocTypeName, GetOrCreateFolderByName(FolderName));
        }

        private static Func<Guid, int> GetOrCreateFolderByName(string folderName)
        {
            return a => GetOrCreateFolder(a, folderName);
        }
    }

    [DataContract]
    public class DocType
    {
        [DataMember]
        public string DocTypeId { get; set; }

        /// <summary>
        /// Todo: replace a lot of references to the current DocTypeId with this, 
        /// (all of the ones that are basically int.Parse(DocTypeId))
        /// and make sure the id is carried as an int (that's what it is in the database)
        /// Note that since DocTypeId is a [DataMember], we can't really change its name.
        /// </summary>
        public int Id { get { return int.Parse(DocTypeId); } }

        public string DocTypeName
        {
            get
            {
                return m_docTypeName;
            }
            set
            {
                if (value.ToLower() != m_docTypeName.ToLower())
                {
                    if (m_docTypeName != "")
                    {
                        m_docTypeNameSet = true;
                    }
                    m_docTypeName = value;
                }
            }
        }

        public EdocClassification EDocClassification { get; set; }

        public EDocumentFolder Folder { get; set; }
        private bool m_docTypeNameSet = false;
        private string m_docTypeName = "";

        [DataMember]
        public string FolderAndDocTypeName
        {
            get
            {
                return Folder.FolderNm + " : " + DocTypeName;
            }
            // Setter is necessary for serialization
            private set {}
        }

        public E_AutoSavePage DefaultPage
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the ID of the document type that ESigned documents should be saved back to.
        /// </summary>
        public int? ESignTargetDocTypeId { get; set; }

        /// <summary>
        /// Save EDocs Doc Type using Principal's brokerId.
        /// </summary>
        public void Save()
        {
            AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;
            Save(principal.BrokerId);
        }

        /// <summary>
        /// Save EDocs Doc Type.
        /// </summary>
        /// <param name="brokerId">A valid broker Id</param>
        public void Save(Guid brokerId)
        {
            if (m_docTypeNameSet)
            {
                List<SqlParameter> cnparameters = new List<SqlParameter>();
                cnparameters.Add(new SqlParameter("@BrokerId", brokerId));
                cnparameters.Add(new SqlParameter("@DocTypeName", DocTypeName));
                using (DbDataReader dr = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_UpdateDocTypeCheckName", cnparameters))
                {
                    if (dr.HasRows)
                        throw new DuplicateNameSelectedException(ErrorMessages.EDocs.DocTypeNameInUse);
                }
            }

            if (DocTypeName.TrimWhitespaceAndBOM().Equals(""))
            {
                throw new CBaseException("Folder name cannot be blank", "Folder name cannot be blank");
            }

            SqlParameter[] parameters = {
                new SqlParameter("@DocTypeId", DocTypeId),
                new SqlParameter("@DocTypeName", DocTypeName),
                new SqlParameter("FolderId", Folder.FolderId),
                new SqlParameter("@ClassificationId", EDocClassification.Id),
                new SqlParameter("@DefaultForPageId", DefaultPage),
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@ESignTargetDocTypeId", this.ESignTargetDocTypeId)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "EDOCS_UpdateDocType", 3, parameters);

            m_docTypeNameSet = false;
        }



        public static IEnumerable<DocType> GetDeletedDocTypes(Guid brokerId)
        {
            List<DocType> docTypes = new List<DocType>();
            Dictionary<int, EDocumentFolder> folders = new Dictionary<int, EDocumentFolder>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_GetDeletedDocTypesByBroker", parameters))
            {
                while (reader.Read())
                {
                    EDocumentFolder folder;

                    if (false == folders.TryGetValue((int)reader["FolderId"], out folder))
                    {
                        folder = new EDocumentFolder(brokerId, reader);
                        folders.Add(folder.FolderId, folder);
                    }

                    docTypes.Add(EDocumentDocType.FromReader(reader, folder));
                }
            }

            return docTypes;
        }

        public static void RestoreDocType(Guid brokerId, int id)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@DocTypeId", id)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "EDOCS_RestoreDocType", 3, parameters );
        }

        public static int GetDocTypeIdFromName(Guid brokerId, string name, out bool isDeleted ) 
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@Name", name)
                                        };
            isDeleted = false;
            using( DbDataReader reader = 
                StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_GetDocTypeIdFromName", parameters))
            {
                if ( reader.Read() ) 
                {
                    isDeleted = !(bool)reader["IsValid"];
                    return (int)reader["DocTypeId"];
                }
            }

            return -1;
        }        
    }
}