﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using LendersOffice.Security;
using System.Web;
using DataAccess;
using System.Threading;
using LendersOffice.HttpModule;
using PdfRasterizerLib;
using LendersOffice.Constants;
using System.IO;
using LendersOffice.ObjLib.Edocs;
using LendersOffice.Drivers.Gateways;

namespace EDocs
{
    public class EDocumentViewerHandler : System.Web.IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(System.Web.HttpContext context)
        {
            try
            {
                PerformanceMonitorItem item = PerformanceMonitorItem.Current;
                if (null != item)
                {
                    // 6/23/2010 dd - Disabled Page Size Warning.
                    item.IsEnabledPageSizeWarning = false;
                }

                string cmd = RequestHelper.GetSafeQueryString("cmd");
                AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;

                if (principal is InternalUserPrincipal && RequestHelper.GetInt("a", -1) == 1)
                {
                    ViewLostDocumentById(context.Response, RequestHelper.GetGuid("id"));
                    return;
                }

                Guid docId = RequestHelper.GetGuid("docid", Guid.Empty);

                if (cmd == "xml")
                {
                    EDocumentRepository repository = EDocumentRepository.GetUserRepository();
                    var doc = repository.GetGenericDocumentById(docId);
                    context.Response.AddHeader("Content-Disposition", string.Format(@"attachment; filename=""{0}""", doc.FileNm));
                    context.Response.ContentType = doc.MIMEType;
                    context.Response.WriteFile(doc.GetContentPath());
                    context.Response.Flush();
                    context.Response.End();
                    return;
                }
                else if (cmd == "date")
                {
                    context.Response.Cache.SetExpires(DateTime.Now);
                    context.Response.Cache.SetLastModified(DateTime.Now);
                    context.Response.Clear();
                    context.Response.ContentType = "image/png";
                    context.Response.WriteFile(EDocumentViewer.GeneratePngForToday());
                    context.Response.Flush();
                    context.Response.End();
                    return;
                }
                else if (cmd == "esign")
                {
                    Guid brokerId = principal.BrokerId;
                    LenderDisclosurePDFGenerator lde = new LenderDisclosurePDFGenerator(brokerId);
                    GeneratePDFFromFile(context.Response, lde.SavePdf(), "eSign Disclosure");
                }
                else if (cmd == "del")
                {
                    ViewByDocId(context.Response, principal, docId, false, true);
                }
                else if (cmd == "viewpdfwithnotes")
                {
                    ViewInternalNotesByDocId(context.Response, principal, docId);
                }
                else if (cmd == "multipdf")
                {
                    var sLId = RequestHelper.GetGuid("sLId");
                    var cacheKey = RequestHelper.GetSafeQueryString("ckey");
                    IEnumerable<Guid> ids = AutoExpiredTextCache.GetFromCache(cacheKey)?.Split('|')?.Select(id => Guid.Parse(id));
                    if (ids == null)
                    {
                        throw new CBaseException(ErrorMessages.Generic, $"Unable to get EDoc Ids for key {cacheKey}.");
                    }

                    if (principal.BrokerDB.IsTestingAmazonEdocs)
                    {
                        EDocumentRepository repo = EDocumentRepository.GetUserRepository();
                        Dictionary<Guid, EDocument> docsByLoanId = repo.GetDocumentsByLoanId(sLId).ToDictionary(p => p.DocumentId);

                        List<Guid> documentIds = new List<Guid>();
                        List<EDocument> documentList = new List<EDocument>();

                        foreach (var id in ids)
                        {
                            EDocument document = null;
                            if (docsByLoanId.TryGetValue(id, out document))
                            {
                                documentList.Add(document);
                            }
                            else
                            {
                                throw new FileNotFoundException("Document not found " + docId);
                            }
                        }

                        string token = AmazonEDocHelper.GenerateShippingPackageRequest(principal, "pdf", documentList);
                        context.Response.Redirect(ConstStage.AmazonEdocsClientUrl + "/edocshippingpackage.aspx?key=" + token);
                    }
                    else
                    {
                        var path = EDocumentViewer.WriteSinglePdf(sLId, ids.ToList());
                        RequestHelper.SendFileToClient(HttpContext.Current, path, "application/pdf", "OrderDocs.pdf");
                        FileOperationHelper.Delete(path);
                    }
                }
                else
                {
                    bool useOriginal = RequestHelper.GetInt("o", -1) == 1;
                    if (principal == null)
                    {
                        DisplayAccessDeniedMessage(context);
                    }

                    ViewByDocId(context.Response, principal, docId, useOriginal, false);
                }

            }
            catch (AccessDenied)
            {
                DisplayAccessDeniedMessage(context);
            }


        }

        #endregion
        private void ViewInternalNotesByDocId(HttpResponse response, AbstractUserPrincipal principal, Guid docId)
        {
            string key = RequestHelper.GetSafeQueryString("k");

            if (principal.BrokerDB.IsTestingAmazonEdocs)
            {
                response.Redirect(ConstStage.AmazonEdocsClientUrl + "/edocpdf.aspx?key=" + key);
            }
            else
            {

                string annotationXml = AutoExpiredTextCache.GetFromCache(key + "_AnnotationXml");
                string pdfPageItemListJson = AutoExpiredTextCache.GetFromCache(key + "_PdfPageItemListJson");

                EDocumentAnnotationContainer container = new EDocumentAnnotationContainer();
                container.LoadContent(annotationXml);

                EDocumentRepository repository = EDocumentRepository.GetUserRepository();
                EDocument document = repository.GetDocumentById(docId);

                List<PdfPageItem> pdfPageList = ObsoleteSerializationHelper.JsonDeserialize<List<PdfPageItem>>(pdfPageItemListJson);

                response.Cache.SetExpires(DateTime.Now);
                response.Cache.SetLastModified(DateTime.Now);
                response.Clear();
                response.ContentType = "application/pdf";
                response.AddHeader("Content-Disposition", "attachment; filename=" + document.DocTypeName + ".pdf");

                EDocument.GeneratePDFDataWithAnnotation(principal, pdfPageList, container, response.OutputStream);

                response.Flush();
                response.End();
            }
        }
        private void ViewByDocId(HttpResponse response, AbstractUserPrincipal principal, Guid docId, bool useOriginal, bool isDeleted)
        {
            EDocumentRepository repository = EDocumentRepository.GetUserRepository();
            EDocument document = isDeleted ? repository.GetDeletedDocumentById(docId) :  repository.GetDocumentById(docId);

            bool isUsingAmazon = false;

            if (principal.BrokerDB.IsTestingAmazonEdocs)
            {
                if (useOriginal && document.OriginalEdocsMetadata == null)
                {
                    // 5/19/2016 - dd - There are some eDocs that does not have non destructive for original version. These are the edocs that migrate from legacy.
                    isUsingAmazon = false;
                }
                else
                {
                    isUsingAmazon = true;
                }
            }

            if (isUsingAmazon)
            {
                RedirectToAmazon(response, document, useOriginal);
            }
            else
            {
                GeneratePdfEdoc(response, document, useOriginal);
            }
        }

        private void RedirectToAmazon(HttpResponse response, EDocument document, bool useOriginal)
        {
            string token = document.GenerateAmazonViewToken(useOriginal);

            response.Redirect(ConstStage.AmazonEdocsClientUrl + "/edocpdf.aspx?key=" + token);
        }

        private void GeneratePdfEdoc(HttpResponse response, EDocument document, bool useOriginal)
        {
            try
            {
                string path = useOriginal ? document.GetPDFTempFile_Original() : document.GetPDFTempFile_Current(); ;

                GeneratePDFFromFile(response, path, document.DocTypeName);
            }
            catch (NotFoundException)
            {
                string html = @"<html><body>Document Not found.</body></html>";
                response.Write(html);
                response.End();
            }
            catch (FileNotFoundException)
            {
                Tools.LogError("PDF Not Found For Document " + document.DocumentId);
                string html = @"<html><body>Document Not found</body></html>";
                response.Write(html);
                response.End();
            }
        }

        private void ViewLostDocumentById(HttpResponse response, Guid docId )
        {
            try
            {
                LostEDocument document = LostEDocument.GetDocumentById(docId);
                GeneratePdf(response, document.PDFData, docId.ToString());
            }
            catch (NotFoundException)
            {
                string html = @"<html><body>Not found</body></html>";
                response.Write(html);
                response.End();
            }
        }

        private void GeneratePdf(HttpResponse response, byte[] buffer, string fileName)
        {
            response.Cache.SetExpires(DateTime.Now);
            response.Cache.SetLastModified(DateTime.Now);
            response.Clear();
            response.ContentType = "application/pdf";
            response.AddHeader("Content-Disposition", "attachment; filename=" + fileName + ".pdf");
            response.OutputStream.Write(buffer, 0, buffer.Length);
            response.Flush();

            response.End();
        }

        private void GeneratePDFFromFile(HttpResponse response, string tempFileName, string userFileName)
        {
            response.Cache.SetExpires(DateTime.Now);
            response.Cache.SetLastModified(DateTime.Now);
            response.Clear();
            response.ContentType = "application/pdf";
            response.AddHeader("Content-Disposition", "attachment; filename=" + userFileName + ".pdf");
            
            response.Buffer = false;
            response.TransmitFile(tempFileName);
            response.End();

        }

        private void DisplayAccessDeniedMessage(System.Web.HttpContext context)
        {
            string html = @"<html><body>Access Denied</body></html>";
            context.Response.Write(html);
            context.Response.Flush();
            context.Response.End();
        }


        private void DisplayNotFoundException(System.Web.HttpContext context)
        {
            string html = @"<html><body>Document not found. It may have been deleted.</body></html>";
            context.Response.Write(html);
            context.Response.Flush();
            context.Response.End();
        }
    }
}
