﻿namespace EDocs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Provides conversion from a PDF form to an EDoc.
    /// </summary>
    public class PdfFormToEDocConversion
    {
        /// <summary>
        /// Matches a borrower or co-borrower signature, initial, or signature date field name.
        /// </summary>
        private static readonly Regex BorrowerSigningFieldRegex = new Regex(@"^a(?<BorC>B|C)(?<SignatureType>Signature|Initials|SignatureDate)$", RegexOptions.IgnoreCase);

        /// <summary>
        /// Matches an agent signature, initial, or signature date field name.
        /// </summary>
        private static readonly Regex AgentOfRoleSigningFieldRegex = new Regex(@"^GetAgentOfRole\[(?<Role>\w+)\]\.(?<SignatureType>Signature|Initials|SignatureDate)$", RegexOptions.IgnoreCase);

        /// <summary>
        /// Determines the correct value for <see cref="LendersOffice.PdfLayout.PdfField.Type"/> based on the name of the field.<para/>
        /// This is necessary because the CPEs will use an older version of Adobe Acrobat reader that
        /// does not support more precise field types that we want to use, and because we've elected
        /// to modify this here rather than alter David's PdfFormsMigrator.exe, which the CPEs use to
        /// create the XML from the standard PDF.
        /// </summary>
        /// <param name="name">The name of the field.</param>
        /// <param name="rawType">The raw type of the field as read from the XML.</param>
        /// <returns>The actual type of the field.</returns>
        public static LendersOffice.PdfLayout.PdfFieldType DeterminePdfFieldType(string name, LendersOffice.PdfLayout.PdfFieldType rawType)
        {
            if (name.EndsWith("Signature", StringComparison.OrdinalIgnoreCase))
            {
                if (StringComparer.OrdinalIgnoreCase.Equals(name, "aBSignature")
                    || StringComparer.OrdinalIgnoreCase.Equals(name, "aCSignature")
                    || AgentOfRoleSigningFieldRegex.IsMatch(name))
                {
                    return LendersOffice.PdfLayout.PdfFieldType.Signature;
                }
            }
            else if (name.EndsWith("Initials", StringComparison.OrdinalIgnoreCase))
            {
                if (StringComparer.OrdinalIgnoreCase.Equals(name, "aBInitials")
                    || StringComparer.OrdinalIgnoreCase.Equals(name, "aCInitials")
                    || AgentOfRoleSigningFieldRegex.IsMatch(name))
                {
                    return LendersOffice.PdfLayout.PdfFieldType.Initial;
                }
            }
            else if (name.EndsWith("SignatureDate", StringComparison.OrdinalIgnoreCase))
            {
                if (StringComparer.OrdinalIgnoreCase.Equals(name, "aBSignatureDate")
                    || StringComparer.OrdinalIgnoreCase.Equals(name, "aCSignatureDate")
                    || AgentOfRoleSigningFieldRegex.IsMatch(name))
                {
                    return LendersOffice.PdfLayout.PdfFieldType.SignatureDate;
                }
            }

            return rawType;
        }

        /// <summary>
        /// Converts a PDF layout to the equivalent set of annotations.
        /// </summary>
        /// <param name="layout">The layout to read through.</param>
        /// <returns>The list of annotations.</returns>
        public static IEnumerable<EDocumentAnnotationItem> CreateESignTagAnnotations(LendersOffice.PdfLayout.PdfFormLayout layout)
        {
            foreach (LendersOffice.PdfLayout.PdfField field in layout.FieldList)
            {
                EDocumentAnnotationItem annotation = ConvertToAnnotationItem(field);
                if (annotation != null)
                {
                    yield return annotation;
                }
            }
        }

        /// <summary>
        /// Converts a <see cref="LendersOffice.PdfLayout.PdfField"/> into the equivalent <see cref="EDocumentAnnotationItem"/>,
        /// returning null if no conversion can be performed.
        /// </summary>
        /// <param name="field">The field to convert.</param>
        /// <returns>The field as an annotation item, or null.</returns>
        private static EDocumentAnnotationItem ConvertToAnnotationItem(LendersOffice.PdfLayout.PdfField field)
        {
            E_EDocumentAnnotationItemType itemType = default(E_EDocumentAnnotationItemType);
            if (field.Type == LendersOffice.PdfLayout.PdfFieldType.Signature)
            {
                itemType = E_EDocumentAnnotationItemType.Signature;
            }
            else if (field.Type == LendersOffice.PdfLayout.PdfFieldType.Initial)
            {
                itemType = E_EDocumentAnnotationItemType.Initial;
            }
            else if (field.Type == LendersOffice.PdfLayout.PdfFieldType.SignatureDate)
            {
                itemType = E_EDocumentAnnotationItemType.SignedDate;
            }
            else
            {
                return null;
            }

            Tuple<E_BorrowerModeT?, E_AgentRoleT?> fieldData = GetSigningFieldSignerInformation(field.Name);
            E_BorrowerModeT? associatedBorrower = fieldData.Item1;
            E_AgentRoleT? associatedRole = fieldData.Item2;

            if (associatedBorrower == null && associatedRole == null)
            {
                return null;
            }

            const string CreatedBy = "System";
            return new EDocumentAnnotationItem()
            {
                Id = Guid.NewGuid(),
                ItemType = itemType,
                CreatedBy = CreatedBy,
                LastModifiedBy = CreatedBy,
                Rectangle = field.Rectangle,
                PageNumber = field.PageNumber,
                AssociatedBorrower = associatedBorrower,
                AssociatedRole = associatedRole,
            };
        }

        /// <summary>
        /// Gets the signer information from the name of a signing fields.
        /// </summary>
        /// <param name="name">The name of the field.</param>
        /// <returns>The borrower mode or agent role, if either is found, or an empty tuple.</returns>
        private static Tuple<E_BorrowerModeT?, E_AgentRoleT?> GetSigningFieldSignerInformation(string name)
        {
            E_BorrowerModeT? borrMode = default(E_BorrowerModeT?);
            E_AgentRoleT? role = default(E_AgentRoleT?);
            var borrowerMatch = BorrowerSigningFieldRegex.Match(name);
            if (borrowerMatch.Success)
            {
                borrMode = StringComparer.OrdinalIgnoreCase.Equals(borrowerMatch.Groups["BorC"].Value, "B")
                    ? E_BorrowerModeT.Borrower
                    : E_BorrowerModeT.Coborrower;
            }
            else
            {
                var agentMatch = AgentOfRoleSigningFieldRegex.Match(name);
                if (agentMatch.Success)
                {
                    role = agentMatch.Groups["Role"].Value.ToNullableEnum<E_AgentRoleT>(ignoreCase: true);
                }
            }

            return Tuple.Create(borrMode, role);
        }
    }
}
