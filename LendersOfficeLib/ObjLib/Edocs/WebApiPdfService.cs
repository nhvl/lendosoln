﻿namespace EDocs
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using LendersOffice.Common;
    using LendersOffice.Constants;

    /// <summary>
    /// This class will invoke a local pdf webapi. This will be prefer method because it will allow us to put iTextSharp operation in separate process. If
    /// our old iTextSharp crash or cause StackOverflow then the error is isolate. See case 478659.
    /// </summary>
    public class WebApiPdfService : IPdfService
    {
        /// <summary>
        /// Generate a pdf and return a local path of new pdf.
        /// </summary>
        /// <param name="metaData">Metadata of an edocs to generate PDF.</param>
        /// <returns>The path of the output pdf.</returns>
        public string GeneratePdf(EdocsMetadata metaData)
        {
            PdfGenerateRequest request = new PdfGenerateRequest
            {
                EdocsMetadata = metaData,
                FilePathDictionary = new Dictionary<Guid, string>()
            };

            foreach (var page in metaData.Pages)
            {
                if (!request.FilePathDictionary.ContainsKey(page.ReferenceDocumentId))
                {
                    string path = EDocument.GetNonDestructivePdfFromFileDb(page.ReferenceDocumentId);
                    request.FilePathDictionary.Add(page.ReferenceDocumentId, path);
                }
            }

            var jsonData = SerializationHelper.JsonNetSerialize(request);
            return this.InvokeITextSharpWebApi("/api/pdf/generate", jsonData);
        }

        /// <summary>
        /// Invoke the web api.
        /// </summary>
        /// <param name="path">The path of web api.</param>
        /// <param name="jsonData">Content to be post in the body.</param>
        /// <returns>Result from web api.</returns>
        private string InvokeITextSharpWebApi(string path, string jsonData)
        {
            using (var webClient = new WebClient())
            {
                webClient.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                var requestUrl = ConstStage.ITextSharpWebApiUrl + path;
                string responseJsonData = webClient.UploadString(requestUrl, jsonData);

                return SerializationHelper.JsonNetDeserialize<string>(responseJsonData);
            }
        }
    }
}