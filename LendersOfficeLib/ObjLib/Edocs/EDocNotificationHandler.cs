﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Constants;
using LendersOffice.ObjLib.DatabaseMessageQueue;
using DataAccess;

namespace EDocs
{
    public class EDocNotificationHandler
    {
        private DBMessageQueue m_mQ = new DBMessageQueue(ConstMsg.EDocsQueue);
        
        public void Process()
        {
            int cutoffHours = ConstStage.EDocNotificationCutoffInMinutes / 60;
            int cutoffMinutes = ConstStage.EDocNotificationCutoffInMinutes % 60;
            DateTime emailCutoff = DateTime.Now.Subtract(new TimeSpan(cutoffHours, cutoffMinutes, 0));
            Dictionary<string, List<string>> edocDocumentIDsByLoanID = new Dictionary<string, List<string>>();
            Dictionary<string, List<long>> msgIdsToBeDeletedByLoanID = new Dictionary<string, List<long>>();
            
            foreach (DBMessage msg in m_mQ.Entries)
            {
                try
                {
                    if (edocDocumentIDsByLoanID.ContainsKey(msg.Subject1)) // loanID
                    {
                        edocDocumentIDsByLoanID[msg.Subject1].Add(msg.Subject2); // docID
                        msgIdsToBeDeletedByLoanID[msg.Subject1].Add(msg.Id);
                    }
                    else if (DateTime.Compare(msg.InsertionTime, emailCutoff) < 0)
                    {
                        List<string> newList = new List<string>();
                        newList.Add(msg.Subject2);
                        edocDocumentIDsByLoanID.Add(msg.Subject1, newList);
                        msgIdsToBeDeletedByLoanID.Add(msg.Subject1, new List<long>() { msg.Id });
                    }
                }
                catch (Exception e)
                {
                    Tools.LogError(e);
                }
            }

            foreach (KeyValuePair<string, List<string>> pair in edocDocumentIDsByLoanID)
            {
                try
                {
                    EDocumentNotifier.SendNewEDocumentNotification(pair.Value); // docID list
                    foreach (long id in msgIdsToBeDeletedByLoanID[pair.Key])
                    {
                        m_mQ.ReceiveById(id);
                        m_mQ.DeleteFromArchive(id);
                    }
                }
                catch (Exception e)
                {
                    Tools.LogError(e);
                }
            }
        }
    }
}
