﻿// <copyright file="EdocsMetadata.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is EdocsMetadata class.
// Author: David D
// Date: 6/18/2015
// </summary>
namespace EDocs
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// JSON/Protobuf representation of the Non Destructive <code>Edocs</code>. I used .NET serialization attribute instead of Json.net
    /// and Protobuf-net because it require less code and remove dependency on the serialization library.
    /// Note: Protobuf protocol require numeric ordering. Therefore DO NOT change the numeric once define.
    /// </summary>
    [DataContract]
    public class EdocsMetadata
    {
        /// <summary>
        /// An order list of the pages for this <code>Edocs</code>.
        /// </summary>
        [DataMember(Name = "pages", Order = 1)]
        private List<EdocsPageMetadata> pages = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="EdocsMetadata" /> class.
        /// </summary>
        public EdocsMetadata()
        {
            this.pages = new List<EdocsPageMetadata>();
        }

        /// <summary>
        /// Gets or sets the original document id of the meta data. This attribute is optional. It is only use to retrieve original PDF while PDF rasterizer is taking place.
        /// </summary>
        /// <value>The original document id.</value>
        [DataMember(Name = "original_document_id", Order = 2, EmitDefaultValue = false)]
        public Guid OriginalDocumentId { get; set; }

        /// <summary>
        /// Gets a list of pages.
        /// </summary>
        /// <value>A list of page.</value>
        public IEnumerable<EdocsPageMetadata> Pages
        {
            get { return this.pages; }
        }

        /// <summary>
        /// Add a page to the <code>Edocs</code>.
        /// </summary>
        /// <param name="page">Page to be add.</param>
        public void AddPage(EdocsPageMetadata page)
        {
            this.pages.Add(page);
        }
    }
}