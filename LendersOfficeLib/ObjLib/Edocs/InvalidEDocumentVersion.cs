﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Common;

namespace EDocs
{
    public class InvalidEDocumentVersion : CBaseException
    {
        public InvalidEDocumentVersion(int expectedVersion, int actualVersion) :
            base(ErrorMessages.EDocs.InvalidVersion, "Expected Version=" + expectedVersion + ". Actual Version=" + actualVersion) 
        {
            this.IsEmailDeveloper = false;
        }

        public InvalidEDocumentVersion(Exception e)
            : base(ErrorMessages.EDocs.InvalidVersion, e) 
        {
            this.IsEmailDeveloper = false;
        }
    }
}
