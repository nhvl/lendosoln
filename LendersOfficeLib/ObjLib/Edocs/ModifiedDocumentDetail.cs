﻿namespace EDocs
{
    using System;

    /// <summary>
    /// A plain CLR object used to contain details about recently modified documents.
    /// </summary>
    public class ModifiedDocumentDetail
    {
        /// <summary>
        /// Gets or sets the loan id.
        /// </summary>
        /// <value>The loan id.</value>
        public Guid LoanId { get; set; }

        /// <summary>
        /// Gets or sets the loan name.
        /// </summary>
        /// <value>The loan name.</value>
        public string LoanName { get; set; }

        /// <summary>
        /// Gets or sets the folder name.
        /// </summary>
        /// <value>The folder name.</value>
        public string FolderName { get; set; }

        /// <summary>
        /// Gets or sets the doc type name.
        /// </summary>
        /// <value>The doc type name.</value>
        public string DocTypeName { get; set; }

        /// <summary>
        /// Gets or sets the document id.
        /// </summary>
        /// <value>The document id.</value>
        public Guid DocumentId { get; set; }

        /// <summary>
        /// Gets or sets the borrower's first name.
        /// </summary>
        /// <value>The borrower first name.</value>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the borrrower's last name.
        /// </summary>
        /// <value>The borrower's last name.</value>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the document description.
        /// </summary>
        /// <value>The document description.</value>
        public string Comments { get; set; }

        /// <summary>
        /// Gets or sets the document description.
        /// </summary>
        /// <value>The document description.</value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        /// <value>The document comments.</value>
        public int Pages { get; set; }

        /// <summary>
        /// Gets or sets the document's last modified date.
        /// </summary>
        /// <value>The last modified date.</value>
        public DateTime LastModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the document is hidden from PML users.
        /// </summary>
        /// <value>A value indicating whether the document is hidden from P users.</value>
        public bool HideFromPmlUsers { get; set; }

        /// <summary>
        /// Gets or sets the document type id.
        /// </summary>
        /// <value>The document type id.</value>
        public int DocTypeId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether document is editable.
        /// </summary>
        /// <value>The document is editable.</value>
        public bool IsEditable { get; set; }

        /// <summary>
        /// Gets the borrower's full name.
        /// </summary>
        /// <value>The borrower's full name.</value>
        public string BorrowerFullName
        {
            get
            {
                return string.Concat(this.FirstName, " ", this.LastName);
            }
        }
    }
}
