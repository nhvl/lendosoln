﻿namespace EDocs
{
    /// <summary>
    /// Define a list of PDF operations.
    /// </summary>
    public interface IPdfService
    {
        /// <summary>
        /// Generate a pdf and return a local path of new pdf.
        /// </summary>
        /// <param name="metaData">Metadata of an edocs to generate PDF.</param>
        /// <returns>The path of the output pdf.</returns>
        string GeneratePdf(EdocsMetadata metaData);
    }
}
