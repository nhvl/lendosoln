﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;
using System.Threading;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using System.Collections;
using System.Data;
using LendersOffice.Constants;
using LendersOffice.Common;
using LendersOffice.Admin;

namespace EDocs
{
    public class EDocumentFolderComparer : IComparer<EDocumentFolder>
    {
        #region IComparer<EDocumentFolder> Members

        public int Compare(EDocumentFolder x, EDocumentFolder y)
        {
            if (x.FolderNm.ToUpper().Equals(EDocumentFolder.UNCLASSIFIED_FOLDERNAME) && y.FolderNm.ToUpper().Equals(EDocumentFolder.UNCLASSIFIED_FOLDERNAME))
            {
                return 0;
            }
            
            if (x.FolderNm.ToUpper().Equals(EDocumentFolder.UNCLASSIFIED_FOLDERNAME))
            {
                return -1;
            }

            if (y.FolderNm.ToUpper().Equals(EDocumentFolder.UNCLASSIFIED_FOLDERNAME))
            {
                return 1;
            }

            return x.FolderNm.ToLower().CompareTo(y.FolderNm.ToLower());
        }

        #endregion
    }

    public class EDocumentFolderRolesComparer : IComparer<FolderRoles>
    {
        public int Compare(FolderRoles x, FolderRoles y)
        {
            return x.RoleDisplayDesc.ToLower().CompareTo(y.RoleDisplayDesc.ToLower());
        }
    }

    public class EDocumentFolder : IComparable
    {
        public const string UNCLASSIFIED_FOLDERNAME = "UNCLASSIFIED";
        public const string GENERATED_FOLDERNAME = "GENERATED DOCUMENTS";
        public const string LENDINGQB_FOLDERNAME = "LENDINGQB";
        public const string LENDINGQB_UNDERWRITING_FOLDERNAME = "LENDINGQB UNDERWRITING";
        public const string LENDINGQB_LOCKDESK_FOLDERNAME = "LENDINGQB LOCKDESK";
        public const string FHA_CONNECTION_FOLDERDNAME = "FHA CONNECTION";
        public const string COMPLIANCEEASE_FOLDERNAME = "COMPLIANCEEASE";
        public const string DATAVERIFY_FOLDERNAME = "DATA VERIFY";
        public const string IRS4506T_FOLDERNAME = "IRS 4506-T";
        private static string[] SYSTEM_FOLDERS = new string[] { COMPLIANCEEASE_FOLDERNAME, "DOCMAGIC", GENERATED_FOLDERNAME };
        private int m_folderId = -1;
        private IList<FolderRoles> m_Roles = null;
        private IList<EDocRoles> m_roleIds = null;
        private string m_folderName = string.Empty;
        private bool m_folderNameChanged = false;

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            EDocumentFolder otherFolder = obj as EDocumentFolder;
            if (otherFolder != null)
                return this.FolderNm.CompareTo(otherFolder.FolderNm);
            else
                throw new ArgumentException("Object is not an EDocumentFolder");
        }

        public bool IsSystemFolder
        {
            get {
                return SYSTEM_FOLDERS.Contains(m_folderName, StringComparer.OrdinalIgnoreCase);
            }
        }

        private Guid m_BrokerId
        {
            get
            {
                if (x_brokerID != Guid.Empty)
                {
                    return x_brokerID;
                }

                AbstractUserPrincipal broker = Thread.CurrentPrincipal as AbstractUserPrincipal;

                if (broker == null)
                {
                    return Guid.Empty;
                }

                return broker.BrokerId;
            }
        }

        /// <summary>
        /// This string is used in the Folder UI to display to the users what roles are included in this folder
        /// </summary>
        // Note that we iterate through the list every time instead of caching the string because the roles
        // included in the folder might have changed
        public string RoleListDescString
        {
            get 
            {
                if (IsUnclassifiedFolder)
                {
                    return "All Roles";
                }

                StringBuilder sb = new StringBuilder();

                foreach (FolderRoles folderRole in m_Roles)
                {
                    if (folderRole.IsIncludedInFolder)
                    {
                        if (sb.Length == 0)
                        {
                            sb.Append(folderRole.RoleDisplayDesc);
                        }
                        else
                        {
                            sb.Append(", " + folderRole.RoleDisplayDesc);
                        }
                    }
                }

                return sb.ToString();
            }
        }

        public bool IsUnclassifiedFolder
        {
            get { return m_folderName.ToUpper().Equals(UNCLASSIFIED_FOLDERNAME); }
        }

        public IEnumerable<FolderRoles> FolderRolesList
        {
            get { return m_Roles; }
        }

        /// <summary>
        /// Returns the list of roleIds that are included in this folder.
        /// </summary>
        public IList<EDocRoles> AssociatedRoleIds
        {
            get 
            {
                if (m_roleIds == null || m_roleIds.Count == 0)
                {
                    m_roleIds = new List<EDocRoles>();

                    if (m_Roles != null)
                    {
                        foreach (FolderRoles role in m_Roles)
                        {
                            if (role.IsIncludedInFolder)
                            {
                                m_roleIds.Add(new EDocRoles() { RoleId = role.RoleId, IsPmlUser = role.IsPmlUser});
                            }
                        }
                    }
                }

                return m_roleIds;
            }
        }

        public bool CanRead()
        {
            AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;

            return CanRead(principal);
        }

        public bool CanRead(AbstractUserPrincipal principal)
        {
            if (principal == null || principal is InternalUserPrincipal) // check for pdf processor
            {
                return true;
            }
            foreach (FolderRoles role in m_Roles.Where(p => p.IsPmlUser == (principal.Type == "P")))
            {
                if (role.IsIncludedInFolder && principal.IsInRole(role.RoleDesc))
                {
                    return true;
                }
            }

            return false;
        }

        public int FolderId
        {
            get { return m_folderId; }
        }

        public string FolderNm
        {
            get { return m_folderName; }
            set 
            {
                if (IsSystemFolder)
                {
                    //cant edit a system folder
                    return; 
                }
                if (m_folderName.ToLower() != value.ToLower())
                {
                    m_folderNameChanged = true;
                    m_folderName = value;
                }
            }
        }

        public EDocumentFolder(Guid brokerId)
        {
            x_brokerID = brokerId;
            m_Roles = new List<FolderRoles>();
            PopulateInitialFolderRoles();
        }
        private Guid x_brokerID = Guid.Empty;
        private EDocumentFolder(Guid brokerID, IEnumerable<Tuple<Guid, bool>> includedRoles)
        {
            x_brokerID = brokerID;
            m_Roles = new List<FolderRoles>();
            foreach (var role in includedRoles)
            {
                m_Roles.Add(new FolderRoles(m_folderId, role.Item1 , true, role.Item2));
            } 
        }

        private EDocumentFolder(Guid brokerID, IEnumerable<LendersOffice.BrokerAdmin.Importers.Edocs.ImportedFolderInfo> roles)
        {
            x_brokerID = brokerID;

            m_Roles = new List<FolderRoles>();

            foreach (var role in roles)
            {
                m_Roles.Add(new FolderRoles(
                    folderId: m_folderId, 
                    roleId: role.RoleId, 
                    isIncludedInFolder: true, 
                    isPmlUser: role.IsPmlUser));
            } 
        }

        /// <summary>
        /// Note certain methods won't work like CanRead().  This is intended for use in LoAdmin.
        /// </summary>
        /// <param name="brokerID"></param>
        /// <param name="includedRoles">each item = role ID, IsPml</param>
        public static EDocumentFolder Create(Guid brokerID, IEnumerable<Tuple<Guid, bool>> includedRoles)
        {
            return new EDocumentFolder(brokerID, includedRoles);
        }

        public static EDocumentFolder Create(Guid brokerID, IEnumerable<LendersOffice.BrokerAdmin.Importers.Edocs.ImportedFolderInfo> roles)
        {
            return new EDocumentFolder(brokerID, roles);
        }
        
        private void PopulateInitialFolderRoles()
        {
            AbstractUserPrincipal p = Thread.CurrentPrincipal as AbstractUserPrincipal; 

            if (m_Roles == null)
            {
                m_Roles = new List<FolderRoles>();
            } 

            foreach (var role in Role.AllRoles)
            {
                // Don't show BrokerProcessor, External Secondary or External Post-Closer if no PML
                bool isPmlRole = role.RoleT == E_RoleT.Pml_BrokerProcessor 
                    || role.RoleT == E_RoleT.Pml_Secondary 
                    || role.RoleT == E_RoleT.Pml_PostCloser;
                if (p != null && p.HasFeatures(E_BrokerFeatureT.PriceMyLoan) == false && isPmlRole)
                {
                    continue;
                }
                m_Roles.Add(new FolderRoles(m_folderId, role.Id, false, isPmlRole));

            }

            if (p != null && p.HasFeatures(E_BrokerFeatureT.PriceMyLoan) == false) // Don't show the following fields if they don't have PML
            {
                return;
            }
            m_Roles.Add(new FolderRoles(m_folderId, Role.Get(E_RoleT.LoanOfficer).Id, false, true));
            m_Roles.Add(new FolderRoles(m_folderId, Role.Get(E_RoleT.Administrator).Id, false, true));
        }

        public EDocumentFolder(Guid brokerId, int folderId)
        {
            x_brokerID = brokerId;
            Retrieve(brokerId, folderId);
        }

        private void ClearAndFillRoleDescList(IEnumerable<EDocRoles> roleIds)
        {
            AbstractUserPrincipal p = Thread.CurrentPrincipal as AbstractUserPrincipal; 

            m_Roles = new List<FolderRoles>();
            PopulateInitialFolderRoles();

            foreach (EDocRoles roleId in roleIds)
            {
                if (p != null && !p.HasFeatures(E_BrokerFeatureT.PriceMyLoan) && roleId.IsPmlUser)
                {
                    continue;
                }
                if (roleId.RoleId != Guid.Empty)
                {
                    bool foundMatch = false;
                    foreach (FolderRoles fRole in m_Roles)
                    {
                        if (fRole.RoleId == roleId.RoleId && fRole.IsPmlUser == roleId.IsPmlUser)
                        {
                            fRole.IsIncludedInFolder = true;
                            foundMatch = true;
                        }
                    }

                    if (foundMatch == false)
                    {
                        throw new CBaseException(ErrorMessages.Generic, "Unable to fill role list in EDocs Folder.  Role: " + roleId.RoleId + ", IsPmlUser:" + roleId.IsPmlUser + " was not found among the prepopulated list of roles, which is supposed to contain all available roles.");
                    }
                }
            }

            ((List<FolderRoles>)m_Roles).Sort(new EDocumentFolderRolesComparer());
        }

        public EDocumentFolder(Guid brokerId, IDataRecord reader)
            : this(brokerId, reader.ToDictionary())
        {
        }

        public static Dictionary<int, EDocumentFolder> GetFoldersById(List<Dictionary<string, object>> tableResult)
        {
            Dictionary<int, EDocumentFolder> foldersById = new Dictionary<int, EDocumentFolder>(tableResult.Count);
            foreach (var row in tableResult)
            {
                EDocumentFolder folder;
                int folderId = (int)row["FolderId"];
                if (!foldersById.TryGetValue(folderId, out folder))
                {
                    folder = new EDocumentFolder((Guid)row["BrokerId"], folderId, (string)row["folderName"]);
                    folder.m_Roles = new List<FolderRoles>();
                    foldersById.Add(folderId, folder);
                }

                if (row["roleid"] != DBNull.Value)
                {
                    folder.m_Roles.Add(new FolderRoles(folderId, (Guid)row["roleid"], true, (bool)row["IsPmlUser"]));
                }
            }

            return foldersById;
        }

        public EDocumentFolder(Guid brokerId, IReadOnlyDictionary<string, object> reader)
        {
            x_brokerID = brokerId;
            m_folderId = (int)reader["FolderId"];
            m_folderName = (string)reader["FolderName"];

            string comma_seperated_roles = (string)reader["FolderRoles"];
            string[] roleIds = comma_seperated_roles.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            EDocRoles[] roleIdArray = new EDocRoles[roleIds.Length];
            for (int i = 0; i < roleIds.Length; i++)
            {
                string[] entry = roleIds[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                roleIdArray[i] = new EDocRoles() { RoleId = new Guid(entry[0]), IsPmlUser = Convert.ToBoolean(Convert.ToInt32(entry[1])) };
            }
            ClearAndFillRoleDescList(roleIdArray);
        }

        public EDocumentFolder(Guid brokerId, int folderId, string folderName)
        {
            x_brokerID = brokerId;
            m_folderId = folderId;
            m_folderName = folderName;
        }

        public void SetFolderRoles(IEnumerable<EDocRoles> roles)
        {
            ClearAndFillRoleDescList(roles);
        }

        private void Retrieve(Guid brokerId, int folderId)
        {
            List<EDocRoles> roleIds = new List<EDocRoles>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@FolderId", folderId)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_GetFolderById", parameters))
            {
                while (reader.Read())
                {
                    m_folderId = folderId;
                    m_folderName = (string)reader["FolderName"];
                    Guid roleId = new Guid(reader["RoleId"].ToString());
                    bool isPmlUser = Convert.ToBoolean(reader["IsPmlUser"].ToString());
                    roleIds.Add(new EDocRoles() {RoleId = roleId, IsPmlUser = isPmlUser });
                }

                ClearAndFillRoleDescList(roleIds);
            }
        }

        /// <summary>
        /// Returns the list of folders contained in this broker
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public static IList<EDocumentFolder> GetFoldersInBroker(Guid brokerId)
        {
            List<EDocumentFolder> folders = new List<EDocumentFolder>();
            Dictionary<int, EDocumentFolder> foldersById = new Dictionary<int, EDocumentFolder>();
            Dictionary<int, List<EDocRoles>> rolesByFolder = new Dictionary<int, List<EDocRoles>>();
            EDocumentFolder folder;

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_RetrieveFoldersByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    int folderId = (int)reader["FolderId"];
                    List<EDocRoles> roles;
                    if (!foldersById.TryGetValue(folderId, out folder))
                    {
                        folder = new EDocumentFolder(brokerId, folderId, reader["FolderName"].ToString());
                        foldersById.Add(folderId, folder);
                        roles = new List<EDocRoles>();
                        rolesByFolder.Add(folderId, roles);
                        folders.Add(folder);
                    }
                    else
                    {
                        roles = rolesByFolder[folderId];
                    }

                    if (DBNull.Value != reader["roleid"])
                    {
                        bool ispmluser = (bool)reader["ispmluser"];
                        Guid roleid = (Guid)reader["roleid"];

                        EDocRoles role = new EDocRoles();
                        role.RoleId = roleid;
                        role.IsPmlUser = ispmluser;
                        roles.Add(role);
                    }
                }
            }

            foreach (var item in rolesByFolder)
            {
                folder = foldersById[item.Key];
                folder.SetFolderRoles(item.Value);
            }

            folders.Sort(new EDocumentFolderComparer());
            
            return folders;
        }

        private void SaveImpl(CStoredProcedureExec exce)
        {
            try
            {
                exce.BeginTransactionForWrite();

                if (m_folderId == -1) // new folder
                {
                    CreateFolder(exce);
                }
                else
                {
                    // First delete all existing roles for this folder
                    exce.ExecuteNonQuery("EDOCS_DeleteAllRoles", 3, new SqlParameter("@FolderId", m_folderId));

                    // Then add all roles set in this object
                    AddRolesToFolderAndSave(m_folderId, exce);

                    // And finally change the folder's name if it was changed
                    // If we add more fields to the folder object, we need to not perform this folder
                    // name changed check right here, and instead surround the parameter add for folder name with it
                    // so that other parameters can be saved, even if the folder name did not change.
                    if (m_folderNameChanged)
                    {
                        List<SqlParameter> cnparameters = new List<SqlParameter>();
                        cnparameters.Add(new SqlParameter("@BrokerId", m_BrokerId));
                        cnparameters.Add(new SqlParameter("@FolderName", m_folderName));
                        using (DbDataReader dr = StoredProcedureHelper.ExecuteReader(m_BrokerId, "EDOCS_UpdateFolderCheckName", cnparameters))
                        {
                            if (dr.HasRows)
                                throw new DuplicateNameSelectedException("A folder already exists with the same name.  Please choose a different name.");
                        }

                        ArrayList parameters = new ArrayList();
                        parameters.Add(new SqlParameter("@FolderId", m_folderId));
                        parameters.Add(new SqlParameter("@FolderName", m_folderName));

                        exce.ExecuteNonQuery("EDOCS_UpdateFolder", parameters);
                    }
                }

                exce.CommitTransaction();
            }
            catch (Exception)
            {
                exce.RollbackTransaction();
                throw;
            }
        }
        
        private void Save(CStoredProcedureExec exce)
        {
            if (string.IsNullOrEmpty(m_folderName.TrimWhitespaceAndBOM()))
            {
                throw new CBaseException("Folder name cannot be blank", "Folder name cannot be blank");
            }
            SaveImpl(exce);            
        }
        public void Save()
        {
            using (CStoredProcedureExec exce = new CStoredProcedureExec(m_BrokerId))
            {
                Save(exce);   
            }
        }

        private void CreateFolder(CStoredProcedureExec exce)
        {
            if (m_folderId != -1)
            {
                string devMsg = "An attempt was made to create a new folder using an existing folder object (the object already has a folderId).";
                Tools.LogBug(devMsg);
                throw new CBaseException("Unable to create folder - please contact us if this happens again.", devMsg);
            }

            if (string.IsNullOrEmpty(m_folderName.TrimWhitespaceAndBOM()))
            {
                throw new CBaseException("Folder name cannot be blank", "Folder name cannot be blank");
            }

            SqlParameter[] cnparameters = {
                new SqlParameter("@BrokerId", m_BrokerId),
                new SqlParameter("@FolderName", m_folderName)
            };

            using (DbDataReader dr = StoredProcedureHelper.ExecuteReader(m_BrokerId, "EDOCS_UpdateFolderCheckName", cnparameters))
            {
                if (dr.HasRows)
                {
                    throw new DuplicateNameSelectedException("A folder already exists with the same name.  Please choose a different name.");
                }
            }

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@BrokerId", m_BrokerId));
            parameters.Add(new SqlParameter("@FolderName", m_folderName));

            SqlParameter idParam = new SqlParameter("@FolderId", -1);
            idParam.Direction = ParameterDirection.Output;
            parameters.Add(idParam);

            exce.ExecuteNonQuery("EDOCS_CreateFolder", parameters.ToArray());

            int newFolderId = (int)idParam.Value;
            AddRolesToFolderAndSave(newFolderId, exce);

            m_folderId = newFolderId;
        }

        private void AddRolesToFolderAndSave(int folderId, CStoredProcedureExec exce)
        {
            foreach (FolderRoles frd in m_Roles)
            {
                if (frd.IsIncludedInFolder)
                {
                    AddRoleToFolderAndSave(frd.RoleId, frd.IsPmlUser, folderId, exce);
                }
            }
        }

        public static int CreateUnclassifiedFolderAndSave(Guid brokerId, CStoredProcedureExec exce)
        {
            return CreateAllRolesFolderAndSave(brokerId, exce, UNCLASSIFIED_FOLDERNAME);
        }

        internal static int CreateAllRolesFolderAndSave(Guid brokerId, CStoredProcedureExec exce, string FolderName)
        {
            // Don't create an unclassified folder if one already exists for this broker
            if (FolderExists(brokerId, FolderName))
            {
                return -1;
            }

            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@BrokerId", brokerId));
            parameters.Add(new SqlParameter("@FolderName", FolderName));

            SqlParameter idParam = new SqlParameter("@FolderId", -1);
            idParam.Direction = ParameterDirection.Output;
            parameters.Add(idParam);

            exce.ExecuteNonQuery("EDOCS_CreateFolder", parameters);

            int newFolderId = (int)idParam.Value;

            AddAllRolesToFolderAndSave(newFolderId, exce);

            return newFolderId;
        }

        private static int CreateSystemFolder(Guid brokerId, int index)
        {
            Tools.LogInfo("Creating system folder " + SYSTEM_FOLDERS[index]);
            List<SqlParameter> parameters = new List<SqlParameter>(){
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@FolderName", SYSTEM_FOLDERS[index])
            };

            SqlParameter idParam = new SqlParameter("@FolderId", -1);
            idParam.Direction = ParameterDirection.Output;
            parameters.Add(idParam);
            int newFolderId;
            using (CStoredProcedureExec exec = new CStoredProcedureExec(brokerId))
            {

                exec.BeginTransactionForWrite();
                try
                {
                    exec.ExecuteNonQuery("EDOCS_CreateFolder", parameters.ToArray());
                    newFolderId = (int)idParam.Value;
                    AddAllRolesToFolderAndSave(newFolderId, exec);
                    exec.CommitTransaction();
                }
                catch (Exception)
                {
                    exec.RollbackTransaction();
                    throw;
                }
            }

            return newFolderId;
        }

        private static int CreateAndGetSystemFolder(Guid brokerId, int index)
        {
            int id = GetFolderId(brokerId, SYSTEM_FOLDERS[index]);
            if (id < 0)
            {
                id = CreateSystemFolder(brokerId, index);
            }
            return id;
        }

        public static int CreateAndGetDocMagicFolder(Guid brokerId)
        {
            return CreateAndGetSystemFolder(brokerId, 1);
        }

        public static int CreateAndGetComplianceEaseFolder(Guid brokerId)
        {
            return CreateAndGetSystemFolder(brokerId, 0);
        }

        public static int CreateUnclassifiedFolderAndSave(Guid brokerId)
        {
            return CreateAllRolesFolderAndSave(brokerId, UNCLASSIFIED_FOLDERNAME);
        }

        internal static int CreateAllRolesFolderAndSave(Guid brokerId, string FolderName)
        {
            // Don't create an unclassified folder if one already exists for this broker
            if (FolderExists(brokerId, FolderName))
            {
                return -1;
            }

            using (CStoredProcedureExec exce = new CStoredProcedureExec(brokerId))
            {
                try
                {
                    exce.BeginTransactionForWrite();

                    int folderId = CreateAllRolesFolderAndSave(brokerId, exce, FolderName);

                    exce.CommitTransaction();
                    return folderId;
                }
                catch (Exception)
                {
                    exce.RollbackTransaction();
                    throw;
                }
            }
        }

        private static void AddAllRolesToFolderAndSave(int folderId, CStoredProcedureExec exce)
        {
            List<EDocRoles> roleIds = new List<EDocRoles>();
            foreach (var role in Role.AllRoles)
            {
                if (role.RoleT == E_RoleT.Pml_BrokerProcessor 
                    || role.RoleT == E_RoleT.Pml_Secondary 
                    || role.RoleT == E_RoleT.Pml_PostCloser)
                {
                    roleIds.Add(new EDocRoles() { RoleId = role.Id, IsPmlUser = true });
                }
                else
                {
                    roleIds.Add(new EDocRoles() { RoleId = role.Id, IsPmlUser = false });
                }

                if (role.RoleT == E_RoleT.LoanOfficer || role.RoleT == E_RoleT.Administrator)
                {
                    roleIds.Add(new EDocRoles() { RoleId = role.Id, IsPmlUser = true });
                }                    
            }

            SetRoleListForFolderAndSave(roleIds, folderId, exce);
        }

        public static void SetRoleListForFolderAndSave(List<EDocRoles> roleIds, int folderId, CStoredProcedureExec exce)
        {
            // First delete all existing roles for this folder
            exce.ExecuteNonQuery("EDOCS_DeleteAllRoles", 3, new SqlParameter("@FolderId", folderId));

            // Then add all roles from the parameter list
            AddRolesToFolderAndSave(roleIds, folderId, exce);
        }

        /// <summary>
        /// Only intended for LoAdmin migration - should be removed
        /// Sets a folder's role list to exactly the list given in the parameters (all others are deleted)
        /// NOTE: This function immediately saves! and was designed for LoAdmin use
        /// </summary>
        /// <param name="roleIds"></param>
        /// <param name="folderId"></param>
        public static void SetRoleListForFolderAndSave(Guid brokerId, IList<EDocRoles> roleIds, int folderId)
        {
            using (CStoredProcedureExec exce = new CStoredProcedureExec(brokerId))
            {
                try
                {
                    exce.BeginTransactionForWrite();
                    
                    // First delete all existing roles for this folder
                    exce.ExecuteNonQuery("EDOCS_DeleteAllRoles", 3, new SqlParameter("@FolderId", folderId));
                    
                    // Then add all roles from the parameter list
                    AddRolesToFolderAndSave(roleIds, folderId, exce);
                    
                    exce.CommitTransaction();
                }
                catch (Exception)
                {
                    exce.RollbackTransaction();
                    throw;
                }
            }
        }
        
        /// <summary>
        /// Only intended for LoAdmin migration - should be removed
        /// Adds a list of roles to a folder's role list.  This is private because the callers should use
        /// SetRoleListForFolder.  This way, there won't be problems with trying to set more than 1 of the same role.
        /// </summary>
        /// <param name="roleIds"></param>
        /// <param name="folderId"></param>
        private static void AddRolesToFolderAndSave(IList<EDocRoles> roleIds, int folderId, CStoredProcedureExec exce)
        {
            foreach (EDocRoles role in roleIds)
            {
                AddRoleToFolderAndSave(role.RoleId, role.IsPmlUser, folderId, exce);
            }
        }

        /// <summary>
        /// Only intended for new role migrations
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="folderId"></param>
        /// <param name="exce"></param>
        private static void AddRoleToFolderAndSave(Guid roleId, bool isPmlUser, int folderId, CStoredProcedureExec exce)
        {

            ArrayList parameters = new ArrayList();
            parameters.Add(new SqlParameter("@FolderId", folderId));
            parameters.Add(new SqlParameter("@RoleId", roleId));
            parameters.Add(new SqlParameter("@IsPmlUser", isPmlUser));

            exce.ExecuteNonQuery("EDOCS_AddRoleToFolder", parameters);
        }
        
        public static bool FolderExists(Guid brokerId, string folderName)
        {
            SqlParameter[] parameters = {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@FolderName", folderName)
            };
            
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_CheckIfFolderExists", parameters))
            {
                return reader.HasRows;
            }
        }

        /// <summary>
        /// Gets the compliance ease folder id. If it has not been created it returns -1;
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public static int GetComplianceEaseFolderId(Guid brokerId)
        {
            return GetFolderId(brokerId,SYSTEM_FOLDERS[0]);
        }

        /// <summary>
        /// Gets the docmagic folder id. If it has not been created returns -1
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public static int GetDocMagicFolderId(Guid brokerId)
        {
            return GetFolderId(brokerId, SYSTEM_FOLDERS[1]);
        }

        public static int GetFolderId(Guid brokerId, string folderName)
        {
            SqlParameter[] parameters = {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@FolderName", folderName)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_GetFolderIdByBrokerIdFolderName", parameters))
            {
                if (reader.Read())
                {
                    return (int)reader["FolderId"];
                }
            }

            return -1;
        }

        public static int GetUnclassifiedFolderId(Guid brokerId)
        {
            return GetFolderId(brokerId, UNCLASSIFIED_FOLDERNAME);
        }
        
        public static bool BrokerHasUnclassifiedFolder(Guid brokerId)
        {
            return FolderExists(brokerId, UNCLASSIFIED_FOLDERNAME);
        }

        public static void DeleteFolder(int folderId, Guid brokerId)
        {
            int unclassifiedFolderId = GetUnclassifiedFolderId(brokerId);
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@FolderIdToDelete", folderId));
            parameters.Add(new SqlParameter("@FolderIdToAssignDocTypesTo", unclassifiedFolderId));
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "EDOCS_DeleteFolder", 3, parameters);
        }

        public static void AssignDocTypeToFolder(int folderId, int docTypeId, CStoredProcedureExec exec)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@FolderId", folderId),
                new SqlParameter("@DocTypeId", docTypeId)
            };

            exec.ExecuteNonQuery("EDOCS_AssignDocTypeToFolder", parameters);
        }
    }

    public class EDocRoles
    {
        public EDocRoles()
        {

        }

        public Guid RoleId
        {
            get;
            set;
        }

        public bool IsPmlUser
        {
            get;
            set;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            EDocRoles eDocRoles = obj as EDocRoles;
            if ((System.Object)eDocRoles == null)
            {
                return false;
            }

            return (RoleId == eDocRoles.RoleId) && (IsPmlUser == eDocRoles.IsPmlUser);
        }

        public override int GetHashCode()
        {
            return RoleId.GetHashCode() + IsPmlUser.GetHashCode();
        }
    }
}
