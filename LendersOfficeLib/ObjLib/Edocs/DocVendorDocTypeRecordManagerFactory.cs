﻿namespace LendersOffice.ObjLib.Edocs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;
    using LendersOffice.Edocs.DocMagic;
    using LendersOffice.ObjLib.Edocs.DocuTech;
    using LendersOffice.ObjLib.Edocs.IDS;

    /// <summary>
    /// Factory to create barcode record manager.
    /// </summary>
    public class DocVendorDocTypeRecordManagerFactory
    {
        /// <summary>
        /// Create IDocVendorDocTypeRecordManager from barcodeFmt.
        /// </summary>
        /// <param name="barcodeFmt">The barcode format.</param>
        /// <returns>Return IDocVendorDocTypeRecordManager object.</returns>
        public static IDocVendorDocTypeRecordManager Create(E_DocBarcodeFormatT barcodeFmt)
        {
            switch (barcodeFmt)
            {
            case E_DocBarcodeFormatT.DocuTech:
                return new DocuTechManager();

            case E_DocBarcodeFormatT.DocMagic:
                return new DocMagicManager();

            case E_DocBarcodeFormatT.IDS:
                return new IDSManager();

            default:
                throw new ArgumentException("Don't expect barcode type " + barcodeFmt);
            }
        }

        /// <summary>
        /// Get DocuTect Barcode Record Manager.
        /// </summary>
        private class DocuTechManager : IDocVendorDocTypeRecordManager
        {
            /// <summary>
            /// Create new DocuTech barcode record.
            /// </summary>
            /// <param name="desc">Barcode's decription. </param>
            /// <param name="barcode">Barcode Classification.</param>
            /// <returns>Return barcode record.</returns>
            public int Create(string desc, string barcode)
            {
                return DocuTechDocType.Create(desc, barcode);
            }

            /// <summary>
            /// Delete DocuTech barcode record.
            /// </summary>
            /// <param name="id">The record id to delete.</param>
            public void Delete(int id)
            {
                DocuTechDocType.Delete(id);
            }

            /// <summary>
            /// Retrieve DocuTech barcode record by record id.
            /// </summary>
            /// <param name="id">The record id to look up.</param>
            /// <returns>Return barcode record.</returns>
            public IDocVendorDocTypeRecord Get(int id)
            {
                IEnumerable<IDocVendorDocTypeRecord> records = this.ListRecords();
                return records.Where(r => r.Id == id).First();
            }

            /// <summary>
            /// Get all DocuTect barcode records.
            /// </summary>
            /// <returns>Return list of records.</returns>
            public IEnumerable<IDocVendorDocTypeRecord> ListRecords()
            {
                return DocuTechDocType.ListRecords();
            }

            /// <summary>
            /// Save DocuTech barcode record to database.
            /// </summary>
            /// <param name="id">The record id to update.</param>
            /// <param name="desc">Barcode's decription. </param>
            /// <param name="barcode">Barcode Classification.</param>
            public void Update(int id, string desc, string barcode)
            {
                DocuTechDocType.Update(id, desc, barcode);
            }
        }

        /// <summary>
        /// Get DocMagic Barcode Record Manager.
        /// </summary>
        private class DocMagicManager : IDocVendorDocTypeRecordManager
        {
            /// <summary>
            /// Create new DocMagic barcode record.
            /// </summary>
            /// <param name="desc">Barcode's decription. </param>
            /// <param name="barcode">Barcode Classification.</param>
            /// <returns>Return barcode record.</returns>
            public int Create(string desc, string barcode)
            {
                return DocMagicDocType.Create(desc, barcode);
            }

            /// <summary>
            /// Delete DocMagic barcode record.
            /// </summary>
            /// <param name="id">The record id to delete.</param>
            public void Delete(int id)
            {
                DocMagicDocType.Delete(id);
            }

            /// <summary>
            /// Retrieve DocMagic barcode record by record id.
            /// </summary>
            /// <param name="id">The record id to look up.</param>
            /// <returns>Return barcode record.</returns>
            public IDocVendorDocTypeRecord Get(int id)
            {
                IEnumerable<IDocVendorDocTypeRecord> records = this.ListRecords();
                return records.Where(r => r.Id == id).First();
            }

            /// <summary>
            /// Get all DocMagic barcode records.
            /// </summary>
            /// <returns>Return list of records.</returns>
            public IEnumerable<IDocVendorDocTypeRecord> ListRecords()
            {
                return DocMagicDocType.ListRecords();
            }

            /// <summary>
            /// Save DocMagic barcode record to database.
            /// </summary>
            /// <param name="id">The record id to update.</param>
            /// <param name="desc">Barcode's decription. </param>
            /// <param name="barcode">Barcode Classification.</param>
            public void Update(int id, string desc, string barcode)
            {
                DocMagicDocType.Update(id, desc, barcode);
            }
        }

        /// <summary>
        /// Get IDS Barcode Record Manager.
        /// </summary>
        private class IDSManager : IDocVendorDocTypeRecordManager
        {
            /// <summary>
            /// Create new IDS barcode record.
            /// </summary>
            /// <param name="desc">Barcode's decription. </param>
            /// <param name="barcode">Barcode Classification.</param>
            /// <returns>Return barcode record.</returns>
            public int Create(string desc, string barcode)
            {
                return IDSDocType.Create(desc, barcode);
            }

            /// <summary>
            /// Delete IDS barcode record.
            /// </summary>
            /// <param name="id">The record id to delete.</param>
            public void Delete(int id)
            {
                IDSDocType.Delete(id);
            }

            /// <summary>
            /// Retrieve IDS barcode record by record id.
            /// </summary>
            /// <param name="id">The record id to look up.</param>
            /// <returns>Return barcode record.</returns>
            public IDocVendorDocTypeRecord Get(int id)
            {
                IEnumerable<IDocVendorDocTypeRecord> records = this.ListRecords();
                return records.Where(r => r.Id == id).First();
            }

            /// <summary>
            /// Get all IDS barcode records.
            /// </summary>
            /// <returns>Return list of records.</returns>
            public IEnumerable<IDocVendorDocTypeRecord> ListRecords()
            {
                return IDSDocType.ListRecords();
            }

            /// <summary>
            /// Save IDS barcode record to database.
            /// </summary>
            /// <param name="id">The record id to update.</param>
            /// <param name="desc">Barcode's decription. </param>
            /// <param name="barcode">Barcode Classification.</param>
            public void Update(int id, string desc, string barcode)
            {
                IDSDocType.Update(id, desc, barcode);
            }
        }
    }
}
