﻿// <copyright file="EDocsUpload.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//      Author: Jhairo Erazo
//      Date: 12/23/2016
// </summary>
namespace EDocs
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web.UI.WebControls;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.XPath;
    using CommonProjectLib.Common.Lib;
    using DataAccess;
    using EDocs.Contents;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Constants;
    using LendersOffice.Edocs.DocMagic;
    using LendersOffice.Integration.OCR;
    using LendersOffice.ObjLib.Edocs;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Static utility class for uploading EDocs.
    /// </summary>
    public static class EDocsUpload
    {
        /// <summary>
        /// Extract Appraisal PDF file from an XML file.
        /// </summary>
        /// <param name="xmlPath">XML file path.</param>
        /// <returns>PDF file path.</returns>
        /// <exception cref="XmlException">Throws XmlException if file is not a valid xml file.</exception>
        public static string ParseAppraisalDocFromXml(string xmlPath)
        {
            XDocument xmlDoc = XDocument.Load(xmlPath); // Throws XmlException if file is not a valid xml file.

            return ParseAppraisalDocFromXml(xmlDoc);
        }

        /// <summary>
        /// Extract UCD PDF from an XML string.
        /// </summary>
        /// <param name="xmlString">XML document content string.</param>
        /// <returns>PDF file path.</returns>
        public static string ParseUcdDocFromXml(string xmlString)
        {
            return ParseUcdDocFromXml(XDocument.Parse(xmlString));
        }

        /// <summary>
        /// Extract UCD PDF from an XML file.
        /// </summary>
        /// <param name="xmlDoc">XML document.</param>
        /// <returns>PDF file path.</returns>
        public static string ParseUcdDocFromXml(XDocument xmlDoc)
        {
            // Account for xml namespaces.
            XmlNamespaceManager mgr = new XmlNamespaceManager(new NameTable());
            mgr.AddNamespace("a", xmlDoc.Root.Name.Namespace.ToString());

            IEnumerable<XElement> pdfNodes = xmlDoc.XPathSelectElements("//a:DOCUMENT_SETS/a:DOCUMENT_SET/a:DOCUMENTS/a:DOCUMENT/a:VIEWS/a:VIEW/a:VIEW_FILES/a:VIEW_FILE/a:FOREIGN_OBJECT/a:EmbeddedContentXML", mgr);

            if (!pdfNodes.Any())
            {
                throw new CBaseException(ErrorMessages.EDocs.InvalidUcd, "PDF path not found in XML doc.");
            }
            else if (pdfNodes.Count() == 1)
            {
                return ExtractPdfFromNode(pdfNodes.First());
            }
            else
            {
                // Need to merge docs
                int count = 1;
                var mergeInfo = new List<Tuple<string, Func<byte[]>, Guid>>(pdfNodes.Count());

                foreach (XElement pdfNode in pdfNodes)
                {
                    mergeInfo.Add(new Tuple<string, Func<byte[]>, Guid>("Doc " + count, () => Convert.FromBase64String(pdfNode.Value), Guid.Empty));
                    count++;
                }

                string pdfPath = TempFileUtils.NewTempFilePath();
                using (FileStream fs = File.OpenWrite(pdfPath))
                {
                    EDocumentViewer.WriteSinglePdf(mergeInfo, fs);
                }

                return pdfPath;
            }
        }

        /// <summary>
        /// Upload an EDoc using a File Upload web control.
        /// </summary>
        /// <param name="principal">User Principal.</param>
        /// <param name="fileUpload">A file upload web control.</param>
        /// <param name="loanId">A Loan ID.</param>
        /// <param name="appId">Application ID.</param>
        /// <param name="docTypeId">Doc Type ID.</param>
        /// <param name="internalComments">Internal description/comments.</param>
        /// <param name="description">Public description.</param>
        /// <param name="debugString">Optional StringBuilder reference used to output debug data.</param>
        /// <param name="bypassWorkflow">If true, Workflow UploadEdocs privilege check is bypassed. Used by EDocs API.</param>
        /// <returns>Doc ID, if it exists (Doc saved instantly). Guid.Empty otherwise (Doc sent to queue for processing).</returns>
        public static Guid UploadDoc(AbstractUserPrincipal principal, FileUpload fileUpload, Guid loanId, Guid appId, int docTypeId, string internalComments, string description, StringBuilder debugString = null, bool bypassWorkflow = false)
        {
            var fileData = new FileData(fileUpload);
            return UploadDoc(principal, fileData, loanId, appId, docTypeId, internalComments, description, debugString, bypassWorkflow);
        }

        /// <summary>
        /// Upload an EDoc from the Non-QM pages.
        /// </summary>
        /// <param name="principal">User Principal.</param>
        /// <param name="fileName">The name of the file.</param>
        /// <param name="base64Content">A string containing Base64 pdf content.</param>
        /// <param name="loanId">A Loan ID.</param>
        /// <param name="appId">Application ID.</param>
        /// <returns>Doc ID, if it exists (Doc saved instantly). Guid.Empty otherwise (Doc sent to queue for processing).</returns>
        public static Guid UploadDocFromNonQm(AbstractUserPrincipal principal, string fileName, string base64Content, Guid loanId, Guid appId)
        {
            var pdfContent = Convert.FromBase64String(base64Content);
            var fileData = new FileData(fileName, pdfContent);
            var docTypeId = EDocumentDocType.GetOrCreateUnclassifiedDocType(principal.BrokerId);
            return UploadDoc(principal, fileData, loanId, appId, docTypeId, string.Empty, string.Empty, null, bypassWorkflow: true);
        }

        /// <summary>
        /// Upload an EDoc.
        /// </summary>
        /// <param name="principal">User Principal.</param>
        /// <param name="fileData">Data on the uploaded file.</param>
        /// <param name="loanId">A Loan ID.</param>
        /// <param name="appId">Application ID.</param>
        /// <param name="docTypeId">Doc Type ID.</param>
        /// <param name="internalComments">Internal description/comments.</param>
        /// <param name="description">Public description.</param>
        /// <param name="debugString">Optional StringBuilder reference used to output debug data.</param>
        /// <param name="bypassWorkflow">If true, Workflow UploadEdocs privilege check is bypassed. Used by EDocs API.</param>
        /// <returns>Doc ID, if it exists (Doc saved instantly). Guid.Empty otherwise (Doc sent to queue for processing).</returns>
        public static Guid UploadDoc(AbstractUserPrincipal principal, FileData fileData, Guid loanId, Guid appId, int docTypeId, string internalComments, string description, StringBuilder debugString = null, bool bypassWorkflow = false)
        {
            if (!bypassWorkflow)
            {
                AssertUploadEDocsWorkflowPrivileges(principal, loanId);
            }

            Stopwatch totalStopwatch = Stopwatch.StartNew();

            // Upload to temp file.
            Stopwatch readFileStopwatch = Stopwatch.StartNew();
            bool isXml = fileData.FileName.EndsWith("xml", StringComparison.OrdinalIgnoreCase);

            string filePath = TempFileUtils.NewTempFilePath();

            if (isXml)
            {
                // dd 9/23/2017 - ONLY write as text for XML. Everything else is binary and should write as bytes.
                File.WriteAllText(filePath, fileData.Contents);
            }
            else
            {
                File.WriteAllBytes(filePath, fileData.FileBytes);
            }

            readFileStopwatch.Stop();

            bool isScanBarcode = docTypeId == ConstApp.ScanDocMagicBarcodesDocTypeId;

            Guid docId = Guid.Empty;
            string docDebugInfo;
            Stopwatch saveStopwatch = Stopwatch.StartNew();
            if (fileData.FileName.EndsWith("xls", StringComparison.OrdinalIgnoreCase) || fileData.FileName.EndsWith("xlsx", StringComparison.OrdinalIgnoreCase))
            {
                docId = UploadExcelDoc(principal, loanId, appId, docTypeId, internalComments, description, filePath, fileData.FileName);
                docDebugInfo = $"Uploaded Generic Doc. DocId={docId}";
            }
            else if (!isXml && principal.Type == "P" && principal.BrokerDB.OCRHasConfiguredAccount && principal.BrokerDB.IsOCREnabled
                && OCRService.AddFileFromTPO(principal, fileData.FileName, DateTime.Now, fileData.FileBytes, isScanBarcode ? (int?)null : docTypeId, internalComments, loanId, appId, isScanBarcode))
            {
                docDebugInfo = "Uploaded Doc to OCR service queue";
            }
            else if (isScanBarcode)
            {
                DocMagicPDFManager.UploadDocument(principal, loanId, appId, internalComments, description, filePath);
                docDebugInfo = "Uploaded Doc to Doc Magic splitter queue";
            }
            else if (isXml)
            {
                docId = UploadXmlDoc(principal, loanId, appId, docTypeId, internalComments, description, filePath);
                docDebugInfo = $"Uploaded EDoc from XML. DocId={docId}";
            }
            else
            {
                // Standard PDF file upload.
                docId = UploadPdfDoc(principal, loanId, appId, docTypeId, internalComments, description, filePath);
                docDebugInfo = $"Uploaded EDoc. DocId={docId}";
            }

            saveStopwatch.Stop();
            totalStopwatch.Stop();

            if (debugString != null)
            {
                debugString.AppendFormat(
                    "UploadDocs:: {0}, ReadFile={1,8}ms, SaveFile={2,8}ms, ByteCount={3,10}. Total={4,8}ms",
                    docDebugInfo,
                    readFileStopwatch.ElapsedMilliseconds,
                    saveStopwatch.ElapsedMilliseconds,
                    fileData.FileLength,
                    totalStopwatch.ElapsedMilliseconds);

                debugString.AppendLine();
            }

            return docId;
        }

        /// <summary>
        /// Checks if principal has Workflow privileges for uploading EDocs.
        /// </summary>
        /// <param name="principal">The user principal.</param>
        /// <param name="loanId">The loan ID.</param>
        /// <exception cref="AccessDenied">Throws if user does not have privileges to upload EDocs.</exception>
        public static void AssertUploadEDocsWorkflowPrivileges(AbstractUserPrincipal principal, Guid loanId)
        {
            if (principal is SystemUserPrincipal)
            {
                return;
            }

            Tuple<bool, string> results = Tools.IsWorkflowOperationAuthorized(principal, loanId, WorkflowOperations.UploadEDocs);

            if (!results.Item1)
            {
                string userMsg = "You do not have permission to upload EDocs:" + Environment.NewLine;
                userMsg += results.Item2;

                string devMsg = $"EDocs Upload blocked by workflow. LoginNm=[{principal.LoginNm}]. UserId=[{principal.UserId}. BrokerId=[{principal.BrokerId}]. LoanId=[{loanId}].";

                throw new AccessDenied(userMsg, devMsg);
            }
        }

        /// <summary>
        /// Upload PDF Doc to EDocs.
        /// </summary>
        /// <param name="principal">User Principal.</param>
        /// <param name="loanId">A Loan ID.</param>
        /// <param name="appId">Application ID.</param>
        /// <param name="docTypeId">Doc Type ID.</param>
        /// <param name="internalComments">Internal description/comments.</param>
        /// <param name="description">Public description.</param>
        /// <param name="filePath">Temp path of uploaded file.</param>
        /// <param name="markDocumentAsAccepted">Mark document as accepted.</param>
        /// <returns>Document ID.</returns>
        private static Guid UploadPdfDoc(AbstractUserPrincipal principal, Guid loanId, Guid appId, int docTypeId, string internalComments, string description, string filePath, bool markDocumentAsAccepted = false)
        {
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            EDocument doc = repo.CreateDocument(E_EDocumentSource.UserUpload);
            doc.InternalDescription = internalComments;
            doc.DocumentTypeId = docTypeId;
            doc.LoanId = loanId;
            doc.AppId = appId; // OPM 50973
            doc.IsUploadedByPmlUser = principal.Type == "P";
            doc.EDocOrigin = doc.IsUploadedByPmlUser ? E_EDocOrigin.PML : E_EDocOrigin.LO;
            doc.PublicDescription = description;

            if (markDocumentAsAccepted)
            {
                doc.MarkDocumentAsAccepted();
            }

            doc.UpdatePDFContentOnSave(filePath);
            repo.Save(doc);

            return doc.DocumentId;
        }

        /// <summary>
        /// Used to parse a PDF doc encoded in an XML doc and upload it to EDocs.
        /// </summary>
        /// <param name="principal">User Principal.</param>
        /// <param name="loanId">A Loan ID.</param>
        /// <param name="appId">Application ID.</param>
        /// <param name="docTypeId">Doc Type ID.</param>
        /// <param name="internalComments">Internal description/comments.</param>
        /// <param name="description">Public description.</param>
        /// <param name="filePath">Temp path of uploaded file.</param>
        /// <returns>Document ID.</returns>
        /// <exception cref="XmlException">Throws if the file at the given path does not contain valid XML.</exception>
        /// <exception cref="DeveloperException">Throws if the file payload is not valid LQB Appraisal or MISMO UCD XML.</exception>
        private static Guid UploadXmlDoc(AbstractUserPrincipal principal, Guid loanId, Guid appId, int docTypeId, string internalComments, string description, string filePath)
        {
            XDocument xmlDoc = XDocument.Load(filePath);
            string pdfFile;
            E_FileType fileType;
            if (AppraisalGenericEDocument.IsAppraisalXmlValid(xmlDoc))
            {
                fileType = E_FileType.AppraisalXml;
                pdfFile = ParseAppraisalDocFromXml(xmlDoc);
            }
            else if (UcdGenericEDocument.IsUcdStringValid(xmlDoc, principal.BrokerId))
            {
                fileType = E_FileType.UniformClosingDataset;
                pdfFile = ParseUcdDocFromXml(xmlDoc);
            }
            else
            {
                fileType = E_FileType.UnspecifiedXml;
                pdfFile = null;
            }

            Guid docId = pdfFile == null ? Guid.NewGuid() : UploadPdfDoc(principal, loanId, appId, docTypeId, description, internalComments, pdfFile, true);

            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            var xmleDoc = repo.CreateLinkedGenericDocument(fileType, docId);
            xmleDoc.ApplicationId = appId;
            xmleDoc.Description = description;
            xmleDoc.DocTypeId = docTypeId;
            xmleDoc.InternalComments = internalComments;
            xmleDoc.LoanId = loanId;
            xmleDoc.SetContent(filePath, "document.xml");
            repo.Save(xmleDoc, principal.UserId);

            return docId;
        }

        /// <summary>
        /// Extract Appraisal PDF file from an XML file.
        /// </summary>
        /// <param name="xmlDoc">XML document.</param>
        /// <returns>PDF file path.</returns>
        private static string ParseAppraisalDocFromXml(XDocument xmlDoc)
        {
            XElement documentElement = xmlDoc.XPathSelectElement("/VALUATION_RESPONSE/REPORT/EMBEDDED_FILE/DOCUMENT");
            if (documentElement == null || string.IsNullOrEmpty(documentElement.Value))
            {
                documentElement = xmlDoc.XPathSelectElement("/VALUATION_RESPONSE/REPORT/FORM/IMAGE/EMBEDDED_FILE[@_Type='PDF']/DOCUMENT");
            }

            if (documentElement == null || string.IsNullOrEmpty(documentElement.Value))
            {
                throw new CBaseException(ErrorMessages.EDocs.InvalidXML, "Xml file doesn't contain one of the pdf paths");
            }

            return ExtractPdfFromNode(documentElement);
        }

        /// <summary>
        /// Extracts Base64 encoded PDF from XML node.
        /// </summary>
        /// <param name="pdfNode">An XML node.</param>
        /// <returns>XML in string format.</returns>
        private static string ExtractPdfFromNode(XElement pdfNode)
        {
            string pdfContent = pdfNode.Value;
            byte[] pdfByteContent = Convert.FromBase64String(pdfContent);

            string pdfPath = TempFileUtils.NewTempFilePath();
            using (FileStream fs = File.OpenWrite(pdfPath))
            {
                fs.Write(pdfByteContent, 0, pdfByteContent.Length);
            }

            return pdfPath;
        }

        /// <summary>
        /// Upload Excel Doc to EDocs.
        /// </summary>
        /// <param name="principal">User Principal.</param>
        /// <param name="loanId">A Loan ID.</param>
        /// <param name="appId">Application ID.</param>
        /// <param name="docTypeId">Doc Type ID.</param>
        /// <param name="internalComments">Internal description/comments.</param>
        /// <param name="description">Public description.</param>
        /// <param name="filePath">Temp path of uploaded file.</param>
        /// <param name="uploadedFileName">Original name of uploaded file.</param>
        /// <returns>Document ID.</returns>
        private static Guid UploadExcelDoc(AbstractUserPrincipal principal, Guid loanId, Guid appId, int docTypeId, string internalComments, string description, string filePath, string uploadedFileName)
        {
            if (!principal.BrokerDB.AllowExcelFilesInEDocs)
            {
                string msg = "Uploading Excel files is not allowed.";
                throw new CBaseException(msg, msg);
            }

            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            GenericEDocument genericDocument = repo.CreateGenericDocument(E_FileType.MicrosoftSpreadsheet);
            genericDocument.ApplicationId = appId;
            genericDocument.Description = description;
            genericDocument.DocTypeId = docTypeId;
            genericDocument.InternalComments = internalComments;
            genericDocument.LoanId = loanId;
            genericDocument.SetContent(filePath, uploadedFileName);
            repo.Save(genericDocument, principal.UserId);

            return genericDocument.DocumentId;
        }
    }
}
