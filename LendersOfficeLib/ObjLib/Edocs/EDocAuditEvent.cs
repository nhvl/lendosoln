﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using CommonProjectLib.Common.Lib;
using System.Runtime.Serialization;
using LendersOffice.Common;

namespace EDocs
{

    public enum E_PdfUserAction
    {
        InsertPage,
        ReplacePage,
        DeletePages,
        SplitPages,
        MovePages,
        DeleteAnnotations, 
        EditInternalComments,
        EditDescription,
        EditDocType,
        SentToConsumer,
        ConsumerSignature, 
        LenderSignature,
        EditStatus,
        DeleteStatus,
        PastedAnnotations,
        ModifiedAnnotations,
        Generated,
        EditHideFromPMLUsers,
        UnableToDetermineSignatureStatus,
        UnableToDetermineDocType,
        ModifiedESignTags,
    }


    [DataContract]
    internal class EDocumentIntermediateAuditEvent
    {
        [DataMember]
        public E_PdfUserAction UserAction { get; set; }
  
        [DataMember]
        public DateTime ActionD { get; set; }

        [DataMember]
        public Guid? UserId { get; set; }

        public string PreviousValue { get; set; }
        public string NewValue { get; set; }
        
        public string CustomDescription { get; set; }
    }
    /// <summary>
    /// Keeps track of user actions to an edoc while they are performed this is needed for OPM 67127
    /// </summary>
    [DataContract]
    public class EDocumentIntermediateAuditTracker
    {
        [DataMember]
        private List<EDocumentIntermediateAuditEvent> m_userActions;

        /// <summary>
        /// Constructor for the DataContract
        /// </summary>
        public EDocumentIntermediateAuditTracker() { }


        /// <summary>
        /// Creates a new EDoc Intermediate tracker. 
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="userId"></param>
        public EDocumentIntermediateAuditTracker(Guid documentId, Guid userId)
        {
            m_userActions = new List<EDocumentIntermediateAuditEvent>();
            DocumentId = documentId;
            UserId = userId;
            StorageKey = Tools.ShortenGuid(Guid.NewGuid());
            IsNew = true;
        }

        [DataMember]
        public string StorageKey
        {
            get;
            private set;
        }

        private bool IsNew
        {
            get;
            set;
        }

        [DataMember]
        public Guid DocumentId
        {
            get;
            private set;
        }

        [DataMember]
        public Guid UserId
        {
            get;
            private set;
        }

        public void AddEvent(E_PdfUserAction id)
        {
            if (id == E_PdfUserAction.EditInternalComments || id == E_PdfUserAction.EditDescription || id == E_PdfUserAction.EditDocType)
            {
                throw CBaseException.GenericException("Cannot use audit tracker to record description,doctype,comment changes.");
            }

            m_userActions.Add(new EDocumentIntermediateAuditEvent()
            { 
                UserAction = id,
                ActionD = DateTime.Now,
                UserId = UserId
            });
        }

        public void Save()
        {
            string data = ObsoleteSerializationHelper.JsonSerialize(this);
            if (IsNew)
            {
                AutoExpiredTextCache.AddToCache(data, TimeSpan.FromHours(4), StorageKey);
            }
            else
            {
                AutoExpiredTextCache.UpdateCache(data, StorageKey);
            }
        }

        public static EDocumentIntermediateAuditTracker Retrieve(string storageKey, Guid userId)
        {
            EDocumentIntermediateAuditTracker tracker = RetrieveImpl(storageKey);
            if (tracker == null)
            {
                return tracker;
            }
            if (tracker.UserId != userId)
            {
                throw CBaseException.GenericException("Tried retrieving audit details for a different user.");
            }
            return tracker;
        }

        public static EDocumentIntermediateAuditTracker Retrieve(string storageKey, Guid userId, Guid documentId)
        {
            if (string.IsNullOrEmpty(storageKey))
            {
                return new EDocumentIntermediateAuditTracker(documentId, userId);
            }

            EDocumentIntermediateAuditTracker tracker = RetrieveImpl(storageKey);
            if (tracker.UserId != userId || tracker.DocumentId != documentId)
            {
                throw CBaseException.GenericException("Tried retrieving a object that does not belong to the given user/document.");
            }
            return tracker;
        }

        private static EDocumentIntermediateAuditTracker  RetrieveImpl(string storageKey)
        {
            if (string.IsNullOrEmpty(storageKey))
            {
                return null;
            }
            string data = AutoExpiredTextCache.GetFromCache(storageKey);
            EDocumentIntermediateAuditTracker tracker = ObsoleteSerializationHelper.JsonDeserialize<EDocumentIntermediateAuditTracker>(data);
            return tracker;
        }

        internal IEnumerable<EDocumentIntermediateAuditEvent> Items
        {
            get { return m_userActions.AsReadOnly(); }
        }

       
    }
    /// <summary>
    /// Represents a read only view of the edits done to the edocument. 
    /// </summary>
    public sealed class EDocumentAuditEvent
    {
        private Guid? m_UserId;
        private DateTime m_ModifiedDate;
        private int m_Version;
        private string m_sDescription;
        private string m_sName;


        public Guid? UserId
        {
            get { return m_UserId; }
        }

        public DateTime ModifiedDate
        {
            get { return m_ModifiedDate; } 
        }

        public int Version
        {
            get { return m_Version; }
        }

        public string Description
        {
            get { return m_sDescription; }
        }

        public string Name
        {
            get { return m_sName; }
        }

        public string PreviousValue
        {
            get;
            private set;
        }

        public string NewValue
        {
            get;
            private set;
        }

        public bool HasDetails 
        {
            get;
            private set;
        }



        private EDocumentAuditEvent(DbDataReader data)
        {
            m_UserId = data["ModifiedByUserId"] == DBNull.Value ? new Guid?() : (Guid)data["ModifiedByUserId"];
            m_ModifiedDate =(DateTime) data["ModifiedDate"];
            m_sDescription = (string)data["Description"];
            m_Version = (int)data["Version"];
            m_sName = data["Name"] == DBNull.Value ? "EDMS System" : (string)data["Name"];
            string sDetails = data["Details"] == DBNull.Value ? "" : (string)data["Details"];

            if( false == string.IsNullOrEmpty(sDetails ))
            {
                ExtraDetails details = ObsoleteSerializationHelper.JsonDeserialize<ExtraDetails>(sDetails);
                PreviousValue = details.PreviousValue;
                NewValue = details.NewValue;
                HasDetails = true;
            }
            
        }


        public static IList<EDocumentAuditEvent> RetrieveAuditEventsForDoc(Guid brokerId, Guid documentId) 
        {
            List<EDocumentAuditEvent> events = new List<EDocumentAuditEvent>();
            SqlParameter[] parameters = {
                                            new SqlParameter("@DocumentId", documentId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_FetchAuditEvents", parameters))
            {
                while (reader.Read())
                {
                    events.Add(new EDocumentAuditEvent(reader));
                }
            }

            return events;
        }

        private static string GetDescription( E_PdfUserAction action)
        {
            switch (action)
            {
                case E_PdfUserAction.InsertPage:
                    return "Insert Pages";
                case E_PdfUserAction.ReplacePage:
                    return "Replace Pages";
                case E_PdfUserAction.DeletePages:
                    return "Delete Pages";
                case E_PdfUserAction.SplitPages:
                    return "Split Pages";
                case E_PdfUserAction.MovePages:
                    return "Move Pages";
                case E_PdfUserAction.DeleteAnnotations:
                    return "Delete Notations";
                case E_PdfUserAction.EditInternalComments:
                    return "Edit Internal Comments";
                case E_PdfUserAction.EditDescription:
                    return "Edit Description";
                case E_PdfUserAction.EditDocType:
                    return "Change Doc Type";
                case E_PdfUserAction.SentToConsumer:
                    return "Date sent to Consumer";
                case E_PdfUserAction.ConsumerSignature:
                    return "Date Signed by Consumer";
                case E_PdfUserAction.LenderSignature:
                    return "Signed by Lender";
                case E_PdfUserAction.EditStatus:
                    return "EDoc status changed";
                case E_PdfUserAction.DeleteStatus:
                    return "EDoc status removed";
                case E_PdfUserAction.PastedAnnotations:
                    return "Pasted Annotations";
                case E_PdfUserAction.ModifiedAnnotations:
                    return "Modified Annotations";
                case E_PdfUserAction.Generated:
                    return "Generated";
                case E_PdfUserAction.EditHideFromPMLUsers:
                    return "EDoc visibility for PML Users changed";
                case E_PdfUserAction.UnableToDetermineSignatureStatus:
                    return "Unable to determine signature status on upload. Defaulted to not e-signed.";
                case E_PdfUserAction.UnableToDetermineDocType:
                    return "Unable to determine DocType. Defaulted to Unclassified DocType.";
                case E_PdfUserAction.ModifiedESignTags:
                    return "Modified E-Sign Tags";
                default:
                    throw new UnhandledEnumException(action);
            }
        }

        internal static void InsertEntry(Guid brokerId, Guid documentId, EDocumentIntermediateAuditEvent entry)
        {
            string details  = "";

            if (entry.UserAction == E_PdfUserAction.EditInternalComments || entry.UserAction == E_PdfUserAction.EditDescription)
            {
                ExtraDetails dt = new ExtraDetails()
                {
                    NewValue = entry.NewValue ?? "",
                    PreviousValue = entry.PreviousValue ?? ""
                };
                details = ObsoleteSerializationHelper.JsonSerialize(dt);
            }
            string description = string.IsNullOrEmpty(entry.CustomDescription) ? GetDescription(entry.UserAction) : entry.CustomDescription;
            if (entry.UserAction == E_PdfUserAction.EditStatus || entry.UserAction == E_PdfUserAction.EditHideFromPMLUsers)
            {
                description += " to: " + entry.NewValue; 
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@ModifiedByUserId", entry.UserId),
                                            new SqlParameter("@ModifiedDate", entry.ActionD),
                                            new SqlParameter("@Description", description ),
                                            new SqlParameter("@DocumentId", documentId),
                                            new SqlParameter("@Details", details)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "EDOCS_AddAuditEvent", 2, parameters);
        }

        [DataContract]
        private class ExtraDetails
        {
            [DataMember]
            public string PreviousValue { get; set; }
            [DataMember]
            public string NewValue { get; set; }
        }
		
    }
}
