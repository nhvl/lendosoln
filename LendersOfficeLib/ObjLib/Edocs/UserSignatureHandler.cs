﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Web;
using System.Globalization;
using LendersOffice.Admin;
using LendersOffice.Security;
using DataAccess;
using System.Threading;
using System.Security.Principal;

namespace EDocs
{
    public class UserSignatureHandler : System.Web.IHttpHandler
    {
        private static byte[] x_byteSignature = null; 
        #region IHttpHandler Members

        private static byte[] BlankSignature
        {
            get
            {
                if (x_byteSignature == null)
                {
                    
                    Bitmap objBmpImage = new Bitmap(160, 18);
                    Graphics objGraphics = Graphics.FromImage(objBmpImage);
                    objGraphics.Clear(Color.White);
                    objGraphics.Flush();
                    using (MemoryStream ms = new MemoryStream())
                    {
                        objBmpImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                        x_byteSignature = ms.ToArray();
                    }                    
                }

                return x_byteSignature;
            }
        }

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            string eid = context.Request.QueryString["eid"];
            if (string.IsNullOrEmpty(eid))
            {
                OutputEmptySignature(context);
            }
            else
            {
                EmployeeSignatureInfo signInfo;
                IPrincipal p = Thread.CurrentPrincipal;
                if (!((p as AbstractUserPrincipal)?.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp ?? false))
                {
                    context.Response.StatusCode = 404; // No signature allowed, not even blank
                    return;
                }

                bool enableLqbNonCompliantDocSignStamp = (p as AbstractUserPrincipal).BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp;

                if (p is InternalUserPrincipal)
                {
                    string sbrokerid = context.Request.QueryString["brokerid"];
                    Guid brokerId = new Guid(sbrokerid);
                    signInfo = EmployeeSignatureInfo.Load(brokerId, new Guid(eid), enableLqbNonCompliantDocSignStamp);
                }
                else if (p is BrokerUserPrincipal)
                {
                    BrokerUserPrincipal brokerUser = (BrokerUserPrincipal)p;
                    signInfo = EmployeeSignatureInfo.Load(brokerUser.BrokerId, new Guid(eid), enableLqbNonCompliantDocSignStamp);
                }
                else
                {
                    Tools.LogError("Unexpected principal when generating user signature " + p);
                    OutputEmptySignature(context);
                    return;
                }

                if (signInfo.SignatureKey == Guid.Empty)
                {
                    OutputEmptySignature(context);
                    return;
                }
                string path = FileDBTools.CreateCopy(E_FileDB.Normal, signInfo.SignatureKey.ToString());
                context.Response.ContentType = "image/png";
                context.Response.WriteFile(path);
            }
        }

        private void OutputEmptySignature(HttpContext context)
        {


            CultureInfo provider = CultureInfo.InvariantCulture;
            DateTime lastMod;
            if (DateTime.TryParseExact(context.Request.Headers["If-Modified-Since"], "r", provider,
                DateTimeStyles.AllowInnerWhite |
                DateTimeStyles.AllowLeadingWhite |
                DateTimeStyles.AllowTrailingWhite |
                DateTimeStyles.AllowWhiteSpaces, out lastMod))
            {
                context.Response.StatusCode = 304;
                context.Response.StatusDescription = "Not Modified";
                return;
            }
            context.Response.ContentType = "image/png";
            context.Response.OutputStream.Write(BlankSignature, 0, BlankSignature.Length);
            context.Response.Cache.SetCacheability(HttpCacheability.Public);
            context.Response.Cache.SetLastModified(DateTime.Today);
        }

        #endregion
    }
}
