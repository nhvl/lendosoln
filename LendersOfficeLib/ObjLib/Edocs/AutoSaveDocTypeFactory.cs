﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using DataAccess;
using EDocs;
using EDocs.Contents;
using LendersOffice.AntiXss;
using LendersOffice.Integration.Appraisals;
using LendersOffice.Integration.MortgageInsurance;
using LendersOffice.Security;

namespace LendersOffice.ObjLib.Edocs.AutoSaveDocType
{
    public class DefaultDocTypeInfo
    {
        public readonly E_AutoSavePage PageId;
        public readonly string PageFriendlyName;
        public string DocTypeName { get; private set; }
        public string Folder { get; private set; }
        public DocType DocType { get { return m_DocType; } }
        private DocType m_DocType;

        public DefaultDocTypeInfo(E_AutoSavePage pageid, string folder, string dtName, string friendlyName)
        {
            PageId = pageid;
            PageFriendlyName = friendlyName;
            DocTypeName = dtName;
            Folder = folder;
        }

        public string folderAndDoctypeName
        {
            get
            {
                return Folder + " : " + DocTypeName;
            }
        }

        public void PickDocType(IEnumerable<DocType> docTypes)
        {
            var dt = docTypes.Where(a => a.DefaultPage != E_AutoSavePage.None).FirstOrDefault(a => a.DefaultPage == PageId);
            
            SetDocType(dt);
            return;
        }

        public void SetDocType(DocType dt)
        {
            m_DocType = dt;

            if (dt == null) return;
            DocTypeName = dt.DocTypeName;
            Folder = dt.Folder.FolderNm;
        }

        public DocType GetOrCreate()
        {
            return GetOrCreate(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
        }

        public DocType GetOrCreate(Guid BrokerId)
        {
            var id = EDocumentDocType.GetOrCreateDocType(BrokerId, DocTypeName, Folder);
            return EDocumentDocType.GetDocTypeById(BrokerId, id);
        }
    }

    public static class AutoSaveDocTypeFactory
    {
        private const string BillingReportDescription = "Billing Report";
        private const string ComplianceReportDescription = "Compliance Report";
        private const string CreditReportLqiDescription = "Credit Report - LQI";
        private const string CreditSupplementDescription = "Credit Supplement";
        private const string ESignedCertificateDescription = "Electronic Signature Certificate";
        private const string ESignedDocumentDescription = "Electronic Signature Document";
        private const string Irs4506TDocumentsDescription = "IRS 4506-T Tax Return Transcript";
        private const string PaymentStatement = "Payment Statement";
        private const string UcdFindingsDescription = "UCD Findings";
        private const string VerbalCreditAuthorizationDescription = "Verbal Credit Authorization";
        private const string VoaVodDocumentsDescription = "VOA/VOD Documents";
        private const string VoeVoiDocumentsDescription = "VOE/VOI Documents";
        private static readonly ReadOnlyDictionary<E_AutoSavePage, string> PageIdToFriendlyName = new ReadOnlyDictionary<E_AutoSavePage, string>(new Dictionary<E_AutoSavePage, string>()
        {
            { E_AutoSavePage.BillingReport, BillingReportDescription },
            { E_AutoSavePage.ComplianceEase, ComplianceReportDescription },
            { E_AutoSavePage.CreditReportLqi, CreditReportLqiDescription },
            { E_AutoSavePage.CreditSupplement, CreditSupplementDescription },
            { E_AutoSavePage.ESignedCertificate, ESignedCertificateDescription },
            { E_AutoSavePage.ESignedDocument, ESignedDocumentDescription },
            { E_AutoSavePage.Irs4506TDocuments, Irs4506TDocumentsDescription },
            { E_AutoSavePage.PaymentStatement, PaymentStatement },
            { E_AutoSavePage.UniformClosingDatasetFindings, UcdFindingsDescription },
            { E_AutoSavePage.VerbalCreditAuthorization, VerbalCreditAuthorizationDescription },
            { E_AutoSavePage.VoaVodDocuments, VoaVodDocumentsDescription },
            { E_AutoSavePage.VoeVoiDocuments, VoeVoiDocumentsDescription },
        });

        public static List<DefaultDocTypeInfo> GetDefaultFolders(Guid BrokerId, E_EnforceFolderPermissions enforceFolderPermissionsT)
        {
            IEnumerable<DocType> availableDocTypes = EDocumentDocType.GetDocTypesByBroker(BrokerId, enforceFolderPermissionsT);
            List<DefaultDocTypeInfo> defaults = GetDefaultFolders().Concat(GetBrokerDefaultFolders(BrokerId)).ToList();

            foreach (var d in defaults)
            {
                d.PickDocType(availableDocTypes);
            }

            return defaults;
        }

        public static DefaultDocTypeInfo GetDocTypeForPageId(E_AutoSavePage PageId, Guid BrokerId, E_EnforceFolderPermissions enforceFolderPermissionsT, bool fallbackToUnclassifiedForCertainDocTypes = true)
        {
            DefaultDocTypeInfo info = GetDefaultFolders(BrokerId, enforceFolderPermissionsT).FirstOrDefault(a => a.PageId == PageId);

            if (fallbackToUnclassifiedForCertainDocTypes && (info == null || info.DocType == null))
            {
                string friendlyName = string.Empty;
                if (PageIdToFriendlyName.TryGetValue(PageId, out friendlyName))
                {
                    info = GetUnclassifiedDocTypeInfo(PageId, BrokerId, friendlyName);
                }
            }

            return info;
        }

        public static DefaultDocTypeInfo GetUnclassifiedDocTypeInfo(E_AutoSavePage pageId, Guid brokerId, string friendlyName)
        {
            int unclassifiedId = EDocumentDocType.GetOrCreateDocType(brokerId, "UNCLASSIFIED", EDocumentFolder.UNCLASSIFIED_FOLDERNAME);
            DocType unclassifiedType = EDocumentDocType.GetDocTypeById(brokerId, unclassifiedId);
            DefaultDocTypeInfo info = new DefaultDocTypeInfo(pageId, unclassifiedType.Folder.FolderNm, unclassifiedType.DocTypeName, friendlyName);
            info.SetDocType(unclassifiedType);
            return info;
        }

        public static bool IsAutoSaveEnabled(E_AutoSavePage PageId, Guid BrokerId, E_EnforceFolderPermissions enforceFolderPermissionsT)
        {
            return GetDefaultFolders(BrokerId, enforceFolderPermissionsT).Any(a => a.PageId == PageId && a.DocType != null);
        }

        public static void ClearDocTypesForPageId(E_AutoSavePage PageId, Guid BrokerId)
        {
            foreach (var dt in GetDefaultFolders(BrokerId, E_EnforceFolderPermissions.True).Where(a => a.PageId == PageId && a.DocType != null))
            {
                dt.DocType.DefaultPage = E_AutoSavePage.None;
                dt.DocType.Save(BrokerId);
            }
        }

        /// <summary>
        /// These folders we need to give names based on broker configuration information.
        /// </summary>
        /// <param name="BrokerId"></param>
        /// <returns></returns>
        private static IEnumerable<DefaultDocTypeInfo> GetBrokerDefaultFolders(Guid BrokerId)
        {
            if (TitleProvider.TitleProvider.GetAssociations(BrokerId).Count > 0)
            {
                yield return new DefaultDocTypeInfo(E_AutoSavePage.MiscTitleDocuments, "TITLE DOCUMENTS", "TITLE DOCUMENT", "Title Documents");
            }

            if (AppraisalVendorFactory.GetAvailableVendorsForBroker(BrokerId).Any())
            {
                yield return new DefaultDocTypeInfo(E_AutoSavePage.AppraisalDocs, "APPRAISAL DOCUMENTS", "APPRAISAL DOCUMENT", "Appraisal Documents");
            }

            if (MortgageInsuranceVendorBrokerSettings.ListActiveVendorByBrokerId(BrokerId).Any())
            {
                yield return new DefaultDocTypeInfo(E_AutoSavePage.MortgageInsuranceQuoteDocuments, "MORTGAGE INSURANCE DOCUMENTS", "MORTGAGE INSURANCE QUOTE DOCUMENT", "Mortgage Insurance Quote Documents");
                yield return new DefaultDocTypeInfo(E_AutoSavePage.MortgageInsurancePolicyDocuments, "MORTGAGE INSURANCE DOCUMENTS", "MORTGAGE INSURANCE POLICY DOCUMENT", "Mortgage Insurance Policy Documents");
            }

            yield break;
            
            //var vendor = DocumentVendorFactory.Create(BrokerId);
            //yield return new DefaultDocTypeInfo(E_AutoSavePage.GeneratedDocs, vendor.Skin.VendorName.ToUpper(), "GENERATED DOCS", "Generated Documents");

            //etc.

        }

        private static IEnumerable<DefaultDocTypeInfo> GetDefaultFolders()
        {
            return new List<DefaultDocTypeInfo>()
            {
                new DefaultDocTypeInfo(E_AutoSavePage.PmlSummary, EDocumentFolder.LENDINGQB_FOLDERNAME, "PML SUMMARY", "PML Summary"),
                new DefaultDocTypeInfo(E_AutoSavePage.LPFindings, EDocumentFolder.LENDINGQB_FOLDERNAME, "LP FEEDBACK", "LPA Feedback"),
                new DefaultDocTypeInfo(E_AutoSavePage.DODUFindings, EDocumentFolder.LENDINGQB_FOLDERNAME, "DO/DU FINDINGS", "DO/DU Findings"),

                new DefaultDocTypeInfo(E_AutoSavePage.ApprovalCert, EDocumentFolder.LENDINGQB_UNDERWRITING_FOLDERNAME, "APPROVAL CERTIFICATE", "Approval Certificate"),
                new DefaultDocTypeInfo(E_AutoSavePage.SuspenseNotice, EDocumentFolder.LENDINGQB_UNDERWRITING_FOLDERNAME, "SUSPENSE NOTICE", "Suspense Notice"),
                new DefaultDocTypeInfo(E_AutoSavePage.CertSubmitted, EDocumentFolder.LENDINGQB_FOLDERNAME, "CERTIFICATE SUBMITTED", "Cert Submitted"),
                new DefaultDocTypeInfo(E_AutoSavePage.LockConf, EDocumentFolder.LENDINGQB_LOCKDESK_FOLDERNAME, "LOCK CONFIRMATION", "Lock Confirmation"),

                new DefaultDocTypeInfo(E_AutoSavePage.CreditReport, EDocumentFolder.LENDINGQB_FOLDERNAME, "CREDIT REPORT", "Credit Report"),
                new DefaultDocTypeInfo(E_AutoSavePage.CreditReportLqi, EDocumentFolder.LENDINGQB_FOLDERNAME, "LOAN QUALITY INITIATIVE CREDIT REPORT", CreditReportLqiDescription),
                new DefaultDocTypeInfo(E_AutoSavePage.CreditSupplement, EDocumentFolder.LENDINGQB_FOLDERNAME, "CREDIT SUPPLEMENT", CreditSupplementDescription),
                new DefaultDocTypeInfo(E_AutoSavePage.BillingReport, EDocumentFolder.LENDINGQB_FOLDERNAME, "BILLING REPORT", BillingReportDescription),
                new DefaultDocTypeInfo(E_AutoSavePage.FHATotalScorecard, EDocumentFolder.LENDINGQB_FOLDERNAME, "TOTAL SCORECARD", "FHA TOTAL Scorecard"),

                new DefaultDocTypeInfo(E_AutoSavePage.FHACaseAssignment, EDocumentFolder.FHA_CONNECTION_FOLDERDNAME, "CASE ASSIGNMENT", "FHA Case Assignment"),
                new DefaultDocTypeInfo(E_AutoSavePage.FHACaseQuery, EDocumentFolder.FHA_CONNECTION_FOLDERDNAME, "CASE QUERY", "FHA Case Query"),
                new DefaultDocTypeInfo(E_AutoSavePage.FHACAIVRSAuthorization, EDocumentFolder.FHA_CONNECTION_FOLDERDNAME, "CAIVRS AUTHORIZATION", "FHA CAIVRS Authorization"),

                new DefaultDocTypeInfo(E_AutoSavePage.ComplianceEase, EDocumentFolder.COMPLIANCEEASE_FOLDERNAME, "COMPLIANCEREPORT", ComplianceReportDescription),

                new DefaultDocTypeInfo(E_AutoSavePage.DataVerifyDRIVE, EDocumentFolder.DATAVERIFY_FOLDERNAME, "DRIVE REPORT", "DRIVE Report"),

                new DefaultDocTypeInfo(E_AutoSavePage.FannieMaeEarlyCheckResults, EDocumentFolder.LENDINGQB_FOLDERNAME, "FANNIE MAE EARLYCHECK RESULT", "Fannie Mae EarlyCheck Result"),
                new DefaultDocTypeInfo(E_AutoSavePage.Irs4506TDocuments, EDocumentFolder.IRS4506T_FOLDERNAME, "IRS 4506-T TAX RETURN TRANSCRIPT", Irs4506TDocumentsDescription),

                new DefaultDocTypeInfo(E_AutoSavePage.UniformClosingDataset, EDocumentFolder.LENDINGQB_FOLDERNAME, "UNIFORM CLOSING DATASET", "Uniform Closing Dataset"),
                new DefaultDocTypeInfo(E_AutoSavePage.UniformClosingDatasetFindings, EDocumentFolder.LENDINGQB_FOLDERNAME, "UNIFORM CLOSING DATASET FINDINGS", UcdFindingsDescription),

                new DefaultDocTypeInfo(E_AutoSavePage.ConsumerPortalCreditAuthorization, EDocumentFolder.LENDINGQB_FOLDERNAME, "CREDIT AUTHORIZATION", "Consumer Portal Credit Authorization"),

                new DefaultDocTypeInfo(E_AutoSavePage.VoaVodDocuments, EDocumentFolder.LENDINGQB_FOLDERNAME, "VOA/VOD", VoaVodDocumentsDescription),
                new DefaultDocTypeInfo(E_AutoSavePage.VoeVoiDocuments, EDocumentFolder.LENDINGQB_FOLDERNAME, "VOE/VOI", VoeVoiDocumentsDescription),
                new DefaultDocTypeInfo(E_AutoSavePage.SSA89Documents, EDocumentFolder.LENDINGQB_FOLDERNAME, "SSN VERIFICATION", "SSA-89 Documents"),
                new DefaultDocTypeInfo(E_AutoSavePage.PaymentStatement, EDocumentFolder.LENDINGQB_FOLDERNAME, "PAYMENT STATEMENT", PaymentStatement),

                new DefaultDocTypeInfo(E_AutoSavePage.ESignedCertificate, EDocumentFolder.LENDINGQB_FOLDERNAME, "ESIGNED CERTIFICATE", ESignedCertificateDescription),
                new DefaultDocTypeInfo(E_AutoSavePage.ESignedDocument, EDocumentFolder.LENDINGQB_FOLDERNAME, "ESIGNED DOCUMENT", ESignedDocumentDescription),

                new DefaultDocTypeInfo(E_AutoSavePage.VerbalCreditAuthorization, EDocumentFolder.LENDINGQB_FOLDERNAME, "VERBAL CREDIT AUTHORIZATION", VerbalCreditAuthorizationDescription),
            };
        }

        private static void SaveDocFromFile(Func<FileInfo, string> GetPathToBytes, E_AutoSavePage pageId, string FileDBKey, E_FileDB fdb, Guid LoanId, Guid AppId, AbstractUserPrincipal user)
        {
            SaveDocFromFile(GetPathToBytes, pageId, FileDBKey, fdb, user.BrokerId, LoanId, AppId, user.UserId, "");
        }

        private static Guid SaveDocFromFile(Func<FileInfo, string> GetPathToBytes, E_AutoSavePage pageId, string FileDBKey, E_FileDB fdb, Guid BrokerId, Guid LoanId, Guid AppId, Guid? UserId, string description)
        {
            //Cut out early if we aren't going to save, since it may be expensive to call GetPathToBytes.
            var saveAs = GetDocTypeForPageId(pageId, BrokerId, E_EnforceFolderPermissions.True);
            if (saveAs == null || saveAs.DocType == null) return Guid.Empty;

            Guid docId = Guid.Empty;
            FileDBTools.UseFile(fdb, FileDBKey, (info) =>
            {
                string pdfPath = GetPathToBytes(info);
                docId = SaveDoc(pdfPath, saveAs, LoanId, AppId, BrokerId, UserId, description);
            });

            return docId;
        }

        private static Guid SaveDoc(string pdfPath, DefaultDocTypeInfo saveAs, Guid LoanId, Guid AppId, Guid brokerId,  Guid? userid, string description)
        {
            try
            {
                EDocumentRepository repo = EDocumentRepository.GetSystemRepository(brokerId);
                EDocument doc = new EDocument(E_EDocumentSource.GeneratedDocs, brokerId);

                var doctype = saveAs.DocType;

                doc.DocumentTypeId = doctype.Id;
                doc.LoanId = LoanId;
                doc.AppId = AppId;
                doc.IsUploadedByPmlUser = false;
                doc.PublicDescription = string.IsNullOrEmpty(description) ? saveAs.PageFriendlyName : description;
                doc.EDocOrigin = E_EDocOrigin.LO;

                doc.UpdatePDFContentOnSave(pdfPath);
                doc.AddAuditEntry(E_PdfUserAction.Generated, DateTime.Now, userid);
                repo.Save(doc);
                return doc.DocumentId;
            }
            catch (Exception e)
            {
                Tools.LogError("Error while automatically saving file for page id " + saveAs.PageId, e);
                throw;
            }
        }

        private static Guid SaveXmlDoc(string xmlPath, DefaultDocTypeInfo saveAs, Guid LoanId, Guid AppId, Guid brokerId, Guid? userId, string description, Guid docId, E_FileType fileType)
        {
            try
            {
                EDocumentRepository repo = EDocumentRepository.GetSystemRepository(brokerId);

                var doctype = saveAs.DocType;

                GenericEDocument xmleDoc = repo.CreateLinkedGenericDocument(fileType, docId);

                xmleDoc.ApplicationId = AppId;
                xmleDoc.Description = description;
                xmleDoc.InternalComments = "";
                xmleDoc.DocTypeId = doctype.Id;
                xmleDoc.LoanId = LoanId;
                xmleDoc.SetContent(xmlPath, "document.xml");
                repo.Save(xmleDoc, userId.HasValue ? userId.Value : Guid.Empty);

                return xmleDoc.DocumentId;
            }
            catch (Exception e)
            {
                Tools.LogError("Error while automatically saving xml file for page id " + saveAs.PageId, e);
                throw;
            }
        }

        /// <summary>
        /// Gets a temp file path with the given extension.
        /// </summary>
        /// <param name="pageId">The type of autosave doc.</param>
        /// <param name="extension">The file extension for the path. Do not include the '.'.</param>
        /// <returns></returns>
        private static string GetTempFilePath(E_AutoSavePage pageId, string extension)
        {
            return string.Format("{0}_autosave_{1}.{2}", TempFileUtils.NewTempFilePath(),
                pageId.ToString(), extension);
        }

        #region Saving Pdf Docs

        /// <summary>
        /// Autosaves a pdf document that is in FileDB. 
        /// Does nothing if no autosave doc type is set up for the given page id.
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="FileDBKey"></param>
        /// <param name="fdb"></param>
        /// <param name="LoanId"></param>
        /// <param name="AppId"></param>
        /// <param name="user"></param>
        public static void SavePdfDoc(E_AutoSavePage pageId, string FileDBKey, E_FileDB fdb, Guid LoanId, Guid AppId, AbstractUserPrincipal user)
        {
            SaveDocFromFile(info => info.FullName,
                pageId, FileDBKey, fdb, LoanId, AppId, user);
        }

        /// <summary>
        /// Autosaves a pdf document that is in FileDB.
        /// Does nothing if no autosave doc type is set up for the given page id.
        /// </summary>
        /// <param name="pageId">The LendingQB Document type in the EDocs Document AutoSave Options config.</param>
        /// <param name="fileDBKey">The file db key associated with the document.</param>
        /// <param name="fdbType">The file db type.</param>
        /// <param name="brokerId">The BrokerID associated with the user performing the save.</param>
        /// <param name="loanId">The sLId of the loan file.</param>
        /// <param name="appId">The aAppId of the application.</param>
        /// <param name="userId">The userID of the user performing the save.</param>
        /// <param name="description">The EDoc description.</param>
        /// <returns>Document ID of newly saved doc.</returns>
        public static Guid SavePdfDoc(E_AutoSavePage pageId, string fileDBKey, E_FileDB fdbType, Guid brokerId, Guid loanId, Guid appId, Guid? userId, string description)
        {
            return SaveDocFromFile(info => info.FullName, pageId, fileDBKey, fdbType, brokerId, loanId, appId, userId, description);
        }

        /// <summary>
        /// Autosaves a pdf document from a byte array.
        /// Does nothing if no autosave doc type is set up for the given page id.
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="bytes></param>
        /// <param name="LoanId"></param>
        /// <param name="AppId"></param>
        /// <param name="user"></param>
        public static Guid? SavePdfDoc(E_AutoSavePage pageId, byte[] bytes, Guid LoanId, Guid AppId, AbstractUserPrincipal user)
        {
            return SavePdfDoc(pageId, bytes, user.BrokerId, LoanId, AppId, user.UserId, "", E_EnforceFolderPermissions.True);
        }
        
        /// <summary>
        /// Autosaves a pdf document from a byte array as the system.
        /// Does nothing if no autosave doc type is set up for the given page id.
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="bytes"></param>
        /// <param name="BrokerId"></param>
        /// <param name="LoanId"></param>
        /// <param name="AppId"></param>
        /// <param name="description"></param>
        public static Guid? SavePdfDocAsSystem(E_AutoSavePage pageId, byte[] bytes, Guid BrokerId, Guid LoanId, Guid AppId, string description, E_EnforceFolderPermissions enforceFolderPermissionsT)
        {
            return SavePdfDoc(pageId, bytes, BrokerId, LoanId, AppId, null, description, enforceFolderPermissionsT);
        }
        
        private static Guid? SavePdfDoc(E_AutoSavePage pageId, byte[] bytes, Guid BrokerId, Guid LoanId, Guid AppId, Guid? UserId, string description, E_EnforceFolderPermissions enforceFolderPermissionsT)
        {
            var saveAs = GetDocTypeForPageId(pageId, BrokerId, enforceFolderPermissionsT);
            if (saveAs == null || saveAs.DocType == null)
            {
                return null;
            }

            var tempPath = GetTempFilePath(pageId, "pdf");
            File.WriteAllBytes(tempPath, bytes);

            return SaveDoc(tempPath, saveAs, LoanId, AppId, BrokerId, UserId, description);
        }

        /// <summary>
        /// Autosaves a pdf document at the given path as the system.
        /// Does nothing if no autosave doc type is set up for the given page id.
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="path"></param>
        /// <param name="brokerId"></param>
        /// <param name="loanId"></param>
        /// <param name="appId"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public static Guid SavePdfDocAsSystem(E_AutoSavePage pageId, string path, Guid brokerId, Guid loanId, Guid appId, string description)
        {
            var saveAs = GetDocTypeForPageId(pageId, brokerId, E_EnforceFolderPermissions.True);
            if (saveAs == null || saveAs.DocType == null) return Guid.Empty;
            return SaveDoc(path, saveAs, loanId, appId, brokerId, null, description);
        }

        /// <summary>
        /// Auto-saves a pdf document from the given path as a user.
        /// </summary>
        /// <param name="pageId">The auto-save page id.</param>
        /// <param name="path">The path to the pdf.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="appId">The application id.</param>
        /// <param name="userId">The user id.</param>
        /// <param name="description">The description.</param>
        /// <returns>The document id of the created document. Null if no document was created.</returns>
        public static Guid? SavePdfDoc(E_AutoSavePage pageId, string path, Guid brokerId, Guid loanId, Guid appId, Guid userId, string description)
        {
            var saveAs = GetDocTypeForPageId(pageId, brokerId, E_EnforceFolderPermissions.True);
            if (saveAs == null || saveAs.DocType == null)
            {
                return null;
            }

            return SaveDoc(path, saveAs, loanId, appId, brokerId, userId, description);
        }

        /// <summary>
        /// Autosaves a pdf document that is in FileDB as the system.
        /// Does nothing if no autosave doc type is set up for the given page id.
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="FileDBKey"></param>
        /// <param name="fdb"></param>
        /// <param name="BrokerId"></param>
        /// <param name="LoanId"></param>
        /// <param name="AppId"></param>
        /// <returns>Document ID of newly saved doc.</returns>
        public static Guid SavePdfDocAsSystem(E_AutoSavePage pageId, string FileDBKey, E_FileDB fdb, Guid BrokerId, Guid LoanId, Guid AppId, string description)
        {
            return SaveDocFromFile(info => info.FullName,
                pageId, FileDBKey, fdb, BrokerId, LoanId, AppId, null, description);
        }

        /// <summary>
        /// Autosaves an ESigned pdf document at the given path, ignoring user permissions.
        /// Resolves the configured ESign target doc type ID.
        /// </summary>
        /// <param name="sourceDocType">The "source" document type of the unsigned document. The saved document type will be derived from this document type, if possible.</param>
        /// <param name="pdfPath">The file path of the PDF file to be saved.</param>
        /// <param name="brokerId">The broker ID of the doc.</param>
        /// <param name="loanId">The loan ID of the doc.</param>
        /// <param name="appId">The application ID of the doc.</param>
        /// <param name="description">The description to save the doc under.</param>
        /// <returns>The document ID of the saved document. Null if no document was created.</returns>
        public static Guid? SavePdfDocAsSystemForESignedDocument(DocType sourceDocType, string pdfPath, Guid brokerId, Guid loanId, Guid appId, string description)
        {
            DefaultDocTypeInfo saveAs;
            if (sourceDocType?.ESignTargetDocTypeId != null)
            {
                var esignTargetDocType = EDocumentDocType.GetDocTypeById(brokerId, sourceDocType.ESignTargetDocTypeId.Value);
                saveAs = new DefaultDocTypeInfo(E_AutoSavePage.None, esignTargetDocType.Folder.FolderNm, esignTargetDocType.DocTypeName, description);
                saveAs.SetDocType(esignTargetDocType);
            }
            else
            {
                // Fall back to the standard autosave type for ESigned Docs.
                saveAs = GetDocTypeForPageId(E_AutoSavePage.ESignedDocument, brokerId, E_EnforceFolderPermissions.False);
            }

            if (saveAs == null || saveAs.DocType == null)
            {
                return null;
            }

            return SaveDoc(pdfPath, saveAs, loanId, appId, brokerId, null, description);
        }

        #endregion Saving Pdf Docs

        #region Saving Html Docs

        /// <summary>
        /// Takes an HTML string and returns the path to a pdf file of that string
        /// </summary>
        /// <param name="html"></param>
        /// <param name="pageId"></param>
        /// <returns></returns>
        private static string HtmlToPdfFile(string html, E_AutoSavePage pageId)
        {
            var bytes = CPageBase.ConvertToPdf(html, GetAutoDetermineWidth(pageId));
            var tempPath = GetTempFilePath(pageId, "pdf");
            File.WriteAllBytes(tempPath, bytes);
            return tempPath;
        }

        /// <summary>
        /// Determines if the PDF Converter should try to auto-determine the page's width.
        /// Sometimes this will expand content to fill the page, other times it will 
        /// cause the page to be far too narrow.
        /// </summary>
        /// <param name="pageId"></param>
        /// <returns></returns>
        private static bool GetAutoDetermineWidth(E_AutoSavePage pageId)
        {
            switch (pageId)
            {
                case E_AutoSavePage.CertSubmitted:
                case E_AutoSavePage.FHATotalScorecard:
                case E_AutoSavePage.DataVerifyDRIVE:
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Autosaves as pdf a document that's currently in FileDB as HTML.
        /// Does nothing if no autosave doc type is set up for the given page id.
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="FileDBKey"></param>
        /// <param name="fdb"></param>
        /// <param name="LoanId"></param>
        /// <param name="AppId"></param>
        /// <param name="user"></param>
        public static void SaveHtmlDoc(E_AutoSavePage pageId, string FileDBKey, E_FileDB fdb, Guid LoanId, Guid AppId, AbstractUserPrincipal user)
        {
            //Since this is an html doc, we need to run the html-to-pdf tool on it then write that to a file and give the path to that.
            //As a bonus, since we delay this conversion until we're about to use it, we won't convert to pdf if there's no autosave doc type
            SaveDocFromFile(info => {
                var html = File.ReadAllText(info.FullName);
                var tempPath = HtmlToPdfFile(html, pageId);
                return tempPath;
            }, pageId, FileDBKey, fdb, LoanId, AppId, user);
        }

        /// <summary>
        /// Autosaves a pdf document from an HTML string.
        /// Does nothing if no autosave doc type is set up for the given page id.
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="Html"></param>
        /// <param name="LoanId"></param>
        /// <param name="AppId"></param>
        /// <param name="user"></param>
        public static Guid? SaveHtmlDoc(E_AutoSavePage pageId, Func<string> GetHtml, Guid LoanId, Guid AppId, AbstractUserPrincipal user)
        {
            return SaveHtmlDoc(pageId, GetHtml, user.BrokerId, LoanId, AppId, user.UserId, "", E_EnforceFolderPermissions.True);
        }

        /// <summary>
        /// Autosaves a pdf document from an HTML string.
        /// Does nothing if no autosave doc type is set up for the given page id.
        /// </summary>
        /// <param name="pageId">The page id.</param>
        /// <param name="Html">Function to get html.</param>
        /// <param name="LoanId">The loan id.</param>
        /// <param name="AppId">The app id.</param>
        /// <param name="description">The description for the Edoc.</param>
        /// <param name="user">The user.</param>
        public static Guid? SaveHtmlDoc(E_AutoSavePage pageId, Func<string> GetHtml, Guid LoanId, Guid AppId, string description, AbstractUserPrincipal user)
        {
            return SaveHtmlDoc(pageId, GetHtml, user.BrokerId, LoanId, AppId, user.UserId, description, E_EnforceFolderPermissions.True);
        }

        /// <summary>
        /// Autosaves a pdf document from an HTML string as the system.
        /// Does nothing if no autosave doc type is set up for the given page id.
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="GetHtml"></param>
        /// <param name="BrokerId"></param>
        /// <param name="LoanId"></param>
        /// <param name="AppId"></param>
        /// <param name="description"></param>
        public static Guid? SaveHtmlDocAsSystem(E_AutoSavePage pageId, Func<string> GetHtml, Guid BrokerId, Guid LoanId, Guid AppId, string description, E_EnforceFolderPermissions enforceFolderPermissionsT)
        {
            return SaveHtmlDoc(pageId, GetHtml, BrokerId, LoanId, AppId, null, description, enforceFolderPermissionsT);
        }
        
        private static Guid? SaveHtmlDoc(E_AutoSavePage pageId, Func<string> GetHtml, Guid BrokerId, Guid LoanId, Guid AppId, Guid? UserId, string description, E_EnforceFolderPermissions enforceFolderPermissionsT)
        {
            var saveAs = GetDocTypeForPageId(pageId, BrokerId, enforceFolderPermissionsT);
            if (saveAs == null || saveAs.DocType == null) return null;

            var pdfPath = HtmlToPdfFile(GetHtml(), pageId);
            return SaveDoc(pdfPath, saveAs, LoanId, AppId, BrokerId, UserId, description);
        }

        #endregion Saving Html Docs

        #region Saving Xml Docs

        public static Guid? SaveXmlDoc(E_AutoSavePage pageId, Func<string> GetXmlPath, Guid BrokerId, Guid LoanId, Guid AppId, Guid? UserId, string description, E_FileType? fileType = null)
        {
            var saveAs = GetDocTypeForPageId(pageId, BrokerId, E_EnforceFolderPermissions.True);
            if (saveAs == null || saveAs.DocType == null) return null;

            var xmlPath = GetXmlPath();
            var xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.Load(xmlPath);
            }
            catch (XmlException ex)
            {
                Tools.LogError("Failure parsing xml " + ex.ToString());
                throw;
            }

            XmlNode pdfNode = xmlDoc.SelectSingleNode("/VALUATION_RESPONSE/REPORT/EMBEDDED_FILE/DOCUMENT");
            if (pdfNode == null || string.IsNullOrEmpty(pdfNode.InnerText))
            {
                pdfNode = xmlDoc.SelectSingleNode("/VALUATION_RESPONSE/REPORT/FORM/IMAGE/EMBEDDED_FILE[@_Type='PDF']/DOCUMENT");
            }

            if (fileType == null)
            {
                if (AppraisalGenericEDocument.IsAppraisalXmlValid(System.Xml.Linq.XDocument.Parse(xmlDoc.OuterXml)))
                {
                    fileType = E_FileType.AppraisalXml;
                }
                else if (UcdGenericEDocument.IsUcdStringValid(xmlDoc.OuterXml, BrokerId))
                {
                    fileType = E_FileType.UniformClosingDataset;
                }
                else
                {
                    fileType = E_FileType.UnspecifiedXml;
                }
            }

            if (pdfNode == null || string.IsNullOrEmpty(pdfNode.InnerText))
            {
                var docId = Guid.NewGuid();
                if (fileType == E_FileType.AppraisalXml)
                {
                    var writerSettings = new XmlWriterSettings();
                    writerSettings.OmitXmlDeclaration = true;
                    writerSettings.Indent = true;

                    string html;
                    using (var sw = new StringWriter())
                    using (var writer = XmlWriter.Create(sw, writerSettings))
                    {
                        xmlDoc.WriteTo(writer);
                        writer.Flush();
                        html = @"<html><body><pre>" + AspxTools.HtmlString(sw.ToString()) + @"</pre></body></html>";
                    }

                    var pdfPath = HtmlToPdfFile(html, pageId);
                    docId = SaveDoc(pdfPath, saveAs, LoanId, AppId, BrokerId, UserId, description);
                }

                var edocId = SaveXmlDoc(xmlPath, saveAs, LoanId, AppId, BrokerId, UserId, description, docId, fileType.Value);
                return edocId == Guid.Empty ? (Guid?)null : edocId;
            }
            else
            {
                string pdfContent = pdfNode.InnerText;

                var pdfPath = TempFileUtils.NewTempFilePath() + "_autosave_ " + pageId.ToString() + ".pdf";
                File.WriteAllBytes(pdfPath, Convert.FromBase64String(pdfContent));

                var docId = SaveDoc(pdfPath, saveAs, LoanId, AppId, BrokerId, UserId, description);
                var edocId = SaveXmlDoc(xmlPath, saveAs, LoanId, AppId, BrokerId, UserId, description, docId, fileType.Value);
                return edocId == Guid.Empty ? (Guid?)null : edocId;
            }
        }

        public static Guid? SaveXmlDocAsSystem(E_AutoSavePage pageId, string FileDBKey, E_FileDB fdb, Guid BrokerId, Guid LoanId, Guid AppId, string description, E_FileType? fileType = null)
        {
            Guid? edocId = null;
            FileDBTools.UseFile(fdb, FileDBKey, (info) =>
            {
                edocId = SaveXmlDoc(pageId, () => info.FullName, BrokerId, LoanId, AppId, null, description, fileType);
            });

            return edocId;
        }

        #endregion Saving Xml Docs

        #region Saving Remote Docs

        /// <summary>
        /// Autosaves a pdf document from a url.
        /// Does nothing if no autosave doc type is set up for the given page id.
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="url"></param>
        /// <param name="BrokerId"></param>
        /// <param name="LoanId"></param>
        /// <param name="AppId"></param>
        /// <param name="UserId"></param>
        /// <param name="description"></param>
        /// <exception cref="WebException">May be thrown if there is an issue downloading the file.</exception>
        /// <exception cref="NotSupportedException"></exception>
        /// <returns>The Edoc id if saved. Null if not.</returns>
        public static Guid? SavePdfDocFromUrl(E_AutoSavePage pageId, string url, Guid BrokerId, Guid LoanId, Guid AppId, Guid? UserId, string description)
        {
            var saveAs = GetDocTypeForPageId(pageId, BrokerId, E_EnforceFolderPermissions.True);
            if (saveAs == null || saveAs.DocType == null) return null;

            using (var webClient = new WebClient())
            {
                var tempPath = GetTempFilePath(pageId, "pdf");
                try
                {
                    webClient.DownloadFile(url, tempPath);
                }
                catch (WebException e)
                {
                    Tools.LogError("[AutoSaveDocTypeFactory] Error downloading remote file.", e);
                    throw;
                }
                catch (NotSupportedException e)
                {
                    Tools.LogError("[AutoSaveDocTypeFactory] Error downloading remote file due to threading problem.", e);
                    throw;
                }

                var edocId = SaveDoc(tempPath, saveAs, LoanId, AppId, BrokerId, UserId, description);
                return edocId == Guid.Empty ? (Guid?)null : edocId;
            }
        }

        public static Guid? SavePdfDocFromUrlAsSystem(E_AutoSavePage pageId, string url, Guid BrokerId, Guid LoanId, Guid AppId, string description)
        {
            return SavePdfDocFromUrl(pageId, url, BrokerId, LoanId, AppId, null, description);
        }

        /// <summary>
        /// Autosaves a document from an xml file from a url.
        /// Does nothing if no autosave doc type is set up for the given page id.
        /// </summary>
        /// <remarks>
        /// This will download the XML file and check for embedded pdf content.
        /// If it is there, it will autosave the pdf and the accompanying xml file.
        /// If it is not, it will generate a pdf of the raw xml and save that with
        /// the raw xml file.
        /// </remarks>
        /// <param name="pageId"></param>
        /// <param name="url"></param>
        /// <param name="BrokerId"></param>
        /// <param name="LoanId"></param>
        /// <param name="AppId"></param>
        /// <param name="UserId"></param>
        /// <param name="description"></param>
        /// <exception cref="WebException">May be thrown if there is an issue downloading the file.</exception>
        /// <exception cref="NotSupportedException"></exception>
        public static void SaveXmlDocFromUrl(E_AutoSavePage pageId, string url, Guid BrokerId, Guid LoanId, Guid AppId, Guid? UserId, string description)
        {
            SaveXmlDoc(pageId, () =>
            {
                using (var webClient = new WebClient())
                {
                    var xmlPath = GetTempFilePath(pageId, "xml");
                    try
                    {
                        webClient.DownloadFile(url, xmlPath);
                    }
                    catch (WebException e)
                    {
                        Tools.LogError("[AutoSaveDocTypeFactory] Error downloading remote file.", e);
                        throw;
                    }
                    catch (NotSupportedException e)
                    {
                        Tools.LogError("[AutoSaveDocTypeFactory] Error downloading remote file due to threading problem.", e);
                        throw;
                    }
                    return xmlPath;
                }
            }, BrokerId, LoanId, AppId, UserId, description);
        }

        public static void SaveXmlDocFromUrlAsSystem(E_AutoSavePage pageId, string url, Guid BrokerId, Guid LoanId, Guid AppId, string description)
        {
            SaveXmlDocFromUrl(pageId, url, BrokerId, LoanId, AppId, null, description);
        }

        #endregion Saving Remote Docs
    }
}
