﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using LendersOffice.Constants;
using LendersOffice.ObjLib.DatabaseMessageQueue;

namespace EDocs
{
    /// <summary>
    /// // 4/30/2015 dd - Convert this clas to continuous process.
    /// Only one server run this proecssor. The old code does not implement to be run on multiple servers.
    /// </summary>
    public class EDocNotificationQueueProcessor : CommonProjectLib.Runnable.IRunnable
    {
        /// <summary>
        /// Gets the description of the continuous process.
        /// </summary>
        /// <value>The description of the continuous process.</value>
        public string Description
        {
            get { return "Process Edocs Notification."; }
        }

        public void Run()
        {
            EDocNotificationHandler handler = new EDocNotificationHandler();
            handler.Process();
        }

        public static void SubmitNotification(EDocument doc)
        {
            DBMessageQueue mQ = new DBMessageQueue(ConstMsg.EDocsQueue);
            mQ.Send(doc.LoanId.ToString(), " ", doc.DocumentId.ToString());
        }
    }
}
