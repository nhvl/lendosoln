﻿// <copyright file="EdocsPageMetadata.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is EdocsPageMetadata class.
// Author: David D
// Date: 6/18/2015
// </summary>
namespace EDocs
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// A metadata description describe the page.
    /// </summary>
    [DataContract]
    public class EdocsPageMetadata
    {
        /// <summary>
        /// Gets or sets the key of the original PDF.
        /// </summary>
        /// <value>Key of the original PDF.</value>
        [DataMember(Name = "ref_doc_id", Order = 1)]
        public Guid ReferenceDocumentId { get; set; }

        /// <summary>
        /// Gets or sets the 0-indexed page number of the original PDF.
        /// </summary>
        /// <value>The 0-indexed page number of the original PDF.</value>
        [DataMember(Name = "ref_doc_pg", Order = 2)]
        public int ReferenceDocumentPageNumber { get; set; }

        /// <summary>
        /// Gets or sets the rotation clockwise degree. Only 0, 90, 180, 270 are valid values.
        /// </summary>
        /// <value>Rotation clockwise degree.</value>
        [DataMember(Name = "rotation", Order = 3)]
        public int Rotation { get; set; }

        /// <summary>
        /// Gets or sets the width in pixel of PDF page.
        /// </summary>
        /// <value>Width in pixel of PDF page.</value>
        [DataMember(Name = "pdf_width", Order = 4)]
        public int PdfWidth { get; set; }

        /// <summary>
        /// Gets or sets the height in pixel of PDF page.
        /// </summary>
        /// <value>Height in pixel of PDF page.</value>
        [DataMember(Name = "pdf_height", Order = 5)]
        public int PdfHeight { get; set; }

        /// <summary>
        /// Gets or sets the width in pixel of full size PNG image.
        /// </summary>
        /// <value>Width in pixel of full size PNG image.</value>
        [DataMember(Name = "png_full_width", Order = 6, EmitDefaultValue = false, IsRequired = false)]
        public int PngFullWidth { get; set; }

        /// <summary>
        /// Gets or sets the height in pixel of full size PNG image.
        /// </summary>
        /// <value>Height in pixel of full size PNG image.</value>
        [DataMember(Name = "png_full_height", Order = 7, EmitDefaultValue = false, IsRequired = false)]
        public int PngFullHeight { get; set; }

        /// <summary>
        /// Gets or sets the width in pixel of thumb size PNG image.
        /// </summary>
        /// <value>Width in pixel of thumb size PNG image.</value>
        [DataMember(Name = "png_thumb_width", Order = 8, EmitDefaultValue = false, IsRequired = false)]
        public int PngThumbWidth { get; set; }

        /// <summary>
        /// Gets or sets the height in pixel of thumb size PNG image.
        /// </summary>
        /// <value>Height in pixel of thumb size PNG image.</value>
        [DataMember(Name = "png_thumb_height", Order = 9, EmitDefaultValue = false, IsRequired = false)]
        public int PngThumbHeight { get; set; }
    }
}