﻿using System;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;
using System.Collections;
using LendersOffice.Security;
using System.Collections.Generic;
using System.Threading;
using System.Linq;

namespace EDocs
{
    public class EDocumentShippingTemplate
    {
        private bool m_InLoAdmin;
        private List<ShippingTemplate> m_BrokerShippingTemplateList;
        private List<ShippingTemplate> m_StandardShippingTemplateList;
        private List<ShippingTemplate> m_CombinedShippingTemplateList;
        private List<ShippingTemplate> m_CombinedActiveShippingTemplateList;

        public List<ShippingTemplate> BrokerShippingTemplateList
        {
            get
            {
                if (m_BrokerShippingTemplateList == null)
                    GetShippingTemplatesByBroker();
                
                return m_BrokerShippingTemplateList;
            }
        }

        public List<ShippingTemplate> StandardShippingTemplateList
        {
            get
            {
                if (m_StandardShippingTemplateList == null)
                    GetShippingTemplatesByBroker();
                
                return m_StandardShippingTemplateList;
            }
        }

        public List<ShippingTemplate> CombinedShippingTemplateList
        {
            get
            {
                if (m_CombinedShippingTemplateList == null)
                    GetShippingTemplatesByBroker();

                return m_CombinedShippingTemplateList;
            }
        }

        public List<ShippingTemplate> CombinedActiveShippingTemplateList
        {
            get
            {
                if (m_CombinedActiveShippingTemplateList == null)
                    GetShippingTemplatesByBroker();

                return m_CombinedActiveShippingTemplateList;
            }
        }

        private Guid? BrokerId = null;
        private Guid m_BrokerId
        {
            get
            {
                if (this.BrokerId.HasValue)
                {
                    return this.BrokerId.Value;
                }

                AbstractUserPrincipal broker = Thread.CurrentPrincipal as AbstractUserPrincipal;

                if (broker == null)
                {
                    return Guid.Empty;
                }

                return broker.BrokerId;
            }
        }

        private void GetShippingTemplatesByBroker()
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", m_BrokerId)
                                        };

            using (DbDataReader dr = StoredProcedureHelper.ExecuteReader(m_BrokerId, "EDOCS_GetShippingTemplatesByBroker", parameters))
            {
                m_BrokerShippingTemplateList = new List<ShippingTemplate>();
                m_StandardShippingTemplateList = new List<ShippingTemplate>();
                m_CombinedShippingTemplateList = new List<ShippingTemplate>();
                m_CombinedActiveShippingTemplateList = new List<ShippingTemplate>();
                while (dr.Read())
                {
                    ShippingTemplate template = new ShippingTemplate() { 
                                                                        ShippingTemplateName = dr["ShippingTemplateName"].ToString(), 
                                                                        ShippingTemplateId = dr["ShippingTemplateId"].ToString(),
                                                                        EnabledForBroker = dr["BrokerId"] != DBNull.Value
                                                                       };
                    m_CombinedShippingTemplateList.Add(template);

                    if (template.EnabledForBroker)
                        m_CombinedActiveShippingTemplateList.Add(template);

                    if (dr["BrokerId"] != DBNull.Value)
                        m_BrokerShippingTemplateList.Add(template);
                    else
                        m_StandardShippingTemplateList.Add(template);
                }
            }

            m_CombinedShippingTemplateList.Sort((x, y) => x.ShippingTemplateName.CompareTo(y.ShippingTemplateName));
            m_CombinedActiveShippingTemplateList.Sort((x, y) => x.ShippingTemplateName.CompareTo(y.ShippingTemplateName));
            m_BrokerShippingTemplateList.Sort((x, y) => x.ShippingTemplateName.CompareTo(y.ShippingTemplateName));
            m_StandardShippingTemplateList.Sort((x, y) => x.ShippingTemplateName.CompareTo(y.ShippingTemplateName));
        }

        
        private Dictionary<int, EDocumentFolder> folderCache;
        private EDocumentFolder GetFolderById(int folderId)
        {
            if (folderCache == null)
            {
                folderCache = new Dictionary<int, EDocumentFolder>();
            }

            EDocumentFolder folder; 

            if (!folderCache.TryGetValue(folderId, out folder))
            {
                folder = new EDocumentFolder(m_BrokerId, folderId);
                folderCache.Add(folderId, folder);
            }

            return folder;
        }
        

        public List<DocType> GetShippingTemplateData(int shippingTemplateId)
        {
            List<DocType> docTypes = new List<DocType>();
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@ShippingTemplateId", shippingTemplateId));


            if (!m_InLoAdmin)
            {
                parameters.Add(new SqlParameter("@BrokerId", m_BrokerId));
            }
            

            using (var reader = StoredProcedureHelper.ExecuteReader(m_BrokerId, "EDOCS_GetShippingTemplateData", parameters))
            {
                while (reader.Read())
                {
                    docTypes.Add(new DocType() { DocTypeName = reader["DocTypeName"].ToString(), DocTypeId = reader["DocTypeId"].ToString(), Folder = GetFolderById((int)reader["FolderId"]) });
                }
            }
            return docTypes;
        }

        public void AddShippingTemplateType(string shippingTemplateName, string stackingOrder)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@ShippingTemplateName", shippingTemplateName.TrimWhitespaceAndBOM()));
            parameters.Add(new SqlParameter("@List", stackingOrder));
            
            if (!m_InLoAdmin)
            {
                parameters.Add(new SqlParameter("@BrokerId", m_BrokerId));
                checkNameAndPerform(m_BrokerId, "EDOCS_ShippingTemplateAdd", parameters);
            }
            else
                checkNameAndPerform(Guid.Empty, "EDOCS_ShippingTemplateAddInternal", parameters);
        }

        public void EditShippingTemplateType(string shippingTemplateName, string stackingOrder, int shippingTemplateId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@ShippingTemplateName", shippingTemplateName.TrimWhitespaceAndBOM()));
            parameters.Add(new SqlParameter("@List", stackingOrder));
            parameters.Add(new SqlParameter("@ShippingTemplateId", shippingTemplateId));

            if (!m_InLoAdmin)
            {
                parameters.Add(new SqlParameter("@BrokerId", m_BrokerId));

                using (DbDataReader dr = StoredProcedureHelper.ExecuteReader(m_BrokerId, "EDOCS_ShippingTemplateEditCheckAccess", parameters))
                {
                    if (dr.HasRows)
                        throw new AccessDenied();
                }

                // Need to recreate arraylist to get around an exception which is occuring because 
                // the process thinks the original SqlParameters are still being used
                parameters.Clear();
                parameters.Add(new SqlParameter("@ShippingTemplateName", shippingTemplateName.TrimWhitespaceAndBOM()));
                parameters.Add(new SqlParameter("@List", stackingOrder));
                parameters.Add(new SqlParameter("@ShippingTemplateId", shippingTemplateId));
                parameters.Add(new SqlParameter("@BrokerId", m_BrokerId));
                checkNameAndPerform(m_BrokerId, "EDOCS_ShippingTemplateEdit", parameters);
            }
            else
                checkNameAndPerform(Guid.Empty, "EDOCS_ShippingTemplateEditInternal", parameters);
        }

        public void DeleteShippingTemplate(int shippingTemplateId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@ShippingTemplateId", shippingTemplateId));

            Guid brokerId = Guid.Empty;
            if (!m_InLoAdmin)
            {
                parameters.Add(new SqlParameter("@BrokerId", m_BrokerId));
                brokerId = m_BrokerId;
            }

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "EDOCS_DeleteShippingTemplate", 3, parameters);
            clearLists();
        }

        public void CopyShippingTemplate(int shippingTemplateId, string shippingTemplateName)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@ShippingTemplateId", shippingTemplateId));
            parameters.Add(new SqlParameter("@ShippingTemplateName", shippingTemplateName.TrimWhitespaceAndBOM()));
            parameters.Add(new SqlParameter("@BrokerId", m_BrokerId));

            checkNameAndPerform(m_BrokerId, "EDOCS_CopyShippingTemplate", parameters);
        }

        // db - this is used for the LoAdmin migration
        public static void CopyShippingTemplate(int shippingTemplateId, string shippingTemplateName, Guid brokerId, CStoredProcedureExec exce)
        {
            SqlParameter[] parameters = {
                new SqlParameter("@ShippingTemplateId", shippingTemplateId),
                new SqlParameter("@ShippingTemplateName", shippingTemplateName.TrimWhitespaceAndBOM()),
                new SqlParameter("@BrokerId", brokerId)
            };
            string procedureName = "EDOCS_CopyShippingTemplate";

            using (DbDataReader dr = StoredProcedureHelper.ExecuteReader(brokerId, procedureName + "CheckName", parameters))
            {
                if (dr.HasRows)
                {
                    throw new DuplicateNameSelectedException(ErrorMessages.EDocs.ShippingTemplateNameInUse);
                }
            }

            parameters = new SqlParameter[] {
                new SqlParameter("@ShippingTemplateId", shippingTemplateId),
                new SqlParameter("@ShippingTemplateName", shippingTemplateName.TrimWhitespaceAndBOM()),
                new SqlParameter("@BrokerId", brokerId)
            };
            exce.ExecuteNonQuery(procedureName, 3, parameters);
        }

        public string GetShippingTemplateName(int shippingTemplateId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@ShippingTemplateId", shippingTemplateId));
            Guid brokerId = Guid.Empty;

            if (!m_InLoAdmin)
            {
                parameters.Add(new SqlParameter("@BrokerId", m_BrokerId));
                brokerId = m_BrokerId;
            }

            string shippingTemplateName = "";
            using (DbDataReader dr = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_GetShippingTemplateName", parameters))
            {
                if (dr.Read())
                    shippingTemplateName = dr["ShippingTemplateName"].ToString();
            }

            return shippingTemplateName;
        }

        public int GetActiveShippingTemplateID(string sShippingTemplateName)
        {
            bool bTemplateFound = false;
            int iShippingTemplateID = 0;

            foreach (ShippingTemplate template in CombinedActiveShippingTemplateList)
            {
                if (String.Compare(template.ShippingTemplateName, sShippingTemplateName, true) == 0)
                {
                    bTemplateFound = true;
                    iShippingTemplateID = Int32.Parse(template.ShippingTemplateId);
                    break;
                }
            }

            if (!bTemplateFound)
            {
                throw new CBaseException(ErrorMessages.EDocs.ShippingTemplateNotFound, ErrorMessages.EDocs.ShippingTemplateNotFound);
            }

            return iShippingTemplateID;
        }

        // Call this method every time the list of shipping templates changes
        private void clearLists()
        {
            m_CombinedShippingTemplateList = null;
            m_CombinedActiveShippingTemplateList = null;
            m_BrokerShippingTemplateList = null;
            m_StandardShippingTemplateList = null;
        }

        private void checkNameAndPerform(Guid brokerId, string procedureName, IEnumerable<SqlParameter> parameters)
        {
            using (DbDataReader dr = StoredProcedureHelper.ExecuteReader(brokerId, procedureName + "CheckName", parameters.ToList()))
            {
                if (dr.HasRows)
                    throw new DuplicateNameSelectedException(ErrorMessages.EDocs.ShippingTemplateNameInUse);
            }

            StoredProcedureHelper.ExecuteNonQuery(brokerId, procedureName, 3, parameters.ToList());
            clearLists();
        }
        
        public EDocumentShippingTemplate()
        {
            AbstractUserPrincipal broker = Thread.CurrentPrincipal as AbstractUserPrincipal;

            if (broker.Type == "I")
                m_InLoAdmin = true;
            else
                m_InLoAdmin = false;
        }

        public EDocumentShippingTemplate(Guid brokerId)
        {
            this.BrokerId = brokerId;
        }

        public static IEnumerable<ShippingTemplate> GetShippingTemplatesWithDocType(Guid brokerId, int docTypeId)
        {
            List<ShippingTemplate> shippingTemplates = new List<ShippingTemplate>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@DocTypeId", docTypeId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_GetShippingTemplateNameWithDocType", parameters))
            {
                while (reader.Read())
                {
                    shippingTemplates.Add(new ShippingTemplate()
                    {
                        ShippingTemplateId = reader["ShippingTemplateId"].ToString(),
                        ShippingTemplateName = reader["ShippingTemplateName"].ToString()

                    });
                }
            }

            return shippingTemplates.AsReadOnly();
        }
    }

    public class ShippingTemplate
    {
        public string ShippingTemplateId { get; set; }
        public string ShippingTemplateName { get; set; }
        public bool EnabledForBroker { get; set; }
    }
}