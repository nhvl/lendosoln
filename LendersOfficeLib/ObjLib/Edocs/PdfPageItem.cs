﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Constants;
using LendersOffice.ObjLib.DocumentConditionAssociation;

namespace EDocs
{
    public class PdfPageItem
    {
        public PdfPageItem()
        {
            Scale = ConstAppDavid.PdfPngScaling;
            CanEditSource = true;
        }
        public Guid DocId { get; set; }
        public int Version { get; set; }
        /// <summary>
        /// Page number is index based 1. The reason I used index-1 is because iTextSharp use index-1 for page number.
        /// </summary>
        public int Page { get; set; }
        public int Rotation { get; set; }

        public float Scale { get; set; } // 1 = 100%, 1.5 = 150% etc.
        public int PageWidth { get; set; }
        public int PageHeight { get; set; }
        public string PngKey { get; set; }
        public string Name { get; set; }
        public string Folder { get; set; }
        public string DocType { get; set; }
        public bool CanEditSource { get; set; } //careful with this one so far its only used by sign off editor
        public E_DocumentConditionAssociationStatus DocAssocStatus { get; set; } //same as above

        /// <summary>
        /// Gets or sets the temporary Amazon token.
        /// </summary>
        /// <value>Temporary Amazon token.</value>
        public string Token { get; set; }
    }
}
