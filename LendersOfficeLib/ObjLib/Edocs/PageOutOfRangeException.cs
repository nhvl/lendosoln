﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
namespace EDocs
{
    public class PageOutOfRangeException : CBaseException
    {
        public PageOutOfRangeException() : base("Invalid Page Number", "Invalid Page Number") { }
    }
}
