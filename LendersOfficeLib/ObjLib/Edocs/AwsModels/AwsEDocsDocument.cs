﻿// <copyright file="AwsEDocsDocument.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: David D
// Date: 5/3/2016
// </summary>
namespace EDocs.AwsModels
{
    using System.Collections.Generic;
    using Newtonsoft.Json;

    /// <summary>
    /// Model for EDocs document.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class AwsEDocsDocument
    {
        /// <summary>
        /// Gets or sets doc type name.
        /// </summary>
        /// <value>Doc type name.</value>
        [JsonProperty("doc_type_name")]
        public string DocTypeName { get; set; }

        /// <summary>
        /// Gets or sets the public description.
        /// </summary>
        /// <value>Public description.</value>
        [JsonProperty("description")]
        public string PublicDescription { get; set; }

        /// <summary>
        /// Gets or sets the list of pages.
        /// </summary>
        /// <value>The list of pages.</value>
        [JsonProperty("pages")]
        public List<AwsEDocsPage> Pages { get; set; }

        /// <summary>
        /// Gets or sets the list of annotations.
        /// </summary>
        /// <value>The list of annotations.</value>
        [JsonProperty("annotations")]
        public List<AwsEDocsAnnotation> Annotations { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the document is e-signed.
        /// </summary>
        /// <value>A value indicating whether the document is e-signed.</value>
        [JsonProperty("isesigned")]
        public bool IsESigned { get; set; }
    }
}
