﻿// <copyright file="AwsEDocsPage.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: David D
// Date: 5/3/2016
// </summary>
namespace EDocs.AwsModels
{
    using System;
    using Newtonsoft.Json;

    /// <summary>
    /// Model for EDocs page.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class AwsEDocsPage
    {
        /// <summary>
        /// Gets or sets the id of original document.
        /// </summary>
        /// <value>The id of original document.</value>
        [JsonProperty("ref_doc_id")]
        public Guid ReferenceDocumentId { get; set; }

        /// <summary>
        /// Gets or sets the page number of original document.
        /// </summary>
        /// <value>The page number of original document.</value>
        [JsonProperty("ref_doc_pg")]
        public int ReferenceDocumentPageNumber { get; set; }

        /// <summary>
        /// Gets or sets the page rotation in degree.
        /// </summary>
        /// <value>The page rotation in degree.</value>
        [JsonProperty("rotation")]
        public int Rotation { get; set; }
    }
}
