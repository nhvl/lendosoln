﻿// <copyright file="AwsEDocsAnnotation.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: David D
// Date: 5/3/2016
// </summary>
namespace EDocs.AwsModels
{
    using System;
    using Newtonsoft.Json;

    /// <summary>
    /// Model for annotation.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class AwsEDocsAnnotation
    {
        /// <summary>
        /// Gets or sets the id of annotation.
        /// </summary>
        /// <value>The id of annotation.</value>
        [JsonProperty("id")]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>The created date.</value>
        [JsonProperty("created_date")]
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the author of annotation.
        /// </summary>
        /// <value>The author of annotation.</value>
        [JsonProperty("created_by")]
        public string CreatedBy { get; set; }

        /// <summary>
        /// Gets or sets the last modified date.
        /// </summary>
        /// <value>The last modified date.</value>
        [JsonProperty("last_modified_date")]
        public DateTime LastModifiedDate { get; set; }

        /// <summary>
        /// Gets or sets last modified by.
        /// </summary>
        /// <value>The last modified by.</value>
        [JsonProperty("last_modified_by")]
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// Gets or sets the annotation type.
        /// </summary>
        /// <value>The annotation type.</value>
        [JsonProperty("type")]
        public E_EDocumentAnnotationItemType AnnotationType { get; set; }

        /// <summary>
        /// Gets or sets the state of annotation note.
        /// </summary>
        /// <value>The state of annotation note.</value>
        [JsonProperty("note_state")]
        public E_DocumentAnnotationNoteStateType NoteState { get; set; }

        /// <summary>
        /// Gets or sets the text of annotation note.
        /// </summary>
        /// <value>The text of annotation note.</value>
        [JsonProperty("text")]
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the left position.
        /// </summary>
        /// <value>The left position.</value>
        [JsonProperty("left")]
        public float Left { get; set; }

        /// <summary>
        /// Gets or sets the bottom position.
        /// </summary>
        /// <value>The bottom position.</value>
        [JsonProperty("bottom")]
        public float Bottom { get; set; }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>The width.</value>
        [JsonProperty("width")]
        public float Width { get; set; }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        /// <value>The height.</value>
        [JsonProperty("height")]
        public float Height { get; set; }

        /// <summary>
        /// Gets or sets the page contains annotation.
        /// </summary>
        /// <value>The page number.</value>
        [JsonProperty("page")]
        public int Page { get; set; }
    }
}
