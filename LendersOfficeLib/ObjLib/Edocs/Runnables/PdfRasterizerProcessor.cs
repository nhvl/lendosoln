﻿// <copyright file="PdfRasterizerProcessor.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is PdfRasterizerProcessor class.
//  Date: 6/19/2015
//  Author: David Dao
// </summary>
namespace LendersOffice.ObjLib.Edocs.Runnables
{
    using System;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Messaging;
    using System.Net;
    using DataAccess;
    using EDocs;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using PdfRasterizerLib;

    /// <summary>Convert PDF to PNG.</summary>
    public class PdfRasterizerProcessor
    {
        /// <summary>
        /// Run PDF rasterizer as a schedule executable.
        /// </summary>
        /// <param name="args">Arguments object.</param>
        public static void Execute(string[] args)
        {
            Trace.Listeners.Add(new SimpleTraceListener()); // 6/2/2015 dd - Add listener here so we can route log.

            PdfRasterizerProcessor processor = new PdfRasterizerProcessor();
            processor.RunImpl();
        }

        /// <summary>
        /// Processes a single queue item.
        /// </summary>
        /// <param name="item">The item to process.</param>
        /// <remarks>
        /// This method is specifically for synchronous processing of a single document, which is mostly useful for development.
        /// </remarks>
        public static void ProcessSingleItem(EDocument.NonDestructiveQueueItem item)
        {
            PdfRasterizerProcessor processor = new PdfRasterizerProcessor();
            processor.ProcessItem(item, isEnforceLicense: ConstAppDavid.CurrentServerLocation == ServerLocation.Production);
        }

        /// <summary>
        /// Run PDF Rasterizer.
        /// </summary>
        private void RunImpl()
        {
            bool isProduction = ConstAppDavid.CurrentServerLocation == ServerLocation.Production;

            if (string.IsNullOrEmpty(ConstStage.MSMQ_PdfRasterize))
            {
                throw new ArgumentException("MSMQ_PdfRasterize is not setup.");
            }

            using (var queue = Drivers.Gateways.MessageQueueHelper.PrepareForJSON(ConstStage.MSMQ_PdfRasterize))
            {
                using (var workerTiming = new WorkerExecutionTiming("PdfRasterizerProcessor"))
                {
                    while (true)
                    {
                        try
                        {
                            DateTime arrivalTime;
                            EDocument.NonDestructiveQueueItem item = Drivers.Gateways.MessageQueueHelper.ReceiveJSON<EDocument.NonDestructiveQueueItem>(queue, 1, out arrivalTime);

                            if (item == null)
                            {
                                return; // same behavior as MessageQueueException.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout
                            }

                            using (var itemTiming = workerTiming.RecordItem(item.EdocsDocumentId.ToString(), arrivalTime))
                            {
                                try
                                {
                                    this.ProcessItem(item, isProduction);
                                }
                                catch (Exception exc)
                                {
                                    itemTiming.RecordError(exc);
                                    Tools.LogErrorWithCriticalTracking(exc);
                                    throw;
                                }
                            }
                        }
                        catch (MessageQueueException exc)
                        {
                            if (exc.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                            {
                                return;
                            }

                            Tools.LogErrorWithCriticalTracking(exc);
                            throw;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Process <paramref name="item"/>.
        /// </summary>
        /// <param name="item">The item to process.</param>
        /// <param name="isEnforceLicense">Whether the PDF rasterizer should enforce the license.</param>
        private void ProcessItem(EDocument.NonDestructiveQueueItem item, bool isEnforceLicense)
        {
            try
            {
                try
                {
                    Guid documentId = new Guid(item.FileDbKey);
                    AmazonEDocHelper.UploadOriginalPdfToAmazon(item.BrokerId, documentId);
                }
                catch (WebException exc)
                {
                    // Any WebException due to upload to Amazon, we will ignore and continue.
                    Tools.LogErrorWithCriticalTracking(exc);
                }

                DocumentMetadata documentMetadata = null;

                if (item.IsUploadToAmazon)
                {
                    documentMetadata = this.ExtractMetadata(item.FileDbSiteCode, item.FileDbKey, item.FullPngScale, item.ThumbPngScale);
                }
                else
                {
                    documentMetadata = PdfRasterizerV2.ConvertToPng(isEnforceLicense, item.FileDbSiteCode, item.FileDbKey, item.FullPngScale, item.ThumbPngScale);
                }

                string json = SerializationHelper.JsonNetSerialize(documentMetadata);

                this.SaveOriginalDocument(item.BrokerId, item.SourceT, documentMetadata, json);

                // Update Status of EDOCS
                string postAction = item.PostAction;

                if (postAction == EDocument.POST_ACTION_UPDATE_STATUS_ONLY)
                {
                    EDocument.UpdateImageStatus(item.BrokerId, item.EdocsDocumentId, E_ImageStatus.HasCachedImages);
                }
                else if (postAction == EDocument.POST_ACTION_UPDATE_MIGRATION)
                {
                    EDocument.UpdateImageStatusAfterSuccessfulMigration(item.BrokerId, item.EdocsDocumentId, documentMetadata);
                }
                else if (postAction == EDocument.POST_ACTION_UPDATE_METADATA_EXCLUDE_ORIGINAL)
                {
                    // Update the same set of data as after migration.
                    EDocument.UpdateImageStatusAfterSuccessfulMigration(item.BrokerId, item.EdocsDocumentId, documentMetadata);
                }
                else
                {
                    EDocument.UpdateImageStatusWithMetadataJson(item.BrokerId, item.EdocsDocumentId, E_ImageStatus.HasCachedImages, documentMetadata);
                }
            }
            catch
            {
                EDocument.UpdateImageStatus(item.BrokerId, item.EdocsDocumentId, E_ImageStatus.Error);
                throw;
            }
        }

        /// <summary>
        /// Only extract the metadata from the PDF.
        /// </summary>
        /// <param name="databaseName">FileDB name.</param>
        /// <param name="pdfFileDbKey">Key of the pdf.</param>
        /// <param name="fullScale">Scale of image full size.</param>
        /// <param name="thumbScale">Scale of image thumb nail.</param>
        /// <returns>Metadata of the PDF.</returns>
        private DocumentMetadata ExtractMetadata(string databaseName, string pdfFileDbKey, float fullScale, float thumbScale)
        {
            if (fullScale < thumbScale)
            {
                throw new ArgumentException("FullScale=" + fullScale + " must be greater than thumbScale=" + thumbScale);
            }

            DocumentMetadata documentMetadata = new DocumentMetadata();
            documentMetadata.DocumentId = new Guid(pdfFileDbKey);

            FileDBTools.UseFile(
                databaseName.GetFileDBType(), 
                pdfFileDbKey, 
                fileInfo =>
            {
                documentMetadata.PdfSize = fileInfo.Length;

                using (FileStream stream = File.OpenRead(fileInfo.FullName))
                {
                    TallComponents.PDF.Rasterizer.Document document = new TallComponents.PDF.Rasterizer.Document(stream);

                    int numberOfPages = document.Pages.Count;

                    for (int pg = 0; pg < numberOfPages; pg++)
                    {
                        TallComponents.PDF.Rasterizer.Page pdfPage = document.Pages[pg];

                        PageMetadata pageMetadata = new PageMetadata();
                        pageMetadata.ReferenceDocumentPageNumber = pg;
                        pageMetadata.ReferenceDocumentId = documentMetadata.DocumentId;

                        documentMetadata.AddPage(pageMetadata);

                        pageMetadata.PdfWidth = (int)pdfPage.Width;
                        pageMetadata.PdfHeight = (int)pdfPage.Height;

                        pageMetadata.PngFullWidth = (int)(fullScale * pdfPage.Width);
                        pageMetadata.PngFullHeight = (int)(fullScale * pdfPage.Height);

                        pageMetadata.PngThumbWidth = (int)(thumbScale * pdfPage.Width);
                        pageMetadata.PngThumbHeight = (int)(thumbScale * pdfPage.Height);
                    }
                }
            });

            return documentMetadata;
        }

        /// <summary>
        /// Save the original document.
        /// </summary>
        /// <param name="brokerId">Broker Id to save document.</param>
        /// <param name="sourceT">Source of the non destructive document.</param>
        /// <param name="document">Document to save.</param>
        /// <param name="json">Json content to save.</param>
        private void SaveOriginalDocument(Guid brokerId, EDocument.E_NonDestructiveSourceT sourceT, DocumentMetadata document, string json)
        {
            SqlParameter[] parameters = 
                                        {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@SourceT", (int)sourceT),
                                            new SqlParameter("@DocumentId", document.DocumentId),
                                            new SqlParameter("@MetadataJsonContent", string.Empty),
                                            new SqlParameter("@NumPages", document.Pages.Count()),
                                            new SqlParameter("@PdfSizeInBytes", document.PdfSize),
                                            new SqlParameter("@PngSizeInBytes", document.PngFullSize + document.PngThumbSize)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "EDOCS_DOCUMENT_ORIGINAL_Update", 3, parameters);

            string fileDbKey = EDocument.GetOriginalDocumentMetadataJsonContentFileDBKey(brokerId, document.DocumentId);
            FileDBTools.WriteData(E_FileDB.EDMS, fileDbKey, json);
        }

        /// <summary>
        /// Simple trace listener that put log to ElasticSearch.
        /// </summary>
        private class SimpleTraceListener : TraceListener
        {
            /// <summary>
            /// Write message to log.
            /// </summary>
            /// <param name="message">Message to write.</param>
            public override void Write(string message)
            {
                Tools.LogInfo(message);
            }

            /// <summary>
            /// Write message to log.
            /// </summary>
            /// <param name="message">Message to write.</param>
            public override void WriteLine(string message)
            {
                Tools.LogInfo(message);
            }

            /// <summary>
            /// Write message to log.
            /// </summary>
            /// <param name="o">Message to write.</param>
            public override void Write(object o)
            {
                if (o != null)
                {
                    if (o is Exception)
                    {
                        Tools.LogError((Exception)o);
                    }
                    else
                    {
                        Tools.LogInfo(o.ToString());
                    }
                }
            }

            /// <summary>
            /// Write message to log.
            /// </summary>
            /// <param name="o">Message to write.</param>
            /// <param name="category">Category to write.</param>
            public override void Write(object o, string category)
            {
                if (o != null)
                {
                    if (o is Exception)
                    {
                        Tools.LogError((Exception)o);
                    }
                    else
                    {
                        Tools.LogInfo(category, o.ToString());
                    }
                }
            }
        }
    }
}