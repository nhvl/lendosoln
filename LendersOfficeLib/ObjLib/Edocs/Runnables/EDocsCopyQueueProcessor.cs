﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using CommonProjectLib.Logging;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Email;
using LendersOffice.ObjLib.DatabaseMessageQueue;
namespace EDocs.Runnable
{
    /// <summary>
    /// Class for LendersOfficeContinuousService used to copy EDocs between loans.
    /// Each message represents an entire copy transaction from the user.
    /// </summary>
    public class EDocsCopyQueueProcessor : CommonProjectLib.Runnable.IRunnable
    {
        static EDocsCopyQueueProcessor()
        {
            string msmqLogConnectStr = ConstStage.MsMqLogConnectStr;
            if (string.IsNullOrEmpty(msmqLogConnectStr) == false)
            {
                LogManager.Add(new LendersOffice.Common.MSMQLogger(msmqLogConnectStr, ConstStage.EnvironmentName));
            }

            string server = ConstStage.SmtpServer;
            string notifyOnErrorEmail = ConfigurationManager.AppSettings["NotifyOnErrorEmailAddress"];
            string notifyFromErrorEmail = ConfigurationManager.AppSettings["NotifyFromOnError"];
            int port = ConstStage.SmtpServerPortNumber;

            if (false == string.IsNullOrEmpty(server) && false == string.IsNullOrEmpty(notifyOnErrorEmail) && false == string.IsNullOrEmpty(notifyFromErrorEmail))
            {
                LogManager.Add(new EmailLogger(server, port, notifyOnErrorEmail, notifyFromErrorEmail));
            }
        }

        public static void Execute(string[] args) 
        {
            EDocsCopyQueueProcessor processor = new EDocsCopyQueueProcessor();
            processor.Run();
        }
        public string Description
        {
            get
            {
                return "Copies edocs from loan to loan.";
            }
        }

        private Dictionary<string, string> ParseData(string data, char[] keySplit, char[] keyValueSplit)
        {
            if (string.IsNullOrEmpty(data))
            {
                return null;
            }

            Dictionary<string, string> keysAndValues = new Dictionary<string, string>();
            string[] keyValuePairs = data.Split(keySplit, StringSplitOptions.RemoveEmptyEntries);

            foreach (string keyValuePair in keyValuePairs)
            {
                string[] keyAndValue = keyValuePair.Split(keyValueSplit, StringSplitOptions.RemoveEmptyEntries);
                if (keyAndValue.Length != 2 || String.IsNullOrEmpty(keyAndValue[0]) || String.IsNullOrEmpty(keyAndValue[1]))
                {
                    throw new IncorrectMessageFormatException("Key-value pair of wrong format.");
                }

                keysAndValues.Add(keyAndValue[0], keyAndValue[1]);
            }

            return keysAndValues;
        }

        private bool ReceiveMessage(DBMessageQueue queue, out DBMessage msg)
        {
            try
            {
                msg = queue.Receive();
                if (msg == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (DBMessageQueueException e)
            {

                if (e.MessageQueueErrorCode != DBMessageQueueErrorCode.IOTimeout &&
                    e.MessageQueueErrorCode != DBMessageQueueErrorCode.EmptyQueue)
                {
                    LogManager.Error("[EDocsCopyQueueProcessor]DBMQException " + e.Message);
                    throw;
                }

                // The queue is empty.
                msg = null;
                return false;
            }
        }

        /// <summary>
        /// Notify the requesting user that the copy request has been completed.
        /// </summary>
        /// <param name="emailAddr"></param>
        /// <param name="srcLoanNm"></param>
        /// <param name="destLoanIds"></param>
        /// <param name="success"></param>
        private void EmailResult(Guid brokerId, string emailAddr, string srcLoanNm, string[] destLoanIds, Dictionary<string, List<string>> failedCopies, bool criticalError)
        {
            StringBuilder destLoanNms = new StringBuilder();
            foreach (string loanId in destLoanIds)
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(new Guid(loanId), typeof(EDocsCopyQueueProcessor));
                dataLoan.InitLoad();
                destLoanNms.AppendFormat("{0}, ", dataLoan.sLNm);
            }
            destLoanNms.Remove(destLoanNms.Length - 2, 2); // get rid of trailing ", "

            StringBuilder messageBuilder = new StringBuilder();
            if (!criticalError)
            {
                messageBuilder.AppendFormat("Your request to copy documents from {0} to {1} has been completed.", srcLoanNm, destLoanNms);

                if (failedCopies.Count > 0)
                {
                    messageBuilder.Remove(messageBuilder.Length - 1, 1);
                    messageBuilder.AppendFormat(" with errors.{0}{0}There were errors copying the following documents:{0}", Environment.NewLine);

                    foreach (string folderAndDocTypeName in failedCopies.Keys)
                    {
                        messageBuilder.AppendFormat("{0} to ", folderAndDocTypeName);
                        foreach (string sLNm in failedCopies[folderAndDocTypeName])
                        {
                            messageBuilder.AppendFormat("{0}, ", sLNm);
                        }
                        messageBuilder.Remove(messageBuilder.Length - 2, 2);
                        messageBuilder.Append(Environment.NewLine);
                    }
                }
            }
            else
            {
                messageBuilder.AppendFormat("There was an error processing your request to copy documents from {0} to {1}."
                                        + " Please contact us if this happens again.", srcLoanNm, destLoanNms);
            }
            
            CBaseEmail email = new CBaseEmail(brokerId);
            email.Subject = "Request to Copy Documents Completed";
            email.From = ConstStage.DefaultDoNotReplyAddress;
            email.To = emailAddr;
            email.Message = messageBuilder.ToString();
            email.Send();
        }

        private char[] m_keyValueSplit = new char[] { '=' };
        private char[] m_csvSplit = new char[] { ',' };
        private char[] m_loanIdDocIdSplit = new char[] { ';' };

        /// <summary>
        /// Copy the given documents to the given loans and send email on completion.
        /// Message format
        ///     Subject: UserId=[userId],BrokerId=[brokerId],EmployeeId=[employeeId],SrcLoanId=[srcLoanId]
        ///     Data: DestLoanIds=[destLoanId1],[destLoanId2],...;DocIds=[docId1],[docId2],...
        /// </summary>
        /// <param name="subject1">Message subject. Should be of format "UserId=[userId],BrokerId=[brokerId],EmployeeId=[employeeId],SrcLoanId=[srcLoanId]"</param>
        /// <param name="data">Message data. Should be of format "DestLoanIds=[destLoanId1],[destLoanId2],...;DocIds=[docId1],[docId2],..."</param>
        private void HandleMessage(string subject1, string data)
        {
            #region message parsing
            Dictionary<string, string> parsedSubject = ParseData(subject1, m_csvSplit, m_keyValueSplit);
            if (parsedSubject == null || parsedSubject.Count != 4)
            {
                throw new IncorrectMessageFormatException("Message subject did not have the expected format.");
            }
            Guid userId = new Guid(parsedSubject["UserId"]);
            Guid brokerId = new Guid(parsedSubject["BrokerId"]);
            Guid employeeId = new Guid(parsedSubject["EmployeeId"]);
            Guid srcLoanId = new Guid(parsedSubject["SrcLoanId"]);

            Dictionary<string, string> parsedData = ParseData(data, m_loanIdDocIdSplit, m_keyValueSplit);
            if (parsedData == null || parsedData.Count != 2)
            {
                throw new IncorrectMessageFormatException("Message data did not have the expected format.");
            }
            string[] destLoanIds = parsedData["DestLoanIds"].Split(m_csvSplit, StringSplitOptions.RemoveEmptyEntries);
            string[] docIds = parsedData["DocIds"].Split(m_csvSplit, StringSplitOptions.RemoveEmptyEntries);
            #endregion

            CPageData srcDataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(srcLoanId, typeof(EDocsCopyQueueProcessor));
            srcDataLoan.InitLoad();

            var repo = EDocumentRepository.GetUserRepositoryForSystem(userId, brokerId);
            var requestingUser = EmployeeDB.RetrieveById(brokerId, employeeId);
            var emailAddr = requestingUser.Email;

            // Dictionary of failed copies. Map FolderAndDocTypeName -> loan names that were not copied to
            var failedCopies = new Dictionary<string, List<string>>();

            foreach (string lId in destLoanIds)
            {
                Guid destLoanId = new Guid(lId);
                CPageData destDataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(destLoanId, typeof(EDocsCopyQueueProcessor));
                destDataLoan.InitLoad();

                foreach (string dId in docIds)
                {
                    Guid docId = new Guid(dId);

                    try
                    {
                        EDocument existingDoc = repo.GetDocumentById(docId);
                        bool successfulCopy = CopyDocument(repo, existingDoc, destDataLoan, srcDataLoan.sLNm);
                        if (!successfulCopy)
                        {
                            if (!failedCopies.ContainsKey(existingDoc.FolderAndDocTypeName))
                            {
                                failedCopies.Add(existingDoc.FolderAndDocTypeName, new List<string>());
                            }
                            failedCopies[existingDoc.FolderAndDocTypeName].Add(destDataLoan.sLNm);
                        }
                    }
                    catch (NotFoundException exc)
                    {
                        var msg = String.Format("[EDocsCopyQueueProcessor] - Failed to load existing doc. DocId: {0}, Destination Loan Id: {1}", docId, destLoanId);
                        LogManager.Error(msg, exc);
                    }
                    catch (Exception exc)
                    {
                        var msg = String.Format("[EDocsCopyQueueProcessor] - Unexpected error when copying doc. DocId: {0}, Destination Loan Id: {1}", docId, destLoanId);
                        LogManager.Error(msg, exc);
                        EmailResult(brokerId, emailAddr, srcDataLoan.sLNm, destLoanIds, failedCopies, true);
                        throw;
                    }
                }
            }

            EmailResult(brokerId, emailAddr, srcDataLoan.sLNm, destLoanIds, failedCopies, false);
        }

        private bool CopyDocument(EDocumentRepository repo, EDocument existingDoc, CPageData destDataLoan, string sourceLoanName)
        {
            var newDoc = repo.CreateDocument(E_EDocumentSource.UserCopy);
            newDoc.CopiedFromFileName = sourceLoanName;

            newDoc.LoanId = destDataLoan.sLId;
            newDoc.AppId = destDataLoan.GetAppData(0).aAppId;
            newDoc.DocumentTypeId = existingDoc.DocumentTypeId;
            newDoc.PublicDescription = existingDoc.PublicDescription;
            newDoc.InternalDescription = existingDoc.InternalDescription;

            newDoc.CopyPdf(existingDoc, suppressSignatureDetection: false);
            repo.Save(newDoc);

            return true;
        }

        #region IRunnable Members
        public void Run()
        {
            DBMessageQueue copyEDocQueue = new DBMessageQueue(ConstMsg.CopyEDocQueue);

            DBMessage msg = null;
            using (var workerTiming = new WorkerExecutionTiming("EDocsCopyQueueProcessor"))
            {
                while (ReceiveMessage(copyEDocQueue, out msg))
                {
                    if (msg == null)
                    {
                        break;
                    }

                    using (var itemTiming = workerTiming.RecordItem(msg.Subject1, msg.InsertionTime))
                    {

                        try
                        {
                            HandleMessage(msg.Subject1, msg.Data);
                        }
                        catch (IncorrectMessageFormatException exc)
                        {
                            itemTiming.RecordError(exc);
                            var errMsg = String.Format("Message subject did not have the expected format. Subject: {0}, Data: {1}, Id: {2}",
                                msg.Subject1, msg.Data, msg.Id);
                            LogManager.Error(errMsg, exc);
                        }
                    }

                }
            }
        }
        #endregion

        public class IncorrectMessageFormatException : CBaseException
        {

            public IncorrectMessageFormatException(string message)
                : base(message, message)
            {
            }
        }
    }
}
