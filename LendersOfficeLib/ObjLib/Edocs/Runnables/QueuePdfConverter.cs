﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using CommonProjectLib.Logging;
using DataAccess;
using EDocs.Utils;
using iTextSharp.text.pdf;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.ObjLib.DatabaseMessageQueue;
using PdfRasterizerLib;

namespace EDocs.Runnable
{
    public class QueuePdfConverter : CommonProjectLib.Runnable.IRunnable
    {

        private class SimpleTraceListener : TraceListener
        {
            public override void Write(string message)
            {
                Tools.LogInfo(message);
            }

            public override void WriteLine(string message)
            {
                Tools.LogInfo(message);
            }

            public override void Write(object o)
            {
                if (o != null)
                {
                    if (o is Exception)
                    {
                        Tools.LogError((Exception)o);
                    }
                    else
                    {
                        Tools.LogInfo(o.ToString());
                    }
                }
            }

            public override void Write(object o, string category)
            {
                if (o != null)
                {
                    if (o is Exception)
                    {
                        Tools.LogError((Exception)o);
                    }
                    else
                    {
                        Tools.LogInfo(category, o.ToString());
                    }
                }
            }

        }

        static QueuePdfConverter()
        {

            string msmqLogConnectStr = ConstStage.MsMqLogConnectStr;
            if (string.IsNullOrEmpty(msmqLogConnectStr) == false)
            {
                LogManager.Add(new LendersOffice.Common.MSMQLogger(msmqLogConnectStr, ConstStage.EnvironmentName));
            }

            string server = ConstStage.SmtpServer;
            int port = ConstStage.SmtpServerPortNumber;
            string notifyOnErrorEmail = ConfigurationManager.AppSettings["NotifyOnErrorEmailAddress"];
            string notifyFromErrorEmail = ConfigurationManager.AppSettings["NotifyFromOnError"];
            if (false == string.IsNullOrEmpty(server) && false == string.IsNullOrEmpty(notifyOnErrorEmail) && false == string.IsNullOrEmpty(notifyFromErrorEmail))
            {
                LogManager.Add(new EmailLogger(server, port, notifyOnErrorEmail, notifyFromErrorEmail));
            }
        }

        private string[] m_split;
        private string[] m_keySplit;
        private IPdfRasterizer convertor;
        private StringBuilder m_performanceLog;
        private int m_indentation; 


        #region IRunnable Members


        public string Description
        {
            get
            {
                return "Creates a png for each page of a pdf comign from the PNGConversionQueue,PNGConversionPMLQueue and PNGConversionFaxQueue. Basically prepares the edoc for the editor.";
            }
        }

        public static void Execute(string[] args)
        {
            // 5/28/2015 dd - This method is call from our ScheduleExecutable
            try
            {
                bool isProduction = ConstAppDavid.CurrentServerLocation == ServerLocation.Production;

                bool ismsmq = ConstStage.EnableEDocsMSMQ;

                if (isProduction && TallComponents.Licensing.License.IsValid() == false)
                {
                    // 5/28/2015 dd - This check is to make sure proper license is setup before running on production.
                    throw new Exception("TallsComponents.PdfRasterizer does not have proper license. Make sure valid key is in 'PDFRasterizer.NET 3.0 Windows Domain Key' of the config file");
                }

                int maxItem = 100;
                if (args.Length > 1)
                {
                    if (int.TryParse(args[1], out maxItem) == false)
                    {
                        maxItem = 100;
                    }
                }

                if (args.Length > 2)
                {
                    if (!bool.TryParse(args[2], out ismsmq))
                    {
                        Console.WriteLine("Could not parse arg2 ismsmq param should be True or False defaulting to stage");
                        ismsmq = ConstStage.EnableEDocsMSMQ;
                    }
                }

                Trace.Listeners.Add(new SimpleTraceListener()); // 6/2/2015 dd - Add listener here so we can route log from QueuePdfConverter.

                EDocs.Runnable.QueuePdfConverter converter = new EDocs.Runnable.QueuePdfConverter();
                int numberOfIdleRun = 0;
                // 10/15/2015 - Because we are running this task under Windows TaskScheduler, we do not want the converter to run for under minutes and then
                // stop. We will try to sleep and wait for at least of few minutes of no work before terminate the process.
                while (numberOfIdleRun < 5)
                {
                    Stopwatch sw = Stopwatch.StartNew();
                    converter.RunImpl(string.Empty /* Do need to go through Remoting service */, maxItem, ismsmq);
                    sw.Stop();
                    if (sw.ElapsedMilliseconds < 60000)
                    {
                        // Consider an idle run if it is under 1 minute.
                        numberOfIdleRun++;
                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(30000);
                    }
                    else
                    {
                        numberOfIdleRun = 0;
                    }
                }

            }
            catch (DBMessageQueueException)
            {
            }
        }
        public void Run()
        {
            RunImpl(ConstSite.PdfRasterizerHost, int.MaxValue, ConstStage.EnableEDocsMSMQ);
        }

        private void RunImpl(string pdfRasterizerHost, int maxProcessingItems, bool ismsmq)
        {
            Tools.SetupServicePointManager();
            m_split = new string[] { ";" };
            m_keySplit = new string[] { "=" };
            convertor = PdfRasterizerFactory.Create(pdfRasterizerHost);


            var queues = new[]  { 
                new { 
                    Q = new DBMessageQueue(ConstMsg.PNGConversionQueue),
                    C = ConstStage.QueuePdfConverter_LOQSlice,
                    N = "LO_Q",
                    E = E_EDocQueueT.Main
                },
                new {
                    Q = new DBMessageQueue(ConstMsg.PNGConversionPMLQueue),
                    C = ConstStage.QueuePdfConverter_PMLQSlice,
                    N = "PML_Q",
                    E = E_EDocQueueT.PML
                },
                new {
                    Q = new DBMessageQueue(ConstMsg.PNGConversionFaxQueue),
                    C = ConstStage.QueuePdfConverter_FAXQSlice,
                    N = "FAX_Q",
                    E = E_EDocQueueT.Fax
                }
            };


            bool quit;
            DBMessage m;
            int currentItemProcessCount = 0;
            using (var workerTiming = new WorkerExecutionTiming("QueuePdfConverter"))
            {
                do
                {
                    quit = true;
                    for (int i = 0; i < queues.Length; i++)
                    {
                        var qd = queues[i];
                        for (int x = 0; x < qd.C; x++)
                        {
                            if (ismsmq)
                            {
                                EDocQueueDetails details = EDocConversionQueue.Dequeue(qd.E);

                                if (details == null)
                                {
                                    break;
                                }

                                using (var itemTiming = workerTiming.RecordItem("Q=" + qd.N + "; " + details.ToString(), details.ArrivalTime))
                                {
                                    try
                                    {
                                        HandleMessage(details.DocumentId, details.Values);
                                    }
                                    catch (KeyNotFoundException e)
                                    {
                                        LogManager.Error(details.ToString(), e);
                                        itemTiming.RecordError(e);
                                    }
                                    catch (Exception e)
                                    {
                                        itemTiming.RecordError(e);
                                        Tools.LogErrorWithCriticalTracking("[QueuePdfConverter] " + System.Environment.MachineName, e);
                                        throw;
                                    }
                                }

                                quit = false;
                                if (currentItemProcessCount++ >= maxProcessingItems)
                                {
                                    // 5/28/2015 dd - Stop processing.
                                    return;
                                }
                            }
                            else
                            {
                                #region dbqueuehandling
                                try
                                {
                                    m = qd.Q.Receive();

                                    if (m == null)
                                    {
                                        break;
                                    }
                                    using (var itemTiming = workerTiming.RecordItem("Q=" + qd.N + ";" + m.Data, m.InsertionTime))
                                    {
                                        Dictionary<string, string> data = ParseData(m.Data);
                                        try
                                        {
                                            HandleMessage(m.Subject1, data);
                                        }
                                        catch (KeyNotFoundException e)
                                        {
                                            LogManager.Error(m.Data, e);
                                            itemTiming.RecordError(e);
                                        }
                                        catch (Exception e)
                                        {
                                            itemTiming.RecordError(e);
                                            throw;
                                        }
                                    }

                                    quit = false;
                                    if (currentItemProcessCount++ >= maxProcessingItems)
                                    {
                                        // 5/28/2015 dd - Stop processing.
                                        return;
                                    }
                                }
                                catch (DBMessageQueueException e)
                                {
                                    if (e.MessageQueueErrorCode != DBMessageQueueErrorCode.IOTimeout && e.MessageQueueErrorCode != DBMessageQueueErrorCode.EmptyQueue)
                                    {
                                        LogManager.Error("[QueuePdfConverter]DBMQException " + e.Message);
                                        throw;
                                    }
                                    break;//empty no need to check this queue again

                                }
                                catch (Exception e)
                                {
                                    LogManager.Error("[QueuePdfConverter]Exception ", e);
                                    throw;
                                }
                                #endregion
                            }
                        }
                    }
                } while (!quit);
            }

        }
        #endregion

        private Dictionary<string, string> ParseData(string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                return null;
            }
            Dictionary<string, string> keysAndValues = new Dictionary<string, string>();
            string[] entries = data.Split(m_split, StringSplitOptions.RemoveEmptyEntries);

            foreach (string entry in entries)
            {
                string[] keySplit = entry.Split(m_keySplit, StringSplitOptions.RemoveEmptyEntries);
                if (keySplit.Length != 2 || keySplit[0].Length == 0 || keySplit[1].Length == 0)
                {
                    continue; //should probably log
                }

                keysAndValues.Add(keySplit[0], keySplit[1]);
            }
            return keysAndValues;
        }

        private void HandleMessage(string docid, Dictionary<string,string> data)
        {
            float thumbScale = float.Parse(data["ThumbScale"]);
            float scale = float.Parse( data["Scale"]);
            string filedb =  data["FileDbSiteCode"];
            string key = data["DocumentFileDBLocation"];
            string suffix = data["ThumbSuffix"];
            bool verifyExisting = bool.Parse(data["ErrorRequeue"]);
            Guid brokerId = new Guid(data["BrokerId"].TrimWhitespaceAndBOM()); 

            HandleRequest(new Guid(docid), filedb, key, suffix, scale, thumbScale, verifyExisting, brokerId);
        }


        private void RunAndTime(string label, Action action)
        {
            RunAndTime(label, action, null);
        }

        private void RunAndTime(string label, Action action, params Object[] parameters)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            string formattedLabel = "".PadLeft(m_indentation * 3);
            if (parameters == null)
            {
                formattedLabel += label;
            }
            else
            {
                formattedLabel += string.Format(label, parameters);
            }
            try
            {

                m_performanceLog.AppendFormat("{0,-50}{1,-20}{2,-20}", formattedLabel, DateTime.Now.TimeOfDay, 0);
                m_performanceLog.AppendLine();
                m_indentation += 1;
                action();
            }
            catch (Exception)
            {
                label += "*";
                throw;
            }
            finally
            {
                sw.Stop();
                m_indentation -= 1;
                if (sw.ElapsedMilliseconds != 0)
                {
                    m_performanceLog.AppendFormat("{0,-50}{1,-20}{2,-10}", formattedLabel, DateTime.Now.TimeOfDay, sw.ElapsedMilliseconds);
                    m_performanceLog.AppendLine();
                }
            }
        }

        private void HandleRequest(Guid docId, string fileDB, string filedbKey, string thumbSuffix, float scale, float thumbScale, bool verifyExistingImages, Guid brokerId)
        {
            Tools.ResetLogCorrelationId();

            m_performanceLog = new StringBuilder();
            long startingTicks = DateTime.Now.Ticks;
            m_indentation = 0;

            m_performanceLog.AppendFormat("[QueuePdfConverter] - DocId: {0} FileDB: {1} Key: {2} Scale: {3} ThumbSuffix: {4}", docId, fileDB, filedbKey, scale, thumbSuffix);
            m_performanceLog.AppendLine();

            try
            {
                RunAndTime("Convert", () => InspectAndIssueRequestForConversion(docId, fileDB, filedbKey, scale, thumbScale, thumbSuffix, verifyExistingImages, brokerId));
            }
            catch (Exception e)
            {
                LogManager.Error("[QueuePdfConverter]  " + docId + " " + filedbKey + " " + e.ToString());
                EDocument.UpdateImageStatus(brokerId, docId, E_ImageStatus.Error);
                throw;
            }

            finally
            {
                long endingTicks = DateTime.Now.Ticks;
                double elapseTime = (endingTicks - startingTicks) / (double)TimeSpan.TicksPerMillisecond;
                m_performanceLog.AppendFormat("Executed in {0}ms", elapseTime);
                LogManager.Info(m_performanceLog.ToString());
            }
        }

        private int InspectAndIssueRequestForConversion(Guid docId, string dbname, string dbkey, float scale, float thumbScale, string thumbSuffix, bool verifyExistingImages, Guid brokerId)
        {
            string path = "";
            int pageCount = 0;

            try
            {
                RunAndTime("RetrievePDF", () => path = FileDBTools.CreateCopy(dbname.GetFileDBType(), dbkey));
            }
            catch (FileNotFoundException)
            {
                string currentKey = EDocument.GetCurrentPDFKey(brokerId, docId);
                if (currentKey.Equals(dbkey, StringComparison.OrdinalIgnoreCase))
                {
                    Tools.LogError(string.Format("Conversion issued for broker {0} document {1} pdfkey {2} but pdf not found.", brokerId, docId, dbkey));
                }
                return 0;
            }

            RunAndTime("UpdateStatus", () => EDocument.UpdateImageStatus(brokerId, docId, E_ImageStatus.NoCachedImagesButInQueue));


            List<float> scales = new List<float>(2) { scale, thumbScale };
            List<Dictionary<int, string>> pageAndIds = new List<Dictionary<int, string>>(2)
            {
                new Dictionary<int, string>(),
                new Dictionary<int, string>()
            };

            var newPagePngs = pageAndIds[0];
            var newThumbPngs = pageAndIds[1];
            PdfReader reader = null;

            RunAndTime("CreatePdfReader", () => reader = EDocumentViewer.CreatePdfReader(path));


            using (PDFPageIDManager pdfManager = new PDFPageIDManager(path))
            {
                pageCount = pdfManager.PageCount;

                foreach (int page in pdfManager.PagesWithNoAnnotations)
                {
                    var key = Guid.NewGuid().ToString();
                    newPagePngs.Add(page - 1, key);
                    newThumbPngs.Add(page - 1, key + thumbSuffix);
                }

                if (verifyExistingImages)
                {
                    foreach (var entry in pdfManager.PagesWithAnnotations)
                    {
                        var key = entry.Value;

                        if (!FileDBTools.DoesFileExist(E_FileDB.EDMS, key))
                        {
                            newPagePngs.Add(entry.Key - 1, key);
                            newThumbPngs.Add(entry.Key - 1, key + thumbSuffix);
                        }
                    }
                }

                if (newPagePngs.Count == 0)
                {
                    RunAndTime("UpdateStatus-Empty", () => EDocument.UpdateImageStatus(brokerId, docId, E_ImageStatus.HasCachedImages));
                    return 0;
                }

                RunAndTime("CallRasterizer", () => CallRasterizer(dbname, dbkey, path, scales, pageAndIds));
        

                //record all the images so we can delete in a few months
                RunAndTime("InsertNewPng", () => EDocument.InsertNewPNG(brokerId, docId, newPagePngs.Values.Union(newThumbPngs.Values)));

                // Update the PDF annotations
                foreach (KeyValuePair<int, string> newPage in newPagePngs)
                {
                    pdfManager.Update(newPage.Key + 1, newPage.Value);
                }

                path = TempFileUtils.NewTempFilePath();
                RunAndTime("PdfManager.Save", () => pdfManager.Save(path));

                DocumentMetaData metaData = null;

                RunAndTime("GetMetaData", () => metaData = pdfManager.GetMetaData(new Guid(dbkey)));
                RunAndTime("WriteFile", () => FileDBTools.WriteFile(dbname.GetFileDBType(), dbkey, path));
                RunAndTime("UpdateImageStatus-Final", () => EDocument.UpdateImageStatus(brokerId, docId, E_ImageStatus.HasCachedImages));
                RunAndTime("VerifyContentsAfterSave", () => VerifyContentsAfterSave(docId, path, dbkey, newPagePngs));

                if (metaData != null)
                {
                    RunAndTime("SaveMeta", () => EDocument.SaveMetaData(brokerId, docId, new Guid(dbkey), metaData));
                }
            }
            m_performanceLog.AppendLine("Number Of Pages: " + pageCount);
            return pageCount;
        }

        private void VerifyContentsAfterSave(Guid docId, string filePath, string key, Dictionary<int,string> pages)
        {
            if (!ConstStage.IsEnableEDocVerify)
            {
                return; 
            }

            string newPath = FileDBTools.CreateCopy(E_FileDB.EDMS, key);

            FileInfo newPathInfo = new FileInfo(newPath);
            FileInfo filePathInfo = new FileInfo(filePath);

            if (newPathInfo.Length != filePathInfo.Length)
            {
                Tools.LogError("IsEnableEDocVerify - File Lengths don't match when checking the updated PDF for docid " + docId + " and " + key); 
            }


            foreach (var entry in pages)
            {
                try
                {
                    FileDBTools.CreateCopy(E_FileDB.EDMS, entry.Value);
                }
                catch (FileNotFoundException)
                {
                    Tools.LogError("Image not found in FileDB even though it should be there " + docId + " key " + key);
                }
            }

        }

        private void CallRasterizer(string dbName, string dbKey, string pdfPath, List<float> scales, List<Dictionary<int, string>> ids)
        {
            if (string.IsNullOrEmpty(ConstSite.PdfRasterizerHost) || ConstSite.PdfRasterizerHost.Equals("localhost"))
            {
                try
                {
                    RunAndTime("Convert-Local", () =>  m_performanceLog.Append( convertor.ConvertToPngAndSaveToFileDBFromFile(dbName, pdfPath, scales, ids)));
                }
                catch (Win32Exception)
                {
                    convertor = PdfRasterizerFactory.Restart(ConstSite.PdfRasterizerHost);
                    RunAndTime("Convert-Restart-Local", () => m_performanceLog.Append(convertor.ConvertToPngAndSaveToFileDBFromFile(dbName, pdfPath, scales, ids)));
                }
            }
            else
            {
                try
                {
                    RunAndTime("Convert-Else", () =>  m_performanceLog.Append( convertor.ConvertToPngAndSaveToFileDBFromFileDB(dbName, dbKey, scales, ids)));
                }
                catch (Win32Exception)
                {
                    convertor = PdfRasterizerFactory.Restart(ConstSite.PdfRasterizerHost);
                    RunAndTime("Convert-Else-Local", () => m_performanceLog.Append(convertor.ConvertToPngAndSaveToFileDBFromFileDB(dbName, dbKey, scales, ids)));
                }
            }
        }

    }
}
