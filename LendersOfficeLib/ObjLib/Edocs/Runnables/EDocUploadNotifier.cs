﻿//-----------------------------------------------------------------------
// <summary>
//      Sends out EDocument upload notifications.
// </summary>
// <copyright file="EDocUploadNotifier.cs" company="MeridianLink">
//      Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <author>Antonio Valencia</author>
//-----------------------------------------------------------------------
namespace EDocs.Runnable
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.DatabaseMessageQueue;
    
    /// <summary>
    /// Sends out email notifications for documents that have been uploaded. 
    /// It waits for a certain amount of time <c>ConstStage.EDocNotificationCheckerSleepTimeInMinutes</c>
    /// and then sends out a single email for a loan containing multiple documents.
    /// </summary>
    public sealed class EDocUploadNotifier : CommonProjectLib.Runnable.IRunnable
    {
        /// <summary>
        /// Keeps the oldest message insertion time. 
        /// </summary>
        private DateTime oldestMessageInsertionTime = DateTime.MinValue;

        /// <summary> Gets a description for the runnable. </summary>
        /// <value>The Description property gets the description of the runnable.</value>
        public string Description
        {
            get 
            { 
                return string.Format("Responsible for sending out EDoc Uploads Notifications in batches every {0} minutes old", ConstStage.EDocNotificationCheckerSleepTimeInMinutes); 
            }
        }

        /// <summary>
        /// Sends out the notifications and catches logs and then throws exceptions.
        /// </summary>
        public void Run()
        {
            try
            {
                this.UnsafeRun();
            }
            catch (Exception e)
            {
                // opm 204145
                Tools.LogErrorWithCriticalTracking("[EDocUploadNotifier] [EDocUploadNotifierUnhandledException] had unhandled exception whilst sending notifications", e);
                throw; // at some point it might be nice to not require this for queue handlers and catch the exception within the main loop.
            }
        }

        /// <summary>
        /// Sends out the notification without catching exceptions.
        /// </summary>
        private void UnsafeRun()
        {
            // There's no point to hit the DB when the oldest message is not outside the cut off.
            if (this.oldestMessageInsertionTime != DateTime.MinValue &&
                this.IsWithinNotificationCutoff(this.oldestMessageInsertionTime))
            {
                return;
            }

            Tools.SetupServicePointManager();

            DBMessageQueue queue = new DBMessageQueue(ConstMsg.EDocsQueue);
            do
            {
                DBMessage[] messages = queue.PeekBySubject();
                if (messages.Length == 0)
                {
                    this.oldestMessageInsertionTime = DateTime.MinValue;
                    return;
                }

                DBMessage message = messages[0];

                if (this.IsWithinNotificationCutoff(message.InsertionTime))
                {
                    this.oldestMessageInsertionTime = message.InsertionTime;
                    return;
                }

                Tools.LogInfo("[EDocUploadNotifier] Sending LoanId " + messages[0].Subject1);

                List<string> docIds = new List<string>();
                foreach (DBMessage msg in messages)
                {
                    docIds.Add(msg.Subject2);
                }

                EDocumentNotifier.SendNewEDocumentNotification(docIds);
                queue.DequeueBySubject(message.Subject1, message.Id, messages.Last().Id);
            }
            while (true);
        }

        /// <summary>
        /// Checks if the given date is within the document notification time constant. 
        /// </summary>
        /// <param name="date">The date to check.</param>
        /// <returns>True if its within the cut off.</returns>
        private bool IsWithinNotificationCutoff(DateTime date)
        {
            return Math.Abs(DateTime.Now.Subtract(date).TotalMinutes) < ConstStage.EDocNotificationCutoffInMinutes;
        }
    }
}
