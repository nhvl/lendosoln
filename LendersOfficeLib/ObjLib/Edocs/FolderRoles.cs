﻿using System;
using System.Collections.Generic;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;

namespace EDocs
{
    public class FolderRoles
    {
        public string RoleDesc { get; private set;}
        public string RoleDisplayDesc { get; private set;}
        public Guid RoleId { get; private set;}
        public int FolderId { get; private set;}
        public bool IsIncludedInFolder { get; set; }
        public bool IsAdmin { get; private set;}
        public bool IsPmlUser { get; private set; }

        public FolderRoles(int folderId, Guid roleId, bool isIncludedInFolder, bool isPmlUser)
        {
            FolderId = folderId;
            RoleId = roleId;
            IsPmlUser = isPmlUser;
            RoleDisplayDesc = RoleModifiableDescriptions.GetRoleModifiableDescription(new EDocRoles() {RoleId = RoleId, IsPmlUser = IsPmlUser });

            Role role = Role.Get(roleId);
            RoleDesc = role.Desc;

            if (role.RoleT == E_RoleT.Administrator && IsPmlUser == false)
            {
                IsIncludedInFolder = true;
                IsAdmin = true;
            }
            else
            {
                IsIncludedInFolder = isIncludedInFolder;
            }

        }
    }

    public static class RoleModifiableDescriptions
    {
        private static Dictionary<EDocRoles, string> roleModifiableDescriptions = null;
        private static bool HasBeenLoaded = false;

        static RoleModifiableDescriptions()
        {
            if (HasBeenLoaded == false)
            {
                roleModifiableDescriptions = new Dictionary<EDocRoles, string>();
                foreach (var role in Role.AllRoles)
                {
                    Guid roleId = role.Id;
                    bool isPmlUser = role.RoleT == E_RoleT.Pml_BrokerProcessor
                        || role.RoleT == E_RoleT.Pml_Secondary
                        || role.RoleT == E_RoleT.Pml_PostCloser;

                    string desc = role.ModifiableDesc;
                    if (role.RoleT != E_RoleT.LoanOfficer)
                    {
                        roleModifiableDescriptions.Add(new EDocRoles() { RoleId = roleId, IsPmlUser = isPmlUser }, desc);
                    }
                    else
                    {
                        roleModifiableDescriptions.Add(new EDocRoles() { RoleId = role.Id, IsPmlUser = false }, "Loan Officer : LOS user");
                    }

                }

                roleModifiableDescriptions.Add(new EDocRoles() { RoleId = Role.Get(E_RoleT.Administrator).Id, IsPmlUser = true }, "PML Supervisor");
                roleModifiableDescriptions.Add(new EDocRoles() { RoleId = Role.Get(E_RoleT.LoanOfficer).Id, IsPmlUser = true }, "Loan Officer : PML user");

                HasBeenLoaded = true;
            }
            
        }

        public static string GetRoleModifiableDescription(EDocRoles roleId)
        {
            if (roleModifiableDescriptions.ContainsKey(roleId))
            {
                return roleModifiableDescriptions[roleId];
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Role: " + roleId.RoleId + ", IsPmlUser:" + roleId.IsPmlUser + " not found when looking for role modifiable description.");
            }
        }
    }
}
