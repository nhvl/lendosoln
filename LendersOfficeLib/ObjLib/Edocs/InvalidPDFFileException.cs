﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using DataAccess;

namespace EDocs
{
    public sealed class InvalidPDFFileException : CBaseException
    {

            public InvalidPDFFileException()
                : base(ErrorMessages.EDocs.InvalidPDF, ErrorMessages.EDocs.InvalidPDF)
            {
                this.IsEmailDeveloper = false;
            }

    }
}
