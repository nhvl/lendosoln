﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using System.Threading;
using LendersOffice.ObjLib.Security;
using EDocs.Contents;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Constants;

namespace EDocs
{
    /// <summary>
    /// Allows access to the Electronic Documents. It enforces permission checking. 
    /// </summary>
    public class EDocumentRepository 
    {
        private EDocumentPermissions m_docPermissions;

        private Guid m_BrokerId;
        private Guid? m_UserId;

        #region private methods 
        private EDocumentRepository(EDocumentPermissions permissions, Guid? userId, Guid brokerId)
        {
            m_docPermissions = permissions;
            m_UserId = userId;
            m_BrokerId = brokerId; 
        }

        private EDocument CreateDocument(E_EDocumentSource source, Guid? documentId)
        {
            if (m_docPermissions.CanCreateDocument())
            {
                var doc = new EDocument(source, this.m_BrokerId, documentId);

                return doc;

            }

            throw new AccessDenied();
        }

        #endregion

        /// <summary>
        /// Gets a value indicating whether the repository will allow document creation.
        /// </summary>
        public bool CanCreateDocument => this.m_docPermissions.CanCreateDocument();

        public bool CanUpdateDocument(EDocument doc)
        {
            return m_docPermissions.CanUpdateDocument(doc);
        }

        /// <summary>
        /// Retrieves a repository which can access all edcouments. 
        /// The instances bypasses all permission checking.
        /// </summary>
        /// <returns></returns>
        public static EDocumentRepository GetSystemRepository(Guid brokerId)
        {
            return new EDocumentRepository(new SystemEDocumentPermissions(), null, brokerId);
        }

        /// <summary>
        /// Retrieves a repository which can access all edocuments. 
        /// The instance bypasses all permission checking.
        /// </summary>
        /// <returns></returns>
        public static EDocumentRepository GetSystemRepositoryForUser(Guid brokerId, Guid? userId)
        {
            return new EDocumentRepository(new SystemEDocumentPermissions(), userId, brokerId);
        }

        /// <summary>
        /// Retrieves a repository where only documents that are in the same broker and the current principal can read. 
        /// </summary>
        /// <param name="principal">Principal to use for access checking.</param>
        /// <returns></returns>
        public static EDocumentRepository GetUserRepository()
        {
            return GetUserRepository((AbstractUserPrincipal)Thread.CurrentPrincipal);
        }

        /// <summary>
        /// Gets the document repository of the user.
        /// </summary>
        /// <param name="principal">The user's principal.</param>
        /// <returns>The document repository.</returns>
        public static EDocumentRepository GetUserRepository(AbstractUserPrincipal principal)
        {
            using (PerformanceStopwatch.Start("EDocumentRepository.GetUserRepository()"))
            {
                Guid? UserId = null;
                if (!(principal is ConsumerUserPrincipal || principal is ConsumerPortalUserPrincipal))
                    UserId = principal.UserId;

                return new EDocumentRepository(new UserEDocumentPermissions(principal), UserId, principal.BrokerId);
            }
        }

        /// <summary>
        /// Retrieves a repository where only documents that are in the same broker and the current principal can read. 
        /// </summary>
        /// <param name="principal">Principal to use for access checking.</param>
        /// <returns></returns>
        public static EDocumentRepository GetUserRepositoryForSystem(Guid userId, Guid brokerId)
        {
            using (PerformanceStopwatch.Start("EDocumentRepository.GetUserRepository()"))
            {

                return new EDocumentRepository(new SystemEDocumentPermissions(), userId, brokerId);
            }
        }

        /// <summary>
        /// Creates a new EDocument. If the repository user does not have the propper roles  a exception will be thrown.
        /// </summary>
        /// <returns></returns>
        public EDocument CreateDocument(E_EDocumentSource source)
        {
            return CreateDocument(source, null);
        }

        /// <summary>
        /// Creates a new EDocument. If the repository user does not have the propper roles a exception will be thrown. 
        /// </summary>
        /// <returns></returns>
        public EDocument CreateDocumentFromExisting(E_EDocumentSource source, Guid documentId)
        {
            return CreateDocument(source, documentId);
        }

        public GenericEDocument CreateGenericDocument(E_FileType fileType)
        {
            return CreateLinkedGenericDocument(fileType, Guid.NewGuid());
        }

        /// <summary>
        /// Creates a new Generic EDocument from the file name. Links it to the srcDocument by 
        /// srcDocumentId. The link is same id.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="srcDocumentId"></param>
        /// <returns></returns>
        public GenericEDocument CreateLinkedGenericDocument(E_FileType fileType,  Guid srcDocumentId)
        {
            if (!m_docPermissions.CanCreateDocument())
            {
                throw new AccessDenied();
            }

            GenericEDocument doc; 
            switch (fileType)
            {
                case E_FileType.AppraisalXml:
                    doc = new AppraisalGenericEDocument(m_BrokerId);
                    break;
                case E_FileType.MicrosoftSpreadsheet:
                    doc = new MicrosoftSpreadsheetGenericEDocument(m_BrokerId);
                    break;
                case E_FileType.UniformClosingDataset:
                    doc = new UcdGenericEDocument(m_BrokerId);
                    break;
                case E_FileType.UnspecifiedXml:
                    doc = new XmlGenericEDocument(m_BrokerId);
                    break;
                default:
                    throw new UnhandledEnumException(fileType);
            }

            doc.DocumentId = srcDocumentId;
            return doc;
        }

        public IEnumerable<ModifiedDocumentDetail> GetRecentlyModifiedDocs(DateTime latestModifiedDate)
        {

            Func<IEnumerable<ModifiedDocumentDetail>> getDocs = () =>
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@BrokerId", this.m_BrokerId),
                    new SqlParameter("@ModifiedDate", latestModifiedDate)
                };

                LinkedList<ModifiedDocumentDetail> modifiedDocuments = new LinkedList<ModifiedDocumentDetail>();

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.m_BrokerId, "EDOCS_GetDocumentsByBrokerAndModifiedDate", parameters))
                {
                    while (reader.Read())
                    {
                        ModifiedDocumentDetail detail = new ModifiedDocumentDetail
                        {
                            LoanId = (Guid)reader["sLId"],
                            LoanName = (string)reader["sLNm"],
                            FolderName = (string)reader["FolderName"],
                            DocTypeName = (string)reader["DocTypeName"],
                            DocumentId = (Guid)reader["DocumentId"],
                            FirstName = (string)reader["aBFirstNm"],
                            LastName = (string)reader["aBLastNm"],
                            Description = (string)reader["Description"],
                            Comments = (string)reader["InternalDescription"],
                            Pages = (int)reader["NumPages"],
                            LastModifiedDate = (DateTime)reader["LastModifiedDate"],
                            HideFromPmlUsers = (bool)reader["HideFromPmlUsers"],
                            DocTypeId = (int)reader["DocTypeId"]
                        };

                        modifiedDocuments.AddLast(detail);
                    }
                }

                return modifiedDocuments;
            };

            return this.m_docPermissions.GetReadableDocuments(getDocs);
        }

        /// <summary>
        /// Saves generic document. Does permission checks as well.
        /// </summary>
        /// <param name="genericDocument"></param>
        /// <param name="userId"></param>
        public void Save(GenericEDocument genericDocument, Guid userId)
        {
            if (genericDocument.IsNew && !m_docPermissions.CanCreateDocument())
            {
                throw new AccessDenied();
            }

            if (!genericDocument.IsNew && !m_docPermissions.CanUpdateGenericDocument(genericDocument))
            {
                throw new AccessDenied();
            }

            genericDocument.Save(userId);
        }

        public void DeleteGenericDoc(Guid documentId, Guid userId)
        {
            GenericEDocument doc = GenericEDocument.GetDocument(m_BrokerId, documentId);
            doc.IsValid = false;
            doc.Save(userId);
        }

        /// <summary>
        /// Saves the given EDocument a audit event is created with the given reason.  
        /// If the user does not have the correct permissions a exception will be thrown.
        /// </summary>
        /// <param name="document"></param>
        /// <param name="reason"></param>
        public void Save(EDocument document)
        {
            if ( document.IsNew && m_docPermissions.CanCreateDocument() )
            {
                document.Save(m_UserId);
                return;
            }
            if (m_docPermissions.CanUpdateDocument(document))
            {
                document.Save(m_UserId);
                return;
            }

            throw new AccessDenied();  
        }

        /// <summary>
        /// Allow saving non-PDF content, such as DocType and Internal Comments, even if the PDF content itself
        /// is no longer editable.  As long as the user has read access to the document, we'll allow them to edit these
        /// fields when updating a document (creation remains the same).
        /// </summary>
        /// <param name="document"></param>
        /// <param name="reason"></param>
        public void SaveNonPdfData(EDocument document)
        {
            if (document.IsNew && m_docPermissions.CanCreateDocument())
            {
                document.Save(m_UserId);
                return;
            }
            if (m_docPermissions.CanReadDocument(document))
            {
                document.Save(m_UserId);
                return;
            }

            throw new AccessDenied();
        }

        /// <summary>
        /// Retrieves the document with the given id. Throws exception if it doesnt exist.
        /// Throws exception if the user cannot read the document.
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public EDocument GetDocumentById(Guid documentId)
        {
            using (PerformanceStopwatch.Start("EDocumentRepository.GetDocumentById"))
            {
                var doc = EDocument.GetDocumentById(documentId, m_BrokerId, false);

                if (m_docPermissions.CanReadDocument(doc))
                {
                    return doc;
                }
                throw new AccessDenied();
            }
        }

        /// <summary>
        /// Retrieves the document with the given id. Throws exception if it doesnt exist.
        /// Throws exception if the user cannot read the document.
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public EDocument GetDeletedDocumentById(Guid documentId)
        {
            using (PerformanceStopwatch.Start("EDocumentRepository.GetDocumentById"))
            {
                var doc = EDocument.GetDocumentById(documentId, m_BrokerId, true);

                if (m_docPermissions.CanReadDocument(doc))
                {
                    return doc;
                }
                throw new AccessDenied();
            }
        }

        /// <summary>
        /// Retrieves all the documents for the given loan. Only returns the ones the user has access to.
        /// </summary>
        /// <param name="loanId"></param>
        /// <returns></returns>
        public IList<EDocument> GetDocumentsByLoanId(Guid loanId)
        {

            var docs = EDocument.GetDocumentsByLoanId(loanId, m_BrokerId);

            IList<EDocument> documents = new List<EDocument>(docs.Count);
            foreach (var doc in docs)
            {
                if (m_docPermissions.CanReadDocument(doc))
                {
                    documents.Add(doc);
                }
            }
            return documents;
        }

        public HashSet<Guid> GetDocumentIdsByLoanId(Guid loanId)
        {
            var documents = this.GetDocumentsByLoanId(loanId);
            HashSet<Guid> documentIds = new HashSet<Guid>();
            foreach (var docs in documents)
            {
                documentIds.Add(docs.DocumentId);
            }

            return documentIds;
        }

        public GenericEDocument GetGenericDocumentById(Guid docId)
        {
            var doc = GenericEDocument.GetDocument(m_BrokerId, docId);
            if (doc == null)
            {
                throw CBaseException.GenericException("EDoc not found.");
            }

            if (!m_docPermissions.CanReadDocument(doc))
            {
                throw new AccessDenied();
            }

            return doc;
        }

        public IEnumerable<GenericEDocument> GetGenericDocumentsByLoanId(Guid loanId)
        {
            foreach (GenericEDocument doc in GenericEDocument.GetDocumentsByLoanId(m_BrokerId, loanId))
            {
                if (m_docPermissions.CanReadDocument(doc))
                {
                    yield return doc;
                }
            }
        }

        public IList<EDocument> GetDeletedDocumentsByLoanId(Guid loanId, Guid brokerId)
        {
            var docs = EDocument.GetDeletedDocumentsByLoanId(loanId, brokerId);

            IList<EDocument> documents = new List<EDocument>(docs.Count);
            foreach (var doc in docs)
            {
                if (m_docPermissions.CanReadDocument(doc))
                {
                    documents.Add(doc);
                }
            }
            return documents;
        }

        /// <summary>
        /// Retrieves all the documents in the given shipping template for the specified loan. The broker id from the principal is used.
        /// </summary>
        /// <param name="shippingTemplate"></param>
        /// <param name="loanId"></param>
        /// <returns></returns>
        public IList<EDocument> GetDocumentsByShippingTemplate(int shippingTemplateId,  Guid loanId)
        {
        
            var docs = EDocument.GetDocumentsByShippingTemplate(shippingTemplateId, m_BrokerId, loanId );
            IList<EDocument> documents = new List<EDocument>(docs.Count);

            foreach (var doc in docs)
            {
                if (m_docPermissions.CanReadDocument(doc))
                {
                    documents.Add(doc);
                }
            }
            return documents;
        }

        /// <summary>
        /// Retrieves all the documents in the given shipping template for the specified loan, sorted by doc type and then last modified date (most recent first). The broker id from the principal is used.
        /// </summary>
        /// <param name="sShippingTemplateName"></param>
        /// <param name="loanId"></param>
        /// <returns></returns>
        public List<EDocument> GetSortedDocumentsByShippingTemplate(string sShippingTemplateName, Guid loanId)
        {
            EDocumentShippingTemplate shippingTemplate = new EDocumentShippingTemplate();
            int iShippingTemplateID = shippingTemplate.GetActiveShippingTemplateID(sShippingTemplateName);

            List<EDocument> unsortedEdocs = new List<EDocument>(GetDocumentsByShippingTemplate(iShippingTemplateID, loanId));
            List<EDocument> sortedEdocs = new List<EDocument>(unsortedEdocs.Count());

            unsortedEdocs.Sort((doc1, doc2) => doc1.LastModifiedDate.CompareTo(doc2.LastModifiedDate));
            unsortedEdocs.Reverse();

            List<DocType> docTypesInTemplate = shippingTemplate.GetShippingTemplateData(iShippingTemplateID);

            foreach (DocType doc in docTypesInTemplate)
            {
                sortedEdocs.AddRange(unsortedEdocs.Where(d => d.DocTypeName.ToLower() == doc.DocTypeName.ToLower() && d.Folder.FolderNm.ToLower() == doc.Folder.FolderNm.ToLower()));
                unsortedEdocs.RemoveAll(d => d.DocTypeName.ToLower() == doc.DocTypeName.ToLower() && d.Folder.FolderNm.ToLower() == doc.Folder.FolderNm.ToLower());
            }

            return sortedEdocs;
        }

        /// <summary>
        /// Marks the document with the id as invalid.  Creates audit event as well. This method may
        /// trigger a loan save when attempting to break associations with Closing Disclosure rows.
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="sReason"></param>
        public void DeleteDocument(Guid documentId, string sReason)
        {
            var doc = EDocument.GetDocumentById(documentId, m_BrokerId, false );

            if (m_docPermissions.CanDeleteDocument(doc))
            {
                this.BreakClosingDisclosureAssociation(doc);
                EDocument.InvalidateDocument(m_BrokerId, documentId, m_UserId, sReason);
                return;
            }

            throw new AccessDenied();
        }

        /// <summary>
        /// Breaks any associations with a Closing Disclosure.
        /// </summary>
        /// <param name="document">The document to disassociate.</param>
        private void BreakClosingDisclosureAssociation(EDocument document)
        {
            var documentId = document.DocumentId;
            var loanId = document.LoanId;

            if (loanId == null || !loanId.HasValue)
            {
                return;
            }

            var dataLoan = CPageData.CreateUsingSmartDependency(loanId.Value, typeof(EDocumentRepository));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            var associatedCd = dataLoan.sClosingDisclosureDatesInfo?.ClosingDisclosureDatesList?.FirstOrDefault(cd => cd.UcdDocument == documentId);
            if (associatedCd != null)
            {
                associatedCd.UcdDocument = Guid.Empty;
                dataLoan.Save();
            }
        }

        public void RestoreDocument(EDocument doc, string sReason)
        {
            if (m_UserId.HasValue == false )
            {
                throw new ArgumentException("UserId Is Required");
            }
            if (doc.IsValid)
            {
                return; // nothing to do
            }
            if (m_docPermissions.CanReadDocument(doc))
            {
                EDocument.RestoreDocument(m_BrokerId, doc.DocumentId, m_UserId.Value, sReason);
                return;
            }

            throw new AccessDenied();
        }
    }
}