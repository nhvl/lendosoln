﻿namespace EDocs
{
    using LendersOffice.Security;

    /// <summary>
    /// Container that only contains esign tags.
    /// </summary>
    public class EDocumentESignTagContainer : EDocumentAnnotationContainer
    {
        /// <summary>
        /// Whether a user has the permission to update the esign tags.
        /// </summary>
        /// <param name="principal">The principal of the user.</param>
        /// <returns>True if able to update esign tags. False otherwise.</returns>
        protected override bool HasUpdatePermission(AbstractUserPrincipal principal)
        {
            return principal.HasPermission(Permission.AllowCreatingDocumentSigningEnvelopes);
        }

        /// <summary>
        /// Whether a user has the permission to view the esign tags.
        /// </summary>
        /// <param name="principal">The principal.</param>
        /// <returns>True if able to view the esign tags. False otherwise.</returns>
        protected override bool HasViewPermission(AbstractUserPrincipal principal)
        {
            return principal.HasPermission(Permission.AllowCreatingDocumentSigningEnvelopes);
        }

        /// <summary>
        /// Validates the annotation item. Makes sure its a proper esign tag item.
        /// </summary>
        /// <param name="item">The item to validate.</param>
        /// <returns>True if valid. False otherwise.</returns>
        protected override bool Validate(EDocumentAnnotationItem item)
        {
            // We want to enforce that only esign fields are saved in this container
            if (item.ItemType != E_EDocumentAnnotationItemType.SignedDate &&
                item.ItemType != E_EDocumentAnnotationItemType.Initial &&
                item.ItemType != E_EDocumentAnnotationItemType.Signature)
            {
                return false;
            }

            if (!item.AssociatedRole.HasValue && !item.AssociatedBorrower.HasValue)
            {
                return false;
            }

            return true;
        }
    }
}
