﻿namespace LendersOffice.ObjLib.Edocs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The Database barcode record information.
    /// </summary>
    public interface IDocVendorDocTypeRecord
    {
        /// <summary>
        /// Gets the field Id in barcode table.
        /// </summary>
        int Id { get; }

        /// <summary>
        /// Gets Barcode Classification.
        /// </summary>
        string BarcodeClassification { get; }

        /// <summary>
        /// Gets barcode's description.
        /// </summary>
        string InternalDescription { get; }
    }
}
