﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.ObjLib.Edocs.DocuTech
{
    public class DocuTechDocType : IDocVendorDocType, IDocVendorDocTypeRecord
    {
        public string Description { get; private set; }
        public int Id { get; private set; }
        public string BarcodeClassification { get; private set; }
        public string InternalDescription { get { return this.Description; } }

        private DocuTechDocType(int id, string description, string barcodeClassification)
        {
            this.Id = id;
            this.Description = description;
            this.BarcodeClassification = barcodeClassification;
        }

        private static TimeBasedCacheObject<IEnumerable<DocuTechDocType>> docuTechDocTypeList;

        static DocuTechDocType()
        {
            ReloadAll();
        }

        private static void ReloadAll()
        {
            // 4/21/2015 dd - Cache the list for 5 minutes.
            docuTechDocTypeList = new TimeBasedCacheObject<IEnumerable<DocuTechDocType>>(new TimeSpan(0, 5, 0), DocuTechDocType.ListAllImpl);
        }

        public static IEnumerable<DocuTechDocType> ListAll()
        {
            return docuTechDocTypeList.Value;
        }

        public static IEnumerable<IDocVendorDocTypeRecord> ListRecords()
        {
            return docuTechDocTypeList.Value;
        }

        public static Dictionary<int, DocuTechDocType> ListAllAsDictionary()
        {
            return docuTechDocTypeList.Value.ToDictionary(o => o.Id);
        }

        private static IEnumerable<DocuTechDocType> ListAllImpl()
        {
            List<DocuTechDocType> list = new List<DocuTechDocType>();

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "EDOCS_DOCUTECH_DOCTYPE_ListAll"))
            {
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    string description = (string)reader["Description"];
                    string barcodeClassification = (string)reader["BarcodeClassification"];

                    list.Add(new DocuTechDocType(id, description, barcodeClassification));
                }
            }

            return list;
        }

        public static int Create( string desc, string barcode)
        {
            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@Description", desc),
                new SqlParameter("@BarcodeClassification", barcode)
            };

            var result = (int)StoredProcedureHelper.ExecuteScalar(DataSrc.LOShare, "EDOCS_DOCUTECH_DOCTYPE_Insert", parameters);
            ReloadAll();

            return result;
        }

        public static void Delete(int recordId)
        {
            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@Id", recordId),
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "EDOCS_DOCUTECH_DOCTYPE_Delete", 3, parameters);
            ReloadAll();
        }

        public static void Update(int recordId, string desc, string barcode)
        {
            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@Id", recordId),
                new SqlParameter("@Description", desc),
                new SqlParameter("@BarcodeClassification", barcode)
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "EDOCS_DOCUTECH_DOCTYPE_Update", 3, parameters);
            ReloadAll();
        }
    }
}
