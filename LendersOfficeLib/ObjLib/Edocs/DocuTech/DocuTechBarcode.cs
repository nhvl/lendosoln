﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.ObjLib.Edocs;
using LendersOffice.Common;

namespace LendersOffice.Edocs.DocuTech
{

    /// <summary>
    /// Represents a DocuTech barcode of the format "|XDC=&lt;LoanNumber&gt;|XDT=&lt;DocTypeCode&gt;|".
    /// </summary>
    public class DocuTechBarcode : IDocVendorBarcode
    {
        public string sLNm { get; private set; }
        public string DocumentClass { get; private set; }
        public int PackagePageNumber { get; private set; }

        public static bool MatchesFormat(Barcode barcode)
        {
            string[] data = barcode.Data.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            return data.Length == 2;
        }

        public DocuTechBarcode(Barcode barcode)
        {
            PackagePageNumber = barcode.Page;

            string[] data = barcode.Data.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            if (data.Length != 2)
            {
                string devMsg = String.Format("Invalid data format for DocuTech barcode. Data: {0}, Page: {1}", barcode.Data, barcode.Page);
                throw new CBaseException("Unexpected barcode format", devMsg);
            }

            var fieldSplit = new char[] { '=' };
            var loanNumPieces = data[0].Split(fieldSplit);
            var docTypeCodePieces = data[1].Split(fieldSplit);
            if (loanNumPieces.Length != 2 || docTypeCodePieces.Length != 2)
            {
                string devMsg = String.Format("Invalid data format for DocuTech barcode. Data: {0}, Page: {1}", barcode.Data, barcode.Page);
                throw new CBaseException("Unexpected barcode format", devMsg);
            }

            sLNm = loanNumPieces[1].TrimWhitespaceAndBOM();
            DocumentClass = docTypeCodePieces[1].TrimWhitespaceAndBOM();
        }
    }
}
