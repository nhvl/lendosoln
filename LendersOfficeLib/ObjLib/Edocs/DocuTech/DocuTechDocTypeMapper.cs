﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using CommonProjectLib.Common.Lib;

namespace LendersOffice.ObjLib.Edocs.DocuTech
{
    public class DocuTechDocTypeMapper : BaseDocTypeMapper, IDocTypeMapper
    {
        public string VendorName
        {
            get
            {
                return "DocuTech";
            }
        }
        public DocuTechDocTypeMapper(Guid brokerId)
            : base(brokerId)
        {
        }

        public void SetAssociation(int docTypeID, IEnumerable<IDocVendorDocType> assocDocTypes)
        {
            SetAssociation(docTypeID, from t in assocDocTypes select t.Id);
        }

        public void SetAssociation(int docTypeId, IEnumerable<int> associatedDocTypes)
        {
            using (CStoredProcedureExec spExec = new CStoredProcedureExec(BrokerId))
            {
                spExec.BeginTransactionForWrite();

                spExec.ExecuteNonQuery("EDOCS_RemoveDocuTechAssociations", new SqlParameter("@BrokerId", BrokerId), new SqlParameter("@docTypeId", docTypeId));

                foreach (int docuTechDocTypeId in associatedDocTypes)
                {
                    spExec.ExecuteNonQuery("EDOCS_AssociateDocuTechDocType", new SqlParameter("@DocuTechDocTypeId", docuTechDocTypeId),
                        new SqlParameter("@BrokerId", BrokerId),
                        new SqlParameter("@DocTypeId", docTypeId));
                }
                spExec.CommitTransaction();
            }
        }

        public IEnumerable<IDocVendorDocType> GetTypesFor(int docTypeId)
        {
            List<IDocVendorDocType> docTypes = new List<IDocVendorDocType>();

            Dictionary<int, DocuTechDocType> dictionary = DocuTechDocType.ListAllAsDictionary();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId),
                                            new SqlParameter("@DocTypeId", docTypeId)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(BrokerId, "EDOCS_DOCUTECH_DOCTYPE_X_DOCTYPE_GetByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    int docuTechDocTypeId = (int)reader["DocuTechDocTypeId"];

                    DocuTechDocType docType = null;

                    if (dictionary.TryGetValue(docuTechDocTypeId, out docType))
                    {
                        docTypes.Add(docType);
                    }
                }
            }

            return docTypes;
        }
        
        public IEnumerable<IDocVendorDocType> GetDisassociatedTypes()
        {
            List<IDocVendorDocType> docTypes = new List<IDocVendorDocType>();

            Dictionary<int, DocuTechDocType> dictionary = DocuTechDocType.ListAllAsDictionary();

            HashSet<int> brokerDocuTechDocTypeSet = new HashSet<int>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(BrokerId, "EDOCS_DOCUTECH_DOCTYPE_X_DOCTYPE_GetByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    int docuTechDocTypeId = (int)reader["DocuTechDocTypeId"];

                    brokerDocuTechDocTypeSet.Add(docuTechDocTypeId);
                }
            }

            // Add all unmapped DocuTech doc types.
            foreach (var o in dictionary)
            {
                if (brokerDocuTechDocTypeSet.Contains(o.Key) == false)
                {
                    docTypes.Add(o.Value);
                }
            }
            return docTypes.Where(dt => !dt.Description.EndsWith("- OBSOLETE"));
        }
        
        public ILookup<int, IDocVendorDocType> GetBrokerDocTypeIdsWithDocVendorMappings()
        {
            List<Tuple<int, IDocVendorDocType>> docTypes = new List<Tuple<int, IDocVendorDocType>>();
            
            var dictionary = DocuTechDocType.ListAllAsDictionary();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerId, "EDOCS_DOCUTECH_DOCTYPE_X_DOCTYPE_GetByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    //int id = (int)reader["DocTypeId"];
                    int docTypeId = (int)reader["DocTypeId"];
                    int docuTechDocTypeId = (int)reader["DocuTechDocTypeId"];

                    DocuTechDocType docType = null;

                    if (dictionary.TryGetValue(docuTechDocTypeId, out docType))
                    {
                        docTypes.Add(Tuple.Create<int, IDocVendorDocType>(docTypeId, docType));
                    }
                }
            }

            return docTypes.ToLookup(p => p.Item1, p => p.Item2);
        }
        
        public IDictionary<string, int> GetVendorDocTypeIdsByDescription()
        {
            Dictionary<string, int> vendorDocTypeIdByDescription = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);

            foreach (var o in DocuTechDocType.ListAll())
            {
                vendorDocTypeIdByDescription.Add(o.Description, o.Id);
            }

            return vendorDocTypeIdByDescription;
        }

        public IDictionary<string, int> GetBrokerDocTypeIdsByBarcodeClassification()
        {
            Dictionary<string, int> brokerDocTypeIdsByBarcodeClassification = new Dictionary<string, int>();

            var dictionary = DocuTechDocType.ListAllAsDictionary();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerId, "EDOCS_DOCUTECH_DOCTYPE_X_DOCTYPE_GetByBrokerId", parameters))
            {
                while (reader.Read())
                {

                    int docTypeId = (int)reader["DocTypeId"];
                    int docuTechDocTypeId = (int)reader["DocuTechDocTypeId"];

                    DocuTechDocType docType = null;

                    if (dictionary.TryGetValue(docuTechDocTypeId, out docType))
                    {
                        brokerDocTypeIdsByBarcodeClassification.Add(docType.BarcodeClassification, docTypeId);
                    }
                }
            }

            return brokerDocTypeIdsByBarcodeClassification;
        }

        public E_DocBarcodeFormatT DocumentFormat
        {
            get { return E_DocBarcodeFormatT.DocuTech; }
        }
    }
}
