﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

class FieldPosition
{
    int m_page;
    float m_left;
    float m_top;
    public FieldPosition(int page, float left, float top)
    {
        m_page = page;
        m_left = left;
        m_top = top;
    }
    public int Page
    {
        get { return m_page; }
    }
    public float Left
    {
        get { return m_left; }
    }
    public float Top
    {
        get { return m_top; }
    }
}

/// <summary>
/// This class contains methods to add barcodes to a PDF document. The code assumes that the 
/// top-left position of the barcode is determined by the top-left position of a form field.
/// If the form field exists in multiple locations in the document, the PDFBarcodeUtil will
/// stamp the bar code image at each of those locations.
/// </summary>
class PDFBarcodeUtil
{
    public PDFBarcodeUtil()
    {
    }
    public void AddDataMatrixImage(PdfReader input, PdfStamper stamper, string fieldName, string fieldValue)
    {
        BarcodeDatamatrix codeDM = new BarcodeDatamatrix();
        codeDM.Generate(fieldValue);


        FieldPosition[] positions = ParsePositions(input.AcroFields.GetFieldPositions(fieldName));
        foreach (FieldPosition position in positions)
        {
            PdfContentByte over = stamper.GetOverContent(position.Page);
            Image imageDM = codeDM.CreateImage();
            float bottom = position.Top - imageDM.Height; // AbsolutePosition is left/bottom
            imageDM.SetAbsolutePosition(position.Left, bottom);
            over.AddImage(imageDM);
        }

        input.AcroFields.RemoveField(fieldName);
    }

    public void AddPDF417Image(PdfReader input, PdfStamper stamper, string fieldName, string fieldValue)
    {
        BarcodePDF417 codePDF = new BarcodePDF417();
        codePDF.SetText(fieldValue);

        FieldPosition[] positions = ParsePositions(input.AcroFields.GetFieldPositions(fieldName));
        foreach (FieldPosition position in positions)
        {
            PdfContentByte over = stamper.GetOverContent(position.Page);
            Image imagePDF = Image.GetInstance(codePDF.CreateDrawingImage(System.Drawing.Color.Black, System.Drawing.Color.White), BaseColor.BLACK);
            float bottom = position.Top - imagePDF.Height; // AbsolutePosition is left/bottom
            imagePDF.SetAbsolutePosition(position.Left, bottom);
            over.AddImage(imagePDF);
        }

        input.AcroFields.RemoveField(fieldName);
    }

    public void AddCode128Iimage(PdfReader input, PdfStamper stamper, string fieldName, string fieldValue)
    {
        Barcode128 code128 = new Barcode128();
        code128.Code = fieldValue;

        FieldPosition[] positions = ParsePositions(input.AcroFields.GetFieldPositions(fieldName));
        foreach (FieldPosition position in positions)
        {
            PdfContentByte over = stamper.GetOverContent(position.Page);
            Image image128 = code128.CreateImageWithBarcode(over, BaseColor.BLACK, BaseColor.BLACK);
            float bottom = position.Top - image128.Height; // AbsolutePosition is left/bottom
            image128.SetAbsolutePosition(position.Left, bottom);
            over.AddImage(image128);
        }

        input.AcroFields.RemoveField(fieldName);
    }

    public void AddCode128IimageLarge(PdfReader input, PdfStamper stamper, string fieldName, string fieldValue)
    {
        Barcode128 code128 = new Barcode128();
        code128.Code = fieldValue;
        code128.ChecksumText = false;
        code128.BarHeight = 50;
        code128.GenerateChecksum = false;
        code128.CodeType = Barcode.CODE128; 

        FieldPosition[] positions = ParsePositions(input.AcroFields.GetFieldPositions(fieldName));
        foreach (FieldPosition position in positions)
        {
            PdfContentByte over = stamper.GetOverContent(position.Page);
            Image image128 = code128.CreateImageWithBarcode(over, BaseColor.BLACK, BaseColor.BLACK);
            float bottom = position.Top - image128.Height; // AbsolutePosition is left/bottom
            image128.SetAbsolutePosition(position.Left + 200, bottom);
            over.AddImage(image128);
        }

        input.AcroFields.RemoveField(fieldName);
    }

    FieldPosition[] ParsePositions(IList<AcroFields.FieldPosition> positions)
    {
        List<FieldPosition> fs =  new List<FieldPosition>();

        for (int i = 0; i < positions.Count; i++)
        {
            fs.Add(new FieldPosition(positions[i].page, positions[i].position.Left, positions[i].position.Top));
        }
        return fs.ToArray();
    }
}