﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Common;

namespace EDocs
{
    public class InsufficientPdfPermissionException : CBaseException
    {
        public InsufficientPdfPermissionException()
            : base(ErrorMessages.EDocs.InsufficientPdfPermission, ErrorMessages.EDocs.InsufficientPdfPermission)
        {
            this.IsEmailDeveloper = false;
        }
    }
}
