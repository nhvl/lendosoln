﻿namespace LendersOffice.ObjLib.Edocs.BarcodeScanning
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using DataAccess;
    using EDocs;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using LendersOffice.Edocs.DocMagic;
    using LendersOffice.Edocs.DocuTech;

    /// <summary>
    /// A document splitter for pdf files that have a barcode on the first page of
    /// each document.
    /// </summary>
    public class BarcodeOnFirstPageOfEachDocDocumentSplitter : DocumentSplitter
    {
        /// <summary>
        /// A map from page number to the pdfs that will be split from the source
        /// file.
        /// </summary>
        private Dictionary<int, SplitPdf> documentsByPackagePageNumber;

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="BarcodeOnFirstPageOfEachDocDocumentSplitter"/> class.
        /// </summary>
        /// <param name="settings">
        /// The information associated with the initial pdf upload.
        /// </param>
        /// <param name="barcodes">The raw barcodes from the pdf.</param>
        /// <param name="format">The barcode format.</param>
        public BarcodeOnFirstPageOfEachDocDocumentSplitter(
            ISplitDocumentUploadSettings settings,
            LendersOffice.ObjLib.Edocs.Barcode[] barcodes,
            E_DocBarcodeFormatT format)
            : base(settings, barcodes, format)
        {
            this.DocTypeMapper = DocTypeMapperFactory.Create(
                format,
                settings.BrokerId);
            this.documentsByPackagePageNumber = new Dictionary<int, SplitPdf>();
        }

        /// <summary>
        /// Gets a value indicating whether at least one split pdf was associated
        /// with a loan id.
        /// </summary>
        /// <value>
        /// True if at least one split document is associated with a loan.
        /// Otherwise, false.
        /// </value>
        protected override bool AbleToDetermineLoanIdFromBarcodes
        {
            get
            {
                return this.documentsByPackagePageNumber.Values
                    .Any(s => s.LoanId != Guid.Empty);
            }
        }

        /// <summary>
        /// Groups the barcodes into separate documents.
        /// </summary>
        protected override void GroupDocumentsByBarcode()
        {
            SplitPdf splitDoc = null;

            foreach (var rawBarcode in Barcodes.OrderBy(p => p.Page))
            {
                var barcode = new DocuTechBarcode(rawBarcode);
                var loanIdAppId = this.GetLoanIdAppIdForBarcode(barcode, splitDoc);
                splitDoc = new SplitPdf(loanIdAppId, barcode);

                try
                {
                    this.documentsByPackagePageNumber.Add(
                        barcode.PackagePageNumber,
                        splitDoc);
                }
                catch (ArgumentException e)
                {
                    var msg = "Two barcodes associated with the same package " +
                        "page number.";
                    Tools.LogError(msg, e);
                }
            }
        }

        /// <summary>
        /// Gets an enumerable of documents that still need to be mapped to 
        /// LendingQB document types.
        /// </summary>
        /// <param name="packagePdf">The pdf reader.</param>
        /// <returns>
        /// An enumerable of documents that still need to be mapped to LendingQB
        /// document types.
        /// </returns>
        protected IEnumerable<UnmappedDocumentUpload> GetDocumentsForMapping(PdfReader packagePdf)
        {
            var splitPages = new bool[packagePdf.NumberOfPages];

            var documentsToMap = new List<UnmappedDocumentUpload>();

            foreach (int packagePageNumber in this.documentsByPackagePageNumber.Keys)
            {
                var splitDocPath = TempFileUtils.NewTempFilePath();
                var document = new Document();

                using (FileStream fs = File.OpenWrite(splitDocPath))
                {
                    var pdfCopier = new PdfCopy(document, fs);
                    document.Open();

                    pdfCopier.AddPage(
                        pdfCopier.GetImportedPage(packagePdf, packagePageNumber));
                    splitPages[packagePageNumber - 1] = true;

                    // Add the rest of the document. We assume that all of the pages 
                    // between barcodes are associated with the previous document/barcode.
                    int pageNum = packagePageNumber + 1;
                    while (!this.documentsByPackagePageNumber.ContainsKey(pageNum) &&
                        pageNum <= packagePdf.NumberOfPages)
                    {
                        pdfCopier.AddPage(
                            pdfCopier.GetImportedPage(packagePdf, pageNum));
                        splitPages[pageNum - 1] = true;
                        pageNum++;
                    }

                    pdfCopier.Close();
                    document.Close();
                }

                var splitPdf = this.documentsByPackagePageNumber[packagePageNumber];
                var upload = new UnmappedDocumentUpload(
                    splitPdf.LoanId,
                    splitPdf.AppId,
                    splitDocPath,
                    splitPdf.DocumentClass);
                documentsToMap.Add(upload);
            }

            if (splitPages.Any(p => !p))
            {
                var upload = this.GetDocumentForPagesWithoutBarcodes(
                    packagePdf,
                    splitPages);
                documentsToMap.Add(upload);
            }

            packagePdf.Close();

            return documentsToMap;
        }

        /// <summary>
        /// Gets all of the split documents that need to be uploaded.
        /// </summary>
        /// <param name="pdfReader">The pdf reader.</param>
        /// <returns>An enumerable of documents that need to be uploaded.</returns>
        protected override IList<DocumentUpload> GetDocumentsForUpload(PdfReader pdfReader)
        {
            var unmappedDocuments = this.GetDocumentsForMapping(pdfReader);
            return this.MapDocuments(unmappedDocuments);
        }
    }
}
