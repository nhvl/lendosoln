﻿namespace LendersOffice.ObjLib.Edocs.BarcodeScanning
{
    using System;
    using DataAccess;

    public interface ISplitDocumentUploadSettings
    {
        Guid BrokerId { get; }
        Guid LoanId { get; }
        Guid AppId { get; }
        Guid UploadingUserId { get; }
        Guid FileDBKey { get; }
        E_FileDB FileDB { get; }
        int PageCount { get; }
        string Description { get; }
        string InternalNotes { get; }
        bool isDropboxFile { get; }
        bool IsBarcodeUpload { get; }

        bool MustDetermineLoanIdFromBarcodes();

    }
}
