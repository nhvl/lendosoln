﻿namespace LendersOffice.ObjLib.Edocs.BarcodeScanning
{
    using System.Collections.Generic;

    /// <summary>
    /// The result of splitting a document.
    /// </summary>
    public class DocumentSplitResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentSplitResult"/>
        /// class.
        /// </summary>
        /// <param name="uploadSourceFileToDropbox">
        /// A value indicating whether the source file should be uploaded to the
        /// EDoc $$Dropbox$$.
        /// </param>
        /// <param name="splitDocuments">
        /// The documents that were split from the source file.
        /// </param>
        private DocumentSplitResult(
            bool uploadSourceFileToDropbox,
            IEnumerable<DocumentUpload> splitDocuments)
        {
            this.UploadSourceFileToDropbox = uploadSourceFileToDropbox;
            this.SplitDocuments = splitDocuments;
        }

        /// <summary>
        /// Gets a value indicating whether the original file should just be 
        /// uploaded to the EDoc $$Dropbox$$.
        /// </summary>
        /// <value>
        /// True if the source file should be uploaded to the EDoc Dropbox.
        /// Otherwise, false.
        /// </value>
        public bool UploadSourceFileToDropbox { get; private set; }

        /// <summary>
        /// Gets the documents that were split from the source file.
        /// </summary>
        /// <value>
        /// An enumerable of the documents that should be uploaded. Null if
        /// source file should be uploaded to EDoc $$Dropbox$$.
        /// </value>
        public IEnumerable<DocumentUpload> SplitDocuments { get; private set; }

        /// <summary>
        /// Creates a new DocumentSplitResult indicating the source file should
        /// be uploaded to the EDoc $$Dropbox$$ and no documents were split
        /// from the source file.
        /// </summary>
        /// <returns>
        /// A new DocumentSplitReuslt indicating the source file should be 
        /// uploaded to the EDoc $$Dropbox$$.
        /// </returns>
        public static DocumentSplitResult UploadSourceFileToDropboxResult()
        {
            return new DocumentSplitResult(true, null);
        }

        /// <summary>
        /// Creates a new DocumentSplitResult with the documents.
        /// </summary>
        /// <param name="documents">
        /// The documents that were split from the source document.
        /// </param>
        /// <returns>
        /// A new DocumentSplitResult containing the given documents.
        /// </returns>
        public static DocumentSplitResult UploadSplitDocumentsResult(
            IEnumerable<DocumentUpload> documents)
        {
            return new DocumentSplitResult(false, documents);
        }
    }
}
