﻿namespace LendersOffice.ObjLib.Edocs.BarcodeScanning
{
    using System;

    /// <summary>
    /// Represents a split document that still needs to be mapped to use
    /// LendingQB document type ids.
    /// </summary>
    public class UnmappedDocumentUpload
    {
        /// <summary>
        /// The id of the loan where this document should be uploaded.
        /// </summary>
        public readonly Guid LoanId;

        /// <summary>
        /// The id of the application where this document should be uploaded.
        /// </summary>
        public readonly Guid AppId;

        /// <summary>
        /// The path to the pdf.
        /// </summary>
        public readonly string PdfPath;

        /// <summary>
        /// The document vendor document class.
        /// </summary>
        public readonly string DocVendorDocumentClass;

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="UnmappedDocumentUpload"/> class.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="appId">The app id.</param>
        /// <param name="path">The path to the pdf.</param>
        /// <param name="documentClass">The document class from the document vendor.</param>
        public UnmappedDocumentUpload(Guid loanId, Guid appId, string path, string documentClass)
        {
            this.LoanId = loanId;
            this.AppId = appId;
            this.PdfPath = path;
            this.DocVendorDocumentClass = documentClass;
        }
    }
}
