﻿namespace LendersOffice.ObjLib.Edocs.BarcodeScanning
{
    using DataAccess;
    using LendersOffice.Edocs.DocMagic;

    /// <summary>
    /// Factory for creating document splitters.
    /// </summary>
    public class DocumentSplitterFactory
    {
        /// <summary>
        /// Creates a document splitter for the given barcode format.
        /// </summary>
        /// <param name="settings">The settings from the split document request.</param>
        /// <param name="barcodes">The barcodes from the document.</param>
        /// <param name="format">The format of the barcodes.</param>
        /// <returns>An IDocumentSplitter to split the document.</returns>
        public static DocumentSplitter GetDocumentSplitter(
            ISplitDocumentUploadSettings settings,
            LendersOffice.ObjLib.Edocs.Barcode[] barcodes,
            E_DocBarcodeFormatT format)
        {
            DocumentSplitter splitter = null;

            switch (format)
            {
                case E_DocBarcodeFormatT.LendingQB:
                    splitter = new LendingQBBarcodeDocumentSplitter(
                        settings,
                        barcodes);
                    break;
                case E_DocBarcodeFormatT.DocMagic:
                case E_DocBarcodeFormatT.IDS:
                    splitter = new BarcodeOnEveryPageDocumentSplitter(
                        settings,
                        barcodes,
                        format);
                    break;
                case E_DocBarcodeFormatT.DocuTech:
                    splitter = new BarcodeOnFirstPageOfEachDocDocumentSplitter(
                        settings,
                        barcodes,
                        format);
                    break;
                default:
                    throw new UnhandledEnumException(format);
            }

            return splitter;
        }
    }
}
