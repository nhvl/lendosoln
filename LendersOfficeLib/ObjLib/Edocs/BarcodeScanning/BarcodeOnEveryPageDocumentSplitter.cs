﻿namespace LendersOffice.ObjLib.Edocs.BarcodeScanning
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using EDocs;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Edocs.DocMagic;
    using LendersOffice.ObjLib.Edocs.IDS;

    /// <summary>
    /// A document splitter for pdf files that have a barcode on every page.
    /// </summary>
    public class BarcodeOnEveryPageDocumentSplitter : DocumentSplitter
    {
        /// <summary>
        /// A list of pdf files that will be split from the source file.
        /// </summary>
        private List<SplitPdf> splitDocuments;

        /// <summary>
        /// Gets a doc vendor barcode from a basic barcode.
        /// </summary>
        private Func<LendersOffice.ObjLib.Edocs.Barcode,
            IDocVendorBarcodeWithDocPageNum> createDocVendorBarcode;

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="BarcodeOnEveryPageDocumentSplitter"/> class.
        /// </summary>
        /// <param name="settings">
        /// The information associated with the initial pdf upload.
        /// </param>
        /// <param name="barcodes">The raw barcodes from the pdf.</param>
        /// <param name="format">The barcode format.</param>
        public BarcodeOnEveryPageDocumentSplitter(
            ISplitDocumentUploadSettings settings,
            LendersOffice.ObjLib.Edocs.Barcode[] barcodes,
            E_DocBarcodeFormatT format)
            : base(settings, barcodes, format)
        {
            this.DocTypeMapper = DocTypeMapperFactory.Create(
                format,
                settings.BrokerId);

            switch (format)
            {
                case E_DocBarcodeFormatT.DocMagic:
                    this.createDocVendorBarcode = (b) => new DocMagicBarcode(b);
                    break;
                case E_DocBarcodeFormatT.IDS:
                    this.createDocVendorBarcode = (b) => new IDSBarcode(b);
                    break;
                default:
                    throw new UnhandledEnumException(format);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance was able to associate
        /// at least one of the split pdfs with a loan id.
        /// </summary>
        /// <value>
        /// Returns true if any of the split documents are associated with a
        /// loan. Otherwise, false.
        /// </value>
        protected override bool AbleToDetermineLoanIdFromBarcodes
        {
            get
            {
                return this.splitDocuments.Any(s => s.LoanId != Guid.Empty);
            }
        }

        /// <summary>
        /// Groups the barcodes into separate documents.
        /// </summary>
        protected override void GroupDocumentsByBarcode()
        {
            this.splitDocuments = new List<SplitPdf>();
            SplitPdf splitDoc = null;

            foreach (var rawBarcode in this.Barcodes.OrderBy(p => p.Page))
            {
                var barcode = this.createDocVendorBarcode(rawBarcode);
                var loanIdAppIdForBarcode = this.GetLoanIdAppIdForBarcode(
                    barcode,
                    splitDoc);
                bool appendBarcode = this.ShouldAppendBarcode(
                    splitDoc,
                    loanIdAppIdForBarcode.LoanId,
                    barcode);

                if (appendBarcode)
                {
                    splitDoc.Add(barcode);
                }
                else
                {
                    splitDoc = new SplitPdf(loanIdAppIdForBarcode, barcode);
                    this.splitDocuments.Add(splitDoc);
                }
            }

            foreach (var splitPdf in this.splitDocuments)
            {
                splitPdf.SortBarcodesByPackagePageNumber();
            }
        }

        /// <summary>
        /// Gets all of the split documents that need to be uploaded.
        /// </summary>
        /// <param name="pdfReader">The pdf reader.</param>
        /// <returns>An enumerable of documents that need to be uploaded.</returns>
        protected override IList<DocumentUpload> GetDocumentsForUpload(PdfReader pdfReader)
        {
            var unmappedDocuments = this.GetDocumentsForMapping(pdfReader);
            return this.MapDocuments(unmappedDocuments);
        }

        /// <summary>
        /// Gets an enumerable of documents that still need to be mapped to 
        /// LendingQB document types.
        /// </summary>
        /// <param name="packagePdf">The pdf reader.</param>
        /// <returns>
        /// An enumerable of documents that still need to be mapped to LendingQB
        /// document types.
        /// </returns>
        protected IEnumerable<UnmappedDocumentUpload> GetDocumentsForMapping(PdfReader packagePdf)
        {
            var splitPages = new bool[packagePdf.NumberOfPages];

            var documentsForMapping = new List<UnmappedDocumentUpload>();

            foreach (var splitPdf in this.splitDocuments)
            {
                var splitDocPath = TempFileUtils.NewTempFilePath();
                var document = new Document();

                using (FileStream fs = File.OpenWrite(splitDocPath))
                {
                    var pdfCopier = new PdfCopy(document, fs);
                    document.Open();

                    foreach (IDocVendorBarcode barcode in splitPdf.Barcodes) 
                    {
                        var importedPage = pdfCopier.GetImportedPage(
                            packagePdf,
                            barcode.PackagePageNumber);
                        pdfCopier.AddPage(importedPage);
                        splitPages[barcode.PackagePageNumber - 1] = true;
                    }

                    pdfCopier.Close();
                    document.Close();
                }

                var upload = new UnmappedDocumentUpload(
                    splitPdf.LoanId,
                    splitPdf.AppId,
                    splitDocPath,
                    splitPdf.DocumentClass);
                documentsForMapping.Add(upload);
            }

            if (splitPages.Any(p => !p))
            {
                var upload = this.GetDocumentForPagesWithoutBarcodes(
                    packagePdf,
                    splitPages);
                documentsForMapping.Add(upload);
            }

            packagePdf.Close();

            if (ConstStage.LogSplitDocumentInfoInBarcodeSplitter)
            {
                this.LogUnmappedDocumentInfo(documentsForMapping, splitPages);
            }

            return documentsForMapping;
        }

        /// <summary>
        /// Gets a value indicating whether the barcode should be associated
        /// with the split pdf.
        /// </summary>
        /// <param name="currentDoc">The document to check for appending.</param>
        /// <param name="loanIdForBarcode">
        /// The loan id for the barcode that may be appended.
        /// </param>
        /// <param name="currentBarcode">The barcode that may be appended.</param>
        /// <returns>
        /// Returns true if currentBarcode should be appended to currentDoc.
        /// Otherwise, false.
        /// </returns>
        private bool ShouldAppendBarcode(
            SplitPdf currentDoc,
            Guid loanIdForBarcode,
            IDocVendorBarcodeWithDocPageNum currentBarcode)
        {
            return currentDoc != null &&
                currentDoc.DocumentClass == currentBarcode.DocumentClass &&
                currentBarcode.DocumentPageNumber != 1 &&
                currentDoc.LoanId == loanIdForBarcode;
        }

        /// <summary>
        /// Logs information about the unmapped uploads to help debug.
        /// </summary>
        /// <param name="unmappedUploads">
        /// The uploads that will be mapped in GetDocumentsForUpload.
        /// </param>
        /// <param name="splitPages">
        /// The bit array of pages that were split out of the source document.
        /// </param>
        private void LogUnmappedDocumentInfo(IEnumerable<UnmappedDocumentUpload> unmappedUploads, bool[] splitPages)
        {
            if (unmappedUploads == null || splitPages == null)
            {
                throw new ArgumentNullException();
            }

            var sb = new StringBuilder("[DocumentSplitter] Uploads to map:" + Environment.NewLine);

            foreach (var upload in unmappedUploads)
            {
                sb.AppendLine(
                    "LoanId: " + upload.LoanId.ToString() + 
                    ", DocClass: " + (upload.DocVendorDocumentClass ?? "NULL"));
            }

            sb.AppendLine();

            var indicesWithoutBarcodes = splitPages.Select((pageHadBarcode, index) => new { index, pageHadBarcode })
                .Where(anon => !anon.pageHadBarcode)
                .Select(anon => anon.index + 1);
            sb.AppendLine("Indices without barcodes: " + string.Join(", ", indicesWithoutBarcodes));

            Tools.LogInfo(sb.ToString());
        }
    }
}
