﻿namespace LendersOffice.ObjLib.Edocs.BarcodeScanning
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Represents a pdf that will be split from a source pdf based on barcodes.
    /// </summary>
    public class SplitPdf
    {
        /// <summary>
        /// The loan id for this document.
        /// </summary>
        public readonly Guid LoanId;

        /// <summary>
        /// The application id for this document.
        /// </summary>
        public readonly Guid AppId;

        /// <summary>
        /// The document vendor document class for this document.
        /// </summary>
        public readonly string DocumentClass;

        /// <summary>
        /// The loan number.
        /// </summary>
        public readonly string LoanNumber;

        /// <summary>
        /// The barcodes associated with this document.
        /// </summary>
        private List<IDocVendorBarcode> barcodes;

        /// <summary>
        /// Initializes a new instance of the <see cref="SplitPdf"/> class.
        /// </summary>
        /// <param name="loanIdAppId">
        /// The loan id and app id associated with the document.
        /// </param>
        /// <param name="barcode">The first barcode for the document.</param>
        public SplitPdf(LoanIdAppId loanIdAppId, IDocVendorBarcode barcode)
        {
            this.LoanId = loanIdAppId.LoanId;
            this.AppId = loanIdAppId.AppId;
            this.DocumentClass = barcode.DocumentClass;
            this.LoanNumber = barcode.sLNm;
            this.barcodes = new List<IDocVendorBarcode>();
            this.Add(barcode);
        }

        /// <summary>
        /// Gets an enumerable of the barcodes for this document.
        /// </summary>
        /// <value>An enumerable of the barcodes for this document.</value>
        public IEnumerable<IDocVendorBarcode> Barcodes
        {
            get { return this.barcodes; }
        }

        /// <summary>
        /// Adds a barcode for this document.
        /// </summary>
        /// <param name="barcode">The barcode to add.</param>
        public void Add(IDocVendorBarcode barcode)
        {
            this.barcodes.Add(barcode);
        }

        /// <summary>
        /// Sorts the barcodes based on page number.
        /// </summary>
        public void SortBarcodesByPackagePageNumber()
        {
            this.barcodes = this.barcodes
                .OrderBy(b => b.PackagePageNumber)
                .ToList();
        }
    }
}
