﻿namespace LendersOffice.ObjLib.Edocs.BarcodeScanning
{
    using System;
    using LendersOffice.Edocs.DocMagic;

    /// <summary>
    /// Contains all of the information required to create an EDoc.
    /// </summary>
    public class DocumentUpload
    {
        /// <summary>
        /// The broker id for the document.
        /// </summary>
        public readonly Guid BrokerId;

        /// <summary>
        /// The loan id to associate the document with.
        /// </summary>
        public readonly Guid LoanId;

        /// <summary>
        /// The app id to associate the document with.
        /// </summary>
        public readonly Guid AppId;

        /// <summary>
        /// The path of the pdf.
        /// </summary>
        public readonly string PdfPath;

        /// <summary>
        /// The description of the document.
        /// </summary>
        public readonly string Description;

        /// <summary>
        /// The internal description for the document.
        /// </summary>
        public readonly string InternalDescription;

        /// <summary>
        /// The id of the user that uploaded the document.
        /// </summary>
        public readonly Guid UploadingUserId;

        /// <summary>
        /// The LendingQB document type id for the document.
        /// </summary>
        public readonly int DocTypeId;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentUpload"/> class.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="appId">The app id.</param>
        /// <param name="path">The path of the pdf.</param>
        /// <param name="docTypeId">The LendingQB document type id.</param>
        /// <param name="splitSettings">
        /// The settings from the document split request.
        /// </param>
        public DocumentUpload(Guid loanId, Guid appId, string path, int docTypeId, ISplitDocumentUploadSettings splitSettings)
        {
            this.LoanId = loanId;
            this.AppId = appId;
            this.PdfPath = path;
            this.DocTypeId = docTypeId;
            this.BrokerId = splitSettings.BrokerId;
            this.Description = splitSettings.Description;
            this.InternalDescription = splitSettings.InternalNotes;
            this.UploadingUserId = splitSettings.UploadingUserId;
        }
    }
}
