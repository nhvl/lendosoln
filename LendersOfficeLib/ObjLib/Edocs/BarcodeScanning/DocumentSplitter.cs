﻿namespace LendersOffice.ObjLib.Edocs.BarcodeScanning
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using Admin;
    using DataAccess;
    using EDocs;
    using iTextSharp.text;
    using iTextSharp.text.pdf;

    /// <summary>
    /// An immutable loan id, app id pair.
    /// </summary>
    public struct LoanIdAppId
    {
        /// <summary>
        /// The loan id.
        /// </summary>
        public readonly Guid LoanId;

        /// <summary>
        /// The app id.
        /// </summary>
        public readonly Guid AppId;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanIdAppId"/> struct.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="appId">The app id.</param>
        public LoanIdAppId(Guid loanId, Guid appId)
        {
            this.LoanId = loanId;
            this.AppId = appId;
        }
    }

    /// <summary>
    /// An abstract class that provides a template for splitting pdf documents
    /// based on barcodes.
    /// </summary>
    public abstract class DocumentSplitter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentSplitter"/> class.
        /// </summary>
        /// <param name="settings">
        /// The information associated with the initial pdf upload.
        /// </param>
        /// <param name="barcodes">The raw barcodes from the pdf.</param>
        /// <param name="format">The barcode format.</param>
        public DocumentSplitter(
            ISplitDocumentUploadSettings settings,
            LendersOffice.ObjLib.Edocs.Barcode[] barcodes,
            E_DocBarcodeFormatT format)
        {
            this.Settings = settings;
            this.Barcodes = barcodes;
            this.BarcodeFormat = format;
        }

        /// <summary>
        /// Gets the settings provided when the document split was requested.
        /// </summary>
        /// <value>
        /// The settings provided when the document split was requested.
        /// </value>
        protected ISplitDocumentUploadSettings Settings { get; private set; }

        /// <summary>
        /// Gets the raw barcodes associated with the document.
        /// </summary>
        /// <value>The raw barcodes associated with the document.</value>
        protected LendersOffice.ObjLib.Edocs.Barcode[] Barcodes { get; private set; }

        /// <summary>
        /// Gets the format of the barcodes.
        /// </summary>
        /// <value>The format of the barcodes.</value>
        protected E_DocBarcodeFormatT BarcodeFormat { get; private set; }

        /// <summary>
        /// Gets or sets the mapper that will allow converting document vendor
        /// document classifications into LendingQB document types.
        /// </summary>
        /// <value>
        /// If the pdf does not require an intermediate mapping, null.
        /// </value>
        protected IDocTypeMapper DocTypeMapper { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance was able to determine
        /// the loan id(s) from the associated barcodes.
        /// </summary>
        /// <value>
        /// Returns true if able to determine the loan id(s). Otherwise, false.
        /// </value>
        protected abstract bool AbleToDetermineLoanIdFromBarcodes { get; }

        /// <summary>
        /// Splits a pdf into separate documents that are mapped to LendingQB
        /// document types.
        /// </summary>
        /// <returns>
        /// The result of the document split. If the no loan id could be
        /// determined, the result will indicate that the file needs to be
        /// uploaded to the EDocs $$Dropbox$$. Otherwise, it will contain an
        /// enumerable of the different documents that are ready to be loaded
        /// into EDocs.
        /// </returns>
        public DocumentSplitResult SplitDocument()
        {
            if (this.Barcodes.Length == 0)
            {
                var msg = "Cannot split a document that does not contain any " +
                    "barcodes.";
                throw new InvalidOperationException(msg);
            }

            this.GroupDocumentsByBarcode();

            DocumentSplitResult result;

            if (this.ShouldUploadFileToDropbox())
            {
                result = DocumentSplitResult.UploadSourceFileToDropboxResult();
            }
            else
            {
                var pdfPath = FileDBTools.CreateCopy(
                    this.Settings.FileDB,
                    this.Settings.FileDBKey.ToString());
                var pdfReader = EDocumentViewer.CreatePdfReader(pdfPath);

                var documents = this.GetDocumentsForUpload(pdfReader);

                // The barcode splitters write a new pdf which will not include the
                // signatures of the original, so we always want to upload the original
                // if it is esigned.
                if (EDocument.ContainsSignature(pdfReader))
                {
                    var loanIdAppId = this.GetLoanIdAppIdForESignedOriginal(documents);
                    int? docTypeId = BrokerDB.GetESignedDocumentDocTypeId(this.Settings.BrokerId);
                    if (docTypeId == null)
                    {
                        docTypeId = EDocumentDocType.GetOrCreateDocType(
                            this.Settings.BrokerId,
                            EDocumentDocType.ESIGNED_UPLOAD_DOCTYPE,
                            EDocumentFolder.GENERATED_FOLDERNAME);
                    }

                    var originalESignedDoc = new DocumentUpload(
                        loanIdAppId.LoanId,
                        loanIdAppId.AppId,
                        pdfPath,
                        docTypeId.Value,
                        this.Settings);

                    documents.Add(originalESignedDoc);
                }

                result = DocumentSplitResult.UploadSplitDocumentsResult(documents);
            }

            return result;
        }

        /// <summary>
        /// Groups the barcodes into separate documents.
        /// </summary>
        protected abstract void GroupDocumentsByBarcode();

        /// <summary>
        /// Gets an enumerable of the separate documents that were split out of
        /// the original pdf.
        /// </summary>
        /// <param name="pdfReader">The pdf reader.</param>
        /// <returns>
        /// An enumerable of the separate documents that were split out of the
        /// original pdf.
        /// </returns>
        protected abstract IList<DocumentUpload> GetDocumentsForUpload(PdfReader pdfReader);

        /// <summary>
        /// Determines whether the file should be uploaded to the EDocs $$Dropbox$$.
        /// </summary>
        /// <returns>
        /// If the file was uploaded to a single loan, this will always return
        /// false. Otherwise, this will return true if no loan id could be
        /// determined from the barcodes.
        /// </returns>
        protected bool ShouldUploadFileToDropbox()
        {
            bool shouldUploadToDropbox = false;

            if (this.Settings.MustDetermineLoanIdFromBarcodes())
            {
                shouldUploadToDropbox = !this.AbleToDetermineLoanIdFromBarcodes;
            }

            return shouldUploadToDropbox;
        }

        /// <summary>
        /// Gets the primary application id for a given loan id.
        /// </summary>
        /// <param name="brokerId">The broker id for the loan.</param>
        /// <param name="loanId">The loan id.</param>
        /// <returns>The primary application id for a given loan id.</returns>
        protected Guid GetPrimaryAppIdFromLoanId(Guid brokerId, Guid loanId)
        {
            var sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter("@LoanId", loanId));

            using (var reader = StoredProcedureHelper.ExecuteReader(
                brokerId,
                "App_GetPrimaryAppIdFromLoanId",
                sqlParameters))
            {
                if (reader.Read())
                {
                    return (Guid)reader["aAppId"];
                }
            }

            return Guid.Empty;
        }

        /// <summary>
        /// Gets a document for all of the pages that did not have a barcode
        /// and were not grouped into separate documents.
        /// </summary>
        /// <param name="packagePdf">The pdf reader for the document.</param>
        /// <param name="splitPages">
        /// An array indicating which pages were already included in split
        /// documents.
        /// </param>
        /// <returns>
        /// A document to upload for all the pages that did not have a barcode
        /// and were not grouped into separate documents.
        /// </returns>
        protected UnmappedDocumentUpload GetDocumentForPagesWithoutBarcodes(
            PdfReader packagePdf,
            bool[] splitPages)
        {
            if (this.BarcodeFormat == E_DocBarcodeFormatT.LendingQB)
            {
                var msg = "DocumentSplitter.GetDocumentForPages() does not " +
                    "support LendingQB barcodes.";
                throw new InvalidOperationException(msg);
            }
            else if (splitPages.All(p => p))
            {
                var msg = "Cannot create document for pages without barcodes " +
                    "when all pages had barcodes and were already split into " +
                    "separate pdfs.";
                throw new InvalidOperationException(msg);
            }
            else
            {
                var unreadablePdfPath = TempFileUtils.NewTempFilePath();
                using (FileStream fs = File.OpenWrite(unreadablePdfPath))
                {
                    var document = new Document();
                    var pdfCopier = new PdfCopy(document, fs);
                    document.Open();

                    for (int i = 0; i < splitPages.Length; i++)
                    {
                        if (splitPages[i])
                        {
                            continue;
                        }

                        pdfCopier.AddPage(
                            pdfCopier.GetImportedPage(packagePdf, i + 1));
                    }

                    pdfCopier.Close();
                    document.Close();
                }

                // We can't determine the loan id from a barcode, so we must use
                // the loan id that was initially supplied.
                return new UnmappedDocumentUpload(
                    this.Settings.LoanId,
                    this.Settings.AppId,
                    unreadablePdfPath,
                    documentClass: null);
            }
        }

        /// <summary>
        /// Maps uploads with document vendor doc types to uploads with LendingQB
        /// doc types.
        /// </summary>
        /// <param name="uploadsToMap">The uploads to map.</param>
        /// <returns>An enumerable of documents to upload.</returns>
        protected IList<DocumentUpload> MapDocuments(
            IEnumerable<UnmappedDocumentUpload> uploadsToMap)
        {
            if (this.BarcodeFormat == E_DocBarcodeFormatT.LendingQB)
            {
                throw new InvalidOperationException(
                    "LendingQB barcodes do not need to be mapped.");
            }

            var uploads = new List<DocumentUpload>();

            int unmappedDocTypeId;
            int unreadableDocTypeId;
            this.DocTypeMapper.GetUnassignedDocTypeAndUnreadableDocType(
                out unmappedDocTypeId,
                out unreadableDocTypeId);
            IDictionary<string, int> docTypes = 
                this.DocTypeMapper.GetBrokerDocTypeIdsByBarcodeClassification();
            
            foreach (var unmappedUpload in uploadsToMap)
            {
                int docTypeId;

                if (unmappedUpload.DocVendorDocumentClass == null)
                {
                    docTypeId = unreadableDocTypeId;
                }
                else if (!docTypes.TryGetValue(
                    unmappedUpload.DocVendorDocumentClass,
                    out docTypeId))
                {
                    docTypeId = unmappedDocTypeId;
                }

                var mappedUpload = new DocumentUpload(
                    unmappedUpload.LoanId,
                    unmappedUpload.AppId,
                    unmappedUpload.PdfPath,
                    docTypeId,
                    this.Settings);

                uploads.Add(mappedUpload);
            }

            return uploads;
        }

        /// <summary>
        /// Determines the loan id and app id to use for a barcode.
        /// </summary>
        /// <param name="barcode">
        /// The barcode to determine the loan id and app id for.
        /// </param>
        /// <param name="splitPdfForPreviousBarcode">
        /// The split pdf associated with the previous barcode.
        /// </param>
        /// <returns>The loan id and app id for the barcode.</returns>
        protected LoanIdAppId GetLoanIdAppIdForBarcode(
            IDocVendorBarcode barcode,
            SplitPdf splitPdfForPreviousBarcode)
        {
            if (this.BarcodeFormat == E_DocBarcodeFormatT.LendingQB)
            {
                var msg = "GetLoanIdApIdForBarcode does not support " +
                    "LendingQB barcode format";
                throw new InvalidOperationException(msg);
            }

            Guid loanId = Guid.Empty;
            Guid appId = Guid.Empty;

            if (this.Settings.LoanId != Guid.Empty)
            {
                loanId = this.Settings.LoanId;
                appId = this.Settings.AppId;
            }
            else if (splitPdfForPreviousBarcode != null &&
                splitPdfForPreviousBarcode.LoanNumber == barcode.sLNm)
            {
                loanId = splitPdfForPreviousBarcode.LoanId;
                appId = splitPdfForPreviousBarcode.AppId;
            }
            else
            {
                loanId = Tools.GetLoanIdByLoanName(
                    this.Settings.BrokerId,
                    barcode.sLNm);

                if (loanId != Guid.Empty)
                {
                    appId = this.GetPrimaryAppIdFromLoanId(
                        this.Settings.BrokerId,
                        loanId);
                }
            }

            return new LoanIdAppId(loanId, appId);
        }

        /// <summary>
        /// Gets the loan id and app id for an e-signed document based on the
        /// split documents that will be uploaded. If all of the entries are
        /// not associated with the same loan and application the returned
        /// loan id and app id will be empty.
        /// </summary>
        /// <param name="documents">The documents that will be uploaded.</param>
        /// <returns>
        /// The loan id and app id associated with all document uploads if it is
        /// the same. Otherwise, both the loan id and app id will be Guid.Empty.
        /// </returns>
        private LoanIdAppId GetLoanIdAppIdForESignedOriginal(
            IList<DocumentUpload> documents)
        {
            if (documents.Count == 0)
            {
                return new LoanIdAppId(Guid.Empty, Guid.Empty);
            }

            var firstLoanId = documents[0].LoanId;
            var firstAppId = documents[0].AppId;

            if (documents.All(d => d.LoanId == firstLoanId && d.AppId == firstAppId))
            {
                return new LoanIdAppId(firstLoanId, firstAppId);
            }
            else
            {
                return new LoanIdAppId(Guid.Empty, Guid.Empty);
            }
        }
    }
}
