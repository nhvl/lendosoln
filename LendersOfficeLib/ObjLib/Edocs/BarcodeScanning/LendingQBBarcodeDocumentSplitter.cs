﻿namespace LendersOffice.ObjLib.Edocs.BarcodeScanning
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using global::CommonProjectLib.Common.Lib;
    using DataAccess;
    using EDocs;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using LendersOffice.Edocs.DocMagic;

    /// <summary>
    /// A document splitter for pdf files with LendingQB barcodes.
    /// </summary>
    public class LendingQBBarcodeDocumentSplitter : DocumentSplitter
    {
        /// <summary>
        /// A list of page number, barcode pairs.
        /// </summary>
        private List<Tuple<int, EdocFaxCover.FaxCoverData>> pagesWithBarcodes;

        /// <summary>
        /// The loan id and app id associated with the documents.
        /// </summary>
        private LoanIdAppId? loanIdAppId;

        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="LendingQBBarcodeDocumentSplitter" /> class.
        /// </summary>
        /// <param name="settings">
        /// The settings for the split document request.
        /// </param>
        /// <param name="barcodes">The barcodes from the document.</param>
        public LendingQBBarcodeDocumentSplitter(
            ISplitDocumentUploadSettings settings,
            LendersOffice.ObjLib.Edocs.Barcode[] barcodes)
            : base(settings, barcodes, E_DocBarcodeFormatT.LendingQB)
        {
            this.pagesWithBarcodes = new List<Tuple<int, EdocFaxCover.FaxCoverData>>();
        }

        /// <summary>
        /// Gets a value indicating whether a loan id was determined for the
        /// split documents.
        /// </summary>
        /// <value>
        /// Returns true if a loan id was determined. Otherwise, false.
        /// </value>
        protected override bool AbleToDetermineLoanIdFromBarcodes
        {
            get
            {
                return this.loanIdAppId.Value.LoanId != Guid.Empty;
            }
        }

        /// <summary>
        /// Groups the barcodes into documents.
        /// </summary>
        protected override void GroupDocumentsByBarcode()
        {
            foreach (var rawBarcode in this.Barcodes)
            {
                var barcode = this.GetLendingQBBarcode(rawBarcode);

                if (barcode != null)
                {
                    this.pagesWithBarcodes.Add(
                        Tuple.Create(rawBarcode.Page, barcode));
                }
            }

            this.pagesWithBarcodes = this.pagesWithBarcodes
                .OrderBy(p => p.Item1)
                .ToList();
            this.loanIdAppId = this.GetLoanIdAppIdForSplitDocuments();
        }

        /// <summary>
        /// Splits the original pdf into separate pdf files that are ready to be uploaded.
        /// </summary>
        /// <param name="packagePdf">The pdf reader.</param>
        /// <returns>An enumerable of the documents to upload to EDocs.</returns>
        protected override IList<DocumentUpload> GetDocumentsForUpload(PdfReader packagePdf)
        {
            this.RemoveEntriesWithInvalidDocTypeIds();

            this.VerifyEntryLoanIdAppIds();

            if (this.pagesWithBarcodes.Count == 0 ||
                this.pagesWithBarcodes[0].Item1 != 1)
            {
                this.PrependEntryForUnreadableBarcodes();
            }

            var documentsToUpload = new List<DocumentUpload>();

            int lastPage = packagePdf.NumberOfPages;

            for (int x = this.pagesWithBarcodes.Count - 1; x >= 0; x--)
            {
                var edoc = this.pagesWithBarcodes[x];
                if (edoc.Item1 == lastPage)
                {
                    lastPage = edoc.Item1 - 1;
                    continue;
                }

                var splitDocPath = TempFileUtils.NewTempFilePath();
                var document = new Document();
                using (FileStream fs = File.OpenWrite(splitDocPath))
                {
                    var pdfCopier = new PdfCopy(document, fs);
                    document.Open();

                    for (int currentPage = edoc.Item1 + 1; currentPage <= lastPage; currentPage++)
                    {
                        pdfCopier.AddPage(pdfCopier.GetImportedPage(packagePdf, currentPage));
                    }

                    pdfCopier.Close();
                    document.Close();
                }

                lastPage = edoc.Item1 - 1;

                var upload = new DocumentUpload(
                    edoc.Item2.LoanId,
                    edoc.Item2.AppId,
                    splitDocPath,
                    edoc.Item2.DocTypeId,
                    this.Settings);
                documentsToUpload.Add(upload);
            }

            packagePdf.Close();

            return documentsToUpload;
        }

        /// <summary>
        /// Determines the loan id and application id for the split documents.
        /// If the initial settings provided a loan id, that will ALWAYS be
        /// used. Otherwise, this will attempt to determine the loan id and app
        /// id from the barcodes.
        /// </summary>
        /// <returns>The loan id and app id for the documents.</returns>
        private LoanIdAppId GetLoanIdAppIdForSplitDocuments()
        {
            Guid loanId = Guid.Empty;
            Guid appId = Guid.Empty;

            if (this.Settings.LoanId != Guid.Empty)
            {
                loanId = this.Settings.LoanId;
                appId = this.Settings.AppId;
            }
            else
            {
                foreach (var entry in this.pagesWithBarcodes)
                {
                    if (entry.Item2.LoanId != Guid.Empty)
                    {
                        loanId = entry.Item2.LoanId;
                        break;
                    }
                }

                if (loanId != Guid.Empty)
                {
                    appId = this.GetPrimaryAppIdFromLoanId(this.Settings.BrokerId, loanId);
                }
            }

            return new LoanIdAppId(loanId, appId);
        }

        /// <summary>
        /// Gets a LendingQB barcode from a raw barcode.
        /// </summary>
        /// <param name="barcode">The raw barcode.</param>
        /// <returns>
        /// A LendingQB barcode if the format matches. Otherwise, null.
        /// </returns>
        private EdocFaxCover.FaxCoverData GetLendingQBBarcode(
            LendersOffice.ObjLib.Edocs.Barcode barcode)
        {
            EdocFaxCover.FaxCoverData lqbBarcode = null;

            int docTypeId;
            if (int.TryParse(barcode.Data, out docTypeId))
            {
                lqbBarcode = new EdocFaxCover.FaxCoverData()
                {
                    DocTypeId = docTypeId,
                    BrokerId = Guid.Empty,
                    AppId = Guid.Empty,
                    LoanId = Guid.Empty
                };
            }
            else if (EdocFaxCover.IsBarcodeType(E_FaxCoverType.PDFProcessor, barcode.Data))
            {
                lqbBarcode = EdocFaxCover.ReadBarcode(barcode.Data);
            }

            return lqbBarcode;
        }

        /// <summary>
        /// Removes entries that contain doc type ids that are not defined for
        /// the lender.
        /// </summary>
        private void RemoveEntriesWithInvalidDocTypeIds()
        {
            var docTypes = EDocumentDocType.GetDocTypesByBroker(
                this.Settings.BrokerId,
                E_EnforceFolderPermissions.True)
                .ToDictionary(p => p.DocTypeId);

            // It is possible that users upload barcodes that match the LQB
            // format, but are not actually LQB barcodes. We must remove these
            // by making sure the doc type we've parsed is actually available
            // for this lender.
            this.pagesWithBarcodes.RemoveAll(entry =>
                !docTypes.ContainsKey(entry.Item2.DocTypeId.ToString()));
        }

        /// <summary>
        /// Makes sure that all entries have the correct loan id and app id.
        /// </summary>
        private void VerifyEntryLoanIdAppIds()
        {
            var entriesToUpdate = this.pagesWithBarcodes
                .Where(entry =>
                    entry.Item2.AppId == Guid.Empty ||
                    entry.Item2.LoanId != this.loanIdAppId.Value.LoanId);

            foreach (var entry in entriesToUpdate)
            {
                entry.Item2.BrokerId = this.Settings.BrokerId;
                entry.Item2.AppId = this.loanIdAppId.Value.AppId;
                entry.Item2.LoanId = this.loanIdAppId.Value.LoanId;
                entry.Item2.Description = this.Settings.Description;
            }
        }

        /// <summary>
        /// Prepends barcode with the unreadable doc type.
        /// </summary>
        private void PrependEntryForUnreadableBarcodes()
        {
            int unreadableDocTypeId, unmappedDocTypeId;

            var mapper = new BaseDocTypeMapper(this.Settings.BrokerId);
            mapper.GetUnassignedDocTypeAndUnreadableDocType(
                out unmappedDocTypeId,
                out unreadableDocTypeId);

            var lqbBarcode = new EdocFaxCover.FaxCoverData()
            {
                LoanId = this.loanIdAppId.Value.LoanId,
                AppId = this.loanIdAppId.Value.AppId,
                BrokerId = this.Settings.BrokerId,
                DocTypeId = unreadableDocTypeId,
                Description = string.Empty
            };

            this.pagesWithBarcodes.Insert(0, Tuple.Create(0, lqbBarcode));
        }
    }
}
