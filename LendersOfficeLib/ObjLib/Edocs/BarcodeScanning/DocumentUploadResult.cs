﻿namespace LendersOffice.ObjLib.Edocs.BarcodeScanning
{
    /// <summary>
    /// The result of uploading split documents.
    /// </summary>
    public class DocumentUploadResult
    {
        /// <summary>
        /// A value indicating whether an error occurred during upload.
        /// </summary>
        public readonly bool HasError;

        /// <summary>
        /// The developer error message.
        /// </summary>
        public readonly string DeveloperErrorMessage;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentUploadResult"/>
        /// class.
        /// </summary>
        /// <param name="hasError">Whether an error occurred during upload.</param>
        /// <param name="devMessage">The developer message in the case of an error.</param>
        public DocumentUploadResult(bool hasError, string devMessage)
        {
            this.HasError = hasError;
            this.DeveloperErrorMessage = devMessage;
        }
    }
}
