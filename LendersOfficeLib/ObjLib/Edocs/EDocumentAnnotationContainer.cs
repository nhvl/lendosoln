﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using LendersOffice.Security;
using CommonProjectLib.Common;
using LendersOffice.Common;

namespace EDocs
{
    public class EDocumentAnnotationContainer
    {

        public bool NotesHaveChanged
        {
            get;
            set; 
        }

        public EDocumentAnnotationContainer()
        {
            NotesHaveChanged = false;
        }

        /// <summary>
        /// Creates a new  EDocumentAnnotationContainer with the given user data. 
        /// This constructor was added specifically for printing so we can cache what the user sees.
        /// </summary>
        /// <param name="p"></param>
        /// <param name="annotationList"></param>
        public EDocumentAnnotationContainer(AbstractUserPrincipal p, IEnumerable<EDocumentAnnotationItem> annotations)
        {
            if (annotations == null)
            {
                return; 
            }

            m_annotationList.AddRange(annotations);
        }

        public int Version { get; set; }
        private List<EDocumentAnnotationItem> m_annotationList = new List<EDocumentAnnotationItem>();

        public IEnumerable<EDocumentAnnotationItem> AnnotationList
        {
            get { return m_annotationList.AsReadOnly(); }
        }

        public int AnnotationCount
        {
            get { return m_annotationList.Count; }
        }

        public void Add(EDocumentAnnotationItem item)
        {
            if (null == item)
            {
                return;
            }

            m_annotationList.Add(item);
        }

        public void Remove(IEnumerable<EDocumentAnnotationItem> annotations)
        {
            foreach (EDocumentAnnotationItem annotation in annotations)
            {
                m_annotationList.RemoveAll(p => p.Id == annotation.Id);
            }
        }

        protected virtual bool HasUpdatePermission(AbstractUserPrincipal principal)
        {
            if (principal == null)
            {
                return false;
            }

            return principal.HasPermission(Permission.CanEditEDocsInternalNotes);
        }

        protected virtual bool HasViewPermission(AbstractUserPrincipal principal)
        {
            if (principal == null)
            {
                return false;
            }

            return principal.HasPermission(Permission.CanViewEDocsInternalNotes);
        }

        protected virtual bool Validate(EDocumentAnnotationItem item)
        {
            return true;
        }

        /// <summary>
        /// Uses the given principal to create new annotations if necassary. It uses the container's annotations as the existing list.
        /// Any annotations with no id in the updated list will be added to the container. Any missing annotations will be removed from the container. Ones with matching ids
        /// will be updated.
        /// </summary>
        /// <param name="p">Principal of user doing the changes</param>
        /// <param name="updatedAnnotationList">The user updated annotation data to update the container with.</param>
        public void Update(AbstractUserPrincipal p, IList<EDocumentAnnotationItem> updatedAnnotationList)
        {
            //if the list is null dont bother doing anything
            if (updatedAnnotationList == null)
            {
                return;
            }

            //if the user can not edit bail
            if (!this.HasUpdatePermission(p))
            {
                return;
            }
            
            //clone current list
            List<EDocumentAnnotationItem> existingAnnotations = new List<EDocumentAnnotationItem>(m_annotationList);

            foreach (EDocumentAnnotationItem editorAnnotation in updatedAnnotationList)
            {
                if (!this.Validate(editorAnnotation))
                {
                    continue;
                }

                //item is new add it 
                if (editorAnnotation.Id == Guid.Empty)
                {
                    editorAnnotation.CreatedBy = p.DisplayName;
                    editorAnnotation.CreatedDate = DateTime.Today;
                    editorAnnotation.Id = Guid.NewGuid();
                    editorAnnotation.LastModifiedBy = p.DisplayName;
                    editorAnnotation.LastModifiedDate = DateTime.Today;
                    m_annotationList.Add(editorAnnotation);
                    NotesHaveChanged = true;
                    continue;  //we are done go ahead and continue with the next one
                }

                //find the item update it and remove it
                EDocumentAnnotationItem existingAnnotation = existingAnnotations.FirstOrDefault(c => c.Id == editorAnnotation.Id);
                if (null == existingAnnotation)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Expected to find a note but no note found. This should not happen " + existingAnnotation.Id);
                }

                if (false == existingAnnotation.Text.Equals(editorAnnotation.Text ?? ""))
                {
                    //if the notes changed go ahead and update the last modified date
                    existingAnnotation.Text = editorAnnotation.Text;
                    existingAnnotation.LastModifiedDate = DateTime.Now;
                    NotesHaveChanged = true;
                }

                if (existingAnnotation.Rectangle.Top != editorAnnotation.Rectangle.Top || existingAnnotation.Rectangle.Left != editorAnnotation.Rectangle.Left)
                {
                    NotesHaveChanged = true;
                }
                existingAnnotation.Rectangle = editorAnnotation.Rectangle;
                
                existingAnnotation.NoteStateType = editorAnnotation.NoteStateType;
                existingAnnotation.MaximizedNoteWidth = editorAnnotation.MaximizedNoteWidth;
                existingAnnotation.MaximizedNoteHeight = editorAnnotation.MaximizedNoteHeight;
                existingAnnotation.PageNumber = editorAnnotation.PageNumber;
                existingAnnotation.AssociatedBorrower = editorAnnotation.AssociatedBorrower;
                existingAnnotation.AssociatedRole = editorAnnotation.AssociatedRole;
                //since it was update it here we wont be updating it again go ahead and remove it from the list
                existingAnnotations.Remove(existingAnnotation);
            }

            //Any annotation that is still in the list go ahead and remove it since we didnt find it in the user data it was removed
            foreach (EDocumentAnnotationItem remainingAnnotation in existingAnnotations)
            {
                m_annotationList.Remove(remainingAnnotation);
                NotesHaveChanged = true;
            }
        }


        /// <summary>
        /// If the principal does not have permission to view notes an empty list is returned otherwise the list of notes is returned.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public virtual List<EDocumentAnnotationItem> GenerateEditorList(AbstractUserPrincipal p)
        {
            if (HasViewPermission(p))
            {
                return m_annotationList;
            }
            return new List<EDocumentAnnotationItem>();
        }

        /// <summary>
        /// Returns array of page numbers that have annotations on them.
        /// Used to warn users w/o note privileges but with pdf edit privileges that if they delete the page
        /// the notes will be deleted.
        /// </summary>
        /// <param name="pageCount"></param>
        /// <returns></returns>
        public List<int> WhichPagesHaveAnnotations()
        {
            HashSet<int> pages = new HashSet<int>();
            foreach (EDocumentAnnotationItem item in m_annotationList)
            {
                pages.Add(item.PageNumber);
            }

            return pages.ToList();
        }

        /// <summary>
        /// Removes all the annotations on a given page.
        /// </summary>
        /// <param name="page"></param>
        public void RemoveAnnotationsOnPage(int page)
        {
            List<EDocumentAnnotationItem> annoations = m_annotationList.Where(p => p.PageNumber == page).ToList();

            foreach (EDocumentAnnotationItem annotation in annoations)
            {
                m_annotationList.Remove(annotation);
            }
        }


        #region Xml Serialization
        private string ElementName = "annotations";
        private int ReadInt(XmlReader reader, string attributeName, int defaultValue)
        {
            string s = reader.GetAttribute(attributeName);

            int value;
            if (!int.TryParse(s, out value))
            {
                value = defaultValue;
            }
            return value;
        }
        private void LoadImpl(XmlReader reader)
        {
            m_annotationList = new List<EDocumentAnnotationItem>();

            while (reader.NodeType != XmlNodeType.Element || reader.Name != ElementName)
            {
                reader.Read();
                if (reader.EOF) return;

            }
            Version = ReadInt(reader, "version", 0);
            if (reader.IsEmptyElement)
            {
                return;
            }
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == ElementName) return;
                if (reader.NodeType != XmlNodeType.Element) continue;

                switch (reader.Name)
                {
                    case EDocumentAnnotationItem.ElementName:
                        EDocumentAnnotationItem o = new EDocumentAnnotationItem();
                        o.ReadXml(reader);
                        Add(o);
                        break;

                }
            }

        }

        public void Load(string path)
        {
            using (XmlReader reader = new XmlTextReader(new StreamReader(path)))
            {
                LoadImpl(reader);
            }
        }
        public void Load(Stream stream)
        {
            using (XmlReader reader = new XmlTextReader(stream))
            {
                LoadImpl(reader);
            }
        }
        public void LoadContent(string content)
        {
            using (XmlReader reader = new XmlTextReader(new StringReader(content)))
            {
                LoadImpl(reader);
            }
        }
        public string GetXmlContent()
        {
            using (StringWriter writer = new StringWriter())
            {
                Save(writer);
                writer.Flush();
                return writer.ToString();
            }
        }
        public void Save(string outputPath)
        {
            using (XmlTextWriter writer = new XmlTextWriter(outputPath, Encoding.ASCII))
            {
                SaveImpl(writer);
            }
        }
        public void Save(Stream stream)
        {
            using (XmlWriter writer = new XmlTextWriter(stream, Encoding.ASCII))
            {
                SaveImpl(writer);
            }
        }
        public void Save(TextWriter output)
        {
            using (XmlWriter writer = new XmlTextWriter(output))
            {
                SaveImpl(writer);
            }
        }
        private void SaveImpl(XmlWriter writer)
        {
            writer.WriteStartElement(ElementName);
            writer.WriteAttributeString("version", Version.ToString());

            foreach (var o in m_annotationList)
            {
                o.WriteXml(writer);
            }
            writer.WriteEndElement();
        }
        #endregion
    }
}
