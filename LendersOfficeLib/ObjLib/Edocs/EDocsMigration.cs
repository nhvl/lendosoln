﻿// <copyright file="EDocsMigration.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   8/4/2015
// </summary>
namespace EDocs
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Messaging;
    using System.Threading;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;

    /// <summary>
    /// This class will perform migration from legacy EDocs to non destructive EDocs.
    /// </summary>
    public static class EDocsMigration
    {
        /// <summary>
        /// Checks to see if there are docs that require conversion to non destructive EDocs.
        /// </summary>
        /// <param name="brokerId">The broker id of the loan file.</param>
        /// <param name="loanId">The loan file id.</param>
        /// <returns>A bool indicating if the file requires migration.</returns>
        public static bool DoesLoanHaveLegacyEDocs(Guid brokerId, Guid loanId)
        {
            SqlParameter[] parameters =
                                 {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@sLId", loanId)
                                        };

            return (int)StoredProcedureHelper.ExecuteScalar(brokerId, "EDOCS_DOCUMENT_LoanHasLegacyEdocs", parameters) > 0;
        }

        /// <summary>
        /// Make a request to convert all legacy EDocs to non destructive EDocs in a loan file.
        /// </summary>
        /// <param name="brokerId">A broker id of the loan file.</param>
        /// <param name="loanId">The loan file.</param>
        public static void RequestMigrationForLegacyEdocs(Guid brokerId, Guid loanId)
        {
            SqlParameter[] parameters = 
                                        {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@sLId", loanId)
                                        };

            List<EDocsMigrationRequestItem> queueList = new List<EDocsMigrationRequestItem>();

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_DOCUMENT_GetLegacyEdocsByLoanId", parameters))
            {
                while (reader.Read())
                {
                    EDocsMigrationRequestItem item = new EDocsMigrationRequestItem();
                    item.BrokerId = brokerId;
                    item.DocumentId = (Guid)reader["DocumentId"];
                    item.FileDBKey_CurrentRevision = (Guid)reader["FileDBKey_CurrentRevision"];

                    queueList.Add(item);
                }
            }

            if (queueList.Count > 0)
            {
                foreach (var item in queueList)
                {
                    EDocument.UpdateImageStatus(brokerId, item.DocumentId, E_ImageStatus.NoCachedImagesButInQueue);
                    LendersOffice.Drivers.Gateways.MessageQueueHelper.SendJSON(ConstStage.MSMQ_EdocsMigration, null, item);
                }
            }
        }

        /// <summary>
        /// Perform migration requests.
        /// </summary>
        /// <param name="args">Arguments of schedule executable.</param>
        public static void ExecuteMigration(string[] args)
        {
            if (args.Length == 1)
            {
                MigrateMainQueue();
            }
            else if (args.Length == 3)
            {
                string queueType = args[1];
                int threadCount = int.Parse(args[2]);

                Thread[] threadList = new Thread[threadCount];
                for (int i = 0; i < threadCount; i++)
                {
                    Thread thread = null;

                    if (queueType == "main")
                    {
                        thread = new Thread(new ThreadStart(MigrateMainQueue));
                    }
                    else if (queueType == "nightly")
                    {
                        thread = new Thread(new ThreadStart(MigrateNighltyQueue));
                    }

                    thread.Name = "MigrationThread_" + i;
                    thread.Start();
                    threadList[i] = thread;
                }

                foreach (var thread in threadList)
                {
                    thread.Join();
                }
            }
        }

        /// <summary>
        /// Perform migration using the nightly migration queue.
        /// </summary>
        private static void MigrateNighltyQueue()
        {
            MigrateEdocs(ConstStage.MSMQ_EdocsNightlyMigration);
        }

        /// <summary>
        /// Perform migration using main migration queue.
        /// </summary>
        private static void MigrateMainQueue()
        {
            MigrateEdocs(ConstStage.MSMQ_EdocsMigration);
        }

        /// <summary>
        /// Perform migration.
        /// </summary>
        /// <param name="queueName">The queue contains migration request.</param>
        private static void MigrateEdocs(string queueName)
        {
            using (var queue = LendersOffice.Drivers.Gateways.MessageQueueHelper.PrepareForJSON(queueName))
            {
                Stopwatch sw = Stopwatch.StartNew();
                int count = 0;
                while (true)
                {
                    try
                    {
                        DateTime arrivalTime;
                        EDocsMigrationRequestItem item = LendersOffice.Drivers.Gateways.MessageQueueHelper.ReceiveJSON<EDocsMigrationRequestItem>(queue, 60, out arrivalTime);
                        if (item == null)
                        {
                            return; // same behavior as MessageQueueException.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout
                        }

                        MigrationToNonDestructiveEdocs(item);

                        if (count % 10 == 0)
                        {
                            Console.WriteLine(sw.ElapsedMilliseconds + " - " + Thread.CurrentThread.Name + " - Count=" + count + " Migrated executed in " + sw.ElapsedMilliseconds + "ms.");
                        }

                        count++;
                    }
                    catch (MessageQueueException exc)
                    {
                        if (exc.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                        {
                            return;
                        }
                        else
                        {
                            throw;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Perform migration request.
        /// </summary>
        /// <param name="item">Contains information about the request.</param>
        private static void MigrationToNonDestructiveEdocs(EDocsMigrationRequestItem item)
        {
            // Perform migration of the old edocs to non destructive edocs.
            Guid fileDBKey_CurrentRevision = item.FileDBKey_CurrentRevision;

            SqlParameter[] parameters = 
                                        {
                                            new SqlParameter("@BrokerId", item.BrokerId),
                                            new SqlParameter("@DocumentId", item.FileDBKey_CurrentRevision)
                                        };
            bool isMigrate = false;
            using (var reader = StoredProcedureHelper.ExecuteReader(item.BrokerId, "EDOCS_DOCUMENT_ORIGINAL_Exists", parameters))
            {
                if (reader.Read())
                {
                    // This document is already migrate. Skip.
                    isMigrate = true;
                }
            }

            if (isMigrate)
            {
                string fileDbKey = EDocument.GetOriginalDocumentMetadataJsonContentFileDBKey(item.BrokerId, item.FileDBKey_CurrentRevision);
                string migrateJson = string.Empty;
                try
                {
                    migrateJson = FileDBTools.ReadDataText(E_FileDB.EDMS, fileDbKey);
                }
                catch (FileNotFoundException)
                {
                    migrateJson = string.Empty;
                }

                if (string.IsNullOrEmpty(migrateJson) == false)
                {
                    var documentMetadata = SerializationHelper.JsonNetDeserialize<PdfRasterizerLib.DocumentMetadata>(migrateJson);
                    EDocument.UpdateImageStatusAfterSuccessfulMigration(item.BrokerId, item.DocumentId, documentMetadata);
                }

                return;
            }

            string pdfKey = fileDBKey_CurrentRevision.ToString();

            string edmsFileDB = ConstStage.FileDBSiteCodeForEdms;
            DocumentMetaData documentMetaData = null;

            int pdfFileSize = 0;

            Action<FileInfo> readHandler = delegate(FileInfo fi)
            {
                documentMetaData = DocumentMetaData.ReadFromPath(Guid.Empty, fi.FullName);

                pdfFileSize = (int)fi.Length;

                // 5/30/2018 - dd - Case 47025 - Copy the pdf to EdmsPdf FileDB.
                FileDBTools.WriteFile(E_FileDB.EdmsPdf, pdfKey, fi.FullName);
            };

            try
            {
                FileDBTools.UseFile(E_FileDB.EDMS, fileDBKey_CurrentRevision.ToString(), readHandler);
            }
            catch (FileNotFoundException)
            {
                Tools.LogError("MigrationToNonDestructiveEdocs. Could Not Migrate DocId=" + item.DocumentId + ", FileDBKey_CurrentRevision Not Found=" + item.FileDBKey_CurrentRevision);
                return;
            }

            bool isRequireFullConversion = false;
            int pngFileSize = 0;

            PdfRasterizerLib.DocumentMetadata nonDestructiveMetadata = new PdfRasterizerLib.DocumentMetadata();
            nonDestructiveMetadata.DocumentId = fileDBKey_CurrentRevision;
            nonDestructiveMetadata.PdfSize = pdfFileSize;
            Stopwatch sw = Stopwatch.StartNew();

            foreach (var page in documentMetaData.Pages)
            {
                if (string.IsNullOrEmpty(page.Key))
                {
                    isRequireFullConversion = true;
                }
                else
                {
                    PdfRasterizerLib.PageMetadata pageMetaData = new PdfRasterizerLib.PageMetadata();
                    pageMetaData.ReferenceDocumentId = fileDBKey_CurrentRevision;
                    pageMetaData.ReferenceDocumentPageNumber = page.Number - 1;
                    pageMetaData.Rotation = 0;
                    pageMetaData.PdfHeight = page.Height;
                    pageMetaData.PdfWidth = page.Width;
                    pageMetaData.PngFullHeight = (int)(ConstAppDavid.PdfPngScaling * page.Height);
                    pageMetaData.PngFullWidth = (int)(ConstAppDavid.PdfPngScaling * page.Width);
                    pageMetaData.PngThumbHeight = (int)(ConstAppDavid.PdfPngThumbRequeueScaling * page.Height);
                    pageMetaData.PngThumbWidth = (int)(ConstAppDavid.PdfPngThumbRequeueScaling * page.Width);

                    nonDestructiveMetadata.AddPage(pageMetaData);

                    try
                    {
                        Action<FileInfo> fullReadHandler = delegate(FileInfo fi)
                        {
                            if (isRequireFullConversion == false)
                            {
                                var key = pdfKey + "_" + (page.Number - 1) + "_full";
                                FileDBTools.WriteFile(E_FileDB.EdmsPdf, key, fi.FullName);

                                pngFileSize += (int)fi.Length;

                                pageMetaData.PngFullSize = fi.Length;
                            }

                            FileDBTools.Delete(E_FileDB.EDMS, page.Key);
                        };

                        FileDBTools.UseFile(E_FileDB.EDMS, page.Key, fullReadHandler);
                    }
                    catch (FileNotFoundException)
                    {
                        // Could not locate a full png. Force full conversion.
                        isRequireFullConversion = true;
                    }

                    var fileDBGetKey = page.Key + ConstAppDavid.PdfPngThumbFileDbSuffix;

                    try
                    {
                        Action<FileInfo> thumbReadHandler = delegate(FileInfo fi)
                        {
                            if (isRequireFullConversion == false)
                            {
                                var key = pdfKey + "_" + (page.Number - 1) + "_thumb";
                                FileDBTools.WriteFile(E_FileDB.EdmsPdf, key, fi.FullName);

                                pngFileSize += (int)fi.Length;
                                pageMetaData.PngThumbSize = fi.Length;
                            }

                            FileDBTools.Delete(E_FileDB.EDMS, page.Key + ConstAppDavid.PdfPngThumbFileDbSuffix);
                        };

                        FileDBTools.UseFile(E_FileDB.EDMS, fileDBGetKey, thumbReadHandler);
                    }
                    catch (FileNotFoundException)
                    {
                        // Could not locate a thumb png. Force full conversion.
                        isRequireFullConversion = true;
                    }
                }
            }

            if (isRequireFullConversion == false)
            {
                sw.Stop();

                nonDestructiveMetadata.DebugInfo = "Migration executed in " + sw.ElapsedMilliseconds + "ms.";
                string json = SerializationHelper.JsonNetSerialize(nonDestructiveMetadata);
                parameters = new SqlParameter[] 
                                        {
                                            new SqlParameter("@BrokerId", item.BrokerId),
                                            new SqlParameter("@SourceT", EDocs.EDocument.E_NonDestructiveSourceT.Migration),
                                            new SqlParameter("@DocumentId", nonDestructiveMetadata.DocumentId),
                                            new SqlParameter("@MetadataJsonContent", string.Empty),
                                            new SqlParameter("@NumPages", nonDestructiveMetadata.Pages.Count()),
                                            new SqlParameter("@PdfSizeInBytes", nonDestructiveMetadata.PdfSize),
                                            new SqlParameter("@PngSizeInBytes", nonDestructiveMetadata.PngFullSize + nonDestructiveMetadata.PngThumbSize)
                                        };

                StoredProcedureHelper.ExecuteNonQuery(item.BrokerId, "EDOCS_DOCUMENT_ORIGINAL_CreateForMigration", 3, parameters);

                string fileDbKey = EDocument.GetOriginalDocumentMetadataJsonContentFileDBKey(item.BrokerId, nonDestructiveMetadata.DocumentId);
                FileDBTools.WriteData(E_FileDB.EDMS, fileDbKey, json);

                EDocument.UpdateImageStatusAfterSuccessfulMigration(item.BrokerId, item.DocumentId, nonDestructiveMetadata);
            }
            else
            {
                EDocument.UpdateImageStatus(item.BrokerId, item.DocumentId, E_ImageStatus.NoCachedImagesButInQueue);

                EDocument.EnqueueNonDestructivePdfRasterizeRequest(item.BrokerId, item.DocumentId, pdfKey, EDocs.EDocument.E_NonDestructiveSourceT.Migration, EDocument.POST_ACTION_UPDATE_METADATA);
            }
        }
    }
}