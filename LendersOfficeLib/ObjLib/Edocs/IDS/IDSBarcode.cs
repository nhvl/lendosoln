﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Text.RegularExpressions;
using LendersOffice.Common;

namespace LendersOffice.ObjLib.Edocs.IDS
{
    /// <summary>
    /// Represents an IDS barcode of the format "&lt;LoanNumber&gt;|&lt;DocId&gt;|&lt;TotalNumPages&gt;|&lt;CurrentPageNum&gt;"
    /// </summary>
    public class IDSBarcode : IDocVendorBarcodeWithDocPageNum
    {
        public string sLNm { get; private set; }
        public string DocumentClass { get; private set; }
        public int PackagePageNumber { get; private set; }
        public int DocumentPageNumber { get; private set; }
        public int DocumentTotalPageNumber { get; private set; }
        private static Regex x_tokenRegex = new Regex("^<(.*)>$");

        public static bool MatchesFormat(Barcode barcode)
        {
            string[] data = barcode.Data.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            return data.Length == 4;
        }

        public IDSBarcode(Barcode barcode)
        {
            PackagePageNumber = barcode.Page;

            string[] data = barcode.Data.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            if (data.Length != 4)
            {
                string devMsg = String.Format("Invalid data format for IDS barcode. Data: {0}, Page: {1}", barcode.Data, barcode.Page);
                throw new CBaseException("Unexpected barcode format", devMsg);
            }

            try
            {
                sLNm = data[0].TrimWhitespaceAndBOM();
                DocumentClass = data[1].TrimWhitespaceAndBOM();
                DocumentTotalPageNumber = int.Parse(data[2].TrimWhitespaceAndBOM());
                DocumentPageNumber = int.Parse(data[3].TrimWhitespaceAndBOM());
            }
            catch (FormatException)
            {
                try
                {
                    var match = x_tokenRegex.Match(data[0].TrimWhitespaceAndBOM());
                    sLNm = match.Groups[1].Value;
                    match = x_tokenRegex.Match(data[1].TrimWhitespaceAndBOM());
                    DocumentClass = match.Groups[1].Value;
                    match = x_tokenRegex.Match(data[2].TrimWhitespaceAndBOM());
                    DocumentTotalPageNumber = int.Parse(match.Groups[1].Value);
                    match = x_tokenRegex.Match(data[3].TrimWhitespaceAndBOM());
                    DocumentPageNumber = int.Parse(match.Groups[1].Value);
                }
                catch (FormatException)
                {
                    string devMsg = String.Format("Invalid data format for IDS barcode. Data: {0}, Page: {1}", barcode.Data, barcode.Page);
                    throw new CBaseException("Unexpected barcode format", devMsg);
                }
            }

            
        }
    }
}
