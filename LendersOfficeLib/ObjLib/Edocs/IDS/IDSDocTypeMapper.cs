﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using CommonProjectLib.Common.Lib;

namespace LendersOffice.ObjLib.Edocs.IDS
{
    public class IDSDocTypeMapper : BaseDocTypeMapper, IDocTypeMapper
    {
        public IDSDocTypeMapper(Guid brokerId)
            : base(brokerId)
        {
        }

        public void SetAssociation(int docTypeID, IEnumerable<IDocVendorDocType> assocDocTypes)
        {
            SetAssociation(docTypeID, from t in assocDocTypes select t.Id);
        }

        public void SetAssociation(int docTypeId, IEnumerable<int> associatedDocTypes)
        {
            using (CStoredProcedureExec spExec = new CStoredProcedureExec(BrokerId))
            {
                spExec.BeginTransactionForWrite();

                spExec.ExecuteNonQuery("EDOCS_RemoveIDSAssociations", new SqlParameter("@BrokerId", BrokerId), new SqlParameter("@docTypeId", docTypeId));

                foreach (int idsDocTypeId in associatedDocTypes)
                {
                    spExec.ExecuteNonQuery("EDOCS_AssociateIDSDocType", new SqlParameter("@IDSDocTypeId", idsDocTypeId),
                        new SqlParameter("@BrokerId", BrokerId),
                        new SqlParameter("@DocTypeId", docTypeId));
                }
                spExec.CommitTransaction();
            }
        }

        public IEnumerable<IDocVendorDocType> GetTypesFor(int docTypeId)
        {
            List<IDocVendorDocType> docTypes = new List<IDocVendorDocType>();

            Dictionary<int, IDSDocType> dictionary = IDSDocType.ListAllAsDictionary();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId),
                                            new SqlParameter("@DocTypeId", docTypeId)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(BrokerId, "EDOCS_IDS_DOCTYPE_X_DOCTYPE_GetByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    int idsDocTypeId = (int)reader["IDSDocTypeId"];

                    IDSDocType docType = null;

                    if (dictionary.TryGetValue(idsDocTypeId, out docType))
                    {
                        docTypes.Add(docType);
                    }
                }
            }

            return docTypes;
        }

        public IEnumerable<IDocVendorDocType> GetDisassociatedTypes()
        {
            List<IDocVendorDocType> docTypes = new List<IDocVendorDocType>();

            Dictionary<int, IDSDocType> dictionary = IDSDocType.ListAllAsDictionary();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId)
                                        };

            HashSet<int> brokerIDSDocTypeSet = new HashSet<int>();

            using (var reader = StoredProcedureHelper.ExecuteReader(BrokerId, "EDOCS_IDS_DOCTYPE_X_DOCTYPE_GetByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    int idsDocTypeId = (int)reader["IDSDocTypeId"];

                    brokerIDSDocTypeSet.Add(idsDocTypeId);
                }
            }

            // Add all unmapped IDS doc types.
            foreach (var o in dictionary)
            {
                if (brokerIDSDocTypeSet.Contains(o.Key) == false)
                {
                    docTypes.Add(o.Value);
                }
            }

            return docTypes.Where(dt => !dt.Description.EndsWith("- OBSOLETE"));
        }

        public ILookup<int, IDocVendorDocType> GetBrokerDocTypeIdsWithDocVendorMappings()
        {
            List<Tuple<int, IDocVendorDocType>> docTypes = new List<Tuple<int, IDocVendorDocType>>();

            Dictionary<int, IDSDocType> dictionary = IDSDocType.ListAllAsDictionary();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerId, "EDOCS_IDS_DOCTYPE_X_DOCTYPE_GetByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    int docTypeId = (int)reader["DocTypeId"];
                    int idsDocTypeId = (int)reader["IDSDocTypeId"];

                    IDSDocType docType = null;

                    if (dictionary.TryGetValue(idsDocTypeId, out docType))
                    {
                        docTypes.Add(Tuple.Create<int, IDocVendorDocType>(docTypeId, docType));
                    }
                    
                }
            }
            return docTypes.ToLookup(p => p.Item1, p => p.Item2);
        }

        public IDictionary<string, int> GetVendorDocTypeIdsByDescription()
        {
            Dictionary<string, int> vendorDocTypeIdByDescription = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);

            foreach (var o in IDSDocType.ListAll())
            {
                vendorDocTypeIdByDescription.Add(o.Description, o.Id);
            }

            return vendorDocTypeIdByDescription;
        }

        public IDictionary<string, int> GetBrokerDocTypeIdsByBarcodeClassification()
        {
            Dictionary<string, int> brokerDocTypeIdsByBarcodeClassification = new Dictionary<string, int>();

            var dictionary = IDSDocType.ListAllAsDictionary();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerId, "EDOCS_IDS_DOCTYPE_X_DOCTYPE_GetByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    int docTypeId = (int)reader["DocTypeId"];
                    int idsDocTypeId = (int)reader["IDSDocTypeId"];

                    IDSDocType docType = null;

                    if (dictionary.TryGetValue(idsDocTypeId, out docType))
                    {
                        brokerDocTypeIdsByBarcodeClassification.Add(docType.BarcodeClassification, docTypeId);
                    }
                }
            }

            return brokerDocTypeIdsByBarcodeClassification;
        }

        public string VendorName
        {
            get { return "IDS"; }
        }

        public DataAccess.E_DocBarcodeFormatT DocumentFormat
        {
            get { return DataAccess.E_DocBarcodeFormatT.IDS; }
        }
    }
}
