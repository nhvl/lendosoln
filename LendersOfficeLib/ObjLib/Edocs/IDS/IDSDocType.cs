﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.ObjLib.Edocs.IDS
{
    public class IDSDocType : IDocVendorDocType, IDocVendorDocTypeRecord
    {
        private string description;
        public string Description
        {
            get
            {
                return $"{this.BarcodeClassification} - {this.description}";
            }

            private set
            {
                this.description = value;
            }
        }

        public string InternalDescription { get { return this.description; } }

        public int Id { get; private set; }
        public string BarcodeClassification { get; private set; }

        public IDSDocType(int id, string description, string barcodeClassification)
        {
            this.Id = id;
            this.description = description;
            this.BarcodeClassification = barcodeClassification;
        }

        private static TimeBasedCacheObject<IEnumerable<IDSDocType>> idsDocTypeList;

        static IDSDocType()
        {
            ReloadAll();
        }

        private static void ReloadAll()
        {
            // 4/21/2015 dd - Cache the list for 5 minutes.
            idsDocTypeList = new TimeBasedCacheObject<IEnumerable<IDSDocType>>(new TimeSpan(0, 5, 0), IDSDocType.ListAllImpl);
        }

        public static IEnumerable<IDSDocType> ListAll()
        {
            return idsDocTypeList.Value;
        }

        public static IEnumerable<IDocVendorDocTypeRecord> ListRecords()
        {
            return idsDocTypeList.Value;
        }

        public static Dictionary<int, IDSDocType> ListAllAsDictionary()
        {
            return idsDocTypeList.Value.ToDictionary(o => o.Id);
        }

        private static IEnumerable<IDSDocType> ListAllImpl()
        {
            List<IDSDocType> list = new List<IDSDocType>();

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "EDOCS_IDS_DOCTYPE_ListAll"))
            {
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    string description = (string)reader["Description"];
                    string barcodeClassification = (string)reader["BarcodeClassification"];

                    list.Add(new IDSDocType(id, description, barcodeClassification));
                }
            }

            return list;
        }

        public static int Create(string desc, string barcode)
        {
            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@Description", desc),
                new SqlParameter("@BarcodeClassification", barcode)
            };

            var result = (int)StoredProcedureHelper.ExecuteScalar(DataSrc.LOShare, "EDOCS_IDS_DOCTYPE_Insert", parameters);
            ReloadAll();

            return result;
        }

        public static void Delete(int recordId)
        {
            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@Id", recordId),
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "EDOCS_IDS_DOCTYPE_Delete", 3, parameters);
            ReloadAll();
        }

        public static void Update(int recordId, string desc, string barcode)
        {
            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@Id", recordId),
                new SqlParameter("@Description", desc),
                new SqlParameter("@BarcodeClassification", barcode)
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "EDOCS_IDS_DOCTYPE_Update", 3, parameters);
            ReloadAll();
        }

    }
}
