﻿using System;
using System.Collections.Generic;
using System.IO;
using iTextSharp.text.pdf;
using LendersOffice.Common;
using System.Linq;

namespace EDocs
{
    public class PDFPageIDManager  : IDisposable
    {
        public string Path { get; private set; }
        public int PageCount { get; private set; }
        private const string ANNOTATION_TITLE = "PML_PAGE_ID";
        private PageMetaData[] m_pageData; 

        private Dictionary<int, string> m_PageIds = new Dictionary<int, string>();
        private List<int> m_pagesWithNoIds = new List<int>();
   
        PdfReader m_reader;

        public bool HasIds { get; set; }
        public bool HasUnsupportedFeatures { 
            get { 
                return (m_reader.AcroFields.GetSignatureNames().Count > 0); 
            }
        }
        
        public PDFPageIDManager(string path)
        {
            Path = path;
            Init(EDocumentViewer.CreatePdfReader(path));

        }

        public PDFPageIDManager(PdfReader reader)
        {
            Path = null;
            Init(reader);

        }

        public IEnumerable<KeyValuePair<int,string>> PagesWithAnnotations
        {
            get { return m_PageIds; }
        }

        private void Init(PdfReader reader)
        {
            m_reader = reader;
            PageCount = m_reader.NumberOfPages;
            ReadPDF();
        }

        private void ReadPDF()
        {
            m_pageData = new PageMetaData[PageCount];

            for (int currentPageNumber = 1; currentPageNumber <= PageCount; currentPageNumber++)
            {
                var size = m_reader.GetPageSizeWithRotation(currentPageNumber);
                string titleValue = GetPageId(m_reader, currentPageNumber); 

                if (string.IsNullOrEmpty(titleValue))
                {
                    m_pagesWithNoIds.Add(currentPageNumber);
                }
                else
                {
                    m_PageIds.Add(currentPageNumber, titleValue);
                }

                m_pageData[currentPageNumber - 1] = new PageMetaData((int)size.Height, (int)size.Width, titleValue, currentPageNumber);

            }

            HasIds = m_PageIds.Count > 0;
        }

        public static string GetPageId(PdfReader reader, int page)
        {
            using (PerformanceStopwatch.Start("PDFPageIDManager.GetPageId()"))
            {
                PdfDictionary pageData = reader.GetPageN(page);
                PdfObject titleValue = pageData.Get(new PdfName(ANNOTATION_TITLE));

                if (titleValue == null || titleValue.IsNull() || titleValue.IsString() == false)
                {
                    return null;
                }
                else
                {
                    return titleValue.ToString();
                }
            }
        }

        /// <summary>
        /// Updates the PML PAGE Id on the given page with the value of key.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="key"></param>
        public  static  void Update(PdfReader reader, int page, string key)
        {
            PdfDictionary dictionary = reader.GetPageN(page);
            dictionary.Put(new PdfName(ANNOTATION_TITLE), new PdfString(key));
        }

        /// <summary>
        /// Updates the PML PAGE Id on the given page with the value of key.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="key"></param>
        public void Update(int page, string key)
        {
            Update(m_reader,page, key);
            m_pageData[page - 1].Key = key;
        }


        /// <summary>
        /// Saves changes to the given path.
        /// </summary>
        /// <param name="path"></param>
        public void Save(string path)
        {
            using (FileStream fs = File.OpenWrite(path))
            {
                PdfStamper stamper = new PdfStamper(m_reader, fs);
                stamper.Close();
            }
        }


        public IEnumerable<int> PagesWithNoAnnotations
        {
            get
            {
                return m_pagesWithNoIds.AsReadOnly();
            }
        }

        public string GetPageId(int page)
        {
            if (m_PageIds.ContainsKey(page))
            {
                return m_PageIds[page];
            }
            return null;
        }




        public static void DeletePageIdOn(PdfReader reader, int pageNumber)
        {
            PdfDictionary dictionary = reader.GetPageN(pageNumber);
            PdfName keyName = new PdfName(ANNOTATION_TITLE);
            if (dictionary.Contains(keyName))
            {
                dictionary.Remove(keyName);
            }
        }

        public void DeletePageIdOn(int pageNumber)
        {
            DeletePageIdOn(m_reader, pageNumber);
            m_pageData[pageNumber - 1].Key = "";
        }

        public void DeletePageIds()
        {
            for (int i = 1; i <= PageCount; i++)
            {
                DeletePageIdOn(m_reader, i);
            }
        }

        public DocumentMetaData GetMetaData(Guid key)
        {
            DocumentMetaData data = new DocumentMetaData(key, m_pageData);
            return data;
        }


        #region IDisposable Members

        public void Dispose()
        {
            if (string.IsNullOrEmpty(Path))
            {
                return;
            }

            m_reader.Close();
        }

        #endregion
    }
}
