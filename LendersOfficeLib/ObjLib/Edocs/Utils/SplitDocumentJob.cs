﻿// <copyright file="SplitDocumentJob.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Antonio Valencia
//  Date:   12/4/2014 11:07:38 AM
// </summary>
namespace EDocs.Utils
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// A plain old object that keeps track of the details for a new pdf from an existing one.
    /// </summary>
    public class SplitDocumentJob
    {
        /// <summary>
        /// Gets or sets the application id the new <see cref="EDocs.EDocument">EDocument</see> will belong to.
        /// </summary>
        /// <value>The application id the document will belong to.</value>
        public Guid AppId { get; set; }

        /// <summary>
        /// Gets or sets the document type id for the new document.
        /// </summary>
        /// <value>The document type for the new document.</value>
        public int DocType { get; set; }

        /// <summary>
        /// Gets or sets the public description for the new document.
        /// </summary>
        /// <value>The public description for the new document.</value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the internal description for the new document.
        /// </summary>
        /// <value>The internal description for the new document.</value>
        public string Comments { get; set; }

        /// <summary>
        /// Gets or sets the instructions to create the new PDF.
        /// </summary>
        /// <value>A list of pages that will compose the new document.</value>
        public List<PdfPageItem> EDocPages { get; set; }

        /// <summary>
        /// Gets or sets the document status for the new document.
        /// </summary>
        /// <value>The status of the new document.</value>
        public E_EDocStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the conditions that will be associated with the document.
        /// </summary>
        /// <value>The list of task to associate with the documents.</value>
        public List<string> Conditions { get; set; }
    }
}
