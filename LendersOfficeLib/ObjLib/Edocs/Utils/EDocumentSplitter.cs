﻿// <copyright file="EDocumentSplitter.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Antonio Valencia
//  Date:   12/4/2014 10:51:28 AM
// </summary>
namespace EDocs.Utils
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using DataAccess;
    using iTextSharp.text;
    using iTextSharp.text.pdf;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.DocumentConditionAssociation;
    using LendersOffice.Security;

    /// <summary>
    /// Creates new <see cref="EDocs.EDocument">EDocuments</see> from  an an 
    /// existing <see cref="EDocs.EDocument">EDocument</see>. 
    /// </summary>
    public class EDocumentSplitter : IDisposable
    {
        /// <summary>
        /// A list of temporary files used in the split process.
        /// </summary>
        private HashSet<string> temporaryFiles;

        /// <summary>
        /// The repository used to retrieve the documents.
        /// </summary>
        private EDocumentRepository repository;

        /// <summary>
        /// The parent of all the new documents.
        /// </summary>
        private EDocument sourceDocument;

        /// <summary>
        /// The list of new documents that will be created.
        /// </summary>
        private List<SplitDocumentJob> jobs;

        /// <summary>
        /// The principal being used to split the documents.
        /// </summary>
        private AbstractUserPrincipal principal; 

        /// <summary>
        /// Initializes a new instance of the <see cref="EDocumentSplitter" /> 
        /// class.
        /// </summary>
        /// <param name="principal">The user principal requesting the new documents.</param>
        /// <param name="sourceDocumentId">The identifier of the new document.</param>
        /// <param name="jobs">The list of new documents that will be created from the source document.</param>
        public EDocumentSplitter(AbstractUserPrincipal principal, Guid sourceDocumentId, List<SplitDocumentJob> jobs)
        {
            this.temporaryFiles = new HashSet<string>();
            this.repository = EDocumentRepository.GetUserRepositoryForSystem(principal.UserId, principal.BrokerId);
            this.sourceDocument = this.repository.GetDocumentById(sourceDocumentId);
            this.jobs = jobs;
            this.principal = principal;
        }

        /// <summary>
        /// Cleans up the temporary files used by the class.
        /// </summary>
        public void Dispose()
        {
            foreach (string file in this.temporaryFiles)
            {
                if (File.Exists(file))
                {
                    File.Delete(file);
                }
            }
        }

        /// <summary>
        /// Splits the source document until multiple docs. This method does not alter the source document.
        /// </summary>
        /// <param name="isNonDestructiveSplit">Whether to perform the split using legacy code or non destructive mode.</param>
        /// <returns>An array containing the new document ids.</returns>
        public Guid[] PerformSplitAndCreateDocs(bool isNonDestructiveSplit)
        {
            if (this.jobs == null || this.jobs.Count() == 0)
            {
                return new Guid[0];
            }

            if (isNonDestructiveSplit)
            {
                return this.PerformSplitAndCreateDocsUsingNonDestructive();
            }
            else
            {
                return this.PerformSplitAndCreateDocsUsingLegacyCode();
            }
        }

        /// <summary>
        /// Splits the source document using non destructive mode.
        /// </summary>
        /// <returns>An array containing the new document ids.</returns>
        private Guid[] PerformSplitAndCreateDocsUsingNonDestructive()
        {
            using (PerformanceStopwatch.Start("EDocumentSplitter.PerformSplitAndCreateDocsUsingNonDestructive"))
            {
                Guid[] newIds = new Guid[this.jobs.Count()];
                int index = 0;

                foreach (SplitDocumentJob job in this.jobs)
                {
                    Guid id = this.CreateNonDestructiveEDocument(job);
                    newIds[index] = id;
                    index++;
                }

                return newIds;
            }
        }

        /// <summary>
        /// Splits the source document using legacy code.
        /// </summary>
        /// <returns>An array containing the new document ids.</returns>
        private Guid[] PerformSplitAndCreateDocsUsingLegacyCode()
        {
            foreach (SplitDocumentJob job in this.jobs)
            {
                foreach (PdfPageItem item in job.EDocPages)
                {
                    if (item.Version != this.sourceDocument.Version)
                    {
                        throw new InvalidEDocumentVersion(item.Version, this.sourceDocument.Version);
                    }

                    if (item.DocId != this.sourceDocument.DocumentId)
                    {
                        throw new ArgumentException("The splitter only supports one source document.");
                    }
                }
            }

            using (PerformanceStopwatch.Start("EDocumentSplitter.PerformSplit"))
            {
                PdfReader sourceReader = this.GetSourceReader();
                Guid[] newIds = new Guid[this.jobs.Count()];
                int index = 0;

                foreach (SplitDocumentJob job in this.jobs)
                {
                    string file = this.CreateNewPDF(job, sourceReader);
                    Guid id = this.CreateEDocument(job, file);
                    newIds[index] = id;
                    index++;
                }

                return newIds;
            }
        }

        /// <summary>
        /// Fetches the PDF file and creates a <see cref="iTextSharp.text.pdf.PdfReader">PDF reader.</see>
        /// </summary>
        /// <returns>A <see cref="iTextSharp.text.pdf.PdfReader">PDF reader</see> for the source document..</returns>
        private PdfReader GetSourceReader()
        {
            using (PerformanceStopwatch.Start("EDocumentSplitter.GetSourceReader"))
            {
                string file = this.TrackTemporaryFile(() => this.sourceDocument.GetPDFTempFile_Current());

                if (AntiVirusScanner.Scan(file) == E_AntiVirusResult.HasVirus)
                {
                    throw new CBaseException("The EDoc contains a virus.", "Virus found in PDF " + this.sourceDocument.DocumentId);
                }

                return EDocumentViewer.CreatePdfReader(file);
            }
        }

        /// <summary>
        /// Executes the delegate which should retrieve a temporary file path. It then adds the file path to
        /// the list of temporary files for cleanup.
        /// </summary>
        /// <param name="fileFunction">A delegate returning a file path.</param>
        /// <returns>The path of the file.</returns>
        private string TrackTemporaryFile(Func<string> fileFunction)
        {
            string file = fileFunction();
            this.temporaryFiles.Add(file);
            return file;
        }

        /// <summary>
        /// Creates a brand new document representing the job.
        /// </summary>
        /// <param name="job">The job representing the new document.</param>
        /// <param name="sourceReader">The pdf reader for the source document.</param>
        /// <returns>The file path with the PDF document.</returns>
        private string CreateNewPDF(SplitDocumentJob job, PdfReader sourceReader)
        {
            if (job == null || job.EDocPages == null || job.EDocPages.Count == 0)
            {
                return null;
            }

            string newPdf = this.TrackTemporaryFile(() => TempFileUtils.NewTempFilePath() + "_PDFSPLIT.pdf");

            string monitorlabel = "EDocumentSplitter.SplitDocument.CreatePDF_" + job.EDocPages.Count;

            using (PerformanceStopwatch.Start(monitorlabel))
            {
                Document document = new Document();

                using (FileStream fs = File.OpenWrite(newPdf))
                {
                    int currentPageNumber = 0;
                    PdfCopy pdfWriter = new PdfCopy(document, fs);
                    document.Open();

                    foreach (PdfPageItem item in job.EDocPages)
                    {
                        currentPageNumber++;

                        if (item.Rotation != 0)
                        {
                            int rotation = sourceReader.GetPageRotation(item.Page) + item.Rotation;
                            sourceReader.GetPageN(item.Page).Put(PdfName.ROTATE, new PdfNumber(rotation));
                        }

                        PDFPageIDManager.DeletePageIdOn(sourceReader, item.Page);
                        PdfImportedPage page = pdfWriter.GetImportedPage(sourceReader, item.Page);
                        pdfWriter.AddPage(page);
                    }

                    document.Close();
                }
            }

            return newPdf;
        }

        /// <summary>
        /// Create a document based on the split job.
        /// </summary>
        /// <param name="job">The details that where used to generate the EDocument.</param>
        /// <returns>The DocumentId of the new EDoc.</returns>
        private Guid CreateNonDestructiveEDocument(SplitDocumentJob job)
        {
            EDocument doc = this.repository.CreateDocumentFromExisting(E_EDocumentSource.SplitDoc, this.sourceDocument.DocumentId);
            doc.LoanId = this.sourceDocument.LoanId;
            doc.AppId = job.AppId;
            doc.DocStatus = job.Status;
            doc.DocumentTypeId = job.DocType;
            doc.PublicDescription = job.Description;
            doc.InternalDescription = job.Comments;
            doc.UpdateNonDestructiveContentOnSave(job.EDocPages, null, null);

            if (this.principal.Type == "P")
            {
                doc.EDocOrigin = E_EDocOrigin.PML;
            }
            else
            {
                doc.EDocOrigin = E_EDocOrigin.LO;
            }

            this.repository.Save(doc);

            foreach (var taskId in job.Conditions)
            {
                var assoc = new DocumentConditionAssociation(this.principal.BrokerId, doc.LoanId.Value, taskId, doc.DocumentId);
                assoc.Save();
            }

            return doc.DocumentId;
        }

        /// <summary>
        /// Creates a <see cref="EDocs.EDocument">document</see> based on the <see cref="SplitDocumentJob">split job</see>.
        /// </summary>
        /// <param name="job">The details that where used to generate the EDocument.</param>
        /// <param name="path">The temporary file path that is holding the PDF.</param>
        /// <returns>The DocumentId of the new EDoc.</returns>
        private Guid CreateEDocument(SplitDocumentJob job, string path)
        {
            EDocument doc = this.repository.CreateDocumentFromExisting(E_EDocumentSource.SplitDoc, this.sourceDocument.DocumentId);

            doc.LoanId = this.sourceDocument.LoanId;
            doc.AppId = job.AppId;
            doc.DocStatus = job.Status;
            doc.DocumentTypeId = job.DocType;
            doc.PublicDescription = job.Description;
            doc.InternalDescription = job.Comments;
            doc.SetPDFContent(path, job.EDocPages.Count);

            if (this.principal.Type == "P")
            {
                doc.EDocOrigin = E_EDocOrigin.PML;
            }
            else
            {
                doc.EDocOrigin = E_EDocOrigin.LO;
            }

            this.repository.Save(doc);

            foreach (var taskId in job.Conditions)
            {
                var assoc = new DocumentConditionAssociation(this.principal.BrokerId, doc.LoanId.Value, taskId, doc.DocumentId);
                assoc.Save();
            }

            return doc.DocumentId;
        }
    }
}
