﻿// <copyright file="EDocConversionQueue.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Antonio Valencia
//  Date:   7/1/2015 8:14:23 PM
// </summary>
namespace EDocs.Utils
{
    using System;
    using System.Linq;
    using System.Messaging;
    using System.Web;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Constants;

    /// <summary>
    /// Represents a queue for EDocument conversion request.
    /// </summary>
    public static class EDocConversionQueue
    {
        /// <summary>
        /// Retrieves a EDocument conversion request from the given queue.
        /// </summary>
        /// <param name="queueT">The type of queue to get items from.</param>
        /// <returns>A message or null.</returns>
        public static EDocQueueDetails Dequeue(E_EDocQueueT queueT)
        {
            string connectionString;

            switch (queueT)
            {
                case E_EDocQueueT.Main:
                    connectionString = ConstStage.MSMQ_EDoc_NormalQueue;
                    break;
                case E_EDocQueueT.PML:
                    connectionString = ConstStage.MSMQ_EDoc_PMLQueue;
                    break;
                case E_EDocQueueT.Fax:
                    connectionString = ConstStage.MSMQ_EDoc_FaxQueue;
                    break;
                default:
                    throw new UnhandledEnumException(queueT);
            }

            if (string.IsNullOrEmpty(connectionString))
            {
                return null;
            }

            using (var queue = LendersOffice.Drivers.Gateways.MessageQueueHelper.PrepareForXML(connectionString, true))
            {
                try
                {
                    DateTime arrivalTime;
                    var xdoc = LendersOffice.Drivers.Gateways.MessageQueueHelper.ReceiveXML(queue, null, out arrivalTime);
                    if (xdoc == null)
                    {
                        return null; // same behavior as MessageQueueException.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout
                    }

                    return new EDocQueueDetails(xdoc, arrivalTime);
                }
                catch (MessageQueueException e)
                {
                    if (e.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                    {
                        return null;
                    }

                    throw;
                }
            }
        }

        /// <summary>
        /// Enqueues the given document for image regeneration.
        /// </summary>
        /// <param name="doc">The  <see cref="EDocs.EDocument" /> to enqueue for conversion.</param>
        /// <param name="checkImages">A boolean indicating whether the processor should verify images exists.</param>
        public static void Enqueue(EDocument doc, bool checkImages)
        {
            if (ConstStage.EnableEDocsMSMQ == false)
            {
                EDocument.UpdateImageStatus(doc.BrokerId, doc.DocumentId, E_ImageStatus.Error);

                return;
            }

            Guid brokerId = doc.BrokerId; 

            XElement root = new XElement("e");
            root.Add(new XAttribute("k", doc.FileDbKey_CurrentRevision.ToString()));
            root.Add(new XAttribute("b", brokerId.ToString()));
            root.Add(new XAttribute("d", doc.DocumentId.ToString()));
            root.Add(new XAttribute("r", checkImages.ToString()));

            XDocument xdoc = new XDocument(root);
                       
            // Logic mimics database queue selection.
            string connectionString = ConstStage.MSMQ_EDoc_PMLQueue;
            E_EDocumentSource[] userActionSources = { E_EDocumentSource.NotApplicable, E_EDocumentSource.UserUpload, E_EDocumentSource.UserCopy, E_EDocumentSource.SplitDoc };
            
            if (doc.PageCount >= ConstStage.BatchQueuePageCountCutOff)
            {
                connectionString = ConstStage.MSMQ_EDoc_FaxQueue;
            }
            else if (HttpContext.Current != null && userActionSources.Contains(doc.CreationSource))
            {
                connectionString = ConstStage.MSMQ_EDoc_NormalQueue;
            }

            // We dont want this to fail, specially if called withing save.
            try
            {
                LendersOffice.Drivers.Gateways.MessageQueueHelper.SendXML(connectionString, null, xdoc);
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                if (brokerId != Guid.Empty)
                {
                    EDocument.UpdateImageStatus(brokerId, doc.DocumentId, E_ImageStatus.Error);
                }
            }
        }
    }
}
