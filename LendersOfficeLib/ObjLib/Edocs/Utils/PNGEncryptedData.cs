﻿// <copyright file="PNGEncryptedData.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   11/27/2014 3:26:22 PM 
// </summary>
namespace EDocs.Utils
{
    using System;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Secures access to PNG images. It takes a some image information and encrypts it by giving it to the user. 
    /// This allows the application to decrypt the data and return the image. Saving a few database hits. If our encrypted string is compromised
    /// we can no longer use this class.
    /// </summary>
    public class PNGEncryptedData
    {
        /// <summary>
        /// Static version to indicate the encrypted data is for non destructive <code>Edocs</code>.
        /// </summary>
        public const int NonDestructiveEdocsVersion = -94298;

        /// <summary>
        /// Initializes a new instance of the <see cref="PNGEncryptedData" /> class.
        /// </summary>
        /// <param name="encryptedKey">The encrypted key to decode.</param>
        public PNGEncryptedData(string encryptedKey)
        {
            string decryptedValue = EncryptionHelper.Decrypt(encryptedKey);
            string[] pngData = decryptedValue.Split('|');
            if (pngData.Length != 5)
            {
                throw CBaseException.GenericException("Invalid Encryption key.");
            }

            this.DocumentId = new Guid(pngData[0]);
            this.Version = int.Parse(pngData[1]);
            this.PageNumber = int.Parse(pngData[2]);
            this.PNGKey = pngData[3];
            this.LoginNm = pngData[4];
        }

        /// <summary>
        /// Gets the document identifier that owns the image.
        /// </summary>
        /// <value>Gets the document identifier.</value>
        public Guid DocumentId { get; private set; }

        /// <summary>
        /// Gets the version number used in the token.
        /// </summary>
        /// <value>Gets the version number used in token.</value>
        public int Version { get; private set; }

        /// <summary>
        /// Gets the page number in the document of the image.
        /// </summary>
        /// <value>Gets the page number of the picture.</value>
        public int PageNumber { get; private set; }

        /// <summary>
        /// Gets the PNG filedb identifier key. PNGs are stored in EDocs FileDB.
        /// </summary>
        /// <value>Gets the filedb key of the PNG.</value>
        public string PNGKey { get; private set; }

        /// <summary>
        /// Gets the login name of the user who requested the image in the editor.
        /// </summary>
        /// <value>Gets the login name of the user who requested the image.</value>
        public string LoginNm { get; private set; }

        /// <summary>
        /// Takes the 5 pieces of information and makes a encrypted token to give the user.
        /// </summary>
        /// <param name="documentId">The EDocument ID.</param>
        /// <param name="version">The Version Number.</param>
        /// <param name="pageNumber">The page number of the image in the document.</param>
        /// <param name="pdfKey">The PDF key locating the image in the system.</param>
        /// <param name="logingNm">The user who is requesting to view the images.</param>
        /// <returns>The encrypted token to give back to the client.</returns>
        public static string Encrypt(Guid documentId, int version, int pageNumber, string pdfKey, string logingNm)
        {
            return EncryptionHelper.Encrypt(string.Format("{0}|{1}|{2}|{3}|{4}", documentId, version, pageNumber, pdfKey, logingNm));
        }

        /// <summary>
        /// Gets a string representing the class.
        /// </summary>
        /// <returns>A string representation of the internal state of the class.</returns>
        public override string ToString()
        {
            return string.Format("PNGKEY-{0}|{1}|{2}|{3}|{4}", this.DocumentId, this.Version, this.PageNumber, this.PNGKey, this.LoginNm);
        }
    }
}
