﻿// <copyright file="EDocQueueDetails.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Antonio Valencia
//  Date:   7/1/2015 8:42:17 PM
// </summary>
namespace EDocs.Utils
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;
    using LendersOffice.Constants;

    /// <summary>
    /// A plain old object containing each value needed to perform the conversion.
    /// </summary>
    public class EDocQueueDetails
    {
        /// <summary>
        /// The fields for the pdf processor.
        /// </summary>
        private Dictionary<string, string> values; 

        /// <summary>
        /// Initializes a new instance of the <see cref="EDocQueueDetails" /> 
        /// class.
        /// </summary>
        /// <param name="doc">The document containing the details.</param>
        /// <param name="arrivalTime">The time the message was queued.</param>
        public EDocQueueDetails(XDocument doc, DateTime arrivalTime)
        {
            this.ArrivalTime = arrivalTime;

            // Keep in sync with EDocument EnqueuePDFConversion and Dequeue.
            this.DocumentId = doc.Root.Attribute("d").Value;

            this.values = new Dictionary<string, string>()
            {
                { "FileDbSiteCode", ConstStage.FileDBSiteCodeForEdms },
                { "Scale", ConstAppDavid.PdfPngScaling.ToString() },
                { "DocumentFileDBLocation", doc.Root.Attribute("k").Value },
                { "ThumbScale", ConstAppDavid.PdfPngThumbRequeueScaling.ToString() },
                { "ThumbSuffix", ConstAppDavid.PdfPngThumbFileDbSuffix },
                { "ErrorRequeue", doc.Root.Attribute("r").Value },
                { "BrokerId",  doc.Root.Attribute("b").Value },
            };
        }

        /// <summary>
        /// Gets the id for the document.
        /// </summary>
        /// <value>The document id.</value>
        public string DocumentId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the insertion time of the message.
        /// </summary>
        /// <value>The insertion time.</value>
        public DateTime ArrivalTime { get; private set; }

        /// <summary>
        /// Gets the values for the processor.
        /// </summary>
        /// <value>The values for the processor.</value>
        public Dictionary<string, string> Values
        {
            get { return this.values; }
        }
        
        /// <summary>
        /// Gets a string representing the current details.
        /// </summary>
        /// <returns>String with important details.</returns>
        public override string ToString()
        {
            return string.Format("k:{0} r:{1} b:{2} d:{3}", this.values["DocumentFileDBLocation"], this.values["ErrorRequeue"], this.values["BrokerId"], this.DocumentId);
        }
    }
}
