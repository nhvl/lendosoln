﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Common;

namespace EDocs
{
    public class DuplicateNameSelectedException : CBaseException
    {
        public DuplicateNameSelectedException(string message)
            : base(message, message)
        {
            this.IsEmailDeveloper = false;
        }
    }

    public class DocTypeNotFoundException : CBaseException
    {
        public DocTypeNotFoundException(string message)
            : base(message, message)
        {
            this.IsEmailDeveloper = false;
        }
    }

    public class FolderNotFoundException : CBaseException
    {
        public FolderNotFoundException(string message)
            : base(message, message)
        {
            this.IsEmailDeveloper = false;
        }
    }
}
