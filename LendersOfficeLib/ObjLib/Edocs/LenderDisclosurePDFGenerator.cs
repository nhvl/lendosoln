﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using System.Reflection;
using System.IO;
using iTextSharp.text.pdf;
using DataAccess;
using LendersOffice.Admin;

namespace LendersOffice.ObjLib.Edocs
{
    public class LenderDisclosurePDFGenerator
    {
        private Guid m_brokerId;
        
        public LenderDisclosurePDFGenerator(Guid brokerId)
        {
            m_brokerId = brokerId;     
        }

        public string SavePdf()
        {
            BrokerDB db = BrokerDB.RetrieveById(m_brokerId);
            Assembly assembly = Assembly.GetExecutingAssembly();
            using (Stream stream = assembly.GetManifestResourceStream("LendersOffice.ObjLib.Edocs.ESignConsent.pdf"))
            {
                string newPdf = TempFileUtils.NewTempFilePath() + ".pdf";
                PdfReader pdfReader = new PdfReader(stream);
                using (FileStream fs = File.Create(newPdf))
                {
                    PdfStamper stamper = new PdfStamper(pdfReader, fs);
                    AcroFields pdfFormFields = stamper.AcroFields;
                    
                    var companyAddr = string.Format("{0}, {1}",
                        db.Name, Tools.FormatSingleLineAddress(db.Address.StreetAddress,
                        db.Address.City, db.Address.State, db.Address.Zipcode));
                    pdfFormFields.SetField("CompanyAddr", companyAddr);
                    pdfFormFields.SetField("CompanyPhone", db.Phone);
                    pdfFormFields.SetField("CompanyName", db.Name);

                    stamper.FormFlattening = true;
                    stamper.Close();
                    pdfReader.Close();
                }
                
                return newPdf;
            }
        }


    }
}
