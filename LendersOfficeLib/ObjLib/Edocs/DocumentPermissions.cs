﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.Admin;
using LendersOffice.ConfigSystem;
using DataAccess;
using LendersOffice.ObjLib.Security;
using LendersOffice.ObjLib.ConsumerPortal;
using EDocs.Contents;
using LendersOffice.ObjLib.Loan.Security;
using LendersOffice.ConfigSystem.Operations;

namespace EDocs
{
    internal abstract class EDocumentPermissions
    {
        internal abstract bool CanReadDocument(EDocument document);
        internal abstract bool CanUpdateDocument(EDocument document);
        internal abstract bool CanUpdateGenericDocument(GenericEDocument document);
        internal abstract bool CanCreateDocument();
        internal abstract bool CanDeleteDocument(EDocument document);
        internal abstract bool CanReadDocument(GenericEDocument doc);

        internal abstract IEnumerable<ModifiedDocumentDetail> GetReadableDocuments(Func<IEnumerable<ModifiedDocumentDetail>> getDetails);
    }


    internal sealed class SystemEDocumentPermissions : EDocumentPermissions
    {
        internal override bool CanReadDocument(EDocument document)
        {
            return true;
        }

        internal override bool CanUpdateDocument(EDocument document)
        {
            return true;
        }

        internal override bool CanDeleteDocument(EDocument document)
        {
            return true;
        }

        internal override bool CanCreateDocument()
        {
            return true;
        }


        internal override bool CanUpdateGenericDocument(GenericEDocument document)
        {
            return true;
        }

        internal override bool CanReadDocument(GenericEDocument doc)
        {
            return true;
        }

        internal override IEnumerable<ModifiedDocumentDetail> GetReadableDocuments(Func<IEnumerable<ModifiedDocumentDetail>> getDetails)
        {
            return getDetails();
        }

    }

    internal sealed class UserEDocumentPermissions : EDocumentPermissions
    {
        private AbstractUserPrincipal m_principal;
        private Dictionary<Guid, bool> m_loanAccessCache = new Dictionary<Guid, bool>();
        
        private bool CanRead(EDocument document)
        {
            if (m_principal.Type == "C")
            {
                // 10/15/2010 dd - Consumer.
                LoanAccessInfo loanInfo = new LoanAccessInfo();
                loanInfo.Retrieve(document.LoanId.Value);
                AccessBinding accessBinding = ConsumerPortalAccessControlProcessor.Resolve((ConsumerUserPrincipal)m_principal, loanInfo);

                return accessBinding.CanRead;
            }
            else if (m_principal.Type == "D")
            {
                ConsumerPortalUserPrincipal p = (ConsumerPortalUserPrincipal)m_principal;
                return ConsumerPortalUser.IsLinkedToLoan(p.BrokerId, p.Id, document.LoanId.Value);
            }
            else
            {
                bool hasLoanAccess;
                if (false == m_loanAccessCache.TryGetValue(document.LoanId.Value, out hasLoanAccess))
                {
                    LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(m_principal.ConnectionInfo, m_principal.BrokerId, document.LoanId.Value, WorkflowOperations.ReadLoanOrTemplate);
                    valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(m_principal));
                    m_loanAccessCache[document.LoanId.Value] = hasLoanAccess = LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, valueEvaluator);
                }

                return hasLoanAccess;
            }
        }
     
        internal UserEDocumentPermissions(AbstractUserPrincipal principal)
        {
            m_principal = principal; 
        }

        internal override IEnumerable<ModifiedDocumentDetail> GetReadableDocuments(Func<IEnumerable<ModifiedDocumentDetail>> getDetails)
        {
            // Have to keep this in sync with the rest of the methods. This logic is taken from the methods in this class and EDocument.CanRead(). 
            if (!this.m_principal.HasPermission(Permission.CanViewEDocs))
            {
                return Enumerable.Empty<ModifiedDocumentDetail>();
            }

            var details = getDetails();
            Dictionary<int, DocType> docTypes = EDocumentDocType.GetDocTypesByBroker(this.m_principal.BrokerId, false).ToDictionary(p => p.Id);

            if (m_principal.IsInRole(ConstApp.ROLE_CONSUMER))
            {
                details = details.Where(p => !p.IsEditable);
            }

            if (this.m_principal.Type.Equals("P", StringComparison.OrdinalIgnoreCase))
            {
                details = details.Where(p => !p.HideFromPmlUsers);
            }

            details = details.Where(p => docTypes.ContainsKey(p.DocTypeId) && docTypes[p.DocTypeId].Folder.CanRead(this.m_principal));
            var loanIds = details.Select(p => p.LoanId).ToList();

            LoanReadPermissionResolver resolver = new LoanReadPermissionResolver(this.m_principal.BrokerId);
            var readableLoans = resolver.GetReadableLoans(this.m_principal, loanIds);

            return details.Where(p => readableLoans.Contains(p.LoanId));
        }

        internal override bool CanReadDocument(EDocument document)
        {
            if (!document.LoanId.HasValue || !document.AppId.HasValue)
            {
                return false;
            }

            // OPM 64817 - PML Users should be able to see all docs in folders with PML roles,
            // regardless of who uploaded them, except those marked as hidden from PML Users
            if (m_principal.Type == "P" && document.HideFromPMLUsers)
            {
                return false;
            }

            // OPM 42065
            if (document.CanRead(m_principal) == false) // Checks if the user has the right role based on the folder the document is contained in
            {
                return false;
            }

            return CheckPermissions(document.BrokerId, Permission.CanViewEDocs) && CanRead(document);
        }

        internal override bool CanReadDocument(GenericEDocument doc)
        {
            return CheckPermissions(doc.BrokerId, Permission.CanViewEDocs) && doc.Folder.CanRead(m_principal); 
        }

        internal override bool CanUpdateDocument(EDocument document)
        {
            // OPM 42065
            // Checks if the user has the right role based on the folder the document is contained in
            // and if the document is editable
            if (document.CanEdit == false) 
            {
                return false;
            }

            //if there was no status change and the approve status is approved then user needs editing approve doc permission to edit edoc.
            if (document.DocStatusInitial == E_EDocStatus.Approved && CheckPermissions(document.BrokerId, Permission.AllowEditingApprovedEDocs) == false)
            {
                //you cannot edit a approved doc.
                return false;
            }

            return CanRead(document) && (CheckPermissions(document.BrokerId, Permission.CanEditEDocs) || m_principal.Type == "P") ;
        }

        internal override bool CanUpdateGenericDocument(GenericEDocument document)
        {
            return CheckPermissions(document.BrokerId, Permission.CanEditEDocs); 
        }

        internal override bool CanDeleteDocument(EDocument document)
        {
            // OPM 42065
            // Checks if the user has the right role based on the folder the document is contained in
            if (document.CanDelete == false)
            {
                return false;
            }

            if (document.DocStatus == E_EDocStatus.Approved && CheckPermissions(document.BrokerId, Permission.AllowEditingApprovedEDocs) == false)
            {
                //you cannot edit a approved doc.
                return false;
            }

            return CanRead(document) && (CheckPermissions(document.BrokerId, Permission.CanEditEDocs) || m_principal.Type == "P");
        }

        private  bool CheckPermissions(Guid? brokerId, Permission permissions)
        {

            if (m_principal.BrokerId != brokerId)
            {
                return false;
            }

            return m_principal.HasPermission(permissions);
        }

        internal override bool CanCreateDocument()
        {
            
            return m_principal.Type == "P" ||  CheckPermissions(m_principal.BrokerId, Permission.CanViewEDocs);

        }


    }
}
