﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Email;
using System.Xml;
using System.Xml.Linq;
using LendersOffice.Constants;
using LendersOffice.Security;
using LendersOffice.Admin;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using LendersOffice.ObjLib.Security;
using LendersOffice.ObjLib.ConsumerPortal;

namespace EDocs
{
    public static class EDocumentNotifier
    {
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>
    (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }

        #region get default from address
        private static string DefaultConsumerEmailAddress(Guid employeeId, Guid brokerId, out string nmls)
        {
            EmployeeDB db = new EmployeeDB(employeeId, brokerId);
            db.Retrieve();

            return DefaultConsumerEmailAddress(db, out nmls);
        }

        private static string DefaultConsumerEmailAddress(EmployeeDB employeeDb, out string nmls)
        {
            string fromAddress = employeeDb.Email;
            nmls = employeeDb.LosIdentifier;

            if (string.IsNullOrEmpty(fromAddress))
            {
                nmls = "";
                fromAddress = ConstStage.DefaultDoNotReplyAddress;          //SHOULD NEVER HAPPEN 
            }

            return fromAddress;
        }

        private static string DefaultInternalUserEmailAddress(Guid brokerId, Guid loanId)
        {
            EventNotificationRulesView ev = new EventNotificationRulesView(brokerId);
            return ev.GetFromAddressString(loanId);
        }

        #endregion 

        /// <summary>
        /// Adds new EDocument Notification To Queue
        /// </summary>
        /// <param name="doc"></param>
        public static void AddNewEDocumentNotificationToQueue(EDocument doc)
        {
            if (doc.LoanId.HasValue == false || doc.AppId.HasValue == false)
            {
                Tools.LogBug("Edocument notification doc loan id is null or app id is null");
                return;
            }

            Guid brokerId = doc.BrokerId;

            if (brokerId == Guid.Empty)
            {
                Tools.LogError("Edocument notification  cannot determine the broker who the doc was uploaded from  disregarding notification.");
                return;
            }

            BrokerDB brokerdb = BrokerDB.RetrieveById(brokerId);

            if (brokerdb.IsEDocNotificationsEnabled)
            {
                EDocNotificationQueueProcessor.SubmitNotification(doc);
            }
        }
        
        /// <summary>
        /// Sends new EDocument Notification
        /// </summary>
        /// <param name="doc"></param>
        public static void SendNewEDocumentNotification(List<string> docIdList)
        {
            bool firstDocFound = false;
            EDocument firstDoc = null;
            for (int i = 0; i < docIdList.Count && !firstDocFound; i++)
            {
                try
                {
                    firstDoc = EDocument.GetDocumentByIdObsolete(new Guid(docIdList[i]));
                    firstDocFound = true;
                }
                catch (NotFoundException) {}
            }

            if (!firstDocFound)
            {
                // All uploaded docs has been deleted. No need to send an email.
                return;
            }

            CPageData loanData = GetBypassCPage(firstDoc.LoanId.Value);
            loanData.InitLoad();

            if (loanData.IsDeleted)
            {
                // skip loans that are deleted.
                Tools.LogInfo("[EDocNoSendBCDeleted] Not sending edocnotifications for loan with id: " 
                    + loanData.sLId + " because it's deleted.");
                return; 
            }

            //update http://lqbdevfaq.lendingqb.com/questions/25/who-receives-new-edocument-notifications-and-when
            //if you change this
            string location = "LendersOffice.ObjLib.Edocs.EmailTemplates.EDocReceivalEmailTemplate.xslt";
            string underwriterEmail = loanData.sEmployeeUnderwriterEmail;
            string underwriterName = loanData.sEmployeeUnderwriterName;
            string juniorUnderwriterEmail = loanData.sEmployeeJuniorUnderwriterEmail;
            string juniorUnderwriterName = loanData.sEmployeeJuniorUnderwriterName;
            string processorEmail = loanData.sEmployeeProcessorEmail;
            string processorName = loanData.sEmployeeProcessorName;
            string juniorProcessorEmail = loanData.sEmployeeJuniorProcessorEmail;
            string juniorProcessorName = loanData.sEmployeeJuniorProcessorName;
            string postCloserEmail = loanData.sEmployeePostCloserEmail;
            string postCloserName = loanData.sEmployeePostCloserName;
            string address = DefaultInternalUserEmailAddress(loanData.sBrokerId, firstDoc.LoanId.Value);
            string sLNm = loanData.sLNm;
            string subject = string.Format("{0} - {1} - {2} - eDocs received", sLNm, loanData.GetAppData(0).aBNm, loanData.sStatusT_rep);
            string receivedOn = firstDoc.CreatedDate.ToShortDateString();
            
            var parameterXml = new XDocument(
                new XElement("DocumentNotificationData",
                    new XElement("sLNm", Tools.SantizeXmlString(sLNm)),
                    new XElement("Count", docIdList.Count),
                    new XElement("ReceivedOn", receivedOn)
                )
            );

            // OPM 179074 - Send notification to Post Closer, but only for Docs uploaded through Consumer Portal after closing date
            // Use same template as processor/underwriter email
            bool bSendPostCloserEmail = false;
            var postCloserParameterXml = new XDocument(parameterXml);

            foreach (string docId in docIdList)
            {
                try
                {
                    EDocument edoc = EDocument.GetDocumentById(new Guid(docId), loanData.sBrokerId, false);
                    AddDocToParameterXml(parameterXml, edoc);

                    // OPM 179074 - Send notification to Post Closer, but only for Docs uploaded through Consumer Portal after closing date
                    if (edoc.EDocOrigin == E_EDocOrigin.ConsumerPortal
                        && loanData.sDocMagicClosingD.IsValid
                        && loanData.sDocMagicClosingD.DateTimeForComputation <= edoc.LastModifiedDate)
                    {
                        bSendPostCloserEmail = true;
                        AddDocToParameterXml(postCloserParameterXml, edoc);
                    }
                }
                catch (NotFoundException)
                {
                    // EDoc was deleted before the notification could be sent. Ignore and move on to the next doc.
                }
            }

            var emailAddressesNotified = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            if (!string.IsNullOrEmpty(underwriterEmail))
            {
                emailAddressesNotified.Add(underwriterEmail);
                parameterXml.Element("DocumentNotificationData").Add(new XElement("RecipentName", underwriterName));
                SendEmail(loanData.sBrokerId, location, parameterXml, subject, underwriterEmail, address, true, E_DisclaimerType.NORMAL);
                parameterXml.Element("DocumentNotificationData").Element("RecipentName").Remove();
            }

            if (!string.IsNullOrEmpty(processorEmail) && !emailAddressesNotified.Contains(processorEmail))
            {
                emailAddressesNotified.Add(processorEmail);
                parameterXml.Element("DocumentNotificationData").Add(new XElement("RecipentName", processorName));
                SendEmail(loanData.sBrokerId, location, parameterXml, subject, processorEmail, address, true, E_DisclaimerType.NORMAL);
                parameterXml.Element("DocumentNotificationData").Element("RecipentName").Remove();
            }

            if (!string.IsNullOrEmpty(juniorUnderwriterEmail) && !emailAddressesNotified.Contains(juniorUnderwriterEmail))
            {
                emailAddressesNotified.Add(juniorUnderwriterEmail);
                parameterXml.Element("DocumentNotificationData").Add(new XElement("RecipentName", juniorUnderwriterName));
                SendEmail(loanData.sBrokerId, location, parameterXml, subject, juniorUnderwriterEmail, address, true, E_DisclaimerType.NORMAL);
                parameterXml.Element("DocumentNotificationData").Element("RecipentName").Remove();
            }

            if (!string.IsNullOrEmpty(juniorProcessorEmail) && !emailAddressesNotified.Contains(juniorProcessorEmail))
            {
                emailAddressesNotified.Add(juniorProcessorEmail);
                parameterXml.Element("DocumentNotificationData").Add(new XElement("RecipentName", juniorProcessorName));
                SendEmail(loanData.sBrokerId, location, parameterXml, subject, juniorProcessorEmail, address, true, E_DisclaimerType.NORMAL);
                parameterXml.Element("DocumentNotificationData").Element("RecipentName").Remove();
            }

            // OPM 179074 - Send notification to Post Closer, but only for Docs uploaded through Consumer Portal after closing date
            if (bSendPostCloserEmail && !string.IsNullOrEmpty(postCloserEmail) && !emailAddressesNotified.Contains(postCloserEmail))
            {
                emailAddressesNotified.Add(postCloserEmail);
                postCloserParameterXml.Element("DocumentNotificationData").Add(new XElement("RecipentName", postCloserName));
                SendEmail(loanData.sBrokerId, location, postCloserParameterXml, subject, postCloserEmail, address, true, E_DisclaimerType.NORMAL);
                postCloserParameterXml.Element("DocumentNotificationData").Element("RecipentName").Remove();
            }
        }

        private static void AddDocToParameterXml(XDocument parameterXml, EDocument edoc)
        {
            parameterXml.Element("DocumentNotificationData").Add(
                new XElement("doc",
                    new XElement("ApplicationName", Tools.SantizeXmlString(edoc.AppName)),
                    new XElement("DocumentType", Tools.SantizeXmlString(edoc.DocTypeName)),
                    new XElement("FolderName", Tools.SantizeXmlString(edoc.Folder.FolderNm)),
                    new XElement("PublicDescription", Tools.SantizeXmlString(edoc.PublicDescription))
                )
            );
        }

        public class LoginNotification
        {
            public Guid AppId;
            public bool IsBorrower;
            public string Email;
            public string Name;
        }

        public static List<LoginNotification> GetConsumersNeedingLoginNotification(Guid LoanID)
        {
            var dataLoan = GetCPageData(LoanID);
            dataLoan.InitLoad();

            var emails = new List<LoginNotification>();
            var BrokerID = dataLoan.BrokerDB.BrokerID;

            for (int i = 0; i < dataLoan.nApps; i++)
            {
                var dataApp = dataLoan.GetAppData(i);

                if (NeedsNotification(dataApp.aBEmail, BrokerID))
                {
                    emails.Add(new LoginNotification(){
                        Email = dataApp.aBEmail,
                        IsBorrower = true,
                        AppId = dataApp.aAppId,
                        Name = dataApp.aBFirstNm
                    });
                }

                if (NeedsNotification(dataApp.aCEmail, BrokerID))
                {
                    emails.Add(new LoginNotification()
                    {
                        Email = dataApp.aCEmail,
                        IsBorrower = false,
                        AppId = dataApp.aAppId,
                        Name = dataApp.aCFirstNm
                    });
                }
            }

            return emails;
        }

        private static bool NeedsNotification(string email, Guid brokerId)
        {
            //We'll ignore empty emails
            if (string.IsNullOrEmpty(email)) return false;
            var passwordIsTemporary = ConsumerUserPrincipal.RetrievePasswordIsTemporaryByEmail(email, brokerId);
            //And the user needs to be notified if they either have no account or their password is temporary.
            return !passwordIsTemporary.HasValue || passwordIsTemporary.Value == true;
        }

        /// <summary>
        /// Sends login notifications to users who have never logged in for this loan.
        /// </summary>
        /// <param name="LoanId"></param>
        /// <param name="BrokerId"></param>
        public static void SendLoginNotifications(Guid LoanId, Guid BrokerId)
        {
            var toSend = GetConsumersNeedingLoginNotification(LoanId);
            var log = new StringBuilder("Sending notifications for consumers from SendLoginNotifications in loan id " + LoanId);
            log.AppendLine();
            foreach (var note in toSend)
            {
                var tempPwd = CreateConsumerAccountTempPassword(note.Email, BrokerId, LoanId, note.AppId, note.IsBorrower);

                var parameterXml = GetParameterXml(LoanId, tempPwd, note.Name, note.Email);
                var companyNode = parameterXml.Descendants("CompanyName").FirstOrDefault();
                string companyName = "";

                if (companyNode != default(XElement)) companyName = " with " + companyNode.Value;

                string subject = "Action required for your loan application" + companyName;
                string nmls;
                string fromAddress = DefaultConsumerEmailAddress(BrokerUserPrincipal.CurrentPrincipal.EmployeeId, BrokerUserPrincipal.CurrentPrincipal.BrokerId, out nmls);
                string location = "LendersOffice.ObjLib.Edocs.EmailTemplates.ConsumerLoginRequestTemplate.xslt";
                SendEmailToBorrower(location, BrokerId, parameterXml, subject, note.Email, fromAddress);

                var firstThree = string.IsNullOrEmpty(note.Name) ? "empty or null" : note.Name.PadRight(4, ' ').Substring(0, 3) + " (first three letters)";
                var emailLen = string.IsNullOrEmpty(note.Email) ? "empty or null" : note.Email.Length + "";
                var borrOrCoborr = note.IsBorrower ? "borrower" : "coborrower";
                log.AppendLine("Queued up a login notification email to " + borrOrCoborr + " in app: " + note.AppId + " name: " + firstThree + " email length: " + emailLen);
            }

            Tools.LogInfo(log.ToString());
        }


        public static void SendAccountInformation(Guid brokerId, Guid loanId, string name, string email, string tempPassword)
        {
            var parameterXml = GetParameterXml(loanId, tempPassword, name, email);
            var companyNode = parameterXml.Descendants("CompanyName").FirstOrDefault();
            string companyName = "";
            if (companyNode != default(XElement)) companyName = "for " + companyNode.Value;
            string nmls;
            string subject = "Borrower Portal Account Information " + companyName;
            string fromAddress = DefaultConsumerEmailAddress(BrokerUserPrincipal.CurrentPrincipal.EmployeeId, BrokerUserPrincipal.CurrentPrincipal.BrokerId, out nmls);
            string location = "LendersOffice.ObjLib.Edocs.EmailTemplates.ConsumerLoginRequestTemplate.xslt";
            SendEmailToBorrower(location, brokerId, parameterXml, subject, email, fromAddress);
        }

        private static string CreateConsumerAccountTempPassword(string sEmail, Guid BrokerId, Guid guidLoanId, Guid guidAppId, bool bIsBorrower)
        {
            if (string.IsNullOrEmpty(sEmail)) throw new CBaseException("sEmail cannot be empty", new ArgumentException());

            string sTempPassword = ("p" + Guid.NewGuid().ToString().Substring(1, 5)).ToLower();
            string passwordHash = Tools.HashOfPassword(sTempPassword);

            SqlParameter[] parameters;
            
            //Create an account for the user if necessary
            SqlParameter paramReturn = new SqlParameter("@ReturnValue", DbType.Int32);
            paramReturn.Direction = ParameterDirection.ReturnValue;
            //Create a new cportal user (this does nothing if they already exist)
            parameters = new[] {
                                            new SqlParameter("@oldEmail", string.Empty),
                                            new SqlParameter("@newEmail", sEmail),
                                            new SqlParameter("@loanId", guidLoanId),
                                            new SqlParameter("@appId", guidAppId),
                                            new SqlParameter("@isBorrower", bIsBorrower),
                                            new SqlParameter("@hashPasswordForNewAcc", passwordHash),
                                            new SqlParameter("@forceUpdate", 1),

                                            paramReturn
                                        };

            StoredProcedureHelper.ExecuteScalar(BrokerId, "CP_CheckAndUpdateConsumerAccount", parameters);    

            //And then reset their password and make them have a temp password
            parameters = new[] 
            { 
                new SqlParameter("@Email", sEmail), 
                new SqlParameter("@BrokerId", BrokerId), 
                new SqlParameter("@PasswordHash", passwordHash) 
            };

            StoredProcedureHelper.ExecuteNonQuery(BrokerId, "CP_UpdatePasswordForNeverLoggedInUser", 3, parameters);

            return sTempPassword;
        }

        private static XDocument GetParameterXml(Guid loanId, string tempPassword, string name, string email)
        {
            var loanData = GetCPageData(loanId);
            loanData.InitLoad();

            AbstractUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
            EmployeeDB employeeDb = new EmployeeDB(principal.EmployeeId, principal.BrokerId);
            employeeDb.Retrieve();

            string companyName = BranchDB.RetrieveById(employeeDb.BrokerID, employeeDb.BranchID).DisplayNm;
            string phoneNumber = employeeDb.Phone;
            string requesterName = employeeDb.FullName;
            string nmls;
            string fromAddress = DefaultConsumerEmailAddress(employeeDb, out nmls);

            string loginURL = GetCPLoginUrl(loanData.BrokerDB, loanData.sBranchId);

            return new XDocument(
                            new XElement("DocumentNotificationData",
                                    new XElement("LoginUrl", loginURL),
                                    new XElement("CompanyName", companyName),
                                    new XElement("CPTempPassword", tempPassword),
                                    new XElement("ConsumerName", name),
                                    new XElement("RequesterName", requesterName),
                                    new XElement("PhoneNumber", phoneNumber),
                                    new XElement("Email", email),
                                    new XElement("NMLS", nmls ?? ""),
                                    new XElement("TempPasswordValidLength", ConstApp.CPTempPasswordValidLengthInDays)
                          )
                        );
        }

        private static string GetCPLoginUrl(BrokerDB db, Guid sBranchId)
        {
            string loginUrl;
            if (db.IsEnableNewConsumerPortal)
            {
                BranchDB branchdb = new BranchDB(sBranchId, db.BrokerID);
                branchdb.Retrieve();
                if (!branchdb.ConsumerPortalId.HasValue)
                {
                    throw new CBaseException(String.Format("Error: Please assign branch {0} to a consumer portal.", branchdb.Name), "Branch not associated to consumer portal.");
                }

                ConsumerPortalConfig config = ConsumerPortalConfig.Retrieve(db.BrokerID, branchdb.ConsumerPortalId.Value);

                if (!string.IsNullOrEmpty(config.ReferralUrl))
                    return config.ReferralUrl;

                // OPM 141650 - if referralURL is blank, use BorrowerLoginUrl instead
                ConsumerPortalIdData consumerPortal = new ConsumerPortalIdData(branchdb.ConsumerPortalId.Value, config.Name);
                return consumerPortal.BorrowerLoginUrl;
            }
            else
            {
                if (false == ConstStage.ConsumerPortalBaseUrl.EndsWith("/"))
                {
                    loginUrl = ConstStage.ConsumerPortalBaseUrl + "?" + Tools.ShortenGuid(db.PmlSiteID);
                }
                else
                {
                    loginUrl = ConstStage.ConsumerPortalBaseUrl + "login.aspx?SiteId=" +db.PmlSiteID ;
                }
            }
            return loginUrl;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="consumerActionItem"></param>
        /// <param name="isBorrower"></param>
        public static void SendESignConfirmation(ConsumerActionItem consumerActionItem, bool isBorrower)
        {
            //we are only reading   to send email so bypass okay
            var loanData =  GetBypassCPage(consumerActionItem.sLId);
            loanData.InitLoad();

            CAppData appData = loanData.GetAppData(consumerActionItem.aAppId);
            string aCEmail = appData.aCEmail;

            // The coborrower can be a title only borrower and he wont be in the app.
            if (consumerActionItem.CoborrowerTitleBorrowerId.HasValue && !isBorrower)
            {
                TitleBorrower borrower = loanData.sTitleBorrowers.FirstOrDefault(p=>p.Id == consumerActionItem.CoborrowerTitleBorrowerId.Value);
                if (borrower == null)
                {
                    Tools.LogErrorWithCriticalTracking("Title Borrower no longer exist not sending any notification!");
                    return;
                }
                aCEmail = borrower.Email; 
            }

            string subject = "Document Signature Confirmation";
            string location = "LendersOffice.ObjLib.Edocs.EmailTemplates.ConsumerESignConfirmationEmailTemplate.xslt";

            string name = isBorrower ? consumerActionItem.aBFullName : consumerActionItem.aCFullName; 

            var docType = EDocumentDocType.GetDocTypeById(loanData.sBrokerId, consumerActionItem.DocTypeId);

            IPreparerFields prep = loanData.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            string companyName = prep.CompanyName;
            string phoneNumber = prep.Phone;

            string nmls;
            string fromAddress = DefaultConsumerEmailAddress(consumerActionItem.CreatorEmployeeId, loanData.sBrokerId, out nmls);

            var parameters = new XDocument(
                new XElement("DocumentNotificationData",
                    new XElement("WhoSigned", name),
                    new XElement("DocTypeName", docType.DocTypeName),
                    new XElement("DocDescription", consumerActionItem.Description),
                    new XElement("SignedOn", DateTime.Today.ToShortDateString()), 
                    new XElement("CompanyName", companyName),
                    new XElement("PhoneNumber", phoneNumber),
                    new XElement("NMLS", nmls)
                )
            );

            if (false == string.IsNullOrEmpty(appData.aBEmail))
            {
                SendEmail(loanData.sBrokerId, location, parameters, subject, appData.aBEmail, fromAddress, false, E_DisclaimerType.CONSUMER);
            }

            if (false == string.IsNullOrEmpty(aCEmail) && appData.aBEmail.ToLower() != aCEmail.ToLower())
            {
                SendEmail(loanData.sBrokerId, location, parameters, subject, aCEmail, fromAddress, false, E_DisclaimerType.CONSUMER);
            }

        }

        public static void SendConsumerRequestCompletedNotification(ConsumerActionItem consumerActionItem, bool isFax, bool isUpload)
        {
            var recivalType = isFax ? "faxed in" : isUpload ? "uploaded" : "signed";
            var loanData = GetBypassCPage(consumerActionItem.sLId);
            loanData.InitLoad();

            string appNames = consumerActionItem.aBFullName;
            if (false == string.IsNullOrEmpty( consumerActionItem.aCFullName ) )
            {
                if( false == string.IsNullOrEmpty(appNames) ) { appNames += " & ";}
                appNames += consumerActionItem.aCFullName; 
            }

            if (string.IsNullOrEmpty(loanData.sEmployeeLoanRepEmail) || string.IsNullOrEmpty(loanData.sEmployeeProcessorEmail) || string.IsNullOrEmpty(loanData.sEmployeeUnderwriterEmail))
            {
                return; //no one to send emails to 
            }

            var docType = EDocumentDocType.GetDocTypeById(loanData.sBrokerId, consumerActionItem.DocTypeId );
            var parameterXml = new XDocument(
                new XElement("DocumentNotificationData",
                new XElement("DocumentType", docType.DocTypeName),
                new XElement("FolderName", docType.Folder.FolderNm),
                new XElement("PublicDescription", consumerActionItem.Description),
                new XElement("sLNm", loanData.sLNm), 
                new XElement("ReceivalType",recivalType),
                new XElement("ApplicationNames", appNames)
                )
            );

            string fromAddres = DefaultInternalUserEmailAddress(loanData.sBrokerId, loanData.sLId);

            if (string.IsNullOrEmpty(fromAddres))
            {
                fromAddres = ConstStage.DefaultDoNotReplyAddress;
            }

            string subject = "Consumer Document Received " + docType.DocTypeName + " for loan " + loanData.sLNm;
            string location = "LendersOffice.ObjLib.Edocs.EmailTemplates.ConsumerPortalDocumentReceivalEmailTemplate.xslt";

            string emails = "";

            if (string.IsNullOrEmpty(loanData.sEmployeeUnderwriterEmail) == false)
            {
                emails += loanData.sEmployeeUnderwriterEmail; 
            }

            if (string.IsNullOrEmpty(loanData.sEmployeeProcessorEmail) == false && emails.ToLower().Contains(loanData.sEmployeeProcessorEmail.ToLower()) == false )
            {
                if (!string.IsNullOrEmpty(emails)) { emails += ","; }
                emails += loanData.sEmployeeProcessorEmail;
            }

            if (string.IsNullOrEmpty(loanData.sEmployeeLoanRepEmail) == false && emails.ToLower().Contains(loanData.sEmployeeLoanRepEmail.ToLower()) == false)
            {
                if (!string.IsNullOrEmpty(emails)) { emails += ","; }
                emails += loanData.sEmployeeLoanRepEmail;
            }

            SendEmail(loanData.sBrokerId, location, parameterXml, subject, emails, fromAddres, false, E_DisclaimerType.NORMAL);

        }

        private static CPageData GetCPageData(Guid sLId)
        {
            CPageData data = CPageData.CreateUsingSmartDependency(sLId, typeof(EDocumentNotifier));
            return data;
        }

        private static CPageData GetBypassCPage(Guid sLId)
        {
            CPageData data = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(EDocumentNotifier));
            return data;
        }
        private static void SendEmail(Guid brokerId, string location, XDocument parameterXml, string subject, string recipent, string from, bool isHtmlEmail, E_DisclaimerType discType)
        {
            string emailMsg = XslTransformHelper.ApplyDataToStylesheet(location, parameterXml.ToString(SaveOptions.DisableFormatting));
            CBaseEmail cbe = new CBaseEmail(brokerId)
            {
                Subject = subject,
                From = from,
                Bcc = "",
                To = recipent,
                Message = emailMsg,
                IsHtmlEmail = isHtmlEmail,
                DisclaimerType = discType
            };
            cbe.Send();  //!? ought to be in try/catch block?
        }

        private static void SendEmailToBorrower(string location, Guid brokerId, XDocument parameterXml, string subject, string recipent, string from)
        {
            byte[] logoBytes = null;
            string logoId = "Logo";
            var pmlSiteId = BrokerDB.RetrieveById(brokerId).PmlSiteID;

            try
            {
                logoBytes = FileDBTools.ReadData(E_FileDB.Normal, pmlSiteId.ToString().ToLower() + ".logo.gif");
            }
            catch (FileNotFoundException)
            {}

            if (logoBytes != null)
            {
                parameterXml.Descendants().First().FirstNode.AddAfterSelf(
                        new XElement("LogoCID", "cid:" + logoId)
                    );
            }

            string emailMsg = XslTransformHelper.ApplyDataToStylesheet(location, parameterXml.ToString(SaveOptions.DisableFormatting));
            CBaseEmail cbe = new CBaseEmail(brokerId)
            {
                Subject = subject,
                From = from,
                Bcc = "",
                To = recipent,
                Message = emailMsg,
                IsHtmlEmail = true,
                DisclaimerType = E_DisclaimerType.CONSUMER
            };
            if (logoBytes != null)
            {
                cbe.AddAttachment(logoId, logoBytes, true);
            }

            cbe.Send();  //!? ought to be in try/catch block?
        }


        public static void SetupAccountsAndSendEmails(ConsumerActionItem consumerActionItem)
        {
            SetupAccountsAndSendEmails(
                consumerActionItem,
                BrokerUserPrincipal.CurrentPrincipal.BrokerId,
                BrokerUserPrincipal.CurrentPrincipal.EmployeeId,
                BrokerUserPrincipal.CurrentPrincipal.DisplayName,
                false);
        }

        public static void SetupAccountsAndSendEmails(ConsumerActionItem consumerActionItem, Guid brokerId, Guid employeeId, string displayName, bool isForMultipleDocuments)
        {
            // 6/15/2014 gf - This is a system action on behalf of the user and
            // the Document Request Queue processor needs to be able to call this
            // without a principal.
            var loanData = GetBypassCPage(consumerActionItem.sLId);
            loanData.InitLoad();
            CAppData appData = loanData.GetAppData(consumerActionItem.aAppId);
            string aCEmail = appData.aCEmail;
            string aCNm = appData.aCNm;
            string aCFirstNm = appData.aCFirstNm;

            if (consumerActionItem.CoborrowerTitleBorrowerId.HasValue)
            {
                TitleBorrower borrower = loanData.sTitleBorrowers.FirstOrDefault(p => p.Id == consumerActionItem.CoborrowerTitleBorrowerId.Value);

                if (borrower == null)
                {
                    Tools.LogErrorWithCriticalTracking("Need to check title borrower info when request is created.");
                    return;
                }
                aCEmail = borrower.Email;
                aCNm = borrower.FullName;
                aCFirstNm = borrower.FirstNm;
            }
            
            if (string.IsNullOrEmpty(appData.aBEmail) && string.IsNullOrEmpty(aCEmail))
            {
                Tools.LogBug("Request created but there are no emails to notify.");
                return;
            }

            bool sendBorrowerEmail = true, sendCoborrowerEmail = true;
            string borrowerTempPassword = "", coborrowerTempPassword = "";


            #region create (if needed) and grant access to loans
            BrokerDB db = BrokerDB.RetrieveById(loanData.sBrokerId);
            if (db.IsEnableNewConsumerPortal)
            {
                BranchDB branchDB = new BranchDB(loanData.sBranchId, db.BrokerID);
                branchDB.Retrieve();

                if (!branchDB.ConsumerPortalId.HasValue)
                {
                    throw new ArgumentException(String.Format("Branch {0} has no consumer portal", db.Name), "sBranchId");
                }

                ConsumerPortalUser aBAccount = GetCP2Consumer(db.BrokerID, branchDB.ConsumerPortalId.Value, appData.aBEmail, out borrowerTempPassword);
                ConsumerPortalUser aCAccount = GetCP2Consumer(db.BrokerID, branchDB.ConsumerPortalId.Value, aCEmail, out coborrowerTempPassword);
                if (aBAccount != null)
                {
                    sendBorrowerEmail = aBAccount.IsNotifyOnNewDocRequests;
                    ConsumerPortalUser.LinkLoan(db.BrokerID, aBAccount.Id, loanData.sLId);
                }
                else
                {
                    sendBorrowerEmail = false;
                }

                if (aCAccount != null)
                {
                    sendCoborrowerEmail = aCAccount.IsNotifyOnNewDocRequests;
                    if (aCAccount.Id != aBAccount.Id)
                    {
                        ConsumerPortalUser.LinkLoan(db.BrokerID, aCAccount.Id, loanData.sLId);
                    }
                }
                else
                {
                    sendCoborrowerEmail = false;
                }
            }
            else
            {
                SetupAccountForCP1(db.BrokerID, appData.aBEmail, consumerActionItem.sLId, consumerActionItem.aAppId, true, out borrowerTempPassword);
                SetupAccountForCP1(db.BrokerID, aCEmail, consumerActionItem.sLId, consumerActionItem.aAppId, false, out coborrowerTempPassword);
            }
            #endregion

            IPreparerFields prep = loanData.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            string companyName = prep.CompanyName;
            string phoneNumber = prep.Phone;
            string requesterName = null;
            EmployeeDB employeeDb = null;
            if (displayName == null)
            {
                employeeDb = new EmployeeDB(employeeId, brokerId);
                employeeDb.Retrieve();
                requesterName = employeeDb.FullNmWithMNmAndSuffix;
            }
            else
            {
                requesterName = displayName;
            }

            string nmls;
            string fromAddress = null;
            if (employeeDb == null)
            {
                fromAddress = DefaultConsumerEmailAddress(employeeId, brokerId, out nmls);
            }
            else
            {
                fromAddress = DefaultConsumerEmailAddress(employeeDb, out nmls);
            } 

            string loginURL = GetCPLoginUrl(loanData.BrokerDB, loanData.sBranchId);

            string subject = "Action requested for your loan application with " + companyName;
            string location = consumerActionItem.IsSignatureRequested ? "LendersOffice.ObjLib.Edocs.EmailTemplates.ConsumerDocumentRequestCreationEmailTemplate.xslt" : "LendersOffice.ObjLib.Edocs.EmailTemplates.ConsumerDocumentRequest.xslt";
            
            var log = new StringBuilder("Sending notifications for consumers from SendNewConsumerRequest in loan id " + consumerActionItem.sLId);
            log.AppendLine();
            if (false == string.IsNullOrEmpty(appData.aBEmail) && sendBorrowerEmail)
            {
                // 8/17/2016 BS - User consumer portal first name and last name if borrower already has cportal account. Otherwise, use loan file first name and last name. Case 246565
                ConsumerPortalUser cportalUser = ConsumerPortalUser.Retrieve(brokerId, appData.aBEmail);
                string consumerName = (cportalUser == null) ? appData.aBFirstNm + " " + appData.aBLastNm : cportalUser.FirstName + " " + cportalUser.LastName;

                var parameterXml = new XDocument(
                    new XElement("DocumentNotificationData",
                            new XElement("LoginUrl", loginURL),
                            new XElement("CompanyName", companyName),
                            new XElement("CPTempPassword", borrowerTempPassword),
                            new XElement("ConsumerName", consumerName),
                            new XElement("RequesterName", requesterName),
                            new XElement("PhoneNumber", phoneNumber),
                            new XElement("Email", appData.aBEmail),
                            new XElement("TempPasswordValidLength", ConstApp.CPTempPasswordValidLengthInDays),
                            new XElement("NMLS", nmls),
                            new XElement("IsForMultipleDocuments", isForMultipleDocuments)
                  )
                );
                SendEmailToBorrower(location, loanData.BrokerDB.BrokerID, parameterXml, subject, appData.aBEmail, fromAddress);

                var firstThree = string.IsNullOrEmpty(consumerName) ? "empty or null" : consumerName.PadRight(4, ' ').Substring(0, 3) + " (first three letters)";
                var emailLen = string.IsNullOrEmpty(appData.aBEmail) ? "empty or null" : appData.aBEmail.Length + "";
                var borrOrCoborr = "borrower";
                var hasPassword = string.IsNullOrEmpty(borrowerTempPassword) ? "email does NOT contain a temporary password." : "email contains a temporary password";
                log.AppendLine("Queued up a login notification email to " + borrOrCoborr + " in app: " + appData.aAppId + " name: " + firstThree + " email length: " + emailLen + ", " + hasPassword);
            }
            if (false == string.IsNullOrEmpty(aCEmail) && appData.aBEmail.ToLower() != aCEmail.ToLower() && sendCoborrowerEmail)
            {
                // 8/17/2016 BS - User consumer portal first name and last name if borrower already has cportal account. Otherwise, use loan file first name and last name. Case 246565
                ConsumerPortalUser cportalCoUser = ConsumerPortalUser.Retrieve(brokerId, appData.aCEmail);
                string coBorrName = (cportalCoUser == null) ? appData.aCFirstNm + " " + appData.aCLastNm : cportalCoUser.FirstName + " " + cportalCoUser.LastName;

                var parameterXml = new XDocument(
                    new XElement("DocumentNotificationData",
                            new XElement("LoginUrl", loginURL),
                            new XElement("CompanyName", companyName),
                            new XElement("CPTempPassword", coborrowerTempPassword),
                            new XElement("ConsumerName", coBorrName),
                            new XElement("RequesterName", requesterName),
                            new XElement("PhoneNumber", phoneNumber),
                            new XElement("Email", aCEmail),
                            new XElement("TempPasswordValidLength", ConstApp.CPTempPasswordValidLengthInDays)
                  )
                );
                SendEmailToBorrower(location, loanData.BrokerDB.BrokerID, parameterXml, subject, aCEmail, fromAddress);

                var firstThree = string.IsNullOrEmpty(coBorrName) ? "empty or null" : coBorrName.PadRight(4, ' ').Substring(0, 3) + " (first three letters)";
                var emailLen = string.IsNullOrEmpty(aCEmail) ? "empty or null" : aCEmail.Length + "";
                var borrOrCoborr = "coborrower";
                var hasPassword = string.IsNullOrEmpty(coborrowerTempPassword) ? "email does NOT contain a temporary password." : "email contains a temporary password";
                log.AppendLine("Queued up a login notification email to " + borrOrCoborr + " in app: " + appData.aAppId + " name: " + firstThree + " email length: " + emailLen + ", " + hasPassword);
            }

            Tools.LogInfo(log.ToString());
        }

        private static ConsumerPortalUser GetCP2Consumer(Guid brokerId, Guid portalId, string email, out string tempPassword)
        {
            tempPassword = null;
            if (string.IsNullOrEmpty(email))
            {
                return null;
            }
            ConsumerPortalUser user = ConsumerPortalUser.Retrieve(brokerId, email);
            if (user == null)
            {
                user = ConsumerPortalUser.Create(brokerId, email);
                tempPassword = user.GenerateTempPassword();
                user.Save();
            }
            return user;
        }

        private static void SendShareEmail(ConsumerPortalUser user, string tempPassword, CPageData loanData, SharedDocumentRequest req)
        {
            IPreparerFields prep = loanData.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            string companyName = prep.CompanyName;
            string phoneNumber = prep.Phone;
            string requesterName = null;
            Guid employeeId;
            Guid brokerId;
            EmployeeDB employeeDb = null;
            if (BrokerUserPrincipal.CurrentPrincipal == null)
            {
                employeeDb = new EmployeeDB(req.CreatorEmployeeId, req.BrokerId);
                employeeDb.Retrieve();

                requesterName = employeeDb.FullNmWithMNmAndSuffix;
                employeeId = req.CreatorEmployeeId;
                brokerId = req.BrokerId;
            }
            else
            {
                requesterName = BrokerUserPrincipal.CurrentPrincipal.DisplayName;
                employeeId = BrokerUserPrincipal.CurrentPrincipal.EmployeeId;
                brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            }
            

            string nmls;
            string fromAddress = null;
            if (employeeDb == null)
            {
                fromAddress = DefaultConsumerEmailAddress(employeeId, brokerId, out nmls);
            }
            else
            {
                fromAddress = DefaultConsumerEmailAddress(employeeDb, out nmls);
            }

            string loginURL = GetCPLoginUrl(loanData.BrokerDB, loanData.sBranchId);

            string subject = "Action required for your loan application with " + companyName;
            string location = "LendersOffice.ObjLib.Edocs.EmailTemplates.ConsumerShareRequest.xslt";


            var parameterXml = new XDocument(new XElement("DocumentNotificationData",
                            new XElement("LoginUrl", loginURL),
                            new XElement("CompanyName", companyName),
                            new XElement("CPTempPassword", tempPassword),
                            new XElement("ConsumerName", user.FirstName + " " + user.LastName),
                            new XElement("RequesterName", requesterName),
                            new XElement("PhoneNumber", phoneNumber),
                            new XElement("NMLS", nmls),
                            new XElement("Email", user.Email),
                            new XElement("TempPasswordValidLength", ConstApp.CPTempPasswordValidLengthInDays)
                  ));

            SendEmailToBorrower(location, loanData.BrokerDB.BrokerID, parameterXml, subject, user.Email, fromAddress);
        }

        public static void NotifyBorrowersAboutSharedDoc(SharedDocumentRequest req)
        {
            CPageData pageData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(req.sLId, typeof(EDocumentNotifier));
            pageData.InitLoad();
            CAppData app = pageData.GetAppData(req.aAppId);

            var borrowers = new[] { 
               new { Email = app.aBEmail, FirstName = app.aBFirstNm, LastName = app.aBLastNm },
               new { Email = app.aCEmail, FirstName = app.aCFirstNm, LastName = app.aCLastNm }
            };


            foreach (var borrower in borrowers.DistinctBy(p=>p.Email.ToLower()))
            {
                if (string.IsNullOrEmpty(borrower.Email))
                {
                    continue;
                }

                string password = "";
                ConsumerPortalUser user = ConsumerPortalUser.Retrieve(req.BrokerId, borrower.Email);
                if (user == null)
                {
                    user = ConsumerPortalUser.Create(req.BrokerId, borrower.Email);
                    user.FirstName = borrower.FirstName;
                    user.LastName = borrower.LastName;
                    password = user.GenerateTempPassword();
                    user.Save();
                }

                ConsumerPortalUser.LinkLoan(req.BrokerId, user.Id, req.sLId);
                SendShareEmail(user, password, pageData, req);
            }

            req.MarkAsNotified();
        }


        private static void SetupAccountForCP1(Guid brokerId, string sEmail, Guid loanId, Guid appId, bool isBorrower, out string tempPassword)
        {
            tempPassword = "";
            if (string.IsNullOrEmpty(sEmail))
            {
                return;
            }

            string sTempPassword = ("p" + Guid.NewGuid().ToString().Substring(1, 5)).ToLower();
            string passwordHash = Tools.HashOfPassword(sTempPassword);
            SqlParameter paramReturn = new SqlParameter("@ReturnValue", DbType.Int32);
            paramReturn.Direction = ParameterDirection.ReturnValue;

            SqlParameter[] parameters = {
                                            new SqlParameter("@oldEmail", string.Empty),
                                            new SqlParameter("@newEmail", sEmail),
                                            new SqlParameter("@loanId", loanId),
                                            new SqlParameter("@appId", appId),
                                            new SqlParameter("@isBorrower", isBorrower),
                                            new SqlParameter("@hashPasswordForNewAcc", passwordHash),
                                            paramReturn
                                        };

            StoredProcedureHelper.ExecuteScalar(brokerId, "CP_CheckAndUpdateConsumerAccount", parameters);
            bool bIsNewAccount = ((int)paramReturn.Value == 1);
            if (bIsNewAccount)
            {
                tempPassword = sTempPassword;
            }
            return;
        }

    }



   
}
