﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text;
using System.Xml;
using System.Runtime.Serialization;
using DataAccess;
using LendersOffice.Common;

namespace EDocs
{
    /// <summary>
    /// // 11/12/2010 dd - WARNING: Since we are saving the enum description in the xml, DO NOT rename the enum description
    /// once it is define and in use in production.
    /// </summary>
    public enum E_EDocumentAnnotationItemType
    {
        Highlight = 0,
        Notes = 1,
        Signature = 2,
        Initial = 3,
        SignedDate = 4
    }

    public enum E_DocumentAnnotationNoteStateType
    {
        Minimized = 0,
        Maximized = 1
    }

    [DataContract]
    public class EDocumentAnnotationItem
    {
        public EDocumentAnnotationItem()
        {
            CreatedDate = DateTime.Now;
            LastModifiedDate = DateTime.Now;
        }
        [DataMember]
        public Guid Id { get; set; }

        private Rectangle m_rectangle = null;

        [DataMember]
        public bool ApplyToPDF
        {
            get;
            set; 
        }

        public Rectangle Rectangle
        {
            get
            {
                if (null == m_rectangle)
                {
                    m_rectangle = new Rectangle(0, 0, 0, 0);
                }
                return m_rectangle;
            }
            set
            {
                m_rectangle = value;
            }
        }

        [DataMember(Name="pg")]
        public int PageNumber { get; set; }

        public E_EDocumentAnnotationItemType ItemType { get; set; }
        
        [DataMember(Name="cssClass")]
        public string ItemType_rep
        {
            get
            {
                return MapType(ItemType);
            }
            set
            {
                ItemType = MapType(value);
            }
        }

        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }

        [DataMember(Name="LastModifiedOn")]
        public string LastModifiedDate_rep
        {
            get
            {
                return LastModifiedDate.ToShortDateString();
            }
            set
            {
                LastModifiedDate = DateTime.Parse(value);
            }
        }
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// Used only when ItemType is  Note and the NoteStateType is Minimized
        /// This stores the maximized size of the note.
        /// </summary>
        [DataMember(Name="width")]
        public float MaximizedNoteWidth { get; set; }
        
        /// <summary>
        /// Used only when ItemType is  Note and the NoteStateType is Minimized
        /// This stores the maximized size of the note.
        /// </summary>
        [DataMember(Name="height")]
        public float MaximizedNoteHeight { get; set; }

        [DataMember]
        public string Text { get; set; }

        public E_DocumentAnnotationNoteStateType NoteStateType { get; set; }

        [DataMember(Name="NoteStateType")]
        public string NoteStateType_rep
        {
            get
            {
                return MapState(NoteStateType);
            }
            set
            {
                NoteStateType = MapState(value);
            }
        }

        [DataMember]
        public E_AgentRoleT? AssociatedRole
        {
            get;
            set;
        }

        [DataMember]
        public E_BorrowerModeT? AssociatedBorrower
        {
            get;
            set;
        }

        [DataMember]
        public string RecipientDescription
        {
            get
            {
                if (this.AssociatedRole.HasValue)
                {
                    return LendersOffice.Rolodex.RolodexDB.GetTypeDescription(this.AssociatedRole.Value, useShortened: true);
                }
                else if (this.AssociatedBorrower.HasValue)
                {
                    if (this.AssociatedBorrower.Value == E_BorrowerModeT.Borrower)
                    {
                        return "Borrower";
                    }
                    else
                    {
                        return "Coborrower";
                    }
                }

                return string.Empty;
            }

            set
            {
            }
        }

        public EDocumentAnnotationItem CreateDeepCopy()
        {
            var copier = new System.Xml.Linq.XDocument();
            using (XmlWriter writer = copier.CreateWriter())
            {
                this.WriteXml(writer);
            }

            var copy = new EDocumentAnnotationItem();
            using (XmlReader reader = copier.CreateReader())
            {
                copy.ReadXml(reader);
            }

            return copy;
        }

        #region IXmlSerializable Members

        public const string ElementName = "annotation";
        public void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name !=ElementName)
            {
                reader.Read();
                if (reader.EOF) return;
            }
            CreatedDate = ReadDateTime(reader, "created_date");
            CreatedBy = ReadString(reader, "created_by");
            LastModifiedDate = ReadDateTime(reader, "last_modified_date");
            LastModifiedBy = ReadString(reader, "last_modified_by");
            ItemType = ReadEnum(reader, "type", E_EDocumentAnnotationItemType.Notes);
            NoteStateType = ReadEnum(reader, "note_state", E_DocumentAnnotationNoteStateType.Minimized);
            Id = ReadGuid(reader, "id");
            Text = ReadString(reader, "text");
            MaximizedNoteWidth = ReadFloat(reader, "width",150);
            MaximizedNoteHeight = ReadFloat(reader, "height",150);
            this.AssociatedBorrower = ReadNullableEnum<E_BorrowerModeT>(reader, "associatedBorrower");
            this.AssociatedRole = ReadNullableEnum<E_AgentRoleT>(reader, "associatedRole");

            float lx = ReadFloat(reader, "lx", 0);
            float ly = ReadFloat(reader, "ly", 0);
            float ux = ReadFloat(reader, "ux", 0);
            float uy = ReadFloat(reader, "uy", 0);

            PageNumber = ReadInt(reader, "page", 1);

            // 5/12/2010 dd - Normalize coordinate so that lx < ux and ly < uy
            Rectangle.Left = lx <= ux ? lx : ux;
            Rectangle.Right = lx <= ux ? ux : lx;
            Rectangle.Bottom = ly <= uy ? ly : uy;
            Rectangle.Top = ly <= uy ? uy : ly;







            if (reader.IsEmptyElement)
            {
                return;
            }

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Text)
                {
                    Text = reader.Value;
                    continue;
                }
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == ElementName)
                {
                    return;
                }
            }
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement(ElementName);
            WriteAttribute(writer, "id", Id);
            WriteAttribute(writer, "created_date", CreatedDate);
            WriteAttribute(writer, "created_by", CreatedBy);
            WriteAttribute(writer, "last_modified_date", LastModifiedDate);
            WriteAttribute(writer, "last_modified_by", LastModifiedBy);
            WriteAttribute(writer, "type", ItemType);
            WriteAttribute(writer, "note_state", NoteStateType);
            WriteAttribute(writer, "text", Text);
            WriteAttribute(writer, "width", MaximizedNoteWidth);
            WriteAttribute(writer, "height", MaximizedNoteHeight);
            WriteAttribute(writer, "associatedBorrower", this.AssociatedBorrower);
            WriteAttribute(writer, "associatedRole", this.AssociatedRole);

            if (null != Rectangle)
            {
                WriteAttribute(writer, "lx", Rectangle.Left);
                WriteAttribute(writer, "ly", Rectangle.Bottom);
                WriteAttribute(writer, "ux", Rectangle.Right);
                WriteAttribute(writer, "uy", Rectangle.Top);
            }
 
            WriteAttribute(writer, "page", PageNumber);

            if (string.IsNullOrEmpty(Text) == false)
            {
                writer.WriteString(Text);
            }
            writer.WriteEndElement();
        }

        private T ReadEnum<T>(string s, T defaultValue) where T : struct
        {
            T value = defaultValue;

            if (!string.IsNullOrEmpty(s))
            {
                try
                {
                    value = (T)Enum.Parse(typeof(T), s);
                }
                catch (ArgumentException)
                {
                }
                catch (OverflowException)
                {
                }
            }
            return value;


        }

        private T ReadEnum<T>(XmlReader reader, string attributeName, T defaultValue) where T : struct
        {
            string s = reader.GetAttribute(attributeName);
            return ReadEnum(s, defaultValue);
        }

        private T? ReadNullableEnum<T>(XmlReader reader, string attributeName) where T : struct
        {
            string s = reader.GetAttribute(attributeName);
            if (string.IsNullOrEmpty(s))
            {
                return null;
            }
            else
            {
                return (T)Enum.Parse(typeof(T), s);
            }
        }

        private DateTime ReadDateTime(XmlReader reader, string attributeName) 
        {
            string s = reader.GetAttribute(attributeName);

            DateTime dt = DateTime.MinValue;
            if (DateTime.TryParse(s, out dt) == false)
            {
                dt = DateTime.MinValue;
            }
            return dt;
        }
        private float ReadFloat(XmlReader reader, string attributeName, float defaultValue)
        {
            string s = reader.GetAttribute(attributeName);

            float value;
            if (!float.TryParse(s, out value))
            {
                value = defaultValue;
            }
            return value;
        }
        private int ReadInt(XmlReader reader, string attributeName, int defaultValue)
        {
            string s = reader.GetAttribute(attributeName);

            int value;
            if (!int.TryParse(s, out value))
            {
                value = defaultValue;
            }
            return value;
        }
        private bool ReadBool(XmlReader reader, string attributeName)
        {
            string s = reader.GetAttribute(attributeName);
            bool v = false;

            if (!string.IsNullOrEmpty(s))
            {
                s = s.ToLower();
                if (s == "true")
                {
                    v = true;
                }
                else if (s == "false")
                {
                    v = false;
                }
            }
            return v;
        }
        private string ReadString(XmlReader reader, string attributeName)
        {
            string s = reader.GetAttribute(attributeName);
            if (string.IsNullOrEmpty(s))
            {
                s = string.Empty;
            }
            return s;
        }

        private Guid ReadGuid(XmlReader reader, string attributeName)
        {
            string s = reader.GetAttribute(attributeName);
            return new Guid(s);
        }

        private void WriteAttribute(XmlWriter writer, string attributeName, float v)
        {
            WriteAttribute(writer, attributeName, v.ToString());
        }
        private void WriteAttribute(XmlWriter writer, string attributeName, string attributeValue)
        {
            if (!string.IsNullOrEmpty(attributeValue))
            {
                writer.WriteAttributeString(attributeName, attributeValue);
            }
        }
        private void WriteAttribute(XmlWriter writer, string attributeName, Enum type)
        {
            if (type != null)
            {
                WriteAttribute(writer, attributeName, type.ToString());
            }
        }
        private void WriteAttribute(XmlWriter writer, string attributeName, Guid g)
        {
            WriteAttribute(writer, attributeName, g.ToString());
        }
        private void WriteAttribute(XmlWriter writer, string attributeName, bool b)
        {
            WriteAttribute(writer, attributeName, b ? "true" : "false");
        }
        private void WriteAttribute(XmlWriter writer, string attributeName, DateTime dt)
        {
            if (dt == DateTime.MinValue || dt == DateTime.MaxValue)
            {
                return;
            }
            WriteAttribute(writer, attributeName, dt.ToString());
        }
        #endregion


        #region helper methods for js serialization
        private string MapType(E_EDocumentAnnotationItemType type)
        {
            switch (type)
            {
                case E_EDocumentAnnotationItemType.Highlight:
                    return TYPE_HIGHLIGHT;
                case E_EDocumentAnnotationItemType.Notes:
                    return TYPE_NOTE;
                case E_EDocumentAnnotationItemType.Signature:
                    return TYPE_ESIGN_SIGNATURE;
                case E_EDocumentAnnotationItemType.Initial:
                    return TYPE_ESIGN_INITIAL;
                case E_EDocumentAnnotationItemType.SignedDate:
                    return TYPE_ESIGN_DATE;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        public E_EDocumentAnnotationItemType MapType(string type)
        {
            switch (type)
            {
                case TYPE_NOTE:
                    return E_EDocumentAnnotationItemType.Notes;
                case TYPE_HIGHLIGHT:
                    return E_EDocumentAnnotationItemType.Highlight;
                case TYPE_ESIGN_SIGNATURE:
                    return E_EDocumentAnnotationItemType.Signature;
                case TYPE_ESIGN_INITIAL:
                    return E_EDocumentAnnotationItemType.Initial;
                case TYPE_ESIGN_DATE:
                    return E_EDocumentAnnotationItemType.SignedDate;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Invalid string note type: " + type);
            }
        }

        public string MapState(E_DocumentAnnotationNoteStateType type)
        {
            switch (type)
            {
                case E_DocumentAnnotationNoteStateType.Maximized:
                    return STATE_MAX;
                case E_DocumentAnnotationNoteStateType.Minimized:
                    return STATE_MIN;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        public E_DocumentAnnotationNoteStateType MapState(string type)
        {
            switch (type)
            {
                case STATE_MAX:
                    return E_DocumentAnnotationNoteStateType.Maximized;
                case STATE_MIN:
                //might be missing from new highlight fields...
                default:
                    return E_DocumentAnnotationNoteStateType.Minimized;
            }
        }

        private const string TYPE_HIGHLIGHT = "pdf-field-highlight";
        private const string TYPE_NOTE = "pdf-field-note";
        private const string TYPE_ESIGN_SIGNATURE = "pdf-field-esign-signature";
        private const string TYPE_ESIGN_INITIAL = "pdf-field-esign-initial";
        private const string TYPE_ESIGN_DATE = "pdf-field-esign-date";
        private const string STATE_MAX = "maximized";
        private const string STATE_MIN = "minimized";
        #endregion          

        #region coordinates
        [DataMember]
        private float lx
        {
            get { return Rectangle.Left; }
            set { Rectangle.Left = value; }
        }
        [DataMember]
        private float ly
        {
            get { return Rectangle.Bottom; }
            set { Rectangle.Bottom = value; }
        }
        [DataMember]
        private float ux
        {
            get { return Rectangle.Right; }
            set { Rectangle.Right = value; }
        }
        [DataMember]
        private float uy
        {
            get { return Rectangle.Top; }
            set { Rectangle.Top = value; }
        }

        #endregion 
    }
}
