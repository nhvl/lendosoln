﻿// <copyright file="EDocumentPNGHandler.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   11/27/2014 3:26:22 PM 
// </summary>
namespace EDocs.Handlers
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Web;
    using DataAccess;
    using EDocs.Utils;
    using LendersOffice.Common;
    using LendersOffice.HttpModule;
    using LendersOffice.Security;

    /// <summary>
    /// Serves EDocument PNGs.
    /// </summary>
    public class EDocumentPNGHandler : IHttpHandler
    {
        /// <summary>
        /// Gets a value indicating whether the application to reuse a single controller for multiple instances.
        /// </summary>
        /// <value>Gets a value indicating whether the handler instance can be reused for multiple request.</value>
        public bool IsReusable
        {
            get { return true; }
        }

        /// <summary>
        /// Processes a request.
        /// </summary>
        /// <param name="context">The context of the current request.</param>
        public void ProcessRequest(HttpContext context)
        {
            Stopwatch sw = Stopwatch.StartNew();
            string encryptedKey = RequestHelper.GetSafeQueryString("pngkey");
            bool isThumb = RequestHelper.GetBool("thumb");
            
            PNGEncryptedData data = new PNGEncryptedData(encryptedKey);
            PerformanceMonitorItem.LogAndRestart("Decrypt", sw);

            string expectedETag = string.Format("{0}_{1}_{2}_{3}", Tools.ShortenGuid(data.DocumentId), data.PageNumber, isThumb, data.Version);
            string foundETag = context.Request.Headers["If-None-Match"];

            string expectedLogin = PrincipalFactory.CurrentPrincipal.LoginNm;

            if (!data.LoginNm.Equals(expectedLogin, StringComparison.OrdinalIgnoreCase))
            {
                context.Response.Clear();
                context.Response.StatusCode = (int)System.Net.HttpStatusCode.BadRequest;
                context.Response.SuppressContent = true;
                sw.Stop();
                return;
            }
                
            if (expectedETag.Equals(foundETag))
            {
                context.Response.Clear();
                context.Response.StatusCode = (int)System.Net.HttpStatusCode.NotModified;
                context.Response.SuppressContent = true;
                sw.Stop();
                return;
            }

            PerformanceMonitorItem.LogAndRestart("SendImg", sw);
            string path = string.Empty;

            if (data.Version == PNGEncryptedData.NonDestructiveEdocsVersion)
            {
                // 6/23/2015 dd - Using Non Destructive Edocs format.
                path = EDocumentViewer.GetNonDestructivePngPath(data.DocumentId, data.PageNumber, isThumb);
            }
            else
            {
                // 6/23/2015 dd - Use legacy method to retrieve PNG
                if (string.IsNullOrEmpty(data.PNGKey))
                {
                    this.SendNotReadyImage(context, isThumb);
                    return;
                }

                try
                {
                    path = EDocumentViewer.GetPngPath(encryptedKey, isThumb);
                }
                catch (FileNotFoundException)
                {
                    Tools.LogInfo("Image not found : " + data);
                    this.SendNotReadyImage(context, isThumb);
                    return;
                }
            }

            PerformanceMonitorItem.LogAndRestart("GetPath", sw);

            context.Response.Cache.SetCacheability(HttpCacheability.ServerAndPrivate);
            context.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            context.Response.Cache.SetETag(expectedETag);
            context.Response.Cache.SetExpires(DateTime.Now.AddHours(8));
            context.Response.ContentType = "image/png";
            context.Response.Cache.SetOmitVaryStar(true);

            FileInfo fileInfo = new FileInfo(path);

            context.Response.Buffer = false;
            context.Response.BufferOutput = false;
            context.Response.AddHeader("Content-Length", fileInfo.Length.ToString());

            // Do not exceed 80k as this will be allocated to the LOH.
            int blockSize = 32768;

            if (fileInfo.Length < blockSize)
            {
                blockSize = (int)fileInfo.Length;
            }

            byte[] buffer = new byte[blockSize];
            int bytesRead;

            sw.Reset();
            sw.Start();
            using (FileStream fs = fileInfo.OpenRead())
            {
                while ((bytesRead = fs.Read(buffer, 0, blockSize)) > 0)
                {
                    context.Response.OutputStream.Write(buffer, 0, bytesRead);
                }

                context.Response.OutputStream.Flush();
            }

            sw.Stop();
            PerformanceMonitorItem.AddTimingDetailsToCurrent("Write File To Response", sw.ElapsedMilliseconds);
        }

        /// <summary>
        /// Sends a not ready image picture to the client.
        /// </summary>
        /// <param name="context">The HTTP context to use.</param>
        /// <param name="isThumb">Whether the image is a thumbnail or not.</param>
        private void SendNotReadyImage(HttpContext context, bool isThumb)
        {
            // 2/10/2015 dd - The PNG for this page is not ready yet. Return a place holder.
            string placeHolder = Tools.GetServerMapPath(isThumb ? "~/images/ImageNotReadyThumbs.png" : "~/images/ImageNotReady.png");

            context.Response.Clear();
            context.Response.ContentType = "image/png";

            FileInfo placeHolderFileInfo = new FileInfo(placeHolder);
            context.Response.AddHeader("Content-Length", placeHolderFileInfo.Length.ToString());

            context.Response.WriteFile(placeHolder);
            context.Response.Flush();
            context.Response.End();
        }
    }
}
