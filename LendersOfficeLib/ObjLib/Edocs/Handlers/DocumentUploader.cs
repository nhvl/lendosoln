﻿namespace LendersOffice.ObjLib.Edocs.Handlers
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Web;
    using System.Xml;
    using DataAccess;
    using EDocs;
    using EDocs.Contents;
    using LendersOffice.Common;
    using LendersOffice.Drivers.FileSystem;
    using LendersOffice.ObjLib.Edocs.AutoSaveDocType;
    using LendersOffice.ObjLib.UserDropbox;
    using LendersOffice.Security;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Provides handling for document uploads.
    /// </summary>
    public class DocumentUploader : IHttpHandler
    {
        /// <summary>
        /// Gets the size of the buffer when reading a document into memory.
        /// </summary>
        private const int ByteBufferLength = 16384;

        /// <summary>
        /// Enumerates the possible types of document upload requests.
        /// </summary>
        private enum DocumentUploadRequestType
        {
            UploadDocument = 0,
            UploadPdfToDropbox = 1,
            UpdateGenericDocContent = 2,
            UploadDocumentRequestForm = 3,
            UploadPaymentStatement = 4
        }

        /// <summary>
        /// Gets a value indicating whether the handler is reusable.
        /// </summary>
        public bool IsReusable => true;

        /// <summary>
        /// Processes the document upload request.
        /// </summary>
        /// <param name="context">
        /// The context for the document upload.
        /// </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("LendingQBStyleCop.LendingQBCustomRules", "LB1003:CannotSwallowGeneralException", Justification = "Return consistent error to client.")]
        public void ProcessRequest(HttpContext context)
        {
            var method = this.GetRequestType(context.Request);
            if (method == null)
            {
                this.SendError(context.Response, ErrorMessages.Generic);
                return;
            }

            // This handler currently supports single-document uploads.
            // Callers should transmit each file separately to avoid
            // sending large POST requests.
            if (context.Request.Files.Count != 1)
            {
                this.SendError(context.Response, ErrorMessages.Generic);
                return;
            }

            try
            {
                switch (method.Value)
                {
                    case DocumentUploadRequestType.UploadDocument:
                        this.UploadDocument(context);
                        break;
                    case DocumentUploadRequestType.UploadPdfToDropbox:
                        this.UploadPdfToDropbox(context);
                        break;
                    case DocumentUploadRequestType.UpdateGenericDocContent:
                        this.UpdateGenericDocContent(context);
                        break;
                    case DocumentUploadRequestType.UploadDocumentRequestForm:
                        this.UploadDocumentRequestForm(context);
                        break;
                    case DocumentUploadRequestType.UploadPaymentStatement:
                        this.UploadPaymentStatement(context);
                        break;
                    default:
                        throw new UnhandledEnumException(method.Value);
                }
            }
            catch (Exception e)
            {
                this.SendError(context.Response, ErrorMessages.Generic);
                Tools.LogError("Unexpected exception processing request type " + method, e);
            }
        }

        /// <summary>
        /// Uploads a document to a loan file.
        /// </summary>
        /// <param name="context">
        /// The context for the document upload.
        /// </param>
        private void UploadDocument(HttpContext context)
        {
            var principal = PrincipalFactory.CurrentPrincipal;
            if (principal.BrokerDB.IsEDocDoctypeMergeEnabled || !principal.HasPermission(Permission.CanViewEDocs))
            {
                // dd 7/30/2015  As of right now no lender is using this feature. The feature was request by PML0177 (OPM 83484), 
                // however PML0177 is no active and also right before they disable the feature was turn off in 2012.
                this.SendError(context.Response, ErrorMessages.GenericAccessDenied);
                return;
            }

            var postedFile = context.Request.Files[0];
            var filename = postedFile.FileName;

            try
            {
                var fileBytes = Tools.ReadFully(postedFile.InputStream, ByteBufferLength);
                var fileData = new FileData(filename, fileBytes);

                if (context.Request["RestrictToUcd"].ToNullableBool() ?? false)
                {
                    // When we're restricting the upload to only UCD files, fail early if possible.
                    if (!UcdGenericEDocument.IsUcdStringValid(fileData.Contents, principal.BrokerId))
                    {
                        this.SendError(context.Response, "Only valid UCD files can be uploaded: " + filename);
                        return;
                    }
                }

                var loanId = context.Request["loanid"].ToNullable<Guid>(Guid.TryParse).ForceValue();
                var appId = context.Request["aAppId"].ToNullable<Guid>(Guid.TryParse).ForceValue();

                var docTypeId = context.Request["DocTypeId"].ToNullable<int>(int.TryParse).ForceValue();
                var internalDescription = context.Request["InternalDescription"] ?? string.Empty;
                var publicDescription = context.Request["PublicDescription"] ?? string.Empty;

                var debugBuilder = new StringBuilder();
                var docId = EDocsUpload.UploadDoc(principal, fileData, loanId, appId, docTypeId, internalDescription, publicDescription, debugBuilder);

                var startTicks = context.Request["StartTicks"].ToNullable<long>(long.TryParse);
                if (startTicks.HasValue)
                {
                    var durationSinceUserClickedButton = (DateTime.Now.Ticks - startTicks.Value) / 10000L;
                    debugBuilder.AppendLine($"Total time since user click Upload={durationSinceUserClickedButton} ms.");
                }

                Tools.LogInfo(debugBuilder.ToString());

                var results = new Dictionary<string, object>(1)
                {
                    ["DocumentId"] = docId
                };
                this.SetResult(context.Response, results);
            }
            catch (XmlException ex)
            {
                Tools.LogWarning("Failure parsing xml", ex);
                this.SendError(context.Response, filename + " is not a valid xml file.");
            }
            catch (InvalidPDFFileException)
            {
                this.SendError(context.Response, filename + " is not a valid pdf.");
            }
            catch (InsufficientPdfPermissionException)
            {
                this.SendError(context.Response, filename + " is password protected.");
            }
            catch (CBaseException cbe) when (cbe.UserMessage == ErrorMessages.EDocs.InvalidXML || cbe.UserMessage == ErrorMessages.EDocs.InvalidUcd)
            {
                this.SendError(context.Response, "Unexpected XML Format: " + filename);
            }
            catch (CBaseException e)
            {
                this.SendError(context.Response, "Could not process " + filename + " Error: " + e.UserMessage);
                Tools.LogError("Unexpected exception from pdf reader", e);
            }
        }

        /// <summary>
        /// Uploads a PDF to a user's dropbox.
        /// </summary>
        /// <param name="context">
        /// The context for the document upload.
        /// </param>
        private void UploadPdfToDropbox(HttpContext context)
        {
            var postedFile = context.Request.Files[0];
            var fileBytes = Tools.ReadFully(postedFile.InputStream, ByteBufferLength);

            Tools.ScanForVirus(fileBytes);

            var uploadDate = DateTime.Now;
            var principal = PrincipalFactory.CurrentPrincipal;

            Dropbox.AddFile(principal.UserId, principal.BrokerId, postedFile.FileName, uploadDate, fileBytes);
        }

        /// <summary>
        /// Updates the content of a generic document.
        /// </summary>
        /// <param name="context">
        /// The context for the document upload.
        /// </param>
        private void UpdateGenericDocContent(HttpContext context)
        {
            var principal = PrincipalFactory.CurrentPrincipal;
            if (!principal.HasPermission(Permission.CanViewEDocs))
            {
                this.SendError(context.Response, ErrorMessages.GenericAccessDenied);
                return;
            }

            try
            {
                var loanId = context.Request["loanid"].ToNullable<Guid>(Guid.TryParse).ForceValue();
                var docId = context.Request["DocumentId"].ToNullable<Guid>(Guid.TryParse).ForceValue();

                EDocsUpload.AssertUploadEDocsWorkflowPrivileges(principal, loanId);

                var repo = EDocumentRepository.GetUserRepository();
                var doc = repo.GetGenericDocumentById(docId);

                if (doc.LoanId != loanId)
                {
                    this.SendError(context.Response, ErrorMessages.GenericAccessDenied);
                    return;
                }

                var postedFile = context.Request.Files[0];

                var filepath = TempFileUtils.NewTempFile();
                postedFile.SaveAs(filepath.Value);

                doc.SetContent(filepath.Value, postedFile.FileName);
                repo.Save(doc, principal.UserId);

                FileOperationHelper.Delete(filepath);
            }
            catch (LqbException e)
            {
                Tools.LogError(e);
                this.SendError(context.Response, e.Context.Serialize());
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                this.SendError(context.Response, e.UserMessage);
            }
        }

        /// <summary>
        /// Uploads a document for a consumer portal document request.
        /// </summary>
        /// <param name="context">
        /// The context for the document upload.
        /// </param>
        private void UploadDocumentRequestForm(HttpContext context)
        {
            var principal = PrincipalFactory.CurrentPrincipal;
            if (!principal.HasPermission(Permission.CanViewEDocs))
            {
                this.SendError(context.Response, ErrorMessages.GenericAccessDenied);
                return;
            }

            var postedFile = context.Request.Files[0];
            var filename = postedFile.FileName;

            try
            {
                var loanId = context.Request["LoanId"].ToNullable<Guid>(Guid.TryParse).ForceValue();
                EDocsUpload.AssertUploadEDocsWorkflowPrivileges(principal, loanId);

                var fileBytes = Tools.ReadFully(postedFile.InputStream, ByteBufferLength);

                bool hasPdfOwnerPassword;
                int pageCount;
                EDocumentViewer.GetPdfInfo(fileBytes, out hasPdfOwnerPassword, out pageCount);

                if (hasPdfOwnerPassword)
                {
                    this.SendError(context.Response, filename + " is protected with owner password.");
                    return;
                }

                var docId = Guid.NewGuid().ToString();
                var fullKey = docId + "_" + principal.UserId.ToString();
                AutoExpiredTextCache.AddToCache(fileBytes, new TimeSpan(1, 0, 0), fullKey); // Cache upload file for 1 hr.

                var results = new Dictionary<string, object>(2)
                {
                    ["DocId"] = docId,
                    ["PageCount"] = pageCount
                };
                this.SetResult(context.Response, results);
            }
            catch (InvalidPDFFileException)
            {
                this.SendError(context.Response, filename + " is not a valid pdf.");
            }
            catch (InsufficientPdfPermissionException)
            {
                this.SendError(context.Response, filename + " is password protected.");
            }
            catch (CBaseException)
            {
                this.SendError(context.Response, "Could not read " + filename);
            }
        }

        /// <summary>
        /// Uploads a payment statement document to a loan file.
        /// </summary>
        /// <param name="context">
        /// The context for the document upload.
        /// </param>
        private void UploadPaymentStatement(HttpContext context)
        {
            var loanId = context.Request["loanid"].ToNullable<Guid>(Guid.TryParse).ForceValue();
            var appId = context.Request["aAppId"].ToNullable<Guid>(Guid.TryParse).ForceValue();

            var principal = PrincipalFactory.CurrentPrincipal;
            EDocsUpload.AssertUploadEDocsWorkflowPrivileges(principal, loanId);

            var postedFile = context.Request.Files[0];
            var fileBytes = Tools.ReadFully(postedFile.InputStream, ByteBufferLength);

            Guid? documentId = AutoSaveDocTypeFactory.SavePdfDoc(
                E_AutoSavePage.PaymentStatement,
                fileBytes,
                loanId,
                appId,
                principal);

            var results = new Dictionary<string, object>(1)
            {
                ["DocumentId"] = documentId
            };
            this.SetResult(context.Response, results);
        }

        /// <summary>
        /// Gets the type of the document upload request.
        /// </summary>
        /// <param name="request">
        /// The request for the document upload.
        /// </param>
        /// <returns>
        /// The document request type or null if the request type is unrecognized.
        /// </returns>
        private DocumentUploadRequestType? GetRequestType(HttpRequest request)
        {
            var requestType = request.QueryString["requestType"];

            DocumentUploadRequestType parsedType;
            return requestType.TryParseDefine(out parsedType, ignoreCase: true) ? parsedType : default(DocumentUploadRequestType?);
        }

        /// <summary>
        /// Sets the result of the document upload request.
        /// </summary>
        /// <param name="response">
        /// The response for the document upload.
        /// </param>
        /// <param name="result">
        /// The results of the request.
        /// </param>
        private void SetResult(HttpResponse response, Dictionary<string, object> result)
        {
            response.Write(SerializationHelper.JsonNetAnonymousSerialize(new
            {
                value = result
            }));
        }

        /// <summary>
        /// Sends an error response to the client.
        /// </summary>
        /// <param name="response">
        /// The response for the document upload.
        /// </param>
        /// <param name="message">
        /// The error message to send to the client.
        /// </param>
        private void SendError(HttpResponse response, string message)
        {
            response.Write(SerializationHelper.JsonNetAnonymousSerialize(new
            {
                error = true,
                UserMessage = message
            }));
        }
    }
}
