﻿// <copyright file="ScannerPDFUploader.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Antonio Valencia
//  Date:   6/3/2015 7:56:20 PM
// </summary>
namespace EDocs.Handlers
{
    using System;
    using System.IO;
    using System.Web;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Edocs.DocMagic;
    using LendersOffice.ObjLib.DatabaseMessageQueue;

    /// <summary>
    /// Accepts PDFs for Scanning.
    /// </summary>
    public class ScannerPDFUploader : IHttpHandler
    {
        /// <summary>
        /// Gets a value indicating whether the application to reuse a single controller for multiple instances.
        /// </summary>
        /// <value>Gets a value indicating whether the handler instance can be reused for multiple request.</value>
        public bool IsReusable
        {
            get { return true; }
        }

        /// <summary>
        /// Processes a request.
        /// </summary>
        /// <param name="context">The context of the current request.</param>
        public void ProcessRequest(HttpContext context)
        {
            string token = context.Request.Headers["LQBToken"];

            if (string.IsNullOrEmpty(token))
            {
                context.Response.StatusCode = 412;
                context.Response.TrySkipIisCustomErrors = true;
                context.Response.Write("Unable to locate token.");
                return;
            }

            Guid brokerId;

            try
            {
                brokerId = EncryptionHelper.GetBrokerIdFromBarcodeUploaderToken(token);
            }
            catch (FormatException)
            {
                context.Response.StatusCode = 412;
                context.Response.TrySkipIisCustomErrors = true;
                context.Response.Write("Token Not Recognized.");
                Tools.LogWarning("Could not recognize user token for barcode upload " + token);
                return;
            }

            string fileName = context.Request.Headers["LQBFileName"];

            if (string.IsNullOrEmpty(fileName))
            {
                fileName = string.Format("file_{0}_{1}.pdf", brokerId, DateTime.Now.Ticks);
            }

            string path = TempFileUtils.NewTempFilePath();

            using (Stream output = File.OpenWrite(path))
            using (Stream input = context.Request.InputStream)
            {
                byte[] buffer = new byte[8192];
                int bytesRead;
                while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    output.Write(buffer, 0, bytesRead);
                }
            }

            int pageCount = 0;

            try
            {
                var reader = EDocumentViewer.CreateRandomAccessPdfReader(path);
                pageCount = reader.NumberOfPages;
                Tools.LogInfo(string.Format("Received {0} from {1} with size {2}.", fileName, brokerId, context.Request.InputStream.Length));

                E_AntiVirusResult result = AntiVirusScanner.Scan(path);
                string errorMessage = AntiVirusScanner.GetUserErrorMessage(result);

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    Tools.LogWarning("PDF File had issues " + errorMessage);
                    context.Response.StatusCode = 500;
                    context.Response.TrySkipIisCustomErrors = true;
                    context.Response.Write(errorMessage);
                    return;
                }
            }
            catch (InvalidPDFFileException)
            {
                Tools.LogWarning(string.Format("Received invalid pdf file {0} for {1}", fileName, brokerId));
                context.Response.StatusCode = 500;
                context.Response.TrySkipIisCustomErrors = true;
                context.Response.Write("Received Invalid PDF.");
                return;
            }

            Guid key = Guid.NewGuid();
            FileDBTools.WriteFile(E_FileDB.Temp, key.ToString(), path);

            DocMagicPDFData uploadData = new DocMagicPDFData()
            {
                UploadingUserId = Guid.Empty,
                AppId = Guid.Empty,     // will get from barcode (maybe)
                BrokerId = brokerId,
                LoanId = Guid.Empty,    // will get from barcode (maybe)
                InternalNotes = string.Empty,     // string.Empty, because leaving null causes errors
                Description = string.Empty,       // string.Empty, because leaving null causes errors
                FileDB = E_FileDB.Temp,
                FileDBKey = key,
                PageCount = pageCount,
                IsBarcodeUpload = true,
                DropboxFileName = fileName,
                DropboxUploadD = DateTime.Now
            };

            DBMessageQueue mainQ = new DBMessageQueue(ConstMsg.DocMagicDocSplitterQueue);
            mainQ.Send(key.ToString(), uploadData.Serialize());
        }
    }
}
