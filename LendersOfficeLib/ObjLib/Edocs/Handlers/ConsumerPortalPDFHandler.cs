﻿using System;
using System.Web;
using LendersOffice.Common;
using LendersOffice.ObjLib.ConsumerPortal;
using DataAccess;
using EDocs;
using LendersOffice.Security;
using System.Linq;
namespace LendersOffice.ObjLib.Edocs.Handlers
{
    public class ConsumerPortalPDFHandler : IHttpHandler
    {
        private void GeneratePdf(HttpResponse response, byte[] buffer, string fileName)
        {
            response.Cache.SetExpires(DateTime.Now);
            response.Cache.SetLastModified(DateTime.Now);
            response.Clear();
            response.ContentType = "application/pdf";
            response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
            response.OutputStream.Write(buffer, 0, buffer.Length);
            response.Flush();
            response.End();
        }
        private void GeneratePdf(HttpResponse response, string path, string fileName)
        {
            response.Cache.SetExpires(DateTime.Now);
            response.Cache.SetLastModified(DateTime.Now);
            response.Clear();
            response.ContentType = "application/pdf";
            response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
            response.WriteFile(path);
            response.Flush();
            response.End();
        }
        private void ShowError(HttpContext context, int errorCode)
        {
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            context.Response.Cache.SetNoStore();
            context.Response.Cache.SetExpires(DateTime.MinValue);
            context.Response.StatusCode = errorCode;
            context.Response.End();
        }

        /// <summary>
        /// You will need to configure this handler in the web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            string k = RequestHelper.GetSafeQueryString("k");
            EDocPortalTicket ticket = EDocPortalTicket.GetPortalTicket(k);
            if (ticket == null)
            {
                ShowError(context, 404);
                return;
            }

            System.Threading.Thread.CurrentPrincipal = ticket.GetPrincipal();
                        

            switch (ticket.TicketType)
            {
                case E_TicketType.FaxRequest:
                    HandleFaxRequest(context, ticket.Data, true, ticket.AllowViewSubmittedLoanId);
                    break;
                case E_TicketType.DownloadEDoc:
                    HandleDocDownload(context, ticket.Data, ticket.AllowViewSubmittedLoanId);
                    break;
                case E_TicketType.DownloadRequest:
                    HandleFaxRequest(context, ticket.Data, false, ticket.AllowViewSubmittedLoanId);
                    break;
                case E_TicketType.ESignRequest:
                default:
                    Tools.LogError("Unhandled enum " + ticket.TicketType);
                    ShowError(context, 500);
                    break;
            }
        }


        private void HandleFaxRequest(HttpContext context, string data, bool includeFax, Guid expectedLoanId)
        {
            long requestId;

            if (data.StartsWith("s") && long.TryParse(data.Substring(1), out requestId))
            {
                SharedDocumentRequest req = SharedDocumentRequest.GetRequestById(PrincipalFactory.CurrentPrincipal.BrokerId, requestId);
                if (req.sLId != expectedLoanId)
                {
                    ShowError(context, 403);
                    return;
                }
                req.MarkDocumentAsDownloaded();
                GeneratePdf(context.Response, req.GetDocumentPath(), "document.pdf");
                return;
            }

            else if (!long.TryParse(data, out requestId))
            {
                ShowError(context, 500);
                return;
            }

            ConsumerActionItem actionItem = new ConsumerActionItem(requestId);

            if (actionItem.sLId != expectedLoanId)
            {
                ShowError(context, 403);
                return;
            }

            if (actionItem.ResponseStatus == E_ConsumerResponseStatusT.PendingResponse || actionItem.ResponseStatus == E_ConsumerResponseStatusT.Accepted)
            {
                string path = FileDBTools.CreateCopy(E_FileDB.EDMS, actionItem.PdfFileDbKey.ToString());
                GeneratePdf(context.Response, path, "document.pdf");
            }
            else
            {
                byte[] pdfBytes = ConsumerActionItem.GetFaxCover(requestId, includeFax);
                GeneratePdf(context.Response, pdfBytes, "document.pdf");
            }
        }

        private void HandleDocDownload(HttpContext context, string data, Guid expectedLoanId)
        {
            Guid documentId;
            try
            {
                documentId = new Guid(data);
            }
            catch (FormatException)
            {
                ShowError(context, 500);
                return;
            }

            if (new Guid("99999999-9999-9999-9999-999999999999") == documentId)
            {
                Guid brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
                LenderDisclosurePDFGenerator lde = new LenderDisclosurePDFGenerator(brokerId);
                GeneratePdf(context.Response, lde.SavePdf(), "eSign Disclosure.pdf");
                return;
            }

            try
            {
                EDocumentRepository repo = EDocumentRepository.GetSystemRepository(PrincipalFactory.CurrentPrincipal.BrokerId);
                EDocument doc = repo.GetDocumentById(documentId);
                if (!doc.Folder.FolderRolesList.Any(p => p.RoleId == CEmployeeFields.s_ConsumerId))
                {
                    ShowError(context, 403);
                    return;
                }
                if (doc.LoanId != expectedLoanId)
                {
                    ShowError(context, 403);
                    return;
                }
                string path = doc.GetPDFTempFile_Current();
                GeneratePdf(context.Response, path, "document.pdf");
            }
            catch (AccessDenied)
            {
                ShowError(context, 403);
            }
        }

        #endregion
    }
}
