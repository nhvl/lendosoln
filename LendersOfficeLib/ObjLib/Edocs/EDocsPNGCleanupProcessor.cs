﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using DataAccess;
using LendersOffice.Constants;


namespace EDocs
{
    public class EDocsPNGCleanupProcessor : CommonProjectLib.Runnable.IRunnable
    {
        #region IRunnable Members

        public string Description
        {
            get
            {
                return string.Format("Removes images from edocs that are older than {0} months.", ConstApp.EdocsImagesRemovalAgeInMonths);
            }
        }

        // If a document contains ANY image older than X months, ALL images referenced and previously referenced by that document will be removed
        public void Run()
        {
            IList<EDocument> docs;

            Tools.ResetLogCorrelationId();
            Stopwatch sw = Stopwatch.StartNew();
            int imagesDeletedCount = 0;
            int errorCount = 0;

            docs = EDocument.GetDocumentsWithImagesOlderThanXMonths(ConstApp.EdocsImagesRemovalAgeInMonths);

            if (docs.Count == 0)
            {
                return;
            }

            foreach (var doc in docs)
            {
                try
                {
                    int imagesDeleted = doc.RemoveCachedImages();
                    imagesDeletedCount += imagesDeleted;
                    Tools.LogInfo(String.Format("Finished up images for {0} {1}", doc.DocumentId, imagesDeleted));
                }
                catch (NullReferenceException exc)
                {
                    errorCount++;
                    Tools.LogError("EDocsPNGCleanupProcessor: NullReferenceException. " + exc.Message + Environment.NewLine + exc.StackTrace);
                }
                catch (CBaseException exc)
                {
                    errorCount++;
                    Tools.LogError("EDocsPNGCleanupProcessor: CBaseException. " + exc.DeveloperMessage + Environment.NewLine + exc.StackTrace);
                }
                catch (SqlException exc)
                {
                    errorCount++;
                    Tools.LogError("EDocsPNGCleanupProcessor: SqlException. " + exc.Message + Environment.NewLine + exc.StackTrace);
                }
            }

            sw.Stop();
            Tools.LogInfo("EDocsPNGCleanupProcessor: Processed " + imagesDeletedCount + " successes. " + errorCount + " errors. Executed in " + sw.ElapsedMilliseconds + "ms.");
        }
    }

        #endregion
}
