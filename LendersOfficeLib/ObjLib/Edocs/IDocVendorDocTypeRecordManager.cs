﻿namespace LendersOffice.ObjLib.Edocs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Manage vendor barcode records.
    /// </summary>
    public interface IDocVendorDocTypeRecordManager
    {
        /// <summary>
        /// Get all barcode records.
        /// </summary>
        /// <returns>Return list of records.</returns>
        IEnumerable<IDocVendorDocTypeRecord> ListRecords();

        /// <summary>
        /// Retrieve barcode record by record id.
        /// </summary>
        /// <param name="id">The record id to look up.</param>
        /// <returns>Return barcode record.</returns>
        IDocVendorDocTypeRecord Get(int id);

        /// <summary>
        /// Create new barcode record.
        /// </summary>
        /// <param name="desc">Barcode's decription. </param>
        /// <param name="barcode">Barcode Classification.</param>
        /// <returns>Return barcode record.</returns>
        int Create(string desc, string barcode);

        /// <summary>
        /// Save barcode record to database.
        /// </summary>
        /// <param name="id">The record id to update.</param>
        /// <param name="desc">Barcode's decription. </param>
        /// <param name="barcode">Barcode Classification.</param>
        void Update(int id, string desc, string barcode);

        /// <summary>
        /// Delete barcode record.
        /// </summary>
        /// <param name="id">The record id to delete.</param>
        void Delete(int id);
    }
}
