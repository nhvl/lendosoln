﻿namespace EDocs.Contents
{
    using System;
    using System.Data.Common;
using System.Data.SqlClient;

    /// <summary>
    /// An MS Excel spreadsheet generic doc type.
    /// </summary>
    public class MicrosoftSpreadsheetGenericEDocument : GenericEDocument
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MicrosoftSpreadsheetGenericEDocument"/> class.
        /// </summary>
        /// <param name="reader">A SQL data reader.</param>
        internal MicrosoftSpreadsheetGenericEDocument(DbDataReader reader)
            : base(reader)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MicrosoftSpreadsheetGenericEDocument"/> class.
        /// </summary>
        /// <param name="brokerId">A broker ID.</param>
        internal MicrosoftSpreadsheetGenericEDocument(Guid brokerId)
            : base(brokerId)
        {
        }

        /// <summary>
        /// Gets the spreadsheet file type.
        /// </summary>
        /// <value>The spreadsheet file type.</value>
        public override E_FileType FileType => E_FileType.MicrosoftSpreadsheet;

        /// <summary>
        /// Gets a value indicating whether this generic EDoc type supports updating its contents.
        /// </summary>
        /// <value>A value indicating whether this generic EDoc type supports updating its contents.</value>
        public override bool SupportsContentUpdate => true;

        /// <summary>
        /// Gets the MIME type.
        /// </summary>
        /// <value>The MIME type.</value>
        public override string MIMEType
        {
            get
            {
                if (FileNm.EndsWith(".xlsx", StringComparison.OrdinalIgnoreCase))
                {
                    return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }
                else
                {
                    return "application/vnd.ms-excel";
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether this generic EDoc type is linked to a regular EDoc.
        /// </summary>
        /// <value>A value indicating whether this generic EDoc type is linked to a regular EDoc.</value>
        public override bool IsLinkedToEDoc => false;

        /// <summary>
        /// Gets the user-friendly name for the spreadsheet file type.
        /// </summary>
        /// <value>The user-friendly name for the spreadsheet file type.</value>
        public override string FriendlyFileType => "Excel";

        /// <summary>
        /// Determines whether the given file is valid for this generic EDoc type.
        /// </summary>
        /// <param name="path">The file path.</param>
        /// <param name="fileName">The file name.</param>
        /// <returns>A boolean indicating whether the file is valid.</returns>
        protected override bool IsContentValid(string path, string fileName)
        {
            return fileName.EndsWith(".xls", StringComparison.OrdinalIgnoreCase) ||
                fileName.EndsWith(".xlsx", StringComparison.OrdinalIgnoreCase);
        }
    }
}
