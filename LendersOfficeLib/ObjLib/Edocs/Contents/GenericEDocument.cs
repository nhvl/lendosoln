﻿namespace EDocs.Contents
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Valid file types for generic EDocuments.
    /// </summary>
    public enum E_FileType 
    {
        /// <summary>
        /// A LendingQB appraisal XML payload.
        /// </summary>
        AppraisalXml = 0,

        /// <summary>
        /// An MS Excel spreadsheet.
        /// </summary>
        MicrosoftSpreadsheet = 1,

        /// <summary>
        /// A UCD XML payload.
        /// </summary>
        UniformClosingDataset = 2,

        /// <summary>
        /// An unknown XML format.
        /// </summary>
        UnspecifiedXml
    }

    /// <summary>
    /// Represents a Generic EDocument (Non PDF). 
    /// </summary>
    public abstract class GenericEDocument
    {
        /// <summary>
        /// The name of the creation stored procedure.
        /// </summary>
        private static readonly string CreateSp = "EDOCS_GENERIC_Create";

        /// <summary>
        /// The name of the update stored procedure.
        /// </summary>
        private static readonly string UpdateSp = "EDOCS_GENERIC_Update";

        /// <summary>
        /// The name of the fetch stored procedure.
        /// </summary>
        private static readonly string FetchSp = "EDOCS_GENERIC_Fetch";

        /// <summary>
        /// The path to the content file.
        /// </summary>
        private string contentPath = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericEDocument"/> class.
        /// </summary>
        /// <param name="reader">A SQL data reader.</param>
        internal GenericEDocument(DbDataReader reader)
        {
            this.DocumentId = (Guid)reader["DocumentId"];
            this.CreatedD = (DateTime)reader["CreatedD"];
            this.LoanId = (Guid)reader["sLId"];
            this.BrokerId = (Guid)reader["BrokerId"];
            this.ApplicationId = (Guid)reader["aAppId"];
            this.DocTypeId = (int)reader["DocTypeId"];
            this.Description = (string)reader["Description"];
            this.InternalComments = (string)reader["InternalComments"];
            this.CreatedByUserId = (Guid)reader["CreatedByUserId"];
            this.FileDBKey = (Guid)reader["FileDBKey"];
            this.IsValid = (bool)reader["IsValid"];
            this.FolderId = (int)reader["FolderId"];
            this.DocTypeName = (string)reader["DocTypeName"];
            this.FileNm = (string)reader["FileNm"];
            this.Folder = new EDocumentFolder(this.BrokerId, reader);
            this.FolderNm = this.Folder.FolderNm;

            if (string.IsNullOrEmpty(this.FileNm))
            {
                this.FileNm = "document.xml";
            }

            string borrowerName = (string)reader["aBFirstNm"] + " " + (string)reader["aBLastNm"];
            borrowerName = borrowerName.TrimWhitespaceAndBOM();

            string coborrowerName = (string)reader["aCFirstNm"] + " " + (string)reader["aCLastNm"];
            coborrowerName = coborrowerName.TrimWhitespaceAndBOM();

            this.AppName = borrowerName;
            if (!string.IsNullOrEmpty(coborrowerName))
            {
                if (!string.IsNullOrEmpty(this.AppName))
                {
                    this.AppName += " & ";
                }

                this.AppName += coborrowerName;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericEDocument"/> class.
        /// </summary>
        /// <param name="brokerId">The broker ID.</param>
        internal GenericEDocument(Guid brokerId)
        {
            this.IsNew = true;
            this.IsValid = true;
            this.BrokerId = brokerId;
        }

        /// <summary>
        /// Gets the file type for the generic EDoc.
        /// </summary>
        /// <value>The file type for the generic EDoc.</value>
        public abstract E_FileType FileType { get; }

        /// <summary>
        /// Gets a value indicating whether the contents of this generic EDoc can be changed.
        /// </summary>
        /// <value>A value indicating whether the contents of this generic EDoc can be changed.</value>
        public abstract bool SupportsContentUpdate { get; }

        /// <summary>
        /// Gets a value indicating whether this generic EDoc is linked to a PDF EDoc.
        /// </summary>
        /// <value>A value indicating whether this generic EDoc is linked to a PDF EDoc.</value>
        public abstract bool IsLinkedToEDoc { get; }

        /// <summary>
        /// Gets the MIME type of the EDoc contents.
        /// </summary>
        /// <value>The MIME type of the EDoc contents.</value>
        public abstract string MIMEType { get; }

        /// <summary>
        /// Gets the EDoc ID.
        /// </summary>
        /// <value>The EDoc ID.</value>
        public Guid DocumentId { get; internal set; }

        /// <summary>
        /// Gets or sets the associated loan ID.
        /// </summary>
        /// <value>The associated loan ID.</value>
        public Guid LoanId { get; set; }

        /// <summary>
        /// Gets the associated broker ID.
        /// </summary>
        /// <value>The associated broker ID.</value>
        public Guid BrokerId { get; private set; }
        
        /// <summary>
        /// Gets or sets the associated application ID.
        /// </summary>
        /// <value>The associated application ID.</value>
        public Guid ApplicationId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the creating user.
        /// </summary>
        /// <value>The ID of the creating user.</value>
        public Guid CreatedByUserId { get; set; }
        
        /// <summary>
        /// Gets the associated application name.
        /// </summary>
        /// <value>The associated application name.</value>
        public string AppName { get; private set; }
        
        /// <summary>
        /// Gets the doc type name for the generic EDoc.
        /// </summary>
        /// <value>The doc type name for the generic EDoc.</value>
        public string DocTypeName { get; private set; }

        /// <summary>
        /// Gets or sets the description for the generic EDoc.
        /// </summary>
        /// <value>The description for the generic EDoc.</value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets internal comments on the generic EDoc.
        /// </summary>
        /// <value>Internal comments on the generic EDoc.</value>
        public string InternalComments { get; set; }

        /// <summary>
        /// Gets the folder name where the EDoc resides.
        /// </summary>
        /// <value>The folder name where the EDoc resides.</value>
        public string FolderNm { get; internal set; }

        /// <summary>
        /// Gets or sets the doc type ID.
        /// </summary>
        /// <value>The doc type ID.</value>
        public int DocTypeId { get; set; }

        /// <summary>
        /// Gets the ID of the folder where the EDoc resides.
        /// </summary>
        /// <value>The ID of the folder where the EDoc resides.</value>
        public int FolderId { get; private set; }

        /// <summary>
        /// Gets the date the EDoc was created.
        /// </summary>
        /// <value>The date the EDoc was created.</value>
        public DateTime CreatedD { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether the EDoc is valid.
        /// </summary>
        /// <value>A value indicating whether the EDoc is valid.</value>
        public bool IsValid { get; set; }

        /// <summary>
        /// Gets a value indicating whether the EDoc is new.
        /// </summary>
        /// <value>A value indicating whether the EDoc is new.</value>
        public bool IsNew { get; private set; }

        /// <summary>
        /// Gets the folder for the EDoc.
        /// </summary>
        /// <value>The folder for the EDoc.</value>
        public EDocumentFolder Folder { get; private set; }

        /// <summary>
        /// Gets the friendly file type string.
        /// </summary>
        /// <value>The friendly file type string.</value>
        public abstract string FriendlyFileType { get; }

        /// <summary>
        /// Gets the file name.
        /// </summary>
        /// <value>The file name.</value>
        public string FileNm { get; private set; }

        /// <summary>
        /// Gets or sets the FileDB key for the EDoc contents.
        /// </summary>
        /// <value>The FileDB key for the EDoc contents.</value>
        private Guid FileDBKey { get; set; }

        /// <summary>
        /// Gets all generic EDocs associated with a given loan.
        /// </summary>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="loanId">The loan ID.</param>
        /// <returns>A collection of generic EDocuments associated with the loan.</returns>
        public static IEnumerable<GenericEDocument> GetDocumentsByLoanId(Guid brokerId, Guid loanId)
        {
            return GetDocumentsImpl(brokerId, loanId, null);
        }

        /// <summary>
        /// Gets one generic EDoc associated with a given loan.
        /// </summary>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="documentId">The document ID.</param>
        /// <returns>The generic EDoc associated with the specified loan and document.</returns>
        public static GenericEDocument GetDocument(Guid brokerId, Guid documentId)
        {
            return GetDocumentsImpl(brokerId, null, documentId).FirstOrDefault();
        }

        /// <summary>
        /// Creates a generic EDoc in the broker's system repository.
        /// </summary>
        /// <param name="path">The file path.</param>
        /// <param name="fileType">The file type.</param>
        /// <param name="docTypeId">The DocType ID.</param>
        /// <param name="linkedDocId">The linked document ID.</param>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="appId">The application ID.</param>
        /// <param name="userId">The user ID.</param>
        /// <param name="description">A description of the file.</param>
        /// <returns>A generic EDoc.</returns>
        public static GenericEDocument CreateGenericDoc(string path, E_FileType fileType, int docTypeId, Guid linkedDocId, Guid brokerId, Guid loanId, Guid appId, Guid userId, string description)
        {
            EDocumentRepository repo = EDocumentRepository.GetSystemRepository(brokerId);
            var xmleDoc = repo.CreateLinkedGenericDocument(fileType, linkedDocId);
            xmleDoc.ApplicationId = appId;
            xmleDoc.Description = description;
            xmleDoc.InternalComments = string.Empty;
            xmleDoc.DocTypeId = docTypeId;
            xmleDoc.LoanId = loanId;
            xmleDoc.SetContent(path, "document.xml");
            xmleDoc.Save(userId);

            return xmleDoc;
        }

        /// <summary>
        /// Updates the Generic Document Content with given path.
        /// Requires a call to save.
        /// </summary>
        /// <param name="path">The file path to load data from.</param>
        /// <param name="fileName">The file name for upload, download, etc.</param>
        public void SetContent(string path, string fileName)
        {
            if (!this.IsNew && !this.SupportsContentUpdate)
            {
                throw CBaseException.GenericException("Cannot update Generic EDocument Content. Not Supported");
            }

            if (!this.IsContentValid(path, fileName))
            {
                throw new CBaseException("File is not a valid " + this.FriendlyFileType.ToLower() + " document.", "Invalid file. " + fileName);
            }

            this.FileNm = Path.GetFileName(fileName);

            this.contentPath = path;
            this.FileDBKey = Guid.NewGuid();
        }

        /// <summary>
        /// Gets a file path to access the contents.
        /// </summary>
        /// <returns>A string file path.</returns>
        public string GetContentPath()
        {
            return FileDBTools.CreateCopy(E_FileDB.EDMS, this.FileDBKey.ToString());
        }

        /// <summary>
        /// Saves the generic EDoc.
        /// </summary>
        /// <param name="userId">The user ID.</param>
        internal void Save(Guid userId)
        {
            if (this.IsNew && string.IsNullOrEmpty(this.contentPath))
            {
                throw CBaseException.GenericException("Cannot create a Generic EDocument without a file.");
            }

            if (!string.IsNullOrEmpty(this.contentPath))
            {
                FileDBTools.WriteFile(E_FileDB.EDMS, this.FileDBKey.ToString(), this.contentPath);
            }

            bool wasSaved = false;
            try
            {
                string sp = this.IsNew ? CreateSp : UpdateSp;
                StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, sp, 3, this.GenerateParameters(userId));
                wasSaved = true;
            }
            finally
            {
                // Save failed, delete FileDB entry.
                if (wasSaved == false)
                {
                    FileDBTools.Delete(E_FileDB.EDMS, this.contentPath);
                }
            }
        }

        /// <summary>
        /// Indicates whether the contents of the given file are valid for the generic EDoc file type.
        /// </summary>
        /// <param name="path">The file path.</param>
        /// <param name="fileName">The file type.</param>
        /// <returns>A boolean indicating whether the contents are valid.</returns>
        protected abstract bool IsContentValid(string path, string fileName);

        /// <summary>
        /// Retrieves generic EDocs from the DB.
        /// </summary>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="documentId">The document ID.</param>
        /// <returns>A collection of generic EDocuments associated with the loan or document IDs.</returns>
        private static IEnumerable<GenericEDocument> GetDocumentsImpl(Guid brokerId, Guid? loanId, Guid? documentId)
        {
            SqlParameter[] parameters =
                {
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@sLId", loanId),
                    new SqlParameter("@DocumentId", documentId)
                };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, FetchSp, parameters))
            {
                while (reader.Read())
                {
                    var type = (E_FileType)Enum.Parse(typeof(E_FileType), reader["FileType"].ToString());

                    switch (type)
                    {
                        case E_FileType.AppraisalXml:
                            yield return new AppraisalGenericEDocument(reader);
                            break;
                        case E_FileType.UniformClosingDataset:
                            yield return new UcdGenericEDocument(reader);
                            break;
                        case E_FileType.MicrosoftSpreadsheet:
                            yield return new MicrosoftSpreadsheetGenericEDocument(reader);
                            break;
                        case E_FileType.UnspecifiedXml:
                            yield return new XmlGenericEDocument(reader);
                            break;
                        default:
                            throw new UnhandledEnumException(type);
                    }
                }
            }
        }

        /// <summary>
        /// Generates SQL parameters for the generic EDoc.
        /// </summary>
        /// <param name="userId">The user ID.</param>
        /// <returns>An array of SQL parameters.</returns>
        private SqlParameter[] GenerateParameters(Guid userId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@DocumentId", this.DocumentId),
                new SqlParameter("@aAppId", this.ApplicationId),
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@DocTypeId", this.DocTypeId),
                new SqlParameter("@Description", this.Description),
                new SqlParameter("@InternalComments", this.InternalComments),
                new SqlParameter("@UserId", userId),
                new SqlParameter("@FileDBKey", this.FileDBKey),
                new SqlParameter("@IsValid", this.IsValid),
                new SqlParameter("@FileType", this.FileType),
                new SqlParameter("@FileName", this.FileNm)
            };

            if (this.IsNew)
            {
                parameters.Add(new SqlParameter("@sLId", this.LoanId));
            }

            return parameters.ToArray();
        }
    }
}
