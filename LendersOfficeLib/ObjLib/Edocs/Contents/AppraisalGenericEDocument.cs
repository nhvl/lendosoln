﻿namespace EDocs.Contents
{
    using System;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.IO;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// A LendingQB Appraisal XML file.
    /// </summary>
    public sealed class AppraisalGenericEDocument : GenericEDocument
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppraisalGenericEDocument"/> class.
        /// </summary>
        /// <param name="reader">A SQL data reader.</param>
        internal AppraisalGenericEDocument(DbDataReader reader)
            : base(reader)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppraisalGenericEDocument"/> class.
        /// </summary>
        /// <param name="brokerId">A broker ID.</param>
        internal AppraisalGenericEDocument(Guid brokerId)
            : base(brokerId)
        {
        }

        /// <summary>
        /// Gets the Appraisal file type.
        /// </summary>
        /// <value>The appraisal file type.</value>
        public override E_FileType FileType => E_FileType.AppraisalXml;

        /// <summary>
        /// Gets the user-friendly name for the Appraisal file type.
        /// </summary>
        /// <value>The user-friendly name for the Appraisal file type.</value>
        public override string FriendlyFileType => "Appraisal XML";

        /// <summary>
        /// Gets the MIME type.
        /// </summary>
        /// <value>The MIME type.</value>
        public override string MIMEType => "text/xml";

        /// <summary>
        /// Gets a value indicating whether this generic EDoc type is linked to a regular EDoc.
        /// </summary>
        /// <value>A value indicating whether this generic EDoc type is linked to a regular EDoc.</value>
        public override bool IsLinkedToEDoc => true;

        /// <summary>
        /// Gets a value indicating whether this generic EDoc type supports updating its contents.
        /// </summary>
        /// <value>A value indicating whether this generic EDoc type supports updating its contents.</value>
        public override bool SupportsContentUpdate => false;

        /// <summary>
        /// Determines the validity of Appraisal XML. AMCs send back a MISMO 2.6
        /// XML file with a VALUATION_RESPONSE root. If that element does not exist, we can
        /// assume that this is not a valid Appraisal XML file.
        /// </summary>
        /// <param name="xml">An XML document.</param>
        /// <returns>A boolean indicating whether the XML is a valid Appraisal payload.</returns>
        public static bool IsAppraisalXmlValid(XDocument xml)
        {
            try
            {
                return xml.Root.Name.LocalName.Equals("VALUATION_RESPONSE");
            }
            catch (XmlException)
            {
                Tools.LogError(ErrorMessages.EDocs.InvalidAppraisalXml);
                return false;
            }
        }

        /// <summary>
        /// Determines whether the contents of the given file are valid for this generic EDoc type.
        /// </summary>
        /// <param name="path">The file path.</param>
        /// <param name="fileName">The file name.</param>
        /// <returns>A boolean indicating whether the file contents are valid.</returns>
        protected override bool IsContentValid(string path, string fileName) => IsAppraisalXmlValid(XDocument.Load(path));
    }
}
