﻿namespace EDocs.Contents
{
    using System;
    using System.Data.Common;
    using System.Data.SqlClient;

    /// <summary>
    /// An XML generic doc type with unknown format.
    /// </summary>
    public class XmlGenericEDocument : GenericEDocument
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="XmlGenericEDocument"/> class.
        /// </summary>
        /// <param name="reader">A SQL data reader.</param>
        internal XmlGenericEDocument(DbDataReader reader)
            : base(reader)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlGenericEDocument"/> class.
        /// </summary>
        /// <param name="brokerId">A broker ID.</param>
        internal XmlGenericEDocument(Guid brokerId)
            : base(brokerId)
        {
        }

        /// <summary>
        /// Gets the xml file type.
        /// </summary>
        /// <value>The xml file type.</value>
        public override E_FileType FileType => E_FileType.UnspecifiedXml;

        /// <summary>
        /// Gets a value indicating whether this generic EDoc type supports updating its contents.
        /// </summary>
        /// <value>A value indicating whether this generic EDoc type supports updating its contents.</value>
        public override bool SupportsContentUpdate => true;

        /// <summary>
        /// Gets the MIME type.
        /// </summary>
        /// <value>The MIME type.</value>
        public override string MIMEType => "text/xml";

        /// <summary>
        /// Gets a value indicating whether this generic EDoc type is linked to a regular EDoc.
        /// </summary>
        /// <value>A value indicating whether this generic EDoc type is linked to a regular EDoc.</value>
        public override bool IsLinkedToEDoc => false;

        /// <summary>
        /// Gets the user-friendly name for the XML file type.
        /// </summary>
        /// <value>The user-friendly name for the XML file type.</value>
        public override string FriendlyFileType => "XML";

        /// <summary>
        /// Determines whether the given file is valid for this generic EDoc type.
        /// </summary>
        /// <param name="path">The file path.</param>
        /// <param name="fileName">The file name.</param>
        /// <returns>A boolean indicating whether the file is valid.</returns>
        protected override bool IsContentValid(string path, string fileName)
        {
            try
            { 
                System.Xml.Linq.XDocument.Load(path);
            }
            catch (SystemException)
            {
                return false;
            }

            return true;
        }
    }
}
