namespace EDocs.Contents
{
    using System;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// A Uniform Closing Dataset XML generic doc type.
    /// </summary>
    public sealed class UcdGenericEDocument : GenericEDocument
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UcdGenericEDocument"/> class.
        /// </summary>
        /// <param name="reader">A SQL data reader.</param>
        internal UcdGenericEDocument(DbDataReader reader)
            : base(reader)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UcdGenericEDocument"/> class.
        /// </summary>
        /// <param name="brokerId">A broker ID.</param>
        internal UcdGenericEDocument(Guid brokerId)
            : base(brokerId)
        {
        }

        /// <summary>
        /// Gets the UCD file type.
        /// </summary>
        /// <value>The UCD file type.</value>
        public override E_FileType FileType => E_FileType.UniformClosingDataset;

        /// <summary>
        /// Gets the user-friendly name for the UCD file type.
        /// </summary>
        /// <value>The user-friendly name for the UCD file type.</value>
        public override string FriendlyFileType => "Uniform Closing Dataset";

        /// <summary>
        /// Gets the MIME type.
        /// </summary>
        /// <value>The MIME type.</value>
        public override string MIMEType => "text/xml";

        /// <summary>
        /// Gets a value indicating whether this generic EDoc type is linked to a regular EDoc.
        /// </summary>
        /// <value>A value indicating whether this generic EDoc type is linked to a regular EDoc.</value>
        public override bool IsLinkedToEDoc => true;

        /// <summary>
        /// Gets a value indicating whether this generic EDoc type supports updating its contents.
        /// </summary>
        /// <value>A value indicating whether this generic EDoc type supports updating its contents.</value>
        public override bool SupportsContentUpdate => false;

        /// <summary>
        /// Validates an EDoc that already exists in the system. This only verifies that an
        /// existing EDoc has type <see cref="UcdGenericEDocument"/>, and does not check the
        /// document contents directly. The document contents may not be valid MISMO UCD per
        /// the temp option described in OPM 461943.
        /// </summary>
        /// <param name="documentId">The document ID.</param>
        /// <param name="brokerId">The broker ID.</param>
        /// <returns>A boolean indicating whether the document is a valid UCD file.</returns>
        public static bool IsUcdEdocValid(Guid documentId, Guid brokerId)
        {
            var repo = EDocumentRepository.GetSystemRepository(brokerId);
            GenericEDocument linkedDoc;

            try
            {
                linkedDoc = repo.GetGenericDocumentById(documentId);
            }
            catch (CBaseException exc)
            {
                Tools.LogWarning(exc);
                return false;
            }

            return linkedDoc != null && linkedDoc is UcdGenericEDocument;
        }

        /// <summary>
        /// Validates a UCD XML string. This should be used to ensure any new UCD file
        /// is compliant before uploading to EDocs.
        /// </summary>
        /// <param name="ucdXml">The XML string to validate.</param>
        /// <param name="brokerId">The broker ID associated with the generic EDoc.</param>
        /// <returns>A boolean indicating whether the XML is valid UCD.</returns>
        public static bool IsUcdStringValid(string ucdXml, Guid brokerId)
        {
            try
            {
                if (brokerId != Guid.Empty && BrokerDB.RetrieveById(brokerId).AllowAnyXmlToValidateAsUcd)
                {
                    return ucdXml != null;
                }

                return Tools.IsValidMismo33Xml(ucdXml);
            }
            catch (XmlException e)
            {
                Tools.LogError(ErrorMessages.EDocs.InvalidUcd, e);
            }

            return false;
        }

        /// <summary>
        /// Validates a UCD XML string. This should be used to ensure any new UCD file
        /// is compliant before uploading to EDocs.
        /// </summary>
        /// <param name="ucdXml">The XML to validate.</param>
        /// <param name="brokerId">The broker ID associated with the generic EDoc.</param>
        /// <returns>A boolean indicating whether the XML is valid UCD.</returns>
        public static bool IsUcdStringValid(XDocument ucdXml, Guid brokerId)
        {
            if (brokerId != Guid.Empty && BrokerDB.RetrieveById(brokerId).AllowAnyXmlToValidateAsUcd)
            {
                return ucdXml != null;
            }

            return Tools.IsValidMismo33Xml(ucdXml);
        }

        /// <summary>
        /// Determines whether the contents of the given file are valid for this generic EDoc type.
        /// </summary>
        /// <param name="path">The file path.</param>
        /// <param name="fileName">The file name.</param>
        /// <returns>A boolean indicating whether the file contents are valid.</returns>
        protected override bool IsContentValid(string path, string fileName)
        {
            string xml = File.ReadAllText(path);
            return IsUcdStringValid(xml, this.BrokerId);
        }
    }
}
