﻿// <copyright file="EDocsMigrationRequestItem.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   8/4/2015
// </summary>
namespace EDocs
{
    using System;
    using Newtonsoft.Json;

    /// <summary>
    /// A structure that contains information about migration.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class EDocsMigrationRequestItem
    {
        /// <summary>
        /// Gets or sets the broker id.
        /// </summary>
        /// <value>The broker id.</value>
        [JsonProperty("brokerid")]
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets the document id.
        /// </summary>
        /// <value>The document id.</value>
        [JsonProperty("documentid")]
        public Guid DocumentId { get; set; }

        /// <summary>
        /// Gets or sets the filedb key.
        /// </summary>
        /// <value>The filedb key.</value>
        [JsonProperty("filedbkey")]
        public Guid FileDBKey_CurrentRevision { get; set; }
    }
}