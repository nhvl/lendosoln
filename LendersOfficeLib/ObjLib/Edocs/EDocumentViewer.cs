﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DataAccess;
using EDocs.Utils;
using ICSharpCode.SharpZipLib.Zip;
using iTextSharp.text;
using iTextSharp.text.pdf;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.HttpModule;
using LendersOffice.PdfLayout;
using LendersOffice.Security;
namespace EDocs
{


    public class EDocumentViewer
    {

        public static string GeneratePngForToday()
        {
            System.Drawing.Font f = new System.Drawing.Font("Arial", 8f, FontStyle.Bold);
            return CreatePngFileFromText(DateTime.Now.ToShortDateString(), f, System.Drawing.Color.White, System.Drawing.Color.Black);
        }
        public static string CreatePngFileFromText(string text, System.Drawing.Font font, System.Drawing.Color backgroundColor, System.Drawing.Color foreColor)
        {
            SizeF textSize;
            string fileName = TempFileUtils.NewTempFilePath();
            using (System.Drawing.Image img = new Bitmap(1, 1))
            using (Graphics drawing = Graphics.FromImage(img))
            {
                textSize = drawing.MeasureString(text, font);
            }
            using (System.Drawing.Image img = new Bitmap((int)textSize.Width, (int)textSize.Height))
            using (Graphics drawing = Graphics.FromImage(img))
            {
                drawing.Clear(backgroundColor);
                using (Brush textBrush = new SolidBrush(foreColor))
                {
                    drawing.DrawString(text, font, textBrush, 0, 0);
                }
                drawing.Save();
                img.Save(fileName, System.Drawing.Imaging.ImageFormat.Png);
            }

            return fileName;
        }


        private static string GetPngFileName(PNGEncryptedData data, bool isThumb)
        {
            return string.Format("{0}\\{1}{2}.png", ConstApp.TempFolder, data.PNGKey, isThumb ? ConstAppDavid.PdfPngThumbFileDbSuffix : "");
        }

        private static string GetNonDestructivePngFileName(Guid documentId, int pg, bool isThumb)
        {
            return string.Format("{0}\\{1}_{2}_{3}", ConstApp.TempFolder, documentId, pg, isThumb ? "thumb" : "full");
        }
        public static string GetNonDestructivePngPath(Guid documentId, int pg, bool isThumb)
        {
            string fileName = GetNonDestructivePngFileName(documentId, pg, isThumb);

            if (File.Exists(fileName))
            {
                return fileName;
            }

            string pngKey = string.Format("{0}_{1}_{2}", documentId, pg, isThumb ? "thumb" : "full");

            // dd 5/29/2018 - case 47025 - Attempt to get the PNG from the EdmsPdf FileDB first.
            string path = string.Empty;

            try
            {
                path = FileDBTools.CreateCopy(E_FileDB.EdmsPdf, pngKey);
            }
            catch (FileNotFoundException)
            {
                path = FileDBTools.CreateCopy(E_FileDB.EDMS, pngKey);
            }


            if (File.Exists(fileName) == false)
            {
                // 2/10/2015 dd - Why do I perform File.Exists again?
                // There could be scenario where independent request just finish create the png file since the last check.
                // It is rare but does happen on production.
                File.Move(path, fileName);
            }

            return fileName;
        }
        /// <summary>
        /// Loads the png from the file system if it exist otherwise it loads it from filedb. If the thumbnale does not exist it creates it 
        /// and stores it from filedb.
        /// </summary>
        /// <param name="encryptedPNGData"></param>
        /// <param name="loadThumb"></param>
        /// <returns></returns>
        public static string GetPngPath(string encryptedPNGData, bool loadThumb)
        {
            Stopwatch sw = Stopwatch.StartNew();
            PNGEncryptedData decryptedPNGData = new PNGEncryptedData(encryptedPNGData);
            PerformanceMonitorItem.LogAndRestart("Decrypt", sw);
            AbstractUserPrincipal user = System.Threading.Thread.CurrentPrincipal as AbstractUserPrincipal;
            if (user == null || !user.HasPermission(Permission.CanViewEDocs))
            {
                throw new AccessDenied();
            }
            if (decryptedPNGData.PageNumber <= 0)
            {
                throw new PageOutOfRangeException();
            }


            if (user.LoginNm != decryptedPNGData.LoginNm)
            {
                throw new AccessDenied();
            }

            //If this ever happens, we need to prevent access.
            if(string.IsNullOrEmpty(decryptedPNGData.PNGKey))
            {
                throw new AccessDenied();
            }

            string fileName = GetPngFileName(decryptedPNGData, loadThumb);
            PerformanceMonitorItem.LogAndRestart("GetFileName", sw);

            string pngKey = decryptedPNGData.PNGKey;

            if (loadThumb)
            {
                pngKey += ConstAppDavid.PdfPngThumbFileDbSuffix;
            }

            if (File.Exists(fileName) )
            {
                return fileName;
            }

            try
            {
                PerformanceMonitorItem.LogAndRestart("InitCopy", sw);
                string path = FileDBTools.CreateCopy(E_FileDB.EDMS, pngKey);
                PerformanceMonitorItem.LogAndRestart("CopyDone", sw);

                if (File.Exists(fileName) == false)
                {
                    // 2/10/2015 dd - Why do I perform File.Exists again?
                    // There could be scenario where independent request just finish create the png file since the last check.
                    // It is rare but does happen on production.
                    File.Move(path, fileName);
                }

                PerformanceMonitorItem.LogAndRestart("Move", sw);
            }
            catch (FileNotFoundException)
            {
                if (!loadThumb)
                {
                    //Flag the doc as error, the image was not in the DB. 
                    EDocument.UpdateImageStatus(user.BrokerId, decryptedPNGData.DocumentId, E_ImageStatus.Error);
                    throw;
                }

                CreateAndStoreThumbnail(user.BrokerId, decryptedPNGData.DocumentId, decryptedPNGData.PNGKey, fileName);
                PerformanceMonitorItem.LogAndRestart("CreateThumb", sw);
            }
            return fileName;
        }

        private static void CreateAndStoreThumbnail(Guid brokerId, Guid docId, string srcPngKey, string dstFileName)
        {
            Tools.LogInfo("Generating an on demand thumbnail for doc " + docId.ToString() + ", pngKey: " + srcPngKey + ", dest: " + dstFileName);
            string path = FileDBTools.CreateCopy(E_FileDB.EDMS, srcPngKey);
            float thumbScale = ConstAppDavid.PdfPngThumbOnDemandScaling;
            int width;
            int height;


            using (System.Drawing.Image image = Bitmap.FromFile(path))
            {
                width = (int) (image.Width * thumbScale);
                height = (int)(image.Height * thumbScale);
                string tempPath = TempFileUtils.NewTempFilePath();
                using (Bitmap bm = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format24bppRgb))
                using (Graphics g = Graphics.FromImage(bm))
                {
                    //These options are default on dev/local, 
                    //but for some reason prod defaults to lower quality settings so we have to set them explicitly.
                    g.SmoothingMode = SmoothingMode.HighQuality;
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    g.DrawImage(image, 0, 0, width, height);
                    bm.Save(tempPath, ImageFormat.Png);
                }

                FileDBTools.WriteFile(E_FileDB.EDMS, srcPngKey + ConstAppDavid.PdfPngThumbFileDbSuffix, tempPath);
                File.Move(tempPath, dstFileName);
                EDocument.InsertNewPNG(brokerId, docId, new string[] { srcPngKey + ConstAppDavid.PdfPngThumbFileDbSuffix });
            }
        }

        public static byte[] LoadPage(Guid docId, int version, int page, int rotation)
        {
            AbstractUserPrincipal user = System.Threading.Thread.CurrentPrincipal as AbstractUserPrincipal;

            if (user == null || !user.HasPermission(Permission.CanViewEDocs))
            {
                throw new AccessDenied();

            }

            var repo = EDocumentRepository.GetUserRepository();
            var doc = repo.GetDocumentById(docId);

            if (doc.Version != version)
            {
                throw new InvalidEDocumentVersion(version, doc.Version);
            }

            PdfReader pdfReader = CreatePdfReader(doc.GetPDFTempFile_Current());

            if (page <= 0 || page > pdfReader.NumberOfPages)
            {
                throw new PageOutOfRangeException();
            }

            if (rotation != 0)
            {
                int newRotation = pdfReader.GetPageRotation(page) + rotation;
                pdfReader.GetPageN(page).Put(PdfName.ROTATE, new PdfNumber(newRotation));

            }

            // Write Single Page to buffer.
            Document document = new Document();
            MemoryStream outputMemoryStream = new MemoryStream();
            PdfCopy pdfWriter = new PdfCopy(document, outputMemoryStream);
            document.Open();

            pdfWriter.AddPage(pdfWriter.GetImportedPage(pdfReader, page));
            document.Close();

            pdfReader.Close();
            return outputMemoryStream.ToArray();
        }


        private static bool DoesPdfNeedUpdate(Guid docId, int oldPageCount, List<PdfPageItem> pages)
        {
            if (oldPageCount != pages.Count)
            {
                return true;
            }

            for (int i = 0; i < pages.Count; i++)
            {
                if (pages[i].DocId != docId)
                {
                    return true;
                }
                if (pages[i].Page != i + 1)
                {
                    return true;
                }
                if (pages[i].Rotation != 0)
                {
                    return true;
                }
            }

            return false;
        }


        /// <summary>
        /// Warning: Do not use this method with another tmethod that modifies the pdf. 
        /// </summary>
        /// <param name="principal"></param>
        /// <param name="doc"></param>
        /// <param name="pdfFields"></param>
        public static string AddSignatures(AbstractUserPrincipal principal, EDocument doc, List<PdfField> pdfFields)
        {
            EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(principal.BrokerId, principal.EmployeeId, principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);

            if (signInfo.HasUploadedSignature == false)
            {
                throw new CBaseException("There is no signature to apply.", ErrorMessages.Generic);
            }

            string sigPdfTempPath = TempFileUtils.NewTempFilePath();
            using (FileStream pdfOutput = File.Create(sigPdfTempPath))
            {

                byte[] dateImg = File.ReadAllBytes(GeneratePngForToday());
                byte[] sigImg = FileDBTools.ReadData(E_FileDB.Normal, signInfo.SignatureKey.ToString());
                string path = doc.GetPDFTempFile_Current();
                PdfReader reader = EDocumentViewer.CreatePdfReader(path);
                PdfStamper stamper = new PdfStamper(reader, pdfOutput);

                foreach (PdfField field in pdfFields)
                {
                    byte[] imgData = field.Type == PdfFieldType.Signature ? sigImg : dateImg;
                    var pdfcontentbyte = stamper.GetOverContent(field.PageNumber);
                    var img = iTextSharp.text.Image.GetInstance(imgData);
                    img.ScaleToFit(field.Rectangle.Width, field.Rectangle.Height);
                    img.SetAbsolutePosition(field.Rectangle.Left, field.Rectangle.Bottom);
                    pdfcontentbyte.AddImage(img);
                    doc.AddAuditEntry(E_PdfUserAction.ConsumerSignature, DateTime.Now, principal.UserId);

                }
                stamper.Close();
            }

            return doc.OverwritePDFContentForExistingDocs(sigPdfTempPath);
        }
   

        public static void SavePages(AbstractUserPrincipal principal, Guid docId, int version, int docTypeId, string description,
            string publicDescription, E_EDocStatus status, string statusDescription, bool hideFromPML, List<EDocumentAnnotationItem> annotationList,
            List<PdfPageItem> pages, out bool wasPdfUpdated, EDocumentIntermediateAuditTracker actions, List<PdfField> signatureFields, List<EDocumentAnnotationItem> eSignTags)
        {
            signatureFields = signatureFields ?? new List<PdfField>();
            using (PerformanceStopwatch.Start("EdocumentViewer.SavePages"))
            {
                StringBuilder sbLog = new StringBuilder();
                Stopwatch sw = Stopwatch.StartNew();
                var repo = EDocumentRepository.GetUserRepository();
                var doc = repo.GetDocumentById(docId);
                if (false == repo.CanUpdateDocument(doc))
                {
                    throw new AccessDenied();
                }
                sbLog.AppendLine("Get Doc: " + sw.ElapsedMilliseconds);
                if (doc.Version != version)
                {
                    throw new InvalidEDocumentVersion(version, doc.Version);
                }

                var oldPageCount = doc.PageCount;
                wasPdfUpdated = DoesPdfNeedUpdate(docId, oldPageCount, pages);

                List<EDocumentAnnotationItem> annotationsToApply = new List<EDocumentAnnotationItem>();
                if (annotationList != null)
                {
                    for (int x = annotationList.Count - 1; x >= 0; x--)
                    {
                        var annotation = annotationList[x];
                        if (annotation.ApplyToPDF)
                        {
                            annotationList.RemoveAt(x);
                            annotationsToApply.Add(annotation);
                        }
                    }

                    wasPdfUpdated = annotationsToApply.Count > 0 || wasPdfUpdated;
                }

                sbLog.AppendLine("PDF Save Check: " + sw.ElapsedMilliseconds);

                if (signatureFields.Any())
                {
                    doc.AddAuditEntry(E_PdfUserAction.LenderSignature, DateTime.Now, principal.UserId);
                    wasPdfUpdated = true;
                }

                string deletedPdfPath = null;

                if (wasPdfUpdated)
                {
                    if (doc.EdocsMetadata != null)
                    {
                        // 6/23/2015 dd - Update Edocs as NonDestructive.
                        sbLog.AppendLine("Update Pages using NonDestructive.");
                        
                        doc.UpdateNonDestructiveContentOnSave(pages, annotationsToApply, signatureFields);
                    }
                    else
                    {
                        // 6/23/2015 dd - Edocs is in legacy mode.
                        bool createDeletedEDocPdf = principal.BrokerDB.EnableDeletedPageEDocCreation;
                        bool requiresRegeneration;
                        List<string> pngsToStore;
                        doc.UpdatePDFContentOnSave(GenerateNewEdocCombinePdf(principal, docId, pages, signatureFields, annotationsToApply,
                            createDeletedEDocPdf, out requiresRegeneration, out pngsToStore, out deletedPdfPath), requiresRegeneration, pages.Count);

                        sbLog.AppendLine("Create PDF " + sw.ElapsedMilliseconds);

                        EDocument.InsertNewPNG(principal.BrokerId, docId, pngsToStore);
                        sbLog.AppendLine("Insert Pages  " + sw.ElapsedMilliseconds);
                    }
                }

                doc.InternalDescription = description;
                doc.DocumentTypeId = docTypeId;
                doc.PublicDescription = publicDescription;

                // OPM 64817 - Check permissions before changing doc visibility for PML users
                if (principal.HasPermission(Permission.AllowHideEDocsFromPML))
                {
                    doc.HideFromPMLUsers = hideFromPML;
                }

				//Ignore saving status if it's invalid
				//Used for handling Duplicate Protected Documents (OPM 106903)
                if (Enum.IsDefined(typeof(E_EDocStatus), status))
                {
                    doc.DocStatus = status;
                    doc.StatusDescription = statusDescription;
                }

                if (annotationList == null) //if annotations is null it means the user cannot edit them so go ahead and remap them
                {
                    FixAnnotationPageMappings(oldPageCount, pages, doc, (edoc) => edoc.InternalAnnotations);
                }
                else //means the PDF editor has already taken care of the mappings
                {
                    doc.InternalAnnotations.Update(principal, annotationList);
                }

                if (principal.HasPermission(Permission.CanEditEDocs) && (doc.DocStatus != E_EDocStatus.Approved || principal.HasPermission(Permission.AllowEditingApprovedEDocs)))
                {
                    if (eSignTags == null)
                    {
                        FixAnnotationPageMappings(oldPageCount, pages, doc, (edoc) => edoc.ESignTags);
                    }
                    else
                    {
                        var docuSignSettings = LendersOffice.Admin.DocuSign.LenderDocuSignSettings.Retrieve(principal.BrokerId);
                        bool canEditTags = principal.HasPermission(Permission.AllowCreatingDocumentSigningEnvelopes) && docuSignSettings != null && docuSignSettings.IsSetupComplete() && docuSignSettings.DocuSignEnabled;

                        // We only want to update if we have the permissions. We already have EDoc editing permission so just check tag editting permissions.
                        if (canEditTags)
                        {
                           doc.ESignTags.Update(principal, eSignTags);
                        }
                    }
                }

                sbLog.AppendLine("Annotation Handling " + sw.ElapsedMilliseconds);
                doc.AddAuditEntries(actions);
                if (annotationsToApply.Count > 0)
                {
                    doc.AddAuditEntry(E_PdfUserAction.PastedAnnotations, DateTime.Now, principal.UserId);
                }

                // 2/5/2015 AV - 198591 Escalated Item: Batch Editor and deletion of pages
                if (!string.IsNullOrEmpty(deletedPdfPath))
                {
                    EDocumentRepository systemRepo = EDocumentRepository.GetSystemRepository(principal.BrokerId); 
                    var deletedPageEdoc = systemRepo.CreateDocument(E_EDocumentSource.UserCopy);
                    deletedPageEdoc.CopiedFromFileName = doc.FolderAndDocTypeName + ". Pages deleted by " + principal.FirstName + " " + principal.LastName;
                    deletedPageEdoc.AppId = doc.AppId;

                    deletedPageEdoc.Comments = doc.Comments;
                    deletedPageEdoc.DocumentTypeId = doc.DocumentTypeId;
                    deletedPageEdoc.EDocOrigin = E_EDocOrigin.LO;
                    deletedPageEdoc.InternalDescription = doc.InternalDescription;
                    deletedPageEdoc.PublicDescription = doc.PublicDescription + " (deleted pages)";
                    deletedPageEdoc.LoanId = doc.LoanId;
                    deletedPageEdoc.UpdatePDFContentOnSave(deletedPdfPath);
                    systemRepo.Save(deletedPageEdoc);
                    systemRepo.DeleteDocument(deletedPageEdoc.DocumentId, "All Pages Deleted");
                    
                }

                repo.Save(doc);
                sw.Stop();

                Tools.LogInfo("[SavePages] Save took " + sw.ElapsedMilliseconds + " pages = " + pages.Count + " Log: \n" + sbLog.ToString());
            }
        }

        private static void FixAnnotationPageMappings(int oldPageCount, List<PdfPageItem> pages, EDocument doc, Func<EDocument, EDocumentAnnotationContainer> annotationContainerGetter)
        {
            var annotationContainer = annotationContainerGetter(doc);
            List<EDocumentAnnotationItem>[] itemsPerPage = new List<EDocumentAnnotationItem>[oldPageCount];

            #region generate items per page
            foreach (EDocumentAnnotationItem annotation in annotationContainer.AnnotationList)
            {
                int index = annotation.PageNumber - 1;
                if (itemsPerPage[index] == null)
                {
                    itemsPerPage[index] = new List<EDocumentAnnotationItem>();
                }

                itemsPerPage[index].Add(annotation);
            }
            #endregion

            #region update annotations for users w/o edit privilege
            for (var oldPageIndex = 1; oldPageIndex <= itemsPerPage.Length; oldPageIndex++) //is using 1 based index
            {
                var newPageIndex = pages.FindIndex(p => p.Page == oldPageIndex - 1) + 1; // Pages are using 0-index
                var affectedAnnotations = itemsPerPage[oldPageIndex - 1]; //this is the affected annotations  itemsPerPage uses 0 base index

                if (affectedAnnotations == null) //there where no annotatiosn on the page so continue on
                {
                    continue;
                }

                if (newPageIndex == oldPageIndex)      //the page number didnt change so dont do anything
                {
                    continue; //dont need to do anything
                }
                else if (newPageIndex < 1) //the page was removed so go ahead and delete the annotations
                {
                    annotationContainer.Remove(affectedAnnotations);
                }
                else //the page number changed so go ahead and update the page numbers
                {
                    foreach (var annotation in affectedAnnotations)
                    {
                        annotation.PageNumber = newPageIndex;
                    }
                }
            }

            #endregion
        }

        public static void SaveNonPdfInfo(Guid docId, int version, E_EDocStatus status, int docTypeId, string description, string publicDescription, bool hideFromPML, List<EDocumentAnnotationItem> annotationList, List<EDocumentAnnotationItem> eSignTags, EDocumentIntermediateAuditTracker auditTracker)
        {
            var repo = EDocumentRepository.GetUserRepository();
            var doc = repo.GetDocumentById(docId);
            if (doc.Version != version)
            {
                throw new InvalidEDocumentVersion(version, doc.Version);
            }
            doc.InternalDescription = description;
            doc.DocumentTypeId = docTypeId;
            doc.PublicDescription = publicDescription;
            doc.DocStatus = status;

            var principal = BrokerUserPrincipal.CurrentPrincipal;
            // OPM 64817 - Check permissions before changing doc visibility for PML users
            if (BrokerUserPrincipal.CurrentPrincipal.HasPermission(Permission.AllowHideEDocsFromPML))
                doc.HideFromPMLUsers = hideFromPML;

            doc.InternalAnnotations.Update(BrokerUserPrincipal.CurrentPrincipal, annotationList);

            bool canEditEdoc = principal.HasPermission(Permission.CanEditEDocs) && (doc.DocStatus != E_EDocStatus.Approved || principal.HasPermission(Permission.AllowEditingApprovedEDocs));

            var docuSignSettings = LendersOffice.Admin.DocuSign.LenderDocuSignSettings.Retrieve(principal.BrokerId);
            bool canEditTags = principal.HasPermission(Permission.AllowCreatingDocumentSigningEnvelopes) && docuSignSettings != null && docuSignSettings.IsSetupComplete() && docuSignSettings.DocuSignEnabled;
            if (canEditEdoc && canEditTags)
            {
                doc.ESignTags.Update(BrokerUserPrincipal.CurrentPrincipal, eSignTags);
            }

            doc.AddAuditEntries(auditTracker);
            repo.SaveNonPdfData(doc);
        }

        /// <summary>
        /// Use this method to regenerate a pdf that will be stored in EDOCS. It deals with the png and restructuring. 
        /// </summary>
        /// <param name="originalDocId">The doc id of the original edoc.</param>
        /// <param name="pages">Each page description</param>
        /// <param name="requiresRegeneration">Will be set if the pdf needs regeneration by the png service.</param>
        /// <returns>The path of the temp file.</returns>
        private static string GenerateNewEdocCombinePdf(AbstractUserPrincipal principal, Guid originalDocId, List<PdfPageItem> pages,
            List<PdfField> signatureFields, IEnumerable<EDocumentAnnotationItem> toApplyAnnotations, bool generateDeletedPagePdf, out bool requiresRegeneration, out List<string> pdfKeysToStore, out string deletedPagePdfPath)
        {

            ILookup<int, EDocumentAnnotationItem> pageAnnotations = toApplyAnnotations.ToLookup(p => p.PageNumber, p => p);

            using (PerformanceStopwatch.Start("EDocumentViewer.GenerateNewEdocCombinePdf"))
            {
                requiresRegeneration = false;
                StringBuilder sb = new StringBuilder();

                Stopwatch sw = Stopwatch.StartNew();
                var repo = EDocumentRepository.GetUserRepository();
                sb.AppendFormat("GETREPO {0}\n", sw.ElapsedMilliseconds);
                Dictionary<Guid, PdfReader> hash = new Dictionary<Guid, PdfReader>();
                List<string> tempFilesToDelete = new List<string>();

                // Load all different docs in memory.
                using (PerformanceStopwatch.Start("EDocumentViewer.GenerateNewEdocCombinePdf - Load Docs To Memory"))
                {
                    foreach (PdfPageItem item in pages)
                    {
                        if (!hash.ContainsKey(item.DocId))
                        {
                            EDocument d = repo.GetDocumentById(item.DocId);
                            sb.AppendFormat("\tGETDOC  {0} {1}\n", item.DocId, sw.ElapsedMilliseconds);

                            if (d.Version != item.Version)
                                throw new InvalidEDocumentVersion(item.Version, d.Version);

                            string path = d.GetPDFTempFile_Current(); //dont load the reader again
                            sb.AppendFormat("\tGETFILE  {0} \n", sw.ElapsedMilliseconds);
                            tempFilesToDelete.Add(path);
                            PdfReader reader = CreatePdfReader(path);
                            sb.AppendFormat("\tCreate Reader  {0} \n", sw.ElapsedMilliseconds);
                            hash.Add(item.DocId, reader);
                        }
                    }
                }

                IEnumerable<int> deletedPagesFromOriginalDoc = null;
                deletedPagePdfPath = null;
                // We need to inspect the result PDF and make sure each page is going to be in it.
                if (generateDeletedPagePdf)
                {
                    PdfReader originalDoc = hash[originalDocId];
                    // Note that Page is 1 index.
                    IEnumerable<int> existingPages = pages.Where(p => p.DocId == originalDocId).Select(p => p.Page);
                    IEnumerable<int> range = Enumerable.Range(1, originalDoc.NumberOfPages);
                    deletedPagesFromOriginalDoc = range.Except(existingPages); 
                }


                sb.AppendFormat("FETCHALLDOCS {0}\n", sw.ElapsedMilliseconds);
                string newPath = string.Empty;
                using (PerformanceStopwatch.Start("EDocumentViewer.GenerateNewEdocCombinePdf - Write Id To Page"))
                {
                    Document document = new Document();
                    newPath = TempFileUtils.NewTempFilePath() + ".pdf";
                    int currentPageNumber = 0;
                    pdfKeysToStore = new List<string>();
                    using (FileStream fs = File.OpenWrite(newPath))
                    {

                        PdfCopy pdfWriter = new PdfCopy(document, fs);
                        document.Open();

                        foreach (PdfPageItem item in pages)
                        {


                            currentPageNumber++;
                            PdfReader pdfReader = hash[item.DocId];

                            if (item.Rotation != 0)
                            {
                                int rotation = pdfReader.GetPageRotation(item.Page) + item.Rotation;
                                pdfReader.GetPageN(item.Page).Put(PdfName.ROTATE, new PdfNumber(rotation));

                            }

                            string pngId = PDFPageIDManager.GetPageId(pdfReader, item.Page);
                            if (false == pageAnnotations.Contains(currentPageNumber) && false == string.IsNullOrEmpty(pngId) && item.Rotation == 0 && false == signatureFields.Any(p => p.PageNumber == currentPageNumber))  //we have a png and doing no rotation so we can use.
                            {
                                if (item.DocId != originalDocId) //if the image is from a different doc make a copy of it.
                                {
                                    string newPngCopyId = Guid.NewGuid().ToString();
                                    bool copySuccessful = CreateFileDBCopy(PDFPageIDManager.GetPageId(pdfReader, item.Page), newPngCopyId);
                                    if (copySuccessful)
                                    {
                                        PDFPageIDManager.Update(pdfReader, item.Page, newPngCopyId);
                                        pdfKeysToStore.Add(newPngCopyId);
                                    }
                                    else
                                    {
                                        requiresRegeneration = true;
                                    }
                                }
                            }
                            else
                            {
                                PDFPageIDManager.DeletePageIdOn(pdfReader, item.Page);
                                requiresRegeneration = true;
                            }

                            using (PerformanceStopwatch.Start("EDocumentViewer.GenerateNewEdocCombinePdf - Write Id To Page - pdfWriter.GetImportedPage"))
                            {
                                PdfImportedPage page = pdfWriter.GetImportedPage(pdfReader, item.Page);
                                pdfWriter.AddPage(page);
                            }
                            sb.AppendFormat("\tpage {0} {1}\n", currentPageNumber, sw.ElapsedMilliseconds);
                        }
                        sb.AppendFormat("PDFI {0}\n", sw.ElapsedMilliseconds);

                        using (PerformanceStopwatch.Start("EDocumentViewer.GenerateNewEdocCombinePdf - Write Id To Page - Close Document"))
                        {
                            document.Close();
                        }


                    }

                }

                if (deletedPagesFromOriginalDoc != null && deletedPagesFromOriginalDoc.Count() > 0)
                {
                    deletedPagePdfPath = String.Concat(TempFileUtils.NewTempFilePath(), ".pdf");
                    Document document = new Document();
                    PdfReader pdfReader = hash[originalDocId];
                    using (FileStream fs = File.OpenWrite(deletedPagePdfPath))
                    {
                        PdfCopy pdfWriter = new PdfCopy(document, fs);
                        document.Open();

                        foreach (var pageNumber in deletedPagesFromOriginalDoc.OrderBy(p => p))
                        {
                            PDFPageIDManager.DeletePageIdOn(pdfReader, pageNumber); 
                            PdfImportedPage page = pdfWriter.GetImportedPage(pdfReader, pageNumber);
                            pdfWriter.AddPage(page);
                        }
                        document.Close();
                    }
                }

                // Close all PdfReader
                foreach (var o in hash)
                {
                    o.Value.Close();
                }

                foreach (string path in tempFilesToDelete)
                {
                    try
                    {
                        File.Delete(path);
                    }
                    catch (IOException e)
                    {
                        Tools.LogError("Could not delete file skipping " + e);
                    }
                    catch (NotSupportedException e)
                    {
                        Tools.LogError("Could not delete file skipping " + e);
                    }
                    catch (UnauthorizedAccessException e)
                    {
                        Tools.LogError("Could not delete file skipping " + e);
                    }
                }

                sw.Stop();
                sb.AppendFormat("PDFD {0}\n", sw.ElapsedMilliseconds);


                if (signatureFields.Any())
                {
                    EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(principal.BrokerId, principal.EmployeeId, principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);
                    
                    if (signInfo.HasUploadedSignature == false)
                    {
                        throw CBaseException.GenericException("Signature does not exist. User cannot sign.");
                    }
                    string sigTempPath = TempFileUtils.NewTempFilePath();
                    using (FileStream fs1 = File.Create(sigTempPath))
                    {
                        PdfReader reader = CreatePdfReader(newPath);
                        PdfStamper stamper = new PdfStamper(reader, fs1);
                        byte[] dateImg = File.ReadAllBytes(GeneratePngForToday());
                        byte[] sigImg = FileDBTools.ReadData(E_FileDB.Normal, signInfo.SignatureKey.ToString());
                        foreach (PdfField field in signatureFields)
                        {
                            byte[] imgData = field.Type == PdfFieldType.Signature ? sigImg : dateImg;
                            var pdfcontentbyte = stamper.GetOverContent(field.PageNumber);
                            var img = iTextSharp.text.Image.GetInstance(imgData);
                            img.ScaleToFit(field.Rectangle.Width, field.Rectangle.Height);
                            img.SetAbsolutePosition(field.Rectangle.Left, field.Rectangle.Bottom);
                            pdfcontentbyte.AddImage(img);
                        }
                        stamper.Close();
                        newPath = sigTempPath;
                        requiresRegeneration = true;
                    }
                }

                if (toApplyAnnotations.Any())
                {
                    newPath = EDocument.GeneratePDFDataWithPastedAnnotation(newPath, pageAnnotations);
                }

                Tools.LogInfo("[GenerateCombinePdf] Save Time :" + sw.ElapsedMilliseconds + " Pages " + pages.Count + " log:\n" + sb.ToString());
                return newPath;
            }
        }

        public static int GetPDFPageCount(byte[] data)
        {
            PdfReader pdfReader = CreatePdfReader(data);

            return pdfReader.NumberOfPages;
        }

        public static void GetPdfInfo(byte[] data, out bool hasPdfOwnerPassword, out int pageCount)
        {
            hasPdfOwnerPassword = false;
            pageCount = 0;
            try
            {
                PdfReader reader = new PdfReader(data);   //if the pdf has a user password than this will fail with an IO exception
                hasPdfOwnerPassword = reader.IsOpenedWithFullPermissions == false;
                pageCount = reader.NumberOfPages;
            }
            catch (IOException e)
            {
                if (e.Message.Contains("Bad user Password"))
                {
                    Tools.LogWarning("[EDOCS-PDFOPEN] Invalid Password", e);
                    throw new InsufficientPdfPermissionException();
                }
                else
                {
                    Tools.LogWarning("[EDOCS-PDFOPEN]", e);
                    throw new InvalidPDFFileException();
                }
            }
        }

        public static void GetPdfInfo(string path, out int pageCount)
        {
            try
            {

                RandomAccessFileOrArray raf = new RandomAccessFileOrArray(path);
                PdfReader reader = new PdfReader(raf, null);   //if the pdf has a user password than this will fail with an IO exception
                pageCount = reader.NumberOfPages;
                reader.Close();
            }
            catch (IOException e)
            {
                if (e.Message.Contains("Bad user Password"))
                {
                    Tools.LogWarning("[EDOCS-PDFOPEN] Invalid Password", e);
                    throw new InsufficientPdfPermissionException();
                }
                else
                {
                    Tools.LogWarning("[EDOCS-PDFOPEN]", e);
                    throw new InvalidPDFFileException();
                }
            }
        }

        public static PdfReader CreatePdfReader(byte[] data)
        {
            PdfReader reader;
            try
            {
                reader = new PdfReader(data);
            }
            catch (IOException e)
            {
                if (e.Message.Contains("Bad user Password", StringComparison.OrdinalIgnoreCase))
                {
                    throw new InsufficientPdfPermissionException();
                }
                else if (e.Message.Contains("PDF header signature not found"))
                {
                    throw new InvalidPDFFileException();
                }

                throw new CBaseException(ErrorMessages.EDocs.InvalidPDF, e);
            }

            if (!reader.IsOpenedWithFullPermissions)
            {
                throw new InsufficientPdfPermissionException();
            }

            return reader;
        }

        public static PdfReader CreatePdfReader(string path)
        {
            using (PerformanceStopwatch.Start("EDocumentViewer.CreatePdfReader"))
            {
                PdfReader reader;
                try
                {
                    reader = new PdfReader(path, null);
                }
                catch (IOException e)
                {
                    if (e.Message.Contains("Bad user Password", StringComparison.OrdinalIgnoreCase))
                    {
                        throw new InsufficientPdfPermissionException();
                    }
                    else if (e.Message.Contains("PDF header signature not found"))
                    {
                        throw new InvalidPDFFileException();
                    }

                    throw new CBaseException(ErrorMessages.EDocs.InvalidPDF, e);
                }

                if (!reader.IsOpenedWithFullPermissions)
                {
                    throw new InsufficientPdfPermissionException();
                }

                return reader;
            }
        }

        /// <summary>
        /// Instead of forcing the pdf reader to read the entire pdf it only reads the stuff it needs. 
        /// It keeps reading randomly. Note that this will not work for editing edocs.. 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static PdfReader CreateRandomAccessPdfReader(string path)
        {
            RandomAccessFileOrArray raf = new RandomAccessFileOrArray(path);
            PdfReader reader;
            try
            {
                reader = new PdfReader(raf, null);
            }
            catch (IOException e)
            {
                if (e.Message.Contains("PDF header signature not found"))
                {
                    throw new InvalidPDFFileException();
                }
                throw new CBaseException(ErrorMessages.EDocs.InvalidPDF, e);
            }

            if (!reader.IsOpenedWithFullPermissions)
            {
                throw new InsufficientPdfPermissionException();
            }
            return reader;
        }

        /// <summary>
        /// Writes the given documents to a zip file. Merges all documents that
        /// are not signed into a single pdfs. Each signed document will get its
        /// own pdf.
        /// </summary>
        /// <param name="sLId">The loan id.</param>
        /// <param name="documentIds">The documents.</param>
        /// <returns>The path to the zip file.</returns>
        public static string WriteToZipMergingDocsWithoutSignature(Guid sLId, List<Guid> documentIds)
        {
            return WriteToZip(sLId, documentIds, mergeUnsignedDocs: true);
        }

        public static string WriteToZip(Guid sLId, List<Guid> documentIds)
        {
            return WriteToZip(sLId, documentIds, mergeUnsignedDocs: false);
        }

        private static string WriteToZip(Guid sLId, List<Guid> documentIds, bool mergeUnsignedDocs)
        {
            if (documentIds == null || documentIds.Count == 0)
            {
                throw new CBaseException("No document to generate.", "No document to generate.");
            }

            EDocumentRepository repository = EDocumentRepository.GetUserRepository();
            Dictionary<Guid, EDocument> docsInLoan = repository.GetDocumentsByLoanId(sLId).ToDictionary(p => p.DocumentId);

            if (documentIds.Any(p => !docsInLoan.ContainsKey(p)))
            {
                throw new FileNotFoundException("Documents not found in loan");
            }

            IEnumerable<Guid> documentIdsToZip;
            IEnumerable<Guid> documentIdsToMerge;
            string mergedDocumentPath = null;

            if (mergeUnsignedDocs)
            {
                var signedDocumentIds = documentIds.Where(id => docsInLoan[id].IsESigned);
                documentIdsToZip = signedDocumentIds;
                documentIdsToMerge = documentIds.Except(signedDocumentIds);

            }
            else
            {
                documentIdsToZip = documentIds;
                documentIdsToMerge = Enumerable.Empty<Guid>();
            }

            if (documentIdsToMerge.Any())
            {
                mergedDocumentPath = WriteSinglePdf(documentIdsToMerge, docsInLoan);
            }

            string zipPath = TempFileUtils.NewTempFilePath() + ".zip";
            byte[] buffer = new byte[32768];

            using (FileStream fs = new FileStream(zipPath, FileMode.Create))
            {
                ZipOutputStream zipoutput = new ZipOutputStream(fs);

                WriteDocumentsToZip(documentIdsToZip.ToList(), docsInLoan, zipoutput, buffer);

                if (mergedDocumentPath != null)
                {
                    BufferFileToZipStream(mergedDocumentPath, "ShippingPackage.pdf", zipoutput, buffer);
                }

                zipoutput.Finish();
                zipoutput.Close();
            }

            return zipPath;
        }

        /// <summary>
        /// Writes the given specified documents to the zip output stream. Does not verify that the documents
        /// are all from the same loan or check permissions.
        /// </summary>
        /// <param name="documentIds">The document ids to include in the zip stream.</param>
        /// <param name="docsInLoan">A map from document id to document.</param>
        /// <param name="zipoutput">The output stream.</param>
        private static void WriteDocumentsToZip(List<Guid> documentIds, Dictionary<Guid, EDocument> docsInLoan, ZipOutputStream zipoutput, byte[] buffer)
        {
            string sequenceFormat = GetFilenameSequenceFormat(documentIds);
            int i = 1;

            foreach (Guid docId in documentIds)
            {
                EDocument doc = docsInLoan[docId];
                string sequence = string.Format(sequenceFormat, i);
                // 1/12/2010 dd - Replace all non-alphanumeric characters with underscore. This is to ensure the filename 
                // is valid for different platforms.
                string newName = Regex.Replace(doc.DocTypeName, "[^a-zA-Z0-9]", "_");
                var zipEntryName = string.Format("{0}-{1}.pdf", sequence, newName);

                try
                {
                    string tempPdfPath = doc.GetPDFTempFile_Current();
                    BufferFileToZipStream(tempPdfPath, zipEntryName, zipoutput, buffer);
                    i++;
                }
                catch (FileNotFoundException exc)
                {
                    exc.Data["DocData_DocumentId"] = doc.DocumentId;
                    exc.Data["DocData_FolderAndDocTypeName"] = doc.FolderAndDocTypeName;
                    exc.Data["DocData_Description"] = doc.PublicDescription;
                    exc.Data["DocData_PageCount"] = doc.PageCount;
                    throw exc;
                }
            }
        }

        private static void BufferFileToZipStream(string tempPdfPath, string zipEntryName, ZipOutputStream zipoutput, byte[] buffer)
        {
            int bytesRead = 0;
            FileInfo fi = new FileInfo(tempPdfPath);
            ZipEntry e = new ZipEntry(zipEntryName);
            e.Size = fi.Length;// 1/15/2010 dd - Need to specify the file size here otherwise windows will not be able to unzip.
            zipoutput.PutNextEntry(e);

            using (FileStream nfs = fi.OpenRead())
            {
                while ((bytesRead = nfs.Read(buffer, 0, buffer.Length)) > 0)
                {
                    zipoutput.Write(buffer, 0, bytesRead);
                }
            }
        }

        private static string GetFilenameSequenceFormat(List<Guid> documentIds)
        {
            string sequenceFormat = "{0}";
            if (documentIds.Count < 10)
            {
                sequenceFormat = "{0:d1}";
            }
            else if (documentIds.Count < 100)
            {
                sequenceFormat = "{0:d2}";
            }
            else if (documentIds.Count < 1000)
            {
                sequenceFormat = "{0:d3}";
            }
            else if (documentIds.Count < 10000)
            {
                sequenceFormat = "{0:d5}";
            }

            return sequenceFormat;
        }

        public static void WriteSinglePdf(List<Tuple<string, Func<byte[]>, Guid>> PdfBytes, Stream outputStream)
        {
            if (PdfBytes == null || PdfBytes.Count == 0)
            {
                throw new CBaseException("No document to generate.", "No document to generate.");
            }
            Guid? lastDocId = new Guid?();
            try
            {
                using (Document document = new Document())
                {
                    PdfCopy pdfWriter = new PdfCopy(document, outputStream);
                    document.Open();

                    PdfContentByte cb = pdfWriter.DirectContent;
                    List<PdfReader> readers = new List<PdfReader>();
                    int pageCount = 1;
                    var allBookmarks = new List<Dictionary<string, object>>();

                    foreach (var docInfo in PdfBytes)
                    {
                        lastDocId = docInfo.Item3;

                        PdfReader pdfReader = CreatePdfReader(docInfo.Item2());
                        try
                        {
                            var NewBookmarkRoot = new Dictionary<string, object>();
                            NewBookmarkRoot["Title"] = docInfo.Item1;
                            NewBookmarkRoot["Action"] = "GoTo";
                            NewBookmarkRoot["Page"] = pageCount.ToString();

                            var bookmarkPrefix = pageCount + "_";

                            IList<Dictionary<string, object>> CurrentDocumentBookmarks = null;
                            try
                            {
                                CurrentDocumentBookmarks = SimpleBookmark.GetBookmark(pdfReader);
                            }
                            catch (NullReferenceException e)
                            {
                                //This will just happen on some docs, there's nothing we can do.
                                Tools.LogWarning("Document with incompatible bookmarks marked for shipping. " +
                                                "It will not have its own bookmarks in the shipping pdf. Doc name: " + docInfo.Item1, e);
                            }

                            if (CurrentDocumentBookmarks != null)
                            {
                                SimpleBookmark.ShiftPageNumbers(CurrentDocumentBookmarks, pageCount - 1, null);
                                CurrentDocumentBookmarks = PrefixNamedBookmarks(CurrentDocumentBookmarks, bookmarkPrefix);
                                NewBookmarkRoot["Kids"] = CurrentDocumentBookmarks;
                            }

                            var namedDestinations = GetAllDestinations(pdfReader);
                            if (namedDestinations != null && namedDestinations.Count > 0)
                            {
                                //Note that if a user merges two documents that have the same named bookmarks, they will all end up pointing to
                                //the first named destination with that name.
                                //This could be remedied by namespacing the named destinations & bookmarks, but that will break internal links
                                //(e.g, from a table of contents) unless we also namespace them (which would be hard, since there's more than one way to do it).
                                //Since internal links are probably more common than users who for some reason decide to include the same pdf
                                //twice and want the internal bookmarks to work, we won't namespace things.
                                namedDestinations = PrefixNamedDestinations(namedDestinations, bookmarkPrefix);
                                namedDestinations = (from kvp in namedDestinations
                                                        select new
                                                        {
                                                            Key = kvp.Key,
                                                            Value = kvp.Value.Replace("null", "0")
                                                        }).ToDictionary(a => a.Key, a => a.Value);
                                pdfWriter.AddNamedDestinations(namedDestinations, pageCount - 1);
                            }

                            allBookmarks.Add(NewBookmarkRoot);

                            for (int pageNum = 1; pageNum <= pdfReader.NumberOfPages; pageNum++)
                            {
                                var page = pdfReader.GetPageN(pageNum);
                                PrefixNamedLinks(page, bookmarkPrefix);
                                var importedPage = pdfWriter.GetImportedPage(pdfReader, pageNum);
                                pdfWriter.AddPage(importedPage);

                                pageCount++;
                            }
                        }
                        finally
                        {
                            pdfReader.Close();
                        }
                    }

                    pdfWriter.Outlines = allBookmarks;
                }
            }
            catch(Exception exc)
            {
                if (lastDocId.HasValue && lastDocId.Value != Guid.Empty)
                {
                    Tools.LogError("[WriteSinglePdfCorrupt] UserNm=" + PrincipalFactory.CurrentPrincipal.LoginNm + " LastDocId: " + lastDocId.Value, exc);

                    EDocumentRepository repository = EDocumentRepository.GetUserRepository();
                    EDocument doc = repository.GetDocumentById(lastDocId.Value);

                    exc.Data["DocData_DocumentId"] = doc.DocumentId;
                    exc.Data["DocData_FolderAndDocTypeName"] = doc.FolderAndDocTypeName;
                    exc.Data["DocData_Description"] = doc.PublicDescription;
                    exc.Data["DocData_PageCount"] = doc.PageCount;
                    throw;
                }

                throw;
            }
            
        }

        public static string WriteSinglePdf(Guid sLId, List<Guid> documentIds)
        {
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            Dictionary<Guid, EDocument> docsByLoanId = repo.GetDocumentsByLoanId(sLId).ToDictionary(p => p.DocumentId);

            foreach (var docId in documentIds)
            {
                if (!docsByLoanId.ContainsKey(docId))
                {
                    throw new FileNotFoundException("Document not found " + docId);
                }
            }

            return WriteSinglePdf(documentIds, docsByLoanId);
        }

        public static void WriteSinglePdfToEDocument(Guid sLId, List<Guid> documentIds, Guid appId, int docTypeId, string description, string internalComments)
        {
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            Dictionary<Guid, EDocument> docsByLoanId = repo.GetDocumentsByLoanId(sLId).ToDictionary(p => p.DocumentId);

            foreach (var docId in documentIds)
            {
                if (!docsByLoanId.ContainsKey(docId))
                {
                    throw new FileNotFoundException("Document not found " + docId);
                }
            }

            EDocument document = repo.CreateDocument(EDocs.E_EDocumentSource.UserUpload);
            document.LoanId = sLId;
            document.AppId = appId;
            document.DocumentTypeId = docTypeId;
            document.PublicDescription = description;
            document.InternalDescription = internalComments;

            int pagesProcessed = 0;
            foreach (Guid docId in documentIds)
            {
                EDocument doc = docsByLoanId[docId];
                foreach (EDocumentAnnotationItem tag in doc.ESignTags.AnnotationList)
                {
                    var copyTag = tag.CreateDeepCopy();
                    copyTag.PageNumber += pagesProcessed;
                    document.ESignTags.Add(copyTag);
                }

                pagesProcessed += doc.PageCount;
            }

            string tempFile = WriteSinglePdf(documentIds, docsByLoanId);
            document.UpdatePDFContentOnSave(tempFile);
            repo.Save(document);
        }

        /// <summary>
        /// Generates a single pdf from the given document ids. Does not verify that the documents
        /// are all from the same loan or check permissions.
        /// </summary>
        /// <param name="documentIds">The documents to combine.</param>
        /// <param name="docsByLoanId">A map from document id to document.</param>
        /// <returns>The path to the created file.</returns>
        private static string WriteSinglePdf(IEnumerable<Guid> documentIds, Dictionary<Guid, EDocument> docsByLoanId)
        {
            string file = TempFileUtils.NewTempFilePath();
            var allBookmarks = new List<Dictionary<string, object>>();
            int pageCount = 1;

            using (FileStream fs = new FileStream(file, FileMode.Create))
            {
                using (Document document = new Document())
                {
                    PdfCopy pdfWriter = new PdfCopy(document, fs);
                    document.Open();

                    PdfContentByte cb = pdfWriter.DirectContent;

                    foreach (var id in documentIds)
                    {
                        EDocument doc = docsByLoanId[id];

                        string path = doc.GetPDFTempFile_Current();

                        PdfReader pdfReader = CreatePdfReader(path);
                        try
                        {
                            var NewBookmarkRoot = new Dictionary<string, object>();
                            NewBookmarkRoot["Title"] = doc.DocTypeName;
                            NewBookmarkRoot["Action"] = "GoTo";
                            NewBookmarkRoot["Page"] = pageCount.ToString();

                            var bookmarkPrefix = pageCount + "_";

                            IList<Dictionary<string, object>> CurrentDocumentBookmarks = null;
                            try
                            {
                                CurrentDocumentBookmarks = SimpleBookmark.GetBookmark(pdfReader);
                            }
                            catch (NullReferenceException e)
                            {
                                //This will just happen on some docs, there's nothing we can do.
                                Tools.LogWarning("Document with incompatible bookmarks marked for shipping. " +
                                              "It will not have its own bookmarks in the shipping pdf. Doc name: " + doc.DocTypeName, e);
                            }

                            if (CurrentDocumentBookmarks != null)
                            {
                                SimpleBookmark.ShiftPageNumbers(CurrentDocumentBookmarks, pageCount - 1, null);
                                CurrentDocumentBookmarks = PrefixNamedBookmarks(CurrentDocumentBookmarks, bookmarkPrefix);
                                NewBookmarkRoot["Kids"] = CurrentDocumentBookmarks;
                            }

                            var namedDestinations = GetAllDestinations(pdfReader);
                            if (namedDestinations != null && namedDestinations.Count > 0)
                            {
                                //Note that if a user merges two documents that have the same named bookmarks, they will all end up pointing to
                                //the first named destination with that name.
                                //This could be remedied by namespacing the named destinations & bookmarks, but that will break internal links
                                //(e.g, from a table of contents) unless we also namespace them (which would be hard, since there's more than one way to do it).
                                //Since internal links are probably more common than users who for some reason decide to include the same pdf
                                //twice and want the internal bookmarks to work, we won't namespace things.
                                namedDestinations = PrefixNamedDestinations(namedDestinations, bookmarkPrefix);
                                namedDestinations = (from kvp in namedDestinations
                                                     select new
                                                     {
                                                         Key = kvp.Key,
                                                         Value = kvp.Value.Replace("null", "0")
                                                     }).ToDictionary(a => a.Key, a => a.Value);
                                pdfWriter.AddNamedDestinations(namedDestinations, pageCount - 1);
                            }

                            allBookmarks.Add(NewBookmarkRoot);

                            for (int pageNum = 1; pageNum <= pdfReader.NumberOfPages; pageNum++)
                            {
                                var page = pdfReader.GetPageN(pageNum);
                                PrefixNamedLinks(page, bookmarkPrefix);
                                var importedPage = pdfWriter.GetImportedPage(pdfReader, pageNum);
                                pdfWriter.AddPage(importedPage);

                                pageCount++;
                            }
                        }
                        finally
                        {
                            pdfReader.Close();
                        }

                    }


                    pdfWriter.Outlines = allBookmarks;
                }
            }

            return file;
        }

        public static void WriteSinglePdf(List<Guid> docIdList, Stream outputStream)
        {
            var toMerge = new List<Tuple<string, Func<byte[]>, Guid>>(docIdList.Count);
            EDocumentRepository repository = EDocumentRepository.GetUserRepository();
            foreach(var docId in docIdList)
            {
                EDocument doc = repository.GetDocumentById(docId);
                Func<byte[]> getBytes = () =>
                {
                    try
                    {
                        return File.ReadAllBytes(doc.GetPDFTempFile_Current());
                    }
                    catch (Exception exc)
                    {
                        exc.Data["DocData_DocumentId"] = doc.DocumentId;
                        exc.Data["DocData_FolderAndDocTypeName"] = doc.FolderAndDocTypeName;
                        exc.Data["DocData_Description"] = doc.PublicDescription;
                        exc.Data["DocData_PageCount"] = doc.PageCount;
                        throw;
                    }
                };

                toMerge.Add(Tuple.Create(doc.DocTypeName, getBytes, doc.DocumentId));
            }

            WriteSinglePdf(toMerge, outputStream);
        }

        public static Dictionary<string, string> GetAllDestinations(PdfReader pdfReader)
        {
            var ret = new Dictionary<string,string>();
            var fromStrings = SimpleNamedDestination.GetNamedDestination(pdfReader, false);
            var fromNames = SimpleNamedDestination.GetNamedDestination(pdfReader, true);

            foreach (var kvp in fromNames)
            {
                ret[kvp.Key] = kvp.Value;
            }

            foreach (var kvp in fromStrings)
            {
                ret[kvp.Key] = kvp.Value;
            }

            return ret;
        }

        /// <summary>
        /// Renames all named destinations from an iTextSharp SimpleNamedDestination.GetNamedDestination to be of the format [prefix][old name]. Use with NamespaceNamedBookmarks.
        /// </summary>
        /// <param name="namedDestinations"></param>
        /// <param name="prefix"></param>
        private static Dictionary<string, string> PrefixNamedDestinations(Dictionary<string, string> namedDestinations, string prefix)
        {
            return (from a in namedDestinations 
                    select new { Key = prefix + a.Key, Value = a.Value })
                    .ToDictionary(a => a.Key, a => a.Value);
        }

        /// <summary>
        /// Searches through a SimpleBookmark.GetBookmark() structure looking for actions to go to a specific page; when it finds one, it changes the destination page name to "[prefix][old page name]". Use with NamespaceNamedDestinations.
        /// </summary>
        /// <param name="bookmarks">The result of a call to SimpleBookmark.GetBookmark()</param>
        /// <param name="prefix">The namespace prefix.</param>
        private static IList<Dictionary<string, object>> PrefixNamedBookmarks(IList<Dictionary<string, object>> bookmarks, string prefix)
        {
            //We could call this function recursively, but using an explicit stack seems cleaner.
            var lookAt = new Stack<Dictionary<string, object>>(bookmarks);

            object kids;
            object target;
            while (lookAt.Count > 0)
            {
                var current = lookAt.Pop();
                if (current.TryGetValue("Kids", out kids) && kids is IList<Dictionary<string, object>>)
                {
                    foreach (var kid in (IList<Dictionary<string, object>>)kids)
                    {
                        lookAt.Push(kid);
                    }
                }

                if (current.TryGetValue("Named", out target) && target is string)
                {
                    current["Named"] = prefix + target;
                }
            }

            return bookmarks;
        }

        /// <summary>
        /// Looks through a PdfDictionary page (created with PdfReader.GetPageN()) and renames internal links to [prefix][old name] using a couple of different methods.
        /// </summary>
        /// <param name="page"></param>
        /// <param name="prefix"></param>
        private static void PrefixNamedLinks(PdfDictionary page, string prefix)
        {
            var annotations = page.GetAsArray(PdfName.ANNOTS);
            
            if (annotations == null || annotations.Length == 0) return;

            foreach (var annotation in from a in annotations.ArrayList select (PdfDictionary)PdfReader.GetPdfObject(a))
            {
                //The Dest key and the Action key are mutually exclusive so we can just call both
                PrefixNamedDestination(annotation, prefix);
                PrefixNamedAction(annotation, prefix);
            }
        }

        private static bool PrefixNamedDestination(PdfDictionary annotation, string prefix)
        {
            PdfString destination = annotation.Get(PdfName.DEST) as PdfString;
            if (destination == null || !destination.IsString()) return false;
            annotation.Put(PdfName.DEST, new PdfString(prefix + destination.ToString()));
            return true;
        }

        private static bool PrefixNamedAction(PdfDictionary annotation, string prefix)
        {
            PdfDictionary action = annotation.Get(PdfName.A) as PdfDictionary;
            if (action == null) return false;

            var type = action.Get(PdfName.S);
            if (type == null) return false;
            if (!type.Equals(PdfName.GOTO)) return false;
            var destination = action.Get(PdfName.D) as PdfString;
            if (destination == null) return false;

            action.Put(PdfName.D, new PdfString(prefix + destination.ToString()));

            return true;
        }



        /// <summary>
        /// Creates a copy of a file within FileDB.
        /// </summary>
        /// <param name="key">The FileDB key of the file to copy.</param>
        /// <param name="newKey">The FileDB key of the copy.</param>
        /// <returns>True if the copy occurred successfully. Otherwise, false.</returns>
        private static bool CreateFileDBCopy(string key, string newKey)
        {
            try
            {
                FileDBTools.Copy(E_FileDB.EDMS, key, newKey);
                return true;
            }
            catch (FileNotFoundException)
            {
                return false;
            }
        }

        public static void SignPdf(BrokerUserPrincipal principal, Guid pdfid, List<PdfField> signatureFields)
        {
            if (signatureFields.Count == 0)
            {
                return;
            }

            EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(principal.BrokerId, principal.EmployeeId, principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);
            
            if (signInfo.HasUploadedSignature == false)
            {
                throw CBaseException.GenericException("Tried signing a pdf but don't have a signature.");
            }

            string path = FileDBTools.CreateCopy(E_FileDB.EDMS, pdfid.ToString());
            byte[] sigImg = FileDBTools.ReadData(E_FileDB.Normal, signInfo.SignatureKey.ToString());
            byte[] dateImg = File.ReadAllBytes(GeneratePngForToday());

            Action<Stream> saveDelegate = stream =>
            {
                PdfReader reader = CreatePdfReader(path);
                PdfStamper stamper = new PdfStamper(reader, stream);
                foreach (PdfField field in signatureFields)
                {
                    byte[] imgData = field.Type == PdfFieldType.Signature ? sigImg : dateImg;
                    var pdfcontentbyte = stamper.GetOverContent(field.PageNumber);
                    var img = iTextSharp.text.Image.GetInstance(imgData);
                    img.ScaleToFit(field.Rectangle.Width, field.Rectangle.Height);
                    img.SetAbsolutePosition(field.Rectangle.Left, field.Rectangle.Bottom);
                    pdfcontentbyte.AddImage(img);
                }
                stamper.Close();
            };

            FileDBTools.WriteData(E_FileDB.EDMS, pdfid.ToString(), saveDelegate);
        }

        public static void SignPdf(BrokerUserPrincipal principal, string key, List<PdfField> fields)
        {
            if (fields.Count == 0 || !principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp)
            {
                return;
            }

            byte[] pdfData = AutoExpiredTextCache.GetBytesFromCache(key);
            EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(principal.BrokerId, principal.EmployeeId, principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);
            
            if (signInfo.HasUploadedSignature == false)
            {
                throw CBaseException.GenericException("Tried signing a pdf but don't have a signature.");
            }

            using (MemoryStream ms = new MemoryStream())
            {
                PdfReader reader = CreatePdfReader(pdfData);
                PdfStamper stamper = new PdfStamper(reader, ms);
                byte[] dateImg = File.ReadAllBytes(GeneratePngForToday());
                byte[] sigImg = FileDBTools.ReadData(E_FileDB.Normal, signInfo.SignatureKey.ToString());
                foreach (PdfField field in fields)
                {
                    byte[] imgData = field.Type == PdfFieldType.Signature ? sigImg : dateImg;
                    var pdfcontentbyte = stamper.GetOverContent(field.PageNumber);
                    var img = iTextSharp.text.Image.GetInstance(imgData);
                    img.ScaleToFit(field.Rectangle.Width, field.Rectangle.Height);
                    img.SetAbsolutePosition(field.Rectangle.Left, field.Rectangle.Bottom);
                    pdfcontentbyte.AddImage(img);
                }
                stamper.Close();
                AutoExpiredTextCache.UpdateCacheBytes(ms.ToArray(), key);
            }


        }

    }
}