﻿namespace LendersOffice.ObjLib.Edocs
{
    using System;
    using System.Messaging;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;

    /// <summary>
    /// Manages the EDoc Status Update queue.
    /// </summary>
    public static class EDocStatusUpdateQueue
    {
        /// <summary>
        /// Sends an item to the queue.
        /// </summary>
        /// <param name="item">An item to queue up.</param>
        public static void Send(EDocStatusUpdateQueueItem item)
        {
            if (item == null || string.IsNullOrEmpty(ConstStage.MSMQ_EDoc_StatusUpdateQueue))
            {
                return;
            }

            MessageQueueHelper.SendXML(ConstStage.MSMQ_EDoc_StatusUpdateQueue, null, item.ToXDocument());

            Tools.LogInfo(
                $"EDocStatusUpdateQueue::Send - Sent item - "
                + $"LoanId:{item.LoanId}, BrokerId:{item.BrokerId}, DocumentId:{item.DocumentId}, "
                + $"OriginStatus:{item.OriginStatus}, DestinationStatus{item.DestinationStatus}");
        }

        /// <summary>
        /// Retrieves an item from the queue.
        /// </summary>
        /// <returns>The next item in the queue.</returns>
        public static EDocStatusUpdateQueueItem Receive()
        {
            if (string.IsNullOrEmpty(ConstStage.MSMQ_EDoc_StatusUpdateQueue))
            {
                return null;
            }

            using (var queue = MessageQueueHelper.PrepareForXML(ConstStage.MSMQ_EDoc_StatusUpdateQueue, false))
            {
                try
                {
                    DateTime arrivalTime;
                    XDocument doc = MessageQueueHelper.ReceiveXML(queue, null, out arrivalTime);

                    if (doc == null)
                    {
                        return null;
                    }

                    return EDocStatusUpdateQueueItem.Parse(doc);
                }
                catch (MessageQueueException exc) when (exc.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                {
                    return null; // No item in queue.
                }
            }
        }
    }
}
