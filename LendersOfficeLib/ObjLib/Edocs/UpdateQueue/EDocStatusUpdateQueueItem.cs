﻿namespace LendersOffice.ObjLib.Edocs
{
    using System;
    using System.Xml.Linq;
    using EDocs;

    /// <summary>
    /// A queue item that triggers an update to the status of an EDoc.
    /// </summary>
    public class EDocStatusUpdateQueueItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EDocStatusUpdateQueueItem"/> class.
        /// </summary>
        /// <param name="loanId">The loan ID associated with the EDoc.</param>
        /// <param name="brokerId">The broker ID associated with the EDoc.</param>
        /// <param name="documentId">The ID of the EDoc.</param>
        /// <param name="originStatus">A doc must have this status to be updated.</param>
        /// <param name="destinationStatus">If updated, the doc will be set to this status.</param>
        public EDocStatusUpdateQueueItem(Guid loanId, Guid brokerId, Guid documentId, E_EDocStatus originStatus, E_EDocStatus destinationStatus)
        {
            this.LoanId = loanId;
            this.BrokerId = brokerId;
            this.DocumentId = documentId;
            this.OriginStatus = originStatus;
            this.DestinationStatus = destinationStatus;
        }

        /// <summary>
        /// Gets the loan ID associated with the document.
        /// </summary>
        /// <value>The loan ID associated with the document.</value>
        public Guid LoanId { get; private set; }

        /// <summary>
        /// Gets the broker ID associated with the document.
        /// </summary>
        /// <value>The broker ID associated with the document.</value>
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets the ID of the document to be updated.
        /// </summary>
        /// <value>The ID of the document to be updated.</value>
        public Guid DocumentId { get; private set; }

        /// <summary>
        /// Gets the origin document status. The document must
        /// currently have this status to be updated.
        /// </summary>
        /// <value>The origin document status.</value>
        public E_EDocStatus OriginStatus { get; private set; }

        /// <summary>
        /// Gets the destination status. If the document is currently set
        /// to the origin status, it will be updated to the destination status.
        /// </summary>
        /// <value>The destination status.</value>
        public E_EDocStatus DestinationStatus { get; private set; }

        /// <summary>
        /// Parses the queue item information out of an XDocument.
        /// </summary>
        /// <param name="doc">The XDocument containing the queue item information.</param>
        /// <returns>A queue item object.</returns>
        public static EDocStatusUpdateQueueItem Parse(XDocument doc)
        {
            if (doc == null)
            {
                return null;
            }

            XElement root = doc.Root;

            var loanId = Guid.Empty;
            var brokerId = Guid.Empty;
            var documentId = Guid.Empty;
            var targetDocStatus = E_EDocStatus.Blank;
            var updatedStatus = E_EDocStatus.Blank;

            XAttribute attribute = null;

            attribute = root.Attribute("LoanId");
            if (attribute != null)
            {
                loanId = Guid.Parse(attribute.Value);
            }

            attribute = root.Attribute("BrokerId");
            if (attribute != null)
            {
                brokerId = Guid.Parse(attribute.Value);
            }

            attribute = root.Attribute("DocumentId");
            if (attribute != null)
            {
                documentId = Guid.Parse(attribute.Value);
            }

            attribute = root.Attribute("OriginStatus");
            if (attribute != null)
            {
                targetDocStatus = (E_EDocStatus)Enum.Parse(typeof(E_EDocStatus), attribute.Value);
            }

            attribute = root.Attribute("DestinationStatus");
            if (attribute != null)
            {
                updatedStatus = (E_EDocStatus)Enum.Parse(typeof(E_EDocStatus), attribute.Value);
            }

            return new EDocStatusUpdateQueueItem(loanId, brokerId, documentId, targetDocStatus, updatedStatus);
        }

        /// <summary>
        /// Converts the queue item to XML.
        /// </summary>
        /// <returns>An XDocument holding the queue item information.</returns>
        public XDocument ToXDocument()
        {
            XDocument doc = new XDocument(
                new XElement(
                    "item",
                    new XAttribute(nameof(this.LoanId), this.LoanId.ToString()),
                    new XAttribute(nameof(this.BrokerId), this.BrokerId.ToString()),
                    new XAttribute(nameof(this.DocumentId), this.DocumentId.ToString()),
                    new XAttribute(nameof(this.OriginStatus), this.OriginStatus.ToString("D")),
                    new XAttribute(nameof(this.DestinationStatus), this.DestinationStatus.ToString("D"))));

            return doc;
        }
    }
}
