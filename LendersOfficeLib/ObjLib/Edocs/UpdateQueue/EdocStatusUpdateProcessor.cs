﻿namespace LendersOffice.ObjLib.Edocs
{
    using System;
    using System.Linq;
    using CommonProjectLib.Runnable;
    using DataAccess;
    using EDocs;

    /// <summary>
    /// Processes EDoc Status Update queue items.
    /// </summary>
    public class EDocStatusUpdateProcessor : IRunnable
    {
        /// <summary>
        /// Gets a description of the processor.
        /// </summary>
        /// <value>A description of the processor.</value>
        public string Description
        {
            get { return "Updates an EDoc status."; }
        }

        /// <summary>
        /// Processes each EDoc status update item in the queue.
        /// </summary>
        public void Run()
        {
            while (true)
            {
                var item = EDocStatusUpdateQueue.Receive();

                if (item == null)
                {
                    return; // Queue is empty.
                }

                try
                {
                    var repo = EDocumentRepository.GetSystemRepository(item.BrokerId);
                    var edoc = repo.GetDocumentsByLoanId(item.LoanId)
                        .FirstOrDefault(doc => doc.DocumentId == item.DocumentId);

                    if (edoc == null)
                    {
                        continue; // Doc not found.
                    }

                    if (edoc.DocStatus == item.OriginStatus)
                    {
                        edoc.DocStatus = item.DestinationStatus;
                        repo.Save(edoc);
                    }
                    else
                    {
                        Tools.LogInfo($"Document {edoc.DocumentId} did not have its status updated because it had a status of {edoc.DocStatus} and would only be updated if it had a status of {item.OriginStatus}.");
                    }
                }
                catch (CBaseException exc)
                {
                    Tools.LogError(exc);
                    throw;
                }
                catch (SystemException exc)
                {
                    Tools.LogError(exc);
                    throw;
                }
            }
        }
    }
}
