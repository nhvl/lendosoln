﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.ObjLib.Edocs
{
    public interface IDocVendorBarcode
    {
        string sLNm { get; }
        string DocumentClass { get; }
        int PackagePageNumber { get; }
    }
    public interface IDocVendorBarcodeWithDocPageNum : IDocVendorBarcode
    {
        int DocumentPageNumber { get; }
    }
}
