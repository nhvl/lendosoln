﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.ObjLib.Edocs
{
    public sealed class Barcode
    {
        public string Data { get; private set; }
        public int Page { get; set; }
        public int Score { get; set; }

        public Barcode(int page, string data)
        {
            Data = data;
            Page = page;
        }
    }
}
