﻿namespace LendersOffice.AntiXss
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.HtmlControls;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Extensions;

    /// <summary>
    /// This class is tightly integrate with StorageTools.
    /// </summary>
    /// <remarks>
    /// NOTE: This file is duplicated into CommonControlsAspxTools in the MeridianLinkCommonControls project. 
    /// This was done to avoid having that project depend on LendersOfficeLib. If you are updating existing 
    /// functionality here, please make a corresponding update to the MeridianLinkCommonControls version.
    /// </remarks>
    public static class AspxTools
    {
        private static string[] SafeJsCharacters = new string[char.MaxValue];
        private static string[] SafeHtmlCharacters = new string[char.MaxValue];
        private static LqbSingleThreadInitializeLazy<string[]> SafeUrlWhitelist = 
            new LqbSingleThreadInitializeLazy<string[]>(() => new string[] { ConstSite.EDocsSite, ConstSite.LOSite, ConstStage.AmazonEdocsClientUrl });

        static AspxTools()
        {
            ConstructSafeJsCharacters();
            ConstructSafeHtmlCharacters();
        }
        private static bool IsWhiteList(char ch)
        {
            // Only following characters are consider safe and can be render back as-is.
            if (ch == ' ' || ch == '-' || ch == ',' || ch == '.' || ch == '$' || ch == '_' || ch == ':')
            {
                return true;
            }
            if (ch >= 65 && ch <= 90) // A-Z
            {
                return true;
            }
            else if (ch >= 48 && ch <= 57) // 0-9
            {
                return true;
            }
            else if (ch >= 97 && ch <= 122) // a-z
            {
                return true;
            }
            return false;
        }
        private static void ConstructSafeHtmlCharacters()
        {
            for (char ch = char.MinValue; ch < char.MaxValue; ch++)
            {
                if (IsWhiteList(ch))
                {
                    SafeHtmlCharacters[ch] = ch.ToString();
                }
                else if (ch == '&')
                {
                    SafeHtmlCharacters[ch] = "&amp;";
                }
                else if (ch == '<')
                {
                    SafeHtmlCharacters[ch] = "&lt;";
                }
                else if (ch == '>')
                {
                    SafeHtmlCharacters[ch] = "&gt;";
                }
                else
                {
                    SafeHtmlCharacters[ch] = "&#x" + ((ushort) ch).ToString("X2") + ";"; // Convert to &#xFF
                }
            }
        }
        private static void ConstructSafeJsCharacters() 
        {
            for (char ch = char.MinValue; ch < char.MaxValue; ch++) {
                if (IsWhiteList(ch)) 
                {
                    SafeJsCharacters[ch] = ch.ToString();
                }
                else if (ch == '\'')
                {
                    SafeJsCharacters[ch] = "\\'";
                }
                else
                {
                    SafeJsCharacters[ch] = "\\x" + ((ushort) ch).ToString("X2"); // Convert to "\xFF"
                }
            }
        }

        public static string JsArray(List<object> list)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            if (null != list)
            {
                bool isFirst = true;
                foreach (object o in list)
                {
                    if (!isFirst)
                    {
                        sb.Append(",");
                    }
                    if (null != o)
                    {
                        Type t = o.GetType();
                        if (typeof(List<object>) == t)
                        {
                            sb.Append(JsArray((List<object>)o));
                        }
                        else if (typeof(int) == t || typeof(long) == t || typeof(decimal) == t || typeof(float) == t)
                        {
                            sb.Append(o.ToString());
                        }
                        else if (typeof(Guid) == t)
                        {
                            sb.Append(JsString((Guid)o));
                        }
                        else
                        {
                            sb.Append(JsString(o.ToString()));
                        }
                        
                    }
                    isFirst = false;

                }
            }
            sb.Append("]");
            return sb.ToString();
        }
        public static string JsArray(IEnumerable<string> list)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("[");
            if (null != list)
            {
                int i = 0;
                foreach (string item in list)
                {
                    if (i != 0)
                    {
                        sb.Append(",");
                    }

                    sb.Append(JsString(item));
                    i++;
                }
            }
            sb.Append("]");
            return sb.ToString();
        }
        public static string JsArray(params Enum[] list)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("[");
            if (null != list)
            {
                for (int i = 0; i < list.Length; i++)
                {
                    if (i > 0) sb.Append(",");
                    sb.Append(JsString(list[i]));
                }
            }
            sb.Append("]");
            return sb.ToString();
        }
        /// <summary>
        /// Usage:
        ///     var s = <%= AspxTools.JsString(m_serverSideString) %>;
        ///     
        /// Incorrect usage.
        ///     var s = '<%= m_serverSideString%>'; // This is unsafe. m_serverSideString may contains XSS script.
        ///     var s = '<%= AspxTools.JsString(m_serverSideString)%>'; // This is incorrect. AspxTools.JsString already return single quote.
        ///     var s = <%= AspxTools.JsString(m_serverSideString) + m_another%>; // This is incorect and unsafe.
        ///     
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string JsString(string input)
        {
            if (string.IsNullOrEmpty(input))
                return "''"; // Empty string.

            StringBuilder sb = new StringBuilder(input.Length * 4);
            sb.Append("'");

            char[] charList = input.ToCharArray();
            for (int i = 0; i < charList.Length; i++)
            {
                char ch = charList[i];
                // Need to take care of StorageTools.Decode algorithm.
                /*
                if (ch == StorageTools.Deliminator && ((i+1) < charList.Length))
                {

                    char ch1 = charList[i + 1];
                    if (StorageTools.IsDangerousCharacter(ch1))
                    {
                        sb.Append(SafeJsCharacters[ch1]);

                    }
                    else
                    {
                        sb.Append(SafeJsCharacters[ch]);
                        sb.Append(SafeJsCharacters[ch1]);
                    }
                    i++;
                }
                else
                {
                    sb.Append(SafeJsCharacters[ch]);
                }
                 * */
                sb.Append(SafeJsCharacters[ch]);

            }
            sb.Append("'");
            return sb.ToString();

        }
        public static string JsString(Guid input)
        {
            return "'" + input.ToString() + "'"; // Guid is safe.
        }
        public static string JsString(Enum input)
        {
            return "'" + input.ToString("D") + "'"; // 7/30/2010 dd - Always return numeric representation of enum.
        }
        public static string JsString(int input)
        {
            return "'" + input + "'"; // 7/30/2010 dd - Put numeric value in string quote
        }

        public static string JsNumeric(int input)
        {
            return input.ToString();
        }
        public static string JsNumeric(long input)
        {
            return input.ToString();
        }
        public static string JsNumeric(decimal input)
        {
            return input.ToString();
        }
        public static string JsNumeric(Enum input)
        {
            return input.ToString("D");
        }
        public static string JsBool(bool value)
        {
            return value ? "true" : "false";
        }

        // Thien's note: 10/3/2017 this special method is used for the attribute WebControls.CheckBox.Checked
        // If use JsStringUnquoted or JsBool with the above attribute, we will get the error message 
        //  “error CS0030: Cannot convert type 'string' to 'bool'”
        public static bool GetBool(bool value)
        {
            return value;
        }

        public static string JsGetElementById(Control ctrl)
        {
            if (null == ctrl)
                return "null";

            // We can safely assume ctrl.ClientId will not return any dangerous script.
            return "document.getElementById('" + ctrl.ClientID + "')";
        }
        public static string JsGetClientIdString(Control ctrl)
        {
            if (null == ctrl)
                return "''";
            return "'" + ctrl.ClientID + "'";
        }

        public static string JQuery(params Control[] ctrlList) {
            if (null == ctrlList || ctrlList.Length == 0)
            {
                throw new CBaseException(ErrorMessages.Generic, "Control List cannot be null or empty length");
            }
            // Small optimization for a single control: use getElementById rather than query by ID
            if (ctrlList.Length == 1)
            {
                return $"jQuery(document.getElementById('{ctrlList[0].ClientID}'))";
            }
            StringBuilder sb = new StringBuilder();
            sb.Append("jQuery(\"");
            for (int i = 0; i < ctrlList.Length; i++)
            {
                if (i != 0)
                {
                    sb.Append(",");
                }
                sb.Append("#" + ctrlList[i].ClientID);
            }
            sb.Append("\")");
            return sb.ToString();
        }

        public static string HtmlString<T>(T input)
        {
            return HtmlString(input?.ToString());
        }

        public static string HtmlString(string input)
        {
            if (string.IsNullOrEmpty(input))
                return "";
            StringBuilder sb = new StringBuilder(input.Length * 6);
            char[] charList = input.ToCharArray();
            for (int i = 0; i < charList.Length; i++)
            {
                char ch = charList[i];
                /*
                // Need to take care of StorageTools.Decode algorithm.
                if (ch == StorageTools.Deliminator && ((i + 1) < charList.Length))
                {

                    char ch1 = charList[i + 1];
                    if (StorageTools.IsDangerousCharacter(ch1))
                    {
                        sb.Append(SafeHtmlCharacters[ch1]);

                    }
                    else
                    {
                        sb.Append(SafeHtmlCharacters[ch]);
                        sb.Append(SafeHtmlCharacters[ch1]);
                    }
                    i++;
                }
                else
                {
                    sb.Append(SafeHtmlCharacters[ch]);
                }
                 * */
                sb.Append(SafeHtmlCharacters[ch]);

            }

            return sb.ToString();
        }
        public static string HtmlStringFromQueryString(string key)
        {
            string s = HttpContext.Current.Request.QueryString[key];

            return HtmlString(s);
        }
        public static string HtmlAttribute(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return "\"\"";
            }
            return "\"" + HtmlString(input) + "\"";
        }

        public static string ClientId(Control ctrl)
        {
            System.Diagnostics.Debug.Assert(ctrl != null);

            return ctrl.ClientID;
        }

        public static string JSelector(params Control[] ctrls)
        {
            if (ctrls.Length == 0)
            {
                return "";
            }
            StringBuilder sb = new StringBuilder();
            foreach( Control ctrl in ctrls)
            {
                sb.AppendFormat("#{0},", ctrl.ClientID);
            }

            sb.Length = sb.Length - 1; //remove last comma
            return sb.ToString();
        }

        /// <summary>
        /// Use this if you need to reference a local dynamic url in "src" attribute. DO NOT ENCLOSE the call with quote.
        /// This will also work for urls that use the hardcoded whitelist.
        /// Example:
        ///     DO
        ///         * <a href=<%= AspxTools.SafeUrl("/local/localpage.aspx?loanid=" + blah) %> >
        ///         * <img src=<%= AspxTools.SafeUrl(m_dynamicLink) %> >
        ///     DO NOT
        ///         * <a href=<%= AspxTools.HtmlAttribute("/local/localpage.aspx?loanid=" + blah) %>  // This will result in invalid url.
        ///         * <a href=<%= AspxTools.SafeUrl("http://www.yahoo.com") %> // SafeUrl will not return valid string for external url.
        ///         * <a href=<%= QueryString["url"]%> // Just plain bad.
        /// </summary>
        /// <param name="inputUrl"></param>
        /// <returns></returns>
        public static string SafeUrl(string inputUrl)
        {
            if (string.IsNullOrEmpty(inputUrl))
            {
                return "\"\"";
            }
            bool isValid = true;
            bool isPathPart = true;

            StringBuilder sb = new StringBuilder(inputUrl.Length * 3);

            foreach (string s in SafeUrlWhitelist.Value) // keep the domain unsanitized
            {
                if (string.IsNullOrEmpty(s) == false)
                {
                    if (inputUrl.StartsWith(s))
                    {
                        sb.Append(s);
                        inputUrl = inputUrl.Substring(s.Length); // chop off the prefix
                        break;
                    }
                }
            }

            // A valid url will be consider the following part.
            
            // {path}?{key0}={value0}&{key1}={value1}&.......

            char[] charArray = inputUrl.ToCharArray();
            for (int i = 0; i < charArray.Length; i++ )
            {
                char ch = charArray[i];
                if (isPathPart)
                {
                    if (ch == '?')
                    {
                        isPathPart = false; // We are moving to query part now.
                        sb.Append(ch);
                    }
                    else if (ch >= 48 && ch <= 57) // 0-9
                    {
                        sb.Append(ch);
                    }
                    else if (ch >= 65 && ch <= 90) // A-Z
                    {
                        sb.Append(ch);
                    }
                    else if (ch >= 97 && ch <= 122) // a-z 
                    {
                        sb.Append(ch);
                    }
                    else if (ch == '_' || ch == '-' || ch == '.')
                    {
                        sb.Append(ch);
                    }
                    else if (ch == '/')
                    {
                        if (i < charArray.Length - 1) // Checking that there is atleast one more character in the array
                        {
                            // OPM 40172 vm - Reject urls with more than one '/' in a row
                            if (charArray[i + 1] == '/')
                            {
                                return "\"\"";
                            }
                        }
                        sb.Append(ch);
                    }
                    else
                    {
                        isValid = false;
                        break; // Stop Parsing as soon as we reach invalid character.
                    }
                }
                else
                {
                    if (ch >= 48 && ch <= 57) // 0-9
                    {
                        sb.Append(ch);
                    }
                    else if (ch >= 65 && ch <= 90) // A-Z
                    {
                        sb.Append(ch);
                    }
                    else if (ch >= 97 && ch <= 122) // a-z 
                    {
                        sb.Append(ch);
                    }
                    else if (ch == '=' || ch == '&')
                    {
                        sb.Append(ch);
                    }
                    else if (ch == '%')
                    {
                        // Check to see if part of the url already encode. If it is then skip to prevent double encoding.
                        if (i + 2 < charArray.Length)
                        {
                            char ch1 = charArray[i + 1];
                            char ch2 = charArray[i + 2];
                            if (((ch1 >= 48 && ch1 <= 57) || (ch1 >= 65 && ch1 <= 70) || (ch1 >= 97 && ch1 <= 102)) &&
                                ((ch2 >= 48 && ch2 <= 57) || (ch2 >= 65 && ch2 <= 70) || (ch2 >= 97 && ch2 <= 102)))
                            {
                                i += 2;
                                sb.Append(ch);
                                sb.Append(ch1);
                                sb.Append(ch2);
                            }
                            else
                            {
                                sb.Append("%" + ((ushort)ch).ToString("X2"));
                            }
                        }
                        else
                        {
                            sb.Append("%" + ((ushort)ch).ToString("X2"));
                        }
                    }
                    else
                    {
                        sb.Append("%" + ((ushort)ch).ToString("X2"));
                    }

                }
            }

            if (isValid)
            {
                return "\"" + sb.ToString() + "\"";
            }
            else
            {
                Tools.LogErrorWithCriticalTracking("Invalid URL encounter in AspxTools.SafeUrl. Url=" + inputUrl);
                return "\"\"";
            }
        }

        public static string SafeUrlFromQueryString(string key)
        {
            string s = HttpContext.Current.Request.QueryString[key];

            return SafeUrl(s);
        }

        /// <summary>
        /// This method will allow developer to render html fragment in aspx/ascx code.
        /// 
        /// Instead of writing
        ///     Code-Behind cs:
        ///          protected string GenerateLink(string title) {
        ///               return "<a>" + title + "</a>";
        ///          }
        ///     Aspx/Ascx Code
        ///          <%= GenerateLink("blah") %>
        /// Use this
        ///     Code-Behind cs:
        ///         protected HtmlAnchor GenerateLink(string title) {
        ///               HtmlAnchor a = new HtmlAnchor();
        ///               a.InnerText = title;
        ///               return a;
        ///         }
        ///     Aspx/Ascx code
        ///          <%= AspxTools.HtmlControl(GenerateLink("blah")) %>
        ///          
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public static string HtmlControl(HtmlControl control)
        {
            return HtmlControls(new[] { control });
        }

        public static string HtmlControls(IEnumerable<HtmlControl> controls)
        {
            if (controls == null)
            {
                return string.Empty;
            }

            StringBuilder sb = new StringBuilder();
            using (HtmlTextWriter writer = new HtmlTextWriter(new StringWriter(sb)))
            {
                foreach (HtmlControl control in controls)
                {
                    if (control != null)
                    {
                        control.RenderControl(writer);
                    }
                }
            }

            return sb.ToString();
        }

        public static string JsStringUnquoted(Enum input)
        {
            return input.ToString("D");
        }

        public static string JsStringUnquoted(string input)
        {
            if (string.IsNullOrEmpty(input))
                return string.Empty;

            StringBuilder sb = new StringBuilder(input.Length * 4);

            char[] charList = input.ToCharArray();
            for (int i = 0; i < charList.Length; i++)
            {
                char ch = charList[i];
                sb.Append(SafeJsCharacters[ch]);
            }
            return sb.ToString();

        }

        public static string JsStringUnquoted(bool input)
        {
            return input.ToString();
        }
    }
}
