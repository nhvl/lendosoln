﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace LendersOffice.ObjLib.Task
{
    /// <summary>
    /// Represents a error dealing with a task. It stores enough information to display to the user w/o having to reload the task
    /// </summary>
    [Serializable]
    public class TaskFailure : IXmlSerializable
    {
        public string Id { get; set; }
        public string Subject { get; set; }
        public string Reason { get; set; }

        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            Id = reader.GetAttribute("Id");
            Subject = reader.GetAttribute("Subject");
            Reason = reader.GetAttribute("Reason");
            reader.Read();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteAttributeString("Id", Id);
            writer.WriteAttributeString("Subject", Subject);
            writer.WriteAttributeString("Reason", Reason);
        }

        #endregion
    }

    public enum E_TaskSaveFailureOperation
    {
        SAVE_CONDITIONS,
        SAVE_TASKLIST
    }

    [Serializable]
    public class BatchTaskFailure : IXmlSerializable 
    {
        public const int CACHE_TIME_IN_MINUTES = 10; 
        private LinkedList<TaskFailure> m_failures = new LinkedList<TaskFailure>();
        private bool m_IsReadOnly = true;
        public E_TaskSaveFailureOperation TaskSaveFailureOperation { get; private set; }
        public IEnumerable<TaskFailure> Failures
        {
            get
            {
                return m_failures;
            }
        }

        public BatchTaskFailure()
        {

        }
        public BatchTaskFailure(E_TaskSaveFailureOperation taskSaveFailureOperation)
        {
            TaskSaveFailureOperation = taskSaveFailureOperation;
            m_IsReadOnly = false;
        }

        public void Add(TaskFailure failure)
        {
            if (m_IsReadOnly)
            {
                throw CBaseException.GenericException("Cannot add failures from a loaded list.");
            }
            m_failures.AddLast(failure);
        }

        public string SaveToCache()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(BatchTaskFailure));
            StringBuilder sb = new StringBuilder();
            using (TextWriter writer = new StringWriter(sb))
            {
                serializer.Serialize(writer,this);
            }
            return AutoExpiredTextCache.AddToCache(sb.ToString(), TimeSpan.FromMinutes(CACHE_TIME_IN_MINUTES));

        }

        public static BatchTaskFailure LoadFromCache(string cacheId)
        {
            XmlSerializer xs = new XmlSerializer(typeof(BatchTaskFailure));

            string data = AutoExpiredTextCache.GetFromCache(cacheId);

            if (data == null)
            {
                throw CBaseException.GenericException("The failure list is no longer in the cache.");
            }

            using (StringReader reader = new StringReader(data))
            {
                return (BatchTaskFailure)xs.Deserialize(reader);
            }
        }

        public bool HasErrors()
        {
            return m_failures.Count != 0;
        }




        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            reader.MoveToContent();
            TaskSaveFailureOperation = (E_TaskSaveFailureOperation)int.Parse(reader["TaskSaveFailureOperation"]);
            if (reader.ReadToDescendant("TaskFailure"))
            {
                while (reader.MoveToContent() == XmlNodeType.Element && reader.LocalName == "TaskFailure")
                {
                    TaskFailure tf = new TaskFailure();
                    tf.ReadXml(reader);
                    m_failures.AddLast(tf);
                }
            }
            reader.Read();
        }
        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteAttributeString("TaskSaveFailureOperation", TaskSaveFailureOperation.ToString("D"));

            foreach (TaskFailure failure in m_failures)
            {
                writer.WriteStartElement("TaskFailure");
                failure.WriteXml(writer);
                writer.WriteEndElement();
            }
        }

        #endregion
    }
}
