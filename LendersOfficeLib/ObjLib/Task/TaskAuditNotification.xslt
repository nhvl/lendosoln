﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="html"/>

  <!-- If you make any edits to this file, please update TaskAudit.xslt as well... or refactor. -->
  <xsl:template match="/TaskHistory">
    <xsl:apply-templates select="Save"/>
  </xsl:template>

  <xsl:template match="Save">

    <xsl:apply-templates select ="EmailIn"></xsl:apply-templates>
    <xsl:apply-templates select ="EmailOut"></xsl:apply-templates>
    <xsl:apply-templates select ="Assigned"></xsl:apply-templates>
    <xsl:apply-templates select ="AssignedClosed"></xsl:apply-templates>
    <xsl:apply-templates select ="Closed"></xsl:apply-templates>
    <xsl:apply-templates select ="OpenedAssigned"></xsl:apply-templates>
    <xsl:apply-templates select ="ImportedAssigned"></xsl:apply-templates>
    <xsl:apply-templates select ="Edited"></xsl:apply-templates>
    <xsl:apply-templates select ="ReactivatedAssigned"></xsl:apply-templates>
    <xsl:apply-templates select ="Reactivated"></xsl:apply-templates>
    <xsl:apply-templates select ="ResolvedAssignedTo"></xsl:apply-templates>
    <xsl:apply-templates select ="ResolvedClosedAssignedTo"></xsl:apply-templates>
    <xsl:apply-templates select ="ResolvedClosed"></xsl:apply-templates>
    <xsl:apply-templates select ="Resolved"></xsl:apply-templates>
    <xsl:apply-templates select ="Deleted"></xsl:apply-templates>
    <xsl:apply-templates select ="Restored"></xsl:apply-templates>

    <xsl:apply-templates select="@By"></xsl:apply-templates>
    <xsl:apply-templates select="@From"></xsl:apply-templates>
    <xsl:apply-templates select="@ImportedFrom"></xsl:apply-templates>
    <xsl:text> </xsl:text>
    <xsl:value-of select="@When"/>
   
    <xsl:apply-templates select="Field"/>
    <xsl:if test="HTMLComment">
      <xsl:apply-templates select="HTMLComment"/>
    </xsl:if>
    <xsl:if test="not(HTMLComment)">
      <!-- 1/22/2012 - dd - This is require for old task that does not have HTML comment section -->
      <xsl:apply-templates select="Comment"/>
    </xsl:if>
    <xsl:apply-templates select="Email"/>
    <br/>
    <br/>
  </xsl:template>
  
  <xsl:template match="@By">
    <strong> by </strong>
    <i>
      <xsl:value-of select="." />
    </i>
  </xsl:template>

  <xsl:template match="@From">
    <strong> from </strong>
    <i>
      <xsl:value-of select="." />
    </i>
  </xsl:template>

  <xsl:template match="@ImportedFrom">
    <strong> from </strong>
    <i>
      <xsl:value-of select="." />
    </i>
  </xsl:template>

  <xsl:template match="EmailIn">
    <br/>
    <strong>Email received  </strong>
  </xsl:template>
  <xsl:template match="EmailOut">
    <br/>
    <strong>Emailed </strong>
  </xsl:template>

  <xsl:template match="Email">
    
    <br/>
    <style type="text/css">
      table.mail { border-style:solid; border-color:darkgray; border-width:5px; border-collapse:collapse; }
      header { background:darkgray; }
    </style>
    <table class="mail" rules="groups">

      <thead>
        <tr >
          <td class="header">
            <strong>From:</strong>
          </td>
          <td>
            <xsl:value-of select="@From"/>
          </td>
        </tr>

        <tr >
          <td class="header">
            <strong>Date:</strong>
          </td>
          <td>
            <xsl:value-of select="@Date"/>
          </td>
        </tr>

        <tr >
          <td class="header">
            <strong>To:</strong>
          </td>
          <td>
            <xsl:value-of select="@To"/>
          </td>
        </tr>

        <xsl:apply-templates select="@Cc"></xsl:apply-templates>

        <tr >
          <td class="header">
            <strong>Subject:</strong>
          </td>
          <td>
            <xsl:value-of select="@Subject"/>
          </td>
        </tr>
      </thead>
      
      <tbody bgcolor="white">
        <tr >
          <td colspan="2">
          
              <xsl:value-of select="Body" disable-output-escaping="yes"/>
          </td>
        </tr>
      </tbody>
    </table>
  </xsl:template>

  <xsl:template match="@Cc">
    <tr >
      <td class="header">
        <strong>Cc:</strong>
      </td>
      <td>
        <xsl:value-of select="."/>
      </td>
    </tr>
  </xsl:template>
  
  <xsl:template match ="HTMLComment">
    <br/>
    <text>&#160;&#160;</text>
    <xsl:value-of disable-output-escaping="yes" select="."/>
  </xsl:template>

  <xsl:template match ="Comment">
    <br/>
    <text>&#160;&#160;</text>
    <xsl:value-of disable-output-escaping="yes" select="."/>
  </xsl:template>   

  <xsl:template match="Assigned">
    <strong>Assigned to </strong>
    <i>
      <xsl:value-of select="@To"/>
    </i>
  </xsl:template>

  <xsl:template match="AssignedClosed">
    <strong>Closed and assigned to </strong>
    <i>
      <xsl:value-of select="@To"/>
    </i>
  </xsl:template>

  <xsl:template match="Closed">
    <strong>Closed </strong>
  </xsl:template>

  <xsl:template match="OpenedAssigned">
    <strong>Opened and assigned to </strong>
    <i>
      <xsl:value-of select="@To"/>
    </i>
  </xsl:template>

  <xsl:template match="ImportedAssigned">
    <strong>Imported and assigned to </strong>
    <i>
      <xsl:value-of select="@To"/>
    </i>
  </xsl:template>

  <xsl:template match="Edited">
    <strong>Edited </strong>
  </xsl:template>

  
  <xsl:template match="ReactivatedAssigned">
    <strong>Reactivated and assigned to </strong>
    <i>
      <xsl:value-of select="@To"/>
    </i>
  </xsl:template>

  <xsl:template match="Reactivated">
    <strong>Reactivated </strong>
  </xsl:template>
  
  <xsl:template match="ResolvedAssignedTo">
    <strong>Resolved and assigned to </strong>
    <i>
      <xsl:value-of select="@To"/>
    </i>
  </xsl:template>

  <xsl:template match="ResolvedClosedAssignedTo">
    <strong>Resolved, closed, and assigned to </strong>
    <i>
      <xsl:value-of select="@To"/>
    </i>
  </xsl:template>
  
  <xsl:template match="ResolvedClosed">
    <strong>Resolved and closed </strong>
    <i>
      <xsl:value-of select="@To"/>
    </i>
  </xsl:template>

  
  <xsl:template match="Resolved">
  <strong>Resolved </strong>
</xsl:template>
  
  <xsl:template match="Deleted">
    <strong>Deleted </strong>
  </xsl:template>

  <xsl:template match="Restored">
    <strong>Restored </strong>
  </xsl:template>

  <xsl:template match="Field">
    <br/>
    <i><xsl:value-of select="@Name"/>
    </i>
    
    <xsl:text> changed from </xsl:text>

    <i><xsl:value-of select="@Old"/>
    </i>
    <xsl:text> to </xsl:text>

    <i><xsl:value-of select="@New"/>
    </i>
      
    
  </xsl:template>
  
  
</xsl:stylesheet>
