﻿namespace LendersOffice.ObjLib.Task
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Xml.Linq;

    using DataAccess;
    using EDocs;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Email;
    using LendersOffice.ObjLib.BackgroundJobs.TaskAutomation;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    public enum E_TaskStatus : byte
    {
        Active = 0,
        Resolved = 1,
        Closed = 2
    }

    public enum E_ConditionDeletionState
    {
        NoChange = 0,
        BeingDeleted = 1,
        BeingRestored = 2
    }

    public class TaskException : CBaseException
    {
        public TaskException()
            : base(ErrorMessages.Generic, "Generic task exception.")
        {
        }

        public TaskException(string userMessage, string developerMessage)
            : base(userMessage, developerMessage)
        {
        }
        public E_TaskExceptionReason ExceptionReason { get; protected set; }

    }

    public class TaskNotFoundException : TaskException
    {

        public TaskNotFoundException(string taskId, Guid brokerId)
            : base("Unable to find task " + taskId + ".", "Unable to find task with id: " + taskId + " at broker: " + brokerId)
        {
            ExceptionReason = E_TaskExceptionReason.TaskNotFound;
        }
        public TaskNotFoundException()
            : base("Unable to find task.", "Unable to find task.")
        {
            ExceptionReason = E_TaskExceptionReason.TaskNotFound;
        }
    }

    public class TaskPermissionException : TaskException
    {
        public TaskPermissionException()
            : base("You do not have the required permissions to access this task.", "Task permission denied.")
        {
            ExceptionReason = E_TaskExceptionReason.TaskPermission;
        }
        public TaskPermissionException(string msg)
            : base("You do not have the required permissions to access this task.", "Task permission denied. " + msg)
        {
            ExceptionReason = E_TaskExceptionReason.TaskPermission;
        }
        public TaskPermissionException(string userMessage, string devMessage)
            : base (userMessage, "Task permission denied. " + devMessage)
        {
            ExceptionReason = E_TaskExceptionReason.TaskPermission;
        }
    }

    public class TaskVersionMismatchException : TaskException
    {
        public TaskVersionMismatchException()
            : base("This task has been updated by another user.", "Task version mismatch")
        {
            ExceptionReason = E_TaskExceptionReason.TaskVersion;
        }
    }

    public class TaskResolvedReassignmentException : TaskException
    {
        public TaskResolvedReassignmentException()
            : base("Resolved tasks may not be re-assigned. Re-activate any resolved tasks that you wish to re-assign and try again.", "Resolved task reassignment. Resolved tasks may not be reassigned.")
        {
            ExceptionReason = E_TaskExceptionReason.TaskResolvedReassignment;
        }
    }

    public class TaskLargeAuditException : TaskException
    {
        public TaskLargeAuditException()
            : base(ErrorMessages.Generic, "Task history too large to save.")
        {
        }
    }
    public class TaskSaveAndSendWhenNotActiveException : TaskException
    {
        public TaskSaveAndSendWhenNotActiveException()
            :base("Can't send email unless task is active.", "Can't send email unless task is active.")
        {
        }
    }

    public enum E_TaskExceptionReason
    {
        TaskNotFound = 0,
        TaskPermission = 1,
        TaskVersion = 2,
        TaskResolvedReassignment = 3
    }

    public class Task
    {
        #region Members vars        
        private bool m_isDirty = false;
        private Dictionary<string, string> m_loanDates = null;
        private TaskChangeProfile m_taskDelta = new TaskChangeProfile();
        private bool m_sendNotifications = true; // OPM 67948: don't send out notifications if X-Auto-Response-Suppress exists

        /// <summary>
        /// Used to avoid evaluating the resolution block trigger multiple times
        /// when the ResolutionTriggerProcessor property is not set..
        /// </summary>
        private bool? resolutionBlockTriggerResult;
        #endregion

        #region constants
        private const int HISTORY_WARNING_MAX_MB = 1;
        private const int HISTORY_BLOCKING_MAX_MB = 4;
        #endregion

        #region Fields

        public Guid BrokerId { get; private set; }
        public string TaskId { get; private set; }
        public Guid LoanId { get; private set; }
        public bool isCalculating { get; set; }
        
        public bool IsBatchUpdate = false;

        private AbstractUserPrincipal x_userPrincipal = null;
        private AbstractUserPrincipal CurrentPrincipal
        {
            get
            {
                if (x_userPrincipal == null)
                {
                    x_userPrincipal = System.Threading.Thread.CurrentPrincipal as AbstractUserPrincipal;
                }

                return x_userPrincipal;
            }
        }

        /// <summary>
        /// Sets the user principal for this task. Should only be used when principal is not available.
        /// </summary>
        /// <param name="principal">The user principal to use.</param>
        public void SetPrincipal(AbstractUserPrincipal principal)
        {
            x_userPrincipal = principal;
        }

        private TaskHistory m_taskHistory;


        private DateTime m_taskLastModifiedDate;
        public DateTime TaskLastModifiedDate
        {
            get
            {
                return m_taskLastModifiedDate;
            }
        }

        private E_TaskStatus m_taskStatus;
        public E_TaskStatus TaskStatus
        {
            get
            {
                return m_taskStatus;
            }
            set
            {
                if (m_taskStatus != value)
                {
                    MarkAsDirty();

                    m_taskDelta.SetStatusChange(m_taskStatus, value);

                    m_taskStatus = value;
                }
            }
        }
        private bool m_IsDuplicateTask = false;

        private DateTime m_taskCreatedDate;
        public DateTime TaskCreatedDate
        {
            get
            {
                return m_taskCreatedDate;
            }
        }


        private Guid? m_taskResolvedUserId;
        public Guid? TaskResolvedUserId
        {
            get
            {
                return m_taskResolvedUserId;
            }
            set
            {
                if (value == Guid.Empty && m_taskResolvedUserId.HasValue == false) return;

                if (m_taskResolvedUserId != value)
                {
                    MarkAsDirty();

                    m_taskResolvedUserId = value;
                }
            }
        }

        private Guid? m_assignToOnReactivationUserId;
        public Guid? AssignToOnReactivationUserId
        {
            get
            {
                return m_assignToOnReactivationUserId;
            }
            set
            {
                if (value == Guid.Empty && m_assignToOnReactivationUserId.HasValue == false) return;

                if (m_assignToOnReactivationUserId != value)
                {
                    MarkAsDirty();

                    m_assignToOnReactivationUserId = value;
                }
            }
        }

        private string m_borrowerNmCached;
        public string BorrowerNmCached
        {
            get
            {
                return m_borrowerNmCached;
            }
            private set
            {
                m_borrowerNmCached = value;
            }
        }

        private bool m_isTemplate;
        public bool IsTemplate
        {
            get
            {
                return m_isTemplate;
            }
            private set
            {
                m_isTemplate = value;
            }
        }

        private string m_loanNumCached;
        public string LoanNumCached
        {
            get
            {
                return m_loanNumCached;
            }
            private set
            {
                m_loanNumCached = value;
            }
        }

        private Guid m_taskAssignedUserId;
        public Guid TaskAssignedUserId
        {
            get
            {
                return m_taskAssignedUserId;
            }
            set
            {
                if (m_taskAssignedUserId != value)
                {
                    MarkAsDirty();

                    m_taskAssignedUserId = value;
                    m_taskToBeAssignedRoleId = null;

                    AssignedUserFullName = value == Guid.Empty ? "" : GetUserDisplayName(value);
                }
            }
        }

        private int? m_CondRequiredDocTypeId;

        public int? CondRequiredDocTypeId
        {
            get { return m_CondRequiredDocTypeId; }
            set {
                if (!m_CondRequiredDocTypeId.Equals(value))
                {
                    MarkAsDirty();


                    string oldValue = GetDocTypeDesc(m_CondRequiredDocTypeId);
                    string newValue = GetDocTypeDesc(value);

                    if (newValue == null)
                    {
                        throw CBaseException.GenericException("Invalid doctype - Did not find in broker.");
                    }

                    m_taskDelta.AddChange("CondRequiredDocTypeId", oldValue, newValue);
                    m_CondRequiredDocTypeId = value; 
                }
            }
        }

        public string CondRequiredDocTypeId_rep
        {
            get
            {
                return this.CondRequiredDocTypeId.ToString();
            }
        }

        private List<DocType> m_listOfDoctypes = null;

        public string CondRequiredDocType_rep
        {
            get { return GetDocTypeDesc(m_CondRequiredDocTypeId); }
        }

        private string GetDocTypeDesc(int? id)
        {
            if (!id.HasValue)
            {
                return "None";
            }

            if (m_listOfDoctypes == null)
            {
                m_listOfDoctypes = EDocumentDocType.GetDocTypesByBroker(BrokerId, false).ToList();
            }

            var selectedDocType = m_listOfDoctypes.Find((DocType d) => d.DocTypeId == id.Value.ToString());
            
            if (selectedDocType == null)
            {
                return null; //doc type not found.
            }

            return selectedDocType.FolderAndDocTypeName;
        }

        private bool m_isNew = false;
        public bool IsNew 
        { 
            get 
            { 
                return m_isNew;
            } 
        }

        private Guid m_taskPreviousAssignedUserId;
        public Guid TaskPreviousAssignedUserId
        {
            get
            {
                return m_taskPreviousAssignedUserId;
            }
        }

        private Guid? m_taskToBeAssignedRoleId;
        public Guid? TaskToBeAssignedRoleId
        {
            get
            {
                return m_taskToBeAssignedRoleId;
            }
            set
            {
                if (value == Guid.Empty && m_taskToBeAssignedRoleId.HasValue == false) return;

                if (m_taskToBeAssignedRoleId != value)
                {
                    MarkAsDirty();

                    m_taskToBeAssignedRoleId = value;

                    m_taskAssignedUserId = Guid.Empty;

                    AssignedUserFullName = GetRoleDisplayName(value.Value);
                }
            }
        }

        //Require Manage if not assigned, otherwise only workon.
        private DateTime? m_taskFollowUpDate;
        public DateTime? TaskFollowUpDate
        {
            get
            {
                return m_taskFollowUpDate;
            }
            set
            {
                if (m_taskFollowUpDate != value)
                {
                    MarkAsDirty();
                    m_taskDelta.AddChange("TaskFollowUpDate"
                        , m_taskFollowUpDate.HasValue ? m_taskFollowUpDate.Value.ToShortDateString() : "Blank"
                        , value.HasValue ? value.Value.ToShortDateString() : "Blank"
                        );

                    m_taskFollowUpDate = value;
                }
            }
        }

        private static System.Text.RegularExpressions.Regex s_unixNewlineRegex = 
            new System.Text.RegularExpressions.Regex("[^\r]\n", System.Text.RegularExpressions.RegexOptions.Compiled);
        private string m_taskSubject = string.Empty;
        public string TaskSubject
        {
            get
            {
                return m_taskSubject;
            }
            set
            {
                string val = Tools.ReplaceInvalidUnicodeChars(value);
                // 4/30/14 gf - opm 179705 normalize newlines if needed
                bool updateSubject = false;
                if (m_taskSubject != null && val != null
                    && (s_unixNewlineRegex.IsMatch(m_taskSubject) || s_unixNewlineRegex.IsMatch(val)))
                {
                    updateSubject = m_taskSubject.Replace("\r\n", "\n").TrimWhitespaceAndBOM() != val.Replace("\r\n", "\n").TrimWhitespaceAndBOM();
                }
                else if (m_taskSubject != null && val != null)
                {
                    updateSubject = m_taskSubject.TrimWhitespaceAndBOM() != val.TrimWhitespaceAndBOM();
                }
                else
                {
                    updateSubject = m_taskSubject != val;
                }

                if (updateSubject)
                {
                    MarkAsDirty();

                    m_taskDelta.AddChange(
                          "TaskSubject",
                          m_taskSubject,
                          val
                  );

                    m_taskSubject = val;
                }
            }
        }

        private bool m_taskDueDateLocked;
        public bool TaskDueDateLocked
        {
            get
            {
                return m_taskDueDateLocked;
            }
            set
            {
                if (m_taskDueDateLocked != value)
                {
                    MarkAsDirty();

                    m_taskDueDateLocked = value;
                }
            }
        }

        private string m_originalDueDateDescription = null;

        private string OriginalDueDateDescription 
        {
            get
            {
                LoadOriginalDueDateDescription();

                return this.m_originalDueDateDescription;
            }
        }

        /// <summary>
        /// Loads the original due date description if it has not been loaded.
        /// </summary>
        private void LoadOriginalDueDateDescription()
        {
            if (this.m_originalDueDateDescription == null)
            {
                this.m_originalDueDateDescription = this.DueDateDescription(false);
            }
        }

        private DateTime? m_taskDueDate;
        public DateTime? TaskDueDate
        {
            get
            {
                return m_taskDueDate;
            }
            set
            {

                string changeType = "TaskDueDate";
                if (m_taskDueDate != value)
                {
                    MarkAsDirty();

                    string previousDate;
                    if (m_taskDueDate.HasValue)
                    {
                        previousDate = m_taskDueDate.Value.ToShortDateString();
                    }
                    else if (this.OriginalDueDateDescription != string.Empty)
                    {
                        previousDate = this.OriginalDueDateDescription;
                    }
                    else
                    {
                        previousDate = DueDateDescription(false);
                    }

                    //OPM 69370: suppress the audit entry if the change is the result of a calculation update.
                    if (!isCalculating)
                    {
                        m_taskDelta.AddChange(changeType,
                                previousDate,
                                value.HasValue ? value.Value.ToShortDateString() : DueDateDescription(false)
                                );
                    }


                    m_taskDueDate = value;
                }
                else if (value == null && this.OriginalDueDateDescription != string.Empty)
                {
                    // Special case for 67759.
                    m_taskDelta.AddChange(changeType,
                        this.OriginalDueDateDescription,
                        value.HasValue ? value.Value.ToShortDateString() : DueDateDescription(false)
                        );
                }
            }
        }

        private Guid m_taskOwnerUserId;
        public Guid TaskOwnerUserId
        {
            get
            {
                return m_taskOwnerUserId;
            }
            set
            {
                if (m_taskOwnerUserId != value)
                {
                    MarkAsDirty();

                    m_taskOwnerUserId = value;
                    m_taskToBeOwnerRoleId = null;

                    OwnerFullName = value == Guid.Empty ? "" : GetUserDisplayName(value);
                }
            }
        }

        // Access check should be for who has ownership now,
        // not who will get it if save is successful.
        private Guid m_originalOwnerId = Guid.Empty;
        public Guid TaskOwnerUserIdForPermissionCheck
        {
            get
            {
                return m_isNew ? m_taskOwnerUserId : m_originalOwnerId;
            }
        }

        private List<DocumentConditionAssociation.DocumentConditionAssociation> m_associations = null;
        public List<DocumentConditionAssociation.DocumentConditionAssociation> AssociatedDocs
        {
            get
            {
                if (m_associations == null)
                {
                    if (string.IsNullOrEmpty(TaskId))
                    {
                        m_associations = new List<DocumentConditionAssociation.DocumentConditionAssociation>(); // returns empty list
                    }
                    else
                    {
                        m_associations = DocumentConditionAssociation.DocumentConditionAssociation.GetAssociationsByLoanConditionHeavy(BrokerId, LoanId, TaskId);
                    }
                }
                return m_associations.ToList();
            }
        }

        private IEnumerable<Guid> m_docsToAssociateOnSave = null;
        public void UpdateDocumentAssociations(IEnumerable<Guid>docIDs)
        {
            m_docsToAssociateOnSave = docIDs;
        }

        private int? m_condCategoryId;
        public int? CondCategoryId
        {
            get
            {
                return m_condCategoryId;
            }
            set
            {
                if (m_condCategoryId != value)
                {
                    MarkAsDirty();

                    m_taskDelta.AddChange("CondCategory"
                            , m_condCategoryId.HasValue ? GetCategoryDisplayName(m_condCategoryId.Value) : "Blank"
                            , GetCategoryDisplayName(value.Value));

                    m_condCategoryId = value;
                }
            }
        }

        public string CondCategoryId_rep
        {
            get
            {
                if (m_condCategoryId.HasValue)
                {
                    return GetCategoryDisplayName(m_condCategoryId.Value);
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        private decimal m_condRank;
        public decimal CondRank
        {
            get
            {
                return m_condRank;
            }
            set
            {
                if (m_condRank != value)
                {
                    MarkAsDirty();

                    m_condRank = value;
                }
            }
        }

        private int m_condRowId = 0;
        public int CondRowId
        {
            get
            {
                return m_condRowId;
            }
            set
            {
                if (m_condRowId != value)
                {
                    MarkAsDirty();
                    m_condRowId = value;
                }
            }
        }

        private DateTime? m_condDeletedDate;
        public DateTime? CondDeletedDate
        {
            get
            {
                return m_condDeletedDate;
            }
            private set
            {
                if (m_condDeletedDate != value)
                {
                    MarkAsDirty();

                    m_condDeletedDate = value;
                }
            }
        }

        public string CondDeletedDate_rep
        {
            get
            {
                if (this.CondDeletedDate.HasValue)
                {
                    return this.CondDeletedDate.Value.ToShortDateString();
                }
                else
                {
                    return string.Empty;
                }
            }
            set
            {
                try
                {
                    this.CondDeletedDate = Convert.ToDateTime(value);
                }
                catch (FormatException)
                {
                    this.CondDeletedDate = null;
                }
            }
        }

        private string m_condDeletedByUserNm;
        public string CondDeletedByUserNm
        {
            get
            {
                return m_condDeletedByUserNm;
            }
            private set
            {
                if (m_condDeletedByUserNm != value)
                {
                    MarkAsDirty();

                    m_condDeletedByUserNm = value;
                }
            }
        }

        private bool m_condIsDeleted;
        public bool CondIsDeleted
        {
            get
            {
                return m_condIsDeleted;
            }
            private set
            {
                if (m_condIsDeleted != value)
                {
                    MarkAsDirty();

                    m_condIsDeleted = value;
                }
            }
        }

        private string m_condInternalNotes;
        public string CondInternalNotes
        {
            get
            {
                return m_condInternalNotes;
            }
            set
            {
                // 4/30/14 gf - opm 179705 normalize newlines if needed
                bool updateCondInternalNotes = false;
                if (m_condInternalNotes != null && value != null
                    && (s_unixNewlineRegex.IsMatch(m_condInternalNotes) || s_unixNewlineRegex.IsMatch(value)))
                {
                    updateCondInternalNotes = m_condInternalNotes.Replace("\r\n", "\n").TrimWhitespaceAndBOM() != value.Replace("\r\n", "\n").TrimWhitespaceAndBOM();
                }
                else if (m_condInternalNotes != null && value != null)
                {
                    updateCondInternalNotes = m_condInternalNotes.TrimWhitespaceAndBOM() != value.TrimWhitespaceAndBOM();
                }
                else
                {
                    updateCondInternalNotes = m_condInternalNotes != value;
                }

                if (updateCondInternalNotes)
                {
                    MarkAsDirty();

                    m_taskDelta.AddChange("CondInternalNotes", m_condInternalNotes, value);

                    m_condInternalNotes = value;
                }
            }
        }

        private bool m_taskIsCondition;
        public bool TaskIsCondition
        {
            get
            {
                return m_taskIsCondition;
            }
            set
            {
                if (m_taskIsCondition != value)
                {


                    MarkAsDirty();
                    m_taskDelta.AddChange("TaskIsCondition", m_taskIsCondition ? "Yes" : "No", value ? "Yes" : "No");

                    m_taskIsCondition = value;
                }
            }
        }

        private string m_condMessageId = string.Empty;
        public string CondMessageId
        {
            get { return m_condMessageId; }
            set { if (value != null) m_condMessageId = value; }
        }

        // Should be controlled by the existing permission
        private bool m_condIsHidden;
        public bool CondIsHidden
        {
            get
            {
                return m_condIsHidden;
            }
            set
            {
                if (m_condIsHidden != value)
                {
                    MarkAsDirty();

                    m_taskDelta.AddChange("CondIsHidden", m_condIsHidden ? "Yes" : "No", value ? "Yes" : "No");

                    m_condIsHidden = value;
                }
            }
        }

        private Guid? m_taskToBeOwnerRoleId;
        public Guid? TaskToBeOwnerRoleId
        {
            get
            {
                return m_taskToBeOwnerRoleId;
            }
            set
            {
                //if (value == Guid.Empty && m_taskToBeOwnerRoleId.HasValue == false) return;

                if (m_taskToBeOwnerRoleId != value)
                {
                    MarkAsDirty();
                    m_taskToBeOwnerRoleId = value;
                    m_taskOwnerUserId = Guid.Empty;

                    OwnerFullName = GetRoleDisplayName(value.Value);
                }
            }
        }

        private Guid m_taskCreatedByUserId;
        public Guid TaskCreatedByUserId
        {
            get
            {
                return m_taskCreatedByUserId;
            }
            set
            {
                if (m_taskCreatedByUserId != value)
                {
                    MarkAsDirty();

                    m_taskCreatedByUserId = value;
                }
            }
        }

        private string m_taskDueDateCalcField;
        public string TaskDueDateCalcField
        {
            get
            {
                return m_taskDueDateCalcField;
            }
            set
            {
                if (m_taskDueDateCalcField != value)
                {
                    MarkAsDirty();

                    this.LoadOriginalDueDateDescription();

                    m_taskDueDateCalcField = value;
                }
            }
        }

        private int m_taskDueDateCalcDays;
        public int TaskDueDateCalcDays
        {
            get
            {
                return m_taskDueDateCalcDays;
            }
            set
            {
                if (m_taskDueDateCalcDays != value)
                {
                    MarkAsDirty();

                    this.LoadOriginalDueDateDescription();

                    m_taskDueDateCalcDays = value;
                }
            }
        }

        private int m_taskPermissionLevelId;
        public int TaskPermissionLevelId
        {
            get
            {
                return m_taskPermissionLevelId;
            }

            set
            {
                if (m_taskPermissionLevelId != value)
                {

                    MarkAsDirty();

                    m_taskDelta.AddChange("PermissionLevel", GetPermissionLevelDisplayName(m_taskPermissionLevelId), GetPermissionLevelDisplayName(value));

                    m_taskPermissionLevelId = value;

                    CalculatePermission();
                }
            }
        }

        public string TaskPermissionLevelId_rep
        {
            get
            {
                return this.TaskPermissionLevelId.ToString();
            }
        }

        public string PermissionLevelName
        {
            get { return GetPermissionLevelDisplayName(m_taskPermissionLevelId); }
        }

        public string PermissionLevelDescription
        {
            get { return GetPermissionLevelDescription(m_taskPermissionLevelId); }
        }

        public bool PermissionLevelAppliesToConditions
        {
            get { return GetPermissionLevelAppliesToConditions(m_taskPermissionLevelId); }
        }

        public PermissionLevel DefaultPermissionForCondition
        {
            get
            {
                // 1/16/13. mf. OPM 108448.  Basically, get the first permission what would allow the user
                // to manage.  Returns null if no such permission exists.
                List<Guid> UserGroups = new List<Guid>();
                foreach (Group ugroup in GroupDB.ListInclusiveGroupForEmployee(CurrentPrincipal.BrokerId, CurrentPrincipal.EmployeeId))
                {
                    UserGroups.Add(ugroup.Id);
                }
                List<E_RoleT> Roles = new List<E_RoleT>();
                foreach (E_RoleT role in CurrentPrincipal.GetRoles())
                {
                    Roles.Add(role);
                }

                var permissionLevels = PermissionLevel.RetrieveManageLevelsForUser(BrokerId, Roles, UserGroups);

                return permissionLevels.Where(p => p.IsAppliesToConditions && p.IsActive ).FirstOrDefault();
            }

        }

        private Guid? m_taskClosedUserId;
        public Guid? TaskClosedUserId
        {
            get
            {
                return m_taskClosedUserId;
            }
            set
            {
                if (value == Guid.Empty && m_taskClosedUserId.HasValue == false) return;

                if (m_taskClosedUserId != value)
                {
                    MarkAsDirty();

                    m_taskClosedUserId = value;

                    ClosingUserFullName = GetUserDisplayName(value.Value);
                }
            }
        }

        private DateTime? m_taskClosedDate;
        public DateTime? TaskClosedDate
        {
            get
            {
                return m_taskClosedDate;
            }
            set
            {
                if (m_taskClosedDate != value)
                {
                    MarkAsDirty();

                    m_taskClosedDate = value;
                }
            }
        }

        public string TaskClosedDate_rep
        {
            get
            {
                if (TaskClosedDate.HasValue)
                {
                    return TaskClosedDate.Value.ToShortDateString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                try
                {
                    TaskClosedDate = Convert.ToDateTime(value);
                }
                catch (FormatException)
                {
                    TaskClosedDate = null;
                }
            }
        }

        public string TaskHistoryHtml
        {
            get
            {
                return m_isNew ? string.Empty : TaskUtilities.SanitizeTaskText(m_taskHistory.GetHTMLContentForAllEntries(), CurrentPrincipal);
            }
        }

        public string TaskHistoryFirstComment
        {
            get { return m_taskHistory.GetFirstComment(); }
        }

        public string RecentTaskHistoryHtmlRaw
        {
            get
            {
                return m_taskHistory.GetHtmlContentForNewEntries();
            }
        }

        public IEnumerable<TaskHistoryItem> GetTaskHistoryItems()
        {
            if (m_isNew)
            {
                return new List<TaskHistoryItem>();
            }
            else
            {
                return m_taskHistory.ListHistoryItems();
            }
            
        }
        public string Comments
        {
            set
            {
                MarkAsDirty();
                m_taskHistory.StoreComment(value);
                m_taskDelta.AddCommentChange(value, CurrentPrincipal.DisplayName);
            }
        }
        public string DeveloperAuditMessage
        {
            set
            {
                m_taskHistory.StoreDeveloperAudit(value);
            }
        }
        public string LogEdit
        {
            set
            {
                MarkAsDirty();                
                m_taskHistory.StoreEditMessage(value);                
            }
        }

        // OPM 136559.
        private void SetHistoryFromOtherTaskForCopy(Task task)
        {
            
            m_taskHistory = task.m_taskHistory;
        }

        private byte[] m_taskRowVersion;
        public string TaskRowVersion
        {
            get
            {
                return Convert.ToBase64String(m_taskRowVersion);
            }
        }

        private string resolutionBlockTriggerName;

        /// <summary>
        /// Gets or sets the name of the trigger which must be satisfied before 
        /// the task can be resolved.
        /// </summary>
        public string ResolutionBlockTriggerName
        {
            get { return this.resolutionBlockTriggerName; }
            set
            {
                if (this.resolutionBlockTriggerName != value)
                {
                    this.MarkAsDirty();

                    m_taskDelta.AddChange("ResolutionBlockTriggerName", this.ResolutionBlockTriggerName, value);

                    this.resolutionBlockTriggerName = value;
                    this.resolutionBlockTriggerResult = null;
                }
            }
        }

        private string resolutionDenialMessage;

        /// <summary>
        /// Gets or sets the message to display when task resolution or closing 
        /// is blocked becuase the resolution block trigger is not satisfied.
        /// </summary>
        public string ResolutionDenialMessage
        {
            get { return this.resolutionDenialMessage; }
            set
            {
                if (this.resolutionDenialMessage != value)
                {
                    this.MarkAsDirty();

                    this.resolutionDenialMessage = value;
                }
            }
        }

        private string resolutionDateSetterFieldId;

        /// <summary>
        /// Gets or sets the field id of the date field which will be set upon 
        /// task resolution.
        /// </summary>
        public string ResolutionDateSetterFieldId
        {
            get { return this.resolutionDateSetterFieldId; }
            set
            {
                if (this.resolutionDateSetterFieldId != value)
                {
                    this.MarkAsDirty();

                    m_taskDelta.AddChange("ResolutionDateSetterFieldId", this.GetResolutionDateSetterFieldDescription(),
                        this.GetFieldDescription(value));

                    this.resolutionDateSetterFieldId = value;
                }
            }
        }

        private ParameterReporting cachedParameterReporting;

        /// <summary>
        /// Gets the system parameter reporting object.
        /// </summary>
        /// <remarks>
        /// Caches the value for the life of the instance.
        /// </remarks>
        private ParameterReporting parameterReporting
        {
            get
            {
                if (this.cachedParameterReporting == null)
                {
                    this.cachedParameterReporting = ParameterReporting.RetrieveSystemParameters(); 
                }

                return this.cachedParameterReporting;
            }
        }

        /// <summary>
        /// Gets the friendly description of a field.
        /// </summary>
        /// <param name="fieldId">The id of the field.</param>
        /// <returns>
        /// The friendly description of the field with the given id. Returns 
        /// "None" if the field id is null or empty. Returns "" if the field 
        /// was not found.
        /// </returns>
        private string GetFieldDescription(string fieldId)
        {
            if (string.IsNullOrEmpty(fieldId))
            {
                return "None";
            }

            var parameter = this.parameterReporting[fieldId];
            return (parameter == null) ? "" : parameter.FriendlyName;
        }

        /// <summary>
        /// Gets the friendly description of the date field that will be set upon
        /// task resolution.
        /// </summary>
        /// <returns>
        /// The friendly description of the date field. If the value has not 
        /// been set, returns "None". Returns "" if the field cannot be found.
        /// </returns>
        public string GetResolutionDateSetterFieldDescription()
        {
            return this.GetFieldDescription(this.ResolutionDateSetterFieldId);
        }

        #endregion

        #region props

        public TaskPermissionProcessor PermissionProcessor
        {
            get;
            set;
        }

        private string m_assignedUserFullName;
        public string AssignedUserFullName
        {
            get
            {
                return m_assignedUserFullName;
            }
            private set
            {
                // The "Broker Processor" role was relabeled to "Processor (External)"
                // We don't want to record this label change in the audit history.
                bool ignoreChange = this.TaskAssignedUserId == Guid.Empty &&
                    this.TaskToBeAssignedRoleId.HasValue &&
                    this.TaskToBeAssignedRoleId == CEmployeeFields.s_BrokerProcessorId &&
                    value == "Processor (External)" && 
                    this.m_assignedUserFullName == "Broker Processor";

                if (!ignoreChange)
                {
                    m_taskDelta.AddChange("AssignedUserFullName", m_assignedUserFullName, value);
                }

                m_assignedUserFullName = value;
            }

        }

        private string m_ownerFullName;
        public string OwnerFullName
        {
            get
            {
                return m_ownerFullName;
            }
            private set
            {
                // The "Broker Processor" role was relabeled to "Processor (External)"
                // We don't want to wrongly prevent users w/o manage permission from
                // modifying this task if the user name is changed by the system due
                // to the relabel.
                bool ignoreChange = this.TaskOwnerUserId == Guid.Empty &&
                    this.TaskToBeOwnerRoleId.HasValue &&
                    this.TaskToBeOwnerRoleId == CEmployeeFields.s_BrokerProcessorId &&
                    value == "Processor (External)" && 
                    this.m_ownerFullName == "Broker Processor";

                if (!ignoreChange)
                {
                    m_taskDelta.AddChange("OwnerFullName", m_ownerFullName, value);
                }

                m_ownerFullName = value;
            }
        }


        private string m_closingUserFullName;
        public string ClosingUserFullName
        {
            get
            {
                return m_closingUserFullName;
            }
            private set
            {
                m_closingUserFullName = value;
            }
        }

        /// <summary>
        /// The latest email address that this task was emailed from.
        /// </summary>
        public string RecentlyEmailedFrom
        {
            get
            {
                
                string xmlHistory = this.m_taskHistory.GetXmlContentForNewEntries();
                XDocument xDoc = XDocument.Parse(xmlHistory);
                var xElements = xDoc.Descendants("Email");
                if (xElements != null && xElements.Count() > 0)
                {
                    return xElements.First().Attribute("From").Value;
                }
                return null;
            }
        }
        #endregion


        public static Task Create(Guid loanId, Guid brokerId)
        {
            return Task.Create(loanId, brokerId, true);
        }

        // Only use this if you have reason.
        public static Task CreateWithoutPermissionCheck(Guid loanId, Guid brokerId, LoanDataForTasks loanData = null, PermissionLevelData permissionLevels = null, int? defaultPermissionLevelId = null)
        {
            return Task.Create(loanId, brokerId, false, loanData, permissionLevels, defaultPermissionLevelId);
        }


        private static Task Create(Guid loanId, Guid brokerId, bool enforcePermissions, LoanDataForTasks loanData = null, PermissionLevelData permissionLevels = null, int? defaultPermissionLevel = null)
        {
            Task newTask = new Task(loanId, brokerId);
            newTask.EnforcePermissions = enforcePermissions;
            LoadCachedData(newTask, loanData);

            if (permissionLevels != null)
            {
                newTask.SetPermissionLevelData(permissionLevels);
            }

            AbstractUserPrincipal creatingUser = System.Threading.Thread.CurrentPrincipal as AbstractUserPrincipal;
            if (creatingUser != null)
            {
                newTask.AddFullNameForUserId(creatingUser.UserId, creatingUser.DisplayName);
                newTask.TaskAssignedUserId = creatingUser.UserId;
            }

            newTask.TaskPermissionLevelId = defaultPermissionLevel ?? GetDefaultPermissionLevel(brokerId);
            newTask.TaskDueDateLocked = true;

            return newTask;
        }

        public static Task Create(Guid loanId, Guid brokerId, string borrowerNm, string loanNm)
        {
            Task newTask = new Task(loanId, brokerId);
            newTask.BorrowerNmCached = borrowerNm;
            newTask.LoanNumCached = loanNm;

            return newTask;
        }

        public static Task CreateWithoutPermissionCheck(Guid loanId, Guid brokerId, string borrowerNm, string loanNm)
        {
            Task newTask = new Task(loanId, brokerId);
            newTask.EnforcePermissions = false;
            newTask.BorrowerNmCached = borrowerNm;
            newTask.LoanNumCached = loanNm;

            return newTask;
        }




        // creates a new task
        internal Task(Guid loanId, Guid brokerId)
        {
            m_isNew = true;
            LoanId = loanId;
            BrokerId = brokerId;
            isCalculating = false;

            m_taskHistory = new TaskHistory("", true);
        }

        public void SetImportingSource(string sourceFormat)
        {
            m_taskHistory.StoreImportingSource(sourceFormat);
        }

        internal Task()
        {
        }


        #region Permission

        private bool m_enforcePermissions = true;
        /// <summary>
        /// Determines whether or not this task will enforce permissions. It is only safe and useful to set to false on a task that has not yet been retrieved;
        /// tasks which are being retrieved will 
        /// </summary>
        public bool EnforcePermissions
        {
            get
            {
                return m_enforcePermissions;
            }
            set
            {
                m_enforcePermissions = value;
                CalculatePermission();

            }
        }

        public E_UserTaskPermissionLevel UserPermissionLevel { get; private set; }

        readonly string[] ManagedOnlyFields =
        {
            "TaskSubject"
            , "TaskDueDate"
            , "OwnerFullName"
            , "CondCategory"
            , "CondInternalNotes"
            , "TaskIsCondition"
        };

        readonly string[] ManagedIfNotAssigned = 
        {
            "TaskFollowUpDate"
        };

        public E_UserTaskPermissionLevel GetPermissionLevelFor(AbstractUserPrincipal p)
        {
            if (PermissionProcessor != null && PermissionProcessor.CurrentPrincipal.UserId == p.UserId)
            {
                return PermissionProcessor.Resolve(this);
            }
            return TaskPermissionProcessor.GetMergedTaskAccessLevel(this, p);
        }


        private void VerifyFieldWritePermission(string fieldName, Tuple<string, string> fieldValues)
        {
            if (UserPermissionLevel == E_UserTaskPermissionLevel.Manage) return;

            // Make sure setting user is allowed to set
            // OPM 78391: Allow setting due date when task is getting reactivated
            if (ManagedOnlyFields.Contains(fieldName) && !(IsBeingReactivated() && fieldName.Equals("TaskDueDate")))
            {
                var devMsg = string.Format("Setting {0} requires Manage permission. Old Value: \"{1}\", New Value: \"{2}\". TaskId: {3}. BrokerId: {4}.",
                    fieldName, fieldValues.Item1, fieldValues.Item2, TaskId, BrokerId);
                throw new TaskPermissionException(devMsg);
            }

            // Per case 67556 we are not enforcing the assignment check right now.
            //if (ManagedIfNotAssigned.Contains(fieldName) && TaskAssignedUserId != CurrentPrincipal.UserId)
            //{
            //    throw new TaskPermissionException("Setting " + fieldName + " requires Manage permission, or being assigned to the task.");
            //}

        }

        private void CalculatePermission()
        {

            if (m_enforcePermissions == false || (m_isNew && TaskPermissionLevelId == 0))
            {
                UserPermissionLevel = E_UserTaskPermissionLevel.Manage;
                return;
            }

            if (CurrentPrincipal == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "Cannot enforce permissions with no accessing user. Set EnforcePermissions to false. If access without user principal is required.");
            }

            if (PermissionProcessor != null && PermissionProcessor.CurrentPrincipal.UserId == CurrentPrincipal.UserId)
            {
                UserPermissionLevel = PermissionProcessor.Resolve(this);
                return;
            }

            UserPermissionLevel = TaskPermissionProcessor.GetMergedTaskAccessLevel(this, CurrentPrincipal);

        }

        #endregion

        #region DataAccess

        internal void Retrieve(string taskId, Guid brokerId, Dictionary<string, string> loanDateCache = null)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@TaskId", taskId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_RetrieveTaskByBrokerIdTaskId", parameters ))
            {
                if (reader.Read())
                {
                    LoadTask(reader, categories: null, loanDates: loanDateCache);
                }
                else
                {
                    throw new TaskNotFoundException(taskId, brokerId);
                }
            }

            try
            {
                CalculatePermission();
            }
            catch (LoanNotFoundException)
            {
                //if the loan is deleted. return task not found. OPM 71691
                throw new TaskNotFoundException(taskId, brokerId);
            }

            if (EnforcePermissions && UserPermissionLevel == E_UserTaskPermissionLevel.None)
            {
                // Without workon or manage permission, user cannot do anything.
                throw new TaskPermissionException("Do not have permission to load this task");
            }

        }

        internal void Retrieve(string taskId, Guid brokerId, E_PortalMode portalMode, Guid pmlBrokerId)
        {
            int BranchChannelT = 0;
            int CorrespondentProcessT = -1;
            bool AllowBlankChannel = false;
            bool IsCorrespondent = false;
            int DelegatedProcessT = (int)E_sCorrespondentProcessT.Delegated;
            int PriorProcessT = (int)E_sCorrespondentProcessT.PriorApproved;

            switch(portalMode)
            {
                case E_PortalMode.Broker:
                    BranchChannelT = (int)E_BranchChannelT.Wholesale;
                    AllowBlankChannel = true;
                    break;
                case E_PortalMode.MiniCorrespondent:
                    BranchChannelT = (int)E_BranchChannelT.Correspondent;
                    CorrespondentProcessT = (int)E_sCorrespondentProcessT.MiniCorrespondent;
                    break;
                case E_PortalMode.Correspondent:
                    BranchChannelT = (int)E_BranchChannelT.Correspondent;
                    PmlBroker pB = PmlBroker.RetrievePmlBrokerById(pmlBrokerId, brokerId);
                    AllowBlankChannel = true;
                    IsCorrespondent = true;
                    CorrespondentProcessT = (int)E_sCorrespondentProcessT.Blank;
                    break;
                case E_PortalMode.Retail:
                    BranchChannelT = (int)E_BranchChannelT.Retail;
                    AllowBlankChannel = true;
                    break;
                default:
                    throw new UnhandledEnumException(portalMode);
            }
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@TaskId", taskId),
                                            new SqlParameter("@sBranchChannelT", BranchChannelT),
                                            new SqlParameter("@sCorrespondentProcessT", CorrespondentProcessT),
                                            new SqlParameter("@AllowBlankChannel", AllowBlankChannel),
                                            new SqlParameter("@IsCorrespondent", IsCorrespondent),
                                            new SqlParameter("@DelegatedProcessT", DelegatedProcessT),
                                            new SqlParameter("@PriorProcessT", PriorProcessT)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_RetrieveTaskByBrokerIdTaskIdPortalMode", parameters))
            {
                if (reader.Read())
                {
                    LoadTask(reader);
                }
                else
                {
                    throw new TaskNotFoundException(taskId, brokerId);
                }
            }

            try
            {
                CalculatePermission();
            }
            catch (LoanNotFoundException)
            {
                //if the loan is deleted. return task not found. OPM 71691
                throw new TaskNotFoundException(taskId, brokerId);
            }

            if (EnforcePermissions && UserPermissionLevel == E_UserTaskPermissionLevel.None)
            {
                // Without workon or manage permission, user cannot do anything.
                throw new TaskPermissionException("Do not have permission to load this task");
            }

        }

        private void LoadTask(DbDataReader reader)
        {
            LoadTask(reader, null, null);
        }
        private void LoadTask(DbDataReader reader, Dictionary<int, ConditionCategory> categories, Dictionary<string, string> loanDates)
        {
            BrokerId = (Guid)reader["BrokerId"];
            TaskId = reader["TaskId"].ToString();
            m_taskAssignedUserId = (Guid)reader["TaskAssignedUserId"];
            m_taskPreviousAssignedUserId = m_taskAssignedUserId;
            m_taskStatus = (E_TaskStatus)(byte)reader["TaskStatus"];
            LoanId = (Guid)reader["LoanId"];
            m_taskOwnerUserId = m_originalOwnerId = (Guid)reader["TaskOwnerUserId"];
            m_taskCreatedDate = (DateTime)reader["TaskCreatedDate"];
            m_taskSubject = Tools.ReplaceInvalidUnicodeChars(reader["TaskSubject"].ToString());
            m_taskDueDate = reader["TaskDueDate"] == DBNull.Value ? null : (DateTime?)reader["TaskDueDate"];
            m_taskFollowUpDate = reader["TaskFollowUpDate"] == DBNull.Value ? null : (DateTime?)reader["TaskFollowUpDate"];
            m_condInternalNotes = reader["CondInternalNotes"].ToString();
            m_taskIsCondition = (bool)reader["TaskIsCondition"];
            m_taskResolvedUserId = reader["TaskResolvedUserId"] == DBNull.Value ? null : (Guid?)reader["TaskResolvedUserId"];
            m_assignToOnReactivationUserId = reader["AssignToOnReactivationUserId"] == DBNull.Value ? null : (Guid?)reader["AssignToOnReactivationUserId"];
            m_taskLastModifiedDate = (DateTime)reader["TaskLastModifiedDate"];
            m_borrowerNmCached = reader["BorrowerNmCached"].ToString();
            m_loanNumCached = reader["LoanNumCached"].ToString();
            m_taskToBeAssignedRoleId = reader["TaskToBeAssignedRoleId"] == DBNull.Value ? null : (Guid?)reader["TaskToBeAssignedRoleId"];
            m_taskToBeOwnerRoleId = reader["TaskToBeOwnerRoleId"] == DBNull.Value ? null : (Guid?)reader["TaskToBeOwnerRoleId"];
            m_taskCreatedByUserId = (Guid)reader["TaskCreatedByUserId"];
            m_taskDueDateLocked = (bool)reader["TaskDueDateLocked"];
            m_taskDueDateCalcField = reader["TaskDueDateCalcField"].ToString();
            m_taskDueDateCalcDays = (int)reader["TaskDueDateCalcDays"];
            string taskHistoryXml = reader["TaskHistoryXml"].ToString();
            m_taskPermissionLevelId = (int)reader["TaskPermissionLevelId"];
            m_taskClosedUserId = reader["TaskClosedUserId"] == DBNull.Value ? null : (Guid?)reader["TaskClosedUserId"];
            m_taskClosedDate = reader["TaskClosedDate"] == DBNull.Value ? null : (DateTime?)reader["TaskClosedDate"];
            m_taskRowVersion = (byte[])reader["TaskRowVersion"];
            m_condCategoryId = reader["CondCategoryId"] == DBNull.Value ? null : (int?)reader["CondCategoryId"];
            m_condIsHidden = (bool)reader["CondIsHidden"];
            //m_condRank = (decimal)reader["CondRank"];
            m_condRowId = (int)reader["CondRowId"];
            m_condDeletedDate = reader["CondDeletedDate"] == DBNull.Value ? null : (DateTime?)reader["CondDeletedDate"];
            m_condDeletedByUserNm = reader["CondDeletedByUserNm"].ToString();
            m_condIsDeleted = (bool)reader["CondIsDeleted"];
            m_assignedUserFullName = reader["TaskAssignedUserFullName"].ToString();
            m_ownerFullName = reader["TaskOwnerFullName"].ToString();
            m_closingUserFullName = reader["TaskClosedUserFullName"].ToString();
            m_condMessageId = reader["CondMessageId"].ToString();
            m_CondRequiredDocTypeId = reader.AsNullableInt("CondRequiredDocTypeId");
            this.resolutionBlockTriggerName = (string)reader["ResolutionBlockTriggerName"];
            this.resolutionDenialMessage = (string)reader["ResolutionDenialMessage"];
            this.resolutionDateSetterFieldId = (string)reader["ResolutionDateSetterFieldId"];

            m_isNew = false;

            //if there is no assigned user pull role names. May want to pull this into the db? -av 
            if (m_taskAssignedUserId == Guid.Empty || m_taskOwnerUserId == Guid.Empty)
            {

                if (m_taskAssignedUserId == Guid.Empty)
                {
                    if (m_taskToBeAssignedRoleId.HasValue)
                    {
                        AssignedUserFullName = Role.Get(m_taskToBeAssignedRoleId.Value).ModifiableDesc;
                    }
                    else
                    {
                        Tools.LogBug("Invalid task There is no assigned user and the role id is missing " + TaskId);
                    }
                }
                if (m_taskOwnerUserId == Guid.Empty)
                {
                    if (m_taskToBeOwnerRoleId.HasValue)
                    {
                        OwnerFullName = Role.Get(m_taskToBeOwnerRoleId.Value).ModifiableDesc;
                    }
                    else
                    {
                        Tools.LogBug("Invalid task There is no to be owner role id and the owner user id is empty " + TaskId);
                    }
                }
            }

            if (loanDates != null)
            {
                m_loanDates = loanDates;
            }

            m_taskHistory = new TaskHistory(taskHistoryXml, false);

            m_categoryDescription = categories ?? m_categoryDescription;
        }

        /// <summary>
        /// Checks if a task's status has changed.
        /// </summary>
        /// <returns>True if we detect that the TaskStatus has been modified. False otherwise.</returns>
        public bool CheckIfTaskStatusChanged()
        {
            return m_taskDelta.ModifiedFields != null && m_taskDelta.ModifiedFields.ContainsKey("TaskStatus");
        }

        public bool Save(bool isUpdateLoanStatistics)
        {
            return this.Save(isUpdateLoanStatistics, isUpdateAsPartOfRoleChange: false);
        }

        // saves the edit to the task and adds event to history
        public bool Save(bool isUpdateLoanStatistics, bool isUpdateAsPartOfRoleChange)
        {
            PrepareForSave(isUpdateAsPartOfRoleChange);
            if (m_isNew == true)
            {
                CreateTask();
                // New tasks will not have subscribers yet. We don't need to hit the db
                // to check for them.
                EnqueueTaskNotification(bypassSubscribers: true);
                if (isUpdateLoanStatistics)
                {
                    if (TempIsMigration == false)
                    {
                        // 12/16/2011 dd - Only run this when not create task through migration
                        TaskUtilities.EnqueueTasksDueDateUpdate(this.BrokerId, this.LoanId);
                        if (TaskIsCondition)
                        {
                            TaskUtilities.BumpLoanConditionSummaryVersionAndEnqueue(this.BrokerId, this.LoanId);
                        }

                    }
                }
                return true;
            }
            else if (m_isDirty == true)
            {
                bool result = false;
                using (CStoredProcedureExec exec = new CStoredProcedureExec(this.BrokerId))
                {
                    exec.BeginTransactionForWrite();
                    try
                    {
                        result = UpdateTask(exec);
                        if (result)
                        {
                            SaveAssociations(exec);
                        }
                        exec.CommitTransaction();
                    }
                    catch (SqlException)
                    {
                        result = false;
                        exec.RollbackTransaction();
                        throw;
                    }
                }

                if (result)
                {
                    EnqueueTaskNotification();
                    UpdateTaskLastActivityForMobileApp();
                    if (isUpdateLoanStatistics)
                    {
                        TaskUtilities.EnqueueTasksDueDateUpdate(this.BrokerId, this.LoanId);
                        if (TaskIsCondition)
                        {
                            TaskUtilities.BumpLoanConditionSummaryVersionAndEnqueue(this.BrokerId, this.LoanId);
                        }
                    }
                }

                return result;
            }
            else
            {
                if (m_docsToAssociateOnSave != null)
                {
                    using (CStoredProcedureExec exec = new CStoredProcedureExec(this.BrokerId))
                    {
                        exec.BeginTransactionForWrite();
                        try
                        {
                            SaveAssociations(exec);
                            exec.CommitTransaction();
                        }
                        catch
                        {
                            exec.RollbackTransaction();
                            throw;
                        }
                    }
                }
                return true;
            }
        }

        private void TrackModifiedLoan()
        {
            Tools.TrackIntegrationModifiedFile(BrokerId, LoanId, LoanNumCached);
        }

        //! may want to use this, but need to verify nothing breaks.
        //public bool Save(bool isUpdateLoanStatistics)
        //{
        //! if using this, be sure to rename other save methods to SaveInternal
        //    bool oldRet = SaveInternal(isUpdateLoanStatistics);
        //    m_taskDelta = new TaskChangeProfile();
        //    return oldRet;
        //}
        private void UpdateTaskLastActivityForMobileApp()
        {
            // 11/30/2011 dd - According to Mobile Lending QB spec by Binh we are only return the following activities.
            // - Task status change + last comment
            // - New Task comment
            // - Task subject change
            // Note we will not report task creation as an activity.

            Tuple<string, string> modifiedField = null;

            if (m_taskDelta.ModifiedFields.TryGetValue("TaskSubject", out modifiedField) == true)
            {
                // 11/30/2011 dd - Task subject change.
                // 4/11/2014 bs - Add activity type part of mobile spec v2.4
                if (this.TaskIsCondition != true)    
                    LendersOffice.MobileApp.LatestActivity.Send(this.LoanId, modifiedField.Item2 + "; TaskId = " + this.TaskId, "1");
                else
                    LendersOffice.MobileApp.LatestActivity.Send(this.LoanId, modifiedField.Item2 + "; TaskId = " + this.TaskId, "2");
            }
            else if (m_taskDelta.ModifiedFields.TryGetValue("TaskStatus", out modifiedField) == true)
            {
                string lastComment = m_taskDelta.NewComment;
                if (string.IsNullOrEmpty(lastComment) == true)
                {
                    // 12/8/2011 dd - Use task subject if user does not enter comment.
                    lastComment = this.m_taskSubject;
                }
                string status = modifiedField.Item2;
                if (modifiedField.Item2 == "Active" && modifiedField.Item1 != modifiedField.Item2)
                {
                    status = "Re-activated";
                }
                // 3/12/2014 bs - Change the wording Closed to Cleared if task is condition based on mobile spec v2.4
                if (modifiedField.Item2 == "Closed" && modifiedField.Item1 != modifiedField.Item2 && this.TaskIsCondition == true)
                {
                    status = "Cleared";
                }
                // 4/11/2014 bs - Add activity type part of mobile spec v2.4
                if (this.TaskIsCondition != true)
                    LendersOffice.MobileApp.LatestActivity.Send(this.LoanId, status + ": " + lastComment + "; TaskId = " + this.TaskId, "1");
                else
                    LendersOffice.MobileApp.LatestActivity.Send(this.LoanId, status + ": " + lastComment + "; TaskId = " + this.TaskId, "2");
            }
            else if (string.IsNullOrEmpty(m_taskDelta.NewComment) == false)
            {
                // 4/11/2014 bs - Add activity type part of mobile spec v2.4
                if (this.TaskIsCondition != true)
                    LendersOffice.MobileApp.LatestActivity.Send(this.LoanId, this.TaskSubject + ": Edited by " + m_taskDelta.NewCommentEditedBy + ". " + m_taskDelta.NewComment + "; TaskId = " + this.TaskId, "1");
                else
                    LendersOffice.MobileApp.LatestActivity.Send(this.LoanId, this.TaskSubject + ": Edited by " + m_taskDelta.NewCommentEditedBy + ". " + m_taskDelta.NewComment + "; TaskId = " + this.TaskId, "2");
            }
        }
        private void SaveAssociations(CStoredProcedureExec exec)
        {
            if (m_docsToAssociateOnSave != null)
            {
                m_associations = null; // need to update cached doc condition associations
                DocumentConditionAssociation.DocumentConditionAssociation.DeleteAllAssociationsForTask(exec, LoanId, TaskId);
                foreach (Guid docId in m_docsToAssociateOnSave)
                {
                    DocumentConditionAssociation.DocumentConditionAssociation dc = new LendersOffice.ObjLib.DocumentConditionAssociation.DocumentConditionAssociation(this.BrokerId, LoanId, TaskId, docId);
                    dc.Save(exec);
                }
            }
        }

        public bool Save(CStoredProcedureExec exec)
        {
            PrepareForSave(isUpdateAsPartOfRoleChange: false);
            if (m_isNew == true)
            {
                CreateTask(exec);
                EnqueueTaskNotification();
                SaveAssociations(exec);
                return true;
            }
            else if (m_isDirty == true)
            {
                bool result = UpdateTask(exec);
                SaveAssociations(exec);
                if (result)
                {
                    UpdateTaskLastActivityForMobileApp();
                    EnqueueTaskNotification();
                }

                return result;

            }
            else
            {
                SaveAssociations(exec);
                return true;
            }
        }

        public bool Save(string lastLoadedVersion, bool isUpdateLoanStatistics)
        {
            PrepareForSave(isUpdateAsPartOfRoleChange: false);
            if (m_isNew)
            {
                return false;
            }

            if (m_isDirty == false)
            {
                return true;
            }

            // Throw an exception if the versions do not match
            if (Convert.ToBase64String(m_taskRowVersion) != lastLoadedVersion)
                throw new TaskVersionMismatchException();

            bool ret = UpdateTask();
            if (isUpdateLoanStatistics)
            {
                TaskUtilities.EnqueueTasksDueDateUpdate(this.BrokerId, this.LoanId);
                if (TaskIsCondition)
                {
                    TaskUtilities.BumpLoanConditionSummaryVersionAndEnqueue(this.BrokerId, this.LoanId);
                }
            }
            if (ret)
            {
                EnqueueTaskNotification();
            }
            return ret;
        }

        /// <summary>
        /// Save and send email upon completion.
        /// </summary>
        public bool SaveAndSendEmail(string lastLoadedVersion, CBaseEmail email)
        {
            if (E_TaskStatus.Active != TaskStatus)
            {
                throw new TaskSaveAndSendWhenNotActiveException();
            }
            m_taskHistory.StoreOutgoingEmail(
                email.From
                , Tools.GetDateTimeDescription(DateTime.Now)
                , email.To
                , email.CCRecipient
                , email.Subject
                , email.Message
                );

            if (Save(lastLoadedVersion, true))
            {
                email.Send();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if we should send out a notification. False otherwise.
        /// </summary>
        /// <returns></returns>
        private bool NotifyEventContentExists()
        {
            if (!m_sendNotifications)
                return false;

            if (m_taskDelta.ModifiedFields.Count == 1 && m_taskDelta.ModifiedFields.ContainsKey("CondInternalNotes"))
                return false;

            return true;
        }

        /// <summary>
        /// Check permission. Data integrity, Audit Event.
        /// </summary>
        private void PrepareForSave(bool isUpdateAsPartOfRoleChange)
        {
            // Make sure we have latest permission.
            CalculatePermission();

            if (UserPermissionLevel == E_UserTaskPermissionLevel.None)
            {
                throw new TaskPermissionException("Do not have permission to modify this task.");
            }

            // Make sure the data in this task is complete and appropriate to save
            VerifyDataForSave(isUpdateAsPartOfRoleChange);

            // Make sure all fields changing are allowed to be
            CheckFieldLevelPermissionForSave();

            // Let the audit history component know what has changed
            SetAuditFieldsForSave();
            //Tools.LogInfo(m_taskDelta.ToString());
        }

        private void VerifyDataForSave(bool isUpdateAsPartOfRoleChange)
        {
            // Checking some business logic before save
            string exc = "";

            #region Task is new?
            if (false == m_isNew)
            {
                //? what is the below?, why does it matter if m_isNew?
                if (m_taskDelta.ModifiedFields.ContainsKey("TaskStatus"))
                {
                    Tuple<string, string> change = m_taskDelta.ModifiedFields["TaskStatus"];

                    if (change.Item1 == "Closed" && change.Item2 == "Closed")
                    {
                        throw new TaskPermissionException("Editing a closed task is not permmited. It should be reactivated first.");
                    }
                }


                //? What is the below?
                if (Tools.IsPmlUser(CurrentPrincipal)
                    && m_taskDelta.ModifiedFields.ContainsKey("AssignedUserFullName")
                    && !m_taskDelta.ModifiedFields.ContainsKey("TaskStatus")
                    && E_TaskStatus.Resolved == TaskStatus)
                {
                    // OPM 69225
                    throw new CBaseException("Resolved Tasks may not be re-assigned.", "PML user may not re-assign resolved task.");
                }


            }
            else// if (m_isNew == true)
            {
                //if (TaskCreatedByUserId != TaskOwnerUserId)  //? is this really necessary?
                //{
                //    exc = "Cannot save new task where created user id != owner user id";
                //    throw new CBaseException(exc, exc);
                //}

                //?
                //if (E_TaskStatus.Active != TaskStatus)
                //{
                //    exc = "Cannot save new task where status isn't active.";
                //    throw new CBaseException(exc, exc);
                //}
            }
            #endregion

            #region Condition? Related
            if (TaskIsCondition)
            {
                string permLevelName;
                bool permLevelAppliesToConditions;

                if (this.m_permissionLevels == null
                    || this.m_permissionLevelAppliesToConditions == null
                    || !this.m_permissionLevels.TryGetValue(this.TaskPermissionLevelId, out permLevelName)
                    || !this.m_permissionLevelAppliesToConditions.TryGetValue(this.TaskPermissionLevelId, out permLevelAppliesToConditions))
                {
                    var currentPermissionLevel = PermissionLevel.Retrieve(BrokerId, TaskPermissionLevelId);
                    permLevelAppliesToConditions = currentPermissionLevel.IsAppliesToConditions;
                    permLevelName = currentPermissionLevel.Name;
                }

                // Ensure this permission level allows save (use cache version)
                if (!permLevelAppliesToConditions)
                {
                    throw new TaskPermissionException(permLevelName + " Permission does not allow it to be condition.");
                }
                if (false == CondCategoryId.HasValue)
                {
                    exc = "Can't make task a condition without giving it a category.";
                    throw new CBaseException(exc, exc);
                }
                if (Tools.IsPmlUser(CurrentPrincipal))
                {
                    //! add this back when resolve with EDocsTest.EDocCreationTest.TestPmlCreationAndEditing
                    //if (m_taskDelta.ModifiedFields.ContainsKey("TaskIsCondition"))
                    //{
                    //    exc = "Can't let current PML users set tasks as conditions.";
                    //    throw new CBaseException(exc, exc);
                    //}

                    // disallow giving pml users conditions with internal notes unless duplicating. opm 242983 and others.
                    if (
                           !m_IsDuplicateTask
                        && m_taskDelta.ModifiedFields.ContainsKey("CondInternalNotes")
                       )
                    {
                        exc = "PML users can't modify internal notes for conditions.";
                        throw new CBaseException(exc, exc);
                    }
                }
                // Disallow hidden conditions from being assigned to pml users.
                // below pertains to opm 69776
                if (null != TaskAssignedUserId && Guid.Empty != TaskAssignedUserId)
                {
                    if (CondIsHidden &&
                        (m_taskDelta.ModifiedFields.ContainsKey("AssignedUserId") ||
                        m_taskDelta.ModifiedFields.ContainsKey("CondIsHidden")))
                    // last two are for performance only
                    {
                        if (Tools.IsPmlUser(TaskAssignedUserId, this.BrokerId))
                        {
                            exc = "May not assign a hidden condition to a PML user";
                            throw new CBaseException(exc, exc);
                        }
                    }
                }
            }
            else//  if(false == TaskIsCondition)
            {
                if (true == CondIsHidden)
                {
                    exc = "Can't be hidden condition if not condition.";
                    throw new CBaseException(exc, exc);
                }
                if (true == CondCategoryId.HasValue)
                {
                    exc = "Can't have condition category if not condition.";
                    throw new CBaseException(exc, exc);
                }

                if (false == string.IsNullOrEmpty(CondInternalNotes))
                {
                    exc = "Can't have condition internal notes if not condition.";
                    throw new CBaseException(exc, exc);
                }
                // becoming a condition is a one-way trip
                if (m_taskDelta.ModifiedFields.ContainsKey("TaskIsCondition"))
                {
                    Tuple<string, string> change = m_taskDelta.ModifiedFields["TaskIsCondition"];

                    if (change.Item1 == "Yes" && change.Item2 == "No")
                        throw new CBaseException(ErrorMessages.Generic, "Logic Error: Task cannot lose condition status.");
                }
            }

            #endregion

            #region Status Related
            if (E_TaskStatus.Closed == TaskStatus)
            {
                // if it is and was closed, make sure no  modifications took place.
                if (false == m_taskDelta.ModifiedFields.ContainsKey("TaskStatus"))
                {
                    //! the above may be insufficient though.
                    if (m_taskDelta.ModifiedFields.Count > 0)
                    {
                        throw new TaskPermissionException("Editing a closed task is not permitted.  It should first be reactivated.");
                    }
                }
                //? add prevention of going from active -> closed directly?
            }
            else if (E_TaskStatus.Resolved == TaskStatus)
            {
                //! add this back when fix resolving.
                //if (TaskAssignedUserId != TaskOwnerUserId)
                //{
                //    throw new CBaseException("Resolved tasks must be assigned to owner.",
                //        "Resolved tasks must be assigned to owner. Per 2.2.2 and 2.3.4.2");
                //}
                //? should this instead be part of the property setter, since it doesn't involve any 
                // other properties?
                if (m_taskDelta.ModifiedFields.ContainsKey("TaskStatus"))
                {
                    if (m_taskDelta.ModifiedFields["TaskStatus"].Item1 != "Active")
                    {
                        exc = "Can't resolve a task that wasn't active.";
                        throw new CBaseException(exc, exc);
                    }
                }
                if (false == m_IsDuplicateTask)
                {
                    //? 2.3.4.1 - do this for all resolved tasks, or just when resolving?
                    if (m_taskDelta.ModifiedFields.ContainsKey("TaskIsCondition"))
                    {
                        //if (m_taskDelta.ModifiedFields["TaskIsCondition"].Item1 == "No")
                        //{
                        exc = "Can't edit task's condition state when it's resolved.";
                        throw new CBaseException(exc, exc);
                        //}
                    }
                }
            }
            else // if E_TaskStatus.Active == TaskStatus
            {
            }

            if (E_TaskStatus.Active != TaskStatus)
            {
                // if it was and is resolved/closed (i.e. don't yet care if status changed)
                // opm 235662 ejm - We only want this to trigger if this update isn't due to a role being assigned to a user.
                if (false == m_taskDelta.ModifiedFields.ContainsKey("TaskStatus") && !isUpdateAsPartOfRoleChange)
                {
                    if (m_taskDelta.ModifiedFields.ContainsKey("AssignedUserFullName"))
                    {
                        //? the above should be TaskAssignedUserId, no?  But that isn't tracked by m_taskDelta.
                        exc = "Can't assign users for resolved/closed tasks";
                        throw new CBaseException(exc, exc);
                    }
                }
            }
            #endregion

            #region Field / Field Relations
            if (string.IsNullOrEmpty(TaskSubject))
            {
                exc = "Must have a subject";
                throw new CBaseException(exc, exc);
            }
            //? what to do about task due dates?
            if (false == TaskDueDate.HasValue && string.IsNullOrEmpty(TaskDueDateCalcField)) //? || isCalculating))
            {
                exc = "Must have a task due date or a calculated field due date.";
                throw new CBaseException(exc, exc);
            }
            //?
            //if(TaskDueDate.HasValue && false == string.IsNullOrEmpty(TaskDueDateCalcField))
            //{
            //    exc = "Can't have both a due date and a calculated field due date.";    
            //    throw new CBaseException(exc, exc);
            //}
            if (false == string.IsNullOrEmpty(TaskDueDateCalcField))
            {
                if (false == Task.ListValidDateFields(false).Contains(TaskDueDateCalcField))
                {
                    exc = "Can't have " + TaskDueDateCalcField + " as a calc date field " +
                        "as it is not a valid date field.";
                    throw new CBaseException(exc, exc);
                }
            }
            #endregion

            // Per OPM 69689, if this task is being assigned to someone who cannot access it,
            // we have to throw.

            if (IsBatchUpdate)
            {
                if (m_taskDelta.ModifiedFields.ContainsKey("AssignedUserFullName")
                    && this.TaskAssignedUserId != Guid.Empty)
                {
                    if (TaskPermissionProcessor.GetMergedTaskAccessLevel(this, TaskAssignedUserId) == E_UserTaskPermissionLevel.None)
                    {
                        throw new TaskPermissionException("Assigned user " + m_taskDelta.ModifiedFields["AssignedUserFullName"].Item2 + " would not be able to access this task.");
                    }
                }
            }
        }

        private void CheckFieldLevelPermissionForSave()
        {
            foreach (string fieldId in m_taskDelta.ModifiedFields.Keys)
            {
                VerifyFieldWritePermission(fieldId, m_taskDelta.ModifiedFields[fieldId]);
            }

        }

        private void SetAuditFieldsForSave()
        {
            // Regular Fields
            foreach (KeyValuePair<string, Tuple<string, string>> pair in m_taskDelta.ModifiedFields)
            {
                switch (pair.Key)
                {
                    case "AssignedUserFullName":
                        {
                            m_taskHistory.StoreAssignmentEvent(pair.Value.Item2);
                            break;
                        }
                    case "TaskStatus":
                        {
                            if (m_isNew == false)
                                m_taskHistory.StoreStatusChangeEvent(m_taskDelta.StatusChange.Item1, m_taskDelta.StatusChange.Item2);
                            break;
                        }

                    case "CondInternalNotes":
                        break; // Does not create audit event
                    default:
                        {
                            if (m_isNew == false)
                                m_taskHistory.StoreNormalFieldChange(
                                    GetFriendlyFieldNameForAudit(pair.Key) // This guy will throw for unknown fields
                                    , pair.Value.Item1
                                    , pair.Value.Item2
                                    );
                            break;
                        }


                }
            }
        }

        private string GetFriendlyFieldNameForAudit(string fieldId)
        {
            switch (fieldId)
            {
                case "TaskFollowUpDate": return "Follow-up Date";
                case "TaskSubject": return "Subject";
                case "TaskDueDate": return "Due Date";
                case "OwnerFullName": return "Owner";
                case "TaskIsCondition": return "IsCondition";
                case "CondCategory": return "Condition Category";
                case "PermissionLevel": return "Permission Level";
                case "CondIsHidden": return "Condition is hidden";
                case "CondRequiredDocTypeId": return "Required Document Type";
                case "ResolutionBlockTriggerName": return "Resolution Block Trigger";
                case "ResolutionDateSetterFieldId": return "Resolution Date Setter";
                default: throw new CBaseException(ErrorMessages.Generic, "Unknown field: " + fieldId);
            }
        }



        private void CreateTask()
        {
            CreateTaskImpl((name, parameters) => StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, name, 3, parameters));
            TrackModifiedLoan();
        }

        private void CreateTask(CStoredProcedureExec exec)
        {
            CreateTaskImpl((name, parameters) => exec.ExecuteNonQuery(name, 3, parameters));
            TrackModifiedLoan();
        }

        /// <summary>
        /// Usually we need to copy paste this code twice in order to support
        /// transactions and non transaction saves. By using the saveMethod delegate I can reuse the code. 
        /// The callers delegate calls the NonQuery to do the save. 
        /// </summary>
        /// <param name="saveMethod"></param>
        private void CreateTaskImpl(Action<string, SqlParameter[]> saveMethod)
        {
            List<SqlParameter> parameters = BuildSaveParameterList();
            SqlParameter newTaskId = new SqlParameter("@TaskId", SqlDbType.VarChar, 10);
            newTaskId.Direction = ParameterDirection.Output;
            parameters.Add(newTaskId);

            SqlParameter borrowerNmCachedParam = new SqlParameter("@BorrowerNmCached", SqlDbType.VarChar, 60);
            borrowerNmCachedParam.Direction = ParameterDirection.Output;
            parameters.Add(borrowerNmCachedParam);

            SqlParameter loanNumCachedParam = new SqlParameter("@LoanNumCached", SqlDbType.VarChar, 36);
            loanNumCachedParam.Direction = ParameterDirection.Output;
            parameters.Add(loanNumCachedParam);

            saveMethod("TASK_CreateTask", parameters.ToArray());
            TaskId = (string)newTaskId.Value;
            m_borrowerNmCached = (string)borrowerNmCachedParam.Value;
            m_loanNumCached = (string)loanNumCachedParam.Value;

            m_isNew = false;
            m_taskDelta = new TaskChangeProfile(); // Start fresh
        }

        public bool IsAutoResolveTriggerTrue()
        {
            return IsTriggerTrueImpl(TaskTriggerType.AutoResolve);
        }

        public bool IsAutoCloseTriggerTrue()
        {
            return IsTriggerTrueImpl(TaskTriggerType.AutoClose);
        }

        private bool IsTriggerTrueImpl(TaskTriggerType triggerType)
        {
            if (this.IsResolutionBlocked())
            {
                return false;
            }

            TaskTriggerAssociation association = TaskTriggerAssociation.GetTaskTriggerAssociation(this, triggerType);
            if (association == null)
            {
                return false;
            }

            HashSet<string> dependencies = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            dependencies.UnionWith(LendingQBExecutingEngine.GetDependencyFieldsByTrigger(this.BrokerId, association.TriggerName));

            LoanValueEvaluator valueEvaluator = SetUpLoanValueEvaluator(dependencies);

            return LendingQBExecutingEngine.EvaluateTrigger(association.TriggerName, valueEvaluator);
        }

        /// <summary>
        /// Used to evaluate task automation triggers on save.
        /// </summary>
        internal void EvaluateTaskAutomationTriggers()
        {
            if (this.IsResolutionBlocked())
            {
                return;
            }

            // Get triggers.
            IReadOnlyDictionary<TaskTriggerType, TaskTriggerAssociation> associationsByType = TaskTriggerAssociation.GetTaskTriggerAssociations(this);
            if (!associationsByType.Any())
            {
                return;
            }

            // Set up workflow evaluation.
            HashSet<string> dependencies = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            foreach (TaskTriggerAssociation association in associationsByType.Values)
            {
                dependencies.UnionWith(LendingQBExecutingEngine.GetDependencyFieldsByTrigger(this.BrokerId, association.TriggerName));
            }

            LoanValueEvaluator valueEvaluator = SetUpLoanValueEvaluator(dependencies);

            // Check Auto-Close trigger before Auto-Resolve trigger. We can skip Auto-Resolve check if we know we're going to close the task anyway.
            TaskTriggerAssociation autoCloseAssociation = null;
            TaskTriggerAssociation autoResolveAssociation = null;
            if (associationsByType.TryGetValue(TaskTriggerType.AutoClose, out autoCloseAssociation)
                && LendingQBExecutingEngine.EvaluateTrigger(autoCloseAssociation.TriggerName, valueEvaluator))
            {
                TaskAutomationJob.CreateTaskAutomationJob(autoCloseAssociation.BrokerId, autoCloseAssociation.LoanId, autoCloseAssociation.TaskId, TaskAutomationJobType.AutoClose);
            }
            else if (associationsByType.TryGetValue(TaskTriggerType.AutoResolve, out autoResolveAssociation)
                && LendingQBExecutingEngine.EvaluateTrigger(autoResolveAssociation.TriggerName, valueEvaluator))
            {
                TaskAutomationJob.CreateTaskAutomationJob(autoResolveAssociation.BrokerId, autoResolveAssociation.LoanId, autoResolveAssociation.TaskId, TaskAutomationJobType.AutoResolve);
            }
        }

        private LoanValueEvaluator SetUpLoanValueEvaluator(HashSet<string> dependencies)
        {
            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(dependencies, this.LoanId);

            if (this.CurrentPrincipal is SystemUserPrincipal)
            {
                valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(this.BrokerId, (SystemUserPrincipal) this.CurrentPrincipal));
            }
            else
            {
                valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(this.CurrentPrincipal));
            }

            return valueEvaluator;
        }

        private bool UpdateTask()
        {
            bool updated = false;
            UpdateTaskImpl((spName, parameters) => updated = StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, spName, 3, parameters) != 0);
            if (updated)
            {
                TrackModifiedLoan();
            }
            return updated;
        }

        private bool UpdateTask(CStoredProcedureExec exec)
        {
            bool updated = false;
            UpdateTaskImpl((spName, parameters) => updated = exec.ExecuteNonQuery(spName, 3, parameters) != 0);
            if (updated)
            {
                TrackModifiedLoan();
            }
            return updated;
        }

        /// <summary>
        /// Builds up the data needed for the save. Calls action delegate that takes the sp name and the parameters. 
        /// The Action should call ExecuteNonQuery on CStoredProcedureExec or StoredProcedureHelper
        /// </summary>
        /// <param name="saveMethod"></param>
        private void UpdateTaskImpl(Action<string, SqlParameter[]> saveMethod)
        {
            List<SqlParameter> parameters = BuildSaveParameterList();
            parameters.Add(new SqlParameter("@TaskId", TaskId));
            saveMethod("TASK_UpdateTask", parameters.ToArray());

            // OPM 471933 - Automation triggers need to be evaluated if ResolutionBlockTriggerName changed.
            if (m_taskDelta.ModifiedFields.ContainsKey("ResolutionBlockTriggerName"))
            {
                EvaluateTaskAutomationTriggers();
            }
        }

        private List<SqlParameter> BuildSaveParameterList()
        {

            if (string.IsNullOrEmpty(TaskSubject))
            {
                throw new TaskException("The subject cannot be blank", "The subject for task is blank");
            }
            string taskHistoryXml = m_taskHistory.GetXmlContentForAllEntries();
            VerifyAuditHistory(taskHistoryXml.Length);

            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@BrokerId", BrokerId));
            parameters.Add(new SqlParameter("@TaskAssignedUserId", TaskAssignedUserId));
            parameters.Add(new SqlParameter("@TaskStatus", TaskStatus));
            parameters.Add(new SqlParameter("@LoanId", LoanId));
            parameters.Add(new SqlParameter("@TaskOwnerUserId", TaskOwnerUserId));
            parameters.Add(new SqlParameter("@TaskSubject", TaskSubject));
            if (TaskDueDate.HasValue) parameters.Add(new SqlParameter("@TaskDueDate", TaskDueDate));
            if (TaskFollowUpDate.HasValue) parameters.Add(new SqlParameter("@TaskFollowUpDate", TaskFollowUpDate));
            parameters.Add(new SqlParameter("@CondInternalNotes", CondInternalNotes));
            parameters.Add(new SqlParameter("@TaskIsCondition", TaskIsCondition));
            if (TaskResolvedUserId.HasValue) parameters.Add(new SqlParameter("@TaskResolvedUserId", TaskResolvedUserId));
            if (AssignToOnReactivationUserId.HasValue) parameters.Add(new SqlParameter("@AssignToOnReactivationUserId", AssignToOnReactivationUserId));
            if (TaskToBeAssignedRoleId.HasValue && TaskToBeAssignedRoleId.Value != Guid.Empty) parameters.Add(new SqlParameter("@TaskToBeAssignedRoleId", TaskToBeAssignedRoleId));
            if (TaskToBeOwnerRoleId.HasValue && TaskToBeOwnerRoleId.Value != Guid.Empty) parameters.Add(new SqlParameter("@TaskToBeOwnerRoleId", TaskToBeOwnerRoleId));
            parameters.Add(new SqlParameter("@TaskCreatedByUserId", TaskCreatedByUserId));
            parameters.Add(new SqlParameter("@TaskDueDateLocked", TaskDueDateLocked));
            parameters.Add(new SqlParameter("@TaskDueDateCalcField", TaskDueDateCalcField));
            parameters.Add(new SqlParameter("@TaskDueDateCalcDays", TaskDueDateCalcDays));
            parameters.Add(new SqlParameter("@TaskHistoryXml", taskHistoryXml));
            parameters.Add(new SqlParameter("@TaskPermissionLevelId", TaskPermissionLevelId));
            if (TaskClosedUserId.HasValue) parameters.Add(new SqlParameter("@TaskClosedUserId", TaskClosedUserId));
            if (TaskClosedDate.HasValue) parameters.Add(new SqlParameter("@TaskClosedDate", TaskClosedDate));
            if (CondCategoryId.HasValue) parameters.Add(new SqlParameter("@CondCategoryId", CondCategoryId));
            parameters.Add(new SqlParameter("@CondIsHidden", CondIsHidden));
            //parameters.Add(new SqlParameter("@CondRank", CondRank));
            parameters.Add(new SqlParameter("@CondRowId", CondRowId)); // We will be sending the current CondRowId, but it should only be incremented in the SP
            if (CondDeletedDate.HasValue) parameters.Add(new SqlParameter("@CondDeletedDate", CondDeletedDate));
            parameters.Add(new SqlParameter("@CondDeletedByUserNm", CondDeletedByUserNm));
            parameters.Add(new SqlParameter("@CondIsDeleted", CondIsDeleted));
            parameters.Add(new SqlParameter("@CondRequiredDocTypeId", CondRequiredDocTypeId));

            parameters.Add(new SqlParameter("@TaskAssignedUserFullName", AssignedUserFullName));
            parameters.Add(new SqlParameter("@TaskOwnerFullName", OwnerFullName));
            parameters.Add(new SqlParameter("@TaskClosedUserFullName", ClosingUserFullName));

            parameters.Add(new SqlParameter("@ResolutionBlockTriggerName", this.ResolutionBlockTriggerName));
            parameters.Add(new SqlParameter("@ResolutionDenialMessage", this.ResolutionDenialMessage));
            parameters.Add(new SqlParameter("@ResolutionDateSetterFieldId", this.ResolutionDateSetterFieldId));

            if (m_isNew == false)
            {
                parameters.Add(new SqlParameter("@TaskRowVersion", m_taskRowVersion));
            }
            parameters.Add(new SqlParameter("@CondMessageId", m_condMessageId));
            //Tools.LogWarning("TASK debug : " + Environment.NewLine + Environment.NewLine + m_taskHistory.GetXmlContentForAllEntries());

            return parameters;
        }

        // Check per OPM 68076 to avoid endlessly large audit history list.
        private void VerifyAuditHistory(int length)
        {
            int warningCharlimit = HISTORY_WARNING_MAX_MB * 1024 * 1024;
            int blockingCharlimit = HISTORY_BLOCKING_MAX_MB * 1024 * 1024;

            // If it hits the upper limit, throw exception denying save
            if (length > blockingCharlimit)
            {
                // Throw exception.  This save cannot be allowed.
                throw new TaskLargeAuditException();
            }

            // If saving above the warning threshold, log and send email to devs,
            // but let the saves go through.  
            if (length > warningCharlimit)
            {
                Tools.LogErrorWithCriticalTracking(
                    String.Format(
                    "Task {0} has very large audit history of length {1}."
                    , TaskId
                    , length
                    ));
            }
        }

        private void MarkAsDirty()
        {
            m_isDirty = true;
        }

        private static void LoadCachedData(Task task, LoanDataForTasks loanData)
        {
            if (loanData != null)
            {
                task.LoanNumCached = loanData.LoanNumber;
                task.BorrowerNmCached = loanData.PrimaryBorrowerFullName;
            }
            else
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(task.LoanId, typeof(Task));
                dataLoan.InitLoad();
                task.LoanNumCached = dataLoan.sLNm;
                task.BorrowerNmCached = dataLoan.sPrimBorrowerFullNm;
            }
        }

        public static int GetDefaultPermissionLevel(Guid brokerId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_GetDefaultPermissionLevelByBrokerId", parameters))
            {
                if (reader.Read())
                {
                    return (int)reader["TaskPermissionLevelId"];
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, "Cannot get default permission level");
                }
            }
        }

        #endregion

        public string TaskStatus_rep
        {
            get
            {
                return TaskStatus.ToString();
            }
        }

        public string TaskFromAddress
        {
            get
            {
                return BrokerDB.TaskAliasEmailAddress;
            }
        }

        public string TaskEmailSubject
        {
            get
            {
                return string.Format("Task {0} Due: {1} - {2} - {3} - {4}", TaskId, TaskSingleDueDate, LoanNumCached, BorrowerNmCached, TaskSubject);
            }
        }

        public string CondIsHidden_rep
        {
            get
            {
                return CondIsHidden ? "Yes" : "No";
            }
        }

        public string TaskDueDate_rep
        {
            get
            {
                if (TaskDueDate.HasValue)
                {
                    return TaskDueDate.Value.ToShortDateString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                try
                {
                    TaskDueDate = Convert.ToDateTime(value);
                }
                catch (FormatException)
                {
                    TaskDueDate = null;
                }
            }
        }

        public string TaskSingleDueDate
        {
            get
            {
                return TaskUtilities.SanitizeTaskText(TaskSingleDueDateRaw, CurrentPrincipal);
            }
        }

        /// <summary>
        /// This is not sanitized
        /// </summary>
        public string TaskSingleDueDateRaw
        {
            get
            {
                string d = TaskDueDate_rep;
                if (string.IsNullOrEmpty(d))
                {
                    d = DueDateDescription(false);
                }
                return d;
            }
        }

        public string TaskFollowUpDate_rep
        {
            get
            {
                if (TaskFollowUpDate.HasValue)
                {
                    return TaskFollowUpDate.Value.ToShortDateString();
                }
                else
                {
                    return "";
                }
            }
            set
            {
                try
                {
                    TaskFollowUpDate = Convert.ToDateTime(value);
                }
                catch (FormatException)
                {
                    TaskFollowUpDate = null;
                }
            }
        }

        public string TaskOwnerUserId_rep
        {
            get
            {
                return TaskOwnerUserId.ToString();
            }
        }

        public string TaskAssignedUserId_rep
        {
            get
            {
                return TaskAssignedUserId.ToString();
            }
        }

        public string TaskToBeOwnerRoleId_rep
        {
            get
            {
                if (TaskToBeOwnerRoleId.HasValue)
                {
                    return TaskToBeOwnerRoleId.ToString();
                }
                else
                {
                    return Guid.Empty.ToString();
                }
            }
        }

        public string TaskToBeAssignedRoleId_rep
        {
            get
            {
                if (TaskToBeAssignedRoleId.HasValue)
                {
                    return TaskToBeAssignedRoleId.ToString();
                }
                else
                {
                    return Guid.Empty.ToString();
                }
            }
        }

        /// <summary>
        /// Intended for use when performing batch operations. This will cache
        /// workflow trigger results for a given loan file to improve performance.
        /// </summary>
        public ResolutionTriggerProcessor ResolutionTriggerProcessor { get; set; }

        /// <summary>
        /// Intended for use when performing batch operations. This will aggregate
        /// resolution date fields which need to be set when an operation is complete.
        /// </summary>
        public ResolutionDateSetter ResolutionDateSetter { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the resolution block trigger 
        /// should be disabled. This is intended for use with migrations.
        /// </summary>
        public bool DisableResolutionBlockTrigger { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the resolution date setter
        /// should be disabled. This is intended for use with migrations.
        /// </summary>
        public bool DisableResolutionDateSetter { get; set; }

        #region DisplayNameMapping
        private Dictionary<Guid, string> m_knownUsers = new Dictionary<Guid, string>();

        /// <summary>
        /// Stores a user name for the user id to avoid unnecessary database hits.
        /// </summary>
        /// <param name="userId">The user id.</param>
        /// <param name="fullName">The employee's name in "{FirstName} {LastName}" format.</param>
        private void AddFullNameForUserId(Guid userId, string fullName)
        {
            if (!this.m_knownUsers.ContainsKey(userId))
            {
                this.m_knownUsers.Add(userId, fullName);
            }
        }

        private string GetUserDisplayName(Guid userId)
        {
            if (userId == Guid.Empty)
            {
                return "Blank";
            }

            if (userId == SystemUserPrincipal.TaskSystemUser.UserId)
            {
                return SystemUserPrincipal.TaskSystemUser.DisplayName;
            }

            if (m_knownUsers.ContainsKey(userId) == false)
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@EmployeeUserID", userId)
                                            };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, "GetEmployeeNameByUserId", parameters))
                {
                    if (reader.Read())
                    {
                        m_knownUsers[userId] = reader["FirstLast"].ToString();
                    }
                    else
                    {
                        throw new CBaseException(ErrorMessages.Generic, "Cannot find user " + userId.ToString());
                    }
                }
            }

            return m_knownUsers[userId];
        }

        public void DisableNotifications()
        {
            m_sendNotifications = false;
        }

        private Dictionary<int, string> m_permissionLevels = null;
        private Dictionary<int, string> m_permissionLevelDescriptions = null;
        private Dictionary<int, bool> m_permissionLevelAppliesToConditions = null;

        private string GetPermissionLevelDisplayName(int permissionId)
        {
            if (permissionId == 0) return "";

            if (m_permissionLevels == null)
            {
                GetPermissionLevelData();
            }

            return m_permissionLevels[permissionId];

        }

        private string GetPermissionLevelDescription(int permissionId)
        {
            if (m_permissionLevels == null)
            {
                GetPermissionLevelData();
            }

            return m_permissionLevelDescriptions[permissionId];
        }

        private bool GetPermissionLevelAppliesToConditions(int permissionId)
        {
            if (m_permissionLevels == null)
            {
                GetPermissionLevelData();
            }

            return m_permissionLevelAppliesToConditions[permissionId];
        }

        private void GetPermissionLevelData()
        {
            this.SetPermissionLevelData(PermissionLevelData.Retrieve(this.BrokerId));
        }

        private void SetPermissionLevelData(PermissionLevelData permissionLevels)
        {
            m_permissionLevels = permissionLevels.NameById;
            m_permissionLevelDescriptions = permissionLevels.DescriptionById;
            m_permissionLevelAppliesToConditions = permissionLevels.AppliesToConditionsById;
        }

        private string GetRoleDisplayName(Guid roleId)
        {
            if (roleId == Guid.Empty) return "Blank";

            return Role.Get(roleId).ModifiableDesc;
        }

        private Dictionary<int, ConditionCategory> m_categoryDescription = new Dictionary<int, ConditionCategory>();
        private string GetCategoryDisplayName(int categoryId)
        {
            if (m_categoryDescription.ContainsKey(categoryId) == false)
            {
                ConditionCategory cat = ConditionCategory.GetCategory(BrokerId, categoryId);
                m_categoryDescription.Add(categoryId, cat);
            }

            return m_categoryDescription[categoryId].Category;
        }

        private string GetStatusDisplayName(E_TaskStatus status)
        {
            switch (status)
            {
                case E_TaskStatus.Active: return "Active";
                case E_TaskStatus.Resolved: return "Resolved";
                case E_TaskStatus.Closed: return "Closed";
                default:
                    throw new UnhandledEnumException(status);
            }
        }
        #endregion

        /// <summary>
        /// Gets a human readable representation of the due date. This operation
        /// may require loading the loan.
        /// </summary>
        /// <param name="sanitize"></param>
        /// <returns></returns>
        public string DueDateDescription(bool sanitize)
        {
            // If more task members are required to get the due date desc,
            // please check to see if their setters need to be updated to load
            // original due date description. That field is lazy loaded and, if
            // any of its dependences are updated, we need the value to be what
            // it was BEFORE the dependency was updated.
            return Tools.GetTaskDueDateDescription(LoanId, TaskDueDateCalcDays, TaskDueDateCalcField, sanitize, m_loanDates);
        }

        public static string GetCalculatedDueDate(Guid loanId, int taskDueDateCalcDays, string taskDueDateCalcField)
        {
            return GetCalculatedDueDate(loanId, taskDueDateCalcDays, taskDueDateCalcField, null);
        }

        public static LoanDataForTasks GetRequiredLoanData(Guid loanId, IEnumerable<DueDateCalcInfo> dueDateInfo)
        {
            var dependencies = dueDateInfo.Select(info => info.FieldId)
                .Where(id => !string.IsNullOrEmpty(id))
                .ToList();
            dependencies.Add("sfCalculateOffset");
            dependencies.Add("sLNm");
            dependencies.Add("sPrimBorrowerFullNm");

            var loan = new CFullAccessPageData(loanId, dependencies);
            loan.AllowLoadWhileQP2Sandboxed = true;
            loan.ByPassFieldSecurityCheck = true;
            loan.InitLoad();

            var dateCache = GenerateDateCache(loan, dueDateInfo);

            return new LoanDataForTasks(loan.sLNm, loan.sPrimBorrowerFullNm, dateCache);
        }

        private static Dictionary<string, string> GenerateDateCache(CPageData dataLoan, IEnumerable<DueDateCalcInfo> dueDateInfo)
        {
            var cache = new Dictionary<string, string>();

            foreach (var calcInfo in dueDateInfo)
            {
                if (string.IsNullOrEmpty(calcInfo.FieldId))
                {
                    continue;
                }

                var key = GetLoanDateCacheKey(calcInfo.FieldId, calcInfo.Offset);
                if (cache.ContainsKey(key))
                {
                    continue;
                }

                DateTime? dt = dataLoan.GetDateTimeOffset(calcInfo.FieldId, calcInfo.Offset);
                string dateDesc = dt.HasValue ? dt.Value.ToShortDateString() : string.Empty;

                cache.Add(key, dateDesc);
            }

            return cache;
        }

        public void SetLoanDates(Dictionary<string, string> loanDates)
        {
            if (loanDates == null)
            {
                return;
            }

            this.m_loanDates = loanDates;
        }

        private static string GetLoanDateCacheKey(string taskDueDateCalcField, int taskDueDateCalcDays)
        {
            return taskDueDateCalcField + taskDueDateCalcDays.ToString();
        }

        public static string GetCalculatedDueDate(Guid loanId, int taskDueDateCalcDays, string taskDueDateCalcField, Dictionary<string, string> loanDateCache)
        {
            if (string.IsNullOrEmpty(taskDueDateCalcField))
            {
                return string.Empty;
            }

            string cacheKey = GetLoanDateCacheKey(taskDueDateCalcField, taskDueDateCalcDays);
            if (loanDateCache != null && loanDateCache.ContainsKey(cacheKey))
            {
                return loanDateCache[cacheKey];
            }

            CPageData dataLoan = new CFullAccessPageData(loanId, new string[] { taskDueDateCalcField, "sfCalculateOffset" });
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();
            dataLoan.ByPassFieldSecurityCheck = true;
            DateTime? dt = dataLoan.GetDateTimeOffset(taskDueDateCalcField, taskDueDateCalcDays);

            string dateDesc = dt.HasValue ? dt.Value.ToShortDateString() : string.Empty;

            if (loanDateCache != null)
            {
                loanDateCache.Add(cacheKey, dateDesc);
            }

            return dateDesc;
        }

        public void SetDueDate(DateTime dueDate)
        {
            // Set date and date locked
            TaskDueDateLocked = true;
            TaskDueDate = dueDate;


            // Clear offset and loan name
            TaskDueDateCalcDays = 0;
            TaskDueDateCalcField = string.Empty;

        }

        public void SetDueDate(string dueDateCalcField, int dueDateCalcDays)
        {
            // Set Field, offset.
            TaskDueDateCalcDays = dueDateCalcDays;
            TaskDueDateCalcField = dueDateCalcField;
            TaskDueDateLocked = false;

            // Set date (perform calculation)
            string dateResult = GetCalculatedDueDate(LoanId, dueDateCalcDays, dueDateCalcField);

            if (dateResult == string.Empty)
            {
                TaskDueDate = null;
            }
            else
            {
                TaskDueDate = DateTime.Parse(dateResult);
            }

        }

        // reactivates the task and saves
        public void Reactivate()
        {
            if (UserPermissionLevel == E_UserTaskPermissionLevel.None)
                throw new TaskPermissionException("Must have at least Workon permission to reactivate task.");

            MarkAsDirty();


            m_taskDelta.SetStatusChange(m_taskStatus, E_TaskStatus.Active);
            //m_taskHistory.StoreStatusChangeEvent(m_taskStatus, E_TaskStatus.Active);

            m_taskStatus = E_TaskStatus.Active;

            m_assignToOnReactivationUserId = null;
            m_taskClosedDate = null;
            m_taskClosedUserId = null;
            ClosingUserFullName = "";

            if (CurrentPrincipal != null)
            {
                if (!m_taskDelta.ModifiedFields.ContainsKey("AssignedUserFullName")
                    && CurrentPrincipal.UserId != SystemUserPrincipal.TaskSystemUser.UserId)
                {
                    m_taskAssignedUserId = CurrentPrincipal.UserId;
                    AssignedUserFullName = GetUserDisplayName(m_taskAssignedUserId);
                }
            }
            else
            {
                // Email processor cannot resolve
                throw new CBaseException(ErrorMessages.Generic, "Only user can resolve");
            }


            Save(true);
        }

        private bool IsBeingReactivated()
        {
            return m_taskDelta.ModifiedFields.ContainsKey("TaskStatus") && m_taskStatus == E_TaskStatus.Active;
        }

        /// <summary>
        /// Determine if resolution is blocked by the resolution block trigger.
        /// </summary>
        /// <returns>
        /// Returns true when the resolution of the task is blocked. False when 
        /// there is no trigger set, when the trigger evaluates to false, when the
        /// DisableResolutionBlockTrigger property is true or when the trigger 
        /// cannot be found.
        /// </returns>
        public bool IsResolutionBlocked()
        {
            if (string.IsNullOrEmpty(this.ResolutionBlockTriggerName) || this.DisableResolutionBlockTrigger)
            {
                return false;
            }
            else if (this.ResolutionTriggerProcessor != null && this.ResolutionTriggerProcessor.LoanId == this.LoanId &&
                this.CurrentPrincipal.UserId == this.ResolutionTriggerProcessor.Principal.UserId)
            {
                return this.ResolutionTriggerProcessor.GetTriggerResult(this.resolutionBlockTriggerName);
            }
            else
            {
                if (!this.resolutionBlockTriggerResult.HasValue)
                {
                    this.resolutionBlockTriggerResult = ResolutionTriggerProcessor.GetResolutionTriggerResult(this, this.CurrentPrincipal);
                }

                return this.resolutionBlockTriggerResult.Value;
            }
        }

        /// <summary>
        /// If the ResolutionDateSetter property has not been set, this will set 
        /// the resolution date field on the loan and save. If the ResolutionDateSetter
        /// property has been set, this will add this instance's resolution date
        /// as a field that needs to be set. No action will be taken if the 
        /// DisableResolutionDateSetter property is true.
        /// </summary>
        public void SetResolutionDateField()
        {
            if (string.IsNullOrEmpty(this.ResolutionDateSetterFieldId)
                || this.DisableResolutionDateSetter
                || ConstApp.TaskResolutionDateSetterFieldIdBlacklist.Contains(this.resolutionDateSetterFieldId))
            {
                return;
            }

            if (this.ResolutionDateSetter == null)
            {
                ResolutionDateSetter.SetResolutionDateForTask(this, this.CurrentPrincipal);
            }
            else
            {
                this.ResolutionDateSetter.AddField(this.ResolutionDateSetterFieldId);
            }
        }

        /// <summary>
        /// Resolves the task and saves. Batch operations should use a 
        /// ResolutionDateSetter to aggregate field changes.
        /// </summary>
        /// <param name="lastLoadedVersion">The last loaded version of the task</param>
        public void Resolve(string lastLoadedVersion)
        {
            if (Convert.ToBase64String(m_taskRowVersion) != lastLoadedVersion)
                throw new TaskVersionMismatchException();
            Resolve();
        }

        /// <summary>
        /// Resolves the task and saves. Batch operations should use a 
        /// ResolutionDateSetter to aggregate field changes.
        /// </summary>
        public void Resolve()
        {
            if (UserPermissionLevel == E_UserTaskPermissionLevel.None)
                throw new TaskPermissionException("Must have at least Workon permission to resolve task.");
            if (this.IsResolutionBlocked())
                throw new TaskException(this.ResolutionDenialMessage, "Task did not satisfy resolution block trigger.");

            MarkAsDirty();

            if (CurrentPrincipal != null)
            {
                if (CurrentPrincipal is SystemUserPrincipal)
                {
                    m_assignToOnReactivationUserId = TaskAssignedUserId;
                }

                m_taskResolvedUserId = CurrentPrincipal.UserId;
            }
            else
            {
                // Email processor cannot resolve
                throw new CBaseException(ErrorMessages.Generic, "Only user can resolve.");
            }

            if (m_taskOwnerUserId != Guid.Empty)
            {
                TaskAssignedUserId = m_taskOwnerUserId;
            }
            else if (m_taskToBeOwnerRoleId != Guid.Empty)
            {
                TaskToBeAssignedRoleId = m_taskToBeOwnerRoleId;
            }

            if (CondRequiredDocTypeId.HasValue && UserPermissionLevel != E_UserTaskPermissionLevel.Manage)
            {
                var associations = DocumentConditionAssociation.DocumentConditionAssociation.GetAssociationsByLoanConditionHeavy(BrokerId, LoanId, TaskId);
                if (associations.Count == 0 || !associations.Any(p => p.DocTypeId == CondRequiredDocTypeId.Value))
                {
                    throw new TaskPermissionException("This condition is missing an association with a required document type. Please associate with the required document type in order to resolve.",
                        "Only users with manage permission can resolve a condition with a required doctype and zero associated documents with required doctype.");
                }
            }

            m_taskDelta.SetStatusChange(m_taskStatus, E_TaskStatus.Resolved);

            // We only want to set the resolution date field if the task was not
            // already resolved or closed.
            bool needToSetResolutionDateField = m_taskStatus == E_TaskStatus.Active;

            m_taskStatus = E_TaskStatus.Resolved;

            if (Save(true) && needToSetResolutionDateField)
            {
                this.SetResolutionDateField();
            }
        }

        private bool? m_conditionFulfilsAssociatedDocs = null;
        public bool ConditionFulfilsAssociatedDocs()
        {
            if (!TaskIsCondition || !CondRequiredDocTypeId.HasValue || !BrokerDB.RetrieveById(BrokerId).IsEDocsEnabled)
            {
                return true;
            }
            if (!m_conditionFulfilsAssociatedDocs.HasValue)
            {
                m_conditionFulfilsAssociatedDocs = AssociatedDocs.Any(p => p.DocTypeId == CondRequiredDocTypeId.Value);
            }

            return m_conditionFulfilsAssociatedDocs.Value;
        }

        public string ConditionFulfilsAssociatedDocs_rep()
        {
            if (!TaskIsCondition || !CondRequiredDocTypeId.HasValue || !BrokerDB.RetrieveById(BrokerId).IsEDocsEnabled)
            {
                return "N/A";
            }
            if (ConditionFulfilsAssociatedDocs())
            {
                return "Yes";
            }
            else
            {
                return "No";
            }
        }

        /// <summary>
        /// Resolves and closes the task with a single audit event. Batch
        /// operations should probably not set the resolution date field.
        /// </summary>
        public void ResolveAndClose()
        {
            if (UserPermissionLevel != E_UserTaskPermissionLevel.Manage &&
                UserPermissionLevel != E_UserTaskPermissionLevel.Close)
                throw new TaskPermissionException("You do not have permission to resolve and close this task.", "Must have manage or close permission to resolve and close task.");
            else if (this.IsResolutionBlocked())
                throw new TaskException(this.ResolutionDenialMessage, "Task did not satisfy resolution block trigger.");

            MarkAsDirty();

            if (CurrentPrincipal != null)
            {
                if (CurrentPrincipal is SystemUserPrincipal)
                {
                    m_assignToOnReactivationUserId = TaskAssignedUserId;
                }

                m_taskClosedUserId = CurrentPrincipal.UserId;
                m_taskResolvedUserId = CurrentPrincipal.UserId;
                ClosingUserFullName = GetUserDisplayName(CurrentPrincipal.UserId);
            }
            else
            {
                // Email processor cannot close
                throw new CBaseException(ErrorMessages.Generic, "Only user can resolve and close");
            }

            if (m_taskOwnerUserId != Guid.Empty)
            {
                TaskAssignedUserId = m_taskOwnerUserId;
            }
            else if (m_taskToBeOwnerRoleId != Guid.Empty)
            {
                TaskToBeAssignedRoleId = m_taskToBeOwnerRoleId;
            }

            if (TaskIsCondition && CondRequiredDocTypeId.HasValue && UserPermissionLevel != E_UserTaskPermissionLevel.Manage)
            {
                var associations = DocumentConditionAssociation.DocumentConditionAssociation.GetAssociationsByLoanConditionHeavy(BrokerId, LoanId, TaskId);
                if (associations.Count == 0 || !associations.Any(p => p.DocTypeId == CondRequiredDocTypeId.Value))
                {
                    throw new TaskPermissionException("This condition is missing an association with a required document type. Please associate with the required document type in order to resolve.",
                        "Only users with manage permission can resolve a condition with a required doctype and zero associated documents with required doctype.");
                }
            }

            m_taskDelta.SetStatusChange(m_taskStatus, E_TaskStatus.Closed);
            m_taskClosedDate = DateTime.Now;

            // We only want to set the resolution date field if the task was not
            // already resolved or closed.
            bool needToSetResolutionDateField = m_taskStatus == E_TaskStatus.Active;

            m_taskStatus = E_TaskStatus.Closed;

            if (Save(true) && needToSetResolutionDateField)
            {
                this.SetResolutionDateField();
            }
        }

        /// <summary>
        /// Closes the task and saves.
        /// </summary>
        /// <param name="lastLoadedVersion">The last loaded version of the task.</param>
        public void Close(string lastLoadedVersion)
        {
            if (Convert.ToBase64String(m_taskRowVersion) != lastLoadedVersion)
                throw new TaskVersionMismatchException();
            Close();
        }

        /// <summary>
        /// Closes the task and saves. Batch operations should use a 
        /// ResolutionDateSetter to aggregate field changes.
        /// </summary>
        public void Close()
        {
            if (UserPermissionLevel != E_UserTaskPermissionLevel.Manage &&
                UserPermissionLevel != E_UserTaskPermissionLevel.Close)
            {
                string userMessage = TaskIsCondition ? "You do not have permission to sign off on this condition." 
                    : "You do not have permission to close this task.";
                throw new TaskPermissionException(userMessage, "Must have manage or close permission to resolve and close task.");
            }
            else if (this.IsResolutionBlocked())
                throw new TaskException(this.ResolutionDenialMessage, "Task did not satisfy resolution block trigger.");

            MarkAsDirty();

            if (CurrentPrincipal != null)
            {
                m_taskClosedUserId = CurrentPrincipal.UserId;
                ClosingUserFullName = GetUserDisplayName(CurrentPrincipal.UserId);
            }
            else
            {
                // Email processor cannot close
                throw new CBaseException(ErrorMessages.Generic, "Only user can close");
            }

            // 4/2/14 gf - opm 170314, enforce this check for resolving and closing.
            if (TaskIsCondition && CondRequiredDocTypeId.HasValue && UserPermissionLevel != E_UserTaskPermissionLevel.Manage)
            {
                var associations = DocumentConditionAssociation.DocumentConditionAssociation.GetAssociationsByLoanConditionHeavy(BrokerId, LoanId, TaskId);
                if (associations.Count == 0 || !associations.Any(p => p.DocTypeId == CondRequiredDocTypeId.Value))
                {
                    throw new TaskPermissionException("This condition is missing an association with a required document type. Please associate with the required document type in order to sign off.",
                        "Only users with manage permission can resolve a condition with a required doctype and zero associated documents with required doctype.");
                }
            }

            m_taskDelta.SetStatusChange(m_taskStatus, E_TaskStatus.Closed);
            m_taskClosedDate = DateTime.Now;

            // We only want to set the resolution date field if the task was not
            // already resolved or closed.
            bool needToSetResolutionDateField = m_taskStatus == E_TaskStatus.Active;

            m_taskStatus = E_TaskStatus.Closed;

            if (Save(true) && needToSetResolutionDateField)
            {
                this.SetResolutionDateField();
            }
        }

        public void Close(Guid brokerId, DateTime closedDate, string closingUser)
        {
            if (UserPermissionLevel != E_UserTaskPermissionLevel.Manage &&
                UserPermissionLevel != E_UserTaskPermissionLevel.Close)
                throw new TaskPermissionException("Must have manage or close permission to resolve and close task.");
            else if (this.IsResolutionBlocked())
                throw new TaskException(this.ResolutionDenialMessage, "Task did not satisfy resolution block trigger.");

            // 4/2/14 gf - opm 170314, enforce this check for resolving and closing.
            if (TaskIsCondition && CondRequiredDocTypeId.HasValue && UserPermissionLevel != E_UserTaskPermissionLevel.Manage)
            {
                var associations = DocumentConditionAssociation.DocumentConditionAssociation.GetAssociationsByLoanConditionHeavy(brokerId, LoanId, TaskId);
                if (associations.Count == 0 || !associations.Any(p => p.DocTypeId == CondRequiredDocTypeId.Value))
                {
                    throw new TaskPermissionException("This condition is missing an association with a required document type. Please associate with the required document type in order to sign off.",
                        "Only users with manage permission can resolve a condition with a required doctype and zero associated documents with required doctype.");
                }
            }

            MarkAsDirty();

            // Conditions can be imported when they are already closed.
            m_taskClosedDate = closedDate;
            m_taskClosedUserId = Guid.Empty;
            m_closingUserFullName = closingUser;

            m_taskDelta.SetStatusChange(m_taskStatus, E_TaskStatus.Closed);

            // We only want to set the resolution date field if the task was not
            // already resolved or closed.
            bool needToSetResolutionDateField = m_taskStatus == E_TaskStatus.Active;

            m_taskStatus = E_TaskStatus.Closed;

            if (Save(true) && needToSetResolutionDateField)
            {
                this.SetResolutionDateField();
            }
        }

        //public void SetClosingDataForImport( DateTime closedDate, string closingUser)
        //{
        //    // Conditions can be imported when they are already closed.
        //    m_taskClosedDate = closedDate;
        //    m_taskClosedUserId = Guid.Empty;
        //    m_closingUserFullName = closingUser;
        //}

        public void Delete(string userName, bool updateLoanStatistics)
        {
            if (TaskIsCondition == false)
            {
                string message = "Cannot delete a task that is not a condition.";
                throw new CBaseException(message, message);
            }
            // 3/12/14 gf - enforce that the user has Manage access for deletion.
            CalculatePermission();
            if (UserPermissionLevel != E_UserTaskPermissionLevel.Manage)
            {
                throw new TaskPermissionException("You do not have permission to delete this condition.", 
                    "Must have manage permission to delete condition.");
            }

            if (CondIsDeleted)
            {
                return;
            }

            CondIsDeleted = true;
            CondDeletedDate = DateTime.Now;
            CondDeletedByUserNm = userName;
            CondRank = 1000; //move them to the end
            m_taskHistory.StoreDeleteEvent();
            Save(updateLoanStatistics); //not sure if this should be here
        }

        /// <summary>
        /// Restore condition 
        /// </summary>
        public void Restore(bool updateLoanStatistics)
        {
            if (m_isNew) throw new CBaseException(ErrorMessages.Generic, "New task should not get restored");

            if (TaskIsCondition == false || CondIsDeleted == false)
            {
                return;
            }

            CondIsDeleted = false;
            CondDeletedByUserNm = "";
            CondDeletedDate = new Nullable<DateTime>();
            m_taskHistory.StoreRestoreEvent();
            Save(updateLoanStatistics);
        }

        public void StoreIncomingEmail(string from, string sendDate, string to, string cc, string subject, string body, bool sendNotifications)
        {

            if (m_isNew) throw new CBaseException(ErrorMessages.Generic, "New task should not get incoming email");

            m_sendNotifications = sendNotifications;

            try
            {
                m_taskHistory.StoreIncomingEmail(from, sendDate, to, cc, subject, body);
                MarkAsDirty();

                Save(true);
            }
            catch (TaskLargeAuditException)
            {
                HashSet<Guid> subscribedUserId = new HashSet<Guid>(TaskSubscription.GetSubscribedUserIds(this.BrokerId, TaskId));
                if (TaskOwnerUserId != Guid.Empty)
                {
                    subscribedUserId.Add(TaskOwnerUserId);
                }
                if (TaskAssignedUserId != Guid.Empty)
                {
                    subscribedUserId.Add(TaskAssignedUserId);
                }

                bool reply = false;
                string name = "";
                foreach (Guid userId in subscribedUserId)
                {
                    SqlParameter[] parameters = {
                                                    new SqlParameter("@EmployeeUserID", userId)
                                                };
                    using (DbDataReader sr = StoredProcedureHelper.ExecuteReader(this.BrokerId, "GetEmployeeNameByUserId", parameters))
                    {
                        if (sr.Read() && sr["Email"].ToString().TrimWhitespaceAndBOM().Equals(from.TrimWhitespaceAndBOM(), StringComparison.OrdinalIgnoreCase))
                        {
                            name = sr["FirstLast"].ToString();
                            reply = true;
                            break;
                        }
                    }
                }

                if (reply && sendNotifications)
                {

                    CBaseEmail email = new CBaseEmail(this.BrokerId)
                    {
                        Subject = "Failed to deliver message to Task " + TaskId,
                        To = from,
                        From = BrokerDB.TaskAliasEmailAddress
                    };

                    StringBuilder sb = new StringBuilder();
                    sb.Append(name);
                    sb.AppendLine(",");
                    sb.AppendLine();
                    sb.AppendLine("We were not able to store your email in task " + TaskId + " due to the size of the audit.");
                    sb.AppendLine();
                    sb.AppendLine("The LendingQB Team");
                    sb.AppendLine();
                    sb.AppendLine("------------------------------------------------------------");
                    sb.AppendLine("From: " + from);
                    sb.AppendLine("Sent: " + sendDate);
                    sb.AppendLine("To: " + to);
                    sb.AppendLine("Subject: " + subject);
                    sb.AppendLine();
                    sb.Append(body);
                    email.Message = sb.ToString();
                    email.Send();
                }
            }
        }


        // sends out an email based on the input and saves
        public void EMail(string to, string CC, string body)
        {
            Save(true);
        }


        // turns the task into a condition
        public void MakeCondition()
        {
            TaskIsCondition = true;
        }


        public bool HasEmailEntry
        {
            get
            {
                return m_taskHistory.HasEmailEntry();
            }
        }

        public string MostRecentEmailContent
        {
            get
            {
                return m_taskHistory.GetMostRecentEmailContent().Replace("<br />", Environment.NewLine).Replace("<br/>", Environment.NewLine);  // sk 106415
            }
        }

        //use by email processor        might need to change to email alias, task id 
        public static Task RetrieveWithoutPermissionCheck(Guid brokerId, string taskId)
        {
            Task ret = new Task();
            ret.EnforcePermissions = false;
            ret.Retrieve(taskId, brokerId);
            return ret;
        }

        public static Task Retrieve(Guid brokerId, string taskId, Dictionary<string, string> loanDateCache = null, TaskPermissionProcessor permissionProcessor = null, Dictionary<int, ConditionCategory> conditionCategories = null, PermissionLevelData permissionLevels = null)
        {
            Task ret = new Task();
            ret.PermissionProcessor = permissionProcessor;

            if (conditionCategories != null)
            {
                ret.m_categoryDescription = conditionCategories;
            }

            if (permissionLevels != null)
            {
                ret.SetPermissionLevelData(permissionLevels);
            }

            ret.Retrieve(taskId, brokerId, loanDateCache);
            return ret;
        }

        public static Task Retrieve(Guid brokerId, string taskId, E_PortalMode portalMode, Guid pmlBrokerId)
        {
            Task ret = new Task();
            ret.Retrieve(taskId, brokerId, portalMode, pmlBrokerId);
            return ret;
        }

        private static Task Load(DbDataReader reader)
        {
            return Load(reader, null, null);
        }

        private static Task Load(DbDataReader reader, Dictionary<int, ConditionCategory> categories, Dictionary<string, string> loanDateCache, PermissionLevelData permissionLevels = null)
        {
            // This internal load will do check for access--
            // assuming listing method does appropriate filtering based on need.
            Task ret = new Task();

            if (permissionLevels != null)
            {
                ret.SetPermissionLevelData(permissionLevels);
            }

            ret.LoadTask(reader, categories, loanDateCache);
            return ret;
        }

        public static string ToCSV(Guid brokerId, string taskId)
        {
            Task task = Retrieve(brokerId, taskId);
            return ToCSV(brokerId, task);
        }

        public static string ToCSV(Guid brokerId, Task task)
        {
            return string.Join(",", new string[] { task.TaskId, FormatForCsv(task.TaskSubject), task.TaskStatus_rep, FormatForCsv(task.LoanNumCached), FormatForCsv(task.BorrowerNmCached), task.TaskDueDate == null ? "" : ((DateTime)task.TaskDueDate).ToShortDateString(), task.TaskFollowUpDate == null ? "" : ((DateTime)task.TaskFollowUpDate).ToShortDateString(), task.TaskLastModifiedDate == null ? "" : task.TaskLastModifiedDate.ToString(), FormatForCsv(task.AssignedUserFullName), FormatForCsv(task.OwnerFullName) });
        }

        private static string FormatForCsv(string content)
        {
            if (string.IsNullOrEmpty(content)) return content;

            if (content.IndexOf(',') != -1 || content.IndexOf('\"') != -1
                        || content.IndexOf('\r') != -1 || content.IndexOf('\n') != -1)
            {
                return "\"" + content.Replace("\"", "\"\"") + "\"";
            }

            return content;
        }

        public void LogEmail(string from, DateTime date, string to, string cc, string subject, string body)
        {

        }

        #region Static Methods

        #region AM

        /// <summary>
        /// Return all (non-delete) tasks in loan
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="loanId"></param>
        /// <returns></returns>
        public static List<Task> GetTasksInLoan(Guid brokerId, Guid loanId)
        {
            // return all the tasks (which are not delete) in a loan file
            // DO NOT USE THIS METHOD for UI--it bypasses permission check.

            List<Task> taskList = new List<Task>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),                                            
                                            new SqlParameter("@LoanId", loanId),                                            
                                            new SqlParameter("@IncludeOpen", true),
                                            new SqlParameter("@IncludeClosed", true)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_ListTasksByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    taskList.Add(Task.Load(reader));
                }
            }

            return taskList;
        }

        public static List<Task> GetTasksByStoredProcedureListTasksByBrokerId(Guid brokerId, params SqlParameter[] parameters)
        {
            List<Task> taskList = new List<Task>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_ListTasksByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    taskList.Add(Task.Load(reader));
                }
            }

            return taskList;
        }

        // Output: All Due tasks in the loan file user has read access for (either work-on or Manage)
        public static List<Task> GetDueTasksInLoan(Guid brokerId, Guid loanId, Guid userId)
        {
            List<Task> taskList = new List<Task>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@LoanId", loanId),
                                            new SqlParameter("@UserId", userId),
                                            new SqlParameter("@IncludeOpen", true),
                                            new SqlParameter("@IncludeClosed", true)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_ListTasksByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    taskList.Add(Task.Load(reader));
                }
            }


            DateTime today = DateTime.Today;
            TaskPermissionProcessor processor = new TaskPermissionProcessor((AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal);

            taskList.RemoveAll(task => (task.TaskDueDate.HasValue ? (task.TaskDueDate > today) : true) || processor.Resolve(task) == E_UserTaskPermissionLevel.None);

            return taskList;
        }

        // Output: All open tasks assigned to a user w/status = Active | Resolved. If userid is empty return tasks assigned to any user.
        public static List<Task> GetOpenTasksByAssignedEmployeeId(Guid userId) // Need to Add BrokerId
        {
            List<Task> taskList = new List<Task>();

            Guid brokerId = ((AbstractUserPrincipal) System.Threading.Thread.CurrentPrincipal).BrokerId;
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@UserId", userId),
                                            new SqlParameter("@IncludeOpen", true),
                                            new SqlParameter("@IncludeClosed", false)

                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_ListTasksByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    Task newTask = Task.Load(reader);
                    if (newTask.IsTemplate == false)
                        taskList.Add(newTask);
                }
            }

            TaskPermissionProcessor processor = new TaskPermissionProcessor((AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal);
            taskList.RemoveAll(task => processor.Resolve(task) == E_UserTaskPermissionLevel.None);
            return taskList;
        }


        public static List<Task> GetOpenTasksByAssignedUserId(Guid userId, Guid brokerId, string sortField, string sortDirection, E_TaskStatus statusFilter)
        {
            // Calling the old way so UI can be wired while this is implemennted
            return GetOpenTasksByAssignedUserId(userId, brokerId, sortField, sortDirection);
        }

        // Output: All open tasks assigned to a user w/status = Active | Resolved. If userid is empty return tasks assigned to any user.
        // For pipeline, we reduce returned result.
        public static List<Task> GetOpenTasksByAssignedUserId(Guid userId, Guid brokerId, string sortField, string sortDirection)
        {
            List<Task> taskList = new List<Task>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@UserId", userId),
                                            new SqlParameter("@OrderBy", sortField),
                                            new SqlParameter("@Desc", sortDirection == "DESC")

                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_ListTasksByBrokerIdUserIdForPipeline", parameters))
            {
                while (reader.Read())
                {
                    Task newTask = Task.Load(reader);
                    if (newTask.IsTemplate == false)
                        taskList.Add(newTask);
                }
            }

            TaskPermissionProcessor processor = new TaskPermissionProcessor((AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal);
            taskList.RemoveAll(task => processor.Resolve(task) == E_UserTaskPermissionLevel.None);
            return taskList;
        }

        public static IReadOnlyDictionary<string, string> GetAllTaskResolutionBlockTriggers(Guid brokerId, Guid loanId)
        {
            Dictionary<string, string> idsToTriggers = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

            SqlParameter[] parameters = new[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", loanId),
            };

            using (var conn = DbAccessUtils.GetConnection(DataSrc.LOShare))
            using (IDataReader reader = StoredProcedureDriverHelper.ExecuteReader(
                conn,
                null,
                StoredProcedureName.Create("TASK_GetAllResolutionBlockTriggersByLoanId").Value,
                parameters,
                TimeoutInSeconds.Default))
            {
                while (reader.Read())
                {
                    idsToTriggers.Add((string)reader["TaskId"], (string)reader["ResolutionBlockTriggerName"]);
                }
            }

            return idsToTriggers;
        }

        /// <summary>
        /// Ensures user can edit/view the hidden data.
        /// </summary>
        /// <param name="principal">The user principal.</param>
        /// <returns>A boolean indicating whether the user can view hidden information.</returns>
        public static bool CanUserViewHiddenInformation(AbstractUserPrincipal principal)
        {
            return principal.HasPermission(Permission.CanViewHiddenInformation)
                || principal.HasPermission(Permission.CanModifyLoanPrograms);
        }

        public static List<Task> GetOpenTasksByAssignedUserId_new(Guid userId, Guid brokerId, string sortField, string sortDirection)
        {
            // We hit the DB in batch to load because it is very possilbe that results will return that the user has no access to.
            const int resultCap = 50;
            const int batchSize = 100;

            List<Task> taskList = new List<Task>();

            TaskPermissionProcessor processor = new TaskPermissionProcessor((AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal);
            bool doneReading = false;
            int escapeCount = 10;
            int rowCount = 0;
            while (doneReading == false)
            {
                SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@UserId", userId),
                                            new SqlParameter("@OrderBy", sortField),
                                            new SqlParameter("@Desc", sortDirection == "DESC"),
                                            new SqlParameter("@StartRow", rowCount )
                                        };


                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_ListTasksByBrokerIdUserIdForPipelineInBatch", parameters))
                {
                    if (reader.Read() == false)
                    {
                        // No records remain to check
                        Tools.LogInfo("Pipeline: read produced no entries");
                        doneReading = true;
                        break;
                    }

                    do
                    {
                        Task newTask = Task.Load(reader);
                        if (newTask.IsTemplate == false && processor.Resolve(newTask) != E_UserTaskPermissionLevel.None)
                        {
                            taskList.Add(newTask);
                            if (taskList.Count >= resultCap)
                            {
                                // We found enough to display

                                Tools.LogInfo("Pipeline: found enough: " + taskList.Count);
                                doneReading = true;


                                break;
                            }
                        }
                    }
                    while (reader.Read());
                }

                Tools.LogInfo("Pipeline: entries read: " + taskList.Count);

                if (--escapeCount == 0)
                {
                    Tools.LogInfo("Pipeline: Could not finish after 10 tries");
                    break;
                }

                rowCount += batchSize;
            }

            return taskList;
        }

        public static Task GetDuplicateCopyOfTask(Guid brokerId, Guid loanId, Task sourceTask, Dictionary<string, string> loanDateCacheDest, Dictionary<string, string> loanDateCacheSrc, PermissionLevelData permissionLevels, Dictionary<int, ConditionCategory> conditionCategories)
        {
            if (loanDateCacheSrc != null)
                sourceTask.m_loanDates = loanDateCacheSrc;

            Task task_dest = new Task(loanId, brokerId);
            task_dest.m_IsDuplicateTask = true;
            task_dest.EnforcePermissions = false;

            if (loanDateCacheDest != null)
            {
                task_dest.m_loanDates = loanDateCacheDest;
            }

            if (permissionLevels != null)
            {
                task_dest.SetPermissionLevelData(permissionLevels);
            }

            if (conditionCategories != null)
            {
                task_dest.m_categoryDescription = conditionCategories;
            }

            task_dest.TaskSubject = sourceTask.TaskSubject;
            task_dest.SetHistoryFromOtherTaskForCopy(sourceTask); // OPM 136559

            task_dest.TaskStatus = sourceTask.TaskStatus;
            task_dest.TaskResolvedUserId = sourceTask.TaskResolvedUserId;
            task_dest.AssignToOnReactivationUserId = sourceTask.AssignToOnReactivationUserId;
            task_dest.BorrowerNmCached = sourceTask.BorrowerNmCached;

            if (sourceTask.TaskToBeAssignedRoleId.HasValue)
            {
                task_dest.TaskToBeAssignedRoleId = sourceTask.TaskToBeAssignedRoleId;
            }
            if (sourceTask.TaskAssignedUserId != Guid.Empty)  //dont clear out role name if its guid empty
            {
                task_dest.AddFullNameForUserId(sourceTask.TaskAssignedUserId, sourceTask.AssignedUserFullName);
                task_dest.TaskAssignedUserId = sourceTask.TaskAssignedUserId;
            }

            task_dest.TaskFollowUpDate = sourceTask.TaskFollowUpDate;
            task_dest.TaskSubject = sourceTask.TaskSubject;
            task_dest.TaskDueDateLocked = sourceTask.TaskDueDateLocked;
            task_dest.TaskDueDate = sourceTask.TaskDueDate;
            task_dest.CondCategoryId = sourceTask.CondCategoryId;
            task_dest.CondRank = sourceTask.CondRank;
            task_dest.CondDeletedDate = sourceTask.CondDeletedDate;
            task_dest.CondDeletedByUserNm = sourceTask.CondDeletedByUserNm;
            task_dest.CondIsDeleted = sourceTask.CondIsDeleted;
            task_dest.CondInternalNotes = sourceTask.CondInternalNotes;
            task_dest.TaskIsCondition = sourceTask.TaskIsCondition;
            task_dest.CondIsHidden = sourceTask.CondIsHidden;
            if (sourceTask.TaskToBeOwnerRoleId.HasValue)
            {
                task_dest.TaskToBeOwnerRoleId = sourceTask.TaskToBeOwnerRoleId;
            }
            if (sourceTask.TaskOwnerUserId != Guid.Empty)   //dont clear out the role name if it was set and owner user id is empty
            {
                task_dest.AddFullNameForUserId(sourceTask.TaskOwnerUserId, sourceTask.OwnerFullName);
                task_dest.TaskOwnerUserId = sourceTask.TaskOwnerUserId;
            }
            task_dest.TaskCreatedByUserId = sourceTask.TaskCreatedByUserId;
            task_dest.TaskDueDateCalcField = sourceTask.TaskDueDateCalcField;
            task_dest.TaskDueDateCalcDays = sourceTask.TaskDueDateCalcDays;
            task_dest.TaskPermissionLevelId = sourceTask.TaskPermissionLevelId;

            if (sourceTask.TaskClosedUserId.HasValue && sourceTask.TaskClosedUserId.Value != Guid.Empty)
            {
                task_dest.AddFullNameForUserId(sourceTask.TaskClosedUserId.Value, sourceTask.ClosingUserFullName);
            }

            task_dest.TaskClosedUserId = sourceTask.TaskClosedUserId;
            task_dest.TaskClosedDate = sourceTask.TaskClosedDate;

            task_dest.ResolutionBlockTriggerName = sourceTask.ResolutionBlockTriggerName;
            task_dest.resolutionDenialMessage = sourceTask.resolutionDenialMessage;
            task_dest.ResolutionDateSetterFieldId = sourceTask.ResolutionDateSetterFieldId;

            return task_dest;
        }

        /// <summary>
        /// Set task assignment and ownership using as per roles in loan assignments. OPM 70088
        /// </summary>
        /// <param name="task"></param>
        public static void SetTaskRoles(Guid brokerId, Task task, LoanAssignmentContactTable assignments, Dictionary<Guid, Guid> empIdUserIdMap)
        {
            foreach (var assignedEmployee in assignments.Items)
            {
                Guid roleId = assignedEmployee.RoleId;
                if (task.TaskToBeAssignedRoleId != Guid.Empty && task.TaskToBeAssignedRoleId == roleId)
                {
                    if (empIdUserIdMap.ContainsKey(assignedEmployee.EmployeeId) == false)
                    {
                        EmployeeDB db = new EmployeeDB(assignedEmployee.EmployeeId, brokerId);
                        db.Retrieve();
                        empIdUserIdMap.Add(assignedEmployee.EmployeeId, db.UserID);
                    }

                    task.TaskAssignedUserId = empIdUserIdMap[assignedEmployee.EmployeeId];
                }

                if (task.TaskToBeOwnerRoleId != Guid.Empty && task.TaskToBeOwnerRoleId == roleId)
                {
                    if (empIdUserIdMap.ContainsKey(assignedEmployee.EmployeeId) == false)
                    {
                        EmployeeDB db = new EmployeeDB(assignedEmployee.EmployeeId, brokerId);
                        db.Retrieve();
                        empIdUserIdMap.Add(assignedEmployee.EmployeeId, db.UserID);
                    }
                    task.TaskOwnerUserId = empIdUserIdMap[assignedEmployee.EmployeeId];
                }
            }
        }

        public static IEnumerable<string> DuplicateTasksToLoan(Guid srcLoanId, Guid destLoanId, AbstractUserPrincipal principal, bool duplicateOnlyConditions, bool updateSortOrder)
        {
            //OPM 77649: If a PML user is calling this, it's because they're trying to make a new loan
            //from the PML loan template; in that case, we want to let them make a full copy of the tasks.
            bool isPmlUser = principal.Type == "P";
            bool enforceTaskPermissions = !isPmlUser;

            return DuplicateTasksToLoan(principal.BrokerId, srcLoanId, destLoanId, principal.EmployeeId, duplicateOnlyConditions, enforceTaskPermissions, updateSortOrder);
        }

        /// <summary>
        /// Takes 2 loan ids and returns the app id mapping from one loan to the other.
        /// </summary>
        /// <param name="srcLoanId"></param>
        /// <param name="AppId"></param>
        /// <returns></returns>
        private static Dictionary<Guid, Guid> GetNewAppIdMapping(Guid srcLoanId, Guid dstLoanId)
        {
            CPageData srcPageData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(srcLoanId, typeof(Task));
            CPageData dstPageData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(dstLoanId, typeof(Task));

            srcPageData.AllowLoadWhileQP2Sandboxed = true; // for lead -> loan conversion with template
            srcPageData.InitLoad();
            dstPageData.AllowLoadWhileQP2Sandboxed = true;
            dstPageData.InitLoad();


            Dictionary<Guid, Guid> mapping = new Dictionary<Guid, Guid>();

            for (int i = 0; i < srcPageData.nApps; i++)
            {
                mapping.Add(srcPageData.GetAppData(i).aAppId, dstPageData.GetAppData(i).aAppId);
            }

            return mapping;
        }

        /// <summary>
        /// Duplicates all of the tasks from the loan identified by srcLoanId in to the loan identified by destLoanId
        /// Normally follows employee access restrictions, but if the employee is a PML user all loan tasks are copied
        /// </summary>
        /// <param name="brokerId">The broker to which these loans belong</param>
        /// <param name="srcLoanId">The source loan to copy from</param>
        /// <param name="destLoanId">The destination loan to copy to</param>
        /// <param name="employeeId">The id of the employee requesting the duplication</param>
        /// <param name="duplicateOnlyConditions">True if only conditions should be duplicated; false if both conditions and tasks should be duplicated</param>
        /// <param name="isEnforcePermissionOnSourceLoan">Pass false if you do not want to enforce permission on src file. THis should only pass false during copy from loan template</param>
        /// <returns>An enumeration of task ids that were created in the destination loan</returns>
        public static IEnumerable<string> DuplicateTasksToLoan(Guid brokerId, Guid srcLoanId, Guid destLoanId, Guid employeeId, bool duplicateOnlyConditions, bool isEnforcePermissionOnSourceLoan, bool updateSortOrder)
        {
            return DuplicateTasksToLoan(brokerId, srcLoanId, destLoanId, employeeId, duplicateOnlyConditions, isEnforcePermissionOnSourceLoan, updateSortOrder, true);
        }
        /// <summary>
        /// Duplicates all of the tasks from the loan identified by srcLoanId in to the loan identified by destLoanId
        /// Normally follows employee access restrictions, but if the employee is a PML user all loan tasks are copied
        /// </summary>
        /// <param name="brokerId">The broker to which these loans belong</param>
        /// <param name="srcLoanId">The source loan to copy from</param>
        /// <param name="destLoanId">The destination loan to copy to</param>
        /// <param name="employeeId">The id of the employee requesting the duplication</param>
        /// <param name="duplicateOnlyConditions">True if only conditions should be duplicated; false if both conditions and tasks should be duplicated</param>
        /// <param name="isEnforcePermissionOnSourceLoan">Pass false if you do not want to enforce permission on src file. THis should only pass false during copy from loan template</param>
        /// <param name="includeClosedTask">Whether to include closed loans.</param>
        /// <returns>An enumeration of task ids that were created in the destination loan</returns>
        public static IEnumerable<string> DuplicateTasksToLoan(Guid brokerId, Guid srcLoanId, Guid destLoanId, Guid employeeId, bool duplicateOnlyConditions, bool isEnforcePermissionOnSourceLoan, bool updateSortOrder, bool includeClosedTask)
        {
            var taskTriggerAssociations = TaskTriggerQueueItem.GetTaskTriggerAssociationsFor(brokerId, srcLoanId)
                .ToDictionary(p => p.TriggerName);

            Dictionary<Guid, Guid> srcToDstAppIdMapping = null;

            if (taskTriggerAssociations.Count > 0)
            {
                srcToDstAppIdMapping = GetNewAppIdMapping(srcLoanId, destLoanId);
            }

            List<Task> tasks;
            if (isEnforcePermissionOnSourceLoan)
            {
                tasks = Task.GetTasksByEmployeeAccess(brokerId, srcLoanId, employeeId);
            }
            else
            {
                tasks = Task.GetTasksInLoan(brokerId, srcLoanId);
            }

            var ids = new List<string>();

            // Some cache to reduce the DB loading here.
            var loanDateCacheSrc = new Dictionary<string, string>(tasks.Count);
            var loanDateCacheDest = new Dictionary<string, string>();
            var userIdCache = new Dictionary<Guid, Guid>();
            var assignments = new LoanAssignmentContactTable(brokerId, destLoanId);
            var permissionLevels = PermissionLevelData.Retrieve(brokerId);
            var conditionCategories = ConditionCategory.GetCategories(brokerId)
                .ToDictionary(c => c.Id);

            const int maxAttempts = 3;
            var oldIdAndNewCondition = new List<Tuple<string, Task>>();
            var taskIds = new List<string>(); //only task, not conditions

            foreach (Task task in tasks)
            {
                if (task.TaskStatus == E_TaskStatus.Closed && !includeClosedTask)
                {
                    continue;
                }
                else if (duplicateOnlyConditions && !task.TaskIsCondition)
                {
                    continue;
                }

                try
                {
                    for (int nAttempts = 0; nAttempts < maxAttempts; ++nAttempts)
                    {
                        try
                        {
                            Task t = Task.GetDuplicateCopyOfTask(brokerId, srcLoanId, task, loanDateCacheDest, loanDateCacheSrc, permissionLevels, conditionCategories);
                            t.LoanId = destLoanId;

                            SetTaskRoles(brokerId, t, assignments, userIdCache);

                            t.Save(false);

                            ids.Add(t.TaskId);

                            if (taskTriggerAssociations.ContainsKey(task.TaskId))
                            {
                                TaskTriggerQueueItem existing = taskTriggerAssociations[task.TaskId];
                                if (srcToDstAppIdMapping.ContainsKey(existing.aAppId))
                                {
                                    TaskTriggerQueueItem association = new TaskTriggerQueueItem(existing.TaskTemplateId, t.TaskId, existing.BrokerId, destLoanId, srcToDstAppIdMapping[existing.aAppId], existing.RecordId);
                                    TaskTriggerTemplate.LinkTaskToTaskTriggerTemplate(t, association);
                                }
                            }

                            if (t.TaskIsCondition)
                            {
                                oldIdAndNewCondition.Add(Tuple.Create(task.TaskId, t));
                            }
                            else
                            {
                                taskIds.Add(t.TaskId);
                            }

                            // Create trigger associations
                            IReadOnlyDictionary<TaskTriggerType, TaskTriggerAssociation> sourceTriggers = TaskTriggerAssociation.GetTaskTriggerAssociations(task);
                            foreach (TaskTriggerAssociation trigger in sourceTriggers.Values)
                            {
                                TaskTriggerAssociation.CreateNewAssociation(t, trigger.TriggerType, trigger.TriggerName);
                            }

                            break;
                        }
                        catch (SqlException ex)
                        {
                            Tools.LogWarning("Duplicating loan tasks: Attempt #" + nAttempts + " failed.", ex);

                            if (nAttempts == maxAttempts)
                            {
                                Tools.LogError(ex);
                                throw new CBaseException(ErrorMessages.Generic, "Failed from loan " + srcLoanId + " for employeeId " + employeeId.ToString() + ".");
                            }
                        }
                    }
                }
                catch (CBaseException exc)
                {
                    throw new CBaseException(ErrorMessages.Generic, string.Format("Failed to duplicate loan tasks. Source - {0} , Destination {1}, EmployeeId {2}. ErrorMsg: {3} .Stack Trace {4}", srcLoanId, destLoanId, employeeId, exc.Message, exc.StackTrace));
                }
            }

            if (updateSortOrder && oldIdAndNewCondition.Count > 0)
            {
                ConditionOrderManager srcOrderManager = ConditionOrderManager.Load(srcLoanId);

                var oldConditionIdToNewConditionId = oldIdAndNewCondition.ToDictionary(tuple => tuple.Item1, tuple => tuple.Item2.TaskId);
                var translatedOrderManager = srcOrderManager.MapToNewConditionIds(oldConditionIdToNewConditionId);

                var newConditionIdToNewCondition = oldIdAndNewCondition.ToDictionary(tuple => tuple.Item2.TaskId, tuple => tuple.Item2);
                var newConditionsSorted = SortConditionTaskImpl(translatedOrderManager, newConditionIdToNewCondition);
                var newSortedConditionIds = newConditionsSorted.Select(t => t.TaskId);

                var destOrderManager = ConditionOrderManager.Load(destLoanId);
                ConditionOrderManager.StoreUserSort(
                    destLoanId,
                    destOrderManager.SortedTaskIds.Concat(newSortedConditionIds));

                int expectedCount = ids.Count;
                // Order matters for the import from template button and the create from template cares about all task. 
                ids = taskIds.Concat(newSortedConditionIds).ToList();
                Tools.Assert(expectedCount == ids.Count, "We did not end up with the same number of duplicated conditions. Theres a bug Need to fix.");
            }

            TaskUtilities.EnqueueTasksDueDateUpdate(brokerId, destLoanId);
            return ids;
        }

        #endregion

        #region VM

        public static List<string> ListValidDateFields()
        {
            return ListValidDateFields(true);
        }

        // Gives me a list of the date fields for due date calculation
        public static List<string> ListValidDateFields(bool enforcePermissions)
        {
            List<string> fields = null;
            fields = (from p in ListValidDateFieldParameters(enforcePermissions) select p.Id).ToList<string>();
            return fields;
        }

        public static bool CanViewField(AbstractUserPrincipal principal, string fieldId)
        {
            if (fieldId == null)
            {
                fieldId = "";
            }
            if (LoanFieldSecurityManager.IsSecureField(fieldId) == false)
            {
                return true;
            }
            else if (principal.Type == "P")
            {
                return false;
            }
            else
            {
                //BrokerUserPrincipal brokerUser = principal as BrokerUserPrincipal;
                //if (brokerUser == null) throw new CBaseException(ErrorMessages.Generic, "Can only check permission for B user");

                return LoanFieldSecurityManager.CanReadField(fieldId, principal);
            }
        }

        public static List<SecurityParameter.Parameter> ListValidDateFieldParameters()
        {
            return ListValidDateFieldParameters(true);
        }

        public static IEnumerable<SecurityParameter.Parameter> ListValidResolutionDateSetterParameters()
        {
            return ListValidResolutionDateSetterParameters(true);
        }

        public static IEnumerable<SecurityParameter.Parameter> ListValidResolutionDateSetterParameters(bool enforcePermissions)
        {
            var currentPrincipal = PrincipalFactory.CurrentPrincipal;
            if (enforcePermissions && currentPrincipal == null)
            {
                return new List<SecurityParameter.Parameter>();
            }

            var parameterReporting = ParameterReporting.RetrieveSystemParameters();

            E_PageDataClassType classType;

            var validFields = parameterReporting.Parameters
                .Where(p => p.Type == SecurityParameter.SecurityParameterType.Date)
                .Where(p => !string.IsNullOrEmpty(p.CategoryName))
                .Where(p => p.Id != p.FriendlyName)
                .Where(p => PageDataUtilities.GetPageDataClassType(p.Id, out classType) ? classType == E_PageDataClassType.CBasePage : false)
                .Where(p => !ConstApp.TaskResolutionDateSetterFieldIdBlacklist.Contains(p.Id))
                .Where(p => !enforcePermissions || CanViewField(currentPrincipal, p.Id))
                .OrderBy(p => p.CategoryName)
                .ThenBy(p => p.FriendlyName);

            return validFields;
        }

        public static List<SecurityParameter.Parameter> ListValidDateFieldParameters(bool enforcePermissions)
        {
            List<SecurityParameter.Parameter> ret = new List<SecurityParameter.Parameter>();
            ParameterReporting pR = ParameterReporting.RetrieveSystemParameters();
            
            // This list comes from Task Frontend Spec 2.1.2
            HashSet<string> validCategories = new HashSet<string>(){ "Custom Fields", "Funding", "HMDA Auditing", "Investor Rate Lock", "Loan Information", "Loan Status", "Purchase Advice"};

            AbstractUserPrincipal currentPrincipal = System.Threading.Thread.CurrentPrincipal as AbstractUserPrincipal;
            if (enforcePermissions && currentPrincipal == null)
            {
                return ret; // No date field parameters are visible to an invalid principal
            }

            var dateFields = from p in pR.Parameters
                             where p.Type == SecurityParameter.SecurityParameterType.Date
                             where validCategories.Contains(p.CategoryName)
                             where (!enforcePermissions || CanViewField(currentPrincipal, p.Id) )
                             select p;

            foreach (var p in dateFields)
            {
                if (p.Id == "aBNextBirthday" || p.Id == "aCNextBirthday" || p.Id == "sLoanOfficerRateLockedD" || p.Id == "sLastActiveD"
                    || p.Id == "sApprovDTime" || p.Id == "sClearToCloseDTime" || p.Id == "sClosedDTime" || p.Id == "sCreatedDTime"
                    || p.Id == "sDocsBackDTime" || p.Id == "sDocsDTime" || p.Id == "sEstCloseDTime"
                    || p.Id == "sFinalDocsDTime" || p.Id == "sFinalUnderwritingDTime" || p.Id == "sFundDTime"
                    || p.Id == "sFundingConditionsDTime" || p.Id == "sLeadDTime" || p.Id == "sOpenedDTime" || p.Id == "sPreApprovDTime"
                    || p.Id == "sPreQualDTime" || p.Id == "sProcessingDTime" || p.Id == "sRecordedDTime" || p.Id == "sSubmitDTime"
                    || p.Id == "sSchedFundDTime" || p.Id == "sLoanSubmittedDTime" || p.Id == "sUnderwritingDTime"
                    || p.Id == "sMortgageLoanCommitmentPrepareDate" || p.Id == "sNewEdocsUploadedD" || p.Id == "PoolCommitmentD"
                    || p.Id == "PoolCommitmentExpD" || p.Id == "sOnHoldDTime" || p.Id == "sCanceledDTime" || p.Id == "sRejectDTime"
                    || p.Id == "sSuspendedDTime" || p.Id == "sShippedToInvestorDTime" || p.Id == "sSuspendedByInvestorDTime"
                    || p.Id == "sCondSentToInvestorDTime" || p.Id == "sLPurchaseDTime" || p.Id == "sPreProcessingDTime" 
                    || p.Id == "sDocumentCheckDTime" || p.Id == "sDocumentCheckFailedDTime" || p.Id == "sPreUnderwritingDTime" 
                    || p.Id == "sConditionReviewDTime" || p.Id == "sPreDocQCDTime" || p.Id == "sDocsOrderedDTime" 
                    || p.Id == "sDocsDrawnDTime" || p.Id == "sReadyForSaleDTime" || p.Id == "sSubmittedForPurchaseReviewDTime" 
                    || p.Id == "sInPurchaseReviewDTime" || p.Id == "sPrePurchaseConditionsDTime" 
                    || p.Id == "sSubmittedForFinalPurchaseDTime" || p.Id == "sInFinalPurchaseReviewDTime" 
                    || p.Id == "sClearToPurchaseDTime" || p.Id == "sPurchasedDTime" || p.Id == "sCounterOfferDTime" 
                    || p.Id == "sWithdrawnDTime" || p.Id == "sArchivedDTime" || p.Id == "sUDNOrderedD" || p.Id == "sUDNDeactivatedD"
                    || p.Id == "sLeadCanceledD" || p.Id == "sLeadDeclinedD" || p.Id == "sQCCompDateTime" || p.Id == "sInterestRateSetD"
                    || p.Id == "sHmdaApplicationDate"
                    )
                {
                    // 6/17/2011 dd - These are the fields that appear in custom report but not available in datalayer.
                    continue;
                }
                else
                {
                    ret.Add(p);
                }
            }

            return ret;
        }

        // Gives a list of all the users who can be assigned to that task
        // Since this is necessary for tasks template also, I will be passing in the loan or template id as input. Same list can be used to pick the owner.
        List<Guid> ListAssignableUsers(string taskId)
        {
            // Might end up being list of users assigned to loanfile which is better.
            return null;
        }

        // All the task permission levels belonging to a lender have task can be permission set to no.
        IEnumerable<PermissionLevel> ListTaskPermissionLevels(Guid brokerId)
        {
            return PermissionLevel.RetrieveAll(brokerId);
        }

        #endregion

        #region MP

        // Get a list of open tasks for a loan
        // USE: GetOpenTasksByAssignedEmployeeId

        /// <summary>
        /// Get a list of all tasks for a loan
        /// </summary>
        /// <param name="brokerId">We have an index on the brokerId</param>
        /// <param name="loanId">The loan to get the tasks from</param>
        /// <param name="userId">This doesn't actually do anything</param>
        /// <returns>A list of tasks for the loan</returns>
        public static List<Task> GetTasksByEmployeeAccess(Guid brokerId, Guid loanId, Guid userId)
        {
            List<Task> taskList = new List<Task>();
            Dictionary<string, string> loanDateCache = new Dictionary<string, string>();
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@LoanId", loanId),
                                            new SqlParameter("@IncludeOpen", true),
                                            new SqlParameter("@IncludeClosed", true)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_ListTasksByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    try
                    {
                        taskList.Add(Task.Load(reader, null, loanDateCache));
                    }
                    catch (TaskPermissionException)
                    {
                        continue; // do not show this task in the task list
                    }
                }
            }

            TaskPermissionProcessor processor = new TaskPermissionProcessor((AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal);
            taskList.RemoveAll(task => processor.Resolve(task) == E_UserTaskPermissionLevel.None);

            return taskList;
        }

        public static IEnumerable<Task> GetTasksByEmployeeAccess(Guid loanId, AbstractUserPrincipal principal, IEnumerable<string> taskIds, TaskPermissionProcessor permissionProcessor)
        {
            var tasks = new List<Task>();

            if (!taskIds.Any())
            {
                return tasks;
            }

            var idXml = new XElement("root", taskIds.Select(id => new XElement("id", id)))
                .ToString(SaveOptions.DisableFormatting);
            var loanDateCache = new Dictionary<string, string>();
            var conditionCategories = ConditionCategory.GetCategories(principal.BrokerId)
                .ToDictionary(c => c.Id);
            var permissionLevels = PermissionLevelData.Retrieve(principal.BrokerId);

            SqlParameter[] parameters = 
            {
                new SqlParameter("@BrokerId", principal.BrokerId),
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@IdXml", idXml)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "TASK_ListTasksByBrokerIdLoanIdTaskId", parameters))
            {
                while (reader.Read())
                {
                    try
                    {
                        tasks.Add(Task.Load(reader, conditionCategories, loanDateCache));
                    }
                    catch (TaskPermissionException)
                    {
                        continue; // do not show this task in the task list
                    }
                }
            }

            if (permissionProcessor == null)
            {
                permissionProcessor = new TaskPermissionProcessor(principal);
            }

            tasks.RemoveAll(task => permissionProcessor.Resolve(task) == E_UserTaskPermissionLevel.None);

            // Return the tasks in the same order as the ids were initially passed in.
            var orderedTasks = new List<Task>();
            var taskIdToTask = tasks.ToDictionary(t => t.TaskId);

            foreach (var taskId in taskIds)
            {
                Task task;
                if (taskIdToTask.TryGetValue(taskId, out task))
                {
                    orderedTasks.Add(task);
                }
            }

            return orderedTasks;
        }

        public static List<Tuple<string, Guid, Guid>> GetRoleUsersForLoan(Guid brokerId, Guid loanId)
        {
            var roles = new List<Tuple<string, Guid, Guid>>();

            // Populate roles
            var loanAssignments = new LoanAssignmentContactTable(brokerId, loanId);
            foreach (EmployeeLoanAssignment assignment in loanAssignments.Items)
            {
                roles.Add(
                    Tuple.Create(
                        string.Format(
                            "{0}: {1}",
                            assignment.RoleModifiableDesc,
                            assignment.LastName + ", " + assignment.FirstName // EmployeeName formats it wrong, so use this instead
                        ),
                        assignment.RoleId, // Role id's are unique across the system
                        assignment.UserId
                    )
                );
            }
            return roles;
        }

        // Get a list of users that are not assigned a role to this loan but are assigned a task in this loan
        public static List<Tuple<string, Guid>> GetUnassignedUsersWithTasksForLoan(Guid brokerId, Guid loanId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@LoanId", loanId),
                                        };

            var users = new List<Tuple<string, Guid>>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_ListUnassignedUsersWithTasksByLoanId", parameters))
            {
                while (reader.Read())
                {
                    users.Add(
                        Tuple.Create((string)reader["UserName"],
                        new Guid(reader["UserId"].ToString()))
                    );
                }
            }

            return users;
        }
        public bool TempIsMigration 
        {
            get { return ConstSite.IsTaskMigrationProcess; }
        }
        /// <summary>
        /// Notify subscribers and the task assignee about recent changes that other people have made.
        /// This notification is handled by the TaskNotificationHandler
        /// </summary>
        private void EnqueueTaskNotification(bool bypassSubscribers = false)
        {
            if (TempIsMigration == true)
            {
                return; // 6/24/2011 dd - Temporary disable enqueue task notification when migrate.
            }
            if (NotifyEventContentExists() == false) return;

            TaskNotification notification = new TaskNotification(this, bypassSubscribers);
            if (notification.Recipients.Count == 0)
            {
                // Don't send a notification if there's nobody to send it to
                return;
            }

            string currentUserLoginNm = ""; // Grab the login name for logging purposes
            if (CurrentPrincipal != null)
            {
                currentUserLoginNm = CurrentPrincipal.LoginNm;
            }

            TaskNotificationQueueProcessor.SubmitNotification(
                TaskId,
                notification.ToXml().ToString(),
                currentUserLoginNm
            );
        }
        #endregion

        #region AV
        public static HashSet<String> GetAllTaskSubjectsFor(Guid brokerId, Guid loanId)
        {
            HashSet<string> subjects = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@sLId", loanId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "Task_RetrieveTaskSubjects", parameters ))
            {
                while (reader.Read())
                {
                    subjects.Add(reader["TaskSubject"].ToString());
                }
            }

            return subjects;
        }

        public static Task Create(Guid sLId, ConditionChoice choice, LoanDataForTasks loanData, TaskPermissionProcessor permissionProcessor, PermissionLevelData permissionLevels, LoanAssignmentContactTable loanAssignmentTable, Dictionary<int, ConditionCategory> conditionCategories, int defaultPermissionLevelId)
        {
            AbstractUserPrincipal currentUser = (AbstractUserPrincipal)Thread.CurrentPrincipal;
            Task t = Task.CreateWithoutPermissionCheck(sLId, currentUser.BrokerId, loanData, permissionLevels, defaultPermissionLevelId);
            t.m_loanDates = loanData.LoanDateCache;
            t.PermissionProcessor = permissionProcessor;
            t.m_categoryDescription = conditionCategories;
            t.CondCategoryId = choice.CategoryId;
            t.TaskSubject = choice.ConditionSubject;
            t.TaskAssignedUserId = Guid.Empty;
            t.TaskDueDateLocked = false;
            t.TaskDueDateCalcField = choice.DueDateFieldName;
            t.TaskDueDateCalcDays = choice.DueDateAddition;

            t.CondIsHidden = choice.IsHidden;

            t.TaskIsCondition = true;
            t.TaskCreatedByUserId = currentUser.UserId;

            t.CondRequiredDocTypeId = choice.RequiredDocTypeId;

            bool assignedAssignee = false, assignedOwner = false;

            foreach (var assignedEmployee in loanAssignmentTable.Items)
            {
                if (choice.ToBeAssignedRoleId == assignedEmployee.RoleId)
                {
                    if (assignedEmployee.UserId != Guid.Empty)
                    {
                        // 12.29.11 mf. OPM 76067
                        // If employee cannot login, they should not get the auto-task assignment.
                        // Leave it as the role.
                        t.AddFullNameForUserId(assignedEmployee.UserId, assignedEmployee.FullName);
                        t.TaskAssignedUserId = assignedEmployee.UserId;
                        assignedAssignee = true;
                    }
                }

                if (choice.ToBeOwnedByRoleId == assignedEmployee.RoleId)
                {
                    if (assignedEmployee.UserId != Guid.Empty)
                    {
                        t.AddFullNameForUserId(assignedEmployee.UserId, assignedEmployee.FullName);
                        t.TaskOwnerUserId = assignedEmployee.UserId;
                        assignedOwner = true;
                    }
                }
            }

            if (!assignedAssignee)
            {
                t.TaskToBeAssignedRoleId = choice.ToBeAssignedRoleId;
            }

            if (!assignedOwner)
            {
                t.TaskToBeOwnerRoleId = choice.ToBeOwnedByRoleId;
            }

            t.TaskPermissionLevelId = choice.Category.DefaultTaskPermissionLevelId;
            return t;
        }

        private static List<Task> SortConditionTask(Guid sLId, Dictionary<string, Task> tasks)
        {
            ConditionOrderManager orderManager = ConditionOrderManager.Load(sLId);
            return SortConditionTaskImpl(orderManager, tasks);
        }

        private static List<Task> SortConditionTaskImpl(ConditionOrderManager orderManager, Dictionary<string, Task> tasks)
        {

            if (tasks.Count < 2)
            {
                return tasks.Values.ToList();
            }

            

            List<Task> warningTask = new List<Task>();
            List<Task> miscTask = new List<Task>();
            List<Task> userSortedTask = new List<Task>();
            List<Task> unSortedTask = new List<Task>();



            foreach (string taskId in orderManager.SortedTaskIds)
            {
                Task currentTask;
                if (tasks.TryGetValue(taskId, out currentTask))
                {
                    userSortedTask.Add(currentTask);
                    tasks.Remove(taskId);
                }
            }

            foreach (Task remainingTask in tasks.Values)
            {
                if (remainingTask.CondCategoryId_rep.Equals("WARNING", StringComparison.OrdinalIgnoreCase))
                {
                    warningTask.Add(remainingTask);
                }
                else if (remainingTask.CondCategoryId_rep.Equals("MISC", StringComparison.OrdinalIgnoreCase))
                {
                    miscTask.Add(remainingTask);
                }
                else
                {
                    unSortedTask.Add(remainingTask);
                }
            }


            IEnumerable<Task> sortedWarningTask = warningTask.OrderBy(task => task.TaskSubject, StringComparer.OrdinalIgnoreCase);
            IEnumerable<Task> sortedMiscTask = miscTask.OrderBy(task => task.TaskSubject, StringComparer.OrdinalIgnoreCase);
            IEnumerable<Task> sortedUnsortedTask = unSortedTask.OrderBy(task => task.CondCategoryId_rep, StringComparer.OrdinalIgnoreCase).
                ThenBy(task => task.TaskSubject, StringComparer.OrdinalIgnoreCase);


            return sortedWarningTask.Union(userSortedTask).Union(sortedUnsortedTask).Union(sortedMiscTask).ToList();
        }

        //TO DO FIX ME - SLOW 
        /// <summary>
        /// Returns all the conditions but not the deleted ones. To Get deleted Conditions use GetDeletedConditionsByLoanId
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="sLId"></param>
        /// <param name="showHidden"></param>
        /// <returns></returns>
        public static List<Task> GetActiveConditionsByLoanId(Guid brokerId, Guid sLId, bool showHidden)
        {
            return GetActiveConditionsByLoanId(brokerId, sLId, showHidden, false);
        }

     
        /// <summary>
        /// A specialized condition fetch method that will pre load a lot of data instead of waiting until calling properties to do it as this will result in individial task sql hits 
        /// resulting in many sql hits when in reality the data can be shared between all task. 
        /// </summary>
        /// <param name="brokerId">The broker identifier for the conditions.</param>
        /// <param name="sLId">The loan identifier for the loan containing the conditions.</param>
        /// <param name="showHidden">A bool indicating whether hidden conditions should be shown.</param>
        /// <param name="orderManager">The order manager that will be used to sort the list.</param>
        /// <returns>A list of sorted conditions that are active.</returns>
        public static List<Task> GetConditionsWithAssociatedDocsByLoanId(AbstractUserPrincipal principal, Guid sLId, bool showHidden, bool includeDeleted, ConditionOrderManager orderManager)
        {
            var associationDictionary = DocumentConditionAssociation.DocumentConditionAssociation.GetAssociationsForLoanByTaskIdHeavy(principal, sLId);
            PermissionLevelData data = PermissionLevelData.Retrieve(principal.BrokerId);
            return GetConditionsByLoanId(principal.BrokerId, sLId, showHidden, includeDeleted, null, false, permissionLevelData: data, associationCache: associationDictionary, orderManager: orderManager);
        }

        public static List<Task> GetActiveConditionsByLoanId(Guid brokerId, Guid sLId, bool showHidden, bool excludeThoseUserDoesntHaveAccessTo)
        {
            return GetConditionsByLoanId(brokerId, sLId, showHidden, false, null, excludeThoseUserDoesntHaveAccessTo);
        }
        public static List<Task> GetActiveConditionsByLoanId(Guid brokerId, Guid sLId, bool showHidden, ICollection<ConditionCategory> categories)
        {
            return GetConditionsByLoanId(brokerId, sLId, showHidden, false, categories, false);
        }

        private static List<Task> GetConditionsByLoanId(Guid brokerId, Guid sLId, bool showHidden, bool includeDeleted, ICollection<ConditionCategory> categories, bool excludeThoseUserDoesntHaveAccessTo, PermissionLevelData permissionLevelData = null, IDictionary<string, List<DocumentConditionAssociation.DocumentConditionAssociation>> associationCache = null, ConditionOrderManager orderManager = null)
        {
            Dictionary<int, ConditionCategory> idCategoryMap = new Dictionary<int, ConditionCategory>();
            if (categories == null)
            {
                categories = ConditionCategory.GetCategories(brokerId).ToList();
            }

            idCategoryMap = categories.ToDictionary(p => p.Id);
            Dictionary<string, string> loanDateCache = new Dictionary<string, string>();

            var taskList = new List<Task>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@IncludeOpen", true),
                                            new SqlParameter("@IncludeClosed", true),
                                            new SqlParameter("@LoanId", sLId),
                                            new SqlParameter("@OnlyConditions", true),
                                            new SqlParameter("@IncludeDeleted", includeDeleted),
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_ListTasksByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    Task currentTask = Task.Load(reader, idCategoryMap, loanDateCache);
                    if ((showHidden == false && currentTask.CondIsHidden == false) || showHidden == true)
                    {
                        taskList.Add(currentTask);

                        if (associationCache != null)
                        {
                            List<DocumentConditionAssociation.DocumentConditionAssociation> associations; 

                            if (!associationCache.TryGetValue(currentTask.TaskId, out associations))
                            {
                                associations = new List<DocumentConditionAssociation.DocumentConditionAssociation>();
                            }
                            currentTask.m_associations = associations;
                        }

                        if (permissionLevelData != null)
                        {
                            currentTask.SetPermissionLevelData(permissionLevelData);
                        }
                    }
                }
            }


            if (excludeThoseUserDoesntHaveAccessTo)
            {
                TaskPermissionProcessor processor = new TaskPermissionProcessor((AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal);
                taskList.RemoveAll(task => processor.Resolve(task) == E_UserTaskPermissionLevel.None);
            }
            var taskByTaskID = new Dictionary<string, Task>(StringComparer.CurrentCultureIgnoreCase);
            foreach(var task in taskList)
            {
                taskByTaskID.Add(task.TaskId, task);
            }

            if (orderManager == null)
            {
                return SortConditionTask(sLId, taskByTaskID);
            }
            else
            {
                return SortConditionTaskImpl(orderManager, taskByTaskID);
            }
        }

        public static List<Task> GetAllConditionsByLoanId(Guid brokerId, Guid sLId)
        {
            return GetAllConditionsByLoanId(brokerId, sLId, null);
        }
        public static List<Task> GetAllConditionsByLoanId(Guid brokerId, Guid sLId, ICollection<ConditionCategory> categories)
        {

            Dictionary<int, ConditionCategory> idCategoryMap = new Dictionary<int, ConditionCategory>();
            if (categories == null)
            {
                categories = ConditionCategory.GetCategories(brokerId).ToList();
            }

            idCategoryMap = categories.ToDictionary(p => p.Id);

            Dictionary<string, Task> taskList = new Dictionary<string, Task>(StringComparer.CurrentCultureIgnoreCase);
            Dictionary<string, string> loanDateCache = new Dictionary<string, string>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@IncludeOpen", true),
                                            new SqlParameter("@IncludeClosed", true),
                                            new SqlParameter("@LoanId", sLId),
                                            new SqlParameter("@OnlyConditions", true),
                                            new SqlParameter("@IncludeDeleted", true),
                                            new SqlParameter("@IncludeInvalidLoans", false)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_ListTasksByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    Task currentTask = Task.Load(reader, idCategoryMap, loanDateCache);
                    taskList.Add(currentTask.TaskId, currentTask);
                }
            }

            return SortConditionTask(sLId, taskList);
        }

        //TO DO FIX ME - SLOW 
        public static List<Task> GetDeletedConditionsByLoanId(Guid brokerId, Guid sLId, bool fetchHidden)
        {
            Dictionary<string, Task> taskList = new Dictionary<string, Task>(StringComparer.CurrentCultureIgnoreCase);

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@IncludeOpen", true),
                                            new SqlParameter("@IncludeClosed", true),
                                            new SqlParameter("@LoanId", sLId),
                                            new SqlParameter("@OnlyConditions", true),
                                            new SqlParameter("@IncludeDeleted", true),
                                            new SqlParameter("@IncludeActive", false),
                                        };

            Func<Task, bool> filters = task =>
                (fetchHidden == true) || (fetchHidden == false && task.CondIsHidden == false);   //if fetch hidden is off dont show hidden conditions

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_ListTasksByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    Task currentTask = Task.Load(reader);
                    if (filters(currentTask))
                    {
                        taskList.Add(currentTask.TaskId, currentTask);
                    }
                }
            }

            return SortConditionTask(sLId, taskList);
        }

        public static void SaveMultiple(IEnumerable<Task> tasks, CStoredProcedureExec exec)
        {
            SaveMultipleImpl(tasks, task => task.Save(exec));
        }

        public static void SaveMultipleImpl(IEnumerable<Task> tasks, Func<Task, bool> saveMethod)
        {
            foreach (Task task in tasks)
            {
                if (saveMethod(task) == false)
                {
                    throw CBaseException.GenericException("A Task  Update Failed.");
                }
            }
        }

        public static void DeleteAllConditions(Guid sLid)
        {
            AbstractUserPrincipal p = (AbstractUserPrincipal)Thread.CurrentPrincipal;
            DeleteAllConditionsImpl(p.BrokerId, sLid, (name, parameters) => StoredProcedureHelper.ExecuteNonQuery(p.BrokerId, name, 3, parameters));
        }

        public static void DeleteAllConditions(Guid sLId, CStoredProcedureExec exec)
        {
            AbstractUserPrincipal p = (AbstractUserPrincipal)Thread.CurrentPrincipal;
            DeleteAllConditionsImpl(p.BrokerId, sLId, (name, parameters) => exec.ExecuteNonQuery(name, 3, parameters));
        }

        //todo please do matthew.
        private static void DeleteAllConditionsImpl(Guid brokerId, Guid sLId, Action<string, SqlParameter[]> dbSaveMethod)
        {
            dbSaveMethod("TASK_DeleteConditionsOnLoan", new SqlParameter[]{
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@sLid", sLId)});
        }

        #endregion
        #endregion

    }

    public class TaskChangeProfile
    {
        // Appropriate conditional permission checking, audit logging with real-time setting is difficult to manage.
        // We use this so order does not matter when setting to task, and to help centralizwe logic

        private Dictionary<string, Tuple<string, string>> m_changes = new Dictionary<string, Tuple<string, string>>();
        private Tuple<E_TaskStatus, E_TaskStatus> m_statusChange = null;
        public Tuple<E_TaskStatus, E_TaskStatus> StatusChange
        {
            get { return m_statusChange; }
        }

        // 11/30/2011 dd - For the mobile app, we track when the new comment is added. We do not need
        // to track the comment in audit log, therefore the new comment is only keep in memory for this object.
        // 5/15/2014 bs - Add person's name who made comment
        private string m_newComment = string.Empty;
        private string m_newCommentEditedBy = string.Empty;
        public void AddCommentChange(string comment, string editedBy)
        {
            if (string.IsNullOrEmpty(comment) == false)
            {
                m_newComment = comment;
                m_newCommentEditedBy = editedBy;
            }
        }
        public string NewComment { get { return m_newComment; } }
        public string NewCommentEditedBy { get { return m_newCommentEditedBy; } }
        public void AddChange(string fieldId, string oldValue, string newValue)
        {
            if (oldValue == null) oldValue = "";

            if (newValue == null) newValue = "";


            if (oldValue != newValue)
            {
                if (m_changes.ContainsKey(fieldId))
                {
                    // We want to know the change from what was loaded from DB if value is set multiple times.
                    string originalVal = m_changes[fieldId].Item1;
                    m_changes.Remove(fieldId);
                    m_changes.Add(fieldId, Tuple.Create(originalVal, newValue));

                }
                else
                {
                    m_changes.Add(fieldId, Tuple.Create(oldValue, newValue));
                }
            }
        }

        public void SetStatusChange(E_TaskStatus oldValue, E_TaskStatus newValue)
        {
            m_statusChange = Tuple.Create(oldValue, newValue);
            AddChange("TaskStatus", oldValue.ToString(), newValue.ToString());
        }

        public Dictionary<string, Tuple<string, string>> ModifiedFields
        {
            get { return m_changes; }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<string, Tuple<string, string>> pair in m_changes)
            {
                sb.AppendFormat("Field: {0}: {1} -> {2}{3}"
                    , pair.Key
                    , pair.Value.Item1
                    , pair.Value.Item2
                    , Environment.NewLine
                    );
            }
            return sb.ToString();
        }
    }
}