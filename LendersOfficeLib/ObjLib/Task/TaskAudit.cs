﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using CommonProjectLib.Common;
using LendersOffice.Security;
using System.Xml.Linq;
using System.Xml.Xsl;
using System.IO;
using System.Reflection;
using LendersOffice.Common;
using System.Xml.XPath;
using LendersOffice.Constants;

namespace LendersOffice.ObjLib.Task
{
    public class TaskHistoryItem
    {
        public TaskHistoryItem(string action, string actionByUser, string date, string description, string developerAuditText)
        {
            Action = action;
            ActionByUser = actionByUser;
            Date = date;
            Description = description;
            DeveloperAuditText = developerAuditText;
        }
        public string Action { get; private set; }
        public string ActionByUser { get; private set; }
        public string Date { get; private set; }
        public string Description { get; private set; }
        public string DeveloperAuditText { get; private set; }
    }

    /// <summary>
    /// Warning: This class is not multi-thread safe
    /// </summary>
    internal class TaskHistory
    {
        /*
        private enum E_KeyActionT
        {
            Create,
            Resolve,            
            AssignUser,// not ownership assignment
            SendEmail,
            ReceiveEmail,
            Edit // this is the default if it's none of the above 
        }
         */

        private string m_xmlTaskHistory;
        private XDocument x_xDoc = null;
        private string x_authorName = "";
        
        private bool m_isCreating;
        private string m_formatImportedFrom = "";
        
        private bool m_isAssigning = false;
        private string x_assignedToUserName = "";

        private bool m_isChangingStatus = false;
        private E_TaskStatus m_statusOld = E_TaskStatus.Active; // only has meaning if m_isChangingStatus = true
        private E_TaskStatus m_statusNew = E_TaskStatus.Active; // only has meaning if m_isChangingStatus = true
        private E_ConditionDeletionState m_deletionState = E_ConditionDeletionState.NoChange;
        private XElement m_newMailInEntry = null;
        private XElement m_newMailOutEntry = null;
  
        internal TaskHistory(string xmlTaskHistory, bool isTaskBeingCreated )
        {
            m_hasNewEditEntry = isTaskBeingCreated;
            m_xmlTaskHistory = xmlTaskHistory;
            m_isCreating = isTaskBeingCreated;
            if (m_isCreating)
                x_assignedToUserName = m_authorName;
        }
        internal void StoreImportingSource(string importedFrom)
        {
             m_formatImportedFrom = importedFrom;
        }

        private string m_authorName
        {
            get
            {
                if (x_authorName == "")
                {
                    if (System.Threading.Thread.CurrentPrincipal != null && System.Threading.Thread.CurrentPrincipal is AbstractUserPrincipal)
                    {
                        AbstractUserPrincipal principal = (AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal;
                        x_authorName = principal.FirstName + " " + principal.LastName;
                    }
                    else
                    {
                        x_authorName = "Task System";
                    }                        
                }
                return x_authorName;
            }
        }

        private XDocument m_xDoc
        {
            get
            {
                if (x_xDoc == null)
                    x_xDoc = XDocument.Parse(m_xmlTaskHistory);

                return x_xDoc;
            }
        }

        internal void StoreAssignmentEvent( string newUserName )
        {
            m_hasNewEditEntry = true;

            m_isAssigning = true;
            x_assignedToUserName = newUserName;
        }

        internal void StoreStatusChangeEvent( E_TaskStatus statusOld, E_TaskStatus statusNew )
        {
            m_hasNewEditEntry = true;

            m_isChangingStatus = true;
            m_statusOld = statusOld;
            m_statusNew = statusNew;
        }

        internal void StoreDeleteEvent()
        {
            m_hasNewEditEntry = true;
            m_deletionState = E_ConditionDeletionState.BeingDeleted;
        }

        internal void StoreRestoreEvent()
        {
            m_hasNewEditEntry = true;
            m_deletionState = E_ConditionDeletionState.BeingRestored;
        }

        private XElement CreateEmailXElement(string from, string sendDate, string to, string cc, string subject, string body)
        {
            XElement e = new XElement(STR_EMAIL_ELEMENT_NAME,
                                       new XAttribute("From", from),
                                       new XAttribute("Date", sendDate),
                                       new XAttribute("To", to),
                                       new XAttribute("Subject", subject),
                                       new XElement("Body", DataAccess.Tools.SantizeXmlString(body)));

            if (cc != "")
                e.Add(new XAttribute("Cc", cc));

            return e;
        }

        internal void StoreOutgoingEmail( string from, string sendDate, string to, string cc, string subject, string body)
        {
            m_newMailOutEntry = CreateNewEntryNode( /*includeDoneBy*/ true );
            m_newMailOutEntry.Add(new XElement("EmailOut"),
                CreateEmailXElement(from, sendDate, to, cc, subject, body.Replace("\r\n", "<br/>")));
        }

        internal void StoreIncomingEmail( string from, string sendDate, string to, string cc, string subject, string body )
        {
            m_newMailInEntry = CreateNewEntryNode( /*includeDoneBy*/ false );
            m_newMailInEntry.Add(new XElement("EmailIn"),
                CreateEmailXElement(from, sendDate, to, cc, subject, body));
        }

        internal bool HasEmailEntry()
        {
            foreach (XElement e in m_xDoc.Descendants(STR_EMAIL_ELEMENT_NAME))
            {
                // Using for-loop to do this to avoid parsing through the entire doc
                return true;
            }
            return false;
        }

        internal string GetMostRecentEmailContent()
        {
            XDocument xDoc = null;
            // Using for-each loop to avoid parsing through the entire doc. -thinh 6/15/2011
            foreach (XElement e in m_xDoc.Descendants(STR_EMAIL_ELEMENT_NAME))
            {
                XElement taskHistory = new XElement(STR_TASK_HISTORY, e.Parent);
                xDoc = new XDocument(taskHistory);
                break;
            }
            
            StringBuilder sb = new StringBuilder();
            using (MemoryStream outputStream = new MemoryStream())
            {
                XslTransformHelper.TransformFromEmbeddedResource("LendersOffice.ObjLib.Task.TaskAuditLastEmail.xslt", xDoc.ToString(), outputStream, null, Encoding.UTF8);

                using (StreamReader reader = new StreamReader(outputStream))
                {
                    //outputStream.Close();
                    reader.BaseStream.Position = 0;
                    return reader.ReadToEnd();
                }
            }
        }

        internal string GetLatestComment()
        {
            foreach (XElement e in m_xDoc.Descendants(STR_SAVE))
            {
                foreach (XElement c in e.Descendants("Comment"))
                {
                    if (string.IsNullOrEmpty(c.Value) == false)
                    {
                        return c.Value;
                    }
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Retrieves the oldest comment in the Task History, or the first comment on the task.
        /// </summary>
        /// <returns>The oldest comment, or the empty string if none found.</returns>
        internal string GetFirstComment()
        {
            foreach (XElement e in m_xDoc.Descendants(STR_SAVE).Reverse())
            {
                foreach (XElement c in e.Descendants("Comment"))
                {
                    if (string.IsNullOrEmpty(c.Value) == false)
                    {
                        return c.Value;
                    }
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// WARNING: Internal notes should NOT be included in task history. Don't pass it in here.
        /// </summary>
        /// <param name="FieldName"></param>
        /// <param name="oldVal"></param>
        /// <param name="newVal"></param>
        internal void StoreNormalFieldChange(string fieldName, string oldVal, string newVal)
        {
            m_hasNewEditEntry = true;

            m_commonFieldEdits.Add(
                new XElement("Field",
                    new XAttribute(STR_FIELD_NAME, fieldName),
                    new XAttribute("Old", oldVal),
                    new XAttribute("New", newVal)));
            
        }

        internal void StoreComment(string comment)
        {
            if (comment != "")
            {
                string formattedComment = comment.Replace("\n", @"<br />");
                m_commonFieldEdits.Add(new XElement("Comment", comment));
                m_commonFieldEdits.Add(new XElement("HTMLComment", formattedComment));
                m_hasNewEditEntry = true; ;
            }
        }

        internal void StoreDeveloperAudit(string auditMessage)
        {
            if (string.IsNullOrEmpty(auditMessage) == false)
            {
                m_commonFieldEdits.Add(new XElement("DeveloperAudit", auditMessage));
                m_hasNewEditEntry = true;
            }
        }
        internal void StoreEditMessage(string message)
        {
            if (message != "")
            {
                XElement newEditEntry = CreateNewEntryNode(true);
                string formattedComment = message.Replace("\n", @"<br />");

                m_commonFieldEdits.Add(newEditEntry);                
                m_commonFieldEdits.Add(new XElement("Comment", message));
                m_commonFieldEdits.Add(new XElement("HTMLComment", formattedComment));

                m_hasNewEditEntry = true; ;
            }
        }

        private XDocument GetXDocEntries( bool onlyNewEntries )
        {
            XDocument xDoc;
            XElement taskHistory;
            if (m_xmlTaskHistory == "" || onlyNewEntries )
            {
                taskHistory = new XElement(STR_TASK_HISTORY);
                xDoc = new XDocument(taskHistory);
            }
            else
            {
                xDoc = XDocument.Parse(DataAccess.Tools.SantizeXmlString(m_xmlTaskHistory));
                taskHistory = xDoc.Element(STR_TASK_HISTORY);
            }

            if (m_newMailInEntry != null)
                taskHistory.AddFirst(m_newMailInEntry);

            if (m_newMailOutEntry != null)
                taskHistory.AddFirst(m_newMailOutEntry);
            
            XElement newEditEntry = GetNewEditEntry();
            if (newEditEntry != null)
                taskHistory.AddFirst(newEditEntry);

            return xDoc;
        }
        internal string GetXmlContentForAllEntries()
        {
            return this.GetXDocEntries( /* onlyNew */ false ).ToString();
        }
        internal string GetXmlContentForNewEntries()
        {
            return this.GetXDocEntries( /* onlyNew */ true ).ToString();
        }

        private string m_assignedToUserName
        {
            get
            {
                if (x_assignedToUserName == "")
                    throw new CBaseException(CBaseException.UserMsg_Generic, "TaskHistory class is having empty assigned-to user name when it should not.");
                return x_assignedToUserName;
            }
        }

        private const string STR_DONE_BY = "By";
        private const string STR_ASSIGNED_TO = "To";
        private const string STR_TASK_HISTORY = "TaskHistory";
        private const string STR_KEY_ACTION = "KeyAction";
        private const string STR_SAVE = "Save";
        private const string STR_FIELD_NAME = "Name";
        private const string STR_EMAIL_ELEMENT_NAME = "Email";

        private bool m_hasNewEditEntry = false;
        
        private List<XElement> m_commonFieldEdits = new List<XElement>(10);

        private XElement CreateNewEntryNode( bool includeDoneBy )
        {
            DateTime now = DateTime.Now;

            XAttribute attWhen = new XAttribute("When", string.Format("{0} {1} PT", now.ToShortDateString(), now.ToShortTimeString()));

            if( includeDoneBy )
                return new XElement(STR_SAVE, attWhen, new XAttribute(STR_DONE_BY, m_authorName));
            return new XElement(STR_SAVE, attWhen);
        }

        private XElement GetNewEditEntry()
        {
            if (!m_hasNewEditEntry)
                return null;

            XElement newEditEntry = CreateNewEntryNode( /*includeDoneBy*/ true );

            #region KEY_ACTION
            if (!string.IsNullOrEmpty(m_formatImportedFrom)) newEditEntry.Add(new XAttribute("ImportedFrom", m_formatImportedFrom));

            E_TaskAuditKeyActionTable_Action r = TaskAuditKeyActionTable.LookUp(m_isCreating, m_isAssigning, m_isChangingStatus, m_deletionState, m_statusOld, m_statusNew);
            switch (r)
            {
                case E_TaskAuditKeyActionTable_Action.AssignedBy:
                    {
                        newEditEntry.Add( new XElement("Assigned", new XAttribute(STR_ASSIGNED_TO, m_assignedToUserName)));
                        break;
                    }
                case E_TaskAuditKeyActionTable_Action.ClosedAndAssignedBy:
                    {
                        newEditEntry.Add( new XElement( "ClosedAssigned", new XAttribute(STR_ASSIGNED_TO, m_assignedToUserName)));
                        break;
                    }
                case E_TaskAuditKeyActionTable_Action.ClosedBy:
                    {
                        newEditEntry.Add(new XElement("Closed"));
                        break;
                    }
                case E_TaskAuditKeyActionTable_Action.OpenedAndAssignedBy:
                    {
                        if (!string.IsNullOrEmpty(m_formatImportedFrom))
                        {
                            newEditEntry.Add(new XElement("ImportedAssigned",
                                    new XAttribute(STR_ASSIGNED_TO, m_assignedToUserName)));
                        }
                        else
                        {
                            newEditEntry.Add(new XElement("OpenedAssigned",
                                new XAttribute(STR_ASSIGNED_TO, m_assignedToUserName)));
                        }
                        break;
                    }
                case E_TaskAuditKeyActionTable_Action.EditedBy:
                    {
                        newEditEntry.Add(new XElement("Edited"));
                        break;
                    }
                case E_TaskAuditKeyActionTable_Action.ReactivatedAndAssignedBy:
                    {
                        newEditEntry.Add(new XElement("ReactivatedAssigned",new XAttribute(STR_ASSIGNED_TO, m_assignedToUserName)));
                        break;
                    }
                case E_TaskAuditKeyActionTable_Action.ReactivatedBy:
                    {
                        newEditEntry.Add(new XElement("Reactivated"));
                        break;
                    }
                case E_TaskAuditKeyActionTable_Action.ResolvedAndAssignedBy:
                    {
                        newEditEntry.Add(new XElement("ResolvedAssignedTo",new XAttribute(STR_ASSIGNED_TO, m_assignedToUserName)));
                        break;
                    }
                case E_TaskAuditKeyActionTable_Action.ResolvedClosedAndAssignedBy:
                    {
                        newEditEntry.Add(new XElement("ResolvedClosedAssignedTo",new XAttribute(STR_ASSIGNED_TO, m_assignedToUserName)));
                        break;
                    }
                case E_TaskAuditKeyActionTable_Action.ResolvedAndClosedBy:
                    {
                        newEditEntry.Add(new XElement("ResolvedClosed"));
                        break;
                    }
                case E_TaskAuditKeyActionTable_Action.ResolvedBy:
                    {
                        newEditEntry.Add(new XElement("Resolved"));
                        break;
                    }
                case E_TaskAuditKeyActionTable_Action.DeletedBy:
                    {
                        newEditEntry.Add(new XElement("Deleted"));
                        break;
                    }
                case E_TaskAuditKeyActionTable_Action.RestoredBy:
                    {
                        newEditEntry.Add(new XElement("Restored"));
                        break;
                    }
                default:
                    throw CBaseException.CreateUnexpectedEnumException(r);
            }

            #endregion // KEY_ACTION

            #region NORMAL_FIELDS_AND_COMMENT
            foreach (XElement xe in m_commonFieldEdits)
            {
                newEditEntry.Add(xe);
            }
            #endregion

            return newEditEntry;
        }

        internal string GetHtmlContentForNewEntries()
        {
            using (MemoryStream outputStream = new MemoryStream())
            {
                // Uses the lighter weight TaskAuditNotification.xslt
                XslTransformHelper.TransformFromEmbeddedResource("LendersOffice.ObjLib.Task.TaskAuditNotification.xslt", GetXDocEntries( /*onlyNew*/ true).ToString(), outputStream, null, Encoding.UTF8);

                using (StreamReader reader = new StreamReader(outputStream))
                {
                    //outputStream.Close();
                    reader.BaseStream.Position = 0;
                    return reader.ReadToEnd();
                }
            }
        }

        internal string GetHTMLContentForAllEntries()
        {
            StringBuilder sb = new StringBuilder();
            using (MemoryStream outputStream = new MemoryStream())
            {
                XsltArgumentList xsltArgument = new XsltArgumentList();
                if (ConstSite.DisplayPolicyAndRuleIdOnLpeResult)
                {
                    // 4/25/2012 dd - Only display the developer audit on LOAUTH.
                    xsltArgument.AddParam("ShowDeveloperAudit", "", "True");
                }
                else
                {
                    xsltArgument.AddParam("ShowDeveloperAudit", "", "False");

                }

                XslTransformHelper.TransformFromEmbeddedResource("LendersOffice.ObjLib.Task.TaskAudit.xslt", GetXDocEntries( /*onlyNew*/ false ).ToString(), outputStream, xsltArgument, Encoding.UTF8);
                
                using (StreamReader reader = new StreamReader(outputStream))
                {
                    //outputStream.Close();
                    reader.BaseStream.Position = 0;
                    return reader.ReadToEnd();
                }
            }
        }
        private static string GetSafeAttr(XElement el, string attrName)
        {
            string str = string.Empty;
            if (el != null)
            {
                XAttribute attr = el.Attribute(attrName);
                if (attr != null)
                {
                    str = attr.Value;
                }
            }
            return str;
        }
        internal IEnumerable<TaskHistoryItem> ListHistoryItems()
        {
            List<TaskHistoryItem> list = new List<TaskHistoryItem>();

            XDocument xdoc = GetXDocEntries(false);

            foreach (XElement saveElement in xdoc.XPathSelectElements("//Save"))
            {
                string actionByUser = saveElement.Attribute("By").Value;
                string date = saveElement.Attribute("When").Value;

                string action = string.Empty;

                XElement actionEl = saveElement.Element("EmailIn");

                if (actionEl != null)
                {
                    action = "Email received";
                }

                actionEl = saveElement.Element("EmailOut");
                if (actionEl != null)
                {
                    action = "Emailed";
                }

                actionEl = saveElement.Element("Assigned");
                if (actionEl != null)
                {
                    action = "Assigned to " + GetSafeAttr(actionEl, "To");
                }

                actionEl = saveElement.Element("AssignedClosed");
                if (actionEl != null)
                {
                    action = "Closed and assigned to " + GetSafeAttr(actionEl, "To");
                }

                actionEl = saveElement.Element("Closed");
                if (actionEl != null)
                {
                    action = "Closed";
                }

                actionEl = saveElement.Element("OpenedAssigned");
                if (actionEl != null)
                {
                    action = "Opened and assigned to " + GetSafeAttr(actionEl, "To");
                }

                actionEl = saveElement.Element("ImportedAssigned");
                if (actionEl != null)
                {
                    action = "Imported and assigned to " + GetSafeAttr(actionEl, "To");
                }

                actionEl = saveElement.Element("Edited");
                if (actionEl != null)
                {
                    action = "Edited";
                }

                actionEl = saveElement.Element("ReactivatedAssigned");
                if (actionEl != null)
                {
                    action = "Reactivated and assigned to " + GetSafeAttr(actionEl, "To");
                }

                actionEl = saveElement.Element("Reactivated");
                if (actionEl != null)
                {
                    action = "Reactivated";
                }

                actionEl = saveElement.Element("ResolvedAssignedTo");
                if (actionEl != null)
                {
                    action = "Resolved and assigned to " + GetSafeAttr(actionEl, "To");
                }

                actionEl = saveElement.Element("ResolvedClosedAssignedTo");
                if (actionEl != null)
                {
                    action = "Resolved, closed, and assigned to " + GetSafeAttr(actionEl, "To");
                }

                actionEl = saveElement.Element("ResolvedClosed");
                if (actionEl != null)
                {
                    action = "Resolved and closed " + GetSafeAttr(actionEl, "To");
                }

                actionEl = saveElement.Element("Resolved");
                if (actionEl != null)
                {
                    action = "Resolved";
                }

                actionEl = saveElement.Element("Deleted");
                if (actionEl != null)
                {
                    action = "Deleted";
                }

                actionEl = saveElement.Element("Restored");
                if (actionEl != null)
                {
                    action = "Restored";
                }


                string description = string.Empty;
                foreach (XElement fieldEl in saveElement.Elements("Field"))
                {
                    description += fieldEl.Attribute("Name").Value + " changed from " + fieldEl.Attribute("Old").Value + " to " + fieldEl.Attribute("New").Value + Environment.NewLine;
                }

                XElement commentEl = saveElement.Element("Comment");
                if (commentEl != null)
                {
                    description = commentEl.Value;
                }

                XElement developerAuditTextEl = saveElement.Element("DeveloperAudit");

                string developerAuditText = string.Empty;
                if (developerAuditTextEl != null)
                {
                    developerAuditText = developerAuditTextEl.Value;
                }

                list.Add(new TaskHistoryItem(action, actionByUser, date, description, developerAuditText));



            }
            return list;
        }

    }
}
