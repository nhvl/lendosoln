﻿namespace LendersOffice.ObjLib.Task
{
    /// <summary>
    /// Type of task automation trigger.
    /// </summary>
    public enum TaskTriggerType
    {
        AutoResolve = 0,
        AutoClose = 1
    }
}