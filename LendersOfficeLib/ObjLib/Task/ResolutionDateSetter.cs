﻿// <copyright file="ResolutionDateSetter.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   6/10/2014 10:58:56 AM 
// </summary>
namespace LendersOffice.ObjLib.Task
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    /// <summary>
    /// Aggregates field ids and provides the ability to set them. Useful for 
    /// batch operations.
    /// </summary>
    public class ResolutionDateSetter
    {
        /// <summary>
        /// The fields that need to be set to the current date.
        /// </summary>
        private HashSet<string> fieldsToSet = null;

        /// <summary>
        /// Initializes a new instance of the ResolutionDateSetter class.
        /// </summary>
        /// <param name="loanId">The id of the loan in which the fields will be set.</param>
        /// <param name="principal">The principal of the user.</param>
        public ResolutionDateSetter(Guid loanId, AbstractUserPrincipal principal)
        {
            this.LoanId = loanId;
            this.fieldsToSet = new HashSet<string>();
            this.Principal = principal;
        }

        /// <summary>
        /// Gets the id of the loan we are aggregating changes for.
        /// </summary>
        /// <value>The id of the loan.</value>
        public Guid LoanId { get; private set; }

        /// <summary>
        /// Gets the principal of the associated user.
        /// </summary>
        /// <value>
        /// The principal of the user.
        /// </value>
        public AbstractUserPrincipal Principal { get; private set; }

        /// <summary>
        /// Sets the resolution date field to the loan and saves the loan.
        /// </summary>
        /// <param name="task">The task with which to set the resolution date field on the associated loan.</param>
        /// <param name="principal">The principal of the executing user.</param>
        public static void SetResolutionDateForTask(Task task, AbstractUserPrincipal principal)
        {
            var resolutionDateSetter = new ResolutionDateSetter(task.LoanId, principal);

            resolutionDateSetter.AddField(task.ResolutionDateSetterFieldId);

            try
            {
                resolutionDateSetter.SetFieldsToCurrentDateAndTime();
            }
            catch (PageDataValidationException ex) 
            {
                Tools.LogError(ex);

                // If setting this single field causes a pre-save validation 
                // error, just don't set it.
            }
        }

        /// <summary>
        /// Adds a field to the group of fields that need to be set upon operation completion.
        /// </summary>
        /// <param name="id">The id of the field to set.</param>
        public void AddField(string id)
        {
            this.fieldsToSet.Add(id);
        }

        /// <summary>
        /// Sets the fields on the loan to the current date and time. If the user 
        /// does not have access to the loan or lacks permission to write any of 
        /// the fields, those fields will not be set.
        /// </summary>
        /// <remarks>
        /// This will attempt to set all of the fields initially. If this fails, 
        /// it will retry without trying to set the fields which caused workflow
        /// errors.
        /// </remarks>
        public void SetFieldsToCurrentDateAndTime()
        {
            try
            {
                this.SetFields(this.fieldsToSet);
            }
            catch (LoanFieldWritePermissionDenied exc)
            {
                var errorMessage = string.Format(
                    "Unable to set resolution date fields\r\nFields:{0}\r\nInvalid field changes:{1}",
                    string.Join(", ", this.fieldsToSet),
                    string.Join(", ", exc.InvalidFieldIdChangeList));
                Tools.LogError(errorMessage, exc);

                // 6/19/2014 gf - This should be improved in the future, I'm 
                // doing it like this due to time constraints.
                var errorFieldIds = exc.InvalidFieldIdChangeList
                    .Select(idWithValues => idWithValues.Split(' ')[0]);
                var fieldsWithoutErrors = this.fieldsToSet.Except(errorFieldIds);

                // We do not need to retry setting the dates if we haven't successfully
                // removed the field which caused the error. Unfortunately, this can 
                // happen when setting one field results in another field being set.
                if (fieldsWithoutErrors.Count() != this.fieldsToSet.Count)
                {
                    try
                    {
                        this.SetFields(fieldsWithoutErrors);
                    }
                    catch (LoanFieldWritePermissionDenied exc2)
                    {
                        var msg = string.Format(
                            "Unable to set resolution date fields on second try\r\nOriginal fields: {0}\r\nRetry fields: {1}",
                            string.Join(", ", this.fieldsToSet),
                            string.Join(", ", fieldsWithoutErrors));
                        Tools.LogError(msg, exc2);
                    }
                }
            }
        }

        /// <summary>
        /// Sets the given fields on the file to the current date and time.
        /// </summary>
        /// <param name="fields">The field ids that need to be set.</param>
        /// <exception cref="PageDataValidationException">
        /// Thrown when setting the fields causes a pre-save validation error.
        /// </exception>
        private void SetFields(IEnumerable<string> fields)
        {
            if (!fields.Any())
            {
                return;
            }

            // 5/28/2014 gf - Access control is intended here.
            CPageData dataLoan;
            if (this.Principal is SystemUserPrincipal)
            {
                // Skip access control check for system users.
                dataLoan = new CFullAccessPageData(this.LoanId, fields);
            }
            else
            {
                dataLoan = new CPageData(this.LoanId, "ResolutionDateSetter", fields);
            }

            try
            {
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            }
            catch (PageDataLoadDenied ex)
            {
                Tools.LogError(ex);
                return;
            }

            string valueToSet = DateTime.Now.ToString();

            foreach (var fieldId in fields)
            {
                try
                {
                    PageDataUtilities.SetValue(dataLoan, dataLoan.GetAppData(0), fieldId, valueToSet);
                }
                catch (NotFoundException exc)
                {
                    var errorMessage = string.Format(
                        "[Task][ResolutionDateSetter] Field id {0} not found for broker id {1}, user id {2}.",
                        fieldId,
                        this.Principal.BrokerId,
                        this.Principal.UserId);

                    Tools.LogErrorWithCriticalTracking(errorMessage, exc);

                    continue;
                }
            }

            try
            {
                dataLoan.Save();
            }
            catch (PageDataSaveDenied ex2)
            {
                Tools.LogError(ex2);
                return;
            }
        }
    }
}
