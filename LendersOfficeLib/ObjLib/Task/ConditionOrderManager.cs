﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml.Linq;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOffice.ObjLib.Task
{
    public class ConditionOrderManager 
    {
        private static readonly string NODE_ROOT = "ConditionSortOrder";
        private static readonly string NODE_SORTED = "Sorted";
        private static readonly string NODE_TASK = "Task";
        private static readonly string ATTR_ID = "Id";

        private CPageData _pageData;
        private bool canSave; 


        private ICollection<string> m_idsSorted = new List<string>();

        private ConditionOrderManager(string sortOrderXml)
        {
            this.canSave = false;
            this.ReadXml(sortOrderXml);
        }

        private ConditionOrderManager(ICollection<string> idsSorted)
        {
            this.canSave = false;
            this.m_idsSorted = idsSorted;
        }

        private ConditionOrderManager(Guid sLId)
        {
            this.canSave = true;
            _pageData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(ConditionOrderManager));
            _pageData.AllowSaveWhileQP2Sandboxed = true;
            _pageData.InitSave(ConstAppDavid.SkipVersionCheck);
            ReadXml(_pageData.sConditionsSortOrder);
        }


        public IEnumerable<string> SortedTaskIds
        {
            get { return m_idsSorted; }
        }

        /// <summary>
        /// Maps the current condition order manager to a new one based on a
        /// different set of task ids.
        /// </summary>
        /// <param name="oldIdToNewId">
        /// A mapping from original task id to new task id. This does not need 
        /// to contain all of the task ids that are managed in this instance. 
        /// However, the returned instance will only have task ids that were 
        /// included in the mapping.
        /// </param>
        /// <returns>
        /// A condition order manager to be used with the new task ids provided in the mapping.
        /// </returns>
        public ConditionOrderManager MapToNewConditionIds(Dictionary<string, string> oldIdToNewId)
        {
            var newIdsSorted = new List<string>();

            foreach (var oldId in this.m_idsSorted)
            {
                string newTaskId;
                if (oldIdToNewId.TryGetValue(oldId, out newTaskId))
                {
                    newIdsSorted.Add(newTaskId);
                }
            }

            return new ConditionOrderManager(newIdsSorted);
        }

        private void ReadXml(string xml)
        {
            if (string.IsNullOrEmpty(xml))
            {
                return;
            }

            XDocument doc = XDocument.Parse(xml);
            XElement root = doc.Element(NODE_ROOT).Element(NODE_SORTED);

            foreach (XElement task in root.Elements(NODE_TASK))
            {
                m_idsSorted.Add(task.Attribute(ATTR_ID).Value);
            }

        }


        private string WriteXml()
        {
            List<XElement> sorted = new List<XElement>();

            foreach (string taskId in m_idsSorted)
            {
                sorted.Add(new XElement(NODE_TASK, new XAttribute(ATTR_ID, taskId)));
            }

            XDocument doc = new XDocument(
                new XElement(NODE_ROOT,
                    new XElement(NODE_SORTED, sorted.ToArray()))
            );

            return doc.ToString();
        }


        private void Save()
        {
            if (!canSave)
            {
                throw new InvalidOperationException("Cannot save due to load from xml.");
            }

            _pageData.sConditionsSortOrder = WriteXml();
            _pageData.sConditionLastModifiedD = DateTime.Now;
            _pageData.Save();
        }

        /// <summary>
        /// Loads the current sort data clears the sorted partition then removes the given task ids from warning/misc if they exist and finally
        /// adds them to the sorted partition. In the end commits the changes to the database.
        /// Note does not do access control checks because the user may not have access to edit the loan file. The condition roder does not count.
        /// </summary>
        /// <param name="sLid">The loan id of the task we are modifying</param>
        /// <param name="taskIds">The ordered list of task ids.</param>
        public static void StoreUserSort(Guid sLId, IEnumerable<string> taskIds)
        {
            ConditionOrderManager cm = new ConditionOrderManager(sLId);
            cm.m_idsSorted.Clear(); //user sort will populate this

            foreach (string taskId in taskIds)
            {
                cm.m_idsSorted.Add(taskId); 
            }

            cm.Save();
        }

        public static ConditionOrderManager Load(Guid sLId)
        {
            ConditionOrderManager cm = new ConditionOrderManager(sLId);
            return cm;
        }

        public static ConditionOrderManager Load(string sortOrderXml)
        {
            ConditionOrderManager manager = new ConditionOrderManager(sortOrderXml);
            return manager;
        }
    }
    
}
