﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonProjectLib.Email;
using LendersOffice.Constants;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using mshtml;
using CommonProjectLib.Logging;
using System.Text.RegularExpressions;
using LendersOffice.Email;
using System.Threading;
using LendersOffice.Common;
using LendingQBPop3Mail;

namespace LendersOffice.ObjLib.Task
{
    public class ClientIdTaskEmailReceiver : CommonProjectLib.Runnable.IRunnable
    {

        public string Description
        {
            get { return "Handles Task emails from Clients"; }
        }


        private string m_sEmailAddress;
        private string m_sEmailPassword;
        private string m_sEmailServer;
        private int m_iEmailServerPort;
        private bool m_EmailIsSsl; 

        private Regex m_sSubjectMatcher = new Regex("task\\s*([A-z0-9]+)[_]([A-z0-9]+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private Regex m_EmailMatcher = new Regex(ConstApp.EmailValidationExpression, RegexOptions.Compiled | RegexOptions.IgnoreCase);

        private Dictionary<string, Guid> m_clientCodes;

        public ClientIdTaskEmailReceiver()
        {
            m_sEmailAddress = ConstStage.TaskEmailAddress;
            m_sEmailPassword = ConstStage.TaskEmailPassword;
            m_sEmailServer = ConstStage.EmailReceivingServer;
            m_iEmailServerPort = ConstStage.EmailReceivingServerPort;
            m_EmailIsSsl = ConstStage.EmailReceivingServerIsSsl;
        }

        private string ProcessHTML(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return "";
            }

            StringBuilder newText = new StringBuilder(text.Length * 2);
            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] == '<')
                {
                    newText.Append(' ');
                }
                newText.Append(text[i]);
            }
            return newText.ToString();
        }

        private void HandleMessage(string id, Pop3Message emailMessage)
        {
            if (string.IsNullOrEmpty(emailMessage.Subject) && string.IsNullOrEmpty(emailMessage.Body))
            {
                Tools.LogInfo("ClientIdTaskEmailReceiver", "Skipping email with blank subject and body. from=[" + emailMessage.From + "]");
                return;
            }

            Tools.LogInfo("ClientIdTaskEmailReceiver", "Process email from=[" + emailMessage.From + "], subject=[" + emailMessage.Subject + "]");

#if LQB_NET45
            HTMLDocument doc = new HTMLDocument();
#else
            // 9/9/2015 - dd - After we no longer support .net 3.5, remove this line.
            HTMLDocumentClass doc = new HTMLDocumentClass();
#endif

            IHTMLDocument2 doc2 = (IHTMLDocument2)doc;
            string emailBody = string.Empty;
            if (emailMessage.HTMLBody != null && emailMessage.HTMLBody.TrimWhitespaceAndBOM().Length > 0)
            {
                string data = ProcessHTML(emailMessage.HTMLBody);
                doc2.write(new object[] { data });
                emailBody = (doc.body != null ? doc.body.innerText : string.Empty);
                if (string.IsNullOrEmpty(emailBody))
                {
                    LogManager.Warn(
                        "email has htmlbody, but we weren't able to convert it to an emailbody" + Environment.NewLine
                        + "Subject: " + emailMessage.Subject ?? string.Empty + Environment.NewLine 
                        +"HTMLBody: " + Environment.NewLine
                        + emailMessage.HTMLBody);
                }
            }
            if(string.IsNullOrEmpty(emailBody))
            {
                emailBody = emailMessage.Body;
            }
            emailBody = (null == emailBody ? string.Empty : emailBody);
            emailBody = emailBody.Replace(Environment.NewLine, "<br/>");


            bool found = false;
            if (!string.IsNullOrEmpty(emailMessage.Subject))
            {
                foreach (Match match in m_sSubjectMatcher.Matches(emailMessage.Subject))
                {

                    if (match.Groups.Count < 3)
                    {
                        LogManager.Error("Cannot get task number " + emailMessage.Subject);
                        break;
                    }

                    Task task = Task.RetrieveWithoutPermissionCheck(GetBrokerFrom(match.Groups[1].Value), match.Groups[2].Value);
                    StringBuilder ccSb = new StringBuilder();
                    StringBuilder toSb = new StringBuilder();
                    foreach (var r in emailMessage.Recipients)
                    {
                        if (r.ReType == Pop3RecipientType.Cc)
                        {
                            ccSb.Append(r.Email + ";");
                        }
                        else
                        {
                            toSb.Append(r.Email + ";");
                        }
                    }

                    if (ccSb.Length > 0)
                    {
                        ccSb.Remove(ccSb.Length - 1, 1);
                    }

                    if (toSb.Length > 0)
                    {
                        toSb.Remove(toSb.Length - 1, 1);
                    }
                    bool sendNotifications = emailMessage.Headers.Text.IndexOf("X-Auto-Response-Suppress", StringComparison.OrdinalIgnoreCase) == -1;

                    if (task != null)
                    {
                        task.StoreIncomingEmail(emailMessage.From, emailMessage.Date.ToShortDateString(), toSb.ToString(), ccSb.ToString(), emailMessage.Subject, emailBody, sendNotifications);
                        found = true;
                        break;
                    }
                }
            }

            if (false == found)
            {
                LogManager.Error("Did not find task number -" + emailMessage.Subject ?? "NO SUBJECT");
            }
        }


        private Guid GetBrokerFrom(string address)
        {
            if (m_clientCodes == null)
            {
                PopulateEmailAliases();
            }


            Guid brokerID = Guid.Empty;
            m_clientCodes.TryGetValue(address, out brokerID);
            return brokerID;
        }

        private void PopulateEmailAliases()
        {
            m_clientCodes = new Dictionary<string, Guid>(StringComparer.OrdinalIgnoreCase);
            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "ListAllCustomerCodesWithBrokerIds", null))
                {
                    while (reader.Read())
                    {
                        string clientCode = (string)reader["CustomerCode"];
                        if (false == string.IsNullOrEmpty(clientCode))
                        {
                            if (m_clientCodes.ContainsKey(clientCode))
                            {
                                if (ConstAppDavid.CurrentServerLocation == ServerLocation.Production)
                                {
                                    Tools.LogError("Ignoring dup client code " + clientCode);
                                }
                                continue;
                            }
                            m_clientCodes.Add(clientCode, (Guid)reader["BrokerId"]);
                        }
                    }
                }
            }
        }

        private string StringifyMessage(Pop3Message message)
        {
            return string.Format("Subject: {1}{0}{0}Body:{2}{0}",
            Environment.NewLine, message.Subject, message.Body);
        }

        private CBaseEmail CreateEmailAndSend(string subject, Exception e, Pop3Message message, string devMessage)
        {
            try
            {
                CBaseEmail c = new CBaseEmail(ConstAppDavid.SystemBrokerGuid)
                {
                    Subject = subject,
                    To = ConstStage.EmailAddressOfSupport,
                    From = "scottk@meridianlink.com",
                    Message = string.Format("devMessage:{7}{0}{0}Error details:{3}{0}{0}Stack Trace:{0}\t{4}{0}{0}Inner Exception:{0}\t{5}{0}{0}Subject: {1}{0}{0}Body:{2}{0}",
                    Environment.NewLine, message.Subject, message.Body, e.Message, e.StackTrace, e.InnerException, message.BodyText, devMessage)
                };
                
                // hotfix only: removing the send.  Longer term I'll just remove this method entirely.

                return c;
            }
            catch (Exception ex)
            {
                Tools.LogError(ex);
                return null;
            }
        }
        private CBaseEmail CreateEmailAndSend(string subject, Exception e, Pop3Message message)
        {
            return CreateEmailAndSend(subject, e, message, "NONE");
        }


        #region IRunnable Members

        public void Run()
        {
            Tools.ResetLogCorrelationId();

            Tools.LogInfo("ClientIdTaskEmailReceiver", "ClientIdTaskEmailReceiver started. Email Server=[" + m_sEmailServer + "]. TaskEmailAddress=[" + m_sEmailAddress + "]");

            try
            {
                using (JMailEmailProcessor processor = new JMailEmailProcessor(
                    m_sEmailAddress,
                    m_sEmailPassword,
                    m_sEmailServer,
                    m_iEmailServerPort, 
                    m_EmailIsSsl))
                {
                    //LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(10000); // only when attaching the debugger.
                    int count = 0;
                    foreach (var message in processor.GetEmailMessages())
                    {
                        string uniqueid = processor.GetMessageUniqueId(count);
                        try
                        {
                            HandleMessage(uniqueid, message);
                        }
                        catch (TaskNotFoundException tnfe)
                        {
                            Tools.LogError("ClientIdTaskEmailReceiver TaskNotFoundException " + StringifyMessage(message), tnfe);
                            ReplyThatUploadFailed(message);
                        }
                        catch (CBaseException cbe)
                        {
                            Tools.LogError("ClientIdTaskEmailReceiver cbaseexception " + StringifyMessage(message), cbe);
                            CreateEmailAndSend("ClientIdTaskEmailReceiver cbaseexception.",
                                cbe,
                                message);
                            ReplyThatUploadFailed(message);
                        }
                        catch (Exception e)
                        {
                            Tools.LogError("ClientIdTaskEmailReceiver exception " + StringifyMessage(message), e);
                            CreateEmailAndSend("ClientIdTaskEmailReceiver exception.",
                                e,
                                message);
                            ReplyThatUploadFailed(message);
                        }
                        finally
                        {
                            processor.DeleteMessage(count);
                        }
                        count++;
                    }

                    Tools.LogInfo("ClientIdTaskEmailReceiver", "ClientIdTaskEmailReceiver processed " + count + " emails.");
                }
            }
            catch (Exception e)
            {
                Tools.LogError("ClientIdTaskEmailReceiver exception: ", e);
                throw;
            }
        }
        #endregion

        private void ReplyThatUploadFailed(Pop3Message message)
        {
            try
            {
                if (string.Compare(message.From.TrimWhitespaceAndBOM(), m_sEmailAddress.TrimWhitespaceAndBOM(), true) == 0)
                {
                    // Don't send it back to ourselves :-)
                    return;
                }
                var cbe = new CBaseEmail(ConstAppDavid.SystemBrokerGuid)
                {
                    To = message.From,
                    From = ConstStage.DefaultDoNotReplyAddress,
                    Subject = "Task message upload failure",
                    Message = "The task system was unable to upload your message. Please be sure to notify the intended recipients. For assistance contact your LendingQB system administrator."
                    + Environment.NewLine
                    + Environment.NewLine
                    + StringifyMessage(message)
                };
                cbe.Send();
            }
            catch (Exception e)
            {
                Tools.LogErrorWithCriticalTracking("ClientIdTaskEmailReceiver exception, error sending email back to original sender.", e);
            }
        }

    }
}
