﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web.UI;
using System.Xml;
using System.Xml.Linq;

using CommonProjectLib.Caching;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Email;
using LendersOffice.ObjLib.DatabaseMessageQueue;
using LendersOffice.Security;

namespace LendersOffice.ObjLib.Task
{
    using TaskLoanGroupType = Dictionary<string, List<TaskNotificationHandler.TaskMessage>>;
    using TaskDueGroupType = Dictionary<string, Dictionary<string, List<TaskNotificationHandler.TaskMessage>>>;

    /// <summary>
    /// This class
    ///   * accumulates task notifications together per recipient per task,
    ///   * checks permissions on recipients (just the previous assignee for now),
    ///   * sanitizes text,
    ///   * and emails the result out.
    /// </summary>
    /// <remarks>
    /// Revised 1/31/12 for opm 76482. Pretty much a complete rewrite.
    /// 
    /// There may eventually be TONS of task emails created. We should look
    /// into making the email bodies smaller.
    /// </remarks>
    public class TaskNotificationHandler
    {
        /// <summary>
        /// Holds all the recent data for a single task for a user
        /// </summary>
        public class TaskMessage
        {
            private string dueDate;

            public string ToEmailAddress;
            public string TaskSubject;
            public string TaskID;
            public E_TaskStatus TaskStatus;

            /// <summary>
            /// Get or Set string timestamp. Update DueDate will affect property DateTimeD.
            /// </summary>
            public string DueDate
            {
                get
                {
                    return dueDate;
                }
                set
                {
                    dueDate = value;
                    DateTime dueTime;
                    if (DateTime.TryParse(dueDate, out dueTime))
                    {
                        DueDateD = dueTime;
                    }
                    else
                    {
                        DueDateD = DateTime.MaxValue ;
                    }
                }
            }

            /// <summary>
            /// Get or set timestamp of task message. Can be use in comparison.
            /// Default of this value should be DateTime.MaxValue (for tasks don't have due date).
            /// </summary>
            public DateTime DueDateD { get; private set; }

            public string LoanNumber;
            public string BorrowerName;

            /// <summary>
            /// Fullname of task assignee.
            /// </summary>
            public string AssingedUserName;

            public string Subject
            {
                get
                {
                    return string.Format(
                        "Task {0} Due: {1} - {2} - {3} - {4}",
                        this.TaskID,
                        this.DueDate,
                        this.LoanNumber,
                        this.BorrowerName,
                        this.TaskSubject
                    );
                }
            }

            public List<string> TaskEntries = new List<string>();

            /// <summary>
            /// Initializes a new instance of the TaskMessage class.
            /// </summary>
            public TaskMessage()
            {
                this.DueDateD = DateTime.MaxValue;
            }
            public void AddTaskEntry(string value)
            {
                this.TaskEntries.Add(value);
            }

            public void RenderHTML(HtmlTextWriter writer)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Strong);
                writer.Write(this.Subject);
                writer.RenderEndTag();
                writer.WriteLine();

                // Have the most recent entries on top
                for (int i = this.TaskEntries.Count - 1; i >= 0; i--) // SO UGLY
                {
                    writer.AddStyleAttribute(HtmlTextWriterStyle.MarginLeft, "1em");
                    writer.RenderBeginTag(HtmlTextWriterTag.P);
                    writer.Write(this.TaskEntries[i]);
                    writer.RenderEndTag();
                    writer.WriteLine();
                }
            }
        }

        /// <summary>
        /// Holds the information necessary for an email to a single user.
        /// Will have one or more TaskMessages
        /// </summary>
        public class TaskNotificationEmail
        {
            public Guid BrokerId;
            public string Subject = "Task Activity";
            public string ToEmailAddress;
            public string FromEmailAddress = BrokerDB.TaskAliasEmailAddress;
            public LinkedList<TaskMessage> TaskMessages = new LinkedList<TaskMessage>();

            const string PastTaskLabel = "Overdue";
            const string LongFutureTaskLabel = "Due in Future";

            public TaskNotificationEmail(Guid brokerId)
            {
                this.BrokerId = brokerId;
            }

            public void RenderHTML(HtmlTextWriter writer)
            {
                // Add each task to the HTML
                foreach (var message in this.TaskMessages)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Div);
                    message.RenderHTML(writer);
                    writer.RenderEndTag();
                    writer.WriteLine();
                }
            }

            /// <summary>
            /// Render the html having group by due date.            
            /// </summary>
            /// <returns>Return html string of task messages group by due date and loan number.</returns>
            public void RenderHTMLWithDueGroup(HtmlTextWriter writer)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Div);
                GroupedTaskNotifications groupedTaskMessages = this.GroupTaskMessages(this.TaskMessages);

                writer.RenderBeginTag(HtmlTextWriterTag.H3);
                writer.WriteLine("[ACTIVE TASKS]");
                writer.RenderEndTag();

                if (groupedTaskMessages.ActiveTaskNotifications.Any())
                {
                    foreach (var dueKeypair in groupedTaskMessages.ActiveTaskNotifications)
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.P);
                        writer.RenderBeginTag(HtmlTextWriterTag.Strong);
                        string dueLabel = dueKeypair.Key;
                        writer.Write(dueLabel);
                        writer.RenderEndTag();
                        writer.RenderEndTag();

                        var notificationsByLoan = dueKeypair.Value;
                        bool shouldShowDueDate = (dueLabel == PastTaskLabel || dueLabel == LongFutureTaskLabel);
                        WriteNotificationsForLoans(writer, notificationsByLoan, shouldShowDueDate);
                    }
                }
                else
                {
                    writer.WriteLine("None");
                }

                writer.WriteBreak(); // Writes <br />
                writer.RenderBeginTag(HtmlTextWriterTag.H3);
                writer.WriteLine("[RESOLVED/CLOSED TASKS]");
                writer.RenderEndTag();

                if (groupedTaskMessages.ResolvedTaskNotifications.Any())
                {
                    WriteNotificationsForLoans(writer, groupedTaskMessages.ResolvedTaskNotifications, shouldShowDueDate: false);
                }
                else
                {
                    writer.WriteLine("None");
                }


                writer.RenderEndTag();
            }

            private static void WriteNotificationsForLoans(HtmlTextWriter writer, TaskLoanGroupType notificationsByLoan, bool shouldShowDueDate)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Ul);
                foreach (var loanKeyPair in notificationsByLoan)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Li);
                    writer.Write("\tLoan " + loanKeyPair.Key);
                    writer.RenderBeginTag(HtmlTextWriterTag.Ul);
                    foreach (var message in loanKeyPair.Value)
                    {
                        writer.RenderBeginTag(HtmlTextWriterTag.Li);
                        writer.Write("Task " + message.TaskID + "-" + message.TaskSubject);
                        writer.RenderBeginTag(HtmlTextWriterTag.Ul);

                        if (!string.IsNullOrEmpty(message.AssingedUserName))
                        {
                            writer.RenderBeginTag(HtmlTextWriterTag.Li);
                            writer.Write("Assigned to: ");
                            writer.RenderBeginTag(HtmlTextWriterTag.Strong);
                            writer.Write(message.AssingedUserName);
                            writer.RenderEndTag();
                            writer.RenderEndTag();
                        }

                        if (!string.IsNullOrEmpty(message.DueDate) && shouldShowDueDate)
                        {
                            writer.RenderBeginTag(HtmlTextWriterTag.Li);
                            writer.Write("Due date: ");
                            writer.RenderBeginTag(HtmlTextWriterTag.Strong);
                            writer.Write(message.DueDate);
                            writer.RenderEndTag();
                            writer.RenderEndTag();
                        }

                        writer.RenderEndTag();
                        writer.RenderEndTag();
                    }
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                }
                writer.RenderEndTag();
            }

            /// <summary>
            /// Group  task messages by due date then by loan.
            /// </summary>
            /// <param name="rawTaskMessageLinkedList">Linked list of task messages.</param>
            /// <returns>Dictionary of task message by duadate and by loan</returns>
            private GroupedTaskNotifications GroupTaskMessages(LinkedList<TaskMessage> rawTaskMessageLinkedList)
            {
                var resolvedTaskIds = new HashSet<string>(
                    rawTaskMessageLinkedList
                        .Where(taskMessage => taskMessage.TaskStatus != E_TaskStatus.Active)
                        .Select(taskMessage => taskMessage.TaskID));
                // prepare the output
                var resolvedTasks = new TaskLoanGroupType();
                var taskMessageLinkedList = rawTaskMessageLinkedList.OrderBy(x => x.DueDateD);
                TaskDueGroupType taskLoanDict = new TaskDueGroupType();
                // for each message
                foreach (TaskMessage msg in taskMessageLinkedList)
                {
                    if (resolvedTaskIds.Contains(msg.TaskID))
                    {
                        string loanLabel = msg.LoanNumber + " - Borrower: \"" + msg.BorrowerName + "\"";
                        List<TaskMessage> messagesForLoan;

                        if (!resolvedTasks.TryGetValue(loanLabel, out messagesForLoan))
                        {
                            messagesForLoan = new List<TaskMessage>();
                            resolvedTasks.Add(loanLabel, messagesForLoan);
                        }

                        messagesForLoan.Add(msg);
                    }
                    else
                    {
                        // Get lable fro date
                        string timeLabel = this.GetLabelFromDue(msg.DueDateD);
                        // If label not exist, create
                        if (!taskLoanDict.ContainsKey(timeLabel))
                        {
                            taskLoanDict.Add(timeLabel, new TaskLoanGroupType());
                        }

                        string loanLabel = msg.LoanNumber + " - Borrower: \"" + msg.BorrowerName + "\"";
                        if (!taskLoanDict[timeLabel].ContainsKey(loanLabel))
                        {
                            taskLoanDict[timeLabel].Add(loanLabel, new List<TaskMessage>());
                        }

                        // insert to label
                        taskLoanDict[timeLabel][loanLabel].Add(msg);
                    }
                }

                return new GroupedTaskNotifications()
                {
                    ActiveTaskNotifications = taskLoanDict,
                    ResolvedTaskNotifications = resolvedTasks
                };
            }

            /// <summary>
            /// Get the label to be shown for tasks group by due date. Use this method to divide all task messages to groups.
            /// </summary>
            /// <param name="dueTime">Due time  of a task.</param>
            /// <returns>Label of a group for that task due time.</returns>
            private string GetLabelFromDue(DateTime dueTime)
            {
                // Detect of message and store to the output 
                if (dueTime == null)
                {
                    return LongFutureTaskLabel;
                }
                else if (dueTime.Date < DateTime.Today)
                {
                    return PastTaskLabel;
                }
                else if (dueTime.Date == DateTime.Today)
                {
                    return "Due Today " + dueTime.Date.ToShortDateString();
                }
                else if (dueTime.Date == DateTime.Today.AddDays(1).Date)
                {
                    return "Due Tomorrow " + dueTime.Date.ToShortDateString(); ;
                }
                else if (dueTime.Date <= DateTime.Today.AddDays(7))
                {
                    return "Due " + dueTime.Date.ToShortDateString();
                }
                else
                {
                    return LongFutureTaskLabel;
                }
            }

            public string ToHTMLString()
            {
                StringWriter stringWriter = new StringWriter();
                using (HtmlTextWriter writer = new HtmlTextWriter(stringWriter))
                {
                    this.RenderHTMLWithDueGroup(writer);
                }
                return stringWriter.ToString();
            }

            /// <summary>
            /// Creates a CBaseEmail and sends it.
            /// </summary>
            public void Send()
            {
                CBaseEmail cbe = new CBaseEmail(this.BrokerId)
                {
                    Subject = this.Subject,
                    From = this.FromEmailAddress,
                    To = this.ToEmailAddress,
                    CCRecipient = "",
                    Bcc = "",
                    Message = this.ToHTMLString(),
                    IsHtmlEmail = true,
                    DisclaimerType = E_DisclaimerType.NORMAL
                };

                cbe.Send(); // Watch out for "missing to or bcc address" exception
            }

        }

        private class TaskNotificationEmployeeEmailData
        {
            public E_TaskRelatedEmailOptionT EmailOption { get; set; }
            public string EmailAddress { get; set; }
        }

        private class GroupedTaskNotifications
        {
            public Dictionary<string, List<TaskMessage>> ResolvedTaskNotifications { get; set; }

            public Dictionary<string, Dictionary<string, List<TaskMessage>>> ActiveTaskNotifications { get; set; }
        }

        private readonly TimeSpan CacheDuration = TimeSpan.FromMinutes(10.0);

        private DBMessageQueue m_mQ = new DBMessageQueue(ConstMsg.Task2011Queue);
        private Dictionary<Guid, Dictionary<string, TaskMessage>> m_recipientTaskIndex = new Dictionary<Guid, Dictionary<string, TaskMessage>>(); // recipientId -> taskId -> taskMessage
        private Dictionary<Guid, TaskNotificationEmail> m_emailIndex = new Dictionary<Guid, TaskNotificationEmail>(); // recipientId -> email

        /// <summary>
        /// Map of recipientId to email message.
        /// </summary>
        public Dictionary<Guid, TaskNotificationEmail> EmailIndex { get { return m_emailIndex; } }

        /// <summary>
        /// Read messages from the queue. PeekN()
        /// Accumulate messages per recipient per task. HandleMessage()
        /// Accumulate messages per recipient. CreateEmails()
        /// Send them. SendEmails()
        /// 
        /// If we ever do multithreaded...
        ///   Separate messages by subject
        ///   Each subject gets a thread to coalesce messages.
        ///   Lock the recipient->task index when updating it.
        ///   Each recipient gets a thread to create emails.
        ///   Single thread to send emails.
        ///   (This will probably never happen.)
        /// </summary>
        public void Process()
        {
            // Note: the messages are constructed in Task.cs::EnqueueTaskNotification
            DBMessage[] messages;
            try
            {
                // Grab messages, but don't remove them from the queue yet
                messages = m_mQ.PeekN(ConstStage.TaskNotificationNumMessagesToProcess); // Performance issues? Try reducing the number here.
            }
            catch (SqlException) // The queue must have timed out. Well, at least the messages are still in the queue. Try again next time.
            {
                return;
            }
            if (messages.Count() == 0) // No messages. Do nothing.
            {
                return;
            }

            // Process the messages
            int messagesProcessed = 0;
            foreach (DBMessage msg in messages)
            {
                // BEGIN PROCESSING A SINGLE TASK
                XDocument xmlMessageData;
                try
                {
                    xmlMessageData = XDocument.Parse(msg.Data);
                }
                catch (XmlException)
                { 
                    // If we get bad XML, log and remove the message; we can't do anything to fix it.
                    Tools.LogError(
                        string.Format("Malformed task notification XML! TaskId: {0}, MessageId: {1}, dropping message.\r\n{2}",
                            msg.Subject1,
                            msg.Id,
                            msg.Data
                        )
                    );
                    m_mQ.ReceiveById(msg.Id);
                    m_mQ.DeleteFromArchive(msg.Id);
                    continue;
                }

                TaskNotification notification = new TaskNotification(xmlMessageData);
                HandleNotification(notification);
                messagesProcessed++;
                // END PROCESSING A SINGLE TASK
            }

            if (messagesProcessed == 0)
            {
                // Either something went wrong or there are no messages
                // If something went wrong, we want to keep them in the queue for reprocessing
                return;
            }

            CreateEmails();
            
            SendEmails();
            // PROBLEM! If this process repeatedly fails between the time that it sends out the emails and the time that it
            // deletes the messages from the queue, then it will keep on sending the emails out, resulting in an
            // EMAIL FLOOD OF BIBLICAL PROPORTIONS.

            // Dequeue after the emails have been sent, to ensure that we do not lose any messages.
            long startId = messages.First().Id;
            long endId = messages.Last().Id;
            m_mQ.DequeueRange(startId, endId);
            
        }

        /// <summary>
        /// Produces a single TaskMessage for each recipient and places it in
        /// the m_recipientTaskIndex.
        /// 
        /// SIDE EFFECT:
        ///   Users and messages added to m_recipientTaskIndex
        /// </summary>
        /// <param name="notification">TaskNotification</param>
        private void HandleNotification(TaskNotification notification)
        {
            var emailFrom = notification.EmailedFrom ?? "";
            var emailTo = "";
            var previousAssignee = notification.PreviousAssignedUserId;

            var taskId = notification.TaskId;
            var dueDate = notification.TaskDueDate;
            var loanNum = notification.LoanNumCached;
            var borrowerNm = notification.BorrowerNmCached;
            var taskSubject = notification.TaskSubject;
            var brokerId = notification.BrokerId;
            var taskStatus = notification.TaskStatus;

            var recipients = notification.Recipients;

            var body = notification.Body;
            var assignedUserId = notification.CurrentAssignedUserId;

            foreach (var userId in recipients)
            {
                // First, check if we need to even email this message to this recipient
                var employeeData = this.GetEmployeeDataByUserId(brokerId, userId);
                if (employeeData != null &&
                    employeeData.EmailOption == E_TaskRelatedEmailOptionT.ReceiveEmail && // Does the employee have "Receieve task-related notifications" on in the admin page?
                    !string.Equals(employeeData.EmailAddress, emailFrom, StringComparison.OrdinalIgnoreCase)) // Is this task not emailed from this user?
                {
                    emailTo = employeeData.EmailAddress;
                }
                else
                {
                    continue;
                }

                // If recipient was previous assignee, check if they still can see the task
                if (userId == previousAssignee)
                {
                    if (!HasPermission(brokerId, userId, taskId))
                        continue;
                }

                // If the user's not already in the dict, add 'em
                if (!m_recipientTaskIndex.ContainsKey(userId))
                {
                    m_recipientTaskIndex.Add(userId, new Dictionary<string, TaskMessage>());
                }

                // If the task is not already in the dict, add it
                if (!m_recipientTaskIndex[userId].ContainsKey(taskId))
                {
                    m_recipientTaskIndex[userId].Add(taskId, new TaskMessage());
                }

                // Add our task entry and other relevant information
                var taskMessage = m_recipientTaskIndex[userId][taskId];
                taskMessage.ToEmailAddress = emailTo; // Override previous values if they exist
                taskMessage.TaskSubject = taskSubject;
                taskMessage.TaskID = taskId;
                taskMessage.DueDate = dueDate;
                taskMessage.LoanNumber = loanNum;
                taskMessage.BorrowerName = borrowerNm;
                taskMessage.AddTaskEntry(body);
                taskMessage.AssingedUserName = this.GetUserDisplayName(assignedUserId, brokerId);
                taskMessage.TaskStatus = taskStatus;
            }
        }
        
        /// <summary>
        /// For each recipient, join the task information into a single email
        /// and send it. Requires m_recipientTaskIndex to be updated.
        /// 
        /// SIDE EFFECT:
        ///   Creates TaskNotificationEmails and puts them in the emailIndex.
        /// </summary>
        public void CreateEmails()
        {
            foreach (var kv in m_recipientTaskIndex)
            {
                var recipientId = kv.Key;
                var messageDict = kv.Value;

                var email = CreateEmail(recipientId, messageDict);
                if (email != null)
                {
                    m_emailIndex.Add(recipientId, email);
                }
                
            }
        }

        /// <summary>
        /// Requires m_emailIndex to be updated.
        /// 
        /// SIDE EFFECT:
        ///   Sends out emails.
        /// </summary>
        public void SendEmails()
        {
            int counter = 0;
            foreach (var kv in m_emailIndex)
            {
                // var address = kv.Key
                var email = kv.Value;
                // Send out the email. It'll take care of all the HTML formatting.
                email.Send();

                // If some email in the middle of this loop fails,
                // we're in trouble.
                counter++;
            }
            Tools.LogInfo("TaskNotificationHandler::Queued " + counter.ToString() + " emails.");
        }

        public TaskNotificationEmail CreateEmail(Guid recipientId, Dictionary<string, TaskMessage> messageDict)
        {
            Guid brokerId = Guid.Empty;

            try
            {
                DbConnectionInfo.GetConnectionInfoByUserId(recipientId, out brokerId);
            }
            catch (NotFoundException)
            {
                // 4/25/2015 dd - Unable to locate recipient id. Do not create email task notification.
                return null; 
            }

            var recipientPrincipal = PrincipalFactory.RetrievePrincipalForUser(brokerId, recipientId, "?") as AbstractUserPrincipal;
            if (recipientPrincipal == null)
            {
                // 4/25/2015 dd - Unable to locate recipient id. Do not create email task notification.
                return null;
            }

            TaskNotificationEmail email = new TaskNotificationEmail(brokerId);

            foreach (var message in messageDict.Values)
            {
                // Sanitize text
                message.DueDate = TaskUtilities.SanitizeTaskText(message.DueDate, recipientPrincipal);

                // All of it
                for (int i = 0; i < message.TaskEntries.Count; i++)
                {
                    message.TaskEntries[i] = TaskUtilities.SanitizeTaskText(message.TaskEntries[i], recipientPrincipal);
                }

                // While we're here... let's grab the email address
                email.ToEmailAddress = message.ToEmailAddress;

                // And finally, add the recent task audit information to the email
                email.TaskMessages.AddLast(message);
            }

            return email;
        }

        /// <summary>
        /// Check if a user has permission to a specific task
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="userId"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        private bool HasPermission(Guid brokerId, Guid userId, string taskId)
        {
            if (ConstStage.EnableTaskNotificationPermissionCaching)
            {
                var task = MemoryCacheUtilities.GetOrAddExisting(
                    brokerId.ToString("N") + "_" + taskId.ToLower(), 
                    () => this.RetrieveTaskImpl(brokerId, taskId), 
                    this.CacheDuration);

                return MemoryCacheUtilities.GetOrAddExisting(
                    brokerId.ToString("N") + "_" + userId.ToString("N") + "_" + taskId.ToLower(),
                    () => this.HasPermissionImpl(task, userId),
                    this.CacheDuration);
            }
            else
            {
                var task = this.RetrieveTaskImpl(brokerId, taskId);
                return this.HasPermissionImpl(task, userId);
            }
        }

        /// <summary>
        /// Retrieves a task given the task ID and broker ID.
        /// </summary>
        /// <param name="brokerId">
        /// The broker ID for the task.
        /// </param>
        /// <param name="taskId">
        /// The ID for the task.
        /// </param>
        /// <returns>
        /// The task.
        /// </returns>
        private Task RetrieveTaskImpl(Guid brokerId, string taskId)
        {
            try
            {
                return Task.RetrieveWithoutPermissionCheck(brokerId, taskId);
            }
            catch (TaskNotFoundException e)
            {
                Tools.LogError(e.Message + ". Skipping this task. Stack trace:\n" + e.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the user has permission
        /// to access the task.
        /// </summary>
        /// <param name="task">
        /// The task to use.
        /// </param>
        /// <param name="userId">
        /// The user to check.
        /// </param>
        /// <returns>
        /// True if the user has access, false otherwise.
        /// </returns>
        private bool HasPermissionImpl(Task task, Guid userId)
        {
            if (task == null)
            {
                return false;
            }

            E_UserTaskPermissionLevel taskAccessLevel = E_UserTaskPermissionLevel.None;
            try
            {
                taskAccessLevel = TaskPermissionProcessor.GetMergedTaskAccessLevel(task, userId);
            }
            catch (CBaseException e)
            {
                Tools.LogError(string.Format("Could not get access level for task: {0}, user: {1}. Skipping task.\n", task.TaskId, userId), e);
                return false; // We should still send out the email to the remaining recipients
            }
            if (taskAccessLevel == E_UserTaskPermissionLevel.None)
            {
                return false; // Skip this user, they shouldn't be able to see the changes
            }

            return true;
        }

        /// <summary>
        /// Retrieves email data for the employee to be notified.
        /// </summary>
        /// <param name="brokerId">
        /// The broker ID for the user.
        /// </param>
        /// <param name="userId">
        /// The ID for the user.
        /// </param>
        /// <returns>
        /// The email data.
        /// </returns>
        private TaskNotificationEmployeeEmailData GetEmployeeDataByUserId(Guid brokerId, Guid userId)
        {
            if (ConstStage.EnableTaskNotificationEmailCaching)
            {
                return MemoryCacheUtilities.GetOrAddExisting(
                    brokerId.ToString("N") + "_" + userId.ToString("N"),
                    () => this.GetEmployeeDataByUserIdImpl(brokerId, userId),
                    this.CacheDuration);
            }

            var employee = this.GetEmployeeByUserId(brokerId, userId);
            if (employee != null && employee.Retrieve())
            {
                return new TaskNotificationEmployeeEmailData()
                {
                    EmailOption = employee.TaskRelatedEmailOptionT,
                    EmailAddress = employee.Email
                };
            }

            return null;
        }

        /// <summary>
        /// Retrieves employee email data from the database when it does not already
        /// exist in the cache.
        /// </summary>
        /// <param name="brokerId">
        /// The broker ID for the user.
        /// </param>
        /// <param name="userId">
        /// The ID for the user.
        /// </param>
        /// <returns>
        /// The email data from the database.
        /// </returns>
        private TaskNotificationEmployeeEmailData GetEmployeeDataByUserIdImpl(Guid brokerId, Guid userId)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@UserId", userId)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "TaskNotification_GetEmployeeEmailDataFromUserId", parameters))
            {
                if (reader.Read())
                {
                    var userType = reader.SafeString("Type");
                    var employeeTaskEmailOption = (E_TaskRelatedEmailOptionT)reader.SafeInt("EmployeeTaskEmailOption");
                    var employeePermissionPopulation = (EmployeePopulationMethodT)reader.SafeInt("PopulatePMLPermissionsT");

                    var originatingCompanyTaskEmailOption = reader["OriginatingCompanyTaskEmailOption"] == DBNull.Value 
                        ? default(E_TaskRelatedEmailOptionT?) 
                        : (E_TaskRelatedEmailOptionT)reader["OriginatingCompanyTaskEmailOption"];

                    var taskEmailOption = EmployeeDB.ResolveTaskEmailSetting(
                        userType, 
                        employeeTaskEmailOption, 
                        employeePermissionPopulation, 
                        originatingCompanyTaskEmailOption);

                    return new TaskNotificationEmployeeEmailData()
                    {
                        EmailOption = taskEmailOption,
                        EmailAddress = reader.SafeString("Email")
                    };
                }

                return null;
            }
        }

        /// <summary>
        /// Returns the EmployeeDB of a user given a userId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private EmployeeDB GetEmployeeByUserId(Guid brokerId, Guid userId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", userId)
                                        };

            // Get the employee information
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetEmployeeIdFromUserId", parameters))
            {
                if (!reader.Read())
                {
                    return null; // Can't find this broker. Skip.
                }

                var employeeId = new Guid(reader["EmployeeId"].ToString());

                return new EmployeeDB(employeeId, brokerId);
            }
        }

        #region DisplayNameMapping

        private List<Tuple<Guid, Guid, string>> m_knownUsers = new List<Tuple<Guid, Guid, string>>();

        /// <summary>
        /// Get fullname of a user from userId and BrokerId. The return is cache to a list.
        /// </summary>
        /// <param name="userId">Guid value for user Id.</param>
        /// <param name="brokerId">Guid value for groupId.</param>
        /// <returns>Return the name of that userId, if userId doesn't exist return empty</returns>
        private string GetUserDisplayName(Guid userId, Guid brokerId)
        {
            if (userId == Guid.Empty || brokerId == Guid.Empty) return string.Empty;

            var knownUser = m_knownUsers.Find(x => (x.Item1 == userId && x.Item2 == brokerId));
            if (knownUser == null)
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@EmployeeUserID", userId)
                                            };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetEmployeeNameByUserId", parameters))
                {
                    string userName;
                    if (reader.Read())
                    {
                        userName = reader["FirstLast"].ToString();
                        
                    }
                    else
                    {
                        userName = string.Empty;
                    }
                    knownUser = new Tuple<Guid, Guid, string>(userId, brokerId, userName);
                    m_knownUsers.Add(knownUser);
                }
            }

            return knownUser.Item3;
        }

        #endregion
    }
}