﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using LendersOffice.Security;
using System.Threading;

namespace LendersOffice.ObjLib.Task
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// If you change this class, make sure the following areas are updated as well
    /// Task.cs:EnqueueTaskNotification,
    /// TaskNotificationTest.cs
    /// TaskNotificationHandler.cs
    /// </remarks>
    public class TaskNotification
    {
        public string EmailedFrom;
        public Guid PreviousAssignedUserId;
        public Guid CurrentAssignedUserId;

        // Header
        public string TaskId;
        public string TaskDueDate;
        public string LoanNumCached;
        public string BorrowerNmCached;
        public string TaskSubject;
        public Guid BrokerId;
        public E_TaskStatus TaskStatus = E_TaskStatus.Active;

        // Recipients
        public List<Guid> Recipients = new List<Guid>();

        // Body
        public string Body;

        /// <summary>
        /// This constructor for testing purposes ONLY.
        /// </summary>
        public TaskNotification()
        {

        }

        /// <summary>
        /// Create a TaskNotification from an XDocument.
        /// CurrentAssignedUserId is not set.
        /// </summary>
        /// <param name="doc"></param>
        public TaskNotification(XDocument doc)
        {
            XElement root = doc.Element("Root");
            XElement header = root.Element("Header");

            XElement xEmailFrom = header.Element("EmailFrom");
            if (xEmailFrom != null)
            {
                this.EmailedFrom = xEmailFrom.Value;
            }
            //PreviousAssignedUserId set in recipients section;
            //CurrentAssignedUserId set in recipients too to load for task notification email.

            //*** HEADER
            this.TaskId = header.Element("TaskId").Value;
            this.TaskDueDate = header.Element("TaskDueDate").Value;
            this.LoanNumCached = header.Element("LoanNumCached").Value;
            this.BorrowerNmCached = header.Element("BorrowerNmCached").Value;
            this.TaskSubject = header.Element("TaskSubject").Value;
            this.BrokerId = new Guid(header.Element("BrokerId").Value);

            var statusElement = header.Element("TaskStatus");
            if (statusElement != null)
            {
                this.TaskStatus = (E_TaskStatus)Enum.Parse(typeof(E_TaskStatus), statusElement.Value);
            }

            //*** RECIPIENTS
            var recipients = root.Element("Recipients").Descendants("Recipient");
            foreach (var recipient in recipients)
            {
                Guid userId = new Guid(recipient.Attribute("userId").Value);
                if (recipient.Attribute("previousAssignee") != null)
                {
                    this.PreviousAssignedUserId = userId;
                }

                if (recipient.Attribute("currentAssignee") != null)
                {
                    this.CurrentAssignedUserId = userId;
                }
                this.Recipients.Add(userId);
            }

            //*** BODY
            this.Body = root.Element("Body").Value;
        }

        /// <summary>
        /// Create a TaskNotification from a Task
        /// </summary>
        /// <param name="task"></param>
        public TaskNotification(Task task, bool bypassSubscribers = false)
        {
            this.EmailedFrom = task.RecentlyEmailedFrom;
            this.PreviousAssignedUserId = task.TaskPreviousAssignedUserId;
            this.CurrentAssignedUserId = task.TaskAssignedUserId;

            //*** HEADER
            this.TaskId = task.TaskId;
            this.TaskDueDate = task.TaskSingleDueDateRaw;
            this.LoanNumCached = task.LoanNumCached;
            this.BorrowerNmCached = task.BorrowerNmCached;
            this.TaskSubject = task.TaskSubject;
            this.BrokerId = task.BrokerId;
            this.TaskStatus = task.TaskStatus;

            //*** RECIPIENTS
            // Subscribed users, the current assigned user, the previous assigned user
            var userIdSet = new HashSet<Guid>();
            userIdSet.Add(this.CurrentAssignedUserId);

            if (!bypassSubscribers)
            {
                var subscribedUserIds = TaskSubscription.GetSubscribedUserIds(this.BrokerId, this.TaskId);
                userIdSet.UnionWith(subscribedUserIds);
            }

            if (this.PreviousAssignedUserId != null)
            {
                userIdSet.Add(this.PreviousAssignedUserId); // the assigned user id when the task was loaded
            }

            // Remove the current user from the recipients list per spec 4.2.B
            // This assumes the current user made the most change that has caused this task to be enqueued
            Guid currentUserId;
            AbstractUserPrincipal currentUser = Thread.CurrentPrincipal as AbstractUserPrincipal;
            if (currentUser == null)
            {
                currentUserId = Guid.Empty;
            }
            else
            {
                currentUserId = currentUser.UserId;
            }

            userIdSet.Remove(currentUserId);
            userIdSet.Remove(Guid.Empty);

            this.Recipients = userIdSet.ToList();

            //*** BODY
            this.Body = task.RecentTaskHistoryHtmlRaw;

        }

        public XDocument ToXml()
        {
            XDocument xmlMessageData = new XDocument();
            XElement root = new XElement("Root");

            //*** HEADER
            XElement header = new XElement("Header");
            header.SetElementValue("TaskId", this.TaskId);
            header.SetElementValue("TaskDueDate", this.TaskDueDate);
            header.SetElementValue("LoanNumCached", this.LoanNumCached);
            header.SetElementValue("BorrowerNmCached", this.BorrowerNmCached);
            header.SetElementValue("TaskSubject", this.TaskSubject);
            header.SetElementValue("BrokerId", this.BrokerId);
            header.SetElementValue("TaskStatus", this.TaskStatus.ToString("D"));
            //header.SetElementValue("FromAddress", ); // FromAddress to be set in NotificationHandler

            // If a person emailed this task, that person should not get this notification
            // Store their address and notification handler should know what to do
            if (!string.IsNullOrEmpty(this.EmailedFrom))
            {
                header.SetElementValue("EmailFrom", this.EmailedFrom);
            }

            root.Add(header);

            //*** RECIPIENTS
            XElement recipients = new XElement("Recipients");
            foreach (var userId in this.Recipients)
            {
                var el = new XElement("Recipient");
                el.SetAttributeValue("userId", userId);

                // Check if recipient is previous assignee
                if (userId == this.PreviousAssignedUserId)
                {
                    el.SetAttributeValue("previousAssignee", true);
                }

                // Check if recipient is current assignee.
                if (userId == this.CurrentAssignedUserId)
                {
                    el.SetAttributeValue("currentAssignee", true);
                }

                recipients.Add(el);
            }
            root.Add(recipients);

            //*** BODY
            XElement body = new XElement("Body", new XCData(this.Body));
            root.Add(body);

            xmlMessageData.Add(root);
            return xmlMessageData;
        }
    }
}
