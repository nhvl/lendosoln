﻿// <copyright file="ResolutionTriggerProcessor.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   6/10/2014 7:51:12 AM 
// </summary>

namespace LendersOffice.ObjLib.Task
{
    using System;
    using System.Collections.Generic;
    using global::ConfigSystem.Engine;
    using DataAccess;
    using LendersOffice.ConfigSystem;
    using LendersOffice.Security;

    /// <summary>
    /// Provides a cache for evaluating workflow triggers. The purpose of this 
    /// class is to minimize the performance impact of batch operations.
    /// </summary>
    public class ResolutionTriggerProcessor
    {
        /// <summary>
        /// The id of the broker for this processor.
        /// </summary>
        private Guid brokerId;

        /// <summary>
        /// The trigger names for this processor. The dependencies of these triggers
        /// will be loaded by the LoanValueEvaluator.
        /// </summary>
        private HashSet<string> triggers = null;

        /// <summary>
        /// The cache of trigger results by trigger name.
        /// </summary>
        private Dictionary<string, bool> triggerResultsByName = null;

        /// <summary>
        /// The ExecutingEngine for the broker of this instance.
        /// </summary>
        private ExecutingEngine executingEngine = null;

        /// <summary>
        /// The system wide ExecutingEngine.
        /// </summary>
        private ExecutingEngine systemEngine = null;

        /// <summary>
        /// The LoanValueEvaluator for this instance. Normal access should use
        /// the property, as this field is lazy loaded.
        /// </summary>
        private LoanValueEvaluator loanValueEvaluator = null;

        /// <summary>
        /// Initializes a new instance of the ResolutionTriggerProcessor class.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="principal">The principal of the user resolving the tasks.</param>
        /// <param name="triggers">The names of the triggers that will be processed.</param>
        public ResolutionTriggerProcessor(Guid loanId, Guid brokerId, AbstractUserPrincipal principal, IEnumerable<string> triggers)
        {
            if (principal == null)
            {
                throw new ArgumentNullException(
                    "principal",
                    "Cannot create an instance of the ResolutionTriggerProcessor class with a null principal");
            }

            this.LoanId = loanId;
            this.brokerId = brokerId;
            this.Principal = principal;
            this.triggers = new HashSet<string>(triggers);
            this.triggerResultsByName = new Dictionary<string, bool>();

            var configRepository = ConfigHandler.GetRepository(this.brokerId);
            this.executingEngine = ExecutingEngine.GetEngineByBrokerId(configRepository, this.brokerId);
            this.systemEngine = this.executingEngine.SystemExecutingEngine; // Cache the system engine.
        }

        /// <summary>
        /// Gets the id of the loan for which this processor will cache trigger results.
        /// </summary>
        /// <value>
        /// The id of the loan for this instance.
        /// </value>
        public Guid LoanId { get; private set; }

        /// <summary>
        /// Gets the user principal for this processor.
        /// </summary>
        /// <value>
        /// The principal for this processor.
        /// </value>
        public AbstractUserPrincipal Principal { get; private set; }

        /// <summary>
        /// Gets the LoanValueEvaluator for this instance. This is lazy loaded.
        /// </summary>
        /// <value>
        /// The LoanValueEvaluator for this instance.
        /// </value>
        private LoanValueEvaluator LoanValueEvaluator
        {
            get
            {
                if (this.loanValueEvaluator == null)
                {
                    var dependencies = new HashSet<string>();

                    foreach (var trigger in this.triggers)
                    {
                        bool lenderWorkflowContainsTrigger = this.executingEngine.HasCustomVariable(trigger);
                        bool systemWorkflowContainsTrigger = this.systemEngine.HasCustomVariable(trigger);

                        if (!lenderWorkflowContainsTrigger && !systemWorkflowContainsTrigger)
                        {
                            var warning = string.Format(
                                "[Task][ResolutionTriggerProcessor] Unable to find trigger with name {0} for broker id {1}.",
                                trigger,
                                this.brokerId);

                            Tools.LogWarning(warning);
                            continue;
                        }

                        dependencies.UnionWith(LendingQBExecutingEngine.GetDependencyFieldsByTrigger(this.brokerId, trigger));
                    }

                    this.loanValueEvaluator = new LoanValueEvaluator(dependencies, this.LoanId);

                    if (this.Principal is SystemUserPrincipal)
                    {
                        this.loanValueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(this.brokerId, (SystemUserPrincipal)this.Principal));
                    }
                    else
                    {
                        this.loanValueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(this.Principal));
                    }
                }

                return this.loanValueEvaluator;
            }
        }

        /// <summary>
        /// Evaluates the resolution block trigger for the given task.
        /// </summary>
        /// <param name="task">The task to evaluate.</param>
        /// <param name="principal">The evaluating user principal.</param>
        /// <returns>
        /// True if the resolution block trigger evaluates to true. False if the 
        /// trigger evaluates to false or if it is not found.
        /// </returns>
        public static bool GetResolutionTriggerResult(Task task, AbstractUserPrincipal principal)
        {
            var processor = new ResolutionTriggerProcessor(
                task.LoanId,
                task.BrokerId,
                principal,
                new string[] { task.ResolutionBlockTriggerName });

            return processor.GetTriggerResult(task.ResolutionBlockTriggerName);
        }

        /// <summary>
        /// Gets the result of the trigger.
        /// </summary>
        /// <param name="trigger">The name of the trigger.</param>
        /// <returns>
        /// True if the trigger evaluates to true. False if the trigger evaluates 
        /// to false or if the trigger is not found.
        /// </returns>
        public bool GetTriggerResult(string trigger)
        {
            if (this.triggerResultsByName.ContainsKey(trigger))
            {
                return this.triggerResultsByName[trigger];
            }
            else 
            {
                bool result = this.EvaluateTrigger(trigger);

                this.triggerResultsByName.Add(trigger, result);

                return result;
            }
        }

        /// <summary>
        /// Evaluates the trigger.
        /// </summary>
        /// <param name="trigger">The name of the trigger.</param>
        /// <returns>
        /// True if the trigger evaluates to true. False if the trigger evaluates 
        /// to false or if the trigger is not found.
        /// </returns>
        private bool EvaluateTrigger(string trigger)
        {
            if (this.executingEngine.HasCustomVariable(trigger))
            {
                return this.executingEngine.EvaluateCustomVar(trigger, this.LoanValueEvaluator);
            }
            else if (this.systemEngine.HasCustomVariable(trigger))
            {
                return this.systemEngine.EvaluateCustomVar(trigger, this.LoanValueEvaluator);
            }
            else
            {
                return false;
            }
        }
    }
}