﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;

namespace LendersOffice.ObjLib.Task
{
    public class LightCondition
    {
        public string Id { get; private set; }
        public string CondRowId { get; private set; }
        public int CategoryId { get; private set; }
        public string Subject { get; private set; }
        public string InternalNotes { get; private set; }
        public string AssignedTo { get; private set; }
        public string DueDateDescription { get; private set; }
        public bool IsHidden { get; private set; }

        public string Status { get; private set; }
        public string CreatedOn { get; private set; }

        public string Permission { get; private set; }
        public int PermissionLevelId { get; private set; }

        public string ClosedBy { get; private set; }
        public string ClosedDate { get; private set; }
        public string DueDateFieldId { get; private set; }
        public string DueDateFieldOffset { get; private set; }
        public bool IsDueDateLocked { get; private set; }
        public bool IsCurrentUserOwner { get; set; }

        // Used in the condition signoff page
        public IEnumerable<DocumentConditionAssociation.DocumentConditionAssociation> Associations { get; set; }
        public string LoanId { get; private set; }

        public string ResolutionBlockTriggerName { get; private set; }
        public string ResolutionDateSetterFieldId { get; private set; }

        public string Category
        {
            get;
            private set; 
        }

        public bool ConditionFulfilsAssociatedDocs { get; private set; }

        public LightCondition(AbstractUserPrincipal principal, Task task, bool showHiddenData, TaskPermissionProcessor processor)
        {
            Id = task.TaskId;
            
            CategoryId = task.CondCategoryId.Value;
            Subject = task.TaskSubject;
            
            if (showHiddenData)
            {
                IsHidden = task.CondIsHidden;
            }

            this.IsCurrentUserOwner = task.TaskOwnerUserId == PrincipalFactory.CurrentPrincipal.UserId;

            InternalNotes = task.CondInternalNotes;
            DueDateDescription = task.TaskSingleDueDate;

            AssignedTo = task.AssignedUserFullName;
            
            Status = task.TaskStatus_rep;
            CreatedOn = task.TaskCreatedDate.ToShortDateString();

            if (processor != null)
            {
                Permission = processor.Resolve(task).GetFriendlyDescription();
            }
            else
            {
                Permission = task.GetPermissionLevelFor(principal).GetFriendlyDescription();
            }
            PermissionLevelId = task.TaskPermissionLevelId;

            ClosedBy = task.ClosingUserFullName;
            ClosedDate = task.TaskClosedDate.HasValue ? task.TaskClosedDate.Value.ToShortDateString() : "";

            Category = task.CondCategoryId_rep;

            if (false == task.TaskDueDateLocked)
            {
                if (Task.CanViewField(BrokerUserPrincipal.CurrentPrincipal, task.TaskDueDateCalcField))
                {
                    DueDateFieldId = task.TaskDueDateCalcField;
                }
                else
                {
                    DueDateFieldId = "";
                }
                DueDateFieldOffset = task.TaskDueDateCalcDays.ToString();
            }

            IsDueDateLocked = task.TaskDueDateLocked;
            CondRowId = task.CondRowId.ToString("D");

            ConditionFulfilsAssociatedDocs = task.ConditionFulfilsAssociatedDocs();

            this.ResolutionBlockTriggerName = task.ResolutionBlockTriggerName;
            this.ResolutionDateSetterFieldId = task.ResolutionDateSetterFieldId;

            LoanId = task.LoanId.ToString();
        }
    }
}
