﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Security;

namespace LendersOffice.ObjLib.Task
{
    public class NightlyTaskRunnerProcessor : CommonProjectLib.Runnable.IRunnable
    {

        public string Description
        {
            get { return "Updates task assignments."; }
        }

        public static void PopulateLoansNeedToBeRun()
        {
            // 7/18/2011 dd - This method should be run after midnight.

            HashSet<Guid> loanIdList = new HashSet<Guid>(); // 9/13/2011 dd - This will only store unique Loan Id.

            DateTime startDate = DateTime.Today.Date.AddDays(-1);
            DateTime endDate = DateTime.Today.Date;



            // 7/18/2011 dd - Step 1 - List all the loans that contains task belong to user that recently modify.
            try
            {
                foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                {
                    SqlParameter[] parameters = {
                                                 new SqlParameter("@StartDate", startDate),
                                                 new SqlParameter("@EndDate", endDate)
                                             };
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "ListLoansNeedForNightlyBasedOnRecentModifyUser", parameters))
                    {
                        while (reader.Read())
                        {
                            Guid sLId = (Guid)reader["LoanId"];
                            loanIdList.Add(sLId);
                        }
                    }
                }
            }
            catch (SqlException exc)
            {
                Tools.LogErrorWithCriticalTracking("NightlyTaskRunnerProcessor", exc);
            }

            // 7/18/2011 dd - Step 2- List all loans that recently modified.
            try
            {
                SqlParameter[] parameters = {
                                                 new SqlParameter("@StartDate", startDate),
                                                 new SqlParameter("@EndDate", endDate)
                                             };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOTransient, "ListLoansNeedForNightlyBasedOnRecentModifyLoan", TimeSpan.FromMinutes(5).Seconds, parameters))
                {
                    while (reader.Read())
                    {
                        Guid sLId = (Guid)reader["LoanId"];
                        loanIdList.Add(sLId);
                    }

                }
            }
            catch (SqlException exc)
            {
                Tools.LogErrorWithCriticalTracking("NightlyTaskRunnerProcessor", exc);
            }


            // 9/13/2011 dd - Step3 - List all loans that has task due date today or previous day.
            try
            {
                foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                {
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "ListLoansNeedForNightlyBasedOnTaskDueDate", null))
                    {
                        while (reader.Read())
                        {
                            Guid sLId = (Guid)reader["LoanId"];
                            loanIdList.Add(sLId);
                        }
                    }
                }
            }
            catch (SqlException exc)
            {
                Tools.LogErrorWithCriticalTracking("NightlyTaskRunnerProcessor", exc);
            }
            Tools.LogInfo("NightlyTaskRunnerProcessor: There are " + loanIdList.Count + " loans need to run through nightly process.");
            foreach (var sLId in loanIdList)
            {
                SqlParameter[] parameters = { new SqlParameter("@sLId", sLId) };
                StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "TaskNightly_InsertLoan", 2, parameters);
            }
        }
        #region IRunnable Members

        public void Run()
        {
            if (ConstStage.ExecuteTaskNightlyInParallel)
            {
                this.RunParallel();
                return;
            }

            Stopwatch sw = Stopwatch.StartNew();
            Thread.CurrentPrincipal = SystemUserPrincipal.TaskSystemUser;
            int successfulCount = 0;
            int errorCount = 0;
            int skipCount = 0;
            while (true)
            {
                Guid sLId = Guid.Empty;
                long id = 0;
                try
                {

                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOTransient, "TaskNightly_GetTopLoanId"))
                    {
                        if (reader.Read())
                        {
                            id = (long)reader["Id"];
                            sLId = (Guid)reader["sLId"];
                        }
                        else
                        {
                            break; // Terminate loop.
                        }
                    }
                    if (sLId != Guid.Empty)
                    {
                        if (IsQualifyToRunNightlyTask(sLId))
                        {
                            NightlyTaskRunner runner = new NightlyTaskRunner(sLId);
                            runner.Process();
                            successfulCount++;
                        }
                        else
                        {
                            skipCount++;
                        }
                    }
                }
                catch (CBaseException exc)
                {
                    errorCount++;
                    Tools.LogError("NightlyTaskRunnerProcessor: Unable to process sLId=" + sLId + ". " + exc.DeveloperMessage + Environment.NewLine + exc.StackTrace);
                }
                catch (SqlException exc)
                {
                    errorCount++;
                    Tools.LogError("NightlyTaskRunnerProcessor: Unable to process sLId=" + sLId + ". " + exc.Message + Environment.NewLine + exc.StackTrace);
                }
                finally
                {
                    SqlParameter[] parameters = { new SqlParameter("@Id", id) };
                    StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "TaskNightly_DeleteById", 2, parameters);

                }
            }
            sw.Stop();
            Tools.LogInfo("NightlyTaskRunnerProcessor: Processed " + successfulCount + " success loans. " + errorCount + " error loans. " + skipCount + " skip loans. Executes in " + sw.ElapsedMilliseconds + "ms.");
        }

        private void RunParallel()
        {
            // The NightlyTaskRunner loads up a loan file to process
            // tasks on the loan, which requires obtaining the field 
            // info table. To prevent multiple threads from running
            // the initialization process before the table is initialized, 
            // initialize the table here.
            var instance = CFieldInfoTable.GetInstance();

            // Use the instance to make sure the optimizer doesn't remove it.
            Tools.LogInfo("NightlyTaskRunnerProcessor: Initialized " + instance.GetType().Name + ". Starting run with MaxDegreeOfParallelism = " + ConstStage.MaxNightlyTaskThreads);

            // Use a two minute threshold to prevent timeouts when there
            // are many loans to process.
            const int timeout = 120;

            Stopwatch sw = Stopwatch.StartNew();
            Thread.CurrentPrincipal = SystemUserPrincipal.TaskSystemUser;

            var loanList = new LinkedList<KeyValuePair<long, Guid>>();
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOTransient, "TaskNightly_GetLoansForRunner", customTimeoutInSeconds: timeout, parameters: null))
            {
                while (reader.Read())
                {
                    loanList.AddLast(new KeyValuePair<long, Guid>((long)reader["Id"], (Guid)reader["sLId"]));
                }
            }

            var errorCount = 0;

            var parallelOptions = new ParallelOptions()
            {
                MaxDegreeOfParallelism = ConstStage.MaxNightlyTaskThreads
            };

            Parallel.ForEach(
                loanList, 
                parallelOptions,
                kvp =>
            {
                try
                {
                    if (IsQualifyToRunNightlyTask(kvp.Value))
                    {
                        var runner = new NightlyTaskRunner(kvp.Value);
                        runner.Process();
                    }
                }
                catch (Exception exc)
                {
                    Tools.LogError("NightlyTaskRunnerProcessor: Unable to process sLId=" + kvp.Value, exc);
                    Interlocked.Increment(ref errorCount);
                }

                try
                {
                    SqlParameter[] parameters = { new SqlParameter("@Id", kvp.Key) };
                    StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "TaskNightly_DeleteByIdNoTransaction", 2, parameters);
                }
                catch (Exception exc)
                {
                    Tools.LogError("NightlyTaskRunnerProcessor: Unable to delete from transient table id=" + kvp.Key, exc);
                }
            });

            sw.Stop();

            Tools.LogInfo("NightlyTaskRunnerProcessor: Processed sucessfully. " + errorCount + " error loans. Executes in " + sw.ElapsedMilliseconds + "ms.");
        }
        #endregion

        private static bool IsQualifyToRunNightlyTask(Guid sLId)
        {
            // 7/3/2014 dd - Check to see if the loan is belong to broker with IsUseNewCondition turn on AND
            //               loan is valid and not a template

            Guid brokerId = Guid.Empty;

            DbConnectionInfo.GetConnectionInfoByLoanId(sLId, out brokerId);

            bool ret = false;

            SqlParameter[] parameters = {
                                            new SqlParameter("@sLId", sLId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "IsQualifyRunNightlyTask", parameters))
            {
                if (reader.Read())
                {
                    ret = true;
                }
            }

            return ret;
        }
    }
}
