﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using LendersOffice.Constants;
using DataAccess;
using LendersOffice.ObjLib.DatabaseMessageQueue;

namespace LendersOffice.ObjLib.Task
{
    /// <summary>
    /// 4/30/2015 dd - 
    /// Convert this class to continuous process.
    /// Only one server run this processor. The old code does not implement to be run on multiple servers.
    /// </summary>
    public class TaskNotificationQueueProcessor : CommonProjectLib.Runnable.IRunnable
    {
        /// <summary>
        /// Gets the description of the continuous process.
        /// </summary>
        /// <value>The description of the continuous process.</value>
        public string Description
        {
            get { return "Process Task Notification."; }
        }

        public void Run()
        {
            Tools.ResetLogCorrelationId();
            TaskNotificationHandler handler = new TaskNotificationHandler();
            handler.Process();
        }

        /// <summary>
        /// Submit a task notification to the queue
        /// </summary>
        /// <param name="taskId">Task ID for the notification</param>
        /// <param name="text">Notification data in XML format</param>
        /// <param name="extra">Any extra information that will show up in LOAdmin</param>
        public static void SubmitNotification(string taskId, string text, string extra)
        {
            if (ConstSite.IsTaskMigrationProcess)
            {
                // 3/3/2012 dd - DO NOT update task queue during task migration
                // because we do not want to generate massive email during this process.
                return;
            }

            DBMessageQueue mQ = new DBMessageQueue(ConstMsg.Task2011Queue);
            mQ.Send(taskId, text, extra);
        }
    }

}