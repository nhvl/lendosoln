﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Constants;
using LendersOffice.Security;
using System.Diagnostics;
using LendersOffice.ConfigSystem;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.ConfigSystem.Operations;

namespace LendersOffice.ObjLib.Task
{
    public class NightlyTaskRunner
    {
        private Dictionary<Guid, Guid> m_employeeIdToUserIdDictionary = new Dictionary<Guid, Guid>();
        private Dictionary<Guid, AbstractUserPrincipal> m_userIdPrincipalDictionary = new Dictionary<Guid, AbstractUserPrincipal>();
        private Dictionary<Guid, bool> m_userIdCanAccessLoanDictionary = new Dictionary<Guid, bool>();
        private Dictionary<Guid, Guid> m_roleLastModifiedUserIdDictionary = new Dictionary<Guid, Guid>();
        private Dictionary<Guid, Guid> m_userIdUserIdDictionary = new Dictionary<Guid, Guid>();
        private Dictionary<Guid, EmployeeDB> m_userIdEmployeeDBDictionary = new Dictionary<Guid, EmployeeDB>();
        private Dictionary<Guid, List<Guid>> m_userIdRoleIdListDictionary = new Dictionary<Guid, List<Guid>>();
        private CPageData m_dataLoan = null;
        private LoanValueEvaluator m_loanValueEvaluator = null;


        public Guid LoanId { get; private set; }
        public Guid BrokerId { get; private set; }

        private BrokerDB x_brokerDB = null;

        private BrokerDB m_brokerDB
        {
            get
            {
                if (x_brokerDB == null)
                {
                    x_brokerDB = BrokerDB.RetrieveById(BrokerId);
                }
                return x_brokerDB;
            }
        }
        public NightlyTaskRunner(Guid sLId)
        {
            m_dataLoan = CreatePageData(sLId);
            m_dataLoan.AllowLoadWhileQP2Sandboxed = true;
            m_dataLoan.InitLoad();

            LoanId = sLId;
            BrokerId = m_dataLoan.sBrokerId;

        }
        private CPageData CreatePageData(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(NightlyTaskRunner));
        }


        public void Process()
        {

            if (m_brokerDB.IsUseNewTaskSystem == false)
            {
                return;
            }
            m_loanValueEvaluator = new LoanValueEvaluator(BrokerId, LoanId, WorkflowOperations.ReadLoanOrTemplate);

            List<Task> taskList = Task.GetTasksInLoan(BrokerId, LoanId);

            bool bNeedUpdateLoanTaskStatistics = false;

            foreach (Task task in taskList)
            {
                Process(task);
                if (bNeedUpdateLoanTaskStatistics == false && task.TaskDueDate.HasValue)
                {
                    TimeSpan ts = DateTime.Today.Subtract(task.TaskDueDate.Value);
                    if (ts.Days >= -1 && ts.Days <= 120)
                    {
                        // 7/15/2011 dd - Since the nightly update run after 12:00AM. Therefore,
                        // the only time we need to update loan task statistics is if there is task that has due date yesterday.
                        // or due today.

                        bNeedUpdateLoanTaskStatistics = true;
                    }
                }
            }
            if (bNeedUpdateLoanTaskStatistics)
            {
                TaskUtilities.UpdateTasksDueDateByLoan(BrokerId, LoanId);
            }
            
        }
        private bool Process(Task task)
        {
            if (task.TaskStatus == E_TaskStatus.Closed)
            {
                return false;
            }
            if (task.TaskStatus == E_TaskStatus.Resolved)
            {
                // 7/3/2014 dd - Task.Save will throw exception if assignment occur when task is in resolved status.
                return false;
            }
            StringBuilder debuggingMessage = new StringBuilder();
            debuggingMessage.AppendLine("NightlyTaskRunner.Process");

            bool isAssignmentChanged = ProcessTaskAssignment(task, debuggingMessage);

            bool isOwnerChanged = ProcessTaskOwner(task, debuggingMessage);


            if (isAssignmentChanged || isOwnerChanged)
            {
                task.EnforcePermissions = false; // No need to enforce permission check.
                task.DeveloperAuditMessage = debuggingMessage.ToString();
                task.Save(false);
            }

            return isAssignmentChanged || isOwnerChanged;
        }

        private bool ProcessTaskOwner(Task task, StringBuilder debuggingMessage)
        {
            if (task.TaskOwnerUserId == Guid.Empty)
            {
                return CheckTaskOwnershipNoExplicitUser(task, debuggingMessage);
            }
            else
            {
                return CheckTaskOwnershipWithExplicitUser(task, debuggingMessage);
            }
        }
        private bool CheckTaskOwnershipNoExplicitUser(Task task, StringBuilder debuggingMessage)
        {
            if (m_dataLoan.sStatusT == E_sStatusT.Loan_Canceled || m_dataLoan.sStatusT == E_sStatusT.Loan_Rejected)
            {
                return false;
            }
            if (task.TaskToBeOwnerRoleId.HasValue)
            {
                debuggingMessage.AppendLine("    Task To Be Own To RoleId=" + task.TaskToBeOwnerRoleId.Value);

                Guid userId = GetUserIdByRole(task.TaskToBeOwnerRoleId.Value);
                debuggingMessage.AppendLine("    Task To Be Own Base On RoleId. UserId=" + userId);
                if (userId != Guid.Empty)
                {
                    if (IsUserHasTaskAccess(task, userId, debuggingMessage))
                    {
                        debuggingMessage.AppendLine("    Assign Task Owner to UserId=" + userId);
                        task.TaskOwnerUserId = userId;
                        return true;
                    }
                }
                if (task.TaskToBeOwnerRoleId.Value == CEmployeeFields.s_CloserId ||
                    task.TaskToBeOwnerRoleId.Value == CEmployeeFields.s_AccountantId ||
                    task.TaskToBeOwnerRoleId.Value == CEmployeeFields.s_ShipperId ||
                    task.TaskToBeOwnerRoleId.Value == CEmployeeFields.s_AdministratorRoleId)
                {
                    userId = FindLastModifiedUserWithRole(task.TaskToBeOwnerRoleId.Value);
                    debuggingMessage.AppendLine("    User id with role last modify loan. UserId=" + userId);
                    if (userId != Guid.Empty)
                    {
                        if (IsUserHasTaskAccess(task, userId, debuggingMessage))
                        {
                            debuggingMessage.AppendLine("    Assign Task Owner to UserId=" + userId);

                            task.TaskOwnerUserId = userId;
                            return true;
                        }
                    }
                }

            }
            return false;
        }
        private Guid FindLastModifiedUserWithRole(Guid role)
        {
            Guid userId = Guid.Empty;

            if (m_roleLastModifiedUserIdDictionary.TryGetValue(role, out userId) == false)
            {

                // Get a list of unique modify user order by last modified date in descending order.
                SqlParameter[] parameters = {
                                                new SqlParameter("@sLId", this.LoanId)
                                            };
                List<Guid> userIdList = new List<Guid>();

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOTransient, "LoanModification_GetUniqueUserIdModifiedByLoan", parameters))
                {
                    while (reader.Read())
                    {
                        userIdList.Add((Guid)reader["UserId"]);
                    }

                }

                // 7/3/2014 dd - For each user find the first user with the role.

                foreach (var o in userIdList)
                {
                    List<Guid> roleList = GetRoleIdListByUserId(o);
                    if (roleList.Contains(role))
                    {
                        userId = o;
                        break;
                    }
                }

                m_roleLastModifiedUserIdDictionary.Add(role, userId);

            }
            return userId;
        }
        private bool CheckTaskOwnershipWithExplicitUser(Task task, StringBuilder debuggingMessage)
        {
            if (IsUserHasTaskAccess(task, task.TaskOwnerUserId, debuggingMessage))
            {
                return false;
            }

            if (IsUserAssignToLoan(task.TaskOwnerUserId) == false)
            {
                debuggingMessage.AppendLine("   Current Task Owner is no longer assign to loan. TaskOwnerUserId=" + task.TaskOwnerUserId);
                Guid userId = GetCurrentAssignedUserIdBaseOnUser(task.TaskOwnerUserId);
                debuggingMessage.AppendLine("   Get new userId base on TaskOwnerUserId. UserId=" + userId);
                if (userId != Guid.Empty)
                {
                    if (IsUserHasTaskAccess(task, userId, debuggingMessage))
                    {
                        debuggingMessage.AppendLine("    Assign Task Owner to UserId=" + userId);
                        task.TaskOwnerUserId = userId;
                        return true;
                    }
                }
            }

            // User is currently assign to loan but lose access.
            // Move up to TaskOwner then Task Opener then AccountUser

            else if (task.TaskCreatedByUserId != task.TaskOwnerUserId && IsUserHasTaskAccess(task, task.TaskCreatedByUserId, debuggingMessage))
            {
                debuggingMessage.AppendLine("    Assign Task Owner To Task Opener. TaskCreatedByUserId=" + task.TaskOwnerUserId);
                task.TaskOwnerUserId = task.TaskCreatedByUserId;
                return true;
            }
            else
            {
                Guid accountOwnerUserId = m_brokerDB.AccountOwnerUserId;
                if (IsUserHasTaskAccess(task, accountOwnerUserId, debuggingMessage))
                {
                    debuggingMessage.AppendLine("    Assign Task Owner to Account Owner.");
                    task.TaskAssignedUserId = accountOwnerUserId;
                    return true;
                }
            }
            return false;
        }
        private Guid GetCurrentAssignedUserIdBaseOnUser(Guid targetUserId)
        {
            // 7/18/2011 dd - Base from the specs, if the target user is no longer assign to loan
            // then we need to get the role of the target user and find current user assign to the loan with that same role.
            // If the role are (Manager, Processor, AE, Lock Desk, Underwriter) and there is loan officer assign to loan
            // then use LO employee relationship.

            Guid userId = Guid.Empty;
            Guid[] roleInRelationshipList = { 
                                                CEmployeeFields.s_ManagerRoleId,
                                                CEmployeeFields.s_ProcessorRoleId,
                                                CEmployeeFields.s_LenderAccountExecRoleId,
                                                CEmployeeFields.s_LockDeskRoleId,
                                                CEmployeeFields.s_UnderwriterRoleId,
                                                CEmployeeFields.s_JuniorProcessorId,
                                                CEmployeeFields.s_JuniorUnderwriterId
                                            };
            
            if (m_userIdUserIdDictionary.TryGetValue(targetUserId, out userId) == false)
            {
                // Find all the role of the user.
                List<Guid> roleIdList = GetRoleIdListByUserId(targetUserId);
                
                foreach (var roleId in roleIdList)
                {
                    userId = GetUserIdByRole(roleId);
                    if (userId == Guid.Empty && roleInRelationshipList.Contains(roleId))
                    {
                        Guid loanOfficerId = GetUserIdByRole(CEmployeeFields.s_LoanRepRoleId);
                        if (loanOfficerId != Guid.Empty)
                        {

                            EmployeeDB employee = m_userIdEmployeeDBDictionary[loanOfficerId];


                            Guid associateEmployeeId = Guid.Empty;
                            if (roleId == CEmployeeFields.s_ManagerRoleId)
                            {
                                associateEmployeeId = employee.ManagerEmployeeID;
                            }
                            else if (roleId == CEmployeeFields.s_ProcessorRoleId)
                            {
                                associateEmployeeId = employee.ProcessorEmployeeID;
                            }
                            else if (roleId == CEmployeeFields.s_LenderAccountExecRoleId)
                            {
                                associateEmployeeId = employee.LenderAcctExecEmployeeID;
                            }
                            else if (roleId == CEmployeeFields.s_LockDeskRoleId)
                            {
                                associateEmployeeId = employee.LockDeskEmployeeID;
                            }
                            else if (roleId == CEmployeeFields.s_UnderwriterRoleId)
                            {
                                associateEmployeeId = employee.UnderwriterEmployeeID;
                            }
                            else if (roleId == CEmployeeFields.s_JuniorUnderwriterId)
                            {
                                associateEmployeeId = employee.JuniorUnderwriterEmployeeID;
                            }
                            else if (roleId == CEmployeeFields.s_JuniorProcessorId)
                            {
                                associateEmployeeId = employee.JuniorProcessorEmployeeID;
                            }
                            if (associateEmployeeId != Guid.Empty)
                            {
                                userId = GetUserIdByEmployeeId(associateEmployeeId);
                            }
                            
                        }
                    }
                    if (userId != Guid.Empty)
                    {
                        break;
                    }
                }
                m_userIdUserIdDictionary.Add(targetUserId, userId);
            }

            return userId;
            
        }
        private bool ProcessTaskAssignment(Task task, StringBuilder debuggingMessage)
        {
            if (task.TaskAssignedUserId == Guid.Empty)
            {
                debuggingMessage.AppendLine("    Task has no explicit assigned user.");
                return CheckTaskAssignmentNoExplicitUserAssign(task, debuggingMessage);
            }
            else
            {
                return CheckTaskAssignmentWithExplicitUserAssign(task, debuggingMessage);
            }
        }
        private bool CheckTaskAssignmentNoExplicitUserAssign(Task task, StringBuilder debuggingMessage)
        {
            if (m_dataLoan.sStatusT == E_sStatusT.Loan_Canceled || m_dataLoan.sStatusT == E_sStatusT.Loan_Rejected)
            {
                return false;
            }
            if (task.TaskToBeAssignedRoleId.HasValue)
            {
                debuggingMessage.AppendLine("    Task To Be Assign To RoleId=" + task.TaskToBeAssignedRoleId.Value);
                Guid userId = GetUserIdByRole(task.TaskToBeAssignedRoleId.Value);
                debuggingMessage.AppendLine("    User currently assigned to role is UserId=" + userId);
                if (userId != Guid.Empty)
                {
                    if (IsUserHasTaskAccess(task, userId, debuggingMessage))
                    {
                        debuggingMessage.AppendLine("    User has access to task. Assign to this user.");
                        task.TaskAssignedUserId = userId;
                        return true;
                    }
                }

                if (task.TaskToBeAssignedRoleId.Value == CEmployeeFields.s_CloserId ||
                    task.TaskToBeAssignedRoleId.Value == CEmployeeFields.s_AccountantId ||
                    task.TaskToBeAssignedRoleId.Value == CEmployeeFields.s_ShipperId ||
                    task.TaskToBeAssignedRoleId.Value == CEmployeeFields.s_AdministratorRoleId)
                {
                    userId = FindLastModifiedUserWithRole(task.TaskToBeAssignedRoleId.Value);
                    debuggingMessage.AppendLine("    User that last modified for this role. UserId=" + userId);
                    if (userId != Guid.Empty)
                    {
                        if (IsUserHasTaskAccess(task, userId, debuggingMessage))
                        {
                            debuggingMessage.AppendLine("    User has access to task. Assign to this user.");

                            task.TaskAssignedUserId = userId;
                            return true;
                        }
                    }
                }

            }
            return false;
        }
        private bool CheckTaskAssignmentWithExplicitUserAssign(Task task, StringBuilder debuggingMessage)
        {
            if (IsUserHasTaskAccess(task, task.TaskAssignedUserId, debuggingMessage))
            {
                return false;
            }

            if (IsUserAssignToLoan(task.TaskAssignedUserId) == false)
            {
                debuggingMessage.AppendLine("    Task Assigned user is no longer assign to loan.");
                Guid userId = GetCurrentAssignedUserIdBaseOnUser(task.TaskAssignedUserId);
                debuggingMessage.AppendLine("    Get UserId based on Task Assigned User. UserId=" + userId);
                if (userId != Guid.Empty)
                {
                    if (IsUserHasTaskAccess(task, userId, debuggingMessage))
                    {
                        debuggingMessage.AppendLine("    UserId has access to task. Assign loan to user. UserId=" + userId);
                        task.TaskAssignedUserId = userId;
                        return true;
                    }
                }


            }
            // User is currently assign to loan but lose access.
            // Move up to TaskOwner then Task Opener then AccountUser
            if (task.TaskOwnerUserId != task.TaskAssignedUserId && IsUserHasTaskAccess(task, task.TaskOwnerUserId, debuggingMessage))
            {
                debuggingMessage.AppendLine("     Current assigned user lose loan access. Assigned to Task Owner. TaskOwnerUserId=" + task.TaskOwnerUserId);
                task.TaskAssignedUserId = task.TaskOwnerUserId;
                return true;
            }
            else if (task.TaskCreatedByUserId != task.TaskOwnerUserId &&
                task.TaskCreatedByUserId != task.TaskAssignedUserId && IsUserHasTaskAccess(task, task.TaskCreatedByUserId, debuggingMessage))
            {
                debuggingMessage.AppendLine("     Current assigned user lose loan access. Assigned to Task Opener. TaskCreatedByUserId=" + task.TaskCreatedByUserId);

                task.TaskAssignedUserId = task.TaskCreatedByUserId;
                return true;
            }
            else
            {
                Guid accountOwnerUserId = m_brokerDB.AccountOwnerUserId;
                if (IsUserHasTaskAccess(task, accountOwnerUserId, debuggingMessage))
                {
                    debuggingMessage.AppendLine("     Current assigned user lose loan access. Assigned to Account Owner. AccountOwner=" + accountOwnerUserId);

                    task.TaskAssignedUserId = accountOwnerUserId;
                    return true;
                }
            }
            return false;

        }
        private bool IsUserAssignToLoan(Guid userId)
        {
            Guid[] roleIdList = {
                                    CEmployeeFields.s_LoanRepRoleId,
                                    CEmployeeFields.s_BrokerProcessorId,
                                    CEmployeeFields.s_ManagerRoleId,
                                    CEmployeeFields.s_RealEstateAgentRoleId,
                                    CEmployeeFields.s_UnderwriterRoleId,
                                    CEmployeeFields.s_LockDeskRoleId,
                                    CEmployeeFields.s_ProcessorRoleId,
                                    CEmployeeFields.s_CallCenterAgentRoleId,
                                    CEmployeeFields.s_LoanOpenerId,
                                    CEmployeeFields.s_LenderAccountExecRoleId,
                                    CEmployeeFields.s_CloserId,
                                    CEmployeeFields.s_ShipperId,
                                    CEmployeeFields.s_FunderRoleId,
                                    CEmployeeFields.s_PostCloserId,
                                    CEmployeeFields.s_InsuringId,
                                    CEmployeeFields.s_CollateralAgentId,
                                    CEmployeeFields.s_DocDrawerId,
                                    CEmployeeFields.s_CreditAuditorId,
                                    CEmployeeFields.s_DisclosureDeskId,
                                    CEmployeeFields.s_JuniorProcessorId,
                                    CEmployeeFields.s_JuniorUnderwriterId,
                                    CEmployeeFields.s_LegalAuditorId,
                                    CEmployeeFields.s_LoanOfficerAssistantId,
                                    CEmployeeFields.s_PurchaserId,
                                    CEmployeeFields.s_QCComplianceId,
                                    CEmployeeFields.s_SecondaryId,
                                    CEmployeeFields.s_ServicingId,
                                    CEmployeeFields.s_ExternalSecondaryId,
                                    CEmployeeFields.s_ExternalPostCloserId
                                };
            foreach (var roleId in roleIdList)
            {
                if (userId == GetUserIdByRole(roleId))
                {
                    return true;
                }
            }

            return false;
        }
        private bool IsUserHasTaskAccess(Task task, Guid userId, StringBuilder debuggingMessage)
        {
            debuggingMessage.AppendLine("Evaluate IsUserHasTaskAccess for UserId=" + userId);
            if (userId == Guid.Empty || userId == SystemUserPrincipal.TaskSystemUser.UserId)
            {
                return false;
            }

            AbstractUserPrincipal principal = null;
            if (m_userIdPrincipalDictionary.TryGetValue(userId, out principal) == false)
            {


                principal = PrincipalFactory.RetrievePrincipalForUser(task.BrokerId, userId, "?") as AbstractUserPrincipal;
                m_userIdPrincipalDictionary.Add(userId, principal);

            }

            if (principal == null)
            {
                debuggingMessage.AppendLine("    Could not locate user. Return False.");
                return false;
            }

            bool canAccessLoan = false;

            if (m_userIdCanAccessLoanDictionary.TryGetValue(userId, out canAccessLoan) == false)
            {
                try
                {
                    m_loanValueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));
                    canAccessLoan = LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, m_loanValueEvaluator);

                }
                catch (AccessDenied)
                {
                    canAccessLoan = false;
                }
                m_userIdCanAccessLoanDictionary.Add(userId, canAccessLoan);
            }

            if (canAccessLoan == false)
            {
                debuggingMessage.AppendLine("    User does not have permission to access loan. Return False.");
                return false;
            }


            var permissionLevel = TaskPermissionProcessor.GetTaskAccessLevel(task, principal);

            debuggingMessage.AppendLine("    PermissionLevel=" + permissionLevel + ". Return " + (permissionLevel != E_UserTaskPermissionLevel.None));
            return permissionLevel != E_UserTaskPermissionLevel.None;
        }
        private Guid GetUserIdByRole(Guid roleId)
        {
            Guid employeeId = Guid.Empty;

            if (roleId == CEmployeeFields.s_LoanRepRoleId)
            {
                employeeId = m_dataLoan.sEmployeeLoanRepId;
            }
            else if (roleId == CEmployeeFields.s_BrokerProcessorId)
            {
                employeeId = m_dataLoan.sEmployeeBrokerProcessorId;
            }
            else if (roleId == CEmployeeFields.s_ManagerRoleId)
            {
                employeeId = m_dataLoan.sEmployeeManagerId;
            }
            else if (roleId == CEmployeeFields.s_RealEstateAgentRoleId)
            {
                employeeId = m_dataLoan.sEmployeeRealEstateAgentId;
            }
            else if (roleId == CEmployeeFields.s_UnderwriterRoleId)
            {
                employeeId = m_dataLoan.sEmployeeUnderwriterId;
            }
            else if (roleId == CEmployeeFields.s_LockDeskRoleId)
            {
                employeeId = m_dataLoan.sEmployeeLockDeskId;
            }
            else if (roleId == CEmployeeFields.s_ProcessorRoleId)
            {
                employeeId = m_dataLoan.sEmployeeProcessorId;
            }
            else if (roleId == CEmployeeFields.s_CallCenterAgentRoleId)
            {
                employeeId = m_dataLoan.sEmployeeCallCenterAgentId;
            }
            else if (roleId == CEmployeeFields.s_LoanOpenerId)
            {
                employeeId = m_dataLoan.sEmployeeLoanOpenerId;
            }
            else if (roleId == CEmployeeFields.s_LenderAccountExecRoleId)
            {
                employeeId = m_dataLoan.sEmployeeLenderAccExecId;
            }
            else if (roleId == CEmployeeFields.s_CloserId)
            {
                employeeId = m_dataLoan.sEmployeeCloserId;
            }
            else if (roleId == CEmployeeFields.s_ShipperId)
            {
                employeeId = m_dataLoan.sEmployeeShipperId;
            }
            else if (roleId == CEmployeeFields.s_FunderRoleId)
            {
                employeeId = m_dataLoan.sEmployeeFunderId;
            }
            else if (roleId == CEmployeeFields.s_PostCloserId)
            {
                employeeId = m_dataLoan.sEmployeePostCloserId;
            }
            else if (roleId == CEmployeeFields.s_InsuringId)
            {
                employeeId = m_dataLoan.sEmployeeInsuringId;
            }
            else if (roleId == CEmployeeFields.s_CollateralAgentId)
            {
                employeeId = m_dataLoan.sEmployeeCollateralAgentId;
            }
            else if (roleId == CEmployeeFields.s_DocDrawerId)
            {
                employeeId = m_dataLoan.sEmployeeDocDrawerId;
            }
            else if (roleId == CEmployeeFields.s_CreditAuditorId)
            {
                employeeId = m_dataLoan.sEmployeeCreditAuditorId;
            }
            else if (roleId == CEmployeeFields.s_DisclosureDeskId)
            {
                employeeId = m_dataLoan.sEmployeeDisclosureDeskId;
            }
            else if (roleId == CEmployeeFields.s_JuniorProcessorId)
            {
                employeeId = m_dataLoan.sEmployeeJuniorProcessorId;
            }
            else if (roleId == CEmployeeFields.s_JuniorUnderwriterId)
            {
                employeeId = m_dataLoan.sEmployeeJuniorUnderwriterId;
            }
            else if (roleId == CEmployeeFields.s_LegalAuditorId)
            {
                employeeId = m_dataLoan.sEmployeeLegalAuditorId;
            }
            else if (roleId == CEmployeeFields.s_LoanOfficerAssistantId)
            {
                employeeId = m_dataLoan.sEmployeeLoanOfficerAssistantId;
            }
            else if (roleId == CEmployeeFields.s_PurchaserId)
            {
                employeeId = m_dataLoan.sEmployeePurchaserId;
            }
            else if (roleId == CEmployeeFields.s_QCComplianceId)
            {
                employeeId = m_dataLoan.sEmployeeQCComplianceId;
            }
            else if (roleId == CEmployeeFields.s_SecondaryId)
            {
                employeeId = m_dataLoan.sEmployeeSecondaryId;
            }
            else if (roleId == CEmployeeFields.s_ServicingId)
            {
                employeeId = m_dataLoan.sEmployeeServicingId;
            }
            else if (roleId == CEmployeeFields.s_ExternalSecondaryId)
            {
                employeeId = m_dataLoan.sEmployeeExternalSecondaryId;
            }
            else if (roleId == CEmployeeFields.s_ExternalPostCloserId)
            {
                employeeId = m_dataLoan.sEmployeeExternalPostCloserId;
            }
            else if (roleId == CEmployeeFields.s_CloserId ||
                roleId == CEmployeeFields.s_AccountantId ||
                roleId == CEmployeeFields.s_AdministratorRoleId)
            {
                // 7/18/2011 dd - These roles are not assignable in loan file.
                employeeId = Guid.Empty;
            }
            else
            {
                throw CBaseException.GenericException(roleId + " is not an assignable role in LoanFile");
            }

            if (employeeId == Guid.Empty)
            {
                return Guid.Empty;
            }

            return GetUserIdByEmployeeId(employeeId);

        }
        private Guid GetUserIdByEmployeeId(Guid employeeId)
        {
            Guid userId = Guid.Empty;
            if (m_employeeIdToUserIdDictionary.TryGetValue(employeeId, out userId) == false)
            {
                EmployeeDB employee = new EmployeeDB(employeeId, BrokerId);
                employee.Retrieve();
                userId = employee.UserID;

                m_employeeIdToUserIdDictionary.Add(employeeId, userId);
                if (m_userIdEmployeeDBDictionary.ContainsKey(userId) == false)
                {
                    m_userIdEmployeeDBDictionary.Add(userId, employee);
                }
            }

            return userId;
        }
        private List<Guid> GetRoleIdListByUserId(Guid userId)
        {
            List<Guid> list = null;

            if (m_userIdRoleIdListDictionary.TryGetValue(userId, out list) == false)
            {
                list = new List<Guid>();

                SqlParameter[] parameters = { new SqlParameter("@UserId", userId) };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, "ListRolesByUserId", parameters))
                {
                    while (reader.Read())
                    {
                        list.Add((Guid)reader["RoleId"]);
                    }
                }
                m_userIdRoleIdListDictionary.Add(userId, list);
            }
            return list;
        }
    }
}
