﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Admin;
using LendersOffice.Security;
using CommonProjectLib.Common.Lib;
using LendersOffice.Reminders;
using LendersOffice.ObjLib.Relationships;

namespace LendersOffice.ObjLib.Task
{
    /// <summary>
    /// Still working on this needs to be much more complex :(
    /// </summary>
    public class ConditionImporter  : IConditionImporter
    {

        private List<UniqueCondition> m_uniqueConditions;
        private List<ConditionDesc> m_updateConditions;
        private ConditionCategory m_defaultAusCategory = null;
        private CPageBase m_data;
        private Guid m_loanOfficerRoleId;
        private Guid m_underwriterRoleId;


        public string ImportSource { get; set; }
        // 9/7/2011 dd - OPM 70845 - When import condition from DU/LP/Total we only check condition description as unique.
        public bool IsSkipCategoryCheck { get; set; }

        public ConditionImporter(CPageBase data)
        {
            m_data = data;
            m_uniqueConditions = new List<UniqueCondition>();
            m_updateConditions = new List<ConditionDesc>();

            m_loanOfficerRoleId = Role.Get(E_RoleT.LoanOfficer).Id;
            m_underwriterRoleId = Role.Get(E_RoleT.Underwriter).Id;
        }

        public void AddUnique(string category, string desc)
        {
            if (string.IsNullOrEmpty(category))
            {
                category = "misc";
            }
            if (string.IsNullOrEmpty(desc))
            {
                return; //dont add empty conditions -- tis required
            }
            m_uniqueConditions.Add(new UniqueCondition(category, desc, ""));
        }

        public void AddUniqueAus(string desc)
        {
            AddUniqueAus(desc, "");
        }

        public void AddUniqueAus(string desc, string messageIdentifier)
        {
            
            if (m_defaultAusCategory == null)
            {

                if (m_data.BrokerDB.AusImportDefaultConditionCategoryId.HasValue == false)
                {
                    throw CBaseException.GenericException("AusImportDefaultConditionCategoryId for " + m_data.sBrokerId);
                }

                m_defaultAusCategory = ConditionCategory.GetCategory(m_data.sBrokerId, m_data.BrokerDB.AusImportDefaultConditionCategoryId.Value);
            }

            m_uniqueConditions.Add(new UniqueCondition(m_defaultAusCategory.Category, desc, messageIdentifier));
        }
        public void MergeIn(ConditionDesc desc)
        {
            if (string.IsNullOrEmpty(desc.Category))
            {
                desc.Category = "misc";
            }
            if (string.IsNullOrEmpty(desc.Description))
            {
                return;
            }

            m_updateConditions.Add(desc);
        }



        /// <summary>
        /// Call me after the changes have been applied.
        /// </summary>
        public void Save(AbstractUserPrincipal principal, Guid processorId, Guid loanOfficerId, Guid underwriterId)
        {
            if (m_uniqueConditions.Count == 0 && m_updateConditions.Count == 0)
            {
                return; //nothing to do
            }
            
            if (m_data.BrokerDB.DefaultTaskPermissionLevelIdForImportedCategories.HasValue == false)
            {
                throw CBaseException.GenericException("DefaultTaskPermissionLevelIdForImportedCategories must have a value in brokerdb.");
            }

            Guid taskToBeAssignedToUserId = Guid.Empty;
            int taskPermissionLevel = m_data.BrokerDB.DefaultTaskPermissionLevelIdForImportedCategories.Value;
            
            List<Task> currentConditions = Task.GetAllConditionsByLoanId(m_data.BrokerDB.BrokerID, m_data.sLId);
            Dictionary<string, ConditionCategory> categories = ConditionCategory.GetCategories(m_data.BrokerDB.BrokerID).
                ToDictionary(category => category.Category, StringComparer.OrdinalIgnoreCase);

            // Resolve employee roles
            if (processorId != Guid.Empty)
            {
                EmployeeDetails d = new EmployeeDetails(m_data.BrokerDB.BrokerID, processorId);
                taskToBeAssignedToUserId = d.UserId; 

            }
            else if( loanOfficerId != Guid.Empty )
            {
                // If there is a loan officer, look up his relationship for a 
                // processor.
                var relationships = new EmployeeRelationships();
                relationships.Retrieve(m_data.BrokerDB.BrokerID, loanOfficerId);

                var relationshipSet = relationships.GetApplicableRelationshipSet(
                    this.m_data.sBranchChannelT,
                    this.m_data.sCorrespondentProcessT);

                Relationship processorinfo = relationshipSet[E_RoleT.Processor];

                if (processorinfo != null)
                {
                    EmployeeDetails d = new EmployeeDetails(m_data.BrokerDB.BrokerID, processorinfo.EmployeeId);
                    taskToBeAssignedToUserId = d.UserId;
                }
                else
                {
                    EmployeeDetails d = new EmployeeDetails(m_data.BrokerDB.BrokerID, loanOfficerId);
                    taskToBeAssignedToUserId = d.UserId;
                }
            }

            if (underwriterId != Guid.Empty)
            {
                EmployeeDetails d = new EmployeeDetails(m_data.BrokerDB.BrokerID, underwriterId);
                underwriterId = d.UserId; 
            }

            if (underwriterId == Guid.Empty)
            {
                EmployeeRelationships relationships = new EmployeeRelationships();
                relationships.Retrieve(principal.BrokerId, principal.EmployeeId);

                var relationshipSet = relationships.GetApplicableRelationshipSet(
                    this.m_data.sBranchChannelT,
                    this.m_data.sCorrespondentProcessT);

                Relationship underwriterInfo = relationshipSet[E_RoleT.Underwriter];
                if (underwriterInfo != null)
                {
                    EmployeeDetails d = new EmployeeDetails(m_data.BrokerDB.BrokerID, underwriterInfo.EmployeeId);
                    underwriterId = d.UserId;
                }
            }
            if (taskToBeAssignedToUserId == Guid.Empty)
            {
                taskToBeAssignedToUserId = principal.UserId;
            }

            bool createdTask = false;

            HashSet<string> ambiguousMessageIds = FindAmbiguousMessageIds(currentConditions.Where(task => !task.CondIsDeleted));

            // Create conditions from m_uniqueConditions
            foreach (var conditionInfo in m_uniqueConditions)
            {
                string category = conditionInfo.Category;
                string taskSubject = conditionInfo.Description;
                string messageId = conditionInfo.MessageIdentifier;

                if (currentConditions.Any(task => (IsSkipCategoryCheck || task.CondCategoryId_rep.Equals(category, StringComparison.OrdinalIgnoreCase))
                    && task.TaskSubject.Equals(taskSubject, StringComparison.OrdinalIgnoreCase)
                    && (string.IsNullOrEmpty(messageId) || messageId == task.CondMessageId)))
                {
                    continue; // already exist ignore
                }

                if (!string.IsNullOrEmpty(messageId) && !ambiguousMessageIds.Contains(messageId))
                {
                    if (UpdateExistingConditionFromMessageId(currentConditions, messageId, taskSubject))
                    {
                        continue; // already updated an existing condition with the data
                    }
                }

                ConditionCategory conditionCategory;

                // If the category does not exist, add it
                if (false == categories.TryGetValue(category, out conditionCategory))
                {
                    conditionCategory = new ConditionCategory(principal.BrokerId);
                    conditionCategory.Category = category;
                    conditionCategory.DefaultTaskPermissionLevelId = taskPermissionLevel;
                    conditionCategory.Save();
                    categories.Add(category, conditionCategory);
                }

                CreateTask(principal, taskToBeAssignedToUserId, underwriterId, taskSubject, conditionCategory, false, null, DateTime.Today, messageId);
                createdTask = true;
            }

            //This loop is used by datatrac right now. Could change later though.
            foreach (ConditionDesc desc in m_updateConditions)
            {
                string taskSubject = desc.Description;
                string category = desc.Category;

                //find existing case to update
                Task currentTask = currentConditions.FirstOrDefault(task => task.CondCategoryId_rep.Equals(category, StringComparison.OrdinalIgnoreCase)
                        && task.TaskSubject.Equals(taskSubject, StringComparison.OrdinalIgnoreCase));

                if (currentTask != null )
                {
                    currentTask.EnforcePermissions = false;
                    if (currentTask.TaskStatus == E_TaskStatus.Closed && false == desc.IsDone)
                    {
                        currentTask.Reactivate();
                    }
                    else if ((currentTask.TaskStatus == E_TaskStatus.Active || currentTask.TaskStatus == E_TaskStatus.Resolved) && desc.IsDone)
                    {
                        currentTask.Close(principal.BrokerId, DateTime.Parse(desc.DateDone), desc.CondCompletedByUserNm);
                    }
                    //else the status matching and we have nothign to do 
                    continue;
                }

                ConditionCategory conditionCategory;
                //if the category does not exist add it
                if (false == categories.TryGetValue(category, out conditionCategory))
                {
                    conditionCategory = new ConditionCategory(principal.BrokerId);
                    conditionCategory.Category = category;
                    conditionCategory.DefaultTaskPermissionLevelId = taskPermissionLevel;
                    conditionCategory.Save();
                    categories.Add(category, conditionCategory);
                }

                DateTime dateClosed;
                if (false == desc.IsDone || false == DateTime.TryParse(desc.DateDone, out dateClosed))
                {
                    dateClosed = DateTime.Today;
                }
                
                CreateTask(principal, taskToBeAssignedToUserId, underwriterId, desc.Description, conditionCategory, desc.IsDone, desc.CondCompletedByUserNm, dateClosed, "");
                createdTask = true;
            }

            if (createdTask)
            {
                TaskUtilities.BumpLoanConditionSummaryVersionAndEnqueue(m_data.sBrokerId, m_data.sLId);
                TaskUtilities.EnqueueTasksDueDateUpdate(m_data.sBrokerId, m_data.sLId);
            }
        }

        private bool UpdateExistingConditionFromMessageId(List<Task> currentConditions, string messageId, string taskSubject)
        {
            foreach (var task in currentConditions)
            {
                if (task.CondMessageId == messageId && !task.CondIsDeleted)
                {
                    var oldEnforcePermissions = task.EnforcePermissions; // 3/13/14 gf - don't want to enforce Manage access to delete condition. This is a system operation.
                    task.EnforcePermissions = false;
                    task.SetImportingSource(ImportSource);
                    task.Comments = "Automatically updated from new findings";
                    task.TaskSubject = taskSubject;
                    if (task.TaskStatus != E_TaskStatus.Active)
                    {
                        task.Reactivate();
                    }
                    task.Save(true);
                    task.EnforcePermissions = oldEnforcePermissions;
                    return true;
                }
            }
            return false;
        }

        private void CreateTask(AbstractUserPrincipal principal, Guid loanOfficerId, Guid underwriterId, string taskSubject, ConditionCategory conditionCategory, bool isDone, string closedBy, DateTime closedOn, string messageId)
        {
            Task newTask = Task.CreateWithoutPermissionCheck(m_data.sLId, principal.BrokerId, m_data.sPrimaryNm, m_data.sLNm);
            newTask.TaskIsCondition = true;
            newTask.CondCategoryId = conditionCategory.Id;
            newTask.TaskPermissionLevelId = conditionCategory.DefaultTaskPermissionLevelId;
            newTask.TaskSubject = taskSubject;
            newTask.TaskDueDateLocked = false;
            newTask.TaskDueDateCalcField = "sEstCloseD";
            newTask.CondMessageId = messageId;
            if (false == string.IsNullOrEmpty(ImportSource))
            {
                newTask.SetImportingSource(ImportSource);
            }

            if (loanOfficerId != Guid.Empty)
            {
                newTask.TaskAssignedUserId = loanOfficerId;
            }
            else
            {
                newTask.TaskToBeAssignedRoleId = m_loanOfficerRoleId;
            }
            if (underwriterId != Guid.Empty)
            {
                newTask.TaskOwnerUserId = underwriterId;
            }
            else
            {
                newTask.TaskToBeOwnerRoleId = m_underwriterRoleId;
            }

            newTask.Save(false);

            if (isDone) //this stuff is for datatrac... nothing else tries to import completed items. If this changes we need to update at least the closed by.
            {
                newTask = Task.Retrieve(principal.BrokerId, newTask.TaskId);
                newTask.EnforcePermissions = false;
                newTask.Close(principal.BrokerId, closedOn, closedBy);
            }
        }

        /// <summary>
        /// Finds Message Ids with ambiguous values, i.e. either
        /// <para>A. Multiple imported conditions match</para>
        /// <para>B. One imported condition messageId matches multiple LendingQB conditions</para>
        /// </summary>
        /// <param name="currentNonDeletedConditions">
        /// The current conditions of the loan which are not deleted.
        /// <para>Since deleted conditions will not be updated, we should ignore them for purposes of ambiguity for updates.</para>
        /// </param>
        /// <returns>A set of all CondMessageIds which cannot be safely imported over existing conditions.</returns>
        private HashSet<string> FindAmbiguousMessageIds(IEnumerable<Task> currentNonDeletedConditions)
        {
            // Part A: Multiple imported conditions match
            HashSet<string> ambiguousMessageIds = new HashSet<string>();
            HashSet<string> importingMessageIds = new HashSet<string>();
            foreach (var conditionInfo in m_uniqueConditions)
            {
                if (!string.IsNullOrEmpty(conditionInfo.MessageIdentifier) && !importingMessageIds.Add(conditionInfo.MessageIdentifier))
                {
                    ambiguousMessageIds.Add(conditionInfo.MessageIdentifier);
                }
            }

            foreach (var messageId in ambiguousMessageIds)
            {
                StringBuilder conditionLog = new StringBuilder();
                conditionLog.AppendFormat(
                    "ConditionImporter detected one or more conditions from {0} with the same MessageIdentifier: '{1}'{2}sLId={3}{2}{2}",
                    ImportSource,
                    messageId,
                    Environment.NewLine,
                    m_data.sLId);
                foreach (var condition in m_uniqueConditions.Where(cond => cond.MessageIdentifier == messageId))
                {
                    conditionLog.AppendFormat(
                        "Category: '{1}'{0}Description: '{2}'{0}{0}",
                        Environment.NewLine,
                        condition.Category,
                        condition.Description);
                }

                conditionLog.AppendLine("Will create new conditions for each.");
                Tools.LogWarning(conditionLog.ToString());
            }

            // Part B: One imported condition ID matches multiple LendingQB conditions
            HashSet<string> currentCondMessageIds = new HashSet<string>();
            HashSet<string> duplicateLQBMessageIds = new HashSet<string>();
            foreach (var task in currentNonDeletedConditions)
            {
                if (!string.IsNullOrEmpty(task.CondMessageId) && !currentCondMessageIds.Add(task.CondMessageId))
                {
                    duplicateLQBMessageIds.Add(task.CondMessageId);
                }
            }

            foreach (var messageId in duplicateLQBMessageIds)
            {
                // only report ambiguity within LQB if trying to import a matching id.
                if (importingMessageIds.Contains(messageId))
                {
                    StringBuilder conditionLog = new StringBuilder();
                    conditionLog.AppendFormat(
                        "ConditionImporter detected one or more conditions in LQB with the same MessageIdentifier ('{0}') as a condition from {1}{2}sLId={3}{2}{2}",
                        messageId,
                        ImportSource,
                        Environment.NewLine,
                        m_data.sLId);
                    foreach (var task in currentNonDeletedConditions.Where(cond => cond.CondMessageId == messageId))
                    {
                        conditionLog.AppendFormat(
                            "TaskId: '{1}'{0}Category: '{2}'{0}Description: '{3}'{0}{0}",
                            Environment.NewLine,
                            task.TaskId,
                            task.CondCategoryId_rep,
                            task.TaskSubject);
                    }

                    conditionLog.AppendLine("Condition(s) from " + ImportSource + ":");
                    foreach (var condition in m_uniqueConditions.Where(cond => cond.MessageIdentifier == messageId))
                    {
                        conditionLog.AppendFormat("Category: '{1}'{0}Description: '{2}'{0}{0}",
                            Environment.NewLine,
                            condition.Category,
                            condition.Description);
                    }

                    conditionLog.AppendLine("Will create new conditions for each.");
                    Tools.LogWarning(conditionLog.ToString());
                    ambiguousMessageIds.Add(messageId);
                }
            }

            return ambiguousMessageIds;
        }
    }

    public class UniqueCondition
    {
        public string Category { get; set; }
        public string Description { get; set; }
        public string MessageIdentifier { get; set; }
        public UniqueCondition(string category, string description, string messageIdentifier)
        {
            Category = category;
            Description = description;
            MessageIdentifier = messageIdentifier;
        }
    }

    public interface IConditionImporter
    {
        void AddUnique(string category, string desc);
        void AddUniqueAus(string desc);
        void AddUniqueAus(string desc, string messageIdentifier);
        void MergeIn(ConditionDesc desc);
        // 9/7/2011 dd - OPM 70845 - When import condition from DU/LP/Total we only check condition description as unique.
        bool IsSkipCategoryCheck { get; set; }

    }
}
