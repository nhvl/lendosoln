﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;
using EDocs;

namespace LendersOffice.ObjLib.Task
{
    public enum E_AutoTaskFrequency
    {
        SingleUse = 0,
        Recurring = 1
    }
    public class TaskTriggerTemplate
    {

        private const string SP_Fetch = "TASK_TRIGGER_TEMPLATE_FETCH";
        private const string SP_Save = "TASK_TRIGGER_TEMPLATE_SAVE";
        private const string SP_DELETE = "TASK_TRIGGER_TEMPLATE_DELETE";

        public int AutoTaskTemplateId { get; private set; }
        public Guid BrokerId { get; private set; }
        public string Subject { get; set; }
        public string TriggerName { get; set; }
        public E_AutoTaskFrequency Frequency { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public Guid AssignedUserId { get; set; }
        public Guid? ToBeAssignedRoleId { get; set; }
        public Guid OwnerUserId { get; set; }
        public Guid? ToBeOwnedByRoleId { get; set; }
        public string DueDateCalculatedFromField { get; set; }
        public int DueDateCalculationOffset { get; set; }
        public int TaskPermissionLevelId { get; set; }
        public bool IsConditionTemplate { get; set; }
        public int? ConditionCategoryId { get; set; }
        public string ConditionCategoryId_rep
        {
            get
            {
                if (ConditionCategoryId.HasValue)
                {
                    return GetCategoryDisplayName(ConditionCategoryId.Value);
                }
                else
                {
                    return String.Empty;
                }
            }
        }
        public bool CondIsHidden { get; set; }
        public string CondInternalNotes { get; set; }

        public string Comments { get; set; }

        public string PermissionLevelName
        {
            get { return GetPermissionLevelDisplayName(TaskPermissionLevelId); }
        }

        public string PermissionLevelDescription
        {
            get { return GetPermissionLevelDescription(TaskPermissionLevelId); }
        }

        public bool PermissionLevelAppliesToConditions
        {
            get { return GetPermissionLevelAppliesToConditions(TaskPermissionLevelId); }
        }

        public string AssignedUserFullName { get; set; }
        public string OwnerFullName { get; set; }

        public int? CondRequiredDocTypeId { get; set; }
        public string CondRequiredDocTypeRep
        {
            get
            {
                if (IsConditionTemplate && CondRequiredDocTypeId.HasValue)
                {
                    return EDocumentDocType.GetDocTypeById(this.BrokerId, CondRequiredDocTypeId.Value).FolderAndDocTypeName;
                }
                return "None";
            }
        }

        public string ResolutionBlockTriggerName { get; set; }

        public string ResolutionDenialMessage { get; set; }

        public string ResolutionDateSetterFieldId { get; set; }

        public string AutoResolveTriggerName { get; set; }

        public string AutoCloseTriggerName { get; set; }

        public string GetResolutionDateSetterFieldDescription()
        {
            if (string.IsNullOrEmpty(this.ResolutionDateSetterFieldId))
            {
                return "None";
            }

            var parameterReporting = new ParameterReporting(this.BrokerId);
            var parameter = parameterReporting[this.ResolutionDateSetterFieldId];
            return (parameter == null) ? "" : parameter.FriendlyName;
        }

        #region DisplayNameMapping
        private Dictionary<int, string> m_permissionLevels = null;
        private Dictionary<int, string> m_permissionLevelDescriptions = null;
        private Dictionary<int, bool> m_permissionLevelAppliesToConditions = null;

        private string GetPermissionLevelDisplayName(int permissionId)
        {
            if (permissionId == 0) return "";

            if (m_permissionLevels == null)
            {
                GetPermissionLevelData();
            }

            return m_permissionLevels[permissionId];

        }

        private string GetPermissionLevelDescription(int permissionId)
        {
            if (m_permissionLevels == null)
            {
                GetPermissionLevelData();
            }

            return m_permissionLevelDescriptions[permissionId];
        }

        private bool GetPermissionLevelAppliesToConditions(int permissionId)
        {
            if (m_permissionLevels == null)
            {
                GetPermissionLevelData();
            }

            return m_permissionLevelAppliesToConditions[permissionId];
        }

        private void GetPermissionLevelData()
        {
            m_permissionLevels = new Dictionary<int, string>();
            m_permissionLevelDescriptions = new Dictionary<int, string>();
            m_permissionLevelAppliesToConditions = new Dictionary<int, bool>();

            foreach (PermissionLevel level in PermissionLevel.RetrieveAll(BrokerId))
            {
                m_permissionLevels.Add(level.Id, level.Name);
                m_permissionLevelDescriptions.Add(level.Id, level.Description);
                m_permissionLevelAppliesToConditions.Add(level.Id, level.IsAppliesToConditions);
            }
        }

        private string GetRoleDisplayName(Guid roleId)
        {
            if (roleId == Guid.Empty) return "Blank";

            return Role.Get(roleId).ModifiableDesc;;
        }

        private Dictionary<int, ConditionCategory> m_categoryDescription = new Dictionary<int, ConditionCategory>();
        private string GetCategoryDisplayName(int categoryId)
        {
            if (m_categoryDescription.ContainsKey(categoryId) == false)
            {
                ConditionCategory cat = ConditionCategory.GetCategory(BrokerId, categoryId);
                m_categoryDescription.Add(categoryId, cat);
            }

            return m_categoryDescription[categoryId].Category;
        }

        private string GetStatusDisplayName(E_TaskStatus status)
        {
            switch (status)
            {
                case E_TaskStatus.Active: return "Active";
                case E_TaskStatus.Resolved: return "Resolved";
                case E_TaskStatus.Closed: return "Closed";
                default:
                    throw new UnhandledEnumException(status);
            }
        }
        #endregion

        public string DueDateFriendlyDescription
        {
            get
            {
                if (m_isNew)
                {
                    throw new InvalidOperationException();
                }

                return Tools.GetTaskDueDateDescription(DueDateCalculationOffset, DueDateCalculatedFromField, true);
            }

        }

        private bool m_isNew = false;

        private TaskTriggerTemplate(DbDataReader reader)
        {
            AutoTaskTemplateId = (int)reader["AutoTaskTemplateId"];
            BrokerId = (Guid)reader["BrokerId"];
            Subject = (string)reader["Subject"];
            TriggerName = (string)reader["TriggerName"];
            Frequency = (E_AutoTaskFrequency)(int)reader["Frequency"];
            LastModifiedDate = (DateTime)reader["LastModifiedDate"];
            AssignedUserId = (Guid)reader["AssignedUserId"];
            ToBeAssignedRoleId = reader["ToBeAssignedRoleId"] == DBNull.Value ? null : (Guid?)reader["ToBeAssignedRoleId"];
            OwnerUserId = (Guid)reader["OwnerUserId"];
            ToBeOwnedByRoleId = reader["ToBeOwnedByRoleId"] == DBNull.Value ? null : (Guid?)reader["ToBeOwnedByRoleId"];
            DueDateCalculatedFromField = (string)reader["DueDateCalculatedFromField"];
            DueDateCalculationOffset = (int)reader["DueDateCalculationOffset"];
            TaskPermissionLevelId = (int)reader["TaskPermissionLevelId"];
            Comments = (string)reader["Comments"];
            IsConditionTemplate = (bool)reader["IsConditionTemplate"];
            CondIsHidden = (bool)reader["CondIsHidden"];
            CondInternalNotes = (string)reader["CondInternalNotes"];
            CondRequiredDocTypeId = reader.AsNullableInt("CondRequiredDocTypeId");
            ResolutionBlockTriggerName = (string)reader["ResolutionBlockTriggerName"];
            ResolutionDenialMessage = (string)reader["ResolutionDenialMessage"];
            ResolutionDateSetterFieldId = (string)reader["ResolutionDateSetterFieldId"];
            AutoResolveTriggerName = (string)reader["AutoResolveTriggerName"];
            AutoCloseTriggerName = (string)reader["AutoCloseTriggerName"];

            if (IsConditionTemplate)
            {
                ConditionCategoryId = (int)reader["ConditionCategoryId"];
            }

            if (ToBeAssignedRoleId.HasValue)
            {
                AssignedUserFullName = Role.Get(ToBeAssignedRoleId.Value).ModifiableDesc;
            }
            else if (AssignedUserId != Guid.Empty)
            {
                AssignedUserFullName = GetUserDisplayName(BrokerId, AssignedUserId);
            }
            else
            {
                Tools.LogBug("Invalid task There is no assigned user and the role id is missing " + AutoTaskTemplateId);
            }

            if (ToBeOwnedByRoleId.HasValue)
            {
                OwnerFullName = Role.Get(ToBeOwnedByRoleId.Value).ModifiableDesc;
            }
            else if (OwnerUserId != Guid.Empty)
            {
                OwnerFullName = GetUserDisplayName(BrokerId, OwnerUserId);
            }
            else
            {
                Tools.LogBug("Invalid task There is no to be owner role id and the owner user id is empty " + AutoTaskTemplateId);
            }
        }

        public TaskTriggerTemplate(Guid brokerId)
        {
            m_isNew = true;
            BrokerId = brokerId;
            TaskPermissionLevelId = GetDefaultPermissionLevel(brokerId);
        }

        public void Save()
        {
            int? tempId = m_isNew ? new int?() : new int?(AutoTaskTemplateId);
            SqlParameter[] parameters = new SqlParameter[] { 
                new SqlParameter("@AutoTaskTemplateId", tempId), 
                new SqlParameter("@BrokerId", BrokerId),
                new SqlParameter("@Subject", Subject),
                new SqlParameter("@TriggerName", TriggerName),
                new SqlParameter("@Frequency", Frequency),
                new SqlParameter("@LastModifiedDate", DateTime.Now),
                new SqlParameter("@AssignedUserId", AssignedUserId),
                new SqlParameter("@ToBeAssignedRoleId", ToBeAssignedRoleId),
                new SqlParameter("@OwnerUserId", OwnerUserId),
                new SqlParameter("@ToBeOwnedByRoleId", ToBeOwnedByRoleId),
                new SqlParameter("@DueDateCalculatedFromField", DueDateCalculatedFromField),
                new SqlParameter("@DueDateCalculationOffset", DueDateCalculationOffset),
                new SqlParameter("@TaskPermissionLevelId", TaskPermissionLevelId),
                new SqlParameter("@IsConditionTemplate", IsConditionTemplate),
                new SqlParameter("@CondIsHidden", CondIsHidden),
                new SqlParameter("@CondInternalNotes", CondInternalNotes ?? ""),
                new SqlParameter("@ConditionCategoryId", ConditionCategoryId),
                new SqlParameter("@Comments", Comments),
                new SqlParameter("@CondRequiredDocTypeId", CondRequiredDocTypeId),
                new SqlParameter("@ResolutionBlockTriggerName", ResolutionBlockTriggerName),
                new SqlParameter("@ResolutionDenialMessage", ResolutionDenialMessage),
                new SqlParameter("@ResolutionDateSetterFieldId", ResolutionDateSetterFieldId),
                new SqlParameter("@AutoResolveTriggerName", AutoResolveTriggerName),
                new SqlParameter("@AutoCloseTriggerName", AutoCloseTriggerName)
                };

            StoredProcedureHelper.ExecuteNonQuery(BrokerId, SP_Save, 3, parameters);
        }

        private string GetUserDisplayName(Guid brokerId, Guid userId)
        {
            if (userId == Guid.Empty) return "Blank";

            SqlParameter[] parameters = {
                                            new SqlParameter("@EmployeeUserID", userId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetEmployeeNameByUserId", parameters))
            {
                if (reader.Read())
                {
                    return reader["FirstLast"].ToString();
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, "Cannot find user " + userId.ToString());
                }
            }
        }

        private static IEnumerable<TaskTriggerTemplate> GetTaskTriggerTemplates(Guid brokerId, int? taskId)
        {
            List<TaskTriggerTemplate> templates = new List<TaskTriggerTemplate>();
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@AutoTaskTemplateId", taskId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, SP_Fetch, parameters))
            {
                while (reader.Read())
                {
                    templates.Add(new TaskTriggerTemplate(reader));
                }
            }

            return templates;
        }

        public static TaskTriggerTemplate GetTaskTriggerTemplate(Guid brokerId, int taskId)
        {
            var templates = GetTaskTriggerTemplates(brokerId, taskId);
            if (templates.Count() == 0)
            {
                throw new TaskNotFoundException(taskId.ToString(), brokerId);
            }
            return templates.First();
        }

        public static IEnumerable<TaskTriggerTemplate> GetTaskTriggerTemplatesByBroker(Guid brokerId)
        {
            return GetTaskTriggerTemplates(brokerId, null);
        }

        public static void Delete(Guid brokerId, int taskId)
        {
            SqlParameter[] parameters = {
                                             new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@AutoTaskTemplateId", taskId)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, SP_DELETE, 1, parameters);
        }

        private static int GetDefaultPermissionLevel(Guid brokerId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_GetDefaultPermissionLevelByBrokerId", parameters))
            {
                if (reader.Read())
                {
                    return (int)reader["TaskPermissionLevelId"];
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, "Cannot get default permission level");
                }
            }
        }

        public static List<SecurityParameter.Parameter> ListValidDateFieldParameters()
        {
            List<SecurityParameter.Parameter> ret = new List<SecurityParameter.Parameter>();
            ret = Task.ListValidDateFieldParameters();
            ret.Add(ParameterReporting.RetrieveSystemParameters().Parameters.First((param) => param.Id == "sDateTriggered"));

            return ret;
        }

        public static Task GetTaskFromTaskTemplate(Guid brokerId, int templateId, Guid loanId)
        {
            TaskTriggerTemplate template = TaskTriggerTemplate.GetTaskTriggerTemplate(brokerId, templateId);

            Task task = new Task(loanId, brokerId);
            task.EnforcePermissions = false;
            
            task.TaskSubject = template.Subject;
            task.TaskIsCondition = template.IsConditionTemplate;
            task.CondCategoryId = template.ConditionCategoryId;
            task.CondIsHidden = template.CondIsHidden;
            task.CondInternalNotes = template.CondInternalNotes;
            task.TaskAssignedUserId = template.AssignedUserId;
            task.TaskOwnerUserId = template.OwnerUserId;
            task.TaskToBeAssignedRoleId = template.ToBeAssignedRoleId;
            task.TaskToBeOwnerRoleId = template.ToBeOwnedByRoleId;
            if (template.DueDateCalculatedFromField.ToLower() == "sdatetriggered") // sDateTriggered: The date that the trigger was, uh, triggered
            {
                CPageData dataLoan = new CFullAccessPageData(loanId, new string[] { "sfCalculateOffset" });
                dataLoan.AllowLoadWhileQP2Sandboxed = true;
                dataLoan.InitLoad();
                dataLoan.ByPassFieldSecurityCheck = true;
                task.TaskDueDate = dataLoan.CalculateOffset(DateTime.Now, template.DueDateCalculationOffset);
                task.TaskDueDateLocked = true;
            }
            else
            {
                task.TaskDueDateCalcField = template.DueDateCalculatedFromField;
                task.TaskDueDateCalcDays = template.DueDateCalculationOffset;
                var dateString = Task.GetCalculatedDueDate(loanId, template.DueDateCalculationOffset, template.DueDateCalculatedFromField);
                DateTime dueDate;
                if (DateTime.TryParse(dateString, out dueDate))
                {
                    task.TaskDueDate = dueDate;
                }
            }
            task.TaskPermissionLevelId = template.TaskPermissionLevelId;
            task.Comments = template.Comments;
            
            task.TaskStatus = E_TaskStatus.Active;
            if (task.TaskIsCondition)
            { 
                task.CondRequiredDocTypeId = template.CondRequiredDocTypeId;
            }

            task.ResolutionBlockTriggerName = template.ResolutionBlockTriggerName;
            task.ResolutionDenialMessage = template.ResolutionDenialMessage;
            task.ResolutionDateSetterFieldId = template.ResolutionDateSetterFieldId;

            return task;
        }

        public static string GetTaskIdCreatedFromTriggerTemplate(Guid brokerId, Guid loanId, int templateId, Guid recordId)
        {
            string sId = string.Empty;
            SqlParameter[] parameters = {
                                            new SqlParameter("@brokerId", brokerId),
                                             new SqlParameter("@loanId", loanId),                        
                                             new SqlParameter("@templateId", templateId),
                                             new SqlParameter("@recordId", recordId)
                                        };

            using(DbDataReader rd = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_GetLoanTasksCreatedFromTemplate", parameters))
            {
                if (rd.Read())
                {
                    sId = rd["TaskId"].ToString();
                }
            }

            return sId;
        }

        public static void LinkTaskToTaskTriggerTemplate(Task task, TaskTriggerQueueItem item)
        {
            using (CStoredProcedureExec exec = new CStoredProcedureExec(item.BrokerId))
            {
                try
                {
                    exec.BeginTransactionForWrite();

                    exec.ExecuteNonQuery("TASK_TrackTaskCreationFromTemplate",
                        new SqlParameter("@brokerId", item.BrokerId),
                        new SqlParameter("@loanId", item.sLId),
                        new SqlParameter("@taskId", task.TaskId),
                        new SqlParameter("@templateId", item.TaskTemplateId),
                        new SqlParameter("@appId", item.aAppId),
                        new SqlParameter("@recordId", item.RecordId)
                        );

                    exec.CommitTransaction();
                }
                catch (SqlException)
                {
                    exec.RollbackTransaction();
                    throw;
                }
            }
            return;
        }
    }
}