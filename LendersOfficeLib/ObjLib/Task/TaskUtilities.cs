﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Security;
using System.Threading;
using LendersOffice.Admin;
using LendersOffice.Constants;
using System.Collections.Specialized;
using CommonProjectLib.Common.Lib;
using System.Data;
using System.Xml.Linq;
using LendersOffice.ObjLib.DatabaseMessageQueue;
using LendersOffice.ObjLib.LoanFileCache;
using LqbGrammar.Drivers.SecurityEventLogging;
using LqbGrammar.DataTypes;

namespace LendersOffice.ObjLib.Task
{

    public static class TaskUtilities
    {
        private class TaskStatisticsItem
        {
            public int sNumOfActiveTasks = 0; // Number of active tasks
            public int sNumOfActiveDueTodayTasks = 0; // Number of active tasks that has duedate = today
            public int sNumOfActivePastDueTasks = 0; // Number of active tasks that has duedate < today
            public int sNumOfOutstandingTasks = 0; // Number of Active & Resolved Tasks
            public int sNumOfOutstandingDueTodayTasks = 0; // Number of active & resolved tasks that has DueDate = Today
            public int sNumOfOutstandingPastDueTasks = 0; // Number of active & resolved tasks that has due date < today
            public int sNumOfClosedTasks = 0; // Number of closed tasks
            /// <summary>
            /// Number of active task not including conditions.
            /// </summary>
            public int sNumOfActiveTasksOnly = 0; 
            /// <summary>
            /// Number of active conditions.
            /// </summary>
            public int sNumOfActiveConditionOnly = 0;
            /// <summary>
            /// Number of active past due task (not including condition) duedate &lt; today
            /// </summary>
            public int sNumOfActivePastDueTasksOnly = 0;
            /// <summary>
            /// Number of active past due duedate &lt; today conditions.
            /// </summary>
            public int sNumOfActivePastDueConditionOnly = 0;
            /// <summary>
            /// Number of active due today task only (no conditions)
            /// </summary>
            public int sNumOfActiveDueTodayTasksOnly = 0;
            /// <summary>
            /// Number of active due today condition only
            /// </summary>
            public int sNumOfActiveDueTodayConditionOnly = 0;

        }
        public static void EnqueueTasksDueDateUpdate(Guid brokerId, Guid sLId)
        {
            if (ConstSite.IsTaskMigrationProcess)
            {
                // 3/3/2012 dd - DO NOT update workflow queue during task migration
                // because we do not want to generate massive email during this process.
                return;
            }
            DBMessageQueue mainQ = new DBMessageQueue(ConstMsg.WorkflowCacheUpdaterQueue);
            string key = string.Format("{0}_UpdateDueDates", sLId); 
            mainQ.SendIfNew(key, brokerId.ToString(), string.Empty);
        }

        public static void UpdateTasksDueDateByLoan(Guid brokerId, Guid sLId, Guid? loanCacheUpdateRequestId = null)
        {
            var originalPrincipal = Thread.CurrentPrincipal;

            try
            {
                Thread.CurrentPrincipal = SystemUserPrincipal.TaskSystemUser; // So this can be run outside LO context

                CPageData dataLoan = new NotEnforceAccessControlPageData(sLId, new[] { "sLNm", "sPrimBorrowerFullNm" });
                dataLoan.AllowLoadWhileQP2Sandboxed = true;
                dataLoan.InitLoad();
                string sLNm = dataLoan.sLNm;
                string sPrimBorrowerFullNm = dataLoan.sPrimBorrowerFullNm;

                List<Task> taskList = Task.GetTasksInLoan(brokerId, sLId);
                var tasksWithCalculatedDueDates = new List<Task>();
                var dueDateCalculationDependencies = new HashSet<string>();
                var statisticsItem = new TaskStatisticsItem();
                bool updateTaskCacheInfo = false;

                foreach (var task in taskList)
                {
                    if (task.LoanNumCached != sLNm || task.BorrowerNmCached != sPrimBorrowerFullNm)
                    {
                        updateTaskCacheInfo = true;
                    }

                    if (task.TaskDueDateLocked)
                    {
                        CalculateStatistic(task, statisticsItem);
                    }
                    else
                    {
                        tasksWithCalculatedDueDates.Add(task);
                        dueDateCalculationDependencies.Add(task.TaskDueDateCalcField);
                    }
                }

                if (tasksWithCalculatedDueDates.Any())
                {
                    dueDateCalculationDependencies.Add("sfCalculateOffset");
                    dataLoan = new NotEnforceAccessControlPageData(sLId, dueDateCalculationDependencies);
                    dataLoan.AllowLoadWhileQP2Sandboxed = true;
                    dataLoan.InitLoad();

                    foreach (var task in tasksWithCalculatedDueDates)
                    {
                        DateTime? dt = dataLoan.GetDateTimeOffset(task.TaskDueDateCalcField, task.TaskDueDateCalcDays);
                        if (dt.HasValue)
                        {
                            //OPM 69370: Make sure the task knows that any updates to the date are the result of a calculation
                            task.isCalculating = true;

                            task.EnforcePermissions = false;
                            task.TaskDueDate = dt;
                            task.Save(false);
                            
                            task.isCalculating = false;
                        }

                        CalculateStatistic(task, statisticsItem);
                    }
                }
                
                SqlParameter[] parameters = 
                {
                    new SqlParameter("@sLId", sLId),
                    new SqlParameter("@sNumOfActiveTasks", statisticsItem.sNumOfActiveTasks),
                    new SqlParameter("@sNumOfActiveDueTodayTasks", statisticsItem.sNumOfActiveDueTodayTasks),
                    new SqlParameter("@sNumOfActivePastDueTasks", statisticsItem.sNumOfActivePastDueTasks),
                    new SqlParameter("@sNumOfOutstandingTasks", statisticsItem.sNumOfOutstandingTasks),
                    new SqlParameter("@sNumOfOutStandingDueTodayTasks", statisticsItem.sNumOfOutstandingDueTodayTasks),
                    new SqlParameter("@sNumOfOutStandingPastDueTasks", statisticsItem.sNumOfOutstandingPastDueTasks),
                    new SqlParameter("@sNumOfClosedTasks", statisticsItem.sNumOfClosedTasks),

                    new SqlParameter("@sNumOfActiveTasksOnly", statisticsItem.sNumOfActiveTasksOnly),
                    new SqlParameter("@sNumOfActiveConditionOnly", statisticsItem.sNumOfActiveConditionOnly),
                    new SqlParameter("@sNumOfActivePastDueTasksOnly", statisticsItem.sNumOfActivePastDueTasksOnly),
                    new SqlParameter("@sNumOfActivePastDueConditionOnly", statisticsItem.sNumOfActivePastDueConditionOnly),
                    new SqlParameter("@sNumOfActiveDueTodayTasksOnly", statisticsItem.sNumOfActiveDueTodayTasksOnly),
                    new SqlParameter("@sNumOfActiveDueTodayConditionOnly", statisticsItem.sNumOfActiveDueTodayConditionOnly),
                };

                // If this fails, then the loan fields above may be out of date in the cache.
                StoredProcedureHelper.ExecuteNonQuery(brokerId, "UpdateTaskStatistics", 3, parameters);

                if (updateTaskCacheInfo)
                {
                    parameters = new[] { new SqlParameter("@sLId", sLId) };

                    // If this fails, then the denormalized borrower name and loan number stored on the task may be out of date.
                    StoredProcedureHelper.ExecuteNonQuery(brokerId, "TASK_SyncLoanCachedInfoToTaskByLoanId", 3, parameters);
                }

                if (loanCacheUpdateRequestId != null)
                {
                    LoanCacheUpdateRequest.Delete(loanCacheUpdateRequestId.Value, brokerId);
                }
            }
            catch (Exception exc)
            {
                var principal = PrincipalFactory.CurrentPrincipal;
                string loginNm = principal?.LoginNm ?? string.Empty;

                string errorDetails = loanCacheUpdateRequestId.HasValue 
                    ? $"Updating exising entry in LOAN_CACHE_UPDATE_REQUEST ({loanCacheUpdateRequestId.Value})" 
                    : "Inserting new entry into LOAN_CACHE_UPDATE_REQUEST";
                Tools.LogError($"Failed_updating_task_duedates_by_loan {sLId}. {errorDetails}.", exc);

                if (loanCacheUpdateRequestId != null)
                {
                    LoanCacheUpdateRequest.UpdateForTaskFailure(loanCacheUpdateRequestId.Value, brokerId, exc, loginNm);
                }
                else
                {
                    Guid userId = principal?.UserId ?? Guid.Empty;
                    LoanCacheUpdateRequest.InsertForTaskFailure(sLId, brokerId, userId, loginNm, exc);
                }
            }
            finally
            {
                // Restore the principal to it's original value. We don't want
                // to leave it as the tasksystem user if the thread will be
                // processing other data. 
                Thread.CurrentPrincipal = originalPrincipal;
            }
        }
 
        private static void CalculateStatistic(Task task, TaskStatisticsItem item)
        {
            int dueToday = 0;
            int pastDue = 0;

            if (task.TaskDueDate.HasValue)
            {
                if (task.TaskDueDate.Value.Date == DateTime.Now.Date)
                {
                    dueToday = 1;
                }
                else if (task.TaskDueDate.Value.Date < DateTime.Now.Date)
                {
                    pastDue = 1;
                }
            }
            switch (task.TaskStatus)
            {
                case E_TaskStatus.Active:
                    item.sNumOfActiveTasks++;
                    item.sNumOfOutstandingTasks++;
                    item.sNumOfActiveDueTodayTasks += dueToday;
                    item.sNumOfActivePastDueTasks += pastDue;
                    item.sNumOfOutstandingDueTodayTasks += dueToday;
                    item.sNumOfOutstandingPastDueTasks += pastDue;

                    if (task.TaskIsCondition)
                    {
                        item.sNumOfActiveConditionOnly++;
                        item.sNumOfActiveDueTodayConditionOnly += dueToday;
                        item.sNumOfActivePastDueConditionOnly += pastDue;
                    }
                    else
                    {
                        item.sNumOfActiveTasksOnly++;
                        item.sNumOfActivePastDueTasksOnly += pastDue;
                        item.sNumOfActiveDueTodayTasksOnly += dueToday;
                    }

                    break;
                case E_TaskStatus.Resolved:
                    item.sNumOfOutstandingTasks++;

                    item.sNumOfOutstandingDueTodayTasks += dueToday;
                    item.sNumOfOutstandingPastDueTasks += pastDue;
                    break;
                case E_TaskStatus.Closed:
                    item.sNumOfClosedTasks++;
                    break;
                default:
                    throw new UnhandledEnumException(task.TaskStatus);
            }
        }

        public static void UpdateTaskAssignmentsByLoan(Guid brokerId, Guid sLId, NameValueCollection roleAndEmployeeIds, Dictionary<string, Guid> employeesThatLostAccess)
        {
          
            List<Task> taskList = Task.GetTasksInLoan(brokerId, sLId);
            if (taskList.Count == 0)
            {
                return; //nothing to do 
            }
            //RoleList rl = RoleList.RetrieveFromCache(true);

            Dictionary<Guid, EmployeeDetails> employeeDetails = new Dictionary<Guid,EmployeeDetails>();
            Dictionary<Guid, AbstractUserPrincipal> principals = new Dictionary<Guid, AbstractUserPrincipal>();
            foreach (KeyValuePair<string, Guid> entry in employeesThatLostAccess)
            {
                if (entry.Value == Guid.Empty)
                {
                    continue;
                }
                if (employeeDetails.ContainsKey(entry.Value))
                {
                    continue;//we already looked up this user look up someone else
                }

                EmployeeDetails details = new EmployeeDetails(brokerId, entry.Value);
                employeeDetails[details.UserId] = details;
            }

            Dictionary<Guid, Guid> employeeIdToUserIdMap = new Dictionary<Guid, Guid>();

            foreach (Task task in taskList)
            {
                bool needToSaveTask = false;
                task.EnforcePermissions = false; //not sure if we need this.

                if (task.TaskAssignedUserId != Guid.Empty &&     //check to see if the assigned user was unassigned from the loan.
                    task.TaskStatus == E_TaskStatus.Active && employeeDetails.ContainsKey(task.TaskAssignedUserId))
                {
                    EmployeeDetails currentDetails;
                    if (employeeDetails.TryGetValue(task.TaskAssignedUserId, out currentDetails))
                    {
                        //this user was unassigned from the loan file.. lets see if he she still has access
                        AbstractUserPrincipal principal;

                        if (false == principals.TryGetValue(task.TaskAssignedUserId, out principal))
                        {
                            principal = (AbstractUserPrincipal)PrincipalFactory.RetrievePrincipalForUser(brokerId, currentDetails.UserId, currentDetails.Type);
                            if (null == principal)
                            {
                                Tools.LogInfo(
                                    "RetrievePrincipalForUser: Couldn't create principal for" 
                                    + " currentDetails.UserID "+ currentDetails.UserId
                                    + " currentDetails.Type " + currentDetails.Type
                                    + " even though we got EmployeeDetails for task.TaskAssignedUserId of " + task.TaskAssignedUserId
                                    + " for task with id " + task.TaskId
                                    + " loan " + sLId
                                    + " broker" + brokerId
                                    + Environment.NewLine
                                    + "The user is probably inactive."
                                );
                            }

                            principals.Add(task.TaskAssignedUserId, principal);
                        }

                        if (null == principal || task.GetPermissionLevelFor(principal) == E_UserTaskPermissionLevel.None)
                        {
                            TaskSubscription.Unsubscribe(currentDetails.BrokerId, task.TaskId, currentDetails.UserId);
                             //user lost access, or is inactive.
                            if (employeesThatLostAccess.ContainsKey(ConstApp.ROLE_LOAN_OFFICER) &&
                                employeesThatLostAccess[ConstApp.ROLE_LOAN_OFFICER] == currentDetails.EmployeeId)
                            {
                                task.TaskToBeAssignedRoleId = Role.Get(E_RoleT.LoanOfficer).Id;
                            }
                            else
                            {
                                string role = employeesThatLostAccess.First(p => p.Value == currentDetails.EmployeeId).Key;
                                task.TaskToBeAssignedRoleId = Role.Get(role).Id;
                            }
                            needToSaveTask = true;
                        }
                    }
                }

                if (task.TaskOwnerUserId != Guid.Empty &&
                    task.TaskStatus != E_TaskStatus.Closed &&
                    employeeDetails.ContainsKey(task.TaskOwnerUserId))
                {
                    EmployeeDetails currentDetails;
                    if (employeeDetails.TryGetValue(task.TaskOwnerUserId, out currentDetails))
                    {
                        //this user was unassigned from the loan file.. lets see if he she still has access
                        AbstractUserPrincipal principal;

                        if (false == principals.TryGetValue(task.TaskOwnerUserId, out principal))
                        {
                            principal = (AbstractUserPrincipal)PrincipalFactory.RetrievePrincipalForUser(brokerId, currentDetails.UserId, currentDetails.Type);
                            principals[currentDetails.UserId] = principal;
                        }

                        if (null == principal || task.GetPermissionLevelFor(principal) == E_UserTaskPermissionLevel.None)
                        {
                            TaskSubscription.Unsubscribe(currentDetails.BrokerId, task.TaskId, currentDetails.UserId);
                            //user lost access, or is inactive.
                            if (employeesThatLostAccess.ContainsKey(ConstApp.ROLE_LOAN_OFFICER) && 
                                employeesThatLostAccess[ConstApp.ROLE_LOAN_OFFICER] == currentDetails.EmployeeId)
                            {
                                task.TaskToBeOwnerRoleId = Role.Get(E_RoleT.LoanOfficer).Id;
                            }
                            else
                            {
                                string role = employeesThatLostAccess.First(p => p.Value == currentDetails.EmployeeId).Key;
                                task.TaskToBeOwnerRoleId = Role.Get(role).Id;
                            }
                            needToSaveTask = true;
                        }
                    }
                }

                string employeeId = task.TaskToBeOwnerRoleId.HasValue ? roleAndEmployeeIds[Role.Get(task.TaskToBeOwnerRoleId.Value).Desc] : null;
                //if the task has no user id AND the role to which it is going to be assigned to was changed recently process
                if (task.TaskOwnerUserId == Guid.Empty && employeeId != null)
                {
                    Guid userId = GetUserIdFromEmployeeId(employeeIdToUserIdMap, new Guid(employeeId), brokerId);
                    if (userId != Guid.Empty)
                    {
                        task.TaskOwnerUserId = userId;
                        needToSaveTask = true;
                    }
                }
                employeeId = task.TaskToBeAssignedRoleId.HasValue ? roleAndEmployeeIds[Role.Get(task.TaskToBeAssignedRoleId.Value).Desc] : null;
                if (task.TaskAssignedUserId == Guid.Empty && employeeId != null)
                {
                    Guid userId = GetUserIdFromEmployeeId(employeeIdToUserIdMap, new Guid(employeeId), brokerId);
                    if (userId != Guid.Empty)
                    {
                        task.TaskAssignedUserId = userId;
                        needToSaveTask = true;
                    }
                }

                // We only want to save a Closed task if the Task Status has been changed.
                bool taskCanBeSavedBasedOnStatus = true;
                if (task.TaskStatus == E_TaskStatus.Closed && !task.CheckIfTaskStatusChanged())
                {
                    taskCanBeSavedBasedOnStatus = false;
                }

                if (needToSaveTask && taskCanBeSavedBasedOnStatus)
                {
                    task.Save(true, isUpdateAsPartOfRoleChange:true); //todo ask matthew
                }
            }
      

        }

        private static List<string> BuildTaskDateBlackList(AbstractUserPrincipal principal)
        {

            List<string> blackList = new List<string>();

            ParameterReporting pR = ParameterReporting.RetrieveSystemParameters();

            BrokerUserPrincipal brokerPrincipal = principal as BrokerUserPrincipal;

            foreach (SecurityParameter.Parameter param in pR.Parameters)
            {
                if (LoanFieldSecurityManager.IsSecureField(param.Id))
                {
                    if (brokerPrincipal == null || LoanFieldSecurityManager.CanReadField(param.Id, brokerPrincipal) == false)
                    {
                        blackList.Add("[" + param.FriendlyName + "]");
                    }
                }
            }

            return blackList;
        }
        
        public static string SanitizeTaskText(string text, AbstractUserPrincipal principal)
        {
            List<string> blackList = BuildTaskDateBlackList( principal );

            return SanitizeText(text, blackList, "[Internal Date]");
        }

        private static string SanitizeText(string text, List<string> blackList, string replacementText)
        {
            StringBuilder ret = new StringBuilder(text);

            foreach (string s in blackList.Distinct())
            {
                ret.Replace(s, replacementText);
            }

            return ret.ToString();
        }
 

        //private static Guid GetEmployeeAssigmentFromLoan(CPageData data, Guid roleId, RoleList roleList, Dictionary<Guid,Guid> employeeMap, Guid brokerId)
        //{
        //    Guid employeeId; 
        //    switch (roleList[roleId].RoleDesc)
        //    {
        //        case ConstApp.ROLE_BROKERPROCESSOR:
        //            employeeId = data.sEmployeeBrokerProcessorId;
        //            break;
        //        case ConstApp.ROLE_CALL_CENTER_AGENT:
        //            employeeId = data.sEmployeeCallCenterAgentId;
        //            break;
        //        case ConstApp.ROLE_CLOSER:
        //            employeeId = data.sEmployeeCloserId;
        //            break;
        //        case ConstApp.ROLE_LENDER_ACCOUNT_EXEC:
        //            employeeId = data.sEmployeeLenderAccExecId;
        //            break;
        //        case ConstApp.ROLE_LOAN_OPENER:
        //            employeeId = data.sEmployeeLoanOpenerId;
        //            break;
        //        case ConstApp.ROLE_LOAN_OFFICER:
        //            employeeId = data.sEmployeeLoanRepId;
        //            break;
        //        case ConstApp.ROLE_LOCK_DESK:
        //            employeeId = data.sEmployeeLockDeskId;
        //            break;
        //        case ConstApp.ROLE_MANAGER:
        //            employeeId = data.sEmployeeManagerId;
        //            break;
        //        case ConstApp.ROLE_PROCESSOR:
        //            employeeId = data.sEmployeeProcessorId;
        //            break;
        //        case ConstApp.ROLE_REAL_ESTATE_AGENT:
        //            employeeId = data.sEmployeeRealEstateAgentId;
        //            break;
        //        case ConstApp.ROLE_UNDERWRITER:
        //            employeeId = data.sEmployeeUnderwriterId;
        //            break;
        //        default:
        //            //should not happen
        //            throw CBaseException.GenericException("Got a missing role. " + roleId);
        //    }
        //    return GetUserIdFromEmployeeId(employeeMap, employeeId, brokerId);
        //}

        private static Guid GetUserIdFromEmployeeId(Dictionary<Guid, Guid> hashedValues, Guid employeeId, Guid brokerId)
        {
            Guid userId;
            if (hashedValues.TryGetValue(employeeId, out userId))
            {
                return userId;
            }

            EmployeeDB db = new EmployeeDB(employeeId, brokerId);
            db.Retrieve();

            if (db.IsActive && db.AllowLogin)
            {
                return hashedValues[employeeId] = db.UserID;
            }
            else
            {
                return hashedValues[employeeId] = Guid.Empty;
            }
        }
        public static List<ReadOnlyTask> GetTasksInPipeline(AbstractUserPrincipal principal, Guid assignedUserIdFilter, E_TaskStatus filterTaskStatus, string sortField, SortOrder sortOrder)
        {
            return GetTasksInPipeline(principal, assignedUserIdFilter, filterTaskStatus, sortField, sortOrder, E_PortalMode.Blank, null);
        }

        public static List<ReadOnlyTask> GetTasksInPipeline(AbstractUserPrincipal principal, Guid assignedUserIdFilter, E_TaskStatus filterTaskStatus, string sortField, SortOrder sortOrder, E_PortalMode portalMode, IEnumerable<E_sStatusT> sStatusTFilters, bool? getConditions = null)
        {
            if (principal == null)
            {
                throw CBaseException.GenericException(principal + " is null");
            }

            if (filterTaskStatus != E_TaskStatus.Active && filterTaskStatus != E_TaskStatus.Resolved) 
            {
                throw CBaseException.GenericException(filterTaskStatus + " is not supported in GetTasksInPipeline");
            }
            string sortDirection = string.Empty;
            switch (sortOrder)
            {
                case SortOrder.Ascending:
                    sortDirection = " ASC";
                    break;
                case SortOrder.Descending:
                    sortDirection = " DESC";
                    break;
                default:
                    throw new UnhandledEnumException(sortOrder);
            }

            string view = string.Empty;
            // Use default view for conditioncategory sort
            if (sortField.Equals("TaskDueDate", StringComparison.OrdinalIgnoreCase) ||
                sortField.Equals("ConditionCategory", StringComparison.OrdinalIgnoreCase))
            {
                view = "VIEW_ACTIVE_RESOLVED_TASK_ORDERBY_DUEDATE";
            }
            else if (sortField.Equals("TaskFollowUpDate", StringComparison.OrdinalIgnoreCase))
            {
                view = "VIEW_ACTIVE_RESOLVED_TASK_ORDERBY_FOLLOWUPDATE";
            }
            else if (sortField.Equals("TaskLastModifiedDate", StringComparison.OrdinalIgnoreCase))
            {
                view = "VIEW_ACTIVE_RESOLVED_TASK_ORDERBY_LASTMODIFIEDDATE";
            }
            else
            {
                throw CBaseException.GenericException(sortField + " is not support as sort field in GetTasksInPipeline");
            }
            int maxRowCount = 50;
            List<SqlParameter> parameters = new List<SqlParameter>();
            List<string> whereClauses = new List<string>();

            List<string> selectColumns = new List<string>()
            {
                #region Fields Display In Pipeline
                "TaskId",
                "LoanId",
                "TaskSubject",
                "TaskStatus",
                "lc1.sLNm AS LoanNumCached",
                "lc1.sPrimBorrowerFullNm AS BorrowerNmCached",
                "lc1.aBLastNm AS BorrowerLastNmCached",
                "TaskDueDate",
                "TaskFollowUpDate",
                "TaskLastModifiedDate",
                "TaskAssignedUserFullName",
                "TaskOwnerFullName",
                #endregion
                #region Fields require for Task Permission Level Check
                "TaskPermissionLevelId",
                "CondIsHidden",
                "TaskOwnerUserId",
                "TaskOwerUserType",
                "TaskIsCondition",
                "CondCategoryId" 
                #endregion
            };

            #region Generic Filters
            whereClauses.Add("t.BrokerId = @BrokerId");
            parameters.Add(new SqlParameter("@BrokerId", principal.BrokerId));

            whereClauses.Add("t.TaskStatus = @TaskStatus");
            parameters.Add(new SqlParameter("@TaskStatus", filterTaskStatus));

            whereClauses.Add("(lc1.sLoanFileT IS NULL OR lc1.sLoanFileT = " + E_sLoanFileT.Loan.ToString("D") + ")");
            #endregion

            #region User Access Level Filter
            string filterIndividualAccessClause = @" (lc1.sEmployeeManagerId = @EmployeeId
												OR lc1.sEmployeeProcessorId = @EmployeeId OR lc1.sEmployeeLoanRepId = @EmployeeId
												OR lc1.sEmployeeLoanOpenerId = @EmployeeId OR lc1.sEmployeeCallCenterAgentId = @EmployeeId
												OR lc1.sEmployeeRealEstateAgentId = @EmployeeId OR lc1.sEmployeeLenderAccExecId = @EmployeeId
												OR lc1.sEmployeeLockDeskId = @EmployeeId OR lc1.sEmployeeUnderwriterId = @EmployeeId
                                                OR lc1.sEmployeeCloserId = @EmployeeId OR lc3.sEmployeeShipperId = @EmployeeId
                                                OR lc3.sEmployeeFunderId = @EmployeeId OR lc3.sEmployeePostCloserId = @EmployeeId
                                                OR lc3.sEmployeeInsuringId = @EmployeeId OR lc3.sEmployeeCollateralAgentId = @EmployeeId
                                                OR lc3.sEmployeeDocDrawerId = @EmployeeId OR lc3.sEmployeeCreditAuditorId = @EmployeeId 
                                                OR lc3.sEmployeeDisclosureDeskId = @EmployeeId OR lc3.sEmployeeJuniorProcessorId = @EmployeeId 
                                                OR lc3.sEmployeeJuniorUnderwriterId = @EmployeeId OR lc3.sEmployeeLegalAuditorId = @EmployeeId 
                                                OR lc3.sEmployeeLoanOfficerAssistantId = @EmployeeId OR lc3.sEmployeePurchaserId = @EmployeeId 
                                                OR lc3.sEmployeeQCComplianceId = @EmployeeId OR lc3.sEmployeeSecondaryId = @EmployeeId 
                                                OR lc3.sEmployeeServicingId = @EmployeeId ) ";

            if (principal.ApplicationType == E_ApplicationT.LendersOffice ||
                principal.PortalMode == E_PortalMode.Retail)
            {
                if (principal.HasPermission(Permission.BrokerLevelAccess))
                {
                    // No-Op.
                } 
                else if (principal.HasPermission(Permission.BranchLevelAccess)) 
                {
                    whereClauses.Add("( lc1.sBranchId = @BranchId OR " + filterIndividualAccessClause + ")");

                    parameters.Add(new SqlParameter("@BranchId", principal.BranchId));
                    parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));
                }
                else
                {
                    whereClauses.Add(filterIndividualAccessClause);
                    parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));
                }
            }
            else if (principal.ApplicationType == E_ApplicationT.PriceMyLoan)
            {
                if (principal.PmlLevelAccess == E_PmlLoanLevelAccess.Corporate)
                {
                    whereClauses.Add("lc1.sPmlBrokerId=@PmlBrokerId");
                    parameters.Add(new SqlParameter("@PmlBrokerId", principal.PmlBrokerId));
                }
                else if (principal.PmlLevelAccess == E_PmlLoanLevelAccess.Supervisor)
                {
                    whereClauses.Add("( ((lc1.sEmployeeLoanRepId=@EmployeeId OR lc1.sEmployeeBrokerProcessorId=@EmployeeId OR lc2.sEmployeeExternalSecondaryId=@EmployeeId OR lc2.sEmployeeExternalPostCloserId=@EmployeeId) AND lc1.sPmlBrokerId=@PmlBrokerId) OR lc1.PmlExternalManagerEmployeeId=@EmployeeId OR lc1.PmlExternalBrokerProcessorManagerEmployeeId =@EmployeeId OR lc2.PmlExternalSecondaryManagerEmployeeId=@EmployeeId OR lc2.PmlExternalPostCloserManagerEmployeeId=@EmployeeId)");
                    parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));
                    parameters.Add(new SqlParameter("@PmlBrokerId", principal.PmlBrokerId));
                }
                else if (principal.PmlLevelAccess == E_PmlLoanLevelAccess.Individual)
                {
                    whereClauses.Add("(lc1.sEmployeeLoanRepId=@EmployeeId OR lc1.sEmployeeBrokerProcessorId=@EmployeeId OR lc2.sEmployeeExternalSecondaryId=@EmployeeId OR lc2.sEmployeeExternalPostCloserId=@EmployeeId)");
                    whereClauses.Add("lc1.sPmlBrokerId = @PmlBrokerId");
                    parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));
                    parameters.Add(new SqlParameter("@PmlBrokerId", principal.PmlBrokerId));
                }
                else
                {
                    throw new UnhandledEnumException(principal.PmlLevelAccess);
                }
                // 7/25/2011 dd - OPM 68181 - Don't display resolved task that are not owned by PML user.
                if (filterTaskStatus == E_TaskStatus.Resolved)
                {
                    whereClauses.Add(" TaskOwerUserType='P'");
                }
            }
            else
            {
                throw new UnhandledEnumException(principal.ApplicationType);
            }
            #endregion

            #region Assigned User Filter
            if (assignedUserIdFilter != Guid.Empty)
            {
                whereClauses.Add("t.TaskAssignedUserId = @TaskAssignedUserId");
                parameters.Add(new SqlParameter("@TaskAssignedUserId", assignedUserIdFilter));
            }
            #endregion

            #region Portal filter

            if (portalMode == E_PortalMode.Broker)
            {
                whereClauses.Add("(lc2.sBranchChannelT = @sBranchChannelT OR lc2.sBranchChannelT = " + (int)E_BranchChannelT.Blank + ")");
                parameters.Add(new SqlParameter("@sBranchChannelT", E_BranchChannelT.Wholesale));
            }
            if (portalMode == E_PortalMode.MiniCorrespondent)
            {
                whereClauses.Add("lc2.sBranchChannelT = @sBranchChannelT");
                parameters.Add(new SqlParameter("@sBranchChannelT", E_BranchChannelT.Correspondent));

                whereClauses.Add("lc3.sCorrespondentProcessT = @sCorrespondentProcessT1");
                parameters.Add(new SqlParameter("@sCorrespondentProcessT1", E_sCorrespondentProcessT.MiniCorrespondent));
            }
            if (portalMode == E_PortalMode.Correspondent)
            {
                whereClauses.Add("lc2.sBranchChannelT = @sBranchChannelT");
                parameters.Add(new SqlParameter("@sBranchChannelT", E_BranchChannelT.Correspondent));

                whereClauses.Add("(lc3.sCorrespondentProcessT = @sCorrespondentProcessT1 OR lc3.sCorrespondentProcessT = @sCorrespondentProcessT2 OR sCorrespondentProcessT = @sCorrespondentProcessT3)");
                parameters.Add(new SqlParameter("@sCorrespondentProcessT1", E_sCorrespondentProcessT.Blank));
                parameters.Add(new SqlParameter("@sCorrespondentProcessT2", E_sCorrespondentProcessT.Delegated));
                parameters.Add(new SqlParameter("@sCorrespondentProcessT3", E_sCorrespondentProcessT.PriorApproved));
            }
            if (portalMode == E_PortalMode.Retail)
            {
                whereClauses.Add("(lc2.sBranchChannelT IN (@RetailBranchChannel, @BrokerBranchChannel, @BlankBranchChannel))");
                parameters.Add(new SqlParameter("@RetailBranchChannel", E_BranchChannelT.Retail));
                parameters.Add(new SqlParameter("@BrokerBranchChannel", E_BranchChannelT.Broker));
                parameters.Add(new SqlParameter("@BlankBranchChannel", E_BranchChannelT.Blank));
            }
            #endregion

            if (getConditions.HasValue)
            {
                whereClauses.Add("t.TaskIsCondition = @GetConditions");
                parameters.Add(new SqlParameter("@GetConditions", getConditions.Value));
            }

            if (sortField == "TaskDueDate" || sortField == "TaskFollowUpDate")
            {
                // 09/14/11 mf. OPM 68972. Order by using smalldatetime.max instead of NULL
                // Execution plan shows it still only does Clustered Index Seek so speed should be hopefully OK.
                sortField = "ISNULL(" + sortField + ", '2079-06-06')";
            }

            string orderBy = "";
            if (!sortField.Equals("ConditionCategory", StringComparison.OrdinalIgnoreCase))
            {
                orderBy = " ORDER BY " + sortField + sortDirection;
            }

            string sql = "SELECT TOP " + (maxRowCount * 3) + " " + string.Join(",", selectColumns.ToArray()) +
                " FROM " + view + " t "
                + " JOIN LOAN_FILE_CACHE lc1 on t.LoanId = lc1.slid" // opm 185732, need to not show sandboxed tasks.  per DD, getting rid of VIEW_LOAN_FILE_CACHE.
                + " JOIN LOAN_FILE_CACHE_2 lc2 on lc1.sLId = lc2.slid"
                + " JOIN LOAN_FILE_CACHE_3 lc3 on lc1.sLId = lc3.sLId"
                + " JOIN LOAN_FILE_CACHE_4 lc4 ON lc1.sLId = lc4.sLId WHERE lc1.IsValid=1 AND "
                + string.Join(" AND ", whereClauses.ToArray()) + orderBy;

            DataSet ds = new DataSet();
            // The only change in this file is to this line, where an old method call is replace by a new method call.
            // Since this new method has already been proved to be correct there is no point creating a unit test for this change.
            DBSelectUtility.FillDataSet(principal.BrokerId, ds, sql, null, parameters);

            int count = 0;
            DataTable table = ds.Tables[0];
            List<ReadOnlyTask> taskList = new List<ReadOnlyTask>();

            Dictionary<int, ConditionCategory> categories =  ConditionCategory.GetCategories(principal.BrokerId).ToDictionary(p => p.Id);
            TaskPermissionProcessor processor = new TaskPermissionProcessor(principal);
            foreach (DataRow row in table.Rows)
            {
                if (count == maxRowCount)
                {
                    break;
                }
                ReadOnlyTask task = new ReadOnlyTask(row);
                if (processor.Resolve(task) != E_UserTaskPermissionLevel.None)
                {
                    if (task.IsCondition && task.CondCategoryId.HasValue )
                    {
                        ConditionCategory category;

                        if (!categories.TryGetValue(task.CondCategoryId.Value, out category))
                        {
                            Tools.LogError("Did not find condition category " + task.CondCategoryId.Value + " " + principal.BrokerId + " " + task.TaskId);
                        }
                        else
                        {
                            task.ConditionCategory = category.Category;
                        }
                    }
                    taskList.Add(task);
                    count++;
                }

            }

            if (sortField.Equals("ConditionCategory", StringComparison.OrdinalIgnoreCase))
            {
                if (sortOrder == SortOrder.Ascending)
                    taskList = taskList.OrderBy(o => o.ConditionCategory).ToList();
                else
                    taskList = taskList.OrderByDescending(o => o.ConditionCategory).ToList();
            }

            return taskList;

        }

        /// <summary>
        /// Updates the cached condition summary of a loan file.
        /// </summary>
        public static void UpdateLoanConditionSummary( Guid sLId )
        {
            List<string> loanNameDependencyFieldList = new List<string>();
            loanNameDependencyFieldList.Add("sfUpdateConditionSummaryContent");
            CPageData dataLoan = new NotEnforceAccessControlPageData(sLId, loanNameDependencyFieldList);
            dataLoan.AllowSaveWhileQP2Sandboxed = true;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.UpdateConditionSummaryContent();
            dataLoan.IsSkipTaskUpdateUponSave = true;
            dataLoan.Save();
        }

        /// <summary>
        /// Increments the loan condition summary version in a loan file,
        /// and adds the loan to the queue to be processed.
        /// </summary>
        public static void BumpLoanConditionSummaryVersionAndEnqueue(Guid brokerId, Guid sLId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@LoanId", sLId)
                                        };
            // Add to queue to be updated later.

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "LOAN_BumpLoanConditionVersion", 3, parameters);
            AddToWorkflowQueue(brokerId, sLId);
        }

        /// <summary>
        /// Add an entry to the condition summary queue
        /// </summary>
        public static void AddToWorkflowQueue(Guid brokerId, Guid sLId)
        {
            if (ConstSite.IsTaskMigrationProcess)
            {
                // 3/3/2012 dd - DO NOT update workflow queue during task migration
                // because we do not want to generate massive email during this process.
                return;
            }
            DBMessageQueue mainQ = new DBMessageQueue(ConstMsg.WorkflowCacheUpdaterQueue);
            string key = string.Format("{0}_ConditionSummary", sLId);
            mainQ.SendIfNew(key, brokerId.ToString(), string.Empty);
        }

        public enum E_TaskConditionOption
        {
            Task = 0,
            Condition = 1,
            IncludeBoth = 2
        }

        /// <summary>
        /// Gets the recent number of resolved task. 
        /// </summary>
        /// <param name="principal">Principal of user we want to count.</param>
        /// <param name="mode">controls which task to count</param>
        /// <returns>The recently closed task or condition (depending on mode)</returns>
        public static int CountRecentlyResolvedTasks(AbstractUserPrincipal principal, E_TaskConditionOption mode)
        {
            int result = 0;
            DateTime since = DateTime.Today.AddDays(-7);
            //"recent" means since last week
            SqlParameter[] parms = GetParametersForTaskStatistics(since, principal, mode);
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.ConnectionInfo, "TASK_GetClosedTaskCountByUserId", parms))
            {
                reader.Read();
                result += int.Parse(reader["UserClosedTasks"].ToString());
            }

            parms = GetParametersForTaskStatistics(since, principal, mode);
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.ConnectionInfo, "TASK_GetResolvedTasksByUserId", parms))
            {
                //Since we don't have a column that tracks resolved dates, we need to examine the task history.
                //This stored procedure returns the histories of all tasks that were
                // 1. Resolved by the user
                // 2. Modified since the "since" date. 
                //This is not exactly what we want, so we need to do a bit of checking before we count it all up.

                while (reader.Read())
                {
                    XDocument doc = XDocument.Parse((string)reader["TaskHistoryXml"]);

                    foreach (XElement SaveEntry in doc.Descendants("Save"))
                    {
                        var whenAttr = SaveEntry.Attribute("When");
                        if (whenAttr == null) continue;

                        //whenAttr has "PT" at the very end, which DateTime can't parse.
                        DateTime when = DateTime.Parse(whenAttr.Value.Replace("PT", "")).Date;
                        //Since the document order matches historical order, we can skip everything that happens before our time period
                        if (when < since) break;

                        //We only care about things that the current user did.
                        var byAttr = SaveEntry.Attribute("By");
                        if (byAttr == null) continue;
                        //This is how the task system calculates the By field's value
                        if (!byAttr.Value.Equals(principal.FirstName + " " + principal.LastName)) continue;

                        //If it was recently closed by this user we don't want to double-count this task - the previous query counts it.
                        if (SaveEntry.Descendants("Closed").Count() > 0) break;

                        //And finally if it was resolved we count it and we're done.
                        if (SaveEntry.Descendants("Resolved").Count() > 0 || SaveEntry.Descendants("ResolvedAssignedTo").Count() > 0)
                        {
                            result++;
                            break;
                        }
                    }
                }
            }

            return result;
        }

        private static SqlParameter[] GetParametersForTaskStatistics(DateTime since, AbstractUserPrincipal BrokerUser, E_TaskConditionOption mode)
        {

            bool? isCondition;

            switch (mode)
            {
                case E_TaskConditionOption.Task:
                    isCondition = false ;
                    break;
                case E_TaskConditionOption.Condition:
                    isCondition = true;
                    break;
                case E_TaskConditionOption.IncludeBoth:
                    isCondition = null;
                    break;
                default:
                    throw new UnhandledEnumException(mode);
            }

            return new SqlParameter[]
            {
                new SqlParameter("@BrokerId", BrokerUser.BrokerId),
                new SqlParameter("@UserId", BrokerUser.UserId),
                new SqlParameter("@SinceDate", since),
                new SqlParameter("@IsCondition", isCondition)
            };
        }

    }
}
