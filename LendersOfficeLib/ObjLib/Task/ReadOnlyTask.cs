﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace LendersOffice.ObjLib.Task
{
    public class ReadOnlyTask
    {
        public string TaskId { get; private set; }
        public Guid LoanId { get; private set; }
        public string TaskSubject { get; private set; }
        public E_TaskStatus TaskStatus { get; private set;}
        public string TaskStatus_rep { get { return TaskStatus.ToString(); } }
        public string LoanNumCached { get; private set; }
        public string BorrowerNmCached { get; private set; }
        public string BorrowerLastNmCached { get; private set; }
        public DateTime? TaskDueDate { get; private set; }
        public DateTime? TaskFollowUpDate { get; private set; }
        public DateTime TaskLastModifiedDate { get; private set; }
        public string AssignedUserFullName { get; private set; }
        public string OwnerFullName { get; private set; }
        public bool CondIsHidden { get; private set; }
        public int TaskPermissionLevelId { get; private set; }
        public Guid TaskOwnerUserId { get; private set; }
        public int? CondCategoryId { get; private set; }
        public bool IsCondition { get; private set; }

        /// <summary>
        /// Populated by caller to save a join in the view. Need to review this.
        /// </summary>
        public string ConditionCategory { get; set; }

        public ReadOnlyTask(DataRow row)
        {
            TaskId = (string) row["TaskId"];
            LoanId = (Guid)row["LoanId"];
            TaskSubject = (string)row["TaskSubject"];
            TaskStatus = (E_TaskStatus)row["TaskStatus"];
            LoanNumCached = (string)row["LoanNumCached"];
            BorrowerNmCached = (string)row["BorrowerNmCached"];
            TaskDueDate = row["TaskDueDate"] == DBNull.Value ? null : (DateTime?)row["TaskDueDate"];
            TaskFollowUpDate = row["TaskFollowUpDate"] == DBNull.Value ? null : (DateTime?)row["TaskFollowUpDate"];
            TaskLastModifiedDate = (DateTime)row["TaskLastModifiedDate"];
            AssignedUserFullName = (string)row["TaskAssignedUserFullName"];
            OwnerFullName = (string)row["TaskOwnerFullName"];

            CondIsHidden = (bool)row["CondIsHidden"];
            TaskPermissionLevelId = (int)row["TaskPermissionLevelId"];
            TaskOwnerUserId = (Guid)row["TaskOwnerUserId"];
            BorrowerLastNmCached = (string)row["BorrowerLastNmCached"];

            IsCondition = (bool)row["TaskIsCondition"];
            if (IsCondition)
            {
                CondCategoryId = (int)row["CondCategoryId"];
            }
        }
    }
}
