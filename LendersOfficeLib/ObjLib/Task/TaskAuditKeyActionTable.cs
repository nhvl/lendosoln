using System.Collections.Generic;
using System;
using System.IO;
using LookUpTableLib;
using DataAccess;

namespace LendersOffice.ObjLib.Task
{
	public enum E_TaskAuditKeyActionTable_Action
	{
		AssignedBy,
		ClosedAndAssignedBy,
		ClosedBy,
		EditedBy,
		OpenedAndAssignedBy,
		ReactivatedAndAssignedBy,
		ReactivatedBy,
		ResolvedAndAssignedBy,
		ResolvedAndClosedBy,
		ResolvedBy,
		ResolvedClosedAndAssignedBy,
        DeletedBy,
        RestoredBy,
	}
	
/* Table's Schema 
* ======================
* isCreating(bool):false true
* isAssigning(bool):false true
* isChangingStatus(bool):false true
* StatusFrom(enum:E_TaskStatus):Active Resolved Closed
* StatusTo(enum:E_TaskStatus):Active Resolved Closed
*/

	public class TaskAuditKeyActionTable : ComboTableLUBase
	{
		private static readonly TaskAuditKeyActionTable Instance = new TaskAuditKeyActionTable();
		private TaskAuditKeyActionTable()
		{
			schema = new List<NewTableVariable>();
			var Vars = new []
			{
				new { name = "isCreating", TypeName = "bool", values = new string[]{"false", "true"}},
				new { name = "isAssigning", TypeName = "bool", values = new string[]{"false", "true"}},
				new { name = "isChangingStatus", TypeName = "bool", values = new string[]{"false", "true"}},
				new { name = "DeletionState", TypeName = "enum:E_ConditionDeletionState", values = new string[]{"NoChange", "BeingDeleted", "BeingRestored"}},
				new { name = "StatusFrom", TypeName = "enum:E_TaskStatus", values = new string[]{"Active", "Resolved", "Closed"}},
				new { name = "StatusTo", TypeName = "enum:E_TaskStatus", values = new string[]{"Active", "Resolved", "Closed"}}
			};
			
			foreach (var v in Vars)
			{
				schema.Add(new NewTableVariable(v.name, v.TypeName, v.values));
			}
			actionStrs = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase){
				{"iscreating:false;isassigning:false;ischangingstatus:false;deletionstate:nochange;statusfrom:active;statusto:active;", "EditedBy"},
				{"iscreating:false;isassigning:false;ischangingstatus:false;deletionstate:nochange;statusfrom:active;statusto:resolved;", "+(ResolvedBy)"},
				{"iscreating:false;isassigning:false;ischangingstatus:false;deletionstate:nochange;statusfrom:active;statusto:closed;", "+(ResolvedAndClosedBy)"},
				{"iscreating:false;isassigning:false;ischangingstatus:false;deletionstate:nochange;statusfrom:resolved;statusto:active;", "+(ReactivatedBy)"},
				{"iscreating:false;isassigning:false;ischangingstatus:false;deletionstate:nochange;statusfrom:resolved;statusto:resolved;", "EditedBy"},
				{"iscreating:false;isassigning:false;ischangingstatus:false;deletionstate:nochange;statusfrom:resolved;statusto:closed;", "+(ClosedBy)"},
				{"iscreating:false;isassigning:false;ischangingstatus:false;deletionstate:nochange;statusfrom:closed;statusto:active;", "+(ReactivatedBy)"},
				{"iscreating:false;isassigning:false;ischangingstatus:false;deletionstate:nochange;statusfrom:closed;statusto:resolved;", "+(-(-(+(+(-(EmailedBy)));+(+(-(EditedBy))));-(+(+(-(EmailedBy)));+(+(-(EditedBy))))))"},
				{"iscreating:false;isassigning:false;ischangingstatus:false;deletionstate:nochange;statusfrom:closed;statusto:closed;", "EditedBy"},
				{"iscreating:false;isassigning:false;ischangingstatus:true;deletionstate:nochange;statusfrom:active;statusto:active;", "+(EditedBy)"},
				{"iscreating:false;isassigning:false;ischangingstatus:true;deletionstate:nochange;statusfrom:active;statusto:resolved;", "ResolvedBy"},
				{"iscreating:false;isassigning:false;ischangingstatus:true;deletionstate:nochange;statusfrom:active;statusto:closed;", "ResolvedAndClosedBy"},
				{"iscreating:false;isassigning:false;ischangingstatus:true;deletionstate:nochange;statusfrom:resolved;statusto:active;", "ReactivatedBy"},
				{"iscreating:false;isassigning:false;ischangingstatus:true;deletionstate:nochange;statusfrom:resolved;statusto:resolved;", "+(EditedBy)"},
				{"iscreating:false;isassigning:false;ischangingstatus:true;deletionstate:nochange;statusfrom:resolved;statusto:closed;", "ClosedBy"},
				{"iscreating:false;isassigning:false;ischangingstatus:true;deletionstate:nochange;statusfrom:closed;statusto:active;", "ReactivatedBy"},
				{"iscreating:false;isassigning:false;ischangingstatus:true;deletionstate:nochange;statusfrom:closed;statusto:resolved;", "+(-(-(+(+(-(EmailedBy)));+(+(-(EditedBy))));-(+(+(-(EmailedBy)));+(+(-(EditedBy))))))"},
				{"iscreating:false;isassigning:false;ischangingstatus:true;deletionstate:nochange;statusfrom:closed;statusto:closed;", "+(EditedBy)"},
				{"iscreating:false;isassigning:true;ischangingstatus:false;deletionstate:nochange;statusfrom:active;statusto:active;", "AssignedBy"},
				{"iscreating:false;isassigning:true;ischangingstatus:false;deletionstate:nochange;statusfrom:active;statusto:resolved;", "+(ResolvedAndAssignedToBy)"},
				{"iscreating:false;isassigning:true;ischangingstatus:false;deletionstate:nochange;statusfrom:active;statusto:closed;", "+(ResolvedAndClosedAndAssignedToBy)"},
				{"iscreating:false;isassigning:true;ischangingstatus:false;deletionstate:nochange;statusfrom:resolved;statusto:active;", "+(ReactivatedAndAssignedToBy)"},
				{"iscreating:false;isassigning:true;ischangingstatus:false;deletionstate:nochange;statusfrom:resolved;statusto:resolved;", "AssignedBy"},
				{"iscreating:false;isassigning:true;ischangingstatus:false;deletionstate:nochange;statusfrom:resolved;statusto:closed;", "+(ClosedAndAssignedToBy)"},
				{"iscreating:false;isassigning:true;ischangingstatus:false;deletionstate:nochange;statusfrom:closed;statusto:active;", "+(ReactivatedAndAssignedToBy)"},
				{"iscreating:false;isassigning:true;ischangingstatus:false;deletionstate:nochange;statusfrom:closed;statusto:closed;", "AssignedBy"},
				{"iscreating:false;isassigning:true;ischangingstatus:true;deletionstate:nochange;statusfrom:active;statusto:active;", "+(AssignedToBy)"},
				{"iscreating:false;isassigning:true;ischangingstatus:true;deletionstate:nochange;statusfrom:active;statusto:resolved;", "ResolvedAndAssignedBy"},
				{"iscreating:false;isassigning:true;ischangingstatus:true;deletionstate:nochange;statusfrom:active;statusto:closed;", "ResolvedClosedAndAssignedBy"},
				{"iscreating:false;isassigning:true;ischangingstatus:true;deletionstate:nochange;statusfrom:resolved;statusto:active;", "ReactivatedAndAssignedBy"},
				{"iscreating:false;isassigning:true;ischangingstatus:true;deletionstate:nochange;statusfrom:resolved;statusto:resolved;", "+(AssignedToBy)"},
				{"iscreating:false;isassigning:true;ischangingstatus:true;deletionstate:nochange;statusfrom:resolved;statusto:closed;", "ClosedAndAssignedBy"},
				{"iscreating:false;isassigning:true;ischangingstatus:true;deletionstate:nochange;statusfrom:closed;statusto:active;", "ReactivatedAndAssignedBy"},
				{"iscreating:false;isassigning:true;ischangingstatus:true;deletionstate:nochange;statusfrom:closed;statusto:closed;", "+(AssignedToBy)"},
				{"iscreating:true;isassigning:true;ischangingstatus:false;deletionstate:nochange;statusfrom:active;statusto:active;", "OpenedAndAssignedBy"},
				{"iscreating:true;isassigning:true;ischangingstatus:true;deletionstate:nochange;statusfrom:active;statusto:active;", "+(CreatedAndAssignedToBy)"},
				{"iscreating:false;isassigning:false;ischangingstatus:false;deletionstate:beingdeleted;statusfrom:active;statusto:active;", "DeletedBy"},
				{"iscreating:false;isassigning:false;ischangingstatus:false;deletionstate:beingdeleted;statusfrom:resolved;statusto:resolved;", "DeletedBy"},
				{"iscreating:false;isassigning:false;ischangingstatus:false;deletionstate:beingdeleted;statusfrom:closed;statusto:closed;", "DeletedBy"},
				{"iscreating:false;isassigning:false;ischangingstatus:false;deletionstate:beingrestored;statusfrom:active;statusto:active;", "RestoredBy"},
				{"iscreating:false;isassigning:false;ischangingstatus:false;deletionstate:beingrestored;statusfrom:resolved;statusto:resolved;", "RestoredBy"},
				{"iscreating:false;isassigning:false;ischangingstatus:false;deletionstate:beingrestored;statusfrom:closed;statusto:closed;", "RestoredBy"},
			};
		}
		
        private E_TaskAuditKeyActionTable_Action LookUpNonStatic(Boolean @isCreating, Boolean @isAssigning, Boolean @isChangingStatus, E_ConditionDeletionState @DeletionState, E_TaskStatus @StatusFrom, E_TaskStatus @StatusTo)
		{
			switch(@StatusFrom)
			{
				case E_TaskStatus.Active:
					break;
				case E_TaskStatus.Resolved:
					break;
				case E_TaskStatus.Closed:
					break;
				default:
					throw new Exception(@StatusFrom+" was not recognized as a E_TaskStatus.");
			}
			
			switch(@StatusTo)
			{
				case E_TaskStatus.Active:
					break;
				case E_TaskStatus.Resolved:
					break;
				case E_TaskStatus.Closed:
					break;
				default:
					throw new Exception(@StatusTo+" was not recognized as a E_TaskStatus.");
			}

            switch (@DeletionState)
			{
                case E_ConditionDeletionState.NoChange:
                case E_ConditionDeletionState.BeingDeleted:
                case E_ConditionDeletionState.BeingRestored:
					break;
				default:
                    throw new Exception(@DeletionState + " was not recognized as a E_ConditionDeletionState.");
			}


            List<string> varnames = new List<string>(new string[] { "isCreating", "isAssigning", "isChangingStatus", "DeletionState", "StatusFrom", "StatusTo" });
            List<string> vals = new List<string>(new string[] { @isCreating.ToString(), @isAssigning.ToString(), @isChangingStatus.ToString(), @DeletionState.ToString(), @StatusFrom.ToString(), @StatusTo.ToString() });
			string actionStr = pLookUp(varnames, vals);
			Type actionType = typeof(E_TaskAuditKeyActionTable_Action);
			E_TaskAuditKeyActionTable_Action action = (E_TaskAuditKeyActionTable_Action)Enum.Parse(actionType, actionStr, true);
			return action;
		}
        public static E_TaskAuditKeyActionTable_Action LookUp(Boolean @isCreating, Boolean @isAssigning, Boolean @isChangingStatus, E_ConditionDeletionState @DeletionState, E_TaskStatus @StatusFrom, E_TaskStatus @StatusTo)
		{
            return Instance.LookUpNonStatic(@isCreating, @isAssigning, @isChangingStatus, @DeletionState, @StatusFrom, @StatusTo);
		}
	}
}
#region <xml>
/*
<?xml version="1.0" encoding="utf-8"?>
<NewTableObj xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <head>
    <NewTableVariable>
      <name>isCreating</name>
      <currValueIndex>0</currValueIndex>
      <TypeName>bool</TypeName>
      <Name>isCreating</Name>
      <Values>
        <string>false</string>
        <string>true</string>
      </Values>
      <CurrValueIndex>0</CurrValueIndex>
    </NewTableVariable>
    <NewTableVariable>
      <name>isAssigning</name>
      <currValueIndex>0</currValueIndex>
      <TypeName>bool</TypeName>
      <Name>isAssigning</Name>
      <Values>
        <string>false</string>
        <string>true</string>
      </Values>
      <CurrValueIndex>0</CurrValueIndex>
    </NewTableVariable>
    <NewTableVariable>
      <name>isChangingStatus</name>
      <currValueIndex>0</currValueIndex>
      <TypeName>bool</TypeName>
      <Name>isChangingStatus</Name>
      <Values>
        <string>false</string>
        <string>true</string>
      </Values>
      <CurrValueIndex>0</CurrValueIndex>
    </NewTableVariable>
    <NewTableVariable>
      <name>StatusFrom</name>
      <currValueIndex>0</currValueIndex>
      <TypeName>enum:E_TaskStatus</TypeName>
      <Name>StatusFrom</Name>
      <Values>
        <string>Active</string>
        <string>Resolved</string>
        <string>Closed</string>
      </Values>
      <CurrValueIndex>0</CurrValueIndex>
    </NewTableVariable>
    <NewTableVariable>
      <name>StatusTo</name>
      <currValueIndex>0</currValueIndex>
      <TypeName>enum:E_TaskStatus</TypeName>
      <Name>StatusTo</Name>
      <Values>
        <string>Active</string>
        <string>Resolved</string>
        <string>Closed</string>
      </Values>
      <CurrValueIndex>0</CurrValueIndex>
    </NewTableVariable>
  </head>
  <actions>
    <actionDic numItems="36">
      <item>
        <key>iscreating:false;isassigning:false;ischangingstatus:false;statusfrom:resolved;statusto:active;</key>
        <value>+(ReactivatedBy)</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:false;ischangingstatus:false;statusfrom:closed;statusto:active;</key>
        <value>+(ReactivatedBy)</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:false;ischangingstatus:false;statusfrom:active;statusto:resolved;</key>
        <value>+(ResolvedBy)</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:false;ischangingstatus:false;statusfrom:active;statusto:closed;</key>
        <value>+(ResolvedAndClosedBy)</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:false;ischangingstatus:false;statusfrom:resolved;statusto:closed;</key>
        <value>+(ClosedBy)</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:false;ischangingstatus:false;statusfrom:closed;statusto:resolved;</key>
        <value>+(-(-(+(+(-(EmailedBy))); +(+(-(EditedBy)))); -(+(+(-(EmailedBy))); +(+(-(EditedBy))))))</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:true;ischangingstatus:false;statusfrom:active;statusto:resolved;</key>
        <value>+(ResolvedAndAssignedToBy)</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:true;ischangingstatus:false;statusfrom:active;statusto:closed;</key>
        <value>+(ResolvedAndClosedAndAssignedToBy)</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:true;ischangingstatus:false;statusfrom:resolved;statusto:active;</key>
        <value>+(ReactivatedAndAssignedToBy)</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:true;ischangingstatus:false;statusfrom:resolved;statusto:closed;</key>
        <value>+(ClosedAndAssignedToBy)</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:true;ischangingstatus:false;statusfrom:closed;statusto:active;</key>
        <value>+(ReactivatedAndAssignedToBy)</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:false;ischangingstatus:true;statusfrom:active;statusto:active;</key>
        <value>+(EditedBy)</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:false;ischangingstatus:true;statusfrom:resolved;statusto:resolved;</key>
        <value>+(EditedBy)</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:false;ischangingstatus:true;statusfrom:closed;statusto:resolved;</key>
        <value>+(-(-(+(+(-(EmailedBy))); +(+(-(EditedBy)))); -(+(+(-(EmailedBy))); +(+(-(EditedBy))))))</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:false;ischangingstatus:true;statusfrom:closed;statusto:closed;</key>
        <value>+(EditedBy)</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:true;ischangingstatus:true;statusfrom:active;statusto:active;</key>
        <value>+(AssignedToBy)</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:true;ischangingstatus:true;statusfrom:resolved;statusto:resolved;</key>
        <value>+(AssignedToBy)</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:true;ischangingstatus:true;statusfrom:closed;statusto:closed;</key>
        <value>+(AssignedToBy)</value>
      </item>
      <item>
        <key>iscreating:true;isassigning:true;ischangingstatus:true;statusfrom:active;statusto:active;</key>
        <value>+(CreatedAndAssignedToBy)</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:false;ischangingstatus:false;statusfrom:active;statusto:active;</key>
        <value>EditedBy</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:false;ischangingstatus:false;statusfrom:resolved;statusto:resolved;</key>
        <value>EditedBy</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:false;ischangingstatus:false;statusfrom:closed;statusto:closed;</key>
        <value>EditedBy</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:false;ischangingstatus:true;statusfrom:active;statusto:resolved;</key>
        <value>ResolvedBy</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:false;ischangingstatus:true;statusfrom:active;statusto:closed;</key>
        <value>ResolvedAndClosedBy</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:false;ischangingstatus:true;statusfrom:resolved;statusto:active;</key>
        <value>ReactivatedBy</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:false;ischangingstatus:true;statusfrom:resolved;statusto:closed;</key>
        <value>ClosedBy</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:false;ischangingstatus:true;statusfrom:closed;statusto:active;</key>
        <value>ReactivatedBy</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:true;ischangingstatus:false;statusfrom:active;statusto:active;</key>
        <value>AssignedBy</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:true;ischangingstatus:false;statusfrom:resolved;statusto:resolved;</key>
        <value>AssignedBy</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:true;ischangingstatus:false;statusfrom:closed;statusto:closed;</key>
        <value>AssignedBy</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:true;ischangingstatus:true;statusfrom:active;statusto:resolved;</key>
        <value>ResolvedAndAssignedBy</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:true;ischangingstatus:true;statusfrom:active;statusto:closed;</key>
        <value>ResolvedClosedAndAssignedBy</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:true;ischangingstatus:true;statusfrom:resolved;statusto:active;</key>
        <value>ReactivatedAndAssignedBy</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:true;ischangingstatus:true;statusfrom:resolved;statusto:closed;</key>
        <value>ClosedAndAssignedBy</value>
      </item>
      <item>
        <key>iscreating:false;isassigning:true;ischangingstatus:true;statusfrom:closed;statusto:active;</key>
        <value>ReactivatedAndAssignedBy</value>
      </item>
      <item>
        <key>iscreating:true;isassigning:true;ischangingstatus:false;statusfrom:active;statusto:active;</key>
        <value>OpenedAndAssignedBy</value>
      </item>
    </actionDic>
  </actions>
  <rules>
    <rules>
      <NewRule>
        <variables>
          <NewTableVariable>
            <name>StatusFrom</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusFrom</Name>
            <Values>
              <string>Closed</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusTo</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusTo</Name>
            <Values>
              <string>Resolved</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
        </variables>
        <actions>[hide]</actions>
      </NewRule>
      <NewRule>
        <variables>
          <NewTableVariable>
            <name>IsCreating</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>bool</TypeName>
            <Name>IsCreating</Name>
            <Values>
              <string>true</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusFrom</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusFrom</Name>
            <Values>
              <string>Resolved</string>
              <string>Closed</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
        </variables>
        <actions>[hide]</actions>
      </NewRule>
      <NewRule>
        <variables>
          <NewTableVariable>
            <name>IsCreating</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>bool</TypeName>
            <Name>IsCreating</Name>
            <Values>
              <string>true</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusTo</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusTo</Name>
            <Values>
              <string>Resolved</string>
              <string>Closed</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
        </variables>
        <actions>[hide]</actions>
      </NewRule>
      <NewRule>
        <variables>
          <NewTableVariable>
            <name>IsCreating</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>bool</TypeName>
            <Name>IsCreating</Name>
            <Values>
              <string>true</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>IsAssigning</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>bool</TypeName>
            <Name>IsAssigning</Name>
            <Values>
              <string>false</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
        </variables>
        <actions>[hide]</actions>
      </NewRule>
      <NewRule>
        <variables>
          <NewTableVariable>
            <name>IsChangingStatus</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>bool</TypeName>
            <Name>IsChangingStatus</Name>
            <Values>
              <string>false</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusFrom</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusFrom</Name>
            <Values>
              <string>Active</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusTo</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusTo</Name>
            <Values>
              <string>Resolved</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
        </variables>
        <actions>[hide]</actions>
      </NewRule>
      <NewRule>
        <variables>
          <NewTableVariable>
            <name>IsChangingStatus</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>bool</TypeName>
            <Name>IsChangingStatus</Name>
            <Values>
              <string>false</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusFrom</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusFrom</Name>
            <Values>
              <string>Active</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusTo</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusTo</Name>
            <Values>
              <string>Closed</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
        </variables>
        <actions>[hide]</actions>
      </NewRule>
      <NewRule>
        <variables>
          <NewTableVariable>
            <name>IsChangingStatus</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>bool</TypeName>
            <Name>IsChangingStatus</Name>
            <Values>
              <string>false</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusFrom</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusFrom</Name>
            <Values>
              <string>Resolved</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusTo</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusTo</Name>
            <Values>
              <string>Active</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
        </variables>
        <actions>[hide]</actions>
      </NewRule>
      <NewRule>
        <variables>
          <NewTableVariable>
            <name>IsChangingStatus</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>bool</TypeName>
            <Name>IsChangingStatus</Name>
            <Values>
              <string>false</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusFrom</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusFrom</Name>
            <Values>
              <string>Resolved</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusTo</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusTo</Name>
            <Values>
              <string>Closed</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
        </variables>
        <actions>[hide]</actions>
      </NewRule>
      <NewRule>
        <variables>
          <NewTableVariable>
            <name>IsChangingStatus</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>bool</TypeName>
            <Name>IsChangingStatus</Name>
            <Values>
              <string>false</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusFrom</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusFrom</Name>
            <Values>
              <string>Closed</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusTo</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusTo</Name>
            <Values>
              <string>Active</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
        </variables>
        <actions>[hide]</actions>
      </NewRule>
      <NewRule>
        <variables>
          <NewTableVariable>
            <name>IsChangingStatus</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>bool</TypeName>
            <Name>IsChangingStatus</Name>
            <Values>
              <string>false</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusFrom</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusFrom</Name>
            <Values>
              <string>Closed</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusTo</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusTo</Name>
            <Values>
              <string>Resolved</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
        </variables>
        <actions>[hide]</actions>
      </NewRule>
      <NewRule>
        <variables>
          <NewTableVariable>
            <name>IsChangingStatus</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>bool</TypeName>
            <Name>IsChangingStatus</Name>
            <Values>
              <string>true</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusFrom</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusFrom</Name>
            <Values>
              <string>Active</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusTo</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusTo</Name>
            <Values>
              <string>Active</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
        </variables>
        <actions>[hide]</actions>
      </NewRule>
      <NewRule>
        <variables>
          <NewTableVariable>
            <name>IsChangingStatus</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>bool</TypeName>
            <Name>IsChangingStatus</Name>
            <Values>
              <string>true</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusFrom</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusFrom</Name>
            <Values>
              <string>Resolved</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusTo</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusTo</Name>
            <Values>
              <string>Resolved</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
        </variables>
        <actions>[hide]</actions>
      </NewRule>
      <NewRule>
        <variables>
          <NewTableVariable>
            <name>IsChangingStatus</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>bool</TypeName>
            <Name>IsChangingStatus</Name>
            <Values>
              <string>true</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusFrom</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusFrom</Name>
            <Values>
              <string>Closed</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
          <NewTableVariable>
            <name>StatusTo</name>
            <currValueIndex>0</currValueIndex>
            <TypeName>enum:E_TaskStatus</TypeName>
            <Name>StatusTo</Name>
            <Values>
              <string>Closed</string>
            </Values>
            <CurrValueIndex>0</CurrValueIndex>
          </NewTableVariable>
        </variables>
        <actions>[hide]</actions>
      </NewRule>
    </rules>
  </rules>
</NewTableObj>*/
#endregion
