﻿namespace LendersOffice.ObjLib.Task
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;

    using DataAccess;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;
    
    /// <summary>
    /// State bag for holding information about a task and the triggers associated with it.
    /// </summary>
    public class TaskTriggerAssociation
    {
        /// <summary>
        /// Stored procedure name for creating a task trigger association.
        /// </summary>
        private static readonly StoredProcedureName TaskTriggerAssociationCreateSp = StoredProcedureName.Create("TASK_TRIGGER_ASSOCIATION_Create").Value;

        /// <summary>
        /// Stored procedure name for deleting a task trigger association by type.
        /// </summary>
        private static readonly StoredProcedureName TaskTriggerAssociationDeleteSp = StoredProcedureName.Create("TASK_TRIGGER_ASSOCIATION_Delete").Value;

        /// <summary>
        /// Stored procedure name for getting all task trigger associations for a task or loan.
        /// </summary>
        private static readonly StoredProcedureName TaskTriggerAssociationGetSp = StoredProcedureName.Create("TASK_TRIGGER_ASSOCIATION_Get").Value;

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskTriggerAssociation"/> class.
        /// </summary>
        /// <param name="reader">The reader to read from.</param>
        private TaskTriggerAssociation(IDataReader reader)
        {
            this.Id = (int)reader["Id"];
            this.BrokerId = (Guid)reader["BrokerId"];
            this.LoanId = (Guid)reader["LoanId"];
            this.TaskId = (string)reader["TaskId"];
            this.TriggerType = (TaskTriggerType)Convert.ToInt32(reader["TriggerType"]);
            this.TriggerName = (string)reader["TriggerName"];
        }

        /// <summary>
        /// Gets the Association ID.
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Gets the broker ID.
        /// </summary>
        public Guid BrokerId { get; }

        /// <summary>
        /// Gets the loan ID.
        /// </summary>
        public Guid LoanId { get; }

        /// <summary>
        /// Gets the task ID.
        /// </summary>
        public string TaskId { get; }

        /// <summary>
        /// Gets the task trigger type.
        /// </summary>
        public TaskTriggerType TriggerType { get; }

        /// <summary>
        /// Gets the trigger name.
        /// </summary>
        public string TriggerName { get; }

        /// <summary>
        /// Gets an enumerable of task trigger associations for all tasks for the specified loan.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <param name="loanId">The Loan ID.</param>
        /// <returns>An <see cref="IReadOnlyCollection"/> of <see cref="TaskTriggerAssociation"/>.</returns>
        public static IReadOnlyDictionary<string, IReadOnlyDictionary<TaskTriggerType, TaskTriggerAssociation>> GetTaskTriggerAssociationsForLoan(Guid brokerId, Guid loanId)
        {
            var dictionary = new Dictionary<string, IReadOnlyDictionary<TaskTriggerType, TaskTriggerAssociation>>(StringComparer.OrdinalIgnoreCase);

            SqlParameter[] parameters = new[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", loanId)
            };

            using (var conn = DbAccessUtils.GetConnection(brokerId))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, TaskTriggerAssociationGetSp, parameters, TimeoutInSeconds.Default))
            {
                while (reader.Read())
                {
                    TaskTriggerAssociation association = new TaskTriggerAssociation(reader);

                    Dictionary<TaskTriggerType, TaskTriggerAssociation> associationsByType = null;
                    IReadOnlyDictionary<TaskTriggerType, TaskTriggerAssociation> readOnlyDict = null;
                    if (dictionary.TryGetValue(association.TaskId, out readOnlyDict))
                    {
                        associationsByType = (Dictionary<TaskTriggerType, TaskTriggerAssociation>)readOnlyDict;
                    }
                    else
                    {
                        associationsByType = new Dictionary<TaskTriggerType, TaskTriggerAssociation>();
                        dictionary.Add(association.TaskId, associationsByType);
                    }

                    associationsByType.Add(association.TriggerType, association);
                }
            }

            return dictionary;
        }

        /// <summary>
        /// Gets a mapping of task trigger associations by trigger type for the specified task.
        /// </summary>
        /// <param name="task">The Task whose trigger associations we want.</param>
        /// <returns>An <see cref="IReadOnlyDictionary"/> of <see cref="TaskTriggerType"/> mapped to <see cref="TaskTriggerAssociation"/>.</returns>
        public static IReadOnlyDictionary<TaskTriggerType, TaskTriggerAssociation> GetTaskTriggerAssociations(Task task)
        {
            return GetTaskTriggerAssociations(task.BrokerId, task.LoanId, task.TaskId);
        }

        /// <summary>
        /// Gets a mapping of task trigger associations by trigger type for the specified task.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <param name="loanId">The Loan ID.</param>
        /// <param name="taskId">The Task ID.</param>
        /// <returns>An <see cref="IReadOnlyDictionary"/> of <see cref="TaskTriggerType"/> mapped to <see cref="TaskTriggerAssociation"/>.</returns>
        public static IReadOnlyDictionary<TaskTriggerType, TaskTriggerAssociation> GetTaskTriggerAssociations(Guid brokerId, Guid loanId, string taskId)
        {
            Dictionary<TaskTriggerType, TaskTriggerAssociation> associationsByType = new Dictionary<TaskTriggerType, TaskTriggerAssociation>();

            // Task ID is null for new tasks, which should not have any existing associations anyway.
            if (string.IsNullOrEmpty(taskId))
            {
                return associationsByType;
            }

            SqlParameter[] parameters = new[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@TaskId", taskId)
            };

            using (var conn = DbAccessUtils.GetConnection(brokerId))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, TaskTriggerAssociationGetSp, parameters, TimeoutInSeconds.Default))
            {
                while (reader.Read())
                {
                    TaskTriggerAssociation association = new TaskTriggerAssociation(reader);
                    associationsByType.Add(association.TriggerType, association);
                }
            }

            return associationsByType;
        }

        /// <summary>
        /// Gets a mapping of task trigger associations by trigger type for the specified task.
        /// </summary>
        /// <param name="task">The Task whose trigger associations we want.</param>
        /// <param name="triggerType">Task Trigger Type.</param>
        /// <returns>A <see cref="TaskTriggerAssociation"/> of the given <see cref="TaskTriggerType"/>. Or null if no association of that type exists.</returns>
        public static TaskTriggerAssociation GetTaskTriggerAssociation(Task task, TaskTriggerType triggerType)
        {
            return GetTaskTriggerAssociation(task.BrokerId, task.LoanId, task.TaskId, triggerType);
        }

        /// <summary>
        /// Gets a mapping of task trigger associations by trigger type for the specified task.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <param name="loanId">The Loan ID.</param>
        /// <param name="taskId">The Task ID.</param>
        /// <param name="triggerType">The type of trigger the association is for.</param>
        /// <returns>A <see cref="TaskTriggerAssociation"/> of the given <see cref="TaskTriggerType"/>. Or null if no association of that type exists.</returns>
        public static TaskTriggerAssociation GetTaskTriggerAssociation(Guid brokerId, Guid loanId, string taskId, TaskTriggerType triggerType)
        {
            SqlParameter[] parameters = new[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@TaskId", taskId),
                new SqlParameter("@TriggerType", triggerType)
            };

            using (var conn = DbAccessUtils.GetConnection(brokerId))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, TaskTriggerAssociationGetSp, parameters, TimeoutInSeconds.Default))
            {
                if (reader.Read())
                {
                    return new TaskTriggerAssociation(reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Saves task trigger association data to DB.
        /// Should be called whenever a task is saved/updated.
        /// </summary>
        /// <param name="task">A task with up to date info.</param>
        /// <param name="triggerType">The type of trigger the association is for.</param>
        /// <param name="triggerName">The name of the Trigger.</param>
        public static void CreateNewAssociation(Task task, TaskTriggerType triggerType, string triggerName)
        {
            SqlParameter[] parameters = new[]
           {
                new SqlParameter("@BrokerId", task.BrokerId),
                new SqlParameter("@LoanId", task.LoanId),
                new SqlParameter("@TaskId", task.TaskId),
                new SqlParameter("@TriggerType", triggerType),
                new SqlParameter("@TriggerName", triggerName)
            };

            using (var conn = DbAccessUtils.GetConnection(task.BrokerId))
            {
                StoredProcedureDriverHelper.ExecuteNonQuery(conn, null, TaskTriggerAssociationCreateSp, parameters, TimeoutInSeconds.Default);
            }
        }

        /// <summary>
        /// Delete task trigger associations by type.
        /// </summary>
        /// <param name="task">The Task whose trigger associations we want to delete.</param>
        /// <param name="triggerType">Task Trigger Type to delete.</param>
        public static void DeleteTaskTriggerAssociations(Task task, TaskTriggerType triggerType)
        {
            DeleteTaskTriggerAssociations(task.BrokerId, task.LoanId, task.TaskId, triggerType);
        }

        /// <summary>
        /// Delete task trigger associations by type.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <param name="loanId">The Loan ID.</param>
        /// <param name="taskId">The Task ID.</param>
        /// <param name="triggerType">The Trigger Type.</param>
        public static void DeleteTaskTriggerAssociations(Guid brokerId, Guid loanId, string taskId, TaskTriggerType triggerType)
        {
            SqlParameter[] parameters = new[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@TaskId", taskId),
                new SqlParameter("@TriggerType", triggerType)
            };

            using (var conn = DbAccessUtils.GetConnection(brokerId))
            {
                StoredProcedureDriverHelper.ExecuteNonQuery(conn, null, TaskTriggerAssociationDeleteSp, parameters, TimeoutInSeconds.Default);
            }
        }
    }
}
