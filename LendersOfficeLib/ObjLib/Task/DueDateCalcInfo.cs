﻿namespace LendersOffice.ObjLib.Task
{
    /// <summary>
    /// Contains the information necessary to calculate a due date for a task.
    /// </summary>
    public struct DueDateCalcInfo
    {
        /// <summary>
        /// The field id the calculation is based on.
        /// </summary>
        public readonly string FieldId;

        /// <summary>
        /// The offset in days from the base field value.
        /// </summary>
        public readonly int Offset;

        /// <summary>
        /// Initializes a new instance of the <see cref="DueDateCalcInfo"/> struct.
        /// </summary>
        /// <param name="conditionChoice">The condition choice to pull the calculation info from.</param>
        public DueDateCalcInfo(ConditionChoice conditionChoice)
        {
            this.FieldId = conditionChoice.DueDateFieldName;
            this.Offset = conditionChoice.DueDateAddition;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DueDateCalcInfo"/> struct.
        /// </summary>
        /// <param name="task">The task to pull the calculation info from.</param>
        public DueDateCalcInfo(Task task)
        {
            this.FieldId = task.TaskDueDateCalcField;
            this.Offset = task.TaskDueDateCalcDays;
        }
    }
}
