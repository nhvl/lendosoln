﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Admin;
using ConfigSystem.DataAccess;
using LendersOffice.ConfigSystem;
using ConfigSystem.Engine;
using DataAccess;
using LendersOffice.Security;

namespace LendersOffice.ObjLib.Task
{
    public class TaskTriggerTemplateNightlyRunner
    {
        public Guid LoanId { get; private set; }
        public Guid BrokerId { get; private set; }

        private BrokerDB x_brokerDB = null;

        private BrokerDB m_brokerDB
        {
            get
            {
                if (x_brokerDB == null)
                {
                    x_brokerDB = BrokerDB.RetrieveById(BrokerId);
                }
                return x_brokerDB;
            }
        }


        public TaskTriggerTemplateNightlyRunner(Guid sBrokerId, Guid sLId)
        {
            LoanId = sLId;
            BrokerId = sBrokerId;
        }

        public void Process()
        {
            var triggerList = m_brokerDB.ListTaskTriggerTemplateContainsDateParameter();

            var triggerNameList = new List<string>();

            foreach (var o in triggerList)
            {
                triggerNameList.Add(o.Value);
            }

            var fieldList = LendingQBExecutingEngine.GetDependencyFieldsByTrigger(BrokerId, true, triggerNameList.ToArray());

            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(fieldList, LoanId);
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(BrokerId, (SystemUserPrincipal) SystemUserPrincipal.TaskSystemUser));
            foreach (var o in triggerList)
            {
                bool bEval = LendingQBExecutingEngine.EvaluateTrigger(o.Value, valueEvaluator);

                if (bEval)
                {
                    TaskTriggerQueueItem item = new TaskTriggerQueueItem(o.Key, o.Value, BrokerId, LoanId, Guid.Empty, Guid.Empty);
                    TaskTriggerQueue.Send(item);

                    DataAccess.Tools.LogInfo(
                        string.Format("TaskTriggerTemplateNightlyRunner::Send - Sent item - "
                            + "BrokerId:{0}, "
                            + "LoanId:{1}, "
                            + "RecordId:{2}, "
                            + "TemplateId:{3}, "
                            + "TriggerName:{4}",
                            item.BrokerId, item.sLId, item.RecordId.ToString(), item.TaskTemplateId, item.TriggerName));
                }
            }

        }
    }
}
