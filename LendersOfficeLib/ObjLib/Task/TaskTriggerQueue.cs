﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonProjectLib.Common;
using System.Xml.Linq;
using System.Messaging;
using LendersOffice.Constants;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;

namespace LendersOffice.ObjLib.Task
{
    public class TaskTriggerQueueItem
    {

        private TaskTriggerQueueItem(DbDataReader reader)
        {
            this.BrokerId = (Guid)reader["BrokerId"];
            this.sLId = (Guid)reader["LoanId"];
            this.aAppId = (Guid)reader["aAppId"];
            this.TaskTemplateId = (int)reader["TaskTemplateId"];
            this.TriggerName = (string)reader["TaskId"];
            this.RecordId = (Guid)reader["RecordId"];
        }

        public TaskTriggerQueueItem(int taskTemplateId, string triggerName, Guid brokerId, Guid _sLId, Guid _aAppId, Guid recordId)
        {
            this.TaskTemplateId = taskTemplateId;
            this.TriggerName = triggerName ?? string.Empty;
            this.sLId = _sLId;
            this.BrokerId = brokerId;
            this.RecordId = recordId;
            this.aAppId = _aAppId;
        }
        public int TaskTemplateId { get; private set; }
        public string TriggerName { get; private set; }
        public Guid sLId { get; private set; }
        public Guid aAppId { get; private set; }
        public Guid BrokerId { get; private set; }
        public Guid RecordId { get; private set; }

        public XDocument ToXDocument()
        {
            XDocument xdoc = new XDocument(new XElement("item",
                new XAttribute("TaskTemplateId", this.TaskTemplateId.ToString()),
                new XAttribute("TriggerName", this.TriggerName),
                new XAttribute("sLId", this.sLId.ToString()),
                new XAttribute("aAppId", this.aAppId.ToString()),
                new XAttribute("BrokerId", this.BrokerId.ToString()),
                new XAttribute("RecordId", this.RecordId.ToString())));

            return xdoc;


        }
        public static IEnumerable<TaskTriggerQueueItem> GetTaskTriggerAssociationsFor(Guid brokerId, Guid sLid)
        {
            List<TaskTriggerQueueItem> items = new List<TaskTriggerQueueItem>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@loanId", sLid),
                                            new SqlParameter("@brokerId", brokerId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_GetTaskTriggerAssociations", parameters))
            {
                while (reader.Read())
                {
                    items.Add(new TaskTriggerQueueItem(reader));
                }
            }

            return items;
        }
        public static TaskTriggerQueueItem Parse(XDocument xdoc)
        {
            if (null == xdoc)
            {
                return null;
            }
            XElement el = xdoc.Root;
            int taskTemplateId = 0;
            string triggerName = string.Empty;
            Guid sLId = Guid.Empty;
            Guid brokerId = Guid.Empty;
            Guid recordId = Guid.Empty;
            Guid aAppId = Guid.Empty;

            XAttribute attr = null;

            attr = el.Attribute("TaskTemplateId");
            if (attr != null)
            {
                taskTemplateId = int.Parse(attr.Value);
            }

            attr = el.Attribute("TriggerName");
            if (attr != null)
            {
                triggerName = attr.Value;
            }

            attr = el.Attribute("sLId");
            if (attr != null)
            {
                sLId = new Guid(attr.Value);
            }
            attr = el.Attribute("aAppId");
            if (attr != null)
            {
                aAppId = new Guid(attr.Value);
            }
            attr = el.Attribute("BrokerId");
            if (attr != null)
            {
                brokerId = new Guid(attr.Value);
            }
            attr = el.Attribute("RecordId");
            if (attr != null)
            {
                recordId = new Guid(attr.Value);
            }

            return new TaskTriggerQueueItem(taskTemplateId, triggerName, brokerId, sLId, aAppId, recordId);
        }
    }
    public static class TaskTriggerQueue
    {
        public static void Send(TaskTriggerQueueItem item)
        {

            if (null == item)
            {
                return;
            }
            if (string.IsNullOrEmpty(ConstStage.MSMQ_TaskTriggerTemplate))
            {
                return;
            }

            Drivers.Gateways.MessageQueueHelper.SendXML(ConstStage.MSMQ_TaskTriggerTemplate, null, item.ToXDocument());

            DataAccess.Tools.LogInfo(
                string.Format("TaskTriggerQueue::Send - Sent item - "
                    + "BrokerId:{0}, "
                    + "LoanId:{1}, "
                    + "RecordId:{2}, "
                    + "TemplateId:{3}, "
                    + "TriggerName:{4}",
                    item.BrokerId, item.sLId, item.RecordId.ToString(), item.TaskTemplateId, item.TriggerName));

        }

        public static TaskTriggerQueueItem Receive()
        {
            if (string.IsNullOrEmpty(ConstStage.MSMQ_TaskTriggerTemplate))
            {
                return null;
            }

            using (var queue = Drivers.Gateways.MessageQueueHelper.PrepareForXML(ConstStage.MSMQ_TaskTriggerTemplate, false))
            {
                try
                {
                    DateTime arrivalTime;
                    XDocument xdoc = Drivers.Gateways.MessageQueueHelper.ReceiveXML(queue, null, out arrivalTime);
                    if (xdoc == null)
                    {
                        return null; // same behavior as MessageQueueException.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout
                    }

                    return TaskTriggerQueueItem.Parse(xdoc);
                }
                catch (MessageQueueException e)
                {
                    if (e.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
                    {
                        // 12/27/2011 dd - No item in queue.
                        return null;
                    }
                    throw;
                }
            }
        }

    }
}
