﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Admin;
using System.Data;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.Common;
using LendersOffice.ConfigSystem;
using LendersOffice.ConfigSystem.Operations;

namespace LendersOffice.ObjLib.Task
{
    public class TaskPermissionProcessor
    {
        
        private Dictionary<int, E_UserTaskPermissionLevel> m_taskResultCache = new Dictionary<int, E_UserTaskPermissionLevel>();
        private Dictionary<Guid, bool> m_loanResultCache = new Dictionary<Guid,bool>();
        
        private AbstractUserPrincipal m_principal;

        public AbstractUserPrincipal CurrentPrincipal
        {
            get { return m_principal; }
        }

        public TaskPermissionProcessor(AbstractUserPrincipal principal)
        {
            m_principal = principal;
        }

        /// <summary>
        /// Returns the UserTaskPermissionLevel of the current principal for a specified permissionId, loanId, hiddenConditionStatus, and taskOwnerUserId
        /// </summary>
        private E_UserTaskPermissionLevel Resolve(Guid loanId, int permissionId, bool condIsHidden, Guid taskOwnerUserId)
        {
            //Guid loanId = task.LoanId;
            //int permissionId = task.TaskPermissionLevelId;

            if (m_loanResultCache.ContainsKey(loanId) == false)
            {
                m_loanResultCache.Add(loanId, CanAccessLoan(loanId, m_principal));
            }

            // If there is no acces to the loan file, there is no access to the task.
            if (m_loanResultCache[loanId] == false) return E_UserTaskPermissionLevel.None;

            if (m_taskResultCache.ContainsKey(permissionId) == false)
            {
                m_taskResultCache.Add(permissionId, ComputeTaskPermissionAccess(permissionId, m_principal));
            }

            E_UserTaskPermissionLevel taskResult = ComputeTaskBasedPermssionAccess(condIsHidden, taskOwnerUserId, m_principal);

            // These ones count before permissionlevel check
            if (taskResult == E_UserTaskPermissionLevel.None || taskResult == E_UserTaskPermissionLevel.Manage) return taskResult;

            return m_taskResultCache[permissionId];

        }
        /// <summary>
        /// Returns the UserTaskPermissionLevel of the current principal for a specified task
        /// </summary>
        public E_UserTaskPermissionLevel Resolve(ReadOnlyTask task) 
        {
            return Resolve(task.LoanId, task.TaskPermissionLevelId, task.CondIsHidden, task.TaskOwnerUserId);
        }
        /// <summary>
        /// Returns the UserTaskPermissionLevel of the current principal for a specified task
        /// </summary>
        public E_UserTaskPermissionLevel Resolve(Task task)
        {
            return Resolve(task.LoanId, task.TaskPermissionLevelId, task.CondIsHidden, task.TaskOwnerUserIdForPermissionCheck);
            //Guid loanId = task.LoanId;
            //int permissionId = task.TaskPermissionLevelId;

            //if (m_loanResultCache.ContainsKey(loanId) == false)
            //{
            //    m_loanResultCache.Add(loanId, CanAccessLoan(loanId, m_principal));
            //}
            
            //// If there is no acces to the loan file, there is no access to the task.
            //if (m_loanResultCache[loanId] == false) return E_UserTaskPermissionLevel.None;

            //if (m_taskResultCache.ContainsKey(permissionId) == false)
            //{
            //    m_taskResultCache.Add(permissionId, ComputeTaskPermissionAccess(permissionId, m_principal));
            //}

            //E_UserTaskPermissionLevel taskResult = ComputeTaskBasedPermssionAccess(task, m_principal);

            //// These ones count before permissionlevel check
            //if (taskResult == E_UserTaskPermissionLevel.None || taskResult == E_UserTaskPermissionLevel.Manage) return taskResult;

            //return m_taskResultCache[permissionId];
        }


        #region Public statics

        public static bool CanAccessLoan(Guid loanId, AbstractUserPrincipal principal)
        {

            LoanValueEvaluator valueEvaluator;
            valueEvaluator = new LoanValueEvaluator(
                principal.ConnectionInfo
                , principal.BrokerId
                , loanId
                , WorkflowOperations.ReadLoan
                , WorkflowOperations.ReadLoanOrTemplate
                , WorkflowOperations.WriteLoan
                , WorkflowOperations.WriteLoanOrTemplate
                );
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));


            if (principal.Type == "P")
            {
                // 10/03/11 - mf. See OPM 71699 for why we make this distinction.
                // Basically, READ/WRITE_TEMPLATE are not reliable for P user,
                // so we only check READ/WRITE_LOAN for them.
                return ( LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoan, valueEvaluator)
                    || LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteLoan, valueEvaluator ));
            }
            else
            {

                return ( LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, valueEvaluator)
                || LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteLoanOrTemplate, valueEvaluator ));
            }
        }

        public static E_UserTaskPermissionLevel ComputeTaskPermissionAccess(int permissionId, AbstractUserPrincipal principal)
        {
            PermissionLevel taskLevel = PermissionLevel.Retrieve(principal.BrokerId, permissionId);
            Guid[] userGroups = (from ugroup in GroupDB.ListInclusiveGroupForEmployee(principal.BrokerId, principal.EmployeeId)
                                 select ugroup.Id).ToArray();

            return taskLevel.GetUserPermissionLevel(principal.GetRoles().ToArray(), userGroups);

        }

        private static E_UserTaskPermissionLevel ComputeTaskBasedPermssionAccess(bool taskCondIsHidden, Guid taskOwnerUserId, AbstractUserPrincipal principal)
        {
            if (taskCondIsHidden && principal.HasPermission(Permission.CanViewHiddenInformation) == false)
            {
                // This one is hidden to this user
                return E_UserTaskPermissionLevel.None;
            }

            if (taskOwnerUserId == principal.UserId)
            {
                return E_UserTaskPermissionLevel.Manage;
            }

            return E_UserTaskPermissionLevel.WorkOn;
        }
        
        private static E_UserTaskPermissionLevel ComputeTaskBasedPermssionAccess(Task task, AbstractUserPrincipal principal)
        {
            return ComputeTaskBasedPermssionAccess(task.CondIsHidden, task.TaskOwnerUserIdForPermissionCheck, principal);
        }

        /// <summary>
        /// Returns combined access level including task and loan access.
        /// </summary>
        public static E_UserTaskPermissionLevel GetMergedTaskAccessLevel(Task task, AbstractUserPrincipal principal )
        {
            if (principal == null)
            {
                // 4/24/2015 dd - Principal can be null if user is inactive user. Instead of throw exception return NoAccess.
                return E_UserTaskPermissionLevel.None;
            }

            bool canAccessLoan = CanAccessLoan(task.LoanId, principal);

            if (canAccessLoan == false) return E_UserTaskPermissionLevel.None;  // No loan access mean no task access--no exceptions

            return GetTaskAccessLevel(task, principal);
        }

        /// <summary>
        /// This method will only perform permission on task itself. The permission for Loan Access need to occur outside.
        /// 
        /// dd - I added this method so I can call this method directly in my nightly fall-back code. I already cache
        ///      the result of loan access per principal.
        /// </summary>
        /// <param name="task"></param>
        /// <param name="principal"></param>
        /// <returns></returns>
        public static E_UserTaskPermissionLevel GetTaskAccessLevel(Task task, AbstractUserPrincipal principal)
        {
            E_UserTaskPermissionLevel taskResult = ComputeTaskBasedPermssionAccess(task, principal);

            // These ones count before permissionlevel check
            if (taskResult == E_UserTaskPermissionLevel.None || taskResult == E_UserTaskPermissionLevel.Manage) return taskResult;

            return ComputeTaskPermissionAccess(task.TaskPermissionLevelId, principal);

        }

        public static E_UserTaskPermissionLevel GetMergedTaskAccessLevel(Task task, Guid userId)
        {
            return GetMergedTaskAccessLevel(task, PrincipalFactory.RetrievePrincipalForUser(task.BrokerId, userId, "?") as AbstractUserPrincipal);
        }
        #endregion
    }
}