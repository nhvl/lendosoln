﻿namespace LendersOffice.ObjLib.Task
{
    using System.Collections.Generic;

    /// <summary>
    /// Contains a cache of loan data that can be used when batch adding tasks.
    /// It is meant to reduce the amount of loan loading.
    /// </summary>
    public class LoanDataForTasks
    {
        /// <summary>
        /// The loan number.
        /// </summary>
        public readonly string LoanNumber;

        /// <summary>
        /// The primary borrower's full name.
        /// </summary>
        public readonly string PrimaryBorrowerFullName;

        /// <summary>
        /// The cache of due date calculations.
        /// </summary>
        public readonly Dictionary<string, string> LoanDateCache;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanDataForTasks"/> class.
        /// </summary>
        /// <param name="loanNumber">The loan number.</param>
        /// <param name="primaryBorrowerFullName">The primary borrower's full name.</param>
        /// <param name="loanDateCache">The cache of due date calculations.</param>
        public LoanDataForTasks(string loanNumber, string primaryBorrowerFullName, Dictionary<string, string> loanDateCache)
        {
            this.LoanNumber = loanNumber;
            this.PrimaryBorrowerFullName = primaryBorrowerFullName;
            this.LoanDateCache = loanDateCache;
        }
    }
}
