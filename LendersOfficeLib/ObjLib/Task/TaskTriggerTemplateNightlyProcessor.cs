﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Admin;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Constants;
using System.Diagnostics;
using LendersOffice.Security;
using System.Threading;

namespace LendersOffice.ObjLib.Task
{
    public class TaskTriggerTemplateNightlyProcessor : CommonProjectLib.Runnable.IRunnable
    {

        public string Description
        {
            get { return "TaskTriggerTemplateNightlyProcessor"; }
        }


        private const string ComponentName = "TaskTriggerTemplateNightlyProcessor";
        public static void PopulateLoansNeedToBeRun()
        {
            // 1/4/2012 dd - Find out a list of broker id that has auto task trigger contains data parameter.

            List<Guid> brokerIdList = new List<Guid>();

            List<KeyValuePair<Guid, Guid>> loanIdList = new List<KeyValuePair<Guid, Guid>>();
            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "TaskTriggerTemplate_ListDistinctBroker", null))
                {
                    while (reader.Read())
                    {
                        brokerIdList.Add((Guid)reader["BrokerId"]);
                    }
                }
            }


            foreach (var brokerId in brokerIdList)
            {
                BrokerDB brokerDB = BrokerDB.RetrieveById(brokerId);
                if (brokerDB.ListTaskTriggerTemplateContainsDateParameter().Count() > 0)
                {
                    SqlParameter[] parameters = { new SqlParameter("@sBrokerId", brokerId) };
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TaskTriggerTemplate_ListLoanToProcessNightlyByBrokerId", parameters))
                    {
                        while (reader.Read())
                        {
                            loanIdList.Add(new KeyValuePair<Guid, Guid>((Guid)reader["sLId"], (Guid) reader["sBrokerId"]));
                        }
                    }
                }
            }

            Tools.LogInfo(ComponentName + ": There are " + loanIdList.Count + " loans need to run through nightly process.");
            foreach (var o in loanIdList)
            {
                SqlParameter[] parameters = { 
                                                new SqlParameter("@sLId", o.Key),
                                                new SqlParameter("sBrokerId", o.Value)
                                            };
                StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "TaskTriggerNightly_InsertLoan", 2, parameters);
            }

        }
        #region IRunnable Members
        public void Run()
        {
            Stopwatch sw = Stopwatch.StartNew();
            Thread.CurrentPrincipal = SystemUserPrincipal.TaskSystemUser;
            int successfulCount = 0;
            int errorCount = 0;
            while (true)
            {
                Guid sLId = Guid.Empty;
                Guid sBrokerId = Guid.Empty;
                long id = 0;
                try
                {

                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOTransient, "TaskTriggerNightly_GetTopLoanId"))
                    {
                        if (reader.Read())
                        {
                            id = (long)reader["Id"];
                            sLId = (Guid)reader["sLId"];
                            sBrokerId = (Guid)reader["sBrokerId"];
                        }
                        else
                        {
                            break; // Terminate loop.
                        }
                    }
                    if (sLId != Guid.Empty)
                    {
                        var runner = new TaskTriggerTemplateNightlyRunner(sBrokerId, sLId);
                        runner.Process();
                        successfulCount++;
                    }
                }
                catch (CBaseException exc)
                {
                    errorCount++;
                    Tools.LogError(ComponentName + ": Unable to process sLId=" + sLId + ". " + exc.DeveloperMessage + Environment.NewLine + exc.StackTrace);
                }
                catch (SqlException exc)
                {
                    errorCount++;
                    Tools.LogError(ComponentName + ": Unable to process sLId=" + sLId + ". " + exc.Message + Environment.NewLine + exc.StackTrace);
                }
                finally
                {
                    SqlParameter[] parameters = { new SqlParameter("@Id", id) };
                    StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "TaskTriggerNightly_DeleteById", 2, parameters);

                }
            }
            sw.Stop();
            Tools.LogInfo(ComponentName + ": Processed " + successfulCount + " success loans. " + errorCount + " error loans. Executes in " + sw.ElapsedMilliseconds + "ms.");
        }

        #endregion
    }
}
