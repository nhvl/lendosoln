﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Admin;
using System.Xml.Linq;

namespace LendersOffice.ObjLib.Task
{
    public class ConditionCategory : IComparable<ConditionCategory>
    {
        private const string SP_FETCH = "TASK_Categories_Fetch";
        private const string SP_FETCH_MULTIPLE = "TASK_Categories_FetchMultiple";
        private const string SP_SAVE = "TASK_Categories_Save";
        private const string SP_DELETE = "TASK_Cateogries_Delete";

        public int Id { get; private set; }
        public string Category { get; set; }
        public int DefaultTaskPermissionLevelId { get; set; }
        public bool IsDisplayAtTop { get; set; }
        public Guid BrokerId { get; private set; }

        public ConditionCategory(Guid brokerId)
        {
            BrokerId = brokerId;
            Id = -1;    //save handles this as isnew so dont change.
        }

        internal ConditionCategory(DbDataReader reader)
        {
            BrokerId = (Guid)reader["BrokerId"];
            Category = (string)reader["Category"];
            DefaultTaskPermissionLevelId = (int)reader["DefaultTaskPermissionLevelId"];
            IsDisplayAtTop = (bool)reader["IsDisplayAtTop"];
            Id = (int)reader["ConditionCategoryId"];
            Category = Category.ToUpper();
        }

        public void Save()
        {
            List<SqlParameter> storeParameters = new List<SqlParameter>()
            {
                new SqlParameter("@Category", Category.ToUpper()),
                new SqlParameter("@DefaultTaskPermissionLevelId", DefaultTaskPermissionLevelId ),
                new SqlParameter("@IsDisplayAtTop", IsDisplayAtTop),
                new SqlParameter("@BrokerId", BrokerId),
                new SqlParameter("@Id", Id)
            };

            Id = (int)StoredProcedureHelper.ExecuteScalar(this.BrokerId, SP_SAVE, storeParameters);
        }

        private static IEnumerable<ConditionCategory> GetCategoriesImpl(Guid brokerId, int? id)
        {
            LinkedList<ConditionCategory> categories = new LinkedList<ConditionCategory>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@Id", id)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId,SP_FETCH, parameters))
            {
                while (reader.Read())
                {
                    categories.AddLast(new ConditionCategory(reader));
                }
            }
            return categories.OrderByDescending(p => p.IsDisplayAtTop).ThenBy(p => p.Category);
        }

        public static IEnumerable<ConditionCategory> GetCategories(Guid brokerId)
        {
            return GetCategoriesImpl(brokerId, null);
        }

        public static IEnumerable<ConditionCategory> GetCategoriesUnordered(Guid brokerId, IEnumerable<int> ids)
        {
            LinkedList<ConditionCategory> categories = new LinkedList<ConditionCategory>();

            if (!ids.Any())
            {
                return categories;
            }

            var idXml = new XElement("root", ids.Select(id => new XElement("id", id)))
                .ToString(SaveOptions.DisableFormatting);
            SqlParameter[] parameters = 
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@IdXml", idXml)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, SP_FETCH_MULTIPLE, parameters))
            {
                while (reader.Read())
                {
                    categories.AddLast(new ConditionCategory(reader));
                }
            }

            return categories;
        }

        public static ConditionCategory GetCategory(Guid brokerId, int id)
        {
            return GetCategoriesImpl(brokerId, id).FirstOrDefault();
        }


        public static void Delete(Guid brokerId, int id)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@Id", id)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, SP_DELETE, 0, parameters);
        }

        #region IComparable<ConditionCategory> Members

        public int CompareTo(ConditionCategory other)
        {
            return this.Category.CompareTo(other.Category);
        }

        #endregion
    }


    public sealed class ConditionCategoriesWithPermissionInfo
    {

        private ConditionCategory[] m_allCategories;   

        private List<ConditionCategory> m_Manage;
        private List<ConditionCategory> m_Close;
        private List<ConditionCategory> m_WorkOn;
        private List<ConditionCategory> m_NoPermission; 

        private E_RoleT[] UserRoles { get; set; }
        private Guid[] UserGroupId { get; set; }
        private PermissionLevel[] BrokerPermissionLevels { get; set; }



        public ICollection<ConditionCategory> ManageCategories
        {
            get
            {
                return m_Manage.AsReadOnly();
            }
        }

        public ICollection<ConditionCategory> CloseCategories
        {
            get
            {
                return m_Close.AsReadOnly();
            }
        }

        public ICollection<ConditionCategory> WorkOnCategories
        {
            get
            {
                return m_WorkOn.AsReadOnly();
            }
        }

        public ICollection<ConditionCategory> NoPermissionCategories
        {
            get
            {
                return m_NoPermission.AsReadOnly();
            }
        }

        public ICollection<ConditionCategory> AllCategories
        {
            get
            {
                return m_allCategories;
            }
        }


        public ConditionCategoriesWithPermissionInfo(AbstractUserPrincipal principal)
        {
            UserRoles = principal.GetRoles().ToArray();

            UserGroupId = (from employeegroup in GroupDB.ListInclusiveGroupForEmployee(principal.BrokerId, principal.EmployeeId)
                         select employeegroup.Id).ToArray();

            BrokerPermissionLevels = PermissionLevel.RetrieveAll(principal.BrokerId).Where(level => level.IsAppliesToConditions).ToArray();
            m_allCategories = ConditionCategory.GetCategories(principal.BrokerId).ToArray();

            m_Manage = new List<ConditionCategory>(m_allCategories.Length);
            m_Close = new List<ConditionCategory>(m_allCategories.Length);
            m_WorkOn = new List<ConditionCategory>(m_allCategories.Length);
            m_NoPermission = new List<ConditionCategory>(m_allCategories.Length);

            Process();
        }

        private void Process()
        {
            foreach (ConditionCategory category in m_allCategories)
            {
                PermissionLevel level = BrokerPermissionLevels.Where(_level => _level.Id == category.DefaultTaskPermissionLevelId).FirstOrDefault();
                if (level == null)
                {
                    Tools.LogError("Unlikely error, category does not have a permission level. Skipping. CID:" + category.Id + " DID:" + category.DefaultTaskPermissionLevelId);
                    continue;
                }

                E_UserTaskPermissionLevel accessLevel = level.GetUserPermissionLevel(UserRoles, UserGroupId);
                switch (accessLevel)
                {
                    case E_UserTaskPermissionLevel.WorkOn:
                        m_WorkOn.Add(category);
                        break;
                    case E_UserTaskPermissionLevel.Manage:
                        m_Manage.Add(category);
                        break;
                    case E_UserTaskPermissionLevel.Close:
                        m_Close.Add(category);
                        break;
                    case E_UserTaskPermissionLevel.None:
                        m_NoPermission.Add(category);
                        break;
                    default:
                        throw new UnhandledEnumException(accessLevel);
                }
            }
        }
    }
}
