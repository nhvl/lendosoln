﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using LendersOffice.Admin;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendersOffice.ObjLib.Task
{
    /// <summary>
    /// Delete this after settings updated to use <see cref="CancellableTaskTriggerTemplateProcessor"/>.
    /// </summary>
    public class TaskTriggerTemplateProcessor : CommonProjectLib.Runnable.IRunnable
    {
        public TaskTriggerTemplateProcessor()
        {
        }

        #region IRunnable Members

        public string Description
        {
            get { return "Executes a trigger based on workflow."; }
        }
        public void Run()
        {
            using (var workerTiming = new WorkerExecutionTiming("TaskTriggerTemplateProcessor"))
            {
                while (true)
                {
                    TaskTriggerQueueItem item = TaskTriggerQueue.Receive();

                    if (item == null)
                    {
                        return; // 12/27/2011 dd - Exit continuous when there are no more task to process.
                    }

                    using (var itemTiming = workerTiming.RecordItem(item.TriggerName + " - sLId=" + item.sLId, DateTime.UtcNow))
                    {
                        try
                        {
                            HandleTaskTrigger(item);
                        }
                        catch (Exception e)
                        {
                            Tools.LogError(e);
                            itemTiming.RecordError(e);
                            throw;            // Ideally I think we might want to do away with this at some point.
                        }
                    }
                }
            }
        }

        #endregion

        public void HandleTaskTrigger(TaskTriggerQueueItem item)
        {
            Tools.LogInfo(string.Format("Handling task trigger - BrokerId:{0} Loan ID:{1}, AppId:{2}, TaskTemplateId:{3}, TriggerName:{4}, RecordId:{5} ",
                item.BrokerId, item.sLId, item.aAppId, item.TaskTemplateId, item.TriggerName, item.RecordId));
            
            //Set the principal used to create the tasks
            System.Threading.Thread.CurrentPrincipal = LendersOffice.Security.SystemUserPrincipal.TaskSystemUser;
            
            Task task;
            bool bSetTaskRoles = false;
            LoanAssignmentContactTable assignments = null;

            TaskTriggerTemplate template = TaskTriggerTemplate.GetTaskTriggerTemplate(item.BrokerId, item.TaskTemplateId);            
            
            bool bIsCollectionBasedTrigger = item.TriggerName.StartsWith("Liability_", StringComparison.OrdinalIgnoreCase) || item.TriggerName.StartsWith("Asset_", StringComparison.OrdinalIgnoreCase) || item.TriggerName.StartsWith("REO_", StringComparison.OrdinalIgnoreCase);
            
            string sTaskId = TaskTriggerTemplate.GetTaskIdCreatedFromTriggerTemplate(item.BrokerId, item.sLId, item.TaskTemplateId, item.RecordId);
            bool bTaskIsLinked = !string.IsNullOrEmpty(sTaskId); // The task is linked to a template
           
            //if the loan file doesn't have task with this templateid/recordid, create the task            
            if (!bTaskIsLinked)
            {
                task = TaskTriggerTemplate.GetTaskFromTaskTemplate(item.BrokerId, item.TaskTemplateId, item.sLId);
                bSetTaskRoles = true;
            }
            else
            {
                //A task exists for this template id
                if (template.Frequency == E_AutoTaskFrequency.SingleUse && !bIsCollectionBasedTrigger) 
                    return; //do nothing
                
                //Use existing task. Recurring or collection based triggers
                task = new Task();
                task.EnforcePermissions = false;
                task.Retrieve(sTaskId, item.BrokerId);

                if (task.TaskStatus == E_TaskStatus.Closed || task.TaskStatus == E_TaskStatus.Resolved)
                {
                    //update the existing task
                    task.TaskStatus = E_TaskStatus.Active;
                    assignments = new LoanAssignmentContactTable(item.BrokerId, item.sLId);

                    //update the roles according to the templates. Update the role assignments only if there is an employee assigned for the corresponding role in the loan file
                    if (template.ToBeAssignedRoleId != null && template.ToBeAssignedRoleId != Guid.Empty)
                    {
                        foreach (var assignedEmployee in assignments.Items)
                        {
                            if (assignedEmployee.RoleId == template.ToBeAssignedRoleId)
                            {
                                task.TaskToBeAssignedRoleId = template.ToBeAssignedRoleId;
                                break;
                            }
                        }                        
                    }
                    if (template.ToBeOwnedByRoleId != null && template.ToBeOwnedByRoleId != Guid.Empty)
                    {
                        foreach (var assignedEmployee in assignments.Items)
                        {
                            if (assignedEmployee.RoleId == template.ToBeOwnedByRoleId)
                            {
                                task.TaskToBeOwnerRoleId = template.ToBeOwnedByRoleId;
                                break;
                            }
                        }                        
                    }
                    bSetTaskRoles = true;                    
                }
                //Add to audit history                
                task.Comments = "The task system has detected a data change that may require this task to be redone.  Please verify.";
            }
            
            if (bSetTaskRoles)
            {
                //Reassign task roles
                Dictionary<Guid, Guid> userIdCache = new Dictionary<Guid, Guid>();
                if (assignments == null)
                {
                    assignments = new LoanAssignmentContactTable(item.BrokerId, item.sLId);
                }
                Task.SetTaskRoles(item.BrokerId, task, assignments, userIdCache);

                //handle single user assignment, ownership
                if (template.AssignedUserId != null && template.AssignedUserId != Guid.Empty)
                    task.TaskAssignedUserId = template.AssignedUserId;

                if (template.OwnerUserId != null && template.OwnerUserId != Guid.Empty)
                    task.TaskOwnerUserId = template.OwnerUserId;
            }

            if (bIsCollectionBasedTrigger)
            {
                HandleCollectionBasedTriggerTask(item, task);
            }

            task.EnforcePermissions = false;
            task.Save(false);
            if (!bTaskIsLinked)
            {
                TaskTriggerTemplate.LinkTaskToTaskTriggerTemplate(task, item);
            }

            // bTaskIsLinked = true;
            if (task.TaskIsCondition)
            {
                TaskUtilities.BumpLoanConditionSummaryVersionAndEnqueue(task.BrokerId, task.LoanId);
            }

            // Create trigger associations
            if (!string.IsNullOrEmpty(template.AutoResolveTriggerName))
            {
                TaskTriggerAssociation.CreateNewAssociation(task, TaskTriggerType.AutoResolve, template.AutoResolveTriggerName);
            }

            if (!string.IsNullOrEmpty(template.AutoCloseTriggerName))
            {
                TaskTriggerAssociation.CreateNewAssociation(task, TaskTriggerType.AutoClose, template.AutoCloseTriggerName);
            }

            // OPM 471933 - It's possible auto-resolve/close conditions may have already been met. So run automation trigger evaluation to check.
            task.EvaluateTaskAutomationTriggers();
        }

        public void HandleCollectionBasedTriggerTask(TaskTriggerQueueItem item, Task task)
        {
            if(item.RecordId == Guid.Empty)
                return;
            
            if (item.TriggerName.StartsWith("Liability_", StringComparison.OrdinalIgnoreCase))
            {
                CPageData dataLoan = new CLiaData(item.sLId, false);
                dataLoan.InitLoad();
                Guid appId = item.aAppId;
                if (appId == Guid.Empty)
                {
                    appId = dataLoan.GetAppData(0).aAppId;
                }

                CAppData dataApp = dataLoan.GetAppData(appId);

                ILiaCollection liabilities = dataApp.aLiaCollection;
                ILiabilityRegular liability = liabilities.GetRegRecordOf(item.RecordId);
                if (task.IsNew)
                {

                    task.Comments = string.Format(@"
Liability info: 
Debt Type: {0} 
Company Name: {1} 
Balance: {2} 
Account Holder: {3} 
Account Number: {4} 
", liability.DebtT_rep, liability.ComNm, liability.Bal_rep, liability.AccNm, liability.AccNum.Value);

                    task.TaskSubject += string.Format(" {0}: Account ending in {1}", liability.ComNm, GetAccNumberDisplay(liability.AccNum.Value));
                }

            }

            else if (item.TriggerName.StartsWith("Asset_", StringComparison.OrdinalIgnoreCase))
            {
                CPageData dataLoan = new CAssetAllAppsData (item.sLId);
                dataLoan.InitLoad();
                Guid appId = item.aAppId;
                if (appId == Guid.Empty)
                {
                    appId = dataLoan.GetAppData(0).aAppId;
                }

                CAppData dataApp = dataLoan.GetAppData(appId);

                IAssetCollection assets = dataApp.aAssetCollection;
                var asset = assets.GetRegRecordOf(item.RecordId);

                if (task.IsNew)
                {

                    task.Comments = string.Format(@"
Asset info: 
Asset Type: {0} 
Description: {1} 
Market Value: {2} 
Account Holder: {3} 
Account Number: {4} 
", asset.AssetT_rep, asset.Desc, asset.Val, asset.AccNm, asset.AccNum.Value);

                    task.TaskSubject += string.Format(" {0}: Account ending in {1}", asset.ComNm, GetAccNumberDisplay(asset.AccNum.Value));
                }
            }
            else
            {
                CPageData dataLoan = new CAssetAllAppsData(item.sLId);
                dataLoan.InitLoad();
                Guid appId = item.aAppId;
                if (appId == Guid.Empty)
                {
                    appId = dataLoan.GetAppData(0).aAppId;
                }
                CAppData dataApp = dataLoan.GetAppData(appId);
                var reos = dataApp.aReCollection;
                var reo = reos.GetRegRecordOf(item.RecordId);
                if (task.IsNew)
                {
                    string fullAddress = string.Format("{0}, {1}, {2}, {3}", reo.Addr, reo.City, reo.State, reo.Zip);

                    task.Comments = string.Format(@"
REO info:
Address: {0}
Type: {1}
Market Value: {2}
", fullAddress, reo.Type, reo.Val);

                    task.TaskSubject += string.Format(" Address: {0}", fullAddress);
                }
            }
        }

        private string GetAccNumberDisplay(string accNum)
        {
            if (string.IsNullOrEmpty(accNum) || accNum.Length <= 4)
                return accNum;

            //return the last 4 characters
            return accNum.Substring(accNum.Length - 4, 4);
        }
    }
}
