﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Security;
using LendersOffice.Admin;
using EDocs;
using System.Xml.Linq;

namespace LendersOffice.ObjLib.Task
{
    public enum E_ConditionType
    {
        AppraisalProperty = 0,
        Assets = 1, 
        ComplianceDisclosure = 2,
        Credit = 3,
        EmploymentIncome = 4,
        Insurance = 5, 
        Misc = 6, 
        Title = 7,
        Application = 8
    }


    public static class ConditionChoiceExtensions
    {
        public static string FriendlyValue(this E_ConditionType conditionType)
        {
            switch (conditionType)
            {
                case E_ConditionType.AppraisalProperty:
                    return "APPRAISAL/PROPERTY";
                case E_ConditionType.Assets:
                    return "ASSETS";
                case E_ConditionType.ComplianceDisclosure:
                    return "COMPLIANCE/DISCLOSURE";
                case E_ConditionType.Credit:
                    return "CREDIT";
                case E_ConditionType.EmploymentIncome:
                    return "EMPLOYMENT/INCOME";
                case E_ConditionType.Insurance:
                    return "INSURANCE";
                case E_ConditionType.Misc:
                    return "MISC";
                case E_ConditionType.Title:
                    return "ESCROW/TITLE";
                case E_ConditionType.Application:
                    return "APPLICATION";
                default:
                    throw new UnhandledEnumException(conditionType);
            }
        }
        public static string FriendlyValue(this E_ConditionChoice_sLT loanType)
        {
            switch (loanType)
            {
                case E_ConditionChoice_sLT.Conventional:
                    return "Conventional";
                case E_ConditionChoice_sLT.FHA:
                    return "FHA";
                case E_ConditionChoice_sLT.VA:
                    return "VA";
                case E_ConditionChoice_sLT.UsdaRural:
                    return "USDA Rural";
                case E_ConditionChoice_sLT.Other:
                    return "Other";
                case E_ConditionChoice_sLT.Any:
                    return "Any";
                default:
                    throw new UnhandledEnumException(loanType);
            }
        }
    }

    public sealed class ConditionChoice
    {

        private const  string SP_FETCH = "TASK_ConditionChoice_Fetch";
        private const  string SP_FETCH_MULTIPLE = "TASK_ConditionChoice_FetchMultiple";
        private const string SP_SAVE = "TASK_ConditionChoice_Save";
        private const string SP_SAVE_ORDER = "TASK_ConditionChoice_SaveOrder";
        private const string SP_DELETE = "TASK_ConditionChoice_Delete";

        private Nullable<int> m_conditionChoiceId;

        //OPM 71055: It's nice to be able to create one with a specified category before saving it.
        public ConditionChoice(Guid brokerId, ConditionCategory cat) : this(brokerId)
        {
            this.Category = cat;
        }

        public ConditionChoice(Guid brokerId)
        {

            this.BrokerId = brokerId;
            this.m_conditionChoiceId = new int?();
        }

        private ConditionChoice(DbDataReader reader)
        {
            this.m_conditionChoiceId = (int)reader["ConditionChoiceId"];
            this.BrokerId = (Guid)reader["BrokerId"];
            this.ConditionType = (E_ConditionType)reader["ConditionType"];
            this.sLT = (E_ConditionChoice_sLT)reader["sLT"];
            this.ConditionSubject = (string)reader["ConditionSubject"];
            this.ToBeAssignedRoleId = (Guid)reader["ToBeAssignedRole"];
            this.ToBeOwnedByRoleId = (Guid)reader["ToBeOwnedByRole"];
            this.DueDateFieldName = (string)reader["DueDateFieldName"];
            this.DueDateAddition = (int)reader["DueDateAddition"];
            this.Rank = (int)reader["Rank"];
            this.ToBeAssignedRole = (string)reader["ToBeAssignedRoleName"];
            this.ToBeOwnedByRole = (string)reader["ToBeOwnedByRoleName"];
            this.RequiredDocTypeId = reader.AsNullableInt("RequiredDocTypeId");
            this.IsHidden = (bool)reader["IsHidden"];

            Category = new ConditionCategory(reader);
        }
        ConditionCategory _category;
        public ConditionCategory Category
        {
            get { return _category; }
            private set 
            {
                _category = value;
                CategoryId = _category.Id; 
            }
        }

        public int CategoryId
        {
            get;
            set;
        }


        public int ConditionChoiceId
        {
            get
            {
                if (m_conditionChoiceId.HasValue)
                {
                    return m_conditionChoiceId.Value;
                }
                throw CBaseException.GenericException("Cannot get id of a new condition choice object.");
            }
        }
        public Guid BrokerId
        {
            get;
            private set;
        }
        public string ConditionSubject { get; set; }
        public E_ConditionType ConditionType
        {
            get;
            set;
        }
        public string ConditionType_rep
        {
            get
            {
                return ConditionType.FriendlyValue();
            }
        }

        public string DueDateFieldName { get; set; }
        public int DueDateAddition { get; set; }

        public int Rank { get; set; }
        public E_ConditionChoice_sLT sLT { get; set; }
        public string sLT_rep
        {
            get
            {
                return sLT.FriendlyValue().ToUpper();
            }
        }
        public string FriendlyDueDateDescription
        {
            get
            {
                return Tools.GetTaskDueDateDescription(DueDateAddition, DueDateFieldName, true);
            }
        }

        public Guid ToBeAssignedRoleId { get; set; }
        public Guid ToBeOwnedByRoleId { get; set; }

        public string ToBeAssignedRole { get; private set;  }
        public string ToBeOwnedByRole { get; private set; }

        public int? RequiredDocTypeId { get; set; }

        public bool IsHidden { get; set; }

        private List<DocType> m_listOfDoctypes = null;

        public string RequiredDocType_rep
        {
            get { return GetDocTypeDesc(RequiredDocTypeId); }
        }

        private string GetDocTypeDesc(int? id)
        {
            if (!id.HasValue)
            {
                return "None";
            }

            if (m_listOfDoctypes == null)
            {
                m_listOfDoctypes = EDocumentDocType.GetDocTypesByBroker(BrokerId, E_EnforceFolderPermissions.True).ToList();
            }

            var selectedDocType = m_listOfDoctypes.Find((DocType d) => d.DocTypeId == id.Value.ToString());

            if (selectedDocType == null)
            {
                return null; //doc type not found.
            }

            return selectedDocType.FolderAndDocTypeName;
        }

        private SqlParameter[] GetSaveParameters()
        {
            //OPM 71055: Some logic to make that new constructor work
            if (CategoryId == -1)
            {
                CategoryId = Category.Id;
            }
            SqlParameter[] sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", BrokerId),
                new SqlParameter("@Id", m_conditionChoiceId),
                new SqlParameter("@ConditionCategoryId", CategoryId),
                new SqlParameter("@ConditionType", ConditionType),
                new SqlParameter("@sLT", sLT),
                new SqlParameter("@ConditionSubject", ConditionSubject),
                new SqlParameter("@ToBeAssignedRole", ToBeAssignedRoleId),
                new SqlParameter("@ToBeOwnedByRole", ToBeOwnedByRoleId),
                new SqlParameter("@DueDateFieldName", DueDateFieldName),
                new SqlParameter("@DueDateAddition", DueDateAddition),
                new SqlParameter("@Rank", Rank),
                new SqlParameter("@RequiredDocTypeId", RequiredDocTypeId),
                new SqlParameter("@IsHidden", IsHidden)
            };

            return sqlParameters;
        }

        public void Save(Guid brokerId)
        {
            //This is clearly a contradiction but I'm afraid to "fix" it in case that breaks something unrelated.
            if( BrokerId != brokerId )
            {
                throw CBaseException.GenericException("Broker id from saving user doesn't match the condition choice broker id.");
            }
            m_conditionChoiceId = (int)StoredProcedureHelper.ExecuteScalar(brokerId, SP_SAVE, GetSaveParameters());
        }

        public void Save(Guid brokerId, CStoredProcedureExec exec)
        {
            if( BrokerId != brokerId )
            {
                throw CBaseException.GenericException("Broker id from saving user doesn't match the condition choice broker id.");
            }
            exec.ExecuteNonQuery(SP_SAVE, GetSaveParameters());
        }

        private static IEnumerable<ConditionChoice> RetrieveImpl(Guid brokerId,Nullable<int> id)
        {
            LinkedList<ConditionChoice> conditionChoices = new LinkedList<ConditionChoice>();
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId), 
                                            new SqlParameter("@Id", id)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, SP_FETCH, parameters))
            {
                while (reader.Read())
                {
                    conditionChoices.AddLast(new ConditionChoice(reader));
                }
            }
            return conditionChoices;
        }

        public static IEnumerable<ConditionChoice> Retrieve(Guid brokerId, IEnumerable<int> ids)
        {
            return RetrieveImpl(brokerId, ids);
        }
        private static IEnumerable<ConditionChoice> RetrieveImpl(Guid brokerId, IEnumerable<int> ids)
        {
            LinkedList<ConditionChoice> conditionChoices = new LinkedList<ConditionChoice>();

            if (!ids.Any())
            {
                return conditionChoices;
            }

            var idXml = new XElement("root", ids.Select(id => new XElement("id", id)))
                .ToString(SaveOptions.DisableFormatting);
            SqlParameter[] parameters = 
            {
                new SqlParameter("@BrokerId", brokerId), 
                new SqlParameter("@IdXml", idXml)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, SP_FETCH_MULTIPLE, parameters))
            {
                while (reader.Read())
                {
                    conditionChoices.AddLast(new ConditionChoice(reader));
                }
            }

            return conditionChoices;
        }

        public static ConditionChoice Retrieve(Guid brokerId, int id)
        {
            return RetrieveImpl(brokerId, id).FirstOrDefault();
        }

        public static IEnumerable<ConditionChoice> RetrieveForUser(AbstractUserPrincipal principal)
        {
            Guid[] groupIds = (from x in GroupDB.ListInclusiveGroupForEmployee(principal.BrokerId, principal.EmployeeId)
                               select x.Id).ToArray();
            Dictionary<int,PermissionLevel> levels = PermissionLevel.RetrieveAll(principal.BrokerId).ToDictionary(p=>p.Id);
            foreach (ConditionChoice choice in Retrieve(principal.BrokerId))
            {
                PermissionLevel l = levels[choice.Category.DefaultTaskPermissionLevelId];
                if (l.GetUserPermissionLevel(principal.GetRoles().ToArray(), groupIds) != E_UserTaskPermissionLevel.None)
                {
                    yield return choice;
                }
            }
        }

        public static IEnumerable<ConditionChoice> Retrieve(Guid brokerId)
        {
            return RetrieveImpl(brokerId, new Nullable<int>());
        }

        public static void SaveSortAndDelete(Guid brokerId, int[] idsToDelete, int[] sortOrder)
        {
            using (CStoredProcedureExec exec = new CStoredProcedureExec(brokerId))
            {

                exec.BeginTransactionForWrite();
                try
                {
                    foreach (int id in idsToDelete)
                    {
                        exec.ExecuteNonQuery(SP_DELETE,
                            new SqlParameter("@Id", id),
                            new SqlParameter("@BrokerId", brokerId));
                    }

                    for (var rank = 0; rank < sortOrder.Length; rank++)
                    {
                        exec.ExecuteNonQuery(SP_SAVE_ORDER,
                            new SqlParameter("@Id", sortOrder[rank]),
                            new SqlParameter("@BrokerId", brokerId),
                            new SqlParameter("@Rank", rank));
                    }
                    exec.CommitTransaction();
                }
                catch (Exception)
                {
                    exec.RollbackTransaction();
                    throw;
                }

            }
        }

    }
}
