﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;

namespace LendersOffice.ObjLib.Task
{
    public static class TaskSubscription
    {
        public static bool IsSubscribed(Guid brokerId, string taskId, Guid userId)
        {
            SqlParameter[] parameters = {
                                             new SqlParameter("@TaskId", taskId ),
                                            new SqlParameter("@UserId", userId)
                                        };

            using (DbDataReader sr = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_Subscription_GetSubscribed", parameters))
            {
                if (sr.Read())
                {
                    return sr["TaskId"].ToString() == taskId && (Guid)sr["UserId"] == userId;
                }
            }
            return false;
        }

        public static void Subscribe(Guid brokerId, string taskId, Guid userId)
        {
            SqlParameter[] parameters = {
                                                new SqlParameter("@TaskId", taskId),
                                                new SqlParameter("@UserId", userId)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "TASK_Subscription_Subscribe", 2, parameters);
        }

        public static void Unsubscribe(Guid brokerId, string taskId, Guid userId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@TaskId", taskId),
                                            new SqlParameter("@UserId", userId)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "TASK_Subscription_Unsubscribe", 2, parameters);
        }

        public static IEnumerable<Guid> GetSubscribedUserIds(Guid brokerId, string taskId)
        {
            List<Guid> users = new List<Guid>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@TaskId", taskId )
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_Subscription_GetSubscribed", parameters))
            {
                while (reader.Read())
                {
                    users.Add((Guid)reader["UserId"]);
                }
            }
            return users;
        }

    }
}
