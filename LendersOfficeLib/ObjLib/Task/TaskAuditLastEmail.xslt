﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="text"/>
  <xsl:template match="/TaskHistory">
       
    <xsl:apply-templates select="Save/Email"/>
  </xsl:template>


  <xsl:template match="Save/Email">


-----Original Message-----
From:<xsl:value-of select="@From"/>
Date:<xsl:value-of select="@Date"/>
To:<xsl:value-of select="@To"/>
<xsl:apply-templates select="@Cc"></xsl:apply-templates>
Subject:<xsl:value-of select="@Subject"/>

<xsl:text>
</xsl:text>
<xsl:value-of select="Body" disable-output-escaping="yes"/>

  </xsl:template>


  
  

  
  
  
</xsl:stylesheet>
