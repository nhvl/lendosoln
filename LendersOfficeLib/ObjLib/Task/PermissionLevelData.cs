﻿namespace LendersOffice.ObjLib.Task
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Contains the permission level data used during task creation.
    /// Intended to reduce the database load when batch creating tasks.
    /// </summary>
    public class PermissionLevelData
    {
        /// <summary>
        /// The broker id.
        /// </summary>
        public readonly Guid BrokerId;

        /// <summary>
        /// A map from level id to name.
        /// </summary>
        public readonly Dictionary<int, string> NameById;

        /// <summary>
        /// A map from level id to description.
        /// </summary>
        public readonly Dictionary<int, string> DescriptionById;

        /// <summary>
        /// A map from id to a value indicating whether the permission level applies to conditions.
        /// </summary>
        public readonly Dictionary<int, bool> AppliesToConditionsById;

        /// <summary>
        /// Initializes a new instance of the <see cref="PermissionLevelData"/> class.
        /// </summary>
        /// <param name="brokerId">The lender's broker id.</param>
        private PermissionLevelData(Guid brokerId)
        {
            this.BrokerId = brokerId;
            this.NameById = new Dictionary<int, string>();
            this.DescriptionById = new Dictionary<int, string>();
            this.AppliesToConditionsById = new Dictionary<int, bool>();
        }

        /// <summary>
        /// Retrieves the permission level data for the given lender.
        /// </summary>
        /// <param name="brokerId">The lender's broker id.</param>
        /// <returns>The permission level data for the lender.</returns>
        public static PermissionLevelData Retrieve(Guid brokerId)
        {
            var permissionLevels = new PermissionLevelData(brokerId);

            foreach (var level in PermissionLevel.RetrieveAll(brokerId))
            {
                permissionLevels.NameById.Add(level.Id, level.Name);
                permissionLevels.DescriptionById.Add(level.Id, level.Description);
                permissionLevels.AppliesToConditionsById.Add(level.Id, level.IsAppliesToConditions);
            }

            return permissionLevels;
        }
    }
}
