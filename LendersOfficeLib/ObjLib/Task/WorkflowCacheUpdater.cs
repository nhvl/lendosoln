﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Constants;
using DataAccess;
using LendersOffice.ObjLib.DatabaseMessageQueue;
using LendersOffice.Common;

namespace LendersOffice.ObjLib.Task
{
    public class WorkflowCacheUpdater : CommonProjectLib.Runnable.IRunnable
    {
        public string Description
        {
            get
            {
                return "Updates loan file task due dates and Updates the cached condition summary of a loan file. Along with task stats in cache.";
            }
        }



        private void ProcessMessages(DBMessage[] messages)
        {
            Guid sLId;
            Guid brokerId = Guid.Empty;
            bool updateConditionSummary = false;
            bool updateDueDates = false;
            DBMessage message = messages[0];
            string[] parts = message.Subject1.Split('_');
            
            sLId = new Guid(parts[0]);

            //Old format
            if (parts.Length == 1)
            {
                if (message.Data == "ConditionSummary")
                {
                    updateConditionSummary = true;
                }
                else if (message.Data == "UpdateDueDates")
                {
                    updateDueDates = true;
                    brokerId = new Guid(message.Subject2);
                }
            }
            else
            {
                brokerId = new Guid(message.Data);
                updateConditionSummary = parts[1] == "ConditionSummary";
                updateDueDates = parts[1] == "UpdateDueDates";
            }

            try
            {
                if (updateConditionSummary)
                {
                    TaskUtilities.UpdateLoanConditionSummary(sLId);
                }
                if (updateDueDates)
                {
                    if (brokerId == Guid.Empty)
                    {
                        Tools.LogBug("WorkflowCacheUpdater::ProcessMessages. BrokerId is null for sLId=" + sLId);
                    }
                    else
                    {
                        TaskUtilities.UpdateTasksDueDateByLoan(brokerId, sLId);
                    }
                }
            }
            catch (Exception exc)
            {
                Tools.LogError(exc);
                throw;
            }
        }

        #region IRunnable Members

        public void Run()
        {
            DBMessageQueue dbQueue = new DBMessageQueue(ConstMsg.WorkflowCacheUpdaterQueue);

            using (var workerTiming = new WorkerExecutionTiming("WorkflowCacheUpdater"))
            {
                while (true)
                {
                    try
                    {
                        DBMessage message = dbQueue.Receive();

                        if (message == null)
                        {
                            break;
                        }

                        using (var itemTiming = workerTiming.RecordItem(message.Subject1, message.InsertionTime))
                        {
                            try
                            {
                                DBMessage[] messages = { message };

                                ProcessMessages(messages);
                            }
                            catch (Exception e)
                            {
                                itemTiming.RecordError(e);
                                throw;
                            }
                        }
                    }
                    catch (DBMessageQueueException e)
                    {
                        Tools.LogWarning("DBMQ Exception", e);
                        break;
                    }
                }
            }
        }

        #endregion
    }
}
