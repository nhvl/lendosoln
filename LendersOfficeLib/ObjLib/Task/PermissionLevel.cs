﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Security;
using System.Data.Common;
using System.Data.SqlClient;

namespace LendersOffice.ObjLib.Task
{
    public enum E_PermissionLevel
    {
        WorkOn,
        Manage,
        Close
    }

    public enum E_UserTaskPermissionLevel
    {
        WorkOn,
        Manage,
        None,
        Close
    }

    public static class PermissionLevelUtils
    {
        public static string GetFriendlyDescription(this E_UserTaskPermissionLevel level)
        {
            switch (level)
            {
                case E_UserTaskPermissionLevel.WorkOn:
                    return "WorkOn";
                case E_UserTaskPermissionLevel.Manage:
                    return "Manage";
                case E_UserTaskPermissionLevel.None:
                    return "None";
                case E_UserTaskPermissionLevel.Close:
                    return "Close";
                default:
                    throw new UnhandledEnumException(level);
            }
        }   

        public static string GetFriendlyDescription(this E_PermissionLevel level)
        {
            switch (level)
            {
                case E_PermissionLevel.WorkOn:
                    return "Users with any of the below roles or groups can work on tasks with this permission level.";
                case E_PermissionLevel.Manage:
                    return "Users with any of the below roles or groups can manage tasks with this permission level.";
                case E_PermissionLevel.Close:
                    return "Users with any of the below roles or groups can close tasks with this permission level.";
                default:
                    throw new UnhandledEnumException(level);
            }
        }

        public static string GetFriendlyName(this E_PermissionLevel level)
        {
            switch (level)
            {
                case E_PermissionLevel.WorkOn:
                    return "Work On";
                case E_PermissionLevel.Manage:
                    return "Manage";
                case E_PermissionLevel.Close:
                    return "Close";
                default:
                    throw new UnhandledEnumException(level);
            }
        }

        public static string GetFriendlyName(this E_RoleT role)
        {
            switch (role)
            {
                case E_RoleT.Accountant:
                    return "Accountant";
                case E_RoleT.Administrator:
                    return "Administrator";
                case E_RoleT.CallCenterAgent:
                    return "Call Center Agent";
                case E_RoleT.Closer:
                    return "Closer";
                case E_RoleT.Consumer:
                    return "Consumer";
                case E_RoleT.Funder:
                    return "Funder";
                case E_RoleT.LenderAccountExecutive:
                    return "Lender Account Executive";
                case E_RoleT.LoanOfficer:
                    return "Loan Officer";
                case E_RoleT.LoanOpener:
                    return "Loan Opener";
                case E_RoleT.LockDesk:
                    return "Lock Desk";
                case E_RoleT.Manager:
                    return "Manager";
                case E_RoleT.Processor:
                    return "Processor";
                case E_RoleT.RealEstateAgent:
                    return "Real Estate Agent";
                case E_RoleT.Shipper:
                    return "Shipper";
                case E_RoleT.Underwriter:
                    return "Underwriter";
                case E_RoleT.Pml_LoanOfficer:
                    return "PML Loan Officer";
                case E_RoleT.Pml_BrokerProcessor:
                    return "PML Broker Processor";
                case E_RoleT.Pml_Administrator:
                    return "PML Supervisor";
                case E_RoleT.PostCloser:
                    return "Post-Closer";
                case E_RoleT.Insuring:
                    return "Insuring";
                case E_RoleT.CollateralAgent:
                    return "Collateral Agent";
                case E_RoleT.DocDrawer:
                    return "Doc Drawer";
                case E_RoleT.CreditAuditor:
                    return "Credit Auditor";
                case E_RoleT.DisclosureDesk:
                    return "Disclosure Desk";
                case E_RoleT.JuniorProcessor:
                    return "Junior Processor";
                case E_RoleT.JuniorUnderwriter:
                    return "Junior Underwriter";
                case E_RoleT.LegalAuditor:
                    return "Legal Auditor";
                case E_RoleT.LoanOfficerAssistant:
                    return "Loan Officer Assistant";
                case E_RoleT.Purchaser:
                    return "Purchaser";
                case E_RoleT.QCCompliance:
                    return "QC Compliance";
                case E_RoleT.Secondary:
                    return "Secondary";
                case E_RoleT.Servicing:
                    return "Servicing";
                case E_RoleT.Pml_Secondary:
                    return "Secondary (External)";
                case E_RoleT.Pml_PostCloser:
                    return "Post-Closer (External)";
                default:
                    throw new UnhandledEnumException(role);
            }
        }
    }


    public sealed class PermissionLevel
    {
        private  const string SP_Save = "TASK_PermissionLevelSave";
        

        private Dictionary<E_PermissionLevel, LinkedList<E_RoleT>> m_roles;
        private Dictionary<E_PermissionLevel, LinkedList<Guid>> m_groups;

        public int Id { get; private set; }
        public int Rank { get; set; }
        public string Name { get; set; }
        public Guid BrokerId { get; private set; }
        public bool IsActive { get; set; }
        public bool IsAppliesToConditions { get; set; }
        public string Description { get; set; }
        public string DescriptionForHtml
        {
            get
            {
                return Description.Replace("\n", "<br/>");
            }
        }
        private bool IsNew { get; set; }

        public PermissionLevel(Guid brokerId)
        {
            m_roles = new Dictionary<E_PermissionLevel, LinkedList<E_RoleT>>();
            m_groups = new Dictionary<E_PermissionLevel, LinkedList<Guid>>();

            BrokerId = brokerId; 
            Id = -1;
            IsNew = true;
            Rank = 100; //make sure its added at the end.
        }

        private PermissionLevel(DbDataReader reader)
        {
            m_roles = new Dictionary<E_PermissionLevel, LinkedList<E_RoleT>>();
            m_groups = new Dictionary<E_PermissionLevel, LinkedList<Guid>>();

            Id = (int)reader["TaskPermissionLevelId"];
            BrokerId = (Guid)reader["BrokerId"];
            Name = (string)reader["Name"];
            Description = (string)reader["Description"];
            Rank = (int)reader["Rank"];
            IsAppliesToConditions = (bool)reader["IsAppliesToConditions"];
            IsActive = (bool)reader["IsActive"];

            foreach (E_PermissionLevel level in Enum.GetValues(typeof(E_PermissionLevel)))
            {
                PopulateRoles(level, reader);
                PopulateEmployeeGroups(level, reader);
            }
            IsNew = false;
        }

        private void PopulateEmployeeGroups(E_PermissionLevel level, DbDataReader reader)
        {

            LinkedList<Guid> employeeGroups = m_groups[level] = new LinkedList<Guid>();

            string field = GetGroupFieldName(level);

            string groups = (string)reader[field];

            foreach (string group in groups.Split(new string[]{","}, StringSplitOptions.RemoveEmptyEntries))
            {
                try
                {
                    employeeGroups.AddLast(new Guid(group));
                }
                catch (FormatException)
                {
                    Tools.LogBug("FormatException: Required*EmployeeGroup must be a comma-separated list of GUIDs.");
                    continue;
                }
            }
            
        }

        private void PopulateRoles(E_PermissionLevel level, DbDataReader reader)
        {
            LinkedList<E_RoleT> roles = m_roles[level] = new LinkedList<E_RoleT>();
            foreach (E_RoleT role in Enum.GetValues(typeof(E_RoleT)))
            {
                if (role == E_RoleT.Consumer)
                {
                    continue;
                }

                string field = GetFieldName(level, role);
                if ((bool)reader[field])
                {
                    roles.AddLast(role);
                }
            }
        }

        private string GetGroupFieldName(E_PermissionLevel level)
        {
            string field;
            switch (level)
            {
                case E_PermissionLevel.WorkOn:
                    field = "RequiredWorkOnEmployeeGroup";
                    break;
                case E_PermissionLevel.Close:
                    field = "RequiredCloseEmployeeGroup";
                    break;
                case E_PermissionLevel.Manage:
                    field = "RequiredManageEmployeeGroup";
                    break;
                default:
                    throw new UnhandledEnumException(level);
            }

            return field;
        }

        private string GetFieldName(E_PermissionLevel level, E_RoleT role)
        {
            string prefix;
            switch (level)
            {
                case E_PermissionLevel.WorkOn:
                    prefix = "IsRequiredWorkOnRole";
                    break;
                case E_PermissionLevel.Close:
                    prefix = "IsRequiredCloseRole";
                    break;
                case E_PermissionLevel.Manage:
                    prefix = "IsRequiredManageRole";
                    break;
                default:
                    throw new UnhandledEnumException(level);
            }

            string field = prefix;
            switch (role)
            {
                case E_RoleT.Accountant:
                    field += "Accountant";
                    break;
                case E_RoleT.Administrator:
                    field += "Administrator";
                    break;
                case E_RoleT.CallCenterAgent:
                    field += "CallCenterAgent";
                    break;
                case E_RoleT.Closer:
                    field += "Closer";
                    break;
                case E_RoleT.Consumer:
                    throw new UnhandledEnumException(role);
                case E_RoleT.Funder:
                    field += "Funder";
                    break;
                case E_RoleT.LenderAccountExecutive:
                    field += "LenderAccountExecutive";
                    break;
                case E_RoleT.LoanOfficer:
                    field += "LoanOfficer";
                    break;
                case E_RoleT.LoanOpener:
                    field += "LoanOpener";
                    break;
                case E_RoleT.LockDesk:
                    field += "LockDesk";
                    break;
                case E_RoleT.Manager:
                    field += "Manager";
                    break;
                case E_RoleT.Processor:
                    field += "Processor";
                    break;
                case E_RoleT.RealEstateAgent:
                    field += "RealEstateAgent";
                    break;
                case E_RoleT.Shipper:
                    field += "Shipper";
                    break;
                case E_RoleT.Underwriter:
                    field += "Underwriter";
                    break;
                case E_RoleT.Pml_LoanOfficer:
                    field += "PmlLoanOfficer";
                    break;
                case E_RoleT.Pml_BrokerProcessor:
                    field += "PmlBrokerProcessor";
                    break;
                case E_RoleT.Pml_Administrator:
                    field += "PmlSupervisor";
                    break;
                case E_RoleT.PostCloser:
                    field += "PostCloser";
                    break;
                case E_RoleT.Insuring:
                    field += "Insuring";
                    break;
                case E_RoleT.CollateralAgent:
                    field += "CollateralAgent";
                    break;
                case E_RoleT.DocDrawer:
                    field += "DocDrawer";
                    break;
                case E_RoleT.CreditAuditor:
                    field += "CreditAuditor";
                    break;
                case E_RoleT.DisclosureDesk:
                    field += "DisclosureDesk";
                    break;
                case E_RoleT.JuniorProcessor:
                    field += "JuniorProcessor";
                    break;
                case E_RoleT.JuniorUnderwriter:
                    field += "JuniorUnderwriter";
                    break;
                case E_RoleT.LegalAuditor:
                    field += "LegalAuditor";
                    break;
                case E_RoleT.LoanOfficerAssistant:
                    field += "LoanOfficerAssistant";
                    break;
                case E_RoleT.Purchaser:
                    field += "Purchaser";
                    break;
                case E_RoleT.QCCompliance:
                    field += "QCCompliance";
                    break;
                case E_RoleT.Secondary:
                    field += "Secondary";
                    break;
                case E_RoleT.Servicing:
                    field += "Servicing";
                    break;
                case E_RoleT.Pml_Secondary:
                    field += "ExternalSecondary";
                    break;
                case E_RoleT.Pml_PostCloser:
                    field += "ExternalPostCloser";
                    break;
                default:
                    throw new UnhandledEnumException(role);

            }
            return field;
        }

        public IEnumerable<E_RoleT> GetRolesWithPermissionLevel(E_PermissionLevel level)
        {
            LinkedList<E_RoleT> roles;
            if (m_roles.TryGetValue(level, out roles))
            {
                return roles;
            }
            return new E_RoleT[] { };
        }

        public IEnumerable<Guid> GetGroupsWithPermissionLevel(E_PermissionLevel level)
        {
            LinkedList<Guid> items;
            if (m_groups.TryGetValue(level, out items))
            {
                return items;
            }

            return new Guid[]{};
        }

        public void SetRoles(E_PermissionLevel level, IEnumerable<E_RoleT> roles)
        {
            m_roles[level] = new LinkedList<E_RoleT>(roles); 
        }

        public void SetGroups(E_PermissionLevel level, IEnumerable<Guid> employeeGroups)
        {
            m_groups[level] = new LinkedList<Guid>(employeeGroups);
        }

        public bool IsRoleOrGroupInLevel(E_PermissionLevel level, ICollection<E_RoleT> roles, ICollection<Guid> groups)
        {
            return m_roles[level].Any(role => roles.Contains(role)) || m_groups[level].Any(group => groups.Contains(group));
        }

        public E_UserTaskPermissionLevel GetUserPermissionLevel(ICollection<E_RoleT> userRoles, ICollection<Guid> userGroups)
        {
            if (IsRoleOrGroupInLevel(E_PermissionLevel.Manage, userRoles, userGroups ))
            {
                return E_UserTaskPermissionLevel.Manage;
            }
            if (IsRoleOrGroupInLevel(E_PermissionLevel.Close, userRoles, userGroups))
            {
                return E_UserTaskPermissionLevel.Close;
            }
            if (IsRoleOrGroupInLevel(E_PermissionLevel.WorkOn, userRoles, userGroups))
            {
                return E_UserTaskPermissionLevel.WorkOn;
            }

            return E_UserTaskPermissionLevel.None;
        }

        private string ConcatGroupsForSaving(E_PermissionLevel level)
        {
            string[] groups = m_groups[level].Select(x => x.ToString()).ToArray();
            return String.Join(",", groups);
        }

        public void Save()
        {
            SaveImp(null);
        }

        public void Save(CStoredProcedureExec exec)
        {
            SaveImp(exec);
        }

        private void SaveImp(CStoredProcedureExec exec)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@BrokerId", BrokerId),
                new SqlParameter("@Name", Name),
                new SqlParameter("@Description", Description),
                new SqlParameter("@Rank", Rank),
                new SqlParameter("@IsAppliesToConditions", IsAppliesToConditions),
                new SqlParameter("@IsActive", IsActive)
            };

            if( IsNew == false)
            {
                parameters.Add(new SqlParameter("@TaskPermissionLevelId", Id));
            }

            foreach (E_PermissionLevel level in Enum.GetValues(typeof(E_PermissionLevel)))
            {
                string groupField = "@" + GetGroupFieldName(level);
                parameters.Add(new SqlParameter(groupField, ConcatGroupsForSaving(level)));
                foreach (E_RoleT role in m_roles[level])
                {
                    parameters.Add(new SqlParameter("@" + GetFieldName(level, role), true));
                }

            }

            if (exec == null)
            {
                Id = (int)StoredProcedureHelper.ExecuteScalar(this.BrokerId, "TASK_PermissionLevelSave", parameters);
            }
            else
            {
                exec.ExecuteNonQuery("TASK_PermissionLevelSave", parameters.ToArray());
            }

        }

        public static void Save(params PermissionLevel[] levels)
        {
            if (levels == null)
            {
                return;
            }

            Guid brokerId = Guid.Empty;
            if (levels.Length == 1)
            {
                levels[0].Save();
                return;
            }

            foreach (PermissionLevel level in levels)
            {
                if (brokerId == Guid.Empty)
                {
                    brokerId = level.BrokerId;
                }
                else if (brokerId != level.BrokerId)
                {
                    throw CBaseException.GenericException("Unable to save permission levels for multiple brokers.");
                }
            }

            using (CStoredProcedureExec sp = new CStoredProcedureExec(brokerId))
            {
                sp.BeginTransactionForWrite();

                try
                {
                    Save(sp, levels);
                    sp.CommitTransaction();
                }
                catch (Exception)
                {
                    sp.RollbackTransaction();
                    throw;
                }
            }
        }

        public static void Save(CStoredProcedureExec exec, params PermissionLevel[] levels)
        {
            foreach (PermissionLevel level in levels)
            {
                level.Save(exec);
            }
        }

        private static IEnumerable<PermissionLevel> RetrieveImpl(Guid brokerId, int? id)
        {
            LinkedList<PermissionLevel> levels = new LinkedList<PermissionLevel>();
            
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@Id", id)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TASK_PermissionLevelFetch", parameters))
            {
                while (reader.Read())
                {
                    levels.AddLast(new PermissionLevel(reader));
                }
            }

            return levels;

        }

        public static IEnumerable<PermissionLevel> RetrieveManageLevelsForUser(Guid brokerId, ICollection<E_RoleT> userRoles, ICollection<Guid> userGroups)
        {
            foreach (PermissionLevel level in RetrieveImpl(brokerId, null))
            {
                if (level.IsRoleOrGroupInLevel(E_PermissionLevel.Manage, userRoles, userGroups))
                {
                    yield return level;
                }
            }

        }

        public static PermissionLevel Retrieve(Guid brokerId, int? id)
        {
            return RetrieveImpl(brokerId, id).FirstOrDefault();
        }

        public static IEnumerable<PermissionLevel> RetrieveAll(Guid brokerId)
        {
            return RetrieveImpl(brokerId, null);
        }
    }
}

