﻿// <copyright file="EscrowItemDisbursementSchedule.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   8/15/2014 9:48:21 AM 
// </summary>
namespace LendersOffice.ObjLib.Escrow
{
    /// <summary>
    /// Represents the disbursement schedule for an escrow item.
    /// </summary>
    public class EscrowItemDisbursementSchedule
    {
        /// <summary>
        /// Gets or sets the number of disbursements to be made in January.
        /// </summary>
        /// <value>
        /// The number of disbursements to be made in January.
        /// </value>
        public int Jan { get; set; }

        /// <summary>
        /// Gets or sets the number of disbursements to be made in February.
        /// </summary>
        /// <value>
        /// The number of disbursements to be made in February.
        /// </value>
        public int Feb { get; set; }

        /// <summary>
        /// Gets or sets the number of disbursements to be made in March.
        /// </summary>
        /// <value>
        /// The number of disbursements to be made in March.
        /// </value>
        public int Mar { get; set; }

        /// <summary>
        /// Gets or sets the number of disbursements to be made in April.
        /// </summary>
        /// <value>
        /// The number of disbursements to be made in April.
        /// </value>
        public int Apr { get; set; }

        /// <summary>
        /// Gets or sets the number of disbursements to be made in May.
        /// </summary>
        /// <value>
        /// The number of disbursements to be made in May.
        /// </value>
        public int May { get; set; }

        /// <summary>
        /// Gets or sets the number of disbursements to be made in June.
        /// </summary>
        /// <value>
        /// The number of disbursements to be made in June.
        /// </value>
        public int Jun { get; set; }

        /// <summary>
        /// Gets or sets the number of disbursements to be made in July.
        /// </summary>
        /// <value>
        /// The number of disbursements to be made in July.
        /// </value>
        public int Jul { get; set; }

        /// <summary>
        /// Gets or sets the number of disbursements to be made in August.
        /// </summary>
        /// <value>
        /// The number of disbursements to be made in August.
        /// </value>
        public int Aug { get; set; }

        /// <summary>
        /// Gets or sets the number of disbursements to be made in September.
        /// </summary>
        /// <value>
        /// The number of disbursements to be made in September.
        /// </value>
        public int Sep { get; set; }

        /// <summary>
        /// Gets or sets the number of disbursements to be made in October.
        /// </summary>
        /// <value>
        /// The number of disbursements to be made in October.
        /// </value>
        public int Oct { get; set; }

        /// <summary>
        /// Gets or sets the number of disbursements to be made in November.
        /// </summary>
        /// <value>
        /// The number of disbursements to be made in November.
        /// </value>
        public int Nov { get; set; }

        /// <summary>
        /// Gets or sets the number of disbursements to be made in December.
        /// </summary>
        /// <value>
        /// The number of disbursements to be made in December.
        /// </value>
        public int Dec { get; set; }

        /// <summary>
        /// Returns a value indicating whether the disbursement schedule is valid.
        /// </summary>
        /// <returns>
        /// True if the sum of the disbursements is 12.
        /// </returns>
        public bool Validate()
        {
            int numMonthsDisbursed = this.Jan + this.Feb + this.Mar + this.Apr 
                + this.May + this.Jun + this.Jul + this.Aug + this.Sep + this.Oct 
                + this.Nov + this.Dec;

            return numMonthsDisbursed == 12;
        }
    }
}
