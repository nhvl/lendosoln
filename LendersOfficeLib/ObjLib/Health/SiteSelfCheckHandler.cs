﻿// <copyright file="SiteSelfCheckHandler.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   3/31/2016
// </summary>
namespace LendersOffice.ObjLib.Health
{
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Text;
    using System.Web;
    using Adapter;
    using Common;
    using Constants;
    using DataAccess;

    /// <summary>
    /// A handler for IT to verify core component of the site is working after perform windows update.
    /// </summary>
    public class SiteSelfCheckHandler : IHttpHandler
    {
        /// <summary>
        /// Gets a value indicating whether the handler can be reused to service multiple request.
        /// </summary>
        /// <value>Always true since it can.</value>
        public bool IsReusable
        {
            get { return true; }
        }

        /// <summary>
        /// Processes the given request.
        /// </summary>
        /// <param name="context">The request response object.</param>
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            context.Response.Cache.SetNoStore();
            context.Response.Cache.SetExpires(DateTime.MinValue);

            // OPM 237104 - Implement a self check for IT.
            // Per IT recommendation only allow this page from IPs
            // 127.0.0.1
            // 172.30.28.*
            // 172.30.29.*
            // 172.30.26.*
            // 10.11.33.*
            string[] allowIpList =
            {
                "127.0.0.1",
                "172.30.28.",
                "172.30.29.",
                "172.30.26.",
                "10.11.33.",
            };

            bool isInAllowableList = false;

            string ip = RequestHelper.ClientIP;

            foreach (string allowIp in allowIpList)
            {
                if (ip.StartsWith(allowIp))
                {
                    isInAllowableList = true;
                    break;
                }
            }

            if (isInAllowableList == false)
            {
                context.Response.Write("DENIED");
                return;
            }

            this.TestSqlConnections();
            this.TestSplitDatabaseConnections();
            this.TestFileDb();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<html>");
            sb.AppendLine("<body>");
            sb.AppendLine("<h1>" + ConstAppDavid.ServerName + " site check is <span style='color:green'>PASS</span> at " + DateTime.Now + "</h1>");
            sb.AppendLine("</body>");
            sb.AppendLine("</html>");

            context.Response.StatusCode = 200;
            context.Response.Write(sb.ToString());
        }

        /// <summary>
        /// Perform SQL connection all different connections.
        /// </summary>
        private void TestSqlConnections()
        {
            this.AssertSqlConnection(DbAccessUtils.GetConnection(DataSrc.LOShare));
            this.AssertSqlConnection(DbAccessUtils.GetConnection(DataSrc.LOShareROnly));
            this.AssertSqlConnection(DbAccessUtils.GetConnection(DataSrc.LOTransient));
            this.AssertSqlConnection(DbAccessUtils.GetConnection(DataSrc.LpeSrc));
            this.AssertSqlConnection(DbAccessUtils.GetConnection(DataSrc.RateSheet));
        }

        /// <summary>
        /// Connect to SQL Connection and perform simple operation.
        /// </summary>
        /// <param name="conn">Connection to SQL server.</param>
        private void AssertSqlConnection(DbConnection conn)
        {
            using (conn)
            {
                conn.OpenWithRetry();
                using (DbCommand command = conn.CreateCommand())
                {
                    command.CommandText = "SELECT GETDATE();";
                    using (DbDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (reader.Read() == false)
                        {
                            throw new Exception("Unable to execute SELECT GETDATE()");
                        }
                    }
                }

                conn.Close();
            }
        }

        /// <summary>
        /// Test all the split database connection.
        /// </summary>
        private void TestSplitDatabaseConnections()
        {
            foreach (var databaseInfo in DbConnectionInfo.ListAll())
            {
                this.AssertSqlConnection(databaseInfo.GetReadOnlyConnection());
                this.AssertSqlConnection(databaseInfo.GetConnection());
            }
        }

        /// <summary>
        /// Test file db operation.
        /// </summary>
        private void TestFileDb()
        {
            foreach (E_FileDB fileDb in Enum.GetValues(typeof(E_FileDB)))
            {
                string content = "THIS IS A TEST STRING!$%^#" + Guid.NewGuid();

                string fileName = Guid.NewGuid().ToString();

                FileDBTools.WriteData(fileDb, fileName, content);

                string responseContent = FileDBTools.ReadDataText(fileDb, fileName);

                if (content.Equals(responseContent) == false)
                {
                    throw new Exception("Content in FileDB after write is not match. DbName=" + fileDb + ", key=[" + fileName + "]");
                }

                FileDBTools.Delete(fileDb, fileName);
            }
        }
    }
}
