﻿// <copyright file="SiteStatusHandler.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Antonio Valencia
//  Date:   2/19/2015
// </summary>
namespace LendersOffice.ObjLib.Health
{
    using System;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Web;
    using DataAccess;

    /// <summary>
    /// A handler for the loadbalancer. Returns 200 unless a specific file
    /// does not exist or the handler cannot connect to the database.
    /// </summary>
    public class SiteStatusHandler : IHttpHandler
    {
        /// <summary>
        /// Gets a value indicating whether the handler can be reused to service multiple request.
        /// </summary>
        /// <value>Always true since it can.</value>
        public bool IsReusable
        {
            get { return true; }
        }

        /// <summary>
        /// Processes the given request.
        /// </summary>
        /// <param name="context">The request response object.</param>
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            context.Response.Cache.SetNoStore();
            context.Response.Cache.SetExpires(DateTime.MinValue);

            string url = context.Request.Url.ToString();

            int pageNameStartIndex = url.LastIndexOf("/") + 1;

            if (pageNameStartIndex <= 0 || pageNameStartIndex >= url.Length)
            {
                this.ReturnErrorResponse(context, 500);
                return;
            }
            
            string pageName = url.Substring(pageNameStartIndex).Replace("aspx", "txt");

            FileInfo file = new FileInfo(Tools.GetServerMapPath($"~/{pageName}"));

            if (!file.Exists || !file.Name.Contains("sitecheck"))
            {
                this.ReturnErrorResponse(context, 404);
                return;
            }

            string[] data = File.ReadAllLines(file.FullName);
            string line1 = data.Length == 0 ? "alive" : data[0];
            bool runFull = line1.IndexOf("alive") >= 0;
            bool runSql = runFull || line1.IndexOf("sql") >= 0;
            bool runFileDB = runFull || line1.IndexOf("filedb") >= 0;

            try
            {
                if (runSql)
                {
                    Tools.CallDB();
                }
                
                var passed = runFileDB && this.CheckFileDB();
                context.Response.StatusCode = passed ? 200 : 500;
                context.Response.Write("alive");
                return;
            }
            catch (SqlException e)
            {
                Tools.LogError(e);
            }
            catch (FileNotFoundException e)
            {
                Tools.LogError(e);
            }

            this.ReturnErrorResponse(context, 500);
        }

        /// <summary>
        /// Ensures FileDB is working with reading and writing.
        /// </summary>
        /// <returns>True if filedb is up and running false otherwise.</returns>
        private bool CheckFileDB()
        {
            string key = $"sitecheck_{Guid.NewGuid()}";
            byte[] data = Guid.NewGuid().ToByteArray();
            E_FileDB location = E_FileDB.Temp;

            try
            {
                FileDBTools.WriteData(location, key, data);
                var data2 = FileDBTools.ReadData(location, key);
                return data.SequenceEqual(data2);
            }
            finally
            {
                FileDBTools.Delete(location, key.ToString());
            }
        }

        /// <summary>
        /// Writes an error code to the response.
        /// </summary>
        /// <param name="context">The http context.</param>
        /// <param name="errorCode">The code to send.</param>
        private void ReturnErrorResponse(HttpContext context, int errorCode)
        {
            context.Response.StatusCode = errorCode;
        }
    }
}
