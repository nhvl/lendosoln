﻿namespace LendersOffice.ObjLib.Disclosure.EConsentMonitoring
{
    using System;
    using DataAccess;

    /// <summary>
    /// Provides a simple data wrapper for a loan file
    /// that will be affected by disabling e-consent 
    /// monitoring and must be migrated.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Maintain naming of loan properties.")]
    public class DisableEConsentMonitoringAffectedFileData
    {
        /// <summary>
        /// Gets or sets the ID for the file.
        /// </summary>
        public Guid sLId { get; set; }

        /// <summary>
        /// Gets or sets the loan number for the file.
        /// </summary>
        public string sLNm { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the loan needs initial disclosures.
        /// </summary>
        public bool sNeedInitialDisc { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the loan needs redisclosures.
        /// </summary>
        public bool sNeedRedisc { get; set; }

        /// <summary>
        /// Gets or sets the loan's necessary disclosure type.
        /// </summary>
        public E_sDisclosureNeededT sDisclosureNeededT { get; set; }

        /// <summary>
        /// Gets or sets the loan's disclosure due date in string format.
        /// </summary>
        public string sDisclosuresDueD_rep { get; set; }

        /// <summary>
        /// Returns the string representation of the affected file.
        /// </summary>
        /// <returns>
        /// The string representation of the affected file.
        /// </returns>
        public override string ToString()
        {
            return string.Join(
                ",",
                this.sLId,
                this.sLNm,
                this.sNeedInitialDisc,
                this.sNeedRedisc,
                this.sDisclosureNeededT.ToString("G"),
                this.sDisclosuresDueD_rep);
        }
    }
}
