﻿namespace LendersOffice.ObjLib.Disclosure.EConsentMonitoring
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides functionality to disable e-consent monitoring
    /// by migrating loans that would become "stuck" if e-consent
    /// monitoring is disabled.
    /// </summary>
    public class DisableEConsentMonitoringMigrationHelper
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DisableEConsentMonitoringMigrationHelper"/> class.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the lender running the migration.
        /// </param>
        public DisableEConsentMonitoringMigrationHelper(Guid brokerId)
        {
            this.BrokerId = brokerId;
        }

        /// <summary>
        /// Gets or sets the ID of the lender running the migration.
        /// </summary>
        private Guid BrokerId { get; set; }

        /// <summary>
        /// Retrieves the list of affected files that will need to be migrated
        /// before e-consent monitoring can be disabled for the lender.
        /// </summary>
        /// <returns>
        /// The list of affected files.
        /// </returns>
        public IEnumerable<DisableEConsentMonitoringAffectedFileData> GetAffectedFileList()
        {
            var affectedFileList = new LinkedList<DisableEConsentMonitoringAffectedFileData>();

            var procedureName = StoredProcedureName.Create("DisableEConsentMonitoringMigration_GetAffectedFileList").Value;
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", this.BrokerId)
            };

            using (var connection = DbAccessUtils.GetConnection(this.BrokerId))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, procedureName, parameters, TimeoutInSeconds.Sixty))
            {
                while (reader.Read())
                {
                    var affectedFileData = new DisableEConsentMonitoringAffectedFileData()
                    {
                        sLId = (Guid)reader["sLId"],
                        sLNm = (string)reader["sLNm"],
                        sNeedInitialDisc = (bool)reader["sNeedInitialDisc"],
                        sNeedRedisc = (bool)reader["sNeedRedisc"],
                        sDisclosureNeededT = (E_sDisclosureNeededT)reader["sDisclosureNeededT"]
                    };

                    // sDisclosuresDueD has an "old" and "new" database field and a migration
                    // that was completed prior to this helper. Most files should have been
                    // migrated, but just in case, use the same logic that the getter uses
                    // to retrieve the value.
                    var disclosureDueDateField = CDateTime.Create(reader["sDisclosuresDueD"] as DateTime?);
                    if (!disclosureDueDateField.IsValid)
                    {
                        disclosureDueDateField = CDateTime.Create(reader["sDisclosuresDueD_New"] as DateTime?);
                    }

                    affectedFileData.sDisclosuresDueD_rep = disclosureDueDateField.IsValid 
                        ? disclosureDueDateField.DateTimeForComputationWithTime.ToString()
                        : string.Empty;

                    affectedFileList.AddLast(affectedFileData);
                }
            }

            return affectedFileList;
        }

        /// <summary>
        /// Migrates the file with the given loan ID.
        /// </summary>
        /// <param name="loanId">
        /// The ID of the file to migrate.
        /// </param>
        public void MigrateFile(Guid loanId)
        {
            var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(DisableEConsentMonitoringMigrationHelper));
            loan.InitSave();

            loan.ByPassFieldSecurityCheck = true;
            loan.DisableComplianceAutomation = true;
            loan.IsSkipTaskUpdateUponSave = true;

            loan.MigrateDisclosureStateOffEConsentMonitoringAutomation();

            loan.Save();
        }

        /// <summary>
        /// Disables e-consent monitoring for the lender by 
        /// disabling the setting bit.
        /// </summary>
        public void DisableLenderBit()
        {
            var procedureName = StoredProcedureName.Create("DisableEConsentMonitoringMigration_DisableLenderBit").Value;
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", this.BrokerId)
            };

            using (var connection = DbAccessUtils.GetConnection(this.BrokerId))
            {
                StoredProcedureDriverHelper.ExecuteNonQuery(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty);
            }
        }
    }
}
