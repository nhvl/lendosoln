﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Diagnostics;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Security;
using LendersOffice.Constants;
using LendersOffice.Email;
using LendersOffice.Migration;
using LendersOffice.Common;
using LendersOffice.ObjLib.DocumentGeneration;
using LendersOffice.Integration.DocumentVendor;
using System.Threading;
using LendersOffice.Admin;
using ServiceExecutionLogging;
using LendersOffice.ObjLib.Loan.Security;

namespace LendersOffice.ObjLib.Disclosure
{
    /// <summary>
    /// Handles the nightly tasks that are needed for the Disclosure Compliance
    /// Tracking feature. See OPM 105770.
    /// </summary>
    public class NightlyDisclosureRunner
    {
        private const string UnableToDeterminePlanCodeErrorMessage = "Unable to determine loan program for audit.";
        private static readonly Guid FirstTechBrokerId = new Guid("E9F006C9-CB83-4531-8925-08FCCF4ADD63");
        private static readonly Guid DocuTechJXServerVendorId = new Guid("198A7288-6440-4718-8D5C-6BF8616F8A3D");
        private const string DocuTechDisclosurePackageID = "203";
        private static readonly LqbGrammar.DataTypes.DocumentIntegrationPackageName DocuTechDisclosurePackageName = LqbGrammar.DataTypes.DocumentIntegrationPackageName.Create("Initial Disclosure").ForceValue();
        private const string CriticalEmailBody =
            "Use the following pb log query to determine the next action: disclosurenightly " +
            "type:error -server:loextdevweb1\r\n\r\nIf there were errors updating individual " +
            "files, visit the E-Consent Expiration page in LOAdmin. This page provides the " +
            "ability to manually expire e-consent for a single loan id.\r\n\r\nIf the " +
            "process failed to retrieve the files that need to be processed but did not " +
            "crash, it is likely that we will need to start the e-consent portion of the " +
            "nightly process manually. This can be done via ScheduleExecutable.exe " +
            "DisclosureNightly_ProcessLoansForEConsentExpiration.\r\n\r\nIf the process " +
            "crashed while processing e-consent expiration, it is likely the entire " +
            "process needs to be restarted. This can be done with ScheduleExecutable.exe " +
            "DisclosureNightly.\r\n\r\nThe manual updates will need to be verified, as " +
            "another critical case will not be generated unless the entire process is " +
            "restarted.";

        private readonly Logger econsentExpirationLogger;
        private const string EConsentExpiredServiceName = "NightlyDisclosureEConsentExpiration";

        private readonly Logger emailNotificationLogger;
        private const string EmailNotificationServiceName = "NightlyDisclosureEmail";

        private static readonly Guid FirstTechTestingBranchId = new Guid("5545AA94-5256-452C-9293-0319AA2C0ABF");

        public string Description
        {
            get
            {
                return "Handles the nightly tasks that are needed for the Disclosure Compliance Tracking feature. See OPM 105770";
            }
        }

        public NightlyDisclosureRunner()
        {
            this.econsentExpirationLogger = new Logger(EConsentExpiredServiceName);
            this.emailNotificationLogger = new Logger(EmailNotificationServiceName);

            if (this.econsentExpirationLogger.IsImproperlyConfigured())
            {
                SendLoggerImproperlyConfiguredEmail(EConsentExpiredServiceName);
            }
            
            if (this.emailNotificationLogger.IsImproperlyConfigured())
            {
                SendLoggerImproperlyConfiguredEmail(EmailNotificationServiceName);
            }
        }

        public void SendLoggerImproperlyConfiguredEmail(string serviceName)
        {
            var email = new CBaseEmail(ConstAppDavid.SystemBrokerGuid)
            {
                From = ConstStage.DefaultDoNotReplyAddress,
                To = ConstStage.ExceptionDeveloperSupport,
                CCRecipient = "geoffreyf@meridianlink.com",
                Subject = "Nightly Disclosure Monitoring Config Issue",
                Message = "The " + serviceName + " service is not configured properly."
            };

            EmailUtilities.SendEmail(email);
        }

        public void Run()
        {
            Tools.SetupServicePointManager();
            Tools.LogInfo("[DisclosureNightly] Processing e-consent expiration.");
            ProcessEConsentExpiration(generateCriticalOnFailure: true, logger: this.econsentExpirationLogger);

            if (ConstAppDavid.CurrentServerLocation == ServerLocation.Beta
                || ConstAppDavid.CurrentServerLocation == ServerLocation.Production)
            {
                Tools.LogInfo("[DisclosureNightly] Performing automatic disclosures.");
                PerformAutomaticDisclosures(FirstTechBrokerId, null);
            }

            Tools.LogInfo("[DisclosureNightly] Generating disclosure compliance email notifications.");
            SendEmailNotifications(this.emailNotificationLogger);
        }

        public static void ProcessEConsentExpirationForLoans(IEnumerable<Guid> loanIds, bool generateCriticalOnFailure)
        {
            foreach (var loanId in loanIds)
            {
                try
                {
                    CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(NightlyDisclosureRunner));
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                    // 6/17/15 gf - Since this is now a public method, I'm adding this check
                    // to ensure that somebody doesn't accidentally pass in a file that is
                    // in a state where e-consent expiration clearly shouldn't happen. 
                    // Note: this won't prevent somebody from passing in a file that is not
                    // yet due to expire, and it is possible to prematurely expire files if
                    // used incorrectly.
                    if (dataLoan.sDisclosureNeededT == E_sDisclosureNeededT.AwaitingEConsent)
                    {
                        if (!dataLoan.sEConsentCompleted)
                        {
                            dataLoan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.EConsentExpired);
                        }
                        else
                        {
                            // 10/16/2013 gf - related to changes for opm 124417.
                            // If we process a loan and it is marked as awaiting
                            // e-consent and e-consent has been completed, we can
                            // safely say that no disclosure is needed. Otherwise,
                            // the stored procedure will keep returning these files.
                            dataLoan.sDisclosureNeededT = E_sDisclosureNeededT.None;
                        }

                        dataLoan.Save();
                    }
                }
                catch (Exception exc)
                {
                    var msg = "[DisclosureNightly] Encountered unexpected error while processing e-consent expiration for loanId: " + loanId;
                    Tools.LogErrorWithCriticalTracking(msg, exc);

                    if (generateCriticalOnFailure)
                    {
                        EmailUtilities.SendToCritical(msg, CriticalEmailBody);
                    }
                }
            }
        }

        /// <summary>
        /// Process E-Consent expiration. This occurs when E-Consent 
        /// has not been received within 48 hours.
        /// </summary>
        public static void ProcessEConsentExpiration(bool generateCriticalOnFailure, Logger logger)
        {
            bool sentEmailToCritical = false;
            List<Guid> loanIds = new List<Guid>();

            Tools.LogInfo("[DisclosureNightly] Retrieving loans for e-consent expiration.");

            // If this is being run by something other than the nightly process,
            // we don't want to log execution.
            if (logger != null)
            {
                logger.Record(Logger.StatusType.STARTED, ConstAppDavid.ServerName);
            }

            try
            {
                foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
                {
                    using (IDataReader sR = StoredProcedureHelper.ExecuteReader(connInfo, "ListLoansAwaitingEConsentDueToday", null))
                    {
                        while (sR.Read())
                        {
                            loanIds.Add((Guid)sR["sLId"]);
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                var msg = "[DisclosureNightly] Encountered exception while retrieving loans to process for e-consent expiration.";
                Tools.LogErrorWithCriticalTracking(msg, exc);

                if (generateCriticalOnFailure && !sentEmailToCritical)
                {
                    EmailUtilities.SendToCritical(msg, CriticalEmailBody);
                    sentEmailToCritical = true;
                }
            }

            Tools.LogInfo("[DisclosureNightly] Processing " + loanIds.Count + " loans for e-consent expiration.");

            bool generateCriticalForLoanUpdateFailure = generateCriticalOnFailure && !sentEmailToCritical;
            ProcessEConsentExpirationForLoans(loanIds, generateCriticalForLoanUpdateFailure);

            Tools.LogInfo("[DisclosureNightly] Done processing " + loanIds.Count + " loans for e-consent expiration.");

            if (logger != null)
            {
                logger.Record(Logger.StatusType.OK, ConstAppDavid.ServerName);
            }
        }

        private static List<Guid> GetLoanIdsForAutoDisclosure(Guid brokerId, Guid? branchID)
        {
            if (brokerId != FirstTechBrokerId 
                && (ConstAppDavid.CurrentServerLocation == ServerLocation.Production 
                || ConstAppDavid.CurrentServerLocation == ServerLocation.Beta))
            {
                throw new ArgumentException("This should only be configured to run for FirstTech.");
            }
            else if (ConstAppDavid.CurrentServerLocation == ServerLocation.Production &&
                branchID == null)
            {
                throw new ArgumentException("This should only be run for a given branch on production."); ;
            }

            var loanIds = new List<Guid>();

            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            if (branchID != null)
            {
                parameters.Add(new SqlParameter("@BranchId", branchID.Value));
            }

            try
            {
                using (IDataReader sR = StoredProcedureHelper.ExecuteReader(
                    brokerId,
                    "ListLoansForAutoDisclosure",
                    parameters))
                {
                    while (sR.Read())
                    {
                        loanIds.Add((Guid)sR["sLId"]);
                    }
                }
            }
            catch (SqlException exc)
            {
                Tools.LogErrorWithCriticalTracking("[DisclosureNightly] Failed to retrieve loans for auto-disclosure. Broker id: " + brokerId, exc);
            }

            return loanIds;
        }

        private static Dictionary<Guid, Guid> autoDisclosureVendorIdByBrokerId =
            new Dictionary<Guid, Guid>()
        {
            { new Guid("839127ef-72d1-4873-bfdf-18a23416b146"), new Guid("aab1d625-d386-47ac-a6b5-9babe8ffb509") },
            { FirstTechBrokerId, DocuTechJXServerVendorId }
        };

        /// <summary>
        /// Checks the loan file for essential data errors.  For some reason, the doc vendors don't return fatals here...
        /// </summary>
        /// <param name="loan">The loan file to check.</param>
        /// <returns>A list of error messages generated from the loan data.</returns>
        private static List<string> PerformPreAuditCheck(CPageData loan)
        {
            List<string> result = new List<string>();
            if (!loan.sHomeownerCounselingOrganizationLastModifiedD.IsValid)
            {
                result.Add("Automatic disclosure failed because LendingQB detected closest homeowner counseling organizations have not been generated");
            }

            return result;
        }

        private static DocumentVendorResult<LendersOffice.Integration.DocumentVendor.Audit> PerformLoanAudit(
            IDocumentVendor vendor,
            Guid loanId,
            Guid appId,
            out string planCode,
            AbstractUserPrincipal principal)
        {
            planCode = "";

            var auditor = new DocumentAuditor(
                vendor,
                loanId,
                appId);

            var planCodeResult = vendor.GetAvailablePlanCodes(loanId, principal);

            if (planCodeResult.HasError)
            {
                return DocumentVendorResult.Error<LendersOffice.Integration.DocumentVendor.Audit>(
                    planCodeResult.ErrorMessage,
                    new LendersOffice.Integration.DocumentVendor.Audit());
            }
            else
            {
                var allPlanCodes = planCodeResult.Result.Values.SelectMany(p => p);

                if (allPlanCodes.Count() != 1)
                {
                    return GeneratePlanCodeError(vendor, loanId);
                }

                var planCodeToUse = allPlanCodes.First();

                if (string.IsNullOrEmpty(planCodeToUse.Code))
                {
                    return GeneratePlanCodeError(vendor, loanId);
                }
                else
                {
                    planCode = planCodeToUse.Code;

                    return auditor.Audit(
                        DocuTechDisclosurePackageID,
                        planCode,
                        "",
                        "",
                        principal);
                }
            }
        }

        private static DocumentVendorResult<LendersOffice.Integration.DocumentVendor.Audit> GeneratePlanCodeError(
            IDocumentVendor vendor,
            Guid loanId)
        {
            var error = string.Format(
                "[DisclosureNightly] Received unexpected result from " +
                "GetAvailablePlanCodes for vendor: {0}, loan id: {1}.",
                vendor.Skin.VendorName,
                loanId);

            Tools.LogErrorWithCriticalTracking(error, new Exception());

            return DocumentVendorResult.Error<LendersOffice.Integration.DocumentVendor.Audit>(
                UnableToDeterminePlanCodeErrorMessage,
                new LendersOffice.Integration.DocumentVendor.Audit());
        }

        /// <summary>
        /// This is only set up to work for FirstTech.
        /// </summary>
        /// <param name="vendor"></param>
        /// <param name="loan"></param>
        /// <param name="appId"></param>
        /// <param name="planCode"></param>
        /// <returns></returns>
        private static DocumentVendorResult<IDocumentGenerationResult> GenerateDocuments(
            IDocumentVendor vendor,
            CPageData loan,
            Guid appId,
            string planCode,
            AbstractUserPrincipal principal)
        {
            if (vendor == null)
            {
                throw new ArgumentNullException("vendor");
            }
            else if (planCode == null)
            {
                throw new ArgumentNullException("planCode");
            }

            var request = vendor.GetDocumentGenerationRequest(
                appId,
                loan.sLId);

            request.LoanProgramId = planCode;

            bool allowESign = true;
            request.SendEDisclosures(allowESign);

            // OPM 215788 - Make agent for Docutech ESign notification email configurable by lender.
            E_AgentRoleT agentForDocutechEsignNotificationEmail = loan.BrokerDB.DocutechEsignNotificationEmailAgent;

            var agent = loan.GetAgentOfRole(
                agentForDocutechEsignNotificationEmail,
                E_ReturnOptionIfNotExist.ReturnEmptyObject);
            
            string esignNotificationAddress = agent.EmailAddr;
            if (vendor.Config.PlatformType == E_DocumentVendor.DocuTech
                && loan.BrokerDB.UseCustomField60AsEsignNotificationList
                && !string.IsNullOrWhiteSpace(loan.sCustomField60Notes))
            {
                esignNotificationAddress = loan.sCustomField60Notes;
            }

            ((ConfiguredDocumentGenerationRequest)request).SetEsignNotificationEmail(esignNotificationAddress);

            request.DocumentFormat = "PDF";
            request.PackageType = DocuTechDisclosurePackageID;
            request.PackageName = DocuTechDisclosurePackageName;

            var generator = new DocumentGenerator(
                vendor,
                loan.sLId,
                appId);

            return generator.Generate(
                DocuTechDisclosurePackageID,
                request,
                principal);
        }

        /// <summary>
        /// Attempts to generate initial disclosures for eligible loans.
        /// </summary>
        /// <param name="brokerId">
        /// The broker for which to run the auto-disclosure process.
        /// </param>
        /// <param name="onlyRunForTheseLoanIds">
        /// Run auto-disclosure for only these loan ids. If null, run for all.
        /// </param>
        /// <remarks>
        /// If onlyRunForTheseLoanIds is not null, this will attempt to generate
        /// documents regardless of whether the file would have been included in
        /// the automated process.
        /// </remarks>
        public static void PerformAutomaticDisclosures(Guid brokerId, IEnumerable<Guid> onlyRunForTheseLoanIds)
        {
            var originalPrincipal = Thread.CurrentPrincipal;

            try
            {
                AbstractUserPrincipal principal = SystemUserPrincipal.AutomatedDisclosuresSystemUser(ConstStage.AutoDisclosureSystemUserId);
                Thread.CurrentPrincipal = principal;

                if (brokerId != FirstTechBrokerId
                    && (ConstAppDavid.CurrentServerLocation == ServerLocation.Production
                    || ConstAppDavid.CurrentServerLocation == ServerLocation.Beta))
                {
                    Tools.LogErrorWithCriticalTracking(
                        "[DisclosureNightly] Auto-disclosure is only supported for FirstTech.",
                        new Exception());

                    return;
                }

                var brokerDB = BrokerDB.RetrieveById(brokerId);
                Guid? branchID = brokerDB.AutoDisclosureBranchID;

                if (!brokerDB.IsEnableDocuTechAutoDisclosure)
                {
                    return;
                }
                else if (ConstAppDavid.CurrentServerLocation == ServerLocation.Production &&
                    branchID == null)
                {
                    Tools.LogError(
                        "[DisclosureNightly] Running auto-disclosures on " +
                        "production currently requires a branch id to be " +
                        "configured in the broker option xml.",
                        new Exception());
                    return;
                }

                var vendorId = autoDisclosureVendorIdByBrokerId[brokerId];

                var config = VendorConfig.Retrieve(vendorId);

                var credentials = VendorCredentials.GetBrokerLevelCredentials(
                    brokerId,
                    vendorId);

                var vendor = new ConfiguredDocumentVendor(
                    config,
                    credentials,
                    brokerId);

                IEnumerable<Guid> loanIds;

                if (onlyRunForTheseLoanIds != null && onlyRunForTheseLoanIds.Any())
                {
                    loanIds = onlyRunForTheseLoanIds;
                }
                else
                {
                    loanIds = GetLoanIdsForAutoDisclosure(brokerId, branchID);
                }

                foreach (var loanId in loanIds)
                {
                    try
                    {
                        var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                            loanId,
                            typeof(NightlyDisclosureRunner));

                        loan.InitSave(ConstAppDavid.SkipVersionCheck);

                        DocumentVendorResult<Integration.DocumentVendor.Audit> auditResult = null;
                        DocumentVendorResult<IDocumentGenerationResult> docGenerationResult = null;

                        List<string> preAuditErrors = PerformPreAuditCheck(loan);
                        bool autoDisclosureFailed = preAuditErrors != null && preAuditErrors.Any();

                        if (!autoDisclosureFailed)
                        {
                            var appId = loan.GetAppData(0).aAppId;

                            string planCode;

                            auditResult = PerformLoanAudit(
                                vendor,
                                loanId,
                                appId,
                                out planCode,
                                principal);

                            bool auditHadFatalErrors = auditResult.Result != null
                                && auditResult.Result.Results != null
                                && auditResult.Result.Results.Any(a => a.Severity == AuditSeverity.Fatal);

                            autoDisclosureFailed |= auditResult.HasError || auditHadFatalErrors;

                            if (!autoDisclosureFailed)
                            {
                                docGenerationResult = GenerateDocuments(
                                    vendor,
                                    loan,
                                    appId,
                                    planCode,
                                    principal);

                                autoDisclosureFailed |= docGenerationResult.HasError;
                            }
                        }
                        
                        if (autoDisclosureFailed)
                        {
                            loan.sAutoDisclosureFailed = true;
                            loan.Save();
                        }

                        // This automatically saves to FileDB, so we don't need to call Save().
                        loan.sAutoDisclosureAuditHtml = AuditHtmlGenerator.GenerateAuditHtml(
                            auditResult,
                            preAuditErrors,
                            docGenerationResult);
                    }
                    catch (Exception exc)
                    {
                        var error = string.Format(
                            "[DisclosureNightly] Encountered unhandled error " +
                            "for loan id: {0}.",
                            loanId);

                        Tools.LogErrorWithCriticalTracking(
                            error,
                            exc);
                    }
                }
            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking(
                    "[DisclosureNightly] Unexpected error while performing " +
                    "automatic disclosures.",
                    exc);
            }
            finally
            {
                Thread.CurrentPrincipal = originalPrincipal;
            }
        }

        /// <summary>
        /// Generate disclosure notification emails for loans which the user has read access.
        /// <remarks>
        /// Generates a single email for each user. The email contains all of the loans that
        /// the user has disclosure permissions to access and read access to.
        /// </remarks>
        /// </summary>
        private void SendEmailNotifications(Logger logger)
        {
            logger.Record(Logger.StatusType.STARTED, ConstAppDavid.ServerName);
            IEnumerable<Guid> brokerIds = Tools.GetAllActiveBrokers();

            foreach (Guid brokerId in brokerIds)
            {
                try
                {
                    SendEmailNotifications(brokerId);
                }
                catch (Exception exc)
                {
                    Tools.LogError($"[DisclosureNightly] Unexpected error sending disclosure emails for broker id {brokerId}.", exc);
                }
            }

            logger.Record(Logger.StatusType.OK, ConstAppDavid.ServerName);
        }

        private static void SendEmailNotifications(Guid brokerId)
        {
            var lenderTimer = Stopwatch.StartNew();
            var broker = BrokerDB.RetrieveById(brokerId);

            LogInfo($"Generating emails for {broker.CustomerCode}.");
            IReadOnlyList<Guid> loanIdsThatNeedDisclosure = GetLoansThatNeedDisclosure(brokerId);
            LogInfo($"{loanIdsThatNeedDisclosure.Count} loans need disclosure for {broker.CustomerCode}.");

            if (!loanIdsThatNeedDisclosure.Any())
            {
                return;
            }

            var employeesByEmployeeId = GetEmployeesWithDisclosurePermissions(brokerId)
                .ToDictionary(e => e.EmployeeId);

            LogInfo($"{employeesByEmployeeId.Keys.Count} employees have disclosure permissions for {broker.CustomerCode}.");

            var permissionResolver = new LoanReadPermissionResolver(brokerId);

            var loanIdsByEmployeeId = permissionResolver.GetReadableLoans(
                employeesByEmployeeId.Values.Select(e => e.Principal),
                loanIdsThatNeedDisclosure);

            var loanInfoCacheByLoanId = new Dictionary<Guid, LoanInfo>();

            foreach (var employeeId in loanIdsByEmployeeId.Keys)
            {
                var employeeInfo = employeesByEmployeeId[employeeId];
                var loansForNotification = loanIdsByEmployeeId[employeeId];

                StringBuilder messageBody = new StringBuilder();
                messageBody.AppendFormat("The following loan files must be disclosed or redisclosed today:{0}{0}", Environment.NewLine);
                bool sendEmail = false;

                foreach (var loanId in loansForNotification)
                {
                    try
                    {
                        LoanInfo loanInfo;
                        if (!loanInfoCacheByLoanId.TryGetValue(loanId, out loanInfo))
                        {
                            loanInfo = new LoanInfo(loanId);
                            loanInfoCacheByLoanId.Add(loanId, loanInfo);
                        }

                        bool needsNotification = loanInfo.sBranchChannelT == E_BranchChannelT.Retail &&
                            loanInfo.sNeedInitialDisc && employeeInfo.Principal.HasPermission(Permission.ResponsibleForInitialDiscRetail);
                        needsNotification |= loanInfo.sBranchChannelT != E_BranchChannelT.Retail &&
                            loanInfo.sNeedInitialDisc && employeeInfo.Principal.HasPermission(Permission.ResponsibleForInitialDiscWholesale);
                        needsNotification |= loanInfo.sNeedRedisc &&
                            employeeInfo.Principal.HasPermission(Permission.ResponsibleForRedisclosures);
                        // 11/7/2013 gf - opm 143822 If the loan is from FTFCU's TESTING BRANCH, exclude it from the email.
                        needsNotification &= loanInfo.sBranchId != FirstTechTestingBranchId;

                        if (needsNotification)
                        {
                            // 11/8/2013 gf - opm 143912 per FTFCU request, include processor name in email.
                            messageBody.AppendFormat("({0}) {1} - {2} - Processor: {4}{3}", loanInfo.sStatusT_rep, loanInfo.sLNm, loanInfo.aBLastNm,
                                Environment.NewLine, loanInfo.sEmployeeProcessorName);
                            sendEmail = true;
                        }
                    }
                    catch (Exception exc)
                    {
                        var errMsg = String.Format("[DisclosureNightly] Unexpected error for loanId: {0}"
                            + ". Error: {1}", loanId, exc.Message);
                        Tools.LogError(errMsg);
                    }
                }

                if (sendEmail)
                {
                    SendEmail(employeeInfo, messageBody);
                }
            }

            lenderTimer.Stop();

            LogInfo($"Processed {broker.CustomerCode} in {lenderTimer.ElapsedMilliseconds} ms.");
        }

        private static void SendEmail(EmployeeInfo employee, StringBuilder messageBody)
        {
            if (string.IsNullOrEmpty(employee.Email))
            {
                Tools.LogInfo("[DisclosureNightly] - Did not send Disclosure Summary to User Id:[" + employee.UserId + "]. Email is Empty. Email=[" + employee.Email + "]");
            }
            else
            {
                CBaseEmail email = new CBaseEmail (employee.BrokerId)
                {
                    Subject = String.Format("LendingQB Disclosure Summary - {0}", DateTime.Today.ToString("M/d/yyyy")),
                    From = ConstStage.DefaultDoNotReplyAddress,
                    To = employee.Email,
                    Bcc = "",
                    Message = messageBody.ToString(),
                    IsHtmlEmail = false,
                    DisclaimerType = LendersOffice.Common.E_DisclaimerType.NORMAL
                };

                int numTries = 0;
                bool sentEmail = false;
                while (numTries < 3 && !sentEmail)
                {
                    numTries++;
                    try
                    {
                        email.Send();
                        sentEmail = true;
                    }
                    catch (Exception)
                    {
                        Tools.SleepAndWakeupRandomlyWithin(0, 500);
                    }
                }

                if (!sentEmail)
                {
                    try
                    {
                        EmailUtilities.SendEmail(email);
                    }
                    catch (Exception exc)
                    {
                        Tools.LogError(string.Format("[DisclosureNightly] Failed to send email to user id: {0}. Message length: {1}. Email=[{2}]",
                            employee.UserId, messageBody.Length, employee.Email), exc);
                    }
                }
            }
        }

        private static void LogInfo(string message)
        {
            Tools.LogInfo($"[DisclosureNightly] {message}");
        }

        private static IReadOnlyList<Guid> GetLoansThatNeedDisclosure(Guid brokerId)
        {
            List<Guid> loanIds = new List<Guid>();

            try
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", brokerId)
                                            };
                using (IDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, "ListLoansThatNeedDisclosureForBroker", parameters))
                {
                    while (sR.Read())
                    {
                        loanIds.Add((Guid)sR["sLId"]);
                    }
                }
            }
            catch (SqlException exc)
            {
                Tools.LogErrorWithCriticalTracking("[DisclosureNightly] Failed to retrieve loans for broker id: " + brokerId, exc);
            }

            return loanIds;
        }

        private static IEnumerable<EmployeeInfo> GetEmployeesWithDisclosurePermissions(Guid brokerId)
        {
            List<EmployeeInfo> employees = new List<EmployeeInfo>();

            SqlParameter[] paramters = {
                                           new SqlParameter("@BrokerId", brokerId), 
                                           new SqlParameter("@IsActiveFilter", true)
                                       };
            using (IDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, "ListEmployeeByBrokerId", paramters))
            {
                while (sR.Read())
                {
                    if (sR["EmployeeUserId"] == DBNull.Value || sR["CanLogin"] == DBNull.Value)
                    {
                        // This happens for accounts with no login, and can be safely ignored. See OPM 125333.
                        // 10/1/2013 gf - Added check for CanLogin, if they can't login we don't want to include.
                        // TODO: these checks should be eliminated by the stored proc.
                        continue;
                    }
                    bool canLogin = (bool)sR["CanLogin"];
                    Guid employeeId = (Guid)sR["EmployeeId"];
                    Guid userId = (Guid)sR["EmployeeUserId"];
                    string email = (string)sR["Email"];
                    if (canLogin)
                    {
                        employees.Add(new EmployeeInfo(employeeId, userId, email, brokerId));
                    }
                }
            }

            return employees.Where(e => e.HasDisclosurePermission);
        }

        private class EmployeeInfo
        {
            private AbstractUserPrincipal internalPrincipal;
            public readonly Guid BrokerId;
            public readonly Guid EmployeeId;
            public readonly Guid UserId;
            public readonly string Email;

            public AbstractUserPrincipal Principal
            {
                get
                {
                    if (this.internalPrincipal == null)
                    {
                        this.internalPrincipal = PrincipalFactory.RetrievePrincipalForUser(BrokerId, UserId, "?");
                    }

                    return this.internalPrincipal;
                }
            }

            public bool HasDisclosurePermission
            {
                get
                {
                    return this.Principal != null &&
                        (this.Principal.HasPermission(Permission.ResponsibleForInitialDiscRetail)
                        || this.Principal.HasPermission(Permission.ResponsibleForInitialDiscWholesale)
                        || this.Principal.HasPermission(Permission.ResponsibleForRedisclosures));
                }
            }

            public EmployeeInfo(Guid empId, Guid userId, string email, Guid brokerId)
            {
                EmployeeId = empId;
                UserId = userId;
                Email = email;
                BrokerId = brokerId;
            }
        }

        private class LoanInfo
        {
            public readonly E_BranchChannelT sBranchChannelT;
            public readonly bool sNeedInitialDisc;
            public readonly bool sNeedRedisc;
            public readonly Guid sBranchId;
            public readonly string sStatusT_rep;
            public readonly string sLNm;
            public readonly string aBLastNm;
            public readonly string sEmployeeProcessorName;

            public LoanInfo(Guid loanId)
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(LoanInfo));
                dataLoan.InitLoad();

                this.sBranchChannelT = dataLoan.sBranchChannelT;
                this.sNeedInitialDisc = dataLoan.sNeedInitialDisc;
                this.sNeedRedisc = dataLoan.sNeedRedisc;
                this.sBranchId = dataLoan.sBranchId;
                this.sStatusT_rep = dataLoan.sStatusT_rep;
                this.sLNm = dataLoan.sLNm;
                this.aBLastNm = dataLoan.GetAppData(0).aBLastNm;
                this.sEmployeeProcessorName = dataLoan.sEmployeeProcessorName;
            }
        }
    }
}
