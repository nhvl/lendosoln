﻿namespace LendersOffice.ObjLib.Disclosure
{
    using System;
    using DataAccess;
    using LendersOffice.Integration.DocumentVendor;

    /// <summary>
    /// Wrapper class that holds data required for delayed ESign notifications.
    /// </summary>
    public class ESignNotificationDatesUpdateData
    {
        /// <summary>
        /// Gets or sets the vendor provided disclosure metat data object.
        /// </summary>
        public VendorProvidedDisclosureMetadata VendorProvidedDisclosureMetaData { get; set; }

        /// <summary>
        /// Gets or sets the transaction id.
        /// </summary>
        public string TransactionId { get; set; }

        /// <summary>
        /// Gets or sets the disclosure event type.
        /// </summary>
        public E_EDisclosureDisclosureEventT DisclosureType { get; set; }

        /// <summary>
        /// Gets or sets the borrower name.
        /// </summary>
        public string BorrowerName { get; set; }

        /// <summary>
        /// Gets or sets the borrower type.
        /// </summary>
        public E_aTypeT? BorrowerType { get; set; }

        /// <summary>
        /// Gets or sets the application id and borrower type tuple.
        /// </summary>
        public Tuple<Guid, E_BorrowerModeT> AppIdAndBorrowerType { get; set; }
    }
}
