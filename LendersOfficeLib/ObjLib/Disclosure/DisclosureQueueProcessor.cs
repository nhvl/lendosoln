﻿using System;
using System.Collections.Generic;
using System.Configuration;
using CommonProjectLib.Logging;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.ObjLib.DatabaseMessageQueue;
using LendersOffice.Security;

namespace LendersOffice.ObjLib.Disclosure
{
    public class DisclosureQueueProcessor : CommonProjectLib.Runnable.IRunnable
    {
        #region Setup Logging
        static DisclosureQueueProcessor()
        {
            string msmqLogConnectStr = ConstStage.MsMqLogConnectStr;
            if (string.IsNullOrEmpty(msmqLogConnectStr) == false)
            {
                LogManager.Add(new LendersOffice.Common.MSMQLogger(msmqLogConnectStr, ConstStage.EnvironmentName));
            }

            string server = ConstStage.SmtpServer;
            int port = ConstStage.SmtpServerPortNumber;
            string notifyOnErrorEmail = ConfigurationManager.AppSettings["NotifyOnErrorEmailAddress"];
            string notifyFromErrorEmail = ConfigurationManager.AppSettings["NotifyFromOnError"];
            if (false == string.IsNullOrEmpty(server) && false == string.IsNullOrEmpty(notifyOnErrorEmail) && false == string.IsNullOrEmpty(notifyFromErrorEmail))
            {
                LogManager.Add(new EmailLogger(server, port, notifyOnErrorEmail, notifyFromErrorEmail));
            }
        }
        #endregion

        public string Description
        {
            get
            {
                return "DisclosureQueueProcessor";
            }
        }


        private DBMessageQueue m_disclosureQ;
        /// <summary>
        /// Process delayed field change events for disclosure automation.
        /// </summary>
        /// <remarks>
        /// When the user changes certain fields, the information about these 
        /// field changes is entered into a db message queue. The basic idea
        /// is that we want to process field change triggers once they are 30
        /// minutes old. If the current value stored on the loan file differs
        /// from the original value, we want to process the appropriate disclosure
        /// event.
        /// 
        /// We also want to avoid a case where a user changes multiple fields on
        /// a file and then discloses between the processing of the two changes.
        /// To avoid this, we do not process a trigger if it was inserted into 
        /// the queue before the last time the file was disclosed.
        /// 
        /// The datalayer does not check to see if there is already a pending
        /// trigger for a file/field in the queue, so it will always send one.
        /// We only need to process the first item (the one that has been in
        /// the queue the longest) for each field change per loan.
        /// 
        /// We need to load the loan and check if that field is different from
        /// the value on the loan before any field change triggers were enqueued.
        /// 
        /// NOTE: Right now, this is only set up to handle re-disclosure triggers.
        /// </remarks>
        public void Run()
        {
            Tools.SetupServicePointManager();
            m_disclosureQ = new DBMessageQueue(ConstMsg.DisclosureQueue);

            var messageIdsForLoanId = new Dictionary<string, List<long>>();
            var loanToBrokerId = new Dictionary<string, string>();
            var fieldsForLoanId = new Dictionary<string, List<string>>();

            foreach (DBMessage msg in m_disclosureQ.Entries)
            {
                int cutoffHours = ConstStage.DisclosureDelayedTriggerCutoffMinutes / 60;
                int cutoffMinutes = ConstStage.DisclosureDelayedTriggerCutoffMinutes % 60;
                DateTime emailCutoff = DateTime.Now.Subtract(new TimeSpan(cutoffHours, cutoffMinutes, 0));

                var loanId = msg.Subject1;
                
                var messageData = ParseMessageData(msg);
                if (messageData == null) continue;
                var field = messageData["field"];
                var brokerId = messageData["brokerid"];

                if (!loanToBrokerId.ContainsKey(loanId))
                {
                    loanToBrokerId[loanId] = brokerId;
                }

                // If this field for this loan already has a change that is being
                // processed, then process this message.
                if (messageIdsForLoanId.ContainsKey(loanId) 
                    && fieldsForLoanId[loanId].Contains(field))
                {
                    messageIdsForLoanId[loanId].Add(msg.Id);
                }
                else if (msg.InsertionTime.CompareTo(emailCutoff) < 0)
                {
                    if (!messageIdsForLoanId.ContainsKey(loanId))
                    {
                        var messageIds = new List<long>();
                        messageIds.Add(msg.Id);
                        messageIdsForLoanId.Add(loanId, messageIds);
                    }
                    else
                    {
                        messageIdsForLoanId[loanId].Add(msg.Id);
                    }

                    if (!fieldsForLoanId.ContainsKey(loanId))
                    {
                        var fields = new List<string>();
                        fields.Add(field);
                        fieldsForLoanId.Add(loanId, fields);
                    }
                    else
                    {
                        fieldsForLoanId[loanId].Add(field);
                    }
                }
            }

            foreach (var loanId in messageIdsForLoanId.Keys)
            {
                ProcessMessagesForLoan(new Guid(loanId), messageIdsForLoanId[loanId], new Guid(loanToBrokerId[loanId]), fieldsForLoanId[loanId]);
            }
        }

        private void ProcessMessagesForLoan(Guid loanId, List<long> msgIds, Guid brokerId, List<string> fields)
        {
            bool useCustomCocFields = BrokerDB.RetrieveUseCustomCocFieldListBit(brokerId);

            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypassAndGuaranteedFields(loanId, typeof(DisclosureQueueProcessor), fields);
            // sk opm 185732 - I will allow them to process this on the qp2.0 loan, but will block and error if a disclosure would happen.
            dataLoan.AllowSaveWhileQP2Sandboxed = true;
            try
            {                
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            }
            catch (LoanNotFoundException)
            {
                // If we cannot locate the loan it was probably deleted.
                Tools.LogWarning("[DisclosureQueueProcessor] Unable to locate file with id " + loanId);
                foreach (var id in msgIds)
                {
                    this.m_disclosureQ.ReceiveById(id);
                }
                return;
            }
            bool saveLoan = false;
            
            // The snapshot is taken using this format target, so we must use it here.
            dataLoan.SetFormatTarget(FormatTarget.Webform);

            // Set to keep track of which fields we have already processed.
            HashSet<string> processedFields = new HashSet<string>();
            
            foreach (var id in msgIds)
            {
                var msg = m_disclosureQ.ReceiveById(id);
                var msgData = ParseMessageData(msg);
                if (msgData == null) continue;

                Guid userId = new Guid(msgData["userid"]);
                string displayName = msgData["displayname"];
                string field = msgData["field"];
                string oldValue = msgData["oldvalue"];
                string customDescription = null;

                if (msgData.Count == 6)
                {
                    customDescription = msgData["description"];
                }

                // If the first field change event for this field has already
                // been processed, subsequent triggers can be ignored because
                // ultimately we care about whether the currently saved value
                // on the loan file is different from the original value that
                // was recorded on the first trigger.
                if (processedFields.Contains(field))
                {
                    continue;
                }
                else if (dataLoan.sLastDisclosedD.IsValid)
                {
                    // If this message was inserted before the loan was disclosed,
                    // then we can completely ignore this trigger because all of
                    // the most recent information was included in the disclosure.
                    var lastDisclosedD = dataLoan.sLastDisclosedD.DateTimeForComputationWithTime;
                    if (msg.InsertionTime.CompareTo(lastDisclosedD) < 0)
                    {
                        continue;
                    }
                }

                // If the currently saved value for this field is still different,
                // then process the disclosure event.
                if (ValueIsStillDifferent(dataLoan, field, oldValue))
                {
                    var disclosureNeededT = GetDisclosureNeededT(field, useCustomCocFields);
                    if (disclosureNeededT == null)
                    {
                        processedFields.Add(field);
                        var errMsg = String.Format("This disclosure event cannot be processed. sLId: {0}, data: {1}",
                            msg.Subject1, msg.Data);
                        Tools.LogError(errMsg);
                        continue;
                    }
                    else
                    {
                        dataLoan.ProcessDelayedRedisclosureTrigger(disclosureNeededT.Value, userId, displayName, customDescription);

                        if (E_sLoanFileT.QuickPricer2Sandboxed == dataLoan.sLoanFileT)
                        {
                            if (dataLoan.sNeedInitialDisc || dataLoan.sNeedRedisc)
                            {
                                var errMsg = string.Format(
                                    "This disclosure event would trigger a loan save," +
                                    " but QuickPricer 2.0 loans should not trigger a disclosure loan save. " +
                                    "sLId: {0}, data: {1}, loan.sNeedInitialDisc {2}, loan.sNeedRedisc {3}",
                                    msg.Subject1,  //  {0}
                                    msg.Data,   //  {1}
                                    dataLoan.sNeedInitialDisc,  //  {2}
                                    dataLoan.sNeedRedisc    //  {3}
                                    );
                                Tools.LogErrorWithCriticalTracking(errMsg, new Exception(errMsg));
                            }
                            continue;
                        }

                        saveLoan = true;                        
                    }
                }

                processedFields.Add(field);
            }

            if (saveLoan)
            {
                dataLoan.Save();
            }
        }

        private Dictionary<string, string> ParseMessageData(DBMessage msg)
        {
            var msgData = new Dictionary<string, string>();

            string[] paramsAndValues = msg.Data.Split(new string[] { ";;" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string paramAndValue in paramsAndValues)
            {
                string[] split = paramAndValue.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);

                if (split.Length == 1 && !string.IsNullOrEmpty(split[0]))
                {
                    msgData.Add(split[0], string.Empty);
                }
                else if (split.Length == 2 && !string.IsNullOrEmpty(split[0]) && !string.IsNullOrEmpty(split[1]))
                {
                    msgData.Add(split[0], split[1]);
                }
                else
                {
                    var errMsg = String.Format("Message data was of the wrong format. Id: {0}, sLId: {1}, data: {2}",
                        msg.Id, msg.Subject1, msg.Data);
                    Tools.LogError(errMsg);
                    return null;
                }
            }

            if (!msgData.ContainsKey("userid") || !msgData.ContainsKey("displayname")
                || !msgData.ContainsKey("field") || !msgData.ContainsKey("oldvalue")
                || !msgData.ContainsKey("brokerid") || (msgData.Count == 6 && (!msgData.ContainsKey("description"))))
            {
                var errMsg = String.Format("Message data was of the wrong format. Id: {0}, sLId: {1}, data: {2}",
                        msg.Id, msg.Subject1, msg.Data);
                Tools.LogError(errMsg);
                return null;
            }

            return msgData;
        }

        private bool ValueIsStillDifferent(CPageData dataLoan, string field, string oldVal)
        {
            switch (field)
            {
                case "sFinalLAmt":
                    return dataLoan.sFinalLAmt_rep != oldVal;
                case "sLDiscnt":
                    return dataLoan.sLDiscnt_rep != oldVal;
                case "sApprVal":
                    return dataLoan.sApprVal_rep != oldVal;
                case "sCreditScoreLpeQual":
                    return dataLoan.sCreditScoreLpeQual_rep != oldVal;
                case "sMldsHasImpound":
                    return dataLoan.sMldsHasImpound != (oldVal == "Yes");
                case "sPurchPrice":
                    return dataLoan.sPurchPrice_rep != oldVal;
                default:
                    var propInfo = Tools.GetProperty(field, typeof(CPageData));

                    if (propInfo == null)
                    {
                        Tools.LogError("This disclosure event cannot be processed. This field hasn't been implemented: " + field);
                        return false;
                    }

                    var val = propInfo.GetValue(dataLoan);

                    if (val.GetType() == typeof(bool))
                    {
                        return (bool)val != (oldVal == "Yes");
                    }

                    return val.ToString() != oldVal;
            }
        }

        private E_sDisclosureNeededT? GetDisclosureNeededT(string field, bool useCustomCocFields)
        {
            if (useCustomCocFields)
            {
                return E_sDisclosureNeededT.Custom;
            }

            switch (field)
            {
                case "sFinalLAmt":
                    return E_sDisclosureNeededT.CC_LoanAmtChanged;
                case "sLDiscnt":
                    return E_sDisclosureNeededT.CC_GFEChargeDiscountChanged;
                case "sApprVal":
                    return E_sDisclosureNeededT.CC_PropertyValueChanged;
                case "sCreditScoreLpeQual":
                    return E_sDisclosureNeededT.CC_UWCreditScoreChanged;
                case "sMldsHasImpound":
                    return E_sDisclosureNeededT.CC_ImpoundChanged;
                case "sPurchPrice":
                    return E_sDisclosureNeededT.CC_PurchasePriceChanged;
                default:
                    return null;
            }
        }
    }
}
