﻿namespace LendersOffice.ObjLib.Logging
{
    using System;
    using System.Threading;

    /// <summary>
    /// Thread local singleton class that provides contextual information for creating logs.
    /// </summary>
    public class LoggingContext
    {
        /// <summary>
        /// Thread local logging context.
        /// </summary>
        private static ThreadLocal<LoggingContext> threadContext = new ThreadLocal<LoggingContext>();

        /// <summary>
        /// Prevents a default instance of the <see cref="LoggingContext" /> class from being created.
        /// </summary>
        private LoggingContext()
        {
        }

        /// <summary>
        /// Gets the current instance of LoggingContext.
        /// </summary>
        public static LoggingContext Current
        {
            get
            {
                if (threadContext.IsValueCreated)
                {
                    return threadContext.Value;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets or sets the Loan ID.
        /// </summary>
        public Guid? LoanId { get; set; }

        /// <summary>
        /// Gets or sets the Correlation ID.
        /// </summary>
        public Guid? CorrelationId { get; set; }

        /// <summary>
        /// Gets or sets the login name of current user.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Creates a new LoggingContext and sets it as the current context.
        /// </summary>
        public static void CreateNewThreadContext()
        {
            threadContext.Value = new LoggingContext();
        }

        /// <summary>
        /// Clears the current context.
        /// </summary>
        public static void ClearThreadContext()
        {
            threadContext.Value = null;
        }
    }
}
