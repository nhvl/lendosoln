﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Constants;
using DataAccess;

namespace LendersOffice.Logging
{
    public class MSMQLogger : ILogger
    {

        private LendersOffice.Common.MSMQLogger _logger = null;

        public MSMQLogger()
        {
            List<string> connectionStrings = new List<string>();

            if (string.IsNullOrEmpty(ConstStage.MsMqLogConnectStr) == false)
            {
                connectionStrings.Add(ConstStage.MsMqLogConnectStr);
            }

            if (string.IsNullOrEmpty(ConstSite.MsMqPbLogConnectStr) == false)
            {
                connectionStrings.Add(ConstSite.MsMqPbLogConnectStr);
            }

            _logger = new LendersOffice.Common.MSMQLogger(connectionStrings.ToArray(), ConstStage.EnvironmentName);
        }

        #region ILogger Members

        public void Log(LoggingLevel level, string message)
        {
            Log(level, message, null);
        }

        public void Log(LoggingLevel level, string message, Dictionary<string, string> properties)
        {
            if (_logger == null)
            {
                return;
            }
            global::CommonProjectLib.Logging.E_LoggingLevel loggingLevel = global::CommonProjectLib.Logging.E_LoggingLevel.Info;
            switch (level)
            {
                case LoggingLevel.Trace:
                    loggingLevel = global::CommonProjectLib.Logging.E_LoggingLevel.Trace;
                    break;
                case LoggingLevel.Info:
                    loggingLevel = global::CommonProjectLib.Logging.E_LoggingLevel.Info;
                    break;
                case LoggingLevel.Debug:
                    loggingLevel = global::CommonProjectLib.Logging.E_LoggingLevel.Debug;
                    break;
                case LoggingLevel.Warn:
                    loggingLevel = global::CommonProjectLib.Logging.E_LoggingLevel.Warn;
                    break;
                case LoggingLevel.Error:
                    loggingLevel = global::CommonProjectLib.Logging.E_LoggingLevel.Error;
                    break;
                default:
                    throw new UnhandledEnumException(level);
            }
            _logger.Log(loggingLevel, new global::CommonProjectLib.Logging.LogProperties(message, properties));

        }


        #endregion
    }
}
