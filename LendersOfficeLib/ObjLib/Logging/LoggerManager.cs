﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LendersOffice.Logging
{
    public sealed class LoggerManager
    {
        public static ILogger GetDefaultLogger()
        {
            return LqbGrammar.GenericLocator<ILoggerFactory>.Factory.GetLogger();
        }

        public class MSMQFactory : ILoggerFactory
        {
            private static readonly Lazy<MSMQLogger> defaultLogger = new Lazy<MSMQLogger>(System.Threading.LazyThreadSafetyMode.ExecutionAndPublication);

            public ILogger GetLogger() => defaultLogger.Value;
        }
    }
}
