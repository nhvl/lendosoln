﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LendersOffice.Logging
{
    public interface ILogger
    {
        void Log(LoggingLevel level, string message, Dictionary<string, string> properties);
        void Log(LoggingLevel level, string message);
    }

    public interface ILoggerFactory
    {
        ILogger GetLogger();
    }
}
