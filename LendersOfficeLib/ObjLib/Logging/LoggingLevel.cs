﻿
namespace LendersOffice.Logging
{
    public enum LoggingLevel
    {
        Trace = 0,
        Info = 1,
        Debug = 2,
        Warn = 3,
        Error = 4,
    }
}
