﻿namespace LendersOffice.ObjLib.Logging
{
    using System;
    using System.Threading;

    /// <summary>
    /// Our log code uses AppDomain.CurrentDomain.FriendlyName as module name. This class used to customize Module Name. 
    /// </summary>
    public class ModuleNameContext
    {
        /// <summary>
        /// Thread local logging context.
        /// </summary>
        private static ThreadLocal<ModuleNameContext> threadContext = new ThreadLocal<ModuleNameContext>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleNameContext" /> class.
        /// </summary>
        /// <param name="name">Module name.</param>
        private ModuleNameContext(string name)
        {
            this.Name = name;
        }

        /// <summary>
        /// Gets the current instance of ModuleNameContext.
        /// </summary>
        public static ModuleNameContext Current
        {
            get
            {
                if (threadContext.IsValueCreated)
                {
                    return threadContext.Value;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the module name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Customize module for current thread.
        /// </summary>
        /// <param name="name">Module name.</param>
        public static void SetModuleName(string name)
        {
            threadContext.Value = new ModuleNameContext(name);
        }
    }
}
