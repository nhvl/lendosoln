﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;

using DataAccess;
using LendersOffice.ObjLib.Task;
using LendersOffice.Constants;
using LendersOffice.Security;
using EDocs;

namespace LendersOffice.ObjLib.DocumentConditionAssociation
{
    public enum E_DocumentConditionAssociationStatus
    {
        Undefined = 0,
        Satisfied = 1,
        Unsatisfied = 2
    }

    public class AssociatedTask
    {
        public DocumentConditionAssociation Association;
        public Task.Task Condition;

        public bool IsAssociated
        {
            get { return Association != null; }
        }
    }

    public class DocumentConditionAssociation
    {
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Loan ID
        /// </summary>
        public Guid LoanId { get; private set; }

        /// <summary>
        /// EDocument ID
        /// </summary>
        public Guid DocumentId { get; private set; }
        
        // Populated in GetAssociationsByLoanHeavy
        public string DocumentFolderName { get; private set; }
        public string DocumentTypeName { get; private set; }
        public E_EDocStatus DocumentStatus { get; private set; }
        /// <summary>
        /// Task ID
        /// </summary>
        public string TaskId { get; private set; }


        /// <summary>
        /// Only set on get heavy
        /// </summary>
        public int DocTypeId { get; private set; }

        /// <summary>
        /// Only set on get heavy
        /// </summary>
        public int CondRowId { get; private set; }

        // Populated in GetAssociationsByLoanHeavy
        public string TaskSubject { get; private set; }

        /// <summary>
        /// Status
        /// </summary>
        public E_DocumentConditionAssociationStatus Status { get; set; }

        public DocumentConditionAssociation(Guid brokerId, Guid loanId, string taskId, Guid docId)
        {
            this.BrokerId = brokerId;
            this.LoanId = loanId;
            this.TaskId = taskId;
            this.DocumentId = docId;
        }

        private DocumentConditionAssociation(Guid brokerId, DbDataReader reader)
        {
            this.BrokerId = brokerId;
            this.LoanId = (Guid)reader["sLId"];
            this.TaskId = reader["TaskId"].ToString();
            this.DocumentId = (Guid)reader["DocumentId"];
            this.Status = (E_DocumentConditionAssociationStatus)(int)reader["Status"];

            try
            {
                // This information will not be available in most queries
                this.DocumentFolderName = reader["FolderName"].ToString();
                this.DocumentTypeName = reader["DocTypeName"].ToString();
                this.DocumentStatus = (E_EDocStatus)(byte)reader["DocumentStatus"];
                this.TaskSubject = reader["TaskSubject"].ToString();
                this.DocTypeId = (int)reader["DocTypeId"];
            }
            catch (IndexOutOfRangeException) { }
        }

        public void Save()
        {
            var parameters = GetIdentityParameters();
            parameters.Add(new SqlParameter("@Status", this.Status));

            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "DOCCONDITION_Save", 3, parameters);
            // There is a database trigger to reject associations if they are created on rejected documents
        }

        public void Save(CStoredProcedureExec exec)
        {
            var parameters = GetIdentityParameters();
            parameters.Add(new SqlParameter("@Status", this.Status));

            exec.ExecuteNonQuery("DOCCONDITION_Save", 3, parameters.ToArray());
            // There is a database trigger to reject associations if they are created on rejected documents
        }

        private List<SqlParameter> GetIdentityParameters()
        {
            return new List<SqlParameter>() {
                new SqlParameter("@LoanId", this.LoanId),
                new SqlParameter("@TaskId", this.TaskId),
                new SqlParameter("@DocId",  this.DocumentId)
            };
        }

        public void Delete()
        {
            var parameters = GetIdentityParameters();
            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "DOCCONDITION_Delete", 3, parameters);
        }

        /// <summary>
        /// Get all the associations linked to a specific document.
        /// </summary>
        /// <param name="loanId"></param>
        /// <param name="docId"></param>
        /// <returns></returns>
        private static List<DocumentConditionAssociation> GetAssociationsByLoanDocument(Guid brokerId, Guid loanId, Guid docId)
        {
            var parameters = new SqlParameter[] {
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@DocumentId", docId)
            };

            var assocs = new List<DocumentConditionAssociation>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "DOCCONDITION_GetByLoanDocument", parameters))
            {
                while (reader.Read())
                {
                    var assoc = new DocumentConditionAssociation(brokerId, reader);
                    assocs.Add(assoc);
                }
            }
            return assocs;
        }

        /// <summary>
        /// Get all the associations linked to a specific document. Contains extra columns a la GetAssociationsByLoanHeavy
        /// </summary>
        /// <param name="loanId"></param>
        /// <param name="docId"></param>
        /// <returns></returns>
        public static List<DocumentConditionAssociation> GetAssociationsByLoanDocumentHeavy(Guid brokerId, Guid loanId, Guid docId, AbstractUserPrincipal principal)
        {
            var parameters = new SqlParameter[] {
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@DocumentId", docId),
                new SqlParameter("@ExcludeHiddenConditions", !principal.HasPermission(Permission.CanViewHiddenInformation))
            };

            var assocs = new List<DocumentConditionAssociation>();
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "DOCCONDITION_GetByLoanDocumentHeavy", parameters))
            {
                while (reader.Read())
                {
                    var assoc = new DocumentConditionAssociation(brokerId, reader);
                    assoc.CondRowId = (int)reader["CondRowId"];
                    assocs.Add(assoc);
                }
            }
            return assocs;
        }

        /// <summary>
        /// Get all the associations linked to a specific condition
        /// </summary>
        /// <param name="loanId"></param>
        /// <param name="taskId"></param>
        /// <returns></returns>
        public static List<DocumentConditionAssociation> GetAssociationsByLoanCondition(Guid brokerId, Guid loanId, string taskId)
        {
            var parameters = new SqlParameter[] {
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@TaskId", taskId)
            };

            var assocs = new List<DocumentConditionAssociation>();
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "DOCCONDITION_GetByLoanCondition", parameters))
            {
                while (reader.Read())
                {
                    var assoc = new DocumentConditionAssociation(brokerId, reader);
                    assocs.Add(assoc);
                }
            }
            return assocs;
        }

        public static List<DocumentConditionAssociation> GetAssociationsByLoanConditionHeavy(Guid brokerId, Guid loanId, string taskId)
        {
            var parameters = new SqlParameter[] {
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@TaskId", taskId)
            };

            var assocs = new List<DocumentConditionAssociation>();
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "DOCCONDITION_GetByLoanConditionHeavy", parameters))
            {
                while (reader.Read())
                {
                    var assoc = new DocumentConditionAssociation(brokerId, reader);
                    assocs.Add(assoc);
                }
            }
            return assocs;
        }

        /// <summary>
        /// Gets a dictionary containing all the association data for a given loan. It willg roup them in a dictionary by task id.
        /// </summary>
        /// <param name="brokerId">The broker identifier.</param>
        /// <param name="loanId">The loan identifier.</param>
        /// <returns>A dictionary containing for the associations by task id.</returns>
        public static IDictionary<string, List<DocumentConditionAssociation>> GetAssociationsForLoanByTaskIdHeavy(AbstractUserPrincipal principal, Guid loanId)
        {
            return GetAssociationsByLoanHeavy(principal.BrokerId, loanId, principal).GroupBy(p => p.TaskId).ToDictionary(p => p.Key, p => p.ToList(), StringComparer.OrdinalIgnoreCase);
        }


        /// <summary>
        /// Same as GetAssociationsByLoan, except it has three new columns: doc folder name, doc type name, doc status (?), and task subject.
        /// The database call performs a join on VIEW_VALID_EDOCS_DOCUMENT and on TASK
        /// </summary>
        /// <param name="loanId"></param>
        /// <returns></returns>
        public static List<DocumentConditionAssociation> GetAssociationsByLoanHeavy(Guid brokerId, Guid loanId, AbstractUserPrincipal principal)
        {
            var parameters = new SqlParameter[] {
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@ExcludeHiddenConditions", !principal.HasPermission(Permission.CanViewHiddenInformation)),
                new SqlParameter("@BrokerId", brokerId)
            };

            var assocs = new List<DocumentConditionAssociation>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "DOCCONDITION_GetByLoanHeavy", parameters))
            {
                while (reader.Read())
                {
                    var assoc = new DocumentConditionAssociation(brokerId, reader);
                    assoc.CondRowId = (int)reader["CondRowId"];
                    assocs.Add(assoc);
                }
            }
            return assocs;
        }

        public static List<AssociatedTask> GetConditionsByLoanDocument(AbstractUserPrincipal principal, Guid loanId, Guid docId, bool excludeThoseUserDoesntHaveAccessTo)
        {
            var brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            var associationsByTaskId = GetAssociationsByLoanDocument(brokerId, loanId, docId).ToDictionary(a => a.TaskId);
            var conditions = Task.Task.GetActiveConditionsByLoanId(brokerId, loanId, principal.HasPermission(Permission.CanViewHiddenInformation), excludeThoseUserDoesntHaveAccessTo);

            var result = new List<AssociatedTask>(conditions.Count);

            foreach (var cond in conditions)
            {
                DocumentConditionAssociation assoc = null;
                associationsByTaskId.TryGetValue(cond.TaskId, out assoc);
                result.Add(new AssociatedTask() { Association = assoc, Condition = cond });
            }

            return result;
        }

        public static void DeleteAllAssociationsForTask(CStoredProcedureExec exec, Guid sLId, string taskId)
        {
            exec.ExecuteNonQuery("DOCCONDITION_DeleteAllAssociations", 
                new SqlParameter("@LoanId", sLId),
                new SqlParameter("@TaskId", taskId));
        }
    }
}
