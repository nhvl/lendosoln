﻿namespace LendersOffice.ObjLib.Regions
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Runtime.Serialization;
    using Adapter;
    using DataAccess;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Class represending a user defined set of Regions.
    /// </summary>
    [DataContract]
    public class RegionSet
    {
        /// <summary>
        /// A dictionary mapping Regions that are a part of this set by their Region ID.
        /// </summary>
        private Dictionary<int, Region> regions = new Dictionary<int, Region>();

        /// <summary>
        /// Initializes a new instance of the <see cref="RegionSet" /> class.
        /// Used for JSON deserialization.
        /// </summary>
        protected RegionSet()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RegionSet" /> class.
        /// </summary>
        /// <param name="row">RegionSet data retrieved from SQL.</param>
        private RegionSet(DataRow row)
        {
            this.Id = (int)row["RegionSetId"];
            this.BrokerId = (Guid)row["BrokerId"];
            this.Name = (string)row["Name"];
            this.Description = (string)row["Description"];
        }

        /// <summary>
        /// Gets the region set ID.
        /// </summary>
        /// <value>Region set ID.</value>
        [DataMember(Name = "Id")]
        public int Id { get; private set; } = -1;

        /// <summary>
        /// Gets the set's Broker ID.
        /// </summary>
        /// <value>Broker ID.</value>
        [DataMember(Name = "BrokerId")]
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets or sets the region set name.
        /// </summary>
        /// <value>Region set name.</value>
        [DataMember(Name = "Name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the region set's description.
        /// </summary>
        /// <value>Region set description.</value>
        [DataMember(Name = "Description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets a dictionary mapping Regions that are a part of this set by their Region ID.
        /// </summary>
        /// <value>A dictionary mapping Regions that are a part of this set by their Region ID.</value>
        /// <remarks>
        /// Went with a ReadOnlyDictionary to avoid having to deal with remaking the default region whenever
        /// the regions dictionary was changed. Adding/Removing dictionaries should only be happening in the UI anyway.
        /// </remarks>
        public ReadOnlyDictionary<int, Region> Regions
        {
            get
            {
                return new ReadOnlyDictionary<int, Region>(this.regions);
            }
        }

        /// <summary>
        /// Gets or sets an array of regions to use as a model in the UI.
        /// </summary>
        /// <value>Array of Regions.</value>
        [DataMember(Name = "Regions")]
        private Region[] RegionsForUiModel
        {
            get
            {
                return this.regions.Values.OrderBy(r => r.Name).ToArray();
            }

            set
            {
                this.regions = value.ToDictionary(r => r.Id);
            }
        }

        /// <summary>
        /// Gets an enumeration of all region sets for a single broker.
        /// </summary>
        /// <param name="brokerId">Broker Id.</param>
        /// <returns>An enumerable of region sets.</returns>
        public static Dictionary<int, RegionSet> GetAllRegionSets(Guid brokerId)
        {
            Dictionary<int, RegionSet> sets = new Dictionary<int, RegionSet>();

            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
            };

            IStoredProcedureDriver driver = GenericLocator<IStoredProcedureDriverFactory>.Factory.Create(TimeoutInSeconds.Thirty);

            using (var connection = DbAccessUtils.GetConnection(brokerId))
            using (var ds = driver.ExecuteDataSet(connection, null, StoredProcedureName.Create("RegionSet_GetAllRegionSetsByBrokerId").Value, parameters))
            {
                // Create region sets.
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    sets.Add((int)dr["RegionSetId"], new RegionSet(dr));
                }

                // Create regions.
                foreach (DataRow dr in ds.Tables[1].Rows)
                {
                    sets[(int)dr["RegionSetId"]].regions.Add((int)dr["RegionId"], new Region(dr));
                }

                // Add Counties to regions.
                foreach (DataRow dr in ds.Tables[2].Rows)
                {
                    sets[(int)dr["RegionSetId"]].regions[(int)dr["RegionId"]].FipsCountyCodes.Add((int)dr["FipsCountyCode"]);
                }

                // Add default regions.
                foreach (RegionSet set in sets.Values)
                {
                    Region defaultRegion = Region.GetDefaultRegion(set);
                    set.regions.Add(defaultRegion.Id, defaultRegion);
                }
            }

            return sets;
        }

        /// <summary>
        /// Updates/deletes the given collections of RegionSets.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <param name="updatedSets">Sets to update.</param>
        /// <param name="deletedSets">Sets to delete.</param>
        public static void UpdateSets(Guid brokerId, IEnumerable<RegionSet> updatedSets, IEnumerable<RegionSet> deletedSets)
        {
            using (DbConnection conn = DbAccessUtils.GetConnection(brokerId))
            {
                conn.OpenWithRetry();
                using (var trans = conn.BeginTransaction())
                {
                    try
                    {
                        foreach (RegionSet updatedSet in updatedSets)
                        {
                            updatedSet.Save(conn, trans);
                        }

                        foreach (RegionSet deletedSet in deletedSets)
                        {
                            deletedSet.Delete(conn, trans);
                        }

                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        Tools.LogError(e);
                        trans.Rollback();
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Checkes that this set's regions' fips lists are mutually exclusive.
        /// </summary>
        /// <returns>True if regions are mutually exclusive.</returns>
        public bool CheckRegionExclusivity()
        {
            Region[] regions = this.regions.Values.ToArray();
            for (int i = 0; i < regions.Length; i++)
            {
                if (regions[i].Id == Region.DefaultRegionId)
                {
                    continue; // Do not bother checking default region.
                }

                for (int j = i + 1; j < regions.Length; j++)
                {
                    if (regions[j].Id == Region.DefaultRegionId)
                    {
                        continue; // Do not bother checking default region.
                    }

                    if (regions[i].FipsCountyCodes.Intersect(regions[j].FipsCountyCodes).Any())
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Saves the region set to db.
        /// </summary>
        /// <param name="conn">SQL Connection.</param>
        /// <param name="trans">SQL Transaction.</param>
        public void Save(DbConnection conn, DbTransaction trans)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@Name", Tools.TruncateString(this.Name.Trim(), 50)),
                new SqlParameter("@Description", Tools.TruncateString(this.Description.Trim(), 100))
            };

            StoredProcedureName spName;

            if (this.Id < 0)
            {
                spName = StoredProcedureName.Create("RegionSet_CreateRegionSet").Value;
            }
            else
            {
                parameters.Add(new SqlParameter("@RegionSetId", this.Id));
                spName = StoredProcedureName.Create("RegionSet_UpdateRegionSetById").Value;
            }

            IStoredProcedureDriver driver = GenericLocator<IStoredProcedureDriverFactory>.Factory.Create(TimeoutInSeconds.Thirty);
            driver.ExecuteNonQuery(conn, trans, spName, parameters);
        }

        /// <summary>
        /// Deletes Region set and associated regions from DB.
        /// </summary>
        /// <param name="conn">SQL Connection.</param>
        /// <param name="trans">SQL Transaction.</param>
        public void Delete(DbConnection conn, DbTransaction trans)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@RegionSetId", this.Id)
            };

            IStoredProcedureDriver driver = GenericLocator<IStoredProcedureDriverFactory>.Factory.Create(TimeoutInSeconds.Thirty);
            driver.ExecuteNonQuery(conn, trans, StoredProcedureName.Create("RegionSet_Delete").Value, parameters);
        }
    }
}
