﻿namespace LendersOffice.ObjLib.Regions
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Runtime.Serialization;
    using Adapter;
    using DataAccess;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Class representing a user defined set of counties.
    /// </summary>
    [DataContract]
    public class Region
    {
        /// <summary>
        /// ID value that indicates that a set is a Default Region.
        /// </summary>
        public const int DefaultRegionId = int.MinValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="Region" /> class.
        /// </summary>
        /// <param name="row">RegionSet data retrieved from SQL.</param>
        internal Region(DataRow row)
        {
            this.Id = (int)row["RegionId"];
            this.RegionSetId = (int)row["RegionSetId"];
            this.BrokerId = (Guid)row["BrokerId"];
            this.Name = (string)row["Name"];
            this.Description = (string)row["Description"];
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Region" /> class.
        /// Used for JSON deserialization.
        /// </summary>
        protected Region()
        {
        }

        /// <summary>
        /// Gets the region ID.
        /// </summary>
        /// <value>Region ID.</value>
        [DataMember(Name = "Id")]
        public int Id { get; private set; }

        /// <summary>
        /// Gets the ID of the RegionSet this Region belongs to.
        /// </summary>
        /// <value>Region Set ID.</value>
        [DataMember(Name = "RegionSetId")]
        public int RegionSetId { get; private set; }

        /// <summary>
        /// Gets the region's Broker ID.
        /// </summary>
        /// <value>Broker ID.</value>
        [DataMember(Name = "BrokerId")]
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets or sets the region name.
        /// </summary>
        /// <value>Region name.</value>
        [DataMember(Name = "Name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the region's description.
        /// </summary>
        /// <value>Region description.</value>
        [DataMember(Name = "Description")]
        public string Description { get; set; }

        /// <summary>
        /// Gets the set of counties (by FIPS code) that make up this region.
        /// </summary>
        /// <value>Set of FIPS county codes.</value>
        [DataMember(Name = "FipsCountyCodes")]
        public HashSet<int> FipsCountyCodes { get; private set; } = new HashSet<int>();

        /// <summary>
        /// Deletes Region and associated data from DB.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <param name="regionId">ID of Region to delete.</param>
        public static void DeleteById(Guid brokerId, int regionId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@RegionId", regionId)
            };

            IStoredProcedureDriver driver = GenericLocator<IStoredProcedureDriverFactory>.Factory.Create(TimeoutInSeconds.Thirty);

            using (DbConnection conn = DbAccessUtils.GetConnection(brokerId))
            {
                conn.OpenWithRetry();
                using (var trans = conn.BeginTransaction())
                {
                    try
                    {
                        driver.ExecuteNonQuery(conn, trans, StoredProcedureName.Create("Region_Delete").Value, parameters);
                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        Tools.LogError(e);
                        trans.Rollback();
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Saves the region set to db.
        /// </summary>
        public void Save()
        {
            using (DbConnection conn = DbAccessUtils.GetConnection(this.BrokerId))
            {
                conn.OpenWithRetry();
                using (var trans = conn.BeginTransaction())
                {
                    try
                    {
                        this.SaveImpl(conn, trans);
                        trans.Commit();
                    }
                    catch (Exception e)
                    {
                        Tools.LogError(e);
                        trans.Rollback();
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Creates a default region from the existing regions in the given set.
        /// </summary>
        /// <param name="regionSet">The default set this region is for.</param>
        /// <returns>The default region for a provided set.</returns>
        internal static Region GetDefaultRegion(RegionSet regionSet)
        {
            HashSet<int> allCountiesThisRegion = new HashSet<int>(regionSet.Regions.Select(kvp => kvp.Value).SelectMany(r => r.FipsCountyCodes));

            HashSet<int> defaultCounties = new HashSet<int>(StateInfo.Instance.StatesCountiesAndFipsMapExcludingArmy.Select(kvp => kvp.Value).SelectMany(sd => sd.Values));
            defaultCounties.ExceptWith(allCountiesThisRegion);

            Region defaultRegion = new Region();
            defaultRegion.Id = Region.DefaultRegionId;
            defaultRegion.RegionSetId = regionSet.Id;
            defaultRegion.BrokerId = regionSet.BrokerId;
            defaultRegion.Name = "Default Region";
            defaultRegion.Description = "This region contains all counties not included in another region";
            defaultRegion.FipsCountyCodes = defaultCounties;

            return defaultRegion;
        }

        /// <summary>
        /// Saves the region set to db.
        /// </summary>
        /// <param name="conn">SQL Connection.</param>
        /// <param name="trans">SQL Transaction.</param>
        private void SaveImpl(DbConnection conn, DbTransaction trans)
        {
            IStoredProcedureDriver driver = GenericLocator<IStoredProcedureDriverFactory>.Factory.Create(TimeoutInSeconds.Thirty);

            // Save region info.
            SqlParameter regionIdOut = new SqlParameter("@RegionId", SqlDbType.Int);
            regionIdOut.Direction = ParameterDirection.Output;

            List<SqlParameter> regionParameters = new List<SqlParameter>()
            {
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@Name", Tools.TruncateString(this.Name.Trim(), 50)),
                new SqlParameter("@Description", Tools.TruncateString(this.Description.Trim(), 100))
            };

            StoredProcedureName spName;

            if (this.Id < 0)
            {
                regionParameters.Add(new SqlParameter("@RegionSetId", this.RegionSetId));
                regionParameters.Add(regionIdOut);
                spName = StoredProcedureName.Create("Region_CreateRegion").Value;
            }
            else
            {
                regionParameters.Add(new SqlParameter("@RegionId", this.Id));
                spName = StoredProcedureName.Create("Region_UpdateRegionById").Value;
            }

            driver.ExecuteNonQuery(conn, trans, spName, regionParameters);

            if (this.Id < 0)
            {
                this.Id = (int)regionIdOut.Value;
            }

            // Delete old FIPS list.
            SqlParameter[] deleteFipsParameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@RegionId", this.Id)
            };

            driver.ExecuteNonQuery(conn, trans, StoredProcedureName.Create("RegionXFips_DeleteByRegionId").Value, deleteFipsParameters);

            // Save new FIPS list.
            StoredProcedureName addFipsSpName = StoredProcedureName.Create("RegionXFips_Create").Value;
            foreach (int fips in this.FipsCountyCodes)
            {
                SqlParameter[] addFipsParameters = new SqlParameter[]
                {
                    new SqlParameter("@BrokerId", this.BrokerId),
                    new SqlParameter("@RegionSetId", this.RegionSetId),
                    new SqlParameter("@RegionId", this.Id),
                    new SqlParameter("@FipsCountyCode", fips)
                };

                driver.ExecuteNonQuery(conn, trans, addFipsSpName, addFipsParameters);
            }
        }
    }
}
