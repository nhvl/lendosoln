﻿// <copyright file="ToleranceCureDebug.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Jhairo Erazo
//    Date:   05/23/2016
// </summary>
namespace LendersOffice.ObjLib.ToleranceCureDebug
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;

    /// <summary>
    /// Tolerance Cure Debug Data.
    /// </summary>
    public class ToleranceCureDebug
    {
        /// <summary>
        /// Shared LosConvert object.
        /// </summary>
        private readonly LosConvert convert;

        /// <summary>
        /// Initializes a new instance of the <see cref="ToleranceCureDebug" /> class.
        /// </summary>
        /// <param name="dataLoan">The loan data.</param>
        /// <param name="loanEstimateArchive">The loan estimate archive to use in calculating the cure amount.</param>
        /// <param name="tenPercentBasisArchive">The loan estimate archive to use as a basis in calculating the 10% cure amount.</param>
        /// <param name="checkLock">For Zero and Ten Percent cure amounts, if respective amount is locked in loan, mark as locked and set cure amount to loan value.</param>
        public ToleranceCureDebug(CPageBase dataLoan, ClosingCostArchive loanEstimateArchive, ClosingCostArchive tenPercentBasisArchive, bool checkLock)
        {
            this.convert = dataLoan.m_convertLos;

            // Build cure debug data.
            this.ZeroToleranceCure = new ToleranceZeroPercentCureDebug(dataLoan, loanEstimateArchive, checkLock);
            this.TenPercentCumulativeToleranceCure = new ToleranceTenPercentCureDebug(dataLoan, tenPercentBasisArchive, checkLock);
            this.UnlimitedToleranceFees = new UnlimitedToleranceDebug(dataLoan, loanEstimateArchive);

            // Calculate total tolerance cure.
            this.ToleranceCure = Tools.SumMoney(new decimal[] { this.ZeroToleranceCure.CureAmount, this.TenPercentCumulativeToleranceCure.CureAmount });
        }

        /// <summary>
        /// Gets the zero tolerance cure debug data.
        /// </summary>
        /// <value>The zero tolerance cure debug data.</value>
        public ToleranceZeroPercentCureDebug ZeroToleranceCure { get; private set; }

        /// <summary>
        /// Gets the ten tolerance cure debug data.
        /// </summary>
        /// <value>The ten tolerance cure debug data.</value>
        public ToleranceTenPercentCureDebug TenPercentCumulativeToleranceCure { get; private set; }

        /// <summary>
        /// Gets the zero tolerance cure debug data.
        /// </summary>
        /// <value>The zero tolerance cure debug data.</value>
        public UnlimitedToleranceDebug UnlimitedToleranceFees { get; private set; }

        /// <summary>
        /// Gets the calculated tolerance cure amount.
        /// </summary>
        /// <value>The calculated tolerance cure amount.</value>
        public decimal ToleranceCure { get; private set; }

        /// <summary>
        /// Gets money string value of field.
        /// </summary>
        /// <value>Money string value of field.</value>
        public string ToleranceCure_rep
        {
            get { return this.convert.ToMoneyString(this.ToleranceCure, FormatDirection.ToRep); }
        }
    }
}
