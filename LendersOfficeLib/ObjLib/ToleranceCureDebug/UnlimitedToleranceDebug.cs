﻿// <copyright file="UnlimitedToleranceDebug.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Jhairo Erazo
//    Date:   05/24/2016
// </summary>
namespace LendersOffice.ObjLib.ToleranceCureDebug
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using DataAccess;
    using DataAccess.GFE;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;

    /// <summary>
    /// Tolerance cure debug data for the Tolerance Zero Percent Cure.
    /// </summary>
    public class UnlimitedToleranceDebug
    {
        /// <summary>
        /// List of fees that contribute in calculating tolerance cure amount.
        /// </summary>
        private List<ToleranceCureDebugFee> fees;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnlimitedToleranceDebug" /> class.
        /// </summary>
        /// <param name="dataLoan">The loan data.</param>
        /// <param name="loanEstimateArchive">The loan estimate archive to use in calculating the cure amount.</param>
        public UnlimitedToleranceDebug(CPageBase dataLoan, ClosingCostArchive loanEstimateArchive)
        {
            // If no last disclosed archive exists, then return. Fees list is blank.
            if (loanEstimateArchive == null)
            {
                this.fees = new List<ToleranceCureDebugFee>();
                return;
            }

            this.ArchiveId = loanEstimateArchive.Id;
            this.ArchiveDate = loanEstimateArchive.DateArchived;

            this.CreateDebugFees(dataLoan, loanEstimateArchive);
        }

        /// <summary>
        /// Gets the ID of archive used to calculate the tolerance cure data.
        /// </summary>
        /// <value>Archive ID.</value>
        public Guid ArchiveId { get; private set; }

        /// <summary>
        /// Gets the date of the archive used to calculate the tolerance cure data.
        /// </summary>
        /// <value>Date as string.</value>
        public string ArchiveDate { get; private set; }

        /// <summary>
        /// Gets an enumerable of fees that contribute in calculating tolerance cure amount.
        /// </summary>
        /// <value>IEnumerable of fees that contribute in calculating tolerance cure amount.</value>
        public IEnumerable<ToleranceCureDebugFee> Fees
        {
            get { return this.fees; }
        }

        /// <summary>
        /// Creates debug fee objects from the loans closing cost set and last disclosed archive.
        /// </summary>
        /// <param name="dataLoan">The loan data.</param>
        /// <param name="lastDisclosedArchive">The last disclosed closing cost archive on the loan.</param>
        public void CreateDebugFees(CPageBase dataLoan, ClosingCostArchive lastDisclosedArchive)
        {
            // Live Closing Disclosure (ie. current fees).
            BorrowerClosingCostSet liveCCSet = dataLoan.sClosingCostSet;
            Func<BaseClosingCostFee, bool> liveFilter = bFee => ClosingCostSetUtils.OrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)bFee, dataLoan.sOriginatorCompensationPaymentSourceT, dataLoan.sDisclosureRegulationT, dataLoan.sGfeIsTPOTransaction);

            // Last Disclosed Loan Estimate Archive.
            BorrowerClosingCostSet lastDisclosedArchiveSet = new BorrowerClosingCostSet(lastDisclosedArchive);
            Func<BaseClosingCostFee, bool> lastDisclosedArchiveFilter = archivedFee => ClosingCostSetUtils.OrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)archivedFee, lastDisclosedArchiveSet.ClosingCostArchive);

            // Get applicable fees.
            var liveUnlimitedToleranceFees = (from fee in liveCCSet.GetFees(liveFilter).Cast<BorrowerClosingCostFee>()
                                              where GfeToleranceViolationCalculator.ConsiderFeeForUnlimitedToleranceInTRIDMode(fee)
                                              select (BorrowerClosingCostFee)fee.Clone()).ToList();

            var archivedUnlimitedToleranceFees = (from fee in lastDisclosedArchiveSet.GetFees(lastDisclosedArchiveFilter).Cast<BorrowerClosingCostFee>()
                                                  where liveUnlimitedToleranceFees.Find(p => p.ClosingCostFeeTypeId.Equals(fee.ClosingCostFeeTypeId)) != null
                                                  select (BorrowerClosingCostFee)fee.Clone()).ToList();

            // Create debug fees.
            this.fees = ToleranceCureDebugUtilities.CreateDebugFeeList(dataLoan.m_convertLos, archivedUnlimitedToleranceFees, liveUnlimitedToleranceFees);
        }
    }
}
