﻿// <copyright file="ToleranceZeroPercentCureDebug.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Jhairo Erazo
//    Date:   05/23/2016
// </summary>
namespace LendersOffice.ObjLib.ToleranceCureDebug
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using DataAccess;
    using DataAccess.GFE;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;

    /// <summary>
    /// Tolerance cure debug data for the Tolerance Zero Percent Cure.
    /// </summary>
    public class ToleranceZeroPercentCureDebug
    {
        /// <summary>
        /// Shared LosConvert object.
        /// </summary>
        private readonly LosConvert convert;

        /// <summary>
        /// List of fees that contribute in calculating tolerance cure amount.
        /// </summary>
        private List<ToleranceCureDebugFee> fees;

        /// <summary>
        /// Initializes a new instance of the <see cref="ToleranceZeroPercentCureDebug" /> class.
        /// </summary>
        /// <param name="dataLoan">The loan data.</param>
        /// <param name="loanEstimateArchive">The loan estimate archive to use in calculating the cure amount.</param>
        /// <param name="checkLock">If cure amount as locked in loan, mark as locked and set cure amount to loan value.</param>
        public ToleranceZeroPercentCureDebug(CPageBase dataLoan, ClosingCostArchive loanEstimateArchive, bool checkLock)
        {
            this.convert = dataLoan.m_convertLos;
            this.IsCureAmountLckd = checkLock && dataLoan.sToleranceZeroPercentCureLckd;

            // If no last disclosed archive exists, then return. Fees list is blank.
            if (loanEstimateArchive == null)
            {
                this.fees = new List<ToleranceCureDebugFee>();
                this.CalculateCure(dataLoan);   // Returns 0 or locked amount.
                return;
            }

            this.ArchiveId = loanEstimateArchive.Id;
            this.ArchiveDate = loanEstimateArchive.DateArchived;

            this.SetLenderCreditDummyFee(dataLoan, loanEstimateArchive);
            this.CreateDebugFees(dataLoan, loanEstimateArchive);
            this.CalculateCure(dataLoan); 
        }

        /// <summary>
        /// Gets a value indicating whether the cure amount is locked.
        /// </summary>
        /// <value>Is the cure amount locked.</value>
        public bool IsCureAmountLckd { get; private set; }

        /// <summary>
        /// Gets the zero tolerance cure amount.
        /// </summary>
        /// <value>The zero tolerance cure amount.</value>
        public decimal CureAmount { get; private set; }

        /// <summary>
        /// Gets money string value of field.
        /// </summary>
        /// <value>Money string value of field.</value>
        public string CureAmount_rep
        {
            get { return this.convert.ToMoneyString(this.CureAmount, FormatDirection.ToRep); }
        }

        /// <summary>
        /// Gets a value indicating whether the loan has a lender credit applied to it.
        /// </summary>
        /// <value>True, if the loan has a lender credit applied to it.</value>
        public bool HasLenderCredit { get; private set; }

        /// <summary>
        /// Gets the lender credit amount from the archive.
        /// </summary>
        /// <value>Lender credit amount from archive.</value>
        public decimal LenderCreditArchiveAmount { get; private set; }

        /// <summary>
        /// Gets money string value of field.
        /// </summary>
        /// <value>Money string value of field.</value>
        public string LenderCreditArchiveAmount_rep
        {
            get { return this.convert.ToMoneyString(this.LenderCreditArchiveAmount, FormatDirection.ToRep); }
        }

        /// <summary>
        /// Gets the lender credit amount from the live loan data.
        /// </summary>
        /// <value>Lender credit amount from live loan data.</value>
        public decimal LenderCreditLiveAmount { get; private set; }

        /// <summary>
        /// Gets money string value of field.
        /// </summary>
        /// <value>Money string value of field.</value>
        public string LenderCreditLiveAmount_rep
        {
            get { return this.convert.ToMoneyString(this.LenderCreditLiveAmount, FormatDirection.ToRep); }
        }

        /// <summary>
        /// Gets the difference between the lender credit live amount and lender credit archive amount.
        /// </summary>
        /// <value>Calculated difference between lender creadit fees.</value>
        public decimal LenderCreditDifferenceAmount { get; private set; }

        /// <summary>
        /// Gets money string value of field.
        /// </summary>
        /// <value>Money string value of field.</value>
        public string LenderCreditDifferenceAmount_rep
        {
            get { return this.convert.ToMoneyString(this.LenderCreditDifferenceAmount, FormatDirection.ToRep); }
        }

        /// <summary>
        /// Gets the tolerance cure amount for lender credit.
        /// </summary>
        /// <value>Calculated tolerance cure amount for lender credit.</value>
        public decimal LenderCreditCureAmount { get; private set; }

        /// <summary>
        /// Gets money string value of field.
        /// </summary>
        /// <value>Money string value of field.</value>
        public string LenderCreditCureAmount_rep
        {
            get { return this.convert.ToMoneyString(this.LenderCreditCureAmount, FormatDirection.ToRep); }
        }

        /// <summary>
        /// Gets the ID of archive used to calculate the tolerance cure data.
        /// </summary>
        /// <value>Archive ID.</value>
        public Guid ArchiveId { get; private set; }

        /// <summary>
        /// Gets the date of the archive used to calculate the tolerance cure data.
        /// </summary>
        /// <value>Date as string.</value>
        public string ArchiveDate { get; private set; }

        /// <summary>
        /// Gets an enumerable of fees that contribute in calculating tolerance cure amount.
        /// </summary>
        /// <value>IEnumerable of fees that contribute in calculating tolerance cure amount.</value>
        public IEnumerable<ToleranceCureDebugFee> Fees
        {
            get { return this.fees; }
        }

        /// <summary>
        /// Set lender credit dummy fee values.
        /// </summary>
        /// <param name="dataLoan">The loan data.</param>
        /// <param name="lastDisclosedArchive">The last disclosed closing cost archive on the loan.</param>
        public void SetLenderCreditDummyFee(CPageBase dataLoan, ClosingCostArchive lastDisclosedArchive)
        {
            decimal archivedCreditAsCharge = -1 * lastDisclosedArchive.GetMoneyValue("sTRIDLoanEstimateLenderCredits");
            if (archivedCreditAsCharge != 0M || dataLoan.sTRIDLoanEstimateLenderCredits_Neg != 0M)
            {
                this.LenderCreditArchiveAmount = archivedCreditAsCharge;
                this.LenderCreditLiveAmount = dataLoan.sTRIDLoanEstimateLenderCredits_Neg;
                this.LenderCreditDifferenceAmount = Tools.SumMoney(new decimal[] { this.LenderCreditLiveAmount, -1 * this.LenderCreditArchiveAmount });
                this.LenderCreditCureAmount = Math.Max(0, this.LenderCreditDifferenceAmount);
                this.HasLenderCredit = true;
            }
        }

        /// <summary>
        /// Creates debug fee objects from the loans closing cost set and last disclosed archive.
        /// </summary>
        /// <param name="dataLoan">The loan data.</param>
        /// <param name="lastDisclosedArchive">The last disclosed closing cost archive on the loan.</param>
        public void CreateDebugFees(CPageBase dataLoan, ClosingCostArchive lastDisclosedArchive)
        {
            // Live Closing Disclosure (ie. current fees).
            BorrowerClosingCostSet liveCCSet = dataLoan.sClosingCostSet;
            Func<BaseClosingCostFee, bool> liveFilter = bFee => ClosingCostSetUtils.OrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)bFee, dataLoan.sOriginatorCompensationPaymentSourceT, dataLoan.sDisclosureRegulationT, dataLoan.sGfeIsTPOTransaction);

            // Last Disclosed Loan Estimate Archive.
            BorrowerClosingCostSet lastDisclosedArchiveSet = new BorrowerClosingCostSet(lastDisclosedArchive);
            Func<BaseClosingCostFee, bool> lastDisclosedArchiveFilter = archivedFee => ClosingCostSetUtils.OrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)archivedFee, lastDisclosedArchiveSet.ClosingCostArchive);

            // Get applicable fees.
            var liveZeroToleranceFees = (from fee in liveCCSet.GetFees(liveFilter).Cast<BorrowerClosingCostFee>()
                                         where GfeToleranceViolationCalculator.ConsiderFeeForZeroToleranceInTRIDMode(fee)
                                         select (BorrowerClosingCostFee)fee.Clone()).ToList();

            var archivedZeroToleranceFees = (from fee in lastDisclosedArchiveSet.GetFees(lastDisclosedArchiveFilter).Cast<BorrowerClosingCostFee>()
                                             where liveZeroToleranceFees.Find(p => p.ClosingCostFeeTypeId.Equals(fee.ClosingCostFeeTypeId)) != null
                                             select (BorrowerClosingCostFee)fee.Clone()).ToList();

            // Create debug fees.
            this.fees = ToleranceCureDebugUtilities.CreateDebugFeeList(this.convert, archivedZeroToleranceFees, liveZeroToleranceFees);
        }

        /// <summary>
        /// Calculates the zero percent tolerance cure using gathered data.
        /// </summary>
        /// <param name="dataLoan">The loan data.</param>
        public void CalculateCure(CPageBase dataLoan)
        {
            if (this.IsCureAmountLckd)
            {
                this.CureAmount = dataLoan.sToleranceZeroPercentCure;
                return;
            }

            this.CureAmount = Tools.SumMoney(this.Fees.Select(f => f.CureAmount));

            if (this.HasLenderCredit)
            {
                this.CureAmount = Tools.SumMoney(new[] { this.CureAmount, this.LenderCreditCureAmount });
            }
        }
    }
}
