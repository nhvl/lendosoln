﻿// <copyright file="ToleranceCureDebugUtilities.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Jhairo Erazo
//    Date:   05/24/2016
// </summary>
namespace LendersOffice.ObjLib.ToleranceCureDebug
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using DataAccess;

    /// <summary>
    /// Utilities class for ToleranceCureDebug data.
    /// </summary>
    public static class ToleranceCureDebugUtilities
    {
        /// <summary>
        /// Creates the list of ToleranceCureDebugFee items used to calculate cure amount.
        /// </summary>
        /// <param name="convert">LosConvert object.</param>
        /// <param name="archiveToleranceFees">Fees from archive data.</param>
        /// <param name="liveToleranceFees">Fees from live loan data.</param>
        /// <returns>List of debug fees.</returns>
        public static List<ToleranceCureDebugFee> CreateDebugFeeList(
            LosConvert convert,
            List<BorrowerClosingCostFee> archiveToleranceFees,
            List<BorrowerClosingCostFee> liveToleranceFees)
        {
            Dictionary<Guid, ToleranceCureDebugFee> feeCompares = new Dictionary<Guid, ToleranceCureDebugFee>();

            // Add Archive fees.
            foreach (BorrowerClosingCostFee fee in archiveToleranceFees)
            {
                feeCompares.Add(fee.ClosingCostFeeTypeId, new ToleranceCureDebugFee(convert, fee, null));
            }

            // Add Live Closing Disclosure fees.
            foreach (BorrowerClosingCostFee fee in liveToleranceFees)
            {
                // Move section C Fees that weren't shopped for to section B.
                if (fee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionC && !fee.DidShop)
                {
                    fee.IntegratedDisclosureSectionT = E_IntegratedDisclosureSectionT.SectionB;
                }

                if (feeCompares.ContainsKey(fee.ClosingCostFeeTypeId))
                {
                    feeCompares[fee.ClosingCostFeeTypeId].LiveFee = fee;
                }
                else
                {
                    feeCompares.Add(fee.ClosingCostFeeTypeId, new ToleranceCureDebugFee(convert, null, fee));
                }
            }

            List<ToleranceCureDebugFee> ret = feeCompares.Values.ToList();
            ret.Sort();
            return ret;
        }
    }
}
