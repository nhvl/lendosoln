﻿// <copyright file="ToleranceTenPercentCureDebug.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Jhairo Erazo
//    Date:   05/23/2016
// </summary>
namespace LendersOffice.ObjLib.ToleranceCureDebug
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using DataAccess;
    using DataAccess.GFE;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;

    /// <summary>
    /// Tolerance cure debug data for the Tolerance Ten Percent Cure.
    /// </summary>
    public class ToleranceTenPercentCureDebug
    {
        /// <summary>
        /// Shared LosConvert object.
        /// </summary>
        private readonly LosConvert convert;

        /// <summary>
        /// List of recording fees that contribute in calculating tolerance cure amount.
        /// </summary>
        private List<ToleranceCureDebugFee> recordingFees;

        /// <summary>
        /// List of non recording fees that contribute in calculating tolerance cure amount.
        /// </summary>
        private List<ToleranceCureDebugFee> nonRecordingFees;

        /// <summary>
        /// Initializes a new instance of the <see cref="ToleranceTenPercentCureDebug" /> class.
        /// </summary>
        /// <param name="dataLoan">The loan data.</param>
        /// <param name="tenPercentBasisArchive">The loan estimate archive to use as a basis in calculating the 10% cure amount.</param>
        /// <param name="checkLock">If cure amount as locked in loan, mark as locked and set cure amount to loan value.</param>
        public ToleranceTenPercentCureDebug(CPageBase dataLoan, ClosingCostArchive tenPercentBasisArchive, bool checkLock)
        {
            this.convert = dataLoan.m_convertLos;
            this.IsCureAmountLckd = checkLock && dataLoan.sToleranceTenPercentCureLckd;
            
            // If no last disclosed archive exists, then return. Fees list is blank.
            if (tenPercentBasisArchive == null)
            {
                this.recordingFees = new List<ToleranceCureDebugFee>();
                this.nonRecordingFees = new List<ToleranceCureDebugFee>();

                this.CalculateSums();
                this.CalculateCure(dataLoan);   // Returns 0 or locked amount.

                return;
            }

            this.ArchiveId = tenPercentBasisArchive.Id;
            this.ArchiveDate = tenPercentBasisArchive.DateArchived;

            this.CreateDebugRecordingFees(dataLoan, tenPercentBasisArchive);
            this.CreateDebugNonRecordingFees(dataLoan, tenPercentBasisArchive);
            this.CalculateSums();
            this.CalculateCure(dataLoan);
        }

        /// <summary>
        /// Gets a value indicating whether the ten percent tolerance calculation includes recording fees.
        /// </summary>
        /// <value>True, if ten percent tolerance calculation includes recording fees.</value>
        public bool AreRecordingFeesIncluded { get; private set; }

        /// <summary>
        /// Gets the sum of recording fee amounts from the archive.
        /// </summary>
        /// <value>Sum of recording fee amounts from archive.</value>
        public decimal RecordingFeeArchiveAmount { get; private set; }

        /// <summary>
        /// Gets money string value of field.
        /// </summary>
        /// <value>Money string value of field.</value>
        public string RecordingFeeArchiveAmount_rep
        {
            get { return this.convert.ToMoneyString(this.RecordingFeeArchiveAmount, FormatDirection.ToRep); }
        }

        /// <summary>
        /// Gets the sum of recording fee amounts from the live loan data.
        /// </summary>
        /// <value>Sum of recording fee amounts from live loan data.</value>
        public decimal RecordingFeeLiveAmount { get; private set; }

        /// <summary>
        /// Gets money string value of field.
        /// </summary>
        /// <value>Money string value of field.</value>
        public string RecordingFeeLiveAmount_rep
        {
            get { return this.convert.ToMoneyString(this.RecordingFeeLiveAmount, FormatDirection.ToRep); }
        }

        /// <summary>
        /// Gets the difference between the recording fee live amount and recording fee archive amount.
        /// </summary>
        /// <value>Calculated difference between recording fees.</value>
        public decimal RecordingFeeDifferenceAmount { get; private set; }

        /// <summary>
        /// Gets money string value of field.
        /// </summary>
        /// <value>Money string value of field.</value>
        public string RecordingFeeDifferenceAmount_rep
        {
            get { return this.convert.ToMoneyString(this.RecordingFeeDifferenceAmount, FormatDirection.ToRep); }
        }

        /// <summary>
        /// Gets the sum of ten tolerance fee amounts from the archive.
        /// </summary>
        /// <value>Sum of ten tolerance fee amounts from archive.</value>
        public decimal ArchiveFeesSumAmount { get; private set; }

        /// <summary>
        /// Gets money string value of field.
        /// </summary>
        /// <value>Money string value of field.</value>
        public string ArchiveFeesSumAmount_rep
        {
            get { return this.convert.ToMoneyString(this.ArchiveFeesSumAmount, FormatDirection.ToRep); }
        }

        /// <summary>
        /// Gets the sum of ten tolerance fee amounts from the live loan data.
        /// </summary>
        /// <value>Sum of ten tolerance fee amounts from live loan data.</value>
        public decimal LiveFeesSumAmount { get; private set; }

        /// <summary>
        /// Gets money string value of field.
        /// </summary>
        /// <value>Money string value of field.</value>
        public string LiveFeesSumAmount_rep
        {
            get { return this.convert.ToMoneyString(this.LiveFeesSumAmount, FormatDirection.ToRep); }
        }

        /// <summary>
        /// Gets the difference between the ten tolerance live amount sum and ten tolerance archive amount sum.
        /// </summary>
        /// <value>Calculated difference between recording fees.</value>
        public decimal FeesSumDifferenceAmount { get; private set; }

        /// <summary>
        /// Gets money string value of field.
        /// </summary>
        /// <value>Money string value of field.</value>
        public string FeesSumDifferenceAmount_rep
        {
            get { return this.convert.ToMoneyString(this.FeesSumDifferenceAmount, FormatDirection.ToRep); }
        }

        /// <summary>
        /// Gets 10% of the sum of ten tolerance fee amounts from the archive.
        /// </summary>
        /// <value>10% of the sum of ten tolerance fee amounts from archive.</value>
        public decimal TenPercentArchiveFeesSumAmount
        {
            get
            {
                return .1M * this.ArchiveFeesSumAmount;
            }
        }

        /// <summary>
        /// Gets money string value of field.
        /// </summary>
        /// <value>Money string value of field.</value>
        public string TenPercentArchiveFeesSumAmount_rep
        {
            get { return this.convert.ToMoneyString(this.TenPercentArchiveFeesSumAmount, FormatDirection.ToRep); }
        }

        /// <summary>
        /// Gets a value indicating whether the cure amount is locked.
        /// </summary>
        /// <value>Is the cure amount locked.</value>
        public bool IsCureAmountLckd { get; private set; }

        /// <summary>
        /// Gets the ten tolerance cure amount.
        /// </summary>
        /// <value>The ten tolerance cure amount.</value>
        public decimal CureAmount { get; private set; }

        /// <summary>
        /// Gets money string value of field.
        /// </summary>
        /// <value>Money string value of field.</value>
        public string CureAmount_rep
        {
            get { return this.convert.ToMoneyString(this.CureAmount, FormatDirection.ToRep); }
        }

        /// <summary>
        /// Gets the ten tolerance cure amount as calculated from debug data. Does not take lock status into account.
        /// </summary>
        /// <value>The ten tolerance cure amount.</value>
        public decimal CalculatedCureAmount { get; private set; }

        /// <summary>
        /// Gets money string value of field.
        /// </summary>
        /// <value>Money string value of field.</value>
        public string CalculatedCureAmount_rep
        {
            get { return this.convert.ToMoneyString(this.CalculatedCureAmount, FormatDirection.ToRep); }
        }

        /// <summary>
        /// Gets the ID of archive used to calculate the tolerance cure data.
        /// </summary>
        /// <value>Archive ID.</value>
        public Guid ArchiveId { get; private set; }

        /// <summary>
        /// Gets the date of the archive used to calculate the tolerance cure data.
        /// </summary>
        /// <value>Date as string.</value>
        public string ArchiveDate { get; private set; }

        /// <summary>
        /// Gets an enumerable of recording fees that contribute in calculating tolerance cure amount.
        /// </summary>
        /// <value>IEnumerable of recording fees that contribute in calculating tolerance cure amount.</value>
        public IEnumerable<ToleranceCureDebugFee> RecordingFees
        {
            get { return this.recordingFees; }
        }

        /// <summary>
        /// Gets an enumerable of non-recording fees that contribute in calculating tolerance cure amount.
        /// </summary>
        /// <value>IEnumerable of non-recording fees that contribute in calculating tolerance cure amount.</value>
        public IEnumerable<ToleranceCureDebugFee> NonRecordingFees
        {
            get { return this.nonRecordingFees; }
        }

        /// <summary>
        /// Creates debug fee objects for recording fees from the loans closing cost set and last disclosed archive.
        /// </summary>
        /// <param name="dataLoan">The loan data.</param>
        /// <param name="tenPercentArchive">The last disclosed closing cost archive on the loan.</param>
        public void CreateDebugRecordingFees(CPageBase dataLoan, ClosingCostArchive tenPercentArchive)
        {
            // Live Closing Disclosure (ie. current fees).
            BorrowerClosingCostSet liveCCSet = dataLoan.sClosingCostSet;
            Func<BaseClosingCostFee, bool> liveFilter = bFee => ClosingCostSetUtils.OrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)bFee, dataLoan.sOriginatorCompensationPaymentSourceT, dataLoan.sDisclosureRegulationT, dataLoan.sGfeIsTPOTransaction);

            // Ten Tolerance Basis Archive.
            BorrowerClosingCostSet tenPercentArchiveSet = tenPercentArchive.GetClosingCostSet();
            Func<BaseClosingCostFee, bool> tenPercentArchiveFilter = tenPercFee => ClosingCostSetUtils.OrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)tenPercFee, tenPercentArchiveSet.ClosingCostArchive);

            var liveTenToleranceRecordingFees = (from fee in liveCCSet.GetFees(liveFilter).Cast<BorrowerClosingCostFee>()
                                                 where GfeToleranceViolationCalculator.ConsiderFeeForTenToleranceInTRIDMode(fee) &&
                                                       GfeToleranceViolationCalculator.RecordingFeeTypes.Contains(fee.MismoFeeT)
                                                 select (BorrowerClosingCostFee)fee.Clone()).ToList();

            var archivedTenToleranceRecordingFees = (from fee in tenPercentArchiveSet.GetFees(tenPercentArchiveFilter).Cast<BorrowerClosingCostFee>()
                                                     where GfeToleranceViolationCalculator.ConsiderFeeForTenToleranceInTRIDMode(fee) &&
                                                           GfeToleranceViolationCalculator.RecordingFeeTypes.Contains(fee.MismoFeeT)
                                                     select (BorrowerClosingCostFee)fee.Clone()).ToList();

            decimal liveRecFeesSum = Tools.SumMoney(liveTenToleranceRecordingFees.Select(f => f.TotalAmount));

            // Only count recording fee in 10% tolerance if total of all live fees is not 0.
            if (liveRecFeesSum != 0)
            {
                this.RecordingFeeLiveAmount = liveRecFeesSum;
                this.RecordingFeeArchiveAmount = Tools.SumMoney(archivedTenToleranceRecordingFees.Select(f => f.TotalAmount));
                this.RecordingFeeDifferenceAmount = Tools.SumMoney(new decimal[] { this.RecordingFeeLiveAmount, -1 * this.RecordingFeeArchiveAmount });
                this.AreRecordingFeesIncluded = true;

                this.recordingFees = ToleranceCureDebugUtilities.CreateDebugFeeList(this.convert, archivedTenToleranceRecordingFees, liveTenToleranceRecordingFees);
            }
        }

        /// <summary>
        /// Creates debug fee objects for non-recording fees from the loans closing cost set and last disclosed archive.
        /// </summary>
        /// <param name="dataLoan">The loan data.</param>
        /// <param name="tenPercentArchive">The last disclosed closing cost archive on the loan.</param>
        public void CreateDebugNonRecordingFees(CPageBase dataLoan, ClosingCostArchive tenPercentArchive)
        {
            // Live Closing Disclosure (ie. current fees).
            BorrowerClosingCostSet liveCCSet = dataLoan.sClosingCostSet;
            Func<BaseClosingCostFee, bool> liveFilter = bFee => ClosingCostSetUtils.OrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)bFee, dataLoan.sOriginatorCompensationPaymentSourceT, dataLoan.sDisclosureRegulationT, dataLoan.sGfeIsTPOTransaction);

            // Ten Tolerance Basis Archive.
            BorrowerClosingCostSet tenPercentArchiveSet = tenPercentArchive.GetClosingCostSet();
            Func<BaseClosingCostFee, bool> tenPercentArchiveFilter = tenPercFee => ClosingCostSetUtils.OrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)tenPercFee, tenPercentArchiveSet.ClosingCostArchive);

            var liveTenToleranceNonRecordingFees = (from fee in liveCCSet.GetFees(liveFilter).Cast<BorrowerClosingCostFee>()
                                                    where GfeToleranceViolationCalculator.ConsiderFeeForTenToleranceInTRIDMode(fee) &&
                                                          !GfeToleranceViolationCalculator.RecordingFeeTypes.Contains(fee.MismoFeeT) &&
                                                          fee.TotalAmount != 0
                                                    select (BorrowerClosingCostFee)fee.Clone()).ToList();

            var archivedTenToleranceNonRecordingFeeAmounts = (from fee in tenPercentArchiveSet.GetFees(tenPercentArchiveFilter).Cast<BorrowerClosingCostFee>()
                                                              where liveTenToleranceNonRecordingFees.Find(p => p.ClosingCostFeeTypeId.Equals(fee.ClosingCostFeeTypeId)) != null
                                                              select (BorrowerClosingCostFee)fee.Clone()).ToList();

            this.nonRecordingFees = ToleranceCureDebugUtilities.CreateDebugFeeList(this.convert, archivedTenToleranceNonRecordingFeeAmounts, liveTenToleranceNonRecordingFees);
        }

        /// <summary>
        /// Calculates fee sums.
        /// </summary>
        public void CalculateSums()
        {
            this.LiveFeesSumAmount = this.RecordingFeeLiveAmount + Tools.SumMoney(this.NonRecordingFees.Select(f => f.LiveFee.TotalAmount));
            this.ArchiveFeesSumAmount = this.RecordingFeeArchiveAmount + Tools.SumMoney(this.NonRecordingFees.Where(f => f.ArchiveFee != null).Select(f => f.ArchiveFee.TotalAmount));
            this.FeesSumDifferenceAmount = Tools.SumMoney(new decimal[] { this.LiveFeesSumAmount, -1 * this.ArchiveFeesSumAmount });
        }

        /// <summary>
        /// Calculates the ten percent tolerance cure using gathered data.
        /// </summary>
        /// <param name="dataLoan">The loan data.</param>
        public void CalculateCure(CPageBase dataLoan)
        {
            // NOTE: Adding sum + .1 * sum here instead of in Tools.SumMoney fixes issues with banker rounding causing a
            // descrepancy between the debug cure amount and sToleranceTenPercentCure (which uses 1.1M * archive fees sum for its calculation).
            decimal unroundedAdjustedAmount = this.ArchiveFeesSumAmount + this.TenPercentArchiveFeesSumAmount;
            decimal tenCureCalc = Tools.SumMoney(new decimal[] { this.LiveFeesSumAmount, -1 * unroundedAdjustedAmount });
            this.CalculatedCureAmount = Math.Max(0, tenCureCalc);

            if (this.IsCureAmountLckd)
            {
                this.CureAmount = dataLoan.sToleranceTenPercentCure;
            }
            else
            {
                this.CureAmount = this.CalculatedCureAmount;
            }
        }
    }
}
