﻿// <copyright file="ToleranceCureDebugFee.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Jhairo Erazo
//    Date:   05/23/2016
// </summary>
namespace LendersOffice.ObjLib.ToleranceCureDebug
{
    using System;

    using DataAccess;

    /// <summary>
    /// Used to store tolerance cure debug data by fee.
    /// </summary>
    public class ToleranceCureDebugFee : IComparable
    {
        /// <summary>
        /// Shared LosConvert object.
        /// </summary>
        private readonly LosConvert convert;

        /// <summary>
        /// Initializes a new instance of the <see cref="ToleranceCureDebugFee" /> class.
        /// </summary>
        /// <param name="convert">LosConvert object.</param>
        /// <param name="archiveFee">Loan Estimate Fee.</param>
        /// <param name="liveFee">Closing Disclosure Fee.</param>
        public ToleranceCureDebugFee(LosConvert convert, BorrowerClosingCostFee archiveFee, BorrowerClosingCostFee liveFee)
        {
            this.convert = convert;
            this.ArchiveFee = archiveFee;
            this.LiveFee = liveFee;
        }

        /// <summary>
        /// Gets or sets a Loan Estimate Archive Fee.
        /// </summary>
        /// <value>Loan Estimate Archive Fee.</value>
        public BorrowerClosingCostFee ArchiveFee { get; set; }

        /// <summary>
        /// Gets or sets a Closing Disclosure Live Fee.
        /// </summary>
        /// <value>Closing Disclosure Live Fee.</value>
        public BorrowerClosingCostFee LiveFee { get; set; }

        /// <summary>
        /// Gets the description of the fee.
        /// </summary>
        /// <value>Description of the fee.</value>
        /// <remarks>Uses the LiveFee's description unless it is null.</remarks>
        public string Description
        {
            get
            {
                if (this.LiveFee == null)
                {
                    return this.ArchiveFee.Description;
                }

                return this.LiveFee.Description;
            }
        }

        /// <summary>
        /// Gets the original description of the fee. The original description comes from lender level settings.
        /// </summary>
        /// <value>The original description of the fee.</value>
        /// <remarks>Uses the LiveFee's original description unless it is null.</remarks>
        public string OriginalDescription
        {
            get
            {
                if (this.LiveFee == null)
                {
                    return this.ArchiveFee.OriginalDescription;
                }

                return this.LiveFee.OriginalDescription;
            }
        }

        /// <summary>
        /// Gets the difference between the LiveFee amount and ArchiveFee amount.
        /// </summary>
        /// <value>Calculated difference between fees.</value>
        public decimal DifferenceAmount
        {
            get
            {
                decimal archiveAmount = this.ArchiveFee == null ? 0 : this.ArchiveFee.TotalAmount;
                decimal liveAmount = this.LiveFee == null ? 0 : this.LiveFee.TotalAmount;

                return Tools.SumMoney(new decimal[] { liveAmount, -1 * archiveAmount });
            }
        }

        /// <summary>
        /// Gets money string value of field.
        /// </summary>
        /// <value>Money string value of field.</value>
        public string DifferenceAmount_rep
        {
            get { return this.convert.ToMoneyString(this.DifferenceAmount, FormatDirection.ToRep); }
        }

        /// <summary>
        /// Gets the tolerance cure amount for fee.
        /// </summary>
        /// <value>Calculated tolerance cure amount for this fee.</value>
        /// <remarks>Only used for tolerance zero percent cure.</remarks>
        public decimal CureAmount
        {
            get
            {
                if (this.LiveFee == null)
                {
                    return decimal.MinValue;
                }

                return Math.Max(0, this.DifferenceAmount);
            }
        }

        /// <summary>
        /// Gets money string value of field.
        /// </summary>
        /// <value>Money string value of field.</value>
        public string CureAmount_rep
        {
            get
            {
                if (this.CureAmount == decimal.MinValue)
                {
                    return "N/A";
                }

                return this.convert.ToMoneyString(this.CureAmount, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Comparison function for FeeCompare Objects.
        /// </summary>
        /// <param name="obj">Another FeeCompare Object.</param>
        /// <returns>Integer indicating whether the current instance is
        /// greater than, less than, or equal to the given object.</returns>
        public int CompareTo(object obj)
        {
            ToleranceCureDebugFee convFC = (ToleranceCureDebugFee)obj;
            BorrowerClosingCostFee currCompFee = this.ArchiveFee == null ? this.LiveFee : this.ArchiveFee;
            BorrowerClosingCostFee otherCompFee = convFC.ArchiveFee == null ? convFC.LiveFee : convFC.ArchiveFee;

            // Compare section first.
            int sectComp = currCompFee.IntegratedDisclosureSectionT.CompareTo(otherCompFee.IntegratedDisclosureSectionT);
            if (sectComp == 0)
            {
                return currCompFee.Description.CompareTo(otherCompFee.Description);
            }

            return sectComp;
        }
    }
}
