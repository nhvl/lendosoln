﻿namespace LendersOffice.ObjLib.TitleProvider
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Simple data object representing the application of a single fee to the loan file.
    /// </summary>
    public class TitleFeeQuoteApplicationRecord
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TitleFeeQuoteApplicationRecord"/> class.
        /// </summary>
        /// <param name="sortPositionData">The sort data to use when determining the position. For example, the HUD line or LE section.</param>
        /// <param name="feeTypeId">Closing cost fee type ID.</param>
        /// <param name="feeName">The name of the fee.</param>
        /// <param name="borrowerAmount">The amount of the fee that was mapped to the borrower-responsible closing costs.</param>
        /// <param name="sellerAmount">The amount of the fee that was mapped to the seller-responsible closing costs.</param>
        /// <param name="costComponents">The components of the fee (costs) sent by the title vendor.</param>
        public TitleFeeQuoteApplicationRecord(string sortPositionData, Guid feeTypeId, string feeName, decimal? borrowerAmount, decimal? sellerAmount, IReadOnlyCollection<QuoteResultData.TitleSplitCost> costComponents)
        {
            this.SortPositionData = sortPositionData;
            this.FeeTypeId = feeTypeId;
            this.FeeName = feeName;
            this.BorrowerAmount = borrowerAmount;
            this.SellerAmount = sellerAmount;
            this.CostComponents = costComponents;
        }

        /// <summary>
        /// Gets the sort data to use when determining the position. For example, the HUD line or LE section.
        /// </summary>
        /// <value>The sort data to use when determining the position. For example, the HUD line or LE section.</value>
        public string SortPositionData { get; }

        /// <summary>
        /// Gets the Fee type ID.
        /// </summary>
        public Guid FeeTypeId { get; }

        /// <summary>
        /// Gets the name of the fee.
        /// </summary>
        /// <value>The name of the fee.</value>
        public string FeeName { get; }

        /// <summary>
        /// Gets the amount of the fee that was mapped to the borrower-responsible closing costs.
        /// </summary>
        /// <value>The amount of the fee that was mapped to the borrower-responsible closing costs.</value>
        public decimal? BorrowerAmount { get; }

        /// <summary>
        /// Gets the amount of the fee that was mapped to the seller-responsible closing costs.
        /// </summary>
        /// <value>The amount of the fee that was mapped to the seller-responsible closing costs.</value>
        public decimal? SellerAmount { get; }

        /// <summary>
        /// Gets the components of the fee (costs) sent by the title vendor.
        /// </summary>
        /// <value>The components of the fee (costs) sent by the title vendor.</value>
        public IReadOnlyCollection<QuoteResultData.TitleSplitCost> CostComponents { get; }
    }
}
