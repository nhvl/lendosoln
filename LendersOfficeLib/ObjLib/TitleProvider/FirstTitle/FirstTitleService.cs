﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.FirstCloseTitleService;
using System.Web.Script.Serialization;
using System.Net;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using CommonProjectLib.Common.Lib;
using System.Data.SqlClient;
using LendersOffice.Constants;

namespace LendersOffice.ObjLib.TitleProvider.FirstTitle
{
    public class FirstTitleService : TitleService
    {
        public FirstTitleService(int id, Guid brokerId, string username, string password, string accountid)
            : base(id, brokerId, username, password, accountid)
        {
        }
        #region first close title xml helpers

        private AuthenticationType GetAuthentication()
        {
            AuthenticationType authentication = new AuthenticationType();

            authentication.SecurityKey = m_clientId;
            authentication.AccountLogin = m_userName;
            authentication.AccountPassword = m_password;
            return authentication;
        }

        private LoanTypes GetLoanType(E_sLPurposeT sLPurposeT, E_sLienPosT sLienPosT)
        {
            if (sLPurposeT == E_sLPurposeT.Construct || sLPurposeT == E_sLPurposeT.ConstructPerm)
            {
                return LoanTypes.Construction;
            }
            else if (sLienPosT == E_sLienPosT.Second)
            {
                return LoanTypes.Second;
            }
            else if (sLPurposeT == E_sLPurposeT.RefinCashout)
            {
                return LoanTypes.FirstwithCashOut;
            }
            else
            {
                return LoanTypes.FirstwithnoCashOut;
            }
        }
        
        private TransactionTypes GetTransactionType(E_sLPurposeT purpose)
        {
            switch (purpose)
            {
                case E_sLPurposeT.Purchase:
                case E_sLPurposeT.Construct:
                case E_sLPurposeT.ConstructPerm:
                    return TransactionTypes.Purchase;
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.VaIrrrl:
                    return TransactionTypes.Refinance;
                default:
                    throw new UnhandledEnumException(purpose);
            }

        }

        private PolicyTypes GetPolicyType(E_cTitleInsurancePolicyT cTitleInsurancePolicyT)
        {
            switch (cTitleInsurancePolicyT)
            {
                case E_cTitleInsurancePolicyT.Owner:
                    return PolicyTypes.Owner;
                case E_cTitleInsurancePolicyT.Lender:
                    return PolicyTypes.Lender;
                case E_cTitleInsurancePolicyT.Simultaneous:
                    return PolicyTypes.Simultaneous;
                default:
                    throw new UnhandledEnumException(cTitleInsurancePolicyT);
            }
        }

        private RateTypes GetRateType(bool IsOptionArm, E_sFinMethT FinMethod)
        {
            if (IsOptionArm)
            {
                return RateTypes.VariableRateNegAm;
            }
            switch (FinMethod)
            {
                case E_sFinMethT.Fixed:
                    return RateTypes.Fixed;
                case E_sFinMethT.ARM:
                case E_sFinMethT.Graduated:
                    return RateTypes.VariableRate;
                default:
                    throw new UnhandledEnumException(FinMethod);
            }
        }

        private RateService GetRateService()
        {
            RateService service = new RateService();
            service.Timeout = 15000;
            service.Proxy = null;
            return service;
        }
        private PropertyTypes GetPropertyType(E_sProdSpT sProdSpT, E_sProdSpStructureT sProdSpStructureT)
        {
            switch (sProdSpT)
            {
                case E_sProdSpT.TwoUnits:
                    return PropertyTypes.Duplex2unit;
                case E_sProdSpT.ThreeUnits:
                    return PropertyTypes.Triplex3unit;
                case E_sProdSpT.FourUnits:
                    return PropertyTypes.Fourplex4unit;
                case E_sProdSpT.Condo:
                    return PropertyTypes.Condo;
                case E_sProdSpT.Manufactured:
                    return PropertyTypes.ManufacturedHousing;
                case E_sProdSpT.CoOp:
                    return PropertyTypes.House1unit;
                default:
                    break;
            }

            switch (sProdSpStructureT)
            {
                case E_sProdSpStructureT.Detached:
                    return PropertyTypes.House1unit;
                case E_sProdSpStructureT.Attached:
                    return PropertyTypes.Townhouse;
                default:
                    throw new UnhandledEnumException(sProdSpStructureT);
            }
        }

        private ArrayOfGetRecordingOffice_ResponseRecordingOfficeRecordingOffice GetRecordingOffice(QuoteRequestData requestData)
        {
            RateService rateService = GetRateService();

            RecordingOfficeRequest recordingOfficeRequest = new RecordingOfficeRequest();

            recordingOfficeRequest.Authentication = GetAuthentication();
            recordingOfficeRequest.PropertyInfo = new RecordingOfficeRequestPropertyInfo();
            recordingOfficeRequest.PropertyInfo.Address = requestData.sSpAddr;
            recordingOfficeRequest.PropertyInfo.City = requestData.sSpCity;
            recordingOfficeRequest.PropertyInfo.State = requestData.sSpState;
            recordingOfficeRequest.PropertyInfo.ZipCode = requestData.sSpZip;
            recordingOfficeRequest.PropertyInfo.County = requestData.sSpCounty;
            recordingOfficeRequest.PropertyInfo.FIPSCode = requestData.sSpCountyFips.ToString().PadLeft(5, '0');


            GetRecordingOfficeResult recordingOfficeResponse = rateService.GetRecordingOffice(recordingOfficeRequest);
            JavaScriptSerializer js = new JavaScriptSerializer();
            Tools.LogInfo("First Close GetRecordingOffice request was sent. Loan: " + requestData.sLNm + ", Request: " + js.Serialize(recordingOfficeRequest.PropertyInfo));
            Tools.LogInfo("First Close GetRecordingOffice response was received. Loan: " + requestData.sLNm + ", Request: " + js.Serialize(recordingOfficeResponse));

            //Exit out early if we didn't get a response for the recording office
            if (recordingOfficeResponse == null || recordingOfficeResponse.RecordingOffices == null ||
                false == recordingOfficeResponse.ErrorInfo.ErrorDetail.Equals("Success", StringComparison.OrdinalIgnoreCase))
            {
                return null;
            }
            return recordingOfficeResponse.RecordingOffices[0];

        }

        private QuoteRequest BuildQuoteRequest(QuoteRequestData quoteRequestData, ArrayOfGetRecordingOffice_ResponseRecordingOfficeRecordingOffice recordingOffice, PolicyTypes policyType, RateTypes rateType)
        {

            QuoteRequest quoteRequest = new QuoteRequest();
            quoteRequest.Authentication = GetAuthentication();
            quoteRequest.CustomerRef = quoteRequestData.sLNm;

            quoteRequest.PropertyInfo = new QuoteRequestPropertyInfo();
            quoteRequest.PropertyInfo.Address = quoteRequestData.sSpAddr;
            quoteRequest.PropertyInfo.City = quoteRequestData.sSpCity;
            quoteRequest.PropertyInfo.State = quoteRequestData.sSpState;
            quoteRequest.PropertyInfo.ZipCode = quoteRequestData.sSpZip;
            quoteRequest.PropertyInfo.County = recordingOffice.RecordingOffice;
            quoteRequest.PropertyInfo.FIPSCode = quoteRequestData.sSpCountyFips.ToString().PadLeft(5, '0');

            quoteRequest.PolicyInfo = new QuoteRequestPolicyInfo();

            quoteRequest.PolicyInfo.LoanType = GetLoanType(quoteRequestData.sLPurposeT, quoteRequestData.sLienPosT);
            quoteRequest.PolicyInfo.TransactionType = GetTransactionType(quoteRequestData.sLPurposeT);
            quoteRequest.PolicyInfo.PolicyType = policyType;
            quoteRequest.PolicyInfo.RateType = rateType;
            quoteRequest.PolicyInfo.PropertyType = GetPropertyType(quoteRequestData.sProdSpT, quoteRequestData.sProdSpStructureT);

            quoteRequest.PolicyInfo.LoanAmount = Decimal.Round(quoteRequestData.sFinalLAmt, 2);
            quoteRequest.PolicyInfo.LoanAmountSpecified = true;

            if (quoteRequestData.sPurchPrice > 0)
            {
                quoteRequest.PolicyInfo.SalesPrice = Decimal.Round(quoteRequestData.sPurchPrice, 2);
                quoteRequest.PolicyInfo.SalesPriceSpecified = true;
            }

            decimal sApprVal = 0;
            //sHasAppraisal doesn't seem to be a good way of telling if there's actually an appraisal
            //(you can set the sApprVal field without making it "true")
            //so we'll check that the values are > zero.
            if (quoteRequestData.sApprVal > 0)
            {
                sApprVal = quoteRequestData.sApprVal;
            }
            else if (quoteRequestData.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance)
            {
                sApprVal = quoteRequestData.sOriginalAppraisedValue;
            }

            if (sApprVal > 0)
            {
                quoteRequest.PolicyInfo.AppraisedValue = Decimal.Round(sApprVal, 2);
                quoteRequest.PolicyInfo.AppraisedValueSpecified = true;
            }
            else
            {
                quoteRequest.PolicyInfo.AppraisedValueSpecified = false;
            }

            quoteRequest.PolicyInfo.PriorMortgage = new QuoteRequestPolicyInfoPriorMortgage();
            quoteRequest.PolicyInfo.PriorMortgage.InformationAvailable = false;

            quoteRequest.PolicyInfo.PolicyQuestions = new ArrayOfGetQuote_RequestPolicyInfoPolicyQuestionPolicyQuestion[10];
            quoteRequest.PolicyInfo.PolicyQuestions[0] = new ArrayOfGetQuote_RequestPolicyInfoPolicyQuestionPolicyQuestion();
            quoteRequest.PolicyInfo.PolicyQuestions[0].QuestionID = 85;
            quoteRequest.PolicyInfo.PolicyQuestions[0].QuestionValue = (quoteRequestData.aBAge > 55).ToString();
            quoteRequest.PolicyInfo.PolicyQuestions[1] = new ArrayOfGetQuote_RequestPolicyInfoPolicyQuestionPolicyQuestion();
            quoteRequest.PolicyInfo.PolicyQuestions[1].QuestionID = 86;
            quoteRequest.PolicyInfo.PolicyQuestions[1].QuestionValue = (quoteRequestData.aBAge > 60).ToString();
            quoteRequest.PolicyInfo.PolicyQuestions[2] = new ArrayOfGetQuote_RequestPolicyInfoPolicyQuestionPolicyQuestion();
            quoteRequest.PolicyInfo.PolicyQuestions[2].QuestionID = 87;
            quoteRequest.PolicyInfo.PolicyQuestions[2].QuestionValue = (quoteRequestData.aBAge > 62).ToString();
            quoteRequest.PolicyInfo.PolicyQuestions[3] = new ArrayOfGetQuote_RequestPolicyInfoPolicyQuestionPolicyQuestion();
            quoteRequest.PolicyInfo.PolicyQuestions[3].QuestionID = 103;
            quoteRequest.PolicyInfo.PolicyQuestions[3].QuestionValue = (quoteRequestData.sLPurposeT == E_sLPurposeT.RefinCashout).ToString();
            quoteRequest.PolicyInfo.PolicyQuestions[4] = new ArrayOfGetQuote_RequestPolicyInfoPolicyQuestionPolicyQuestion();
            quoteRequest.PolicyInfo.PolicyQuestions[4].QuestionID = 104;
            quoteRequest.PolicyInfo.PolicyQuestions[4].QuestionValue = (quoteRequestData.sLPurposeT == E_sLPurposeT.Construct || quoteRequestData.sLPurposeT == E_sLPurposeT.ConstructPerm).ToString();
            quoteRequest.PolicyInfo.PolicyQuestions[5] = new ArrayOfGetQuote_RequestPolicyInfoPolicyQuestionPolicyQuestion();
            quoteRequest.PolicyInfo.PolicyQuestions[5].QuestionID = 109;
            quoteRequest.PolicyInfo.PolicyQuestions[5].QuestionValue = (quoteRequestData.sLienPosT == E_sLienPosT.Second).ToString();
            quoteRequest.PolicyInfo.PolicyQuestions[6] = new ArrayOfGetQuote_RequestPolicyInfoPolicyQuestionPolicyQuestion();
            quoteRequest.PolicyInfo.PolicyQuestions[6].QuestionID = 110;
            quoteRequest.PolicyInfo.PolicyQuestions[6].QuestionValue = (quoteRequestData.sLT == E_sLT.FHA).ToString();
            quoteRequest.PolicyInfo.PolicyQuestions[7] = new ArrayOfGetQuote_RequestPolicyInfoPolicyQuestionPolicyQuestion();
            quoteRequest.PolicyInfo.PolicyQuestions[7].QuestionID = 111;
            quoteRequest.PolicyInfo.PolicyQuestions[7].QuestionValue = (quoteRequestData.aOccT == E_aOccT.Investment).ToString();
            quoteRequest.PolicyInfo.PolicyQuestions[8] = new ArrayOfGetQuote_RequestPolicyInfoPolicyQuestionPolicyQuestion();
            quoteRequest.PolicyInfo.PolicyQuestions[8].QuestionID = 88;
            quoteRequest.PolicyInfo.PolicyQuestions[8].QuestionValue = (quoteRequestData.aOccT == E_aOccT.PrimaryResidence).ToString();
            quoteRequest.PolicyInfo.PolicyQuestions[9] = new ArrayOfGetQuote_RequestPolicyInfoPolicyQuestionPolicyQuestion();
            quoteRequest.PolicyInfo.PolicyQuestions[9].QuestionID = 119;
            quoteRequest.PolicyInfo.PolicyQuestions[9].QuestionValue = true.ToString();

            #region ny state only questions
            if (quoteRequestData.sSpState.Equals("NY"))
            {
                quoteRequest.PolicyInfo.RecordingQuestions = new ArrayOfGetQuote_RequestPolicyInfoRecordingQuestionRecordingQuestion[4];
                quoteRequest.PolicyInfo.RecordingQuestions[0] = new ArrayOfGetQuote_RequestPolicyInfoRecordingQuestionRecordingQuestion();
                quoteRequest.PolicyInfo.RecordingQuestions[0].RecordingQuestionID = 16;
                quoteRequest.PolicyInfo.RecordingQuestions[0].RecordingQuestionValue = (quoteRequestData.sProdSpT != E_sProdSpT.ThreeUnits && quoteRequestData.sProdSpT != E_sProdSpT.FourUnits).ToString();
                quoteRequest.PolicyInfo.RecordingQuestions[1] = new ArrayOfGetQuote_RequestPolicyInfoRecordingQuestionRecordingQuestion();
                quoteRequest.PolicyInfo.RecordingQuestions[1].RecordingQuestionID = 17;
                quoteRequest.PolicyInfo.RecordingQuestions[1].RecordingQuestionValue = (quoteRequestData.sProdSpT != E_sProdSpT.ThreeUnits && quoteRequestData.sProdSpT != E_sProdSpT.FourUnits).ToString();
                quoteRequest.PolicyInfo.RecordingQuestions[2] = new ArrayOfGetQuote_RequestPolicyInfoRecordingQuestionRecordingQuestion();
                quoteRequest.PolicyInfo.RecordingQuestions[2].RecordingQuestionID = 23;
                quoteRequest.PolicyInfo.RecordingQuestions[2].RecordingQuestionValue = (quoteRequestData.sProdSpT != E_sProdSpT.ThreeUnits).ToString();
                quoteRequest.PolicyInfo.RecordingQuestions[3] = new ArrayOfGetQuote_RequestPolicyInfoRecordingQuestionRecordingQuestion();
                quoteRequest.PolicyInfo.RecordingQuestions[3].RecordingQuestionID = 24;
                quoteRequest.PolicyInfo.RecordingQuestions[3].RecordingQuestionValue = (quoteRequestData.sProdSpT != E_sProdSpT.ThreeUnits).ToString();
            }
            #endregion
            #region la state only questions
            if (quoteRequestData.sSpState.Equals("LA"))
            {
                quoteRequest.PolicyInfo.RecordingQuestions = new ArrayOfGetQuote_RequestPolicyInfoRecordingQuestionRecordingQuestion[1];
                quoteRequest.PolicyInfo.RecordingQuestions[0] = new ArrayOfGetQuote_RequestPolicyInfoRecordingQuestionRecordingQuestion();
                quoteRequest.PolicyInfo.RecordingQuestions[0].RecordingQuestionID = 19;
                quoteRequest.PolicyInfo.RecordingQuestions[0].RecordingQuestionValue = (quoteRequestData.sLienPosT == E_sLienPosT.Second).ToString();
            }
            #endregion

            #region MD only fields
            if (quoteRequestData.sSpState.Equals("MD"))
            {
                quoteRequest.PolicyInfo.RecordingQuestions = new ArrayOfGetQuote_RequestPolicyInfoRecordingQuestionRecordingQuestion[2];
                quoteRequest.PolicyInfo.RecordingQuestions[0] = new ArrayOfGetQuote_RequestPolicyInfoRecordingQuestionRecordingQuestion();
                quoteRequest.PolicyInfo.RecordingQuestions[0].RecordingQuestionID = 21;
                quoteRequest.PolicyInfo.RecordingQuestions[0].RecordingQuestionValue = (quoteRequestData.aOccT == E_aOccT.PrimaryResidence).ToString();
                quoteRequest.PolicyInfo.RecordingQuestions[1] = new ArrayOfGetQuote_RequestPolicyInfoRecordingQuestionRecordingQuestion();
                quoteRequest.PolicyInfo.RecordingQuestions[1].RecordingQuestionID = 46;
                quoteRequest.PolicyInfo.RecordingQuestions[1].RecordingQuestionValue = (quoteRequestData.aOccT == E_aOccT.PrimaryResidence).ToString();
            }
            #endregion

            quoteRequest.RecordingOfficeID = recordingOffice.RecordingOfficeID;

            return quoteRequest;
        }

        private GetQuoteSyncResult GetQuoteResponse(QuoteRequest request)
        {
            RateService service = GetRateService();
            GetQuoteSyncResult response = null;
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            try
            {
                response = service.GetQuoteSync(request);
            }
            catch (SoapException e)
            {
                Tools.LogErrorAndSendMail(e);
                return null;
            }
            catch (WebException wex)
            {
                if (!wex.Message.Contains("Unable to connect") && !wex.Message.Contains("timed out"))
                {
                    Tools.LogErrorAndSendMail(wex);
                }
                else
                {
                    Tools.LogInfo("FirstCloseFailure " + wex.ToString());
                }
                return null;
            }
            request.Authentication.AccountPassword = "******"; // Masked for logging
            request.Authentication.SecurityKey = "******"; // Masked for logging     

            Tools.LogInfo("First Close GetQuote request was sent. Loan: " + request.Authentication + ", Request: " + serializer.Serialize(request));
            if (response != null)
            {
                Tools.LogInfo("First Close GetQuote response was received. Loan: " + request.Authentication + ", Request: " + serializer.Serialize(response));
            }
            return response;
        }
        private static decimal GetHUDItem(ArrayOfGetQuote_ResponsePremiumPremium premium, string number)
        {
            return Convert.ToDecimal(premium.HUD_LINES.ToList().First(p => p.NUMBER.ToString().Equals(number)).VALUE.ToString());
        }

        private static NTuple<ArrayOfGetQuote_ResponsePremiumPremium, ArrayOfGetQuote_ResponsePremiumPremium> GetMinMaxPremium(GetQuoteSyncResult result)
        {
            var premiums = result.Premiums.OrderBy(p => GetHUDItem(p, "103")).ToList();
            var minPremium = premiums.First();
            var maxPremium = premiums.Last();

            return NTuple.Create(minPremium, maxPremium);
        }

        private void SaveQuote(Guid sLid, QuoteRequestData quoteRequestData, PolicyTypes policyType, GetQuoteSyncResult result)
        {
            var premiums = GetMinMaxPremium(result);
            ArrayOfGetQuote_ResponsePremiumPremium premium;

            if (quoteRequestData.sTitleInsuranceCostT == E_sTitleInsuranceCostT.EstimateHighest)
            {
                premium = premiums.Item2;
            }
            else
            {
                premium = premiums.Item1;
            }
            QuoteLog log = new QuoteLog();
            log.LoanData = quoteRequestData;
            log.Results.HassNotaryF = false;
            log.RecordingOffice.Attention = result.RecordingOffice.RecordingOffice;
            log.Results.sCountyRtc = GetHUDItem(premium, "1204");
            log.Results.sEscrowF = GetHUDItem(premium, "1102");
            //log.Results.sNotaryF 
            log.Results.sOwnerTitleInsF = premium.GFE_5_Total;
            log.Results.sRecF = premium.GFE_7_Total;
            log.Results.sStateRtc = GetHUDItem(premium, "1205");
            log.Results.sTitleInsF = GetHUDItem(premium, "1104");

            log.AddItem("sGfeLenderTitleTotalFee", premium.GFE_4_Total.ToString());
            log.AddItem("sGfeOwnerTitleTotalFee", premium.GFE_5_Total.ToString());
            log.AddItem("sGfeGovtRecTotalFee", premium.GFE_7_Total.ToString());
            log.AddItem("sGfeTransferTaxTotalFee", premium.GFE_8_Total.ToString());
            log.AddItem("sGfeTransferTaxTotalFee", premium.GFE_8_Total.ToString());
            log.AddItem("sRecordingOffice", result.RecordingOffice.RecordingOffice);
            log.AddItem("sRateID", premium.RateID);
            log.AddItem("CustomerRef", result.CustomerRef);
            log.AddItem("TransactionRef", result.TransactionRef);

            string responsePdfFileDbKey = "";

            if (result.Documents != null) //todo 
            {
                if (result.Documents.Length > 1)
                {
                    Tools.LogErrorAndSendMail("FirstCloseResponse contains more than 1 doc. Only saving the first one.");
                }
                var document = result.Documents[0];
                string base64 = document.DocumentContents;
                byte[] bytes = Convert.FromBase64String(base64);
                responsePdfFileDbKey = document.DocumentInfo.DocumentName; //why are we using the doc name? Is it not posisble for it to be uresued
                FileDBTools.WriteData(E_FileDB.Normal, responsePdfFileDbKey, bytes);
                log.AddItem("ResponsePdfFileDbKey", responsePdfFileDbKey);

            }

            SaveQuoteLog(sLid, log);
          }


        #endregion
        public override void OrderQuote(Guid sLId, E_cTitleInsurancePolicyT cTitleInsurancePolicyT, bool applyToClosingCost)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(FirstTitleService));
            if (applyToClosingCost)
            {
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            }
            else
            {
                dataLoan.InitLoad();
            }

            QuoteRequestData quoteData = dataLoan.GetTitleQuoteRequestData();

            PolicyTypes policyType = GetPolicyType(cTitleInsurancePolicyT);

            if (GetTransactionType(quoteData.sLPurposeT) == TransactionTypes.Refinance)
            {
                policyType = PolicyTypes.Lender;
            }

            RateTypes rateType = GetRateType(quoteData.sIsOptionArm, quoteData.sFinMethT);

            var recordingOffice = GetRecordingOffice(quoteData);
            var request = BuildQuoteRequest(quoteData, recordingOffice, policyType, rateType);
            var response = GetQuoteResponse(request);

            if (response == null || response.Premiums == null)
            {
                throw CBaseException.GenericException("response is null or there are no premiums");
            }

            SaveQuote(sLId, quoteData, policyType, response);
        }

        public override void OrderPolicy(Guid sLId)
        {
            throw new NotImplementedException();
        }

        public override void ReceiveDocFromVendor()
        {
            throw new NotImplementedException();
        }

        public override void SendDocToVendor(Guid sLid, Guid sDocumentId)
        {
            throw new NotImplementedException();
        }

        public override E_cTitleInsurancePolicyT[] GetAvailablePolicyTypes(E_sLPurposeT sLPurposeT, string sSpState)
        {
            if (GetTransactionType(sLPurposeT) == TransactionTypes.Refinance)
            {
                return new E_cTitleInsurancePolicyT[] { E_cTitleInsurancePolicyT.Lender };
            }
            else
            {
                return new E_cTitleInsurancePolicyT[] { E_cTitleInsurancePolicyT.Lender, E_cTitleInsurancePolicyT.Owner, E_cTitleInsurancePolicyT.Simultaneous };
            }

        }

        public override PricingQuotes CacheQuotesForPricing(Guid sLId)
        {
            throw new NotImplementedException();
        }
        public override bool HasQuotesForPricing(Guid sLId)
        {
            throw new NotImplementedException();

        }
        public override PricingQuotes GetCachedQuotes(Guid sLId)
        {
            throw new NotImplementedException();
        }

    }
}
