﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Security;

namespace LendersOffice.ObjLib.TitleProvider
{
    public enum E_TitleVendorIntegrationType
    {
        FirstAmerican = 1
    }

    public static class TitleProvider
    {
        private static IEnumerable<Vendor> GetProvidersImpl(int? id)
        {
            //todo enforce loadmin checks

            List<Vendor> providers = new List<Vendor>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@Id", id)
                                        };
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "TITLE_VENDOR_Get", parameters))
            {
                while (reader.Read())
                {
                    providers.Add(new Vendor(reader));
                }
            }

            return providers;
        }

        public static IEnumerable<Vendor> GetProviders()
        {
            return GetProvidersImpl(null);
        }

        public static Vendor GetProvider(int id)
        {
            return GetProvidersImpl(id).FirstOrDefault();
        }

        /// <summary>
        /// Given a LQB Username and LQB Password for the provider handler returns the service or null if it could not be found.
        /// </summary>
        /// <param name="providerUsername"></param>
        /// <param name="providerPassword"></param>
        /// <param name="loanId"></param>
        /// <returns></returns>
        public static TitleService GetService(string providerUsername, string providerPassword, Guid loanId)
        {
            var titleVendorList = GetProviders();

            Vendor vendor = null;

            // 4/17/2015 dd - Check to see if providerUserName and providerPassword match in vendor list.
            foreach (var o in titleVendorList)
            {
                if (o.LQBServiceUserName == providerUsername && o.LQBServicePassword == providerPassword)
                {
                    vendor = o;
                    break;
                }
            }

            if (vendor == null)
            {
                // No vendor info match.
                return null;
            }

            Guid brokerId = Guid.Empty;

            DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfoByLoanId(loanId, out brokerId);

            var associationList = GetAssociationsImpl(brokerId, vendor.Id, titleVendorList);

            if (associationList.Count == 0)
            {
                return null;
            }
            else
            {
                return TitleService.GetServiceFor(associationList.First());
            }

            
        }

        public static TitleService GetService(Guid brokerId, int id)
        {
            var titleVendorList = GetProviders();

            BrokerVendorAssociation info = GetAssociationsImpl(brokerId, id, titleVendorList).FirstOrDefault();

            if (info == null)
            {
                throw CBaseException.GenericException("There is no provider association with broker id and id of " + brokerId + " " + id);
            }

            return TitleService.GetServiceFor(info);
        }
        /// <summary>
        /// Updates or creates the given vendor.
        /// </summary>
        /// <param name="vendor"></param>
        public static void Update(Vendor vendor)
        {
            //todo enforce loadmin checks
            List<SqlParameter> parameters = new List<SqlParameter>();
            if (vendor.HasId)
            {
                parameters.Add(new SqlParameter("@Id", vendor.Id));
            }
            else
            {
                vendor.SetupNewEncryptionKey(EncryptionHelper.GenerateNewKey());
                parameters.Add(new SqlParameter("@EncryptionKeyId", vendor.EncryptionKeyId.Value));
            }

            byte[] encryptedLqbServicePassword = null;
            if (vendor.EncryptionKeyId != default(LqbGrammar.DataTypes.EncryptionKeyIdentifier))
            {
                encryptedLqbServicePassword = EncryptionHelper.EncryptString(vendor.EncryptionKeyId, vendor.LQBServicePassword);
            }

            parameters.Add(new SqlParameter("@Name", vendor.Name));
            parameters.Add(new SqlParameter("@ExportPath", vendor.ExportPath));
            parameters.Add(new SqlParameter("@AccountIdRequired", vendor.RequiresAccountId));
            parameters.Add(new SqlParameter("@LQBServiceUserName", vendor.LQBServiceUserName));
            parameters.Add(new SqlParameter("@LQBServicePassword", vendor.LQBServicePassword));
            parameters.Add(new SqlParameter("@EncryptedLQBServicePassword", encryptedLqbServicePassword ?? new byte[0]));
            parameters.Add(new SqlParameter("@LQBServiceSourceName", vendor.LQBServiceSourceName));
            parameters.Add(new SqlParameter("@TitleVendorExportPathTitle", vendor.ExportPathTitle));
            parameters.Add(new SqlParameter("@TitleVendorIntegrationType", vendor.TitleVendorIntegrationType));
            parameters.Add(new SqlParameter("@EnableConnectionTest", vendor.EnableConnectionTest));

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "TITLE_VENDOR_Update", 2, parameters.ToArray());
        }

        /// <summary>
        /// Deletes the given vendor and removes all the associations.
        /// </summary>
        /// <param name="vendorId"></param>
        public static void Delete(int vendorId)
        {
            //todo enforce loadmin checks
            // 4/17/2015 dd - Delete all the title from the broker mapping table.
            SqlParameter[] parameters = null;
            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                parameters = new SqlParameter[] {
                                                new SqlParameter("@TitleVendorId", vendorId)
                                            };
                StoredProcedureHelper.ExecuteNonQuery(connInfo, "TITLE_VENDOR_X_BROKER_DeleteByVendorId", 2, parameters);
            }

            parameters = new SqlParameter[] {
                        new SqlParameter("@Id", vendorId)
                    };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "TITLE_VENDOR_Delete", 2, parameters);
        }

        /// <summary>
        /// Creates or updates an association
        /// </summary>
        /// <param name="association"></param>
        public static void UpdateAssociation(BrokerVendorAssociation association)
        {
            //todo enforce loadmin checks
            
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@BrokerId", association.BrokerId));
            parameters.Add(new SqlParameter("@TitleVendorId ", association.VendorId));
            parameters.Add(new SqlParameter("@Login ", association.UsernameQuote));
            parameters.Add(new SqlParameter("@Password  ", association.EncryptedPasswordQuote));
            parameters.Add(new SqlParameter("@ClientCode   ", association.AccountIdQuote));
            parameters.Add(new SqlParameter("@LoginTitle   ", association.UsernameTitle));
            parameters.Add(new SqlParameter("@PasswordTitle   ", association.EncryptedPasswordTitle));
            parameters.Add(new SqlParameter("@ClientCodeTitle   ", association.AccountIdTitle));

            StoredProcedureHelper.ExecuteNonQuery(association.BrokerId, "TITLE_VENDOR_X_BROKER_Update ", 2, parameters);
        }

        /// <summary>
        /// Deletes an association
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="vendorId"></param>
        public static void DeleteAssociation(Guid brokerId, int vendorId)
        {
            //todo enforce loadmin checks

            SqlParameter[] parameters = {
                                            new SqlParameter("@VendorId", vendorId),
                                            new SqlParameter("@BrokerId", brokerId)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "TITLE_VENDOR_X_BROKER_Delete", 2, parameters);

        }
        public static List<BrokerVendorAssociation> GetAssociations(Guid brokerId)
        {
            var titleVendorList = GetProviders();

            return GetAssociationsImpl(brokerId, null, titleVendorList);
        }

        private static List<BrokerVendorAssociation> GetAssociationsImpl(Guid brokerId, int? id, IEnumerable<Vendor> titleVendorList)
        {
            List<BrokerVendorAssociation> providers = new List<BrokerVendorAssociation>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@Id", id)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "TITLE_VENDOR_X_BROKER_Get", parameters))
            {
                while (reader.Read())
                {
                    providers.Add(new BrokerVendorAssociation(reader, titleVendorList));
                }
            }

            return providers;
        }

        /// <summary>
        /// Returns a list of all vendors with the broker info if an association exist.  The broker specific info
        /// will be empty if it does not exist.
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public static List<BrokerVendorAssociation> GetAllAssociationsWithBrokerInfo(Guid brokerId)
        {
            
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId)
                                        };

            var titleVendorList = GetProviders();

            var associationList = GetAssociationsImpl(brokerId, null, titleVendorList);

            
            List<BrokerVendorAssociation> entries = new List<BrokerVendorAssociation>(associationList);

            foreach (var vendor in titleVendorList)
            {
                // Find vendor that is not in association list and add empty association object.
                bool found = false;
                foreach (var association in associationList)
                {
                    if (association.VendorId == vendor.Id)
                    {
                        found = true;
                        break;
                    }
                }

                if (found == false)
                {
                    entries.Add(new BrokerVendorAssociation(vendor));
                }
            }

            return entries;
        }

        public static PricingQuotes GetQuotesForPricing(Guid brokerId, QuoteRequestData data, IDictionary<int, QuoteRequestOptions> requestOptionsByVendorId)
        {
            if (!ValidTitleLocale(data.sSpState, data.sSpCounty))
            {
                return null;
            }

            PricingQuotes results = new PricingQuotes() { LoanData = data };
            bool throwException = false;
            try
            {
                using (PerformanceStopwatch.Start("TitleProvider.GetQuotesForPricing"))
                {
                    BrokerDB db = BrokerDB.RetrieveById(brokerId);

                    if (db.CalculateclosingCostInPML 
                        && db.IsTitleInterfaceEnabled(data.sBranchChannelT,data.sLPurposeT))
                    {

                        foreach (int vendorId in requestOptionsByVendorId.Keys)
                        {
                            TitleService provider = TitleProvider.GetService(brokerId, vendorId);
                            QuoteResultData quote = provider.GetQuotes(data, requestOptionsByVendorId[vendorId]);

                            if (quote == null || quote.HasZeroFees)
                            {
                                Tools.LogError("[FACCDebugXML] Did not get any results " + ObsoleteSerializationHelper.JavascriptJsonSerialize(data) + "\n");
                                if (quote != null)
                                {
                                    Tools.LogError("[FACCDebugXML] Error - " + quote.TitleDebugInfo);
                                }
                                else
                                {
                                    quote = new QuoteResultData();
                                    quote.HasError = true;
                                    throwException = true;
                                }
                            }
                            else if (quote.MissingRecordingDocs)
                            {
                                throwException = true;
                                Tools.LogError("[FACCDebugXML] Missing recording docs : " + quote.TitleDebugInfo);

                            }
                            else
                            {
                                Tools.LogInfo("[FACCDebugXMLWorking]\n" + quote.TitleDebugInfo);
                            }

                            results.Quotes.Add(quote);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                //we dont want to crash pricing
                Tools.LogError(e);

                QuoteResultData quote = new QuoteResultData();
                quote.HasError = true;
                results.Quotes.Add(quote);
            }

            //if cp runs pricing we need to crash it.
            if (data.RunPmlRequestSourceT == E_RunPmlRequestSourceT.ConsumerPortal && throwException)
            {
                if (data.sLPurposeT != E_sLPurposeT.HomeEquity && data.sLienPosT != E_sLienPosT.Second)
                {
                    throw CBaseException.GenericException("Could not run Title Integration.");
                }
            }
            return results;
        }

        private static void CacheQuote(string key, PricingQuotes quotes)
        {
            string json = ObsoleteSerializationHelper.JavascriptJsonSerialize(quotes);
            Tools.LogInfo("[FACCQuote] " + json);
            if (!AutoExpiredTextCache.UpdateCache(json, key))
            {
                AutoExpiredTextCache.AddToCache(json, TimeSpan.FromHours(2), key);
            }
        }

        /// <summary>
        /// Sanity check the given state and county, without either of which the title vendor will not be able to generate a title quote.
        /// </summary>
        /// <param name="sSpState">The subject property state.</param>
        /// <param name="sSpCounty">The subject property county.</param>
        /// <returns>True if both state and county are non-blank. Otherwise false.</returns>
        private static bool ValidTitleLocale(string sSpState, string sSpCounty)
        {
            return !(string.IsNullOrEmpty(sSpState) || string.IsNullOrEmpty(sSpCounty));
        }
    }
}
