﻿namespace LendersOffice.ObjLib.TitleProvider
{
    using System.Collections.Generic;
    using System.Linq;

    public class PricingQuotes
    {
        public QuoteRequestData LoanData { get; set; }

        public List<QuoteResultData> Quotes { get; set; } = new List<QuoteResultData>();

        public bool HasError
        {
            get
            {
                return Quotes.Any(q => q.HasError);
            }
        }
    }
}
