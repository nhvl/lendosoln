﻿namespace LendersOffice.ObjLib.TitleProvider
{
    /// <summary>
    /// Options for quote request. Determines what will be requested from the Title Vendor.
    /// </summary>
    public class QuoteRequestOptions
    {
        /// <summary>
        /// Gets or sets a value indicating whether to request Title.
        /// </summary>
        /// <value>Should title be requested.</value>
        public bool RequestTitle { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to request Closing.
        /// </summary>
        /// <value>Should closing be requested.</value>
        public bool RequestClosing { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to request Recording.
        /// </summary>
        /// <value>Should recording be requested.</value>
        public bool RequestRecording { get; set; }
    }
}
