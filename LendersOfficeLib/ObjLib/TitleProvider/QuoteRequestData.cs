﻿namespace LendersOffice.ObjLib.TitleProvider
{
    using System;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Common;

    public sealed class QuoteRequestData : XmlSerializableCommon.AbstractXmlSerializable
    {
        public string sLNm { get; set; }
        public string sSpAddr { get; set; }
        public string sSpCity { get; set; }
        public string sSpState { get; set; }
        public string sSpZip { get; set; }
        public string sSpCounty { get; set; }
        public string sSpCountyFips { get; set; }
        public E_sLPurposeT sLPurposeT { get; set; }
        public E_sLienPosT sLienPosT { get; set; }
        public E_sFinMethT sFinMethT { get; set; }
        public bool sIsOptionArm { get; set; }
        public E_sProdSpT sProdSpT { get; set; }
        public E_sProdSpStructureT sProdSpStructureT { get; set; }
        public decimal sFinalLAmt { get; set; }
        public decimal sPurchPrice { get; set; }
        public decimal sApprVal { get; set; }
        public decimal sOriginalAppraisedValue { get; set; }
        public E_sLT sLT { get; set; }
        public string sLAmtCalc_rep { get; set; }
        public string sFinalLAmt_rep { get; set; }
        public string sApprVal_rep { get; set; }
        public string sOriginalAppraisedValue_rep { get; set; }
        public bool HasLinkedLoan { get; set; }
        public decimal sCreditLineAmt { get; set; }
        public E_RunPmlRequestSourceT RunPmlRequestSourceT { get; set; }
        public decimal sRefPdOffAmt1003 { get; set; }
        
        /// <summary>
        /// Gets or sets the outstanding principal balance of existing liens. Corresponds to the existing liens amount in section II of the 1003 (refi only).
        /// </summary>
        /// <value>The outstanding principal balance of existing liens.</value>
        public decimal sSpLien { get; set; }

        /// <summary>
        /// Gets or sets the closing cost fee version type of the loan. Corresponds to <see cref="CPageData.sClosingCostFeeVersionT"/>.
        /// </summary>
        /// <value>The closing cost fee version type of the loan.</value>
        public E_sClosingCostFeeVersionT sClosingCostFeeVersionT { get; set; }

        /// <summary>
        /// Gets or sets the disclosure regulation type of the loan. Corresponds to <see cref="CPageData.sDisclosureRegulationT"/>.
        /// </summary>
        /// <value>The disclosure regulation type of the loan.</value>
        public E_sDisclosureRegulationT sDisclosureRegulationT { get; set; }

        /// <summary>
        /// Gets or sets the business channel of the loan.
        /// </summary>
        public E_BranchChannelT sBranchChannelT { get; set; }

        #region app fields
        public int aBAge { get; set; }
        public E_aOccT aOccT { get; set; }
        #endregion

        public Guid sLId { get; set; }
        public Guid sBrokerId { get; set; }
        public E_sTitleInsuranceCostT sTitleInsuranceCostT { get; set; }

        public override void WriteXml(XmlWriter writer)
        {
            WriteElementString(writer, "sLNm", sLNm);
            WriteElementString(writer, "sSpAddr", sSpAddr);
            WriteElementString(writer, "sSpCity", sSpCity);
            WriteElementString(writer, "sSpState", sSpState);
            WriteElementString(writer, "sSpZip", sSpZip);
            WriteElementString(writer, "sSpCounty", sSpCounty);
            WriteElementString(writer, "sSpCountyFips", sSpCountyFips);
            WriteElementString(writer, "sLPurposeT", sLPurposeT.ToString("D"));
            WriteElementString(writer, "sLienPosT", sLienPosT.ToString("D"));
            WriteElementString(writer, "sFinMethT", sFinMethT.ToString("D"));
            WriteElementString(writer, "sIsOptionArm", sIsOptionArm.ToString());
            WriteElementString(writer, "sProdSpT", sProdSpT.ToString("D"));
            WriteElementString(writer, "sProdSpStructureT", sProdSpStructureT.ToString("D"));
            WriteElementString(writer, "sFinalLAmt", sFinalLAmt.ToString());
            WriteElementString(writer, "sPurchPrice", sPurchPrice.ToString());
            WriteElementString(writer, "sApprVal", sApprVal.ToString());
            WriteElementString(writer, "sOriginalAppraisedValue", sOriginalAppraisedValue.ToString());
            WriteElementString(writer, "sLT", sLT.ToString("D"));
            WriteElementString(writer, "sLAmtCalc_rep", sLAmtCalc_rep);
            WriteElementString(writer, "sFinalLAmt_rep", sFinalLAmt_rep);
            WriteElementString(writer, "sApprVal_rep", sApprVal_rep);
            WriteElementString(writer, "sOriginalAppraisedValue_rep", sOriginalAppraisedValue_rep);
            WriteElementString(writer, "sCreditLineAmt", sCreditLineAmt.ToString());
            WriteElementString(writer, "sRefPdOffAmt1003", sRefPdOffAmt1003.ToString());
            WriteElementString(writer, "sSpLien", sSpLien.ToString());
            WriteElementString(writer, "sBranchChannelT", sBranchChannelT.ToString("D"));
        }

        public override void ReadXml(XmlReader reader)
        {
            reader.MoveToContent();
            reader.ReadStartElement(); //consume opening tag

            while (reader.NodeType == XmlNodeType.Element)
            {
                switch (reader.Name)
                {
                    case "sLNm":
                        sLNm = reader.ReadElementSafe();
                        break;
                    case "sSpAddr":
                        sSpAddr = reader.ReadElementSafe();
                        break;
                    case "sSpCity":
                        sSpCity = reader.ReadElementSafe();
                        break;
                    case "sSpState":
                        sSpState = reader.ReadElementSafe();
                        break;
                    case "sSpZip":
                        sSpZip = reader.ReadElementSafe();
                        break;
                    case "sSpCounty":
                        sSpCounty = reader.ReadElementSafe();
                        break;
                    case "sSpCountyFips":
                        sSpCountyFips = reader.ReadElementSafe();
                        break;
                    case "sLPurposeT":
                        sLPurposeT = reader.ReadAsEnum<E_sLPurposeT>();
                        break;
                    case "sLienPosT":
                        sLienPosT = reader.ReadAsEnum<E_sLienPosT>();
                        break;
                    case "sFinMethT":
                        sFinMethT = reader.ReadAsEnum<E_sFinMethT>();
                        break;
                    case "sIsOptionArm":
                        sIsOptionArm = bool.Parse(reader.ReadElementSafe());
                        break;
                    case "sProdSpT":
                        sProdSpT = reader.ReadAsEnum<E_sProdSpT>();
                        break;
                    case "sProdSpStructureT":
                        sProdSpStructureT = reader.ReadAsEnum<E_sProdSpStructureT>();
                        break;
                    case "sFinalLAmt":
                        sFinalLAmt = reader.ReadElementAsDecimalSafe() ;
                        break;
                    case "sPurchPrice":
                        sPurchPrice = reader.ReadElementAsDecimalSafe();
                        break;
                    case "sApprVal":
                        sApprVal = reader.ReadElementAsDecimalSafe();
                        break;
                    case "sOriginalAppraisedValue":
                        sOriginalAppraisedValue = reader.ReadElementAsDecimalSafe();
                        break;
                    case "sLT":
                        sLT = reader.ReadAsEnum<E_sLT>();
                        break;
                    case "sLAmtCalc_rep":
                        sLAmtCalc_rep = reader.ReadElementSafe();
                        break;
                    case "sFinalLAmt_rep":
                        sFinalLAmt_rep = reader.ReadElementSafe();
                        break;
                    case "sApprVal_rep":
                        sApprVal_rep = reader.ReadElementSafe();
                        break;
                    case "sOriginalAppraisedValue_rep":
                        sOriginalAppraisedValue_rep = reader.ReadElementSafe();
                        break;
                    case "sCreditLineAmt":
                        sCreditLineAmt = reader.ReadElementAsDecimalSafe();
                        break;
                    case "sRefPdOffAmt1003":
                        sRefPdOffAmt1003 = reader.ReadElementAsDecimalSafe();
                        break;
                    case "sSpLien":
                        sSpLien = reader.ReadElementAsDecimalSafe();
                        break;
                    case "sBranchChannelT":
                        sBranchChannelT = reader.ReadAsEnum<E_BranchChannelT>();
                        break;
                    default:
                        throw new NotImplementedException(reader.Name);
                }
            }

            reader.ReadEndElement();
        }
    }
}
