﻿namespace LendersOffice.ObjLib.TitleProvider
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;

    using DataAccess;
    using EDocs;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Edocs.AutoSaveDocType;
    using LendersOffice.ObjLib.TitleProvider.FirstAmerican;
    using LQBTitle.VendorXml;

    public class PolicyInfo
    {
        public PolicyInfo(DbDataReader reader)
        {
            this.Id = (string)reader["PolicyId"];
            this.Name = (string)reader["PolicyName"];
            this.PolicyT = (E_cTitleInsurancePolicyT)(int)reader["PolicyType"];
        }

        public PolicyInfo(string id, string name, E_cTitleInsurancePolicyT policyType)
        {
            this.Id = id;
            this.Name = name;
            this.PolicyT = policyType;
        }

        public string Id { get; private set; }

        public string Name { get; private set; }

        public E_cTitleInsurancePolicyT PolicyT { get; private set; }
    }

    public class TitleServiceResult
    {
        public TitleServiceResult(bool isSuccess, string resultMessage)
        {
            this.Success = isSuccess;
            this.ResultMessage = resultMessage;
        }

        public string ResultMessage { get; private set; }

        public bool Success { get; private set; }
    }

    public abstract partial class TitleService
    {
        public TitleService(BrokerVendorAssociation association)
        {
            this.UserNameQuote = association.UsernameQuote;
            this.PasswordQuote = association.PasswordQuote;
            this.ClientIdQuote = association.AccountIdQuote;

            this.UserNameTitle = association.UsernameTitle;
            this.PasswordTitle = association.PasswordTitle;
            this.ClientIdTitle = association.AccountIdTitle;

            this.BrokerId = association.BrokerId;
            this.VendorId = association.VendorId;

            this.ExportPathTitle = association.ExportPathTitle;
            this.ExportPathQuote = association.ExportPathQuote;
        }

        protected string UserNameQuote { get; private set; }

        protected string PasswordQuote { get; private set; }

        protected string ClientIdQuote { get; private set; }

        protected string UserNameTitle { get; private set; }

        protected string PasswordTitle { get; private set; }

        protected string ClientIdTitle { get; private set; }

        protected string ExportPathQuote { get; set; }

        protected string ExportPathTitle { get; private set; }

        protected Guid BrokerId { get; private set; }

        protected int VendorId { get; private set; }

        public TitleServiceResult TitleServiceResult { get; protected set; }

        /// <summary>
        /// Retrieves the save quote along with policy info for a loan. 
        /// </summary>
        /// <param name="brokerId">The broker of the loan.</param>
        /// <param name="sLId">Loan id of the loan.</param>
        /// <returns>Quote log of the order.</returns>
        public static QuoteLog GetQuoteOrderLog(Guid brokerId, Guid sLId)
        {
            BrokerVendorAssociation info = null;
            string xml = null;

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@sLId", sLId)
                                        };
            var titleVendorList = TitleProvider.GetProviders();

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "TITLE_VENDOR_RESPONSE_GetInfo", parameters))
            {
                if (reader.Read())
                {
                    info = new BrokerVendorAssociation(reader, titleVendorList);
                    xml = (string)reader["TitleXml"];
                }
            }

            if (string.IsNullOrEmpty(xml))
            {
                return null;
            }

            TitleService service = GetServiceFor(info);
            QuoteLog log = new QuoteLog();

            using (StringReader sr = new StringReader(xml))
            {
                using (XmlReader xr = XmlReader.Create(sr))
                {
                    log.ReadXml(xr);
                    service.PopulateDetails(log);
                }
            }

            return log;
        }

        public static TitleService GetServiceFor(BrokerVendorAssociation info)
        {
            switch (info.TitleVendorIntegrationType)
            {
                case E_TitleVendorIntegrationType.FirstAmerican:
                    return new FirstAmericanServiceV3(info);
                default:
                    throw new NotImplementedException(info.VendorName);
            }
        }

        public static void ApplySavedQuote(Guid brokerId, Guid sLId)
        {
            QuoteLog log = GetQuoteOrderLog(brokerId, sLId);
            CPageData loanData = CPageData.CreateUsingSmartDependency(sLId, typeof(TitleService));
            loanData.InitSave(ConstAppDavid.SkipVersionCheck);
            ApplyQuote(loanData, log.Results, null, null);
            loanData.Save();
        }

        public static IReadOnlyCollection<TitleFeeQuoteApplicationRecord> ApplyQuote(
            CPageData loanData,
            QuoteResultData results,
            Action<LoanClosingCostFee> logFunc,
            Action<string, string> legacyLogFunc)
        {
            // Do note that most of the fee application logic is duplicated between here and LendersOffice.Integration.TitleFramework.TitleIntegrationQuote
            // Please be sure to update both locations when changing logic.
            if (loanData.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
            {
                ApplyLegacyTitleFees(loanData, results, legacyLogFunc);
                return null;
            }
            else
            {
                bool enableSubFees = loanData.BrokerDB.EnableSubFees;
                bool importBorrowerAndSellerFees = loanData.BrokerDB.ImportFirstAmericanBorrowerAndSellerFees;
                List<TitleFeeQuoteApplicationRecord> feeRecords = new List<TitleFeeQuoteApplicationRecord>();
                foreach (var titleFee in results.Fees)
                {
                    if (enableSubFees)
                    {
                        feeRecords.AddRange(SplitTitleFeeAndApplyToClosingCostSet(titleFee, loanData, importBorrowerAndSellerFees, logFunc));
                    }
                    else
                    {
                        feeRecords.Add(ApplyTitleFeeToClosingCostSet(titleFee, loanData, importBorrowerAndSellerFees, logFunc));
                    }
                }

                return feeRecords;
            }
        }

        private static void ApplyLegacyTitleFees(CPageData loanData, QuoteResultData results, Action<string, string> logFunc)
        {
            if (results.IsEscrowFSet)
            {
                loanData.sEscrowF = results.sEscrowF;

                if (logFunc != null)
                {
                    logFunc("sEscrowFMb", loanData.sEscrowF_rep);
                }
            }

            if (results.IsNotaryFSet)
            {
                loanData.sNotaryF = results.sNotaryF;

                if (logFunc != null)
                {
                    logFunc("sNotaryF", loanData.sNotaryF_rep);
                }
            }

            if (results.IsOwnerTitleInsFSet)
            {
                loanData.sOwnerTitleInsF = results.sOwnerTitleInsF;

                if (logFunc != null)
                {
                    logFunc("sOwnerTitleInsFMb", loanData.sOwnerTitleInsF_rep);
                }
            }

            if (results.IsTitleInsFSet)
            {
                loanData.sTitleInsF = results.sTitleInsF;
                if (logFunc != null)
                {
                    logFunc("sTitleInsFMb", loanData.sTitleInsF_rep);
                }
            }

            if (results.IsRecFSet)
            {
                loanData.sRecFLckd = true;
                loanData.sRecFPc = 0m;
                loanData.sRecFMb = results.sRecF;

                if (logFunc != null)
                {
                    logFunc("sRecFMb", loanData.sRecFMb_rep);
                }
            }

            if (results.IsCountyRtcSet)
            {
                loanData.sCountyRtcPc = 0;
                loanData.sCountyRtcMb = results.sCountyRtc;

                if (logFunc != null)
                {
                    logFunc("sCountyRtcMb", loanData.sCountyRtcMb_rep);
                }
            }

            if (results.IsStateRtcSet)
            {
                loanData.sStateRtcMb = results.sStateRtc;
                loanData.sStateRtcPc = 0;

                if (logFunc != null)
                {
                    logFunc("sStateRtcMb", loanData.sStateRtcMb_rep);
                }
            }
        }

        private static IReadOnlyCollection<TitleFeeQuoteApplicationRecord> SplitTitleFeeAndApplyToClosingCostSet(
            QuoteResultData.TitleFee titleFee,
            CPageData loanData,
            bool importSellerFee,
            Action<LoanClosingCostFee> logFunc)
        {
            var feeRecords = new List<TitleFeeQuoteApplicationRecord>();

            // If the fee is not set or has no values, then do nothing.
            if (!titleFee.IsSet || (!titleFee.BorrowerAmount.HasValue && !titleFee.SellerAmount.HasValue))
            {
                return feeRecords;
            }

            // Delete existing Fees from closing cost set.
            if (titleFee.BorrowerAmount.HasValue)
            {
                DeleteExistingFeesFromClosingCostSet(titleFee.LegacyGfeFieldT, loanData.sClosingCostSet);
            }

            if (titleFee.SellerAmount.HasValue)
            {
                DeleteExistingFeesFromClosingCostSet(titleFee.LegacyGfeFieldT, loanData.sSellerResponsibleClosingCostSet);
            }

            // Generate list of used HUD lines.
            HashSet<int> inUseHudLines = new HashSet<int>(loanData.sClosingCostSet.GetFees(null).Select(f => f.HudLine));
            inUseHudLines.UnionWith(loanData.sSellerResponsibleClosingCostSet.GetFees(null).Select(f => f.HudLine));

            // Get list of reserved HUD lines (may intersect with used HUD lines, which is fine). 
            HashSet<int> reservedHudLines = new HashSet<int>(loanData.sDynamicClosingCostFeeHistory.Items.Values.Select(i => i.HudLine));
            reservedHudLines.UnionWith(loanData.BrokerDB.GetUnlinkedClosingCostSet().GetFees(null).Select(f => f.HudLine));

            // Iterate through costs, and create a fee per cost.
            foreach (QuoteResultData.TitleSplitCost cost in titleFee.Costs)
            {
                LoanClosingCostFee borrowerFee = CreateSplitTitleFee(cost, titleFee, loanData, cost.BorrowerResponsibleAmount, loanData.sClosingCostSet, inUseHudLines, reservedHudLines);
                if (borrowerFee != null && logFunc != null)
                {
                    // Only log changes to borrower fees, since seller fees do not show up in the visible logs.
                    logFunc(borrowerFee);
                }

                LoanClosingCostFee sellerFee = null;
                if (importSellerFee)
                {
                    sellerFee = CreateSplitTitleFee(cost, titleFee, loanData, cost.SellerResponsibleAmount, loanData.sSellerResponsibleClosingCostSet, inUseHudLines, reservedHudLines);
                }

                // Add HUD line to in use list.
                // NOTE: Don't do this earlier or borrower and seller fees could end up having different HUD line numbers.
                inUseHudLines.Add(borrowerFee.HudLine);

                string sortPosition = loanData.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID
                            ? (borrowerFee?.HudLine ?? sellerFee?.HudLine ?? 0).ToString("D5")
                            : ClosingCostSetUtils.MapIntegratedDisclosureSectionToString(borrowerFee?.IntegratedDisclosureSectionT ?? sellerFee?.IntegratedDisclosureSectionT ?? E_IntegratedDisclosureSectionT.LeaveBlank);

                feeRecords.Add(new TitleFeeQuoteApplicationRecord(
                    sortPosition,
                    borrowerFee?.ClosingCostFeeTypeId ?? sellerFee?.ClosingCostFeeTypeId ?? Guid.Empty,
                    borrowerFee?.OriginalDescription ?? sellerFee?.OriginalDescription ?? titleFee.LegacyGfeFieldT.ToString(),
                    borrowerFee?.TotalAmount,
                    sellerFee?.TotalAmount,
                    new QuoteResultData.TitleSplitCost[] { cost }));
            }

            return feeRecords;
        }

        private static TitleFeeQuoteApplicationRecord ApplyTitleFeeToClosingCostSet(QuoteResultData.TitleFee titleFee, CPageData loanData, bool importSellerFee, Action<LoanClosingCostFee> logFunc)
        {
            LoanClosingCostFee borrowerFee = CreateMergedTitleFee(titleFee, loanData, titleFee.BorrowerAmount, loanData.sClosingCostSet);
            if (borrowerFee != null && logFunc != null)
            {
                // Only log changes to borrower fees, since seller fees do not show up in the visible logs.
                logFunc(borrowerFee);
            }

            LoanClosingCostFee sellerFee = null;
            if (importSellerFee)
            {
                sellerFee = CreateMergedTitleFee(titleFee, loanData, titleFee.SellerAmount, loanData.sSellerResponsibleClosingCostSet);
            }

            string sortPosition = loanData.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID
                        ? (borrowerFee?.HudLine ?? sellerFee?.HudLine ?? 0).ToString("D5")
                        : ClosingCostSetUtils.MapIntegratedDisclosureSectionToString(borrowerFee?.IntegratedDisclosureSectionT ?? sellerFee?.IntegratedDisclosureSectionT ?? E_IntegratedDisclosureSectionT.LeaveBlank);

            return new TitleFeeQuoteApplicationRecord(
                sortPosition,
                borrowerFee?.ClosingCostFeeTypeId ?? sellerFee?.ClosingCostFeeTypeId ?? Guid.Empty,
                borrowerFee?.OriginalDescription ?? sellerFee?.OriginalDescription ?? titleFee.LegacyGfeFieldT.ToString(),
                borrowerFee?.TotalAmount,
                sellerFee?.TotalAmount,
                titleFee.Costs);
        }

        private static LoanClosingCostFee CreateMergedTitleFee(QuoteResultData.TitleFee titleFee, CPageData loanData, decimal? feeAmount, LoanClosingCostSet closingCostSet)
        {
            if (!titleFee.IsSet || !feeAmount.HasValue)
            {
                return null;
            }

            LoanClosingCostFee loanFee = (LoanClosingCostFee)closingCostSet.GetFirstFeeByLegacyType(titleFee.LegacyGfeFieldT, loanData.sBrokerId, true);
            loanFee.FormulaT = E_ClosingCostFeeFormulaT.MinBaseOnly;
            loanFee.BaseAmount = feeAmount.Value;
            return loanFee;
        }

        private static LoanClosingCostFee CreateSplitTitleFee(
            QuoteResultData.TitleSplitCost cost,
            QuoteResultData.TitleFee titleFee,
            CPageData loanData,
            decimal? feeAmount,
            LoanClosingCostSet closingCostSet,
            HashSet<int> inUseHudLines,
            HashSet<int> reservedHudLines)
        {
            if (!titleFee.IsSet || !feeAmount.HasValue)
            {
                return null;
            }

            // Get and clone broker fee.
            LoanClosingCostFee newFee = closingCostSet.GetBrokerFee(loanData.sBrokerId, titleFee.LegacyGfeFieldT, false); // Get broker fee. Don't add yet.

            DynamicClosingCostFeeHistoryItem historyItem;
            loanData.sDynamicClosingCostFeeHistory.Items.TryGetValue(cost.Name, out historyItem);

            newFee.SourceFeeTypeId = newFee.ClosingCostFeeTypeId;
            newFee.ClosingCostFeeTypeId = historyItem?.ClosingCostFeeTypeId ?? Guid.NewGuid();
            newFee.Description = cost.Name;
            newFee.OriginalDescription = cost.Name;
            newFee.FormulaT = E_ClosingCostFeeFormulaT.MinBaseOnly;
            newFee.BaseAmount = feeAmount.Value;
            newFee.HudLine = GetHudLine(newFee.HudLine, closingCostSet, titleFee.LegacyGfeFieldT, historyItem, inUseHudLines, reservedHudLines);

            closingCostSet.AddOrUpdate(newFee);
            return newFee;
        }

        public static int GetHudLine(int sourceHudLine, LoanClosingCostSet closingCostSet, E_LegacyGfeFieldT legacyGfeFieldT, DynamicClosingCostFeeHistoryItem historyItem, HashSet<int> inUseHudLines, HashSet<int> reservedHudLines)
        {
            // Special case for Recording Fees: always use line 1202.
            if (legacyGfeFieldT == E_LegacyGfeFieldT.sRecF
                || legacyGfeFieldT == E_LegacyGfeFieldT.sRecDeed
                || legacyGfeFieldT == E_LegacyGfeFieldT.sRecMortgage
                || legacyGfeFieldT == E_LegacyGfeFieldT.sRecRelease)
            {
                return 1202;
            }

            // Largest sub fee gets source hud line. Since cost are already sorted
            // first fee encountered is largest fee, so just check if fee exists in closing cost set
            if (!closingCostSet.FindFeeByHudline(sourceHudLine).Any())
            {
                return sourceHudLine;
            }

            // Use HUD line from history if it exists and does not need to be reassigned.
            // Reassign hudline if historical hudline for this fee is:
            // 1) The source fee's HUD line - implies a special case that may no longer be true.
            // 2) In use by another fee - avoids possible collisions.
            if (historyItem != null && historyItem.HudLine != sourceHudLine && !inUseHudLines.Contains(historyItem.HudLine))
            {
                return historyItem.HudLine;
            }

            // Reassign hudline.
            int? hudLine = GetFirstAvailableHudLine(sourceHudLine, inUseHudLines, reservedHudLines);
            if (hudLine.HasValue)
            {
                return hudLine.Value;
            }

            // If we are here then we did not find an available HUD line. Use source HUD line as fallback (may cause duplicates).
            return sourceHudLine;
        }

        private static int? GetFirstAvailableHudLine(int sourceHudLine, HashSet<int> inUseHudLines, HashSet<int> reservedHudLines)
        {
            int? hudLine = null;

            switch(sourceHudLine / 100)
            {
                case 11:
                    // Search for usable HUD line in supported range.
                    hudLine = GetFirstAvailableHudLineInRange(1150, 1116, inUseHudLines, reservedHudLines);

                    // If no HUD line found in supported range, search unsupported range.
                    if (!hudLine.HasValue)
                    {
                        hudLine = GetFirstAvailableHudLineInRange(1198, 1151, inUseHudLines, reservedHudLines);
                    }

                    break;
                case 12:
                    // Search for usable HUD line in supported range.
                    hudLine = GetFirstAvailableHudLineInRange(1230, 1209, inUseHudLines, reservedHudLines);

                    // If no HUD line found in supported range, search unsupported range.
                    if (!hudLine.HasValue)
                    {
                        hudLine = GetFirstAvailableHudLineInRange(1298, 1131, inUseHudLines, reservedHudLines);
                    }

                    break;
            }

            return hudLine;
        }

        private static int? GetFirstAvailableHudLineInRange(int maxLineNumber, int minLineNumber, HashSet<int> inUseHudLines, HashSet<int> reservedHudLines)
        {
            int? freeHudLine = null;
            int? firstReservedButNotUsedHudLine = null;
            for (int i = maxLineNumber; i >= minLineNumber; i--)
            {
                // Exclude HUD lines already in use.
                if (inUseHudLines.Contains(i))
                {
                    continue;
                }

                // Avoid reserved HUD lines
                if (reservedHudLines.Contains(i))
                {
                    // Keep track of 1st available HUD line that is reserved but not in use on the loan.
                    // This HUD line will be assigned to the fee if a more suitable one cannot be found.
                    if (!firstReservedButNotUsedHudLine.HasValue)
                    {
                        firstReservedButNotUsedHudLine = i;
                    }

                    continue;
                }

                // If we got here then we found an available HUD line and can cut the loop short.
                freeHudLine = i;
                break;
            }

            return freeHudLine ?? firstReservedButNotUsedHudLine;
        }

        // Reassigns the given fees HUD line and updates fee history.
        // NOTE: This will keep current HUD line if no other HUD line is available.
        public static void ReassignHudLine(CPageBase loanBase, LoanClosingCostSet closingCostSet, LoanClosingCostFee fee)
        {
            // Generate list of used HUD lines.
            HashSet<int> inUseHudLines = new HashSet<int>(loanBase.sClosingCostSet.GetFees(null).Select(f => f.HudLine));
            inUseHudLines.UnionWith(loanBase.sSellerResponsibleClosingCostSet.GetFees(null).Select(f => f.HudLine));

            // Get list of reserved HUD lines (may intersect with used HUD lines, which is fine). 
            HashSet<int> reservedHudLines = new HashSet<int>(loanBase.sDynamicClosingCostFeeHistory.Items.Values.Select(i => i.HudLine));
            reservedHudLines.UnionWith(loanBase.BrokerDB.GetUnlinkedClosingCostSet().GetFees(null).Select(f => f.HudLine));

            int? hudLine = GetFirstAvailableHudLine(fee.HudLine, inUseHudLines, reservedHudLines);

            if (!hudLine.HasValue)
            {
                // Do Nothing.
                return;
            }

            fee.HudLine = hudLine.Value;
        }

        public static void DeleteExistingFeesFromClosingCostSet(E_LegacyGfeFieldT legacyGfeFieldT, LoanClosingCostSet closingCostSet)
        {
            // Delete existing fees from set (gets both standard and sub fees).
            IEnumerable<BaseClosingCostFee> feesToRemove = closingCostSet.GetFees(f => f.LegacyGfeFieldT == legacyGfeFieldT);
            foreach (BaseClosingCostFee feeToRemove in feesToRemove)
            {
                closingCostSet.Remove(feeToRemove);
            }
        }

        protected static LQBTitle.LQBXml.LqbTitleResponse GetLQBTitleResponse(string sResponseXML)
        {
            using (StringReader sReader = new StringReader(sResponseXML))
            using (XmlReader xmlReader = XmlReader.Create(sReader))
            {
                LQBTitle.LQBXml.LqbTitleResponse response = new LQBTitle.LQBXml.LqbTitleResponse();
                response.ReadXml(xmlReader);
                return response;
            }
        }

        protected static bool IsPurchase(E_sLPurposeT sLPurposeT)
        {
            bool isPurchase;
            switch (sLPurposeT)
            {
                case E_sLPurposeT.Purchase:
                case E_sLPurposeT.Construct:
                case E_sLPurposeT.ConstructPerm:
                    isPurchase = true;
                    break;
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.HomeEquity:
                    isPurchase = false;
                    break;
                case E_sLPurposeT.Other:
                default:
                    throw new UnhandledEnumException(sLPurposeT);
            }

            return isPurchase;
        }
    }

    /// <summary>
    /// Abstract/virtual members of the class.
    /// </summary>
    public abstract partial class TitleService
    {
        public abstract string ClosingCostDisclaimer { get; }

        public abstract bool SupportsQuote { get; }

        public abstract bool RequestPolicy { get; }

        public abstract void OrderQuote(Guid sLId, E_cTitleInsurancePolicyT cTitleInsurancePolicyT);

        public abstract void OrderPolicy(Guid sLId, string sOwnerPolicyName, string sLenderPolicyName, bool IsEscrowRequested);

        public virtual E_TitleVendorResponseRequestStatus ReceiveDocFromVendor(TitleVendorRequest request, out List<ServerMessage> serverMessages, out List<LenderDocSubmissionReceiptResponse> receivedItems)
        {
            Guid loanId = new Guid(request.Request.TitleVendorDocSubmissionRequest.LoanId);
            receivedItems = new List<LenderDocSubmissionReceiptResponse>();

            CPageData data = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(TitleService));
            data.InitSave(ConstAppDavid.SkipVersionCheck);

            serverMessages = new List<ServerMessage>();

            var saveAs = AutoSaveDocTypeFactory.GetDocTypeForPageId(E_AutoSavePage.MiscTitleDocuments, this.BrokerId, E_EnforceFolderPermissions.True);
            if (saveAs == null || saveAs.DocType == null)
            {
                serverMessages.Add(new ServerMessage()
                {
                    Description = "Account is not configured to accept title documents.",
                    Type = E_ServerMessageType.ServerError
                });
                Tools.LogErrorWithCriticalTracking("Broker not configured for title vendor request. " + this.BrokerId);
                return E_TitleVendorResponseRequestStatus.Error;
            }

            QuoteLog log = GetQuoteOrderLog(this.BrokerId, loanId);

            if (log == null)
            {
                throw CBaseException.GenericException("No Policy Order Request found for loan.");
            }

            foreach (var item in request.Request.TitleVendorDocSubmissionRequest.DocumentList)
            {
                if (item.Format != LQBTitle.VendorXml.E_DocumentFormat.PDF)
                {
                    serverMessages.Add(new ServerMessage()
                    {
                        Description = string.Format("Document {0} - Expected PDF Format", item.Name),
                        Type = E_ServerMessageType.DataError
                    });
                    continue;
                }

                if (string.IsNullOrEmpty(item.InnerText))
                {
                    serverMessages.Add(new ServerMessage()
                    {
                        Description = string.Format("Error: Document {0} - No base 64 content attached", item.Name),
                        Type = E_ServerMessageType.DataError
                    });
                }

                string path;
                try
                {
                    path = Utilities.Base64StringToFile(item.InnerText, "title_document");
                }
                catch (Exception asd)
                {
                    serverMessages.Add(new ServerMessage()
                    {
                        Description = string.Format("Error: Document {0} - Base64 encoded content expected.", item.Name),
                        Type = E_ServerMessageType.DataError
                    });
                    Tools.LogError(asd);
                    continue;
                }

                try
                {
                    string name = item.MismoDocClass ?? string.Empty;
                    if (!string.IsNullOrEmpty(item.Name))
                    {
                        if (name.Length > 0)
                        {
                            name += "-" + item.Name;
                        }
                    }

                    Guid documentId = AutoSaveDocTypeFactory.SavePdfDocAsSystem(E_AutoSavePage.MiscTitleDocuments, path, this.BrokerId, loanId, data.GetAppData(0).aAppId, name);
                    log.DocumentReceipts.Add(new DocumentReceipt()
                    {
                        DocumentName = item.Name,
                        EDocumentId = documentId,
                        MismoClass = item.MismoDocClass,
                        ReceivedD = DateTime.Now
                    });
                }
                catch (InsufficientPdfPermissionException)
                {
                    serverMessages.Add(new ServerMessage()
                    {
                        Description = string.Format("Document {0} - Password protected.", item.Name),
                        Type = E_ServerMessageType.DataError
                    });
                }
                catch (InvalidPDFFileException)
                {
                    serverMessages.Add(new ServerMessage()
                    {
                        Description = string.Format("Document {0} - Invalid PDF.", item.Name),
                        Type = E_ServerMessageType.DataError
                    });
                }
                catch (CBaseException)
                {
                    serverMessages.Add(new ServerMessage()
                    {
                        Description = string.Format("Document {0} - Invalid PDF.", item.Name),
                        Type = E_ServerMessageType.DataError
                    });
                }
                catch (Exception e)
                {
                    serverMessages.Add(new ServerMessage()
                    {
                        Description = string.Format("Document {0} - Invalid pdf.", item.Name),
                        Type = E_ServerMessageType.DataError
                    });
                    Tools.LogError(e);
                }

                receivedItems.Add(new LenderDocSubmissionReceiptResponse()
                {
                    DocumentName = item.Name
                });
            }

            data.sPrelimRprtRd_rep = DateTime.Now.ToString();
            data.Save();
            this.SaveQuoteLog(loanId, log);
            return serverMessages.Count > 0 ? E_TitleVendorResponseRequestStatus.Error : E_TitleVendorResponseRequestStatus.Success;
        }

        public abstract void SendDocToVendor(Guid sLId, Guid documentId);

        public abstract E_cTitleInsurancePolicyT[] GetAvailablePolicyTypes(E_sLPurposeT sLPurposeT, string sSpState);

        /// <summary>
        /// Get quotes for Owner/Lender and Simultaneous if needed for a loan. 
        /// For Refi only owner quotes are needed so nothing else is required. 
        /// </summary>
        /// <param name="data"></param>
        public abstract QuoteResultData GetQuotes(QuoteRequestData data, QuoteRequestOptions options);

        public abstract bool AreQuotesStillValid(QuoteRequestData initialData, QuoteRequestData currentData);

        /// <summary>
        /// Allows the provider to add notes to the quote log to display on the result page.
        /// </summary>
        /// <param name="log"></param>
        protected abstract void PopulateDetails(QuoteLog log);
    }

    /// <summary>
    /// Instance members of the class.
    /// </summary>
    public abstract partial class TitleService
    {
        public IEnumerable<PolicyInfo> GetPolicies(E_sLPurposeT sLPurposeT, string sSpState)
        {
            bool isPurchase = IsPurchase(sLPurposeT);

            List<PolicyInfo> policies = new List<PolicyInfo>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@State", sSpState),
                                            new SqlParameter("@TitleVendorId", this.VendorId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "TITLE_VENDOR_POLICY_Get", parameters))
            {
                while (reader.Read())
                {
                    PolicyInfo p = new PolicyInfo(reader);
                    if (!isPurchase && p.PolicyT == E_cTitleInsurancePolicyT.Lender)
                    {
                        continue;
                    }

                    policies.Add(p);
                }
            }

            return policies;
        }

        protected void SaveQuoteLog(Guid sLId, QuoteLog log)
        {
            StringBuilder sb = new StringBuilder();
            using (StringWriter sw = new StringWriter(sb))
            using (XmlWriter xw = XmlWriter.Create(sw, new XmlWriterSettings() { OmitXmlDeclaration = true }))
            {
                log.WriteXml(xw);
            }

            this.SaveTitleInfo(sLId, sb.ToString());
        }

        private void SaveTitleInfo(Guid sLId, string xml)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@sLId", sLId),
                                            new SqlParameter("@RequestResponseXml", xml),
                                            new SqlParameter("@BrokerId", this.BrokerId),
                                            new SqlParameter("@TitleVendorId", this.VendorId)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "TITLE_VENDOR_RESPONSE_SaveInfo", 3, parameters);
        }
    }
}