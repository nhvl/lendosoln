﻿namespace LendersOffice.ObjLib.TitleProvider
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Common;

    public sealed class QuoteResultData
    {
        public QuoteResultData()
            : this(new TitleFeeBuilder(), false, string.Empty)
        {
        }

        public QuoteResultData(TitleFeeBuilder feeBuilder, bool missingRecordingDocs, string titleDebugInfo)
        {
            this.Fees = feeBuilder.ToTitleFeeList().ToList();
            this.MissingRecordingDocs = missingRecordingDocs;
            this.TitleDebugInfo = titleDebugInfo;
        }

        public IReadOnlyCollection<TitleFee> Fees { get; }

        public bool MissingRecordingDocs { get; private set; }

        public string TitleDebugInfo { get; private set; }

        public bool HasError { get; set; }

        private decimal GetBorrowerFeeAmount(E_LegacyGfeFieldT gfeFieldType)
        {
            return this.Fees.SingleOrDefault(f => f.LegacyGfeFieldT == gfeFieldType)?.BorrowerAmount ?? 0;
        }

        private bool GetBorrowerFeeIsSet(E_LegacyGfeFieldT gfeFieldType)
        {
            TitleFee fee = this.Fees.SingleOrDefault(f => f.LegacyGfeFieldT == gfeFieldType);
            return fee != null && fee.IsSet && fee.BorrowerAmount.HasValue;
        }

        public bool HasZeroFees
        {
            get { return this.Fees.All(fee => !fee.IsSet) || this.Fees.All(fee => !fee.BorrowerAmount.HasValue || fee.BorrowerAmount == 0); }
        }

        // 1102
        public decimal sEscrowF
        {
            get { return this.GetBorrowerFeeAmount(E_LegacyGfeFieldT.sEscrowF); }
            //set { m_sEscrowF = value;  }
        }

        // 1103
        public decimal sOwnerTitleInsF
        {
            get { return this.GetBorrowerFeeAmount(E_LegacyGfeFieldT.sOwnerTitleInsF); }
            //set { m_sOwnerTitleInsF = value; }
        }

        // 1104
        public decimal sTitleInsF
        {
            get { return this.GetBorrowerFeeAmount(E_LegacyGfeFieldT.sTitleInsF); }
            //set { m_sTitleInsF = value; }
        }

        // 1201
        public decimal sRecF
        {
            get
            {
                return CPageBase.Calculate_Unlocked_sRecF(
                  this.GetBorrowerFeeAmount(E_LegacyGfeFieldT.sRecDeed),
                  this.GetBorrowerFeeAmount(E_LegacyGfeFieldT.sRecMortgage),
                  this.GetBorrowerFeeAmount(E_LegacyGfeFieldT.sRecRelease));
            }
            //set { m_sRecF = value; }
        }

        // 1204
        public decimal sCountyRtc
        {
            get { return this.GetBorrowerFeeAmount(E_LegacyGfeFieldT.sCountyRtc); }
            //set { m_sCountyRtc = value; }
        }

        // 1205
        public decimal sStateRtc
        {
            get { return this.GetBorrowerFeeAmount(E_LegacyGfeFieldT.sStateRtc); }
            //set { m_sStateRtc = value; }
        }

        // 1111
        public decimal sNotaryF
        {
            get { return this.GetBorrowerFeeAmount(E_LegacyGfeFieldT.sNotaryF); }
            //set { m_sNotaryF = value; }
        }

        public bool IsEscrowFSet
        {
            get { return this.GetBorrowerFeeIsSet(E_LegacyGfeFieldT.sEscrowF); }
            //set { m_sEscrowFSet = value; }
        }

        public bool IsOwnerTitleInsFSet
        {
            get { return this.GetBorrowerFeeIsSet(E_LegacyGfeFieldT.sOwnerTitleInsF); }
            //set { m_sOwnerTitleInsFSet = value; }
        }

        public bool IsTitleInsFSet
        {
            get { return this.GetBorrowerFeeIsSet(E_LegacyGfeFieldT.sTitleInsF); }
            //set { m_sTitleInsFSet = value; }
        }

        public bool IsRecFSet
        {
            get { return this.GetBorrowerFeeIsSet(E_LegacyGfeFieldT.sRecF); }
            //set { m_sRecFSet = value; }
        }

        public bool IsCountyRtcSet
        {
            get { return this.GetBorrowerFeeIsSet(E_LegacyGfeFieldT.sCountyRtc); }
            //set { m_sCountyRtcSet = value; }
        }

        public bool IsStateRtcSet
        {
            get { return this.GetBorrowerFeeIsSet(E_LegacyGfeFieldT.sStateRtc); }
            //set { m_sStateRtcSet = value; }
        }

        public bool IsNotaryFSet
        {
            get { return this.GetBorrowerFeeIsSet(E_LegacyGfeFieldT.sNotaryF); }
            //set { m_sNotaryFSet = value; }
        }

        /// <summary>
        /// Represents a simple, immutable fee within the quote result.
        /// </summary>
        public class TitleFee
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TitleFee"/> class.
            /// </summary>
            public TitleFee(E_LegacyGfeFieldT legacyGfeFieldT, string legacyFeeName, bool isSet, IReadOnlyCollection<TitleSplitCost> costs)
            {
                if (costs == null)
                {
                    throw new ArgumentNullException("costs");
                }

                this.LegacyGfeFieldT = legacyGfeFieldT;
                this.LegacyFeeName = legacyFeeName;
                this.IsSet = isSet;
                this.Costs = costs.ToArray();
                foreach (TitleSplitCost cost in costs)
                {
                    if (cost.BorrowerResponsibleAmount.HasValue)
                    {
                        this.BorrowerAmount = (this.BorrowerAmount ?? 0) + cost.BorrowerResponsibleAmount;
                    }

                    if (cost.SellerResponsibleAmount.HasValue)
                    {
                        this.SellerAmount = (this.SellerAmount ?? 0) + cost.SellerResponsibleAmount;
                    }
                }
            }

            /// <summary>
            /// Gets the borrower's amount of the fee.
            /// </summary>
            public decimal? BorrowerAmount { get; }

            /// <summary>
            /// Gets the seller's amount of the fee.
            /// </summary>
            public decimal? SellerAmount { get; }

            /// <summary>
            /// Gets a value indicating whether or not the fee was logically set during the title integration.
            /// </summary>
            public bool IsSet { get; }

            /// <summary>
            /// Gets the GFE field corresponding to this fee.
            /// </summary>
            public E_LegacyGfeFieldT LegacyGfeFieldT { get; }

            /// <summary>
            /// Gets the field id in <see cref="CPageData"/> corresponding to the field this fee represents.
            /// </summary>
            public string LegacyFeeName { get; }

            /// <summary>
            /// Gets the costs associated with the title fee, containing the borrower and seller split.
            /// </summary>
            public IReadOnlyCollection<TitleSplitCost> Costs { get; }
        }

        public class TitleSplitCost
        {
            /// <summary>
            /// The name of the cost to the title quote.
            /// </summary>
            public readonly string Name;

            /// <summary>
            /// The borrower-responsible amount of the cost to the title quote.
            /// </summary>
            public readonly decimal? BorrowerResponsibleAmount;

            /// <summary>
            /// The seller-responsible amount of the cost to the title quote.
            /// </summary>
            public readonly decimal? SellerResponsibleAmount;

            /// <summary>
            /// The amount of the cost for which others are responsible in the title quote.
            /// </summary>
            public readonly decimal? OtherResponsibleAmount;

            /// <summary>
            /// The total amount of the cost, from all responsible parties.
            /// </summary>
            public decimal TotalAmount
            {
                get { return (this.BorrowerResponsibleAmount ?? 0M) + (this.SellerResponsibleAmount ?? 0M) + (this.OtherResponsibleAmount ?? 0M); }
            }

            public TitleSplitCost(string name, decimal? borrowerResponsibleAmount, decimal? sellerResponsibleAmount, decimal? otherResponsibleAmount)
            {
                this.Name = name;
                this.BorrowerResponsibleAmount = borrowerResponsibleAmount;
                this.SellerResponsibleAmount = sellerResponsibleAmount;
                this.OtherResponsibleAmount = otherResponsibleAmount;
            }
        }

        public class TitleFeeBuilder
        {
            private Dictionary<E_LegacyGfeFieldT, List<TitleSplitCost>> feeLookup = new Dictionary<E_LegacyGfeFieldT, List<TitleSplitCost>>();

            /// <summary>
            /// Gets a dictionary containing the fees that have been set by the Title Vendor.  Used to prevent partial results.
            /// </summary>
            public Dictionary<E_LegacyGfeFieldT, bool> FeesSetByTheTitleVendor { get; } = new Dictionary<E_LegacyGfeFieldT, bool>();

            public void AddCost(E_LegacyGfeFieldT gfeFieldType, TitleSplitCost cost)
            {
                List<TitleSplitCost> feeCostList;
                if (!this.feeLookup.TryGetValue(gfeFieldType, out feeCostList))
                {
                    feeCostList = new List<TitleSplitCost>();
                    this.feeLookup.Add(gfeFieldType, feeCostList);
                }

                feeCostList.Add(cost);
            }

            public IReadOnlyCollection<TitleFee> ToTitleFeeList()
            {
                bool isSet;
                return this.feeLookup.Select(fee => new TitleFee(
                    fee.Key,
                    LookupLegacyFeeId(fee.Key),
                    this.FeesSetByTheTitleVendor.TryGetValue(fee.Key, out isSet) && isSet,
                    fee.Value.OrderByDescending(c => c.BorrowerResponsibleAmount).ThenBy(c => c.Name).ToList())).ToList(); // OPM 457222 - Sort costs by amount, then by name.
            }

            private static string LookupLegacyFeeId(E_LegacyGfeFieldT gfeFieldType)
            {
                switch (gfeFieldType)
                {
                    case E_LegacyGfeFieldT.sEscrowF: return "sEscrowFMb";
                    case E_LegacyGfeFieldT.sOwnerTitleInsF: return "sOwnerTitleInsFMb";
                    case E_LegacyGfeFieldT.sTitleInsF: return "sTitleInsFMb";
                    case E_LegacyGfeFieldT.sRecF: return "sRecFMb";
                    case E_LegacyGfeFieldT.sCountyRtc: return "sCountyRtcMb";
                    case E_LegacyGfeFieldT.sStateRtc: return "sStateRtcMb";
                    case E_LegacyGfeFieldT.sNotaryF: return "sNotaryF";
                    case E_LegacyGfeFieldT.sRecDeed: return "sRecDeed";
                    case E_LegacyGfeFieldT.sRecMortgage: return "sRecMortgage";
                    case E_LegacyGfeFieldT.sRecRelease: return "sRecRelease";
                    default:
                        throw new UnhandledEnumException(gfeFieldType);
                }
            }
        }
    }
}