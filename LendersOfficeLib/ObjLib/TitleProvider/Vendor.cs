﻿using System;
using System.Data;
using LendersOffice.Common;
using LqbGrammar.DataTypes;

namespace LendersOffice.ObjLib.TitleProvider
{
    public sealed class Vendor
    {

        private Lazy<string> lazyLqbServicePassword = new Lazy<string>(() => string.Empty);

        private int? m_id;

        public string Name { get;  set; }
        public string ExportPath { get; set; }
        public string ExportPathTitle { get; set; }
        public bool RequiresAccountId { get;  set; }
        public E_TitleVendorIntegrationType TitleVendorIntegrationType { get; set; }
        public string LQBServiceUserName { get; set; }
        public string LQBServicePassword
        {
            get { return this.lazyLqbServicePassword.Value; }
            set { this.lazyLqbServicePassword = new Lazy<string>(() => value); }
        }

        public string LQBServiceSourceName { get; set; }
        public bool EnableConnectionTest { get; set; }

        public int Id
        {
            get { return m_id.Value; }
            set
            {
                if (value >= 0) { 
                    m_id = value; 
                }
            }
        }
        public bool HasId { get { return m_id.HasValue; } }
        internal EncryptionKeyIdentifier EncryptionKeyId { get; private set; }

        internal Vendor(IDataRecord reader)
        {
            this.m_id = (int)reader["TitleVendorId"];
            this.Name = (string)reader["TitleVendorName"];
            this.RequiresAccountId = (bool)reader["TitleVendorRequiresAccountId"];
            this.ExportPath = (string)reader["TitleVendorExportPath"];
            this.ExportPathTitle = (string)reader["TitleVendorExportPathTitle"];
            this.TitleVendorIntegrationType = (E_TitleVendorIntegrationType)reader["TitleVendorIntegrationType"];
            this.LQBServiceUserName = (string)reader["LQBServiceUserName"];
            var maybeEncryptionKeyId = EncryptionKeyIdentifier.Create((Guid)reader["EncryptionKeyId"]);
            if (maybeEncryptionKeyId.HasValue)
            {
                this.EncryptionKeyId = maybeEncryptionKeyId.Value;
                byte[] encryptedLQBServicePassword = (byte[])reader["EncryptedLQBServicePassword"];
                this.lazyLqbServicePassword = new Lazy<string>(() => EncryptionHelper.DecryptString(maybeEncryptionKeyId.Value, encryptedLQBServicePassword));
            }
            else
            {
                string unencryptedLQBServicePassword = (string)reader["LQBServicePassword"];
                this.lazyLqbServicePassword = new Lazy<string>(() => unencryptedLQBServicePassword);
            }

            this.LQBServiceSourceName = (string)reader["LQBServiceSourceName"];
            this.EnableConnectionTest = (bool)reader["EnableConnectionTest"];
        }

        public Vendor()
        {
            m_id = new int?();
        }

        internal void SetupNewEncryptionKey(EncryptionKeyIdentifier encryptionKeyId)
        {
            if (encryptionKeyId != default(EncryptionKeyIdentifier))
            {
                this.EncryptionKeyId = encryptionKeyId;
            }
        }
    }
}
