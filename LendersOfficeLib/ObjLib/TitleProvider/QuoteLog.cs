﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using LendersOffice.Common;


namespace LendersOffice.ObjLib.TitleProvider
{


    public class DocumentReceipt : XmlSerializableCommon.AbstractXmlSerializable
    {

        public DateTime ReceivedD { get; set; }
        public string DocumentName { get; set; }
        public string MismoClass { get; set; }
        public Guid EDocumentId { get; set; }


        public override void ReadXml(System.Xml.XmlReader reader)
        {
            reader.MoveToContent();
            ReceivedD = new DateTime(long.Parse(reader["receivedd"]));
            DocumentName = reader["documentname"];
            MismoClass = reader["mismoclass"];
            EDocumentId = new Guid(reader["edocumentid"]);
            bool isEmpty = reader.IsEmptyElement;
            reader.ReadStartElement();

            if (!isEmpty)
            {
                reader.ReadEndElement();
            }
        }

        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            WriteAttribute(writer, "documentname", DocumentName ?? "");
            WriteAttribute(writer, "mismoclass", MismoClass ?? "");
            WriteAttribute(writer, "edocumentid", EDocumentId.ToString());
            WriteAttribute(writer, "receivedd", ReceivedD.Ticks.ToString());
        }
    }

    public class RecordingOfficeInfo : XmlSerializableCommon.AbstractXmlSerializable
    {
        public string Attention { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        public RecordingOfficeInfo()
        {
            Attention = "";
            Phone = "";
            Fax = "";
            Address = "";
            City = "";
            State = "";
            Zip = "";
        }

        public override void ReadXml(System.Xml.XmlReader reader)
        {
            reader.MoveToContent();

            bool isEmpty = reader.IsEmptyElement;
            reader.ReadStartElement();
            if (isEmpty)
            {
                return;
            }
            while (reader.NodeType == System.Xml.XmlNodeType.Element)
            {
                switch (reader.Name)
                {
                    case "Attention":
                        Attention = reader.ReadElementSafe();
                        break;
                    case "Phone":
                        Phone = reader.ReadElementSafe();
                        break;
                    case "Fax":
                        Fax = reader.ReadElementSafe();
                        break;
                    case "Address":
                        Address = reader.ReadElementSafe();
                        break;
                    case "City":
                        City = reader.ReadElementSafe();
                        break;
                    case "State":
                        State = reader.ReadElementSafe();
                        break;
                    case "Zip":
                        Zip = reader.ReadElementSafe();
                        break;
                    default:
                        throw new NotImplementedException(reader.Name);
                }
            }

            reader.ReadEndElement();
        }

        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            WriteElementString(writer, "Attention", Attention);
            WriteElementString(writer, "Phone", Phone);
            WriteElementString(writer, "Fax", Fax);
            WriteElementString(writer, "Address", Address);
            WriteElementString(writer, "City", City);
            WriteElementString(writer, "State", State);
            WriteElementString(writer, "Zip", Zip);
        }
    }

    /// <summary>
    /// Represents an ordered quote that has been applied to the loan file. 
    /// The xml request can be logged here per integration along with any other custom data that is needed. 
    /// </summary>
    public sealed class QuoteLog : XmlSerializableCommon.AbstractXmlSerializable
    {
        /// <summary>
        /// Classifications for the details.
        /// </summary>
        public enum E_QuoteDetailType
        {
            INFO = 0,
            WARNING = 1
        }

        /// <summary>
        /// Extra info 
        /// </summary>
        public class QuoteDetail
        {
            public string Detail { get; set; }
            public E_QuoteDetailType Classification { get; set; }
        }

        public string QuoteProviderName { get; set; }

        public string PolicyProviderName { get; set; }

        private class ExtraItem : XmlSerializableCommon.AbstractXmlSerializable
        {
            public string Name { get; set; }
            public string Value { get; set; }

            public override void ReadXml(System.Xml.XmlReader reader)
            {
                reader.MoveToContent();
                Name = ReadAttribute(reader, "name");
                Value = reader.ReadElementSafe();
            }

            public override void WriteXml(System.Xml.XmlWriter writer)
            {
                writer.WriteAttributeString ( "name", Name);
                writer.WriteCData(Value);
            }
        }
        private Dictionary<string, ExtraItem> m_IntegrationSpecificItems = new Dictionary<string, ExtraItem>();

        
        public QuoteLog()
        {
            m_IntegrationSpecificItems = new Dictionary<string, ExtraItem>();
            LoanData = new QuoteRequestData();
            Results = new QuoteResultData();
            RecordingOffice = new RecordingOfficeInfo();
            Details = new List<QuoteDetail>();
            WereClosingCostRequested = false;
            DocumentReceipts = new List<DocumentReceipt>();
        }
        public RecordingOfficeInfo RecordingOffice { get; set; }
        public QuoteRequestData LoanData { get; set; }
        public QuoteResultData Results { get; set; }
        public List<DocumentReceipt> DocumentReceipts { get; private set; }
        public List<QuoteDetail> Details { get; private set; }

        public DateTime? PolicyOrderedD { get; set; }
        public string PolicyID { get; set; }
        public string TransactionID { get; set; }
        public string QuoteID { get; set; }
        public DateTime QuoteOrderedDate { get; set; }
        public bool WereClosingCostRequested { get; set; }
        public bool IsAPolicyOrderLog { get; set; }

        public void AddItem(string key, string value)
        {
            m_IntegrationSpecificItems.Add(key, new ExtraItem() { Name = key, Value = value } );
        }

        public string GetItem(string key)
        {
            return m_IntegrationSpecificItems[key].Value;
        }

        public bool HasItem(string key)
        {
            return m_IntegrationSpecificItems.ContainsKey(key);
        }

        public void AddDetail(E_QuoteDetailType detailType, string detail)
        {
            Details.Add(new QuoteDetail()
            {
                Classification = detailType,
                Detail = detail
            });
        }

        private void ConsumeCurrentElement(System.Xml.XmlReader reader)
        {
            reader.MoveToContent();
            bool isEmpty = reader.IsEmptyElement;
            reader.ReadStartElement();

            if (isEmpty)
            {
                return;
            }

            reader.ReadEndElement();
        }

        public override void ReadXml(System.Xml.XmlReader reader)
        {
            reader.MoveToContent();
            reader.ReadStartElement();

            while (reader.NodeType == System.Xml.XmlNodeType.Element)
            {
                switch (reader.Name)
                {
                    case "LoanData":
                        LoanData = new QuoteRequestData();
                        LoanData.ReadXml(reader);
                        break;
                    case "Results":
                        Results = new QuoteResultData();
                        ConsumeCurrentElement(reader);
                        ////Results.ReadXml(reader);
                        break;
                    case "Extras":
                        List<ExtraItem> items = reader.ReadAsList<ExtraItem>("extra");
                        m_IntegrationSpecificItems = items.ToDictionary(p => p.Name);
                        break;
                    case "RecordingOffice":
                        RecordingOffice = new RecordingOfficeInfo();
                        RecordingOffice.ReadXml(reader);
                        break;
                    case "PolicyOrdereedD":
                        PolicyOrderedD = reader.ReadElementAsTicks();
                        break;
                    case "QuoteID":
                        QuoteID = reader.ReadElementSafe();
                        break;
                    case "QuoteOrderedDate":
                        var date = reader.ReadElementAsTicks();
                        if (date.HasValue)
                        {
                            QuoteOrderedDate = date.Value;
                        }
                        break;
                    case "PolicyID":
                        PolicyID = reader.ReadElementSafe();
                        break;
                    case "TransactionID":
                        TransactionID = reader.ReadElementSafe();
                        break;
                    case "QuoteProviderName":
                        QuoteProviderName = reader.ReadElementSafe();
                        break;
                    case "PolicyProviderName":
                        PolicyProviderName = reader.ReadElementSafe();
                        break;
                    case "WereClosingCostRequested":
                        WereClosingCostRequested = bool.Parse(reader.ReadElementSafe());
                        break;
                    case "IsAPolicyOrderLog":
                        IsAPolicyOrderLog = bool.Parse(reader.ReadElementSafe());
                        break;
                    case "DocumentReceipts":
                        DocumentReceipts = reader.ReadAsList<DocumentReceipt>("documentreceipt");
                        break;
                    default:
                        throw new NotImplementedException(reader.Name);
                }
            }
        }

        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("QuoteLog");
            writer.WriteElementString("QuoteProviderName", QuoteProviderName);
            if (PolicyOrderedD.HasValue)
            {
                writer.WriteStartElement("PolicyOrdereedD");
                writer.WriteString(PolicyOrderedD.Value.Ticks.ToString());
                writer.WriteEndElement();
            }
            writer.WriteElementString("PolicyProviderName", PolicyProviderName);
            writer.WriteElementString("PolicyID", PolicyID);
            writer.WriteElementString("IsAPolicyOrderLog", IsAPolicyOrderLog.ToString());
            writer.WriteElementString("TransactionID", TransactionID);
            writer.WriteElementString("WereClosingCostRequested", WereClosingCostRequested.ToString());
            writer.WriteElementString("QuoteOrderedDate", QuoteOrderedDate.Ticks.ToString());
            writer.WriteElementString("QuoteID", QuoteID);
            writer.WriteList("DocumentReceipts", "documentreceipt", DocumentReceipts);
                writer.WriteStartElement("LoanData");
                    LoanData.WriteXml(writer);
                writer.WriteEndElement();
                writer.WriteStartElement("Results");
                    ////Results.WriteXml(writer);
                writer.WriteEndElement();
                writer.WriteStartElement("RecordingOffice");
                    RecordingOffice.WriteXml(writer);
                writer.WriteEndElement();
                writer.WriteList("Extras", "extra", m_IntegrationSpecificItems.Values);
            writer.WriteEndElement();
        }
    }
}
