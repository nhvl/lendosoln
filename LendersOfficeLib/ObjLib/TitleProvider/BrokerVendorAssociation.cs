﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Common;

namespace LendersOffice.ObjLib.TitleProvider
{
    public sealed class BrokerVendorAssociation
    {
        public Guid BrokerId { get; set; }
        public int VendorId { get; set; }

        public string UsernameQuote { get; set; }
        public string AccountIdQuote { get; set; }
        public string EncryptedPasswordQuote { get; set; }
        public string UsernameTitle { get; set; }
        public string AccountIdTitle { get; set; }
        public string ExportPathQuote { get; set; }
        public string ExportPathTitle { get; set; }

        public string EncryptedPasswordTitle { get; set; }
        public E_TitleVendorIntegrationType TitleVendorIntegrationType { get; set; }
        public string PasswordQuote
        {
            get { return EncryptionHelper.Decrypt(EncryptedPasswordQuote); }
        }


        public string PasswordTitle
        {
            get { return EncryptionHelper.Decrypt(EncryptedPasswordTitle); }
        }

        public string VendorName { get; set; }
        public bool VendorRequiresAccountId { get; set; }
        public bool HasBrokerInformation { get; set; }

        internal BrokerVendorAssociation(DbDataReader reader, IEnumerable<Vendor> vendorList)
        {
            VendorId = (int)reader["TitleVendorId"];

            Vendor vendor = null;

            foreach (var o in vendorList)
            {
                if (o.Id == VendorId)
                {
                    vendor = o;
                    break;
                }
            }

            if (vendor == null)
            {
                vendor = new Vendor();
            }

            VendorName = vendor.Name;
            VendorRequiresAccountId = vendor.RequiresAccountId;
            TitleVendorIntegrationType = vendor.TitleVendorIntegrationType;

            if (reader["Login"] == DBNull.Value)
            {
                HasBrokerInformation = false;
                UsernameQuote = "";
                AccountIdQuote = "";
                EncryptedPasswordQuote = "";

                UsernameTitle = "";
                AccountIdTitle = "";
                EncryptedPasswordTitle = "";
                ExportPathQuote = "";
                ExportPathTitle = "";
            }
            else
            {
                BrokerId = (Guid)reader["BrokerId"];
                HasBrokerInformation = true;

                UsernameQuote = (string)reader["Login"];
                AccountIdQuote = (string)reader["AccountId"];
                EncryptedPasswordQuote = (string)reader["Password"];

                UsernameTitle = (string)reader["LoginTitle"];
                AccountIdTitle = (string)reader["AccountIdTitle"];
                EncryptedPasswordTitle = (string)reader["PasswordTitle"];

                ExportPathQuote = vendor.ExportPath;
                ExportPathTitle = vendor.ExportPathTitle;
                

                if (!VendorRequiresAccountId)
                {
                    AccountIdQuote = "N/A";
                }
            }
        }

        public BrokerVendorAssociation(Guid brokerId, int vendorId)
        {
            BrokerId = brokerId;
            VendorId = vendorId;
        }

        internal BrokerVendorAssociation(Vendor vendor)
        {
            VendorId = vendor.Id;
            VendorName = vendor.Name;
            VendorRequiresAccountId = vendor.RequiresAccountId;
            TitleVendorIntegrationType = vendor.TitleVendorIntegrationType;

            HasBrokerInformation = false;
            UsernameQuote = string.Empty;
            AccountIdQuote = string.Empty;
            EncryptedPasswordQuote = string.Empty;

            UsernameTitle = string.Empty;
            AccountIdTitle = string.Empty;
            EncryptedPasswordTitle = string.Empty;
            ExportPathQuote = string.Empty;
            ExportPathTitle = string.Empty;

        }
        public BrokerVendorAssociation()
        {

        }

        public void SetPassword(bool forQuote, string password)
        {
            if (forQuote)
            {
                EncryptedPasswordQuote = EncryptionHelper.Encrypt(password);
            }
            else
            {
                EncryptedPasswordTitle = EncryptionHelper.Encrypt(password);
            }
        }

 
    }
}
