﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Audit;
using LendersOffice.Security;
using LendersOffice.Common;

namespace LendersOffice.ObjLib.TitleProvider.Audit
{
    public class AuditRule
    {
        public string Description { get; set; }
        public bool Satisfied { get; set; }
        public string Name { get; set; }
        public string SuccessMessage { get; set; }
        public string FailureMessage { get; set; }
        public string Data { get; set; }
        public string GetRuleResult()
        {
            return Satisfied ? SuccessMessage : FailureMessage; 
        }
    }
    public class TitleAuditor
    {
        private CPageData m_loanData;
        private CAppData[] m_dataApps;
        private List<AuditRule> m_rules;

        public TitleAuditor(Guid sLId)
        {
            m_loanData = CPageData.CreateUsingSmartDependency(sLId, typeof(TitleAuditor)); 
            m_loanData.InitLoad();

            m_dataApps = new CAppData[m_loanData.nApps];
            for (int i = 0; i < m_loanData.nApps; i++)
            {
                m_dataApps[i] = m_loanData.GetAppData(i);
                m_dataApps[i].BorrowerModeT = E_BorrowerModeT.Borrower;
            }

            AuditData();
        }

        private void AuditData()
        {
            m_rules = new List<AuditRule>();

            AuditRule rule = new AuditRule();
            rule.Name = "Property Address Is Valid";
            rule.Description = "Subject Property Address";
            rule.Satisfied = m_loanData.HasCompletePropertyAddressForTitle;
            rule.FailureMessage = "Complete property address is required";
            rule.SuccessMessage = string.Empty;
            rule.Data = m_loanData.sSpAddr_MultiLine;
            m_rules.Add(rule);

            rule = new AuditRule();
            rule.Name = "County Is Valid";
            rule.Description = "Subject Property County";
            rule.Satisfied = m_loanData.sSpCounty.TrimWhitespaceAndBOM().Length > 0;
            rule.FailureMessage = "County is required";
            rule.SuccessMessage = string.Empty;
            rule.Data = m_loanData.sSpCounty;
            m_rules.Add(rule);

            rule = new AuditRule();
            rule.Name = "Purchase Price Is Valid";
            rule.Description = "Purchase Price";
            rule.Satisfied = m_loanData.sLPurposeT != E_sLPurposeT.Purchase || m_loanData.sPurchPrice > 0;
            rule.FailureMessage = "Purchase loans must have a purchase price";
            rule.SuccessMessage = string.Empty;
            rule.Data = m_loanData.sPurchPrice_rep;
            m_rules.Add(rule);

            string error = "Appraised value is required for all loans";
            decimal value = m_loanData.sApprVal;
            string rep = m_loanData.sApprVal_rep;
            string desc = "Appraised Value";
            if (m_loanData.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance)
            {
                error = "Original " + error;
                value = m_loanData.sOriginalAppraisedValue;
                desc = "Original " + desc;
                rep = m_loanData.sOriginalAppraisedValue_rep;
            }

            rule = new AuditRule();
            rule.Name = "Appraised Value Is Valid";
            rule.Description = desc;
            rule.Satisfied = value > 0;
            rule.FailureMessage = error;
            rule.SuccessMessage = string.Empty;
            rule.Data = rep;
            m_rules.Add(rule);

            rule = new AuditRule();
            rule.Name = "Amort Type";
            rule.Description = "Amortization Type";
            rule.Satisfied = true;
            rule.FailureMessage = string.Empty;
            rule.SuccessMessage = string.Empty;
            rule.Data = m_loanData.sFinMethT_rep;
            m_rules.Add(rule);

            rule = new AuditRule();
            rule.Name = "Loan Purpose Is Valid";
            rule.Description = "Loan Purpose";
            rule.Satisfied = m_loanData.sLPurposeT != E_sLPurposeT.Other;
            rule.FailureMessage = "Other is not a supported purpose";
            rule.SuccessMessage = string.Empty;
            rule.Data = m_loanData.sLPurposeT_rep;
            m_rules.Add(rule);


            rule = new AuditRule();
            rule.Name = "Lien Position";
            rule.Description = "Lien Position";
            rule.Satisfied = true;
            rule.FailureMessage = string.Empty;
            rule.SuccessMessage = string.Empty;
            rule.Data = m_loanData.sLienPosT_rep;
            m_rules.Add(rule);

            rule = new AuditRule();
            rule.Name = "Loan Amount Must Be greater than 0";
            rule.Description = "Loan Amount";
            rule.Satisfied = m_loanData.sFinalLAmt > 0;
            rule.FailureMessage = "Loan amount must be > 0";
            rule.SuccessMessage = string.Empty;
            rule.Data = m_loanData.sFinalLAmt_rep;
            m_rules.Add(rule);

            for (int i = 0; i < m_loanData.nApps; i++)
            {
                bool bBorrowerIsOnTitle = (m_dataApps[i].aBTypeT != E_aTypeT.NonTitleSpouse);
                bool bCoBorrowerIsOnTitle = ((m_dataApps[i].aCIsValidNameSsn) && (m_dataApps[i].aCTypeT != E_aTypeT.NonTitleSpouse));

                if (bBorrowerIsOnTitle || bCoBorrowerIsOnTitle)
                {
                    rule = new AuditRule();
                    rule.Name = "Manner in which Title will be held";
                    rule.Description = string.Format("Manner in which Title will be held : {0}", m_dataApps[i].aAppNm);
                    rule.Satisfied = true;
                    rule.FailureMessage = string.Empty;
                    rule.SuccessMessage = string.Empty;
                    rule.Data = m_dataApps[i].aManner;
                    m_rules.Add(rule);

                    if (bBorrowerIsOnTitle)
                    {
                        m_rules.Add(MaritalStatusRule(i));
                    }

                    if (bCoBorrowerIsOnTitle)
                    {
                        m_dataApps[i].BorrowerModeT = E_BorrowerModeT.Coborrower;
                        m_rules.Add(MaritalStatusRule(i));
                    }
                }
            }

            Success = !m_rules.Any(p => !p.Satisfied);
        }

        public IEnumerable<AuditRule> Rules
        {
            get { return m_rules; }
        }

        public bool Success { get; private set; }

        public static void RecordQCAudit(string sResponseXML, Guid sLoanID)
        {
            AbstractAuditItem xmlAuditItem = new TitlePolicySubmissionXmlAuditItem(PrincipalFactory.CurrentPrincipal, sResponseXML);
            AuditManager.RecordAudit(sLoanID, xmlAuditItem);
        }

        private AuditRule MaritalStatusRule(int iAppIndex)
        {
            AuditRule rule = new AuditRule();
            rule.Name = "Marital Status";
            rule.Description = string.Format("Marital Status : {0}", m_dataApps[iAppIndex].aNm);
            rule.Satisfied = m_dataApps[iAppIndex].aMaritalStatT != E_aBMaritalStatT.LeaveBlank;
            rule.FailureMessage = "Marital Status is required";
            rule.SuccessMessage = string.Empty;
            rule.Data = m_dataApps[iAppIndex].aMaritalStatTDescription;

            return rule;
        }
    }
}
