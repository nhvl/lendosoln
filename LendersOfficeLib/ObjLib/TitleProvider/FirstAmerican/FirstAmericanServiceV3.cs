﻿namespace LendersOffice.ObjLib.TitleProvider.FirstAmerican
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web.Hosting;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Serialization;
    using System.Xml.XPath;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.FirstAmerican.CAPI.V3_0;
    using LendersOffice.ObjLib.TitleProvider.Audit;
    using LendersOffice.ObjLib.TitleProvider.FirstAmerican.XmlSerialization;
    using LendersOffice.ObjLib.TitleProvider.TitlePolicy;
    using LendersOffice.ObjLib.TitleProvider.TitlePolicy.Request;
    using LQBTitle.LQBXml;
    using Resource;

    /// <summary>
    /// This class implements the LendingQB to First American integration using <c>CAPI V3</c> version of
    /// their web services.
    /// </summary>
    public sealed class FirstAmericanServiceV3 : TitleService
    {
        /// <summary>
        /// The key used in <see cref="QuoteLog"/> to hold the quote request.
        /// </summary>
        private const string XmlQuoteRequestKey = "Request1Response";

        /// <summary>
        /// The key used in <see cref="QuoteLog"/> to hold the quote response.
        /// </summary>
        private const string XmlQuoteResponseKey = "QuoteResponse";

        /// <summary>
        /// The key used in <see cref="QuoteLog"/> to hold the policy request.
        /// </summary>
        private const string XmlPolicyRequestKey = "TitlePolicyRequest";

        /// <summary>
        /// The key used in <see cref="QuoteLog"/> to hold the policy response.
        /// </summary>
        private const string XmlPolicyResponseKey = "TitlePolicyResponse";

        /// <summary>
        /// A logging action to apply to our messages.  This was added to enable external testing tools to inject saving to files or the like.
        /// </summary>
        private readonly Action<string> logInfo;

        /// <summary>
        /// The XML representation of First American's county and closing information.
        /// </summary>
        private XDocument faccCountyInfo;

        /// <summary>
        /// A private variable to cache the <see cref="BrokerDB"/> for this instance.
        /// </summary>
        private BrokerDB brokerDb;

        /// <summary>
        /// Initializes a new instance of the <see cref="FirstAmericanServiceV3"/> class.
        /// </summary>
        /// <param name="association">The association between a lender and vendor to use for this service.</param>
        public FirstAmericanServiceV3(BrokerVendorAssociation association)
            : this(association, ResourceManager.Instance.GetResourcePath(ResourceType.FaccCountyInfoXml, association.BrokerId))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FirstAmericanServiceV3"/> class.
        /// </summary>
        /// <param name="association">The association between a lender and vendor to use for this service.</param>
        /// <param name="countyPath">The file path to the XML file containing First American's county and closing county information.</param>
        /// <param name="logger">The logger to use when recording data, if an external logger is needed.  If null is passed, it defaults to <seealso cref="Tools.LogInfo(string)"/>.</param>
        public FirstAmericanServiceV3(BrokerVendorAssociation association, string countyPath, Action<string> logger = null)
            : base(association)
        {
            this.faccCountyInfo = XDocument.Load(countyPath);
            this.logInfo = logger ?? Tools.LogInfo;
        }

        /// <summary>
        /// Gets the disclaimer from the vendor to go with closing cost statements.
        /// </summary>
        /// <value>The disclaimer from the vendor to go with closing cost statements.</value>
        public override string ClosingCostDisclaimer
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// Gets a value indicating whether this title service supports quotes.
        /// </summary>
        /// <value><see langword="false"/> in all circumstances.</value>
        public override bool SupportsQuote
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether this title service allows policy requests.
        /// </summary>
        /// <value><see langword="true"/> in all circumstances.</value>
        public override bool RequestPolicy
        {
            get { return true; }
        }

        /// <summary>
        /// Gets the cached <see cref="BrokerDB"/> used by this instance.
        /// </summary>
        private BrokerDB CurrentBrokerDB
        {
            get
            {
                if (this.brokerDb == null)
                {
                    this.brokerDb = BrokerDB.RetrieveById(this.BrokerId);
                }

                return this.brokerDb;
            }
        }

        /// <summary>
        /// Gets an enumeration of the title policies available for a specified subset of loan data.
        /// </summary>
        /// <param name="propertyCity">The property's city; maps from <see cref="CPageData.sSpCity"/>.</param>
        /// <param name="propertyCounty">The property's county; maps from <see cref="CPageData.sSpCounty"/>.</param>
        /// <param name="propertyState">The property's state; maps from <see cref="CPageData.sSpState"/>.</param>
        /// <param name="loanPurposeType">The purpose of the loan; maps from <see cref="CPageData.sLPurposeT"/>.</param>
        /// <param name="lienPositionType">The lien position type of the loan; maps from <see cref="CPageData.sLienPosT"/>.</param>
        /// <returns>An enumeration of the title policies for a particular set of data.</returns>
        public IEnumerable<PolicyInfo> GetPoliciesForTitleOrder(string propertyCity, string propertyCounty, string propertyState, E_sLPurposeT loanPurposeType, E_sLienPosT lienPositionType)
        {
            string propertyCountyFixed = FixUpCounties(propertyState, propertyCounty);
            int propertyCountyId = this.GetCountyId(propertyState, propertyCountyFixed);
            int? propertyTypeId = this.GetPropertyTypeId(propertyState);
            int purposeOfTransaction = this.GetPurposeOfTransaction(loanPurposeType, lienPositionType, propertyState, propertyTypeId);

            return this.GetFAPolicyList(propertyState, propertyCountyId, purposeOfTransaction, propertyTypeId);
        }

        /// <summary>
        /// Gets an enumeration of the default title policies for a specified subset of loan data.
        /// </summary>
        /// <param name="propertyCity">The property's city; maps from <see cref="CPageData.sSpCity"/>.</param>
        /// <param name="propertyCounty">The property's county; maps from <see cref="CPageData.sSpCounty"/>.</param>
        /// <param name="propertyState">The property's state; maps from <see cref="CPageData.sSpState"/>.</param>
        /// <param name="loanPurposeType">The purpose of the loan; maps from <see cref="CPageData.sLPurposeT"/>.</param>
        /// <param name="lienPositionType">The lien position type of the loan; maps from <see cref="CPageData.sLienPosT"/>.</param>
        /// <returns>An enumeration of the <see cref="title_productsTitle_product.policy_id"/> of the default title policies.</returns>
        public IEnumerable<int> GetIDsOfDefaultPolicies(string propertyCity, string propertyCounty, string propertyState, E_sLPurposeT loanPurposeType, E_sLienPosT lienPositionType)
        {
            string propertyCountyFixed = FixUpCounties(propertyState, propertyCounty);
            int propertyCountyId = this.GetCountyId(propertyState, propertyCountyFixed);
            int? propertyTypeId = this.GetPropertyTypeId(propertyState);
            int purposeOfTransaction = this.GetPurposeOfTransaction(loanPurposeType, lienPositionType, propertyState, propertyTypeId);

            QuoteRequestOptions options = new QuoteRequestOptions()
            {
                RequestTitle = true,
                RequestClosing = true,
                RequestRecording = true
            };

            FACCProductDefaults defaults = this.GetProductDefaults(options, propertyState, propertyCountyId, purposeOfTransaction, propertyTypeId);

            return defaults.TitlePolicies.Select(policy => policy.PolicyId).ToList();
        }

        /// <summary>
        /// Gets the available policy types for the specified loan data.
        /// </summary>
        /// <param name="loanPurposeType">The purpose of the loan; maps from <see cref="CPageData.sLPurposeT"/>.</param>
        /// <param name="propertyState">The property's state; maps from <see cref="CPageData.sSpState"/>.</param>
        /// <returns>An array of available policy types.</returns>
        public override E_cTitleInsurancePolicyT[] GetAvailablePolicyTypes(E_sLPurposeT loanPurposeType, string propertyState)
        {
            return new E_cTitleInsurancePolicyT[] { };
        }

        /// <summary>
        /// Compares loan data used in initial request to what the current request would be.
        /// </summary>
        /// <param name="initialData">The initial data used in a previous request.</param>
        /// <param name="currentData">The current request data on the loan.</param>
        /// <returns><see langword="true"/> if <paramref name="initialData"/> matches <paramref name="currentData"/> on all points; <see langword="false"/> otherwise.</returns>
        public override bool AreQuotesStillValid(QuoteRequestData initialData, QuoteRequestData currentData)
        {
            var requirementsMap = new Func<QuoteRequestData, string>[]
            {
                data => data.sSpState,
                data => data.sSpCounty,
                data => data.sSpZip,
                data => data.sSpCity,
                data => data.sFinalLAmt.ToString(),
                data => data.sLPurposeT.ToString(),
                data => data.sPurchPrice.ToString(),
            };

            return requirementsMap.All(map => map(initialData) == map(currentData));
        }

        /// <summary>
        /// Get the Quotes for a specified set of loan data. 
        /// </summary>
        /// <param name="data">The loan data to use to retrieve quotes.</param>
        /// <param name="options">Request options used to determine what quotes to order.</param>
        /// <returns>A collection of <see cref="QuoteResultData"/> data.</returns>
        public override QuoteResultData GetQuotes(QuoteRequestData data, QuoteRequestOptions options)
        {
            XDocument debugLog = new XDocument(new XElement("FADebugInfo", new XAttribute("CreateDate", DateTime.Now.ToString())));
            QuoteResultData quote;
            try
            {
                quote = this.GetQuotesForPricing(data, options, debugLog);
            }
            catch (Exception exc)
            {
                Tools.LogError(debugLog.ToString(SaveOptions.None), exc);
                throw;
            }

            this.logInfo(debugLog.ToString(SaveOptions.None));
            return quote;
        }

        /// <summary>
        /// Orders a Title Quote.
        /// </summary>
        /// <param name="loanId">The loan ID (<see cref="CPageData.sLId"/>) of the loan needing a title quote.</param>
        /// <param name="titleInsurancePolicyT">The type of policy requested in the order.</param>
        /// <exception cref="NotImplementedException">Thrown any time this method is called.</exception>
        public override void OrderQuote(Guid loanId, E_cTitleInsurancePolicyT titleInsurancePolicyT)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Orders a Title Policy.
        /// </summary>
        /// <param name="loanId">The loan ID (<see cref="CPageData.sLId"/>) of the loan needing a title policy.</param>
        /// <param name="ownerPolicyName">The name of the owner title policy.</param>
        /// <param name="lenderPolicyName">The name of the lender title policy.</param>
        /// <param name="isEscrowRequested">Indicates whether escrow is requested.</param>
        public override void OrderPolicy(Guid loanId, string ownerPolicyName, string lenderPolicyName, bool isEscrowRequested)
        {
            TitleRequestProvider policyRequestProvider = new TitleRequestProvider(this.UserNameTitle, this.PasswordTitle, "First American Title", this.ClientIdTitle, loanId);

            // First American does not support signing
            TitleRequest policyRequest = policyRequestProvider.CreateTitlePolicyRequest(string.Empty, ownerPolicyName, lenderPolicyName, isEscrowRequested, false);

            try
            {
                this.SubmitPolicyOrder(loanId, policyRequest);
            }
            catch (WebException webExc)
            {
                StringBuilder debugMsg = new StringBuilder();
                debugMsg.AppendLine("Title Policy Order Failed.");
                debugMsg.AppendLine(webExc.Message + Environment.NewLine + webExc.StackTrace);
                Tools.LogInfo(debugMsg.ToString());

                this.TitleServiceResult = new TitleServiceResult(false, "Title Policy Order Failed. Please wait a few minutes and retry.");
            }
        }

        /// <summary>
        /// Sends a document to the title vendor.
        /// </summary>
        /// <param name="loanId">The loan ID (<see cref="CPageData.sLId"/>) of the loan containing the title policy.</param>
        /// <param name="documentId">The document identifier of the document to send to the title vendor.</param>
        /// <exception cref="NotImplementedException">Thrown any time this method is called.</exception>
        public override void SendDocToVendor(Guid loanId, Guid documentId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Reads saved XML from <paramref name="log"/> and populates values within <paramref name="log"/> accordingly.
        /// </summary>
        /// <param name="log">A log of a completed quote.</param>
        protected override void PopulateDetails(QuoteLog log)
        {
            if (log.HasItem(XmlQuoteRequestKey))
            {
                string requestXml = log.GetItem(XmlQuoteRequestKey);
                string responseXml = log.GetItem(XmlQuoteResponseKey);
                CCRequest request = (CCRequest)SerializationHelper.XmlDeserialize(requestXml, typeof(CCRequest));
                CCResponse response = (CCResponse)SerializationHelper.XmlDeserialize(responseXml, typeof(CCResponse));

                LosConvert convert = new LosConvert();

                if (request.title_products != null)
                {
                    for (int i = 0; i < request.title_products.Length; i++)
                    {
                        CCRequestTitle_product product = request.title_products[i];
                        string detail = string.Format(
                            "Ordered title product {0} {1} with with liability amount {2}",
                            product.ratetype_id,
                            product.policy_name,
                            convert.ToMoneyString(product.liability_amount, FormatDirection.ToRep));

                        foreach (CCRequestEndorsement endorsement in request.endorsements.Where(p => p.title_product_ordinal == i))
                        {
                            detail += string.Format("\n\tIncluding endorsement {0} ({1})", endorsement.endorsement_name, endorsement.endorsement_id);
                        }

                        log.AddDetail(QuoteLog.E_QuoteDetailType.INFO, detail);
                    }
                }

                if (request.closing_costs != null && request.closing_costs.closing_id > 0)
                {
                    foreach (CCRequestClosing_costsClosing_fee fee in request.closing_costs.closing_fees)
                    {
                        log.AddDetail(QuoteLog.E_QuoteDetailType.INFO, "Requested fee " + fee.name);
                    }
                }

                foreach (note note in response.notes)
                {
                    log.AddDetail(QuoteLog.E_QuoteDetailType.WARNING, note.value);
                }
            }
            else
            {
                log.IsAPolicyOrderLog = true;
            }
        }

        /// <summary>
        /// Serializes a <paramref name="xmlSerializableObject"/> directly into <paramref name="doc"/>, under the specified tag name.
        /// </summary>
        /// <param name="doc">The XML document to which the object is added.</param>
        /// <param name="tag">The XML element tag that will contain the object.</param>
        /// <param name="xmlSerializableObject">The object to serialize into the XML.</param>
        private static void AppendSerializableToXDoc(XDocument doc, string tag, object xmlSerializableObject)
        {
            using (XmlWriter writer = doc.Root.CreateWriter())
            {
                writer.WriteStartElement(tag);
                if (xmlSerializableObject != null)
                {
                    XmlSerializer serializer = new XmlSerializer(xmlSerializableObject.GetType());
                    serializer.Serialize(writer, xmlSerializableObject);
                }

                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// Fills in data to <paramref name="request"/> for required parameters that are both recognized and currently blank.
        /// </summary>
        /// <param name="request">The request to update with new values.</param>
        /// <param name="data">The data about the loan needing the quote request.</param>
        /// <param name="debugLog">A running debug log of the whole quote request process.</param>
        /// <returns>A boolean value indicating whether <paramref name="request"/> was updated.</returns>
        private static bool FillOutRequiredParams(CCRequest request, QuoteRequestData data, XDocument debugLog)
        {
            XElement missingArguments = new XElement("MissingArguments");
            debugLog.Root.Add(missingArguments);

            bool changes = false;
            if (request.@params != null)
            {
                foreach (CCRequestParam requiredParams in request.@params.Where(p => p.required == "Y"))
                {
                    if (string.IsNullOrEmpty(requiredParams.param_value.value))
                    {
                        if (requiredParams.name.Equals("Number Of Loans"))
                        {
                            requiredParams.param_value.value = data.HasLinkedLoan ? "2" : "1";
                            changes = true;
                        }
                        else if (requiredParams.name.Equals("Zip Code") || requiredParams.name.Equals("Excess Prior Liability over Prod 1"))
                        {
                            // ignore
                        }
                        else if (requiredParams.name.Equals("Outstanding Principal balance of original loan?", StringComparison.OrdinalIgnoreCase)
                            || requiredParams.code.Equals("295")
                            || requiredParams.key.Equals("CCC_27_0", StringComparison.OrdinalIgnoreCase)
                            || requiredParams.key.Equals("CCC_4_0", StringComparison.OrdinalIgnoreCase)
                            || requiredParams.key.Equals("CCC_7_0", StringComparison.OrdinalIgnoreCase)
                            || requiredParams.key.Equals("CCC_19_0", StringComparison.OrdinalIgnoreCase))
                        {
                            // 3/19/2015 BB - OPM 206703; Unpaid Principal Amount
                            requiredParams.param_value.value = Math.Round(data.sSpLien, 2).ToString();
                            changes = true;
                        }
                        else if (requiredParams.code.Equals("1187"))
                        {
                            // 3/18/2015 BB - OPM 206703; Expedited Loan (1) or Full Search Loan (2);
                            // Default to Full so that the quoted fees reflect a search for easements/restrictions, i.e. worst case fee
                            requiredParams.param_value.value = "2";
                            changes = true;
                        }
                        else if (requiredParams.code.Equals("370"))
                        {
                            // 3/20/2015 BB - OPM 206703; Prior Loan Policy Type
                            requiredParams.param_value.value = "1";
                            changes = true;
                        }
                        else if (requiredParams.code.Equals("11045") || requiredParams.key.Equals("CCC_45_0", StringComparison.OrdinalIgnoreCase))
                        {
                            // 07/20/2015 BB - Will the new policy be issued with a policy date that is within three years from the policy date of a previously issued policy by any title insurance company
                            requiredParams.param_value.value = "0";
                            changes = true;
                        }
                        else if (requiredParams.key.Equals("CCC_1_0", StringComparison.OrdinalIgnoreCase))
                        {
                            // 07/31/2015 BB - Is this a refinance or existing HELOC transaction?
                            requiredParams.param_value.value = data.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || data.sLPurposeT == E_sLPurposeT.HomeEquity || data.sLPurposeT == E_sLPurposeT.Refin || data.sLPurposeT == E_sLPurposeT.RefinCashout || data.sLPurposeT == E_sLPurposeT.VaIrrrl ? "1" : "0";
                            changes = true;
                        }
                        else if (requiredParams.key.Equals("CCC_3_0", StringComparison.OrdinalIgnoreCase) || requiredParams.key.Equals("CCC_18_0", StringComparison.OrdinalIgnoreCase))
                        {
                            // 07/31/2015 BB - Date of prior mortgage
                            requiredParams.param_value.value = DateTime.Now.ToString("G");
                            changes = true;
                        }
                        else if (requiredParams.key.Equals("CCC_6_0", StringComparison.OrdinalIgnoreCase) || requiredParams.key.Equals("CCC_20_0", StringComparison.OrdinalIgnoreCase))
                        {
                            // 07/31/2015 BB - Is the mortgage to be insured paying off a mortgage to the borrower on the same property?
                            requiredParams.param_value.value = data.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || data.sLPurposeT == E_sLPurposeT.Refin || data.sLPurposeT == E_sLPurposeT.RefinCashout || data.sLPurposeT == E_sLPurposeT.VaIrrrl ? "1" : "0";
                            changes = true;
                        }
                        else if (requiredParams.key.Equals("CCC_15_0", StringComparison.OrdinalIgnoreCase))
                        {
                            // 07/31/2015 BB - Has the borrower has provided a prior Owners Policy naming them as insured?
                            requiredParams.param_value.value = "0";
                            changes = true;
                        }
                        else if (requiredParams.key.Equals("CCC_16_0", StringComparison.OrdinalIgnoreCase)
                            || requiredParams.key.Equals("CCC_42_0", StringComparison.OrdinalIgnoreCase)
                            || requiredParams.key.Equals("CCC_55_0", StringComparison.OrdinalIgnoreCase))
                        {
                            // 07/31/2015 BB - Prior Owners Policy Amount
                            requiredParams.param_value.value = "0";
                            changes = true;
                        }
                        else if (requiredParams.key.Equals("CCC_17_0", StringComparison.OrdinalIgnoreCase))
                        {
                            // 07/31/2015 BB - Is the same borrower refinancing a loan with the same lender on the same property or the new loan is more than $250,000 even if the new lender is different from the lender being paid off?
                            requiredParams.param_value.value = data.sFinalLAmt > 250000.00m ? "1" : "0";
                            changes = true;
                        }
                        else if (requiredParams.key.Equals("CCC_21_0", StringComparison.OrdinalIgnoreCase) || requiredParams.key.Equals("CCC_25_0", StringComparison.OrdinalIgnoreCase))
                        {
                            // 07/31/2015 BB - Closing date of new loan
                            requiredParams.param_value.value = DateTime.Now.ToString("G");
                            changes = true;
                        }
                        else if (requiredParams.key.Equals("CCC_22_0", StringComparison.OrdinalIgnoreCase))
                        {
                            // 07/31/2015 BB - Prior Policy date
                            requiredParams.param_value.value = DateTime.Now.ToString("G");
                            changes = true;
                        }
                        else if (requiredParams.key.Equals("CCC_23_0", StringComparison.OrdinalIgnoreCase)
                            || requiredParams.key.Equals("CCC_51_0", StringComparison.OrdinalIgnoreCase)
                            || requiredParams.key.Equals("CCC_57_0", StringComparison.OrdinalIgnoreCase))
                        {
                            // 07/31/2015 BB - Prior Policy amount
                            requiredParams.param_value.value = "0";
                            changes = true;
                        }
                        else if (requiredParams.key.Equals("CCC_24_0", StringComparison.OrdinalIgnoreCase))
                        {
                            // 07/31/2015 BB - Is the new loan paying off an insured existing mortgage that has been insured in the last 10 years?
                            requiredParams.param_value.value = "0";
                            changes = true;
                        }
                        else if (requiredParams.key.Equals("CCC_26_0", StringComparison.OrdinalIgnoreCase))
                        {
                            // 07/31/2015 BB - Prior Deed Of Trust dated
                            requiredParams.param_value.value = DateTime.Now.ToString("G");
                            changes = true;
                        }
                        else if (requiredParams.key.Equals("CCC_41_0", StringComparison.OrdinalIgnoreCase))
                        {
                            // 07/31/2015 BB - Has an Owners Policy been issued to the borrower in the last 2 years?
                            requiredParams.param_value.value = "0";
                            changes = true;
                        }
                        else if (requiredParams.key.Equals("CCC_50_0", StringComparison.OrdinalIgnoreCase))
                        {
                            // 08/24/2015 BB - Has a policy been issued on the same land within 15 years?
                            requiredParams.param_value.value = "0";
                            changes = true;
                        }
                        else if (requiredParams.key.Equals("CCC_54_0", StringComparison.OrdinalIgnoreCase))
                        {
                            // 07/31/2015 BB - Has an owners policy been issued to the borrower in the last 3 years?
                            requiredParams.param_value.value = "0";
                            changes = true;
                        }
                        else if (requiredParams.key.Equals("CCC_56_0", StringComparison.OrdinalIgnoreCase))
                        {
                            // 07/31/2015 BB - Is this a TX Home Equity loan?
                            requiredParams.param_value.value = data.sLPurposeT == E_sLPurposeT.HomeEquity && data.sSpState.Equals("TX", StringComparison.OrdinalIgnoreCase) ? "1" : "0";
                            changes = true;
                        }
                        else if (requiredParams.key.Equals("CCC_58_0", StringComparison.OrdinalIgnoreCase))
                        {
                            // 07/31/2015 BB - Is the reissue credit applicable?
                            requiredParams.param_value.value = "0";
                            changes = true;
                        }
                        else if (requiredParams.key.Equals("CCC_59_0", StringComparison.OrdinalIgnoreCase))
                        {
                            // 07/31/2015 BB - Enter Face Amount of existing mortgages or purchase price, whichever is greater
                            if (data.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || data.sLPurposeT == E_sLPurposeT.Refin || data.sLPurposeT == E_sLPurposeT.RefinCashout || data.sLPurposeT == E_sLPurposeT.VaIrrrl)
                            {
                                requiredParams.param_value.value = Math.Round(data.sSpLien, 2).ToString();
                            }
                            else
                            {
                                requiredParams.param_value.value = Math.Round(data.sPurchPrice, 2).ToString();
                            }

                            changes = true;
                        }
                        else
                        {
                            missingArguments.Add(new XElement("MissingArgument", new XAttribute("name", requiredParams.name), new XAttribute("key", requiredParams.key)));
                        }
                    }
                }
            }

            return changes;
        }

        /// <summary>
        /// Fix the values of all <c>param_ref</c> items.<para/>
        /// This means removing duplicate keys and fixing a specific First American bug in the <c>param_path</c>.
        /// </summary>
        /// <param name="request">The request returned by First American's.</param>
        /// <returns>A boolean value indicating whether values within <paramref name="request"/> were changed.</returns>
        private static bool FixUpParamRefs(CCRequest request)
        {
            bool changes = false;

            // Alas, a spot that was easier with XPaths.  Need to manually select all the items with param_ref elements.
            // Before adding a new parameter type below, ensure that it has [XmlArrayItem("param_ref", IsNullable=false)]
            // to ensure that nulling the value will be sufficient to remove it from the array.
            ////foreach (XElement paramRef in doc.XPathSelectElements("descendant::param_ref"))
            foreach (param_refsParam_ref[] paramRefs in
                (request.title_products == null ? new param_refsParam_ref[0][] : request.title_products.Select(p => p.param_refs))
                .Concat(request.endorsements == null ? new param_refsParam_ref[0][] : request.endorsements.Select(p => p.param_refs))
                .Concat(request.cpls == null ? new param_refsParam_ref[0][] : request.cpls.Select(p => p.param_refs))
                .Concat(request.closing_costs == null ? new param_refsParam_ref[0][] : new param_refsParam_ref[][] { request.closing_costs.param_refs })
                .Concat(request.closing_costs == null || request.closing_costs.closing_fees == null ? new param_refsParam_ref[0][] : request.closing_costs.closing_fees.Select(p => p.param_refs))
                .Concat(request.recording_documents == null ? new param_refsParam_ref[0][] : request.recording_documents.Select(p => p.param_refs)))
            {
                changes |= FixUpParamRefs(paramRefs);
            }

            return changes;
        }

        /// <summary>
        /// Fix the values of a specific array of <c>param_ref</c> items. This means removing duplicate
        /// keys and fixing a specific First American bug in the <c>param_path</c>.
        /// </summary>
        /// <param name="inputParamRefs">The array of <c>param_ref</c> items to look through.</param>
        /// <returns>A boolean value indicating whether elements of <paramref name="inputParamRefs"/> were changed.</returns>
        private static bool FixUpParamRefs(param_refsParam_ref[] inputParamRefs)
        {
            if (inputParamRefs == null)
            {
                return false;
            }

            bool changes = false;
            Dictionary<string, int> paramRefsItems = new Dictionary<string, int>();
            for (int i = 0; i < inputParamRefs.Length; ++i)
            {
                param_refsParam_ref paramRef = inputParamRefs[i];
                if (paramRef == null)
                {
                    continue;
                }

                if (!string.IsNullOrEmpty(paramRef.param_key))
                {
                    if (paramRefsItems.ContainsKey(paramRef.param_key))
                    {
                        Tools.LogWarning("Found duplicate param_ref param_key " + paramRef.param_key);
                        inputParamRefs[i] = null;
                        continue;
                    }
                    else
                    {
                        paramRefsItems.Add(paramRef.param_key, i);
                    }
                }

                // 3/27/2015 tj - This hasn't happened in the last month; probably safe to remove
                if (!string.IsNullOrEmpty(paramRef.param_path) && paramRef.param_path.EndsWith("]'"))
                {
                    Tools.LogWarning("FACCBUG - Fixing Param ref " + paramRef.param_path);
                    paramRef.param_path = paramRef.param_path.Replace("]'", "']");
                    changes = true;
                }
            }

            return changes;
        }

        /// <summary>
        /// Rename some of our counties to match First American's county names.
        /// </summary>
        /// <param name="state">The state containing the county.</param>
        /// <param name="county">The county to fix.</param>
        /// <returns>The county name as it appears in First American's system.</returns>
        private static string FixUpCounties(string state, string county)
        {
            if (state.Equals("FL", StringComparison.OrdinalIgnoreCase) && county.Equals("Miami dade", StringComparison.OrdinalIgnoreCase))
            {
                return "Miami-Dade";
            }
            else if (state.Equals("VA", StringComparison.OrdinalIgnoreCase) && county.Equals("Bristol City", StringComparison.OrdinalIgnoreCase))
            {
                return "BRISTOL";
            }
            else
            {
                return county;
            }
        }

        /// <summary>
        /// Creates an XML element to represent the cost for debugging.
        /// </summary>
        /// <param name="cost">The cost to convert to XML.</param>
        /// <param name="mappedCost">The mapped cost to convert to XML.</param>
        /// <param name="gfeFieldType">The GFE field type of the cost.</param>
        /// <returns>An XML summary of the cost.</returns>
        private static XElement GetDebugElementFromCost(CCResponseCost cost, QuoteResultData.TitleSplitCost mappedCost, E_LegacyGfeFieldT gfeFieldType)
        {
            object[] gfeSectionLogAttributes = cost.GFE_Section == null ? null :
                new object[]
                {
                    new XAttribute("GFEName", cost.GFE_Section.name ?? string.Empty),
                    new XAttribute("amount", cost.GFE_Section.amount),
                    new XAttribute("amount_buyer", cost.GFE_Section.amount_buyer),
                    new XAttribute("amount_seller", cost.GFE_Section.amount_seller),
                };
            return new XElement(
                "CostMapping",
                new XAttribute("Name", cost.name ?? string.Empty),
                new XAttribute("Key", cost.key ?? string.Empty),
                new XAttribute("CostType", cost.cost_type),
                new XAttribute("CostSubType", cost.cost_sub_type),
                new XAttribute("PolicyEndoRel", cost.policy_endo_rel),
                new XElement(
                    "Amount",
                    new XAttribute("amount", cost.amount),
                    new XAttribute("amount_buyer", cost.amount_buyer),
                    new XAttribute("amount_seller", cost.amount_seller)),
                new XElement("GFE", gfeSectionLogAttributes),
                new XElement(
                    "Mapping",
                    new XAttribute("To", gfeFieldType),
                    new XAttribute("BorrowerAmount", mappedCost.BorrowerResponsibleAmount.ToString()), // ToString needed because these are decimal? fields, and XAttribute will throw if they're null
                    new XAttribute("SellerAmount", mappedCost.SellerResponsibleAmount.ToString())));
        }

        /// <summary>
        /// Creates a <see cref="CCRequestParam"/> from the specified values.  This serves as a parameter to their calculations.
        /// </summary>
        /// <param name="key">The <see cref="CCRequestParam.key"/> of the parameter.</param>
        /// <param name="code">The <see cref="CCRequestParam.code"/> of the parameter.</param>
        /// <param name="name">The <see cref="CCRequestParam.name"/> of the parameter.</param>
        /// <param name="type">The <see cref="CCRequestParamParam_value.value_type"/> of the parameter.</param>
        /// <param name="value">The <see cref="CCRequestParamParam_value.value"/> of the parameter.</param>
        /// <returns>The parameter with the specified values.</returns>
        private static CCRequestParam CreateParam(int key, int code, string name, E_ValueType type, string value)
        {
            return new CCRequestParam()
            {
                key = key.ToString(),
                code = code.ToString(),
                name = name,
                answered = ToYN(true),
                required = ToYN(true),
                param_value = new CCRequestParamParam_value()
                {
                    value_type = type.ToString(),
                    value = value
                }
            };
        }

        /// <summary>
        /// Returns the amount of the specified consideration source from the <paramref name="data"/>.
        /// </summary>
        /// <param name="considerationSource">The source of the data.<para/>Current valid values are "Loan Amount" or "Sales Amount".</param>
        /// <param name="data">The loan data from which to draw the amount.</param>
        /// <returns>The amount of the specified consideration source.</returns>
        /// <exception cref="CBaseException">The consideration source is not "Loan Amount" or "Sales Amount", case insensitive.</exception>
        private static decimal GetConsiderationAmount(string considerationSource, QuoteRequestData data)
        {
            switch (considerationSource.ToUpper())
            {
                case "LOAN AMOUNT":
                    if (data.sLPurposeT == E_sLPurposeT.HomeEquity)
                    {
                        return Math.Round(data.sCreditLineAmt, 2);
                    }
                    else
                    {
                        return Math.Round(data.sFinalLAmt, 2);
                    }

                case "SALES AMOUNT":
                    return Math.Round(data.sPurchPrice, 2);
                case "N/A":
                    return 0M;
                default:
                    throw CBaseException.GenericException("Uhandled liability source : " + considerationSource);
            }
        }

        /// <summary>
        /// Converts a boolean value to <c>"Y"</c> or <c>"N"</c>, respectively corresponding to <see langword="true"/> or <see langword="false"/>.
        /// </summary>
        /// <param name="val">The value to compare.</param>
        /// <returns><c>"Y"</c> or <c>"N"</c>, respectively corresponding to <see langword="true"/> or <see langword="false"/>.</returns>
        private static string ToYN(bool val)
        {
            return val ? "Y" : "N";
        }

        /// <summary>
        /// Reads the cost amount from <see cref="CCResponseCost.GFE_Section"/>, falling back
        /// to <see cref="CCResponseCost.amount_buyer"/> if null.
        /// </summary>
        /// <param name="cost">The cost item to read.</param>
        /// <returns>The amount from <see cref="CCResponseCost.GFE_Section"/> or <see cref="CCResponseCost.amount_buyer"/>.</returns>
        private static decimal FallbackAmountBuyer(CCResponseCost cost)
        {
            if (cost.GFE_Section != null)
            {
                return cost.GFE_Section.amount;
            }

            return cost.amount_buyer;
        }

        /// <summary>
        /// Reads the cost amount from <see cref="CCResponseCost.GFE_Section"/>, falling back
        /// to <see cref="CCResponseCost.amount"/> if null.
        /// </summary>
        /// <param name="cost">The cost item to read.</param>
        /// <returns>The amount from <see cref="CCResponseCost.GFE_Section"/> or <see cref="CCResponseCost.amount"/>.</returns>
        private static decimal FallbackAmount(CCResponseCost cost)
        {
            if (cost.GFE_Section != null)
            {
                return cost.GFE_Section.amount;
            }

            return cost.amount;
        }

        /// <summary>
        /// Returns a boolean value indicating whether <paramref name="gfeSection"/> is non-null and
        /// has a non-zero value for either <see cref="CCResponseCostGFE_Section.amount_buyer"/> or
        /// <see cref="CCResponseCostGFE_Section.amount_seller"/>.
        /// </summary>
        /// <param name="gfeSection">The <see cref="CCResponseCostGFE_Section"/> to test for having non-zero amounts.</param>
        /// <returns><see langword="true"/> if <paramref name="gfeSection"/> is non-null and has non-zero buyer or seller amounts; false otherwise.</returns>
        private static bool HasNonZeroAmountBuyerOrSeller(CCResponseCostGFE_Section gfeSection)
        {
            return gfeSection != null && (gfeSection.amount_buyer != 0 || gfeSection.amount_seller != 0);
        }

        /// <summary>
        /// Maps <paramref name="cost"/> to the appropriate policy type.
        /// </summary>
        /// <param name="policyOrder">The list of policy order type and key pairs that <paramref name="cost"/> can be mapped to.</param>
        /// <param name="cost">The cost item to be mapped.</param>
        /// <returns>The type of policy to which <paramref name="cost"/> should be linked.</returns>
        /// <exception cref="DataAccess.CBaseException">The cost could not be associated to an appropriate policy.</exception>
        private static E_cTitleInsurancePolicyT DetermineLinkedPolicy(List<Tuple<E_cTitleInsurancePolicyT, string>> policyOrder, CCResponseCost cost)
        {
            if (cost.policy_endo_rel == -1)
            {
                var mapping = policyOrder.FirstOrDefault(p => cost.key.StartsWith(p.Item2, StringComparison.OrdinalIgnoreCase));
                if (mapping != null)
                {
                    return mapping.Item1;
                }

                Func<int, bool> isHudline = hudline => cost.GFE_Section != null && cost.GFE_Section.HUD_line == "HUD-1 Line " + hudline;
                if (isHudline(1103))
                {
                    return E_cTitleInsurancePolicyT.Owner;
                }
                else if (isHudline(1104))
                {
                    return E_cTitleInsurancePolicyT.Lender;
                }
                else
                {
                    throw CBaseException.GenericException("Cannot do endorsement - Has no policy association");
                }
            }
            else
            {
                return policyOrder[cost.policy_endo_rel].Item1;
            }
        }

        /// <summary>
        /// Creates a summary element for logging the quote request.
        /// </summary>
        /// <param name="data">The raw data of the quote request.</param>
        /// <param name="pot">First American's numerical value for the purpose of transaction.</param>
        /// <returns>The element for the summary of the request.</returns>
        private static XElement CreateSummaryLog(QuoteRequestData data, int pot)
        {
            return new XElement(
                "Summary",
                new XAttribute("LoanAmount", data.sFinalLAmt),
                new XAttribute("SalesPrice", data.sPurchPrice),
                new XAttribute("Pot", pot),
                new XAttribute("State", data.sSpState),
                new XAttribute("County", data.sSpCounty),
                new XAttribute("City", data.sSpCity),
                new XAttribute("Zipcode", data.sSpZip),
                new XAttribute("ClosingState", "NA"),
                new XAttribute("ClosingCounty", "NA"),
                new XAttribute("ClosingCity", "NA"),
                new XAttribute("LoanId", data.sLId),
                new XAttribute("LoanNumber", data.sLNm));
        }

        /// <summary>
        /// Computes the purpose of transaction we expect to use for a given loan scenario.
        /// </summary>
        /// <param name="loanPurpose">The loan purpose, read from <see cref="CPageData.sLPurposeT"/>.</param>
        /// <param name="lienPosition">The lien position, read from <see cref="CPageData.sLienPosT"/>.</param>
        /// <returns>The purpose of transaction we expect to use.</returns>
        private static int GetExpectedPurposeOfTransaction(E_sLPurposeT loanPurpose, E_sLienPosT lienPosition)
        {
            if (lienPosition == E_sLienPosT.First)
            {
                switch (loanPurpose)
                {
                    case E_sLPurposeT.Purchase: return 11; // Sale w/Mortgage
                    case E_sLPurposeT.Refin:
                    case E_sLPurposeT.RefinCashout:
                    case E_sLPurposeT.FhaStreamlinedRefinance:
                    case E_sLPurposeT.VaIrrrl: return 8; // Refinance
                    case E_sLPurposeT.Construct:
                    case E_sLPurposeT.ConstructPerm: return 4; // Construction Loan
                    case E_sLPurposeT.HomeEquity: return 5; // Equity Loan
                    case E_sLPurposeT.Other:
                    default:
                        throw new UnhandledEnumException(loanPurpose);
                }
            }

            // lienPosition == E_sLienPosT.Second
            return loanPurpose == E_sLPurposeT.HomeEquity
                ? 5 // Equity Loan
                : 15; // Second Mortgage
        }

        /// <summary>
        /// Determines the integer Purpose Of Transaction (POT) used by First American's calculation for v2 of the service.
        /// </summary>
        /// <param name="loanPurpose">The loan purpose, read from <see cref="CPageData.sLPurposeT"/>.</param>
        /// <param name="lienPosition">The lien position, read from <see cref="CPageData.sLienPosT"/></param>
        /// <param name="state">The state where the property is located.</param>
        /// <returns>The Purpose Of Transaction value First American uses for the given loan data.</returns>
        /// <remarks>
        /// This is preserved for unmigrated states, and will be deleted once First American completes their migration of
        /// the states.
        /// </remarks>
        private static int GetV2PurposeOfTransaction(E_sLPurposeT loanPurpose, E_sLienPosT lienPosition, string state)
        {
            if (loanPurpose == E_sLPurposeT.HomeEquity)
            {
                return 5; // Equity Loan
            }
            else if (lienPosition == E_sLienPosT.Second)
            {
                return 15; // Second Mortgage
            }
            else if (loanPurpose == E_sLPurposeT.Purchase)
            {
                return 11; // sale with mortgage
            }
            else
            {
                if (state == "NV")
                {
                    return 42; // residential refinance
                }
                else
                {
                    return 8; // refinance
                }
            }
        }

        /// <summary>
        /// Determines the integer Purpose Of Transaction (POT) used by First American's calculation.
        /// </summary>
        /// <param name="loanPurpose">The loan purpose, read from <see cref="CPageData.sLPurposeT"/>.</param>
        /// <param name="lienPosition">The lien position, read from <see cref="CPageData.sLienPosT"/>.</param>
        /// <param name="state">The state where the property is located.</param>
        /// <param name="propertyTypeId">The property type, or null if First American has not migrated the state.</param>
        /// <returns>The Purpose Of Transaction value First American uses for the given loan data.</returns>
        private int GetPurposeOfTransaction(E_sLPurposeT loanPurpose, E_sLienPosT lienPosition, string state, int? propertyTypeId)
        {
            // Per OPM 456467, we should return POT 61 if client ID is greater than one,
            // loan purpose is rate-term/cash-out/FHA Streamline/VA IRRRL, and the broker bit is indicated.
            int clientIdQuoteInt;
            bool isNonHomeEquityRefinance = loanPurpose == E_sLPurposeT.Refin
                || loanPurpose == E_sLPurposeT.RefinCashout
                || loanPurpose == E_sLPurposeT.FhaStreamlinedRefinance
                || loanPurpose == E_sLPurposeT.VaIrrrl;
            if (isNonHomeEquityRefinance
                && int.TryParse(this.ClientIdQuote, out clientIdQuoteInt)
                && clientIdQuoteInt > 1
                && this.CurrentBrokerDB.UsePOT61ToPullFirstAmericanLocalRefinanceRates)
            {
                return 61;
            }

            bool stateIsMigrated = propertyTypeId.HasValue;
            if (!stateIsMigrated)
            {
                return GetV2PurposeOfTransaction(loanPurpose, lienPosition, state);
            }

            int expectedPurposeOfTransaction = GetExpectedPurposeOfTransaction(loanPurpose, lienPosition);
            var service = this.CreateRateServiceClient();
            var response = this.LogAndCall(
                new enumTransactionTypes()
                {
                    ClientID = this.ClientIdQuote,
                    UserID = this.UserNameQuote,
                    State = state,
                    TransactionDate = DateTime.Now,
                    PropertyTypeID = propertyTypeId.GetValueOrDefault(),
                    PropertyTypeIDSpecified = propertyTypeId.HasValue
                },
                r => service.enumTransactionTypes(r)).FACCWSResponse;

            string expectedPotString = expectedPurposeOfTransaction.ToString();
            if (response.POTs.Any(purposeOfTransaction => purposeOfTransaction.id == expectedPotString))
            {
                return expectedPurposeOfTransaction;
            }

            throw CBaseException.GenericException($"First American did not return a purpose of transaction matching the expected id of \"{expectedPotString}\"");
        }

        /// <summary>
        /// Determines the property type id First American specifies for the given property.<para />
        /// A null value means that First American has not migrated the state.
        /// </summary>
        /// <param name="state">The state where the property is located.</param>
        /// <returns>The property type id, or null if the state has not been migrated.</returns>
        private int? GetPropertyTypeId(string state)
        {
            var service = this.CreateRateServiceClient();
            var response = this.LogAndCall(
                new enumPropertyTypes()
                {
                    UserID = this.UserNameQuote,
                    State = state,
                    TransactionDate = DateTime.Now
                },
                r => service.enumPropertyTypes(r)).FACCWSResponse;

            if (response.property_types.Length == 0)
            {
                return default(int?); // The state is not migrated to support this method
            }

            if (response.property_types.Any(propertyType => propertyType.id == "1"))
            {
                return 1; // "Residential"; all the others are not supported
            }

            throw CBaseException.GenericException($"Unable to find supported property type in state {state}.");
        }

        /// <summary>
        /// Calls the specified service with the given parameter, logging the parameter and the response.
        /// </summary>
        /// <typeparam name="TRequest">The type of the request.</typeparam>
        /// <typeparam name="TResponse">The type of the response.</typeparam>
        /// <param name="request">The object specified as the request item.</param>
        /// <param name="serviceCall">The service call to make.</param>
        /// <returns>The value returned when <paramref name="serviceCall"/> is called with <paramref name="request"/>.</returns>
        private TResponse LogAndCall<TRequest, TResponse>(TRequest request, Func<TRequest, TResponse> serviceCall)
        {
            // 4/8/2015 tj - Probably more logging than we want in the long run, but this will help debug during initial testing
            const string XmlSerializationDefaultNamespace = "FACCWebService";
            this.logInfo(SerializationHelper.XmlSerialize(request, false, XmlSerializationDefaultNamespace));
            TResponse response = default(TResponse);

            string requestTypeName = request.GetType().Name;
            Stopwatch sw = Stopwatch.StartNew();
            using (PerformanceStopwatch.Start($"FirstAmericanService - {requestTypeName}"))
            {
                response = serviceCall(request);
            }

            sw.Stop();
            this.logInfo(SerializationHelper.XmlSerialize(response, false, XmlSerializationDefaultNamespace));
            this.logInfo($"Execute FirstAmericanService {requestTypeName} in {sw.ElapsedMilliseconds:#,#} ms");
            return response;
        }

        /// <summary>
        /// Gets a set of quotes for pricing, using First American's Quick Quote concept.
        /// </summary>
        /// <param name="data">The request data about the loan requesting the quote.</param>
        /// <param name="options">Request options used to determine what quotes to order.</param>
        /// <param name="debugLog">The running debug log of the request.</param>
        /// <returns>A set of quotes of rates for title expenses.</returns>
        private QuoteResultData GetQuotesForPricing(QuoteRequestData data, QuoteRequestOptions options, XDocument debugLog)
        {
            string county = FixUpCounties(data.sSpState, data.sSpCounty);
            int? propertyTypeId = this.GetPropertyTypeId(data.sSpState);
            int pot = this.GetPurposeOfTransaction(data.sLPurposeT, data.sLienPosT, data.sSpState, propertyTypeId);
            int countyId = this.GetCountyId(data.sSpState, county);
            debugLog.Root.Add(CreateSummaryLog(data, pot));

            FACCProductDefaults defaults = this.GetProductDefaults(options, data.sSpState, countyId, pot, propertyTypeId);
            CCRequest request = this.GenerateInitialQuoteRequest(data, defaults, pot, county, countyId, propertyTypeId);

            var service = this.CreateRateServiceClient();
            getAllParamsResponse getAllParamsResponse = this.LogAndCall(
                new getAllParams()
                {
                    CCRequest = request,
                    UserID = this.UserNameQuote
                },
                r => service.getAllParams(r));

            if (getAllParamsResponse.CCRequest != null)
            {
                bool changesToRequiredParameters = FillOutRequiredParams(getAllParamsResponse.CCRequest, data, debugLog);
                bool fixedSomeParamRefs = FixUpParamRefs(getAllParamsResponse.CCRequest);
                if (changesToRequiredParameters || fixedSomeParamRefs)
                {
                    AppendSerializableToXDoc(debugLog, "getAllParamsChanges", getAllParamsResponse);
                }

                getAllParamsResponse.CCResponse = this.LogAndCall(
                    new runCalculation()
                    {
                        CCRequest = getAllParamsResponse.CCRequest,
                        UserID = this.UserNameQuote
                    },
                    r => service.runCalculation(r)).CCResponse;
            }

            CCResponse response = getAllParamsResponse.CCResponse;
            QuoteResultData fees = new QuoteResultData(
                this.GetCost(debugLog, data, request, response),
                defaults.HasRecordingDocsButNoneSpecified,
                debugLog.ToString());

            return fees;
        }

        /// <summary>
        /// Looks up First American's identifier for the specified county.
        /// </summary>
        /// <param name="state">The two letter abbreviation of the state's name.</param>
        /// <param name="county">The name of the county.</param>
        /// <returns>The identifier for the county in First American's system.</returns>
        private int GetCountyId(string state, string county)
        {
            // Need to use double quotes around the county because some counties can have single quotes in them.
            string selector = string.Format("/FACCCOUNTYINFO/STATE[@ID='{0}']/COUNTY[@NAME=\"{1}\"]", state, county.ToUpper());
            XElement countyElement = this.faccCountyInfo.XPathSelectElement(selector);
            if (countyElement == null)
            {
                if (county.EndsWith("County", StringComparison.OrdinalIgnoreCase))
                {
                    string updatedCounty = Regex.Replace(county, " County$", string.Empty, RegexOptions.IgnoreCase).TrimWhitespaceAndBOM();
                    XElement element = this.faccCountyInfo.XPathSelectElement(string.Format("/FACCCOUNTYINFO/STATE[@ID='{0}']/COUNTY[@NAME=\"{1}\"]", state, updatedCounty));
                    if (element != null)
                    {
                        return int.Parse(element.Attribute("ID").Value);
                    }
                }

                Tools.LogError("[FACC] - Could not find county in " + selector);
                throw CBaseException.GenericException("Could not generate quote; county was not found.");
            }
            else
            {
                return int.Parse(countyElement.Attribute("ID").Value);
            }
        }

        /// <summary>
        /// Gets a list of products to use by default with the products.
        /// </summary>
        /// <param name="options">Request options used to determine what quotes to order.</param>
        /// <param name="state">The state, used as both property and closing state.</param>
        /// <param name="propertyCountyId">The identifier for the property county in First American's system.</param>
        /// <param name="pot">The identifier for the Purpose Of Transaction in First American's system.</param>
        /// <param name="propertyTypeId">The property type, or null if First American has not migrated the state.</param>
        /// <returns>Information about the default products available for the specified situation.</returns>
        private FACCProductDefaults GetProductDefaults(QuoteRequestOptions options, string state, int propertyCountyId, int pot, int? propertyTypeId)
        {
            var service = this.CreateRateServiceClient();

            // If we determine that the state is one that the FATCO interface will request title and recording fees for but not closing/escrow fees, then we 
            // send "false" in the closing parameter. 
            string closingValue = options.RequestClosing && !this.CurrentBrokerDB.TitleInterfaceTitleRecordingOnly(state) ? "true" : "false";

            var getProductDefaults = new getProductDefaults()
            {
                UserID = this.UserNameQuote,
                PropertyState = state,
                ClosingState = state,
                Title = options.RequestTitle.ToString().ToLower(),
                Closing = closingValue,
                Recording = options.RequestRecording.ToString().ToLower(),
                TransactionDate = DateTime.Now,
                POTID = pot,
                ClientID = this.ClientIdQuote,
                PropertyCountyLocationID = propertyCountyId,
                ClosingCountyLocationID = 0,
                PropertyTypeID = propertyTypeId.GetValueOrDefault(),
                PropertyTypeIDSpecified = propertyTypeId.HasValue,
            };

            var getProductDefaultsResponse = this.LogAndCall(
                getProductDefaults,
                r => service.getProductDefaults(r));

            return new FACCProductDefaults(getProductDefaults, getProductDefaultsResponse);
        }

        /// <summary>
        /// Determines which amount to read cost value, based on the <see cref="CurrentBrokerDB"/>.
        /// </summary>
        /// <param name="cost">The cost item to read from.</param>
        /// <param name="defaultValue">The default value for the cost, used when <see cref="BrokerDB.FirstAmericanImportSource"/> is Normal.</param>
        /// <returns>The amount of the cost under the configured mapping.</returns>
        /// <exception cref="UnhandledEnumException"><see cref="BrokerDB.FirstAmericanImportSource"/> is an unhandled value.</exception>
        private decimal GetLegacyAmountFromCost(CCResponseCost cost, Func<CCResponseCost, decimal> defaultValue)
        {
            E_FirstAmericanFeeImportSource source = this.CurrentBrokerDB.FirstAmericanImportSource;

            switch (source)
            {
                case E_FirstAmericanFeeImportSource.Normal:
                    return defaultValue(cost);
                case E_FirstAmericanFeeImportSource.Buyer:
                    return cost.amount_buyer;
                case E_FirstAmericanFeeImportSource.Seller:
                    return cost.amount_seller;
                default:
                    throw new UnhandledEnumException(source);
            }
        }

        /// <summary>
        /// Determines the amounts to map from the cost into values to go into LendingQB.
        /// </summary>
        /// <param name="cost">The cost item to map into the appropriate amounts.</param>
        /// <param name="data">The quote request data.</param>
        /// <param name="legacyDefaultMapping">The previous default mapping of the cost.</param>
        /// <returns>The amounts of the cost, possibly split into borrower and seller responsible sections.</returns>
        private QuoteResultData.TitleSplitCost DetermineCost(CCResponseCost cost, QuoteRequestData data, Func<CCResponseCost, decimal> legacyDefaultMapping)
        {
            if (data.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                return new QuoteResultData.TitleSplitCost(
                    cost.name,
                    HasNonZeroAmountBuyerOrSeller(cost.GFE_Section) ? cost.GFE_Section.amount_buyer : cost.amount_buyer,
                    HasNonZeroAmountBuyerOrSeller(cost.GFE_Section) ? cost.GFE_Section.amount_seller : cost.amount_seller,
                    cost.cost_type == "PBO" ? cost.GFE_Section?.amount ?? cost.amount : default(decimal?));
            }
            else
            {
                return new QuoteResultData.TitleSplitCost(cost.name, this.GetLegacyAmountFromCost(cost, legacyDefaultMapping), null, null);
            }
        }

        /// <summary>
        /// Reads the cost elements from First American's <paramref name="response"/> to create a <see cref="QuoteResultData"/> containing the values of the fees we received.
        /// </summary>
        /// <param name="debugLog">The running debug log of the quote process.</param>
        /// <param name="data">The loan data for this quote request.</param>
        /// <param name="request">The initial request sent to First American, used to check for the types of fees the defaults had.</param>
        /// <param name="response">The calculation response from First American.</param>
        /// <returns>The total fees of the quote generated by the request.</returns>
        private QuoteResultData.TitleFeeBuilder GetCost(XDocument debugLog, QuoteRequestData data, CCRequest request, CCResponse response)
        {
            XElement costMappings = new XElement("CostMappings");
            debugLog.Root.Add(costMappings);

            QuoteResultData.TitleFeeBuilder quoteFees = new QuoteResultData.TitleFeeBuilder();
            if (response.costs == null)
            {
                return quoteFees;
            }

            // we need this in case of endorsements. They use the order to determine which policy it goes to.
            var policyOrder = new List<Tuple<E_cTitleInsurancePolicyT, string>>();
            List<CCResponseCost> endorsementsAndMisc = new List<CCResponseCost>();

            foreach (CCResponseCost cost in response.costs)
            {
                E_LegacyGfeFieldT gfeFieldType;
                Func<CCResponseCost, decimal> legacyDefaultMapping;

                if (cost.key.StartsWith("P_MISC", StringComparison.OrdinalIgnoreCase))
                {
                    // according to the GFE Logic Word Doc these go under either lender or owner title insurance 
                    // so there must be a policy relation. 
                    endorsementsAndMisc.Add(cost);
                    continue;
                }
                else if (cost.key.StartsWith("P", StringComparison.OrdinalIgnoreCase))
                {
                    if (cost.cost_sub_type == "TAX")
                    {
                        // we can't figure this one out yet and sadly theres no endo id either :(.
                        endorsementsAndMisc.Add(cost);
                        continue;
                    }

                    legacyDefaultMapping = FallbackAmount;
                    if (cost.name.IndexOf("owner", StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        gfeFieldType = E_LegacyGfeFieldT.sOwnerTitleInsF;
                        policyOrder.Add(Tuple.Create(E_cTitleInsurancePolicyT.Owner, cost.key));
                    }
                    else
                    {
                        gfeFieldType = E_LegacyGfeFieldT.sTitleInsF;
                        policyOrder.Add(Tuple.Create(E_cTitleInsurancePolicyT.Lender, cost.key));
                    }
                }
                else if (cost.key.StartsWith("E", StringComparison.OrdinalIgnoreCase))
                {
                    // according to gfe logic doc we need to check which title policy they fall under.
                    endorsementsAndMisc.Add(cost);
                    continue;
                }
                else if (cost.key.StartsWith("CT", StringComparison.OrdinalIgnoreCase))
                {
                    gfeFieldType = E_LegacyGfeFieldT.sEscrowF;
                    legacyDefaultMapping = FallbackAmountBuyer;
                }
                else if (cost.key.StartsWith("F", StringComparison.OrdinalIgnoreCase))
                {
                    if ((data.sSpState == "AZ" && cost.name.Equals("RECORDING FEE", StringComparison.OrdinalIgnoreCase)) || cost.name.StartsWith("RECORDING FEE (", StringComparison.OrdinalIgnoreCase))
                    {
                        gfeFieldType = E_LegacyGfeFieldT.sRecF;
                        legacyDefaultMapping = FallbackAmount;
                    }
                    else
                    {
                        gfeFieldType = E_LegacyGfeFieldT.sEscrowF;
                        legacyDefaultMapping = FallbackAmountBuyer;
                    }
                }
                else if (cost.key.StartsWith("RF", StringComparison.OrdinalIgnoreCase))
                {
                    if (cost.name.StartsWith("DEED -", StringComparison.OrdinalIgnoreCase))
                    {
                        gfeFieldType = E_LegacyGfeFieldT.sRecDeed;
                        legacyDefaultMapping = FallbackAmount;
                    }
                    else if (cost.name.StartsWith("RELEASE -", StringComparison.OrdinalIgnoreCase))
                    {
                        gfeFieldType = E_LegacyGfeFieldT.sRecRelease;
                        legacyDefaultMapping = FallbackAmount;
                    }
                    else
                    {
                        gfeFieldType = E_LegacyGfeFieldT.sRecMortgage;
                        legacyDefaultMapping = FallbackAmount;
                    }
                }
                else if (cost.key.StartsWith("RT", StringComparison.OrdinalIgnoreCase))
                {
                    gfeFieldType = E_LegacyGfeFieldT.sCountyRtc;
                    legacyDefaultMapping = FallbackAmount;
                }
                else if (cost.key.StartsWith("CPL", StringComparison.OrdinalIgnoreCase))
                {
                    throw CBaseException.GenericException("CPL Cost are unhandled");
                }
                else
                {
                    continue;
                }

                var mappedCost = this.DetermineCost(cost, data, legacyDefaultMapping);
                quoteFees.AddCost(gfeFieldType, mappedCost);
                costMappings.Add(GetDebugElementFromCost(cost, mappedCost, gfeFieldType));
            }

            foreach (CCResponseCost cost in endorsementsAndMisc)
            {
                var mappedCost = this.DetermineCost(cost, data, FallbackAmount);
                if (mappedCost.TotalAmount == 0)
                {
                    continue;
                }

                E_cTitleInsurancePolicyT policy = DetermineLinkedPolicy(policyOrder, cost);
                E_LegacyGfeFieldT gfeFieldType;
                if (policy == E_cTitleInsurancePolicyT.Lender)
                {
                    gfeFieldType = E_LegacyGfeFieldT.sTitleInsF;
                }
                else
                {
                    gfeFieldType = E_LegacyGfeFieldT.sOwnerTitleInsF;
                }

                quoteFees.AddCost(gfeFieldType, mappedCost);
                costMappings.Add(GetDebugElementFromCost(cost, mappedCost, gfeFieldType));
            }

            bool hasTitleProducts = request.title_products.Length > 0;
            quoteFees.FeesSetByTheTitleVendor[E_LegacyGfeFieldT.sTitleInsF] = hasTitleProducts;
            quoteFees.FeesSetByTheTitleVendor[E_LegacyGfeFieldT.sOwnerTitleInsF] = hasTitleProducts;

            bool hasValidClosingTypeId = request.closing_costs.closing_id > 0;
            ////quoteFees.FeesSetByTheTitleVendor[E_LegacyGfeFieldT.sNotaryF] = hasValidClosingTypeId; // we don't populate notary anymore
            quoteFees.FeesSetByTheTitleVendor[E_LegacyGfeFieldT.sEscrowF] = hasValidClosingTypeId;

            bool hasRecordingDocuments = request.recording_documents.Length > 0;
            quoteFees.FeesSetByTheTitleVendor[E_LegacyGfeFieldT.sCountyRtc] = hasRecordingDocuments;
            ////quoteFees.FeesSetByTheTitleVendor[E_LegacyGfeFieldT.sRecF] = hasRecordingDocuments;
            quoteFees.FeesSetByTheTitleVendor[E_LegacyGfeFieldT.sRecDeed] = hasRecordingDocuments;
            quoteFees.FeesSetByTheTitleVendor[E_LegacyGfeFieldT.sRecMortgage] = hasRecordingDocuments;
            quoteFees.FeesSetByTheTitleVendor[E_LegacyGfeFieldT.sRecRelease] = hasRecordingDocuments;

            return quoteFees;
        }

        /// <summary>
        /// Submits an order for a title policy to the vendor.
        /// </summary>
        /// <param name="loanId">The <see cref="CPageData.sLId"/> of the loan placing the policy order.</param>
        /// <param name="policyOrder">The policy order to submit to the vendor.</param>
        private void SubmitPolicyOrder(Guid loanId, TitleRequest policyOrder)
        {
            // Serialize to XML
            XmlSerializer serializer = new XmlSerializer(typeof(TitleRequest));
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);

            byte[] bytes = null;
            using (MemoryStream stream = new MemoryStream(50000))
            {
                using (XmlTextWriter xmlTextWriter = new XmlTextWriter(stream, System.Text.Encoding.ASCII))
                {
                    serializer.Serialize(xmlTextWriter, policyOrder, ns);
                }

                bytes = stream.ToArray();
            }

            string requestString = System.Text.Encoding.ASCII.GetString(bytes);
            requestString = Regex.Replace(requestString, "<Password>[^ ]+</Password>", "<Password>******</Password>"); // mask the password
            StringBuilder log = new StringBuilder("=== TITLE POLICY REQUEST ===" + Environment.NewLine + requestString);

            // Submit
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(this.ExportPathTitle);
            webRequest.Method = "POST";
            webRequest.ContentType = "text/xml";
            webRequest.ContentLength = bytes.Length;
            webRequest.Timeout = 60000;

            DateTime orderDateTime = DateTime.Now;

            using (Stream stream = webRequest.GetRequestStream())
            {
                stream.Write(bytes, 0, bytes.Length);
            }

            // Get the response
            StringBuilder sb = new StringBuilder();
            using (WebResponse webResponse = webRequest.GetResponse())
            {
                using (Stream stream = webResponse.GetResponseStream())
                {
                    byte[] buffer = new byte[60000];
                    int size = stream.Read(buffer, 0, buffer.Length);
                    while (size > 0)
                    {
                        sb.Append(System.Text.Encoding.ASCII.GetString(buffer, 0, size));
                        size = stream.Read(buffer, 0, buffer.Length);
                    }
                }
            }

            string responseXml = sb.ToString();
            log.AppendLine();
            log.Append("=== TITLE POLICY RESPONSE ===" + Environment.NewLine + responseXml);
            Tools.LogInfo(log.ToString());

            LqbTitleResponse parsedResponse = GetLQBTitleResponse(responseXml);

            // write the order to the logs & set the result
            this.SavePolicyLog(loanId, requestString, responseXml, orderDateTime, parsedResponse.TransactionId, parsedResponse.Response.PolicyResponse.PolicyId);

            TitleAuditor.RecordQCAudit(responseXml, loanId);

            switch (parsedResponse.RequestStatus)
            {
                case E_LqbTitleResponseRequestStatus.Error:
                    this.TitleServiceResult = new TitleServiceResult(false, parsedResponse.ServerMessage.Description);
                    break;
                case E_LqbTitleResponseRequestStatus.Success:
                    this.TitleServiceResult = new TitleServiceResult(true, parsedResponse.Response.PolicyResponse.ConfirmationMessage);
                    break;
                case E_LqbTitleResponseRequestStatus.Undefined:
                    this.TitleServiceResult = new TitleServiceResult(true, "Title policy order sent.");
                    break;
                default:
                    throw new UnhandledEnumException(parsedResponse.RequestStatus);
            }
        }

        /// <summary>
        /// Saves a log of the policy as ordered, with the vendor's response.
        /// </summary>
        /// <param name="loanId">The loan Id (<see cref="CPageData.sLId"/>) of the loan for which title has been ordered.</param>
        /// <param name="policyRequest">The string representation of the policy request.</param>
        /// <param name="vendorResponse">The string representation of the vendor's response.</param>
        /// <param name="orderDateTime">The date and time when the order was placed.</param>
        /// <param name="transactionId">The identifier of the transaction.</param>
        /// <param name="policyId">The identifier of the policy ordered.</param>
        private void SavePolicyLog(Guid loanId, string policyRequest, string vendorResponse, DateTime orderDateTime, string transactionId, string policyId)
        {
            QuoteLog policyLogResult = new QuoteLog();
            policyLogResult.PolicyOrderedD = orderDateTime;
            policyLogResult.PolicyProviderName = E_TitleVendorIntegrationType.FirstAmerican.ToString();

            CPageData loanData = CPageData.CreateUsingSmartDependency(loanId, typeof(FirstAmericanServiceV3));
            loanData.InitLoad();
            policyLogResult.LoanData = loanData.GetTitleQuoteRequestData();

            policyLogResult.AddItem(XmlPolicyRequestKey, policyRequest);
            policyLogResult.AddItem(XmlPolicyResponseKey, vendorResponse);

            policyLogResult.PolicyID = policyId;
            policyLogResult.TransactionID = transactionId;
            policyLogResult.IsAPolicyOrderLog = true;

            this.SaveQuoteLog(loanId, policyLogResult);
        }

        /// <summary>
        /// Generates the basic request, including the recording docs.
        /// </summary>
        /// <param name="data">The request data to transform into a <see cref="CCRequest"/>.</param>
        /// <param name="defaults">The product defaults to apply to the <see cref="CCRequest"/>.</param>
        /// <param name="pot">The purpose of transaction for this quote request.</param>
        /// <param name="county">The name of the county in First American's system.</param>
        /// <param name="countyId">The identifier defined by First American for the county.</param>
        /// <param name="propertyTypeId">The property type, or null if First American has not migrated the state.</param>
        /// <returns>
        /// The <see cref="CCRequest"/> to transmit to First American as our initial request.
        /// </returns>
        private CCRequest GenerateInitialQuoteRequest(QuoteRequestData data, FACCProductDefaults defaults, int pot, string county, int countyId, int? propertyTypeId)
        {
            CCRequest request = new CCRequest();
            request.transaction_dt = DateTime.Now.ToShortDateString();
            request.client_id = this.ClientIdQuote;
            request.calc_method = "one_phase";
            request.disclosure_type = data.sDisclosureRegulationT == E_sDisclosureRegulationT.GFE ? "GFE" : "LoanEstimate";

            request.recording_documents = new CCRequestRecording_document[defaults.RecordingDocuments.Count];
            for (int i = 0; i < defaults.RecordingDocuments.Count; ++i)
            {
                FACCRecordingDocInfo recEntry = defaults.RecordingDocuments[i];
                var document = new CCRequestRecording_document();
                document.page_count = recEntry.EstNumberOfPages;
                document.page_countSpecified = true;
                document.document_type = recEntry.DocumentType.ToString();
                document.consideration_amount = GetConsiderationAmount(recEntry.LiabilitySource, data);
                document.consideration_amountSpecified = true;

                if (document.page_count == 0)
                {
                    document.page_count = 15;
                }

                request.recording_documents[i] = document;
            }

            request.title_products = new CCRequestTitle_product[defaults.TitlePolicies.Count];
            List<CCRequestEndorsement> endorsements = new List<CCRequestEndorsement>();
            for (int i = 0; i < defaults.TitlePolicies.Count; i++)
            {
                FACCTitlePolicyInfo titleEntry = defaults.TitlePolicies[i];
                var product = new CCRequestTitle_product();
                product.ratetype_id = titleEntry.RateTypeId.ToString();
                product.NoSimuDiscount = ToYN(defaults.TitlePolicies.Count == 1);
                product.policy_id = titleEntry.PolicyId.ToString();
                product.liability_amount = GetConsiderationAmount(titleEntry.LiabilitySource, data);
                product.liability_amountSpecified = true;
                request.title_products[i] = product;

                endorsements.AddRange(
                    titleEntry.EndorsementsIds.Select(id => new CCRequestEndorsement()
                    {
                        endorsement_id = id,
                        endorsement_idSpecified = true,
                        title_product_ordinal = i,
                        title_product_ordinalSpecified = true,
                    }));
            }

            request.endorsements = endorsements.ToArray();

            List<CCRequestParam> paramsToAdd = new List<CCRequestParam>();

            // First-level Params
            int key = 1;
            paramsToAdd.Add(CreateParam(key++, 275, "City", E_ValueType.STRING, data.sSpCity));
            paramsToAdd.Add(CreateParam(key++, 277, "County", E_ValueType.STRING, county));
            paramsToAdd.Add(CreateParam(key++, 280, "Liability Amount", E_ValueType.CURRENCY, Math.Round(data.sFinalLAmt, 2).ToString()));
            paramsToAdd.Add(CreateParam(key++, 283, "Purpose of transaction", E_ValueType.STRING, pot.ToString()));
            paramsToAdd.Add(CreateParam(key++, 291, "Sale Amount", E_ValueType.CURRENCY, Math.Round(data.sPurchPrice, 2).ToString()));
            paramsToAdd.Add(CreateParam(key++, 293, "State", E_ValueType.STRING, data.sSpState));
            paramsToAdd.Add(CreateParam(key++, 296, "Zip Code", E_ValueType.INTEGER, data.sSpZip));
            paramsToAdd.Add(CreateParam(key++, 1279, "Not for Profit Lender (Credit Union)", E_ValueType.BOOLEAN, BrokerDB.RetrieveById(data.sBrokerId).IsTaxExemptCreditUnion ? "1" : "2"));

            // Closing Costs and fees
            request.closing_costs = new CCRequestClosing_costs();
            if (defaults.HasClosing)
            {
                paramsToAdd.Add(CreateParam(key++, 330, "Closing State", E_ValueType.STRING, data.sSpState));
                paramsToAdd.Add(CreateParam(key++, 331, "Closing County", E_ValueType.STRING, "Default"));
                paramsToAdd.Add(CreateParam(key++, 332, "Closing City", E_ValueType.STRING, "..."));

                if (defaults.ClosingId != 0)
                {
                    request.closing_costs.closing_id = defaults.ClosingId;
                    request.closing_costs.closing_idSpecified = true;
                }

                request.closing_costs.closing_fees = defaults.ClosingFees.Select(closingFee => new CCRequestClosing_costsClosing_fee()
                {
                    name = closingFee.name,
                    quantity = int.Parse(closingFee.defaultvalue),
                    quantitySpecified = true,
                    id = int.Parse(closingFee.id),
                    idSpecified = true
                })
                .ToArray();
            }

            paramsToAdd.Add(CreateParam(key++, 1027, "Excess Prior Liability over Prod 1", E_ValueType.CURRENCY, string.Empty));
            if (propertyTypeId.HasValue)
            {
                paramsToAdd.Add(CreateParam(key++, 269, "Property Type", E_ValueType.INTEGER, propertyTypeId.ToString()));
            }

            request.@params = paramsToAdd.ToArray();

            return request;
        }

        /// <summary>
        /// Retrieves from First American a list of valid title policies for the specified conditions.
        /// </summary>
        /// <param name="propertyState">The state of the property, mapping from <see cref="CPageData.sSpState"/>.</param>
        /// <param name="propertyCountyID">The identifier for the property county in First American's system.</param>
        /// <param name="purposeOfTransactionID">The Purpose Of Transaction identifier in First American's system.</param>
        /// <param name="propertyTypeId">The property type, or null if First American has not migrated the state.</param>
        /// <returns>An enumeration of the policies available through LendingQB.</returns>
        private IEnumerable<PolicyInfo> GetFAPolicyList(string propertyState, int propertyCountyID, int purposeOfTransactionID, int? propertyTypeId)
        {
            var service = this.CreateRateServiceClient();
            FACCWSResponse response = this.LogAndCall(
                new enumPolicies()
                {
                    ClientID = this.ClientIdQuote,
                    UserID = this.UserNameQuote,
                    State = propertyState,
                    PropertyCountyID = propertyCountyID,
                    PropertyCountyIDSpecified = true,
                    POTID = purposeOfTransactionID,
                    TransactionDate = DateTime.Now,
                    PropertyTypeID = propertyTypeId.GetValueOrDefault(),
                    PropertyTypeIDSpecified = propertyTypeId.HasValue,
                },
                r => service.enumPolicies(r)).FACCWSResponse;

            if (response.title_products != null && response.title_products.title_product != null)
            {
                return response.title_products.title_product
                    .Select(policy => new
                    {
                        Name = policy.policy_name,
                        PolicyID = policy.policy_id.ToString(),
                        CategoryID = int.Parse(policy.policycategory_id)
                    })
                    .Where(policy => policy.CategoryID == 1 || policy.CategoryID == 2) // only include owner and lender policies
                    .Select(policy => new PolicyInfo(policy.PolicyID, policy.Name, policy.CategoryID == 1 ? E_cTitleInsurancePolicyT.Owner : E_cTitleInsurancePolicyT.Lender));
            }

            return new PolicyInfo[0];
        }

        /// <summary>
        /// Creates a service client pointing to the address specified in the lender settings.
        /// </summary>
        /// <returns>The client connecting to the service.</returns>
        private FACCV3_0Client CreateRateServiceClient()
        {
            const string EndpointConfigurationName = "Facc_current_ext_20161228";
            return string.IsNullOrEmpty(this.ExportPathQuote)
                ? new FACCV3_0Client(EndpointConfigurationName)
                : new FACCV3_0Client(EndpointConfigurationName, this.ExportPathQuote);
        }
    }
}
