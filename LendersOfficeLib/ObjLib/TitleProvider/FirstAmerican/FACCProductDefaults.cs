﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.ObjLib.TitleProvider.FirstAmerican.XmlSerialization.CAPI.V1;

namespace LendersOffice.ObjLib.TitleProvider.FirstAmerican
{
    internal class FACCProductDefaults
    {
        internal List<FACCTitlePolicyInfo> TitlePolicies { get; set; }
        internal List<FACCRecordingDocInfo> RecordingDocuments { get; set; }
        internal int ClosingId { get; set; }
        internal IReadOnlyCollection<LendersOffice.FirstAmerican.CAPI.V3_0.closing_feesClosing_fee> ClosingFees { get; set; }
        private bool recordingFeesAvailable;

        private bool recordingFeesRequested;

        internal bool HasTitlePolicies
        {
            get { return this.TitlePolicies.Count > 0; }
        }

        internal bool HasRecordingDocuments
        {
            get { return this.RecordingDocuments.Count > 0; }
        }

        /// <summary>
        /// First American returned that there are rates available for recording docs but didnt include any recording documents to request.
        /// </summary>
        internal bool HasRecordingDocsButNoneSpecified
        {
            get { return this.recordingFeesRequested && this.recordingFeesAvailable && this.RecordingDocuments.Count == 0; }
        }

        internal bool HasClosing
        {
            get { return this.ClosingId > -1; }
        }

        internal FACCProductDefaults(LendersOffice.FirstAmerican.CAPI.V3_0.getProductDefaults request, LendersOffice.FirstAmerican.CAPI.V3_0.getProductDefaultsResponse response)
        {
            this.TitlePolicies = new List<FACCTitlePolicyInfo>();
            this.RecordingDocuments = new List<FACCRecordingDocInfo>();

            var root = response.Product_Defaults;

            if (root.title_products?.title_product != null)
            {
                foreach (var title in root.title_products.title_product)
                {
                    List<int> endorsementIds = new List<int>();

                    if (title.endorsements != null)
                    {
                        foreach (var endorsement in title.endorsements)
                        {
                            endorsementIds.Add(int.Parse(endorsement.endorsement_id));
                        }
                    }

                    this.TitlePolicies.Add(new FACCTitlePolicyInfo()
                    {
                        LiabilitySource = title.liability_source,
                        Order = title.order,
                        PolicyId = title.policy_id,
                        RateTypeId = int.Parse(title.ratetype_id),
                        EndorsementsIds = endorsementIds
                    });
                }
            }

            var element = root.recording_documents;
            this.recordingFeesAvailable = string.Equals(element.rates_available, "Y", StringComparison.OrdinalIgnoreCase);

            this.recordingFeesRequested = string.Equals(request.Recording, "true", StringComparison.OrdinalIgnoreCase);

            if (root.recording_documents?.recording_document != null)
            {
                foreach (var recordingDoc in root.recording_documents.recording_document)
                {
                    this.RecordingDocuments.Add(new FACCRecordingDocInfo()
                    {
                        DocType = recordingDoc.document_type,
                        EstNumberOfPages = int.Parse(recordingDoc.estimated_numberof_pages),
                        LiabilitySource = recordingDoc.liability_source,
                    });
                }
            }

            if (root.closing?.closing_id != null)
            {
                this.ClosingId = int.Parse(root.closing.closing_id);
            }

            this.ClosingFees = root.closing?.closing_fees ?? new LendersOffice.FirstAmerican.CAPI.V3_0.closing_feesClosing_fee[0];
        }
    }

    public class FACCTitlePolicyInfo
    {
        internal int PolicyId { get; set; }
        internal int RateTypeId { get; set; }
        internal int Order { get; set; }
        internal string LiabilitySource { get; set; }
        internal List<int> EndorsementsIds { get; set; }

        internal FACCTitlePolicyInfo()
        {
            this.EndorsementsIds = new List<int>();
        }
    }

    public class FACCRecordingDocInfo
    {
        internal string DocType { get; set; }
        internal E_DocumentType DocumentType
        {
            get
            {
                switch (this.DocType.ToUpper())
                {
                    case "DEED":
                        return E_DocumentType.DEED;
                    case "MORTGAGE":
                        return E_DocumentType.MORTGAGE; 
                    default:
                        throw CBaseException.GenericException("Unhandled doc type " + this.DocType);
                }
            }
        }

        internal int EstNumberOfPages { get; set; }
        internal string LiabilitySource { get; set; }
    }
}
