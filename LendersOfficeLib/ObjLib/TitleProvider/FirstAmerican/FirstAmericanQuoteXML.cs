﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Xml;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.ObjLib.TitleProvider.FirstAmerican.XmlSerialization
{
    internal enum E_ValueType
    {
        INTEGER = 0,
        DATE = 1,
        STRING = 2,
        BOOLEAN = 3,
        CURRENCY = 4,
        NUMBER_RANGE = 5,
        DATE_RANGE = 6
    }
}

namespace LendersOffice.ObjLib.TitleProvider.FirstAmerican.XmlSerialization.CAPI.V1
{
    internal enum E_DocumentType
    {
        DEED,
        MORTGAGE,
        ASSIGNMENT,
        SATISFACTION,
        AMENDMENT,
        SUBORDINATION,
        POA
    }

    internal enum E_CostType
    {
        TITLE,
        LOCAL,
        ESCROW,
        CLOSING
    }

    internal enum E_CostSubType
    {
        RECORDING_TAX,
        RECORDING_FEE,
        TAX,
        CLOSING,
        PREMIUM,
        ENDORSEMENT,
        Total
    }

    internal sealed class Option : XmlSerializableCommon.AbstractXmlSerializable
    {
        public string Value { get; set; }
        public string Key { get; set; }

        public override void ReadXml(System.Xml.XmlReader reader)
        {
            reader.MoveToContent();
            this.Value = reader.GetAttribute("value");
            this.Key = reader.GetAttribute("key");
            bool isEmptyElement = reader.IsEmptyElement; // (1)
            reader.ReadStartElement();

            if (!isEmptyElement)
            {
                reader.ReadEndElement();
            }
        }

        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteAttributeString("value", this.Value);
            writer.WriteAttributeString("key", this.Key);
        }
    }

    internal sealed class ParameterValue : XmlSerializableCommon.AbstractXmlSerializable
    {
        public E_ValueType ValueType { get; set; }
        public string Value { get; set; }
        public string RangeValStart { get; set; }
        public string RangeValEnd { get; set; }

        public override void ReadXml(System.Xml.XmlReader reader)
        {
            reader.MoveToContent();
            string valueType = reader.GetAttribute("value_type");
            if (!string.IsNullOrEmpty(valueType))
            {
                this.ValueType = (E_ValueType)Enum.Parse(typeof(E_ValueType), valueType);
            }

            bool isEmpty = reader.IsEmptyElement;
            reader.ReadStartElement(); // read <param_value>

            if (isEmpty)
            {
                return;
            }

            while (reader.NodeType == System.Xml.XmlNodeType.Element)
            {
                if (!reader.IsEmptyElement)
                {
                    switch (reader.Name)
                    {
                        case "value":
                            this.Value = reader.ReadElementString();
                            break;
                        case "range_val_start":
                            this.RangeValStart = reader.ReadElementString();
                            break;
                        case "range_val_end":
                            this.RangeValEnd = reader.ReadElementString();
                            break;
                        default:
                            throw new NotImplementedException(reader.Name);
                    }
                }
                else
                {
                    reader.ReadStartElement();
                }
            }

            reader.ReadEndElement(); // consume </param_value>
        }

        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            WriteAttribute(writer, "value_type", this.ValueType.ToString());
            writer.WriteElementString("value", this.Value);

            if (!string.IsNullOrEmpty(this.RangeValEnd))
            {
                WriteElementString(writer, "range_val_end", this.RangeValEnd);
            }

            if (!string.IsNullOrEmpty(this.RangeValEnd))
            {
                WriteElementString(writer, "range_val_start", this.RangeValEnd);
            }
        }
    }

    internal sealed class Parameter : XmlSerializableCommon.AbstractXmlSerializable
    {
        /// <summary>
        /// Required - A unique identifier for the parameter within one request.
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Code - Required Parameter Code (multiple instances of the 
        /// same parameter can exist in the same request)
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// A Y or N identifier that is passed in level II parameters 
        /// from FACC to let the calling system know if this is required input or optional input.
        /// </summary>
        public bool Required { get; set; }

        /// <summary>
        /// A Y or N identifier that is passed in level II parameters
        /// from FACC to let the calling system know if the FACC has
        /// already applied a default value to the parameter or not.
        /// </summary>
        public bool Defaulted { get; set; }

        /// <summary>
        /// A Y or N identifier that is passed in level II parameters from 
        /// FACC to let the calling system know if it should present a choice
        /// list to the user rather than free form input.
        /// </summary>
        public bool OptionsEnforced { get; set; }

        /// <summary>
        /// When the FACC returns level II parameters it may provide a hint to the
        /// calling system to let it know the preferred display order for the user.
        /// This can be ignored by the integrator if not practical.
        /// </summary>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// The name of the parameter, this value is just for display purposes.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// A Y or N identifier that the integrated system can use to keep track of whether
        /// the item was answered or not. This value is not used by the FACC. Please see the 
        /// parameter references section for more info on why this may be used.
        /// </summary>
        public bool Answered { get; set; }

        /// <summary>
        /// A string that is returned by the FACC that should be used as the prompting label 
        /// for the user. This string is usually in the form of a question.
        /// </summary>
        public string Prompt { get; set; }

        /// <summary>
        /// A collection of options that should be used as the sole basis for user 
        /// input when the options_enforced attribute is “Y”.  The integrating system
        /// should display these items as a dropdown list.
        /// </summary>
        public List<Option> Options { get; set; }

        /// <summary>
        /// The value that has been assigned to this parameter, see the parameter value node 
        /// </summary>
        public ParameterValue ParameterValue { get; set; }

        public Parameter()
        {
            this.Options = new List<Option>();
        }

        public override void ReadXml(System.Xml.XmlReader reader)
        {
            reader.MoveToContent();
            this.Key = reader.GetAttribute("key");
            this.Code = reader.GetAttribute("code");
            this.Required = reader.GetAttribute("required") == "Y";
            this.OptionsEnforced = reader.GetAttribute("options_enforced") == "Y";
            this.Name = reader.GetAttribute("name");
            this.Answered = reader.GetAttribute("answered") == "Y";
            bool isEmpty = reader.IsEmptyElement;
            reader.ReadStartElement(); // consume <param>

            if (isEmpty)
            {
                return;
            }

            while (reader.NodeType == System.Xml.XmlNodeType.Element)
            {
                switch (reader.Name.ToLower())
                {
                    case "param_value":
                        this.ParameterValue = new ParameterValue();
                        this.ParameterValue.ReadXml(reader);
                        break;
                    case "options":
                        bool hasOptions = !reader.IsEmptyElement;
                        reader.ReadStartElement(); // consume <options>
                        if (hasOptions)
                        {
                            while (reader.NodeType == System.Xml.XmlNodeType.Element)
                            {
                                Option p = new Option();
                                p.ReadXml(reader);
                            }

                            reader.ReadEndElement(); // consume </options>
                        }

                        break;
                    case "prompt":
                        bool hasPrompt = !reader.IsEmptyElement;
                        reader.ReadStartElement(); // consume <prompt>
                        if (hasPrompt)
                        {
                            this.Prompt = reader.ReadString();
                            reader.ReadEndElement(); // consume </prompt>
                        }

                        break;
                    case "condition":
                        bool hasConditions = !reader.IsEmptyElement;
                        reader.ReadStartElement();
                        if (hasConditions)
                        {
                            throw new NotImplementedException("Condition parsing not implemented.");
                        }

                        break;
                    default:
                        throw new NotImplementedException(reader.Name);
                }
            }

            reader.ReadEndElement(); // consume </param>
        }

        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteAttributeString("key", this.Key);
            writer.WriteAttributeString("code", this.Code.ToString());
            writer.WriteAttributeString("required", this.Required.ToYN());
            writer.WriteAttributeString("defaulted", this.Defaulted.ToYN());
            writer.WriteAttributeString("options_enforced", this.OptionsEnforced.ToYN());
            writer.WriteAttributeString("name", this.Name);
            writer.WriteAttributeString("answered", this.Answered.ToYN());

            writer.WriteStartElement("param_value");
            this.ParameterValue.WriteXml(writer);
            writer.WriteEndElement();
        }
    }

    internal sealed class ParameterReference : XmlSerializableCommon.AbstractXmlSerializable
    {
        public string Key { get; set; }
        public string XPath { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            reader.MoveToContent();
            this.Key = reader.GetAttribute("param_key");
            this.XPath = reader.GetAttribute("param_path");
            bool isEmptyElement = reader.IsEmptyElement;
            reader.ReadStartElement();
            if (isEmptyElement)
            {
                return;
            }

            reader.ReadEndElement();
        }

        /// <summary>
        /// We will not write these out.
        /// </summary>
        /// <param name="writer"></param>
        public override void WriteXml(XmlWriter writer)
        {
            throw new NotImplementedException();
        }
    }

    internal sealed class CCException : XmlSerializableCommon.AbstractXmlSerializable
    {
        public string Code { get; set; }
        public string Message { get; set; }

        public override void ReadXml(System.Xml.XmlReader reader)
        {
            reader.MoveToContent();
            this.Code = reader.GetAttribute("code");
            reader.ReadStartElement("exception"); // consume <exception>
            reader.ReadStartElement("message"); // consume <message>
            this.Message = reader.ReadString();
            reader.ReadEndElement(); // consume </message>
            reader.ReadEndElement(); // consume </exception>
        }

        /// <summary>
        /// Does not work on this class as exceptions should not be written,
        /// </summary>
        /// <param name="writer"></param>
        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            throw new NotImplementedException();
        }
    }

    internal sealed class TitleProduct : XmlSerializableCommon.AbstractXmlSerializable
    {
        public int PolicyId { get; set; }
        public int RateTypeId { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string PolicyName { get; set; }
        public decimal LiabilityAmount { get; set; }
        public int PolicyCategoryId { get; set; }
        public bool NoSimuDiscount { get; set; }

        // todo what about valid rate types?
        public List<ParameterReference> ParameterReferences { get; private set; }
        public List<CCException> Exceptions { get; private set; }

        public TitleProduct()
        {
            this.ParameterReferences = new List<ParameterReference>();
            this.Exceptions = new List<CCException>();
        }

        public override void ReadXml(XmlReader reader)
        {
            this.ParameterReferences.Clear();
            this.Exceptions.Clear();

            reader.MoveToContent();
            this.PolicyId = int.Parse(reader.GetAttribute("policy_id"));
            this.RateTypeId = int.Parse(reader.GetAttribute("ratetype_id"));
            ////EffectiveDDate = DateTime.Parse("effective_dt"); //may ne
            this.PolicyName = reader.GetAttribute("policy_name");
            this.LiabilityAmount = decimal.Parse(reader.GetAttribute("liability_amount"));
            var catid = reader.GetAttribute("policycategory_id");
            if (!string.IsNullOrEmpty(catid))
            {
                this.PolicyCategoryId = int.Parse(catid);
            }

            this.NoSimuDiscount = reader.GetAttribute("NoSimuDiscount") == "Y";
            bool isEmpty = reader.IsEmptyElement;
            reader.ReadStartElement();

            if (isEmpty)
            {
                return;
            }

            while (reader.NodeType == XmlNodeType.Element)
            {
                switch (reader.Name)
                {
                    case "param_refs":
                        this.ParameterReferences = reader.ReadAsList<ParameterReference>("param_ref");
                        break;
                    case "exceptions":
                        this.Exceptions = reader.ReadAsList<CCException>("exception");
                        break;
                    case "underwriter_id":
                        reader.ReadElementSafe();
                        break;
                    default:
                        throw new NotImplementedException(reader.Name);
                }
            }

            reader.ReadEndElement();
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("policy_id", this.PolicyId.ToString());
            writer.WriteAttributeString("ratetype_id", this.RateTypeId.ToString());
            writer.WriteAttributeString("policy_name", this.PolicyName);
            writer.WriteAttributeString("liability_amount", Math.Round(this.LiabilityAmount, 2).ToString());
            writer.WriteAttributeString("policycategory_id", this.PolicyCategoryId.ToString());
            writer.WriteAttributeString("NoSimuDiscount", this.NoSimuDiscount.ToYN());
            writer.WriteAttributeString("effective_dt", "0001-01-01T00:00:00");

            writer.WriteStartElement("param_refs");
            writer.WriteEndElement();

            writer.WriteElementString("underwriter_id", "1");
        }
    }

    internal sealed class RecordingDocument : XmlSerializableCommon.AbstractXmlSerializable
    {
        public E_DocumentType DocumentType { get; set; }
        public int PageCount { get; set; }
        public decimal ConsiderationAmount { get; set; }
        public List<ParameterReference> ParameterReferences { get; set; }
        public List<CCException> Exceptions { get; set; }

        public RecordingDocument()
        {
            this.ParameterReferences = new List<ParameterReference>();
            this.Exceptions = new List<CCException>();
        }

        public override void ReadXml(System.Xml.XmlReader reader)
        {
            this.ParameterReferences.Clear();
            this.Exceptions.Clear();

            reader.MoveToContent();
            this.DocumentType = (E_DocumentType)Enum.Parse(typeof(E_DocumentType), reader.GetAttribute("document_type"));
            this.PageCount = int.Parse(reader.GetAttribute("page_count"));
            this.ConsiderationAmount = decimal.Parse(reader.GetAttribute("consideration_amount"));
            bool isEmptyElement = reader.IsEmptyElement;

            if (isEmptyElement)
            {
                return;
            }

            reader.ReadStartElement(); // consume <recording_document>

            while (reader.NodeType == XmlNodeType.Element)
            {
                switch (reader.Name)
                {
                    case "param_refs":
                        this.ParameterReferences = reader.ReadAsList<ParameterReference>("param_ref");
                        break;
                    case "exceptions":
                        this.Exceptions = reader.ReadAsList<CCException>("exception");
                        break;
                    case "recording_documents":
                        reader.ReadAsList<RecordingDocument>("recoding_document");
                        break;
                    default:
                        throw new NotImplementedException(reader.Name);
                }
            }

            reader.ReadEndElement();
        }

        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteAttributeString("document_type", this.DocumentType.ToString());
            writer.WriteAttributeString("page_count", this.PageCount.ToString());
            writer.WriteAttributeString("consideration_amount", this.ConsiderationAmount.ToString());

            // don't need to send param refs not exceptions
        }
    }

    internal sealed class ClosingFee : XmlSerializableCommon.AbstractXmlSerializable
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsStatic { get; set; }
        public int Quantity { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            reader.MoveToContent();
            this.Id = int.Parse(reader.GetAttribute("id"));
            this.Name = reader.GetAttribute("name");
            this.IsStatic = reader.GetAttribute("static") == "true";
            this.Quantity = int.Parse(reader.GetAttribute("quantity"));
            reader.Skip();
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("id", this.Id.ToString());
            writer.WriteAttributeString("name", this.Name);
            writer.WriteAttributeString("static", this.IsStatic.ToTrueFalse());
            writer.WriteAttributeString("quantity", this.Quantity.ToString());
        }
    }

    internal sealed class ClosingCosts : XmlSerializableCommon.AbstractXmlSerializable
    {
        public int ClosingTypeId { get; set; }
        public string ClosingTypeName { get; set; }
        public string Notes { get; set; }
        public List<ParameterReference> ParameterReferences { get; private set; }
        public List<CCException> Exceptions { get; private set; }
        public List<ClosingFee> ClosingFees { get; private set; }

        public ClosingCosts()
        {
            this.ParameterReferences = new List<ParameterReference>();
            this.Exceptions = new List<CCException>();
            this.ClosingFees = new List<ClosingFee>();
        }

        public override void ReadXml(XmlReader reader)
        {
            reader.MoveToContent();
            this.ClosingTypeId = int.Parse(reader.GetAttribute("closing_type_id"));
            this.ClosingTypeName = reader.GetAttribute("closing_type_name");
            bool isEmptyElement = reader.IsEmptyElement;
            reader.ReadStartElement();

            if (isEmptyElement)
            {
                return;
            }

            while (reader.NodeType == XmlNodeType.Element)
            {
                switch (reader.Name)
                {
                    case "notes":
                        bool hasNotes = !reader.IsEmptyElement;
                        reader.ReadStartElement();
                        if (hasNotes)
                        {
                            this.Notes = reader.ReadString();
                            reader.ReadEndElement();
                        }

                        break;
                    case "param_refs":
                        this.ParameterReferences = reader.ReadAsList<ParameterReference>("param_ref");
                        break;
                    case "exceptions":
                        this.Exceptions = reader.ReadAsList<CCException>("exception");
                        break;
                    case "closing_fees":
                        this.ClosingFees = reader.ReadAsList<ClosingFee>("closing_fee");
                        break;
                    default:
                        throw new NotImplementedException(reader.Name);
                }
            }

            reader.ReadEndElement();
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("closing_type_id", this.ClosingTypeId.ToString());
            writer.WriteAttributeString("closing_type_name", this.ClosingTypeName);

            writer.WriteStartElement("notes");
            if (!string.IsNullOrEmpty(this.Notes))
            {
                writer.WriteCData(this.Notes);
            }

            writer.WriteEndElement();

            writer.WriteStartElement("param_refs");
            writer.WriteEndElement();

            writer.WriteStartElement("exceptions");
            writer.WriteEndElement();

            writer.WriteList("closing_fees", "closing_fee", this.ClosingFees);
        }
    }

    internal sealed class FAAddress : XmlSerializableCommon.AbstractXmlSerializable
    {
        public string AddressType { get; set; }
        public string Address1 { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string State { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            reader.MoveToContent();
            this.AddressType = reader.GetAttribute("type");
            this.Address1 = reader.GetAttribute("address1");
            this.City = reader.GetAttribute("city");
            this.Zip = reader.GetAttribute("zip");
            this.State = reader.GetAttribute("state");
            reader.Skip();
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("type", this.AddressType);
            writer.WriteAttributeString("address1", this.Address1);
            writer.WriteAttributeString("city", this.City);
            writer.WriteAttributeString("zip", this.Zip);
            writer.WriteAttributeString("state", this.State);
        }
    }

    internal sealed class RelatedOffice : XmlSerializableCommon.AbstractXmlSerializable
    {
        public string Type { get; set; }
        public List<FAAddress> Addresses { get; private set; }
        public string Attention { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Website { get; set; }
        public string BusinessHours { get; set; }

        public RelatedOffice()
        {
            this.Addresses = new List<FAAddress>();
        }

        public override void ReadXml(XmlReader reader)
        {
            reader.MoveToContent();
            this.Type = reader.GetAttribute("type");
            bool isEmpty = reader.IsEmptyElement;
            reader.ReadStartElement();
            if (isEmpty)
            {
                return;
            }

            while (reader.NodeType == XmlNodeType.Element)
            {
                switch (reader.Name)
                {
                    case "address":
                        if (reader.IsEmptyElement)
                        {
                            FAAddress addr = new FAAddress();
                            addr.ReadXml(reader);
                            this.Addresses.Add(addr);
                        }
                        else
                        {
                            this.Addresses = reader.ReadAsList<FAAddress>("address");
                        }

                        break;
                    case "attention":
                        this.Attention = reader.ReadElementSafe();
                        break;
                    case "phone":
                        this.Phone = reader.ReadElementSafe();
                        break;
                    case "fax":
                        this.Fax = reader.ReadElementSafe();
                        break;
                    case "website":
                        this.Website = reader.ReadElementSafe();
                        break;
                    case "business_hours":
                        this.BusinessHours = reader.ReadElementSafe();
                        break;
                    case "office_attribs":
                    case "special_instruct":
                    case "forms":
                        reader.Skip(); // don't care about these
                        break;
                    default:
                        Tools.LogError(new NotImplementedException(reader.Name));
                        reader.Skip();
                        break;
                }
            }

            reader.ReadEndElement();
        }

        public override void WriteXml(XmlWriter writer)
        {
            throw new NotImplementedException();
        }
    }

    internal sealed class Note : XmlSerializableCommon.AbstractXmlSerializable
    {
        public string Value { get; set; }
        public string NoteType { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            reader.MoveToContent();
            this.Value = reader.GetAttribute("value");
            bool isEmptyElement = reader.IsEmptyElement;
            reader.ReadStartElement();

            if (isEmptyElement)
            {
                return;
            }

            while (reader.NodeType == XmlNodeType.Element)
            {
                switch (reader.Name)
                {
                    case "note_type":
                        this.NoteType = reader.ReadElementSafe();
                        break;
                    default:
                        throw new NotImplementedException(reader.Name);
                }
            }

            reader.ReadEndElement();
        }

        public override void WriteXml(XmlWriter writer)
        {
            throw new NotImplementedException();
        }
    }

    internal sealed class GFESection : XmlSerializableCommon.AbstractXmlSerializable
    {
        public string SectionNumber { get; set; }
        public string SectionName { get; set; }
        public string Name { get; set; }
        public string HudLine { get; set; }
        public string SequenceNumber { get; set; }
        public decimal? Amount { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            reader.ReadStartElement();
            if (isEmpty)
            {
                return;
            }

            while (reader.NodeType == XmlNodeType.Element)
            {
                switch (reader.Name)
                {
                    case "section_number":
                        this.SectionNumber = reader.ReadElementSafe();
                        break;
                    case "section_name":
                        this.SectionName = reader.ReadElementSafe();
                        break;
                    case "name":
                        this.Name = reader.ReadElementSafe();
                        break;
                    case "HUD_line":
                        this.HudLine = reader.ReadElementSafe();
                        break;
                    case "sequence_number":
                        this.SequenceNumber = reader.ReadElementSafe();
                        break;
                    case "amount":
                        this.Amount = reader.ReadElementAsDecimalSafe();
                        break;
                    case "notes":
                        reader.Skip();
                        break;
                    default:
                        Tools.LogError(string.Concat(reader.Name, " is unhandled!! Ignoring"));
                        reader.Skip();
                        break;
                }
            }

            reader.ReadEndElement();
        }

        public override void WriteXml(XmlWriter writer)
        {
            throw new NotImplementedException();
        }
    }

    internal sealed class Cost : XmlSerializableCommon.AbstractXmlSerializable
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public string GFEName { get; set; }
        public E_CostType CostType { get; set; }
        public E_CostSubType CostSubType { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountBuyer { get; set; }
        public decimal AmountSeller { get; set; }
        public string PayableTo { get; set; }
        public List<Note> Notes { get; private set; }
        public decimal GFEAmount { get; set; }
        public decimal GFEAmountBuyer { get; set; }
        public decimal GFEAmountSeller { get; set; }
        public int? PolicyEndoRel { get; set; }

        public decimal FallbackAmountBuyer
        {
            get
            {
                if (this.HasGfeAmount)
                {
                    return this.GfeSection.Amount.Value;
                }

                return this.AmountBuyer;
            }
        }

        public decimal FallbackAmount
        {
            get
            {
                if (this.HasGfeAmount)
                {
                    return this.GfeSection.Amount.Value;
                }

                return this.Amount;
            }
        }

        public GFESection GfeSection { get; set; }
        public bool HasGfeAmount
        {
            get
            {
                return this.GfeSection != null && this.GfeSection.Amount.HasValue;
            }
        }

        /// <summary>
        /// Warning: Do not know what the xml for this node looks like
        /// </summary>
        public List<string> Assumptions { get; private set; }
        public List<string> CostAttribs { get; private set; }

        public Cost()
        {
            this.Notes = new List<Note>();
            this.Assumptions = new List<string>();
            this.CostAttribs = new List<string>();
            this.PolicyEndoRel = new int?();
            this.GfeSection = new GFESection();
        }

        public override void ReadXml(XmlReader reader)
        {
            this.Notes.Clear();
            this.Assumptions.Clear();
            this.CostAttribs.Clear();
            reader.MoveToContent();

            bool isEmpty = reader.IsEmptyElement;
            reader.ReadStartElement();
            if (isEmpty)
            {
                return;
            }

            while (reader.NodeType == XmlNodeType.Element)
            {
                switch (reader.Name)
                {
                    case "policy_endo_rel":
                        this.PolicyEndoRel = reader.ReadElementContentAsInt();
                        break;
                    case "name":
                        this.Name = reader.ReadElementSafe();
                        break;
                    case "GFEname":
                        this.GFEName = reader.ReadElementSafe();
                        break;
                    case "key":
                        this.Key = reader.ReadElementSafe();
                        break;
                    case "cost_type":
                        this.CostType = reader.ReadAsEnum<E_CostType>();
                        break;
                    case "cost_sub_type":
                        this.CostSubType = reader.ReadAsEnum<E_CostSubType>();
                        break;
                    case "notes":
                        reader.ReadAsList<Note>("note");
                        break;
                    case "payable_to":
                        this.PayableTo = reader.ReadElementSafe();
                        break;
                    case "amount":
                        this.Amount = reader.ReadElementAsDecimalSafe();
                        break;
                    case "amount_buyer":
                        this.AmountBuyer = reader.ReadElementAsDecimalSafe();
                        break;
                    case "amount_seller":
                        this.AmountSeller = reader.ReadElementAsDecimalSafe();
                        break;
                    case "GFEamount":
                        this.GFEAmount = reader.ReadElementAsDecimalSafe();
                        break;
                    case "GFEamount_buyer":
                        this.GFEAmountBuyer = reader.ReadElementAsDecimalSafe();
                        break;
                    case "GFEamount_seller":
                        this.GFEAmountSeller = reader.ReadElementAsDecimalSafe();
                        break;
                    case "GFE_Section":
                        this.GfeSection = new GFESection();
                        this.GfeSection.ReadXml(reader);
                        break;
                    case "questionlist":
                    case "assumptions":
                    case "cost_attribs":
                    case "GFE_note":
                    case "GFE_notes":
                    case "ClosingType_IncludedFees":
                        reader.Skip();
                        break;
                    default:
                        throw new NotImplementedException(reader.Name);
                }
            }

            reader.ReadEndElement();
        }

        /// <summary>
        /// not implemented
        /// </summary>
        /// <param name="writer"></param>
        public override void WriteXml(XmlWriter writer)
        {
            throw new NotImplementedException();
        }
    }

    internal sealed class Endorsement : XmlSerializableCommon.AbstractXmlSerializable
    {
        public int EndorsementId { get; set; }
        public string EndorsementName { get; set; }
        public int TitleProductOrdinal { get; set; }
        public decimal LiabilityAmount { get; set; }

        public override void ReadXml(XmlReader reader)
        {
            reader.MoveToContent();
            bool isEmpty = reader.IsEmptyElement;
            this.EndorsementId = int.Parse(reader.GetAttribute("endorsement_id"));
            this.EndorsementName = reader.GetSafeAttribute("endorsement_name");
            this.TitleProductOrdinal = int.Parse(reader.GetSafeAttribute("title_product_ordinal"));
            this.LiabilityAmount = decimal.Parse(reader.GetSafeAttribute("liability_amount"));
            reader.Skip(); // skip all children node-- we dont care about them.
        }

        public override void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("endorsement_id", this.EndorsementId.ToString());
            writer.WriteAttributeString("endorsement_name", this.EndorsementName);
            writer.WriteAttributeString("title_product_ordinal", this.TitleProductOrdinal.ToString());
            writer.WriteAttributeString("liability_amount", this.LiabilityAmount.ToString());
            writer.WriteElementString("underwriter_id", "1");
        }
    }

    internal sealed class CCRequest : XmlSerializableCommon.AbstractXmlSerializable
    {
        /// <summary>
        /// Required - Identifies who the calculation is for, this is needed
        /// to pull in the various business rules that may be associated with 
        /// that client profile
        /// </summary>
        public int ClientId { get; set; }

        /// <summary>
        /// Required - A timestamp of when the caller made the request, 
        /// this should be populated by the caller. Should be in the ISO 
        /// 8601 format: YYYY-MM-DD[T]hh:mm:ss[+|-]NNNN e.g. 2006-01-01T12:00:00+0700
        /// </summary>
        public DateTime TransactionDateTime { get; set; }

        public string CalcMethod { get; set; }

        /// <summary>
        /// This should not be set by the caller. This attribute is only
        /// used when calling the getAllParams method. If all Level II parameters 
        /// are gathered successfully this should be a Y, if not it will be a N.
        /// If you do get an “N” you should check the embedded exceptions collections 
        /// for the reasons why this may have failed.
        /// </summary>
        public bool Success { get; private set; }

        /// <summary>
        /// Makes the runCalculation method to run in Simulation mode. 
        /// The method will retun extra calculation notes in the notes section of each cost.
        /// </summary>
        public bool SimulatorMode { get; set; }

        /// <summary>
        /// Makes the getAllParams method return rates when there are no
        /// additional questions to be answered.  Extra hit to the runCalculation 
        /// method can be avoided when there are no additional questions.
        /// </summary>
        public bool GetCost { get; set; }

        public List<CCException> Exceptions { get; set; }
        public List<Parameter> Parameters { get; set; }

        public List<RecordingDocument> RecordingDocuments { get; private set; }

        public List<Endorsement> Endorsements { get; private set; }

        public List<TitleProduct> TitleProducts { get; private set; }

        public List<Note> Notes { get; set; }

        public ClosingCosts ClosingCosts { get; set; }

        public bool HasRecordingDocError { get; set; }

        public CCRequest()
        {
            this.RecordingDocuments = new List<RecordingDocument>();
            this.Parameters = new List<Parameter>();
            this.TitleProducts = new List<TitleProduct>();
            this.Endorsements = new List<Endorsement>();
            this.Exceptions = new List<CCException>();
            this.Notes = new List<Note>();
        }

        public override void ReadXml(System.Xml.XmlReader reader)
        {
            this.RecordingDocuments.Clear();
            this.TitleProducts.Clear();
            this.Parameters.Clear();
            this.Endorsements.Clear();
            this.Exceptions.Clear();
            this.Notes.Clear();

            reader.MoveToContent();
            this.ClientId = int.Parse(reader.GetAttribute("client_id"));
            this.Success = reader.GetAttribute("success") == "Y"; // todo what happens on error

            if (reader.IsEmptyElement)
            {
                return; // nothing left to do should not happen
            }

            this.Parameters = new List<Parameter>();
            reader.MoveToContent();
            reader.ReadStartElement(); // consume ccrequest

            while (reader.NodeType == System.Xml.XmlNodeType.Element)
            {
                switch (reader.Name)
                {
                    case "params":
                        this.Parameters = reader.ReadAsList<Parameter>("param");
                        break;
                    case "TransactionDate":
                        reader.ReadStartElement();
                        this.TransactionDateTime = DateTime.ParseExact(reader.ReadString(), "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture);
                        reader.ReadEndElement();
                        break;
                    case "recording_documents":
                        this.RecordingDocuments = reader.ReadAsList<RecordingDocument>("recording_document");
                        break;
                    case "echo_values":
                        Tools.Assert(reader.IsEmptyElement, "echo values is not empty");
                        reader.ReadStartElement();
                        break;
                    case "title_products":
                        this.TitleProducts = reader.ReadAsList<TitleProduct>("title_product");
                        break;
                    case "endorsements": // not implemented
                        this.Endorsements = reader.ReadAsList<Endorsement>("endorsement");
                        break;
                    case "closing_costs":
                        this.ClosingCosts = new ClosingCosts();
                        this.ClosingCosts.ReadXml(reader);
                        break;
                    case "cpls": // not implemented
                        Tools.Assert(reader.IsEmptyElement, "cpls is not empty");
                        reader.ReadStartElement();
                        break;
                    case "exceptions":
                        this.Exceptions = reader.ReadAsList<CCException>("exception");
                        break;
                    case "notes":
                        this.Notes = reader.ReadAsList<Note>("note");
                        break;
                    default:
                        throw new NotImplementedException(reader.Name);
                }
            }
        }

        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("CCRequest");
            writer.WriteAttributeString("client_id", this.ClientId.ToString());
            writer.WriteAttributeString("simulatormode", this.SimulatorMode.ToTrueFalse());
            writer.WriteAttributeString("transaction_dt", DateTime.Now.ToString("MM/d/yyyy"));
            writer.WriteAttributeString("calc_method", this.CalcMethod);
            writer.WriteAttributeString("getcost", this.GetCost.ToTrueFalse());
            writer.WriteAttributeString("IsGFERequired", "true");
            writer.WriteAttributeString("disableRFC", "false");
            writer.WriteAttributeString("calc_option_policy_selected", "true");
            writer.WriteAttributeString("calc_option_closingcost_selected", "true");
            writer.WriteAttributeString("calc_option_endo_selected", "true");
            writer.WriteAttributeString("calc_option_recordingdoc_selected", "true");
            
            // http://stackoverflow.com/questions/114983/in-c-given-a-datetime-object-how-do-i-get-a-iso-8601-date-in-string-format
            // xml has calc_method
            writer.WriteElementString("TransactionDate", "0001-01-01T00:00:00");
            writer.Flush();

            writer.WriteList("params", "param", this.Parameters);

            writer.WriteStartElement("echo_values");
            writer.WriteEndElement();

            writer.WriteList("recording_documents", "recording_document", this.RecordingDocuments);
            writer.WriteList("title_products", "title_product", this.TitleProducts);
            writer.WriteList("endorsements", "endorsement", this.Endorsements);

            if (ClosingCosts != null)
            {
                writer.WriteStartElement("closing_costs");
                this.ClosingCosts.WriteXml(writer);
                writer.WriteEndElement();
            }

            writer.WriteEndElement(); // CCRequest
        }
    }

    internal sealed class CCResponse : XmlSerializableCommon.AbstractXmlSerializable
    {
        public string RequestId { get; set; }
        public bool Success { get; set; }
        public bool SuccessTitle { get; set; }
        public bool SuccessLocal { get; set; }
        public bool SuccessClosing { get; set; }

        public decimal Total { get; set; }
        public decimal TotalTitle { get; set; }
        public decimal TotalLocal { get; set; }
        public decimal TotalClosing { get; set; }
        public decimal TotalCPL { get; set; }
        public decimal TotalSalesTax { get; set; }

        public List<Cost> Costs { get; set; }
        public List<Note> Notes { get; set; }
        public List<RelatedOffice> RelatedOffices { get; set; }

        public CCResponse()
        {
            this.Costs = new List<Cost>();
            this.Notes = new List<Note>();
            this.RelatedOffices = new List<RelatedOffice>();
        }

        public override void ReadXml(XmlReader reader)
        {
            reader.MoveToContent();
            this.RequestId = reader.GetAttribute("request_id");
            this.Success = reader.GetAttribute("success").Equals("y", StringComparison.OrdinalIgnoreCase);
            this.SuccessTitle = reader.GetAttribute("success_title").Equals("y", StringComparison.OrdinalIgnoreCase);
            this.SuccessLocal = reader.GetAttribute("success_local").Equals("y", StringComparison.OrdinalIgnoreCase);
            this.SuccessClosing = reader.GetAttribute("success_closing").Equals("y", StringComparison.OrdinalIgnoreCase);

            this.Total = decimal.Parse(reader.GetAttribute("total"));
            this.TotalCPL = decimal.Parse(reader.GetAttribute("total_cpl"));
            this.TotalSalesTax = decimal.Parse(reader.GetAttribute("total_salestax"));
            this.TotalLocal = decimal.Parse(reader.GetAttribute("total_local"));
            this.TotalClosing = decimal.Parse(reader.GetAttribute("total_closing"));

            bool isEmpty = reader.IsEmptyElement;
            reader.ReadStartElement(); // consume ccrequest
            if (isEmpty)
            {
                return;
            }

            while (reader.NodeType == System.Xml.XmlNodeType.Element)
            {
                switch (reader.Name)
                {
                    case "costs":
                        this.Costs = reader.ReadAsList<Cost>("cost");
                        break;
                    case "notes":
                        this.Notes = reader.ReadAsList<Note>("note");
                        break;
                    case "related_office":
                        if (!reader.IsEmptyElement)
                        {
                            this.RelatedOffices = reader.ReadAsList<RelatedOffice>("related_office");
                        }
                        else
                        {
                            reader.Skip();
                        }

                        break;
                    case "gfe_summary_item":
                    case "gfe_summary":
                    case "exceptions":
                    case "echo_values":
                        reader.Skip();
                        break;
                    default:
                        throw new NotImplementedException(reader.Name);
                }
            }

            reader.ReadEndElement();
        }

        public override void WriteXml(XmlWriter writer)
        {
            throw new NotImplementedException();
        }
    }
}
