﻿namespace LendersOffice.ObjLib.TitleProvider.TitlePolicy
{
    using System;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.Conversions.Mismo3.Version2;
    using LendersOffice.Conversions.Mismo3.Version2.Request;
    using LendersOffice.ObjLib.TitleProvider.TitlePolicy.Request;
    using LendersOffice.Security;

    /// <summary>
    /// Creates and populates a Title request.
    /// </summary>
    public class TitleRequestProvider
    {
        protected enum E_PartyType
        {
            Borrower,
            Lender,
            LQB,
            RespondTo,
            TitleProvider,
        }

        protected enum E_BorrowerResidenceType
        {
            Present,
            Mailing,
        }

        #region Variables
        private TitleRequest m_request; // The internal representation of our data
        private E_TitleRequestType m_eRequestType;
        private CPageData m_dataLoan;
        private CAppData m_dataApp;
        private CAppData[] m_dataApps;
        private BrokerDB m_BrokerDB;
        private EmployeeDB m_EmployeeDB;
        private string m_sUserName;
        private string m_sPassword;
        private string m_sVendorAccountID = "";
        private string m_sQuoteID;
        private E_cTitleInsurancePolicyT m_eTitlePolicyType;
        private string m_sOwnerPolicyName;
        private string m_sLenderPolicyName;
        private string m_sVendorLegalName;
        private bool m_bEscrowRequested;
        private int m_iBorrowerSequenceNumber;
        private Guid m_sTransactionID;
        private bool m_bSigningRequested;

        /// <summary>
        /// An instance of the StateInfo object with US states, counties, and FIPS codes.
        /// </summary>
        private StateInfo statesAndTerritories = StateInfo.Instance;

        public string TransactionID
        {
            get
            {
                return m_sTransactionID.ToString();
            }
        }

        private Guid BrokerId
        {
            get
            {
                return PrincipalFactory.CurrentPrincipal.BrokerId;
            }
        }

        private Guid EmployeeId
        {
            get
            {
                return PrincipalFactory.CurrentPrincipal.EmployeeId;
            }
        }

        private string PolicyName
        {
            get
            {
                return (m_eTitlePolicyType == E_cTitleInsurancePolicyT.Lender) ? m_sLenderPolicyName : m_sOwnerPolicyName;
            }
        }

        private string VendorAccountID
        {
            get
            {
                if (string.IsNullOrEmpty(m_sVendorAccountID))
                {
                    return m_BrokerDB.CustomerCode;
                }
                else if (string.Compare("0", m_sVendorAccountID) == 0)
                {
                    return m_BrokerDB.CustomerCode;
                }

                return m_sVendorAccountID;
            }
        }
        #endregion

        public TitleRequestProvider(string sUserName, string sPassword, string sVendorLegalName, string sVendorAccountID, Guid sLId)
        {
            m_dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(TitleRequestProvider));
            m_dataLoan.InitLoad();
            m_dataLoan.SetFormatTarget(FormatTarget.MismoClosing);
            m_dataLoan.ByPassFieldSecurityCheck = true;

            m_dataApps = new CAppData[m_dataLoan.nApps];
            for (int i = 0; i < m_dataLoan.nApps; i++)
            {
                m_dataApps[i] = m_dataLoan.GetAppData(i);
                m_dataApps[i].BorrowerModeT = E_BorrowerModeT.Borrower;
            }
            if (m_dataLoan.nApps > 0)
            {
                m_dataApp = m_dataLoan.GetAppData(0);
            }
            m_dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
            m_iBorrowerSequenceNumber = 0;

            m_sUserName = sUserName;
            m_sPassword = sPassword;
            m_sVendorAccountID = sVendorAccountID;
            m_sVendorLegalName = sVendorLegalName;

            m_BrokerDB = BrokerDB.RetrieveById(BrokerId);
            m_EmployeeDB = new EmployeeDB(EmployeeId, BrokerId);
            m_EmployeeDB.Retrieve();

            m_sTransactionID = Guid.NewGuid();
            m_eTitlePolicyType = E_cTitleInsurancePolicyT.Owner;
        }

        public TitleRequest CreateTitlePolicyRequest(string sQuoteID, string sOwnerPolicyName, string sLenderPolicyName, bool bEscrowRequested, bool bSigningRequested)
        {
            m_eRequestType = E_TitleRequestType.OrderPolicy;
            m_sQuoteID = sQuoteID;
            m_sOwnerPolicyName = sOwnerPolicyName;
            m_sLenderPolicyName = sLenderPolicyName;
            m_bEscrowRequested = bEscrowRequested;
            m_bSigningRequested = bSigningRequested;
            
            m_request = CreateTitleRequest();

            return m_request;
        }

        private TitleRequest CreateTitleRequest()
        {
            TitleRequest titleRequest = new TitleRequest();
            
            switch(m_eRequestType)
            {
                case E_TitleRequestType.OrderPolicy:
                    titleRequest.Request = CreatePolicyRequest();
                    break;
                case E_TitleRequestType.GetQuote:
                case E_TitleRequestType.LenderDocSubmission:
                case E_TitleRequestType.None:
                default:
                    throw new UnhandledEnumException(m_eRequestType);
            }

            titleRequest.RequestSource = CreateRequestSource();
            titleRequest.Authentication = CreateAuthentication();
            titleRequest.MESSAGE = CreateMISMO3_2Message();

            return titleRequest;
        }

        #region Request Source
        private RequestSource CreateRequestSource()
        {
            RequestSource requestSource = new RequestSource();
            requestSource.LoanID = CreateLoanID();
            requestSource.SourceName = CreateSourceName();
            requestSource.TransactionID = CreateTransactionID();

            return requestSource;
        }

        private LoanID CreateLoanID()
        {
            LoanID loanID = new LoanID();
            loanID.Value = m_dataLoan.sLId.ToString();

            return loanID;
        }

        private SourceName CreateSourceName()
        {
            SourceName sourceName = new SourceName();
            sourceName.Value = (ConstAppDavid.CurrentServerLocation == ServerLocation.Production) ? "LendingQBProduction" : "LendingQBSample";

            return sourceName;
        }

        private TransactionID CreateTransactionID()
        {
            TransactionID transactionID = new TransactionID();
            transactionID.Value = TransactionID;

            return transactionID;

        }
        #endregion

        #region Authentication
        private Authentication CreateAuthentication()
        {
            Authentication auth = new Authentication();
            auth.AccountID = CreateAccountID();
            auth.Password = CreatePassword();
            auth.UserName = CreateUserName();

            return auth;
        }

        private AccountID CreateAccountID()
        {
            AccountID accountID = new AccountID();
            accountID.Value = VendorAccountID;

            return accountID;
        }

        private Password CreatePassword()
        {
            Password password = new Password();
            password.Value = m_sPassword;

            return password;
        }

        private UserName CreateUserName()
        {
            UserName userName = new UserName();
            userName.Value = m_sUserName;

            return userName;
        }
        #endregion

        private Policy CreatePolicyRequest()
        {
            Policy policyRequest = new Policy();
            policyRequest.OrderPolicyRequest = CreateOrderPolicyRequest();
            policyRequest.PostbackUrl = ConstStage.FirstAmericanPostbackUrl;

            return policyRequest;
        }

        private OrderPolicyRequest CreateOrderPolicyRequest()
        {
            OrderPolicyRequest orderPolicyRequest = new OrderPolicyRequest();
            orderPolicyRequest.QuoteID = m_sQuoteID;

            return orderPolicyRequest;
        }

        #region MISMO 3.2
        private MESSAGE CreateMISMO3_2Message()
        {
            MESSAGE mismo3_2Message = new MESSAGE();
            mismo3_2Message.DEAL_SETS = CreateDealSets();

            return mismo3_2Message;
        }

        private DEAL_SETS CreateDealSets()
        {
            DEAL_SETS dealSets = new DEAL_SETS();
            dealSets.DEAL_SETList.Add(CreateDealSet());

            return dealSets;
        }

        private DEAL_SET CreateDealSet()
        {
            DEAL_SET dealSet = new DEAL_SET();
            dealSet.DEALS = CreateDeals();

            return dealSet;
        }

        private DEALS CreateDeals()
        {
            DEALS deals = new DEALS();
            // Batch orders (more than one loan/subject property) can be implemented by adding deals
            deals.DEALList.Add(CreateTitleDeal(1));

            return deals;
        }

        private DEAL CreateTitleDeal(int iDealNumber)
        {
            DEAL titleDeal = new DEAL();
            titleDeal.SequenceNumber = iDealNumber;
            titleDeal.COLLATERALS = CreateCollaterals();
            titleDeal.LIABILITIES = CreateLiabilities();
            titleDeal.LOANS = CreateLoans();
            titleDeal.PARTIES = CreateParties();
            titleDeal.SERVICES = CreateServices();
            
            return titleDeal;
        }

        #region Collaterals (subject property)
        private COLLATERALS CreateCollaterals()
        {
            COLLATERALS collaterals = new COLLATERALS();
            collaterals.COLLATERALList.Add(CreateCollateral());

            return collaterals;
        }

        private COLLATERAL CreateCollateral()
        {
            COLLATERAL collateral = new COLLATERAL();
            collateral.PROPERTY = CreateTitleSubjectProperty();

            return collateral;
        }

        private PROPERTY CreateTitleSubjectProperty()
        {
            PROPERTY titleSubjectProperty = new PROPERTY();
            titleSubjectProperty.ValuationUseType = E_ValuationUseBase.SubjectProperty;
            titleSubjectProperty.ADDRESS = CreateAddress();
            titleSubjectProperty.LEGAL_DESCRIPTIONS = CreateLegalDescriptions();
            titleSubjectProperty.PROPERTY_DETAIL = CreatePropertyDetail();
            titleSubjectProperty.PROPERTY_VALUATIONS = CreatePropertyValuations();

            return titleSubjectProperty;
        }

        #region Legal Descriptions
        private LEGAL_DESCRIPTIONS CreateLegalDescriptions()
        {
            LEGAL_DESCRIPTIONS descriptions = new LEGAL_DESCRIPTIONS();
            descriptions.LEGAL_DESCRIPTIONList.Add(CreateLegalDescription(1));

            return descriptions;
        }

        private LEGAL_DESCRIPTION CreateLegalDescription(int iLegalDescriptionNumber)
        {
            LEGAL_DESCRIPTION description = new LEGAL_DESCRIPTION();
            description.SequenceNumber = iLegalDescriptionNumber;
            description.UNPARSED_LEGAL_DESCRIPTIONS = CreateUnparsedLegalDescriptions();

            return description;
        }

        private UNPARSED_LEGAL_DESCRIPTIONS CreateUnparsedLegalDescriptions()
        {
            UNPARSED_LEGAL_DESCRIPTIONS unparsedLegalDescrs = new UNPARSED_LEGAL_DESCRIPTIONS();
            unparsedLegalDescrs.UNPARSED_LEGAL_DESCRIPTIONList.Add(CreateUnparsedLegalDescription(1));

            return unparsedLegalDescrs;
        }

        private UNPARSED_LEGAL_DESCRIPTION CreateUnparsedLegalDescription(int iUnparsedLegalDescriptionNumber)
        {
            UNPARSED_LEGAL_DESCRIPTION unparsedLegalDescr = new UNPARSED_LEGAL_DESCRIPTION();
            unparsedLegalDescr.SequenceNumber = iUnparsedLegalDescriptionNumber;
            unparsedLegalDescr.UnparsedLegalDescription = MISMO3_2Convert.ToMISMO3_2(m_dataLoan.sSpLegalDesc, false);

            return unparsedLegalDescr;
        }
        #endregion

        private PROPERTY_DETAIL CreatePropertyDetail()
        {
            PROPERTY_DETAIL propertyDetail = new PROPERTY_DETAIL();
            propertyDetail.AttachmentType = MISMO3_2Convert.ToMISMO3_2(m_dataLoan.sGseSpT, false);
            propertyDetail.FinancedUnitCount = MISMO3_2Convert.ToMISMO3_2MISMOCount(m_dataLoan.sUnitsNum_rep, false);
            propertyDetail.PropertyStructureBuiltYear = MISMO3_2Convert.ToMISMO3_2MISMOYear(m_dataLoan.sYrBuilt, false);
            propertyDetail.PropertyUsageType = MISMO3_2Convert.ToMISMO3_2(m_dataLoan.sOccT, false);
            propertyDetail.PropertyUsageTypeOtherDescription = MISMO3_2Convert.ToMISMO3_2("", false);
            propertyDetail.PUDIndicator = MISMO3_2Convert.ToMISMO3_2((m_dataLoan.sSpIsInPud || (m_dataLoan.sGseSpT == E_sGseSpT.PUD)), false);

            return propertyDetail;
        }

        #region Valuations
        private PROPERTY_VALUATIONS CreatePropertyValuations()
        {
            PROPERTY_VALUATIONS valuations = new PROPERTY_VALUATIONS();
            valuations.PROPERTY_VALUATIONList.Add(CreatePropertyValuation(1));

            return valuations;
        }

        private PROPERTY_VALUATION CreatePropertyValuation(int iValuationNumber)
        {
            PROPERTY_VALUATION valuation = new PROPERTY_VALUATION();
            valuation.SequenceNumber = iValuationNumber;
            valuation.PROPERTY_VALUATION_DETAIL = CreatePropertyValuationDetail();

            return valuation;
        }

        private PROPERTY_VALUATION_DETAIL CreatePropertyValuationDetail()
        {
            PROPERTY_VALUATION_DETAIL valuationDetail = new PROPERTY_VALUATION_DETAIL();
            valuationDetail.PropertyValuationAmount = MISMO3_2Convert.ToMISMO3_2MISMOAmount(m_dataLoan.sApprVal_rep, false);

            return valuationDetail;
        }
        #endregion
        #endregion

        #region Liabilities
        private LIABILITIES CreateLiabilities()
        {
            LIABILITIES liabilities = new LIABILITIES();
            foreach (CAppData dataApp in m_dataApps)
            {
                ILiaCollection liaList = dataApp.aLiaCollection;
                var liaCollection = liaList.GetSubcollection(true, E_DebtGroupT.ALL);
                int iLiabilityCount = 0;
                foreach (var item in liaCollection)
                {
                    var liability = (ILiability)item;
                    bool bWriteLiability = true;
                    if (CLiaFields.IsTypeOfSpecialRecord(liability.DebtT))
                    {
                        bWriteLiability = (liability.Pmt > 0);
                    }

                    if (bWriteLiability)
                    {
                        iLiabilityCount++;
                        if (liability.WillBePdOff)
                        {
                            liabilities.LIABILITYList.Add(CreateLiabilityToBePaidOff(liability, iLiabilityCount));
                        }
                        else
                        {
                            liabilities.LIABILITYList.Add(CreateLiability(liability, iLiabilityCount));
                        }
                    }
                }
            }

            return liabilities;
        }

        private LIABILITY CreateLiability(ILiability liabilityFields, int iLiabilityNumber)
        {
            LIABILITY liability = new LIABILITY();
            liability.SequenceNumber = iLiabilityNumber;
            liability.LIABILITY_DETAIL = CreateLiabilityDetail(liabilityFields);
            liability.LIABILITY_HOLDER = CreateLiabilityHolder(liabilityFields);

            return liability;
        }

        private LIABILITY CreateLiabilityToBePaidOff(ILiability liabilityFields, int iLiabilityNumber)
        {
            LIABILITY liability = new LIABILITY();
            liability.SequenceNumber = iLiabilityNumber;
            liability.LIABILITY_DETAIL = CreateLiabilityDetail(liabilityFields);
            liability.LIABILITY_HOLDER = CreateLiabilityHolder(liabilityFields);
            liability.PAYOFF = CreateLiabilityPayoff(liabilityFields);
            
            return liability;
        }

        private LIABILITY_DETAIL CreateLiabilityDetail(ILiability liabilityFields)
        {
            LIABILITY_DETAIL liabilityDetail = new LIABILITY_DETAIL();
            liabilityDetail.LiabilityExclusionIndicator = MISMO3_2Convert.ToMISMO3_2(liabilityFields.NotUsedInRatio, false);
            liabilityDetail.LiabilityMonthlyPaymentAmount = MISMO3_2Convert.ToMISMO3_2MISMOAmount(liabilityFields.Pmt_rep, false);
            liabilityDetail.LiabilityPayoffStatusIndicator = MISMO3_2Convert.ToMISMO3_2(liabilityFields.WillBePdOff, false);
            //liabilityDetail.LiabilityPayoffWithCurrentAssetsIndicator;
            liabilityDetail.LiabilityRemainingTermMonthsCount = MISMO3_2Convert.ToMISMO3_2MISMOCount(liabilityFields.RemainMons_rep, false);

            string sOtherType = "";

            if (CLiaFields.IsTypeOfSpecialRecord(liabilityFields.DebtT))
            {
                liabilityDetail.LiabilityType = MISMO3_2Convert.ToMISMO3_2(liabilityFields.DebtT, false);

                var specialLia = liabilityFields as ILiabilitySpecial;
                switch (specialLia.DebtT)
                {
                    case E_DebtSpecialT.Alimony:
                        var alimony = specialLia as ILiabilityAlimony;
                        sOtherType = "Alimony";
                        break;
                    case E_DebtSpecialT.JobRelatedExpense:
                        ILiabilityJobExpense jobExpense = specialLia as ILiabilityJobExpense;
                        sOtherType = "JobRelatedExpense";
                        break;
                    case E_DebtSpecialT.ChildSupport:
                        var childSupport = specialLia as ILiabilityChildSupport;
                        sOtherType = "ChildSupport";
                        break;
                    default:
                        throw new UnhandledEnumException(specialLia.DebtT);
                }
            }
            else
            {
                ILiabilityRegular regularLia = liabilityFields as ILiabilityRegular;
                liabilityDetail.LiabilityAccountIdentifier = MISMO3_2Convert.ToMISMO3_2Identifier(regularLia.AccNum.Value, true);
                liabilityDetail.LiabilityDescription = MISMO3_2Convert.ToMISMO3_2(regularLia.Desc, false);
                liabilityDetail.LiabilityUnpaidBalanceAmount = MISMO3_2Convert.ToMISMO3_2MISMOAmount(regularLia.Bal_rep, false);
                liabilityDetail.SubjectLoanResubordinationIndicator = MISMO3_2Convert.ToMISMO3_2(regularLia.IsPiggyBack, false);
                liabilityDetail.LiabilityType = MISMO3_2Convert.ToMISMO3_2(liabilityFields.DebtT, regularLia.IsSubjectProperty1stMortgage, regularLia.Desc, false);

                sOtherType = regularLia.Desc;
            }

            liabilityDetail.LiabilityTypeOtherDescription = MISMO3_2Convert.ToMISMO3_2(sOtherType, false);

            return liabilityDetail;
        }

        private LIABILITY_HOLDER CreateLiabilityHolder(ILiability liabilityFields)
        {
            LIABILITY_HOLDER holder = new LIABILITY_HOLDER();
            string sHolderName = string.Empty;

            if (CLiaFields.IsTypeOfSpecialRecord(liabilityFields.DebtT))
            {
                var specialLia = liabilityFields as ILiabilitySpecial;
                switch (specialLia.DebtT)
                {
                    case E_DebtSpecialT.Alimony:
                        var alimony = specialLia as ILiabilityAlimony;
                        sHolderName = alimony.OwedTo;
                        break;
                    case E_DebtSpecialT.JobRelatedExpense:
                        ILiabilityJobExpense jobExpense = specialLia as ILiabilityJobExpense;
                        sHolderName = jobExpense.ExpenseDesc;
                        break;
                    case E_DebtSpecialT.ChildSupport:
                        var childSupport = specialLia as ILiabilityChildSupport;
                        sHolderName = childSupport.OwedTo;
                        break;
                    default:
                        throw new UnhandledEnumException(specialLia.DebtT);
                }
            }
            else
            {
                ILiabilityRegular regularLia = liabilityFields as ILiabilityRegular;
                sHolderName = regularLia.ComNm;
            }

            holder.NAME = CreateName(sHolderName, true);

            return holder;
        }

        private PAYOFF CreateLiabilityPayoff(ILiability liabilityFields)
        {
            PAYOFF payoff = new PAYOFF();

            if (CLiaFields.IsTypeOfSpecialRecord(liabilityFields.DebtT))
            {
                payoff.PayoffActionType = MISMO3_2Convert.ToMISMO3_2(true, false, false, E_DebtRegularT.Other, m_bEscrowRequested);
            }
            else
            {
                ILiabilityRegular regularLia = liabilityFields as ILiabilityRegular;
                payoff.PayoffAccountNumberIdentifier = MISMO3_2Convert.ToMISMO3_2Identifier(regularLia.AccNum.Value, true);
                payoff.PayoffActionType = MISMO3_2Convert.ToMISMO3_2(true, regularLia.IsPiggyBack, false, regularLia.DebtT, m_bEscrowRequested);
                //payoff.PayoffActionTypeOtherDescription;    
                payoff.PayoffAmount = MISMO3_2Convert.ToMISMO3_2MISMOAmount(regularLia.Bal_rep, false);
            }

            return payoff;
        }
        #endregion

        #region Loans
        private LOANS CreateLoans()
        {
            LOANS loans = new LOANS();
            TitleRequestLoanProvider loanProvider = new TitleRequestLoanProvider(m_dataLoan.sLId);
            loans.LOANList.Add(loanProvider.CreateLoan(1));

            if (m_dataLoan.sLinkedLoanInfo.IsLoanLinked)
            {
                TitleRequestLoanProvider otherLienProvider = new TitleRequestLoanProvider(m_dataLoan.sLinkedLoanInfo.LinkedLId);
                loans.LOANList.Add(otherLienProvider.CreateLoan(2));
            }
            else if ((m_dataLoan.sConcurSubFin > 0) && (m_dataLoan.sIsOFinNew))
            {
                loans.LOANList.Add(loanProvider.CreateNonLinkedSecondLien(2));
            }

            return loans;
        }
        #endregion

        #region Parties
        private PARTIES CreateParties()
        {
            PARTIES parties = new PARTIES();
            parties.PARTYList.Add(CreateLenderParty(1));
            parties.PARTYList.Add(CreateLQBParty(2));
            parties.PARTYList.Add(CreateRespondToParty(3));
            parties.PARTYList.Add(CreateTitleProviderParty(4));
            foreach (CAppData dataApp in m_dataApps)
            {
                m_dataApp = dataApp;
                m_dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
                m_iBorrowerSequenceNumber++;
                parties.PARTYList.Add(CreateBorrowerParty(4 + m_iBorrowerSequenceNumber));

                if (m_dataApp.aCIsValidNameSsn)
                {
                    m_dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
                    m_iBorrowerSequenceNumber++;
                    parties.PARTYList.Add(CreateBorrowerParty(4 + m_iBorrowerSequenceNumber));
                }

                m_dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
            }
            m_dataApp = m_dataLoan.GetAppData(0); // reset in case it's used anywhere below the borrower blocks
            m_dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;

            foreach (TitleBorrower borrower in m_dataLoan.sTitleBorrowers)
            {
                m_iBorrowerSequenceNumber++;
                parties.PARTYList.Add(CreateTitleBorrowerParty(5 + m_iBorrowerSequenceNumber, borrower ));
            }

            return parties;
        }

        private PARTY CreateLenderParty(int iPartyNumber)
        {
            PARTY lenderParty = new PARTY();
            lenderParty.SequenceNumber = iPartyNumber;
            lenderParty.ADDRESSES = CreateAddresses(E_PartyType.Lender);
            lenderParty.LEGAL_ENTITY = CreateLegalEntity(E_PartyType.Lender);
            lenderParty.ROLES = CreateRoles(E_PartyType.Lender);

            return lenderParty;
        }

        private PARTY CreateLQBParty(int iPartyNumber)
        {
            PARTY LQBParty = new PARTY();
            LQBParty.SequenceNumber = iPartyNumber;
            LQBParty.ADDRESSES = CreateAddresses(E_PartyType.LQB);
            LQBParty.LEGAL_ENTITY = CreateLegalEntity(E_PartyType.LQB);
            LQBParty.ROLES = CreateRoles(E_PartyType.LQB);

            return LQBParty;
        }

        private PARTY CreateRespondToParty(int iPartyNumber)
        {
            PARTY rolesParty = new PARTY();
            rolesParty.SequenceNumber = iPartyNumber;
            rolesParty.ROLES = CreateRoles(E_PartyType.RespondTo);

            return rolesParty;
        }

        private PARTY CreateTitleProviderParty(int iPartyNumber)
        {
            PARTY titleProviderParty = new PARTY();
            titleProviderParty.SequenceNumber = iPartyNumber;
            titleProviderParty.LEGAL_ENTITY = CreateLegalEntity(E_PartyType.TitleProvider);
            titleProviderParty.ROLES = CreateRoles(E_PartyType.TitleProvider);

            return titleProviderParty;
        }

        #region Borrower Party
        private PARTY CreateBorrowerParty(int iPartyNumber)
        {
            PARTY borrowerParty = new PARTY();
            borrowerParty.SequenceNumber = iPartyNumber;
            borrowerParty.INDIVIDUAL = CreateIndividual();
            borrowerParty.ROLES = CreateRoles(E_PartyType.Borrower);
            borrowerParty.TAXPAYER_IDENTIFIERS = CreateTaxPayerIdentifiers();

            return borrowerParty;
        }

        private PARTY CreateTitleBorrowerParty(int iPartyNumber, TitleBorrower borrower)
        {
            PARTY borrowerParty = new PARTY();

            borrowerParty.SequenceNumber = iPartyNumber;

            borrowerParty.INDIVIDUAL = new INDIVIDUAL();
            borrowerParty.INDIVIDUAL.NAME.FirstName = MISMO3_2Convert.ToMISMO3_2(borrower.FirstNm, true);
            borrowerParty.INDIVIDUAL.NAME.MiddleName = MISMO3_2Convert.ToMISMO3_2(borrower.MidNm, true);
            borrowerParty.INDIVIDUAL.NAME.LastName = MISMO3_2Convert.ToMISMO3_2(borrower.LastNm, true);

            #region contact
            CONTACT_POINT contact = new CONTACT_POINT();
            contact.SequenceNumber = 1;
            contact.CONTACT_POINT_EMAIL = new CONTACT_POINT_EMAIL();
            contact.CONTACT_POINT_EMAIL.ContactPointEmailValue = MISMO3_2Convert.ToMISMO3_2MISMOValue(borrower.Email, false);
            #endregion

            borrowerParty.INDIVIDUAL.CONTACT_POINTS.CONTACT_POINTList.Add(contact);

            ROLE role = new ROLE();
            role.SequenceNumber = m_iBorrowerSequenceNumber;
            role.ROLE_DETAIL = new ROLE_DETAIL();
            role.ROLE_DETAIL.PartyRoleType = MISMO3_2Convert.ToMISMO3_2(MISMO3_2Convert.ToMISMO3_2PartyRoleType(E_aTypeT.TitleOnly), false);

            role.BORROWER = new BORROWER();
            role.BORROWER.BORROWER_DETAIL = new BORROWER_DETAIL();
            role.BORROWER.BORROWER_DETAIL.BorrowerClassificationType = MISMO3_2Convert.ToMISMO3_2BorrowerClassification(false, false);
            role.BORROWER.BORROWER_DETAIL.BorrowerMailToAddressSameAsPropertyIndicator = MISMO3_2Convert.ToMISMO3_2(false, false);
            role.BORROWER.BORROWER_DETAIL.BorrowerRelationshipTitleType = MISMO3_2Convert.ToMISMO3_2(borrower.RelationshipTitleT, false);
            role.BORROWER.BORROWER_DETAIL.MaritalStatusType = new MaritalStatusEnum() { eValue = E_MaritalStatusBase.NotProvided };

            RESIDENCE r = new RESIDENCE();
            r.ADDRESS = new ADDRESS();
            r.SequenceNumber = 1;
            r.ADDRESS.AddressLineText = MISMO3_2Convert.ToMISMO3_2(borrower.Address, false);
            r.ADDRESS.AddressType = MISMO3_2Convert.ToMISMO3_2(E_AddressBase.Current, false);
            r.ADDRESS.CityName = MISMO3_2Convert.ToMISMO3_2(borrower.City, false);
            r.ADDRESS.CountryCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(StateInfo.USACountryCode, false);
            r.ADDRESS.CountryName = MISMO3_2Convert.ToMISMO3_2(StateInfo.USACountryName, false);
            r.ADDRESS.PostalCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(m_dataApp.aZip, false);
            r.ADDRESS.StateName = MISMO3_2Convert.ToMISMO3_2(this.statesAndTerritories.GetStateName(m_dataApp.aState), false);

            role.BORROWER.RESIDENCES = new RESIDENCES();
            role.BORROWER.RESIDENCES.RESIDENCEList.Add(r);
            

            if (role.BORROWER.BORROWER_DETAIL.BorrowerRelationshipTitleType.eValue == E_BorrowerRelationshipTitleBase.Other)
            {
                if (m_dataApp.aRelationshipTitleT == E_aRelationshipTitleT.Other)
                {
                    role.BORROWER.BORROWER_DETAIL.BorrowerRelationshipTitleTypeOtherDescription = MISMO3_2Convert.ToMISMO3_2(borrower.RelationshipTitleTOtherDesc, false);
                }
                else
                {
                    role.BORROWER.BORROWER_DETAIL.BorrowerRelationshipTitleTypeOtherDescription = MISMO3_2Convert.ToMISMO3_2(CAppBase.aRelationshipTitleT_Map_rep(borrower.RelationshipTitleT), false);
                }
            }


            borrowerParty.ROLES = new ROLES();
            borrowerParty.ROLES.ROLEList.Add(role);

            borrowerParty.TAXPAYER_IDENTIFIERS = new TAXPAYER_IDENTIFIERS();
            borrowerParty.TAXPAYER_IDENTIFIERS.TAXPAYER_IDENTIFIERList.Add(new TAXPAYER_IDENTIFIER()
            {
                TaxpayerIdentifierType = MISMO3_2Convert.ToMISMO3_2(E_TaxpayerIdentifierBase.SocialSecurityNumber, false),
                TaxpayerIdentifierValue = MISMO3_2Convert.ToMISMO3_2MISMONumericString(borrower.SSN, true)
            });

            return borrowerParty;
        }

        private INDIVIDUAL CreateIndividual()
        {
            INDIVIDUAL individual = new INDIVIDUAL();
            individual.CONTACT_POINTS = CreateContactPoints(E_PartyType.Borrower);
            individual.NAME = CreateName(E_PartyType.Borrower);

            return individual;
        }

        private TAXPAYER_IDENTIFIERS CreateTaxPayerIdentifiers()
        {
            TAXPAYER_IDENTIFIERS taxIDs = new TAXPAYER_IDENTIFIERS();
            taxIDs.TAXPAYER_IDENTIFIERList.Add(CreateTaxPayerSSN(1));

            return taxIDs;
        }

        private TAXPAYER_IDENTIFIER CreateTaxPayerSSN(int iTaxIDSequenceNumber)
        {
            TAXPAYER_IDENTIFIER taxID = new TAXPAYER_IDENTIFIER();
            taxID.SequenceNumber = iTaxIDSequenceNumber;
            taxID.TaxpayerIdentifierType = MISMO3_2Convert.ToMISMO3_2(E_TaxpayerIdentifierBase.SocialSecurityNumber, false);
            taxID.TaxpayerIdentifierValue = MISMO3_2Convert.ToMISMO3_2MISMONumericString(m_dataApp.aSsn, true);

            return taxID;
        }
        #endregion

        #region Legal Entity
        private LEGAL_ENTITY CreateLegalEntity(E_PartyType ePartyType)
        {
            LEGAL_ENTITY legalEntity = new LEGAL_ENTITY();
            legalEntity.CONTACTS = CreateContacts(ePartyType);
            legalEntity.LEGAL_ENTITY_DETAIL = CreateLegalEntityDetail(ePartyType);

            return legalEntity;
        }

        // This method assumes that m_dataApp will already hold the desired app, and that borrower/co-borrower mode has been set
        private LEGAL_ENTITY_DETAIL CreateLegalEntityDetail(E_PartyType ePartyType)
        {
            LEGAL_ENTITY_DETAIL legalEntityDetail = new LEGAL_ENTITY_DETAIL();

            switch(ePartyType)
            {
                case E_PartyType.Borrower:
                    legalEntityDetail.FullName = MISMO3_2Convert.ToMISMO3_2(m_dataApp.aNm, true);
                    break;
                case E_PartyType.Lender: // Lender entity
                    legalEntityDetail.FullName = MISMO3_2Convert.ToMISMO3_2(m_BrokerDB.Name, true);
                    break;
                case E_PartyType.LQB:
                    legalEntityDetail.FullName = MISMO3_2Convert.ToMISMO3_2("LendingQB", true);
                    break;
                case E_PartyType.RespondTo:
                    break; // this type does not require a legal entity
                case E_PartyType.TitleProvider:
                    legalEntityDetail.FullName = MISMO3_2Convert.ToMISMO3_2(m_sVendorLegalName, true);
                    break;
                default:
                    throw new UnhandledEnumException(ePartyType);
            }

            return legalEntityDetail;
        }
        #endregion

        #region Roles
        private ROLES CreateRoles(E_PartyType ePartyType)
        {
            ROLES roles = new ROLES();

            switch(ePartyType)
            {
                case E_PartyType.Borrower:
                    roles.ROLEList.Add(CreateBorrowerRole(ePartyType));
                    break;
                case E_PartyType.Lender:
                    roles.ROLEList.Add(CreateLenderRole(ePartyType, 1));
                    break;
                case E_PartyType.LQB:
                    roles.ROLEList.Add(CreateLQBRole(ePartyType, 1));
                    break;
                case E_PartyType.RespondTo:
                    roles.ROLEList.Add(CreateRespondToRole(ePartyType, 1));
                    break;
                case E_PartyType.TitleProvider:
                    roles.ROLEList.Add(CreateRespondToRole(ePartyType, 1));
                    break;
                default:
                    throw new UnhandledEnumException(ePartyType);
            }

            return roles;
        }

        #region Borrower Role // This block assumes that m_dataApp will already hold the desired app, and that borrower/co-borrower mode has been set
        private ROLE CreateBorrowerRole(E_PartyType ePartyType)
        {
            ROLE borrowerRole = new ROLE();
            borrowerRole.SequenceNumber = m_iBorrowerSequenceNumber;
            borrowerRole.BORROWER = CreateBorrower();
            borrowerRole.ROLE_DETAIL = CreateRoleDetail(ePartyType);

            return borrowerRole;
        }

        private BORROWER CreateBorrower()
        {
            BORROWER borrower = new BORROWER();
            borrower.BORROWER_DETAIL = CreateBorrowerDetail();
            borrower.RESIDENCES = CreateResidences();

            return borrower;
        }

        private BORROWER_DETAIL CreateBorrowerDetail()
        {
            BORROWER_DETAIL borrowerDetail = new BORROWER_DETAIL();
            bool bIsBorrowerPrimaryWE = (m_dataApp.BorrowerModeT == E_BorrowerModeT.Borrower) ? m_dataApp.aBIsPrimaryWageEarner : m_dataApp.aCIsPrimaryWageEarner;
            borrowerDetail.BorrowerClassificationType = MISMO3_2Convert.ToMISMO3_2BorrowerClassification(bIsBorrowerPrimaryWE, false);
            borrowerDetail.BorrowerMailToAddressSameAsPropertyIndicator = MISMO3_2Convert.ToMISMO3_2(m_dataApp.aAddrMailUsePresentAddr, false);
            borrowerDetail.BorrowerRelationshipTitleType = MISMO3_2Convert.ToMISMO3_2(m_dataApp.aRelationshipTitleT, false);
            if (borrowerDetail.BorrowerRelationshipTitleType.eValue == E_BorrowerRelationshipTitleBase.Other)
            {
                if (m_dataApp.aRelationshipTitleT == E_aRelationshipTitleT.Other)
                {
                    borrowerDetail.BorrowerRelationshipTitleTypeOtherDescription = MISMO3_2Convert.ToMISMO3_2(m_dataApp.aRelationshipTitleOtherDesc, false);
                }
                else
                {
                    borrowerDetail.BorrowerRelationshipTitleTypeOtherDescription = MISMO3_2Convert.ToMISMO3_2(m_dataApp.aRelationshipTitleT_rep, false);
                }
            }
            borrowerDetail.MaritalStatusType = MISMO3_2Convert.ToMISMO3_2(m_dataApp.aMaritalStatT, false);

            return borrowerDetail;
        }

        private RESIDENCES CreateResidences()
        {
            RESIDENCES residences = new RESIDENCES();
            residences.RESIDENCEList.Add(CreateBorrowerResidence(E_BorrowerResidenceType.Present, 1));
            if (!m_dataApp.aAddrMailUsePresentAddr)
            {
                residences.RESIDENCEList.Add(CreateBorrowerResidence(E_BorrowerResidenceType.Mailing, 2));
            }

            return residences;
        }

        private RESIDENCE CreateBorrowerResidence(E_BorrowerResidenceType eResidenceType, int iResidenceSequenceNumber)
        {
            RESIDENCE borrowerResidence = new RESIDENCE();
            borrowerResidence.SequenceNumber = iResidenceSequenceNumber;
            switch(eResidenceType)
            {
                case E_BorrowerResidenceType.Mailing:
                    borrowerResidence.ADDRESS = CreateBorrowerMailingAddress();
                    break;
                case E_BorrowerResidenceType.Present:
                    borrowerResidence.ADDRESS = CreateBorrowerAddress();
                    break;
                default:
                    throw new UnhandledEnumException(eResidenceType);
            }

            return borrowerResidence;
        }
        #endregion

        #region Lender Role
        private ROLE CreateLenderRole(E_PartyType ePartyType, int iRoleSequenceNumber)
        {
            ROLE requestingParty = new ROLE();
            requestingParty.SequenceNumber = iRoleSequenceNumber;
            requestingParty.REQUESTING_PARTY = CreateRequestingParty();
            requestingParty.ROLE_DETAIL = CreateRoleDetail(ePartyType);

            return requestingParty;
        }

        private REQUESTING_PARTY CreateRequestingParty()
        {
            REQUESTING_PARTY requestor = new REQUESTING_PARTY();
            requestor.InternalAccountIdentifier = MISMO3_2Convert.ToMISMO3_2Identifier(VendorAccountID, true);
            requestor.RequestedByName = MISMO3_2Convert.ToMISMO3_2(m_EmployeeDB.FullName, false);
            //requestor.RequestingPartyBranchIdentifier;

            return requestor;
        }
        #endregion

        #region LQB Role
        private ROLE CreateLQBRole(E_PartyType ePartyType, int iRoleSequenceNumber)
        {
            ROLE submittingParty = new ROLE();
            submittingParty.SequenceNumber = iRoleSequenceNumber;
            submittingParty.ROLE_DETAIL = CreateRoleDetail(ePartyType);
            submittingParty.SUBMITTING_PARTY = CreateSubmittingParty();

            return submittingParty;
        }

        private SUBMITTING_PARTY CreateSubmittingParty()
        {
            SUBMITTING_PARTY submitter = new SUBMITTING_PARTY();
            submitter.SubmittingPartySequenceNumber = MISMO3_2Convert.ToMISMO3_2(1, false);
            submitter.SubmittingPartyTransactionIdentifier = MISMO3_2Convert.ToMISMO3_2Identifier(TransactionID, true);

            return submitter;
        }
        #endregion

        #region RespondTo Role
        private ROLE CreateRespondToRole(E_PartyType ePartyType, int iRoleSequenceNumber)
        {
            ROLE respondTo = new ROLE();
            respondTo.SequenceNumber = iRoleSequenceNumber;
            respondTo.RETURN_TO = CreateReturnTo();
            respondTo.ROLE_DETAIL = CreateRoleDetail(ePartyType);

            return respondTo;
        }

        private RETURN_TO CreateReturnTo()
        {
            RETURN_TO replyTo = new RETURN_TO();
            replyTo.PREFERRED_RESPONSES = CreatePreferredResponses();

            return replyTo;
        }

        private PREFERRED_RESPONSES CreatePreferredResponses()
        {
            PREFERRED_RESPONSES preferredResponses = new PREFERRED_RESPONSES();
            preferredResponses.PREFERRED_RESPONSEList.Add(CreatePreferredResponse());

            return preferredResponses;
        }

        private PREFERRED_RESPONSE CreatePreferredResponse()
        {
            PREFERRED_RESPONSE preferredResponse = new PREFERRED_RESPONSE();
            //preferredResponse.MIMETypeIdentifier;
            preferredResponse.PreferredResponseDestinationDescription = MISMO3_2Convert.ToMISMO3_2("Direct the response to LendingQB.", false);
            preferredResponse.PreferredResponseFormatType = MISMO3_2Convert.ToMISMO3_2(E_PreferredResponseFormatBase.XML, false);
            //preferredResponse.PreferredResponseFormatTypeOtherDescription;
            preferredResponse.PreferredResponseMethodType = MISMO3_2Convert.ToMISMO3_2(E_PreferredResponseMethodBase.HTTPS, false);
            //preferredResponse.PreferredResponseMethodTypeOtherDescription;
            //preferredResponse.PreferredResponseUseEmbeddedFileIndicator;
            preferredResponse.PreferredResponseVersionIdentifier = MISMO3_2Convert.ToMISMO3_2Identifier("MISMO 3.2.0", false);
            preferredResponse.SequenceNumber = 1;

            return preferredResponse;
        }
        #endregion

        private ROLE_DETAIL CreateRoleDetail(E_PartyType ePartyType)
        {
            ROLE_DETAIL roleDetail = new ROLE_DETAIL();

            E_PartyRoleBase eMISMOPartyType = E_PartyRoleBase.None;
            switch(ePartyType)
            {
                case E_PartyType.Borrower:
                    eMISMOPartyType = MISMO3_2Convert.ToMISMO3_2PartyRoleType(m_dataApp.aTypeT);
                    break;
                case E_PartyType.Lender:
                    eMISMOPartyType = E_PartyRoleBase.RequestingParty;
                    break;
                case E_PartyType.LQB:
                    eMISMOPartyType = E_PartyRoleBase.SubmittingParty;
                    break;
                case E_PartyType.RespondTo:
                    eMISMOPartyType = E_PartyRoleBase.ReturnTo;
                    break;
                case E_PartyType.TitleProvider:
                    eMISMOPartyType = E_PartyRoleBase.RespondingParty;
                    break;
                default:
                    throw new UnhandledEnumException(ePartyType);
            }

            roleDetail.PartyRoleType = MISMO3_2Convert.ToMISMO3_2(eMISMOPartyType, false);
            //roleDetail.PartyRoleTypeOtherDescription;

            return roleDetail;
        }
        #endregion
        #endregion

        #region Services
        private SERVICES CreateServices()
        {
            SERVICES services = new SERVICES();
            
            int iServiceCount = 0;
            if (!string.IsNullOrEmpty(m_sOwnerPolicyName))
            {
                iServiceCount++;
                m_eTitlePolicyType = E_cTitleInsurancePolicyT.Owner;
                services.SERVICEList.Add(CreateTitleService(iServiceCount));
            }

            if (!string.IsNullOrEmpty(m_sLenderPolicyName))
            {
                iServiceCount++;
                m_eTitlePolicyType = E_cTitleInsurancePolicyT.Lender;
                services.SERVICEList.Add(CreateTitleService(iServiceCount));
            }

            return services;
        }

        private SERVICE CreateTitleService(int iTitleServiceSequenceNumber)
        {
            SERVICE service = new SERVICE();
            service.SequenceNumber = iTitleServiceSequenceNumber;
            service.SERVICE_PRODUCT = CreateServiceProduct();
            service.TITLE = CreateTitle();

            return service;
        }

        #region Service Product
        private SERVICE_PRODUCT CreateServiceProduct()
        {
            SERVICE_PRODUCT serviceProduct = new SERVICE_PRODUCT();
            serviceProduct.SERVICE_PRODUCT_REQUEST = CreateServiceProductRequest();

            return serviceProduct;
        }

        private SERVICE_PRODUCT_REQUEST CreateServiceProductRequest()
        {
            SERVICE_PRODUCT_REQUEST serviceProductRequest = new SERVICE_PRODUCT_REQUEST();
            serviceProductRequest.SERVICE_PRODUCT_DETAIL = CreateServiceProductDetail();
            serviceProductRequest.SERVICE_PRODUCT_NAMES = CreateServiceProductNames();

            return serviceProductRequest;
        }

        private SERVICE_PRODUCT_DETAIL CreateServiceProductDetail()
        {
            SERVICE_PRODUCT_DETAIL serviceProductDetail = new SERVICE_PRODUCT_DETAIL();
            serviceProductDetail.ServiceProductDescription = MISMO3_2Convert.ToMISMO3_2("Title", false);
            serviceProductDetail.ServiceProductIdentifier = MISMO3_2Convert.ToMISMO3_2Identifier("1", false);

            return serviceProductDetail;
        }

        #region Service Product Names
        private SERVICE_PRODUCT_NAMES CreateServiceProductNames()
        {
            SERVICE_PRODUCT_NAMES serviceProductNames = new SERVICE_PRODUCT_NAMES();
            serviceProductNames.SERVICE_PRODUCT_NAMEList.Add(CreateServiceProductName(1));

            return serviceProductNames;
        }

        private SERVICE_PRODUCT_NAME CreateServiceProductName(int iServiceProductSequenceNumber)
        {
            SERVICE_PRODUCT_NAME serviceProduct = new SERVICE_PRODUCT_NAME();
            serviceProduct.SequenceNumber = iServiceProductSequenceNumber;
            serviceProduct.SERVICE_PRODUCT_NAME_ADD_ONS = CreateServiceProductNameAddOns();
            serviceProduct.SERVICE_PRODUCT_NAME_DETAIL = CreateServiceProductNameDetail("Title", PolicyName);

            return serviceProduct;
        }

        private SERVICE_PRODUCT_NAME_ADD_ONS CreateServiceProductNameAddOns()
        {
            SERVICE_PRODUCT_NAME_ADD_ONS serviceAddOns = new SERVICE_PRODUCT_NAME_ADD_ONS();
            int iAddonCount = 0;

            if (m_bSigningRequested)
            {
                iAddonCount++;
                serviceAddOns.SERVICE_PRODUCT_NAME_ADD_ONList.Add(CreateServiceProductNameAddOn("Signing", "Signing", iAddonCount));
            }

            if (m_bEscrowRequested)
            {
                iAddonCount++;
                serviceAddOns.SERVICE_PRODUCT_NAME_ADD_ONList.Add(CreateServiceProductNameAddOn("Closing", "Escrow", iAddonCount));
            }

            return serviceAddOns;
        }

        private SERVICE_PRODUCT_NAME_ADD_ON CreateServiceProductNameAddOn(string sAddOnDescription, string sAddOnIdentifier, int iAddOnSequenceNumber)
        {
            SERVICE_PRODUCT_NAME_ADD_ON serviceAddOn = new SERVICE_PRODUCT_NAME_ADD_ON();
            serviceAddOn.SequenceNumber = iAddOnSequenceNumber;
            serviceAddOn.ServiceProductNameAddOnDescription = MISMO3_2Convert.ToMISMO3_2(sAddOnDescription, false);
            serviceAddOn.ServiceProductNameAddOnIdentifier = MISMO3_2Convert.ToMISMO3_2Identifier(sAddOnIdentifier, false);

            return serviceAddOn;
        }

        private SERVICE_PRODUCT_NAME_DETAIL CreateServiceProductNameDetail(string sProductName, string sProductIdentifier)
        {
            SERVICE_PRODUCT_NAME_DETAIL serviceProductNameDetail = new SERVICE_PRODUCT_NAME_DETAIL();
            serviceProductNameDetail.ServiceProductNameDescription = MISMO3_2Convert.ToMISMO3_2(sProductName, false);
            serviceProductNameDetail.ServiceProductNameIdentifier = MISMO3_2Convert.ToMISMO3_2Identifier(sProductIdentifier, false);

            return serviceProductNameDetail;
        }
        #endregion
        #endregion

        #region Title Service
        private TITLE CreateTitle()
        {
            TITLE titleService = new TITLE();
            titleService.TITLE_REQUEST = CreateTitleServiceRequest();

            return titleService;
        }

        private TITLE_REQUEST CreateTitleServiceRequest()
        {
            TITLE_REQUEST titleRequest = new TITLE_REQUEST();

            switch(m_eTitlePolicyType)
            {
                case E_cTitleInsurancePolicyT.Lender:
                    titleRequest.TITLE_REQUEST_DETAIL = CreateLenderTitleServiceRequestDetail();
                    break;
                case E_cTitleInsurancePolicyT.Owner:
                    titleRequest.TITLE_REQUEST_DETAIL = CreateOwnerTitleServiceRequestDetail();
                    break;
                case E_cTitleInsurancePolicyT.Simultaneous:
                default:
                    throw new UnhandledEnumException(m_eTitlePolicyType);
            }
            
            return titleRequest;
        }

        private TITLE_REQUEST_DETAIL CreateLenderTitleServiceRequestDetail()
        {
            TITLE_REQUEST_DETAIL titleRequestDetail = new TITLE_REQUEST_DETAIL();

            titleRequestDetail.InsuredName = MISMO3_2Convert.ToMISMO3_2(m_BrokerDB.Name, false);
            titleRequestDetail.NAICTitlePolicyClassificationType = MISMO3_2Convert.ToMISMO3_2(E_NAICTitlePolicyClassificationBase.Residential, false);
            titleRequestDetail.NamedInsuredType = MISMO3_2Convert.ToMISMO3_2(m_eTitlePolicyType, false);
            //titleRequestDetail.ProcessorIdentifier;
            //titleRequestDetail.RequestedClosingDate;
            //titleRequestDetail.RequestedClosingTime;
            titleRequestDetail.TitleAssociationType = MISMO3_2Convert.ToMISMO3_2TitleAssociation(PolicyName, false);
            //titleRequestDetail.TitleAssociationTypeOtherDescription;
            //titleRequestDetail.TitleOfficeIdentifier;
            //titleRequestDetail.TitleOwnershipType;
            //titleRequestDetail.TitleOwnershipTypeOtherDescription;
            titleRequestDetail.TitleRequestActionType = MISMO3_2Convert.ToMISMO3_2(E_TitleRequestActionBase.Original, false);
            //titleRequestDetail.TitleRequestCommentDescription;
            titleRequestDetail.TitleRequestProposedTitleInsuranceCoverageAmount = MISMO3_2Convert.ToMISMO3_2MISMOAmount(m_dataLoan.sTotalLAmtWithNewSubordinateFinancing_rep, false);
            //titleRequestDetail.VendorOrderIdentifier; // to be used on updates/cancellations
            //titleRequestDetail.VendorTransactionIdentifier;

            return titleRequestDetail;
        }

        private TITLE_REQUEST_DETAIL CreateOwnerTitleServiceRequestDetail()
        {
            TITLE_REQUEST_DETAIL titleRequestDetail = new TITLE_REQUEST_DETAIL();

            titleRequestDetail.InsuredName = MISMO3_2Convert.ToMISMO3_2(m_dataApp.aNm, true);
            titleRequestDetail.NAICTitlePolicyClassificationType = MISMO3_2Convert.ToMISMO3_2(E_NAICTitlePolicyClassificationBase.Residential, false);
            titleRequestDetail.NamedInsuredType = MISMO3_2Convert.ToMISMO3_2(m_eTitlePolicyType, false);
            //titleRequestDetail.ProcessorIdentifier;
            //titleRequestDetail.RequestedClosingDate;
            //titleRequestDetail.RequestedClosingTime;
            titleRequestDetail.TitleAssociationType = MISMO3_2Convert.ToMISMO3_2TitleAssociation(PolicyName, false);
            //titleRequestDetail.TitleAssociationTypeOtherDescription;
            //titleRequestDetail.TitleOfficeIdentifier;
            titleRequestDetail.TitleOwnershipType = MISMO3_2Convert.ToMISMO3_2(E_TitleOwnershipBase.Individual, false);
            //titleRequestDetail.TitleOwnershipTypeOtherDescription;
            titleRequestDetail.TitleRequestActionType = MISMO3_2Convert.ToMISMO3_2(E_TitleRequestActionBase.Original, false);
            //titleRequestDetail.TitleRequestCommentDescription;
            titleRequestDetail.TitleRequestProposedTitleInsuranceCoverageAmount = MISMO3_2Convert.ToMISMO3_2MISMOAmount(m_dataLoan.sApprVal_rep, false);
            //titleRequestDetail.VendorOrderIdentifier; // to be used on updates/cancellations
            //titleRequestDetail.VendorTransactionIdentifier;

            return titleRequestDetail;
        }
        #endregion
        #endregion

        #region Addresses
        private ADDRESSES CreateAddresses(E_PartyType ePartyType)
        {
            ADDRESSES addresses = new ADDRESSES();
            switch (ePartyType)
            {
                case E_PartyType.Lender:
                    addresses.ADDRESSList.Add(CreateLenderAddress(1));
                    break;
                case E_PartyType.LQB:
                    addresses.ADDRESSList.Add(CreateLQBAddress(1));
                    break;
                case E_PartyType.Borrower:
                case E_PartyType.RespondTo:
                case E_PartyType.TitleProvider:
                    break; // these types do not require address info
                default:
                    throw new UnhandledEnumException(ePartyType);
            }

            return addresses;
        }

        // Subject Property
        private ADDRESS CreateAddress()
        {
            ADDRESS subjectPropertyAddress = new ADDRESS();
            //subjectPropertyAddress.AddressAdditionalLineText
            subjectPropertyAddress.AddressLineText = MISMO3_2Convert.ToMISMO3_2(m_dataLoan.sSpAddr, false);
            subjectPropertyAddress.AddressType = MISMO3_2Convert.ToMISMO3_2(E_AddressBase.Other, false);
            subjectPropertyAddress.AddressTypeOtherDescription = MISMO3_2Convert.ToMISMO3_2("subject property", false);
            subjectPropertyAddress.CityName = MISMO3_2Convert.ToMISMO3_2(m_dataLoan.sSpCity, false);
            subjectPropertyAddress.CountryCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(StateInfo.USACountryCode, false);
            subjectPropertyAddress.CountryName = MISMO3_2Convert.ToMISMO3_2(StateInfo.USACountryName, false);
            subjectPropertyAddress.CountyCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(m_dataLoan.sHmdaCountyCode, false);
            subjectPropertyAddress.CountyName = MISMO3_2Convert.ToMISMO3_2(m_dataLoan.sSpCounty, false);
            subjectPropertyAddress.PostalCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(m_dataLoan.sSpZip, false);
            subjectPropertyAddress.SequenceNumber = 1;
            subjectPropertyAddress.StateCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(m_dataLoan.sSpState, false);
            subjectPropertyAddress.StateName = MISMO3_2Convert.ToMISMO3_2(this.statesAndTerritories.GetStateName(m_dataLoan.sSpState), false);

            return subjectPropertyAddress;
        }

        // This method assumes that m_dataApp will already hold the desired app, and that borrower/co-borrower mode has been set
        private ADDRESS CreateBorrowerAddress()
        {
            ADDRESS subjectPropertyAddress = new ADDRESS();

            //subjectPropertyAddress.AddressAdditionalLineText
            subjectPropertyAddress.AddressLineText = MISMO3_2Convert.ToMISMO3_2(m_dataApp.aAddr, false);
            subjectPropertyAddress.AddressType = MISMO3_2Convert.ToMISMO3_2(E_AddressBase.Current, false);
            //subjectPropertyAddress.AddressTypeOtherDescription = MISMO3_2Convert.ToMISMO3_2(, false);
            subjectPropertyAddress.CityName = MISMO3_2Convert.ToMISMO3_2(m_dataApp.aCity, false);
            subjectPropertyAddress.CountryCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(StateInfo.USACountryCode, false);
            subjectPropertyAddress.CountryName = MISMO3_2Convert.ToMISMO3_2(StateInfo.USACountryName, false);
            //subjectPropertyAddress.CountyCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(, false);
            //subjectPropertyAddress.CountyName = MISMO3_2Convert.ToMISMO3_2(, false);
            subjectPropertyAddress.PostalCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(m_dataApp.aZip, false);
            //subjectPropertyAddress.SequenceNumber; // residence is already sequenced
            //subjectPropertyAddress.StateCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(, false);
            subjectPropertyAddress.StateName = MISMO3_2Convert.ToMISMO3_2(this.statesAndTerritories.GetStateName(m_dataApp.aState), false);

            return subjectPropertyAddress;
        }

        private ADDRESS CreateBorrowerMailingAddress()
        {
            ADDRESS subjectPropertyAddress = new ADDRESS();

            //subjectPropertyAddress.AddressAdditionalLineText
            subjectPropertyAddress.AddressLineText = MISMO3_2Convert.ToMISMO3_2(m_dataApp.aAddrMail, false);
            subjectPropertyAddress.AddressType = MISMO3_2Convert.ToMISMO3_2(E_AddressBase.Mailing, false);
            //subjectPropertyAddress.AddressTypeOtherDescription = MISMO3_2Convert.ToMISMO3_2(, false);
            subjectPropertyAddress.CityName = MISMO3_2Convert.ToMISMO3_2(m_dataApp.aCityMail, false);
            subjectPropertyAddress.CountryCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(StateInfo.USACountryCode, false);
            subjectPropertyAddress.CountryName = MISMO3_2Convert.ToMISMO3_2(StateInfo.USACountryName, false);
            //subjectPropertyAddress.CountyCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(, false);
            //subjectPropertyAddress.CountyName = MISMO3_2Convert.ToMISMO3_2(, false);
            subjectPropertyAddress.PostalCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(m_dataApp.aZipMail, false);
            //subjectPropertyAddress.SequenceNumber; // residence is already sequenced
            //subjectPropertyAddress.StateCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(, false);
            subjectPropertyAddress.StateName = MISMO3_2Convert.ToMISMO3_2(this.statesAndTerritories.GetStateName(m_dataApp.aStateMail), false);

            return subjectPropertyAddress;
        }

        private ADDRESS CreateLenderAddress(int iAddressSequenceNumber)
        {
            ADDRESS lenderAddress = new ADDRESS();
            //lenderAddress.AddressAdditionalLineText;
            lenderAddress.AddressLineText = MISMO3_2Convert.ToMISMO3_2(m_BrokerDB.Address.StreetAddress, false);
            lenderAddress.AddressType = MISMO3_2Convert.ToMISMO3_2(E_AddressBase.Current, false);
            //lenderAddress.AddressTypeOtherDescription;
            lenderAddress.CityName = MISMO3_2Convert.ToMISMO3_2(m_BrokerDB.Address.City, false);
            lenderAddress.CountryCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(StateInfo.USACountryCode, false);
            lenderAddress.CountryName = MISMO3_2Convert.ToMISMO3_2(StateInfo.USACountryName, false);
            //lenderAddress.CountyCode;
            //lenderAddress.CountyName;
            lenderAddress.PostalCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(m_BrokerDB.Address.Zipcode, false);
            lenderAddress.SequenceNumber = iAddressSequenceNumber;
            lenderAddress.StateCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(m_BrokerDB.Address.State, false);
            lenderAddress.StateName = MISMO3_2Convert.ToMISMO3_2(this.statesAndTerritories.GetStateName(m_BrokerDB.Address.State), false);

            return lenderAddress;
        }

        private ADDRESS CreateLQBAddress(int iAddressSequenceNumber)
        {
            ADDRESS LQBAddress = new ADDRESS();
            CommonLib.Address lqbAddress = ConstApp.GetLendingQBAddress();

            //LQBAddress.AddressAdditionalLineText;
            LQBAddress.AddressLineText = MISMO3_2Convert.ToMISMO3_2(lqbAddress.StreetAddress, false);
            LQBAddress.AddressType = MISMO3_2Convert.ToMISMO3_2(E_AddressBase.Current, false);
            //LQBAddress.AddressTypeOtherDescription;
            LQBAddress.CityName = MISMO3_2Convert.ToMISMO3_2(lqbAddress.City, false);
            LQBAddress.CountryCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(StateInfo.USACountryCode, false);
            LQBAddress.CountryName = MISMO3_2Convert.ToMISMO3_2(StateInfo.USACountryName, false);
            LQBAddress.CountyCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(this.statesAndTerritories.GetFIPSCodeForCounty(lqbAddress.State, ConstApp.lqbCounty), false);
            LQBAddress.CountyName = MISMO3_2Convert.ToMISMO3_2(ConstApp.lqbCounty, false);
            LQBAddress.PostalCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(lqbAddress.Zipcode, false);
            LQBAddress.SequenceNumber = iAddressSequenceNumber;
            LQBAddress.StateCode = MISMO3_2Convert.ToMISMO3_2MISMOCode(lqbAddress.State, false);
            LQBAddress.StateName = MISMO3_2Convert.ToMISMO3_2(this.statesAndTerritories.GetStateName(lqbAddress.State), false);

            return LQBAddress;
        }
        #endregion

        #region Contacts
        private CONTACTS CreateContacts(E_PartyType ePartyType)
        {
            CONTACTS contacts = new CONTACTS();
            switch (ePartyType)
            {
                case E_PartyType.Lender:
                case E_PartyType.LQB:
                    contacts.CONTACTList.Add(CreateContact(ePartyType, 1));
                    break;
                case E_PartyType.Borrower:
                case E_PartyType.RespondTo:
                case E_PartyType.TitleProvider:
                    break; // these types do not require a contacts block
                default:
                    throw new UnhandledEnumException(ePartyType);
            }

            return contacts;
        }

        private CONTACT CreateContact(E_PartyType ePartyType, int iContactSequenceNumber)
        {
            CONTACT contact = new CONTACT();
            contact.SequenceNumber = iContactSequenceNumber;
            contact.CONTACT_POINTS = CreateContactPoints(ePartyType);
            contact.NAME = CreateName(ePartyType);

            return contact;
        }

        #region Contact Points
        private CONTACT_POINTS CreateContactPoints(E_PartyType ePartyType)
        {
            CONTACT_POINTS contactPoints = new CONTACT_POINTS();
            switch(ePartyType)
            {
                case E_PartyType.Borrower:
                    contactPoints.CONTACT_POINTList.Add(CreateTelephoneContactPoint(ePartyType, E_ContactPointRoleBase.Home, 1));
                    contactPoints.CONTACT_POINTList.Add(CreateTelephoneContactPoint(ePartyType, E_ContactPointRoleBase.Work, 2));
                    contactPoints.CONTACT_POINTList.Add(CreateFaxContactPoint(ePartyType, E_ContactPointRoleBase.Work, 3));
                    contactPoints.CONTACT_POINTList.Add(CreateEmailContactPoint(ePartyType, E_ContactPointRoleBase.Home, 4));
                    break;
                case E_PartyType.Lender:
                    contactPoints.CONTACT_POINTList.Add(CreateTelephoneContactPoint(ePartyType, E_ContactPointRoleBase.Work, 1));
                    contactPoints.CONTACT_POINTList.Add(CreateFaxContactPoint(ePartyType, E_ContactPointRoleBase.Work, 2));
                    contactPoints.CONTACT_POINTList.Add(CreateEmailContactPoint(ePartyType, E_ContactPointRoleBase.Work, 3));
                    break;
                case E_PartyType.LQB:
                    contactPoints.CONTACT_POINTList.Add(CreateTelephoneContactPoint(ePartyType, E_ContactPointRoleBase.Work, 1));
                    contactPoints.CONTACT_POINTList.Add(CreateFaxContactPoint(ePartyType, E_ContactPointRoleBase.Work, 2));
                    contactPoints.CONTACT_POINTList.Add(CreateEmailContactPoint(ePartyType, E_ContactPointRoleBase.Work, 3));
                    break;
                case E_PartyType.RespondTo:
                case E_PartyType.TitleProvider:
                    break; // these types do not require contact points
                default: throw new UnhandledEnumException(ePartyType);
            }
            
            return contactPoints;
        }

        private CONTACT_POINT CreateTelephoneContactPoint(E_PartyType ePartyType, E_ContactPointRoleBase eContactPointType, int iContactPointSequenceNumber)
        {
            CONTACT_POINT telephoneContact = new CONTACT_POINT();
            telephoneContact.SequenceNumber = iContactPointSequenceNumber;
            telephoneContact.CONTACT_POINT_DETAIL = CreateContactPointDetail(eContactPointType);
            telephoneContact.CONTACT_POINT_TELEPHONE = CreateTelephoneContactPointTelephone(ePartyType, eContactPointType);

            return telephoneContact;
        }

        private CONTACT_POINT CreateFaxContactPoint(E_PartyType ePartyType, E_ContactPointRoleBase eContactPointType, int iContactPointSequenceNumber)
        {
            CONTACT_POINT faxContact = new CONTACT_POINT();
            faxContact.SequenceNumber = iContactPointSequenceNumber;
            faxContact.CONTACT_POINT_DETAIL = CreateContactPointDetail(eContactPointType);
            faxContact.CONTACT_POINT_TELEPHONE = CreateFaxContactPointTelephone(ePartyType, eContactPointType);

            return faxContact;
        }

        private CONTACT_POINT CreateEmailContactPoint(E_PartyType ePartyType, E_ContactPointRoleBase eContactPointType, int iContactPointSequenceNumber)
        {
            CONTACT_POINT emailContact = new CONTACT_POINT();
            emailContact.SequenceNumber = iContactPointSequenceNumber;
            emailContact.CONTACT_POINT_DETAIL = CreateContactPointDetail(eContactPointType);
            emailContact.CONTACT_POINT_EMAIL = CreateContactPointEmail(ePartyType);

            return emailContact;
        }

        private CONTACT_POINT_DETAIL CreateContactPointDetail(E_ContactPointRoleBase eContactPointType)
        {
            return CreateContactPointDetail(eContactPointType, string.Empty);
        }

        private CONTACT_POINT_DETAIL CreateContactPointDetail(E_ContactPointRoleBase eContactPointType, string sOtherContactPointDescription)
        {
            CONTACT_POINT_DETAIL contactPointDetail = new CONTACT_POINT_DETAIL();
            contactPointDetail.ContactPointPreferenceIndicator = MISMO3_2Convert.ToMISMO3_2(true, false);
            contactPointDetail.ContactPointRoleType = MISMO3_2Convert.ToMISMO3_2(eContactPointType, false);
            string sContactPointOtherDescr = (eContactPointType == E_ContactPointRoleBase.Other) ? sOtherContactPointDescription : string.Empty;
            contactPointDetail.ContactPointRoleTypeOtherDescription = MISMO3_2Convert.ToMISMO3_2(sContactPointOtherDescr, false);

            return contactPointDetail;
        }

        private CONTACT_POINT_TELEPHONE CreateTelephoneContactPointTelephone(E_PartyType ePartyType, E_ContactPointRoleBase eContactPointType)
        {
            CONTACT_POINT_TELEPHONE telephone = new CONTACT_POINT_TELEPHONE();
            //telephone.ContactPointTelephoneExtensionValue;

            switch(ePartyType)
            {
                case E_PartyType.Borrower:
                    telephone.ContactPointTelephoneValue = MISMO3_2Convert.ToMISMO3_2MISMONumericString(GetBorrowerTelephoneNumber(eContactPointType), false);
                    break;
                case E_PartyType.Lender:
                    telephone.ContactPointTelephoneValue = MISMO3_2Convert.ToMISMO3_2MISMONumericString(GetLenderUserTelephoneNumber(eContactPointType), false);
                    break;
                case E_PartyType.LQB:
                    telephone.ContactPointTelephoneValue = MISMO3_2Convert.ToMISMO3_2MISMONumericString(ConstApp.lqbPhoneNumber, false);
                    break;
                case E_PartyType.RespondTo:
                case E_PartyType.TitleProvider:
                    break; // these types do not require a phone number
                default:
                    throw new UnhandledEnumException(ePartyType);
            }

            return telephone;
        }

        private CONTACT_POINT_TELEPHONE CreateFaxContactPointTelephone(E_PartyType ePartyType, E_ContactPointRoleBase eContactPointType)
        {
            CONTACT_POINT_TELEPHONE fax = new CONTACT_POINT_TELEPHONE();
            //fax.ContactPointFaxExtensionValue;

            switch(ePartyType)
            {
                case E_PartyType.Borrower:
                    fax.ContactPointFaxValue = MISMO3_2Convert.ToMISMO3_2MISMONumericString(GetBorrowerFaxNumber(eContactPointType), false);
                    break;
                case E_PartyType.Lender:
                    fax.ContactPointFaxValue = MISMO3_2Convert.ToMISMO3_2MISMONumericString(GetLenderUserFaxNumber(eContactPointType), false);
                    break;
                case E_PartyType.LQB:
                    fax.ContactPointFaxValue = MISMO3_2Convert.ToMISMO3_2MISMONumericString("7147086956", false);
                    break;
                case E_PartyType.RespondTo:
                case E_PartyType.TitleProvider:
                    break; // these types do not require a fax number
                default:
                    throw new UnhandledEnumException(ePartyType);
            }

            return fax;
        }

        private CONTACT_POINT_EMAIL CreateContactPointEmail(E_PartyType ePartyType)
        {
            CONTACT_POINT_EMAIL email = new CONTACT_POINT_EMAIL();

            switch(ePartyType)
            {
                case E_PartyType.Borrower:
                    email.ContactPointEmailValue = MISMO3_2Convert.ToMISMO3_2MISMOValue(m_dataApp.aEmail, false);
                    break;
                case E_PartyType.Lender:
                    email.ContactPointEmailValue = MISMO3_2Convert.ToMISMO3_2MISMOValue(m_EmployeeDB.Email, false);
                    break;
                case E_PartyType.LQB:
                    email.ContactPointEmailValue = MISMO3_2Convert.ToMISMO3_2MISMOValue(ConstStage.LQBIntegrationEmail, false);
                    break;
                case E_PartyType.RespondTo:
                case E_PartyType.TitleProvider:
                    break; // these types do not require an email address
                default:
                    throw new UnhandledEnumException(ePartyType);
            }
            
            return email;
        }

        #region Party Telephone/Fax Data
        // This method assumes that m_dataApp will already hold the desired app, and that borrower/co-borrower mode has been set
        private string GetBorrowerFaxNumber(E_ContactPointRoleBase eFaxType)
        {
            switch (eFaxType)
            {
                case E_ContactPointRoleBase.Home: 
                    return m_dataApp.aFax;
                case E_ContactPointRoleBase.Mobile:
                case E_ContactPointRoleBase.None:
                case E_ContactPointRoleBase.Other:
                    return string.Empty;
                case E_ContactPointRoleBase.Work:
                    return m_dataApp.aFax;
                default:
                    throw new UnhandledEnumException(eFaxType);
            }
        }

        // This method assumes that m_dataApp will already hold the desired app, and that borrower/co-borrower mode has been set
        private string GetBorrowerTelephoneNumber(E_ContactPointRoleBase eTelephoneType)
        {
            switch (eTelephoneType)
            {
                case E_ContactPointRoleBase.Home: return m_dataApp.aHPhone;
                case E_ContactPointRoleBase.Mobile: return m_dataApp.aCellPhone;
                case E_ContactPointRoleBase.None: return string.Empty;
                case E_ContactPointRoleBase.Other: return string.Empty;
                case E_ContactPointRoleBase.Work: return m_dataApp.aBusPhone;
                default:
                    throw new UnhandledEnumException(eTelephoneType);
            }
        }

        private string GetLenderUserFaxNumber(E_ContactPointRoleBase eFaxType)
        {
            switch (eFaxType)
            {
                case E_ContactPointRoleBase.Home:
                    return m_EmployeeDB.Fax;
                case E_ContactPointRoleBase.Mobile:
                case E_ContactPointRoleBase.None:
                case E_ContactPointRoleBase.Other:
                    return string.Empty;
                case E_ContactPointRoleBase.Work:
                    return m_EmployeeDB.Fax;
                default:
                    throw new UnhandledEnumException(eFaxType);
            }
        }

        private string GetLenderUserTelephoneNumber(E_ContactPointRoleBase eTelephoneType)
        {
            switch (eTelephoneType)
            {
                case E_ContactPointRoleBase.Home:
                    return m_EmployeeDB.Phone;
                case E_ContactPointRoleBase.Mobile: 
                case E_ContactPointRoleBase.None: 
                case E_ContactPointRoleBase.Other:
                    return string.Empty;
                case E_ContactPointRoleBase.Work:
                    return m_EmployeeDB.Phone;
                default:
                    throw new UnhandledEnumException(eTelephoneType);
            }
        }
        #endregion
        #endregion
        #endregion

        #region Names
        private NAME CreateName(string sFullName, bool bIsSensitive)
        {
            NAME name = new NAME();
            name.FullName = MISMO3_2Convert.ToMISMO3_2(sFullName, bIsSensitive);

            return name;
        }

        // This method assumes that m_dataApp will already hold the desired app, and that borrower/co-borrower mode has been set
        private NAME CreateName(E_PartyType ePartyType)
        {
            NAME name = new NAME();

            switch (ePartyType)
            {
                case E_PartyType.Borrower:
                    name.FirstName = MISMO3_2Convert.ToMISMO3_2(m_dataApp.aFirstNm, true);
                    name.FullName = MISMO3_2Convert.ToMISMO3_2(m_dataApp.aNm, true);
                    name.LastName = MISMO3_2Convert.ToMISMO3_2(m_dataApp.aLastNm, true);
                    name.MiddleName = MISMO3_2Convert.ToMISMO3_2(m_dataApp.aMidNm, true);
                    //name.PrefixName = MISMO3_2Convert.ToMISMO3_2(, true);
                    name.SuffixName = MISMO3_2Convert.ToMISMO3_2(m_dataApp.aSuffix, true);
                    break;
                case E_PartyType.Lender: // Lender user
                    name.FirstName = MISMO3_2Convert.ToMISMO3_2(m_EmployeeDB.FirstName, false);
                    name.FullName = MISMO3_2Convert.ToMISMO3_2(m_EmployeeDB.FullName, false);
                    name.LastName = MISMO3_2Convert.ToMISMO3_2(m_EmployeeDB.LastName, false);
                    //name.MiddleName = MISMO3_2Convert.ToMISMO3_2(, true);
                    //name.PrefixName = MISMO3_2Convert.ToMISMO3_2(, true);
                    //name.SuffixName = MISMO3_2Convert.ToMISMO3_2(, true);
                    break;
                case E_PartyType.LQB:
                    //name.FirstName = MISMO3_2Convert.ToMISMO3_2(, true);
                    name.FullName = MISMO3_2Convert.ToMISMO3_2("LendingQB Support", false);
                    //name.LastName = MISMO3_2Convert.ToMISMO3_2(, true);
                    //name.MiddleName = MISMO3_2Convert.ToMISMO3_2(, true);
                    //name.PrefixName = MISMO3_2Convert.ToMISMO3_2(, true);
                    //name.SuffixName = MISMO3_2Convert.ToMISMO3_2(, true);
                    break;
                case E_PartyType.RespondTo:
                    break; // this type does not require a Name
                case E_PartyType.TitleProvider:
                    //name.FirstName = MISMO3_2Convert.ToMISMO3_2(, true);
                    name.FullName = MISMO3_2Convert.ToMISMO3_2(m_sVendorLegalName, false);
                    //name.LastName = MISMO3_2Convert.ToMISMO3_2(, true);
                    //name.MiddleName = MISMO3_2Convert.ToMISMO3_2(, true);
                    //name.PrefixName = MISMO3_2Convert.ToMISMO3_2(, true);
                    //name.SuffixName = MISMO3_2Convert.ToMISMO3_2(, true);
                    break;
                default: throw new UnhandledEnumException(ePartyType);
            }

            return name;
        }
        #endregion
        #endregion
    }
}