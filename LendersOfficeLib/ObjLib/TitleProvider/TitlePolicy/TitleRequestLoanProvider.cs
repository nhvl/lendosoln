﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Conversions.Mismo3.Version2;
using LendersOffice.Conversions.Mismo3.Version2.Request;
using LendersOffice.ObjLib.TitleProvider.TitlePolicy.Request;

namespace LendersOffice.ObjLib.TitleProvider.TitlePolicy
{
    /// <summary>
    /// Creates and populates a Title loan object.
    /// </summary>
    public class TitleRequestLoanProvider
    {
        #region Variables
        private CPageData m_dataLoan;
        #endregion

        public TitleRequestLoanProvider(Guid sLId)
        {
            m_dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(TitleRequestLoanProvider));
            m_dataLoan.InitLoad();
            m_dataLoan.SetFormatTarget(FormatTarget.MismoClosing);
            m_dataLoan.ByPassFieldSecurityCheck = true;
        }

        public LOAN CreateLoan(int iLoanSequenceNumber)
        {
            LOAN loan = new LOAN();
            loan.SequenceNumber = iLoanSequenceNumber;
            loan.LOAN_IDENTIFIERS = CreateLoanIdentifiers();
            loan.TERMS_OF_LOAN = CreateTermsOfLoan();

            return loan;
        }

        public LOAN CreateNonLinkedSecondLien(int iLoanSequenceNumber)
        {
            LOAN loan = new LOAN();
            loan.SequenceNumber = iLoanSequenceNumber;
            loan.LOAN_IDENTIFIERS = CreateNonLinkedSecondLienIdentifiers();
            loan.TERMS_OF_LOAN = CreateTermsOfNonLinkedSecondLien();

            return loan;
        }

        private LOAN_IDENTIFIERS CreateLoanIdentifiers()
        {
            LOAN_IDENTIFIERS identifiers = new LOAN_IDENTIFIERS();
            identifiers.LOAN_IDENTIFIERList.Add(CreateLoanIdentifier(1, m_dataLoan.sLNm, E_LoanIdentifierBase.LenderCase, false));

            return identifiers;
        }

        private LOAN_IDENTIFIERS CreateNonLinkedSecondLienIdentifiers()
        {
            LOAN_IDENTIFIERS identifiers = new LOAN_IDENTIFIERS();
            identifiers.LOAN_IDENTIFIERList.Add(CreateLoanIdentifier(1, "Additional2ndLien", E_LoanIdentifierBase.Other, false));

            return identifiers;
        }

        private LOAN_IDENTIFIER CreateLoanIdentifier(int iLoanIdentifierSequenceNumber, string sIdentifier, E_LoanIdentifierBase eIdentifierType, bool bIsSensitive)
        {
            LOAN_IDENTIFIER identifier = new LOAN_IDENTIFIER();
            identifier.LoanIdentifier = MISMO3_2Convert.ToMISMO3_2Identifier(sIdentifier, bIsSensitive);
            identifier.LoanIdentifierType = MISMO3_2Convert.ToMISMO3_2(eIdentifierType, false);
            string sLoanIdentifierTypeOtherDescr = (eIdentifierType == E_LoanIdentifierBase.Other) ? sIdentifier : string.Empty;
            identifier.LoanIdentifierTypeOtherDescription = MISMO3_2Convert.ToMISMO3_2(sLoanIdentifierTypeOtherDescr, false);
            identifier.SequenceNumber = iLoanIdentifierSequenceNumber;

            return identifier;
        }

        private TERMS_OF_LOAN CreateTermsOfLoan()
        {
            TERMS_OF_LOAN terms = new TERMS_OF_LOAN();
            terms.BaseLoanAmount = MISMO3_2Convert.ToMISMO3_2MISMOAmount(m_dataLoan.sLAmtCalc_rep, false);
            terms.LienPriorityType = MISMO3_2Convert.ToMISMO3_2(m_dataLoan.sLienPosT, false);
            //terms.LienPriorityTypeOtherDescription;
            terms.LoanPurposeType = MISMO3_2Convert.ToMISMO3_2(m_dataLoan.sLPurposeT, false);
            string sLoanPurposeOtherDescr = (terms.LoanPurposeType.eValue == E_LoanPurposeBase.Other) ? m_dataLoan.sOLPurposeDesc : string.Empty;
            terms.LoanPurposeTypeOtherDescription = MISMO3_2Convert.ToMISMO3_2(sLoanPurposeOtherDescr, false);
            terms.MortgageType = MISMO3_2Convert.ToMISMO3_2(m_dataLoan.sLT, false);
            string sLoanTypeOtherDescr = (terms.MortgageType.eValue == E_MortgageBase.Other) ? m_dataLoan.sLTODesc : string.Empty;
            terms.MortgageTypeOtherDescription = MISMO3_2Convert.ToMISMO3_2(sLoanTypeOtherDescr, false);
            //terms.SupplementalMortgageType;
            //terms.SupplementalMortgageTypeOtherDescription;

            return terms;
        }

        private TERMS_OF_LOAN CreateTermsOfNonLinkedSecondLien()
        {
            TERMS_OF_LOAN terms = new TERMS_OF_LOAN();
            terms.BaseLoanAmount = MISMO3_2Convert.ToMISMO3_2MISMOAmount(m_dataLoan.sConcurSubFin_rep, false);
            terms.LienPriorityType = MISMO3_2Convert.ToMISMO3_2(E_sLienPosT.Second, false);
            //terms.LienPriorityTypeOtherDescription;
            //terms.LoanPurposeType;
            //terms.LoanPurposeTypeOtherDescription;
            //terms.MortgageType;
            //terms.MortgageTypeOtherDescription;
            //terms.SupplementalMortgageType;
            //terms.SupplementalMortgageTypeOtherDescription;

            return terms;
        }
    }
}
