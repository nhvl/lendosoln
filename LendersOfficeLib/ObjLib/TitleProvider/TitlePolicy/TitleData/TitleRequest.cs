﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.Reflection;
using LendersOffice.Common;
using LendersOffice.Conversions.Mismo3.Version2.Request;

namespace LendersOffice.ObjLib.TitleProvider.TitlePolicy.Request
{
    public enum E_TitleRequestType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("GetQuote")]
        GetQuote,
        [XmlEnum("LenderDocSubmission")]
        LenderDocSubmission,
        [XmlEnum("OrderPolicy")]
        OrderPolicy,
    }

    [XmlRoot("LQBTitleRequest")]
    public class TitleRequest
    {
        [XmlElement(Order = 1)]
        public RequestSource RequestSource = new RequestSource();
        [XmlElement(Order = 2)]
        public Authentication Authentication = new Authentication();
        [XmlElement(Order = 3, ElementName = "Request")]
        public Request Request;
        [XmlElement(Order = 4)]
        public MESSAGE MESSAGE = new MESSAGE(); // MISMO 3.2
    }

    #region Request Source
    public class RequestSource
    {
        [XmlElement(Order = 1)]
        public SourceName SourceName = new SourceName();
        [XmlElement(Order = 2)]
        public TransactionID TransactionID = new TransactionID();
        [XmlElement(Order = 3)]
        public LoanID LoanID = new LoanID();
    }

    public class SourceName
    {
        [XmlText(Type = typeof(string))]
        public string Value;
    }

    public class TransactionID
    {
        [XmlText(Type = typeof(string))]
        public string Value;
    }

    public class LoanID
    {
        [XmlText(Type = typeof(string))]
        public string Value;
    }
    #endregion

    #region Authentication
    public class Authentication
    {
        [XmlElement(Order = 1)]
        public UserName UserName = new UserName();
        [XmlElement(Order = 2)]
        public AccountID AccountID = new AccountID();
        [XmlElement(Order = 3)]
        public Password Password = new Password();
    }

    public class UserName
    {
        [XmlText(Type = typeof(string))]
        public string Value;
    }

    public class AccountID
    {
        [XmlText(Type = typeof(string))]
        public string Value;
    }

    public class Password
    {
        [XmlText(Type = typeof(string))]
        public string Value;
    }
    #endregion

    [XmlType(Namespace = "")]
    [XmlInclude(typeof(Policy))]
    [XmlInclude(typeof(Quote))]
    [XmlInclude(typeof(DocSubmission))]
    public abstract class Request
    {
        [XmlIgnore()]
        protected E_TitleRequestType RequestType;
        [XmlAttribute(AttributeName = "RequestType")]
        public string RequestTypeSerialized { get { return Utilities.ConvertEnumToString(RequestType); } set { } }
        public bool ShouldSerializeRequestTypeSerialized() { return !string.IsNullOrEmpty(RequestTypeSerialized); }

        [XmlAttribute(AttributeName = "PostbackUrl")]
        public string PostbackUrl { get; set; }
        public bool ShouldSerializePostbackUrl() { return !string.IsNullOrEmpty(PostbackUrl); }
    }

    #region Request Types
    public class Policy : Request
    {
        public Policy()
        {
            RequestType = E_TitleRequestType.OrderPolicy;
        }

        [XmlElement(Order = 1)]
        public OrderPolicyRequest OrderPolicyRequest = new OrderPolicyRequest();
    }

    public class OrderPolicyRequest
    {
        [XmlAttribute()]
        public string QuoteID;
        public bool ShouldSerializeQuoteID() { return !string.IsNullOrEmpty(QuoteID); }
    }

    public class Quote : Request
    {
        public Quote()
        {
            RequestType = E_TitleRequestType.GetQuote;
        }

        [XmlElement(Order = 1)]
        public GetQuoteRequest GetQuoteRequest = new GetQuoteRequest();
    }

    public class GetQuoteRequest
    {
        [XmlAttribute()]
        public string CalcOptionID;
        public bool ShouldSerializeCalcOptionID() { return !string.IsNullOrEmpty(CalcOptionID); }
        [XmlAttribute()]
        public string PolicyTypeID;
        public bool ShouldSerializePolicyTypeID() { return !string.IsNullOrEmpty(PolicyTypeID); }
        // Todo: extend this when the quote submission framework is implemented
    }

    public class DocSubmission : Request
    {
        public DocSubmission()
        {
            RequestType = E_TitleRequestType.LenderDocSubmission;
        }

        [XmlElement(Order = 1)]
        public DocSubmissionRequest DocSubmissionRequest = new DocSubmissionRequest();
    }

    public class DocSubmissionRequest
    {
        [XmlAttribute()]
        public string PolicyID;
        public bool ShouldSerializePolicyID() { return !string.IsNullOrEmpty(PolicyID); }
        // Todo: extend this when the doc submission framework is implemented
    }
    #endregion
}
