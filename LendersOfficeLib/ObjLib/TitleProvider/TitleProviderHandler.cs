﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;
using System.Xml;
using DataAccess;
using LQBTitle.VendorXml;
using LendersOffice.Common;

namespace LendersOffice.ObjLib.TitleProvider
{
    public sealed class TitleProviderHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            TitleVendorRequest request = new TitleVendorRequest();
            TitleVendorResponse response = new TitleVendorResponse();
            string content = string.Empty;
            try
            {
                using (StreamReader sr = new StreamReader(context.Request.InputStream))
                {
                    content = sr.ReadToEnd();
                }

                using (StringReader sr = new StringReader(content))
                using (XmlReader xmlReader = XmlReader.Create(sr))
                {
                    request.ReadXml(xmlReader);
                }

                #region validation
                if (request.Request == null || request.Authentication == null)
                {
                    OutputError(context, response, content, E_ServerMessageType.Other, "TitleVendorRequest/Request and  TitleVendorRequest/Authentication node is required.");
                    return;
                }

                if (request.Request.TitleVendorDocSubmissionRequest == null)
                {
                    OutputError(context, response, content, E_ServerMessageType.Other, "TitleVendorRequest/Request/TitleVendorDocSubmissionRequest is required.");
                    return;
                }

                if (request.Request.TitleVendorDocSubmissionRequest.DocumentList.Count == 0)
                {
                    OutputError(context, response, content, E_ServerMessageType.Other, "TitleVendorRequest/Request/TitleVendorDocSubmissionRequest is required.");
                    return;
                }
                #endregion

                
                Guid loanId; 
                try 
                {
                    loanId = new Guid(request.Request.TitleVendorDocSubmissionRequest.LoanId);
                }
                catch (FormatException)
                {
                    OutputError(context, response, content, E_ServerMessageType.DataError, "Invalid loan identifier.");
                    return;
                }

                string username = request.Authentication.UserName;
                string password = request.Authentication.Password;
                
                TitleService service = TitleProvider.GetService(username, password, loanId);

                if (service == null)
                {
                    OutputError(context, response, content, E_ServerMessageType.LoginError, "Could not locate loan.");
                    return;
                }

                List<ServerMessage> serverMessages;
                List<LenderDocSubmissionReceiptResponse> receivedItems; 
                response.RequestStatus = service.ReceiveDocFromVendor(request, out serverMessages, out receivedItems);
                response.ServerMessageList.AddRange(serverMessages);
                response.Response.LenderDocSubmissionReceiptResponseList.AddRange(receivedItems);
                response.Response.ResponseType = E_ResponseResponseType.VendorDoCSubmissionReceipt;
                OutputResponse(context, content, response);

            }
            catch (XmlException e)
            {
                Tools.LogError(content, e);
                OutputError(context, response, content, E_ServerMessageType.Other, "Invalid XML.");
            }

            
        }

        private void OutputError(HttpContext context, TitleVendorResponse response, string request, E_ServerMessageType messageType, string message)
        {
            response.RequestStatus = E_TitleVendorResponseRequestStatus.Error;
            response.ServerMessageList.Add(new ServerMessage()
            {
                Description = message,
                Type = messageType
            });

            OutputResponse(context, request, response);
        }

        private void OutputResponse(HttpContext context, string request, TitleVendorResponse response)
        {
            context.Response.ContentType = "text/xml";
            StringBuilder content = new StringBuilder("[TitleVendorRequest]\n");
            content.AppendFormat("{0}\n[TitleVendorRequest]\n", request);

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = Encoding.UTF8;

            using (XmlWriter writer = XmlTextWriter.Create(content))
            {
                response.WriteXml(writer);
            }
            Tools.LogInfo(content.ToString());
            using (XmlWriter writer = XmlWriter.Create(context.Response.OutputStream, settings))
            {
                response.WriteXml(writer);
            }
        }

        #endregion
    }
}
