﻿using LendersOffice.Admin;
using LendersOffice.ObjLib.LockPolicies;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using System.Collections;
using LendersOffice.Security;
using LendersOffice.ManualInvestorProductExpiration;
using System.Linq;
using LendersOffice.Migration;
using CommonProjectLib.Common.Lib;
using System.Threading;

namespace LendersOffice.Migrators // BE CAREFUL when exposing this library to users.
{
    public abstract class Migrator
    {
        /// <summary>
        /// This should bit should be checked for every non-private migratory method.
        /// 
        /// It checks if we're running the migration from LoAdmin (as an internal user)
        /// or from an executable (as a system user principal)
        /// 
        /// if the answer is no to those, then it says we can't run the migration.
        /// </summary>
        static bool CanPerformMigration
        {
            get 
            {
                var p = Thread.CurrentPrincipal;
                if (p == null)
                {
                    return false;
                }
                if (p is InternalUserPrincipal)
                {
                    return true;
                }
                if (p is SystemUserPrincipal)
                {
                    return true;
                }
                return false;
            }
        }

        protected static void ThrowErrorIfCantPerformMigration()
        {
            if (!CanPerformMigration)
            {
                throw new AccessDenied("you do not have access to run this migration.");
            }
        }
    }

    #region ( 105723 )
    public class LockPolicyAndPriceGroupMigratorFor105723 : Migrator
    {
        struct Constants
        {
           public static string DEFAULT_LOCK_POLICY_NM { get { return "Default"; } }
        }
        public static void MigrateBroker(Guid BrokerID)
        {
            ThrowErrorIfCantPerformMigration();
            // order matters.  first set up the default lock policy, then set the price groups to default to that lock policy.
            var defaultLPID = LockPolicyBrokerMigratorFor105723.MigrateBroker(BrokerID);
            BrokerPriceGroupMigratorFor105723.MigrateBroker(BrokerID);
            DeleteOldDefaultLockPolicy(BrokerID, defaultLPID); // why do a delete?
        }
        private static void DeleteOldDefaultLockPolicy(Guid brokerID, Guid defaultLPID)
        {
            foreach (var lp in LockPolicy.RetrieveAllForBroker(brokerID))
            {
                if( lp.LockPolicyId != defaultLPID)
                {
                    if (lp.PolicyNm == Constants.DEFAULT_LOCK_POLICY_NM)
                    {
                        LockPolicy.DeleteLockPolicy(brokerID, lp.LockPolicyId);
                    }
                }
            }
        }

        public class LockPolicyBrokerMigratorFor105723
        {
            /// <summary>
            /// returns the id of the default lock policy that is created.
            /// </summary>
            public static Guid MigrateBroker(Guid brokerID)
            {
                var broker = BrokerDB.RetrieveById(brokerID);

                var defaultlp = new LockPolicy(brokerID);

                defaultlp.PolicyNm = Constants.DEFAULT_LOCK_POLICY_NM;
                defaultlp.BrokerId = brokerID;

                defaultlp.DefaultLockDeskID = broker.DefaultLockDeskID;

                defaultlp.IsUsingRateSheetExpirationFeature = broker.UseRateSheetExpirationFeature;
                defaultlp.TimezoneForRsExpiration = broker.TimezoneForRsExpiration;
                defaultlp.LpeIsEnforceLockDeskHourForNormalUser = broker.LpeIsEnforceLockDeskHourForNormalUser;
                //defaultlp.LpeLockDeskWorkHourStartTime = broker.LpeLockDeskWorkHourStartTimeInLenderTimezone; // see opm 128858
                //defaultlp.LpeLockDeskWorkHourEndTime = broker.LpeLockDeskWorkHourEndTimeInLenderTimezone;
                defaultlp.LpeMinutesNeededToLockLoan = broker.LpeMinutesNeededToLockLoan;

                defaultlp.EnableAutoFloatDowns = false;
                defaultlp.EnableAutoLockExtensions = false;
                defaultlp.EnableAutoReLocks = false;


                foreach (DaySetting ds in defaultlp.LpeLockDeskDaySettings)
                {
                    if (ds.Day == DayOfWeek.Saturday || ds.Day == DayOfWeek.Sunday)
                    {
                        ds.IsOpenForBusiness = false;
                        ds.IsOpenForLocks = false;
                    }
                    else
                    {
                        ds.IsOpenForBusiness = true;
                        ds.IsOpenForLocks = true;
                        ds.StartHour = 4;
                        ds.StartMinute = 0;
                        ds.EndHour = 23;
                        ds.EndMinute = 59;
                    }
                }


                defaultlp.Save();

                #region ( Warning Messages )
                List<SqlParameter> warningMessageUpdateParameters = new List<SqlParameter>();
                warningMessageUpdateParameters.Add(new SqlParameter("@Broker", brokerID));
                warningMessageUpdateParameters.Add(new SqlParameter("@PolicyId", defaultlp.LockPolicyId));
                bool transferWarnings = false;

                SqlParameter[] parameters = {
                                                new SqlParameter("@Broker", broker.BrokerID)
                                            };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(broker.BrokerID, "getRSWarningMessageById", parameters))
                {
                    if (reader.Read())
                    {
                        transferWarnings = true;
                        warningMessageUpdateParameters.Add(new SqlParameter("@NormalUserExpiredMsg", (string)reader["NormalUserExpiredCustomMsg"]));
                        warningMessageUpdateParameters.Add(new SqlParameter("@LockDeskExpiredMsg", (string)reader["LockDeskExpiredCustomMsg"]));
                        warningMessageUpdateParameters.Add(new SqlParameter("@NormalUserCutOffMsg", (string)reader["NormalUserCutOffCustomMsg"]));
                        warningMessageUpdateParameters.Add(new SqlParameter("@LockDeskCutOffMsg", (string)reader["LockDeskCutOffCustomMsg"]));
                        warningMessageUpdateParameters.Add(new SqlParameter("@OutsideNormalHrMsg", (string)reader["OutsideNormalHrCustomMsg"]));
                        warningMessageUpdateParameters.Add(new SqlParameter("@OutsideClosureDayHrMsg", (string)reader["OutsideClosureDayHrCustomMsg"]));
                        warningMessageUpdateParameters.Add(new SqlParameter("@OutsideNormalHrAndPassInvestorCutOffMsg", (string)reader["OutsideNormalHrandPassInvestorCutOffCustomMsg"]));
                    }
                }
                if (transferWarnings)
                {
                    var procedure = "UpdateRSWarningMsgByBrokerPolicyId";
                    StoredProcedureHelper.ExecuteNonQuery(brokerID, procedure, 1, warningMessageUpdateParameters);
                }


                #endregion

                #region ( Disable Pricing )
                foreach (var item in InvestorProductManager.ListManualDisabledInvestorProductItem(brokerID))
                {
                    InvestorProductManager.AddForMigration(brokerID, defaultlp.LockPolicyId, item.InvestorName, item.ProductCode,
                        item.DisableStatus, item.MessagesToSubmittingUsers, item.IsHiddenFromResult, item.Notes, 
                        PrincipalFactory.CurrentPrincipal,
                        item.DisableEntryModifiedByEmployeeId, item.DisableEntryModifiedByUserName);
                }
                #endregion

                #region ( Holiday Closures )
                var brokerLockDeskParameters = new SqlParameter[]{
                    new SqlParameter("@BrokerId", brokerID)
                };

                using(var reader = StoredProcedureHelper.ExecuteReader(brokerID, "ListLockDeskClosureByBrokerId", brokerLockDeskParameters))
                {

                    while(reader.Read())
                    {
                        var createParameters = new SqlParameter[]{
                              new SqlParameter("@PolicyId", defaultlp.LockPolicyId)
                            , new SqlParameter("@BrokerId", brokerID)
                            , new SqlParameter("@ClosureDate", reader["ClosureDate"])
                            , new SqlParameter("@LockDeskOpenTime", reader["LockDeskOpenTime"])
                            , new SqlParameter("@LockDeskCloseTime", reader["LockDeskCloseTime"])
                            , new SqlParameter("@IsClosedAllDay", reader["IsClosedAllDay"])
                            , new SqlParameter("@IsClosedForBusiness", reader["IsClosedAllDay"])
                        };
                        var result = StoredProcedureHelper.ExecuteNonQuery(brokerID, "CreateLockDeskClosureForPolicyId", 1, createParameters);
                    }
                }
                #endregion

                broker.DefaultLockPolicyID = defaultlp.LockPolicyId;
                broker.Save();

                return defaultlp.LockPolicyId;
            }
        }

        private class BrokerPriceGroupMigratorFor105723
        {
            public static void MigrateBroker(Guid brokerID)
            {
                var broker = BrokerDB.RetrieveById(brokerID);

                foreach (var pg in GetPriceGroupIDsForBroker(brokerID))
                {
                    MigratePriceGroup(broker, pg);
                }
            }

            private static IEnumerable<Guid> GetPriceGroupIDsForBroker(Guid brokerID)
            {
                var priceGroupIDs = new List<Guid>();
                SqlParameter[] parameters = { new SqlParameter("@BrokerId", brokerID) };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerID, "ListPricingGroupByBrokerId", parameters))
                {
                    while (reader.Read())
                    {
                        priceGroupIDs.Add((Guid)reader["LpePriceGroupId"]);
                    }
                }
                return priceGroupIDs;
            }

            private static void MigratePriceGroup(BrokerDB broker, Guid priceGroupID)
            {
                SqlParameter[] parameters = null;
                string storedProcedure = "";

                storedProcedure = "UpdatePricingGroupById";
                parameters = new SqlParameter[] {
                                                new SqlParameter("@LpePriceGroupId", priceGroupID),
                                                new SqlParameter("@BrokerId", broker.BrokerID),
                                                new SqlParameter("@LockPolicyID", broker.DefaultLockPolicyID),
                                                new SqlParameter("@DisplayPmlFeeIn100Format", broker.DisplayPmlFeeIn100Format),
                                                new SqlParameter("@IsRoundUpLpeFee", broker.RoundUpLpeFee),
                                                new SqlParameter("@RoundUpLpeFeeToInterval", broker.RoundUpLpeFeeInterval),
                                                new SqlParameter("@IsAlwaysDisplayExactParRateOption", broker.IsAlwaysDisplayExactParRateOption)
                                            };

                StoredProcedureHelper.ExecuteNonQuery(broker.BrokerID, storedProcedure, 0, parameters);

            }
        }
    }
    
    #endregion 
}