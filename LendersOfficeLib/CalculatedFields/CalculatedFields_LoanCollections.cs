﻿namespace LendersOffice
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Text;
    using System.Xml;
    using Common;
    using DataAccess;
    using global::LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Utils;

    /// <summary>
    /// Container for classes that hold calculations for collections related to a loan file.
    /// </summary>
    public static partial class CalculatedFields
    {
        /// <summary>
        /// Calculated fields for the asset classes.
        /// </summary>
        public static class Asset
        {
            /// <summary>
            /// Returns the display string for the asset type.
            /// </summary>
            /// <param name="assetT">The asset type.</param>
            /// <param name="otherDesc">The display string for the other description.</param>
            /// <returns>The display string for the asset type.</returns>
            public static string DisplayStringForAssetT(E_AssetRegularT assetT, string otherDesc)
            {
                switch (assetT)
                {
                    case E_AssetRegularT.Auto: return "Auto";
                    case E_AssetRegularT.Bonds: return "Bonds";
                    case E_AssetRegularT.Checking: return "Checking";
                    case E_AssetRegularT.GiftFunds: return "Gift Funds";
                    case E_AssetRegularT.GiftEquity: return "Gift Of Equity";
                    case E_AssetRegularT.OtherIlliquidAsset:
                        otherDesc = otherDesc.TrimWhitespaceAndBOM();
                        if (otherDesc == string.Empty)
                        {
                            return "Other Illiquid Asset";
                        }

                        return otherDesc;
                    case E_AssetRegularT.OtherLiquidAsset:
                        otherDesc = otherDesc.TrimWhitespaceAndBOM();
                        if (otherDesc == string.Empty)
                        {
                            return "Other Liquid Asset";
                        }

                        return otherDesc;
                    case E_AssetRegularT.OtherPurchaseCredit:
                        if (string.IsNullOrEmpty(otherDesc.Trim()))
                        {
                            return "Other Purchase Credit";
                        }

                        return otherDesc;
                    case E_AssetRegularT.Savings:
                        return "Savings";
                    case E_AssetRegularT.Stocks:
                        return "Stocks";
                    case E_AssetRegularT.PendingNetSaleProceedsFromRealEstateAssets:
                        return "Pending Net Sale Proceeds";
                    case E_AssetRegularT.CertificateOfDeposit:
                        return "Certificate Of Deposit";
                    case E_AssetRegularT.MoneyMarketFund:
                        return "Money Market Fund";
                    case E_AssetRegularT.MutualFunds:
                        return "Mutual Funds";
                    case E_AssetRegularT.SecuredBorrowedFundsNotDeposit:
                        return "Secured Borrowed Funds";
                    case E_AssetRegularT.BridgeLoanNotDeposited:
                        return "Bridge Loan";
                    case E_AssetRegularT.TrustFunds:
                        return "Trust Funds";
                    case E_AssetRegularT.StockOptions:
                        return "Stock Options";
                    case E_AssetRegularT.EmployerAssistance:
                        return "Employer Assistance";
                    case E_AssetRegularT.IndividualDevelopmentAccount:
                        return "Individual Development Account";
                    case E_AssetRegularT.ProceedsFromSaleOfNonRealEstateAsset:
                        return "Pending Net Sale Proceeds Non-Real Estate";
                    case E_AssetRegularT.ProceedsFromUnsecuredLoan:
                        return "Unsecured Borrowed Funds";
                    case E_AssetRegularT.Grant:
                        return "Grant";
                    case E_AssetRegularT.LeasePurchaseCredit:
                        return "Rent Credit";
                    case E_AssetRegularT.SweatEquity:
                        return "Sweat Equity";
                    case E_AssetRegularT.TradeEquityFromPropertySwap:
                        return "Trade Equity";
                    default:
                        throw new UnhandledEnumException(assetT);
                }
            }

            /// <summary>
            /// Calculate aggregate data for the assigned to properties on the containing application.
            /// </summary>
            /// <param name="items">A list of assets.</param>
            /// <param name="excludeCashDeposit">Whether to exclude cash deposit assets calculation in liquid asset total.</param>
            /// <returns>The calculated aggregate fields.</returns>
            public static AggregateData CalculateAggregateData(IEnumerable<DataAccess.IAsset> items, bool excludeCashDeposit)
            {
                return AggregateData.Create(items, excludeCashDeposit);
            }

            /// <summary>
            /// AggregateData for an asset collection to be set back to an app.
            /// </summary>
            public class AggregateData
            {
                /// <summary>
                /// Prevents a default instance of the <see cref="AggregateData" /> class from being created.
                /// </summary>
                private AggregateData()
                {
                }

                /// <summary>
                /// Gets the total liquid asset value.
                /// </summary>
                public Money AsstLiqTot { get; private set; }

                /// <summary>
                /// Gets the total non-real estate solid asset value.
                /// </summary>
                public Money AsstNonReSolidTot { get; private set; }

                /// <summary>
                /// Calculate and return the aggregate data for a collection of assets.
                /// </summary>
                /// <param name="items">A list of assets.</param>
                /// <param name="includeCashDeposit">Whether to include cash deposit in asset totals.</param>
                /// <returns>The calculated aggregate fields.</returns>
                internal static AggregateData Create(IEnumerable<DataAccess.IAsset> items, bool includeCashDeposit)
                {
                    var data = new AggregateData();

                    decimal asstLiqTot = 0;
                    decimal asstNonReSolidTot = 0;

                    foreach (var item in items)
                    {
                        item.PrepareToFlush();
                        if (item.IsCreditAtClosing)
                        {
                            continue;
                        }

                        if (item.IsLiquidAsset)
                        {
                            if (item.AssetT == E_AssetT.CashDeposit)
                            {
                                // If it's migrated, then we need to always exclude the cash deposit.
                                if (includeCashDeposit)
                                {
                                    asstLiqTot += item.Val;
                                }
                            }
                            else
                            {
                                asstLiqTot += item.Val;
                            }
                        }
                        else
                        {
                            asstNonReSolidTot += item.Val;
                        }
                    }

                    data.AsstLiqTot = asstLiqTot;
                    data.AsstNonReSolidTot = asstNonReSolidTot;
                    return data;
                }
            }
        }

        /// <summary>
        /// Calculated fields for the EmploymentRecord/EmploymentFields classes.
        /// </summary>
        public static class EmploymentRecord
        {
            /// <summary>
            /// Calculate the remaining months after whole years have been removed from the period between the start and end dates.
            /// </summary>
            /// <remarks>
            /// This is the code used by shims because it is basically a copy over of the legacy code.  The
            /// new entities use TimeUtils (see below) as they are written to work directly with the 
            /// semantic data types.
            /// </remarks>
            /// <param name="start">The date beginning the period of interest.</param>
            /// <param name="end">The date ending the period of interest.</param>
            /// <returns>The calculated months portion of the period of interest.</returns>
            public static Count<UnitType.Month> MonthPortionOfEmployment(UnzonedDate start, UnzonedDate end)
            {
                YearMonthParser ym = new YearMonthParser();
                ym.Parse(start.Date, end.Date);
                return Count<UnitType.Month>.Create(ym.NumberOfMonths).Value;
            }

            /// <summary>
            /// Calculate the whole years contained within the period between the start and end dates.
            /// </summary>
            /// <remarks>
            /// This is the code used by shims because it is basically a copy over of the legacy code.  The
            /// new entities use TimeUtils (see below) as they are written to work directly with the 
            /// semantic data types.
            /// </remarks>
            /// <param name="start">The date beginning the period of interest.</param>
            /// <param name="end">The date ending the period of interest.</param>
            /// <returns>The calculated years portion of the period of interest.</returns>
            public static Count<UnitType.Year> YearPortionOfEmployment(UnzonedDate start, UnzonedDate end)
            {
                YearMonthParser ym = new YearMonthParser();
                ym.Parse(start.Date, end.Date);
                return Count<UnitType.Year>.Create(ym.NumberOfYears).Value;
            }

            /// <summary>
            /// Calculate the duration between two dates as a quantity of years.
            /// </summary>
            /// <param name="beginDate">The begin date from which the number of years is calculated.</param>
            /// <param name="endDate">The end date for the time period. If you pass null, it will default to the current date.</param>
            /// <returns>The calculated duration in units of years.</returns>
            public static Quantity<UnitType.Year>? CalcYearDuration(UnzonedDate? beginDate, UnzonedDate? endDate)
            {
                return TimeUtils.CalculateYearDuration(beginDate, endDate);
            }

            /// <summary>
            /// Calculate the number of whole years that have elapsed between the begin date and end date.
            /// </summary>
            /// <param name="beginDate">The begin date from which the number of years is calculated.</param>
            /// <param name="endDate">The end date for the time period. If you pass null, it will default to the current date.</param>
            /// <returns>The calculated number of years.</returns>
            public static Count<UnitType.Year>? CalcWholeYears(UnzonedDate? beginDate, UnzonedDate? endDate)
            {
                return TimeUtils.CalcWholeYears(beginDate, endDate);
            }

            /// <summary>
            /// Calculate the number of whole months elapsed between the begin and end dates.
            /// </summary>
            /// <param name="beginDate">The begin date from which the number of years is calculated.</param>
            /// <param name="endDate">The end date for the time period. If you pass null, it will default to the current date.</param>
            /// <returns>The calculated number of whole months.</returns>
            public static Count<UnitType.Month>? CalcWholeMonths(UnzonedDate? beginDate, UnzonedDate? endDate)
            {
                return TimeUtils.CalcWholeMonths(beginDate, endDate);
            }

            /// <summary>
            /// Calculate the number of whole months after the whole years have been stripped off the period between the begin and end dates.
            /// </summary>
            /// <param name="beginDate">The begin date from which the number of years is calculated.</param>
            /// <param name="endDate">The end date for the time period. If you pass null, it will default to the current date.</param>
            /// <returns>The calculated number of remaining whole months.</returns>
            public static Count<UnitType.Month>? CalcRemainderWholeMonths(UnzonedDate? beginDate, UnzonedDate? endDate)
            {
                var years = CalcWholeYears(beginDate, endDate);
                if (years != null)
                {
                    var advancedBegin = beginDate.Value.Date.AddYears(years.Value.Value);
                    var advancedUnzoned = UnzonedDate.Create(advancedBegin);
                    return TimeUtils.CalcWholeMonths(advancedUnzoned, endDate);
                }
                else
                {
                    return null;
                }
            }

            /// <summary>
            /// Calculate whether the employment history is valid or has problems.
            /// </summary>
            /// <param name="records">The set of employment records that compose the employment history.</param>
            /// <returns>True if the records provide a valid history, false otherwise.</returns>
            public static bool IsEmploymentHistoryValid(ISubcollection records)
            {
                var specialDetector = new EmploymentSpecialRecords();

                foreach (DataAccess.IEmploymentRecord employmentRecord in records)
                {
                    DateTime startDate, endDate;

                    if (!specialDetector.IsSpecial(employmentRecord))
                    {
                        IRegularEmploymentRecord pastEmployment = (IRegularEmploymentRecord)employmentRecord;

                        if (!DateTime.TryParse(pastEmployment.EmplmtStartD_rep, out startDate))
                        {
                            return false;
                        }

                        if (!pastEmployment.IsCurrent)
                        {
                            if (!DateTime.TryParse(pastEmployment.EmplmtEndD_rep, out endDate))
                            {
                                return false;
                            }

                            if (startDate > endDate)
                            {
                                return false;
                            }
                        }
                    }
                    else
                    {
                        IPrimaryEmploymentRecord currentEmployment = (IPrimaryEmploymentRecord)employmentRecord;

                        // UI does not allow entering negative, but it would mess up keyword evaluation,
                        // and import might pull in bad data.
                        if (currentEmployment.EmplmtLenInYrs < 0 || currentEmployment.EmplmtLenInMonths < 0)
                        {
                            return false;
                        }

                        if (currentEmployment.ProfLen < 0)
                        {
                            return false;
                        }
                    }
                }

                return true;
            }

            /// <summary>
            /// Returns the length of the longest continuous employment gap (in months, rounding down)
            /// within the period specified in the parameter.
            /// </summary>
            /// <param name="months">The number of months of interest.</param>
            /// <param name="jobEntries">The set of employment records used in the calculation.</param>
            /// <returns>The length of the longest continuous employment gap.</returns>
            public static int GetMaxEmploymentGapWithin(int months, ISubcollection jobEntries)
            {
                // 10.24.09 mf. 
                // 1. Collect all jobs' start and end dates.
                // 2. Flatten all overlapping jobs to a single timeline
                // 3. Use this timeline to determine the largest gaps.
                DateTime now = DateTime.Now;
                DateTime intervalStart = now.AddMonths(0 - months);
                const int IStart = 0;
                const int IEnd = 1;

                ArrayList jobs = new ArrayList(jobEntries.Count);
                ArrayList entriesInInterval = new ArrayList();

                var specialDetector = new EmploymentSpecialRecords();

                // 1. Build the list of employment entries from the employment history
                foreach (DataAccess.IEmploymentRecord employmentRecord in jobEntries)
                {
                    DateTime endDate;
                    DateTime startDate;

                    if (!specialDetector.IsSpecial(employmentRecord))
                    {
                        // This is a standard employment record that can contain start and end dates.
                        IRegularEmploymentRecord pastEmployment = (IRegularEmploymentRecord)employmentRecord;
                        if (!DateTime.TryParse(pastEmployment.EmplmtStartD_rep, out startDate))
                        {
                            continue; // Could not parse start date
                        }

                        if (pastEmployment.IsCurrent)
                        {
                            // OPM 48903 - To support multiple jobs, jobs in employment history can be marked as 
                            // current.  We treat them like primary employment.
                            endDate = now;
                        }
                        else
                        {
                            if (!DateTime.TryParse(pastEmployment.EmplmtEndD_rep, out endDate))
                            {
                                continue; // Could not parse end date
                            }
                        }

                        // If a job's end date falls in the period of interest, we need it.
                        // Note that this job may have started before the period of interest.  Thats fine.
                        if (endDate >= intervalStart)
                        {
                            jobs.Add(new DateTime[] { startDate, endDate });
                        }
                    }
                    else
                    {
                        // This is a primary record that only contains years on the job.
                        // We use the years on the job to determine start date.
                        IPrimaryEmploymentRecord currentEmployment = (IPrimaryEmploymentRecord)employmentRecord;
                        startDate = now.AddMonths(0 - (currentEmployment.EmplmtLenInMonths + (currentEmployment.EmplmtLenInYrs * 12)));
                        endDate = now;

                        jobs.Add(new DateTime[] { startDate, endDate });
                    }
                }

                if (jobs.Count == 0)
                {
                    return months;  // Was never employed
                }

                // 2. Merge all records into one flat timeline.
                // Basically, we don't care about dates that are contained by
                // a different job.
                List<DateTime> finalList = new List<DateTime>();
                foreach (DateTime[] job in jobs)
                {
                    bool useStart = true;
                    bool useFinish = true;

                    // Determine if start or end date is in the middle of another existing job.
                    foreach (DateTime[] compareJob in jobs)
                    {
                        if (job == compareJob)
                        {
                            continue;
                        }

                        if (job[IStart] > compareJob[IStart] && job[IStart] < compareJob[IEnd])
                        {
                            useStart = false; // This start is in the middle of another job.
                        }

                        if (job[IEnd] > compareJob[IStart] && job[IEnd] < compareJob[IEnd])
                        {
                            useFinish = false; // This end is in the middle of another job.
                        }
                    }

                    useStart = useStart && !finalList.Contains(job[IStart]);
                    useFinish = useFinish && !finalList.Contains(job[IEnd]);

                    if (useStart)
                    {
                        finalList.Add(job[IStart]);
                    }

                    if (useFinish)
                    {
                        finalList.Add(job[IEnd]);
                    }
                }

                // Here we have the flattened list.
                // If data was valid, sorting by date should
                // produce the single employment timeline.
                finalList.Sort();

                if (finalList.Count > 0 && finalList[0] > intervalStart)
                {
                    // This borrower was unemployed at the start of the interval, simulate an ended job to create the gap.
                    // This is a bit of a hack.
                    finalList.Insert(0, intervalStart);
                    finalList.Insert(0, intervalStart - TimeSpan.FromDays(1));
                }

                bool isStart = true;
                DateTime previousEnd = DateTime.MinValue;
                TimeSpan largestGap = TimeSpan.Zero;
                DateTime largestGapBegins = DateTime.MinValue;

                foreach (DateTime dateOfInterest in finalList)
                {
                    if (isStart && previousEnd != DateTime.MinValue)
                    {
                        // A new job starts.  Measure this gap
                        TimeSpan gap = dateOfInterest - previousEnd;
                        if (gap > largestGap)
                        {
                            largestGap = gap;
                            largestGapBegins = previousEnd;
                        }
                    }

                    if (!isStart)
                    {
                        previousEnd = dateOfInterest; // This is an end.
                    }

                    isStart = !isStart;
                }

                DateTime largestGapEnds = largestGapBegins + largestGap;

                // Note this rounds down the months.
                return (12 * (largestGapEnds.Year - largestGapBegins.Year))
                    + largestGapEnds.Month - largestGapBegins.Month;
            }

            /// <summary>
            /// Verify that all the employment records in the employment history have valid dates.
            /// </summary>
            /// <param name="userMessage">StringBuilder instance where an error message is assembled.</param>
            /// <param name="borrowerName">The name of the borrower, which will be included within the error message.</param>
            /// <param name="records">The set of employment records of interest.</param>
            /// <returns>True if all the employment records have valid dates, false otherwise.</returns>
            public static bool ValidateEmploymentHistoryDates(StringBuilder userMessage, string borrowerName, ISubcollection records)
            {
                bool isValid = true;
                foreach (DataAccess.IEmploymentRecord employmentRecord in records)
                {
                    DateTime startDate, endDate;

                    var specialDetector = new EmploymentSpecialRecords();

                    if (!specialDetector.IsSpecial(employmentRecord))
                    {
                        IRegularEmploymentRecord pastEmployment = (IRegularEmploymentRecord)employmentRecord;
                        string employer = pastEmployment.EmplrNm != string.Empty ?
                            pastEmployment.EmplrNm : "employer " + pastEmployment.RowPos.ToString();

                        if (!DateTime.TryParse(pastEmployment.EmplmtStartD_rep, out startDate) ||
                             !DateTime.TryParse(pastEmployment.EmplmtEndD_rep, out endDate))
                        {
                            userMessage.AppendLine(borrowerName + ": Could not determine employment term for " + employer + ". Please check employment dates.");
                            isValid = false;
                            continue;
                        }

                        if (startDate > endDate)
                        {
                            userMessage.AppendLine(borrowerName + ": Invalid employment term for " + employer + ". Start date of " + startDate.ToShortDateString() + " occurs after end date of " + endDate.ToShortDateString() + ".");
                            isValid = false;
                            continue;
                        }
                    }
                    else
                    {
                        IPrimaryEmploymentRecord currentEmployment = (IPrimaryEmploymentRecord)employmentRecord;
                        string employer = currentEmployment.EmplrNm != string.Empty ?
                            currentEmployment.EmplrNm : "Primary employer";

                        // UI does not allow entering negative, but it would mess up keyword evaluation,
                        // and import might pull in bad data.
                        if (currentEmployment.EmplmtLenInYrs < 0 || currentEmployment.EmplmtLenInMonths < 0)
                        {
                            userMessage.AppendLine(borrowerName + ": Could not determine employment term for " + employer + " Please check years on Job.");
                            isValid = false;
                            continue;
                        }

                        if (currentEmployment.ProfLen < 0)
                        {
                            userMessage.AppendLine(borrowerName + ": Could not determine years in profession term for " + employer + ".");
                            isValid = false;
                            continue;
                        }
                    }
                }

                return isValid;
            }
        }

        /// <summary>
        /// Calculated fields for the RealProperty/Reo classes.
        /// </summary>
        public static class RealProperty
        {
            /// <summary>
            /// Calculates the net rental income.
            /// </summary>
            /// <param name="netRentIncLckd">A value indicating whether the net rental income is locked.</param>
            /// <param name="netRentInc">The net rental income value that has been manually set.</param>
            /// <param name="statT">The status of the real property.</param>
            /// <param name="isForceCalcNetRentalInc">A value indicating whether net rental income should be calculated for Residence and Pending Sale real properties.</param>
            /// <param name="occR">The occupancy rate.</param>
            /// <param name="grossRentI">The gross rent income.</param>
            /// <param name="housingExpenses">The housing expenses.</param>
            /// <param name="mtgPmt">The mortgage payment.</param>
            /// <returns>The calculated net rental income. May return null if needed values are null.</returns>
            public static decimal NetRentInc(
                bool netRentIncLckd,
                decimal netRentInc,
                E_ReoStatusT statT,
                bool isForceCalcNetRentalInc,
                decimal occR,
                decimal grossRentI,
                decimal housingExpenses,
                decimal mtgPmt)
            {
                if (netRentIncLckd)
                {
                    return netRentInc;
                }
                else
                {
                    return (decimal)NetRentInc(
                        statT,
                        isForceCalcNetRentalInc,
                        Percentage.Create(occR),
                        Money.Create(grossRentI),
                        Money.Create(housingExpenses),
                        Money.Create(mtgPmt));
                }
            }

            /// <summary>
            /// Calculates the net rental income.
            /// </summary>
            /// <param name="status">The status of the real property.</param>
            /// <param name="isForceCalcNetRentalInc">A value indicating whether net rental income should be calculated for Residence and Pending Sale real properties.</param>
            /// <param name="occR">The occupancy rate.</param>
            /// <param name="grossRentInc">The gross rent income.</param>
            /// <param name="housingExp">The housing expenses.</param>
            /// <param name="mtgPmt">The mortgage payment.</param>
            /// <returns>The calculated net rental income. May return null if needed values are null.</returns>
            public static Money? NetRentInc(
                E_ReoStatusT? status,
                bool? isForceCalcNetRentalInc,
                Percentage? occR,
                Money? grossRentInc,
                Money? housingExp,
                Money? mtgPmt)
            {
                if (status == null)
                {
                    return null;
                }

                bool? calc;

                switch (status)
                {
                    case E_ReoStatusT.Rental:
                        calc = true;
                        break;
                    case E_ReoStatusT.Residence:
                    case E_ReoStatusT.PendingSale:
                        calc = isForceCalcNetRentalInc;
                        break;
                    case E_ReoStatusT.Sale:
                        calc = false;
                        break;
                    default:
                        throw new UnhandledEnumException(status);
                }

                if (calc == null)
                {
                    return null;
                }
                else if (calc.Value)
                {
                    if (occR == null || grossRentInc == null || housingExp == null || mtgPmt == null)
                    {
                        return null;
                    }

                    return ((decimal)occR.Value * (decimal)grossRentInc.Value / 100) - housingExp - mtgPmt;
                }
                else
                {
                    return 0;
                }
            }

            /// <summary>
            /// Gets the market value minus the mortgage amount.
            /// </summary>
            /// <param name="marketValue">The market value of the property.</param>
            /// <param name="mortgageAmount">The mortgage amount for the property.</param>
            /// <returns>The net value of the property.</returns>
            public static Money? NetValue(Money? marketValue, Money? mortgageAmount)
            {
                if (marketValue == null || mortgageAmount == null)
                {
                    return null;
                }

                return marketValue - mortgageAmount;
            }

            /// <summary>
            /// Gets a value indicating whether the IsForceCalNetRentalI value is visible to the GUI.
            /// </summary>
            /// <param name="statusCode">The status code for a real property.</param>
            /// <returns>True if IsForceCalNetRentalI value is visible to the GUI, false otherwise.</returns>
            public static bool IsForceCalcNetRentalIVisible(string statusCode)
            {
                return statusCode == "PS" || statusCode == string.Empty;
            }

            /// <summary>
            /// Gets a value indicating whether the occupancy rate is readonly and cannot be modified.
            /// </summary>
            /// <param name="status">The status of the real property.</param>
            /// <returns>True if the occupancy rate is readonly, false otherwise.</returns>
            public static bool OccR_repReadOnly(E_ReoStatusT status)
            {
                return status != E_ReoStatusT.Rental;
            }

            /// <summary>
            /// Convert the status enumeration to a status code.
            /// </summary>
            /// <param name="status">The status enumeration value.</param>
            /// <returns>The status code associated with the status enumeration value.</returns>
            public static string StatusCode(E_ReoStatusT status)
            {
                switch (status)
                {
                    case E_ReoStatusT.PendingSale:
                        return "PS";
                    case E_ReoStatusT.Rental:
                        return "R";
                    case E_ReoStatusT.Residence:
                        return string.Empty;
                    case E_ReoStatusT.Sale:
                        return "S";
                    default:
                        throw new UnhandledEnumException(status);
                }
            }

            /// <summary>
            /// Convert a status code to the associated status enumeration value.
            /// </summary>
            /// <param name="code">The status code.</param>
            /// <returns>The status enumeration value associated with the status code.</returns>
            public static E_ReoStatusT StatusFromCode(string code)
            {
                switch (code.ToUpper())
                {
                    case "PS":
                        return E_ReoStatusT.PendingSale;
                    case "R":
                        return E_ReoStatusT.Rental;
                    case "":
                        return E_ReoStatusT.Residence;
                    case "S":
                        return E_ReoStatusT.Sale;
                    default:
                        throw new CBaseException(ErrorMessages.Generic, "Unrecognized REO status code string value.");
                }
            }

            /// <summary>
            /// Convert the type enumeration to a type code.
            /// </summary>
            /// <param name="type">The type enumeration value.</param>
            /// <returns>The type code associated with the type enumeration value.</returns>
            public static string TypeCode(E_ReoTypeT type)
            {
                switch (type)
                {
                    case E_ReoTypeT._2_4Plx:
                        return "2-4PLX";
                    case E_ReoTypeT.ComNR:
                        return "COM-NR";
                    case E_ReoTypeT.ComR:
                        return "COM-R";
                    case E_ReoTypeT.Condo:
                        return "CONDO";
                    case E_ReoTypeT.Coop:
                        return "COOP";
                    case E_ReoTypeT.Farm:
                        return "FARM";
                    case E_ReoTypeT.Land:
                        return "LAND";
                    case E_ReoTypeT.LeaveBlank:
                        return string.Empty;
                    case E_ReoTypeT.Mixed:
                        return "MIXED";
                    case E_ReoTypeT.Mobil:
                        return "MOBIL";
                    case E_ReoTypeT.Multi:
                        return "MULTI";
                    case E_ReoTypeT.Other:
                        return "OTHER";
                    case E_ReoTypeT.SFR:
                        return "SFR";
                    case E_ReoTypeT.Town:
                        return "TOWN";
                    default:
                        throw new UnhandledEnumException(type);
                }
            }

            /// <summary>
            /// Convert a type code to the associated type enumeration value.
            /// </summary>
            /// <param name="code">The type code.</param>
            /// <returns>The type enumeration value associated with the type code.</returns>
            public static E_ReoTypeT TypeFromCode(string code)
            {
                switch (code.ToUpper())
                {
                    case "":
                        return E_ReoTypeT.LeaveBlank;
                    case "2-4PLX":
                    case "4PLX":
                        return E_ReoTypeT._2_4Plx;
                    case "COM-NR":
                        return E_ReoTypeT.ComNR;
                    case "COM-R":
                        return E_ReoTypeT.ComR;
                    case "CONDO":
                        return E_ReoTypeT.Condo;
                    case "COOP":
                        return E_ReoTypeT.Coop;
                    case "FARM":
                        return E_ReoTypeT.Farm;
                    case "LAND":
                        return E_ReoTypeT.Land;
                    case "MIXED":
                        return E_ReoTypeT.Mixed;
                    case "MOBIL":
                        return E_ReoTypeT.Mobil;
                    case "MULTI":
                        return E_ReoTypeT.Multi;
                    case "SFR":
                        return E_ReoTypeT.SFR;
                    case "TOWN":
                        return E_ReoTypeT.Town;
                    case "OTHER":
                        return E_ReoTypeT.Other;
                    default:
                        throw new CBaseException(ErrorMessages.Generic, "Unrecognized REO type string value.");
                }
            }

            /// <summary>
            /// ReCollection.Flush() contains calculations of aggregate data that gets assigned to properties on the containing application.
            /// This method performs the aggregate calculations for any shim.
            /// </summary>
            /// <remarks>
            /// Left undone is the code that pushes this data to the containing application for the new collections.
            /// </remarks>
            /// <param name="items">A list of real properties.</param>
            /// <param name="isPrimaryResidenceForApplication">Flag pulled from the containing application.</param>
            /// <returns>The calculated aggregate fields.</returns>
            public static AggregateData CalculateAggregateData(IEnumerable<IRealEstateOwned> items, bool isPrimaryResidenceForApplication)
            {
                return AggregateData.Create(items, isPrimaryResidenceForApplication);
            }

            /// <summary>
            /// Container for aggregate fields calculated for a collection of real properties.
            /// </summary>
            public class AggregateData
            {
                /// <summary>
                /// Prevents a default instance of the <see cref="AggregateData" /> class from being created.
                /// </summary>
                private AggregateData()
                {
                }

                /// <summary>
                /// Gets the total value.
                /// </summary>
                public Money ReTotVal { get; private set; }

                /// <summary>
                /// Gets the total mortgage amount.
                /// </summary>
                public Money ReTotMAmt { get; private set; }

                /// <summary>
                /// Gets the total gross rental income.
                /// </summary>
                public Money ReTotGrossRentI { get; private set; }

                /// <summary>
                /// Gets the total mortgage payments.
                /// </summary>
                public Money ReTotMPmt { get; private set; }

                /// <summary>
                /// Gets the total housing expenses.
                /// </summary>
                public Money ReTotHExp { get; private set; }

                /// <summary>
                /// Gets the total net rental income.
                /// </summary>
                public Money ReNetRentI { get; private set; }

                /// <summary>
                /// Gets the total net rental income for the borrower.
                /// </summary>
                public Money ReBNetRentI { get; private set; }

                /// <summary>
                /// Gets the total net rental income for the coborrower.
                /// </summary>
                public Money ReCNetRentI { get; private set; }

                /// <summary>
                /// Gets the total rental income from retained properties for the borrower.
                /// </summary>
                public Money ReBRetainedRentI { get; private set; }

                /// <summary>
                /// Gets the total rental income from retained properties for the coborrower.
                /// </summary>
                public Money ReCRetainedRentI { get; private set; }

                /// <summary>
                /// Gets the total income from the subject property.
                /// </summary>
                public Money SubjectPropI { get; private set; }

                /// <summary>
                /// Gets a value indicating whether there is a subject property.
                /// </summary>
                public bool SubjectPropFound { get; private set; }

                /// <summary>
                /// Calculate and return the aggregate data for a collection of real properties.
                /// </summary>
                /// <param name="items">A list of real properties.</param>
                /// <param name="isPrimaryResidenceForApplication">Flag pulled from the containing application.</param>
                /// <returns>The calculated aggregate fields.</returns>
                internal static AggregateData Create(IEnumerable<IRealEstateOwned> items, bool isPrimaryResidenceForApplication)
                {
                    var data = new AggregateData();

                    foreach (var fields in items)
                    {
                        // OPM 143988: DT - Don't include sold REOs in the totals. Subject property should
                        //                  never be a sold REO, I'm not trying to catch that situation here,
                        //                  so numbers will be weird in that case. If this causes problems,
                        //                  prevent that invalid situation upstream instead of trying to
                        //                  handle it here.
                        if (fields.StatT != E_ReoStatusT.Sale)
                        {
                            data.ReTotVal += fields.Val;
                            data.ReTotMAmt += fields.MAmt;
                            data.ReTotGrossRentI += fields.GrossRentI;
                            data.ReTotMPmt += fields.MPmt;
                            data.ReTotHExp += fields.HExp;
                            data.ReNetRentI += fields.NetRentI;

                            if (fields.StatT == E_ReoStatusT.Rental)
                            {
                                // 174375.
                                if (fields.ReOwnerT == E_ReOwnerT.CoBorrower && !fields.IsSubjectProp)
                                {
                                    data.ReCNetRentI += fields.NetRentI;
                                }
                                else
                                {
                                    data.ReBNetRentI += fields.NetRentI;
                                }
                            }
                        }

                        if (!data.SubjectPropFound && fields.IsSubjectProp)
                        {
                            data.SubjectPropFound = true;
                            data.SubjectPropI = fields.NetRentI;
                        }

                        // OPM 174375. Retained.
                        // OPM 179264: DT - There are already fields in the system to handle the borrower's housing
                        //                  expenses from the subject property, or the borrower's primary residence
                        //                  if they are not occupying the subject property. In either of these cases,
                        //                  do not include the negative cash-flow for those REO records.
                        if (fields.StatT == E_ReoStatusT.Residence && !(fields.IsSubjectProp || (fields.IsPrimaryResidence && !isPrimaryResidenceForApplication)))
                        {
                            if (fields.ReOwnerT == E_ReOwnerT.CoBorrower)
                            {
                                data.ReCRetainedRentI += fields.NetRentI;
                            }
                            else
                            {
                                data.ReBRetainedRentI += fields.NetRentI;
                            }
                        }
                    }

                    // Taking out the subject property investment income because REO for subject prop doesn't count
                    // directly into income. Users have to go to the subject property rental income page and enter
                    // the income so it can be compared against the proposed payments that are calculated from the 
                    // data of the loan.
                    data.ReBNetRentI -= data.SubjectPropI;

                    return data;
                }
            }
        }

        /// <summary>
        /// Calculation-related stuff for the liabiilty.
        /// </summary>
        public static class Liability
        {
            /// <summary>
            /// Gets the value for <see cref="Liability.PmtRemainMons_rep1003"/>.
            /// </summary>
            /// <param name="pmt_repPoint">The value of <see cref="Liability.Pmt_repPoint"/>.</param>
            /// <param name="remainMons_rep">The value of <see cref="Liability.RemainMons_rep"/>.</param>
            /// <param name="willBePdOff">The value of <see cref="Liability.WillBePdOff"/>.</param>
            /// <returns>The value for <see cref="Liability.PmtRemainMons_rep1003"/>.</returns>
            public static string PmtRemainMons_rep1003(string pmt_repPoint, string remainMons_rep, bool willBePdOff)
            {
                string str = pmt_repPoint + " / " + remainMons_rep;

                if (willBePdOff)
                {
                    str = " * " + str;
                }

                return str;
            }

            /// <summary>
            /// Gets the value for <see cref="Liability.Pmt_repPoint"/>.
            /// </summary>
            /// <param name="notUsedInRatio">The value of <see cref="Liability.NotUsedInRatio"/>.</param>
            /// <param name="pmt">The value of <see cref="Liability.Pmt"/>.</param>
            /// <param name="pmt_rep">The value of <see cref="Liability.Pmt_rep"/>.</param>
            /// <returns>The value for <see cref="Liability.Pmt_repPoint"/>.</returns>
            public static string Pmt_repPoint(bool notUsedInRatio, decimal pmt, string pmt_rep)
            {
                if (notUsedInRatio && pmt != 0)
                {
                    return "(" + pmt_rep + ")";
                }

                return pmt_rep;
            }

            /// <summary>
            /// Calculates the payoff timing for a liability.
            /// </summary>
            /// <param name="willBePdOff">A value indicating whether the liability will be paid off.</param>
            /// <param name="payoffTimingLckd">A value indicating whether the payoff timing is locked.</param>
            /// <param name="payoffTiming">A value indicating the current non-calculated value for the payoff timing.</param>
            /// <param name="defaultsProvider">A liability defaults provider.</param>
            /// <returns>The calculated payoff timing.</returns>
            public static PayoffTiming? PayoffTiming(bool? willBePdOff, bool? payoffTimingLckd, PayoffTiming? payoffTiming, ILiabilityDefaultsProvider defaultsProvider)
            {
                if (willBePdOff == null)
                {
                    return null;
                }
                else if (!willBePdOff.Value)
                {
                    return LendingQB.Core.Data.PayoffTiming.Blank;
                }
                else if (payoffTimingLckd == null)
                {
                    return null;
                }
                else if (payoffTimingLckd.Value)
                {
                    return payoffTiming;
                }

                return defaultsProvider.PayoffTiming;
            }

            /// <summary>
            /// Determines whether the liabilities are equal.
            /// </summary>
            /// <param name="accountNumber1">The first account number.</param>
            /// <param name="creditorName1">The first creditor name.</param>
            /// <param name="accountNumber2">The second account number.</param>
            /// <param name="creditorName2">The second creditor name.</param>
            /// <returns>True if the tradelines match, false otherwise.</returns>
            public static bool IsSameTradeline(string accountNumber1, string creditorName1, string accountNumber2, string creditorName2)
            {
                string accNum1 = accountNumber1.TrimWhitespaceAndBOM().ToUpper();
                string accNum2 = accountNumber2.TrimWhitespaceAndBOM().ToUpper();

                // Definition: 2 tradeline is the same when at least one of these is true
                // 1) Both account numbers are the same and not empty string
                // 2) If account numbers are both empty string and creditor names are the same.
                if (accNum1 == accNum2)
                {
                    if (accNum1 == string.Empty)
                    {
                        string creditorNm1 = creditorName1.TrimWhitespaceAndBOM().ToUpper();
                        string creditorNm2 = creditorName2.TrimWhitespaceAndBOM().ToUpper();
                        return creditorNm1 == creditorNm2;
                    }

                    return true;
                }

                return false;
            }

            /// <summary>
            /// Generates the audit XML that should result after adding an audit to the supplied xml.
            /// </summary>
            /// <param name="existingPmlAuditTrailXmlContent">The XML to add the audit event to.</param>
            /// <param name="userName">The user name.</param>
            /// <param name="loginName">The user's login.</param>
            /// <param name="action">The action to audit.</param>
            /// <param name="field">The field that was changed.</param>
            /// <param name="value">The value the field was changed to.</param>
            /// <returns>The new audit XML.</returns>
            [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "This is just to mimic what happens for the regular liability.")]
            public static string Audit(string existingPmlAuditTrailXmlContent, string userName, string loginName, string action, string field, string value)
            {
                XmlDocument doc = new XmlDocument();
                string xml = existingPmlAuditTrailXmlContent;

                XmlElement rootElement = null;
                if (xml == null || xml == string.Empty)
                {
                    rootElement = doc.CreateElement("History");
                    doc.AppendChild(rootElement);
                }
                else
                {
                    try
                    {
                        doc.LoadXml(xml);
                        rootElement = (XmlElement)doc.ChildNodes[0];
                    }
                    catch (Exception exc)
                    {
                        Tools.LogError("PmlAuditTrailXmlContent is corrupted. xml=" + xml, exc);
                        rootElement = doc.CreateElement("History");
                        doc.AppendChild(rootElement);
                    }
                }

                XmlElement eventElement = doc.CreateElement("Event");
                rootElement.AppendChild(eventElement);

                eventElement.SetAttribute("UserName", userName);
                eventElement.SetAttribute("LoginId", loginName);
                eventElement.SetAttribute("Action", action);

                if (field.Length != 0)
                {
                    eventElement.SetAttribute("Field", field);
                }

                if (value.Length != 0)
                {
                    eventElement.SetAttribute("Value", value);
                }

                eventElement.SetAttribute("EventDate", Tools.GetDateTimeNowString());
                
                return doc.OuterXml;
            }

            /// <summary>
            /// Hold data for the app-level aggregate calculations for liabilities.
            /// </summary>
            public class AggregateData
            {
                /// <summary>
                /// The liability balance total.
                /// </summary>
                public readonly decimal LiaBalTot;

                /// <summary>
                /// The liability monthly total.
                /// </summary>
                public readonly decimal LiaMonTot;

                /// <summary>
                /// The liability paid off total.
                /// </summary>
                public readonly decimal LiaPdOffTot;

                /// <summary>
                /// Initializes a new instance of the <see cref="AggregateData"/> class.
                /// </summary>
                /// <param name="liaBalTot">The liability balance total.</param>
                /// <param name="liaMonTot">The liability monthly total.</param>
                /// <param name="liaPdOffTot">The liability paid off total.</param>
                public AggregateData(decimal liaBalTot, decimal liaMonTot, decimal liaPdOffTot)
                {
                    this.LiaBalTot = liaBalTot;
                    this.LiaMonTot = liaMonTot;
                    this.LiaPdOffTot = liaPdOffTot;
                }
            }
        }
    }
}
