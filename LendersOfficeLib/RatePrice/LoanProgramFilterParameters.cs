﻿#region Generated Code
namespace LendersOfficeApp.los.RatePrice
#endregion
{
    using DataAccess;
    using System.Collections.Generic;

    /// <summary>
    /// Little class to hold all the filters we have for loan programs.
    /// </summary>
    public class LoanProgramFilterParameters
    {
        /// <summary>
        /// Gets or sets a value indicating whether we're using the 10 year term filter.
        /// </summary>
        /// <value>10 year term filter.</value>
        public bool Due10Years
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether we're using the 15 year term filter.
        /// </summary>
        /// <value>15 year term filter.</value>
        public bool Due15Years
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether we're using the 20 year term filter.
        /// </summary>
        /// <value>20 year term filter.</value>
        public bool Due20Years
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether we're using the 25 year term filter.
        /// </summary>
        /// <value>25 year term filter.</value>
        public bool Due25Years
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether we're using the 30 year term filter.
        /// </summary>
        /// <value>30 year term filter.</value>
        public bool Due30Years
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether we're using the Other term filter.
        /// </summary>
        /// <value>Gets or sets the other term filter.</value>
        public bool DueOther
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the 10 and 25 year terms should be included in the 'Other' filter.
        /// </summary>
        /// <value>Whether the 10 and 25 year terms should be included in the other filer.</value>
        public bool Include10And25YearTermsInOtherDue
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the fixed amoritzation type filter is being used.
        /// </summary>
        /// <value>Fixed amortization type filter.</value>
        public bool FinMethFixed
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the 3 year ARM armotization type filter is used.
        /// </summary>
        /// <value>3 year ARM armotization filter.</value>
        public bool FinMeth3YearsArm
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the 5 year ARM armotization filter is used.
        /// </summary>
        /// <value>The 5 year ARM armotization filter.</value>
        public bool FinMeth5YearsArm
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the 7 year ARM armotization filter is used.
        /// </summary>
        /// <value>The 7 year ARM armotization filter.</value>
        public bool FinMeth7YearsArm
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the 10 year ARM armotization filter is being used.
        /// </summary>
        /// <value>The 10 year ARM armotization filter.</value>
        public bool FinMeth10YearsArm
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the Other armotization filter is being used.
        /// </summary>
        /// <value>The Other armotization filter.</value>
        public bool FinMethOther
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the P&amp;I payment type filter is being used.
        /// </summary>
        /// <value>The P&amp;I payment type filter.</value>
        public bool PaymentTypePI
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the I/O payment type filter is being used.
        /// </summary>
        /// <value>The I/O payment type filter.</value>
        public bool PaymentTypeIOnly
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether only want programs for standalone seconds.
        /// </summary>
        /// <value>Standalone second filter.</value>
        public bool CanOnlyBeStandalone2nd
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the product type that the loan program must have.
        /// </summary>
        /// <value>The product type the loan program must have.</value>
        public string MatchingProductType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the investor name that the loan program must have.
        /// </summary>
        /// <value>The investor name that the loan program must have.</value>
        public string MatchingInvestorName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the lien option.
        /// </summary>
        /// <value>The lien option.</value>
        public CLoanProgramsFromDb.E_Option LienOption
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the product types that will be looked at. Only product types in this list will be considered.
        /// Empty lists still let everything through, however.
        /// </summary>
        /// <value>The list of product types to consider.</value>
        public string[] ProductTypesToConsider { get; set; } = new string[] { };

        /// <summary>
        /// Gets or sets a list of product types to ignore. This filtering comes after filtering based on the considered list.
        /// </summary>
        /// <value>The list of product types to ignore.</value>
        public string[] ProductTypesToIgnore { get; set; } = new string[] { };

        /// <summary>
        /// Gets or sets a list of product types to include. This filtering comes after filtering based on the ignore list.
        /// </summary>
        /// <value>The list of product types to include.</value>
        public string[] ProductTypesToInclude { get; set; } = new string[] { };

        /// <summary>
        /// Gets or sets the loan types to ignore.
        /// </summary>
        /// <value>The loan types to ignore.</value>
        public E_sLT[] LoanTypesToIgnore { get; set; } = new E_sLT[] { };

        /// <summary>
        /// Gets or sets the product codes that the found loan programs need to have.
        /// </summary>
        /// <value>The product codes that the found loan programs need to have.</value>
        public HashSet<string> RequiredProductCodes { get; set; } = new HashSet<string> { };

        /// <summary>
        /// Gets or sets a value indicating whether we're using non-qualified mortgage.
        /// </summary>
        /// <value>Value indiciating whether the loan program is for non qm.</value>
        public bool IsNonQm { get; set; }
    }
}
