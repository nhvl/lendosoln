﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using DataAccess;
using LqbGrammar.DataTypes;
using LendersOffice.RatePrice.CustomRatesheet;

namespace LendersOfficeApp.los.RatePrice
{
    public class LoanProgramSet : AbstractLoanProgramSet
    {

        private CLpGlobal m_lpGlobal;
        private CRowHashtable m_rowsLoanPrograms;
        private string m_friendlyInfo;


        public LoanProgramSet(CLpGlobal lpGlobal, DataTable loanPrograms)
        {
            m_lpGlobal = lpGlobal;
            m_rowsLoanPrograms = new CRowHashtable(loanPrograms.Rows, "lLpTemplateId");
            m_friendlyInfo = string.Format("LoanProgramSet SnapshotStr = [{0}], VersionD=[{1}]", lpGlobal.SnapshotStr(), VersionD);
        }
        public override bool IsCurrentSnapshot
        {
            get { return true; }
        }
        public override DateTime VersionD
        {
            get { return m_lpGlobal.VersionD; }
        }

        public override SHA256Checksum FileKey
        {
            get
            {
                return m_lpGlobal.FileKey;
            }
        }
        private CRowHashtable LoanProductRows
        {
            get { return m_rowsLoanPrograms; }
        }

        public override int Count
        {
            get { return LoanProductRows.Count; }
        }

        public override ILoanProgramTemplate RetrieveByIndex(int index)
        {
            DataRow rowLoanProduct = LoanProductRows.GetRowByIndex(index);

            return new CLoanProductDataForRunningPricing(rowLoanProduct, this.ProductFolderRows, null);

        }

        public override ILoanProgramTemplate RetrieveByKey(Guid lLpTemplateId)
        {
            DataRow rowLoanProduct = LoanProductRows.GetRowByKey(lLpTemplateId);

            return new CLoanProductDataForRunningPricing(rowLoanProduct, this.ProductFolderRows, null);

        }

        private CPricePolicyManager PolicyManager
        {
            get { return m_lpGlobal.PolicyManager; }
        }

        public override AbstractRtPricePolicy GetPricePolicy(Guid policyId)
        {
            return PolicyManager.GetPricePolicy(policyId);
        }
        public override PmiManager PmiManager
        {
            get { return m_lpGlobal.PmiManager; }
        }
        public override IPolicyRelationDb PolicyRelation
        {
            get { return m_lpGlobal.ParentHashtable; }
        }

        private CRowHashtable ProductFolderRows
        {
            get { return m_lpGlobal.ProductFolderRows; }
        }

        public override string FriendlyInfo
        {
            get { return m_friendlyInfo; }
        }

        public override IEnumerable<Guid> GetApplicablePolicies(Guid lLpTemplateId)
        {
            DataRow row = m_rowsLoanPrograms.GetRowByKey(lLpTemplateId);
            ArrayList policyIds = (ArrayList)row["Policies"];

            List<Guid> tmpList = new List<Guid>();

            foreach (Guid id in policyIds) 
            {
                tmpList.Add(id);
            }
            return tmpList;

        }

        ConcurrentDictionary<int, Dictionary<Guid, List<RtRateMarginItem>>> pmlIdxDict = new ConcurrentDictionary<int, Dictionary<Guid, List<RtRateMarginItem>>>();

        public override IEnumerable<RtRateMarginItem> GetTpoRateMargin(Guid brokerId, int pmlBrokerIdx, Guid productId) // TODO: using abstract method
        {
            var dict = pmlIdxDict.GetOrAdd(pmlBrokerIdx, (key) => m_lpGlobal.GetTpoRateMargin(brokerId, key));
            return dict != null && dict.ContainsKey(productId) ? dict[productId] : null;
        }
    }
}
