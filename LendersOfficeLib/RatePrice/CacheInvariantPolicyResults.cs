﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Admin;

namespace LendersOfficeApp.los.RatePrice
{
    public class CacheInvariantPolicyResults // Todo : will encapsulate 
    {
        // Phase 0 Qrule invariant cache ( PolicyId -> PhaseZeroAdjustRecord )
        public Hashtable adjustQscoreRecords = new Hashtable();

        // Phase 2 invariant Cache (PolicyId -> PriceAdjustRecord)
        public Hashtable priceAdjustRecords = new Hashtable();

        // Phase 2 invariant q-value cache result for current Q-value Combination
        // (PolicyId -> PriceAdjustRecord)
        public Hashtable priceAdjustRecords4ConstQValue = new Hashtable();

        // Keeping track of cache for previously-seen QValue combination string
        // eg. "QCLTV=100.000;QLTV=80.000;QLAMT=201000.00;QSCORE=600;"
        private Hashtable qvalueIds = new Hashtable();

        public int numPhaseZeroSuccess;

        public void SetQValueId(string qvalueId)
        {
            priceAdjustRecords4ConstQValue = (Hashtable)qvalueIds[qvalueId];
            if (priceAdjustRecords4ConstQValue == null)
            {
                priceAdjustRecords4ConstQValue = new Hashtable();
                qvalueIds[qvalueId] = priceAdjustRecords4ConstQValue;
            }

        }

        public int NumQValue
        {
            get { return qvalueIds.Count; }
        }
        public Dictionary<string, RateOptionExpirationResult> RateOptionExpirationResultCache = new Dictionary<string, RateOptionExpirationResult>();
        public Dictionary<string, RateOptionExpirationResult> RateOptionExpirationNormalUserResultCache = new Dictionary<string, RateOptionExpirationResult>();

        public Dictionary<string, string> SaeErrors = new Dictionary<string, string>(); // Subject->Body
        public Dictionary<Guid, EmployeeDB> EmployeeDBMap = new Dictionary<Guid, EmployeeDB>();

        // OPM 471515 - This contains the number of PMI executions per provider per run.
        public Dictionary<E_PmiCompanyT, int> pmiCompanyUseCount = new Dictionary<DataAccess.E_PmiCompanyT, int>();

        // OPM 473112. The total wait time from pmi quoting.
        public TimeSpan PmiQuoteWaitTime = TimeSpan.Zero;

        // OPM 473112. Summary of quoting events for MI quoting.
        public string PmiQuoteSummary = string.Empty;

        // This is needed so objects can be aware of long-running pricing
        public DateTime PricingRunStartTime = DateTime.Now;
    }

}
