namespace LendersOfficeApp.los.RatePrice
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Xml;
    using DataAccess;
    using Toolbox;

    public class CApplicantPriceXmlNameComparer : IComparer<CApplicantPriceXml>
    {
        #region IComparer<CApplicantPriceXml> Members

        public int Compare(CApplicantPriceXml x, CApplicantPriceXml y)
        {
            return x.lLpTemplateNm.CompareTo(y.lLpTemplateNm);
        }

        #endregion
    }

    public class CApplicantPriceXmlRepresentativeRateComparer : IComparer<CApplicantPriceXml> 
    {
        #region IComparer<CApplicantPriceXml> Members

        public int Compare(CApplicantPriceXml x, CApplicantPriceXml y)
        {
            // 8/27/2009 dd - Must check for IndexOutOfBoundException otherwise you will get cryptic error message.
            // System.ArgumentException: IComparer (or the IComparable methods it relies upon) did not return zero when Array.Sort called x. CompareTo(x). x: ''  x's type: 'CApplicantPriceXml' The IComparer

            if (x.RepresentativeRateOption.Length == 0 && y.RepresentativeRateOption.Length == 0)
            {
                return 0; // Both has no price.
            }
            else if (x.RepresentativeRateOption.Length == 0)
            {
                return -1; // x has no price so display y first.
            }
            else if (y.RepresentativeRateOption.Length == 0)
            {
                return 1; // y has no price.
            }
            else
            {
                CApplicantRateOption aRate = x.RepresentativeRateOption[0];
                CApplicantRateOption bRate = y.RepresentativeRateOption[0];

                return aRate.Rate.CompareTo(bRate.Rate);
            }
        }

        #endregion
    }

    public class CApplicantPriceXml 
    {
        private E_EvalStatus m_evalStatus = E_EvalStatus.Eval_Eligible;
        private string m_disqualifiedRules = "";
        private string m_errors = "";
        private CApplicantRateOption[] m_applicantRateOptions = new CApplicantRateOption[] {};
        private CApplicantRateOption[] m_representativeRateOption = new CApplicantRateOption[] {};
        private IEnumerable<CStipulation> m_denialReasons = new List<CStipulation>();
        private CSortedListOfGroups m_stips = null;
        private CSortedListOfGroups m_hiddenStips = null;
        private string m_marginDelta_rep = "";
        private string m_rateDelta_rep = "";
        private string m_feeDelta_rep = "";
        private string m_maxDti_rep = "";
        private List<CAdjustItem> m_adjustDescs = new List<CAdjustItem>();
        private List<CAdjustItem> m_hiddenAdjustDescs = new List<CAdjustItem>();
        private E_sLT m_lLT = E_sLT.Conventional;
        private string m_lLpProductType = "";
        private string m_lPrepmtPeriod_rep = "";
        public E_sLienPosT m_lLienPosT; 
        private bool m_IsDuRefiFilterEnabled;

        public bool IsDuRefiFilterEnabled
        {
            get { return m_IsDuRefiFilterEnabled; }
        }

        public string lLpProductType 
        {
            get { return m_lLpProductType; }
        }

        public string lPrepmtPeriod_rep
        {
            get { return m_lPrepmtPeriod_rep; }
        }

        public E_sLienPosT lLienPosT
        {
            get { return m_lLienPosT; }
        }

        public E_sLT lLT 
        {
            get { return m_lLT; }
        }

        public string MaxDti_rep 
        {
            get { return m_maxDti_rep; }
        }

        private string m_lTerm_rep = "";
        public string lTerm_rep 
        {
            get { return m_lTerm_rep; }
        }

        private string m_lDue_rep = "";
        public string lDue_rep 
        {
            get { return m_lDue_rep; }
        }

        private E_sFinMethT m_lFinMethT = E_sFinMethT.Fixed;
        public E_sFinMethT lFinMethT 
        {
            get { return m_lFinMethT; }
        }

        private string m_lLockedDays_rep = "";
        public string lLockedDays_rep 
        {
            get { return m_lLockedDays_rep; }
        }

        private string m_lIOnlyMon_rep = "";
        public string lIOnlyMon_rep 
        {
            get { return m_lIOnlyMon_rep; }
        }

        private string m_lRadj1stCapR_rep = "";
        public string lRadj1stCapR_rep 
        {
            get { return m_lRadj1stCapR_rep; }
        }
        private string m_lRadj1stCapMon_rep = "";
        public string lRadj1stCapMon_rep 
        {
            get { return m_lRadj1stCapMon_rep; }
        }
        private string m_lRAdjCapR_rep = "";
        public string lRAdjCapR_rep 
        { 
            get { return m_lRAdjCapR_rep; } 
        } 


        private string m_lRAdjCapMon_rep;
        public string lRAdjCapMon_rep 
        { 
            get { return m_lRAdjCapMon_rep; } 
        } 


        private string m_lRAdjLifeCapR_rep;
        public string lRAdjLifeCapR_rep 
        { 
            get { return m_lRAdjLifeCapR_rep; } 
        } 


        private string m_lRAdjMarginR_rep;
        public string lRAdjMarginR_rep 
        { 
            get { return m_lRAdjMarginR_rep; } 
        } 


        private string m_lRAdjFloorR_rep;
        public string lRAdjFloorR_rep 
        { 
            get { return m_lRAdjFloorR_rep; } 
        } 


        private string m_lPmtAdjCapR_rep;
        public string lPmtAdjCapR_rep 
        { 
            get { return m_lPmtAdjCapR_rep; } 
        } 
        private string m_lPmtAdjCapMon_rep = "";
        public string lPmtAdjCapMon_rep 
        {
            get { return m_lPmtAdjCapMon_rep; }
        }
        private string m_lPmtAdjRecastPeriodMon_rep = "";
        public string lPmtAdjRecastPeriodMon_rep 
        {
            get { return m_lPmtAdjRecastPeriodMon_rep; }
        }
        private string m_lPmtAdjRecastStop_rep = "";
        public string lPmtAdjRecastStop_rep 
        {
            get { return m_lPmtAdjRecastStop_rep; }
        }

        private string m_lPmtAdjMaxBalPc_rep = "";
        public string lPmtAdjMaxBalPc_rep 
        {
            get { return m_lPmtAdjMaxBalPc_rep; }
        }

        private string m_lArmIndexNameVstr = "";
        public string lArmIndexNameVstr
        {
            get { return m_lArmIndexNameVstr; }
        }

        private string m_lRAdjIndexR_rep = "";
        public string lRAdjIndexR_rep
        {
            get { return m_lRAdjIndexR_rep; }
        }
        
        private string m_lBuydwnMon1_rep = "";
        public string lBuydwnMon1_rep 
        {
            get { return m_lBuydwnMon1_rep; }
        }

        private string m_lBuydwnR1_rep = "";
        public string lBuydwnR1_rep 
        {
            get { return m_lBuydwnR1_rep; }
        }
        private string m_lBuydwnMon2_rep = "";
        public string lBuydwnMon2_rep 
        {
            get { return m_lBuydwnMon2_rep; }
        }
        private string m_lBuydwnR2_rep = "";
        public string lBuydwnR2_rep 
        {
            get { return m_lBuydwnR2_rep; }
        }
        private string m_lBuydwnMon3_rep = "";
        public string lBuydwnMon3_rep 
        {
            get { return m_lBuydwnMon3_rep; }
        }
        private string m_lBuydwnR3_rep = "";
        public string lBuydwnR3_rep 
        {
            get { return m_lBuydwnR3_rep; }
        }
        private string m_lBuydwnMon4_rep = "";
        public string lBuydwnMon4_rep 
        {
            get { return m_lBuydwnMon4_rep; }
        }
        private string m_lBuydwnR4_rep = "";
        public string lBuydwnR4_rep 
        {
            get { return m_lBuydwnR4_rep; }
        }
        private string m_lBuydwnMon5_rep = "";
        public string lBuydwnMon5_rep 
        {
            get { return m_lBuydwnMon5_rep; }
        }
        private string m_lBuydwnR5_rep = "";
        public string lBuydwnR5_rep 
        {
            get { return m_lBuydwnR5_rep; }
        }
        private string m_lLateDays = "";
        public string lLateDays 
        {
            get { return m_lLateDays; }
        }
        private bool m_lAprIncludesReqDeposit;
        public bool lAprIncludesReqDeposit 
        { 
            get { return m_lAprIncludesReqDeposit; } 
        } 
        private bool m_lHasDemandFeature;
        public bool lHasDemandFeature 
        { 
            get { return m_lHasDemandFeature; } 
        } 

        private bool m_isBlockedRateLockSubmission;
        public bool IsBlockedRateLockSubmission 
        {
            get { return m_isBlockedRateLockSubmission; }
            set { m_isBlockedRateLockSubmission = value; }
        }

        private bool m_areRatesExpired;
        public bool AreRatesExpired
        {
            get { return m_areRatesExpired; }
        }

        private string m_rateLockSubmissionUserWarningMessage;
        public string RateLockSubmissionUserWarningMessage 
        {
            get { return m_rateLockSubmissionUserWarningMessage; }
        }

        private string m_rateLockSubmissionDevWarningMessage;
        public string RateLockSubmissionDevWarningMessage 
        {
            get { return m_rateLockSubmissionDevWarningMessage; }
        }

        private bool m_lHasVarRFeature;
        public bool lHasVarRFeature 
        { 
            get { return m_lHasVarRFeature; } 
        } 


        private E_sAssumeLT m_lAssumeLT = E_sAssumeLT.LeaveBlank;
        public E_sAssumeLT lAssumeLT 
        {
            get { return m_lAssumeLT; }
        }
        private E_sPrepmtPenaltyT m_lPrepmtPenaltyT = E_sPrepmtPenaltyT.LeaveBlank;
        public E_sPrepmtPenaltyT lPrepmtPenaltyT 
        {
            get { return m_lPrepmtPenaltyT; }
        }


        public E_EvalStatus Status 
        {
            get { return m_evalStatus; }
        }

        public string DisqualifiedRules 
        {
            get { return m_disqualifiedRules; }
        }
        public List<string> DisqualifiedRuleList
        {
            get
            {
                List<string> list = new List<string>();

                foreach (var o in HttpUtility.HtmlDecode(DisqualifiedRules).Split(new[] { "<br>" }, StringSplitOptions.None))
                {
                    list.Add(o);
                }
                return list;
            }
        }
        public CApplicantRateOption[] RepresentativeRateOption 
        {
            get { return m_representativeRateOption; }
        }
        public CApplicantRateOption[] ApplicantRateOptions 
        {
            get { return m_applicantRateOptions; }
        }

        public string lLpTemplateNm { get; private set;}
        public Guid lLpTemplateId { get; private set; }
        public string Errors 
        {
            get { return m_errors; }
        }

        private ArrayList m_developerErrors = new ArrayList();
        public ArrayList DeveloperErrors 
        {
            get { return m_developerErrors; }
        }
        public IEnumerable<CStipulation> DenialReasons
        {
            get { return m_denialReasons; }
        }

        public CSortedListOfGroups Stips 
        {
            get { return m_stips; }
        }
        public CSortedListOfGroups HiddenStips 
        {
            get { return m_hiddenStips; }
        }
        public string MarginDelta_rep 
        {
            get { return m_marginDelta_rep; }
        }
        public string RateDelta_rep 
        {
            get { return m_rateDelta_rep; }
        }
        public string FeeDelta_rep 
        {
            get { return m_feeDelta_rep; }
        }

        private string m_totalFeeDelta_rep = "";
        public string TotalFeeDelta_rep
        {
            get { return m_totalFeeDelta_rep; }
        }
        private string m_version;
        public string Version 
        {
            get { return m_version; }
        }

        private bool m_lIsArmMarginDisplayed = false;

        public bool lIsArmMarginDisplayed 
        {
            get { return m_lIsArmMarginDisplayed; }
        }

        public List<CAdjustItem> AdjustDescs 
        {
            get { return m_adjustDescs; }
        }

        public List<CAdjustItem> HiddenAdjustDescs
        {
            get { return m_hiddenAdjustDescs; }
        }

        private string m_qScore_rep;
        public string QScore_rep
        {
            get { return m_qScore_rep; }
        }

        private decimal m_PhaseZeroLockDaysAdj;
        public decimal PhaseZeroLockDaysAdj
        {
            get { return m_PhaseZeroLockDaysAdj; }

        }


        private int m_tempIndex = 0;
        public int TempIndex 
        {
            get { return m_tempIndex; }
            set { m_tempIndex = value; }
        }

        public string lLpInvestorNm { get; private set; }
        public string ProductCode { get; private set; }

        public string UniqueChecksum { get; private set; }
        public string MortgageInsuranceProvider { get; private set; }
        public string MortgageInsuranceDesc { get; private set; }
        public string MortgageInsuranceSinglePmt { get; private set; }
        public string MortgageInsuranceMonthlyPmt { get; private set; }
        public string MortgageInsuranceSingleRate { get; private set; }
        public string MortgageInsuranceMonthlyRate { get; private set; }

        public string lHelocDraw_rep { get; private set; }
        public string lHelocRepay_rep { get; private set; }

        public bool lHelocCalculatePrepaidInterest { get; private set; }

        public bool IsNonQmProgram { get; private set; }
        public bool IsDisplayInNonQmQuickPricer { get; private set; }

        public string lRateSheetDownloadEndD_rep { get; private set; }
        public string FriendlyInfo { get; private set; }
        public string DebugSummaryJson { get; private set; }
        public static XmlElement ToXmlElement(CApplicantPriceXml applicantPrice, XmlDocument doc) 
        {
            XmlElement el = doc.CreateElement("CApplicantPriceXml");

            el.SetAttribute("IsDuRefiFilterEnabled", applicantPrice.IsDuRefiFilterEnabled.ToString());
            el.SetAttribute("Status", applicantPrice.Status.ToString("D"));
            el.SetAttribute("lLpTemplateNm", applicantPrice.lLpTemplateNm);
            el.SetAttribute("lLpTemplateId", applicantPrice.lLpTemplateId.ToString());
            el.SetAttribute("MarginDelta_rep", applicantPrice.MarginDelta_rep);
            el.SetAttribute("RateDelta_rep", applicantPrice.RateDelta_rep);
            el.SetAttribute("FeeDelta_rep", applicantPrice.FeeDelta_rep);
            el.SetAttribute("TotalFeeDelta_rep", applicantPrice.TotalFeeDelta_rep);
            el.SetAttribute("lLT", applicantPrice.lLT.ToString("D"));
            el.SetAttribute("lTerm_rep", applicantPrice.lTerm_rep);
            el.SetAttribute("lDue_rep", applicantPrice.lDue_rep);
            el.SetAttribute("lFinMethT", applicantPrice.lFinMethT.ToString("D"));
            el.SetAttribute("lLockedDays_rep", applicantPrice.lLockedDays_rep);
            el.SetAttribute("lIOnlyMon_rep", applicantPrice.lIOnlyMon_rep);
            el.SetAttribute("lRadj1stCapR_rep", applicantPrice.lRadj1stCapR_rep);
            el.SetAttribute("lRadj1stCapMon_rep", applicantPrice.lRadj1stCapMon_rep);
            el.SetAttribute("lRAdjCapR_rep", applicantPrice.lRAdjCapR_rep);
            el.SetAttribute("lRAdjCapMon_rep", applicantPrice.lRAdjCapMon_rep);
            el.SetAttribute("lRAdjLifeCapR_rep", applicantPrice.lRAdjLifeCapR_rep);
            el.SetAttribute("lRAdjMarginR_rep", applicantPrice.lRAdjMarginR_rep);
            el.SetAttribute("lRAdjFloorR_rep", applicantPrice.lRAdjFloorR_rep);
            el.SetAttribute("lPmtAdjCapR_rep", applicantPrice.lPmtAdjCapR_rep);
            el.SetAttribute("lPmtAdjCapMon_rep", applicantPrice.lPmtAdjCapMon_rep);
            el.SetAttribute("lPmtAdjRecastPeriodMon_rep", applicantPrice.lPmtAdjRecastPeriodMon_rep);
            el.SetAttribute("lPmtAdjRecastStop_rep", applicantPrice.lPmtAdjRecastStop_rep);
            el.SetAttribute("lPmtAdjMaxBalPc_rep", applicantPrice.lPmtAdjMaxBalPc_rep);
            el.SetAttribute("lBuydwnMon1_rep", applicantPrice.lBuydwnMon1_rep);
            el.SetAttribute("lBuydwnR1_rep", applicantPrice.lBuydwnR1_rep);
            el.SetAttribute("lBuydwnMon2_rep", applicantPrice.lBuydwnMon2_rep);
            el.SetAttribute("lBuydwnR2_rep", applicantPrice.lBuydwnR2_rep);
            el.SetAttribute("lBuydwnMon3_rep", applicantPrice.lBuydwnMon3_rep);
            el.SetAttribute("lBuydwnR3_rep", applicantPrice.lBuydwnR3_rep);
            el.SetAttribute("lBuydwnMon4_rep", applicantPrice.lBuydwnMon4_rep);
            el.SetAttribute("lBuydwnR4_rep", applicantPrice.lBuydwnR4_rep);
            el.SetAttribute("lBuydwnMon5_rep", applicantPrice.lBuydwnMon5_rep);
            el.SetAttribute("lBuydwnR5_rep", applicantPrice.lBuydwnR5_rep);
            el.SetAttribute("lAprIncludesReqDeposit", applicantPrice.lAprIncludesReqDeposit ? "True" : "False");
            el.SetAttribute("lHasDemandFeature", applicantPrice.lHasDemandFeature ? "True" : "False");
            el.SetAttribute("lHasVarRFeature", applicantPrice.lHasVarRFeature ? "True" : "False");
            el.SetAttribute("lAssumeLT", applicantPrice.lAssumeLT.ToString("D"));
            el.SetAttribute("lPrepmtPenaltyT", applicantPrice.lPrepmtPenaltyT.ToString("D"));
            el.SetAttribute("lLateDays", applicantPrice.lLateDays);
            el.SetAttribute("lIsArmMarginDisplayed", applicantPrice.lIsArmMarginDisplayed ? "True" : "False");
            el.SetAttribute("MaxDti_rep", applicantPrice.MaxDti_rep);
            el.SetAttribute("IsBlockedRateLockSubmission", applicantPrice.IsBlockedRateLockSubmission ? "True" : "False");
            el.SetAttribute("RateLockSubmissionUserWarningMessage", applicantPrice.RateLockSubmissionUserWarningMessage);
            el.SetAttribute("RateLockSubmissionDevWarningMessage", applicantPrice.RateLockSubmissionDevWarningMessage);
            el.SetAttribute("lLpProductType", applicantPrice.lLpProductType);
            el.SetAttribute("QScore_rep", applicantPrice.QScore_rep);
            el.SetAttribute("lArmIndexNameVstr", applicantPrice.lArmIndexNameVstr);
            el.SetAttribute("lRAdjIndexR", applicantPrice.lRAdjIndexR_rep);
            el.SetAttribute("lLpInvestorNm", applicantPrice.lLpInvestorNm);
            el.SetAttribute("ProductCode", applicantPrice.ProductCode);
            el.SetAttribute("UniqueChecksum", applicantPrice.UniqueChecksum);
            el.SetAttribute("lPrepmtPeriod_rep", applicantPrice.lPrepmtPeriod_rep);
            el.SetAttribute("lHelocDraw_rep", applicantPrice.lHelocDraw_rep);
            el.SetAttribute("lHelocCalculatePrepaidInterest", applicantPrice.lHelocCalculatePrepaidInterest ? "True" : "False");
            el.SetAttribute("IsNonQmProgram", applicantPrice.IsNonQmProgram ? "True" : "False");
            el.SetAttribute("IsDisplayInNonQmQuickPricer", applicantPrice.IsDisplayInNonQmQuickPricer ? "True" : "False");
            el.SetAttribute("lHelocRepay_rep", applicantPrice.lHelocRepay_rep);
            el.SetAttribute("lRateSheetDownloadEndD_rep", applicantPrice.lRateSheetDownloadEndD_rep);
            el.SetAttribute("FriendlyInfo", applicantPrice.FriendlyInfo);
            el.SetAttribute("PhaseZeroLockDaysAdj", applicantPrice.PhaseZeroLockDaysAdj.ToString());
            el.SetAttribute("DebugSummaryJson", applicantPrice.DebugSummaryJson);
            el.SetAttribute("lLienPosT", applicantPrice.lLienPosT.ToString("D"));
            XmlElement elDisqualifiedRules = doc.CreateElement("DisqualifiedRules");
            el.AppendChild(elDisqualifiedRules);
            elDisqualifiedRules.InnerText = applicantPrice.DisqualifiedRules;

            XmlElement elErrors = doc.CreateElement("Errors");el.SetAttribute("lRadj1stCapR_rep", applicantPrice.lRadj1stCapR_rep);

            el.AppendChild(elErrors);
            elErrors.InnerText = applicantPrice.Errors;

            #region ApplicantRateOptions
            if (null != applicantPrice.ApplicantRateOptions && applicantPrice.ApplicantRateOptions.Length > 0) 
            {
                XmlElement elApplicantRateOptions = doc.CreateElement("ApplicantRateOptions");
                el.AppendChild(elApplicantRateOptions);

                for (int i = 0; i < applicantPrice.ApplicantRateOptions.Length; i++) 
                {
                    CApplicantRateOption o = applicantPrice.ApplicantRateOptions[i];
                    elApplicantRateOptions.AppendChild(o.ToXmlElement(doc));
                }
            }
            #endregion

            #region RepresentativeRateOption
            if (null != applicantPrice.RepresentativeRateOption && applicantPrice.RepresentativeRateOption.Length > 0) 
            {
                XmlElement elRepresentativeRateOption = doc.CreateElement("RepresentativeRateOption");
                el.AppendChild(elRepresentativeRateOption);

                for (int i = 0; i < applicantPrice.RepresentativeRateOption.Length; i++) 
                {
                    CApplicantRateOption o = applicantPrice.RepresentativeRateOption[i];
                    elRepresentativeRateOption.AppendChild(o.ToXmlElement(doc));
                }
            }
            #endregion

            #region DenialReasons
            if (null != applicantPrice.DenialReasons && applicantPrice.DenialReasons.Count() > 0) 
            {
                XmlElement elDenialReasons = doc.CreateElement("DenialReasons");
                el.AppendChild(elDenialReasons);

                foreach (CStipulation s in applicantPrice.DenialReasons) 
                {
                    XmlElement o = doc.CreateElement("DenialReason");
                    o.SetAttribute("debug", s.DebugString);
                    o.InnerText = s.Description;
                    elDenialReasons.AppendChild(o);
                }
            }
            #endregion

            #region Stips
            if (null != applicantPrice.Stips) 
            {
                XmlElement elStips = doc.CreateElement("Stips");
                el.AppendChild(elStips);

                XmlElement sortedListEl = doc.CreateElement("CSortedListOfGroups");
                sortedListEl.SetAttribute("comparerClass", applicantPrice.Stips.ComparerClassName);
                foreach (string key in applicantPrice.Stips.Keys) 
                {
                    foreach (CStipulation stipulation in applicantPrice.Stips.GetGroupByKey(key).Keys) 
                    {
                        XmlElement o = doc.CreateElement("item");
                        o.SetAttribute("key", key);
                        o.SetAttribute("desc", stipulation.Description);
                        o.SetAttribute("debug", stipulation.DebugString);
                        sortedListEl.AppendChild(o);
                    }
                }
                elStips.AppendChild(sortedListEl);

//                XmlElement elCSorted = applicantPrice.Stips.ToXmlElement(doc);
//                elStips.AppendChild(elCSorted);
            }
            #endregion

            #region Hidden Stips
            if (null != applicantPrice.HiddenStips) 
            {
                XmlElement elStips = doc.CreateElement("HiddenStips");
                el.AppendChild(elStips);

                XmlElement sortedListEl = doc.CreateElement("CSortedListOfGroups");
                sortedListEl.SetAttribute("comparerClass", applicantPrice.HiddenStips.ComparerClassName);
                foreach (string key in applicantPrice.HiddenStips.Keys) 
                {
                    foreach (CStipulation stipulation in applicantPrice.HiddenStips.GetGroupByKey(key).Keys) 
                    {
                        XmlElement o = doc.CreateElement("item");
                        o.SetAttribute("key", key);
                        o.SetAttribute("desc", stipulation.Description);
                        o.SetAttribute("debug", stipulation.DebugString);
                        sortedListEl.AppendChild(o);
                    }
                }
                elStips.AppendChild(sortedListEl);
            }
            #endregion
            #region AdjustDescs
            if (null != applicantPrice.AdjustDescs) 
            {
                XmlElement elAdjustDescs = doc.CreateElement("AdjustDescs");
                el.AppendChild(elAdjustDescs);

                foreach (CAdjustItem adjItem in applicantPrice.AdjustDescs) 
                {
                    XmlElement elItem = doc.CreateElement("Item");
                    elAdjustDescs.AppendChild(elItem);

                    elItem.SetAttribute("margin", adjItem.Margin);
                    elItem.SetAttribute("rate", adjItem.Rate);
                    elItem.SetAttribute("fee", adjItem.Fee);
                    elItem.SetAttribute("qrate", adjItem.QRate);
                    elItem.SetAttribute("teaser", adjItem.TeaserRate);
                    elItem.SetAttribute("description", adjItem.Description);
                    elItem.SetAttribute("debug", adjItem.DebugString);
                }
            }
            #endregion

            #region HiddenAdjustDescs
            if (null != applicantPrice.HiddenAdjustDescs) 
            {
                XmlElement elHiddenAdjustDescs = doc.CreateElement("HiddenAdjustDescs");
                el.AppendChild(elHiddenAdjustDescs);

                foreach (CAdjustItem adjItem in applicantPrice.HiddenAdjustDescs) 
                {
                    XmlElement elItem = doc.CreateElement("Item");
                    elHiddenAdjustDescs.AppendChild(elItem);

                    elItem.SetAttribute("margin", adjItem.Margin);
                    elItem.SetAttribute("rate", adjItem.Rate);
                    elItem.SetAttribute("fee", adjItem.Fee);
                    elItem.SetAttribute("qrate", adjItem.QRate);
                    elItem.SetAttribute("teaser", adjItem.TeaserRate);
                    elItem.SetAttribute("description", adjItem.Description);
                    elItem.SetAttribute("debug", adjItem.DebugString);
                }
            }

            #endregion

            #region DeveloperErrors 
            if (null != applicantPrice.DeveloperErrors && applicantPrice.DeveloperErrors.Count > 0) 
            {
                XmlElement elDeveloperErrors = doc.CreateElement("DeveloperErrors");
                el.AppendChild(elDeveloperErrors);

                foreach (string s in applicantPrice.DeveloperErrors) 
                {
                    XmlElement o = doc.CreateElement("DeveloperError");
                    o.InnerText = s;
                    elDeveloperErrors.AppendChild(o);
                }
            }
            #endregion

            return el;

        }
        public static XmlElement ToXmlElement(CApplicantPrice applicantPrice, XmlDocument doc) 
        {
            XmlElement el = doc.CreateElement("CApplicantPriceXml");

            el.SetAttribute("Status", applicantPrice.Status.ToString("D"));
            el.SetAttribute("lLpTemplateNm", applicantPrice.lLpTemplateNm);
            el.SetAttribute("IsDuRefiFilterEnabled", applicantPrice.IsDuRefiFilterEnabled.ToString());
            el.SetAttribute("lLpTemplateId", applicantPrice.lLpTemplateId.ToString());
            el.SetAttribute("MarginDelta_rep", applicantPrice.MarginDelta_rep);
            el.SetAttribute("RateDelta_rep", applicantPrice.RateDelta_rep);
            el.SetAttribute("FeeDelta_rep", applicantPrice.FeeDelta_rep);
            el.SetAttribute("TotalFeeDelta_rep", applicantPrice.TotalFeeDelta_rep);
            el.SetAttribute("lLT", applicantPrice.lLT.ToString("D"));
            el.SetAttribute("lTerm_rep", applicantPrice.lTerm_rep);
            el.SetAttribute("lDue_rep", applicantPrice.lDue_rep);
            el.SetAttribute("lFinMethT", applicantPrice.lFinMethT.ToString("D"));
            el.SetAttribute("lLockedDays_rep", applicantPrice.lLockedDays_rep);
            el.SetAttribute("lIOnlyMon_rep", applicantPrice.lIOnlyMon_rep);
            el.SetAttribute("lRadj1stCapR_rep", applicantPrice.lRadj1stCapR_rep);
            el.SetAttribute("lRadj1stCapMon_rep", applicantPrice.lRadj1stCapMon_rep);
            el.SetAttribute("lRAdjCapR_rep", applicantPrice.lRAdjCapR_rep);
            el.SetAttribute("lRAdjCapMon_rep", applicantPrice.lRAdjCapMon_rep);
            el.SetAttribute("lRAdjLifeCapR_rep", applicantPrice.lRAdjLifeCapR_rep);
            el.SetAttribute("lRAdjMarginR_rep", applicantPrice.lRAdjMarginR_rep);
            el.SetAttribute("lRAdjFloorR_rep", applicantPrice.lRAdjFloorR_rep);
            el.SetAttribute("lPmtAdjCapR_rep", applicantPrice.lPmtAdjCapR_rep);
            el.SetAttribute("lPmtAdjCapMon_rep", applicantPrice.lPmtAdjCapMon_rep);
            el.SetAttribute("lPmtAdjRecastPeriodMon_rep", applicantPrice.lPmtAdjRecastPeriodMon_rep);
            el.SetAttribute("lPmtAdjRecastStop_rep", applicantPrice.lPmtAdjRecastStop_rep);
            el.SetAttribute("lPmtAdjMaxBalPc_rep", applicantPrice.lPmtAdjMaxBalPc_rep);
            el.SetAttribute("lBuydwnMon1_rep", applicantPrice.lBuydwnMon1_rep);
            el.SetAttribute("lBuydwnR1_rep", applicantPrice.lBuydwnR1_rep);
            el.SetAttribute("lBuydwnMon2_rep", applicantPrice.lBuydwnMon2_rep);
            el.SetAttribute("lBuydwnR2_rep", applicantPrice.lBuydwnR2_rep);
            el.SetAttribute("lBuydwnMon3_rep", applicantPrice.lBuydwnMon3_rep);
            el.SetAttribute("lBuydwnR3_rep", applicantPrice.lBuydwnR3_rep);
            el.SetAttribute("lBuydwnMon4_rep", applicantPrice.lBuydwnMon4_rep);
            el.SetAttribute("lBuydwnR4_rep", applicantPrice.lBuydwnR4_rep);
            el.SetAttribute("lBuydwnMon5_rep", applicantPrice.lBuydwnMon5_rep);
            el.SetAttribute("lBuydwnR5_rep", applicantPrice.lBuydwnR5_rep);
            el.SetAttribute("lAprIncludesReqDeposit", applicantPrice.lAprIncludesReqDeposit ? "True" : "False");
            el.SetAttribute("lHasDemandFeature", applicantPrice.lHasDemandFeature ? "True" : "False");
            el.SetAttribute("lHasVarRFeature", applicantPrice.lHasVarRFeature ? "True" : "False");
            el.SetAttribute("lAssumeLT", applicantPrice.lAssumeLT.ToString("D"));
            el.SetAttribute("lPrepmtPenaltyT", applicantPrice.lPrepmtPenaltyT.ToString("D"));
            el.SetAttribute("lLateDays", applicantPrice.lLateDays);
            el.SetAttribute("lIsArmMarginDisplayed", applicantPrice.lIsArmMarginDisplayed ? "True" : "False");
            el.SetAttribute("MaxDti_rep", applicantPrice.MaxDti_rep);
            el.SetAttribute("Version", applicantPrice.DataVersionD.ToString() );
            el.SetAttribute("IsBlockedRateLockSubmission", applicantPrice.IsBlockedRateLockSubmission ? "True" : "False");
            el.SetAttribute("AreRatesExpired", applicantPrice.AreRatesExpired ? "True" : "False");
            el.SetAttribute("RateLockSubmissionUserWarningMessage", applicantPrice.RateLockSubmissionUserWarningMessage);
            el.SetAttribute("RateLockSubmissionDevWarningMessage", applicantPrice.RateLockSubmissionDevWarningMessage);
            el.SetAttribute("lLpProductType", applicantPrice.lLpProductType);
            el.SetAttribute("QScore_rep", applicantPrice.QScore_rep);
            el.SetAttribute("lArmIndexNameVstr", applicantPrice.lArmIndexNameVstr);
            el.SetAttribute("lRAdjIndexR", applicantPrice.lRAdjIndexR_rep);
            el.SetAttribute("lLpInvestorNm", applicantPrice.lLpInvestorNm);
            el.SetAttribute("ProductCode", applicantPrice.ProductCode);
            el.SetAttribute("UniqueChecksum", applicantPrice.UniqueChecksum);
            el.SetAttribute("MortgageInsuranceProvider", applicantPrice.MortgageInsuranceProvider);
            el.SetAttribute("MortgageInsuranceDesc", applicantPrice.MortgageInsuranceDesc);
            el.SetAttribute("MortgageInsuranceMonthlyPmt", applicantPrice.MortgageInsuranceMonthlyPmt);
            el.SetAttribute("MortgageInsuranceSinglePmt", applicantPrice.MortgageInsuranceSinglePmt);
            el.SetAttribute("MortgageInsuranceMonthlyRate", applicantPrice.MortgageInsuranceMonthlyRate);
            el.SetAttribute("MortgageInsuranceSingleRate", applicantPrice.MortgageInsuranceSingleRate);
            el.SetAttribute("lPrepmtPeriod_rep", applicantPrice.lPrepmtPeriod_rep);
            el.SetAttribute("lHelocDraw_rep", applicantPrice.lHelocDraw_rep);
            el.SetAttribute("lHelocCalculatePrepaidInterest", applicantPrice.lHelocCalculatePrepaidInterest ? "True" : "False");
            el.SetAttribute("lHelocRepay_rep", applicantPrice.lHelocRepay_rep);
            el.SetAttribute("lRateSheetDownloadEndD_rep", applicantPrice.lRateSheetDownloadEndD.ToString("MM/dd/yyyy HH:mm"));
            el.SetAttribute("FriendlyInfo", applicantPrice.FriendlyInfo);
            el.SetAttribute("PhaseZeroLockDaysAdj", applicantPrice.PhaseZeroLockDaysAdj.ToString());
            el.SetAttribute("DebugSummaryJson", applicantPrice.DebugPricingSummary.ToJson());
            el.SetAttribute("lLienPosT", applicantPrice.lLienPosT.ToString("D"));

            XmlElement elDisqualifiedRules = doc.CreateElement("DisqualifiedRules");
            el.AppendChild(elDisqualifiedRules);
            elDisqualifiedRules.InnerText = applicantPrice.DisqualifiedRules;

            XmlElement elErrors = doc.CreateElement("Errors");el.SetAttribute("lRadj1stCapR_rep", applicantPrice.lRadj1stCapR_rep);


            el.AppendChild(elErrors);
            elErrors.InnerText = applicantPrice.Errors;

            #region ApplicantRateOptions
            if (null != applicantPrice.ApplicantRateOptions && applicantPrice.ApplicantRateOptions.Length > 0) 
            {
                XmlElement elApplicantRateOptions = doc.CreateElement("ApplicantRateOptions");
                el.AppendChild(elApplicantRateOptions);

                for (int i = 0; i < applicantPrice.ApplicantRateOptions.Length; i++) 
                {
                    CApplicantRateOption o = applicantPrice.ApplicantRateOptions[i];
                    elApplicantRateOptions.AppendChild(o.ToXmlElement(doc));
                }
            }
            #endregion

            #region RepresentativeRateOption
            if (null != applicantPrice.RepresentativeRateOption && applicantPrice.RepresentativeRateOption.Length > 0) 
            {
                XmlElement elRepresentativeRateOption = doc.CreateElement("RepresentativeRateOption");
                el.AppendChild(elRepresentativeRateOption);

                for (int i = 0; i < applicantPrice.RepresentativeRateOption.Length; i++) 
                {
                    CApplicantRateOption o = applicantPrice.RepresentativeRateOption[i];
                    elRepresentativeRateOption.AppendChild(o.ToXmlElement(doc));
                }
            }
            #endregion

            #region DenialReasons
            if (null != applicantPrice.DenialReasons && applicantPrice.DenialReasons.Count() > 0) 
            {
                XmlElement elDenialReasons = doc.CreateElement("DenialReasons");
                el.AppendChild(elDenialReasons);

                foreach (CStipulation s in applicantPrice.DenialReasons) 
                {
                    XmlElement o = doc.CreateElement("DenialReason");
                    o.SetAttribute("debug", s.DebugString);
                    o.InnerText = s.Description;
                    elDenialReasons.AppendChild(o);
                }
            }
            #endregion
            #region DeveloperErrors 
            if (null != applicantPrice.DeveloperErrors && applicantPrice.DeveloperErrors.Count > 0) 
            {
                XmlElement elDeveloperErrors = doc.CreateElement("DeveloperErrors");
                el.AppendChild(elDeveloperErrors);

                foreach (string s in applicantPrice.DeveloperErrors) 
                {
                    XmlElement o = doc.CreateElement("DeveloperError");
                    o.InnerText = s;
                    elDeveloperErrors.AppendChild(o);
                }
            }
            #endregion
            #region Stips
            if (null != applicantPrice.Stips) 
            {
                XmlElement elStips = doc.CreateElement("Stips");
                el.AppendChild(elStips);

                XmlElement sortedListEl = doc.CreateElement("CSortedListOfGroups");
                sortedListEl.SetAttribute("comparerClass", applicantPrice.Stips.ComparerClassName);
                foreach (string key in applicantPrice.Stips.Keys) 
                {
                    foreach (CStipulation stipulation in applicantPrice.Stips.GetGroupByKey(key).Keys) 
                    {
                        XmlElement o = doc.CreateElement("item");
                        o.SetAttribute("key", key);
                        o.SetAttribute("desc", stipulation.Description);
                        o.SetAttribute("debug", stipulation.DebugString);
                        sortedListEl.AppendChild(o);
                    }
                }
                elStips.AppendChild(sortedListEl);
//                XmlElement elCSorted = applicantPrice.Stips.ToXmlElement(doc);
//                elStips.AppendChild(elCSorted);
            }
            #region Hidden Stips
            if (null != applicantPrice.HiddenStips) 
            {
                XmlElement elStips = doc.CreateElement("HiddenStips");
                el.AppendChild(elStips);

                XmlElement sortedListEl = doc.CreateElement("CSortedListOfGroups");
                sortedListEl.SetAttribute("comparerClass", applicantPrice.HiddenStips.ComparerClassName);
                foreach (string key in applicantPrice.HiddenStips.Keys) 
                {
                    foreach (CStipulation stipulation in applicantPrice.HiddenStips.GetGroupByKey(key).Keys) 
                    {
                        XmlElement o = doc.CreateElement("item");
                        o.SetAttribute("key", key);
                        o.SetAttribute("desc", stipulation.Description);
                        o.SetAttribute("debug", stipulation.DebugString);
                        sortedListEl.AppendChild(o);
                    }
                }
                elStips.AppendChild(sortedListEl);
            }
            #endregion
            #endregion

            #region AdjustDescs
            if (null != applicantPrice.AdjustDescs) 
            {
                XmlElement elAdjustDescs = doc.CreateElement("AdjustDescs");
                el.AppendChild(elAdjustDescs);

                foreach (CAdjustItem adjItem in applicantPrice.AdjustDescs) 
                {
                    XmlElement elItem = doc.CreateElement("Item");
                    elAdjustDescs.AppendChild(elItem);

                    elItem.SetAttribute("margin", adjItem.Margin);
                    elItem.SetAttribute("rate", adjItem.Rate);
                    elItem.SetAttribute("fee", adjItem.Fee);
                    elItem.SetAttribute("qrate", adjItem.QRate);
                    elItem.SetAttribute("teaser", adjItem.TeaserRate);
                    elItem.SetAttribute("description", adjItem.Description);
                    elItem.SetAttribute("debug", adjItem.DebugString);
                    elItem.SetAttribute("optionCode", adjItem.OptionCode);

                }
            }
            #endregion

            #region HiddenAdjustDescs
            if (null != applicantPrice.HiddenAdjustDescs) 
            {
                XmlElement elHiddenAdjustDescs = doc.CreateElement("HiddenAdjustDescs");
                el.AppendChild(elHiddenAdjustDescs);

                foreach (CAdjustItem adjItem in applicantPrice.HiddenAdjustDescs) 
                {
                    XmlElement elItem = doc.CreateElement("Item");
                    elHiddenAdjustDescs.AppendChild(elItem);

                    elItem.SetAttribute("margin", adjItem.Margin);
                    elItem.SetAttribute("rate", adjItem.Rate);
                    elItem.SetAttribute("fee", adjItem.Fee);
                    elItem.SetAttribute("qrate", adjItem.QRate);
                    elItem.SetAttribute("teaser", adjItem.TeaserRate);

                    elItem.SetAttribute("description", adjItem.Description);
                    elItem.SetAttribute("debug", adjItem.DebugString);
                    elItem.SetAttribute("optionCode", adjItem.OptionCode);

                }
            }

            #endregion


            return el;
        }
        public static CApplicantPriceXml From(CApplicantPrice applicantPrice)
        {
            CApplicantPriceXml x =  new CApplicantPriceXml();

            x.m_evalStatus = applicantPrice.Status;
            x.lLpTemplateNm= applicantPrice.lLpTemplateNm;
            x.m_IsDuRefiFilterEnabled= applicantPrice.IsDuRefiFilterEnabled;
            x.lLpTemplateId= applicantPrice.lLpTemplateId;
            x.m_marginDelta_rep= applicantPrice.MarginDelta_rep;
            x.m_rateDelta_rep= applicantPrice.RateDelta_rep;
            x.m_feeDelta_rep= applicantPrice.FeeDelta_rep;
            x.m_totalFeeDelta_rep= applicantPrice.TotalFeeDelta_rep;
            x.m_lLT= applicantPrice.lLT;
            x.m_lTerm_rep= applicantPrice.lTerm_rep;
            x.m_lDue_rep= applicantPrice.lDue_rep;
            x.m_lFinMethT= applicantPrice.lFinMethT;
            x.m_lLockedDays_rep= applicantPrice.lLockedDays_rep;
            x.m_lIOnlyMon_rep= applicantPrice.lIOnlyMon_rep;
            x.m_lRadj1stCapR_rep= applicantPrice.lRadj1stCapR_rep;
            x.m_lRadj1stCapMon_rep= applicantPrice.lRadj1stCapMon_rep;
            x.m_lRAdjCapR_rep= applicantPrice.lRAdjCapR_rep;
            x.m_lRAdjCapMon_rep= applicantPrice.lRAdjCapMon_rep;
            x.m_lRAdjLifeCapR_rep= applicantPrice.lRAdjLifeCapR_rep;
            x.m_lRAdjMarginR_rep= applicantPrice.lRAdjMarginR_rep;
            x.m_lRAdjFloorR_rep= applicantPrice.lRAdjFloorR_rep;
            x.m_lPmtAdjCapR_rep= applicantPrice.lPmtAdjCapR_rep;
            x.m_lPmtAdjCapMon_rep= applicantPrice.lPmtAdjCapMon_rep;
            x.m_lPmtAdjRecastPeriodMon_rep= applicantPrice.lPmtAdjRecastPeriodMon_rep;
            x.m_lPmtAdjRecastStop_rep= applicantPrice.lPmtAdjRecastStop_rep;
            x.m_lPmtAdjMaxBalPc_rep= applicantPrice.lPmtAdjMaxBalPc_rep;
            x.m_lBuydwnMon1_rep= applicantPrice.lBuydwnMon1_rep;
            x.m_lBuydwnR1_rep= applicantPrice.lBuydwnR1_rep;
            x.m_lBuydwnMon2_rep= applicantPrice.lBuydwnMon2_rep;
            x.m_lBuydwnR2_rep= applicantPrice.lBuydwnR2_rep;
            x.m_lBuydwnMon3_rep= applicantPrice.lBuydwnMon3_rep;
            x.m_lBuydwnR3_rep= applicantPrice.lBuydwnR3_rep;
            x.m_lBuydwnMon4_rep= applicantPrice.lBuydwnMon4_rep;
            x.m_lBuydwnR4_rep= applicantPrice.lBuydwnR4_rep;
            x.m_lBuydwnMon5_rep= applicantPrice.lBuydwnMon5_rep;
            x.m_lBuydwnR5_rep= applicantPrice.lBuydwnR5_rep;
            x.m_lAprIncludesReqDeposit= applicantPrice.lAprIncludesReqDeposit;
            x.m_lHasDemandFeature= applicantPrice.lHasDemandFeature;
            x.m_lHasVarRFeature= applicantPrice.lHasVarRFeature;
            x.m_lAssumeLT= applicantPrice.lAssumeLT;
            x.m_lPrepmtPenaltyT= applicantPrice.lPrepmtPenaltyT;
            x.m_lLateDays= applicantPrice.lLateDays;
            x.m_lIsArmMarginDisplayed= applicantPrice.lIsArmMarginDisplayed;
            x.m_maxDti_rep= applicantPrice.MaxDti_rep;
            x.m_version= applicantPrice.DataVersionD.ToString();
            x.m_isBlockedRateLockSubmission = applicantPrice.IsBlockedRateLockSubmission;
            x.m_areRatesExpired = applicantPrice.AreRatesExpired; ;
            x.m_rateLockSubmissionUserWarningMessage= applicantPrice.RateLockSubmissionUserWarningMessage;
            x.m_rateLockSubmissionDevWarningMessage= applicantPrice.RateLockSubmissionDevWarningMessage;
            x.m_lLpProductType= applicantPrice.lLpProductType;
            x.m_qScore_rep= applicantPrice.QScore_rep;
            x.m_lArmIndexNameVstr= applicantPrice.lArmIndexNameVstr;
            x.m_lRAdjIndexR_rep = applicantPrice.lRAdjIndexR_rep;


            x.lLpInvestorNm= applicantPrice.lLpInvestorNm;
            x.ProductCode= applicantPrice.ProductCode;
            x.UniqueChecksum= applicantPrice.UniqueChecksum;
            x.MortgageInsuranceProvider= applicantPrice.MortgageInsuranceProvider;
            x.MortgageInsuranceDesc= applicantPrice.MortgageInsuranceDesc;
            x.MortgageInsuranceMonthlyPmt= applicantPrice.MortgageInsuranceMonthlyPmt;
            x.MortgageInsuranceSinglePmt= applicantPrice.MortgageInsuranceSinglePmt;
            x.MortgageInsuranceMonthlyRate= applicantPrice.MortgageInsuranceMonthlyRate;
            x.MortgageInsuranceSingleRate= applicantPrice.MortgageInsuranceSingleRate;
            x.m_lPrepmtPeriod_rep = applicantPrice.lPrepmtPeriod_rep;
            x.lHelocDraw_rep= applicantPrice.lHelocDraw_rep;
            x.lHelocCalculatePrepaidInterest = applicantPrice.lHelocCalculatePrepaidInterest;
            x.lHelocRepay_rep= applicantPrice.lHelocRepay_rep;
            x.m_disqualifiedRules = applicantPrice.DisqualifiedRules;
            x.m_errors = applicantPrice.Errors;
            x.m_applicantRateOptions = applicantPrice.ApplicantRateOptions;

            x.m_representativeRateOption = applicantPrice.RepresentativeRateOption;
            x.m_denialReasons = applicantPrice.DenialReasons;
            x.m_developerErrors = applicantPrice.DeveloperErrors;
            x.m_stips = applicantPrice.Stips;
            x.m_hiddenStips = applicantPrice.HiddenStips;
            x.m_adjustDescs = applicantPrice.AdjustDescs;
            x.m_hiddenAdjustDescs = applicantPrice.HiddenAdjustDescs;
            x.FriendlyInfo = applicantPrice.FriendlyInfo;
            x.DebugSummaryJson = applicantPrice.DebugPricingSummary.ToJson();
            x.m_PhaseZeroLockDaysAdj = applicantPrice.PhaseZeroLockDaysAdj;
            x.m_lLienPosT = applicantPrice.lLienPosT;
            return x;
        }
        public static CApplicantPriceXml Parse(XmlElement el) 
        {
            CApplicantPriceXml applicantPrice = new CApplicantPriceXml();

            try 
            {
                applicantPrice.m_evalStatus = (E_EvalStatus) int.Parse(el.GetAttribute("Status")); 
            } 
            catch 
            {
                Tools.LogWarning("CApplicantPriceXml.Parse: Unable to convert Status. Status=" + el.GetAttribute("Status"));                
            }

            applicantPrice.lLpTemplateNm = el.GetAttribute("lLpTemplateNm");
            try 
            {
                applicantPrice.lLpTemplateId = new Guid(el.GetAttribute("lLpTemplateId"));
            } 
            catch 
            {
                Tools.LogWarning("CApplicantPriceXml.Parse: Unable to convert lLpTemplateId. lLpTemplateId=" + el.GetAttribute("lLpTemplateId"));                

            }
            applicantPrice.m_IsDuRefiFilterEnabled = bool.Parse(el.GetAttribute("IsDuRefiFilterEnabled"));
            applicantPrice.m_marginDelta_rep = el.GetAttribute("MarginDelta_rep");
            applicantPrice.m_rateDelta_rep = el.GetAttribute("RateDelta_rep");
            applicantPrice.m_feeDelta_rep = el.GetAttribute("FeeDelta_rep");
            applicantPrice.m_totalFeeDelta_rep = el.GetAttribute("TotalFeeDelta_rep");
            applicantPrice.m_lLT = (E_sLT) int.Parse(el.GetAttribute("lLT"));
            applicantPrice.m_lTerm_rep = el.GetAttribute("lTerm_rep");
            applicantPrice.m_lDue_rep = el.GetAttribute("lDue_rep");
            applicantPrice.m_lFinMethT = (E_sFinMethT) int.Parse(el.GetAttribute("lFinMethT"));
            applicantPrice.m_lLockedDays_rep = el.GetAttribute("lLockedDays_rep");
            applicantPrice.m_lIOnlyMon_rep = el.GetAttribute("lIOnlyMon_rep");
            applicantPrice.m_lRadj1stCapR_rep = el.GetAttribute("lRadj1stCapR_rep");
            applicantPrice.m_lRadj1stCapMon_rep = el.GetAttribute("lRadj1stCapMon_rep");
            applicantPrice.m_lRAdjCapR_rep = el.GetAttribute("lRAdjCapR_rep");
            applicantPrice.m_lRAdjCapMon_rep = el.GetAttribute("lRAdjCapMon_rep");
            applicantPrice.m_lRAdjLifeCapR_rep = el.GetAttribute("lRAdjLifeCapR_rep");
            applicantPrice.m_lRAdjMarginR_rep = el.GetAttribute("lRAdjMarginR_rep");
            applicantPrice.m_lRAdjFloorR_rep = el.GetAttribute("lRAdjFloorR_rep");
            applicantPrice.m_lPmtAdjCapR_rep = el.GetAttribute("lPmtAdjCapR_rep");
            applicantPrice.m_lPmtAdjCapMon_rep = el.GetAttribute("lPmtAdjCapMon_rep");
            applicantPrice.m_lPmtAdjRecastPeriodMon_rep = el.GetAttribute("lPmtAdjRecastPeriodMon_rep");
            applicantPrice.m_lPmtAdjRecastStop_rep = el.GetAttribute("lPmtAdjRecastStop_rep");
            applicantPrice.m_lPmtAdjMaxBalPc_rep = el.GetAttribute("lPmtAdjMaxBalPc_rep");
            applicantPrice.m_lBuydwnMon1_rep = el.GetAttribute("lBuydwnMon1_rep");
            applicantPrice.m_lBuydwnR1_rep = el.GetAttribute("lBuydwnR1_rep");
            applicantPrice.m_lBuydwnMon2_rep = el.GetAttribute("lBuydwnMon2_rep");
            applicantPrice.m_lBuydwnR2_rep = el.GetAttribute("lBuydwnR2_rep");
            applicantPrice.m_lBuydwnMon3_rep = el.GetAttribute("lBuydwnMon3_rep");
            applicantPrice.m_lBuydwnR3_rep = el.GetAttribute("lBuydwnR3_rep");
            applicantPrice.m_lBuydwnMon4_rep = el.GetAttribute("lBuydwnMon4_rep");
            applicantPrice.m_lBuydwnR4_rep = el.GetAttribute("lBuydwnR4_rep");
            applicantPrice.m_lBuydwnMon5_rep = el.GetAttribute("lBuydwnMon5_rep");
            applicantPrice.m_lBuydwnR5_rep = el.GetAttribute("lBuydwnR5_rep");
            applicantPrice.m_lAprIncludesReqDeposit = el.GetAttribute("lAprIncludesReqDeposit") ==  "True";
            applicantPrice.m_lHasDemandFeature = el.GetAttribute("lHasDemandFeature") == "True";
            applicantPrice.m_lHasVarRFeature = el.GetAttribute("lHasVarRFeature") == "True";
            applicantPrice.m_lAssumeLT = (E_sAssumeLT) int.Parse(el.GetAttribute("lAssumeLT"));
            applicantPrice.m_lPrepmtPenaltyT = (E_sPrepmtPenaltyT) int.Parse(el.GetAttribute("lPrepmtPenaltyT"));
            applicantPrice.m_lLateDays = el.GetAttribute("lLateDays");
            applicantPrice.m_lIsArmMarginDisplayed = el.GetAttribute("lIsArmMarginDisplayed") == "True";
            applicantPrice.m_maxDti_rep = el.GetAttribute("MaxDti_rep");
            applicantPrice.m_version = el.GetAttribute("Version");
            applicantPrice.m_isBlockedRateLockSubmission = el.GetAttribute("IsBlockedRateLockSubmission") == "True";
            applicantPrice.m_areRatesExpired = el.GetAttribute("AreRatesExpired") == "True";
            applicantPrice.m_rateLockSubmissionUserWarningMessage = el.GetAttribute("RateLockSubmissionUserWarningMessage");
            applicantPrice.m_rateLockSubmissionDevWarningMessage = el.GetAttribute("RateLockSubmissionDevWarningMessage");
            applicantPrice.m_lLpProductType = el.GetAttribute("lLpProductType");
            applicantPrice.m_qScore_rep = el.GetAttribute("QScore_rep");
            applicantPrice.m_lArmIndexNameVstr = el.GetAttribute("lArmIndexNameVstr");
            applicantPrice.m_lRAdjIndexR_rep = el.GetAttribute("lRAdjIndexR");

            applicantPrice.m_lPrepmtPeriod_rep = el.GetAttribute("lPrepmtPeriod_rep");

            applicantPrice.lLpInvestorNm = el.GetAttribute("lLpInvestorNm");
            applicantPrice.ProductCode = el.GetAttribute("ProductCode");
            applicantPrice.UniqueChecksum = el.GetAttribute("UniqueChecksum");
            applicantPrice.MortgageInsuranceProvider = el.GetAttribute("MortgageInsuranceProvider");
            applicantPrice.MortgageInsuranceDesc = el.GetAttribute("MortgageInsuranceDesc");
            applicantPrice.MortgageInsuranceMonthlyPmt = el.GetAttribute("MortgageInsuranceMonthlyPmt");
            applicantPrice.MortgageInsuranceSinglePmt = el.GetAttribute("MortgageInsuranceSinglePmt");
            applicantPrice.MortgageInsuranceMonthlyRate = el.GetAttribute("MortgageInsuranceMonthlyRate");
            applicantPrice.MortgageInsuranceSingleRate = el.GetAttribute("MortgageInsuranceSingleRate");
            applicantPrice.lHelocDraw_rep = el.GetAttribute("lHelocDraw_rep");
            applicantPrice.lHelocCalculatePrepaidInterest = el.GetAttribute("lHelocCalculatePrepaidInterest") == "True";
            applicantPrice.IsNonQmProgram = el.GetAttribute("IsNonQmProgram") == "True";
            applicantPrice.IsDisplayInNonQmQuickPricer = el.GetAttribute("IsDisplayInNonQmQuickPricer") == "True";
            applicantPrice.lHelocRepay_rep = el.GetAttribute("lHelocRepay_rep");
            applicantPrice.lRateSheetDownloadEndD_rep = el.GetAttribute("lRateSheetDownloadEndD_rep");
            applicantPrice.FriendlyInfo = el.GetAttribute("FriendlyInfo");
            applicantPrice.m_PhaseZeroLockDaysAdj = decimal.Parse(el.GetAttribute("PhaseZeroLockDaysAdj"));
            applicantPrice.DebugSummaryJson = el.GetAttribute("DebugSummaryJson");
            applicantPrice.m_lLienPosT = (E_sLienPosT)int.Parse(el.GetAttribute("lLienPosT"));
            XmlElement elDisqualifiedRules = (XmlElement) el.SelectSingleNode("DisqualifiedRules");
            if (null != elDisqualifiedRules)
                applicantPrice.m_disqualifiedRules = elDisqualifiedRules.InnerText;

            XmlElement elErrors = (XmlElement) el.SelectSingleNode("Errors");
            if (null != elErrors)
                applicantPrice.m_errors = elErrors.InnerText;

            XmlNodeList nodeList = el.SelectNodes("ApplicantRateOptions/CApplicantRateOption");
            if (nodeList.Count > 0) 
            {
                applicantPrice.m_applicantRateOptions = new CApplicantRateOption[nodeList.Count];

                for (int i = 0; i < nodeList.Count; i++) 
                {
                    applicantPrice.m_applicantRateOptions[i] = CApplicantRateOption.Parse((XmlElement) nodeList[i]);
                }
            }

            #region RepresentativeRateOption
            nodeList = el.SelectNodes("RepresentativeRateOption/CApplicantRateOption");
            if (nodeList.Count > 0) 
            {
                applicantPrice.m_representativeRateOption = new CApplicantRateOption[nodeList.Count];

                for (int i = 0; i < nodeList.Count; i++) 
                {
                    applicantPrice.m_representativeRateOption[i] = CApplicantRateOption.Parse((XmlElement) nodeList[i]);
                }
            }
            #endregion

            #region DenialReasons
            nodeList = el.SelectNodes("DenialReasons/DenialReason");
            if (nodeList.Count > 0) 
            {
                List<CStipulation> list = new List<CStipulation>();
                foreach (XmlElement o in nodeList)
                    list.Add(new CStipulation(o.InnerText, o.GetAttribute("debug")));

                applicantPrice.m_denialReasons = list;
            }
            #endregion

            #region DeveloperErrors
            nodeList = el.SelectNodes("DeveloperErrors/DeveloperError");
            if (nodeList.Count > 0) 
            {
                applicantPrice.m_developerErrors = new ArrayList();
                foreach (XmlElement o in nodeList) 
                {
                    applicantPrice.m_developerErrors.Add(o.InnerText);
                }
            }
            #endregion
            #region AdjustDescs
            nodeList = el.SelectNodes("AdjustDescs/Item");
            if (nodeList != null) 
            {
                applicantPrice.m_adjustDescs = new List<CAdjustItem>();
                foreach (XmlElement o in nodeList) 
                {
                    CAdjustItem adjItem = new CAdjustItem(o.GetAttribute("margin"),
                                                      o.GetAttribute("rate"),
                                                      o.GetAttribute("fee"),
                                                      o.GetAttribute("qrate"),
                                                      o.GetAttribute("teaser"),
                                                      o.GetAttribute("description"),
                                                      o.GetAttribute("debug"));
                    adjItem.OptionCode = o.GetAttribute("optionCode");
                    applicantPrice.m_adjustDescs.Add(adjItem);
                }
            }
            #endregion

            #region HiddenAdjustDescs
            nodeList = el.SelectNodes("HiddenAdjustDescs/Item");
            if (nodeList != null) 
            {
                applicantPrice.m_hiddenAdjustDescs = new List<CAdjustItem>();
                foreach (XmlElement o in nodeList) 
                {
                    CAdjustItem adjItem = new CAdjustItem(o.GetAttribute("margin"),
                        o.GetAttribute("rate"),
                        o.GetAttribute("fee"),
                        o.GetAttribute("qrate"),
                        o.GetAttribute("teaser"),
                        o.GetAttribute("description"),
                        o.GetAttribute("debug"));
                        adjItem.OptionCode = o.GetAttribute("optionCode");
                    applicantPrice.m_hiddenAdjustDescs.Add(adjItem);
                }
            }
            #endregion

            #region Stips
            XmlElement elStips = (XmlElement) el.SelectSingleNode("Stips");

            XmlElement sortedListEl = (XmlElement) elStips.ChildNodes[0];

            string comparerClassName = sortedListEl.GetAttribute("comparerClass");

            CSortedListOfGroups sortedListOfGroups = null;
            XmlNodeList itemNodeList = sortedListEl.SelectNodes("item");
            switch (comparerClassName) 
            {
                case "Toolbox.CStipulationCategoryComparer":
                    sortedListOfGroups = new CSortedListOfGroups(new CStipulationCategoryComparer(), itemNodeList.Count);
                    break;
                case "":
                    sortedListOfGroups = new CSortedListOfGroups(itemNodeList.Count);
                    break;
                default:
                    Tools.LogBug(comparerClassName + " is not handle in CSortedListOfGroups");
                    sortedListOfGroups = new CSortedListOfGroups(itemNodeList.Count);
                    break;
            }
            foreach (XmlElement o in itemNodeList) 
            {
                string key = o.GetAttribute("key");
                string desc = o.GetAttribute("desc");
                string debugString = o.GetAttribute("debug");
                sortedListOfGroups.Add(key, new CStipulation(desc, debugString));

            }

            applicantPrice.m_stips = sortedListOfGroups;
            //            applicantPrice.m_stips = CSortedListOfGroups.Parse((XmlElement) elStips.ChildNodes[0]);
            #endregion

            #region HiddenStips
            XmlElement elHiddenStips = (XmlElement) el.SelectSingleNode("HiddenStips");
            if (null != elHiddenStips) 
            {

                XmlElement hiddenSortedListEl = (XmlElement) elHiddenStips.ChildNodes[0];

                string hiddenComparerClassName = hiddenSortedListEl.GetAttribute("comparerClass");

                CSortedListOfGroups hiddenSortedListOfGroups = null;
                XmlNodeList hiddenItemNodeList = hiddenSortedListEl.SelectNodes("item");
                switch (hiddenComparerClassName) 
                {
                    case "Toolbox.CStipulationCategoryComparer":
                        hiddenSortedListOfGroups = new CSortedListOfGroups(new CStipulationCategoryComparer(), hiddenItemNodeList.Count);
                        break;
                    case "":
                        hiddenSortedListOfGroups = new CSortedListOfGroups(hiddenItemNodeList.Count);
                        break;
                    default:
                        Tools.LogBug(comparerClassName + " is not handle in CSortedListOfGroups");
                        hiddenSortedListOfGroups = new CSortedListOfGroups(hiddenItemNodeList.Count);
                        break;
                }
                foreach (XmlElement o in hiddenItemNodeList) 
                {
                    string key = o.GetAttribute("key");
                    string desc = o.GetAttribute("desc");
                    string debugString = o.GetAttribute("debug");
                    hiddenSortedListOfGroups.Add(key, new CStipulation(desc, debugString));

                }

                applicantPrice.m_hiddenStips = hiddenSortedListOfGroups;
                //            applicantPrice.m_stips = CSortedListOfGroups.Parse((XmlElement) elStips.ChildNodes[0]);
            }

            #endregion


            return applicantPrice;
        }


    }
}
