using System;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Constants;
using DataAccess;
using LendersOffice.Common;
using LqbGrammar.DataTypes;

namespace LendersOfficeApp.los.RatePrice
{
	public class CLpeReleaseInfo
	{
	
		/// <summary>
		/// 
		/// </summary>
		/// <returns>Null if there is no lpe data release</returns>
        public static CLpeReleaseInfo GetLatestReleaseInfo()
        {
            return GetLatestReleaseInfo(false);
        }

        public static CLpeReleaseInfo GetLatestReleaseInfo(bool allowGetCachedObj)
        {
            return GetLatestReleaseInfoImp(allowGetCachedObj, false /*forceToUseManualImport*/);
        }

        public static CLpeReleaseInfo GetLatestManualImportInfo()
        {
            return GetLatestReleaseInfoImp(false /*allowGetCachedObj*/, true /*forceToUseManualImport*/);
        }

        private static CLpeReleaseInfo GetLatestReleaseInfoImp( bool allowGetCachedObj, bool forceToUseManualImport )
		{
            string storedProcedureName = "RetrieveLpeReleaseCurrent";

            if (!ConstStage.UseLpeDatabaseForPricing || forceToUseManualImport)
            {
                storedProcedureName = "LPE_RELEASE_RetrieveLatestManualImport";
            }

			using( DbDataReader r  = StoredProcedureHelper.ExecuteReader( DataSrc.LOShareROnly, storedProcedureName ) )
			{
                if( r.Read() )
                {
                    DateTime releaseVersion = (DateTime)r["ReleaseVersion"];
                    string snapshotId = (string)r["snapshotId"];
                    DateTime dataLastModifiedD = (DateTime)r["DataLastModifiedD"];

                    SHA256Checksum fileKey = SHA256Checksum.Invalid;

                    var tempFileKey = SHA256Checksum.Create((string)r["FileKey"]);

                    if (tempFileKey != null)
                    {
                        fileKey = tempFileKey.Value;
                    }

                    Func<CLpeReleaseInfo, CLpeReleaseInfo> updateSnapshotName = (CLpeReleaseInfo releaseInfo) =>
                    {
                        DataSrc dataSrc = releaseInfo.DataSrcForSnapshot;
                        if(dataSrc == DataSrc.LpeRelease3 || dataSrc == DataSrc.LpeRelease4)
                        {
                            DbAccessUtils.SetLpeDbSnapshotName(dataSrc, releaseInfo.SnapshotName);
                        }

                        return releaseInfo;
                    };

                    if (allowGetCachedObj == false || forceToUseManualImport)
                    {
                        return updateSnapshotName(new CLpeReleaseInfo(releaseVersion, snapshotId, dataLastModifiedD, fileKey));
                    }


                    lock ( s_lock )
                    {
                        if (
                                s_currentRelease == null
                                || releaseVersion != s_currentRelease.m_releaseVer
                                || snapshotId != s_currentRelease.m_snapshotId
                                || dataLastModifiedD != s_currentRelease.m_dataLastModifiedDate
                                || fileKey != s_currentRelease.FileKey
                           )
                        {
                            s_currentRelease = new CLpeReleaseInfo(releaseVersion, snapshotId, dataLastModifiedD, fileKey);
                            updateSnapshotName(s_currentRelease);
                        }

                        return s_currentRelease;
                    }
                }
				string errMsg = "Lpe_Release table is empty, probably no lpe data snapshot is ensured. We need to make a snapshot then create an entry in Lpe_Release table manually.";
				Tools.LogErrorWithCriticalTracking( errMsg );
				return null;
			}
		}

        static CLpeReleaseInfo              s_currentRelease;
        static Object                       s_lock = new object(); 

		private DateTime					m_releaseVer;
		private DateTime					m_dataLastModifiedDate;
		private string						m_snapshotId; 	// values of snapshotId's are app consts and snapshot name is stage const so we can share multiple stages on the same sqlserver
                                                            // connStr to ss is a composite of a stage constant + ssName

		public CLpeReleaseInfo( DateTime releaseVer, string snapshotId, DateTime dataLastModifiedD, SHA256Checksum fileKey)
		{
			m_releaseVer = releaseVer;
			m_snapshotId = snapshotId;
			m_dataLastModifiedDate = dataLastModifiedD;
            this.FileKey = fileKey;
		}

		public DateTime ReleaseVer
		{
			get { return m_releaseVer; }
		}

		/// <summary>
		/// This is an app constant, SnapshotName is the stage constant
		/// </summary>
		public string SnapshotId
		{
			get { return m_snapshotId; }
		}

		static public string GetSnapshotNameFrom( string snapshotId )
		{
			switch( snapshotId )
			{
				case ConstAppThinh.LpeSnapshot1Id:
					return ConstStage.LpeSnapshot1Name;
				case ConstAppThinh.LpeSnapshot2Id:
					return ConstStage.LpeSnapshot2Name;
				default:
                    if( snapshotId.StartsWith(ConstAppThinh.LpePrefixUndropSnapshotId) )
                    {
                        return snapshotId;
                    }

					string errMsg = string.Format( "{0} is not a recognizable snapshot id", snapshotId );
					Tools.LogErrorWithCriticalTracking( errMsg );
					throw new CBaseException( ErrorMessages.Generic, errMsg );
			}
		}

		static public DataSrc GetDataSrcFrom( string snapshotId )
		{
            if (snapshotId == "ManualImport")
            {
                // dd 7/23/2017 - On AWS, Lpe database does not perform snapshot. Therefore use main lpe data source.
                return DataSrc.LpeSrc;
            }

			switch( snapshotId )
			{
				case ConstAppThinh.LpeSnapshot1Id:
					return DataSrc.LpeRelease1;
				case ConstAppThinh.LpeSnapshot2Id:
					return DataSrc.LpeRelease2;
				default:
                    if (snapshotId.StartsWith(ConstAppThinh.LpeUndropSnapshot1Id /* "ss01" */))
                    {
                        return DataSrc.LpeRelease3;
                    }

                    if (snapshotId.StartsWith(ConstAppThinh.LpeUndropSnapshot2Id /* "ss02" */))
                    {
                        return DataSrc.LpeRelease4;
                    }


                    string errMsg = string.Format( "{0} is not a recognizable snapshot id", snapshotId );
					Tools.LogErrorWithCriticalTracking( errMsg );
					throw new CBaseException( ErrorMessages.Generic, errMsg );
			}
		}

		public DataSrc DataSrcForSnapshot
		{
			get { return GetDataSrcFrom( m_snapshotId ); }
		}

		public TimeSpan ReleaseAge
		{
			get
			{ 
				return DateTime.Now.Subtract( m_releaseVer );
			}
		}

		public DateTime DataLastModifiedD
		{
			get { return m_dataLastModifiedDate; }
		}

		public string SnapshotName
		{
			get { return GetSnapshotNameFrom( m_snapshotId ); }
		}

        public SHA256Checksum FileKey { get; private set; }
	}
}