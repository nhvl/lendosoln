﻿using System;
using System.Collections;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Test
{
    /// <summary>
    /// Build up dependency relationships and store both
    /// directions in the tree.
    /// </summary>

    public class DependencyGraph
    {
        /// <summary>
        /// Build up dependency relationships and store both
        /// directions in the tree.
        /// </summary>

        private Hashtable m_Nodes = new Hashtable();

        public FieldNode this[String sName]
        {
            // Access member.

            get
            {
                return m_Nodes[sName] as FieldNode;
            }
        }

        public void InitializeGraph(KindOfDependsOn kindOfDependsOn, params Type[] tClasses)
        {
            // Clear the current graph and load up all the dependencies
            // without flattening them.

            m_Nodes.Clear();

            foreach (Type type in tClasses)
            {
                foreach (PropertyInfo prop in type.GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
                {
                    foreach (Attribute attr in prop.GetCustomAttributes(false))
                    {
                        // Get the next depends-on attribute from the
                        // specified class set.

                        DependsOnAttribute dependson = attr as DependsOnAttribute;

                        if (dependson == null)
                        {
                            continue;
                        }

                        // Create a field entry in our graph.  Each reference
                        // points to the same entry.  Watch out for cycles.

                        FieldNode node = m_Nodes[prop.Name] as FieldNode;

                        if (node == null)
                        {
                            m_Nodes.Add(prop.Name, node = new FieldNode(prop.Name));
                        }

                        foreach (var d in dependson.GetList())
                        {
                            String dependency = d.Name;

                            if (dependency == "")
                            {
                                Tools.LogError("Dependency graph: Has empty dependency for " + node.Name + ".");

                                continue;
                            }
                            else
                            {
                                if (dependency.IndexOf(":") != -1)
                                {
                                    dependency = dependency.Split(':')[1];
                                }
                            }

                            FieldNode child = m_Nodes[dependency] as FieldNode;

                            if (child == null)
                            {
                                m_Nodes.Add(dependency, child = new FieldNode(dependency));
                            }

                            node.DependsOn.Add(child);
                            child.Affecting.Add(node);
                        }
                    }
                }
            }

        }

        class StackOrQueue
        {
            Stack stack;
            Queue queue;
            public StackOrQueue(bool isStack)
            {
                if (isStack)
                    stack = new Stack();
                else
                    queue = new Queue();
            }

            public void Push(object obj)
            {
                if (stack != null)
                    stack.Push(obj);
                else
                    queue.Enqueue(obj);
            }

            public object Pop()
            {
                if (stack != null)
                    return stack.Pop();
                else
                    return queue.Dequeue();
            }

            public int Count
            {
                get { return stack != null ? stack.Count : queue.Count; }
            }
        }

        public Hashtable GetDependencyNames(string name, bool directDependOnly)
        {
            Hashtable dependNames = new Hashtable();
            FieldNode fNode = this[name];

            if (fNode == null)
                return dependNames;

            StackOrQueue stack = new StackOrQueue( /* isStack = */  false);
            stack.Push(fNode);

            while (stack.Count != 0)
            {
                fNode = stack.Pop() as FieldNode;
                if (fNode == null)
                    continue;
                foreach (FieldNode node in fNode.DependsOn)
                {
                    if (dependNames.Contains(node.Name))
                        continue;

                    dependNames[node.Name] = fNode.Name;

                    if (directDependOnly == false)
                        stack.Push(node);
                }
            }

            return dependNames;
        }

    }

    /// <summary>
    /// Unique list of field nodes.
    /// </summary>

    public class FieldNodeList : ArrayList
    {
        /// <summary>
        /// Append to the tail only if unique.
        /// </summary>
        /// <param name="oNode">
        /// Node to append to the tail.
        /// </param>
        /// <returns>
        /// Add count, or -1 on error.
        /// </returns>

        public override int Add(Object oNode)
        {
            // Only add if unique.

            FieldNode node = oNode as FieldNode;

            if (node == null)
            {
                return -1;
            }

            foreach (FieldNode item in this)
            {
                if (node.Name == item.Name)
                {
                    return -1;
                }
            }

            return base.Add(node);
        }

    }

    /// <summary>
    /// Maintain dependency relationships.
    /// </summary>

    public class FieldNode
    {
        private FieldNodeList m_Affecting;
        private FieldNodeList m_DependsOn;
        private String m_Name;

        public FieldNodeList Affecting
        {
            // Access member.

            set
            {
                m_Affecting = value;
            }
            get
            {
                return m_Affecting;
            }
        }

        public FieldNodeList DependsOn
        {
            // Access member.

            set
            {
                m_DependsOn = value;
            }
            get
            {
                return m_DependsOn;
            }
        }

        public String Name
        {
            // Access member.

            set
            {
                m_Name = value;
            }
            get
            {
                return m_Name;
            }
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public FieldNode(String sName)
        {
            // Initialize members.

            m_Affecting = new FieldNodeList();
            m_DependsOn = new FieldNodeList();

            m_Name = sName;
        }

    }

    /// <summary>
    /// Keep simple view of field -- composite.
    /// </summary>

    public class Field
    {
        /// <summary>
        /// Keep simple view of field -- composite.
        /// </summary>

        private ArrayList m_Children;
        private String m_Name;

        [XmlAttribute
       ]
        public String Name
        {
            // Access member.

            set
            {
                m_Name = value;
            }
            get
            {
                return m_Name;
            }
        }

        public ArrayList Children
        {
            // Access member.

            get
            {
                return m_Children;
            }
        }

        /// <summary>
        /// Recursively adopt the children of the node tree and
        /// build up this field in the process.
        /// </summary>

        public void Adopt(FieldNode fNode, ArrayList fNames)
        {
            // Look for cycles.  We should use a dictionary.

            if (fNames.Contains(fNode.Name) == true)
            {
                Field cycle = new Field();

                m_Name = fNode.Name + " (cycle)";

                return;
            }

            fNames.Add(m_Name = fNode.Name);

            foreach (FieldNode node in fNode.DependsOn)
            {
                // Adopt each child.

                Field child = new Field();

                child.Adopt(node, fNames);

                m_Children.Add(child);
            }

            fNames.Remove(m_Name);
        }

        /// <summary>
        /// Recursively reach the parents of the node tree and
        /// build up this field in the process.
        /// </summary>

        public void Reach(FieldNode fNode, ArrayList fNames)
        {
            // Look for cycles.  We should use a dictionary.

            if (fNames.Contains(fNode.Name) == true)
            {
                Field cycle = new Field();

                m_Name = fNode.Name + " (cycle)";

                return;
            }

            fNames.Add(m_Name = fNode.Name);

            foreach (FieldNode node in fNode.Affecting)
            {
                // Adopt each child.

                Field child = new Field();

                child.Reach(node, fNames);

                m_Children.Add(child);
            }

            fNames.Remove(m_Name);
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public Field()
        {
            // Initialize members.

            m_Children = new ArrayList();

            m_Name = "";
        }

    }

    /// <summary>
    /// Maintain list of selected fields.
    /// </summary>

    public class DependsOnExport
    {
        /// <summary>
        /// Maintain list of selected fields.
        /// </summary>

        private ArrayList m_Fields = new ArrayList();
        private string m_appName = "";

        /// <summary>
        /// Append to the end.
        /// </summary>

        public void Add(Field fItem)
        {
            // Append to the end.

            m_Fields.Add(fItem);
        }

        public string AppName
        {
            get { return m_appName; }
            set { m_appName = value; }
        }

        #region ( Streaming Operators )

        /// <summary>
        /// Translate instance to an xml document.
        /// </summary>
        /// <returns>
        /// Serialized instance.
        /// </returns>

        public override String ToString()
        {
            // Perform simple serialization according to hints.
            // We also update the version to latest so that
            // persisted instances have some semblance of history.

            XmlElement root; XmlDocument xD = new XmlDocument();

            root = xD.CreateElement("DependsOn");
            if (m_appName.Length > 0)
            {
                XmlAttribute xA = xD.CreateAttribute("AppName");
                xA.Value = m_appName;
                root.Attributes.Append(xA);
            }

            foreach (Field field in m_Fields)
            {
                root.AppendChild(AddField(xD, field));
            }

            xD.AppendChild(root);

            return xD.InnerXml;
        }

        private XmlElement AddField(XmlDocument xD, Field fField)
        {
            // Append the given field as a new xml element.

            XmlElement xE = xD.CreateElement(fField.Name.Split(' ')[0].TrimWhitespaceAndBOM());

            if (fField.Name.IndexOf("(cycle)") != -1)
            {
                XmlAttribute xA = xD.CreateAttribute("Warning");

                xA.Value = "Cycle";

                xE.Attributes.Append(xA);
            }

            foreach (Field child in fField.Children)
            {
                xE.AppendChild(AddField(xD, child));
            }

            return xE;
        }

        #endregion

    }

    /// <summary>
    /// Maintain list of selected fields.
    /// </summary>

    public class AffectingExport
    {
        /// <summary>
        /// Maintain list of selected fields.
        /// </summary>

        private ArrayList m_Fields = new ArrayList();
        private string m_appName = "";

        /// <summary>
        /// Append to the end.
        /// </summary>

        public void Add(Field fItem)
        {
            // Append to the end.

            m_Fields.Add(fItem);
        }

        public string AppName
        {
            get { return m_appName; }
            set { m_appName = value; }
        }


        #region ( Streaming Operators )

        /// <summary>
        /// Translate instance to an xml document.
        /// </summary>
        /// <returns>
        /// Serialized instance.
        /// </returns>

        public override String ToString()
        {
            // Perform simple serialization according to hints.
            // We also update the version to latest so that
            // persisted instances have some semblance of history.

            XmlElement root; XmlDocument xD = new XmlDocument();

            root = xD.CreateElement("Affecting");
            if (m_appName.Length > 0)
            {
                XmlAttribute xA = xD.CreateAttribute("AppName");
                xA.Value = m_appName;
                root.Attributes.Append(xA);
            }

            foreach (Field field in m_Fields)
            {
                root.AppendChild(AddField(xD, field));
            }

            xD.AppendChild(root);

            return xD.InnerXml;
        }

        private XmlElement AddField(XmlDocument xD, Field fField)
        {
            // Append the given field as a new xml element.

            XmlElement xE = xD.CreateElement(fField.Name.Split(' ')[0].TrimWhitespaceAndBOM());

            if (fField.Name.IndexOf("(cycle)") != -1)
            {
                XmlAttribute xA = xD.CreateAttribute("Warning");

                xA.Value = "Cycle";

                xE.Attributes.Append(xA);
            }

            foreach (Field child in fField.Children)
            {
                xE.AppendChild(AddField(xD, child));
            }

            return xE;
        }

        #endregion

    }


}
