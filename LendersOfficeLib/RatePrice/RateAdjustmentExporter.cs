﻿// <copyright file="RateAdjustmentExporter.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: David D
// Date: 11/11/2016
// </summary>
namespace LendersOffice.RatePrice
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.IO;
    using DataAccess;
    using LendersOfficeApp.los.RatePrice;

    /// <summary>
    /// An rate adjustment export class.
    /// </summary>
    public class RateAdjustmentExporter
    {
        /// <summary>
        /// Export the rate adjustment of the price policy and all of it children.
        /// </summary>
        /// <param name="pricePolicyId">The parent price policy id to export.</param>
        /// <returns>The location of temp csv file.</returns>
        public string ExportToCsvFile(Guid pricePolicyId)
        {
            if (pricePolicyId == Guid.Empty)
            {
                throw new ArgumentException("PricePolicyId cannot be empty.");
            }

            CPricePolicy pricePolicy = CPricePolicy.Retrieve(pricePolicyId);

            string outputFile = TempFileUtils.NewTempFilePath();

            using (StreamWriter writer = new StreamWriter(outputFile))
            {
                this.WritePolicy(writer, pricePolicy, string.Empty);
            }

            return outputFile;
        }

        /// <summary>
        /// Write a policy and adjustment to file.
        /// </summary>
        /// <param name="writer">The output stream to write content to.</param>
        /// <param name="pricePolicy">The price policy object to export.</param>
        /// <param name="path">The name path of the price policy.</param>
        private void WritePolicy(StreamWriter writer, CPricePolicy pricePolicy, string path)
        {
            if (path.Length > 0)
            {
                path += " ";
            }

            path += pricePolicy.PricePolicyDescription;

            writer.WriteLine($"Policy Id,{pricePolicy.PricePolicyId},Policy Name,{Safe(path)}");

            foreach (var r in pricePolicy.Rules)
            {
                string rateAdj = this.Safe(r.Consequence.RateAdjustTarget.AdjustmentText);
                string feeAdj = this.Safe(r.Consequence.FeeAdjustTarget.AdjustmentText);
                string marginAdj = this.Safe(r.Consequence.MarginAdjustTarget.AdjustmentText);
                string qrateAdj = this.Safe(r.Consequence.QrateAdjustTarget.AdjustmentText);
                string trateAdj = this.Safe(r.Consequence.TeaserRateAdjustTarget.AdjustmentText);

                writer.WriteLine($"Rule Id,{r.RuleId},Rule Name,{Safe(r.Description)},{rateAdj},{feeAdj},{marginAdj},{qrateAdj},{trateAdj}");
            }

            List<Guid> childPolicyIdList = new List<Guid>();

            SqlParameter[] parameters = 
            {
                new SqlParameter("@PricePolicyId", pricePolicy.PricePolicyId)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListPricePolicyChildrenInfo", parameters))
            {
                while (reader.Read())
                {
                    childPolicyIdList.Add((Guid)reader["PricePolicyId"]);
                }
            }

            foreach (Guid pricePolicyId in childPolicyIdList)
            {
                writer.WriteLine();
                CPricePolicy childPolicy = CPricePolicy.Retrieve(pricePolicyId);
                this.WritePolicy(writer, childPolicy, path + " \\");
            }
        }

        /// <summary>
        /// Return a safe string value for a cell in csv. If a string contains comma then return string enclose in double quote.
        /// </summary>
        /// <param name="str">A string value.</param>
        /// <returns>A string value to be use in csv.</returns>
        private string Safe(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }

            if (str.IndexOf(",") != -1)
            {
                return "\"" + str + "\"";
            }
            else
            {
                return str;
            }
        }
    }
}