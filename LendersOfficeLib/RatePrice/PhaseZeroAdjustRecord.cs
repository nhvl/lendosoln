﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using DataAccess;
using MyAdjustTarget = LendersOfficeApp.los.RatePrice.RtAdjustTarget;
using MyRuleList = LendersOfficeApp.los.RatePrice.RtRuleList;
using MyRule = LendersOfficeApp.los.RatePrice.AbstractRtRule;
using MyConsequence = LendersOfficeApp.los.RatePrice.RtConsequence;
using MyPricePolicy = LendersOfficeApp.los.RatePrice.AbstractRtPricePolicy;
using MyCondition = LendersOfficeApp.los.RatePrice.RtCondition;
using LendersOffice.Constants;

namespace LendersOfficeApp.los.RatePrice
{
    public class PhaseZeroAdjustRecord
    {
        public class PhaseZeroValues4Applicant
        {
            PhaseZeroAdjustRecord m_adjustScores;
            PhaseZeroBaseScores m_baseScores;
            CApplicantPrice m_applicant;
            Hashtable m_cacheQscoreRecords;

            public PhaseZeroValues4Applicant(CPageData loanFileData, CApplicantPrice applicant, Hashtable cacheQscoreRecords)
            {
                m_cacheQscoreRecords = cacheQscoreRecords;
                m_adjustScores = new PhaseZeroAdjustRecord(Guid.Empty);
                m_baseScores.ReadFrom(loanFileData);
                m_applicant = applicant;
            }

            void SumUp()
            {
                m_baseScores.qcltv += m_adjustScores.m_qcltvAdj;
                m_baseScores.qhcltv += m_adjustScores.m_qhcltvAdj;
                m_baseScores.qltv += m_adjustScores.m_qltvAdj;
                m_baseScores.qlamt += m_adjustScores.m_qlamtAdj;
                m_baseScores.lockDaysAdj += m_adjustScores.m_lockDaysAdj;

                if (m_adjustScores.ModifiedQscore)
                    m_baseScores.qscore = m_adjustScores.m_qscore;

                m_adjustScores.m_qcltvAdj = m_adjustScores.m_qhcltvAdj = m_adjustScores.m_qltvAdj = m_adjustScores.m_qlamtAdj = 0;
                m_adjustScores.m_lockDaysAdj = 0;
            }

            public string CopyPhaseZeroValuesToLoanData(CPageData loanFileData, List<CAdjustItem> adjDescsHidden, bool isComplete, out decimal phaseZeroLockDaysAdj)
            {
                m_baseScores.WriteTo(loanFileData);
                phaseZeroLockDaysAdj = m_baseScores.lockDaysAdj; // OPM 229841
                foreach (CAdjustItem adjustItem in m_adjustScores.m_adjDescsHidden)
                    adjDescsHidden.Add(adjustItem);

                if (isComplete == false)
                    return "";

                StringBuilder strQValueDebug = new StringBuilder(200);

                if (-1000 == m_baseScores.qcltv)
                    strQValueDebug.Append("QCLTV=;");
                else
                    strQValueDebug.AppendFormat("QCLTV={0};", m_baseScores.qcltv.ToString());

                if (-1000 == m_baseScores.qhcltv)
                    strQValueDebug.Append("QHCLTV=;");
                else
                    strQValueDebug.AppendFormat("QHCLTV={0};", m_baseScores.qhcltv.ToString());

                if (-1000 == m_baseScores.qltv)
                    strQValueDebug.Append("QLTV=;");
                else
                    strQValueDebug.AppendFormat("QLTV={0};", m_baseScores.qltv.ToString());

                if (-1000 == m_baseScores.qlamt)
                    strQValueDebug.Append("QLAMT=;");
                else
                    strQValueDebug.AppendFormat("QLAMT={0};", m_baseScores.qlamt.ToString());

                if (-1000 == m_baseScores.qscore)
                    strQValueDebug.Append("QSCORE=;");
                else
                    strQValueDebug.AppendFormat("QSCORE={0};", m_baseScores.qscore.ToString());

                if (0 == m_baseScores.lockDaysAdj)
                    strQValueDebug.Append("LOCKDAYSADJ=;");
                else
                    strQValueDebug.AppendFormat("LOCKDAYSADJ={0};", m_baseScores.lockDaysAdj.ToString());

                string qvalueId = strQValueDebug.ToString();

                strQValueDebug.AppendFormat("(<a href='javascript:void(0)' onclick='return f_copyToClipboard(\"{0}\");'>copy</a>)", strQValueDebug.ToString());

                adjDescsHidden.Add(new CAdjustItem(strQValueDebug.ToString()));

                return qvalueId;
            }

            static public void ComputePhaseZeroValues(MyPricePolicy pricePolicy, XmlDocument xmlDocEval, CSymbolTable symbolTable,
                                              EvalStack stack, PhaseZeroValues4Applicant finalResult, E_sPricingModeT pricingModeT,
                                            StringBuilder debugInfo, bool isLogDetailInfo)
            {
                Guid policyId = pricePolicy.PricePolicyId;
                PhaseZeroAdjustRecord invariantRecord = (PhaseZeroAdjustRecord)finalResult.m_cacheQscoreRecords[policyId];

                bool hasCacheResult;
                if (invariantRecord == null)
                {
                    invariantRecord = new PhaseZeroAdjustRecord(policyId);
                    hasCacheResult = false;
                }
                else
                {
                    Tools.Assert(invariantRecord.FinishCalc == true, "PhaseZeroRecord : expect FinishCalc = false when calling ComputePhaseZeroScore()");
                    hasCacheResult = true;
                }

                pricePolicy.LoadRules();
                MyRuleList rules = pricePolicy.Rules;

                // step 1 : run invariant-rules and put results in cached record
                if (invariantRecord.FinishCalc == false)
                {
                    for (int i = 0; i < pricePolicy.LastIdxOfInvariant; ++i)
                    {
                        MyRule rule = rules.GetRule(i);
                        if (rule.Consequence.HasQConsequence)
                            invariantRecord.RunOneRule(pricePolicy, rule, xmlDocEval, symbolTable, stack, pricingModeT, debugInfo, isLogDetailInfo);
                    }
                    invariantRecord.FinishCalc = true;
                }

                // step 2 : add cache result into finalResult
                finalResult.m_adjustScores.AddResult(finalResult.m_applicant, ref finalResult.m_baseScores, invariantRecord);

                //step 3 : run  variant-rules
                for (int i = pricePolicy.LastIdxOfInvariant; i < rules.Count; i++)
                {
                    MyRule rule = rules.GetRule(i);
                    finalResult.m_adjustScores.RunOneRule(pricePolicy, rule, xmlDocEval, symbolTable, stack, pricingModeT, debugInfo, isLogDetailInfo);
                }

                finalResult.SumUp();

                // save result to cache
                if (hasCacheResult == false)
                {
                    finalResult.m_cacheQscoreRecords[policyId] = invariantRecord;
                }

            }


        }


        decimal m_qcltvAdj, m_qltvAdj, m_qlamtAdj, m_lockDaysAdj, m_qhcltvAdj;
        int m_qscore = -1000;
        string m_qscoreRuleInfo;

        bool m_finishCalc;
        ArrayList m_adjDescsHidden = new ArrayList();

        Guid m_policyId;
        string m_policyDesc = ""; // will remove
        string m_1stAppliedRuleInfo;



        public PhaseZeroAdjustRecord(Guid policyId)
        {
            m_policyId = policyId;
        }

        void SetAppliedRule(MyRule rule)
        {
            if (m_1stAppliedRuleInfo == null)
            {
                m_1stAppliedRuleInfo = string.Format("rule id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}",
                                                      rule.RuleId.ToString(), rule.Description, m_policyId, m_policyDesc);
            }
        }

        public bool FinishCalc
        {
            get { return m_finishCalc; }
            set { m_finishCalc = value; }
        }

        bool ModifiedQscore
        {
            get { return m_qscoreRuleInfo != null; }
        }


        void RunOneRule(MyPricePolicy pricePolicy, MyRule rule, XmlDocument xmlDocEval, CSymbolTable symbolTable, EvalStack stack, E_sPricingModeT sPricingModeT, StringBuilder debugInfo, bool isLogDetailInfo)
        {
            if (rule.IsLenderRule == true && sPricingModeT == E_sPricingModeT.InternalInvestorPricing)
            {
                // 1/15/2011 dd - OPM 30649 - When run pricing in Investor Pricing Mode we do not include rule that flag as "Is Lender Rule"
                return;
            }

            Guid policyId = pricePolicy.PricePolicyId;
            MyConsequence cons = rule.Consequence;
            if (cons.HasQConsequence == false)
                return;

            Tools.Assert(FinishCalc == false, "PhaseZeroRecord : expect FinishCalc = false when calling RunOneRule()");

            MyCondition condition = rule.Condition;

            try
            {
                StringBuilder subDebugInfo = new StringBuilder();
                bool bEvalVal = condition.Evaluate(xmlDocEval, symbolTable, stack, subDebugInfo, isLogDetailInfo);

                if (isLogDetailInfo && (bEvalVal || !ConstSite.PricingCompactLogEnabled))
                {
                    debugInfo.AppendLine("    PhaseZeroAdjustRecord.RunOneRule - PolicyId:" + pricePolicy.PricePolicyId + ", RuleId:" + rule.RuleId + ", Rule Desc:[" + rule.Description + "] - Result: " + bEvalVal);
                    debugInfo.AppendLine(subDebugInfo.ToString());
                }
                if (bEvalVal == false)
                    return;
            }
            catch (Exception ex)
            {
                string regTestMsg = string.Format("Error encountered in phase 0 (Q values) condition for rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}.",
                    rule.RuleId.ToString(), rule.Description, policyId, pricePolicy.PricePolicyDescription, ex.Message);

                //PrepareInsufficientInfoStatus(ex, regTestMsg, rule.RuleId, policyId);
                //return; // We can stop the execution now, the program's result is "insufficient data"

                throw new PricingCalcException(ex, regTestMsg, rule.RuleId, policyId);
            }

#if postProcess
                if( -1000 == qcltv )
                {
                    string errStr = "CLTV is not valid for this loan file, please check the appraisal value and the sale price of the property.";
                    if( !m_errors.Contains( errStr ) )
                    {
                        Tools.LogRegTest( string.Format( "Error encountered in phase 0 (Q values), CLTV for the loan file is undefined, executing rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                            rule.RuleId.ToString(), rule.Description, policyId, pricePolicy.PricePolicyDescription, errStr ) );
                        m_errors.Add( errStr, null );
                        m_developerErrors.Add(string.Format("{0}. RuleId={1}, PricePolicyId={2}", errStr, rule.RuleId, policyId), null);
                    }
                    m_evalStatus = E_EvalStatus.Eval_InsufficientInfo;  
                    return;
                }

                if( -1000 == qhcltv )
                {
                    string errStr = "HCLTV is not valid for this loan file, please check the appraisal value and the sale price of the property.";
                    if( !m_errors.Contains( errStr ) )
                    {
                        Tools.LogRegTest( string.Format( "Error encountered in phase 0 (Q values), HCLTV for the loan file is undefined, executing rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                            rule.RuleId.ToString(), rule.Description, policyId, pricePolicy.PricePolicyDescription, errStr ) );
                        m_errors.Add( errStr, null );
                        m_developerErrors.Add(string.Format("{0}. RuleId={1}, PricePolicyId={2}", errStr, rule.RuleId, policyId), null);
                    }
                    m_evalStatus = E_EvalStatus.Eval_InsufficientInfo;  
                    return;
                }
#endif


            try
            {
                decimal qcltvAdj = cons.QcltvAdjustTarget.Evaluate(xmlDocEval, symbolTable, stack);

                if (qcltvAdj != 0)
                {
                    m_qcltvAdj += qcltvAdj;
                    // QCLTV adjustment will be used for QHCLTV too. gf opm 224873
                    m_qhcltvAdj += qcltvAdj;
                    CAdjustItem feeAdjDesc = new CAdjustItem(string.Format("Add to QCLTV and QHCLTV by {0}, Policy_{1} (<a href='javascript:void(0)' onclick='return f_copyToClipboard(\"{1}\");'>copy</a>), Rule_{2} (<a href='javascript:void(0)' onclick='return f_copyToClipboard(\"{2}\");'>copy</a>)"
                                                      , qcltvAdj.ToString(), policyId.ToString(), rule.RuleId.ToString()));
                    m_adjDescsHidden.Add(feeAdjDesc);

                    if (isLogDetailInfo)
                    {
                        debugInfo.AppendLine("        Add to QCLTV and QHCLTV by " + qcltvAdj);
                    }
                }

            }
            catch (Exception ex)
            {
                string regTestMsg = string.Format("Error encountered in phase 0 (Q values) QcltvAdjustTarget consequence for rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                    rule.RuleId.ToString(), rule.Description, pricePolicy.PricePolicyId, pricePolicy.PricePolicyDescription, ex.Message);
                //PrepareInsufficientInfoStatus(ex, regTestMsg, rule.RuleId, policyId);
                //return; // stop the execution immediately.
                throw new PricingCalcException(ex, regTestMsg, rule.RuleId, policyId);

            }


#if postProcess
                if( -1000 == qltv )
                {
                    string errStr = "LTV is not valid for this loan file, please check the appraisal value and the sale price of the property.";
                    if( !m_errors.Contains( errStr ) )
                    {
                        Tools.LogRegTest( string.Format( "Error encountered in phase 0 (Q values), LTV for the loan file is undefined, executing rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                            rule.RuleId.ToString(), rule.Description, policyId, pricePolicy.PricePolicyDescription, errStr ) );
                        m_errors.Add( errStr, null );
                        m_developerErrors.Add(string.Format("{0}. RuleId={1}, PricePolicyId={2}", errStr, rule.RuleId, policyId), null);
                    }
                    m_evalStatus = E_EvalStatus.Eval_InsufficientInfo;  
                    return;
                }
#endif


            try
            {
                decimal newQltvAdjustment = cons.QltvAdjustTarget.Evaluate(xmlDocEval, symbolTable, stack);
                /* TO DEBUG THE Q RULE SETTING, TURN THIS ONE ON
                    if( newQltvAdjustment != 0 )
                    {
                        Tools.LogRegTest( string.Format( "Product name={6} id={7}. Adjusting qltv from {4} by {5} in phase 0 (Q values) QltvAdjustTarget consequece for rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}",
                            rule.RuleId.ToString(), rule.Description, pricePolicy.PricePolicyId, pricePolicy.PricePolicyDescription, qltv.ToString(), newQltvAdjustment.ToString(), lLpTemplateNm, lLpTemplateId.ToString() ) );
                    }
                    */


                if (newQltvAdjustment != 0)
                {
                    m_qltvAdj += newQltvAdjustment;
                    CAdjustItem feeAdjDesc = new CAdjustItem(string.Format("Add to QLTV by {0}, Policy_{1} (<a href='javascript:void(0)' onclick='return f_copyToClipboard(\"{1}\");'>copy</a>), Rule_{2} (<a href='javascript:void(0)' onclick='return f_copyToClipboard(\"{2}\");'>copy</a>)"
                                               , newQltvAdjustment.ToString(), policyId.ToString(), rule.RuleId.ToString()));
                    m_adjDescsHidden.Add(feeAdjDesc);

                    if (isLogDetailInfo)
                    {
                        debugInfo.AppendLine("        Add to QLTV by " + newQltvAdjustment);
                    }

                }

            }
            catch (Exception ex)
            {
                string regTestMsg = string.Format("Error encountered in phase 0 (Q values) QltvAdjustTarget consequence for rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                    rule.RuleId.ToString(), rule.Description, pricePolicy.PricePolicyId, pricePolicy.PricePolicyDescription, ex.Message);
                //PrepareInsufficientInfoStatus(ex, regTestMsg, rule.RuleId, policyId);
                //return; // stop the execution immediately.

                throw new PricingCalcException(ex, regTestMsg, rule.RuleId, policyId);
            }


#if postProcess
                if( -1000 == qlamt )
                {
                    string errStr = "Loan amount is not valid for this loan file.";
                    if( !m_errors.Contains( errStr ) )
                    {
                        Tools.LogRegTest( string.Format( "Error encountered in phase 0 (Q values), Loan amount for the loan file is undefined, executing rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                            rule.RuleId.ToString(), rule.Description, policyId, pricePolicy.PricePolicyDescription, errStr ) );
                        m_errors.Add( errStr, null );
                        m_developerErrors.Add(string.Format("{0}. RuleId={1}, PricePolicyId={2}", errStr, rule.RuleId, policyId), null);
                    }
                    m_evalStatus = E_EvalStatus.Eval_InsufficientInfo;  
                    return;
                }
#endif

            try
            {
                decimal newQlamtAdjustment = cons.QLAmtAdjustTarget.Evaluate(xmlDocEval, symbolTable, stack);
                if (newQlamtAdjustment != 0)
                {
                    m_qlamtAdj += newQlamtAdjustment;
                    CAdjustItem feeAdjDesc = new CAdjustItem(string.Format("Add to QLAMT  by {0}, Policy_{1} (<a href='javascript:void(0)' onclick='return f_copyToClipboard(\"{1}\");'>copy</a>), Rule_{2} (<a href='javascript:void(0)' onclick='return f_copyToClipboard(\"{2}\");'>copy</a>)"
                                                   , newQlamtAdjustment.ToString(), policyId.ToString(), rule.RuleId.ToString()));
                    m_adjDescsHidden.Add(feeAdjDesc);

                    if (isLogDetailInfo)
                    {
                        debugInfo.AppendLine("        Add to QLTV by " + newQlamtAdjustment);
                    }
                }
            }
            catch (Exception ex)
            {
                string regTestMsg = string.Format("Error encountered in phase 0 (Q values) QLAmtAdjustTarget consequence for rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                    rule.RuleId.ToString(), rule.Description, pricePolicy.PricePolicyId, pricePolicy.PricePolicyDescription, ex.Message);
                //PrepareInsufficientInfoStatus(ex, regTestMsg, rule.RuleId, policyId);
                //return; // stop the execution immediately.

                throw new PricingCalcException(ex, regTestMsg, rule.RuleId, policyId);

            }

#if postProcess
                if( -1000 == qscore )
                {
                    string errStr = "Score type 1 is not valid for this loan file, please check the raw scores.";
                    if( !m_errors.Contains( errStr ) )
                    {
                        Tools.LogRegTest( string.Format( "Error encountered in phase 0 (Q values), score type 1 for the loan file is undefined, executing rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                            rule.RuleId.ToString(), rule.Description, policyId, pricePolicy.PricePolicyDescription, errStr ) );
                        m_errors.Add( errStr, null );
                        m_developerErrors.Add(string.Format("{0}. RuleId={1}, PricePolicyId={2}", errStr, rule.RuleId, policyId), null);
                    }
                    m_evalStatus = E_EvalStatus.Eval_InsufficientInfo;  
                    return;
                }
#endif

            try
            {
                MyAdjustTarget qscoreTarget = cons.QscoreTarget;
                if (!qscoreTarget.IsBlank)
                {
                    string ruleInfo = string.Format("rule id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}",
                                                     rule.RuleId.ToString(), rule.Description, policyId, pricePolicy.PricePolicyDescription);

                    if (m_qscoreRuleInfo != null)
                        throw new ModifyQScoreException(m_qscoreRuleInfo, ruleInfo);
                    m_qscoreRuleInfo = ruleInfo;

                    m_qscore = (int)cons.QscoreTarget.Evaluate(xmlDocEval, symbolTable, stack);

                    CAdjustItem feeAdjDesc = new CAdjustItem(string.Format("Set to QSCORE {0}, Policy_{1} (<a href='javascript:void(0)' onclick='return f_copyToClipboard(\"{1}\");'>copy</a>), Rule_{2} (<a href='javascript:void(0)' onclick='return f_copyToClipboard(\"{2}\");'>copy</a>)"
                                                              , m_qscore.ToString(), policyId.ToString(), rule.RuleId.ToString()));
                    m_adjDescsHidden.Add(feeAdjDesc);

                    if (isLogDetailInfo)
                    {
                        debugInfo.AppendLine("        Set QSCORE " + m_qscore);
                    }
                }
            }
            catch (Exception ex)
            {
                string regTestMsg = string.Format("Error encountered in phase 0 (Q values) QscoreTarget consequence for rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                    rule.RuleId.ToString(), rule.Description, pricePolicy.PricePolicyId, pricePolicy.PricePolicyDescription, ex.Message);
                //PrepareInsufficientInfoStatus(ex, regTestMsg, rule.RuleId, policyId);
                //return; // stop the execution immediately.
                throw new PricingCalcException(ex, regTestMsg, rule.RuleId, policyId);
            }


            try
            {
                if (sPricingModeT != E_sPricingModeT.InternalInvestorPricing) // OPM 69936 -- Ignore the lock days adjustment when internal pricing mode.
                {
                    decimal newLockDaysAdjustment = cons.LockDaysAdjustTarget.Evaluate(xmlDocEval, symbolTable, stack);
                    /* TO DEBUG THE Q RULE SETTING, TURN THIS ONE ON
                        if( newQltvAdjustment != 0 )
                        {
                            Tools.LogRegTest( string.Format( "Product name={6} id={7}. Adjusting qltv from {4} by {5} in phase 0 (Q values) QltvAdjustTarget consequece for rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}",
                                rule.RuleId.ToString(), rule.Description, pricePolicy.PricePolicyId, pricePolicy.PricePolicyDescription, qltv.ToString(), newQltvAdjustment.ToString(), lLpTemplateNm, lLpTemplateId.ToString() ) );
                        }
                        */


                    if (newLockDaysAdjustment != 0)
                    {
                        m_lockDaysAdj += newLockDaysAdjustment;
                        CAdjustItem feeAdjDesc = new CAdjustItem(string.Format("Add {0} to LockDays, Policy_{1} (<a href='javascript:void(0)' onclick='return f_copyToClipboard(\"{1}\");'>copy</a>), Rule_{2} (<a href='javascript:void(0)' onclick='return f_copyToClipboard(\"{2}\");'>copy</a>)"
                                                   , newLockDaysAdjustment.ToString(), policyId.ToString(), rule.RuleId.ToString()));
                        m_adjDescsHidden.Add(feeAdjDesc);

                        if (isLogDetailInfo)
                        {
                            debugInfo.AppendLine("        Add to Lock Days Adjustment by " + newLockDaysAdjustment);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                string regTestMsg = string.Format("Error encountered in phase 0 (Q values) LockDaysAdjustTarget consequence for rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                    rule.RuleId.ToString(), rule.Description, pricePolicy.PricePolicyId, pricePolicy.PricePolicyDescription, ex.Message);
                //PrepareInsufficientInfoStatus(ex, regTestMsg, rule.RuleId, policyId);
                //return; // stop the execution immediately.

                throw new PricingCalcException(ex, regTestMsg, rule.RuleId, policyId);
            }


            SetAppliedRule(rule);
        }


        void AddResult(CApplicantPrice applicant, ref PhaseZeroBaseScores baseScores, PhaseZeroAdjustRecord adjustRecord)
        {
            Tools.Assert(adjustRecord.FinishCalc, "Expect FinishCalc == false when calling PhaseZeroRecord.AddResult( PhaseZeroRecord cachedResult )");
            Tools.Assert(m_policyId == Guid.Empty, "Expect m_policyId == Guid.Empty when calling PhaseZeroRecord.AddResult( PhaseZeroRecord cachedResult )");

            if (adjustRecord.m_1stAppliedRuleInfo == null)
                return;
            applicant.VerifyBasePhaseZeroValues(ref baseScores, adjustRecord.m_1stAppliedRuleInfo);

            m_qcltvAdj += adjustRecord.m_qcltvAdj;
            m_qhcltvAdj += adjustRecord.m_qhcltvAdj;
            m_qltvAdj += adjustRecord.m_qltvAdj;
            m_qlamtAdj += adjustRecord.m_qlamtAdj;
            m_lockDaysAdj += adjustRecord.m_lockDaysAdj;

            foreach (CAdjustItem adjustItem in adjustRecord.m_adjDescsHidden)
                m_adjDescsHidden.Add(adjustItem);

            if (m_1stAppliedRuleInfo == null)
                m_1stAppliedRuleInfo = adjustRecord.m_1stAppliedRuleInfo;


            if (adjustRecord.ModifiedQscore)
            {
                if (m_qscoreRuleInfo != null)
                    throw new ModifyQScoreException(m_qscoreRuleInfo, adjustRecord.m_qscoreRuleInfo);

                m_qscoreRuleInfo = adjustRecord.m_qscoreRuleInfo;
                m_qscore = adjustRecord.m_qscore;
            }
        }



    }

}
