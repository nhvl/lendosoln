﻿namespace LendersOffice.RatePrice
{
    using System;
    using System.IO;
    using LendersOffice.Drivers.FileSystem;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// A cache layer for price policy, rate options and snapshot. This will check local folder for data before go to actual source to retrieve real data.
    /// Use this to help speed up pricing using file based snapshot. Since it does not have to hit FileDB3 or S3 each time to get price policy, rate options.
    /// I only expect this to use in read-only operations therefore I will not implement a save.
    /// </summary>
    public class CachePriceEngineStorage : IPriceEngineStorage
    {
        /// <summary>
        /// The root parent for temp folder.
        /// </summary>
        private LocalFilePath rootPath;

        /// <summary>
        /// The actual price engine storage to retrieve data.
        /// </summary>
        private IPriceEngineStorage internalPriceEngineStorage;

        /// <summary>
        /// Initializes a new instance of the <see cref="CachePriceEngineStorage" /> class.
        /// </summary>
        /// <param name="root">The root parent.</param>
        /// <param name="priceEngineStorage">Actual pricing engine storage implementation.</param>
        public CachePriceEngineStorage(LocalFilePath root, IPriceEngineStorage priceEngineStorage)
        {
            this.rootPath = root;
            this.internalPriceEngineStorage = priceEngineStorage;
        }

        /// <summary>
        /// Retrieve the file from persistent storage. Throw FileNotFoundException if key is not in storage.
        /// </summary>
        /// <param name="type">The type of file to retrieve.</param>
        /// <param name="key">The key of file to retrieve.</param>
        /// <returns>The local file path contains the file.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Use rethrow")]
        public LocalFilePath Retrieve(PriceEngineStorageType type, SHA256Checksum key)
        {
            Func<Exception, bool> canIgnoreException = (Exception exc) =>
            {
                return exc is IOException || exc.InnerException is IOException || 
                       exc is UnauthorizedAccessException || exc.InnerException is UnauthorizedAccessException;
            };

            string destination = Path.Combine(this.rootPath.Value, type.ToString(), key.Value);

            LocalFilePath path = LocalFilePath.Create(destination).Value;

            if (FileOperationHelper.Exists(path))
            {
                return path;
            }

            var newPath = this.internalPriceEngineStorage.Retrieve(type, key);

            // Check for file existing one more time. Other thread could pull the same file and store in local path.
            if (!FileOperationHelper.Exists(path))
            {
                bool removeTmpFile = false;
                LocalFilePath tmpPath;

                try
                {
                    tmpPath = LocalFilePath.Create(destination + ".tmp").Value;
                    FileOperationHelper.Copy(newPath, tmpPath, allowOverwrite: false);
                    removeTmpFile = true;
                    File.Move(tmpPath.Value, path.Value);
                }
                catch (Exception exc)
                {
                    if (removeTmpFile)
                    {
                        try
                        {
                            FileOperationHelper.Delete(tmpPath);
                        }
                        catch (Exception otherExc)
                        {
                            if (!canIgnoreException(otherExc))
                            {
                                throw;
                            }
                        }
                    }

                    if (!canIgnoreException(exc))
                    {
                        throw;
                    }

                    return newPath;
                }
            }

            return path;
        }

        /// <summary>
        /// Save the file in local path to persistent storage.
        /// </summary>
        /// <param name="type">The type of file to save.</param>
        /// <param name="key">The key of file to save.</param>
        /// <param name="path">The local path of the file save.</param>
        public void Save(PriceEngineStorageType type, SHA256Checksum key, LocalFilePath path)
        {
            // dd 7/18/2017 - This class should only be use for pricing execution. It is only support read operation.
            throw new NotImplementedException();
        }
    }
}
