﻿namespace LendersOffice.RatePrice.Model
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents individual ResultPricing result.
    /// This class primary purpose is to serialize and deserialize to json. Do not add any logics to this.
    /// </summary>
    [DataContract]
    public class ResultPricing
    {
        /// <summary>
        /// Gets or sets the eligible groups value.
        /// </summary>
        /// <value>The eligible groups value.</value>
        [DataMember]
        public List<ResultGroup> EligibleGroups { get; set; }

        /// <summary>
        /// Gets or sets the insufficient groups value.
        /// </summary>
        /// <value>The insufficient groups value.</value>
        [DataMember]
        public List<ResultGroup> InsufficientGroups { get; set; }

        /// <summary>
        /// Gets or sets the ineligible groups.
        /// </summary>
        /// <value>The ineligible groups value.</value>
        [DataMember]
        public List<ResultGroup> IneligibleGroups { get; set; }
    }
}
