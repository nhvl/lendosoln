﻿namespace LendersOffice.RatePrice.Model
{
    /// <summary>
    /// Represents individual rate option.
    /// This class primary purpose is to serialize and deserialize to json. Do not add any logics to this.
    /// </summary>
    public class RateOption
    {
        /// <summary>
        /// Gets or sets rate value.
        /// </summary>
        /// <value>The rate value.</value>
        public string Rate { get; set; }

        /// <summary>
        /// Gets or sets point value.
        /// </summary>
        /// <value>The point value.</value>
        public string Point { get; set; }

        /// <summary>
        /// Gets or sets margin value.
        /// </summary>
        /// <value>The margin value.</value>
        public string Margin { get; set; }

        /// <summary>
        /// Gets or sets qrate base value.
        /// </summary>
        /// <value>Qrate base value.</value>
        public string QRateBase { get; set; }

        /// <summary>
        /// Gets or sets teaser rate value.
        /// </summary>
        /// <value>Teaser rate value.</value>
        public string TeaserRate { get; set; }
    }
}