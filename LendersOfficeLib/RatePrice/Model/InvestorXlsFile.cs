﻿namespace LendersOffice.RatePrice.Model
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Represents individual record in INVESTOR_XLS_FILE table.
    /// THis class primary purpose is to serialize and deserialize to json. Do not add logics to this.
    /// </summary>
    public class InvestorXlsFile
    {
        /// <summary>
        /// Gets or sets investor xls file id.
        /// </summary>
        /// <value>The investor xls file id.</value>
        public long InvestorXlsFileId { get; set; }

        /// <summary>
        /// Gets or sets investor xls file name.
        /// </summary>
        /// <value>The investor xls file name.</value>
        public string InvestorXlsFileName { get; set; }

        /// <summary>
        /// Gets or sets the worksheet name contains effective date.
        /// </summary>
        /// <value>The worksheet name contains effective date.</value>
        public string EffectiveDateTimeWorksheetName { get; set; }

        /// <summary>
        /// Gets or sets the land mark text.
        /// </summary>
        /// <value>The land mark text.</value>
        public string EffectiveDateTimeLandMarkText { get; set; }

        /// <summary>
        /// Gets or sets the row offset.
        /// </summary>
        /// <value>The row offset.</value>
        public int EffectiveDateTimeRowOffset { get; set; }

        /// <summary>
        /// Gets or sets the column offset.
        /// </summary>
        /// <value>The column offset.</value>
        public int EffectiveDateTimeColumnOffset { get; set; }

        /// <summary>
        /// Gets or sets file version date.
        /// </summary>
        /// <value>The file version date.</value>
        public DateTime FileVersionDateTimeInUtc { get; set; }
    }
}