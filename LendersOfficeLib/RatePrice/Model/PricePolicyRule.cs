﻿namespace LendersOffice.RatePrice.Model
{
    using System;

    /// <summary>
    /// Represents individual price policy rule.
    /// This class primary purpose is to serialize and deserialize to json. Do not add any logics to this.
    /// </summary>
    public class PricePolicyRule
    {
        /// <summary>
        /// Gets or sets the id of the rule.
        /// </summary>
        /// <value>Id of the rule.</value>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the rule description.
        /// </summary>
        /// <value>The rule description.</value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets qbc value.
        /// </summary>
        /// <value>The qbc value.</value>
        public string QBC { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it is lender rule.
        /// </summary>
        /// <value>Whether it is lender rule.</value>
        public bool IsLenderRule { get; set; }

        /// <summary>
        /// Gets or sets the condition logic.
        /// </summary>
        /// <value>The condition logic.</value>
        public string Condition { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it is skip rule.
        /// </summary>
        /// <value>Is skip rule.</value>
        public bool Skip { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it is disqualify rule.
        /// </summary>
        /// <value>Is disqualify rule.</value>
        public bool Disqualify { get; set; }

        /// <summary>
        /// Gets or sets stipulation.
        /// </summary>
        /// <value>Stipulation of the rule.</value>
        public string Stipulation { get; set; }

        /// <summary>
        /// Gets or sets fee adjustment.
        /// </summary>
        /// <value>Fee adjustment.</value>
        public string AdjustFee { get; set; }

        /// <summary>
        /// Gets or sets margin adjustment.
        /// </summary>
        /// <value>Margin adjustment.</value>
        public string AdjustMargin { get; set; }

        /// <summary>
        /// Gets or sets rate adjustment.
        /// </summary>
        /// <value>Rate adjustment.</value>
        public string AdjustRate { get; set; }

        /// <summary>
        /// Gets or sets base margin adjustment.
        /// </summary>
        /// <value>Base margin adjustment.</value>
        public string AdjustBaseMagin { get; set; }

        /// <summary>
        /// Gets or sets qltv adjustment.
        /// </summary>
        /// <value>Qltv adjustment.</value>
        public string AdjustQltv { get; set; }

        /// <summary>
        /// Gets or sets qcltv adjustment.
        /// </summary>
        /// <value>Qcltv adjustment.</value>
        public string AdjustQcltv { get; set; }

        /// <summary>
        /// Gets or sets qscore adjustment.
        /// </summary>
        /// <value>Qscore adjustment.</value>
        public string AdjustQscore { get; set; }

        /// <summary>
        /// Gets or sets max front ysp adjustment.
        /// </summary>
        /// <value>Max front ysp adjustment.</value>
        public string AdjustMaxFrontEndYsp { get; set; }

        /// <summary>
        /// Gets or sets max ysp adjustment.
        /// </summary>
        /// <value>Max ysp adjustment.</value>
        public string AdjustMaxYsp { get; set; }

        /// <summary>
        /// Gets or sets qrate adjustment.
        /// </summary>
        /// <value>Qrate adjustment.</value>
        public string AdjustQrate { get; set; }

        /// <summary>
        /// Gets or sets max dti adjustment.
        /// </summary>
        /// <value>Max DTI adjustment.</value>
        public string AdjustMaxDti { get; set; }

        /// <summary>
        /// Gets or sets QLAmt adjustment.
        /// </summary>
        /// <value>QLAmt adjustment.</value>
        public string AdjustQLAmt { get; set; }

        /// <summary>
        /// Gets or sets Qrate adjust.
        /// </summary>
        /// <value>Qrate adjust.</value>
        public string AdjustQrateAdjust { get; set; }

        /// <summary>
        /// Gets or sets teaser rate adjust.
        /// </summary>
        /// <value>Teaser rate adjust.</value>
        public string AdjustTeaserRateAdjust { get; set; }

        /// <summary>
        /// Gets or sets lock days adjust.
        /// </summary>
        /// <value>Lock days adjust.</value>
        public string AdjustLockDaysAdjust { get; set; }
    }
}