﻿namespace LendersOffice.RatePrice.Model
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Represents individual record in LPE_ACCEPTABLE_RS_FILE table.
    /// This class primary purpose is to serialize and deserialize to json. Do not add any logics to this.
    /// </summary>
    public class LpeAcceptableRsFile
    {
        /// <summary>
        /// Gets or sets the acceptable ratesheet name.
        /// </summary>
        /// <value>The acceptable ratesheet name.</value>
        public string LpeAcceptableRsFileId { get; set; }

        /// <summary>
        /// Gets or sets the deployment type.
        /// </summary>
        /// <value>The deployment type.</value>
        public string DeploymentType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether rate sheet map and bot is working.
        /// </summary>
        /// <value>Whether rate sheet map and bot is working.</value>
        public bool IsBothRsMapAndBotWorking { get; set; }

        /// <summary>
        /// Gets or sets the investor xls file id.
        /// </summary>
        /// <value>The investor xls file id.</value>
        public long InvestorXlsFileId { get; set; }
    }
}
