﻿namespace LendersOffice.RatePrice.Model
{
    using System;
    using System.Collections.Generic;
    using ProtoBuf;

    /// <summary>
    /// Represents individual price policy.
    /// This class primary purpose is to serialize and deserialize to protobuf. Do not add any logics to this.
    /// </summary>
    [ProtoContract]
    public class PricePolicyV2
    {
        /// <summary>
        /// Gets or sets the id of policy.
        /// </summary>
        [ProtoMember(1)]
        public Guid PricePolicyId { get; set; }

        /// <summary>
        /// Gets or sets the description of policy.
        /// </summary>
        [ProtoMember(2)]
        public string PricePolicyDescription { get; set; }

        /// <summary>
        /// Gets or sets the id of parent policy.
        /// </summary>
        [ProtoMember(3)]
        public Guid ParentPolicyId { get; set; }

        /// <summary>
        /// Gets or sets mutual exclusive sort id.
        /// </summary>
        [ProtoMember(4)]
        public string MutualExclusiveExecSortedId { get; set; }

        /// <summary>
        /// Gets or sets the list of rules.
        /// </summary>
        [ProtoMember(5)]
        public List<PricePolicyRuleV2> Rules { get; set; }
    }
}
