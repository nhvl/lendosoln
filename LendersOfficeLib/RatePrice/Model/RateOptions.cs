﻿namespace LendersOffice.RatePrice.Model
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Represents individual rate option.
    /// This class primary purpose is to serialize and deserialize to json. Do not add any logics to this.
    /// </summary>
    public class RateOptions
    {
        /// <summary>
        /// Gets or sets the download time. Time is in UTC format.
        /// </summary>
        /// <value>The download time.</value>
        public DateTime DownloadTimeInUtc { get; set; }

        /// <summary>
        /// Gets or sets the list of rates.
        /// </summary>
        /// <value>The list of rates.</value>
        public List<RateOption> Rates { get; set; }

        /// <summary>
        /// Gets or sets the rate sheet file id.
        /// </summary>
        /// <value>The rate sheet file id.</value>
        public string RatesheetFileId { get; set; }

        /// <summary>
        /// Gets or sets the rate sheet file version number.
        /// </summary>
        /// <value>The rate sheet file version number.</value>
        public long RatesheetFileVersionNumber { get; set; }
    }
}