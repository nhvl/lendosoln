﻿namespace LendersOffice.RatePrice.Model
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents individual ResultLoanProgram result.
    /// This class primary purpose is to serialize and deserialize to json. Do not add any logics to this.
    /// </summary>
    [DataContract]
    public class ResultLoanProgram
    {
        /// <summary>
        /// Gets or sets Template Identifier value.
        /// </summary>
        /// <value>The Template Identifier value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public Guid lLpTemplateId { get; set; }

        /// <summary>
        /// Gets or sets Template Name value.
        /// </summary>
        /// <value>The Template Name value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string lLpTemplateNm { get; set; }

        /// <summary>
        /// Gets or sets Investor name value.
        /// </summary>
        /// <value>The investor name value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string lLpInvestorNm { get; set; }

        /// <summary>
        /// Gets or sets the version value.
        /// </summary>
        /// <value>The version value.</value>
        [DataMember]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsBlockedRateLockSubmission.
        /// </summary>
        /// <value>The IsBlockedRateLockSubmission value.</value>
        [DataMember]
        public bool IsBlockedRateLockSubmission { get; set; }

        /// <summary>
        /// Gets or sets RateLockSubmissionUserWarningMessage value.
        /// </summary>
        /// <value>The RateLockSubmissionUserWarningMessage value.</value>
        [DataMember]
        public string RateLockSubmissionUserWarningMessage { get; set; }

        /// <summary>
        /// Gets or sets ProductId value.
        /// </summary>
        /// <value>The ProductId value.</value>
        [DataMember]
        public Guid ProductId { get; set; }

        /// <summary>
        /// Gets or sets ProductCode value.
        /// </summary>
        /// <value>The ProductCode value.</value>
        [DataMember]
        public string ProductCode { get; set; }

        /// <summary>
        /// Gets or sets Max Debt to income value.
        /// </summary>
        /// <value>The Max Debt to income value.</value>
        [DataMember]
        public string MaxDti { get; set; }

        /// <summary>
        /// Gets or sets UniqueChecksum value.
        /// </summary>
        /// <value>The UniqueChecksum value.</value>
        [DataMember]
        public string UniqueChecksum { get; set; }

        /// <summary>
        ///  Gets or sets a value indicating whether AreRatesExpired.
        /// </summary>
        /// <value>The AreRatesExpired value.</value>
        [DataMember]
        public bool AreRatesExpired { get; set; }

        /// <summary>
        ///  Gets or sets a value indicating whether IsHistoric.
        /// </summary>
        /// <value>The IsHistoric value.</value>
        [DataMember]
        public bool IsHistoric { get; set; } = true;

        /// <summary>
        /// Gets or sets the merge group name.
        /// </summary>
        /// <value>The merge group name.</value>
        [DataMember]
        public string MergeGroupName { get; set; }

        /// <summary>
        /// Gets or sets ResultRateOptions value.
        /// </summary>
        /// <value>The ResultRateOptions value.</value>
        [DataMember]
        public List<ResultRateOption> ResultRateOptions { get; set; }

        /// <summary>
        /// Gets or sets DisqualifiedRuleList value.
        /// </summary>
        /// <value>The Reasons why a Program is ineligible.</value>
        [DataMember]
        public IEnumerable<string> DisqualifiedRuleList { get; set; }

        /// <summary>
        /// Gets or sets RepresentativeRateOption value.
        /// </summary>
        /// <value>The RepresentativeRateOption value.</value>
        [DataMember]
        public ResultRateOption RepresentativeRateOption { get; set; }

        /// <summary>
        /// Gets or sets RepresentativeHistoricRateOption value.
        /// </summary>
        /// <value>The RepresentativeHistoricRateOption value.</value>
        [DataMember]
        public ResultRateOption RepresentativeHistoricRateOption { get; set; }
    }
}
