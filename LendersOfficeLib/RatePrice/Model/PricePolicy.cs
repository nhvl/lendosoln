﻿namespace LendersOffice.RatePrice.Model
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Represents individual price policy rule.
    /// This class primary purpose is to serialize and deserialize to json. Do not add any logics to this.
    /// </summary>
    public class PricePolicy
    {
        /// <summary>
        /// Gets or sets price policy id.
        /// </summary>
        /// <value>The price policy id.</value>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the created date. Created date is in UTC.
        /// </summary>
        /// <value>Created date in UTC.</value>
        public DateTime CreatedDateInUtc { get; set; }

        /// <summary>
        /// Gets or sets description.
        /// </summary>
        /// <value>Policy's description.</value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets policy's note.
        /// </summary>
        /// <value>Policy not.</value>
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets parent id. 
        /// </summary>
        /// <value>The parent id.</value>
        public Guid ParentId { get; set; }

        /// <summary>
        /// Gets or sets mutual exclusive sorted id.
        /// </summary>
        /// <value>Mutual exclusive sorted id.</value>
        public string MutualExclusiveExecSortedId { get; set; }

        /// <summary>
        /// Gets or sets rule list.
        /// </summary>
        /// <value>Rule list.</value>
        public List<PricePolicyRule> Rules { get; set; }
    }
}