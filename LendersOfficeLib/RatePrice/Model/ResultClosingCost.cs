﻿namespace LendersOffice.RatePrice.Model
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents individual ResultClosingCost result.
    /// This class primary purpose is to serialize and deserialize to json. Do not add any logics to this.
    /// </summary>
    [DataContract]
    public class ResultClosingCost
    {
        /// <summary>
        /// Gets or sets the Amount value.
        /// </summary>
        /// <value>The Amount value.</value>
        [DataMember]
        public string Amount { get; set; }

        /// <summary>
        /// Gets or sets the Description value.
        /// </summary>
        /// <value>The Description value.</value>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the Section value.
        /// </summary>
        /// <value>The Section value.</value>
        [DataMember]
        public string Section { get; set; }

        /// <summary>
        /// Gets or sets the Source value.
        /// </summary>
        /// <value>The Source value.</value>
        [DataMember]
        public string Source { get; set; }
    }
}
