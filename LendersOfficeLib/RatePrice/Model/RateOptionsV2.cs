﻿namespace LendersOffice.RatePrice.Model
{
    using System;
    using System.Collections.Generic;
    using ProtoBuf;

    /// <summary>
    /// Represents collection of rates for a loan program.
    /// This class primary purpose is to serialize and deserialize to protobuf. Do not add any logics to this.
    /// </summary>
    [ProtoContract]
    public class RateOptionsV2
    {
        /// <summary>
        /// Gets or sets id.
        /// </summary>
        [ProtoMember(1)]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the download time.
        /// </summary>
        [ProtoMember(2)]
        public DateTime DownloadTime { get; set; }

        /// <summary>
        /// Gets or sets the list of rates.
        /// </summary>
        [ProtoMember(3)]
        public List<RateOptionV2> Rates { get; set; }

        /// <summary>
        /// Gets or sets the rate sheet file id.
        /// </summary>
        [ProtoMember(4)]
        public string RatesheetFileId { get; set; }

        /// <summary>
        /// Gets or sets the rate sheet file version number.
        /// </summary>
        [ProtoMember(5)]
        public long RatesheetFileVersionNumber { get; set; }
    }
}