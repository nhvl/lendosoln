﻿namespace LendersOffice.RatePrice.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;
    using LendersOffice.RatePrice.FileBasedPricing;
    using ProtoBuf;

    /// <summary>
    /// Product Price Association V2.
    /// </summary>
    [ProtoContract]
    public class ProductPriceAssociationV2
    {
        /// <summary>
        /// Gets or sets the Loan program Id.
        /// </summary>
        [ProtoMember(1)]
        public Guid ProductId { get; set; }

        /// <summary>
        /// Gets or sets the the associated policies.
        /// </summary>
        [ProtoMember(2)]
        public List<PriceAssociationV2> Items { get; set; }

        /// <summary>
        /// Create ProductPriceAssociationV2 object from FileBasedProductPriceAssociation object.
        /// </summary>
        /// <param name="obj">FileBasedProductPriceAssociation object.</param>
        /// <param name="guidToId">The map from guid to index.</param>
        /// <returns>ProductPriceAssociationV2 object.</returns>
        public static ProductPriceAssociationV2 Create(FileBasedProductPriceAssociation obj, Dictionary<Guid, int> guidToId)
        {
            var compactItems = new List<PriceAssociationV2>();
            foreach (var item in obj.Items)
            {
                compactItems.Add(PriceAssociationV2.Create(item, guidToId));
            }

            return new ProductPriceAssociationV2()
            {
                ProductId = obj.ProductId,
                Items = compactItems
            };
        }

        /// <summary>
        /// Create FileBasedProductPriceAssociation from this object.
        /// </summary>
        /// <param name="idx2Guid">The map from index to guid.</param>
        /// <returns>FileBasedProductPriceAssociation object.</returns>
        public FileBasedProductPriceAssociation ToFileBasedProductPriceAssociation(List<Guid> idx2Guid)
        {
            var list = new List<FileBasedProductPriceAssociationItem>();
            foreach (var item in this.Items)
            {
                list.Add(item.ToFileBasedProductPriceAssociationItem(idx2Guid));
            }

            var result = new FileBasedProductPriceAssociation();
            result.ProductId = this.ProductId;
            result.Items = list;

            return result;
        }
    }
}
