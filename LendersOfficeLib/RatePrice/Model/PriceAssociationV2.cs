﻿namespace LendersOffice.RatePrice.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;
    using LendersOffice.RatePrice.FileBasedPricing;
    using ProtoBuf;

    /// <summary>
    /// Price Association V2.
    /// </summary>
    [ProtoContract]
    public class PriceAssociationV2
    {
        /// <summary>
        /// Bitmask for E_TriState, InheritVal ...
        /// </summary>
        [Flags]
        private enum BitmaskEnum
        {
            Blank = 1,
            Yes = 2,
            No = 4,
            InheritVal = 8,
            InheritValFromBaseProd = 16,
            IsForcedNo = 32
        }

        /// <summary>
        /// Gets or sets the Price Policy Index.
        /// </summary>
        [ProtoMember(1)]
        public int PricePolicyIdx { get; set; }

        /// <summary>
        /// Gets or sets the compact properties that contains information for AssocOverrideTri, InheritVal, InheritValFromBaseProd, IsForcedNo.
        /// </summary>
        [ProtoMember(2)]
        public int Properties { get; set; }

        /// <summary>
        /// Create PriceAssociationV2 from FileBasedProductPriceAssociationItem object.
        /// </summary>
        /// <param name="obj">FileBasedProductPriceAssociationItem object.</param>
        /// <param name="guidToId">The map from guid to index.</param>
        /// <returns>PriceAssociationV2 object.</returns>
        public static PriceAssociationV2 Create(FileBasedProductPriceAssociationItem obj, Dictionary<Guid, int> guidToId)
        {
            int val = 1 << (int)obj.AssocOverrideTri;  // 3 bit
            if (obj.InheritVal)
            {
                val |= (int)BitmaskEnum.InheritVal;
            }

            if (obj.InheritValFromBaseProd)
            {
                val |= (int)BitmaskEnum.InheritValFromBaseProd;
            }

            if (obj.IsForcedNo)
            {
                val |= (int)BitmaskEnum.IsForcedNo;
            }

            if (!guidToId.ContainsKey(obj.PricePolicyId))
            {
                int id = guidToId.Count;
                guidToId.Add(obj.PricePolicyId, id);
            }

            return new PriceAssociationV2()
            {
                PricePolicyIdx = guidToId[obj.PricePolicyId],
                Properties = val
            };
        }

        /// <summary>
        /// Create FileBasedProductPriceAssociationItem from this object.
        /// </summary>
        /// <param name="idx2Guid">The map index to guid.</param>
        /// <returns>FileBasedProductPriceAssociationItem object.</returns>
        public FileBasedProductPriceAssociationItem ToFileBasedProductPriceAssociationItem(List<Guid> idx2Guid)
        {
            E_TriState assocOverrideTri;
            switch ((BitmaskEnum)(this.Properties & 7))
            {
            case BitmaskEnum.Blank:
                assocOverrideTri = E_TriState.Blank;
                break;

            case BitmaskEnum.Yes:
                assocOverrideTri = E_TriState.Yes;
                break;

            case BitmaskEnum.No:
                assocOverrideTri = E_TriState.No;
                break;

            default:
                throw new Exception($"invalid PriceAssociationV2.Properties = {this.Properties}");
            }

            return new FileBasedProductPriceAssociationItem()
            {
                PricePolicyId = idx2Guid[this.PricePolicyIdx],
                AssocOverrideTri = assocOverrideTri,
                InheritVal = (this.Properties & (int)BitmaskEnum.InheritVal) != 0,
                InheritValFromBaseProd = (this.Properties & (int)BitmaskEnum.InheritValFromBaseProd) != 0,
                IsForcedNo = (this.Properties & (int)BitmaskEnum.IsForcedNo) != 0,
            };
        }
    }
}
