﻿namespace LendersOffice.RatePrice.Model
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents individual ResultGroup result.
    /// This class primary purpose is to serialize and deserialize to json. Do not add any logics to this.
    /// </summary>
    [DataContract]
    public class ResultGroup
    {
        /// <summary>
        /// Gets or sets the GroupName value.
        /// </summary>
        /// <value>The GroupName value.</value>
        [DataMember]
        public string GroupName { get; set; }

        /// <summary>
        /// Gets or sets the ResultRateOptions value.
        /// </summary>
        /// <value>The ResultRateOptions value.</value>
        [DataMember]
        public List<ResultRateOption> ResultRateOptions { get; set; }

        /// <summary>
        /// Gets or sets the ResultLoanPrograms value.
        /// </summary>
        /// <value>The ResultLoanPrograms value.</value>
        [DataMember]
        public List<ResultLoanProgram> ResultLoanPrograms { get; set; }

        /// <summary>
        /// Gets or sets the group's par rate.
        /// </summary>
        /// <value>The par rate of the group.</value>
        [DataMember]
        public string ParRate { get; set; }
    }
}
