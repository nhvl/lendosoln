﻿namespace LendersOffice.RatePrice.Model
{
    using System.Collections.Generic;

    /// <summary>
    /// A requests to sync INVESTOR_XLS_FILE, LPE_ACCEPTABLE_RS_FILE and LPE_ACCEPTABLE_RS_FILE_VERSION records.
    /// </summary>
    public class RatesheetDatabaseSyncRequest
    {
        /// <summary>
        /// Gets or sets a list of records to sync in INVESTOR_XLS_FILE.
        /// </summary>
        /// <value>A list of records.</value>
        public List<InvestorXlsFile> InvestorXlsFileList { get; set; }

        /// <summary>
        /// Gets or sets a list of records to sync in LPE_ACCEPTABLE_RS_FILE.
        /// </summary>
        /// <value>A list of records.</value>
        public List<LpeAcceptableRsFile> LpeAcceptableRsFileList { get; set; }

        /// <summary>
        /// Gets or sets a list of records to sync in LPE_ACCEPTABLE_RS_FILE_VERSION.
        /// </summary>
        /// <value>A ist of records.</value>
        public List<LpeAcceptableRsFileVersion> LpeAcceptableRsFileVersionList { get; set; }
    }
}