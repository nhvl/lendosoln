﻿namespace LendersOffice.RatePrice.Model
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Represents a record in LPE_ACCEPTABLE_RS_FILE_VERSION table.
    /// This class primary purpose is to serialize and deserialize to json. Do not add any logics to this.
    /// </summary>
    public class LpeAcceptableRsFileVersion
    {
        /// <summary>
        /// Gets or sets the acceptable ratesheet name.
        /// </summary>
        /// <value>The acceptable ratesheet name.</value>
        public string LpeAcceptableRsFileId { get; set; }

        /// <summary>
        /// Gets or sets the version number.
        /// </summary>
        /// <value>The version number.</value>
        public long VersionNumber { get; set; }

        /// <summary>
        /// Gets or sets the first effective date.
        /// </summary>
        /// <value>The first effective date.</value>
        public DateTime FirstEffectiveDateTimeInUtc { get; set; }

        /// <summary>
        /// Gets or sets the latest effective date.
        /// </summary>
        /// <value>The latest effective date.</value>
        public DateTime LatestEffectiveDateTimeInUtc { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether rate sheet is expire by investor.
        /// </summary>
        /// <value>Whether rate sheet is expire by investor.</value>
        public bool IsExpirationIssuedByInvestor { get; set; }

        /// <summary>
        /// Gets or sets the created date.
        /// </summary>
        /// <value>The created date.</value>
        public DateTime EntryCreatedDInUtc { get; set; }
    }
}