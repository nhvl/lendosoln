﻿namespace LendersOffice.RatePrice.Model
{
    using System;
    using ProtoBuf;

    /// <summary>
    /// Represents individual price policy rule.
    /// This class primary purpose is to serialize and deserialize to protobuf. Do not add any logics to this.
    /// </summary>
    [ProtoContract]
    public class PricePolicyRuleV2
    {
        /// <summary>
        /// Gets or sets the id of the rule.
        /// </summary>
        /// <value>Id of the rule.</value>
        [ProtoMember(1)]
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the rule description.
        /// </summary>
        /// <value>The rule description.</value>
        [ProtoMember(2)]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets qbc value.
        /// </summary>
        /// <value>The qbc value.</value>
        [ProtoMember(3)]
        public string QBC { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it is lender rule.
        /// </summary>
        /// <value>Whether it is lender rule.</value>
        [ProtoMember(4)]
        public bool IsLenderRule { get; set; }

        /// <summary>
        /// Gets or sets the condition logic.
        /// </summary>
        /// <value>The condition logic.</value>
        [ProtoMember(5)]
        public string Condition { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it is skip rule.
        /// </summary>
        /// <value>Is skip rule.</value>
        [ProtoMember(6)]
        public bool Skip { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it is disqualify rule.
        /// </summary>
        /// <value>Is disqualify rule.</value>
        [ProtoMember(7)]
        public bool Disqualify { get; set; }

        /// <summary>
        /// Gets or sets stipulation.
        /// </summary>
        /// <value>Stipulation of the rule.</value>
        [ProtoMember(8)]
        public string Stipulation { get; set; }

        /// <summary>
        /// Gets or sets fee adjustment.
        /// </summary>
        /// <value>Fee adjustment.</value>
        [ProtoMember(9)]
        public string AdjustFee { get; set; }

        /// <summary>
        /// Gets or sets margin adjustment.
        /// </summary>
        /// <value>Margin adjustment.</value>
        [ProtoMember(10)]
        public string AdjustMargin { get; set; }

        /// <summary>
        /// Gets or sets rate adjustment.
        /// </summary>
        /// <value>Rate adjustment.</value>
        [ProtoMember(11)]
        public string AdjustRate { get; set; }

        /// <summary>
        /// Gets or sets base margin adjustment.
        /// </summary>
        /// <value>Base margin adjustment.</value>
        [ProtoMember(12)]
        public string AdjustBaseMagin { get; set; }

        /// <summary>
        /// Gets or sets qltv adjustment.
        /// </summary>
        /// <value>Qltv adjustment.</value>
        [ProtoMember(13)]
        public string AdjustQltv { get; set; }

        /// <summary>
        /// Gets or sets qcltv adjustment.
        /// </summary>
        /// <value>Qcltv adjustment.</value>
        [ProtoMember(14)]
        public string AdjustQcltv { get; set; }

        /// <summary>
        /// Gets or sets qscore adjustment.
        /// </summary>
        /// <value>Qscore adjustment.</value>
        [ProtoMember(15)]
        public string AdjustQscore { get; set; }

        /// <summary>
        /// Gets or sets max front ysp adjustment.
        /// </summary>
        /// <value>Max front ysp adjustment.</value>
        [ProtoMember(16)]
        public string AdjustMaxFrontEndYsp { get; set; }

        /// <summary>
        /// Gets or sets max ysp adjustment.
        /// </summary>
        /// <value>Max ysp adjustment.</value>
        [ProtoMember(17)]
        public string AdjustMaxYsp { get; set; }

        /// <summary>
        /// Gets or sets qrate adjustment.
        /// </summary>
        /// <value>Qrate adjustment.</value>
        [ProtoMember(18)]
        public string AdjustQrate { get; set; }

        /// <summary>
        /// Gets or sets max dti adjustment.
        /// </summary>
        /// <value>Max DTI adjustment.</value>
        [ProtoMember(19)]
        public string AdjustMaxDti { get; set; }

        /// <summary>
        /// Gets or sets QLAmt adjustment.
        /// </summary>
        /// <value>QLAmt adjustment.</value>
        [ProtoMember(20)]
        public string AdjustQLAmt { get; set; }

        /// <summary>
        /// Gets or sets Qrate adjust.
        /// </summary>
        /// <value>Qrate adjust.</value>
        [ProtoMember(21)]
        public string AdjustQrateAdjust { get; set; }

        /// <summary>
        /// Gets or sets teaser rate adjust.
        /// </summary>
        /// <value>Teaser rate adjust.</value>
        [ProtoMember(22)]
        public string AdjustTeaserRateAdjust { get; set; }

        /// <summary>
        /// Gets or sets lock days adjust.
        /// </summary>
        /// <value>Lock days adjust.</value>
        [ProtoMember(23)]
        public string AdjustLockDaysAdjust { get; set; }
    }
}