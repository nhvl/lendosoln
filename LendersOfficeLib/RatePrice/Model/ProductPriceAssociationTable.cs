﻿namespace LendersOffice.RatePrice.Model
{
    using System;
    using System.Collections.Generic;
    using ProtoBuf;

    /// <summary>
    /// This container contains ProductPriceAssociationV2 list and guid table that converts index to guid.
    /// </summary>
    [ProtoContract]
    public class ProductPriceAssociationTable
    {
        /// <summary>
        /// Gets or sets the table that used to convert index to guid.
        /// </summary>
        [ProtoMember(1)]
        public List<Guid> Idx2Guid { get; set; }

        /// <summary>
        /// Gets or sets the ProductPriceAssociationV2 list.
        /// </summary>
        [ProtoMember(2)]
        public List<ProductPriceAssociationV2> Items { get; set; }
    }
}
