﻿namespace LendersOffice.RatePrice.Model
{
    using ProtoBuf;

    /// <summary>
    /// Represents individual rate option.
    /// This class primary purpose is to serialize and deserialize to json. Do not add any logics to this.
    /// </summary>
    [ProtoContract]
    public class RateOptionV2
    {
        /// <summary>
        /// Gets or sets the rate type.
        /// </summary>
        [ProtoMember(1)]
        public DataAccess.E_RateSheetKeywordT ItemType { get; set; }

        /// <summary>
        /// Gets or sets rate value.
        /// </summary>
        /// <value>The rate value.</value>
        [ProtoMember(2)]
        public decimal Rate { get; set; }

        /// <summary>
        /// Gets or sets point value.
        /// </summary>
        /// <value>The point value.</value>
        [ProtoMember(3)]
        public decimal Point { get; set; }

        /// <summary>
        /// Gets or sets margin value.
        /// </summary>
        /// <value>The margin value.</value>
        [ProtoMember(4)]
        public decimal Margin { get; set; }

        /// <summary>
        /// Gets or sets qrate base value.
        /// </summary>
        /// <value>Qrate base value.</value>
        [ProtoMember(5)]
        public decimal QRateBase { get; set; }

        /// <summary>
        /// Gets or sets teaser rate value.
        /// </summary>
        /// <value>Teaser rate value.</value>
        [ProtoMember(6)]
        public decimal TeaserRate { get; set; }
    }
}