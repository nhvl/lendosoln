﻿namespace LendersOffice.RatePrice.Model
{
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents individual rate option result.
    /// This class primary purpose is to serialize and deserialize to json. Do not add any logics to this.
    /// </summary>
    [DataContract]
    public class ResultRateOption
    {
        /// <summary>
        /// Gets or sets the list of rates that are worse than the this rate, if pricing is best only.
        /// </summary>
        /// <value>A list of rate options that are worse than this rate.</value>
        [DataMember]
        public List<ResultRateOption> WorseRates { get; set; } = new List<ResultRateOption>();

        /// <summary>
        /// Gets or sets the true best winner whose DTI exceeds the max limit.  This value is only
        /// initialized if this rate option is the representative rate, and if there exists a rate option
        /// priced better than this one.
        /// </summary>
        /// <value>A rate option if there exists a rate with better priced and who exceeds the DTI. </value>
        [DataMember]
        public ResultRateOption TrueBestOption { get; set; }

        /// <summary>
        /// Gets or sets the list of rates that are priced better than this rate, but violate the max Dti limit.
        /// </summary>
        /// <value>A list of rate options that are better than this rate, but violate the max Dti limit.</value>
        [DataMember]
        public List<ResultRateOption> BetterDtiViolatingRates { get; set; } = new List<ResultRateOption>();

        /// <summary>
        /// Gets or sets rate value.
        /// </summary>
        /// <value>The rate value.</value>
        [DataMember]
        public string Rate { get; set; }

        /// <summary>
        /// Gets or sets the raw rate value.
        /// </summary>
        /// <value>The raw rate value.</value>
        [DataMember]
        public string RawRate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the rate is expired.
        /// </summary>
        /// <value>Whether the rate is expired.</value>
        [DataMember]
        public bool IsRateExpired { get; set; }

        /// <summary>
        /// Gets or sets the teaser rate value.
        /// </summary>
        /// <value>The teaser rate value.</value>
        [DataMember]
        public string TeaserRate { get; set; }

        /// <summary>
        /// Gets or sets the bottom ratio value.
        /// </summary>
        /// <value>The bottom ratio value.</value>
        [DataMember]
        public string BottomRatio { get; set; }

        /// <summary>
        /// Gets or sets the banked amount value.
        /// </summary>
        /// <value>The banked amount value.</value>
        [DataMember]
        public string BankedAmount { get; set; }

        /// <summary>
        /// Gets or sets the rate option id value.
        /// </summary>
        /// <value>The rate option id value.</value>
        [DataMember]
        public string RateOptionId { get; set; }

        /// <summary>
        /// Gets or sets Price value.
        /// </summary>
        /// <value>The Price value.</value>
        [DataMember]
        public string Price { get; set; }

        /// <summary>
        /// Gets or sets Point value.
        /// </summary>
        /// <value>The Point value.</value>
        [DataMember]
        public string Point { get; set; }

        /// <summary>
        /// Gets or sets Total Price Adjustment value.
        /// </summary>
        /// <value>The Total Price Adjustment value.</value>
        [DataMember]
        public string TotalPriceAdj { get; set; }

        /// <summary>
        /// Gets or sets Payment value.
        /// </summary>
        /// <value>The Payment value.</value>
        [DataMember]
        public string Payment { get; set; }

        /// <summary>
        /// Gets or sets Qualify Rate value.
        /// </summary>
        /// <value>The Qualify Rate value.</value>
        [DataMember]
        public string QualRate { get; set; }

        /// <summary>
        /// Gets or sets Qualify Rate value.
        /// </summary>
        /// <value>The Qualify Rate value.</value>
        [DataMember]
        public string QualPmt { get; set; }

        /// <summary>
        /// Gets or sets Margin value.
        /// </summary>
        /// <value>The Margin value.</value>
        [DataMember]
        public string Margin { get; set; }

        /// <summary>
        /// Gets or sets Debt to income value.
        /// </summary>
        /// <value>The Debt to income value.  Also known as the Bottom Ratio.</value>
        [DataMember]
        public string Dti { get; set; }

        /// <summary>
        /// Gets or sets Max Debt to income value.
        /// </summary>
        /// <value>The Max Debt to income value.</value>
        [DataMember]
        public string MaxDti { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the rate option is violating the max DTI.
        /// </summary>
        /// <value>A bool stating whether the max dti is violated.</value>
        [DataMember]
        public bool IsViolateMaxDti { get; set; }

        /// <summary>
        /// Gets or sets Apr value.
        /// </summary>
        /// <value>The Apr value.</value>
        [DataMember]
        public string Apr { get; set; }

        /// <summary>
        /// Gets or sets ClosingCosts value.
        /// </summary>
        /// <value>The ClosingCosts value.</value>
        [DataMember]
        public string ClosingCosts { get; set; }

        /// <summary>
        /// Gets or sets CashToClose value.
        /// </summary>
        /// <value>The CashToClose value.</value>
        [DataMember]
        public string CashToClose { get; set; }

        /// <summary>
        /// Gets or sets Reserves value.
        /// </summary>
        /// <value>The Reserves value.</value>
        [DataMember]
        public string Reserves { get; set; }

        /// <summary>
        /// Gets or sets Break Even value.
        /// </summary>
        /// <value>The Break Even value.</value>
        [DataMember]
        public string BreakEvenMonths { get; set; }

        /// <summary>
        /// Gets or sets PrepaidCharges value.
        /// </summary>
        /// <value>The PrepaidCharges value.</value>
        [DataMember]
        public string PrepaidCharges { get; set; }

        /// <summary>
        /// Gets or sets NonPrepaidCharges value.
        /// </summary>
        /// <value>The NonPrepaidCharges value.</value>
        [DataMember]
        public string NonPrepaidCharges { get; set; }

        /// <summary>
        /// Gets or sets Investor Name value.
        /// </summary>
        /// <value>The Investor Name value.</value>
        [DataMember]
        public string LpInvestorNm { get; set; }

        /// <summary>
        /// Gets or sets Quality Mortgage Mode value.
        /// </summary>
        /// <value>The Quality Mortgage Mode value.</value>
        [DataMember]
        public string QmMode { get; set; }

        /// <summary>
        /// Gets or sets Hpm Mode value.
        /// </summary>
        /// <value>The Quality Mortgage Mode value.</value>
        [DataMember]
        public string HpmMode { get; set; }

        /// <summary>
        /// Gets or sets ProductCode value.
        /// </summary>
        /// <value>The ProductCode value.</value>
        [DataMember]
        public string ProductCode { get; set; }

        /// <summary>
        /// Gets or sets Template Name value.
        /// </summary>
        /// <value>The Template Name value.</value>
        [DataMember]
        public string LpTemplateNm { get; set; }

        /// <summary>
        /// Gets or sets Template Identifier value.
        /// </summary>
        /// <value>The Template Identifier value.</value>
        [DataMember]
        public string LpTemplateId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsBlockedRateLockSubmission is true.
        /// </summary>
        /// <value>The IsBlockedRateLockSubmission value.</value>
        [DataMember]
        public bool IsBlockedRateLockSubmission { get; set; }

        /// <summary>
        /// Gets or sets RateLockSubmissionUserWarningMessage value.
        /// </summary>
        /// <value>The RateLockSubmissionUserWarningMessage value.</value>
        [DataMember]
        public string RateLockSubmissionUserWarningMessage { get; set; }

        /// <summary>
        /// Gets or sets Version value.
        /// </summary>
        /// <value>The Version value.</value>
        [DataMember]
        public string Version { get; set; }

        /// <summary>
        /// Gets or sets UniqueChecksum value.
        /// </summary>
        /// <value>The UniqueChecksum value.</value>
        [DataMember]
        public string UniqueChecksum { get; set; }

        /// <summary>
        /// Gets or sets Disqualification Reason value.
        /// </summary>
        /// <value>The Disqualification Reason value.</value>
        [DataMember]
        public string DisqualReason { get; set; }

        /// <summary>
        /// Gets or sets sTransNetCashNonNegative value.
        /// </summary>
        /// <value>The sTransNetCashNonNegative value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sTransNetCashNonNegative { get; set; }

        /// <summary>
        /// Gets or sets sPurchPrice value.
        /// </summary>
        /// <value>The sPurchPrice value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sPurchPrice { get; set; }

        /// <summary>
        /// Gets or sets sAltCost value.
        /// </summary>
        /// <value>The sAltCost value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sAltCost { get; set; }

        /// <summary>
        /// Gets or sets sLandCost value.
        /// </summary>
        /// <value>The sLandCost value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sLandCost { get; set; }

        /// <summary>
        /// Gets or sets sRefPdOffAmt1003 value.
        /// </summary>
        /// <value>The sRefPdOffAmt1003 value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sRefPdOffAmt1003 { get; set; }

        /// <summary>
        /// Gets or sets sTotEstPp1003 value.
        /// </summary>
        /// <value>The sTotEstPp1003 value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sTotEstPp1003 { get; set; }

        /// <summary>
        /// Gets or sets sTotEstCcNoDiscnt1003 value.
        /// </summary>
        /// <value>The sTotEstCcNoDiscnt1003 value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sTotEstCcNoDiscnt1003 { get; set; }

        /// <summary>
        /// Gets or sets sFfUfmip1003 value.
        /// </summary>
        /// <value>The sFfUfmip1003 value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sFfUfmip1003 { get; set; }

        /// <summary>
        /// Gets or sets sLDiscnt1003 value.
        /// </summary>
        /// <value>The sLDiscnt1003 value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sLDiscnt1003 { get; set; }

        /// <summary>
        /// Gets or sets sTotTransC value.
        /// </summary>
        /// <value>The sTotTransC value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sTotTransC { get; set; }

        /// <summary>
        /// Gets or sets sONewFinBal value.
        /// </summary>
        /// <value>The sONewFinBal value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sONewFinBal { get; set; }

        /// <summary>
        /// Gets or sets  value.
        /// </summary>
        /// <value>The sTotCcPbs value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sTotCcPbs { get; set; }

        /// <summary>
        /// Gets or sets sOCredit1Desc value.
        /// </summary>
        /// <value>The sOCredit1Desc value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sOCredit1Desc { get; set; }

        /// <summary>
        /// Gets or sets sOCredit1Amt value.
        /// </summary>
        /// <value>The sOCredit1Amt value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sOCredit1Amt { get; set; }

        /// <summary>
        /// Gets or sets sOCredit2Desc value.
        /// </summary>
        /// <value>The sOCredit2Desc value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sOCredit2Desc { get; set; }

        /// <summary>
        /// Gets or sets sOCredit2Amt value.
        /// </summary>
        /// <value>The sOCredit2Amt value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sOCredit2Amt { get; set; }

        /// <summary>
        /// Gets or sets sOCredit3Desc value.
        /// </summary>
        /// <value>The  value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sOCredit3Desc { get; set; }

        /// <summary>
        /// Gets or sets sOCredit3Amt value.
        /// </summary>
        /// <value>The sOCredit3Amt value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sOCredit3Amt { get; set; }

        /// <summary>
        /// Gets or sets sOCredit4Desc value.
        /// </summary>
        /// <value>The sOCredit4Desc value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sOCredit4Desc { get; set; }

        /// <summary>
        /// Gets or sets sOCredit4Amt value.
        /// </summary>
        /// <value>The sOCredit4Amt value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sOCredit4Amt { get; set; }

        /// <summary>
        /// Gets or sets sOCredit5Amt value.
        /// </summary>
        /// <value>The sOCredit5Amt value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sOCredit5Amt { get; set; }

        /// <summary>
        /// Gets or sets sONewFinCc value.  This does not actually map to the loan's sONewFinCc value.  It maps to sONewFinCcAsOCreditAmt_rep.
        /// </summary>
        /// <value>The sONewFinCc value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sONewFinCc { get; set; }

        /// <summary>
        /// Gets or sets sONewFinCcInverted value.  This actually maps to sONewFinCc for the loan.
        /// </summary>
        /// <value>The sONewFinCcInverted value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sONewFinCcInverted { get; set; }

        /// <summary>
        /// Gets or sets sTotLiquidAssets value.
        /// </summary>
        /// <value>The sTotLiquidAssets value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sTotLiquidAssets { get; set; }

        /// <summary>
        /// Gets or sets sLAmtCalc value.
        /// </summary>
        /// <value>The sLAmtCalc value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sLAmtCalc { get; set; }

        /// <summary>
        /// Gets or sets sFfUfmipFinanced value.
        /// </summary>
        /// <value>The sFfUfmipFinanced value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sFfUfmipFinanced { get; set; }

        /// <summary>
        /// Gets or sets sFinalLAmt value.
        /// </summary>
        /// <value>The sFinalLAmt value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sFinalLAmt { get; set; }

        /// <summary>
        /// Gets or sets sTransNetCash value.
        /// </summary>
        /// <value>The sTransNetCash value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sTransNetCash { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether sIsIncludeProrationsIn1003Details.
        /// </summary>
        /// <value>The sIsIncludeProrationsIn1003Details value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public bool sIsIncludeProrationsIn1003Details { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether sIsIncludeProrationsInTotPp.
        /// </summary>
        /// <value>The sIsIncludeProrationsInTotPp value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public bool sIsIncludeProrationsInTotPp { get; set; }

        /// <summary>
        /// Gets or sets sTotEstPp value.
        /// </summary>
        /// <value>The sTotEstPp value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sTotEstPp { get; set; }

        /// <summary>
        /// Gets or sets sTotalBorrowerPaidProrations value.
        /// </summary>
        /// <value>The sTotalBorrowerPaidProrations value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sTotalBorrowerPaidProrations { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether sIsIncludeONewFinCcInTotEstCc.
        /// </summary>
        /// <value>The sIsIncludeONewFinCcInTotEstCc value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public bool sIsIncludeONewFinCcInTotEstCc { get; set; }

        /// <summary>
        /// Gets or sets sTotEstCcNoDiscnt value.
        /// </summary>
        /// <value>The sTotEstCcNoDiscnt value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sTotEstCcNoDiscnt { get; set; }
        
        /// <summary>
        /// Gets or sets sMonthlyPmt value.
        /// </summary>
        /// <value>The sMonthlyPmt value.</value>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sMonthlyPmt { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsHistorical.
        /// </summary>
        /// <value>The IsHistorical value.</value>
        [DataMember]
        public bool IsHistorical { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsCurrent.
        /// </summary>
        /// <value>The IsCurrent value.</value>
        [DataMember]
        public bool IsCurrent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsPaired.
        /// </summary>
        /// <value>The IsPaired value.</value>
        [DataMember]
        public bool IsPaired { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsWorse.
        /// </summary>
        /// <value>The IsWorse value.</value>
        [DataMember]
        public bool IsWorse { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether IsEqual.
        /// </summary>
        /// <value>The IsEqual value.</value>
        [DataMember]
        public bool IsEqual { get; set; }

        /// <summary>
        /// Gets or sets Closing Cost Breakdown value.
        /// </summary>
        /// <value>The Closing Cost Breakdown value.</value>
        [DataMember]
        public List<ResultClosingCost> ClosingCostBreakdown { get; set; }

        /// <summary>
        /// Gets or sets Pin Id value.
        /// </summary>
        /// <value>The Pin Id value.</value>
        [DataMember]
        public string PinId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether sAltCost is locked.
        /// </summary>
        /// <value>The value sAltCostLckd.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public bool sAltCostLckd { get; set; }

        /// <summary>
        /// Gets or sets the value sLandIfAcquiredSeparately1003e.
        /// </summary>
        /// <value>The value sLandIfAcquiredSeparately1003.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sLandIfAcquiredSeparately1003 { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether sLDiscnt1003 is locked.
        /// </summary>
        /// <value>The value sLDiscnt1003Lckd.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public bool sLDiscnt1003Lckd { get; set; }

        /// <summary>
        /// Gets or sets the value sPurchasePrice1003.
        /// </summary>
        /// <value>The value sPurchasePrice1003.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sPurchasePrice1003 { get; set; }

        /// <summary>
        /// Gets or sets the value sRefNotTransMortgageBalancePayoffAmte.
        /// </summary>
        /// <value>The value sRefNotTransMortgageBalancePayoffAmt.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sRefNotTransMortgageBalancePayoffAmt { get; set; }

        /// <summary>
        /// Gets or sets the value sRefTransMortgageBalancePayoffAmt.
        /// </summary>
        /// <value>The value sRefTransMortgageBalancePayoffAmt.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sRefTransMortgageBalancePayoffAmt { get; set; }

        /// <summary>
        /// Gets or sets the value sSellerCreditsUlad.
        /// </summary>
        /// <value>The value sSellerCreditsUlad.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sSellerCreditsUlad { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether sTotCcPbs is locked.
        /// </summary>
        /// <value>The value sTotCcPbsLocked.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public bool sTotCcPbsLocked { get; set; }

        /// <summary>
        /// Gets or sets the value sTotCreditsUlad.
        /// </summary>
        /// <value>The value sTotCreditsUlad.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sTotCreditsUlad { get; set; }

        /// <summary>
        /// Gets or sets the value sTotEstBorrCostUlad.
        /// </summary>
        /// <value>The value sTotEstBorrCostUlad.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sTotEstBorrCostUlad { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether sTotEstCc1003 is locked.
        /// </summary>
        /// <value>The value sTotEstCc1003Lckd.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public bool sTotEstCc1003Lckd { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether sTotEstPp1003 is locked.
        /// </summary>
        /// <value>The value sTotEstPp1003Lckd.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public bool sTotEstPp1003Lckd { get; set; }

        /// <summary>
        /// Gets or sets the value sTotMortLAmtUlad.
        /// </summary>
        /// <value>The value sTotMortLAmtUlad.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sTotMortLAmtUlad { get; set; }

        /// <summary>
        /// Gets or sets the value sTotMortLTotCreditUlad.
        /// </summary>
        /// <value>The value sTotMortLAmtUlad.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sTotMortLTotCreditUlad { get; set; }

        /// <summary>
        /// Gets or sets the value sTotOtherCreditsUlad.
        /// </summary>
        /// <value>The value sTotMortLAmtUlad.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sTotOtherCreditsUlad { get; set; }

        /// <summary>
        /// Gets or sets the value sTotTransCUlad.
        /// </summary>
        /// <value>The value sTransNetCashUlad.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sTotTransCUlad { get; set; }

        /// <summary>
        /// Gets or sets the value sTransNetCashUlad.
        /// </summary>
        /// <value>The value sTransNetCashUlad.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sTransNetCashUlad { get; set; }

        /// <summary>
        /// Gets or sets the value sTRIDSellerCredits.
        /// </summary>
        /// <value>The value sTRIDSellerCredits.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        [DataMember]
        public string sTRIDSellerCredits { get; set; }
    }
}
