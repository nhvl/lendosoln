﻿///
/// Author: Thien Nguyen
/// 
namespace LendersOfficeApp.RatePrice.XmlResultPricingCmpUtils
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Xml.Linq;

    /// <summary>
    /// The class SimpleItems contains simple items.
    /// A simple item contains only xml attributes and text. It doesn't contain the nested xml element.
    /// Some simple item has key. For example, the simple item ClosingCost can have key: HudLine="802" Description="General Lender Credit".
    /// Other simple item doesn't have key as DenialReason ...
    /// </summary>
    internal class SimpleItems // simple item has xml attribules only
    {
        /// <summary>
        /// Create a shared empty list EmptyList. Expect: nobody modifies it.
        /// </summary>
        private static readonly List<string> EmptyList = new List<string>();

        /// <summary>
        /// String collection. Each string is created from simple xml element using xml attributes and potential xml text (if includingInnerText = true).
        /// </summary>
        private HashSet<string> items = new HashSet<string>();

        /// <summary>
        /// Some cases the simple item has key. This collection is map from key -> item's attributes.
        /// </summary>
        private Dictionary<string, Dictionary<string, string>> itemDict = new Dictionary<string, Dictionary<string, string>>();

        /// <summary>
        /// Key names.
        /// </summary>
        private List<string> candidateKeys = EmptyList;

        /// <summary>
        /// Owner name. For example AppRateOption contains many ClosingCost elements. The owner name is "AppRateOption" and itemName is "ClosingCost"
        /// Owner is one of item's ancestors. Owner name is used for error message only.
        /// </summary>
        private string ownerName;

        /// <summary>
        /// Item name.
        /// </summary>
        private string itemName;

        /// <summary>
        /// This member is used if item's parent is CSortedListOfGroups.
        /// </summary>
        private string sortedListOfGroupsInfo = string.Empty;

        /// <summary>
        /// Using xml text or not.
        /// </summary>
        private bool incInnerText;

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleItems" /> class.
        /// </summary>
        /// <param name="ownerName"> Owner name as "AppRateOption".</param>
        /// <param name="itemName">Item name as "ClosingCost". </param>
        /// <param name="candidateKeys">Candidate key names. </param>
        /// <param name="incInnerText">Use xml text or not.</param>
        internal SimpleItems(string ownerName, string itemName, List<string> candidateKeys = null, bool incInnerText = false)
        {
            this.ownerName = ownerName;
            this.itemName = itemName;
            this.incInnerText = incInnerText;
            this.candidateKeys = candidateKeys ?? EmptyList;
       }

        /// <summary>
        /// Gets a value indicating whether the collection items has data or not.
        /// </summary>
        internal bool HasData
        {
            get { return this.items.Count > 0; }             
        }

        /// <summary>
        /// Read simple xml element list. 
        /// </summary>
        /// <param name="nodeList">The xml element list that has same XPath.</param>
        /// <param name="ignoreAttributes">Ignore specific attribute list.</param>
        internal void Init(IEnumerable<XElement> nodeList, HashSet<string> ignoreAttributes = null)
        {
            this.items.Clear();
            this.sortedListOfGroupsInfo = string.Empty;
           
            bool isFirst = true;
            foreach (XElement nodeEle in nodeList)
            {
                Dictionary<string, string> nameValues = new Dictionary<string, string>();
                if (isFirst)
                {
                    isFirst = false;
                    XElement parentNode = nodeEle.Parent;
                    if (parentNode.Name.LocalName == "CSortedListOfGroups")
                    {
                        PricingCmpHelper.GetAttributes(parentNode, nameValues);
                        this.sortedListOfGroupsInfo = PricingCmpHelper.ToString(nameValues);
                    }
                }

                PricingCmpHelper.GetAttributes(nodeEle, nameValues, ignoreAttributes);
                string item = PricingCmpHelper.ToString(nameValues);
                if (this.incInnerText)
                {
                    item = item + " *** text='" + nodeEle.Value + "'";
                    string makeupAttrib = this.itemName + "'s content";
                    nameValues[makeupAttrib] = nodeEle.Value;
                }

                this.items.Add(item);

                string key = this.CreateKey(nameValues);
                if (string.IsNullOrEmpty(key) == false && this.itemDict.ContainsKey(key) == false)
                {
                    this.itemDict[key] = nameValues;
                }
                else
                {
                    // not support key because "candidate key" is empty or duplicate.
                    this.candidateKeys = EmptyList;
                    this.itemDict.Clear();
                }
            }
        }

        /// <summary>
        /// Compare with other SimpleItems.
        /// </summary>
        /// <param name="other">The object to compare with the current object. </param>
        /// <param name="indent">Indent for each error message line.</param>
        /// <param name="errLog">Error messages.</param>
        /// <param name="fileLabels">The file labels.</param>
        /// <returns>Return true if the specified object is equal to the current object; otherwise, false.</returns>
        internal bool Compare(SimpleItems other, string indent, out string errLog, Tuple<string, string> fileLabels)
        {
            errLog = string.Empty;
            StringBuilder sb = new StringBuilder();

            if (this.sortedListOfGroupsInfo != other.sortedListOfGroupsInfo)
            {
                sb.AppendFormat("{0}{1} collection has different attributes: '{2}' vs '{3}'", indent, this.itemName, this.sortedListOfGroupsInfo, other.sortedListOfGroupsInfo).AppendLine();
                errLog = sb.ToString();
                return false;
            }

            bool result = true;

            // Does not support "key"?
            if (this.itemDict.Count == 0 || other.itemDict.Count == 0)
            {                
                foreach (string str in this.items)
                {
                    if (other.items.Contains(str) == false)
                    {
                        sb.AppendFormat("{0}Only {4} {1} has {2}: {3}", indent, this.ownerName, this.itemName, str, fileLabels.Item1).AppendLine();
                        result = false;
                    }
                }

                foreach (string str in other.items)
                {
                    if (this.items.Contains(str) == false)
                    {
                        sb.AppendFormat("{0}Only {4} {1} has {2}: {3}", indent, this.ownerName, this.itemName, str, fileLabels.Item2).AppendLine();
                        result = false;
                    }
                }                
            }
            else
            {
                foreach (var entry in this.itemDict)
                {
                    string key = entry.Key;

                    if (other.itemDict.ContainsKey(key) == false)
                    {
                        sb.AppendFormat("{0}Only {4} {1} has {2}: {3}", indent, this.ownerName, this.itemName, key, fileLabels.Item1).AppendLine();
                        result = false;
                    }
                    else
                    {
                        string diff = PricingCmpHelper.GetDifferentList(entry.Value, other.itemDict[key], indent + "\t");
                        if (diff != string.Empty)
                        {
                            sb.AppendFormat("{0}{1} {2} '{3}' different", indent, this.ownerName, this.itemName, key).AppendLine();
                            sb.Append(diff);
                            result = false;
                        }
                    }
                }

                foreach (var entry in other.itemDict)
                {
                    string key = entry.Key;

                    if (this.itemDict.ContainsKey(key) == false)
                    {
                        sb.AppendFormat("{0}Only {4} {1} has {2}: {3}", indent, other.ownerName, other.itemName, key, fileLabels.Item2).AppendLine();
                        result = false;
                    }
                }
            }

            errLog = sb.ToString();
            return result;
        }

        /// <summary>
        /// Create complex key from collection nameValues and candidateKeys.
        /// </summary>
        /// <param name="nameValues">The collection name/value.</param>
        /// <returns>Return key with format "keyName1=value1,keyName2=value2...".</returns>
        private string CreateKey(Dictionary<string, string> nameValues)
        {
            if (this.candidateKeys.Count == 0)
            {
                return string.Empty;
            }

            return PricingCmpHelper.GetValues(nameValues, this.candidateKeys);
        }
    }
}