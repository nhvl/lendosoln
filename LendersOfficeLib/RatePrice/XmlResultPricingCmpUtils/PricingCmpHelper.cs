﻿///
/// Author: Thien Nguyen
/// 
namespace LendersOfficeApp.RatePrice.XmlResultPricingCmpUtils
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;

    /// <summary>
    /// PricingCmpHelper class that provides commond methods for parser xml pricing rule.
    /// </summary>
    internal class PricingCmpHelper
    {
        /// <summary>
        /// Get xml attributes from XElement.
        /// </summary>
        /// <param name="ele">The input xml element.</param>
        /// <param name="keyValues">The key-value container.</param>
        /// <param name="ignoreAttributes">The list attributes are ignored.</param>
        public static void GetAttributes(XElement ele, Dictionary<string, string> keyValues, HashSet<string> ignoreAttributes = null)
        {
            keyValues.Clear();
            foreach (XAttribute attr in ele.Attributes())
            {
                if (ignoreAttributes != null && ignoreAttributes.Contains(attr.Name.LocalName))
                {
                    continue;
                }

                string val = attr.Value;

                // hack remove: execute time
                if (val != null && val.Contains("execution time:"))
                {
                    string pattern = "execution time:\\d+.\\d+ms";
                    Regex rgx = new Regex(pattern);
                    val = rgx.Replace(val, "execution time:");
                }

                keyValues.Add(attr.Name.LocalName, val);
            }
        }

        /// <summary>
        /// Convert key-values to string.
        /// </summary>
        /// <param name="keyValues">The input key-value pairs.</param>
        /// <returns>Return string with format 'key="value", key="value" ....'. </returns>
        public static string ToString(Dictionary<string, string> keyValues)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var entry in keyValues)
            {
                if (sb.Length > 0)
                {
                    sb.Append(", ");
                }

                sb.AppendFormat("{0}=\"{1}\"", entry.Key, entry.Value);
            }

            return sb.ToString();
        }

        /// <summary>
        /// List the different between two collections keyValues and otherKeyValues.
        /// </summary>
        /// <param name="keyValues">The first input key-value pairs.</param>
        /// <param name="otherKeyValues">The second input key-value pairs.</param>
        /// <param name="indent">The indent.</param>
        /// <returns>Return empty string if two collections are same. Otherwise return string with format "   key: value1 vs value2\r\n ....".</returns>
        public static string GetDifferentList(Dictionary<string, string> keyValues, Dictionary<string, string> otherKeyValues, string indent)        
        {
            StringBuilder sb = new StringBuilder();
            foreach (var entry in keyValues)
            {
                string key = entry.Key;
                string otherValue = otherKeyValues.ContainsKey(key) ? otherKeyValues[key] : "???";
                if (!otherKeyValues.ContainsKey(key) || entry.Value != otherValue)
                {
                    sb.AppendFormat("{0}{1}: '{2}' vs '{3}'", indent, key, entry.Value, otherValue).AppendLine();
                }
            }

            foreach (var entry in otherKeyValues)
            {
                string key = entry.Key;
                if (keyValues.ContainsKey(key) == false)
                {
                    sb.AppendFormat("{0}{1}: ??? vs '{2}'", indent, key, entry.Value).AppendLine();
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Get complex value from keyValues and selectedKeys with format "selectedKey1=value1, selectedKey2=value2 ...".
        /// </summary>
        /// <param name="keyValues">The input key-value pairs.</param>
        /// <param name="selectedKeys">The selected keys.</param>
        /// <returns>Return string with format "selectedKey1=value1, selectedKey2=value2 ...". </returns>
        public static string GetValues(Dictionary<string, string> keyValues, IEnumerable<string> selectedKeys)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string key in selectedKeys)
            {
                if (sb.Length > 0)
                {
                    sb.Append(", ");
                }

                string value;
                if (!keyValues.TryGetValue(key, out value))
                {
                    value = string.Empty;
                }

                sb.AppendFormat("{0}={1}", key, value);
            }

            return sb.ToString();
        }
    }
}