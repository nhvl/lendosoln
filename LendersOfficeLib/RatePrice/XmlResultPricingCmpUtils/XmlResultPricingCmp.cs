﻿///
/// Author: Thien Nguyen
/// 
namespace LendersOfficeApp.RatePrice.XmlResultPricingCmpUtils
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml.Linq;
    using System.Xml.XPath;
    using LendersOffice.Drivers.Gateways;

    /// <summary>
    /// Compare 2 xml pricing result strings.
    /// </summary>
    public class XmlResultPricingCmp
    {
        /// <summary>
        /// Compare 2 xml pricing result strings.
        /// </summary>
        /// <param name="dbsnapshotXmlResult">The database snapshot's xml pricing result string.</param>
        /// <param name="filebasedXmlResult">The filebased snapshot's pricing result string.</param>
        /// <param name="logStr">The error messages.</param>
        /// <returns>Return true if equal; otherwise return false.</returns>
        public static bool Compare(string dbsnapshotXmlResult, string filebasedXmlResult, out string logStr)
        {
            logStr = string.Empty;
            StringBuilder log = new StringBuilder();

            ApplicantPrices dbsnapshotAppPrices = new ApplicantPrices();
            dbsnapshotAppPrices.Init(dbsnapshotXmlResult);

            ApplicantPrices filebasedAppPrices = new ApplicantPrices();
            filebasedAppPrices.Init(filebasedXmlResult);

            Tuple<string, string> labels = Tuple.Create("DbSnapshot", "Filebased ");
            bool result = dbsnapshotAppPrices.Compare(filebasedAppPrices, log, labels);
            log.Replace("\t", "    ");
            logStr = log.ToString();

            return result;
        }

        /// <summary>
        /// Save xml string to file.
        /// </summary>
        /// <param name="fname">The file to write to.</param>
        /// <param name="xml">The string to write to the file. </param>
        /// <param name="saveInPrettyFormat">Save with ident or not.</param>
        public static void XmlToFile(string fname, string xml, bool saveInPrettyFormat = true)
        {
            if (saveInPrettyFormat == false)
            {
                TextFileHelper.WriteString(fname, xml, false);
                return;
            }

            XDocument doc = XDocument.Parse(xml);
            var settings = new System.Xml.XmlWriterSettings()
            {
                Indent = true,
                IndentChars = "  "
            };

            using (var writer = System.Xml.XmlWriter.Create(fname, settings))
            {
                doc.WriteTo(writer);
            }
        }
    }
}