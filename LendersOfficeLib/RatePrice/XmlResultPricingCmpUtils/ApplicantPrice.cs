﻿///
/// Author: Thien Nguyen
/// 
namespace LendersOfficeApp.RatePrice.XmlResultPricingCmpUtils
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Xml.Linq;
    using System.Xml.XPath;

    /// <summary>
    /// This class contains information for xml element <CApplicantPriceXml Status="0" lLpTemplateNm="15 FIXED CONF BILLY JOE BOB" IsDuRefiFilterEnabled="False"> ...</CApplicantPriceXml>
    /// </summary>
    internal class ApplicantPrice
    {
        /// <summary>
        /// The xml ApplicantPrice's attributes.
        /// </summary>
        private Dictionary<string, string> attributes = new Dictionary<string, string>();

        /// <summary>
        /// The CApplicantPriceXml's subitems AppRateOption.
        /// </summary>
        private Dictionary<string, AppRateOption> rateOptions = new Dictionary<string, AppRateOption>();

        /// <summary>
        /// The RepresentativeRateOption's subitems AppRateOption.
        /// </summary>
        private Dictionary<string, AppRateOption> representativeRateOptions = new Dictionary<string, AppRateOption>();

        /// <summary>
        /// The subitem DenialReason collection.
        /// </summary>
        private SimpleItems denialReasons = new SimpleItems("ApplicantPrice", "DenialReason", null, true /*process node DenialReason's value*/);

        /// <summary>
        /// The subitem Stip collection.
        /// </summary>
        private SimpleItems stips = new SimpleItems("ApplicantPrice", "Stip");

        /// <summary>
        /// The subitem HiddenStip collection.
        /// </summary>
        private SimpleItems hiddenStips = new SimpleItems("ApplicantPrice", "HiddenStip");

        /// <summary>
        /// The subitem AdjustDesc collection.
        /// </summary>
        private SimpleItems adjustDescs = new SimpleItems("ApplicantPrice", "AdjustDesc");

        /// <summary>
        /// The subitem HiddenAdjustDesc collection.
        /// </summary>
        private SimpleItems hiddenAdjustDescs = new SimpleItems("ApplicantPrice", "HiddenAdjustDesc");

        /// <summary>
        /// The subitem <Errors></Errors>
        /// </summary>
        private string errorMsgs = string.Empty;

        /// <summary>
        /// Allow to write error header.
        /// </summary>
        private bool allowWriteHdr = true;

        /// <summary>
        /// Initialize object from the XML elelement CApplicantPriceXml.
        /// </summary>
        /// <param name="ele">The input XML elelement CApplicantPriceXml.</param>
        internal void Init(XElement ele)
        {
            this.rateOptions.Clear();

            HashSet<string> ignoreAttribultes = new HashSet<string>
            {
                "FriendlyInfo",
                "RateLockSubmissionUserWarningMessage",
                "RateLockSubmissionDevWarningMessage",
                "Version",
            };

            PricingCmpHelper.GetAttributes(ele, this.attributes, ignoreAttribultes);

            IEnumerable<XElement> rateOptList = ele.XPathSelectElements("./ApplicantRateOptions/CApplicantRateOption");
            foreach (XElement rateOptEle in rateOptList)
            {
                AppRateOption rateOpt = new AppRateOption();
                rateOpt.Init(rateOptEle);
                this.rateOptions.Add(rateOpt.Key, rateOpt);
            }

            rateOptList = ele.XPathSelectElements("./RepresentativeRateOption/CApplicantRateOption");
            foreach (XElement rateOptEle in rateOptList)
            {
                AppRateOption rateOpt = new AppRateOption();
                rateOpt.Init(rateOptEle);
                this.representativeRateOptions.Add(rateOpt.Key, rateOpt);
            }

            this.denialReasons.Init(ele.XPathSelectElements("./DenialReasons/DenialReason"));
            this.stips.Init(ele.XPathSelectElements("./Stips/CSortedListOfGroups/item"));
            this.hiddenStips.Init(ele.XPathSelectElements("./HiddenStips/CSortedListOfGroups/item"));
            this.adjustDescs.Init(ele.XPathSelectElements("./AdjustDescs/Item"));
            this.hiddenAdjustDescs.Init(ele.XPathSelectElements("./HiddenAdjustDescs/Item"));

            var errorsEle = ele.XPathSelectElement("./Errors");
            this.errorMsgs = (errorsEle == null || errorsEle.Value == null) ? string.Empty : errorsEle.Value.Trim();
        }

        /// <summary>
        /// Compare with other ApplicantPrice object.
        /// </summary>
        /// <param name="other">The object to compare with the current object.</param>
        /// <param name="log">The log storage.</param>
        /// <param name="fileLabels">The file labels.</param>
        /// <returns>Return true if the specified object is equal to the current object; otherwise return false.</returns>
        internal bool Compare(ApplicantPrice other, StringBuilder log, Tuple<string, string> fileLabels)
        {
            bool result = true;
            this.allowWriteHdr = true;
            const bool IgnoreToCompareDisqualityApplicants = true;

            if (!string.IsNullOrEmpty(this.errorMsgs) && !string.IsNullOrEmpty(other.errorMsgs))
            {
                log.AppendFormat("{0}CApplicantPriceXml's lLpTemplateId = {1} contains some error message.", Environment.NewLine, this.attributes["lLpTemplateId"]).AppendLine();
                return true;
            }

            if (this.denialReasons.HasData && other.denialReasons.HasData && IgnoreToCompareDisqualityApplicants)
            {
                log.AppendFormat("{0}CApplicantPriceXml's lLpTemplateId = {1} disqualify.", Environment.NewLine, this.attributes["lLpTemplateId"]).AppendLine();
                return true;
            }

            string diffList = PricingCmpHelper.GetDifferentList(this.attributes, other.attributes, "\t");
            if (diffList != string.Empty)
            {
                this.WriteErrorHdrIfNeeded(log);
                log.Append(diffList);
                result = false;
            }

            // Compare CApplicantRateOption
            if (this.CompareRateOptions(this.rateOptions, other.rateOptions, log, fileLabels) == false)
            {
                result = false;
            }

            if (this.CompareRateOptions(this.representativeRateOptions, other.representativeRateOptions, log, fileLabels, true /*isRepresentativeRateOption*/) == false)
            {
                result = false;
            }

            string errString;

            // Compare DenialReasons
            if (this.denialReasons.Compare(other.denialReasons, "\t" /*indent*/, out errString, fileLabels) == false)
            {
                this.WriteErrorHdrIfNeeded(log);

                // With some configure, program doesn't get all denial reasons and only get some reasons. 
                // What reported reasons depends on the policiy order.
                if (this.denialReasons.HasData == other.denialReasons.HasData)
                {
                    log.AppendFormat("\tWarning: two {0} and {1} DenialReasons sections are not identical.", fileLabels.Item1.Trim(), fileLabels.Item2.Trim()).AppendLine();
                }
                else
                {
                    log.Append(errString);
                    result = false;
                }
            }

            if (this.stips.Compare(other.stips, "\t" /*indent*/, out errString, fileLabels) == false)
            {
                this.WriteErrorHdrIfNeeded(log);
                log.AppendFormat(errString);
                result = false;
            }

            if (this.hiddenStips.Compare(other.hiddenStips, "\t" /*indent*/, out errString, fileLabels) == false)
            {
                this.WriteErrorHdrIfNeeded(log);
                log.AppendFormat(errString);
                result = false;
            }

            if (this.adjustDescs.Compare(other.adjustDescs, "\t" /*indent*/, out errString, fileLabels) == false)
            {
                this.WriteErrorHdrIfNeeded(log);
                log.AppendFormat(errString);
                result = false;
            }

            if (this.hiddenAdjustDescs.Compare(other.hiddenAdjustDescs, "\t" /*indent*/, out errString, fileLabels) == false)
            {
                this.WriteErrorHdrIfNeeded(log);
                log.AppendFormat(errString);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Write error header if needed.
        /// </summary>
        /// <param name="log">The log storage.</param>
        private void WriteErrorHdrIfNeeded(StringBuilder log)
        {
            if (this.allowWriteHdr)
            {
                log.AppendFormat("{0}CApplicantPriceXml's lLpTemplateId = {1} error: ", Environment.NewLine, this.attributes["lLpTemplateId"]).AppendLine();
                this.allowWriteHdr = false;
            }
        }

        /// <summary>
        /// Compare two AppRateOption collections.
        /// </summary>
        /// <param name="rateOptions">The 1st AppRateOption collection.</param>
        /// <param name="other">The 2nd AppRateOption collection.</param>
        /// <param name="log">The log storage.</param>
        /// <param name="fileLabels">The file labels.</param>
        /// <param name="isRepresentativeRateOption">Specify what kind of AppRateOption.</param>
        /// <returns>Return true if the specified object is equal to the current object; otherwise return false.</returns>
        private bool CompareRateOptions(
                                        Dictionary<string, AppRateOption> rateOptions, 
                                        Dictionary<string, AppRateOption> other, 
                                        StringBuilder log, 
                                        Tuple<string, string> fileLabels, 
                                        bool isRepresentativeRateOption = false)
        {
            // Compare CApplicantRateOption
            bool result = true;
            string prefix = isRepresentativeRateOption ? "RepresentativeRateOptions' " : string.Empty;

            foreach (var entry in rateOptions)
            {
                string key = entry.Key;
                AppRateOption otherAppRateOption;
                if (other.TryGetValue(key, out otherAppRateOption) == false)
                {
                    this.WriteErrorHdrIfNeeded(log);
                    log.AppendFormat("\tOnly {0} CApplicantPrice has {1}CApplicantRateOption: {2}", fileLabels.Item1, prefix, entry.Value.RatePointApr + ", " + key).AppendLine();
                    result = false;
                    continue;
                }

                string errLog;
                if (entry.Value.Compare(otherAppRateOption, "\t\t" /*indent*/, out errLog, fileLabels) == false)
                {
                    string displayKey = key;
                    if (entry.Value.RatePointApr == otherAppRateOption.RatePointApr)
                    {
                        displayKey = entry.Value.RatePointApr + ", " + key;
                    }

                    this.WriteErrorHdrIfNeeded(log);
                    log.AppendFormat("\t{0}CApplicantRateOption {1} different", prefix, displayKey).AppendLine();
                    log.Append(errLog);
                    result = false;
                }
            }

            foreach (string key in other.Keys)
            {
                if (rateOptions.ContainsKey(key) == false)
                {
                    this.WriteErrorHdrIfNeeded(log);
                    log.AppendFormat("\tOnly {0} CApplicantPrice has {1}CApplicantRateOption: {2}", fileLabels.Item2, prefix, other[key].RatePointApr + ", " + key).AppendLine();
                    result = false;
                }
            }

            return result;
        }
    }
}