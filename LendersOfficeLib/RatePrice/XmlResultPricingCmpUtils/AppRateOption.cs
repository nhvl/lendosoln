﻿///
/// Author: Thien Nguyen
/// 
namespace LendersOfficeApp.RatePrice.XmlResultPricingCmpUtils
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Xml.Linq;
    using System.Xml.XPath;

    /// <summary>
    /// This class contains information for xml element <CApplicantRateOption Rate="6.500" Point="-0.861" Apr="6.539%" > ... </CApplicantRateOption>
    /// </summary>
    internal class AppRateOption
    {
        /// <summary>
        /// CApplicantRateOption's attributes.
        /// </summary>
        private Dictionary<string, string> attributes = new Dictionary<string, string>();

        /// <summary>
        /// Subitems ClosingCost.
        /// </summary>
        private SimpleItems closingCosts = new SimpleItems("AppRateOption", "ClosingCost", new List<string>() { "HudLine", "Description" });

        /// <summary>
        /// Gets Key that is created from the AppRateOption's attributes "RawRate", "RawPoint". 
        /// </summary>.
        internal string Key { get; private set; } // "RawRate", "RawPoint"

        /// <summary>
        /// Gets RatePointApr with format "Rate=value, Point=value, Apr=value".
        /// </summary>
        internal string RatePointApr
        {
            get
            {
                return PricingCmpHelper.GetValues(this.attributes, new string[] { "Rate", "Point", "Apr" });
            }
        }

        /// <summary>
        /// Initialize object from xml element CApplicantRateOption.
        /// </summary>
        /// <param name="ele">The xml element CApplicantRateOption.</param>
        internal void Init(XElement ele)
        {
            PricingCmpHelper.GetAttributes(ele, this.attributes);
            this.attributes.Remove("PointVal");
            this.Key = this.CreateKey();

            this.closingCosts.Init(ele.XPathSelectElements("./ClosingCostBreakdown/ClosingCost"));
        }

        /// <summary>
        /// Compare with other AppRateOption object.
        /// </summary>
        /// <param name="other">The object to compare with the current object.</param>
        /// /// <param name="indent">Indent for each error message line.</param>
        /// <param name="errLog">The ouput: error messages.</param>
        /// <param name="fileLabels">The file labels.</param>
        /// <returns>Return true if the specified object is equal to the current object; otherwise return false.</returns>
        internal bool Compare(AppRateOption other, string indent, out string errLog, Tuple<string, string> fileLabels)
        {
            errLog = string.Empty;
            StringBuilder sb = new StringBuilder();

            bool result = true;

            // Compare CApplicantRateOption's attributes
            string diffList = PricingCmpHelper.GetDifferentList(this.attributes, other.attributes, indent);
            if (diffList != string.Empty)
            {
                if (this.RatePointApr != other.RatePointApr)
                {
                    // same RawRate and RawPoint but diffrent RatePointApr
                    sb.AppendFormat("{0}{1} '{2}'", indent, fileLabels.Item1, this.RatePointApr).AppendLine();
                    sb.AppendFormat("{0}{1} '{2}'", indent, fileLabels.Item2, other.RatePointApr).AppendLine();
                }

                sb.Append(diffList);
                result = false;
            }

            // Compare ClosingCost set
            string errString;
            if (this.closingCosts.Compare(other.closingCosts, indent, out errString, fileLabels) == false)
            {
                sb.AppendFormat(errString);
                result = false;
            }

            errLog = sb.ToString();

            return result;
        }

        /// <summary>
        /// Create a key from attributes "RawRate", "RawPoint".
        /// </summary>
        /// <returns>Return key.</returns>
        private string CreateKey()
        {
            return PricingCmpHelper.GetValues(this.attributes, new string[] { "RawRate", "RawPoint" });
        }
    }
}