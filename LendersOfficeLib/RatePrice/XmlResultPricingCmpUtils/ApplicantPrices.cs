﻿///
/// Author: Thien Nguyen
/// 
namespace LendersOfficeApp.RatePrice.XmlResultPricingCmpUtils
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Xml.Linq;
    using System.Xml.XPath;

    /// <summary>
    /// Represent for Pricing result. It contains many CApplicantPriceXml elements.
    /// </summary>
    internal class ApplicantPrices
    {
        /// <summary>
        /// Map : lLpTemplateId -> ApplicantPrice.
        /// </summary>
        private Dictionary<string, ApplicantPrice> appPriceDict = new Dictionary<string, ApplicantPrice>();

        /// <summary>
        /// Initialize object from Xml Pricing Result File Name.
        /// </summary>
        /// <param name="xmlPricingResultFname">Xml Pricing Result File Name.</param>
        public void InitFromFile(string xmlPricingResultFname)
        {
            this.appPriceDict.Clear();
            XDocument doc = XDocument.Load(xmlPricingResultFname);

            this.Init(doc);
        }

        /// <summary>
        /// Initialize object from Xml Pricing Result string.
        /// </summary>
        /// <param name="xmlPricingResult">Xml Pricing Result string.</param>
        public void Init(string xmlPricingResult)
        {
            this.appPriceDict.Clear();
            XDocument doc = XDocument.Parse(xmlPricingResult);
            this.Init(doc);
        }

        /// <summary>
        /// Compare with other ApplicantPrices object.
        /// </summary>
        /// <param name="other">The ApplicantPrices object to compare with the current ApplicantPrices object.</param>
        /// <param name="log">Contain error messages.</param>
        /// <param name="fileLabels">The file labels.</param>
        /// <returns>Return true if the specified object is equal to the current object; otherwise return false.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1305:FieldNamesMustNotUseHungarianNotation", Justification = "Legacy field name.")]
        public bool Compare(ApplicantPrices other, StringBuilder log, Tuple<string, string> fileLabels)
        {
            bool result = true;
            foreach (var entry in this.appPriceDict)
            {
                string lLpTemplateId = entry.Key;
                ApplicantPrice appPrice = entry.Value;

                if (other.appPriceDict.ContainsKey(lLpTemplateId) == false)
                {
                    log.AppendFormat("Only {1} ApplicantPrices has CApplicantPriceXml's lLpTemplateId = {0}", lLpTemplateId, fileLabels.Item1).AppendLine();
                    result = false;
                    continue;
                }

                ApplicantPrice otherAppPrice = other.appPriceDict[lLpTemplateId];

                if (appPrice.Compare(otherAppPrice, log, fileLabels) == false)
                {
                    result = false;
                }
            }

            foreach (string lLpTemplateId in other.appPriceDict.Keys)
            {
                if (this.appPriceDict.ContainsKey(lLpTemplateId) == false)
                {
                    log.AppendFormat("Only {1} ApplicantPrices has CApplicantPriceXml's lLpTemplateId = {0}", lLpTemplateId, fileLabels.Item2).AppendLine();
                    result = false;
                    continue;
                }
            }

            return result;
        }

        /// <summary>
        /// Inititalize object from XDocument.
        /// </summary>
        /// <param name="doc">The imput XDocument object.</param>
        private void Init(XDocument doc)
        {
            var appPriceList = doc.XPathSelectElements("//CApplicantPrices/CApplicantPriceXml");
            foreach (var ele in appPriceList)
            {
                ApplicantPrice appPrice = new ApplicantPrice();
                appPrice.Init(ele);

                this.appPriceDict.Add(ele.Attribute("lLpTemplateId").Value, appPrice);
            }
        }
    }
}