using System;


namespace LendersOfficeApp.los.RatePrice
{
	abstract public class CSymbolEntry
	{
        protected CSymbolEntry(E_Purpose purposes)
            : this((int)purposes)
        {
        }
		protected CSymbolEntry( int purposes )
		{
			InputKeyword = null;
			XmlOperator = "";
			Purposes = purposes;
            ExpressionEvaluator = null;
			XmlReplacement = null;
		}

		public string InputKeyword { get; set;}
		public string XmlOperator { get; set;}
		public int Purposes { get; private set;}
		public CSymbolTable.Evaluator ExpressionEvaluator { get; set;}
		public string XmlReplacement { get; set;}

        public virtual bool IsPeudoKeyword
        {
            get { return false; }
        }

	};

	public class CSymbolNonOpSyntax : CSymbolEntry
	{
		public CSymbolNonOpSyntax( int purposes, string userInput, string replacement )
			: base( purposes )
		{
			InputKeyword = userInput;
			XmlReplacement = replacement;
		}
	}

	public class CSymbolOperator : CSymbolEntry
	{
		public CSymbolOperator( int purposes, string userInput, string operatorName, CSymbolTable.Evaluator evaluator )
			: base( purposes )
		{
			InputKeyword = userInput;
			XmlOperator = operatorName;
			XmlReplacement = "<Op>" + operatorName + "</Op>";
			ExpressionEvaluator = evaluator;
		}
	}
	public class CSymbolFunction : CSymbolEntry
	{
		public static string ParameterText
		{
			get { return "#OfMonths"; }
		}
		public static string BuildFunctionKeyword( string functionName )
		{
			return functionName + ":" + ParameterText;
		}
		private CSymbolTable.Function m_function;
		public CSymbolFunction( int purposes, string functionName,  CSymbolTable.Function function )
			: base( purposes )
		{
			InputKeyword = BuildFunctionKeyword( functionName ); 
			m_function = function;
		}
		public string CalculateXmlFunctionReplacement( string param )
		{
            XmlReplacement = String.Format("<FunctionIntParams param=\"{1}\">{0}</FunctionIntParams>", InputKeyword, param);
            return XmlReplacement;
		}

        public virtual decimal Call(int param )
        {
            if( null == m_function )
                throw new CEvaluationException("Unable to evaluate function " + InputKeyword + ". You may need to pull credit report to execute this rule.");
            return m_function(  param );


        }

	}


	public class CSymbolFunctionStrParams : CSymbolEntry
	{
		public static string ParameterText
		{
			get { return "StrParams"; }
		}

		public static string BuildFunctionKeyword( string functionName )
		{
			return functionName + ":" + ParameterText;
		}
		private CSymbolTable.FunctionStrParams m_function;

        public CSymbolFunctionStrParams(string functionName, CSymbolTable.FunctionStrParams function)
            : base(E_Purpose.Condition)
        {
            InputKeyword = BuildFunctionKeyword(functionName);
            m_function = function;
        }

		public string CalculateXmlFunctionReplacement( string param )
		{
            if( param != null)
                param = LendersOffice.Common.Utilities.SafeHtmlString(param); 
            else
                param = "dummyString";

            XmlReplacement = String.Format("<FunctionStrParams param=\"{1}\">{0}</FunctionStrParams>", InputKeyword, param);

			return XmlReplacement;
		}

        public virtual decimal Call(string param )
        {
            if( null==m_function )
                throw new CEvaluationException("Unable to evaluate function " + InputKeyword + ". You may need to pull credit report to execute this rule.");
            return m_function(  param );


        }
	}

	

	public class CSymbolFieldName : CSymbolEntry
	{
        public CSymbolFieldName(string fieldName, CSymbolTable.Get getDelegate)
            : base (E_Purpose.Condition)
        {
            InputKeyword = fieldName;

            XmlReplacement = "<FieldName>" + fieldName + @"</FieldName>";
            m_get = getDelegate;

        }
		public CSymbolFieldName( int purposes, string fieldName, CSymbolTable.Get getDelegate )
			: base( purposes )
		{
			InputKeyword = fieldName;

            XmlReplacement = "<FieldName>"	+ fieldName + @"</FieldName>";
            m_get = getDelegate;


		}

        private CSymbolTable.Get m_get;

        public virtual decimal Get()
        {
            decimal result = m_get();
            return result;
        }

        virtual public bool DependonLoanData()
        {
            return true;
        }

	}

    // it is a field name but its value doesn't depend on loan data
    public class CSymbolFieldNameAsPureMethod : CSymbolFieldName
    {
        public CSymbolFieldNameAsPureMethod( int purposes, string fieldName, CSymbolTable.Get getDelegate )
            : base( purposes, fieldName, getDelegate )
        {
        }

        override public bool DependonLoanData()
        {
            return false;
        }

    }
	public class CSymbolFieldValue : CSymbolEntry
	{
        decimal m_value;

		public CSymbolFieldValue( int purposes, string userInput, string val )
			: base( purposes )
		{
			InputKeyword = userInput;
			XmlReplacement = String.Format("<Number FieldValue=\"{1}\">{0}</Number>", val, userInput); //= "<Number>" + ToFormat( val ) + "</Number>";

            m_value = decimal.Parse( val );
		}

        public CSymbolFieldValue( int purposes, string userInput, string val, string  assocFieldName)
            : this( purposes, userInput, val)
        {
            m_fieldName = assocFieldName;
        }
        
        public CSymbolFieldValue( int purposes, string userInput, bool val )
			: this( purposes, userInput, val? "1" : "0" )
		{}

        public CSymbolFieldValue( int purposes, string userInput, bool val, string  assocFieldName)
            : this( purposes, userInput, val)
        {
            m_fieldName = assocFieldName;
        }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="enumName">User input but without the _n part</param>
		/// <param name="purposes"></param>
		/// <param name="val"></param>
		public CSymbolFieldValue( int purposes, string enumName, Enum enumVal )
			: base( purposes )
		{
			string val = enumVal.ToString( "D" );
			InputKeyword = enumName + "_" + val;
            XmlReplacement = String.Format("<Number FieldValue=\"{1}\">{0}</Number>", val, InputKeyword);

            m_value = decimal.Parse( val );
		}

        public CSymbolFieldValue( int purposes, string enumName, Enum enumVal, string assocFieldName )
            : this( purposes, enumName, enumVal )
        {
            m_fieldName = assocFieldName;
        }


        private String m_fieldName = String.Empty; // the field name associates with this data
        public  String FieldName 
        {
            get { return m_fieldName; }
        }

        public decimal ConstValue
        {
            get { return m_value; }
        }
        
	}

    // CSymbolEntry Mock classes using for new keywords aka pseudo keywords.

    public class CSymbolFieldNameMock : CSymbolFieldName
    {
        decimal m_val;
        public CSymbolFieldNameMock( string fieldName, decimal val )
            : base(fieldName, null /*getDelegate*/ )
        {
            m_val = val;
        }

        override public decimal Get()
        {
            return m_val;
        }


        override public bool DependonLoanData()
        {
            return false;
        }

        public override bool IsPeudoKeyword
        {
            get { return true; }
        }
    }

    public class CSymbolFunctionMock : CSymbolFunction
    {
        decimal m_val;
        public CSymbolFunctionMock( string functionName, decimal val )
            : base( (int) E_Purpose.Condition, functionName,  null /* CSymbolTable.Function */ )
        {
            m_val = val;
        }

        override public decimal Call(int param )
        {
            return m_val;
        }

        public override bool IsPeudoKeyword
        {
            get { return true; }
        }
    }

    public class CSymbolFunctionStrParamsMock : CSymbolFunctionStrParams
    {
        decimal m_val;
        public CSymbolFunctionStrParamsMock( string functionName, decimal val ):
            base( functionName,  null /* CSymbolTable.FunctionStrParams */ )
        {
            m_val = val;
        }

        override public decimal Call(string param )
        {
            return m_val;
        }

        public override bool IsPeudoKeyword
        {
            get { return true; }
        }
    }

}