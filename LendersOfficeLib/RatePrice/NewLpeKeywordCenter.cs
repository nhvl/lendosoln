using System;
using System.Text;
using System.Collections;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Common;


namespace LendersOfficeApp.los.RatePrice
{

    public class NewLpeKeywordCenter
    {
        static int TimeToLiveInSeconds = CompilerConst.IsReleaseModeVar ? ConstSite.TimeToLiveInSecond4NewKeyword : 2*60;
        static object   s_lock          = new object();
        static DateTime s_expired       = DateTime.MinValue;
        static Hashtable s_keywordTable = new Hashtable();
        static int s_errCounter         = 0;

        static public Hashtable Refresh()
        {
            bool logable = false;
            lock( s_lock )
            {
                try
                {
                    Tools.LogRegTest( "LpeNewKeywordUtility.ReadNewKeywordsFromDB() : refresh new keywords");

                    string errMsg;
                    s_keywordTable = LpeNewKeywordUtility.ReadNewKeywordsFromDB(out errMsg);
                    if( errMsg != "" )
                    {
                        if( s_errCounter < 20 )
                        {
                            Tools.LogError( "LpeNewKeywordUtility.ReadNewKeywordsFromDB() error. " + errMsg);
                            s_errCounter++;
                        }
                    }
                    else
                        s_errCounter = 0;
                    logable = ( s_expired < DateTime.Now );
                    s_expired = DateTime.Now.AddSeconds( TimeToLiveInSeconds );
                }
                catch( Exception exc )
                {
                    s_expired = DateTime.Now.AddSeconds( 60 );
                    Tools.LogErrorWithCriticalTracking( "LpeNewKeywordUtility.ReadNewKeywordsFromDB() error", exc);
                }

                if( logable )
                {
                    StringBuilder sb = new StringBuilder("[LpeNewKeyword] use  " + s_keywordTable.Count + " new keywords");
                    foreach( string keyword in s_keywordTable.Keys )
                        sb.Append( " *** " ).Append( keyword ); 
                    Tools.LogRegTest( sb.ToString() );
                }

                return s_keywordTable;
            }
        }

        static public Hashtable NewLpeKeywords
        {
            get
            {
                lock( s_lock )
                {
                    if( s_expired < DateTime.Now )
                        Refresh();
                    return s_keywordTable;
                }
            }
        }
    }

    public class LpeNewKeywordUtility
    {
        public const string FieldType = "Field";
        public const string FunctionIntType = "FunctionInt";
        public const string FunctionStringType = "FunctionString";

        static CaseInsensitiveHashtable s_officialKeywords;

        static LpeNewKeywordUtility()
        {
            try
            {
                s_officialKeywords = CSymbolTable.OfficialKeywords;
            }
            catch
            {
                s_officialKeywords = new CaseInsensitiveHashtable();
            }

        }


        static public bool IsValidNewKeyword( string keyword, out string errMsg )
        {
            errMsg = "";
            if( keyword == null || keyword.Length <= 1 )
            {
                errMsg = "Error : the keyword's length <= 1";   
                return false;
            }

            bool haveLetter = false;
            foreach( char c in keyword )
                if( char.IsLetter( c ))
                    haveLetter = true;
                else if( char.IsDigit( c ) == false && c != '_' )
                {
                    errMsg = "Error : keyword '" + keyword + "' contain the invalid character '" + c + "'";   
                    return false;
                }

            if( haveLetter == false )
            {
                errMsg = "Error : keyword '" + keyword + "' must contains at least a letter.";   
                return false;
            }


            string []words = {  keyword, 
                                CSymbolFunction.BuildFunctionKeyword(keyword), 
                                CSymbolFunctionStrParams.BuildFunctionKeyword(keyword)
                             };

            foreach( string word in words )
                if( s_officialKeywords.Contains( word ) )
                {
                    errMsg = "Error : keyword '" + keyword + "' is an official keyword";   
                    return false;
                }

            try
            {
                double.Parse( keyword );
                errMsg = "Error : '" + keyword + "' is a number. It is a invalid keyword";   
                return false;

            }
            catch
            {
            }


            return true;
        }

        static public void ExceptionIfInvalidNewKeyword( string keyword )
        {
            string errMsg;
            bool result = IsValidNewKeyword( keyword, out errMsg );
            if( errMsg != "" )
                throw new CBaseException(ErrorMessages.Generic, errMsg);
        }


        static public CSymbolEntry CreateSymbolEntry( string keyword, string defValue, string category)
        {
            ExceptionIfInvalidNewKeyword( keyword );
            try
            {
                decimal.Parse( defValue );
            }
            catch
            {
                throw new CBaseException(ErrorMessages.Generic, "Error : the new keyword's default value '" + defValue + "'  is not a number.");
            }

            decimal val     = decimal.Parse( defValue );

            if( string.Compare( category, LpeNewKeywordUtility.FieldType, true) == 0 )
                return new CSymbolFieldNameMock(  keyword,  val );

            if( string.Compare( category, LpeNewKeywordUtility.FunctionIntType, true) == 0 )
                return new CSymbolFunctionMock(  keyword,  val );

            if( string.Compare( category, LpeNewKeywordUtility.FunctionStringType, true) == 0 )
                return new CSymbolFunctionStrParamsMock(  keyword,  val );

            throw new CBaseException(ErrorMessages.Generic, "Unexpected category = '" + category + "'");
        }

        static public Hashtable ReadNewKeywordsFromDB()
        {
            string errMsg;
            Hashtable  table = ReadNewKeywordsFromDB( out errMsg );
            if( errMsg != "" )
                throw new CBaseException(ErrorMessages.Generic, errMsg);
            return table;

        }
        static public Hashtable ReadNewKeywordsFromDB(out string errMsg)
        {
            StringBuilder sb = new StringBuilder();
            Hashtable table  = new Hashtable();

            try
            {
                using(DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "ListNewLpeKeywords" ) )
                {
                    while( reader.Read() )
                    {
                        string keyword = "";
                        try
                        {
                            keyword         = (string)reader["NewKeywordName"];
                            string defValue = (string)reader["DefaultValue"];
                            string category = (string)reader["Category"];
        
                            CSymbolEntry entry = CreateSymbolEntry( keyword, defValue, category );
                            //Assert.AreNotEqual("", entry.InputKeyword, "Invalid NewLpeKeyword : " + keyword + " *** " + defValue + " *** " + category );
                            table.Add( entry.InputKeyword, entry);
                        }
                        catch( Exception exc )
                        {
                            if( exc.Message.IndexOf( keyword ) > 0 )
                                sb.Append(exc.Message).Append("\n");
                            else
                                sb.AppendFormat("Unsuccessfully process the keyword '{0}' : {1}\n", keyword, exc.Message);

                        }
                    }
                }               

            }
            catch( Exception outerExc )
            {
                sb.Append("\n").Append( outerExc.Message );
            }
            errMsg = sb.ToString();;
            return table;
        }
    }
}