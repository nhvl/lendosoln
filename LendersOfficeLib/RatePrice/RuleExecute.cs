///
/// Author: Thien Nguyen
/// 

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using System.Xml;
using DataAccess;
using LendersOffice.Common;


namespace LendersOfficeApp.los.RatePrice
{
    public enum LpeKeywordCategory
    {
        Invariant,
        QValue,
        Variant,
        Dti,

    }

    public class LpeKeywordUtil
    {
        static string[] DtiKeywords = {   
            //"Score1",
            "DebtRatioBottom".ToLower(),
            //"IncomeMonthly",
            "DebtRatioTop".ToLower(),
            //"PrimaryWageEarnerCreditScoresCount", 
            "NoteRateAfterLOCompAndRounding".ToLower(),
            "NoteRateBeforeLOCompAndRounding".ToLower(),
            "PointsBeforeLOCompAndRounding".ToLower(),
            "NoteRateIsIntegerMultipleOfOneEighth".ToLower(),
            "AvailReserveMonths".ToLower(),
            "RateOptionExceedsMaxDTI".ToLower(),
            "RateOptionFinalPrice".ToLower(),
            "RateOptionPointsPreFEMaxYSP".ToLower(),
            "CashToBorrower".ToLower()
        };

        static string[] QValueKeywords = {
            "QLAmt".ToLower(),
            "Qcltv".ToLower(),
            "Qltv".ToLower(),
            "Qscore".ToLower(),
            "ScoreQBC".ToLower() // QScore-dependent,
        };

        static string[] VariantButNotDtiOrQValueKeywords =
        {
            "AmortizationType".ToLower(),
            "ArmFixedTerm".ToLower(),
            "Due".ToLower(),
            "IncomeMonthly".ToLower(),
            "InterestOnly".ToLower(),
            "IsNegAmort".ToLower(),
            "IsPpAllowedInSubjPropState".ToLower(),
            "IsSameInvestorFor8020".ToLower(),
            "Term".ToLower(),
            "TLAmt".ToLower(),
            "LoanProductType".ToLower(),

            "AssetsAfterClosing".ToLower(),
            "AvailReserveMonths".ToLower(),
            "HasTempBuydown".ToLower(),
            "InterestedPartyContribPercent".ToLower(),
            "IsWithinFHA3Or4UnitMaxLoanAmount".ToLower(),
            "PresentDebtRatioTop".ToLower(),
            "TotalAvailReserveMonths".ToLower(),
            "ThirdPartyUwResultType".ToLower(),
            "InvestorId".ToLower(),
            "ARM1stAdjCap".ToLower(),
            "ARMNextAdjCap".ToLower(),
            "ARMLifeAdjCap".ToLower(),
            "ARMAdjPeriod".ToLower(),
            "ARMIndex".ToLower(),
            "Assumption".ToLower(),
            "IsConvertibleMortgage".ToLower(),
            "InterestOnlyMonths".ToLower(),
            "FannieLTV".ToLower(), //not sure if this needs to be here 
            "FannieCLTV".ToLower(),//its probably mutually exclusive from rennvoation loans.
            "Ltv".ToLower(),
            "CLtv".ToLower(),
            "PpmtPenaltyMonths".ToLower(),
            "IsConformingLAmt".ToLower(),
            "IsHighBalanceConformingLAMT".ToLower(),
            "CustomLoanProgramField_1".ToLower(),
            "CustomLoanProgramField_2".ToLower(),
            "CustomLoanProgramField_3".ToLower(),
            "CustomLoanProgramField_4".ToLower(),
            "CustomLoanProgramField_5".ToLower(),
            "InvestorLoanProgramField_1".ToLower(),
            "InvestorLoanProgramField_2".ToLower(),
            "InvestorLoanProgramField_3".ToLower(),
            "InvestorLoanProgramField_4".ToLower(),
            "InvestorLoanProgramField_5".ToLower(),
            "HCLTV".ToLower(),
            "MIOption".ToLower(),
            "LoanType".ToLower(),
            "IsBaseLAMTHighBalanceConformingLAMT".ToLower(),
            "IsBaseLAMTConformingLAMT".ToLower()
        };


        static bool ContainsKeyword(string expr, string keyword)
        {
            if (expr == null || expr.Length == 0 || keyword.Length == 0)
                return false;

            int idx = -1;

            while (true)
            {

                idx = expr.IndexOf(keyword, idx + 1);
                if (idx < 0)
                    return false;

                // verify : no substring.

                if (idx > 0)
                {
                    char prev = expr[idx - 1];
                    if (char.IsLetterOrDigit(prev))
                        continue;
                }

                int nextPos = idx + keyword.Length;
                if (nextPos < expr.Length)
                {
                    char next = expr[nextPos];
                    if (char.IsLetterOrDigit(next))
                        continue;
                }

                return true;

            }
        }

        static public LpeKeywordCategory GetLpeKeywordCategory(string expr)
        {
            expr = expr.ToLower();
            foreach (string keyword in DtiKeywords)
                if (ContainsKeyword(expr, keyword))
                    return LpeKeywordCategory.Dti;

            foreach (string keyword in VariantButNotDtiOrQValueKeywords)
                if (ContainsKeyword(expr, keyword))
                    return LpeKeywordCategory.Variant;

            foreach (string keyword in QValueKeywords)
                if (ContainsKeyword(expr, keyword))
                    return LpeKeywordCategory.QValue;

            return LpeKeywordCategory.Invariant;
        }
    }


    // the prefix Rt : Run-time

    public class RtAdjustTargetFactory
    {
        // This class only contains static members.
        // prevent create this object by using private constructor
        private RtAdjustTargetFactory()
        {
        }

        static public RtAdjustTarget Create(string targetName, string data, out CAdjustTarget.E_Target target)
        {
            switch (targetName)
            {
                case "Rate":
                    data = data.Replace("%", "");
                    target = CAdjustTarget.E_Target.Rate;
                    break;

                case "Qcltv":
                    data = data.Replace("%", "");
                    target = CAdjustTarget.E_Target.Qcltv;
                    break;

                case "Qltv":
                    data = data.Replace("%", "");
                    target = CAdjustTarget.E_Target.Qltv;
                    break;

                case "Qscore":
                    data = data.Replace("%", "");
                    target = CAdjustTarget.E_Target.Qscore;
                    break;

                case "MaxYsp":
                    data = data.Replace("%", "");
                    target = CAdjustTarget.E_Target.MaxYsp;
                    break;

                case "MaxFrontEndYsp":
                    data = data.Replace("%", "");
                    target = CAdjustTarget.E_Target.MaxFrontEndYsp;
                    break;

                case "MaxDti":
                    data = data.Replace("%", "");
                    target = CAdjustTarget.E_Target.MaxDti;
                    break;

                case "Fee":
                    target = CAdjustTarget.E_Target.Fee;
                    break;

                case "Margin":
                    target = CAdjustTarget.E_Target.Margin;
                    break;


                case "BaseMargin":
                    target = CAdjustTarget.E_Target.BaseMargin;
                    break;


                case "QLAmt":


                    target = CAdjustTarget.E_Target.QLAmt;


                    /*********** 
                    OPM 12935 : Change QLAmt's format from integer to an expression as  1.15 * LAMT
                    try
                    {
                        if( data.Length == 0 )
                            return SimpleRtAdjustTarget.Zero;

                        int number = int.Parse( data );
                        
                        return number != 0 ? new SimpleRtAdjustTarget(number) : SimpleRtAdjustTarget.OtherZero;
                    }
                    catch
                    {
                        return new ErrorRtAdjustTarget( string.Format(  "Encountering a rule that set QLAmt to an acceptable value:{0}", data ) );
                    }
                    ***********/
                    break;




                case "Qrate":
                    target = CAdjustTarget.E_Target.Qrate;
                    return SimpleRtAdjustTarget.Zero;


                case "QrateAdjust": // OPM 7686
                    data = data.Replace("%", "");
                    target = CAdjustTarget.E_Target.QrateAdjust;
                    break;

                case "TeaserRateAdjust": // OPM 7686
                    data = data.Replace("%", "");
                    target = CAdjustTarget.E_Target.TeaserRateAdjust;
                    break;

                case "LockDaysAdjust":
                    target = CAdjustTarget.E_Target.LockDaysAdjust;
                    break;


                default:
                    throw new CBaseException(ErrorMessages.EnumValueNotHandled, "Programming error:  Unexpected target type in CAdjustTarget constructor.");
            }

            if ("" == data)
                return SimpleRtAdjustTarget.Zero;

            try
            {
                decimal number = decimal.Parse(data);
                return number != 0 ? new SimpleRtAdjustTarget(number) : SimpleRtAdjustTarget.OtherZero;
            }
            catch
            {
            }

            return new ComplexRtAdjustTarget(data);
        }

    }


    abstract public class RtAdjustTarget
    {
        abstract public decimal Evaluate(XmlDocument xmlDocEval, CSymbolTable symbolTable, EvalStack stack);
        abstract public bool IsBlank { get; }
        abstract public int EstimateSize();
        abstract public bool IsConstant();

    }

    public class ErrorRtAdjustTarget : RtAdjustTarget
    {
        private string m_errMsg;

        public ErrorRtAdjustTarget(string errMsg)
        {
            m_errMsg = errMsg;
        }

        override public decimal Evaluate(XmlDocument xmlDocEval, CSymbolTable symbolTable, EvalStack stack)
        {
            Tools.LogErrorWithCriticalTracking(m_errMsg);
            return 0;
        }

        override public bool IsBlank { get { return false; } }

        override public bool IsConstant()
        {
            return false;
        }


        override public int EstimateSize()
        {
            int size = 16;

            if (m_errMsg != null)
                size += 20 + 2 * m_errMsg.Length;
            return size;
        }

    }


    public class SimpleRtAdjustTarget : RtAdjustTarget
    {
        readonly static public SimpleRtAdjustTarget Zero = new SimpleRtAdjustTarget(true);
        readonly static public SimpleRtAdjustTarget OtherZero = new SimpleRtAdjustTarget(false);

        static SimpleRtAdjustTarget()
        {
        }

        private decimal m_number;
        private bool m_isBlank;

        public SimpleRtAdjustTarget(decimal number)
        {
            m_isBlank = false;
            m_number = number;
        }

        public SimpleRtAdjustTarget(bool isBlank)
        {
            m_isBlank = isBlank;
            m_number = 0;
        }

        override public decimal Evaluate(XmlDocument xmlDocEval, CSymbolTable symbolTable, EvalStack stack)
        {
            return m_number;
        }

        override public bool IsBlank
        {
            get { return m_isBlank; }
        }

        override public bool IsConstant()
        {
            return true;
        }


        override public int EstimateSize()
        {
            int size = 32;
            return size;
        }


    }

    public class ComplexRtAdjustTarget : RtAdjustTarget
    {
        private string m_exprString;
        private bool m_isBlank;
        object m_expr; // XmlNode or PostfixExpression
        XmlNode m_debugExprNode;

        public ComplexRtAdjustTarget(string exprString)
        {
            m_exprString = exprString;
            m_isBlank = exprString.Length == 0;
        }

        override public decimal Evaluate(XmlDocument xmlDocEval, CSymbolTable symbolTable, EvalStack stack)
        {
            XmlNode exprNode = null;
            if (m_expr == null)
            {
                lock (this)
                {
                    if (m_expr == null)
                    {
                        object tmpExpr = CRuleEvalTool.ParseToXmlNodeOrPostfixExpr(symbolTable, xmlDocEval, m_exprString, ref exprNode);

                        m_exprString = null; // remove it when don't need it any more to save memory.
                        if (tmpExpr is XmlNode)
                        {
                            // "convert to postfix" error => using old method to evaluate expression
                            m_debugExprNode = exprNode;
                            m_expr = exprNode;
                        }
                        else
                        {
                            // typeof(tmpExpr) is PostfixExpression
                            m_debugExprNode = null;
                            m_expr = tmpExpr;
                        }
                    }

                }
            }

            decimal rawResult;
            object expr = m_expr;
            exprNode = m_debugExprNode;

            try
            {
                if (exprNode != null)
                    rawResult = CRuleEvalTool.ExpressionDispatch(exprNode, 0, symbolTable);
                else
                    rawResult = ((PostfixExpression)expr).Evaluate(symbolTable, stack, new StringBuilder(), false);

                return rawResult;
            }
            catch
            {
                return 0;
            }
        }

        override public bool IsBlank
        {
            get { return m_isBlank; }
        }

        override public bool IsConstant()
        {
            return false;
        }


        override public int EstimateSize()
        {
            int size = 32;
            return size;
        }

    }




    public class RtRuleList : List<AbstractRtRule>
    {

        public RtRuleList(int capacity)
            : base(capacity)
        {
        }
        public AbstractRtRule GetRule(int index)
        {
            return this[index];
        }

        public int EstimateSize()
        {
            int size = 28;
            if (Capacity > 0)
                size += 12 + 4 * Capacity;

            foreach (AbstractRtRule rule in this)
                if (rule != null)
                    size += rule.EstimateSize();
            return size;

        }

    }

    public class PricePolicyTools
    {
        public static string ReducePolicySize(string pricePolicyXml)
        {
            pricePolicyXml = RemoveEmptyAdjustTargets(pricePolicyXml);

            StringBuilder sb = new StringBuilder(pricePolicyXml);
            sb.Replace("<Stipulation></Stipulation>", "<Stipulation />");
            sb.Replace("<ConditionEx></ConditionEx>", "<ConditionEx />");

            return sb.ToString();
        }

        public static string RemoveEmptyAdjustTargets(string pricePolicyXml)
        {
            if (pricePolicyXml == null || 0 == pricePolicyXml.Length)
                return pricePolicyXml;

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(pricePolicyXml);

            XmlNodeList adjustElements = xmlDoc.SelectNodes(".//Consequence/Adjust[@Target]");

            ArrayList arrNodes = new ArrayList();
            foreach (XmlNode node in adjustElements)
                arrNodes.Add(node);

            foreach (XmlNode node in arrNodes)
            {
                // 11/05/10 mf. OPM 43151. Front End Max Ysp is non-additive, so 0 can be a legal adjustment target.
                // ( Note also that MaxDTI is non-additive, but we treat 0 as blank).
                bool acceptZeroAdjustment = (node.Attributes["Target"].Value != null &&
                    node.Attributes["Target"].Value == "MaxFrontEndYsp");

                if (node.InnerText != null && node.InnerText.Length > 0
                    && (acceptZeroAdjustment || node.InnerText != "0")
                   )
                    continue;

                if (node.Attributes.Count != 1)
                    continue;

                if (node.ParentNode != null)
                    node.ParentNode.RemoveChild(node);
            }
            pricePolicyXml = xmlDoc.OuterXml;

            return pricePolicyXml;
        }

        public static int ReducePolicySizeFromDb(out int numRecordNeedToModify, ProgressCallback callback)
        {
            return ReducePolicySizeFromDb(int.MaxValue, out numRecordNeedToModify, callback);
        }

        public delegate void ProgressCallback(int processedPolicies, int totalPolicies);

        public static int ReducePolicySizeFromDb(int maxPolicies, out int numRecordNeedToModify, ProgressCallback callback)
        {
            numRecordNeedToModify = 0;
            int total = 0;

            Hashtable policyIds = GetPolicyIds(maxPolicies);

            ArrayList blockIds = new ArrayList();
            ArrayList sqlParameters = new ArrayList();


            const int BlockSize = 200;
            int counter = 0;
            foreach (Guid id in policyIds.Keys)
            {
                counter++;
                blockIds.Add(id);
                if (blockIds.Count >= BlockSize || counter == policyIds.Count)
                {
                    sqlParameters.Clear();

                    string selectSql = "select * from price_policy where PricePolicyId " +
                                       DbTools.CreateParameterized4InClauseSql("Po", blockIds, sqlParameters);

                    int numRecordNeedToModifyTmp = 0;
                    total += ReducePolicySizeFromDb(selectSql, sqlParameters, out numRecordNeedToModifyTmp);
                    numRecordNeedToModify += numRecordNeedToModifyTmp;

                    blockIds.Clear();

                    if (callback != null)
                        callback(counter, policyIds.Count);
                }
            }

            return total;
        }

        static public Hashtable GetPolicyIds(int maxPolicies)
        {
            string selectIdsSql = "select PricePolicyId from PRICE_POLICY";
            if (maxPolicies > 0 && maxPolicies != int.MaxValue)
                selectIdsSql = string.Format("select top {0} PricePolicyId from PRICE_POLICY", maxPolicies);

            Hashtable ids = new Hashtable();
            Action<IDataReader> readerCode = delegate (IDataReader r)
            {
                while (r.Read())
                {
                    ids[r["PricePolicyId"]] = null;
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, selectIdsSql, null, null, readerCode);
            return ids;

        }

        private static int ReducePolicySizeFromDb(string selectSql, ArrayList sqlParameters, out int numRecordNeedToModify)
        {
            // 11/21/2007 ThienNguyen - Reviewed and unsafe in future. Thien will reviewe it again.
            CDataSet ds = new CDataSet();
            ds.LoadWithSqlQueryDangerously(DataSrc.LpeSrc, selectSql, sqlParameters);

            int numRecord = 0;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string xml = row["PricePolicyXmlText"] as string;
                if (xml == null || xml.Length == 0)
                    continue;
                string compactXml = ReducePolicySize(xml);

                if (xml != compactXml)
                {
                    row["PricePolicyXmlText"] = compactXml;
                    numRecord++;
                }
            }
            numRecordNeedToModify = numRecord;

            return ds.Save();

        }
    }



    public struct RuleCategoryCounter // POD
    {
        public int PolicyNotCallLoadRules_Counter;
        public int LoadedRuleCounter;

        public int InvariantRuleCounter;
        public int QValueRuleCounter;
        public int VariantRuleCounter;
        public int DtiRuleCounter;

        public void Init()
        {
            this = new RuleCategoryCounter();
        }


    }

    // 03/28/09 mf. OPM 25872.
    // POD Representing a borrower mode and application index
    public class BorrowerCreditInfo
    {
        public E_aBorrowerCreditModeT BorrowerCredit = E_aBorrowerCreditModeT.Borrower;
        public int appIndex = 0;

        public BorrowerCreditInfo(E_aBorrowerCreditModeT CreditT, int appI)
        {
            BorrowerCredit = CreditT;
            appIndex = appI;
        }
    }

    // 03/28/09 mf. OPM 25872.
    // Collection of applications of a loan.
    public class LoanApplicationList
    {
        private System.Collections.Generic.List<ApplicationSpec> m_applications;
        private CPageData m_dataLoan;
        private BorrowerSpec x_primaryBorrower;

        public LoanApplicationList(CPageData DataLoan)
        {
            m_dataLoan = DataLoan;

            LoadApplications();
        }

        private void LoadApplications()
        {
            BorrowerSpec newBorr;
            BorrowerSpec newCoborr;
            m_applications = new System.Collections.Generic.List<ApplicationSpec>(m_dataLoan.nApps);

            for (int appIndex = 0; appIndex < m_dataLoan.nApps; appIndex++)
            {
                CAppData app = m_dataLoan.GetAppData(appIndex);

                bool IsBorrPrimary = app.aBIsPrimaryWageEarner;
                bool IsCoborrPrimary = app.aCIsPrimaryWageEarner;

                newBorr = new BorrowerSpec(app.aBFirstNm + " " + app.aBLastNm, IsBorrPrimary);
                newCoborr = (app.aCLastNm != string.Empty) ? new BorrowerSpec(app.aCFirstNm + " " + app.aCLastNm, IsCoborrPrimary) : null;

                m_applications.Add(new ApplicationSpec(newBorr, newCoborr, appIndex, app));
            }

        }

        public System.Collections.Generic.List<ApplicationSpec> Applications
        {
            get { return m_applications; }
        }

        // Find (and cache) the primary borrower from all the applications
        public BorrowerSpec PrimaryBorrower
        {
            get
            {
                if (x_primaryBorrower == null)
                {
                    foreach (ApplicationSpec appSpec in m_applications)
                    {
                        foreach (BorrowerSpec borrSpec in new BorrowerSpec[] { appSpec.Borrower, appSpec.Coborrower })
                        {
                            if (borrSpec != null)
                            {
                                if (x_primaryBorrower == null || borrSpec.IsPrimary)
                                    x_primaryBorrower = borrSpec;
                            }
                        }
                    }
                }
                return x_primaryBorrower;
            }
        }

        // Cycle through all applications, only returning the borrowers that match the QBC
        public System.Collections.Generic.IEnumerable<BorrowerSpec> GetBorrowers(E_sRuleQBCType qbc)
        {
            // Code should never make it here with blank QBC--that is for a single app as joint,
            // not individual borrowers handled by this enumerator.  There is no concept of a joint single borrower
            Tools.Assert(qbc != E_sRuleQBCType.Legacy, "LoanApplicationList.GetBorrowers does not support blank QBC.");

            foreach (ApplicationSpec appSpec in m_applications)
            {
                foreach (BorrowerSpec borrSpec in new BorrowerSpec[] { appSpec.Borrower, appSpec.Coborrower })
                {
                    if (borrSpec != null)
                    {
                        switch (qbc)
                        {
                            case E_sRuleQBCType.AllBorrowers:
                                {
                                    yield return borrSpec;
                                    break;
                                }
                            case E_sRuleQBCType.PrimaryBorrower:
                                {
                                    if (borrSpec.IsPrimary)
                                        yield return borrSpec;

                                    break;
                                }
                            case E_sRuleQBCType.OnlyCoborrowers:
                                {
                                    if (!borrSpec.IsPrimary)
                                        yield return borrSpec;

                                    break;
                                }
                            default:
                                Tools.LogBug("Unhandled value of QBC in LoanApplicationList: " + qbc);
                                break;
                        }
                    }
                }
            }
        }

        // Set the application id context of the loanfile, and the credit mode of the credit report.
        public void SetPricingContext(BorrowerSpec borrower)
        {
            SetPricingContext(borrower.Application.AppIndex, borrower.CreditInfo.BorrowerCredit);
        }

        public void SetPricingContext(int ApplicationIndex, E_aBorrowerCreditModeT BorrowerCreditMode)
        {
            m_dataLoan.sSymbolTableForPriceRule.ActiveApplication = ApplicationIndex;
            m_applications[ApplicationIndex].Application.aBorrowerCreditModeT = BorrowerCreditMode;
        }

        // For credit stips, when there are multiple applications, we have to add the 
        // name of borrower who owns the liability.
        public string GetOwnerName(E_LiaOwnerT owner, int applicationIndex)
        {
            ApplicationSpec application = m_applications[applicationIndex];

            switch (owner)
            {
                case E_LiaOwnerT.Borrower:
                    return application.Borrower.FullName;
                case E_LiaOwnerT.CoBorrower:
                    return (application.Coborrower == null) ? string.Empty : application.Coborrower.FullName;
                case E_LiaOwnerT.Joint:
                    return application.Borrower.FullName + ((application.Coborrower == null) ? string.Empty : ("/" + application.Coborrower.FullName));
            }
            return string.Empty;
        }

        // A single application
        public class ApplicationSpec
        {
            public int AppIndex;
            public BorrowerSpec Borrower;
            public BorrowerSpec Coborrower;
            private CAppData m_appData;

            public ApplicationSpec(BorrowerSpec borrower, BorrowerSpec coborrower, int appIndex, CAppData app)
            {
                Borrower = borrower;
                if (Borrower != null)
                    Borrower.Application = this;

                Coborrower = coborrower;
                if (Coborrower != null)
                    Coborrower.Application = this;

                AppIndex = appIndex;
                m_appData = app;
            }

            public E_aBorrowerCreditModeT GetBorrowerType(BorrowerSpec borrower)
            {
                if (Borrower != null && Borrower == borrower)
                    return E_aBorrowerCreditModeT.Borrower;
                if (Coborrower != null && Coborrower == borrower)
                    return E_aBorrowerCreditModeT.Coborrower;

                // LPE is requesting a borrower that does not exist on this application.
                // Throwing exeption, or we will have inaccurate data.
                throw new CBaseException(ErrorMessages.Generic, "Borrower not found in application's borrower collection.");
            }

            public CAppData Application
            {
                get { return m_appData; }
            }

        }

        // A single borrower
        public class BorrowerSpec
        {
            public bool IsPrimary;
            public string FullName;

            public ApplicationSpec Application;
            private BorrowerCreditInfo x_borrowerCreditInfo;

            public BorrowerCreditInfo CreditInfo
            {
                get
                {
                    if (x_borrowerCreditInfo == null)
                    {
                        x_borrowerCreditInfo = new BorrowerCreditInfo(Application.GetBorrowerType(this), Application.AppIndex);
                    }
                    return x_borrowerCreditInfo;
                }
            }

            public BorrowerSpec(string name, bool isPrimary)
            {
                FullName = name;
                IsPrimary = isPrimary;
            }

        }

    }

}
