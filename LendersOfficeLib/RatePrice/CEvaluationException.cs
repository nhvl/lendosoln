/// Author: David Dao

using System;

using DataAccess;
namespace LendersOfficeApp.los.RatePrice
{
    /// <summary>
    /// Should use this class when throw exception during rule evaluation. Since I only provide constructor with developerMessage because user friendly
    /// message should be map from LPE_ERRMSG_MAP table.
    /// </summary>
	public class CEvaluationException : CBaseException
	{
        public enum ErrorTypeEnum 
        {
            MissingAndOp, MissingOrOp, MissingVariableName, MismatchFieldAndFieldValue, UnexpectOp, 
            UnMatchingParenthesis, DontAllowFieldValue, UnexpectColonChar, InvalidToken,
            InvalidFunctionStrParams, EmptyParentheses,


            Unknown = 9999
        };
        private ErrorTypeEnum m_errorType = ErrorTypeEnum.Unknown;

        public ErrorTypeEnum ErrorType
        {
            get { return m_errorType; }
        }

        private bool m_allowVerboseDisplay = false;

        const string DefaultUserMessage = "Loan Program is being updated. System engineers have been notified.";
		public CEvaluationException(string developerMessage) : base (DefaultUserMessage, developerMessage)
		{
		}

        public CEvaluationException(string userMessage, string developerMessage)  : base (userMessage, developerMessage)
        {
        }

        public CEvaluationException(string userMessage, Exception innerException) : base (userMessage, innerException)
        {
        }

        public CEvaluationException(ErrorTypeEnum errType, string developerMessage) : this (developerMessage)
        {
            m_errorType = errType;
        }

        /// <summary>
        /// ApplicantPrice use Exception.Message to display to user.
        /// </summary>
        public override string Message 
        {
            get { return (m_allowVerboseDisplay == false) ? UserMessage : DeveloperMessage; }
        }

        public bool AllowVerboseDisplay
        {
            set { m_allowVerboseDisplay = value; }    
        }
	}
}
