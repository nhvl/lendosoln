﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;

namespace LendersOfficeApp.los.RatePrice
{
    public class RateOptionExpirationResult
    {
        private StringBuilder m_sb; 
        private bool m_isBlockedRateLockSubmission = false;
        private string m_rateLockSubmissionUserWarningMessage = "";
        private string m_rateLockSubmissionDevWarningMessage = "";
        //private static Guid brokerID;

        // 4/19/2011 dd - OPM 61767 - Create this property to case 61767. When running internal
        // pricing, we want to see if expired rate is cause by outside of lock desk hour or not.
        public bool IsExpiredDueToOutsideLockDeskHour { get; set; }

        // 8/28/2013 dd - OPM 130012 - Add more detail on reason of expiration. 
        // Expire due to outside of investor cutoff hour.
        public bool IsExpiredDueToOutsideInvestorCutoffHour { get; set; }

        // 8/28/2013 dd - OPM 130012 - Add more detail on reason of expiration
        // Expire due to invalid map.
        public bool IsExpiredDueToMapError { get; set; }

        // 8/28/2013 dd - OPM 130012 - Add more detail on reason of expiration
        // Expire due to stale ratesheet
        public bool IsExpiredDueToRatesheetNotCurrent { get; set; }

        // 8/28/2013 dd - OPM 130012 - Add more detail on reason of expiration
        // Expire due to block manually by lender.
        public bool IsBlockedManuallyByLender { get; set; }

        // 8/28/2013 dd - OPM 130012 - Use this in internal pricing
        // to determine if the expiration is only cause by outside of lock desk hour.
        public bool IsExpiredDueToOutsideLockDeskHourOnly
        {
            get
            {
                return IsExpiredDueToOutsideLockDeskHour &&
                    (IsExpiredDueToOutsideInvestorCutoffHour == false &&
                    IsExpiredDueToMapError == false &&
                    IsExpiredDueToRatesheetNotCurrent == false &&
                    IsBlockedManuallyByLender == false);
            }
        }


        public bool IsBlockedRateLockSubmission
        {
            get { return m_isBlockedRateLockSubmission; }
            set { m_isBlockedRateLockSubmission = value; }
        }
        public string RateLockSubmissionUserWarningMessage
        {
            get { return m_rateLockSubmissionUserWarningMessage; }
            set { m_rateLockSubmissionUserWarningMessage = value; }
        }

        public string RateLockSubmissionDevWarningMessage
        {
            get { return m_rateLockSubmissionDevWarningMessage; }
            set { m_rateLockSubmissionDevWarningMessage = value; }
        }

        //AV OPM  27772 5 21 09 
        public bool IsHiddenFromResult { get; set; }
        public static bool IsBlockedRateLockSubmissionMessage(string errMsg)
        {
            if (string.IsNullOrEmpty(errMsg))
                return false;

            if (errMsg == ConstAppDavid.BlockedRateLockSubmission_ExpiredMessage ||
                errMsg.StartsWith(ConstAppDavid.BlockedRateLockSubmission_CutOffMessage) ||
                errMsg.StartsWith(ConstAppDavid.BlockedRateLockSubmission_OutsideNormalHour) ||
                errMsg.StartsWith(ConstAppDavid.BlockedRateLockSubmission_OutsideClosureDayHour) ||
                errMsg.StartsWith(ConstAppDavid.BlockedRateLockSubmission_OutsideNormalHourAndPassInvestorCutoff) ||
                errMsg.StartsWith(ConstAppDavid.BlockedRateLockSubmission_LenderManual)
                )
            {
                return true;
            }
            return false;
        }

        public RateOptionExpirationResult()
        {
            m_sb = new StringBuilder();
        }

        public void AppendLineToLog(string log)
        {
            m_sb.AppendLine(log);
        }

        public void AppendLineToLog(string format, params object[] param)
        {
            try
            {
                m_sb.AppendFormat(format, param);
            }
            catch (FormatException)
            {
                Tools.LogWarning("Format doesnt match param count.");
                m_sb.Append(format);
            }
            m_sb.AppendLine();
        }

        public string GetLog()
        {
            return m_sb.ToString();
        }
        public static string GetFriendlyRateLockSubmissionErrorMessage(Guid brokerId, Guid lockPolicyID, E_LpeRsExpirationByPassPermissionT lpeRsExpirationByPassPermission, string errMsg)
        {
            if (string.IsNullOrEmpty(errMsg))
                return string.Empty;

            //jk 12/23/08 - OPM 20030 Attempts to get RS Expiration warning messages from the database. If nothing is found
            // it defaults to the given messages hard coded in the ErrorMessages
            string normalUserExpiredMessage = "";
            string lockDeskExpiredMessage = "";
            string normalUserCutOffMessage = "";
            string lockDeskCutOffMessage = "";
            string outsideNormalHoursMessage = "";
            string outsideClosureDayHourMessage = "";
            string outsideNormalHoursAndPassInvestorCutOffMessage = "";

            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
            if (principal != null)
            {
                if (principal.BrokerId != Guid.Empty)
                {
                    brokerId = principal.BrokerId;
                }
            }

            if (brokerId == Guid.Empty)
            {
                return GetDefaultFriendlyRateLockSubmissionErrorMessage(lpeRsExpirationByPassPermission, errMsg);
            }
            else if (Guid.Empty == lockPolicyID)
            {
                var warningMessageParameters = new List<SqlParameter>();
                warningMessageParameters.Add(new SqlParameter("@Broker", brokerId));
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "getRSWarningMessageById", warningMessageParameters))
                {
                    if (reader.Read())
                    {
                        normalUserExpiredMessage = (string)reader["NormalUserExpiredCustomMsg"];
                        lockDeskExpiredMessage = (string)reader["LockDeskExpiredCustomMsg"];
                        normalUserCutOffMessage = (string)reader["NormalUserCutOffCustomMsg"];
                        outsideNormalHoursMessage = (string)reader["OutsideNormalHrCustomMsg"];
                        lockDeskCutOffMessage = (string)reader["LockDeskCutOffCustomMsg"];
                        outsideClosureDayHourMessage = (string)reader["OutsideClosureDayHrCustomMsg"];
                        outsideNormalHoursAndPassInvestorCutOffMessage = (string)reader["OutsideNormalHrandPassInvestorCutOffCustomMsg"];
                    }
                }
            }
            else
            {

                var warningMessageParameters = new List<SqlParameter>();
                warningMessageParameters.Add(new SqlParameter("@Broker", brokerId));
                warningMessageParameters.Add(new SqlParameter("@PolicyId", lockPolicyID));
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "getRSWarningMessageByBrokerPolicyId", warningMessageParameters))
                {
                    if (reader.Read())
                    {
                        normalUserExpiredMessage = (string)reader["NormalUserExpiredCustomMsg"];
                        lockDeskExpiredMessage = (string)reader["LockDeskExpiredCustomMsg"];
                        normalUserCutOffMessage = (string)reader["NormalUserCutOffCustomMsg"];
                        outsideNormalHoursMessage = (string)reader["OutsideNormalHrCustomMsg"];
                        lockDeskCutOffMessage = (string)reader["LockDeskCutOffCustomMsg"];
                        outsideClosureDayHourMessage = (string)reader["OutsideClosureDayHrCustomMsg"];
                        outsideNormalHoursAndPassInvestorCutOffMessage = (string)reader["OutsideNormalHrandPassInvestorCutOffCustomMsg"];
                    }


                }
            }

            if (errMsg == ConstAppDavid.BlockedRateLockSubmission_ExpiredMessage)
            {
                return BlockedRateLockSubmission_ExpiredMessage(lpeRsExpirationByPassPermission, normalUserExpiredMessage, lockDeskExpiredMessage);
            }
            else if (errMsg.StartsWith(ConstAppDavid.BlockedRateLockSubmission_CutOffMessage))
            {
                return BlockedRateLockSubmission_CutOffMessage(lpeRsExpirationByPassPermission, errMsg, normalUserCutOffMessage, lockDeskCutOffMessage);
            }
            else if (errMsg.StartsWith(ConstAppDavid.BlockedRateLockSubmission_OutsideNormalHour))
            {
                return BlockedRateLockSubmission_OutsideNormalHour(lpeRsExpirationByPassPermission, errMsg, outsideNormalHoursMessage);
            }
            else if (errMsg.StartsWith(ConstAppDavid.BlockedRateLockSubmission_OutsideClosureDayHour))
            {
                return BlockedRateLockSubmission_OutsideClosureDayHour(lpeRsExpirationByPassPermission, outsideClosureDayHourMessage);
            }
            else if (errMsg.StartsWith(ConstAppDavid.BlockedRateLockSubmission_OutsideNormalHourAndPassInvestorCutoff))
            {
                return BlockedRateLockSubmission_OutsideNormalHourAndPassInvestorCutoff(errMsg, outsideNormalHoursAndPassInvestorCutOffMessage);

            }
            else if (errMsg.StartsWith(ConstAppDavid.BlockedRateLockSubmission_LenderManual))
            {
                return BlockedRateLockSubmission_LenderManual(errMsg);
            }
            return string.Empty;
        }

        //OPM jk 1-2-08 OPM 20030
        // Doesn't make any database calls, simply uses the messages that are hard coded
        public static string GetDefaultFriendlyRateLockSubmissionErrorMessage(E_LpeRsExpirationByPassPermissionT lpeRsExpirationByPassPermission, string errMsg)
        {
            if (null == errMsg || "" == errMsg)
                return "";

            string normalUserExpiredMessage = "";
            string lockDeskExpiredMessage = "";
            string normalUserCutOffMessage = "";
            string lockDeskCutOffMessage = "";
            string outsideNormalHoursMessage = "";
            string outsideClosureDayHourMessage = "";
            string outsideNormalHoursAndPassInvestorCutOffMessage = "";

            if (errMsg == ConstAppDavid.BlockedRateLockSubmission_ExpiredMessage)
            {
                return BlockedRateLockSubmission_ExpiredMessage(lpeRsExpirationByPassPermission, normalUserExpiredMessage, lockDeskExpiredMessage);
            }
            else if (errMsg.StartsWith(ConstAppDavid.BlockedRateLockSubmission_CutOffMessage))
            {
                return BlockedRateLockSubmission_CutOffMessage(lpeRsExpirationByPassPermission, errMsg, normalUserCutOffMessage, lockDeskCutOffMessage);
            }
            else if (errMsg.StartsWith(ConstAppDavid.BlockedRateLockSubmission_OutsideNormalHour))
            {
                return BlockedRateLockSubmission_OutsideNormalHour(lpeRsExpirationByPassPermission, errMsg, outsideNormalHoursMessage);
            }
            else if (errMsg.StartsWith(ConstAppDavid.BlockedRateLockSubmission_OutsideClosureDayHour))
            {
                return BlockedRateLockSubmission_OutsideClosureDayHour(lpeRsExpirationByPassPermission, outsideClosureDayHourMessage);
            }
            else if (errMsg.StartsWith(ConstAppDavid.BlockedRateLockSubmission_OutsideNormalHourAndPassInvestorCutoff))
            {
                return BlockedRateLockSubmission_OutsideNormalHourAndPassInvestorCutoff(errMsg, outsideNormalHoursAndPassInvestorCutOffMessage);

            }
            else if (errMsg.StartsWith(ConstAppDavid.BlockedRateLockSubmission_LenderManual))
            {
                return BlockedRateLockSubmission_LenderManual(errMsg);
            }
            return string.Empty;
        }

        public static string GetFriendlyRateLockSubmissionErrorMessage(Guid lockPolicyID, E_LpeRsExpirationByPassPermissionT lpeRsExpirationByPassPermission, string errMsg, Guid brokerID)
        {
            string returnString = GetFriendlyRateLockSubmissionErrorMessage(brokerID, lockPolicyID, lpeRsExpirationByPassPermission, errMsg);
            return returnString;
        }

        public string GetFriendlyRateLockSubmissionErrorMessage(Guid lockPolicyID, E_LpeRsExpirationByPassPermissionT lpeRsExpirationByPassPermission, Guid brokerID)
        {
            if (!m_isBlockedRateLockSubmission)
                return string.Empty;

            return GetFriendlyRateLockSubmissionErrorMessage(brokerID, lockPolicyID, lpeRsExpirationByPassPermission, m_rateLockSubmissionUserWarningMessage);
        }
        private static string BlockedRateLockSubmission_LenderManual(string errMsg)
        {
            if (string.IsNullOrEmpty(errMsg))
                return string.Empty;

            errMsg = errMsg.Substring(ConstAppDavid.BlockedRateLockSubmission_LenderManual.Length);
            if (errMsg.TrimWhitespaceAndBOM() == string.Empty)
            {
                // 12/23/2008 dd - Use default expire message if lender does not specify one.
                errMsg = ErrorMessages.BlockedRateLockSubmission_NormalUserExpiredMessage;
            }
            return errMsg;
        }
        private static string BlockedRateLockSubmission_OutsideNormalHourAndPassInvestorCutoff(string errMsg, string outsideNormalHoursAndPassInvestorCutOffMessage)
        {
            if (null == errMsg || errMsg == "")
                return "";

            errMsg = errMsg.Substring(ConstAppDavid.BlockedRateLockSubmission_OutsideNormalHourAndPassInvestorCutoff.Length);
            string[] parts = errMsg.Split('#');
            if (outsideNormalHoursAndPassInvestorCutOffMessage.Equals(""))
            {
                // OPM 20030 jk 1-14-08 Not using this message, but leaving this in in case it is used again in the future
                // This message instead just copies the message from OutsideNormalHour
                return string.Format(ErrorMessages.BlockedRateLockSubmission_OutsideNormalHourFormat, parts[0], parts[1]);
            }
            else
            {
                try
                {
                    return string.Format(outsideNormalHoursAndPassInvestorCutOffMessage, parts[0], parts[1]);
                }
                catch (FormatException)
                {

                    return outsideNormalHoursAndPassInvestorCutOffMessage;
                }


            }

        }
        private static string BlockedRateLockSubmission_ExpiredMessage(E_LpeRsExpirationByPassPermissionT lpeRsExpirationByPassPermission, string normalUserExpiredMessage, string lockDeskExpiredMessage)
        {
            if (lpeRsExpirationByPassPermission == E_LpeRsExpirationByPassPermissionT.NoByPass)
            {
                if (normalUserExpiredMessage.Equals(""))
                    return ErrorMessages.BlockedRateLockSubmission_NormalUserExpiredMessage;
                else
                    return normalUserExpiredMessage;
            }
            else
            {
                if (lockDeskExpiredMessage.Equals(""))
                    return ErrorMessages.BlockedRateLockSubmission_LockDeskExpiredMessage;
                else
                    return lockDeskExpiredMessage;
            }
        }

        private static string BlockedRateLockSubmission_CutOffMessage(E_LpeRsExpirationByPassPermissionT lpeRsExpirationByPassPermission, string errMsg, string normalUserCutOffMessage, string lockDeskCutOffMessage)
        {
            string cutOffTime = errMsg.Substring(ConstAppDavid.BlockedRateLockSubmission_CutOffMessage.Length);
            if (lpeRsExpirationByPassPermission == E_LpeRsExpirationByPassPermissionT.NoByPass)
            {
                if (normalUserCutOffMessage.Equals(""))
                {
                    return string.Format(ErrorMessages.BlockedRateLockSubmission_NormalUserCutOffMessageFormat, cutOffTime);
                }
                try
                {
                    return string.Format(normalUserCutOffMessage, cutOffTime);
                }
                catch (FormatException)
                {

                    return normalUserCutOffMessage;
                }
            }
            else
            {
                if (lockDeskCutOffMessage.Equals(""))
                {
                    return string.Format(ErrorMessages.BlockedRateLockSubmission_LockDeskCutOffMessageFormat, cutOffTime);
                }
                try
                {
                    return string.Format(lockDeskCutOffMessage, cutOffTime);
                }
                catch (FormatException)
                {

                    return lockDeskCutOffMessage;
                }
            }
        }
        private static string BlockedRateLockSubmission_OutsideNormalHour(E_LpeRsExpirationByPassPermissionT lpeRsExpirationByPassPermission, string errMsg, string outsideNormalHour)
        {
            string cutOffTime = errMsg.Substring(ConstAppDavid.BlockedRateLockSubmission_OutsideNormalHour.Length);
            if (outsideNormalHour.Equals(""))
            {
                return string.Format(ErrorMessages.BlockedRateLockSubmission_OutsideNormalHourFormat, cutOffTime);
            }
            else
            {
                try
                {
                    return string.Format(outsideNormalHour, cutOffTime);
                }
                catch (FormatException)
                {

                    return outsideNormalHour;
                }

            }
        }

        private static string BlockedRateLockSubmission_OutsideClosureDayHour(E_LpeRsExpirationByPassPermissionT lpeRsExpirationByPassPermission, string outsideClosureDayHour)
        {
            if (outsideClosureDayHour.Equals(""))
                return ErrorMessages.BlockedRateLockSubmission_OutsideClosureDayHourFormat;
            else
                return outsideClosureDayHour;
        }

    }
}
