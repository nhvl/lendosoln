﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Text.RegularExpressions;
using Toolbox;
using LendersOffice.Constants;
using MyAdjustTarget = LendersOfficeApp.los.RatePrice.RtAdjustTarget;
using MyCondition = LendersOfficeApp.los.RatePrice.RtCondition;
using MyConsequence = LendersOfficeApp.los.RatePrice.RtConsequence;
using MyPricePolicy = LendersOfficeApp.los.RatePrice.AbstractRtPricePolicy;
using MyRule = LendersOfficeApp.los.RatePrice.AbstractRtRule;
using MyRuleList = LendersOfficeApp.los.RatePrice.RtRuleList;
using System.Xml;
using CommonProjectLib.Common.Lib;
using System.Diagnostics;

namespace LendersOfficeApp.los.RatePrice
{
    public class PriceAdjustRecord
    {
        // For processing DTI rules per rate.
        public enum E_DtiDisqualProcessing
        {
            Normal,
            OnlyDisquals,
            IgnoreAllDisquals
        }
        public struct Args // POD
        {
            public XmlDocument xmlDocEval;
            public EvalStack evalStack;
            public CSymbolTable symbolTable;
            public CLpRunOptions runOptions;
            public bool bUseMaxYspAdjRules;
            public bool IsOptionArm;
            public bool lHasQRateInRateOptions;
            public CacheInvariantPolicyResults cachePolicyResuls;
            public bool IgnoreStips;
            public E_DtiDisqualProcessing dtiDisqualProcessing;

            public Args(XmlDocument xmlDocEval, CSymbolTable symbolTable, CLpRunOptions runOptions,
                         CApplicantPrice applicantPrice, CacheInvariantPolicyResults cachePolicyResuls, EvalStack evalStack_)
            {
                this.xmlDocEval = xmlDocEval;
                this.evalStack = evalStack_;
                this.symbolTable = symbolTable;
                this.runOptions = runOptions;
                bUseMaxYspAdjRules = applicantPrice.lLpeFeeMinAddRuleAdjBit;
                IsOptionArm = applicantPrice.IsOptionArm;
                lHasQRateInRateOptions = applicantPrice.lHasQRateInRateOptions;
                this.cachePolicyResuls = cachePolicyResuls;
                IgnoreStips = false;
                dtiDisqualProcessing = E_DtiDisqualProcessing.Normal;
            }

        }
        public class AdjustItem
        {
            decimal m_marginAdj, m_rateAdj, m_feeAdj, m_qualRAdj, m_teaserRAdj;
            string m_desc, m_debugString;
            bool m_isLenderAdj;
            public Guid RuleId; // Rule 

            public AdjustItem(decimal marginAdj, decimal rateAdj, decimal feeAdj, decimal qualRAdj, decimal teaserRAdj, string desc, string debugString, bool isLenderAdj, Guid ruleId)
            {
                m_marginAdj = marginAdj;
                m_rateAdj = rateAdj;
                m_feeAdj = feeAdj;
                m_qualRAdj = qualRAdj;
                m_teaserRAdj = teaserRAdj;
                m_desc = desc;
                m_debugString = debugString;
                m_isLenderAdj = isLenderAdj;
                RuleId = ruleId;
            }

            public CAdjustItem ToCAdjustItem(LosConvert losConvert, bool lHasQRateInRateOptions, bool isOptionArm)
            {
                if (m_marginAdj == 0 && m_rateAdj == 0 && m_feeAdj == 0)
                {
                    bool need = false;
                    if (lHasQRateInRateOptions && m_qualRAdj != 0)
                        need = true;
                    if (isOptionArm && m_teaserRAdj != 0)
                        need = true;

                    if (need == false)
                        return null;
                }

                return new CAdjustItem(losConvert.ToRateString(m_marginAdj),
                    losConvert.ToRateString(m_rateAdj),
                    losConvert.ToRateString(m_feeAdj),
                    lHasQRateInRateOptions ? losConvert.ToRateString(m_qualRAdj) : "",
                    isOptionArm ? losConvert.ToRateString(m_teaserRAdj) : "",
                    m_desc, m_debugString, m_isLenderAdj);
            }
        }


        public decimal m_marginDeltaHidden, m_marginDeltaDisclosed;
        public decimal m_rateDeltaHidden, m_rateDeltaDisclosed;
        public decimal m_feeDeltaHidden, m_feeDeltaDisclosed;
        public decimal m_teaserRDeltaHidden, m_teaserRDeltaDisclosed;
        public decimal m_qualRDeltaHidden, m_qualRDeltaDisclosed;
        public decimal m_maxDti = decimal.MaxValue;
        public decimal m_adjMaxYsp;
        public decimal m_maxFrontEndYsp = decimal.MinValue;
        public decimal m_marginBaseAllROptions = decimal.MaxValue;
        public CSortedListOfGroups m_stipCollection = new Toolbox.CSortedListOfGroups(new CStipulationCategoryComparer(), 20);
        public CSortedListOfGroups m_hiddenStipCollection = new Toolbox.CSortedListOfGroups(new CStipulationCategoryComparer(), 20);
        public CCategoryVisibilityStip m_MaxDtiStip = null; // OPM 42423
        public List<AdjustItem> m_adjDescsDisclosed = new List<AdjustItem>();
        public List<AdjustItem> m_adjDescsHidden = new List<AdjustItem>();
        public string m_qbcDisqualRule = "";
        public Dictionary<Guid, Tuple<bool, string>> DtiRuleResultCache;

        public PricingCalcException m_adjMaxYspException, m_optionArmException, m_qualRateException;


        public bool m_disqualify;
        public bool m_disallowLock = false;
        public string m_disallowLockReason = "";
        public string m_disqualifiedRules = "";
        public List<CStipulation> m_reasonArray = new List<CStipulation>();
        bool m_finishCalc;
        bool m_isInvariant;

        public PriceAdjustRecord(bool isInvariant)
        {
            m_isInvariant = isInvariant;
        }

        public bool FinishCalc
        {
            get { return m_finishCalc; }
            set { m_finishCalc = value; }
        }

        public bool Disqualify
        {
            get { return m_disqualify; }
        }

        // 04/15/09 mf - OPM 25872
        // Run a single QBC rule. There are only 3 valid consequences (MaxDTI, Disqual, Stip).
        // Pricing edit UI enforces this. All other consequences are ignored.
        void RunOneRuleQbc(MyPricePolicy pricePolicy, MyRule rule, ref PriceAdjustRecord.Args args, LoanApplicationList applications,
            StringBuilder debugInfo, bool isLogDetailInfo)
        {
            StringBuilder subDebugInfo = new StringBuilder();
            foreach (LoanApplicationList.BorrowerSpec borrower in applications.GetBorrowers(rule.QBC))
            {
                Guid policyId = pricePolicy.PricePolicyId;
                MyConsequence cons = rule.Consequence;

                Tools.Assert(FinishCalc == false, "PriceAdjustRecord : expect FinishCalc = false when calling RunOneRule()");

                MyCondition condition = rule.Condition;

                #region Evaluate Rule Condition

                try
                {
                    subDebugInfo.Clear();
                    applications.SetPricingContext(borrower);

                    bool bEvalVal = condition.Evaluate(args.xmlDocEval, args.symbolTable, args.evalStack, subDebugInfo, isLogDetailInfo);

                    if (bEvalVal == false)
                        continue;

                    if (isLogDetailInfo && null != debugInfo)
                    {
                        debugInfo.AppendLine("    *PriceAdjustRecord.RunOneRuleQbc: PolicyId:" + pricePolicy.PricePolicyId + ", RuleId:" + rule.RuleId +
                            ", Rule Desc:" + rule.Description + " Result: " + bEvalVal +
                            (bEvalVal && rule.Consequence.Disqualify ? " => Disqualify rule." : ""));
                        debugInfo.AppendLine(subDebugInfo.ToString());
                    }

                }
                catch (Exception ex)
                {
                    // We are dealing with a loan program requiring data that the loan file doesn't have

                    string regTestMsg = string.Format("Error encountered in phase 2 for rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                        rule.RuleId.ToString(), rule.Description, pricePolicy.PricePolicyId, pricePolicy.PricePolicyDescription, ex.Message);

                    throw new PricingCalcException(ex, regTestMsg, rule.RuleId, policyId);
                }

                #endregion // Evaluate Rule Condition


                #region Disqual consequence

                string[] descParts = Regex.Split(rule.Description, "::");
                bool bHiddenAdj;
                string desc;
                string debugString = "";

                if (1 < descParts.Length)
                {
                    desc = descParts[1];
                    bHiddenAdj = (CApplicantPrice.HIDE_PREFIX_STR == descParts[0].ToUpper());
                }
                else
                {
                    desc = descParts[0];
                    bHiddenAdj = false;
                }

                desc = ReplaceAlias(desc, CApplicantPrice.BORROWER_NAME_ALIAS, borrower.FullName.ToUpper());

                debugString = string.Format(" ::Policy_{1} (<a href='javascript:void(0)' onclick='return f_copyToClipboard(\"{1}\");'>copy</a>), Rule_{2} (<a href='javascript:void(0)' onclick='return f_copyToClipboard(\"{2}\");'>copy</a>)", desc, policyId.ToString(), rule.RuleId.ToString());

                MyConsequence consequence = rule.Consequence;

                // From OPM 16132 internal, we want to allow lenders to skip disqualification. It would allow us
                // selling PML as a pure pricer instead of a full pml.
                if (consequence.Disqualify)
                {
                    //if( args.runOptions.IsLpeDisqualificationEnabled  ) with opm 16132, this value is always true 
                    {
                        m_qbcDisqualRule = rule.Description + " (RuleId: " + rule.RuleId.ToString() + ", PolicyId: " + policyId.ToString() + ")";
                        if (desc.Length > 0)
                        {
                            m_reasonArray.Add(new CStipulation(desc, debugString));
                            if (m_disqualifiedRules.Length > 0)
                                m_disqualifiedRules += "<br>";
                            m_disqualifiedRules += " * " + desc;
                        }
                        m_disqualify = true;
                    }
                }

                #endregion // Disqual consequence

                #region MaxDTI Consequence

                // 8/10/2007 dd - OPM 8797 - Try to find the lowest non-zero max dti from each Policy and display in result screen.
                bool isMaxDtiSet = false;
                decimal tmpMaxDti = consequence.MaxDtiTarget.Evaluate(args.xmlDocEval, args.symbolTable, args.evalStack);
                if (tmpMaxDti != 0 && tmpMaxDti < m_maxDti)
                {
                    m_maxDti = tmpMaxDti;
                    isMaxDtiSet = true;
                    if (isLogDetailInfo && null != debugInfo)
                    {
                        debugInfo.AppendLine("        => MaxDtiTarget: " + tmpMaxDti + Environment.NewLine);
                    }
                }
                #endregion


                #region Stipulation consequence
                if (args.IgnoreStips == false)
                {
                    string stipulation = consequence.Stipulation;
                    if (stipulation.Length > 0)
                    {
                        bool bHiddenStip = false;
                        // 10/10/2007 dd - OPM 18269 - If the stip start with HIDE:: then we will hide the condition for normal user.
                        //                             We also hide stip if the stip is for hidden adjustment.
                        if (stipulation.StartsWith(CApplicantPrice.HIDE_STIP_PREFIX_STR))
                        {
                            bHiddenStip = true;
                            stipulation = stipulation.Substring(CApplicantPrice.HIDE_STIP_PREFIX_STR.Length); // Strip out special prefix.
                        }
                        else if (bHiddenAdj)
                        {
                            bHiddenStip = true; // Hide stip for hidden adjustment.
                        }
                        string[] stip = Regex.Split(stipulation, "::");
                        if (1 == stip.Length)
                            stip = new string[] { CApplicantPrice.MISC_CATEGORY_STR, stip[0] };

                        string stipDesc = ReplaceAlias(stip[1], CApplicantPrice.BORROWER_NAME_ALIAS, borrower.FullName.ToUpper());

                        string stipDebugString = string.Format(" ::Policy_{1} (<a href='javascript:void(0)' onclick='return f_copyToClipboard(\"{1}\");'>copy</a>), Rule_{2} (<a href='javascript:void(0)' onclick='return f_copyToClipboard(\"{2}\");'>copy</a>)", stipDesc, policyId.ToString(), rule.RuleId.ToString());

                        Guid applicationId = borrower.Application.Application.aAppId;

                        if (isMaxDtiSet == false)
                        {
                            if (bHiddenStip)
                            {
                                m_hiddenStipCollection.Add(stip[0], new CStipulation(stipDesc, stipDebugString, applicationId));
                            }
                            else
                            {
                                m_stipCollection.Add(stip[0], new CStipulation(stipDesc, stipDebugString, applicationId));
                            }
                        }
                        else
                        {
                            // OPM 42423.  This rule has set a maxDTI result, but it may be beaten later, so
                            // we keep track of the winning Max DTI stip, and add it after phase 2.
                            m_MaxDtiStip = new CCategoryVisibilityStip()
                            {
                                Category = stip[0],
                                Stip = new CStipulation(stipDesc, stipDebugString, applicationId),
                                IsHidden = bHiddenStip
                            };
                        }
                    }
                    else
                    {
                        if (isMaxDtiSet)
                        {
                            // Meaning that a MaxDTI value was set, but no stip consequence was set.
                            // According to case 42423, this should be rare.

                            m_MaxDtiStip = null;
                        }
                    }
                }
                #endregion // Stipulation consequence

            }
        }

        void RunOneRule(MyPricePolicy pricePolicy, MyRule rule, ref PriceAdjustRecord.Args args, LoanApplicationList applications, StringBuilder debugInfo, E_sPricingModeT sPricingModeT, bool isLogDetailInfo)
        {
            if (args.IgnoreStips && rule.Consequence.HasOnlyStipConsequence)
            {
                //PERF: No need to execute this rule.  The result will be ignored.
                return;
            }

            if (rule.IsLenderRule == true && sPricingModeT == E_sPricingModeT.InternalInvestorPricing)
            {
                // 1/15/2011 dd - OPM 30649 - When run pricing in Investor Pricing Mode we do not include rule that flag as "Is Lender Rule"
                return;
            }

            // 04/15/09 mf - OPM 25872
            if (args.runOptions.UseQbc && rule.QBC != E_sRuleQBCType.Legacy)
            {
                RunOneRuleQbc(pricePolicy, rule, ref args, applications, debugInfo, isLogDetailInfo);
                return;
            }

            if (args.runOptions.UseQbc)
                applications.SetPricingContext(applications.PrimaryBorrower.Application.AppIndex, E_aBorrowerCreditModeT.Both);

            decimal marginAdj = 0;
            decimal rateAdj = 0;
            decimal feeAdj = 0;
            decimal teaserRAdj = 0;
            decimal qualRAdj = 0;
            StringBuilder subDebugInfo = new StringBuilder();

            Guid policyId = pricePolicy.PricePolicyId;
            MyConsequence cons = rule.Consequence;

            Tools.Assert(FinishCalc == false, "PriceAdjustRecord : expect FinishCalc = false when calling RunOneRule()");

            MyCondition condition = rule.Condition;

            try
            {
                Stopwatch sw = Stopwatch.StartNew();
                bool bEvalVal = condition.Evaluate(args.xmlDocEval, args.symbolTable, args.evalStack, subDebugInfo, isLogDetailInfo);

                // Per OPM 87119, need to track hits sometimes.
                if (DtiRuleResultCache != null)
                {
                    //if ( !cons.FeeAdjustTarget.IsBlank || !cons.RateAdjustTarget.IsBlank ) 
                    DtiRuleResultCache[rule.RuleId] = Tuple.Create<bool, string>(bEvalVal, rule.Description);
                }

                if (isLogDetailInfo && null != debugInfo && (bEvalVal || !ConstSite.PricingCompactLogEnabled))
                {
                    debugInfo.AppendLine("    PriceAdjustRecord.RunOneRule: PolicyId:" + pricePolicy.PricePolicyId + ", RuleId:" + rule.RuleId + 
                        ", Rule Desc:" + rule.Description + " Result: " + bEvalVal + ". Execute in " + sw.ElapsedMilliseconds + "ms" +
                        (bEvalVal && rule.Consequence.Disqualify ? " => Disqualify rule." : "") );
                    debugInfo.AppendLine(subDebugInfo.ToString());
                }
                if (bEvalVal == false)
                    return;
            }
            catch (Exception ex)
            {
                // We are dealing with a loan program requiring data that the loan file doesn't have

                string regTestMsg = string.Format("Error encountered in phase 2 for rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                    rule.RuleId.ToString(), rule.Description, pricePolicy.PricePolicyId, pricePolicy.PricePolicyDescription, ex.Message);

                throw new PricingCalcException(ex, regTestMsg, rule.RuleId, policyId);
            }

            string[] descParts = Regex.Split(rule.Description, "::");
            bool bHiddenAdj;
            string desc;
            string debugString = "";
            bool bLockBlock = false;

            if (1 < descParts.Length)
            {
                desc = descParts[1];
                bHiddenAdj = (CApplicantPrice.HIDE_PREFIX_STR == descParts[0].ToUpper());
                
                if (descParts[0].ToUpper() == CApplicantPrice.DISABLE_LOCK_PREFIX_STR && rule.Consequence.Disqualify)
                {
                    bLockBlock = true;
                    m_disallowLock = true;
                    m_disallowLockReason = desc;
                }
            }
            else
            {
                desc = descParts[0];
                bHiddenAdj = false;
            }

            desc = ReplaceAlias(desc, CApplicantPrice.BORROWER_NAME_ALIAS, CApplicantPrice.LEGACY_BORR_NAME_REPLACEMENT);

            debugString = string.Format(" ::Policy_{1} (<a href='javascript:void(0)' onclick='return f_copyToClipboard(\"{1}\");'>copy</a>), Rule_{2} (<a href='javascript:void(0)' onclick='return f_copyToClipboard(\"{2}\");'>copy</a>)", desc, policyId.ToString(), rule.RuleId.ToString());

            MyConsequence consequence = rule.Consequence;


            // From OPM 16132 internal, we want to allow lenders to skip disqualification. It would allow us
            // selling PML as a pure pricer instead of a full pml.
            if (consequence.Disqualify && !bLockBlock)
            {
                //if( args.runOptions.IsLpeDisqualificationEnabled  ) with opm 16132, this value is always true 
                {
                    if (desc.Length > 0)
                    {
                        m_reasonArray.Add(new CStipulation(desc, debugString));
                        if (m_disqualifiedRules.Length > 0)
                            m_disqualifiedRules += "<br>";
                        m_disqualifiedRules += " * " + desc;
                    }
                    m_disqualify = true;
                }
            }

            subDebugInfo.Clear();

            // 8/10/2007 dd - OPM 8797 - Try to find the lowest non-zero max dti from each Policy and display in result screen.
            bool isMaxDtiSet = false;
            decimal tmpMaxDti = consequence.MaxDtiTarget.Evaluate(args.xmlDocEval, args.symbolTable, args.evalStack);
            if (tmpMaxDti != 0 && tmpMaxDti < m_maxDti)
            {
                m_maxDti = tmpMaxDti;
                isMaxDtiSet = true;
                subDebugInfo.AppendFormat(" maxDti={0}", tmpMaxDti);
            }

            // TODO: 11/24/04, Perf tuning opportunity, if we are sure that we don't need to have equations with keywords in the 
            // consequence section, might as well cut this evaluation step.

            #region COLLECT MARGIN ADJUSTMENT
            try
            {
                marginAdj = consequence.MarginAdjustTarget.Evaluate(args.xmlDocEval, args.symbolTable, args.evalStack);
                if (bHiddenAdj)
                    m_marginDeltaHidden += marginAdj;
                else
                    m_marginDeltaDisclosed += marginAdj;

                if ( marginAdj != 0)
                {
                    subDebugInfo.AppendFormat(" marginAdj={0}", marginAdj);
                    if(m_marginDeltaHidden + m_marginDeltaDisclosed != marginAdj)
                    {
                        subDebugInfo.AppendFormat(" (marginDelta={0})", m_marginDeltaHidden + m_marginDeltaDisclosed);
                    }
                }
            }
            catch (Exception ex)
            {
                string regTestMsg = string.Format("Error encountered in phase 2 for rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                    rule.RuleId.ToString(), rule.Description, pricePolicy.PricePolicyId, pricePolicy.PricePolicyDescription, ex.Message);

                throw new PricingCalcException(ex, regTestMsg, rule.RuleId, policyId);

            }
            #endregion // COLLECT MARGIN ADJUSTMENT

            #region COLLECT FEE ADJUSTMENT
            try
            {
                feeAdj = consequence.FeeAdjustTarget.Evaluate(args.xmlDocEval, args.symbolTable, args.evalStack);
                if (bHiddenAdj)
                    m_feeDeltaHidden += feeAdj;
                else
                    m_feeDeltaDisclosed += feeAdj;

                if (feeAdj != 0)
                {
                    subDebugInfo.AppendFormat(" feeAdj={0}", feeAdj);
                    if (m_feeDeltaHidden + m_feeDeltaDisclosed != feeAdj)
                    {
                        subDebugInfo.AppendFormat(" (feeDelta={0})", m_feeDeltaHidden + m_feeDeltaDisclosed);
                    }
                }
            }
            catch (Exception ex)
            {
                string regTestMsg = string.Format("Error encountered in phase 2 for rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                    rule.RuleId.ToString(), rule.Description, pricePolicy.PricePolicyId, pricePolicy.PricePolicyDescription, ex.Message);

                throw new PricingCalcException(ex, regTestMsg, rule.RuleId, policyId);
            }
            #endregion // COLLECT FEE ADJUSTMENT

            #region COLLECT NOTE RATE ADJUSTMENT
            try
            {
                rateAdj = consequence.RateAdjustTarget.Evaluate(args.xmlDocEval, args.symbolTable, args.evalStack);
                if (bHiddenAdj)
                    m_rateDeltaHidden += rateAdj;
                else
                    m_rateDeltaDisclosed += rateAdj;

                if (rateAdj != 0)
                {
                    subDebugInfo.AppendFormat(" rateAdj={0}", rateAdj);
                    if (m_rateDeltaHidden + m_rateDeltaDisclosed != rateAdj)
                    {
                        subDebugInfo.AppendFormat(" (rateDelta={0})", m_rateDeltaHidden + m_rateDeltaDisclosed);
                    }
                }
            }
            catch (Exception ex)
            {
                string regTestMsg = string.Format("Error encountered in phase 2 for rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                    rule.RuleId.ToString(), rule.Description, pricePolicy.PricePolicyId, pricePolicy.PricePolicyDescription, ex.Message);

                throw new PricingCalcException(ex, regTestMsg, rule.RuleId, policyId);
            }
            #endregion // COLLECT NOTE RATE ADJUSTMENT

            #region COLLECT TEASER RATE ADJUSTMENT
            try
            {
                if (m_isInvariant || args.IsOptionArm)
                {
                    teaserRAdj = consequence.TeaserRateAdjustTarget.Evaluate(args.xmlDocEval, args.symbolTable, args.evalStack);
                    if (bHiddenAdj)
                        m_teaserRDeltaHidden += teaserRAdj;
                    else
                        m_teaserRDeltaDisclosed += teaserRAdj;

                    if (teaserRAdj != 0)
                    {
                        subDebugInfo.AppendFormat(" teaserRateAdj={0}", teaserRAdj);
                        if (m_teaserRDeltaHidden + m_teaserRDeltaDisclosed != teaserRAdj)
                        {
                            subDebugInfo.AppendFormat(" (teaserRateDelta={0})", m_teaserRDeltaHidden + m_teaserRDeltaDisclosed);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string regTestMsg = string.Format("Error encountered in phase 2 teaser rate adjustment collection for rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                    rule.RuleId.ToString(), rule.Description, pricePolicy.PricePolicyId, pricePolicy.PricePolicyDescription, ex.Message);

                PricingCalcException optionArmException = new PricingCalcException(ex, regTestMsg, rule.RuleId, policyId);
                if (args.IsOptionArm)
                    throw optionArmException;
                else if (m_optionArmException == null)
                    m_optionArmException = optionArmException;

            }
            #endregion // COLLECT TEASER RATE ADJUSTMENT


            #region COLLECT QUAL RATE ADJUSTMENT
            try
            {
                if (m_isInvariant || args.lHasQRateInRateOptions)
                {
                    qualRAdj = consequence.QrateAdjustTarget.Evaluate(args.xmlDocEval, args.symbolTable, args.evalStack);
                    if (bHiddenAdj)
                        m_qualRDeltaHidden += qualRAdj;
                    else
                        m_qualRDeltaDisclosed += qualRAdj;

                    if (qualRAdj != 0)
                    {
                        subDebugInfo.AppendFormat(" QrateAdj={0}", qualRAdj);
                        if (m_qualRDeltaHidden + m_qualRDeltaDisclosed != qualRAdj)
                        {
                            subDebugInfo.AppendFormat(" (QRateDelta={0})", m_qualRDeltaHidden + m_qualRDeltaDisclosed);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string regTestMsg = string.Format("Error encountered in phase 2 qual rate adjustment collection for rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                                                    rule.RuleId.ToString(), rule.Description, pricePolicy.PricePolicyId, pricePolicy.PricePolicyDescription, ex.Message);

                PricingCalcException qualRateException = new PricingCalcException(ex, regTestMsg, rule.RuleId, policyId);
                if (args.lHasQRateInRateOptions)
                    throw qualRateException;

                if (m_qualRateException == null && args.lHasQRateInRateOptions)
                    m_qualRateException = qualRateException;
            }
            #endregion // COLLECT QUAL RATE ADJUSTMENT


            #region SET MAX YSP CONSEQUENCE
            if (m_isInvariant || args.bUseMaxYspAdjRules)
            {
                try
                {
                    decimal ysp = consequence.MaxYspAdjustTarget.Evaluate(args.xmlDocEval, args.symbolTable, args.evalStack);
                    if (ysp != 0)
                    {
                        m_adjMaxYsp += ysp;
                        subDebugInfo.AppendFormat(" maxYspAdj={0}", ysp);
                        if (m_adjMaxYsp != ysp)
                        {
                            subDebugInfo.AppendFormat(" (total adjMaxYsp={0})", m_adjMaxYsp);
                        }
                    }
                }
                catch (Exception ex)
                {
                    string regTestMsg = string.Format("Error encountered in phase 2 for rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                        rule.RuleId.ToString(), rule.Description, pricePolicy.PricePolicyId, pricePolicy.PricePolicyDescription, ex.Message);

                    PricingCalcException adjMaxYspExecption = new PricingCalcException(ex, regTestMsg, rule.RuleId, policyId);
                    if (args.bUseMaxYspAdjRules)
                        throw adjMaxYspExecption;
                    else if (m_adjMaxYspException == null)
                        m_adjMaxYspException = adjMaxYspExecption;

                } // catch
            } // if ( bUseMaxYspAdjRules )
            #endregion // MAX YSP CONSEQUENCE

            #region SET FRONT END MAX YSP CONSEQUENCE
            try
            {
                if (consequence.MaxFrontEndYspAdjustTarget.IsBlank == false)
                {
                    decimal tmpMaxFrontEndYsp = consequence.MaxFrontEndYspAdjustTarget.Evaluate(args.xmlDocEval, args.symbolTable, args.evalStack);
                    if (tmpMaxFrontEndYsp > m_maxFrontEndYsp)
                    {
                        m_maxFrontEndYsp = tmpMaxFrontEndYsp;
                        subDebugInfo.AppendFormat(" maxFrontEndYsp={0}", tmpMaxFrontEndYsp);
                    }
                }
            }
            catch (Exception ex)
            {
                string regTestMsg = string.Format("Error encountered in phase 2 for rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                    rule.RuleId.ToString(), rule.Description, pricePolicy.PricePolicyId, pricePolicy.PricePolicyDescription, ex.Message);

                throw new PricingCalcException(ex, regTestMsg, rule.RuleId, policyId);
            }
            #endregion // MAX FRONT END YSP CONSEQUENCE



            #region SET MARGIN BASE ALL RATE OPTIONS
            try
            {
                // Note: honor the value set in each rate option if no rule set for baseMargin
                if (!consequence.MarginBaseTarget.IsBlank)
                {
                    //m_isMarginBaseAllRateOptions = true;
                    m_marginBaseAllROptions = consequence.MarginBaseTarget.Evaluate(args.xmlDocEval, args.symbolTable, args.evalStack);
                    subDebugInfo.AppendFormat(" marginBase={0}", m_marginBaseAllROptions);
                }
            }
            catch (Exception ex)
            {
                string regTestMsg = string.Format("Error encountered in phase 2 for rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                    rule.RuleId.ToString(), rule.Description, pricePolicy.PricePolicyId, pricePolicy.PricePolicyDescription, ex.Message);

                throw new PricingCalcException(ex, regTestMsg, rule.RuleId, policyId);
            }
            #endregion // SET MARGINE BASE ALL RATE OPTIONS

            #region COLLECT STIPULATION

            string stipulation = consequence.Stipulation;
            if (stipulation.Length > 0)
            {
                bool bHiddenStip = false;
                // 10/10/2007 dd - OPM 18269 - If the stip start with HIDE:: then we will hide the condition for normal user.
                //                             We also hide stip if the stip is for hidden adjustment.
                if (stipulation.StartsWith(CApplicantPrice.HIDE_STIP_PREFIX_STR))
                {
                    bHiddenStip = true;
                    stipulation = stipulation.Substring(CApplicantPrice.HIDE_STIP_PREFIX_STR.Length); // Strip out special prefix.
                }
                else if (bHiddenAdj)
                {
                    bHiddenStip = true; // Hide stip for hidden adjustment.
                }
                string[] stip = Regex.Split(stipulation, "::");
                if (1 == stip.Length)
                    stip = new string[] { CApplicantPrice.MISC_CATEGORY_STR, stip[0] };
                string stipDesc = stip[1];

                stipDesc = ReplaceAlias(stipDesc, CApplicantPrice.BORROWER_NAME_ALIAS, CApplicantPrice.LEGACY_BORR_NAME_REPLACEMENT);

                string stipDebugString = string.Format(" ::Policy_{1} (<a href='javascript:void(0)' onclick='return f_copyToClipboard(\"{1}\");'>copy</a>), Rule_{2} (<a href='javascript:void(0)' onclick='return f_copyToClipboard(\"{2}\");'>copy</a>)", stipDesc, policyId.ToString(), rule.RuleId.ToString());
                if (ConstSite.DisplayPolicyAndRuleIdOnLpeResult && null != debugInfo)
                {
                    debugInfo.AppendLine("             Stip: IsHidden:" + bHiddenStip + " - [" + stipDesc + "]");
                }

                Guid applicationId = applications.PrimaryBorrower.Application.Application.aAppId;
                if (isMaxDtiSet == false)
                {
                    if (bHiddenStip)
                    {
                        m_hiddenStipCollection.Add(stip[0], new CStipulation(stipDesc, stipDebugString, applicationId));
                    }
                    else
                    {
                        m_stipCollection.Add(stip[0], new CStipulation(stipDesc, stipDebugString, applicationId));
                    }
                }
                else
                {
                    // OPM 42423.  This rule has set a maxDTI result, but it may be beaten later, so
                    // we keep track of the winning Max DTI stip, and add it after phase 2.
                    m_MaxDtiStip = new CCategoryVisibilityStip()
                    {
                        Category = stip[0],
                        Stip = new CStipulation(stipDesc, stipDebugString, applicationId),
                        IsHidden = bHiddenStip
                    };
                }
            }
            else
            {
                if (isMaxDtiSet)
                {
                    // Meaning that a MaxDTI value was set, but no stip consequence was set.
                    // According to case 42423, this should be rare.

                    m_MaxDtiStip = null;
                }
            }

            #endregion // COLLECT STIPULATION


            if (marginAdj != 0 || rateAdj != 0 || feeAdj != 0 || qualRAdj != 0 || teaserRAdj != 0)
            {
                AdjustItem adjDesc = new AdjustItem(marginAdj, rateAdj, feeAdj, qualRAdj, teaserRAdj, desc, debugString, rule.IsLenderRule, rule.RuleId);

                //The else-if check above must be done because we only want to publish the adjustments if there is positive adjustments
                // If none of them is positive, we don't know if they are rule for different targets or not. Sometimes, we encounter 
                // rules w/o target because the rule template might adjust 0 at the moment and the consideration is not significant 
                // enough to be displayed. Considerations like interest-only or not and ppp are significant and Lpe Authors will add notes stipulation
                // to the same rule to make sure users see it. The significant ones are the fields mean to be request only - no guarantee from us.

                if (bHiddenAdj)
                {
                    // 4/23/2007 dd - OPM 12567 - If there are no adjustment then do not display description in hidden adjustment.
                    m_adjDescsHidden.Add(adjDesc);
                }
                else
                {
                    m_adjDescsDisclosed.Add(adjDesc);
                }
            }
            
            if( isLogDetailInfo && null != debugInfo && (subDebugInfo.Length > 0 || string.IsNullOrEmpty(consequence.Stipulation) == false) )
            {
                if (subDebugInfo.Length > 0)
                {
                    debugInfo.AppendLine(  "        => Adjust values: " + subDebugInfo.ToString());
                }

                if (string.IsNullOrEmpty(consequence.Stipulation) == false)
                {
                    debugInfo.AppendFormat("        {0} Stipulation: {1}", (subDebugInfo.Length > 0) ? "  " : "=>", consequence.Stipulation);
                    debugInfo.AppendLine();
                }
            }
        }

        void AddResult(PriceAdjustRecord invariantRecord, ref PriceAdjustRecord.Args args, Guid policyId = new Guid(), StringBuilder debugInfo = null)
        {
            AddResult(invariantRecord, ref args,  /* AllowException =  */  true, policyId, debugInfo);
        }

        void AddResultWithNoException(PriceAdjustRecord invariantRecord, ref PriceAdjustRecord.Args args, Guid policyId = new Guid(), StringBuilder debugInfo = null)
        {
            AddResult(invariantRecord, ref args,  /* AllowException =  */  false, policyId, debugInfo);
        }

        void AddResult(PriceAdjustRecord invariantRecord, ref PriceAdjustRecord.Args args, bool allowException, Guid policyId = new Guid(), StringBuilder debugInfo = null)
        {
            Tools.Assert(FinishCalc == false, "AddResult : expect FinishCalc == false");

            StringBuilder subDebugInfo = new StringBuilder();
            PricingCalcException ex = null;

            try
            {
                if (invariantRecord.m_maxDti < m_maxDti)
                {
                    m_maxDti = invariantRecord.m_maxDti;
                    m_MaxDtiStip = invariantRecord.m_MaxDtiStip;
                    subDebugInfo.AppendFormat(" maxDti={0}", invariantRecord.m_maxDti);
                }

                if (invariantRecord.m_maxFrontEndYsp > m_maxFrontEndYsp)
                {
                    m_maxFrontEndYsp = invariantRecord.m_maxFrontEndYsp;
                    subDebugInfo.AppendFormat(" maxFrontEndYsp={0}", invariantRecord.m_maxFrontEndYsp);
                }

                m_marginDeltaHidden += invariantRecord.m_marginDeltaHidden;
                m_marginDeltaDisclosed += invariantRecord.m_marginDeltaDisclosed;

                decimal marginAdj = invariantRecord.m_marginDeltaHidden + invariantRecord.m_marginDeltaDisclosed;
                if (marginAdj != 0)
                {
                    subDebugInfo.AppendFormat(" marginAdj={0}", marginAdj);
                    if (m_marginDeltaHidden + m_marginDeltaDisclosed != marginAdj)
                    {
                        subDebugInfo.AppendFormat(" (marginDelta={0})", m_marginDeltaHidden + m_marginDeltaDisclosed);
                    }
                }

                m_feeDeltaHidden += invariantRecord.m_feeDeltaHidden;
                m_feeDeltaDisclosed += invariantRecord.m_feeDeltaDisclosed;

                decimal feeAdj = invariantRecord.m_feeDeltaHidden + invariantRecord.m_feeDeltaDisclosed;
                if (feeAdj != 0)
                {
                    subDebugInfo.AppendFormat(" feeAdj={0}", feeAdj);
                    if (m_feeDeltaHidden + m_feeDeltaDisclosed != feeAdj)
                    {
                        subDebugInfo.AppendFormat(" (feeDelta={0})", m_feeDeltaHidden + m_feeDeltaDisclosed);
                    }
                }

                m_rateDeltaHidden += invariantRecord.m_rateDeltaHidden;
                m_rateDeltaDisclosed += invariantRecord.m_rateDeltaDisclosed;

                decimal rateAdj = invariantRecord.m_rateDeltaHidden + invariantRecord.m_rateDeltaDisclosed;
                if (rateAdj != 0)
                {
                    subDebugInfo.AppendFormat(" rateAdj={0}", rateAdj);
                    if (m_rateDeltaHidden + m_rateDeltaDisclosed != rateAdj)
                    {
                        subDebugInfo.AppendFormat(" (rateDelta={0})", m_rateDeltaHidden + m_rateDeltaDisclosed);
                    }
                }

                if (args.IsOptionArm)
                {
                    m_teaserRDeltaHidden += invariantRecord.m_teaserRDeltaHidden;
                    m_teaserRDeltaDisclosed += invariantRecord.m_teaserRDeltaDisclosed;

                    decimal teaserRateAdj = invariantRecord.m_teaserRDeltaHidden + invariantRecord.m_teaserRDeltaDisclosed;
                    if (teaserRateAdj != 0)
                    {
                        subDebugInfo.AppendFormat(" teaserRateAdj={0}", teaserRateAdj);
                        if (m_teaserRDeltaHidden + m_teaserRDeltaDisclosed != teaserRateAdj)
                        {
                            subDebugInfo.AppendFormat(" (teaserRateDelta={0})", m_teaserRDeltaHidden + m_teaserRDeltaDisclosed);
                        }
                    }

                    if (m_optionArmException != null && allowException)
                    {
                        ex = m_optionArmException;
                        throw m_optionArmException;
                    }

                }

                if (args.lHasQRateInRateOptions)
                {
                    m_qualRDeltaHidden += invariantRecord.m_qualRDeltaHidden;
                    m_qualRDeltaDisclosed += invariantRecord.m_qualRDeltaDisclosed;
                    if (m_qualRateException != null && allowException)
                    {
                        ex = m_qualRateException;
                        throw m_qualRateException;
                    }

                    decimal qrateAdj = invariantRecord.m_qualRDeltaHidden + invariantRecord.m_qualRDeltaDisclosed;
                    if (qrateAdj != 0)
                    {
                        subDebugInfo.AppendFormat(" QrateAdj={0}", qrateAdj);
                        if (m_qualRDeltaHidden + m_qualRDeltaDisclosed != qrateAdj)
                        {
                            subDebugInfo.AppendFormat(" (QRateDelta={0})", m_qualRDeltaHidden + m_qualRDeltaDisclosed);
                        }
                    }
                }

                if (args.bUseMaxYspAdjRules)
                {
                    m_adjMaxYsp += invariantRecord.m_adjMaxYsp;

                    if (invariantRecord.m_adjMaxYsp != 0)
                    {
                        subDebugInfo.AppendFormat(" maxYspAdj={0}", invariantRecord.m_adjMaxYsp);
                        if (m_adjMaxYsp != invariantRecord.m_adjMaxYsp)
                        {
                            subDebugInfo.AppendFormat(" (total adjMaxYsp={0})", m_adjMaxYsp);
                        }
                    }

                    if (m_adjMaxYspException != null && allowException)
                    {
                        ex = m_adjMaxYspException;
                        throw m_adjMaxYspException;
                    }

                }


                if (invariantRecord.m_marginBaseAllROptions != decimal.MaxValue)
                {
                    m_marginBaseAllROptions = invariantRecord.m_marginBaseAllROptions;
                    subDebugInfo.AppendFormat(" marginBase={0}", m_marginBaseAllROptions);
                }

                if (invariantRecord.m_disqualify)
                {
                    m_disqualify = true;

                    if (m_disqualifiedRules.Length > 0)
                        m_disqualifiedRules += "<br>";
                    m_disqualifiedRules += " * " + invariantRecord.m_disqualifiedRules;
                    if (invariantRecord.m_qbcDisqualRule.Length != 0)
                        m_qbcDisqualRule = invariantRecord.m_qbcDisqualRule;
                }

                // opm 462227 - Allow Disallow Locks to survive
                if (invariantRecord.m_disallowLock)
                {
                    m_disallowLock = invariantRecord.m_disallowLock;
                    m_disallowLockReason = invariantRecord.m_disallowLockReason;
                }                

                m_reasonArray.AddRange(invariantRecord.m_reasonArray);
                m_stipCollection.Append(invariantRecord.m_stipCollection);
                m_hiddenStipCollection.Append(invariantRecord.m_hiddenStipCollection);
                m_adjDescsHidden.AddRange(invariantRecord.m_adjDescsHidden);
                m_adjDescsDisclosed.AddRange(invariantRecord.m_adjDescsDisclosed);

                // exception stuff
            }
            finally
            {
                if (debugInfo != null && (subDebugInfo.Length > 0 || invariantRecord.m_disqualify || ex != null))
                {
                    debugInfo.AppendFormat("    Add result from PolicyId:{0} => ", policyId);
                    if (subDebugInfo.Length > 0)
                    {
                        debugInfo.Append(subDebugInfo);
                    }

                    if (invariantRecord.m_disqualify)
                    {
                        debugInfo.Append(" *** disqualify");
                    }
                    debugInfo.AppendLine().AppendLine();

                    if( ex != null)
                    {
                        debugInfo.Append("    Exception: " + ex.ToString()).AppendLine();
                    }
                }

            }
        }

        static void RunInvariantOrQValueRule(MyPricePolicy pricePolicy, ref PriceAdjustRecord.Args args,
                                       PriceAdjustRecord invariantRecord, int idxFirst, int lastIdx, LoanApplicationList applications, StringBuilder debugInfo, E_sPricingModeT sPricingModeT, bool isLogDetailInfo, bool isGetAllReasons)
        {
            if (invariantRecord.FinishCalc == false)
            {
                MyRuleList rules = pricePolicy.Rules;
                for (int i = idxFirst; i < lastIdx; ++i)
                {
                    MyRule rule = rules.GetRule(i);
                    MyConsequence cons = rule.Consequence;
                    invariantRecord.RunOneRule(pricePolicy, rule, ref args, applications, debugInfo, sPricingModeT, isLogDetailInfo);
                    if (invariantRecord.m_disqualify && isGetAllReasons == false)
                        break;
                }
                invariantRecord.FinishCalc = true;
            }
        }



        public static void Compute(MyPricePolicy pricePolicy, ref PriceAdjustRecord.Args args,
                                    PriceAdjustRecord finalResult, bool dtiOnly, LoanApplicationList applications, StringBuilder debugInfo, E_sPricingModeT sPricingModeT,
            bool isLogDetailInfo, bool isGetAllReasons)
        {
            object policyId = pricePolicy.PricePolicyId;


            pricePolicy.LoadRules();
            MyRuleList rules = pricePolicy.Rules;

            bool hasCacheResult;
            if (dtiOnly == false)
            {
                // step 1 : run invariant-rules and put results in cached record
                if (pricePolicy.FirstIdxOfInvariant < pricePolicy.LastIdxOfInvariant)
                {

                    PriceAdjustRecord invariantRecord = (PriceAdjustRecord)args.cachePolicyResuls.priceAdjustRecords[policyId];
                    if (invariantRecord == null)
                    {
                        invariantRecord = new PriceAdjustRecord(true /* invariant */ );
                        hasCacheResult = false;
                    }
                    else
                        hasCacheResult = true;

                    try
                    {
                        RunInvariantOrQValueRule(pricePolicy, ref args, invariantRecord, pricePolicy.FirstIdxOfInvariant, pricePolicy.LastIdxOfInvariant, applications, debugInfo, sPricingModeT, isLogDetailInfo, isGetAllReasons);

                        // add cache result into finalResult
                        finalResult.AddResult(invariantRecord, ref args, pricePolicy.PricePolicyId, isLogDetailInfo ? debugInfo : null);

                        if (hasCacheResult == false)
                            args.cachePolicyResuls.priceAdjustRecords[policyId] = invariantRecord;

                    }
                    catch
                    {
                        finalResult.AddResultWithNoException(invariantRecord, ref args, pricePolicy.PricePolicyId, isLogDetailInfo ? debugInfo : null);
                        throw;
                    }
                }


                // step 2 : run qvalue-rules and put results in cached record
                if (pricePolicy.FirstIdxOfQValue < pricePolicy.LastIdxOfQValue)
                {

                    PriceAdjustRecord adjustRecord = (PriceAdjustRecord)args.cachePolicyResuls.priceAdjustRecords4ConstQValue[policyId];
                    if (adjustRecord == null)
                    {
                        adjustRecord = new PriceAdjustRecord(true /* invariant */ );
                        hasCacheResult = false;
                    }
                    else
                        hasCacheResult = true;

                    try
                    {
                        RunInvariantOrQValueRule(pricePolicy, ref args, adjustRecord, pricePolicy.FirstIdxOfQValue, pricePolicy.LastIdxOfQValue, applications, debugInfo, sPricingModeT, isLogDetailInfo, isGetAllReasons);

                        // add cache result into finalResult
                        finalResult.AddResult(adjustRecord, ref args, pricePolicy.PricePolicyId, isLogDetailInfo ? debugInfo : null);

                        if (hasCacheResult == false)
                            args.cachePolicyResuls.priceAdjustRecords4ConstQValue[policyId] = adjustRecord;

                    }
                    catch
                    {
                        finalResult.AddResultWithNoException(adjustRecord, ref args, pricePolicy.PricePolicyId, isLogDetailInfo ? debugInfo : null);
                        throw;
                    }
                }

                //step 3 : run  variant rules
                for (int i = pricePolicy.FirstIdxOfVariant; i < pricePolicy.LastIdxOfVariant; i++)
                {
                    if (finalResult.m_disqualify && isGetAllReasons == false)
                        break;

                    MyRule rule = rules.GetRule(i);
                    finalResult.RunOneRule(pricePolicy, rule, ref args, applications, debugInfo, sPricingModeT, isLogDetailInfo);
                }
                return;
            }

            // at here : dtiOnly == true
            for (int i = pricePolicy.FirstIdxOfDti; i < pricePolicy.LastIdxOfDti; i++)
            {

                if ((finalResult.m_disqualify && isGetAllReasons == false) && finalResult.DtiRuleResultCache == null)
                    break;

                MyRule rule = rules.GetRule(i);
                Tools.Assert(rule.DtiDepend == true, "expect rule.DtiDepend == true");

                // Per rate option makes distinction so it can do disquals at the end.
                if (rule.Consequence.Disqualify)
                {
                    switch (args.dtiDisqualProcessing)
                    {
                        case E_DtiDisqualProcessing.Normal:
                        case E_DtiDisqualProcessing.OnlyDisquals:
                            break;
                        case E_DtiDisqualProcessing.IgnoreAllDisquals: continue;
                        default:
                            throw new UnhandledEnumException(args.dtiDisqualProcessing);
                    }
                }
                else
                {
                    if (args.dtiDisqualProcessing == E_DtiDisqualProcessing.OnlyDisquals) 
                        continue;
                }

                finalResult.RunOneRule(pricePolicy, rule, ref args, applications, debugInfo, sPricingModeT, isLogDetailInfo);
            }
        }

        private static string ReplaceAlias(string originalText, string alias, string replacement)
        {
            // 05/19/09 mf - OPM 30756. Need to make alias replacement case-insensitive.  String.Replace() does not support ignoring the case.
            // PERFORMANCE: If we need to remove the regex overhead, we can do this without it.

            return Regex.Replace(originalText, Regex.Escape(alias), replacement, RegexOptions.IgnoreCase);
        }


    }

}
