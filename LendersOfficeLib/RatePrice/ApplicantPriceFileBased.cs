﻿namespace LendersOfficeApp.los.RatePrice
{
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.RatePrice;

    public class ApplicantPriceFileBased : CApplicantPrice
    {
        public static CApplicantPrice Create(CPageData loanFileData, ILoanProgramTemplate loanProgramTemlate,
                        AbstractLoanProgramSet loanProgramSet,
            CLpRunOptions options,
            CacheInvariantPolicyResults cachResult)
        {
            return new ApplicantPriceFileBased(loanFileData, loanProgramTemlate, loanProgramSet, options,
                cachResult);
        }


        private ApplicantPriceFileBased(CPageData loanFileData, ILoanProgramTemplate loanProgramTemplate,
            AbstractLoanProgramSet loanProgramSet,
            CLpRunOptions options,
            CacheInvariantPolicyResults cachResult)
            : base(loanFileData, loanProgramTemplate, loanProgramSet, options)
        {
            loanFileData.ByPassFieldSecurityCheck = true;

            if (!ConstStage.EnableLegacyQualRateCalculation)
            {
                this.SetupCurrentLoanProgramTemplate(loanProgramSet);
            }

            ComputePrices(cachResult);
        }

        private void SetupCurrentLoanProgramTemplate(AbstractLoanProgramSet loanProgramSet)
        {
            // Certain pricing fields may need to use the current program's values even
            // when running historical pricing, such as the index rate.
            if (loanProgramSet is HybridLoanProgramSet)
            {
                this.CurrentProgramForHistoricalPricing = (loanProgramSet as HybridLoanProgramSet).RetrieveByKey(this.lLpTemplateId, forceCurrent: true);
            }
            else
            {
                this.CurrentProgramForHistoricalPricing = loanProgramSet.RetrieveByKey(this.lLpTemplateId);
            }
        }
    }
}
