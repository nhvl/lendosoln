// 2006/06/22 : CConditionTest class is a unit test for CCondition

using System;
using System.Collections;
using System.Text.RegularExpressions;
using System.Xml;
using DataAccess;
using LendersOffice.Common;


//ThienChange   
//  Purpose   : CRule only depends on SymbolTable
//  Will do   : rename "CPageBase loanData" to "IGetSymbolTable infGetSymbolTable"
//  But       : currently use the following hack 

namespace LendersOfficeApp.los.RatePrice
{
    public class CConditionException : CBaseException
    {
        public CEvaluationException.ErrorTypeEnum ErrorType;
        public CConditionException(CEvaluationException.ErrorTypeEnum errType, string msg) 
            : base(ErrorMessages.Generic, msg) 
        { 
            ErrorType = errType;
        }
    }

	public class CRule
	{
		private XmlElement m_ruleElement;
		private CConsequence m_consequence;
		private CCondition m_condition;
		private Guid			m_id;
		private XmlDocument		m_xmlDoc;

		public CRule( XmlElement ruleElement )
		{ 
			m_xmlDoc      = ruleElement.OwnerDocument;
			m_ruleElement = ruleElement;

            XmlElement conditionElement   = (XmlElement) m_ruleElement.SelectSingleNode( ".//Condition" );
            XmlElement consequenceElement = (XmlElement) m_ruleElement.SelectSingleNode( ".//Consequence" );

            m_condition   = new CCondition( conditionElement );
			m_consequence = new CConsequence( consequenceElement );

			m_id = new Guid( m_ruleElement.Attributes["RuleId"].InnerText );
		}
		
		public CRule(XmlDocument xmlDoc, Guid ruleID)
		{
			InitRule(xmlDoc, ruleID) ;
		}
		/// <summary>
		///  Called by Constructors
		/// </summary>
		private void InitRule(XmlDocument xmlDoc, Guid ruleID)
		{
			m_id = ruleID ;

			m_xmlDoc = xmlDoc;

			m_ruleElement = m_xmlDoc.CreateElement( "Rule" );

			XmlAttribute xmlAttr =  m_xmlDoc.CreateAttribute( "RuleId" );
			xmlAttr.InnerText = m_id.ToString();
			m_ruleElement.Attributes.Append( xmlAttr );
			xmlAttr =  m_xmlDoc.CreateAttribute( "Description" );
			xmlAttr.InnerText = "";
			m_ruleElement.Attributes.Append( xmlAttr );

			m_condition = CCondition.CreateEmptyCondition( m_xmlDoc );
			m_consequence = CConsequence.CreateEmptyConsequence( m_xmlDoc );

			m_ruleElement.AppendChild( m_condition.XmlNode );
			m_ruleElement.AppendChild( m_consequence.XmlNode );
		}
		/// <summary>
		/// This should be called when need to save the xml text for the whole rule.
		/// </summary>
		/// <returns></returns>
		public XmlNode XmlNode
		{
			get { return m_ruleElement; }
		}

		public CCondition	Condition
		{
			get { return m_condition; }
		}

        public CConsequence Consequence
        {
            get { return m_consequence; }
        }

		public string Description
		{
			get 
			{ 
				XmlAttribute descAttr = m_ruleElement.Attributes[ "Description" ];
				return descAttr.Value;
			}
			set 
			{ 
				XmlAttribute descAttr = m_ruleElement.Attributes[ "Description" ];
				descAttr.Value = value;
			}
		}
        //opm 25872 fs 03/06/09
        public E_sRuleQBCType QBC
        {
            get
            {
                //Only allowed values for QBC are: P (primary), A (all), C (coborrowers), "Blank" (legacy)
                XmlAttribute descAttr = m_ruleElement.Attributes["QBC"];
                if (descAttr != null && descAttr.Value != null)
                {
                    switch(descAttr.Value)
                    {
                        case "P":
                            return E_sRuleQBCType.PrimaryBorrower;
                        case "A":
                            return E_sRuleQBCType.AllBorrowers;
                        case "C":
                            return E_sRuleQBCType.OnlyCoborrowers;
                        default:
                            return E_sRuleQBCType.Legacy;
                    }
                }
                return E_sRuleQBCType.Legacy;
            }
            set
            {
                XmlAttribute descAttr = m_ruleElement.Attributes["QBC"];
                if (descAttr == null && value != E_sRuleQBCType.Legacy)
                {
                    descAttr = m_xmlDoc.CreateAttribute("QBC");
                    m_ruleElement.Attributes.Append(descAttr);
                }

                switch (value)
                {
                    case E_sRuleQBCType.PrimaryBorrower:
                        descAttr.Value = "P";
                        break;
                    case E_sRuleQBCType.AllBorrowers:
                        descAttr.Value = "A";
                        break;
                    case E_sRuleQBCType.OnlyCoborrowers:
                        descAttr.Value = "C";
                        break;
                    default:    //Legacy
                        if (descAttr != null)
                            m_ruleElement.Attributes.Remove(descAttr);
                        break;
                }
            }
        }

        //opm 32459 fs 09/08/09
        private E_TriState m_lenderAdjTri = E_TriState.Blank;

        public bool IsLenderRule
        {
            get
            {
                if (E_TriState.Blank == m_lenderAdjTri)
                {
                    if (m_ruleElement.Attributes["LenderRule"] != null)
                        m_lenderAdjTri = E_TriState.Yes;
                    else
                        m_lenderAdjTri = E_TriState.No;
                }
                switch (m_lenderAdjTri)
                {
                    case E_TriState.No: return false;
                    case E_TriState.Yes: return true;
                    default: throw new CBaseException(ErrorMessages.Generic,
                        "Bug in Rule.IsLenderRule. Cached value has unexpected value.");
                }
            }
            set
            {
                if (value == IsLenderRule)
                    return; // no op.

                m_lenderAdjTri = E_TriState.Blank;
                XmlAttribute descAttr = m_ruleElement.Attributes["LenderRule"];

                if (value)
                {
                    if (descAttr == null)
                    {
                        descAttr = m_xmlDoc.CreateAttribute("LenderRule");
                        m_ruleElement.Attributes.Append(descAttr);
                    }                    
                    descAttr.Value = "T";
                }
                else
                {
                    if (descAttr != null)
                    {
                        m_ruleElement.Attributes.Remove(descAttr);
                    }
                }
            }
        }

		public Guid RuleId
		{
			get { return m_id; }
		}
		
	}
	
	public enum E_Purpose
	{
		Condition = 0x1,
		TargetFee = 0x2,
		TargetMargin = 0x4,
		TargetRate = 0x8,
		TargetBaseMargin = 0x10,
		TargetQltv = 0x20,
		TargetQcltv = 0x40,
		TargetQscore = 0x80,
        TargetMaxYsp = 0x100,
        TargetQrate = 0x200,
        TargetMaxDti = 0x400,
        ConditionEx = 0x800,
        TargetQLAmt = 0x1000,
        TargetQRateAdjust = 0x2000,
        TargetTeaserRateAdjust = 0x4000,
        LockDaysAdjust = 0x8000,
        TargetMaxFrontEndYsp = 0x10000,
		ALL = 0xFFFF  
		//ALL_BUT_STIPULATION
	};

	public abstract class CRuleEvalBase  
	{
		#region DATA MEMBERS
		protected XmlNode m_xmlNode;
        protected XmlNode m_exprNode;
		protected string m_errMsg;


        static public CSymbolTable DefaultTable
        {
            get { return CRuleEvalTool.DefaultTable; } 
        }

		#endregion // DATA MEMBERS
		

		protected CRuleEvalBase( string userExpression, string xmlElementName )
		{
			m_xmlNode = ( new XmlDocument()).CreateElement( xmlElementName );
			m_xmlNode.InnerText = userExpression;
            if( userExpression == "" )
                ((XmlElement)m_xmlNode).IsEmpty = true; // using short-form <name /> if value = "" otherwise long-form <name></name>
		}
		
	
		public CRuleEvalBase( XmlNode xmlNode )
		{
			m_xmlNode = xmlNode;
            if( m_xmlNode.InnerText == "" )
                ((XmlElement)m_xmlNode).IsEmpty = true; // using short-form <name /> if value = "" otherwise long-form <name></name>
        }

		public bool IsBlank
		{
			get { return "" == m_xmlNode.InnerText; }
		}


        virtual protected void OnUserExpressionModify()
        {
            lock( this )
            {
                m_exprNode = null;
            }
        }

		public string UserExpression
		{
			get { return m_xmlNode.InnerText; }
			set 
            { 
                string oldValue = m_xmlNode.InnerText;
                if( oldValue != value )
                {
                    m_xmlNode.InnerText = value;
                    if( value == "" )
                    {
                        ((XmlElement)m_xmlNode).IsEmpty = true; // using short-form <name /> if value = "" otherwise long-form <name></name>
                    }

                    OnUserExpressionModify();

                }
            } 
		}
		public XmlNode xmlNode
		{
			get { return this.m_xmlNode; }
		}
		abstract public E_Purpose Purpose
		{
			get;
		}
		public string[] ValidSymbols
		{
			get 
			{
				return DefaultTable.GetValidSymbols( Purpose );
			}
		}

        public string[] GetValidSymbols( Hashtable outNewKeywords  )
        {
            return DefaultTable.GetValidSymbols( Purpose, outNewKeywords );
        }
	}

	public class CCondition : CRuleEvalBase
	{

        /// <summary>
        /// if RestrictSyntax == true, don't allow FieldValue * number, don't allow CASHOUTAMT <> STATE_CA
        /// </summary>
        static public bool RestrictSyntax
        {
            set { CRuleEvalTool.SetRestrictSyntax( value ); }
        }

        static bool MatchingParenthesis(string data, out String errMsg)
        {
            int    row = 1;
            int    col = 1;

            const int LEFT  = -1;
            const int RIGHT = +1;
            int   balance   = 0;

            errMsg = ""; 
            for(int i =0; i < data.Length; i++)
            {
                switch( data[i] )
                {
                    case ')' :
                        balance += RIGHT;
                        if( balance > 0 )
                        {
                            errMsg = String.Format("The parenthesis doesn't match at line {0} col {1}", row, col);
                            return false;
                        }
                        col++;
                        break;

                    case '(' :
                        balance += LEFT;
                        col++;
                        break;

                    default :
                        if( data[i] == '\n' )
                        {
                            row++;
                            col = 1;
                        }
                        else if( data[i] != '\r' )
                            col++;
                        break;
                }
            }

            if( balance != 0 )
            {
                // at here we know : balance < 0
                errMsg = String.Format("The parenthesis doesn't match : expect {0} more )",  -balance );

                return false;
            }
            return true;
        }

        private static bool HasEmptyParentheses(string data, out string errorMessage)
        {
            // OPM 60414. 
            // Check to see if there exists an open parenthesis
            // with nothing but whitespace between it and its matching close.

            errorMessage = string.Empty;

            Match match = System.Text.RegularExpressions.Regex.Match(data, @"\(\s*\)");
            if (match.Success)
            {
                errorMessage = "Empty parentheses at position " + match.Index;
                return true;
            }

            return false;
        }

		
		public static CCondition CreateTestCondition( string userExpression )
		{
            String errMsg;
            if( MatchingParenthesis(userExpression, out errMsg) == false )
                throw new CConditionException( CEvaluationException.ErrorTypeEnum.UnMatchingParenthesis, 
                                               errMsg );

            if ( HasEmptyParentheses(userExpression, out errMsg ) )
                throw new CConditionException( CEvaluationException.ErrorTypeEnum.EmptyParentheses,
                               errMsg );

			return new CCondition( userExpression );
		}
		public static CCondition CreateEmptyCondition( XmlDocument xmlDoc )
		{
			XmlNode newElement = xmlDoc.CreateElement( "Condition" );
			return new CCondition( newElement );
		}
		
		private CCondition( string userExpression )
			: base( userExpression, "Condition" )
		{	
			
		}

		public CCondition( XmlNode xmlNode )
			: base( xmlNode)
		{
		}
		
		override public E_Purpose Purpose
		{
			get { return E_Purpose.Condition; }
		}

        public bool EvaluateTest( XmlDocument xmlDocEval )
        {
            return Evaluate( xmlDocEval, CRuleEvalTool.DefaultTable); 
        }

        
		public bool Evaluate( XmlDocument xmlDocEval, CSymbolTable symbolTable )
		{
            XmlNode xmlNode = null;
            lock( this )
            {

                if( m_exprNode == null )
                {
                    string origText = m_xmlNode.InnerText;

                    string xmlEval = CRuleEvalTool.ReplaceForEval( m_xmlNode.InnerText, symbolTable );
                    m_exprNode = CRuleEvalTool.CreateAnEvalNode( xmlDocEval, xmlEval );
                }
                xmlNode = m_exprNode;
            }

			decimal rawResult  = CRuleEvalTool.ExpressionDispatch( xmlNode, 0, symbolTable );

			if( 0 == rawResult )
				return false;
			else if( 1 == rawResult )
				return true;
			else
				throw new CBaseException( ErrorMessages.Generic, "Invalid expression.  True or false expression expected." );			
		}

		/// <summary>
		/// This should be called when need to save the xml text for the condition.
		/// </summary>
		/// <returns></returns>
		public XmlNode XmlNode
		{
            // THINH_TODO_4552 - Is this property necessary?? Look like this property already define in base class. Override here only create confusion.
			/* EXAMPLE:
				
					<Condition>  
						<Group>
							<Number FieldName="Debt">40000</Number>
							<Op>&lt;</Op>  
							<Number>20000</Number>  
						</Group>  
						<Op>Or</Op>
						<Group>  
							<Number FieldName="LoanAmount">100000</Number>
							<Op>&gt;</Op>
							<Number>20000</Number>  
						</Group>  
					</Condition>  
			 */
			get {return m_xmlNode; }
		}
		
	}

    public class CAdjustTarget : CRuleEvalBase
    {
        private E_Target m_target;
        public enum E_Target
        {
            Fee              = 0,
            Margin           = 1,
            Rate             = 2,
            BaseMargin       = 3,
            Qltv             = 4,
            Qcltv            = 5,
            Qscore           = 6,
            MaxYsp           = 7,  // (aka. Max BACK-END Ysp)
            Qrate            = 8,    // 11/30/2006 dd - OBSOLETE NO LONGER IN USE
            MaxDti           = 9,
            QLAmt            = 10,
            QrateAdjust      = 11,   // 11/30/2006 dd - OPM 7686.
            TeaserRateAdjust = 12,   // 11/30/2006 dd - OPM 7686.
            LockDaysAdjust   = 13,    // 09/29/09 fs opm 17988
            MaxFrontEndYsp   = 14   // 10/19/2010 mf OPM 43151
        };

        public class E_TargetName
        {
            static E_TargetName()
            {
            }

            public const string Fee              = "Fee";
            public const string Margin           = "Margin";
            public const string Rate             = "Rate";
            public const string BaseMargin       = "BaseMargin";
            public const string Qltv             = "Qltv";
            public const string Qcltv            = "Qcltv";
            public const string Qscore           = "Qscore";
            public const string MaxFrontEndYsp   = "MaxFrontEndYsp";      // 10/19/10 mf - OPM 43151
            public const string MaxYsp           = "MaxYsp";
            public const string Qrate            = "Qrate";               // 11/30/2006 dd - OBSOLETE NO LONGER IN USE
            public const string MaxDti           = "MaxDti";
            public const string QLAmt            = "QLAmt";
            public const string QrateAdjust      = "QrateAdjust";         // 11/30/2006 dd - OPM 7686.
            public const string TeaserRateAdjust = "TeaserRateAdjust";    // 11/30/2006 dd - OPM 7686.
            public const string LockDaysAdjust   = "LockDaysAdjust";      // 09/28/09   fs - opm 17988

            public static string Target2String( E_Target target )
            {
                switch ( target )
                {
                    case E_Target.Fee:              return Fee;
                    case E_Target.Margin:           return Margin;
                    case E_Target.Rate:             return Rate;
                    case E_Target.BaseMargin:       return BaseMargin;
                    case E_Target.Qltv:             return Qltv;
                    case E_Target.Qcltv:            return Qcltv;
                    case E_Target.Qscore:           return Qscore;
                    case E_Target.MaxFrontEndYsp:   return MaxFrontEndYsp;
                    case E_Target.MaxYsp:           return MaxYsp;
                    case E_Target.Qrate:            return Qrate;
                    case E_Target.MaxDti:           return MaxDti;
                    case E_Target.QLAmt:            return QLAmt;
                    case E_Target.QrateAdjust:      return QrateAdjust;
                    case E_Target.TeaserRateAdjust: return TeaserRateAdjust;
                    case E_Target.LockDaysAdjust:   return LockDaysAdjust;
                    default:
                        throw new CBaseException( ErrorMessages.EnumValueNotHandled, "Programming error:  Unexpected target type in E_TargetName.Target2String(...)." );
                }

            }

            public static E_Target String2Target( string targetStr )
            {
                switch ( targetStr )
                {
                    case Fee:              return E_Target.Fee;
                    case Margin:           return E_Target.Margin;
                    case Rate:             return E_Target.Rate;
                    case BaseMargin:       return E_Target.BaseMargin;
                    case Qltv:             return E_Target.Qltv;
                    case Qcltv:            return E_Target.Qcltv;
                    case Qscore:           return E_Target.Qscore;
                    case MaxYsp:           return E_Target.MaxYsp;
                    case MaxFrontEndYsp:   return E_Target.MaxFrontEndYsp;
                    case Qrate:            return E_Target.Qrate;
                    case MaxDti:           return E_Target.MaxDti;
                    case QLAmt:            return E_Target.QLAmt;
                    case QrateAdjust:      return E_Target.QrateAdjust;
                    case TeaserRateAdjust: return E_Target.TeaserRateAdjust;
                    case LockDaysAdjust:   return E_Target.LockDaysAdjust;
                    default:
                        throw new CBaseException( ErrorMessages.EnumValueNotHandled, "Programming error:  Unexpected target type in E_TargetName.String2Target(...)." );
                }

            }

        };


        public static CAdjustTarget CreateTestAdjustTarget( E_Target target, string userExpression )
        {
            const bool AllowExceptionForEvaluate = true;
            return new CAdjustTarget( target, userExpression, AllowExceptionForEvaluate );
        }

        public static CAdjustTarget CreateEmptyAdjustTarget( E_Target target, XmlDocument xmlDoc )
        {
            XmlNode newElement = xmlDoc.CreateElement( "Adjust" );

            XmlAttribute newAttribute = CreateTargetAttribute(target, xmlDoc);

            newElement.Attributes.SetNamedItem( newAttribute );

            return new CAdjustTarget( newElement  );
        }

        private static XmlAttribute CreateTargetAttribute(E_Target target, XmlDocument doc) 
        {
            XmlAttribute newAttribute = doc.CreateAttribute("Target");
            newAttribute.Value = E_TargetName.Target2String( target );
                
            return newAttribute;
        }

        bool m_allowExceptionForEvaluate;

        public bool AllowExceptionForEvaluate
        {
            get { return m_allowExceptionForEvaluate; }
            set { m_allowExceptionForEvaluate = value; }

        }

        private CAdjustTarget( E_Target target, string userExpression) : this( target, userExpression, false)
        {
        }

		private CAdjustTarget( E_Target target, string userExpression, bool allowExceptionForEvaluate )
			: base( userExpression, "Adjust" )
		{	
			XmlDocument xmlDoc= m_xmlNode.OwnerDocument;

            XmlAttribute newAttribute = CreateTargetAttribute(target, xmlDoc);
			
			m_xmlNode.Attributes.SetNamedItem( newAttribute );
            m_target = target;
            m_allowExceptionForEvaluate = allowExceptionForEvaluate;
		}

		public string AdjustmentText
		{
			get { return m_xmlNode.InnerText; }
		}

		public CAdjustTarget( XmlNode xmlNode )
			: base( xmlNode )
		{
			XmlAttribute targetAttribute = m_xmlNode.Attributes["Target"];
            m_target = E_TargetName.String2Target( targetAttribute.Value );

		}
		override public E_Purpose Purpose
		{
			get 
			{ 
				switch( m_target )
				{
					case E_Target.Fee: return E_Purpose.TargetFee;
					case E_Target.Margin: return E_Purpose.TargetMargin;
					case E_Target.Rate: return E_Purpose.TargetRate;
					case E_Target.BaseMargin: return E_Purpose.TargetBaseMargin;
					case E_Target.Qltv: return E_Purpose.TargetQltv;
					case E_Target.Qcltv: return E_Purpose.TargetQcltv;
					case E_Target.Qscore: return E_Purpose.TargetQscore;
                    case E_Target.MaxFrontEndYsp: return E_Purpose.TargetMaxFrontEndYsp;
                    case E_Target.MaxYsp: return E_Purpose.TargetMaxYsp;
                    case E_Target.Qrate: return E_Purpose.TargetQrate;
                    case E_Target.MaxDti: return E_Purpose.TargetMaxDti;
                    case E_Target.QLAmt: return E_Purpose.TargetQLAmt;
                    case E_Target.QrateAdjust: return E_Purpose.TargetQRateAdjust;
                    case E_Target.TeaserRateAdjust: return E_Purpose.TargetTeaserRateAdjust;
                    case E_Target.LockDaysAdjust: return E_Purpose.LockDaysAdjust;
					default: throw new CBaseException( ErrorMessages.EnumValueNotHandled, "Programming error:  Target '" + m_target.ToString("D") + "' is not handled." );
				}
			}
		}

        
        public decimal EvaluateTest( XmlDocument xmlDocEval)
        {
            return Evaluate( xmlDocEval, CRuleEvalTool.DefaultTable);
        }
        

		public decimal Evaluate( XmlDocument xmlDocEval, CSymbolTable symbolTable)
		{
			string rawText = m_xmlNode.InnerText;
			
			// 3/8/05 tn: perf optimizing, empty string would eventually evaluate to 0
			if( "" == rawText )
				return 0;

			switch( Purpose )
			{
				case E_Purpose.TargetRate:
				case E_Purpose.TargetQcltv:
				case E_Purpose.TargetQltv:
				case E_Purpose.TargetQscore:
                case E_Purpose.TargetMaxYsp:
                case E_Purpose.TargetMaxFrontEndYsp:
                case E_Purpose.TargetQRateAdjust:
                case E_Purpose.TargetTeaserRateAdjust:
					rawText = rawText.Replace( "%", "" ); // Because we use the percent edit box in the editor for rate adjustment, this is what we get extra.
					break;
                case E_Purpose.TargetMaxDti: // TODO: Need to implement this see cae 4552
                    rawText = rawText.Replace( "%", "" ); // Because we use the percent edit box in the editor for rate adjustment, this is what we get extra.
					break;
				case E_Purpose.TargetQLAmt:
				{
                    /*
                    OPM 12935 : Change QLAmt's format from integer to an expression as  1.15 * LAMT
					try
					{
						return int.Parse( rawText );
					}
					catch
					{
						Tools.LogErrorAndSendMail( string.Format(  "Encountering a rule that set QLAmt to an acceptable value:{0}", rawText ) );
					}
                    return 0;
                    */
                    break;
					
				}
                case E_Purpose.TargetQrate:
                    return 0;
			}

            XmlNode xmlNode = null;
            lock( this )
            {
                if( m_exprNode == null )
                {
                    string xmlEval  = CRuleEvalTool.ReplaceForEval( rawText, symbolTable );
                    m_exprNode = CRuleEvalTool.CreateAnEvalNode( xmlDocEval, xmlEval );
                }
                xmlNode = m_exprNode;
            }
			
			
			try
			{
				return CRuleEvalTool.ExpressionDispatch( xmlNode, 0, symbolTable );
			}
			catch
			{
                if( m_allowExceptionForEvaluate )
                    throw;
				return 0;
			}
		}
	}

	
	/// <summary>
	/// Represent all consequences (targets) in a rule
	/// </summary>
	public class CConsequence
	{
		private XmlNode m_xmlNode;

		private CAdjustTarget m_feeAdjustTarget;
		private CAdjustTarget m_marginAdjustTarget;
		private CAdjustTarget m_rateAdjustTarget;
		private CAdjustTarget m_baseMarginTarget;
		private CAdjustTarget m_qltvAdjustTarget;
		private CAdjustTarget m_qcltvAdjustTarget;
		private CAdjustTarget m_qscoreTarget;
        private CAdjustTarget m_maxFrontEndYspAdjustTarget;
        private CAdjustTarget m_maxYspAdjustTarget;
        private CAdjustTarget m_qrateTarget;
        private CAdjustTarget m_maxDtiTarget;
        private CAdjustTarget m_qlamtAdjustTarget;
        private CAdjustTarget m_qrateAdjustTarget;
        private CAdjustTarget m_teaserRateAdjustTarget;
        private CAdjustTarget m_lockDaysAdjustTarget;

		public static CConsequence CreateEmptyConsequence( XmlDocument xmlDoc )
		{
			XmlNode newElement = xmlDoc.CreateElement( "Consequence" );
			return new CConsequence( newElement );
		}

		public CConsequence( XmlNode xmlNode )
		{
			m_xmlNode = xmlNode;
		}

        CAdjustTarget LoadAdjustTarget( CAdjustTarget.E_Target target )
        {
            string expectTargetName    = CAdjustTarget.E_TargetName.Target2String( target );

            string xpath  = string.Format( ".//Adjust[@Target = '{0}']",  expectTargetName );

            XmlNode targetElement = m_xmlNode.SelectSingleNode(xpath);

            if( targetElement != null )
                return new CAdjustTarget( targetElement ); 


            CAdjustTarget emptyAdjTarget = CAdjustTarget.CreateEmptyAdjustTarget( target, m_xmlNode.OwnerDocument );
            m_xmlNode.AppendChild( emptyAdjTarget.xmlNode  );

            return emptyAdjTarget;
        }

		public CAdjustTarget FeeAdjustTarget
		{
			get 
			{
				if( null == m_feeAdjustTarget )
                    m_feeAdjustTarget  = LoadAdjustTarget( CAdjustTarget.E_Target.Fee);

				return m_feeAdjustTarget;
			}
		}

		
		public bool HasQConsequence
		{
			get
			{
				// TODO: Perf, we can tune this up a little by digging to xml file directly
				// to avoid creating CAdjustTarget object for each call to QltvAdjustTarget etc...
				if( !QltvAdjustTarget.IsBlank )
					return true;
				
				if( !QcltvAdjustTarget.IsBlank )
					return true;

				if( !QscoreTarget.IsBlank )
					return true;

                if( !MaxYspAdjustTarget.IsBlank )
                    return true;

				if( !QLAmtAdjustTarget.IsBlank )
					return true;

				return false;
			}
		}

		public CAdjustTarget QltvAdjustTarget
		{
			get 
			{
				if( null == m_qltvAdjustTarget )
                    m_qltvAdjustTarget  = LoadAdjustTarget( CAdjustTarget.E_Target.Qltv );

				return m_qltvAdjustTarget;
			}
		}

		public CAdjustTarget QcltvAdjustTarget
		{
			get 
			{
				if( null == m_qcltvAdjustTarget )
                    m_qcltvAdjustTarget  = LoadAdjustTarget( CAdjustTarget.E_Target.Qcltv );

				return m_qcltvAdjustTarget;
			}
		}

        public CAdjustTarget QLAmtAdjustTarget
        {
            get 
            {
                if( null == m_qlamtAdjustTarget )
                    m_qlamtAdjustTarget  = LoadAdjustTarget( CAdjustTarget.E_Target.QLAmt );

                return m_qlamtAdjustTarget;
            }
        }

        public CAdjustTarget MaxFrontEndYspAdjustTarget
        {
            get
            {
                if (null == m_maxFrontEndYspAdjustTarget)
                    m_maxFrontEndYspAdjustTarget = LoadAdjustTarget(CAdjustTarget.E_Target.MaxFrontEndYsp);

                return m_maxFrontEndYspAdjustTarget;
            }
        }
        public CAdjustTarget MaxYspAdjustTarget
        {
            get 
            {
                if( null == m_maxYspAdjustTarget )
                    m_maxYspAdjustTarget  = LoadAdjustTarget( CAdjustTarget.E_Target.MaxYsp );

                return m_maxYspAdjustTarget;
            }
        }


		public CAdjustTarget QscoreTarget
		{
			get 
			{
				if( null == m_qscoreTarget )
                    m_qscoreTarget  = LoadAdjustTarget( CAdjustTarget.E_Target.Qscore );
                return m_qscoreTarget;
			}
		}

        public CAdjustTarget QrateTarget 
        {
            get 
            {
                if (null == m_qrateTarget) 
                    m_qrateTarget  = LoadAdjustTarget( CAdjustTarget.E_Target.Qrate );
                return m_qrateTarget;
            }
        }

        public CAdjustTarget QrateAdjustTarget 
        {
            get 
            {
                if (null == m_qrateAdjustTarget) 
                    m_qrateAdjustTarget = LoadAdjustTarget( CAdjustTarget.E_Target.QrateAdjust );
                return m_qrateAdjustTarget;
            }
        }
        public CAdjustTarget TeaserRateAdjustTarget 
        {
            get 
            {
                if (null == m_teaserRateAdjustTarget) 
                    m_teaserRateAdjustTarget = LoadAdjustTarget( CAdjustTarget.E_Target.TeaserRateAdjust );

                return m_teaserRateAdjustTarget;
            }
        }
        public CAdjustTarget MaxDtiTarget 
        {
            get 
            {
                if (null == m_maxDtiTarget) 
                    m_maxDtiTarget  = LoadAdjustTarget( CAdjustTarget.E_Target.MaxDti );

                return m_maxDtiTarget;
            }

        }

        public CAdjustTarget MarginAdjustTarget
        {
            get 
            {
                if( null == m_marginAdjustTarget )
                    m_marginAdjustTarget = LoadAdjustTarget( CAdjustTarget.E_Target.Margin );

                return m_marginAdjustTarget;
            }
        }

		public CAdjustTarget RateAdjustTarget
		{
			get 
			{
				if( null == m_rateAdjustTarget )
                    m_rateAdjustTarget = LoadAdjustTarget( CAdjustTarget.E_Target.Rate );

                return m_rateAdjustTarget;
			}
		}

		public CAdjustTarget MarginBaseTarget
		{
			get 
			{
				if( null == m_baseMarginTarget )
                    m_baseMarginTarget = LoadAdjustTarget( CAdjustTarget.E_Target.BaseMargin );

				return m_baseMarginTarget;
			}
		}

        //opm 17988 fs 09/29/09
        public CAdjustTarget LockDaysAdjustTarget
		{
			get 
			{
                if (null == m_lockDaysAdjustTarget)
                    m_lockDaysAdjustTarget = LoadAdjustTarget(CAdjustTarget.E_Target.LockDaysAdjust);

                return m_lockDaysAdjustTarget;
			}
		}

		public string Stipulation
		{
			get 
			{ 
                XmlNode stipulation = m_xmlNode.SelectSingleNode( ".//Stipulation" ); // selects the "Stipulation" element descendants of the context node
				return stipulation == null ?  "" : stipulation.InnerText; 
			}
			set
			{
				XmlNodeList stipulations = m_xmlNode.SelectNodes( ".//Stipulation" );
				XmlNode stipulationElement;
				if( 0 == stipulations.Count )
				{
					XmlDocument xmlDoc = m_xmlNode.OwnerDocument;
					stipulationElement = xmlDoc.CreateElement( "Stipulation" );
					m_xmlNode.AppendChild( stipulationElement ); 
				}
				else
					stipulationElement = (XmlNode) stipulations[0];
			                			
				stipulationElement.InnerText = value;
                if( value == "" )
                    ((XmlElement)stipulationElement).IsEmpty = true; // using short-form <Stipulation /> if value = "" otherwise long-form <Stipulation></Stipulation>
			}
		}

		private E_TriState m_disqResTri = E_TriState.Blank;

		public bool Disqualify
		{
			get 
			{ 
				if( E_TriState.Blank == m_disqResTri )
				{
					if( m_xmlNode.SelectSingleNode( ".//Disqualify" ) != null )
						m_disqResTri = E_TriState.Yes;
					else
						m_disqResTri = E_TriState.No;
				}

				switch( m_disqResTri )
				{
					case E_TriState.No: return false;
					case E_TriState.Yes: return true;
					default: throw new CBaseException( ErrorMessages.Generic, 
						"Bug in CConsequence.Disqualify:  Cached value for has unexpected value." );
				}
			}
			set
			{
				if( value == Disqualify )
					return; // no op.

				m_disqResTri = E_TriState.Blank;

				if( value )
				{
					XmlNode newElement =  m_xmlNode.OwnerDocument.CreateElement( "Disqualify" );
					m_xmlNode.AppendChild( newElement );
				}
				else
				{
					XmlNodeList disqualifies = m_xmlNode.SelectNodes( ".//Disqualify" );
					for( int i = 0; i < disqualifies.Count; ++i )
					{
						m_xmlNode.RemoveChild( disqualifies[i] );
					}

				}
			}
		}

		private E_TriState	m_skipResTri = E_TriState.Blank;
		public bool Skip
		{
			get 
			{ 
				if( E_TriState.Blank == m_skipResTri )
				{
					if( m_xmlNode.SelectSingleNode( ".//Skip" ) != null )
						m_skipResTri = E_TriState.Yes;
					else
						m_skipResTri = E_TriState.No;
				}
				switch( m_skipResTri )
				{
					case E_TriState.No: return false;
					case E_TriState.Yes: return true;
					default: throw new CBaseException( ErrorMessages.Generic, 
						"Bug in CConsequence.Skip. Cached value has unexpected value." );
				}
			}
			set
			{
				if( value == Skip )
					return; // no op.

				m_skipResTri = E_TriState.Blank;

				if( value )
				{
					XmlNode newElement =  m_xmlNode.OwnerDocument.CreateElement( "Skip" );
					m_xmlNode.AppendChild( newElement );
				}
				else
				{
					XmlNodeList skips = m_xmlNode.SelectNodes( ".//Skip" );
					for( int i = 0; i < skips.Count; ++i )
					{
						m_xmlNode.RemoveChild( skips[i] );
					}

				}
			}
		}

		/// <summary>
		/// This should be called when need to save the xml text for the whole rule.
		/// </summary>
		/// <returns></returns>
		public XmlNode XmlNode
		{
			/* EXAMPLE:

					<Consequence>
						<!--Fee += ( CashoutAmount / 100 + 0.5 )-->
						<Adjust target="Fee">
							<Group>
								<Number FieldName="CashoutAmount">50000</Number>
								<Op>Divide</Op>
								<Number>100</Number>
							</Group>
							<Op>+</Op>
							<Number>0.5</Number>
						</Adjust>
						<!--Margin += -2 -->
						<Adjust target="Margin">
							<Number>-2</Number>
						</Adjust>
						<Stipulation>
							Must still be employed at closing.
						</Stipulation>
					</Consequence> 
			 */
			//Constructing xml node from the information it contains in case users edited its children.
			get 
			{
				return m_xmlNode; 
			}
		}
	}
}