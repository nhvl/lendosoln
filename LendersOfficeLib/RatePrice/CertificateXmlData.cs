using System;
using System.Collections;
using System.Xml;
using DataAccess;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.Security;
using LendersOffice.Reminders;
using LendersOffice.Constants;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LendersOffice.AntiXss;
using LendersOffice.Common;
using LendersOffice.ObjLib.Task;
using LendersOffice.CustomPmlFields;
using LendersOffice.ObjLib.PriceGroups;
using System.Text;
using LendersOffice.Admin;

namespace LendersOfficeApp.newlos.Template
{
    
    public class CertificateXmlData
    {
        protected static Boolean IsPmlEnabled
        {
            get { return (Thread.CurrentPrincipal as AbstractUserPrincipal).HasFeatures(E_BrokerFeatureT.PriceMyLoan); }
        }

        public static CApplicantRateOption GetRateOptionForCertificate(CApplicantPrice product, string requestedRate, out string rate, out string point, out string id)
        {
            CApplicantRateOption selectedRateOption = null;
            rate = point = id = null;

            // 7/15/2005 dd - While waiting for new submission workflow, we will not highlight user requested rate.
            if (null != requestedRate && requestedRate != "0.000:0.000:0.000,0.000")
            {
                requestedRate = requestedRate.Replace("%", "");

                string[] parts = requestedRate.Split(':');
                if (parts.Length == 3)
                {
                    rate = parts[0];
                    point = parts[1];
                    id = parts[2];
                }
                if (id == "0.000,0.000")
                {
                    id = null; // 1/17/2011 dd - We do not compare raw rate / point for this situation.
                }
            }

            if (id != null && product != null && product.ApplicantRateOptions != null)
            {
                foreach (var ro in product.ApplicantRateOptions)
                {
                    if (ro.RateOptionId == id)
                    {
                        selectedRateOption = ro;
                        break;
                    }
                }
            }

            return selectedRateOption;
        }

        public static void Generate(XmlWriter writer, CPageData  dataLoan, CApplicantPrice product, string requestedRate, 
            E_sLienQualifyModeT sLienQualifyModeT, AbstractUserPrincipal principal, string first_sLNm, bool isRateLockRequested, bool isLoadStipFromCondition,
            Guid priceGroupID, decimal selectedParRate) 
        {
            BrokerDB broker = BrokerDB.RetrieveById(principal.BrokerId);
            // dd 05/25/2005 - Any new field need in certificate need to modify DataAccess.DataAccessUtils.AddCertificateFields method

            // OPM 288698 - Find selected rate option, for use in getting DTI/sNoteIR.
            string rate = null;
            string point = null;
            string id = null;
            CApplicantRateOption selectedRateOption = GetRateOptionForCertificate(product, requestedRate, out rate, out point, out id);

            if (sLienQualifyModeT == E_sLienQualifyModeT.OtherLoan) 
            {
                dataLoan.MakeThisInto2ndLienForQualifying(null);
                //10-30-04 tn - added ... to the end of the string as this is not a 
                // guaranteed certificate number for the 2nd loan. In case of name collision
                // 2nd loan will have a numerical postfix.
                dataLoan.SetsLNmWithPermissionBypass(dataLoan.sLNm + "x2nd..."); // 10/19/2004 dd - This is a hack for displaying correct id in second loan program's certificate
            }

            CAppData dataApp = dataLoan.GetAppData(0);
            writer.WriteStartElement("pml_cert");

            DeterminationRuleSet rules = dataLoan.BrokerDB.DeterminationRuleSet;
            if (rules.HasRules)
            {
                writer.WriteStartElement("messages");
                foreach (DeterminationRule rule in dataLoan.BrokerDB.DeterminationRuleSet.QualifyingRules(isRateLockRequested, dataLoan.sBranchChannelT, dataLoan.sStatusT))
                {
                    foreach (DeterminationMessage message in rule.Messages)
                    {
                        if (!string.IsNullOrEmpty(message.Text))
                        {
                            writer.WriteStartElement("message");
                            writer.WriteElementString("text", message.Text);
                            writer.WriteElementString("font", message.Font);
                            writer.WriteEndElement();
                        }
                    }
                }

                writer.WriteEndElement();
            }

            writer.WriteElementString("date_created", Tools.GetDateTimeNowString());

			// 10/04/06 mf OPM 7404. We have to see if this loan comes from a branch
			// that we are supposed to display the name of.
			BranchDB branch = new BranchDB( dataLoan.sBranchId, dataLoan.sBrokerId );
			branch.Retrieve();
            writer.WriteElementString("branch_name", branch.Name); // 8/31/2015 - dd OPM 210318 - So MWF can include branch name on certificate
            writer.WriteElementString("show_logo", (!branch.IsDisplayNmModified).ToString()); 
			writer.WriteElementString("broker_name", branch.DisplayNm);
            writer.WriteElementString("branch_street_address", branch.Address.StreetAddress);
            writer.WriteElementString("branch_city", branch.Address.City);
            writer.WriteElementString("branch_state", branch.Address.State);
            writer.WriteElementString("branch_zip", branch.Address.Zipcode);
            writer.WriteElementString("branch_phone", branch.Phone);
            writer.WriteElementString("branch_fax", branch.Fax);
            writer.WriteElementString("broker_url", broker.Url);
            
            #region <lender>
            #endregion
			//OPM 12711
			//underwriter will always show unless its false to be consistent with old certificates
			//processor will show only when its true
			#region <broker_variables>
			writer.WriteStartElement("broker_variables");
			writer.WriteElementString("ShowProcessorInPMLCertificate", broker.ShowProcessorInPMLCertificate.ToString());
			writer.WriteElementString("ShowJuniorProcessorInPMLCertificate", broker.ShowJuniorProcessorInPMLCertificate.ToString()); // 11/21/2013 gf - opm 145015
			writer.WriteElementString("ShowUnderwriterInPMLCertificate", broker.ShowUnderwriterInPMLCertificate.ToString());
			writer.WriteElementString("ShowJuniorUnderwriterInPMLCertificate", broker.ShowJuniorUnderwriterInPMLCertificate.ToString()); // 11/21/2013 gf - opm 145015
            writer.WriteElementString("IsIncomeAssetsRequiredFhaStreamline", broker.IsIncomeAssetsRequiredFhaStreamline.ToString()); //opm 32180 fs 11/19/09
            writer.WriteElementString("DisplayNoteRateAndDtiOnCertificate", broker.DisplayNoteRateAndDtiOnCertificate.ToString());
			writer.WriteEndElement(); //</broker_variables>
			#endregion 

            #region <account_executive>
                writer.WriteStartElement("account_executive");
                if (broker.CustomerCode.Equals("PML0200", StringComparison.OrdinalIgnoreCase))
                {
                    foreach (CAgentFields aeAgent in dataLoan.GetAgentsOfRole(E_AgentRoleT.Other, E_ReturnOptionIfNotExist.ReturnEmptyObject))
                    {
                        if (aeAgent.OtherAgentRoleTDesc.Equals("Account Executive", StringComparison.OrdinalIgnoreCase))
                        {
                            writer.WriteElementString("name", aeAgent.AgentName);
                            writer.WriteElementString("phone", aeAgent.PhoneOfCompany);
                            writer.WriteElementString("fax", "");
                            writer.WriteElementString("email", aeAgent.EmailAddr);
                            writer.WriteElementString("show", "");
                            break;
                        }
                    }

                }
                else
                {
                    writer.WriteElementString("name", dataLoan.sEmployeeLenderAccExec.FullName);
                    writer.WriteElementString("phone", dataLoan.sEmployeeLenderAccExec.Phone);
                    writer.WriteElementString("fax", dataLoan.sEmployeeLenderAccExec.Fax);
                    writer.WriteElementString("email", dataLoan.sEmployeeLenderAccExec.Email);
                    writer.WriteElementString("show", dataLoan.sEmployeeLenderAccExec.IsValid.ToString());
                }
                writer.WriteEndElement(); // </account_executive>
            #endregion

            #region <underwriter>
            writer.WriteStartElement("underwriter");
            writer.WriteElementString("name", dataLoan.sEmployeeUnderwriter.FullName);
            writer.WriteElementString("phone", dataLoan.sEmployeeUnderwriter.Phone);
            writer.WriteElementString("fax", dataLoan.sEmployeeUnderwriter.Fax);
            writer.WriteElementString("email", dataLoan.sEmployeeUnderwriter.Email);
            writer.WriteEndElement(); // </underwriter>
            #endregion

            #region <junior_underwriter>
            writer.WriteStartElement("junior_underwriter");
            writer.WriteElementString("name", dataLoan.sEmployeeJuniorUnderwriter.FullName);
            writer.WriteElementString("phone", dataLoan.sEmployeeJuniorUnderwriter.Phone);
            writer.WriteElementString("fax", dataLoan.sEmployeeJuniorUnderwriter.Fax);
            writer.WriteElementString("email", dataLoan.sEmployeeJuniorUnderwriter.Email);
            writer.WriteEndElement(); // </junior_underwriter>
            #endregion
			//OPM 12711
			#region <processor>
			writer.WriteStartElement("processor");
			writer.WriteElementString("name", dataLoan.sEmployeeProcessor.FullName);
			writer.WriteElementString("phone", dataLoan.sEmployeeProcessor.Phone);
			writer.WriteElementString("fax", dataLoan.sEmployeeProcessor.Fax);
			writer.WriteElementString("email", dataLoan.sEmployeeProcessor.Email);
			writer.WriteEndElement(); // </processor>
      		#endregion

			#region <junior_processor>
			writer.WriteStartElement("junior_processor");
			writer.WriteElementString("name", dataLoan.sEmployeeJuniorProcessor.FullName);
			writer.WriteElementString("phone", dataLoan.sEmployeeJuniorProcessor.Phone);
			writer.WriteElementString("fax", dataLoan.sEmployeeJuniorProcessor.Fax);
			writer.WriteElementString("email", dataLoan.sEmployeeJuniorProcessor.Email);
			writer.WriteEndElement(); // </junior_processor>
            #endregion

            #region <doc_drawer>
            if (dataLoan.sEmployeeDocDrawer.IsValid)
            {
                writer.WriteStartElement("doc_drawer");
                writer.WriteElementString("name", dataLoan.sEmployeeDocDrawer.FullName);
                writer.WriteElementString("phone", dataLoan.sEmployeeDocDrawer.Phone);
                writer.WriteElementString("fax", dataLoan.sEmployeeDocDrawer.Fax);
                writer.WriteElementString("email", dataLoan.sEmployeeDocDrawer.Email);
                writer.WriteEndElement(); // </doc_drawer>
            }
            #endregion

            #region <lock_desk>
            if (dataLoan.sEmployeeLockDesk.IsValid)
            {
                writer.WriteStartElement("lock_desk");
                writer.WriteElementString("name", dataLoan.sEmployeeLockDesk.FullName);
                writer.WriteElementString("phone", dataLoan.sEmployeeLockDesk.Phone);
                writer.WriteElementString("fax", dataLoan.sEmployeeLockDesk.Fax);
                writer.WriteElementString("email", dataLoan.sEmployeeLockDesk.Email);
                writer.WriteEndElement(); // </lock_desk>
            }
            #endregion

            #region <credit_auditor>
            if (dataLoan.sEmployeeCreditAuditor.IsValid)
            {
                writer.WriteStartElement("credit_auditor");
                writer.WriteElementString("name", dataLoan.sEmployeeCreditAuditor.FullName);
                writer.WriteElementString("phone", dataLoan.sEmployeeCreditAuditor.Phone);
                writer.WriteElementString("fax", dataLoan.sEmployeeCreditAuditor.Fax);
                writer.WriteElementString("email", dataLoan.sEmployeeCreditAuditor.Email);
                writer.WriteEndElement(); // </credit_auditor>
            }
            #endregion

            #region <funder>
            if (dataLoan.sEmployeeFunder.IsValid)
            {
                writer.WriteStartElement("funder");
                writer.WriteElementString("name", dataLoan.sEmployeeFunder.FullName);
                writer.WriteElementString("phone", dataLoan.sEmployeeFunder.Phone);
                writer.WriteElementString("fax", dataLoan.sEmployeeFunder.Fax);
                writer.WriteElementString("email", dataLoan.sEmployeeFunder.Email);
                writer.WriteEndElement(); // </funder>
            }
            #endregion

            #region <E_AgentRoleT_LoanOfficer>
            CAgentFields agent = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            writer.WriteStartElement("E_AgentRoleT_LoanOfficer");
            writer.WriteElementString("CompanyName", agent.CompanyName);
            writer.WriteElementString("PhoneOfCompany", agent.PhoneOfCompany);
            writer.WriteElementString("BranchName", agent.BranchName); // dd 8/3/2015 - OPM 210318 Add for MWF
            writer.WriteElementString("FaxOfCompany", agent.FaxOfCompany);
            writer.WriteElementString("AgentName", agent.AgentName);
            writer.WriteElementString("Phone", agent.Phone);
            writer.WriteElementString("FaxNum", agent.FaxNum);
            writer.WriteElementString("EmailAddr", agent.EmailAddr);

            if (agent != CAgentFields.Empty)
            {
                writer.WriteElementString("show", "");
            }
            writer.WriteEndElement(); // </E_AgentRoleT_LoanOfficer>
            #endregion

            #region <E_AgentRoleT_Processor>
            CAgentFields processorAgent = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.Processor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            writer.WriteStartElement("E_AgentRoleT_Processor");
            writer.WriteElementString("CompanyName", processorAgent.CompanyName);
            writer.WriteElementString("BranchName", processorAgent.BranchName);
            writer.WriteElementString("PhoneOfCompany", processorAgent.PhoneOfCompany);
            writer.WriteElementString("FaxOfCompany", processorAgent.FaxOfCompany);
            writer.WriteElementString("AgentName", processorAgent.AgentName);
            writer.WriteElementString("Phone", processorAgent.Phone);
            writer.WriteElementString("FaxNum", processorAgent.FaxNum);
            writer.WriteElementString("EmailAddr", processorAgent.EmailAddr);
            writer.WriteElementString("StreetAddr", processorAgent.StreetAddr);
            writer.WriteElementString("CityStateZip", processorAgent.CityStateZip);

            if (processorAgent != CAgentFields.Empty)
            {
                writer.WriteElementString("show", string.Empty);
            }

            writer.WriteEndElement(); // </E_AgentRoleT_Processor>
            #endregion

            #region <E_AgentRoleT_BrokerProcessor>
            CAgentFields brokerProcessorAgent = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.BrokerProcessor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            writer.WriteStartElement("E_AgentRoleT_BrokerProcessor");
            writer.WriteElementString("CompanyName", brokerProcessorAgent.CompanyName);
            writer.WriteElementString("BranchName", brokerProcessorAgent.BranchName);
            writer.WriteElementString("PhoneOfCompany", brokerProcessorAgent.PhoneOfCompany);
            writer.WriteElementString("FaxOfCompany", brokerProcessorAgent.FaxOfCompany);
            writer.WriteElementString("AgentName", brokerProcessorAgent.AgentName);
            writer.WriteElementString("Phone", brokerProcessorAgent.Phone);
            writer.WriteElementString("FaxNum", brokerProcessorAgent.FaxNum);
            writer.WriteElementString("EmailAddr", brokerProcessorAgent.EmailAddr);
            writer.WriteElementString("StreetAddr", brokerProcessorAgent.StreetAddr);
            writer.WriteElementString("CityStateZip", brokerProcessorAgent.CityStateZip);

            if (brokerProcessorAgent != CAgentFields.Empty)
            {
                writer.WriteElementString("show", string.Empty);
            }

            writer.WriteEndElement(); // </E_AgentRoleT_BrokerProcessor>
            #endregion

            #region <loan_officer>
            writer.WriteStartElement("loan_officer");
            writer.WriteElementString("name", dataLoan.sEmployeeLoanRep.FullName);
            writer.WriteElementString("pml_broker_name", dataLoan.sEmployeeLoanRepPmlUserBrokerNm);
            writer.WriteElementString("phone", dataLoan.sEmployeeLoanRep.Phone);
            writer.WriteElementString("fax", dataLoan.sEmployeeLoanRep.Fax);
            writer.WriteElementString("email", dataLoan.sEmployeeLoanRepEmail);
			writer.WriteEndElement(); // </loan_officer>
            #endregion
            #region <broker_processor>
            writer.WriteStartElement("broker_processor");
            writer.WriteElementString("name", dataLoan.sEmployeeBrokerProcessor.FullName);
            writer.WriteElementString("phone", dataLoan.sEmployeeBrokerProcessor.Phone);
            writer.WriteElementString("fax", dataLoan.sEmployeeBrokerProcessor.Fax);
            writer.WriteElementString("email", dataLoan.sEmployeeBrokerProcessor.Email);
            if (dataLoan.sEmployeeBrokerProcessor.IsValid && IsPmlEnabled)
            {
                writer.WriteElementString("show", "");
            }
            writer.WriteEndElement(); // </broker_processor>
            #endregion

            #region External Secondary
            writer.WriteStartElement("external_secondary");
            writer.WriteElementString("name", dataLoan.sEmployeeExternalSecondary.FullName);
            writer.WriteElementString("phone", dataLoan.sEmployeeExternalSecondary.Phone);
            writer.WriteElementString("fax", dataLoan.sEmployeeExternalSecondary.Fax);
            writer.WriteElementString("email", dataLoan.sEmployeeExternalSecondary.Email);
            writer.WriteElementString("pml_broker_name", dataLoan.sEmployeeExternalSecondary.PmlUserBrokerNm);

            if (dataLoan.sEmployeeExternalSecondary.IsValid &&
                IsPmlEnabled &&
                dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent)
            {
                writer.WriteElementString("show", "");
            }
            writer.WriteEndElement();
            #endregion

            #region External Post-Closer
            writer.WriteStartElement("external_post_closer");
            writer.WriteElementString("name", dataLoan.sEmployeeExternalPostCloser.FullName);
            writer.WriteElementString("phone", dataLoan.sEmployeeExternalPostCloser.Phone);
            writer.WriteElementString("fax", dataLoan.sEmployeeExternalPostCloser.Fax);
            writer.WriteElementString("email", dataLoan.sEmployeeExternalPostCloser.Email);

            if (dataLoan.sEmployeeExternalPostCloser.IsValid &&
                IsPmlEnabled &&
                dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent)
            {
                writer.WriteElementString("show", "");
            }
            writer.WriteEndElement();
            #endregion

            #region <applicant>
            writer.WriteStartElement("applicant");
            writer.WriteElementString("score_type_1", dataLoan.sCreditScoreType1_rep);
            writer.WriteElementString("score_type_2", dataLoan.sCreditScoreType2_rep);
            writer.WriteElementString("score_type_3", dataLoan.sCreditScoreType3_rep);
            writer.WriteElementString("score_modified", dataLoan.sProdIsCreditScoresAltered ? "Yes" : "No");
            writer.WriteElementString("self_employed", dataLoan.sIsSelfEmployed ? "Yes" : "No");
            writer.WriteElementString("borrower", dataApp.aBNm);
            writer.WriteElementString("borrowerssn", dataApp.aBSsn);
            writer.WriteElementString("borrowercitizenship", GetProdBCitizenDescription(dataApp.aProdBCitizenT));
            writer.WriteElementString("spouse", dataApp.aCNm);
            writer.WriteElementString("spousessn", dataApp.aCSsn);
            writer.WriteElementString("non_mortgage_payment", dataLoan.sTransmOMonPmt_rep);
            writer.WriteElementString("borr_fico", dataApp.aBFicoScore_rep); // 3/14/2014 - dd - Use by JMAC custom certificate
            writer.WriteElementString("spouse_fico", dataApp.aCFicoScore_rep); // 3/14/2014 - dd - Use by JMAC custom certificate
            string aIsBorrSpousePrimaryWageEarner = "";
            if (dataApp.aBHasSpouse) 
            {
                aIsBorrSpousePrimaryWageEarner = dataApp.aIsBorrSpousePrimaryWageEarner ? "Yes" : "No";
            }
            writer.WriteElementString("aIsBorrSpousePrimaryWageEarner", aIsBorrSpousePrimaryWageEarner);

            
            writer.WriteElementString("first_time_buyer", dataLoan.sHas1stTimeBuyer ? "Yes" : "No");
            writer.WriteElementString("total_income", dataLoan.sPrimAppTotNonspI_rep);
            writer.WriteElementString("total_income_borrower", dataApp.aBTotI_rep);
            writer.WriteElementString("total_income_coborrower", dataApp.aCTotI_rep);
            writer.WriteElementString("housing_history", dataLoan.sProdHasHousingHistory ? "Yes" : "No");
			if (broker.PmlRequireEstResidualIncome)
			{
				writer.WriteElementString("residual_income", dataLoan.sProdEstimatedResidualI_rep);
			}


            writer.WriteEndElement(); // </applicant>

            #endregion

            #region <property>
            writer.WriteStartElement("property");
            writer.WriteElementString("type", dataLoan.sProdSpT_rep);
            writer.WriteElementString("state", dataLoan.sSpState);
            writer.WriteElementString("structure_type", Tools.GetStructureTypeDescription(dataLoan.sProdSpStructureT));
            writer.WriteElementString("num_of_units", dataLoan.sUnitsNum_rep);
            writer.WriteElementString("num_of_stories", dataLoan.sProdCondoStories_rep);
            writer.WriteElementString("purpose", GetPropertyPurposeDescription(dataLoan.sOccT));

            if (broker.HasEnabledPMI )
            {
                writer.WriteElementString("additional_monthly_housing_expense", dataLoan.sAddtlMonthlyHousingExpense_rep);
            }
            else
            {
                writer.WriteElementString("property_tax", dataLoan.sProRealETx_rep);
                writer.WriteElementString("other_housing_expense", dataLoan.sMonthlyPmtLessTaxPe_rep);
            }

            writer.WriteElementString("address", dataLoan.sSpAddr);
            writer.WriteElementString("citystatezip", Tools.CombineCityStateZip(dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            writer.WriteElementString("county", dataLoan.sSpCounty);
            writer.WriteElementString("sProdIsSpInRuralArea", dataLoan.sProdIsSpInRuralArea ? "Yes" : "No");
            writer.WriteElementString("sProdIsCondotel", dataLoan.sProdIsCondotel ? "Yes" : "No");
            writer.WriteElementString("sProdIsNonwarrantableProj", dataLoan.sProdIsNonwarrantableProj ? "Yes" : "No");

            writer.WriteElementString("sOccR", dataLoan.sOccR_rep);
            writer.WriteElementString("sSpGrossRent", dataLoan.sSpGrossRent_rep);

            //av opm 47246 Update PML with Anti-flip guidelines and pricing
            if (dataLoan.sLPurposeT == E_sLPurposeT.Purchase && dataLoan.sPriorSalesD.IsValid)
            {
                writer.WriteElementString("sPriorSalesD", dataLoan.sPriorSalesD_rep);
                writer.WriteElementString("sPriorSalesPrice", dataLoan.sPriorSalesPrice_rep);
                writer.WriteElementString("sPriorSalesPropertySellerT", dataLoan.sPriorSalesPropertySellerT_rep);
            }

            writer.WriteEndElement(); // </property>
            #endregion

            #region <loan>
            string lockType = isRateLockRequested ? "Lock" : "Float";

            writer.WriteStartElement("loan");
            writer.WriteElementString("status", dataLoan.sStatusT_rep);
            if (broker.IsRateLockedAtSubmission) 
            {
                writer.WriteElementString("lock_type", lockType);
            }

            var slNm = dataLoan.sLNm;
            if(slNm.Length > 5 && dataLoan.BrokerDB.HidePmlCertSensitiveFields)
            {
                slNm = new string('*', dataLoan.sLNm.Length - 5) + dataLoan.sLNm.Substring(dataLoan.sLNm.Length-5, 5);
            }

            writer.WriteElementString("pml_loanno", slNm);

            if (null != first_sLNm && first_sLNm != "") 
            {
                writer.WriteElementString("first_sLNm", first_sLNm);
            }
            writer.WriteElementString("purpose", dataLoan.sLPurposeT_rep);
            writer.WriteElementString("house_value", dataLoan.sHouseVal_rep);
            if (dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout || dataLoan.sLPurposeT == E_sLPurposeT.Refin)
            {
                writer.WriteElementString("sPurchPrice", "N/A");
            }
            else
            {
                writer.WriteElementString("sPurchPrice", dataLoan.sPurchPrice_rep);
            }

            writer.WriteElementString("sApprVal", dataLoan.sApprVal_rep); // 3/18/2014 dd - Use By JMAC Custom Certificate
            writer.WriteElementString("orig_appr_value", dataLoan.sOriginalAppraisedValue_rep);
            writer.WriteElementString("lien_position", GetLienPositionDescription(dataLoan.sLienPosT));
            writer.WriteElementString("loan_amount", dataLoan.sLAmtCalc_rep);
            if (dataLoan.sLpProductT == E_sLpProductT.Heloc)
            {
                writer.WriteElementString("line_amount", dataLoan.sCreditLineAmt_rep);
            }
            writer.WriteElementString("final_loan_amount", dataLoan.sFinalLAmt_rep); //opm 28035 fs 03/02/09
            writer.WriteElementString("upfront_mip", dataLoan.sFfUfmipFinanced_rep); //opm 28035 fs 03/02/09
            writer.WriteElementString("type", dataLoan.sLT_rep); //opm 28035 fs 03/02/09
            writer.WriteElementString("cltv", dataLoan.sCltvR_rep);
            writer.WriteElementString("impound", dataLoan.sProdImpound ? "Yes" : "No");
            writer.WriteElementString("cashout_amount", dataLoan.sProdCashoutAmt_rep);
            writer.WriteElementString("ltv", dataLoan.sLtvR_rep);
            if ( E_sLpProductT.Heloc == dataLoan.sLpProductT
                || (E_sLienPosT.First == dataLoan.sLienPosT && E_sSubFinT.Heloc == dataLoan.sSubFinT 
                      && (dataLoan.sHas2ndFinPe || (dataLoan.sSubFinT > 0 || dataLoan.sConcurSubFin > 0))) ) 
            {
                writer.WriteElementString("hcltv", dataLoan.sHCLTVRPe_rep); // NOT sHcltvR_rep
            }

            if (dataLoan.sShowAppraisedBasedLtvsOnCert)
            {
                writer.WriteElementString("sAppraisedValueCltvR", dataLoan.sAppraisedValueCltvR_rep);
                writer.WriteElementString("sAppraisedValueLtvR", dataLoan.sAppraisedValueLtvR_rep);
            }

			writer.WriteStartElement("documentation_type");
			writer.WriteAttributeString("no_income", IsNoIncome(dataLoan.sProdDocT) ? "1" : "0");
            writer.WriteString(dataLoan.sProdDocT_rep);
			writer.WriteEndElement(); // </documentation_type>

            writer.WriteElementString("is_fha_streamline", dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance ? "1" : "0"); //opm 32180 fs 11/19/09
            writer.WriteElementString("credit_qualifying", dataLoan.sIsCreditQualifying ? "Yes" : "No" ); //opm 32180 fs 11/19/09
            writer.WriteElementString("is_va_irrrl", dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl ? "1" : "0"); //opm 28103 fs 07/04/10
            writer.WriteElementString("va_funding_fee", dataLoan.sProdVaFundingFee_rep); //opm 28103 fs 07/04/10

			writer.WriteElementString("interest_only", dataLoan.sIsIOnly ? "Yes" : "No");
			if (broker.IsOptionARMUsedInPml ) // OPM 19782.  Do not display OptionARM status if not allowed
				writer.WriteElementString("option_arm", dataLoan.sIsOptionArm ? "Yes" : "No");
            if (dataLoan.sPricingModeT == E_sPricingModeT.InternalInvestorPricing)
            {
                // OPM 69941. When in investor pricing mode, we need to show
                // investor rate lock days on the cert.
                writer.WriteElementString("rate_lock_period", dataLoan.sProdInvestorRLckdDays_rep);
                writer.WriteElementString("rate_locked", dataLoan.sIsInvestorRateLocked.ToString());
            }
            else
            {
                writer.WriteElementString("rate_lock_period", dataLoan.sProdRLckdDays_rep);
                writer.WriteElementString("rate_locked", dataLoan.sIsRateLocked.ToString());
            }

            writer.WriteElementString("rate_lock_date", DateTime.Today.Date.ToShortDateString());
            writer.WriteElementString("rate_lock_expired_date", dataLoan.CalculateRateLockExpirationDate_rep(DateTime.Today.Date.ToShortDateString(), dataLoan.sProdRLckdDays_rep));

            writer.WriteElementString("term", dataLoan.sTerm_rep);
            writer.WriteElementString("due", dataLoan.sDue_rep);
            writer.WriteElementString("estimate_closing_date", dataLoan.sEstCloseD_rep);
            writer.WriteElementString("reserves", dataLoan.sProdAvailReserveMonths_rep == "-1" ? "N/A" : dataLoan.sProdAvailReserveMonths_rep );
            string miDesc = broker.HasEnabledPMI && product != null ?
                product.MortgageInsuranceDesc
                : GetMortgageInsuranceDescription(dataLoan.sProdMIOptionT, dataLoan, broker.HasEnabledPMI);


            if (broker.IsLenderFeeBuyoutEnabledForBranchChannel(dataLoan.sBranchChannelT))
            {
                writer.WriteElementString("sLenderFeeBuyoutRequestedT", dataLoan.sLenderFeeBuyoutRequestedT.ToString());
            }

            writer.WriteElementString("mi", miDesc);

            writer.WriteElementString("mi_provider", product == null ? "": product.MortgageInsuranceProvider );

            if (broker.IsAsk3rdPartyUwResultInPml) 
            {
                writer.WriteElementString("auresponse", Tools.GetProd3rdPartyUwResultTDescription(dataLoan.sProd3rdPartyUwResultT));
                writer.WriteElementString("auresponse_modified", dataLoan.sIsProd3rdPartyUwResultTModifiedByUser ? "Yes" : "No");

				// 04/26/07 mf. OPM 12103 For MyCommunity support
//				writer.WriteElementString("auprocessing", Tools.GetsProd3rdPartyUwProcessingTDescription(dataLoan.sProd3rdPartyUwProcessingT));

                writer.WriteElementString("du_refi_plus", dataLoan.sProdIsDuRefiPlus ? "Yes" : "No");
            }

            writer.WriteElementString("has_appraisal", dataLoan.sHasAppraisal ? "Yes" : "No" );

            string prepayment_penalty = "";
            if (product != null)
            {
                // OPM 112334.
                if (product.lHardPrepmtPeriodMonths != 0)
                {
                    prepayment_penalty += product.lHardPrepmtPeriodMonths_rep + " months hard";
                }
                if (product.lSoftPrepmtPeriodMonths != 0)
                {
                    prepayment_penalty += (prepayment_penalty.Length == 0 ? "" : ", ") + product.lSoftPrepmtPeriodMonths_rep + " months soft";
                }

                if (prepayment_penalty.Length == 0) prepayment_penalty = "No Prepay";
            }
            writer.WriteElementString("prepayment_penalty", prepayment_penalty);
			// OPM 4442. 8/22/07 mf. These fields are used in PML only for stand
			// alone seconds.  We don't display if this is a first, or linked 2nd.
			if (dataLoan.sIsStandAlone2ndLien) writer.WriteElementString("stand_alone_second", "Yes" );
			writer.WriteElementString("other_neg_am", dataLoan.sLpIsNegAmortOtherLien ? "Yes" : "No" ); // OPM 4442
			writer.WriteElementString("other_fin_meth", GetFinanceMethodDescription( dataLoan.sOtherLFinMethT ) ); // OPM 4442
			writer.WriteElementString("other_pmt", dataLoan.sProOFinPmt_rep ); // OPM 4442

            writer.WriteElementString("hpml_status", dataLoan.sHighPricedMortgageT.GetDescription()); // OPM 474889

            if (dataLoan.sIsQualifiedForOriginatorCompensation)
            {
                writer.WriteElementString("comp_src", GetCompensationSourceDescription(dataLoan.sOriginatorCompensationPaymentSourceT));
                writer.WriteElementString("comp_pnt", dataLoan.sOriginatorCompPointForCert_rep);
                writer.WriteElementString("comp_amt", dataLoan.sOriginatorCompForCert_rep);
                writer.WriteElementString("comp_plan_date", dataLoan.sOriginatorCompensationEffectiveD_rep);
                writer.WriteElementString("comp_fixed", dataLoan.sOriginatorCompensationFixedAmount_rep); // 2/28/2014 - dd Add for JMAC

            }
            if (dataLoan.sIsRenovationLoan)
            {
                writer.WriteElementString("is_renovation", "Yes");

                if (dataLoan.sLT == E_sLT.FHA)
                {
                    writer.WriteElementString("as_is_appraised", dataLoan.sAsIsAppraisedValuePeval_rep);
                    writer.WriteElementString("mip_ltv", dataLoan.sFHAMipLtv_rep);
                }

                writer.WriteElementString("as_completed", dataLoan.sApprValPe_rep);
                writer.WriteElementString("total_renovation_costs", dataLoan.sTotalRenovationCosts_rep);
            }


            writer.WriteElementString("sPresLTotPersistentHExp", dataLoan.sPresLTotPersistentHExp_rep);
            writer.WriteElementString("sMaxR", dataLoan.sMaxR_rep);

            writer.WriteElementString("sQualTopR", dataLoan.sLTotI == 0 ? "N/A" : dataLoan.sQualTopR_rep);
            if (selectedRateOption == null)
            {
                writer.WriteElementString("sQualBottomR", dataLoan.sLTotI == 0 ? "N/A" : dataLoan.sQualBottomR_rep);
                writer.WriteElementString("sNoteIR", dataLoan.sNoteIR_rep);
                writer.WriteElementString("sRAdjMarginR", dataLoan.sRAdjMarginR_rep);
            }
            else
            {
                writer.WriteElementString("sQualBottomR", dataLoan.sLTotI == 0 ? "N/A" : selectedRateOption.BottomRatio_rep + "%");
                writer.WriteElementString("sNoteIR", selectedRateOption.Rate_rep);
                writer.WriteElementString("sRAdjMarginR", selectedRateOption.Margin_rep);
            }

            writer.WriteElementString("sBranchChannelT", dataLoan.sBranchChannelT.ToString("D")); // 8/3/2015 - dd - OPM 210318 - Add for MWF.
            writer.WriteElementString("clear_to_close_date", dataLoan.sClearToCloseD_rep);
            var sLoanEstimateDatesInfo = dataLoan.sLoanEstimateDatesInfo;

            if (sLoanEstimateDatesInfo != null)
            {
                var initialLoanEstimate = sLoanEstimateDatesInfo.InitialLoanEstimate;

                if (initialLoanEstimate != null)
                {
                    writer.WriteElementString("sLoanEstimateDatesInfo_InitialLoanEstimate_CreatedDate", initialLoanEstimate.CreatedDate.ToString("MM/dd/yyyy"));
                    writer.WriteElementString("sLoanEstimateDatesInfo_InitialLoanEstimate_IssuedDate", initialLoanEstimate.IssuedDate.ToString("MM/dd/yyyy"));
                    writer.WriteElementString("sLoanEstimateDatesInfo_InitialLoanEstimate_DeliveryMethod", GetDeliveryMethodDescription(initialLoanEstimate.DeliveryMethod));
                    writer.WriteElementString("sLoanEstimateDatesInfo_InitialLoanEstimate_ReceivedDate", initialLoanEstimate.ReceivedDate.ToString("MM/dd/yyyy"));
                }

                var lastDisclosedLoanEstimate = dataLoan.GetLastDisclosedLoanEstimate();

                if (lastDisclosedLoanEstimate != null)
                {
                    writer.WriteElementString("sLoanEstimateDatesInfo_LastDisclosedLoanEstimate_CreatedDate", lastDisclosedLoanEstimate.CreatedDate.ToString("MM/dd/yyyy"));
                    writer.WriteElementString("sLoanEstimateDatesInfo_LastDisclosedLoanEstimate_IssuedDate", lastDisclosedLoanEstimate.IssuedDate.ToString("MM/dd/yyyy"));
                    writer.WriteElementString("sLoanEstimateDatesInfo_LastDisclosedLoanEstimate_DeliveryMethod", GetDeliveryMethodDescription(lastDisclosedLoanEstimate.DeliveryMethod));
                    writer.WriteElementString("sLoanEstimateDatesInfo_LastDisclosedLoanEstimate_ReceivedDate", lastDisclosedLoanEstimate.ReceivedDate.ToString("MM/dd/yyyy"));
                }
            }

            var sClosingDisclosureDatesInfo = dataLoan.sClosingDisclosureDatesInfo;
            if (sClosingDisclosureDatesInfo != null)
            {
                var initialClosingDisclosure = sClosingDisclosureDatesInfo.InitialClosingDisclosure;

                if (initialClosingDisclosure != null)
                {
                    writer.WriteElementString("sClosingDisclosureDatesInfo_InitialClosingDisclosure_CreatedDate", initialClosingDisclosure.CreatedDate.ToString("MM/dd/yyyy"));
                    writer.WriteElementString("sClosingDisclosureDatesInfo_InitialClosingDisclosure_IssuedDate", initialClosingDisclosure.IssuedDate.ToString("MM/dd/yyyy"));
                    writer.WriteElementString("sClosingDisclosureDatesInfo_InitialClosingDisclosure_DeliveryMethod", GetDeliveryMethodDescription(initialClosingDisclosure.DeliveryMethod));
                    writer.WriteElementString("sClosingDisclosureDatesInfo_InitialClosingDisclosure_ReceivedDate", initialClosingDisclosure.ReceivedDate.ToString("MM/dd/yyyy"));
                }

                var finalClosingDisclosure = sClosingDisclosureDatesInfo.FinalClosingDisclosure;

                if (finalClosingDisclosure != null)
                {
                    writer.WriteElementString("sClosingDisclosureDatesInfo_FinalClosingDisclosure_CreatedDate", finalClosingDisclosure.CreatedDate.ToString("MM/dd/yyyy"));
                    writer.WriteElementString("sClosingDisclosureDatesInfo_FinalClosingDisclosure_IssuedDate", finalClosingDisclosure.IssuedDate.ToString("MM/dd/yyyy"));
                    writer.WriteElementString("sClosingDisclosureDatesInfo_FinalClosingDisclosure_DeliveryMethod", GetDeliveryMethodDescription(finalClosingDisclosure.DeliveryMethod));
                    writer.WriteElementString("sClosingDisclosureDatesInfo_FinalClosingDisclosure_ReceivedDate", finalClosingDisclosure.ReceivedDate.ToString("MM/dd/yyyy"));

                }
            }

            writer.WriteElementString("sStatusD", dataLoan.sStatusD_rep);
            writer.WriteElementString("sDocsOrderedD", dataLoan.sDocsOrderedD_rep);
            writer.WriteElementString("sDocsD", dataLoan.sDocsD_rep);
            writer.WriteElementString("sDocsBackD", dataLoan.sDocsBackD_rep);
            writer.WriteElementString("submitted_date", dataLoan.sSubmitD_rep);
            writer.WriteElementString("approved_date", dataLoan.sApprovD_rep);
            writer.WriteElementString("approved_exp_date", dataLoan.sAppExpD_rep);
            writer.WriteElementString("sConditionReviewD", dataLoan.sConditionReviewD_rep);
            writer.WriteElementString("suspended_date", dataLoan.sSuspendedD_rep);
            writer.WriteElementString("sApprRprtOd", dataLoan.sApprRprtOd_rep);
            writer.WriteElementString("sApprRprtDueD", dataLoan.sApprRprtDueD_rep);
            writer.WriteElementString("sApprRprtRd", dataLoan.sApprRprtRd_rep);
            writer.WriteElementString("sLoanSubmittedD ", dataLoan.sLoanSubmittedD_rep);


            // je 12/21/2018 - Add fields for NASA FCU.
            writer.WriteElementString("sApprRprtExpD", dataLoan.sApprRprtExpD_rep);
            writer.WriteElementString("payment_and_interest", dataLoan.sProThisMPmt_rep);

            writer.WriteEndElement(); // </loan>
            #endregion

            #region <underwriting>
            writer.WriteStartElement("underwriting");

            if (null != product) 
            {
                writer.WriteElementString("result", GetUnderwritingResultDescription(product.Status));
            } 
            else 
            {
                writer.WriteElementString("result", "ManualUnderwriting");
            }
            bool displayRateOptionForIneligible = principal.HasPermission(Permission.CanApplyForIneligibleLoanPrograms);
            if (displayRateOptionForIneligible) 
            {
                // 3/1/2005 dd - In LO LPE, user can "submit", "view more detail" on ineligible loan program with rate option.
                // Therefore when user select this option, certificate need to display Pricing and Rate Adjustment.
                writer.WriteElementString("DisplayRateOptionForIneligible", "True");
            } 
            else if (null != product && product.Status == E_EvalStatus.Eval_Ineligible && product.DisqualifiedBecauseMinMaxFeeViolation) 
            {
                // 4/1/2005 dd - OPM 1202 - Allow people to see rate options & adjustments if the loan program is disqualified due to min/max YSP
                writer.WriteElementString("DisplayRateOptionForIneligible", "True");

            }
           
            if (broker.LpeSubmitAgreement != "") 
            {
                writer.WriteElementString("confirmation_phrase", AspxTools.HtmlString(broker.LpeSubmitAgreement));
            }

            #region <message_to_underwriter>
            // 05/23/2005 - dd OPM #1934.
            if ("" != dataLoan.sLpeNotesFromBrokerToUnderwriterHistory) 
            {
                writer.WriteElementString("message_to_underwriter", LendersOffice.Common.Utilities.SafeHtmlString(dataLoan.sLpeNotesFromBrokerToUnderwriterHistory).Replace(Environment.NewLine, "<br>"));
            }
            #endregion

            
            #region <denial_reasons>
            if (null != product) 
            {
                writer.WriteStartElement("denial_reasons");

                if (product.DenialExceptions != null && product.DenialExceptions.Count(p => p.LienPosT == product.lLienPosT) != 0)
                {
                    // OPM 179060.
                    foreach (var reason in product.DenialExceptions)
                    {
                        if (reason.LienPosT == product.lLienPosT)
                        {
                            writer.WriteStartElement("denial_reason");
                            writer.WriteAttributeString("exception", reason.ExceptionReason);
                            writer.WriteString(reason.DenialReason);
                            writer.WriteEndElement(); //</denial_reason>
                        }
                    }
                }
                else
                {
                    foreach (Toolbox.CStipulation s in product.DenialReasons)
                    {
                        writer.WriteStartElement("denial_reason");
                        writer.WriteAttributeString("debug", s.DebugString);
                        writer.WriteString(s.Description);
                        writer.WriteEndElement(); //</denial_reason>
                    }

                }
                writer.WriteEndElement(); // </denial_reasons>
            }

            #endregion

            #region <stipulations>
            if (broker.IsUseNewTaskSystem)
            {
                WriteNewConditionsStips(writer, dataLoan.sLId, broker.BrokerID, isLoadStipFromCondition, product);
            }
            else
            {
                WriteConditionsStipsObsolete(writer, dataLoan.sLId, isLoadStipFromCondition, product);
            }
            #endregion

            writer.WriteEndElement(); // </underwriting>
            #endregion

            #region <pricing>
            writer.WriteStartElement("pricing");

            var priceGroup = PriceGroup.RetrieveByID(priceGroupID, broker.BrokerID);

            #region <adjustments>
            if (null != product)
            {
                writer.WriteStartElement("adjustments");
                CAdjustItem totalAdjustDesc = product.TotalAdjustDesc;
                writer.WriteElementString("total_rate", totalAdjustDesc.Rate);
                writer.WriteElementString("total_margin", totalAdjustDesc.Margin);
                writer.WriteElementString("total_fee", CAdjustItem.AdjustFeeInResult(priceGroup.DisplayPmlFeeIn100Format, totalAdjustDesc.Fee));
                writer.WriteElementString("total_qrate", totalAdjustDesc.QRate);
                writer.WriteElementString("total_teaser", totalAdjustDesc.TeaserRate);

                foreach (CAdjustItem adjItem in product.AdjustDescs)
                {
                    writer.WriteStartElement("adjustment");
                    if (!String.IsNullOrEmpty(adjItem.OptionCode))
                    {
                        writer.WriteAttributeString("perOptionAdjStr", adjItem.OptionCode);
                    }
                    writer.WriteElementString("rate", adjItem.Rate);
                    writer.WriteElementString("margin", adjItem.Margin);
                    writer.WriteElementString("fee", CAdjustItem.AdjustFeeInResult(priceGroup.DisplayPmlFeeIn100Format, adjItem.Fee));
                    writer.WriteElementString("qrate", adjItem.QRate);
                    writer.WriteElementString("teaser", adjItem.TeaserRate);
                    writer.WriteElementString("description", adjItem.Description);
                    writer.WriteElementString("debug", adjItem.DebugString);
                    writer.WriteElementString("persist", adjItem.IsPersist ? "1" : "0");
                    writer.WriteEndElement(); // </adjustment>
                }

                writer.WriteEndElement(); //</adjustments>
            }
            #endregion

            #region <rate_options>
            if (null != product) 
            {
                writer.WriteStartElement("rate_options");
				// 01/25/08 OPM 19525 mf.
				writer.WriteAttributeString("point_basis", priceGroup.DisplayPmlFeeIn100Format ? "100" : "");

				writer.WriteAttributeString("user_warn", product.GetRateLockSubmissionUserWarningMessageDisplay(priceGroup.LockPolicyID.Value, principal.LpeRsExpirationByPassPermission ));
				if (principal.HasPermission (Permission.CanModifyLoanPrograms) )
					writer.WriteAttributeString("dev_warn", product.RateLockSubmissionDevWarningMessage);

                CApplicantRateOption[] applicationRateOptions = product.ApplicantRateOptions;

                if (null != applicationRateOptions) 
                {
                    Dictionary<string, QMStatusCalculator> rateAndQmDebug = new Dictionary<string, QMStatusCalculator>();
                    // OPM 62085. So we can display the compensation in point format
                    bool isQualifiedForOriginatorComp = dataLoan.sIsQualifiedForOriginatorCompensation
                        && dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                        && dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;

                    bool hasDisquals = false;
                    bool hasAdjustments = false;
                    QMStatusCalculator calculator;
                    int rateNum = 0;
                    foreach (CApplicantRateOption rateOption in applicationRateOptions) 
                    {
                        writer.WriteStartElement("rate_option");
                        hasAdjustments = hasAdjustments || rateOption.PerOptionAdjHiddenStr.Length > 0 || rateOption.PerOptionAdjStr.Length > 0;
                        writer.WriteAttributeString("disqualified", rateOption.IsDisqualified.ToString());
                        if (rateOption.IsDisqualified)
                        {
                            hasDisquals = true;
                        }
                        writer.WriteAttributeString("perOptionAdjStr", rateOption.PerOptionAdjStr);
                        writer.WriteAttributeString("perOptionAdjHiddenStr", rateOption.PerOptionAdjHiddenStr);
                        writer.WriteAttributeString("disqualreason", rateOption.DisqualReason);
                        writer.WriteAttributeString("disqualreasondebug", rateOption.DisqualReasonDebug);
                        writer.WriteAttributeString("disable_lock", rateOption.IsDisableLock.ToString());
                        writer.WriteAttributeString("disable_lock_reason", rateOption.DisableLockReason);

                        if (string.IsNullOrEmpty(id) && string.IsNullOrEmpty(rate) == false)
                        {
                            if (rate == rateOption.Rate_rep.Replace("%", ""))
                            {
                                if (string.IsNullOrEmpty(point) == false)
                                {
                                    if (isQualifiedForOriginatorComp && point == rateOption.PointIncludingOriginatorComp_rep.Replace("%", ""))
                                    {
                                        writer.WriteAttributeString("selected", "True");
                                    }
                                    else if (point == rateOption.Point_rep.Replace("%", ""))
                                    {
                                        writer.WriteAttributeString("selected", "True");
                                    }
                                }
                                else
                                {
                                    writer.WriteAttributeString("selected", "True");
                                }
                            }
                        }
                        else if (id == rateOption.RateOptionId)
                        {
                            writer.WriteAttributeString("selected", "True");
                        }

                        #region qm data

                        writer.WriteAttributeString("rid", "r" + rateNum);

                        EmployeeDB empDB = EmployeeDB.RetrieveById(broker.BrokerID, principal.EmployeeId);
                        if (rateOption.QMData != null && (dataLoan.sBranchChannelT != E_BranchChannelT.Correspondent || broker.DisplayQMResultsForCorrespondentFiles) && (broker.ShowQMStatusInPml2 || empDB.ShowQMStatusInPml2))
                        {
                            calculator = new QMStatusCalculator(selectedParRate, rateOption.Apr_rep, rateOption.QMData, dataLoan.m_convertLos, rateOption.Rate);
                            rateAndQmDebug.Add("r"+rateNum, calculator);
                            writer.WriteStartElement("QM");
                            writer.WriteAttributeString("sQMStatusT", calculator.GetQMStatus().ToString("d"));
                            writer.WriteAttributeString("sHighPricedMortgageT", calculator.GetHPMLStatus().ToString("d"));
                            writer.WriteEndElement();
                        }
                        rateNum++;
                        #endregion

                        writer.WriteElementString("rate", rateOption.Rate_rep);
                        writer.WriteElementString("rateOptionId", rateOption.RateOptionId);
                        writer.WriteElementString("point", CApplicantRateOption.PointFeeInResult(priceGroup.DisplayPmlFeeIn100Format, rateOption.Point_rep));
                        writer.WriteElementString("apr", rateOption.Apr_rep);
                        writer.WriteElementString("margin", rateOption.Margin_rep);
                        writer.WriteElementString("payment", rateOption.FirstPmtAmt_rep);

                        if (dataLoan.sLTotI == 0 || string.IsNullOrWhiteSpace(rateOption.BottomRatio_rep) || rateOption.BottomRatio_rep == "-1")
                        {
                            writer.WriteElementString("bottom_ratio", "N/A");
                        }
                        else
                        {
                            writer.WriteElementString("bottom_ratio", rateOption.BottomRatio_rep + "%");
                        }


                        writer.WriteElementString("qualrate", product.lLpProductType == "HELOC" ? "" : rateOption.QRate_rep); // OPM 197769 - Do not display the Qual Rate for HELOCs
						writer.WriteElementString("teaserrate", rateOption.TeaserRate_rep);
                        if (priceGroup.DisplayPmlFeeIn100Format)
                        {
                            if (isQualifiedForOriginatorComp)
                                writer.WriteElementString("comp_point", rateOption.PointIncludingOriginatorCompIn100_rep);
                        }
                        else
                        {
                            if (isQualifiedForOriginatorComp)
                                writer.WriteElementString("comp_point", rateOption.PointIncludingOriginatorComp_rep);
                        }
                        writer.WriteEndElement(); // </rate_option>
                    }

                    if (hasDisquals)
                    {
                        writer.WriteElementString("hasdisquals", hasDisquals.ToString());
                    }
                    if (hasAdjustments)
                    {
                        writer.WriteElementString("hasadjustments", hasAdjustments.ToString());
                    }
                    if (rateAndQmDebug.Count > 0 )
                    {
                        //QM stuff 
                        var qmRateDebugInfo = from x in rateAndQmDebug
                                              select new
                                              {
                                                  RateId = x.Key,
                                                  QMData = new
                                                  {
                                                      sApr = x.Value.sApr_rep,
                                                      sQMParRSpreadCalcDesc = x.Value.sQMParRSpreadCalcDesc,
                                                      sQMParR = x.Value.sQMParR_rep,
                                                      sQMParRSpread = x.Value.sQMParRSpread_rep,
                                                      sQMExcessUpfrontMIP = x.Value.sQMExcessUpfrontMIP_rep,
                                                      sQMExcessUpfrontMIPCalcDesc = x.Value.sQMExcessUpfrontMIPCalcDesc,
                                                      sQMExcessDiscntFPc = x.Value.sQMExcessDiscntFPc_rep,
                                                      sQMExcessDiscntFCalcDesc = x.Value.sQMExcessDiscntFCalcDesc,
                                                      sQMLAmt = x.Value.sQMLAmt_rep,
                                                      sQMLAmtCalcDesc = x.Value.sQMLAmtCalcDesc,
                                                      sQMMaxPointAndFeesAllowedAmt = x.Value.sQMMaxPointAndFeesAllowedAmt_rep,
                                                      sQMTotFeeAmount = x.Value.sQMTotFeeAmount_rep,
                                                      sGfeOriginatorCompF = x.Value.sGfeOriginatorCompF_rep,
                                                      sQMExcessDiscntF = x.Value.sQMExcessDiscntF_rep,
                                                      sQMMaxPrePmntPenalty = x.Value.sQMMaxPrePmntPenalty_rep,
                                                      sQMTotFeeAmt = x.Value.sQMTotFeeAmt_rep,
                                                      sQMLoanPassesPointAndFeesTest = x.Value.sQMLoanPassesPointAndFeesTest,
                                                      sQMLoanDoesNotHaveNegativeAmort = x.Value.sQMLoanDoesNotHaveNegativeAmort,
                                                      sQMLoanDoesNotHaveBalloonFeature = x.Value.sQMLoanDoesNotHaveBalloonFeature,
                                                      sQMLoanDoesNotHaveAmortTermOver30Yr = x.Value.sQMLoanDoesNotHaveAmortTermOver30Yr,
                                                      sQMLoanHasDtiLessThan43Pc = x.Value.sQMLoanHasDtiLessThan43Pc,
                                                      sQMStatusT = x.Value.GetQMStatus(),
                                                      sQMAprRSpread = x.Value.sQMAprRSpread_rep,
                                                      sQMAprRSpreadCalcDesc = x.Value.sQMAprRSpreadCalcDesc,
                                                      sHighPricedMortgageT = x.Value.GetHPMLStatus()
                }
                                              };


                        StringBuilder sb = new StringBuilder("{");
                        foreach (var i in qmRateDebugInfo)
                        {
                            if (sb.Length > 1)
                            {
                                sb.Append(",");
                            }
                            sb.Append(i.RateId);
                            sb.Append(":");
                            sb.Append(ObsoleteSerializationHelper.JavascriptJsonSerialize(i.QMData));
                        }
                        sb.Append("}");


                        writer.WriteElementString("QMDebugDataJSON", sb.ToString());
                    }

                }

          

                writer.WriteEndElement(); // </rate_options>

            }
            #endregion



            #region <hiddenadjustments>
            if (null != product) 
            {
                writer.WriteStartElement("hiddendadjustments");
                foreach (CAdjustItem adjItem in product.HiddenAdjustDescs) 
                {
                    writer.WriteStartElement("adjustment");
                    if (!String.IsNullOrEmpty(adjItem.OptionCode))
                    {
                        writer.WriteAttributeString("perOptionAdjStr", adjItem.OptionCode);
                    }
                    writer.WriteElementString("rate", adjItem.Rate);
                    writer.WriteElementString("margin", adjItem.Margin);
                    writer.WriteElementString("fee", CAdjustItem.AdjustFeeInResult(priceGroup.DisplayPmlFeeIn100Format, adjItem.Fee));
                    writer.WriteElementString("qrate", adjItem.QRate);
                    writer.WriteElementString("teaser", adjItem.TeaserRate);
                    writer.WriteElementString("description", adjItem.Description);
                    writer.WriteElementString("debug", adjItem.DebugString);
                    writer.WriteElementString("persist", adjItem.IsPersist ? "1" : "0");
                    writer.WriteEndElement(); // </adjustment>
                }

                writer.WriteEndElement(); // </hiddenadjustments>
            }
            #endregion


            writer.WriteElementString("HideLOCompPMLCert", (broker.IsHideLOCompPMLCert || (!dataLoan.sHasOriginatorCompensationPlan && dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)) ? "True" : "False"); //OPM 65776
            writer.WriteElementString("HidePricingwithoutLOComp", broker.IsHidePricingwithoutLOCompPMLCert ? "True" : "False"); //OPM 65100


            writer.WriteEndElement(); // </pricing>

            #endregion

            #region <loan_product>

            writer.WriteStartElement("loan_product");
            writer.WriteElementString("name", null == product ? CPageBase.SubmitManuallyProgramName : product.lLpTemplateNm);
			if (null != product) 
			{
				writer.WriteElementString("type", GetLoanProductTypeDescription(product.lLT));
				writer.WriteElementString("term", product.lTerm_rep);
				writer.WriteElementString("due", product.lDue_rep);
				writer.WriteElementString("amort_type", GetFinanceMethodDescription(product.lFinMethT));
				writer.WriteElementString("lock_period", product.lLockedDays_rep);
				writer.WriteElementString("display_margin", product.lIsArmMarginDisplayed ? "True" : "False");

                writer.WriteElementString("investor", product.lLpInvestorNm);
                writer.WriteElementString("product_code", product.ProductCode);

                #region <interest_only>
				writer.WriteStartElement("interest_only");
				writer.WriteElementString("months", product.lIOnlyMon_rep);
				writer.WriteEndElement(); // </interest_only>

                #endregion

                #region <arm>
				writer.WriteStartElement("arm");
				writer.WriteElementString("first_adjust_cap", product.lRadj1stCapR_rep);
				writer.WriteElementString("first_change", product.lRadj1stCapMon_rep);
				writer.WriteElementString("adjust_change", product.lRAdjCapR_rep);
				writer.WriteElementString("adjust_period", product.lRAdjCapMon_rep); // 9/10/2004 dd - ???????
				writer.WriteElementString("life_cap", product.lRAdjLifeCapR_rep);
				writer.WriteElementString("base_margin", product.lRAdjMarginR_rep);
				writer.WriteElementString("floor", product.lRAdjFloorR_rep);

				writer.WriteEndElement(); // </arm>
                #endregion

                #region <apm>
				writer.WriteStartElement("apm");
				writer.WriteElementString("adjust_cap", product.lPmtAdjCapR_rep);
				writer.WriteElementString("adjust_period", product.lPmtAdjCapMon_rep);
				writer.WriteElementString("recast_period", product.lPmtAdjRecastPeriodMon_rep);
				writer.WriteElementString("recast_stop", product.lPmtAdjRecastStop_rep);
				writer.WriteElementString("max_balance", product.lPmtAdjMaxBalPc_rep);

				writer.WriteEndElement(); // </apm>
                #endregion

                #region <buydown>
				writer.WriteStartElement("buydown");

				writer.WriteStartElement("options");

				string[][] buydownRates = {
											  new string[] {product.lBuydwnMon1_rep, product.lBuydwnR1_rep},
											  new string[] {product.lBuydwnMon2_rep, product.lBuydwnR2_rep},
											  new string[] {product.lBuydwnMon3_rep, product.lBuydwnR3_rep},
											  new string[] {product.lBuydwnMon4_rep, product.lBuydwnR4_rep},
											  new string[] {product.lBuydwnMon5_rep, product.lBuydwnR5_rep},
				};

				for (int i = 0; i < buydownRates.Length; i++) 
				{
					writer.WriteStartElement("option");
					writer.WriteElementString("rate", buydownRates[i][1]);
					writer.WriteElementString("term", buydownRates[i][0]);
					writer.WriteEndElement(); // </option>
				}
				writer.WriteEndElement(); // <options>
				writer.WriteEndElement(); // </buydown>

                #endregion

                #region <features>
				writer.WriteStartElement("features");
				writer.WriteElementString("require_deposit", product.lAprIncludesReqDeposit ? "Y" : "N");
				writer.WriteElementString("demand", product.lHasDemandFeature ? "Y" : "N");
				writer.WriteElementString("variable_rate", product.lHasVarRFeature ? "Y" : "N");
				writer.WriteElementString("allow_assumption", product.lAssumeLT == E_sAssumeLT.May ? "Y" : "N");
				writer.WriteElementString("prepayment_penalty", product.lPrepmtPenaltyT == E_sPrepmtPenaltyT.May ? "Y" : "N");
				writer.WriteElementString("grace_period", product.lLateDays);

				writer.WriteEndElement(); // </features>
                #endregion

				#region <base_rates>
				DateTime baseTime = DateTime.Parse("2000-01-01 00:00:00.000"); // Default value we do not display
				writer.WriteStartElement("base_rates");
				writer.WriteAttributeString("download_start", product.lRateSheetDownloadStartD > baseTime ? Tools.GetDateTimeDescription( product.lRateSheetDownloadStartD ) : string.Empty );
				writer.WriteAttributeString("download_end", product.lRateSheetDownloadEndD > baseTime ? Tools.GetDateTimeDescription( product.lRateSheetDownloadEndD ) : string.Empty );
				foreach (IRateItem rateItem in product.GetRawRateSheetWithDeltas())
				{
					writer.WriteStartElement("ratelist");
					writer.WriteAttributeString("rate", rateItem.Rate_rep);
                    if (rateItem.IsSpecialKeyword) 
                    {
                        writer.WriteAttributeString("point", rateItem.Point_rep);
                    } 
                    else 
                    {
                        writer.WriteAttributeString("point", CApplicantRateOption.PointFeeInResult(priceGroup.DisplayPmlFeeIn100Format, rateItem.Point_rep));
                    }
					writer.WriteAttributeString("margin", rateItem.Margin_rep);
					writer.WriteAttributeString("qratebase", rateItem.QRateBase_rep);
					writer.WriteAttributeString("teaserrate", rateItem.TeaserRate_rep);
					writer.WriteEndElement(); // </ratelist>
				}
				writer.WriteEndElement(); // </base_rates>
				#endregion
			}
		writer.WriteEndElement(); // </loan_product>

            #endregion

        #region custom_pml_fields
        writer.WriteStartElement("custom_pml_fields");
        List<CustomPmlField> customPmlFields = broker.CustomPmlFieldList.FieldsSortedByRank;
        writer.WriteElementString("hide_grid", (customPmlFields.Count == 0).ToString());


        // OPM 132054 - Add Is Employee Loan to PML certificate
        writer.WriteElementString("hide_is_emp_loan", (!dataLoan.sIsEmployeeLoan).ToString());

        for (int i = 0; i < customPmlFields.Count; i++)
        {
            var field = customPmlFields[i];
            writer.WriteElementString(String.Format("hide_{0}", i), 
                (!field.IsValid || !field.DisplayFieldForLoanPurpose(dataLoan.sLPurposeT)).ToString());
            writer.WriteElementString(String.Format("description_{0}", i), field.Description);
            string val = GetCustomPmlFieldRep(dataLoan, field.KeywordNum);
            if (field.Type == E_CustomPmlFieldType.Dropdown && !String.IsNullOrEmpty(val))
            {
                val = field.GetEnumRep(val);
            }
            writer.WriteElementString(String.Format("value_{0}", i), val);
        }
        writer.WriteEndElement(); // </custom_pml_fields>
        #endregion

        #region <sPmlBroker>
        Guid sPmlBrokerId = dataLoan.sPmlBrokerId;
        if (sPmlBrokerId != Guid.Empty)
        {
            try
            {
                // dd 8/3/2015 - OPM 210318 - For MWF custom cert
                // je 12/21/2018 - OPM 476731 - Added more PML Broker fields for NASA FCU custom certs.
                PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerById(sPmlBrokerId, dataLoan.sBrokerId);

                writer.WriteStartElement("sPmlBroker");
                writer.WriteElementString("Name", pmlBroker.Name);
                writer.WriteElementString("Phone", pmlBroker.Phone);
                writer.WriteElementString("street_address", pmlBroker.Addr);
                writer.WriteElementString("citystatezip", Tools.CombineCityStateZip(pmlBroker.City, pmlBroker.State, pmlBroker.Zip));
                writer.WriteEndElement(); // </sPmlBroker>
            }
            catch (PmlBrokerNotFoundException)
            {
            }

        }
        #endregion
        #region dev_debug_info
        writer.WriteStartElement("dev_debug_info");
        writer.WriteElementString("version_info", product == null ? "" : product.FriendlyInfo);
        writer.WriteElementString("debug_info", product == null ? "" : product.DebugInfo);
        writer.WriteEndElement(); // </dev_debug_info>
        #endregion
        writer.WriteEndElement(); // </pml_cert>

        }

        #region new task
        private static void WriteNewConditionsStips(XmlWriter writer, Guid sLId, Guid brokerId, bool loadStipsFromConditions, CApplicantPrice product)
        {
            List<Task> conditions = Task.GetAllConditionsByLoanId(brokerId, sLId);

            List<Toolbox.CertConditionObsolete> visibleCertConditions;
            List<Toolbox.CertConditionObsolete> hiddenCertConditions;

            if (false == loadStipsFromConditions && null != product)
            {
                visibleCertConditions = new List<Toolbox.CertConditionObsolete>(MergeConditionsAndStips(conditions.Where(p => p.CondIsHidden == false), GetAllStips(product.Stips, false)));
                hiddenCertConditions = new List<Toolbox.CertConditionObsolete>(MergeConditionsAndStips(conditions.Where(p => p.CondIsHidden), GetAllStips(product.HiddenStips, true)));
            }
            else
            {
                visibleCertConditions = new List<Toolbox.CertConditionObsolete>();
                hiddenCertConditions = new List<Toolbox.CertConditionObsolete>();
                foreach (Task condition in conditions)
                {
                    if (condition.CondIsDeleted )
                    {
                        continue;
                    }
                    var list = condition.CondIsHidden ? hiddenCertConditions : visibleCertConditions;
                    list.Add(new Toolbox.CertConditionObsolete(condition));
                }
            }

            visibleCertConditions.Sort();
            hiddenCertConditions.Sort();


            writer.WriteStartElement("stipulations");
            writer.WriteAttributeString("version", "1");
            WriteStipulations(writer, visibleCertConditions, brokerId);
            writer.WriteEndElement();

            writer.WriteStartElement("hiddenstipulations");
            writer.WriteAttributeString("version", "1");
            WriteStipulations(writer, hiddenCertConditions, brokerId);
            writer.WriteEndElement();

        }
        private static IEnumerable<Toolbox.CertConditionObsolete> MergeConditionsAndStips(IEnumerable<Task> conditions, IEnumerable<Toolbox.CCategoryVisibilityStip> stips)
        {
            List<Toolbox.CCategoryVisibilityStip> stipWorkingSet = new List<Toolbox.CCategoryVisibilityStip>(stips);
            foreach (Task condition in conditions)
            {

                Toolbox.CCategoryVisibilityStip stip = stipWorkingSet.Where(p => p.IsHidden ==
                    condition.CondIsHidden && p.Category.Equals(condition.CondCategoryId_rep, StringComparison.OrdinalIgnoreCase) &&
                    p.Stip.DescriptionForCondition.Equals(condition.TaskSubject, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                stipWorkingSet.Remove(stip);
                if (true == condition.CondIsDeleted)
                {
                    continue;
                }
                yield return new Toolbox.CertConditionObsolete(condition, stip);

            }

            foreach (var stip in stipWorkingSet)
            {
                if (stip.Stip != null && stip.Stip.AssignedToRole.HasValue 
                    && stip.Stip.AssignedToRole.Value == E_RoleT.Consumer
                    && !ConstSite.DisplayPolicyAndRuleIdOnLpeResult)
                {
                    // 6/13/2014 gf - We are now treating all stips that are 
                    // assigned to the consumer as document requests. These 
                    // should not be rendered to the cert unless the site is
                    // configured to display debug info.
                    continue;
                }
                yield return new Toolbox.CertConditionObsolete(stip);
            }
        }

        #endregion 
        #region old task handling
        /// <summary>
        /// Writes the hidden and visible stips. If loading stips from conditions is false and there is a loan product. The procedure 
        /// merges the stips and conditions.  This may sound unintuitive but thats what OPM  46382   Allow lender to add conditions that appear on the preview/register/lock certificate?
        /// requested. I can update the parameter but i wont. In order for a stip and condition to be merged the IsHidden Category and text
        /// need to match. All unmatching stips and conditions are still displayed. If loading stips from condition the code doesnt even bother to look
        /// at the stips. 
        /// Invalid conditions are removed and their corresponding stips if applicable. The ultimate condition list are sorted based on category then text. 
        /// warning shows on top misc on bottom. 
        /// AV 2/14/11 OPM 46382 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="sLId"></param>
        /// <param name="loadStipsFromConditions"></param>
        /// <param name="product"></param>
        private static void WriteConditionsStipsObsolete(XmlWriter writer, Guid sLId, bool loadStipsFromConditions, CApplicantPrice product)
        {
            LoanConditionSetObsolete conditionSet = new LoanConditionSetObsolete();
            conditionSet.RetrieveAll(sLId, false);

            List<Toolbox.CertConditionObsolete> visibleCertConditions;
            List<Toolbox.CertConditionObsolete> hiddenCertConditions;

            if (false == loadStipsFromConditions && null != product)
            {
                List<CLoanConditionObsolete> conditions = new List<CLoanConditionObsolete>();
                foreach (CLoanConditionObsolete condition in conditionSet)
                {
                    conditions.Add(condition);
                }
                visibleCertConditions = new List<Toolbox.CertConditionObsolete>(MergeConditionsAndStips(conditions.Where(p => p.CondIsHidden == false), GetAllStips(product.Stips, false)));
                hiddenCertConditions = new List<Toolbox.CertConditionObsolete>(MergeConditionsAndStips(conditions.Where(p => p.CondIsHidden), GetAllStips(product.HiddenStips, true)));
            }
            else
            {
                visibleCertConditions = new List<Toolbox.CertConditionObsolete>();
                hiddenCertConditions = new List<Toolbox.CertConditionObsolete>();
                foreach (CLoanConditionObsolete condition in conditionSet)
                {
                    if (false == condition.CondIsValid)
                    {
                        continue;
                    }
                    var list = condition.CondIsHidden ? hiddenCertConditions : visibleCertConditions;
                    list.Add(new Toolbox.CertConditionObsolete(condition));
                }
            }

            visibleCertConditions.Sort();
            hiddenCertConditions.Sort();

            writer.WriteStartElement("stipulations");
            writer.WriteAttributeString("version", "1");
            WriteStipulations(writer, visibleCertConditions, Guid.Empty);
            writer.WriteEndElement();

            writer.WriteStartElement("hiddenstipulations");
            writer.WriteAttributeString("version", "1");
            WriteStipulations(writer, hiddenCertConditions, Guid.Empty);
            writer.WriteEndElement();

        }

        private static IEnumerable<Toolbox.CertConditionObsolete> MergeConditionsAndStips(IEnumerable<CLoanConditionObsolete> conditions, IEnumerable<Toolbox.CCategoryVisibilityStip> stips)
        {
            List<Toolbox.CCategoryVisibilityStip> stipWorkingSet = new List<Toolbox.CCategoryVisibilityStip>(stips);
            foreach (CLoanConditionObsolete condition in conditions)
            {
                
                Toolbox.CCategoryVisibilityStip stip = stipWorkingSet.Where(p => p.IsHidden ==
                    condition.CondIsHidden && p.Category.Equals(condition.CondCategoryDesc, StringComparison.InvariantCultureIgnoreCase) &&
                    p.Stip.DescriptionForCondition.Equals(condition.CondDesc, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
                stipWorkingSet.Remove(stip);
                if (false == condition.CondIsValid)
                {
                    continue;
                }
                yield return new Toolbox.CertConditionObsolete(condition, stip);
                
            }

            foreach (var stip in stipWorkingSet)
            {
                yield return new Toolbox.CertConditionObsolete(stip);
            }
        }
        private static IEnumerable<Toolbox.CCategoryVisibilityStip> GetAllStips(Toolbox.CSortedListOfGroups group, bool areHidden)
        {

            //Create a full list of stipulations that we can remove items from as we match them to conditions
            foreach (string category in group.Keys)
            {
                foreach (Toolbox.CStipulation stip in group.GetGroupByKeyAndSort(category))
                {
                    yield return
                        new Toolbox.CCategoryVisibilityStip()
                        {
                            Category = category,
                            Stip = stip,
                            IsHidden = areHidden
                        };
                }
            }
        }



        private static void WriteStipulations(XmlWriter writer, IEnumerable<Toolbox.CertConditionObsolete> certConditions, Guid brokerId)
        {
            if (brokerId != Guid.Empty)
            {
                BrokerDB brokerDB = BrokerDB.RetrieveById(brokerId);
                if (brokerDB.CustomerCode == "PML0229")
                {
                    // JMAC always order stipulation by category then name.
                    certConditions = certConditions.OrderBy(o => o.Category).ThenBy(o => o.Text);
                }
            }
            int count = 1;
            foreach (Toolbox.CertConditionObsolete certCondition in certConditions)
            {
                WriteStipulation(writer, certCondition, count);
                count++;
            }
        }

        private static void WriteStipulation(XmlWriter writer, Toolbox.CertConditionObsolete condition, int index)
        {
            writer.WriteStartElement("stipulation");
            writer.WriteAttributeString("category", condition.Category);
            writer.WriteAttributeString("sort_num", index.ToString());
            if (false == string.IsNullOrEmpty(condition.DateDone))
            {
                writer.WriteAttributeString("donedate", condition.DateDone);
            }
            writer.WriteAttributeString("debug", condition.DebugString);
            writer.WriteString(condition.Text);
            writer.WriteEndElement();
        }
#endregion 


        private static string GetCustomPmlFieldRep(CPageData dataLoan, int keyword)
        {
            switch(keyword)
            {
                case 1:
                    return dataLoan.sCustomPMLField1_rep;
                case 2:
                    return dataLoan.sCustomPMLField2_rep;
                case 3:
                    return dataLoan.sCustomPMLField3_rep;
                case 4:
                    return dataLoan.sCustomPMLField4_rep;
                case 5:
                    return dataLoan.sCustomPMLField5_rep;
                case 6:
                    return dataLoan.sCustomPMLField6_rep;
                case 7:
                    return dataLoan.sCustomPMLField7_rep;
                case 8:
                    return dataLoan.sCustomPMLField8_rep;
                case 9:
                    return dataLoan.sCustomPMLField9_rep;
                case 10:
                    return dataLoan.sCustomPMLField10_rep;
                case 11:
                    return dataLoan.sCustomPMLField11_rep;
                case 12:
                    return dataLoan.sCustomPMLField12_rep;
                case 13:
                    return dataLoan.sCustomPMLField13_rep;
                case 14:
                    return dataLoan.sCustomPMLField14_rep;
                case 15:
                    return dataLoan.sCustomPMLField15_rep;
                case 16:
                    return dataLoan.sCustomPMLField16_rep;
                case 17:
                    return dataLoan.sCustomPMLField17_rep;
                case 18:
                    return dataLoan.sCustomPMLField18_rep;
                case 19:
                    return dataLoan.sCustomPMLField19_rep;
                case 20:
                    return dataLoan.sCustomPMLField20_rep;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Trying to access custom pml field that isn't supported.");
            }
        }

		private static bool IsNoIncome( E_sProdDocT sProdDocT )
		{
			switch ( sProdDocT )
			{
				case E_sProdDocT.NINA:
				case E_sProdDocT.NIVA:
				case E_sProdDocT.NINANE:
				case E_sProdDocT.NIVANE:
				case E_sProdDocT.NISA:
                case E_sProdDocT.DebtServiceCoverage:
                case E_sProdDocT.NoIncome:
					return true;
			}
			return false;
		}

        #region Mapper methods
        private static string GetDeliveryMethodDescription(E_DeliveryMethodT deliveryMethod)
        {
            switch (deliveryMethod)
            {
                case E_DeliveryMethodT.Email:
                    return "Email";
                case E_DeliveryMethodT.Fax:
                    return "Fax";
                case E_DeliveryMethodT.InPerson:
                    return "In Person";
                case E_DeliveryMethodT.LeaveEmpty:
                    return string.Empty;
                case E_DeliveryMethodT.Mail:
                    return "Mail";
                case E_DeliveryMethodT.Overnight:
                    return "Overnight";
                default:
                    throw new UnhandledEnumException(deliveryMethod);
            }
        }

        private static string GetProdBCitizenDescription(E_aProdCitizenT aProdCitizenT) 
        {
            switch (aProdCitizenT) 
            {
                case E_aProdCitizenT.ForeignNational: return "Non-Resident Alien (Foreign National)"; 
                case E_aProdCitizenT.NonpermanentResident: return "Non-permanent Resident";
                case E_aProdCitizenT.PermanentResident: return "Permanent Resident";
                case E_aProdCitizenT.USCitizen: return "US Citizen";
                default:
                    return "";
            }
        }
        private static string GetLPurposeDescription(E_sLPurposeT sLPurposeT) 
        {
            switch (sLPurposeT) 
            {
                case E_sLPurposeT.Construct: return "Construct";
                case E_sLPurposeT.ConstructPerm: return "Construct Perm";
                case E_sLPurposeT.Other: return "Other";
                case E_sLPurposeT.Purchase: return "Purchase";
                case E_sLPurposeT.Refin: return "Refin";
                case E_sLPurposeT.RefinCashout: return "Refin Cashout";
                case E_sLPurposeT.FhaStreamlinedRefinance: return "FHA Streamlined Refi";
                case E_sLPurposeT.VaIrrrl: return "VA IRRRL";
                case E_sLPurposeT.HomeEquity: return "Home Equity";
                default:
                    return "";
            }
        }

        private static string GetPropertyPurposeDescription(E_sOccT sOccT) 
        {
            switch (sOccT) 
            {
                case E_sOccT.Investment: return "Investment"; 
                case E_sOccT.PrimaryResidence: return "Primary Residence"; 
                case E_sOccT.SecondaryResidence: return "Secondary Residence"; 
            }
            return "";
        }

        private static string GetLienPositionDescription(E_sLienPosT sLienPosT) 
        {
            switch (sLienPosT) 
            {
                case E_sLienPosT.First: return "First Lien"; 
                case E_sLienPosT.Second: return "Second Lien"; 
            }
            return "";
        }

        private static string GetUnderwritingResultDescription (E_EvalStatus evalStatus) 
        {
            switch (evalStatus) 
            {
                case E_EvalStatus.Eval_Eligible: return "Eligible";
                case E_EvalStatus.Eval_Ineligible: return "Ineligible";
                case E_EvalStatus.Eval_InsufficientInfo: return "InsufficientInfo";

            }
            return "";
        }

        private static string GetLoanProductTypeDescription(E_sLT sLT) 
        {
            switch (sLT) 
            {
                case E_sLT.Conventional:
                    return"Conventional";
                case E_sLT.FHA:
                    return"FHA";
                case E_sLT.UsdaRural:
                    return "USDA/Rural Housing";
                case E_sLT.Other:
                    return"Other";
                case E_sLT.VA:
                    return"VA";
            }
            return "";
        }
        private static string GetFinanceMethodDescription(E_sFinMethT sFinMethT) 
        {
            switch (sFinMethT) 
            {
                case E_sFinMethT.Fixed:
                    return "Fixed"; 
                case E_sFinMethT.ARM:
                    return "ARM";
                case E_sFinMethT.Graduated:
                    return "Graduated";
            }
            return "";
        }
		private static string GetMortgageInsuranceDescription(E_sProdMIOptionT sProdMIOptionT, CPageData dataLoan, bool HasEnabledPMI)
		{
			
            switch (dataLoan.sLT)
            {
                case E_sLT.FHA:
                    return "FHA";
                case E_sLT.VA:
                    return "VA";
                case E_sLT.UsdaRural:
                    return "USDA/Rural Housing";
                case E_sLT.Conventional:
                case E_sLT.Other:
                default:
                    if (HasEnabledPMI)
                    {
                        return dataLoan.sProdConvMIOptionT_rep;
                    }
                    else
                    {
                        switch (dataLoan.sProdMIOptionT)
                        {
                            case E_sProdMIOptionT.BorrowerPdPmi:
                                return "Borrower Paid";
                            case E_sProdMIOptionT.NoPmi:
                                return "No MI / LPMI / Other";
                            case E_sProdMIOptionT.LeaveBlank:
                                return "N/A";
                            default:
                                throw new UnhandledEnumException(dataLoan.sProdMIOptionT);
                        }
                    }
            }
		}

        private static string GetCompensationSourceDescription(E_sOriginatorCompensationPaymentSourceT sOriginatorCompensationPaymentSourceT)
        {
            switch (sOriginatorCompensationPaymentSourceT)
            {
                case E_sOriginatorCompensationPaymentSourceT.SourceNotSpecified:
                    return "Not Specified";
                case E_sOriginatorCompensationPaymentSourceT.BorrowerPaid:
                    return "Borrower";
                case E_sOriginatorCompensationPaymentSourceT.LenderPaid:
                    return "Lender";
                default:
                    throw new UnhandledEnumException(sOriginatorCompensationPaymentSourceT);
            }
        }
        #endregion
        

    }
}