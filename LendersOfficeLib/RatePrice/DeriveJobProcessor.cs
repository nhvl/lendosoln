﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using LendersOffice.Common;
using LendersOffice.Email;
using System.Collections;
using System.Data;
using DataAccess;
using System.Xml;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Admin;
using System.Diagnostics;


// 4/23/07 mf. OPM 11856. We see derivation errors when too many SREs are
// engaging in price-edit activities.  We want to serialize derivations.
// When a SAE derives a product now, we create a job and place it in a queue.
//
// 08/06/07 mf. OPM 17199. We need to allow SAEs to set programs as enabled
// upon derival as well.
//
// 05/01/2015 dd - Convert the process to continuous process.

namespace LendersOffice.RatePrice
{
    /// <summary>
    /// 5/1/2015 dd - Convert this class to continuous process. Only one server (and it should be the same server running Lpeupload/LpeDownload).
    /// The old code does not implement to be run on multiple servers.
    /// </summary>
    public class DeriveJobProcessor : CommonProjectLib.Runnable.ICancellableRunnable
    {
        // Wait time between individual derivations.  For GC, etc.
        const int THREAD_WAIT_SECONDS = 15;

        // Longest amount of time we should be deriving without a break. After this
        // period passes, we should sleep the thread for the period of idle
        // time needed by the snapshot creation component.
        const int MAX_CONTINUOUS_ACTIVITY_TIME_SECONDS = 600; //10 Minutes

        /// <summary>
        /// Gets the description of the continuous process.
        /// </summary>
        /// <value>The description of the continuous process.</value>
        public string Description
        {
            get { return "SAE Derive Job Processor."; }
        }

        public void Run(CancellationToken cancellationToken)
        {
            Tools.ResetLogCorrelationId();

            try
            {
                // Basically we keep deriving at the interval of THREAD_WAIT unless
                // a period of time greater than MAX_CONTINUOUS_ACTIVITY_TIME has passed
                // without us idling for at least WAIT_FOR_SNAPSHOT.  In that situation,
                // we sleep this thread for the time needed to be idle for WAIT_FOR_SNAPSHOT
                DateTime deriveSessionStart = DateTime.Now;

                while ((DateTime.Now - deriveSessionStart) < TimeSpan.FromSeconds(MAX_CONTINUOUS_ACTIVITY_TIME_SECONDS) &&
                       !cancellationToken.IsCancellationRequested)
                {
                    // Get latest job and derive.
                    // We cannot do this in a tight atomic transaction because it
                    // can be slow and would likely cause DB lock issues. Only one thread should be working.

                    // Get the next job based on Status->Priority->SubmissionDate
                    // and set as InProgress.

                    Tools.ResetLogCorrelationId();
                    DerivationJob latestJob = DerivationJob.GetNextDerivationJob();

                    if (latestJob != null)
                    {
                        // Perform derivation, log, delete from queue, email upon success
                        try
                        {
                            Tools.LogInfo("DeriveJobProcessor", string.Format("<DeriveJobProcessor> {0} has been started.", latestJob.FriendlyDescription));

                            Stopwatch sw = Stopwatch.StartNew();
                            latestJob.PerformDerivation();
                            sw.Stop();

                            Tools.LogInfo("DeriveJobProcessor", string.Format("<DeriveJobProcessor> {0} has completed in {1} ms.", latestJob.FriendlyDescription, sw.ElapsedMilliseconds));
                        }
                        catch (Exception e)
                        {
                            e.Data.Add("JobLabel", latestJob.JobUserLabel);
                            throw;
                        }

                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(TimeSpan.FromSeconds(THREAD_WAIT_SECONDS));
                    }
                    else
                    {
                        // 5/1/2015 dd - No more job pending. Exit loop.
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                Tools.LogErrorWithCriticalTracking("Error in DeriveJobProcessor", e);
                throw;
            }
        }
    }

    public class DerivationJob
    {
        private bool m_inDb;
        private int m_jobId;
        private string m_jobXmlContent;
        private string m_jobStatus;
        private string m_jobPriority;
        private DateTime m_jobSubmitD;
        private string m_jobNotifyEmailAddr;
        private string m_jobUserNotes;
        private string m_userLoginNm;
        private string m_jobUserLabel;
        private string m_targetFolderPath;
        private string m_sourcePathList;
        private Guid m_baseBrokerId;

        public string JobPriority
        {
            get { return m_jobPriority; }
        }
        public string JobNotifyEmailAddr
        {
            get { return m_jobNotifyEmailAddr; }
        }
        public string JobUserNotes
        {
            get { return m_jobUserNotes; }
        }
        public string JobUserLabel
        {
            get { return m_jobUserLabel; }
        }
        public string UserLoginNm
        {
            get { return m_userLoginNm; }
        }

        public string FriendlyDescription
        {
            get
            {
                return string.Format("Derivation {0}:{1}{2}",
                    this.UserLoginNm,
                    this.JobUserLabel,
                    this.JobNotifyEmailAddr.Length > 0 ? (" for " + this.JobNotifyEmailAddr) : string.Empty);
            }
        }

        private DerivationJob(DataRow job)
        {
            m_jobId = int.Parse(job["JobId"].ToString());
            m_jobXmlContent = job["JobXmlContent"].ToString();
            m_jobStatus = job["JobStatus"].ToString();
            m_jobPriority = job["JobPriority"].ToString();
            m_jobSubmitD = (DateTime)job["JobSubmitD"];
            m_jobNotifyEmailAddr = job["JobNotifyEmailAddr"].ToString();
            m_jobUserNotes = job["JobUserNotes"].ToString();
            m_userLoginNm = job["UserLoginNm"].ToString();
            m_jobUserLabel = job["JobUserLabel"].ToString();

            m_inDb = true;
        }

        // 4/23/07 mf. Perform the derivation and email upon sucess
        public bool PerformDerivation()
        {
            if (m_inDb)
            {
                // The status of the current job should be "InProgress" already, so perform
                // derivation.  From here, the result should be either:
                // 1. Derivation success => Delete the row
                // 2. Derivation failure => Set JobStatus set to Error

                try
                {
                    Derive();
                }
                catch (Exception e)
                {
                    SetDeriveErrorStatus(e);
                    throw e;
                }

                // If we made it here, the derive was successful. Send email if 
                // there was a notification address, then delete the row.
                try
                {
                    // Okay (but still not good) if we fail to send.  We still need to clean up
                    SendSuccessEmail();
                }
                catch (Exception e)
                {
                    Tools.LogError("Fail to notify derivation completion: " + m_jobNotifyEmailAddr + ":" + m_jobUserLabel, e);
                }

                DerivationJob.DeleteJob(m_jobId, true);

                return true;
            }
            else
                Tools.LogBug("Attempting to PerformDerivation() when a job is not in db."); // Programmer error: called twice

            return false;
        }

        // 4/23/07 mf. Set the job status to error and update content will error
        // information
        private void SetDeriveErrorStatus(Exception e)
        {

            XmlDocument doc = Tools.CreateXmlDoc(m_jobXmlContent);
            XmlNode errorList = doc.SelectSingleNode("//DeriveJob/ErrorList");
            if (errorList == null)
            {
                XmlNode root = doc.SelectSingleNode("//DeriveJob");
                errorList = doc.CreateElement("ErrorList");
                root.AppendChild(errorList);
            }

            XmlElement errorEl = doc.CreateElement("Error");
            errorList.AppendChild(errorEl);
            if (e is CBaseException)
            {
                errorEl.SetAttribute("Message", ((CBaseException)e).UserMessage);
            }
            else
            {
                errorEl.SetAttribute("Message", e.Message);
            }
            errorEl.SetAttribute("Time", DateTime.Now.ToString());

            m_jobXmlContent = doc.OuterXml;
            UpdateDeriveJob("Error", true);
        }

        // 4/23/07 mf. Update the job in the DB.
        private int UpdateDeriveJob(string status, bool updateXml)
        {
            List<SqlParameter> parameters = new List<SqlParameter>(3);
            parameters.Add(new SqlParameter("@JobId", m_jobId));
            parameters.Add(new SqlParameter("@JobStatus", status));
            if (updateXml)
            {
                parameters.Add(new SqlParameter("@JobXmlContent", m_jobXmlContent));
            }

            return StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "UpdateDeriveJob", 3, parameters);
        }

        // 4/23/07 mf. Send the success email.
        private void SendSuccessEmail()
        {
            if (m_jobNotifyEmailAddr != string.Empty)
            {
                string body =
                    String.Format(
                        "Derivation job {1} submitted at {2} for {3} has completed.{0}{0}Notes:{0}{4}{0}{0}Target Folder:{0}{5}{0}{0}Derived Programs:{0}{6}"
                    , Environment.NewLine      //0
                    , m_jobUserLabel           //1
                    , m_jobSubmitD.ToString()  //2
                    , m_userLoginNm            //3
                    , m_jobUserNotes           //4
                    , m_targetFolderPath       //5
                    , m_sourcePathList         //6
                    );

                CBaseEmail cbe = new CBaseEmail(m_baseBrokerId)
                {
                    DisclaimerType = E_DisclaimerType.NORMAL,
                    From = "Do_Not_Reply@meridianlink.com",
                    To = m_jobNotifyEmailAddr,
                    Subject = "Program Derive: " + m_jobUserLabel,
                    Message = body,
                    IsHtmlEmail = false
                };
                cbe.Send();
            }
        }

        // 4/23/07 mf. Basically this is a copy of original derivation code from InsertExternalProduct
        // that uses the job XML.  This can still be slow.
        private void Derive()
        {
            // Get the data from the XML: folder/program list, target, broker.
            XmlDocument doc = Tools.CreateXmlDoc(m_jobXmlContent);
            XmlNode root = doc.SelectSingleNode("//DeriveJob");
            m_baseBrokerId = new Guid(root.Attributes["BaseBrokerId"].Value);
            Guid targetBrokerId = new Guid(root.Attributes["TargetBrokerId"].Value);
            Guid targetId = new Guid(root.Attributes["TargetId"].Value);
            bool enableResult = root.Attributes["EnableAfterDerive"].Value.ToLower() == "true";

            var independentAttrib = root.Attributes["IndependentAfterCreate"];
            bool becomeIndependent = independentAttrib != null && independentAttrib.Value.ToLower() == "true";

            XmlNodeList nodeList = doc.SelectNodes("//DeriveJob/Program");
            ArrayList selectedItems = new ArrayList(nodeList.Count);

            LoanProductSet source = new LoanProductSet();
            source.Retrieve(m_baseBrokerId, true);

            // We perform safety checks here to save us from more ugly errors later.
            // Make sure the selected programs/folders and target folder still exist.
            // We should not derive from a different state than the job creator saw.

            if (targetId != Guid.Empty)
            {
                CheckTargetFolderExists(targetBrokerId, targetId);
            }

            foreach (XmlElement programNode in nodeList)
            {
                Guid programId = new Guid(programNode.GetAttribute("Id"));

                if (source[programId] == null)
                {
                    // TOREVISIT: We could have much better error reporting here if we stored
                    // the program and folder names in the xml.  SREs could know exactly what
                    // changed, but I would imagine this is too rare a case to increase our
                    // xml size by that much.
                    throw new CBaseException("Program or Folder " + programId.ToString() + " does not exist.", "Program or Folder " + programId.ToString() + " does not exist.");
                }

                selectedItems.Add(programId);
            }

            // Load up current folder structure for Broker who we derive
            // from.

            ArrayList programNodes = new ArrayList(nodeList.Count);
            try
            {
                foreach (Guid checkedId in selectedItems)
                {
                    LoanProductNode lpP, lpN = source[checkedId];

                    for (lpP = source[lpN.Parent]; lpP != null; lpP = source[lpP.Parent])
                    {
                        if (selectedItems.Contains(lpP.Id) == true)
                        {
                            break;
                        }
                    }

                    if (lpP == null)
                    {
                        programNodes.Add(lpN);
                    }
                }
            }
            catch (Exception e)
            {
                throw new CBaseException("Failed to load all selected programs/folders.", e);
            }

            bool successfullyCreated = false;
            // 08/06/07 mf. OPM 17199
            // We need to allow SAEs to set programs as active upon derival.
            // So we will need to now keep track of the Ids of the child
            // programs created.
            //
            // TODO: A long term better solution for this is to save a mapping
            // of base programs and the child we just derived from them.  
            // This change might also allow us to spread smarter here.
            // (We would only spread to the new child, instead of all children
            // of the base program.)
            ArrayList newProgramIds = new ArrayList(programNodes.Count);
            for (int r = 1; r <= 3; ++r)
            {
                try
                {
                    if (!successfullyCreated)  // Try to avoid triple create while maintaining original error handling.
                    {
                        using (CStoredProcedureExec spExec = new CStoredProcedureExec(DataSrc.LpeSrc))
                        {
                            spExec.BeginTransactionForWrite();

                            try
                            {
                                foreach (LoanProductNode lpN in programNodes)
                                {
                                    source.Derive_CalledFromInsertExternalProduct
                                        (lpN
                                        , true
                                        , targetId
                                        , targetBrokerId
                                        , newProgramIds
                                        , spExec
                                        , becomeIndependent
                                        );
                                }
                                successfullyCreated = true;
                            }
                            catch
                            {
                                // D'oh!

                                spExec.RollbackTransaction();
                                newProgramIds.Clear();

                                throw;
                            }

                            spExec.CommitTransaction();
                        }
                    }

                    foreach (LoanProductNode lpN in programNodes)
                    {
                        try
                        {
                            source.Spread
                                (lpN
                                , targetBrokerId
                                , becomeIndependent
                                );
                        }
                        catch (Exception e)
                        {
                            // OPM 17199 mf
                            // Spreading has failed for at least one product.
                            // We cannot allow auto-enabling of this batch lest incompletly
                            // derived programs appear in pricing results.
                            enableResult = false;

                            Tools.LogErrorWithCriticalTracking(String.Format
                                ("Failed spreading {0} {1} :: {2}."
                                , lpN.Type
                                , lpN.Name
                                , lpN.Id
                                )
                                , e
                                );
                        }
                    }

                    // Derivation complete.  For the email, we list the path of everything
                    if (m_jobNotifyEmailAddr != string.Empty)
                    {
                        try
                        {
                            LendersOffice.Admin.BrokerDB broker = LendersOffice.Admin.BrokerDB.RetrieveById(m_baseBrokerId);

                            System.Text.StringBuilder sourcePathList = new System.Text.StringBuilder();
                            foreach (LoanProductNode lpN in programNodes)
                            {
                                GetNodePath(lpN, source, sourcePathList, broker.Name);
                            }
                            m_sourcePathList = sourcePathList.ToString();

                            m_targetFolderPath = GetFolderPath(targetId, targetBrokerId);
                        }
                        catch (Exception e)
                        {
                            // It is okay if this happens.  Not a derivation-related exception
                            Tools.LogError("Error processing derivation paths for notify email", e);
                        }
                    }

                    // OPM 17199 mf.
                    // If we want the result to be enabled, we have to manually set here.
                    // We cannot do it above because we would not know if the derviation
                    // was successful.
                    if (enableResult)
                    {
                        foreach (Guid programId in newProgramIds)
                        {
                            try
                            {
                                CLoanProductData data = new CLoanProductData(programId);
                                data.InitSave();

                                data.IsEnabledOverrideBit = true;
                                data.IsEnabled = true;

                                data.SuppressEventHandlerCall = true;
                                data.Save();
                            }
                            catch (Exception e)
                            {
                                // Failure here should not trigger the retry mechanism.
                                Tools.LogError("Error setting derived program active.", e);
                            }
                        }

                    }

                    break;
                }
                catch (CBaseException e)
                {
                    // Oops!

                    Tools.LogError("Failed deriving products.", e);

                    break;
                }
                catch (Exception e)
                {
                    // Oops!

                    Tools.LogError("Failed deriving products.", e);

                    if (r == 3)
                    {
                        Tools.LogError("Unable to derive from selected base after retries.", e);
                        throw e;
                    }
                }
            }
        }

        public static void CheckTargetFolderExists(Guid targetBrokerId, Guid targetId)
        {
            var listParams = new SqlParameter[] { new SqlParameter("@folderId", targetId), new SqlParameter("@brokerId", targetBrokerId) };
            string sql = "SELECT FolderName from LOAN_PRODUCT_FOLDER where FolderId = @folderId AND BrokerId = @brokerId AND IsLpe = 1";

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (!reader.Read())
                    throw new CBaseException("Target folder " + targetId.ToString() + " does not exist.", "Target folder " + targetId.ToString() + " does not exist.");
            };

            DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sql, null, listParams, readHandler);
        }

        // 4/23/07 mf. Get the path of given node.  For use in reporting email.
        private void GetNodePath(LoanProductNode node, LoanProductSet source, System.Text.StringBuilder pathList, string brokerName)
        {
            System.Text.StringBuilder path = new System.Text.StringBuilder();
            path.Append(node.Name);
            if (node is LoanProductContainer)
                path.Append("\\");

            path.Append(Environment.NewLine);
            LoanProductNode pathNode = source[node.Parent];
            while (null != pathNode)
            {
                path.Insert(0, pathNode.Name + "\\");
                pathNode = source[pathNode.Parent];
            }
            path.Insert(0, brokerName + "\\");
            pathList.Append(path.ToString());

        }

        // 4/23/07 mf. Get the folder path of a given folder program folder
        private string GetFolderPath(Guid folderId, Guid brokerId)
        {
			var listParams = new SqlParameter[] { new SqlParameter("@inputFolderId", folderId), new SqlParameter("@brokerId", brokerId) };
			string sql = @"declare @sPath varchar(200)
				declare @folderId uniqueIdentifier
				set @folderId = @inputFolderId
				set @sPath = ''
				while ( @folderId is not null and @folderId != '00000000-0000-0000-0000-000000000000')
				begin
				  set @sPath = (select FolderName from LOAN_PRODUCT_FOLDER where FolderId = @FolderId and BrokerId = @brokerId) + '\' + @sPath
				  set @folderId = (select ParentFolderId from Loan_Product_Folder where FolderId = @FolderId  and BrokerId = @brokerId)
				end
				select @sPath as FolderPath";

			string brokerName = BrokerDbLite.RetrieveById(brokerId).BrokerNm;

			Action<IDataReader> readHandler = delegate(IDataReader reader)
			{
				if (reader.Read() && reader["FolderPath"] != DBNull.Value)
				{
					brokerName += "\\" + reader["FolderPath"].ToString();
				}
				else
				{
					brokerName = string.Empty;
				}
			};

			DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sql, null, listParams, readHandler);

			return brokerName;
		}


		// 4/23/07 mf. Get next job from queue.  Return null if no jobs are pending
		public static DerivationJob GetNextDerivationJob()
        {
            // If there is a job with InProgress status (probably because we were reset 
            // before finishing a job), take it.
            // Otherwise, we take the highest priority with the earliest request date.

            DataSet ds = new DataSet();
            DataSetHelper.FillWithRetries(ds, DataSrc.LOTransient, "GetDeriveJob", 3, null);
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return new DerivationJob(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null; // No job to return
            }
        }

        // 4/23/07 mf. Delete a job from the queue.
        public static void DeleteJob(int id, bool allowDeleteInProgress)
        {
            // If request comes from UI, do not allow deleting jobs that are marked as
            // "InProgress".  This can only be done when it comes from the derivation
            // processor.
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient
                , "DeleteDeriveJob"
                , 3
                , new SqlParameter("@JobId", id)
                , new SqlParameter("@AllowDeleteInProgress", allowDeleteInProgress));
        }

        // 4/23/07 mf. Set an Errored job to Pending.  Note that the job's
        // priority will be unchanged.
        public static void EnqueueJob(int id)
        {
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient
                , "UpdateDeriveJob"
                , 3
                , new SqlParameter("@JobId", id)
                , new SqlParameter("@JobStatus", "Pending"));
        }
    }

    // 4/23/07 mf. Build a Derivation job to store in DB Queue
    public class DerivationJobBuilder
    {
        private bool m_inDb;
        private string m_jobXmlContent;
        private string m_jobPriority;
        private string m_jobNotifyEmailAddr;
        private string m_jobUserNotes;
        private string m_userLoginNm;
        private string m_jobUserLabel;


        public string JobPriority
        {
            get { return m_jobPriority; }
            set { m_jobPriority = value; }
        }
        public string JobNotifyEmailAddr
        {
            get { return m_jobNotifyEmailAddr; }
            set { m_jobNotifyEmailAddr = value; }
        }
        public string JobUserNotes
        {
            get { return m_jobUserNotes; }
            set { m_jobUserNotes = value; }
        }
        public string JobUserLabel
        {
            get { return m_jobUserLabel; }
            set { m_jobUserLabel = value; }
        }
        public string UserLoginNm
        {
            get { return m_userLoginNm; }
            set { m_userLoginNm = value; }
        }

        public DateTime? JobStartAfterD  { get; set; } = null;

        public void SendToQueue()
        {
            if (!m_inDb)
            {
                StoredProcedureHelper.ExecuteNonQuery(
                    DataSrc.LOTransient
                    , "InsertDeriveJob"
                    , 3
                    , new SqlParameter("@JobXmlContent", m_jobXmlContent)
                    , new SqlParameter("@JobPriority", m_jobPriority)
                    , new SqlParameter("@JobNotifyEmailAddr", m_jobNotifyEmailAddr)
                    , new SqlParameter("@JobUserLabel", m_jobUserLabel)
                    , new SqlParameter("@JobUserNotes", m_jobUserNotes)
                    , new SqlParameter("@UserLoginNm", m_userLoginNm)
                    , new SqlParameter("@JobStartAfterD", JobStartAfterD)
                    );
                m_inDb = true;
            }
            else
                Tools.LogBug("Programmer Error in DerivationJob.  Trying to add item to job list more than once.");
        }

        public DerivationJobBuilder(ArrayList programsFromUI, Guid targetId, Guid targetBrokerId, Guid baseBrokerId, bool enableAfterDerive, bool independentAfterCreate = false)
        {
            XmlDocument doc = new XmlDocument();

            XmlElement rootElement = doc.CreateElement("DeriveJob");
            doc.AppendChild(rootElement);

            rootElement.SetAttribute("TargetId", targetId.ToString());
            rootElement.SetAttribute("TargetBrokerId", targetBrokerId.ToString());
            rootElement.SetAttribute("BaseBrokerId", baseBrokerId.ToString());
            rootElement.SetAttribute("EnableAfterDerive", enableAfterDerive.ToString());
            rootElement.SetAttribute("IndependentAfterCreate", independentAfterCreate ? "true" : "false");

            foreach (Guid checkId in programsFromUI)
            {
                XmlElement programElement = doc.CreateElement("Program");
                rootElement.AppendChild(programElement);
                programElement.SetAttribute("Id", checkId.ToString());
            }

            m_jobXmlContent = doc.OuterXml;
        }
    }
}
