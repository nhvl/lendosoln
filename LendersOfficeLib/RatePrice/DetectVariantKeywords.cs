using System;
using System.Collections;
using System.Text;
using System.Xml;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice
{
    public class DetectLpeKeywordDependsOn
    {
        #region initialize static data
        static internal readonly string [,] Keyword2FieldName;
        static DetectLpeKeywordDependsOn()
        {
            LoanDataWrapper.VerifyMapping();

            Keyword2FieldName = (string[,])LoanDataWrapper.Keyword2FieldName.Clone();
            for( int i =0; i < Keyword2FieldName.GetLength(0); i++ )
            {
                string field   = Keyword2FieldName[i, 1];
                int idx = field.IndexOf( "." );
                if( idx >= 0 )
                {
                    string newField = field.Substring( idx +1 );
                    Keyword2FieldName[i, 1] = newField;
                }
            }
        }

        #endregion

        #region static helper methods
        private static ArrayList BestPath( string[] names, Hashtable parents, ArrayList bestPath  )
        {
            bestPath.Clear();
            ArrayList path = new ArrayList();

            for(int i=0; i < names.Length; i++)
            {
                if( GetDependPath( parents, names[i], path ).Count == 0 )
                    continue;

                if( bestPath.Count == 0 || bestPath.Count > path.Count  )
                {
                    bestPath.Clear();
                    bestPath.AddRange( path );
                }
            }
            return bestPath;
        }

        private static ArrayList GetDependPath( Hashtable parents, string name, ArrayList path)
        {
            path.Clear();

            if( parents.Contains( name ) )
                path.Add( name );

            string cur = name;
            for( int i=0; i <= parents.Count; i++)
            {
                cur = (string) parents[cur];
                if( cur == null )
                    break;
                path.Add(cur);
            }
            path.Reverse();
            if( path.Count > parents.Count )
                throw new CBaseException(ErrorMessages.Generic, "DetectLpeKeywordDependsOn.GetDependPath(...) error : " + DependPathToString(path));
            return path;
        }

        private static string DependPathToString( ArrayList path )
        {
            if( path.Count == 0 )
                return "";
            string dependVar = (string)path[path.Count-1];

            if( path.Count == 1 )
                return dependVar;

            StringBuilder sb = new StringBuilder( dependVar );
            sb.Append(" [ ").Append( (string)path[0] );

            for( int i=1; i < path.Count; i++)
                sb.Append( " -> ").Append(path[i]);

            sb.Append(" ]");

            return sb.ToString();
        }

        #endregion

        SortedList m_affectKeywords = new SortedList();
        public DetectLpeKeywordDependsOn(KindOfDependsOn kindOfDependsOn, string[] loanFieldNames )
        {

            LendersOffice.Test.DependencyGraph dg = new LendersOffice.Test.DependencyGraph();
            dg.InitializeGraph( kindOfDependsOn, typeof( CPageBase ) , typeof( CAppData ) );

            ArrayList path = new ArrayList();
            for( int i =0; i < Keyword2FieldName.GetLength(0); i++ )
            {
                string keyword = Keyword2FieldName[i, 0];
                string field   = Keyword2FieldName[i, 1];

                if( field.IndexOf(".") != -1 )
                    continue;
                if( dg[ field ] == null )
                    continue;

                Hashtable dependFields = dg.GetDependencyNames( field, /* directDependOnly = */ false );
                dependFields[ field ] = null; 

                BestPath( loanFieldNames, dependFields, path );
                if( path.Count > 0 )
                {
                    m_affectKeywords.Add( keyword, path.Clone() );
                }
            }

        }


        private string[] GetTextResult()
        {
            ArrayList result = new ArrayList();
            foreach( string keyword in m_affectKeywords.Keys )
            {
                ArrayList path = (ArrayList)m_affectKeywords[keyword];
                result.Add( string.Format("{0} ( {1} depends on {2} )", keyword, path[0], DependPathToString( path ) ) );
            }
            return (string[])result.ToArray(typeof(string));
        }

        public XmlDocument ToXmlDocument()
        {
            XmlDocument xmlDoc = new XmlDocument();			
            XmlElement  root   = xmlDoc.CreateElement( "AffectingKeywords" );

            XmlAttribute attrib = xmlDoc.CreateAttribute( "Count" );
            attrib.Value = m_affectKeywords.Count.ToString();
            root.Attributes.Append( attrib );

            foreach( string keyword in m_affectKeywords.Keys )
            {
                ArrayList path = (ArrayList)m_affectKeywords[keyword];                

                XmlElement ele = xmlDoc.CreateElement( keyword );
                if( path.Count > 1 )
                {
                    string pathString = String.Join("\\",  (string[])path.ToArray(typeof(string)) );
                    ele.AppendChild(xmlDoc.CreateTextNode( pathString ) ); 
                }

                attrib = xmlDoc.CreateAttribute( "mappingTo" );
                attrib.Value = path[0].ToString();
                ele.Attributes.Append( attrib );

                attrib = xmlDoc.CreateAttribute( "affectedBy" );
                attrib.Value = path[path.Count-1].ToString();
                ele.Attributes.Append( attrib );

                root.AppendChild( ele);

            }
            xmlDoc.AppendChild( root );
            return xmlDoc;
        }


        static public string[] KeywordsDependsOnFieldNames( KindOfDependsOn kindOfDependsOn, string[] loanFieldNames )
        {
            DetectLpeKeywordDependsOn affectKeywords = new DetectLpeKeywordDependsOn( kindOfDependsOn, loanFieldNames );
            return affectKeywords.GetTextResult();
        }
    }


	public class DetectVariantKeywords
	{
		private DetectVariantKeywords()
		{
		}

        static public string[] GetVariantKeywords( )
        {
            return DetectLpeKeywordDependsOn.KeywordsDependsOnFieldNames( DataAccess.KindOfDependsOn.PML, s_variantFields );
        }
        

        static public string[] GetVariantFields()
        {
            return (string[])s_variantFields.Clone();
        }

        static public string[] GetDtiDependKeywords()
        {
            string[] dtiFields = { "sNoteIR", "sBrokComp1Pc", "sRAdjMarginR", "sOptionArmTeaserR", "sQualIR" };
            string[] keywords = DetectLpeKeywordDependsOn.KeywordsDependsOnFieldNames( DataAccess.KindOfDependsOn.PML, dtiFields );
            return keywords;                                            
        }


        #region s_variantFields
        // the loanData's variant field (or volatile field) is a field that is changed 
        // when applying some loan policy on that loan data. 
        static string [] s_variantFields =
        {
            "sNoteIR",
            "sBrokComp1Pc",
            "sRAdjWorstIndex",
            "sRAdjMarginR",
            "sLOrigFPc",
            "sOptionArmTeaserR",
            "sQualIR",

            // --------------------------------------------

            "sCltvLpeQual",
            "sLtvLpeQual",
            "sLAmtCalcLpeQual",
            "sCreditScoreLpeQual",

            // --------------------------------------------

            "sLpTemplateId",
            "sLpIsArmMarginDisplayed",
            "sLpTemplateNm",
            "sLenNm",
            "sLT",
            "sBrokComp1Lckd",
            "sIsOptionArm",
            "sTerm",
            "sFinMethDesc",
            "sFinMethT",
            "sDue",
            "sBrokComp1Desc",
            "sRAdj1stCapR",
            "sRAdj1stCapMon",
            "sRAdjCapR",
            "sRAdjCapMon",
            "sRAdjLifeCapR",
            "sRAdjIndexR",
            "sRAdjFloorR",
            "sRAdjRoundT",
            "sPmtAdjCapR",
            "sPmtAdjCapMon",
            "sPmtAdjRecastPeriodMon",
            "sPmtAdjRecastStop",
            "sPmtAdjMaxBalPc",
            "sBuydwnR1",
            "sBuydwnR2",
            "sBuydwnR3",
            "sBuydwnR4",
            "sBuydwnR5",
            "sBuydwnMon1",
            "sBuydwnMon2",
            "sBuydwnMon3",
            "sBuydwnMon4",
            "sBuydwnMon5",
            "sGradPmtYrs",
            "sGradPmtR",
            "sDtiUsingMaxBalPc",
            "sIOnlyMon",
            "sIsQRateIOnly",
            "sHasVarRFeature",
            "sVarRNotes",
            "sAprIncludesReqDeposit",
            "sHasDemandFeature",
            "sFilingF",
            "sLateDays",
            "sLateChargePc",
            "sLateChargeBaseDesc",
            "sPrepmtPenaltyT",
            "sAssumeLT",
            "sArmIndexAffectInitIRBit",
            "sArmIndexBasedOnVstr",
            "sArmIndexCanBeFoundVstr",
            "sArmIndexNotifyAtLeastDaysVstr",
            "sArmIndexNotifyNotBeforeDaysVstr",
            "sArmIndexT",
            "sArmIndexNameVstr",
            "sFreddieArmIndexT",
            "sArmIndexEffectiveD",
            "sLpInvestorNm",
            "sLpProductType",
            "sFfUfMipIsBeingFinanced",
            "sVaFfExemptTri",
            "sFfUfmipR",
            "sFfUfmip1003Lckd",
            "sFfUfmip1003",
            "sProd3rdPartyUwResultT",
            "sQMAveragePrimeOfferRCalc"
        };
        #endregion 
	}
}