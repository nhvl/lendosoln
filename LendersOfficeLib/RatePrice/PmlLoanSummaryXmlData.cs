﻿using System;
using System.Xml;
using DataAccess;
using LendersOffice.Reminders;
using Toolbox;
using LendersOffice.Security;
using LendersOffice.Constants;
using System.Threading;
using LendersOffice.Admin;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LendersOffice.AntiXss;
using LendersOffice.ObjLib.Task;
using LendersOffice.Common;

namespace LendersOfficeApp.newlos.Underwriting
{
    public class PmlLoanSummaryXmlData
    {
        private class _Item
        {
            public string Description;
            public decimal Rate = 0;
            public decimal Fee = 0;
            public decimal Margin = 0;
            public bool hasAllZeroAdj = false;
        }

        protected static Boolean IsPmlEnabled
        {
            get { return (Thread.CurrentPrincipal as AbstractUserPrincipal).HasFeatures(E_BrokerFeatureT.PriceMyLoan); }
        }

        public static void Generate(XmlWriter writer, Guid sLId)
        {
            Generate(writer, sLId, false);
        }

        public static void Generate(XmlWriter writer, Guid sLId, bool maskSensitiveInformation)
        {
            Generate(writer, sLId, maskSensitiveInformation, true, true, true);
        }

        public static void Generate(XmlWriter writer, Guid sLId, bool maskSensitiveInformation, bool includeCompletedConditions)
        {
            Generate(writer, sLId, maskSensitiveInformation, includeCompletedConditions, true, true);
        }

        public static void Generate(XmlWriter writer, Guid sLId, bool maskSensitiveInformation, bool includeCompletedConditions, bool includeUnderwriterContactInfo)
        {
            Generate(writer, sLId, maskSensitiveInformation, includeCompletedConditions, includeUnderwriterContactInfo, true);
        }

        public static void Generate(XmlWriter writer, Guid sLId, bool maskSensitiveInformation, bool includeCompletedConditions, bool includeUnderwriterContactInfo, bool includeJuniorUnderwriterContactInfo)
        {
            Generate(writer, sLId, maskSensitiveInformation, includeCompletedConditions, includeUnderwriterContactInfo, true, true);
        }

        public static void Generate(XmlWriter writer, Guid sLId, bool maskSensitiveInformation, bool includeCompletedConditions, bool includeUnderwriterContactInfo, bool includeJuniorUnderwriterContactInfo, bool includeNonSuspenseConditions)
        {

            CPageData dataLoan = new CLOPmlLoanViewData(sLId);
            dataLoan.InitLoad();
            dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;

            BrokerDB broker = BrokerDB.RetrieveById(dataLoan.sBrokerId);

            writer.WriteStartElement("pml_loanview");
            writer.WriteElementString("date_created", Tools.GetDateTimeNowString());
            writer.WriteElementString("date_created_full", Tools.GetDateTimeDescription(DateTime.Now, "MMMM dd, yyyy h:mm tt"));

            // 10/04/06 mf OPM 7404. We have to see if this loan comes from a branch
            // that we are supposed to display the name of.
            BranchDB branch = new BranchDB(dataLoan.sBranchId, dataLoan.sBrokerId);
            branch.Retrieve();
            writer.WriteElementString("branch_name", branch.Name); // 8/31/2015 - dd OPM 210318 - So MWF can include branch name on certificate
            writer.WriteElementString("broker_name", branch.DisplayNm);
            writer.WriteElementString("show_logo", (!branch.IsDisplayNmModified).ToString());
            writer.WriteElementString("branch_street_address", branch.Address.StreetAddress);
            writer.WriteElementString("branch_city", branch.Address.City);
            writer.WriteElementString("branch_state", branch.Address.State);
            writer.WriteElementString("branch_zip", branch.Address.Zipcode);
            writer.WriteElementString("branch_phone", branch.Phone);
            writer.WriteElementString("branch_fax", branch.Fax);
            writer.WriteElementString("broker_url", broker.Url);

            #region <account_executive>
            writer.WriteStartElement("account_executive");
            if (broker.CustomerCode.Equals("PML0200", StringComparison.OrdinalIgnoreCase))
            {
                foreach (CAgentFields aeAgent in dataLoan.GetAgentsOfRole(E_AgentRoleT.Other, E_ReturnOptionIfNotExist.ReturnEmptyObject))
                {
                    if (aeAgent.OtherAgentRoleTDesc.Equals("Account Executive", StringComparison.OrdinalIgnoreCase))
                    {
                        writer.WriteElementString("name", aeAgent.AgentName);
                        writer.WriteElementString("phone", aeAgent.PhoneOfCompany);
                        writer.WriteElementString("fax", "");
                        writer.WriteElementString("email", aeAgent.EmailAddr);
                        writer.WriteElementString("show", "");
                        break;
                    }
                }

            }
            else
            {
                writer.WriteElementString("name", dataLoan.sEmployeeLenderAccExec.FullName);
                writer.WriteElementString("phone", dataLoan.sEmployeeLenderAccExec.Phone);
                writer.WriteElementString("fax", dataLoan.sEmployeeLenderAccExec.Fax);
                writer.WriteElementString("email", dataLoan.sEmployeeLenderAccExec.Email);
                writer.WriteElementString("show", dataLoan.sEmployeeLenderAccExec.IsValid.ToString());
            }
            writer.WriteEndElement(); // </account_executive>
            #endregion

            #region <underwriter>
            if (includeUnderwriterContactInfo)
            {
                writer.WriteStartElement("underwriter");
                writer.WriteElementString("name", dataLoan.sEmployeeUnderwriter.FullName);
                writer.WriteElementString("phone", dataLoan.sEmployeeUnderwriter.Phone);
                writer.WriteElementString("fax", dataLoan.sEmployeeUnderwriter.Fax);
                writer.WriteElementString("email", dataLoan.sEmployeeUnderwriter.Email);
                if (dataLoan.sEmployeeUnderwriter.IsValid)
                {
                    writer.WriteElementString("show", "");
                }
                writer.WriteEndElement(); // </underwriter>
            }
            #endregion

            #region <junior_underwriter>
            // 11/22/2013 gf - opm 145015
            if (includeJuniorUnderwriterContactInfo)
            {
                writer.WriteStartElement("junior_underwriter");
                writer.WriteElementString("name", dataLoan.sEmployeeJuniorUnderwriter.FullName);
                writer.WriteElementString("phone", dataLoan.sEmployeeJuniorUnderwriter.Phone);
                writer.WriteElementString("fax", dataLoan.sEmployeeJuniorUnderwriter.Fax);
                writer.WriteElementString("email", dataLoan.sEmployeeJuniorUnderwriter.Email);
                if (dataLoan.sEmployeeJuniorUnderwriter.IsValid)
                {
                    writer.WriteElementString("show", "");
                }
                writer.WriteEndElement(); // </junior_underwriter>
            }
            #endregion

            #region lender info
            CAgentFields lenderagent = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            writer.WriteStartElement("lender_agent");
            writer.WriteElementString("company_name", lenderagent.CompanyName);
            writer.WriteElementString("company_address", lenderagent.StreetAddr);
            writer.WriteElementString("city_state_zip", lenderagent.CityStateZip);
            writer.WriteEndElement();


            #endregion
            #region <E_AgentRoleT_LoanOfficer>
            CAgentFields agent = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            writer.WriteStartElement("E_AgentRoleT_LoanOfficer");
            writer.WriteElementString("CompanyName", agent.CompanyName);
            writer.WriteElementString("BranchName", agent.BranchName); // dd 8/3/2015 - OPM 210318 Add for MWF
            writer.WriteElementString("PhoneOfCompany", agent.PhoneOfCompany);
            writer.WriteElementString("FaxOfCompany", agent.FaxOfCompany);
            writer.WriteElementString("AgentName", agent.AgentName);
            writer.WriteElementString("Phone", agent.Phone);
            writer.WriteElementString("FaxNum", agent.FaxNum);
            writer.WriteElementString("EmailAddr", agent.EmailAddr);
            writer.WriteElementString("StreetAddr", agent.StreetAddr);
            writer.WriteElementString("CityStateZip", agent.CityStateZip);
            if (!agent.IsNewRecord)
            {
                writer.WriteElementString("show", "");
                if (EmployeeDB.RetrieveById(broker.BrokerID, agent.EmployeeId).UserType == 'P')
                {
                    writer.WriteElementString("ispmluser", "");
                }
            }
            writer.WriteEndElement(); // </E_AgentRoleT_LoanOfficer>
            #endregion

            #region <E_AgentRoleT_Processor>
            CAgentFields processorAgent = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.Processor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            writer.WriteStartElement("E_AgentRoleT_Processor");
            writer.WriteElementString("CompanyName", processorAgent.CompanyName);
            writer.WriteElementString("BranchName", processorAgent.BranchName);
            writer.WriteElementString("PhoneOfCompany", processorAgent.PhoneOfCompany);
            writer.WriteElementString("FaxOfCompany", processorAgent.FaxOfCompany);
            writer.WriteElementString("AgentName", processorAgent.AgentName);
            writer.WriteElementString("Phone", processorAgent.Phone);
            writer.WriteElementString("FaxNum", processorAgent.FaxNum);
            writer.WriteElementString("EmailAddr", processorAgent.EmailAddr);
            writer.WriteElementString("StreetAddr", processorAgent.StreetAddr);
            writer.WriteElementString("CityStateZip", processorAgent.CityStateZip);

            if (processorAgent != CAgentFields.Empty)
            {
                writer.WriteElementString("show", string.Empty);
            }

            writer.WriteEndElement(); // </E_AgentRoleT_Processor>
            #endregion

            #region <E_AgentRoleT_BrokerProcessor>
            CAgentFields brokerProcessorAgent = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.BrokerProcessor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            writer.WriteStartElement("E_AgentRoleT_BrokerProcessor");
            writer.WriteElementString("CompanyName", brokerProcessorAgent.CompanyName);
            writer.WriteElementString("BranchName", brokerProcessorAgent.BranchName);
            writer.WriteElementString("PhoneOfCompany", brokerProcessorAgent.PhoneOfCompany);
            writer.WriteElementString("FaxOfCompany", brokerProcessorAgent.FaxOfCompany);
            writer.WriteElementString("AgentName", brokerProcessorAgent.AgentName);
            writer.WriteElementString("Phone", brokerProcessorAgent.Phone);
            writer.WriteElementString("FaxNum", brokerProcessorAgent.FaxNum);
            writer.WriteElementString("EmailAddr", brokerProcessorAgent.EmailAddr);
            writer.WriteElementString("StreetAddr", brokerProcessorAgent.StreetAddr);
            writer.WriteElementString("CityStateZip", brokerProcessorAgent.CityStateZip);

            if (brokerProcessorAgent != CAgentFields.Empty)
            {
                writer.WriteElementString("show", string.Empty);
            }

            writer.WriteEndElement(); // </E_AgentRoleT_BrokerProcessor>
            #endregion

            #region <loan_officer>
            writer.WriteStartElement("loan_officer");

            if (broker.CustomerCode.Equals("PML0200", StringComparison.OrdinalIgnoreCase))
            {
                // 10/3/2012 dd - For REMN we are going to use the official contact.
                CAgentFields loAgent = dataLoan.sAgentCollection.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                writer.WriteElementString("name", loAgent.AgentName);
                writer.WriteElementString("pml_broker_name", loAgent.CompanyName);
                writer.WriteElementString("phone", loAgent.PhoneOfCompany);
                writer.WriteElementString("email", loAgent.EmailAddr);
                if (!agent.IsNewRecord)
                {
                    writer.WriteElementString("show", "");
                }

            }
            else
            {
                writer.WriteElementString("name", dataLoan.sEmployeeLoanRep.FullName);
                writer.WriteElementString("pml_broker_name", dataLoan.sEmployeeLoanRepPmlUserBrokerNm);
                writer.WriteElementString("phone", dataLoan.sEmployeeLoanRep.Phone);
                writer.WriteElementString("fax", dataLoan.sEmployeeLoanRep.Fax);
                writer.WriteElementString("email", dataLoan.sEmployeeLoanRep.Email);
                if (dataLoan.sEmployeeLoanRep.IsValid)
                {
                    writer.WriteElementString("show", "");
                }
            }
            writer.WriteEndElement(); // </loan_officer>
            #endregion

            #region <broker_processor>
            writer.WriteStartElement("broker_processor");
            writer.WriteElementString("name", dataLoan.sEmployeeBrokerProcessor.FullName);
            writer.WriteElementString("pml_broker_name", dataLoan.sEmployeeBrokerProcessor.PmlUserBrokerNm);
            writer.WriteElementString("phone", dataLoan.sEmployeeBrokerProcessor.Phone);
            writer.WriteElementString("fax", dataLoan.sEmployeeBrokerProcessor.Fax);
            writer.WriteElementString("email", dataLoan.sEmployeeBrokerProcessor.Email);
            if (dataLoan.sEmployeeBrokerProcessor.IsValid && IsPmlEnabled)
            {
                writer.WriteElementString("show", "");
            }
            writer.WriteEndElement(); // </broker_processor>
            #endregion

            #region External Secondary
            writer.WriteStartElement("external_secondary");
            writer.WriteElementString("name", dataLoan.sEmployeeExternalSecondary.FullName);
            writer.WriteElementString("pml_broker_name", dataLoan.sEmployeeExternalSecondary.PmlUserBrokerNm);
            writer.WriteElementString("phone", dataLoan.sEmployeeExternalSecondary.Phone);
            writer.WriteElementString("fax", dataLoan.sEmployeeExternalSecondary.Fax);
            writer.WriteElementString("email", dataLoan.sEmployeeExternalSecondary.Email);

            if (dataLoan.sEmployeeExternalSecondary.IsValid &&
                IsPmlEnabled && 
                dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent)
            {
                writer.WriteElementString("show", "");
            }
            writer.WriteEndElement();
            #endregion

            #region External Post-Closer
            writer.WriteStartElement("external_post_closer");
            writer.WriteElementString("name", dataLoan.sEmployeeExternalPostCloser.FullName);
            writer.WriteElementString("pml_broker_name", dataLoan.sEmployeeExternalPostCloser.PmlUserBrokerNm);
            writer.WriteElementString("phone", dataLoan.sEmployeeExternalPostCloser.Phone);
            writer.WriteElementString("fax", dataLoan.sEmployeeExternalPostCloser.Fax);
            writer.WriteElementString("email", dataLoan.sEmployeeExternalPostCloser.Email);

            if (dataLoan.sEmployeeExternalPostCloser.IsValid &&
                IsPmlEnabled && 
                dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent)
            {
                writer.WriteElementString("show", "");
            }
            writer.WriteEndElement();
            #endregion

            #region <Processor>
            // OPM 34389.  mf. 10/27/09
            if (dataLoan.sEmployeeProcessor.IsValid)
            {
                writer.WriteStartElement("processor");
                writer.WriteElementString("name", dataLoan.sEmployeeProcessor.FullName);
                writer.WriteElementString("phone", dataLoan.sEmployeeProcessor.Phone);
                writer.WriteElementString("fax", dataLoan.sEmployeeProcessor.Fax);
                writer.WriteElementString("email", dataLoan.sEmployeeProcessor.Email);
                writer.WriteEndElement(); // </processor>
            }
            #endregion

            #region <junior_processor>
            // 11/22/2013 gf - opm 145015
            if (dataLoan.sEmployeeJuniorProcessor.IsValid)
            {
                writer.WriteStartElement("junior_processor");
                writer.WriteElementString("name", dataLoan.sEmployeeJuniorProcessor.FullName);
                writer.WriteElementString("phone", dataLoan.sEmployeeJuniorProcessor.Phone);
                writer.WriteElementString("fax", dataLoan.sEmployeeJuniorProcessor.Fax);
                writer.WriteElementString("email", dataLoan.sEmployeeJuniorProcessor.Email);
                writer.WriteEndElement(); // </junior_processor>
            }
            #endregion

            #region <doc_drawer>
            // 2/28/2014 - dd - JMAC Use this information on their custom Status
            if (dataLoan.sEmployeeDocDrawer.IsValid)
            {
                writer.WriteStartElement("doc_drawer");
                writer.WriteElementString("name", dataLoan.sEmployeeDocDrawer.FullName);
                writer.WriteElementString("phone", dataLoan.sEmployeeDocDrawer.Phone);
                writer.WriteElementString("fax", dataLoan.sEmployeeDocDrawer.Fax);
                writer.WriteElementString("email", dataLoan.sEmployeeDocDrawer.Email);
                writer.WriteEndElement(); // </doc_drawer>
            }
            #endregion

            #region <lock_desk>
            if (dataLoan.sEmployeeLockDesk.IsValid)
            {
                writer.WriteStartElement("lock_desk");
                writer.WriteElementString("name", dataLoan.sEmployeeLockDesk.FullName);
                writer.WriteElementString("phone", dataLoan.sEmployeeLockDesk.Phone);
                writer.WriteElementString("fax", dataLoan.sEmployeeLockDesk.Fax);
                writer.WriteElementString("email", dataLoan.sEmployeeLockDesk.Email);
                writer.WriteEndElement(); // </lock_desk>
            }
            #endregion

            #region <credit_auditor>
            if (dataLoan.sEmployeeCreditAuditor.IsValid)
            {
                writer.WriteStartElement("credit_auditor");
                writer.WriteElementString("name", dataLoan.sEmployeeCreditAuditor.FullName);
                writer.WriteElementString("phone", dataLoan.sEmployeeCreditAuditor.Phone);
                writer.WriteElementString("fax", dataLoan.sEmployeeCreditAuditor.Fax);
                writer.WriteElementString("email", dataLoan.sEmployeeCreditAuditor.Email);
                writer.WriteEndElement(); // </credit_auditor>
            }
            #endregion

            #region <purchaser>
            if (dataLoan.sEmployeePurchaser.IsValid)
            {
                writer.WriteStartElement("purchaser");
                writer.WriteElementString("name", dataLoan.sEmployeePurchaser.FullName);
                writer.WriteElementString("phone", dataLoan.sEmployeePurchaser.Phone);
                writer.WriteElementString("fax", dataLoan.sEmployeePurchaser.Fax);
                writer.WriteElementString("email", dataLoan.sEmployeePurchaser.Email);
                writer.WriteEndElement(); // </purchaser>
            }
            #endregion

            #region <funder>
            // 2/28/2014 - dd - JMAC Use this information on their custom Status
            if (dataLoan.sEmployeeFunder.IsValid)
            {
                writer.WriteStartElement("funder");
                writer.WriteElementString("name", dataLoan.sEmployeeFunder.FullName);
                writer.WriteElementString("phone", dataLoan.sEmployeeFunder.Phone);
                writer.WriteElementString("fax", dataLoan.sEmployeeFunder.Fax);
                writer.WriteElementString("email", dataLoan.sEmployeeFunder.Email);
                writer.WriteEndElement(); // </funder>
            }
            #endregion

            #region <loan_product>
            writer.WriteStartElement("loan_product");
            writer.WriteElementString("name", dataLoan.sLpTemplateNm);
            writer.WriteElementString("display_margin", dataLoan.sLpIsArmMarginDisplayed ? "True" : "False");

            #region <arm>
            writer.WriteStartElement("arm");
            writer.WriteElementString("adjust_change", dataLoan.sRAdjCapR_rep);
            writer.WriteEndElement(); // </arm>
            #endregion

            writer.WriteEndElement(); // </loan_product>
            #endregion

            #region <applicant>
            CAppData dataApp = dataLoan.GetAppData(0); // Only work with primary applicant for now.
            writer.WriteStartElement("applicant");
            writer.WriteElementString("borrower", dataApp.aBNm);
            if (maskSensitiveInformation)
            {
                writer.WriteElementString("borrowerssn", "***-**-****");
            }
            else
            {
                writer.WriteElementString("borrowerssn", dataApp.aBSsn);
            }
            writer.WriteElementString("borrowercitizenship", GetProdBCitizenDescription(dataApp.aProdBCitizenT));
            writer.WriteElementString("score_modified", dataLoan.sProdIsCreditScoresAltered ? "Yes" : "No");
            writer.WriteElementString("score_type_1", dataLoan.sCreditScoreType1_rep);
            writer.WriteElementString("score_type_2", dataLoan.sCreditScoreType2_rep);
            writer.WriteElementString("spouse", dataApp.aCNm);
            if (maskSensitiveInformation)
            {
                writer.WriteElementString("spousessn", "***-**-****");
            }
            else
            {
                writer.WriteElementString("spousessn", dataApp.aCSsn);
            }
            string aIsBorrSpousePrimaryWageEarner = "";
            if (dataApp.aBHasSpouse)
            {
                aIsBorrSpousePrimaryWageEarner = dataApp.aIsBorrSpousePrimaryWageEarner ? "Yes" : "No";
            }
            writer.WriteElementString("aIsBorrSpousePrimaryWageEarner", aIsBorrSpousePrimaryWageEarner);
            writer.WriteElementString("first_time_buyer", dataLoan.sHas1stTimeBuyer ? "Yes" : "No");
            writer.WriteElementString("total_income", dataLoan.sPrimAppTotNonspI_rep);
            writer.WriteElementString("total_income_borrower", dataApp.aBTotI_rep);
            writer.WriteElementString("total_income_coborrower", dataApp.aCTotI_rep);
            writer.WriteElementString("self_employed", dataLoan.sIsSelfEmployed ? "Yes" : "No");
            writer.WriteElementString("housing_history", dataLoan.sProdHasHousingHistory ? "Yes" : "No");

            writer.WriteElementString("non_mortgage_payment", dataLoan.sTransmOMonPmt_rep);
            if (broker.PmlRequireEstResidualIncome)
            {
                writer.WriteElementString("residual_income", dataLoan.sProdEstimatedResidualI_rep);
            }

            writer.WriteElementString("borr_fico", dataApp.aBFicoScore_rep); // 3/14/2014 - dd - Use by JMAC custom certificate
            writer.WriteElementString("spouse_fico", dataApp.aCFicoScore_rep); // 3/14/2014 - dd - Use by JMAC custom certificate

            writer.WriteEndElement();
            #endregion
            #region <property>
            writer.WriteStartElement("property");
            writer.WriteElementString("type", dataLoan.sProdSpT_rep);
            writer.WriteElementString("state", dataLoan.sSpState);
            writer.WriteElementString("structure_type", Tools.GetStructureTypeDescription(dataLoan.sProdSpStructureT));
            writer.WriteElementString("num_of_units", dataLoan.sUnitsNum_rep);
            writer.WriteElementString("num_of_stories", dataLoan.sProdCondoStories_rep);
            writer.WriteElementString("purpose", GetPropertyPurposeDescription(dataLoan.sOccT));
            writer.WriteElementString("property_tax", dataLoan.sProRealETx_rep);
            writer.WriteElementString("other_housing_expense", dataLoan.sProOHExp_rep);
            writer.WriteElementString("address", dataLoan.sSpAddr);
            writer.WriteElementString("citystatezip", Tools.CombineCityStateZip(dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            writer.WriteElementString("county", dataLoan.sSpCounty);
            writer.WriteElementString("sProdIsSpInRuralArea", dataLoan.sProdIsSpInRuralArea ? "Yes" : "No");
            writer.WriteElementString("sProdIsCondotel", dataLoan.sProdIsCondotel ? "Yes" : "No");
            writer.WriteElementString("sProdIsNonwarrantableProj", dataLoan.sProdIsNonwarrantableProj ? "Yes" : "No");
            writer.WriteElementString("sOccR", dataLoan.sOccR_rep);
            writer.WriteElementString("sSpGrossRent", dataLoan.sSpGrossRent_rep);

            //av opm 47246 Update PML with Anti-flip guidelines and pricing
            if (dataLoan.sLPurposeT == E_sLPurposeT.Purchase && dataLoan.sPriorSalesD.IsValid)
            {
                writer.WriteElementString("sPriorSalesD", dataLoan.sPriorSalesD_rep);
                writer.WriteElementString("sPriorSalesPrice", dataLoan.sPriorSalesPrice_rep);
                writer.WriteElementString("sPriorSalesPropertySellerT", dataLoan.sPriorSalesPropertySellerT_rep);
            }
            writer.WriteEndElement(); // </property>



            #endregion

            #region <broker_variables>
            //25828 fs 12/04/08
            writer.WriteStartElement("broker_variables");
            writer.WriteElementString("IsIncomeAssetsRequiredFhaStreamline", broker.IsIncomeAssetsRequiredFhaStreamline.ToString()); //opm 32180 fs 11/19/09
            writer.WriteElementString("ShowUnderwriterInPMLSummary", broker.ShowUnderwriterInPMLCertificate.ToString()); // gf OPM 3723
            writer.WriteElementString("ShowJuniorUnderwriterInPMLSummary", broker.ShowJuniorUnderwriterInPMLCertificate.ToString()); // 11/22/2013 gf - opm 145015
            writer.WriteElementString("ShowProcessorInPMLSummary", broker.ShowProcessorInPMLCertificate.ToString());     // gf OPM 3723
            writer.WriteElementString("ShowJuniorProcessorInPMLSummary", broker.ShowJuniorProcessorInPMLCertificate.ToString()); // 11/22/2013 gf - opm 145015
            writer.WriteEndElement();
            #endregion

            #region <loan>
            writer.WriteStartElement("loan");
            writer.WriteElementString("sPresLTotPersistentHExp", dataLoan.sPresLTotPersistentHExp_rep);

            //opm  71273 av add point w/o orig comp to rate lock conf
            //opm  71620 rwn made it a broker property instead of hardcoding the brokerid.
            if (broker.IsAddPointWithoutOriginatorCompensationToRateLock)
            {
                if (E_sOriginatorCompensationPaymentSourceT.LenderPaid == dataLoan.sOriginatorCompensationPaymentSourceT &&
                    E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees == dataLoan.sOriginatorCompensationLenderFeeOptionT)
                {
                    writer.WriteElementString("sBrokComp1Pc", dataLoan.sBrokComp1Pc_rep.Replace("%", ""));
                }
            }


            if (dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout || dataLoan.sLPurposeT == E_sLPurposeT.Refin)
            {
                writer.WriteElementString("sPurchPrice", "N/A");
            }
            else
            {
                writer.WriteElementString("sPurchPrice", dataLoan.sPurchPrice_rep);
            }

            writer.WriteElementString("sApprVal", dataLoan.sApprVal_rep);

            //dataLoan.sFfUfmipRPeSyncWithLo = true; // 4/8/2009 dd - OPM 28035 - Sync "Total Loan Amount" and "Financed MIP" on PML summary.   no longer need this
            writer.WriteElementString("status", dataLoan.sStatusT_rep);
            
            var slNm = dataLoan.sLNm;
            if (slNm.Length > 5 && dataLoan.BrokerDB.HidePmlCertSensitiveFields)
            {
                slNm = new string('*', dataLoan.sLNm.Length - 5) + dataLoan.sLNm.Substring(dataLoan.sLNm.Length - 5, 5);
            }

            writer.WriteElementString("pml_loanno", slNm);

            writer.WriteElementString("purpose", dataLoan.sLPurposeT_rep);
            writer.WriteElementString("lien_position", GetLienPositionDescription(dataLoan.sLienPosT));
            writer.WriteElementString("house_value", dataLoan.sHouseVal_rep);
            writer.WriteElementString("orig_appr_value", dataLoan.sOriginalAppraisedValue_rep);
            writer.WriteElementString("loan_amount", dataLoan.sLAmtCalc_rep);
            writer.WriteElementString("final_loan_amount", dataLoan.sFinalLAmt_rep); //opm 28035 fs 03/02/09   
            writer.WriteElementString("sFfUfMipIsBeingFinanced", dataLoan.sFfUfMipIsBeingFinanced.ToString().ToLower());
            writer.WriteElementString("upfront_mip", dataLoan.sFfUfmipFinanced_rep); //opm 28035 fs 03/02/09
            writer.WriteElementString("type", dataLoan.sLT_rep); //opm 28035 fs 03/02/09
            writer.WriteElementString("cltv", dataLoan.sCltvR_rep);
            if (dataLoan.sShowAppraisedBasedLtvsOnCert)
            {
                writer.WriteElementString("sAppraisedValueCltvR", dataLoan.sAppraisedValueCltvR_rep);
                writer.WriteElementString("sAppraisedValueLtvR", dataLoan.sAppraisedValueLtvR_rep);
            }
            writer.WriteElementString("impound", dataLoan.sProdImpound ? "Yes" : "No");
            writer.WriteElementString("cashout_amount", dataLoan.sProdCashoutAmt_rep);
            writer.WriteElementString("ltv", dataLoan.sLtvR_rep);
            //writer.WriteElementString("hcltv", dataLoan.sHcltvR_rep);
            writer.WriteElementString("documentation_type", dataLoan.sProdDocT_rep);
            writer.WriteElementString("interest_only", dataLoan.sIsIOnly ? "Yes" : "No");
            writer.WriteElementString("option_arm", dataLoan.sIsOptionArm ? "Yes" : "No");
            writer.WriteElementString("rate_lock_period", dataLoan.sRLckdDays_rep);
            writer.WriteElementString("rate_locked", dataLoan.sIsRateLocked.ToString());
            writer.WriteElementString("lock_updated", dataLoan.sIsCurrentLockUpdated ? "Yes" : "No");

            writer.WriteElementString("amort_type", GetFinanceMethodDescription(dataLoan.sFinMethT));
            writer.WriteElementString("rate_lock_date", dataLoan.sRLckdD_rep);
            writer.WriteElementString("has_appraisal", dataLoan.sHasAppraisal ? "Yes" : "No");

            writer.WriteElementString("rate_lock_expired_date", dataLoan.sRLckdExpiredD_rep);
            writer.WriteElementString("submitted_date", dataLoan.sSubmitD_rep);
            writer.WriteElementString("approved_date", dataLoan.sApprovD_rep);
            writer.WriteElementString("approved_exp_date", dataLoan.sAppExpD_rep);
            writer.WriteElementString("suspended_date", dataLoan.sSuspendedD_rep);
            writer.WriteElementString("term", dataLoan.sTerm_rep);
            writer.WriteElementString("due", dataLoan.sDue_rep);
            writer.WriteElementString("purchase_contract_date", dataLoan.sPurchaseContractDate_rep);
            writer.WriteElementString("estimate_closing_date", dataLoan.sEstCloseD_rep);

            bool IsNewPmlUIEnabled = broker.IsNewPmlUIEnabled ||
                (Thread.CurrentPrincipal as AbstractUserPrincipal).IsNewPmlUIEnabled;

            if (!broker.CustomerCode.Equals("PML0299", StringComparison.OrdinalIgnoreCase) || !IsNewPmlUIEnabled)
            {
                writer.WriteElementString("reserves", dataLoan.sProdAvailReserveMonths_rep);
            }

            writer.WriteElementString("mi", GetMortgageInsuranceDescription(dataLoan, broker.HasEnabledPMI));
            writer.WriteElementString("stand_alone_second", dataLoan.sIsStandAlone2ndLien ? "Yes" : "No"); // OPM 4442
            writer.WriteElementString("other_neg_am", dataLoan.sLpIsNegAmortOtherLien ? "Yes" : "No"); // OPM 4442
            writer.WriteElementString("other_fin_meth", GetFinanceMethodDescription(dataLoan.sOtherLFinMethT)); // OPM 4442
            writer.WriteElementString("other_pmt", dataLoan.sProOFinPmt_rep); // OPM 4442
            writer.WriteElementString("max_dti", dataLoan.sMaxDti_rep); // OPM 32212

            writer.WriteElementString("dti", dataLoan.sQualBottomR_rep); // OPM 465171

            writer.WriteElementString("sNoteIR", dataLoan.sNoteIR_rep);
            writer.WriteElementString("sRAdjMarginR", dataLoan.sRAdjMarginR_rep);

            writer.WriteElementString("is_fha_streamline", dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance ? "1" : "0"); //opm 32180 fs 11/23/09
            writer.WriteElementString("credit_qualifying", dataLoan.sIsCreditQualifying ? "Yes" : "No"); //opm 32180 fs 11/23/09
            writer.WriteElementString("is_va_irrrl", dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl ? "1" : "0"); //opm 28103 fs 07/04/10
            writer.WriteElementString("va_funding_fee", dataLoan.sProdVaFundingFee_rep); //opm 28103 fs 07/04/10

            writer.WriteElementString("sApprVal", dataLoan.sApprVal_rep);
            writer.WriteElementString("sLTotI", dataLoan.sLTotI_rep);
            writer.WriteElementString("sPurchPrice", dataLoan.sPurchPrice_rep == "$0.00" ? "N/A" : dataLoan.sPurchPrice_rep);// 1/15/2010 dd - OPM 44265

            writer.WriteElementString("subordinate_financing", dataLoan.sConcurSubFin_rep);

            writer.WriteElementString("num_financed_properties", dataLoan.sNumFinancedProperties_rep);

            if (broker.EnableLenderFeeBuyout && dataLoan.BranchChannelT == E_BranchChannelT.Wholesale)
            {
                writer.WriteElementString("lender_fee_buyout", dataLoan.sLenderFeeBuyoutRequestedT.ToString()); // OPM 465171
            }

            var linkedLoanInfo = dataLoan.sLinkedLoanInfo;
            bool writeHCLTV = false;
            if ((E_sLienPosT.Second == dataLoan.sLienPosT && E_sLpProductT.Heloc == dataLoan.sLpProductT && (linkedLoanInfo.IsLoanLinked == false))
                || (E_sLienPosT.First == dataLoan.sLienPosT && dataLoan.sIsOFinCreditLineInDrawPeriod))
            {
                writeHCLTV = true;
            }
            else if (E_sLienPosT.Second == dataLoan.sLienPosT && linkedLoanInfo.IsLoanLinked)
            {
                var linkedLoan = new CPageData(linkedLoanInfo.LinkedLId, "PmlLoanSummaryLinked", new string[] { "sIsOFinCreditLineInDrawPeriod" });
                linkedLoan.InitLoad();
                if (linkedLoan.sIsOFinCreditLineInDrawPeriod)
                {
                    writeHCLTV = true;
                }
            }
            if (writeHCLTV)
            {
                writer.WriteElementString("hcltv", dataLoan.sHcltvR_rep);
            }

            string prepayment_penalty = "";
            if (dataLoan.sPrepmtPeriodMonths != 0)
            {
                prepayment_penalty += dataLoan.sPrepmtPeriodMonths_rep + " months hard";
            }

            if (dataLoan.sSoftPrepmtPeriodMonths != 0)
            {
                prepayment_penalty += (string.IsNullOrEmpty(prepayment_penalty) ? "" : ", ") + dataLoan.sSoftPrepmtPeriodMonths_rep + " months soft";
            }

            if (string.IsNullOrEmpty(prepayment_penalty) && !broker.CustomerCode.Equals("PML0299", StringComparison.OrdinalIgnoreCase))
            {
                prepayment_penalty = "No Prepay";
            }

            if (!string.IsNullOrEmpty(prepayment_penalty))
            {
                writer.WriteElementString("prepayment_penalty", prepayment_penalty);
            }

            if (broker.IsAsk3rdPartyUwResultInPml)
            {
                writer.WriteElementString("auresponse", Tools.GetProd3rdPartyUwResultTDescription(dataLoan.sProd3rdPartyUwResultT));
                writer.WriteElementString("auresponse_modified", dataLoan.sIsProd3rdPartyUwResultTModifiedByUser ? "Yes" : "No");
                // 04/26/07 mf. OPM 12103 For MyCommunity
                //writer.WriteElementString("auprocessing", Tools.GetsProd3rdPartyUwProcessingTDescription(dataLoan.sProd3rdPartyUwProcessingT));
                writer.WriteElementString("du_refi_plus", dataLoan.sProdIsDuRefiPlus ? "Yes" : "No");

            }

            //opm  40509 av 11 22 09
            writer.WriteElementString("sDuCaseId", dataLoan.sDuCaseId);
            writer.WriteElementString("sFreddieLoanId", dataLoan.sFreddieLoanId);
            writer.WriteElementString("sAgencyCaseNum", dataLoan.sAgencyCaseNum);


            writer.WriteElementString("sApprRprtExpD", dataLoan.sApprRprtExpD_rep);
            writer.WriteElementString("sIncomeDocExpD", dataLoan.sIncomeDocExpD_rep);
            writer.WriteElementString("sCrExpD", dataLoan.sCrExpD_rep);
            writer.WriteElementString("sAssetExpD", dataLoan.sAssetExpD_rep);

            writer.WriteElementString("sPrelimRprtDocumentD", dataLoan.sPrelimRprtDocumentD_rep);
            writer.WriteElementString("sPrelimRprtExpD", dataLoan.sPrelimRprtExpD_rep);

            if (dataLoan.sBondDocExpD.IsValid)
            {
                writer.WriteElementString("sBondDocExpD", dataLoan.sBondDocExpD_rep);
            }

            writer.WriteElementString("sCreditScoreLpeQual", dataLoan.sCreditScoreLpeQual_rep);
            writer.WriteElementString("sMaxR", dataLoan.sMaxR_rep);

            if (dataLoan.sMaxDti_rep != "0.000%")
            {
                writer.WriteElementString("max_dti_rep", dataLoan.sMaxDti_rep); // OPM 32212
                writer.WriteElementString("sMaxR", dataLoan.sMaxR_rep);
            }
            else
            {
                writer.WriteElementString("max_dti_rep", "N/A");
            }

            if (dataLoan.sIsRenovationLoan)
            {
                writer.WriteElementString("is_renovation", "Yes");

                if (dataLoan.sLT == E_sLT.FHA)
                {
                    writer.WriteElementString("as_is_appraised", dataLoan.sAsIsAppraisedValuePeval_rep);
                    writer.WriteElementString("mip_ltv", dataLoan.sFHAMipLtv_rep);
                }

                writer.WriteElementString("as_completed", dataLoan.sApprValPe_rep);
                writer.WriteElementString("total_renovation_costs", dataLoan.sTotalRenovationCosts_rep);
            }

            // 2/28/2014 - dd - Add more statuses date requires by JMAC custom status cert.
            writer.WriteElementString("sStatusD", dataLoan.sStatusD_rep);
            writer.WriteElementString("sFundD", dataLoan.sFundD_rep);
            writer.WriteElementString("sApprRprtOd", dataLoan.sApprRprtOd_rep);
            writer.WriteElementString("sApprRprtDueD", dataLoan.sApprRprtDueD_rep);
            writer.WriteElementString("sSpValuationEffectiveD", dataLoan.sSpValuationEffectiveD_rep);
            writer.WriteElementString("sApprRprtRd", dataLoan.sApprRprtRd_rep);
            writer.WriteElementString("sTilInitialDisclosureD", dataLoan.sTilInitialDisclosureD_rep);
            writer.WriteElementString("sInitAPR", dataLoan.sInitAPR_rep);
            writer.WriteElementString("sLastDiscAPR", dataLoan.sLastDiscAPR_rep);
            writer.WriteElementString("sTilRedisclosureD", dataLoan.sTilRedisclosureD_rep);
            writer.WriteElementString("sRedisclosureMethodT", GetDescription(dataLoan.sRedisclosureMethodT));
            writer.WriteElementString("sTilRedisclosuresReceivedD", dataLoan.sTilRedisclosuresReceivedD_rep);
            writer.WriteElementString("sBranchChannelT", dataLoan.sBranchChannelT.ToString("D")); // 8/3/2015 - dd - OPM 210318 - Add for MWF.
            writer.WriteElementString("sConditionReviewD", dataLoan.sConditionReviewD_rep); // 4/1/2016 - dd - OPM 239678 - Add For JMAC
            writer.WriteElementString("sFundingConditionsD", dataLoan.sFundingConditionsD_rep); // 4/1/2016 - dd - OPM 239678 - Add For JMAC
            writer.WriteElementString("sQMStatusT", GetDescription(dataLoan.sQMStatusT)); // 4/1/2016 - dd - OPM 239678 - Add for JMAC
            writer.WriteElementString("sCustomField6D", dataLoan.sCustomField6D_rep); // 4/1/2016 - dd - OPM 239678 - Add for JMAC
            writer.WriteElementString("sCustomField7D", dataLoan.sCustomField7D_rep); // 4/1/2016 - dd - OPM 239678 - Add for JMAC
            writer.WriteElementString("sDocsBackD", dataLoan.sDocsBackD_rep); //4/1/2016 - dd - OPM 239678 - Add for JMAC
            writer.WriteElementString("sDocsD", dataLoan.sDocsD_rep); // 4/1/2016 - dd - OPM 239678 - Add for JMAC
            writer.WriteElementString("sDocsOrderedD", dataLoan.sDocsOrderedD_rep); // 4/1/2016 - dd - OPM 239678 - Add for JMAC.
            writer.WriteElementString("sLoanSubmittedD", dataLoan.sLoanSubmittedD_rep); // 09/22/2016 - je - OPM 279695 - Add for JMAC.
            // 4/1/2016 - dd - OPM 239678 - Add for JMAC
            writer.WriteElementString("clear_to_close_date", dataLoan.sClearToCloseD_rep);
            var sLoanEstimateDatesInfo = dataLoan.sLoanEstimateDatesInfo;

            if (sLoanEstimateDatesInfo != null)
            {
                var initialLoanEstimate = sLoanEstimateDatesInfo.InitialLoanEstimate;

                if (initialLoanEstimate != null)
                {
                    writer.WriteElementString("sLoanEstimateDatesInfo_InitialLoanEstimate_CreatedDate", initialLoanEstimate.CreatedDate.ToString("MM/dd/yyyy"));
                    writer.WriteElementString("sLoanEstimateDatesInfo_InitialLoanEstimate_IssuedDate", initialLoanEstimate.IssuedDate.ToString("MM/dd/yyyy"));
                    writer.WriteElementString("sLoanEstimateDatesInfo_InitialLoanEstimate_DeliveryMethod", GetDescription(initialLoanEstimate.DeliveryMethod));
                    writer.WriteElementString("sLoanEstimateDatesInfo_InitialLoanEstimate_ReceivedDate", initialLoanEstimate.ReceivedDate.ToString("MM/dd/yyyy"));
                }

                var lastDisclosedLoanEstimate = dataLoan.GetLastDisclosedLoanEstimate();

                if (lastDisclosedLoanEstimate != null)
                {
                    writer.WriteElementString("sLoanEstimateDatesInfo_LastDisclosedLoanEstimate_CreatedDate", lastDisclosedLoanEstimate.CreatedDate.ToString("MM/dd/yyyy"));
                    writer.WriteElementString("sLoanEstimateDatesInfo_LastDisclosedLoanEstimate_IssuedDate", lastDisclosedLoanEstimate.IssuedDate.ToString("MM/dd/yyyy"));
                    writer.WriteElementString("sLoanEstimateDatesInfo_LastDisclosedLoanEstimate_DeliveryMethod", GetDescription(lastDisclosedLoanEstimate.DeliveryMethod));
                    writer.WriteElementString("sLoanEstimateDatesInfo_LastDisclosedLoanEstimate_ReceivedDate", lastDisclosedLoanEstimate.ReceivedDate.ToString("MM/dd/yyyy"));
                }
            }

            var sClosingDisclosureDatesInfo = dataLoan.sClosingDisclosureDatesInfo;
            if (sClosingDisclosureDatesInfo != null)
            {
                var initialClosingDisclosure = sClosingDisclosureDatesInfo.InitialClosingDisclosure;

                if (initialClosingDisclosure != null)
                {
                    writer.WriteElementString("sClosingDisclosureDatesInfo_InitialClosingDisclosure_CreatedDate", initialClosingDisclosure.CreatedDate.ToString("MM/dd/yyyy"));
                    writer.WriteElementString("sClosingDisclosureDatesInfo_InitialClosingDisclosure_IssuedDate", initialClosingDisclosure.IssuedDate.ToString("MM/dd/yyyy"));
                    writer.WriteElementString("sClosingDisclosureDatesInfo_InitialClosingDisclosure_DeliveryMethod", GetDescription(initialClosingDisclosure.DeliveryMethod));
                    writer.WriteElementString("sClosingDisclosureDatesInfo_InitialClosingDisclosure_ReceivedDate", initialClosingDisclosure.ReceivedDate.ToString("MM/dd/yyyy"));
                }

                var finalClosingDisclosure = sClosingDisclosureDatesInfo.FinalClosingDisclosure;

                if (finalClosingDisclosure != null)
                {
                    writer.WriteElementString("sClosingDisclosureDatesInfo_FinalClosingDisclosure_CreatedDate", finalClosingDisclosure.CreatedDate.ToString("MM/dd/yyyy"));
                    writer.WriteElementString("sClosingDisclosureDatesInfo_FinalClosingDisclosure_IssuedDate", finalClosingDisclosure.IssuedDate.ToString("MM/dd/yyyy"));
                    writer.WriteElementString("sClosingDisclosureDatesInfo_FinalClosingDisclosure_DeliveryMethod", GetDescription(finalClosingDisclosure.DeliveryMethod));
                    writer.WriteElementString("sClosingDisclosureDatesInfo_FinalClosingDisclosure_ReceivedDate", finalClosingDisclosure.ReceivedDate.ToString("MM/dd/yyyy"));

                }
            }

            // je 12/21/2018 - Add fields for NASA FCU.
            writer.WriteElementString("payment_and_interest", dataLoan.sProThisMPmt_rep);

            writer.WriteEndElement(); // </loan>
            #endregion
            #region <underwriting>
            writer.WriteStartElement("underwriting");

            #region <message_to_underwriter>
            // 05/23/2005 - dd OPM #1934.
            if ("" != dataLoan.sLpeNotesFromBrokerToUnderwriterHistory)
            {
                writer.WriteElementString("message_to_underwriter", LendersOffice.Common.Utilities.SafeHtmlString(dataLoan.sLpeNotesFromBrokerToUnderwriterHistory).Replace(Environment.NewLine, "<br>"));
            }
            #endregion



            if (broker.LpeSubmitAgreement != "")
            {
                writer.WriteElementString("confirmation_phrase", AspxTools.HtmlString(broker.LpeSubmitAgreement));
            }


            writer.WriteEndElement(); // </underwriting>
            #endregion

            #region <conditions>
            writer.WriteStartElement("conditions");
            if (false == broker.IsUseNewTaskSystem)
            {

                CConditionSetObsolete ds = new CConditionSetObsolete(dataLoan.sLId);

                foreach (CConditionObsolete cond in ds)
                {
                    if (includeCompletedConditions == false && cond.IsDone) continue; // OPM 24013

                    writer.WriteStartElement("condition");

                    // This should be obsolete.
                    // writer.WriteElementString("priority", cond.priority);

                    writer.WriteElementString("category", cond.CondCategoryDesc);

                    if (!cond.IsNote)
                    {
                        // 2/24/2005 kb - Only show done status when condition is
                        // not a note type task-condition.  We don't check if the
                        // broker has the lender default feature set turned on
                        // because we assume that all pml brokers have it.  If
                        // this is not so, add the check against the broker bit
                        // using a broker access info instance.

                        writer.WriteElementString("done", cond.IsDone.ToString());

                        if (cond.IsDone)
                        {
                            writer.WriteElementString("date_done", cond.DateDone.ToShortDateString());
                            writer.WriteElementString("cleared_by", cond.CondCompletedByUserNm);
                        }
                    }

                    writer.WriteElementString("subject", cond.CondDesc.Replace(Environment.NewLine, "<br>"));
                    //writer.WriteElementString("status", cond.Status);
                    writer.WriteEndElement(); // </condition>

                }
            }
            else
            {
                List<Task> ds = Task.GetActiveConditionsByLoanId(broker.BrokerID, dataLoan.sLId, false);
                foreach (Task cond in ds)
                {
                    if (includeCompletedConditions == false && cond.TaskStatus == E_TaskStatus.Closed) continue; // OPM 24013

                    writer.WriteStartElement("condition");

                    // This should be obsolete.
                    // writer.WriteElementString("priority", cond.priority);

                    writer.WriteElementString("category", cond.CondCategoryId_rep);


                    // 2/24/2005 kb - Only show done status when condition is
                    // not a note type task-condition.  We don't check if the
                    // broker has the lender default feature set turned on
                    // because we assume that all pml brokers have it.  If
                    // this is not so, add the check against the broker bit
                    // using a broker access info instance.

                    writer.WriteElementString("done", (cond.TaskStatus == E_TaskStatus.Closed).ToString());

                    if (cond.TaskStatus == E_TaskStatus.Closed && cond.TaskClosedDate.HasValue)
                    {
                        writer.WriteElementString("date_done", cond.TaskClosedDate.Value.ToShortDateString());
                        writer.WriteElementString("cleared_by", cond.ClosingUserFullName);
                    }


                    writer.WriteElementString("subject", cond.TaskSubject.Replace(Environment.NewLine, "<br>"));
                    //writer.WriteElementString("status", cond.Status);
                    writer.WriteEndElement(); // </condition>
                }
            }

            writer.WriteEndElement(); // </conditions>
            #endregion

            #region <sVisibleBrokerAdjustmentData>
            {
                writer.WriteStartElement("sVisibleBrokerAdjustmentData");

                var adjustmentData = dataLoan.sVisibleBrokerAdjustmentData;
                writer.WriteElementString("TotalRate", adjustmentData.TotalRate);
                writer.WriteElementString("TotalPoint", adjustmentData.TotalFee.Replace("%", ""));

                // 2/28/2012 dd - OPM 78785 merge visibile adjustments with the same certifate on rate lock certificate.
                Dictionary<string, _Item> dictionary = new Dictionary<string, _Item>(StringComparer.OrdinalIgnoreCase);

                // OPM 466973 - Parkside has a custom Rate-Lock confirmation that adds a column for the margin
                // to the adjustments table. This affects adjustment filtering.
                bool isParkside = broker.CustomerCode.Equals("PML0299", StringComparison.OrdinalIgnoreCase);

                foreach (var adjustment in adjustmentData.VisibleAdjustments)
                {
                    _Item item = null;
                    if (dictionary.TryGetValue(adjustment.Description, out item) == false)
                    {
                        item = new _Item();
                        item.Description = adjustment.Description;
                        dictionary.Add(adjustment.Description, item);
                    }
                    decimal rate = 0;
                    decimal.TryParse(adjustment.Rate.Replace("%", ""), out rate);
                    decimal fee = 0;
                    decimal.TryParse(adjustment.Fee.Replace("%", ""), out fee);
                    decimal margin = 0;
                    decimal.TryParse(adjustment.Margin.Replace("%", ""), out margin);

                    // OPM 231872 - Show adjustments with 0 rate and fee (unless canceled out as per OPM 78785).
                    // OPM 466973 - Filtering rules need to take margin into account, but only for Parkside.
                    if (rate == 0 && fee == 0 && (!isParkside || margin == 0))
                    {
                        item.hasAllZeroAdj = true;
                    }

                    item.Rate += rate;
                    item.Fee += fee;
                    item.Margin += margin;
                }

                foreach (var o in dictionary.Values.Where(p => p.Fee != 0 || p.Rate != 0
                    || (isParkside && p.Margin != 0) || p.hasAllZeroAdj))
                {
                    writer.WriteStartElement("adjustment");
                    writer.WriteAttributeString("Price", o.Fee.ToString("0.000"));
                    writer.WriteAttributeString("Rate", o.Rate.ToString("0.000") + "%");
                    writer.WriteAttributeString("Margin", o.Margin.ToString("0.000"));
                    writer.WriteAttributeString("Description", o.Description);
                    writer.WriteEndElement();

                }

                if (isParkside)
                {
                    bool oldBypass = dataLoan.ByPassFieldsBrokerLockAdjustXmlContent;

                    dataLoan.ByPassFieldsBrokerLockAdjustXmlContent = true;

                    // OPM 466973 - Parkside Rate Lock Confirmation specific fields.
                    writer.WriteElementString("sBrokerLockBrokerBaseNoteIR", dataLoan.sBrokerLockBrokerBaseNoteIR_rep);
                    writer.WriteElementString("sBrokerLockBrokerBaseBrokComp1PcFee", dataLoan.sBrokerLockBrokerBaseBrokComp1PcFee_rep.Replace("%", ""));
                    writer.WriteElementString("sBrokerLockBrokerBaseRAdjMarginR", dataLoan.sBrokerLockBrokerBaseRAdjMarginR_rep.Replace("%", ""));

                    if (dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees)
                    {
                        writer.WriteElementString("sOriginatorCompensationAmount", dataLoan.sOriginatorCompensationAmount_rep);
                        writer.WriteElementString("sBrokerLockOriginatorCompAdjNoteIR", dataLoan.sBrokerLockOriginatorCompAdjNoteIR_rep);
                        writer.WriteElementString("sBrokerLockOriginatorCompAdjBrokComp1PcFee", dataLoan.sBrokerLockOriginatorCompAdjBrokComp1PcFee_rep.Replace("%", ""));
                        writer.WriteElementString("sBrokerLockOriginatorCompAdjRAdjMarginR", dataLoan.sBrokerLockOriginatorCompAdjRAdjMarginR_rep.Replace("%", ""));

                        writer.WriteElementString("FinalPriceRate", dataLoan.sBrokerLockOriginatorPriceNoteIR_rep);
                        writer.WriteElementString("FinalPricePoint", dataLoan.sBrokerLockOriginatorPriceBrokComp1PcFee_rep.Replace("%", ""));
                        writer.WriteElementString("FinalPriceMargin", dataLoan.sBrokerLockOriginatorPriceRAdjMarginR_rep.Replace("%", ""));
                    }
                    else
                    {
                        writer.WriteElementString("FinalPriceRate", dataLoan.sNoteIR_rep);
                        writer.WriteElementString("FinalPricePoint", dataLoan.sBrokComp1Pc_rep.Replace("%", ""));
                        writer.WriteElementString("FinalPriceMargin", dataLoan.sRAdjMarginR_rep.Replace("%", ""));
                    }

                    dataLoan.ByPassFieldsBrokerLockAdjustXmlContent = oldBypass;
                }                
                
                writer.WriteEndElement();
            }
            #endregion

            #region approval conditions and suspense conditions
            {
                if (broker.IsUseNewTaskSystem)
                {
                    List<Task> conditionSet = Task.GetActiveConditionsByLoanId(broker.BrokerID, sLId, true);

                    if (broker.CustomerCode == "PML0229")
                    {
                        // 3/4/2014 - JMAC asked us to always order condition by category group and then text on certificate.
                        conditionSet = conditionSet.OrderBy(o => o.CondCategoryId_rep).ThenBy(o => o.TaskSubject).ToList();
                    }

                    GenerateConditionXml(writer,
                        conditionSet,
                        broker,
                        "approval_conditions",
                        includeCompletedConditions,
                        includeNonSuspenseConditions);

                    // Suspense conditions should come first, then use normal order.
                    var nonSuspenseConditions = conditionSet.Where(condition => condition.CondCategoryId_rep.ToUpper() != "SUSPENSE");
                    var conditionsForSuspenseNotice = conditionSet.Where(condition => condition.CondCategoryId_rep.ToUpper() == "SUSPENSE").ToList();
                    conditionsForSuspenseNotice.AddRange(nonSuspenseConditions);

                    GenerateConditionXml(writer,
                        conditionsForSuspenseNotice,
                        broker,
                        "suspense_conditions",
                        includeCompletedConditions,
                        includeNonSuspenseConditions);
                }
                else
                {
                    LoanConditionSetObsolete fullConditionSet = new LoanConditionSetObsolete();
                    fullConditionSet.Retrieve(sLId, false, true, false);
                    var oldTaskSystemConditions = fullConditionSet.Cast<CLoanConditionObsolete>();

                    GenerateConditionXml(writer,
                        oldTaskSystemConditions,
                        "approval_conditions",
                        includeCompletedConditions,
                        includeNonSuspenseConditions);

                    // Suspense conditions should come first, then use normal order.
                    var nonSuspenseConditions = oldTaskSystemConditions.Where(condition => condition.CondCategoryDesc.ToUpper() != "SUSPENSE");
                    var conditionsForSuspenseNotice = oldTaskSystemConditions.Where(condition => condition.CondCategoryDesc.ToUpper() == "SUSPENSE").ToList();
                    conditionsForSuspenseNotice.AddRange(nonSuspenseConditions);

                    GenerateConditionXml(writer,
                        conditionsForSuspenseNotice,
                        "suspense_conditions",
                        includeCompletedConditions,
                        includeNonSuspenseConditions);

                }
            }
            #endregion

            #region <pricing>
            writer.WriteStartElement("pricing");

            #region <rate_options>
            writer.WriteStartElement("rate_options");
            // 01/25/08 OPM 19525 mf.
            bool displayPmlFeeIn100Format = broker.HasFeatures(E_BrokerFeatureT.PriceMyLoan)
                ? dataLoan.sPriceGroup.DisplayPmlFeeIn100Format
                : broker.DisplayPmlFeeIn100Format;

            writer.WriteAttributeString("point_basis", displayPmlFeeIn100Format ? "100" : "");

            writer.WriteStartElement("rate_option");
            // 9/10/2011 dd - Hotfix - temporary by pass field security so it can retrieve sBrokerLockOriginatorPriceBrokComp1PcFee_rep without exception for Underwriter.
            dataLoan.ByPassFieldSecurityCheck = true;
            writer.WriteElementString("rate", dataLoan.sNoteIR_rep);
            writer.WriteElementString("point", LendersOfficeApp.los.RatePrice.CApplicantRateOption.PointFeeInResult(displayPmlFeeIn100Format, dataLoan.sBrokerLockOriginatorPriceBrokComp1PcFee_rep.Replace("%", "")));
            writer.WriteElementString("apr", dataLoan.sApr_rep);
            writer.WriteElementString("margin", dataLoan.sRAdjMarginR_rep);
            writer.WriteElementString("payment", dataLoan.sProThisMPmt_rep);

            if (dataLoan.sLTotI == 0)
            {
                writer.WriteElementString("ratio", "N/A");
            }
            else
            {
                writer.WriteElementString("ratio", $"{dataLoan.sQualTopR_rep} / {dataLoan.sQualBottomR_rep}");
            }

            writer.WriteEndElement(); // </rate_option>

            writer.WriteEndElement(); //<rate_options>
            #endregion

            writer.WriteEndElement(); // </pricing>
            #endregion

            #region OriginatorComp
            if (!broker.IsHideLOCompPMLCert && (dataLoan.sHasOriginatorCompensationPlan || dataLoan.sOriginatorCompensationPaymentSourceT != E_sOriginatorCompensationPaymentSourceT.LenderPaid))
            {
                writer.WriteStartElement("orig_comp");
                writer.WriteElementString("source", GetCompensationSourceDescription(dataLoan.sOriginatorCompensationPaymentSourceT));
                writer.WriteElementString("amt", dataLoan.sOriginatorCompForCert_rep);
                writer.WriteElementString("point", dataLoan.sOriginatorCompNetPoints_rep);
                writer.WriteElementString("plan_date", dataLoan.sOriginatorCompensationEffectiveD_rep);
                writer.WriteElementString("percent", dataLoan.sOriginatorCompensationPercent_rep);
                writer.WriteElementString("minimum", dataLoan.sOriginatorCompensationMinAmount_rep);
                writer.WriteElementString("minimum_set", dataLoan.sOriginatorCompensationHasMinAmount.ToString());
                writer.WriteElementString("maximum", dataLoan.sOriginatorCompensationMaxAmount_rep);
                writer.WriteElementString("maximum_set", dataLoan.sOriginatorCompensationHasMaxAmount.ToString());
                writer.WriteElementString("fixed", dataLoan.sOriginatorCompensationFixedAmount_rep);

                writer.WriteEndElement(); // </orig_comp>
            }


            #endregion

            #region <sPmlBroker>
            Guid sPmlBrokerId = dataLoan.sPmlBrokerId;
            if (sPmlBrokerId != Guid.Empty)
            {
                try
                {
                    // dd 8/3/2015 - OPM 210318 - For MWF custom cert
                    // je 12/21/2018 - OPM 476731 - Added more PML Broker fields for NASA FCU custom certs.
                    PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerById(sPmlBrokerId, dataLoan.sBrokerId);

                    writer.WriteStartElement("sPmlBroker");
                    writer.WriteElementString("Name", pmlBroker.Name);
                    writer.WriteElementString("Phone", pmlBroker.Phone);
                    writer.WriteElementString("street_address", pmlBroker.Addr);
                    writer.WriteElementString("citystatezip", Tools.CombineCityStateZip(pmlBroker.City, pmlBroker.State, pmlBroker.Zip));
                    writer.WriteEndElement(); // </sPmlBroker>
                }
                catch (PmlBrokerNotFoundException)
                {
                }

            }
            #endregion
            writer.WriteEndElement(); // </pml_loanview>

        }

        private static void GenerateConditionXml(XmlWriter writer, IEnumerable<Task> conditionSet, BrokerDB broker, string conditionsElementName, bool includeCompletedConditions, bool includeNonSuspenseConditions)
        {
            writer.WriteStartElement(conditionsElementName);
            int countCondition = 0;

            string lastGroup = null;
            bool endGroup = false;
            foreach (Task condition in conditionSet)
            {
                if (condition.CondIsDeleted)
                {
                    continue;
                }
                countCondition += 1;
                if (condition.CondIsHidden || (includeCompletedConditions == false && condition.TaskStatus == E_TaskStatus.Closed)
                    || (includeNonSuspenseConditions == false && condition.CondCategoryId_rep.ToUpper() != "SUSPENSE"))
                {
                    continue; //skip any deleted conditions
                }
                if (null == lastGroup || false == lastGroup.Equals(condition.CondCategoryId_rep, StringComparison.OrdinalIgnoreCase))
                {
                    //yes bit tricky but this is for a hotfix. 
                    //pretty much end the group if we started one.
                    if (endGroup)
                    {
                        writer.WriteEndElement();
                    }
                    //start a group because the category doesnt match the last one we saw and set to end group.
                    writer.WriteStartElement("group");
                    writer.WriteAttributeString("name", condition.CondCategoryId_rep);
                    lastGroup = condition.CondCategoryId_rep;
                    endGroup = true;
                }
                writer.WriteStartElement("condition");
                if (condition.CondCategoryId_rep.ToLower().TrimWhitespaceAndBOM() != "notes")
                {
                    // 2/24/2005 kb - Only show done status when condition is
                    // not a note type task-condition.  We don't check if the
                    // broker has the lender default feature set turned on
                    // because we assume that all pml brokers have it.  If
                    // this is not so, add the check against the broker bit
                    // using a broker access info instance.



                    if (condition.TaskStatus == E_TaskStatus.Closed && condition.TaskClosedDate.HasValue)
                        writer.WriteElementString("date_done", condition.TaskClosedDate.Value.ToShortDateString());
                }

                writer.WriteElementString("subject", condition.TaskSubject.Replace(Environment.NewLine, "<br>"));
                writer.WriteElementString("cleared_by", condition.ClosingUserFullName.ToUpper());
                if (broker.IsUseNewTaskSystemStaticConditionIds)
                {
                    writer.WriteElementString("number", condition.CondRowId.ToString());
                }
                else
                {
                    writer.WriteElementString("number", countCondition.ToString());
                }

                // 3/4/2014 - Used by JMAC.
                if (broker.CustomerCode == "PML0229")
                {
                    writer.WriteElementString("sort_num", countCondition.ToString());
                }
                writer.WriteEndElement(); // </condition>
            }

            //since the last condition group will never be ended end it here 
            //if there are no categories this will never be run.
            if (endGroup)
            {
                endGroup = false;
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        private static void GenerateConditionXml(XmlWriter writer, IEnumerable<CLoanConditionObsolete> fullConditionSet, string conditionsElementName, bool includeCompletedConditions, bool includeNonSuspenseConditions)
        {
            writer.WriteStartElement(conditionsElementName);
            int countCondition = 0;
            string lastGroup = null;
            bool endGroup = false;

            foreach (CLoanConditionObsolete cond in fullConditionSet)
            {
                if (cond.CondDeleteD.HasValue || cond.CondIsValid == false)
                {
                    continue;     //deleted conditions stay at their spot so just ignore them
                }
                countCondition += 1;
                if (cond.CondIsHidden || (includeCompletedConditions == false && cond.CondStatus == E_CondStatus.Done)
                    || (includeNonSuspenseConditions == false && cond.CondCategoryDesc.ToUpper() != "SUSPENSE"))     //skip any hidden conditions    we want the correct number though soo keep it there.
                {
                    continue;
                }
                if (lastGroup == null || false == lastGroup.Equals(cond.CondCategoryDesc, StringComparison.CurrentCultureIgnoreCase))
                {
                    //yes bit tricky but this is for a hotfix. 
                    //pretty much end the group if we started one.
                    if (endGroup)
                    {
                        writer.WriteEndElement();
                    }
                    //start a group because the category doesnt match the last one we saw and set to end group.
                    writer.WriteStartElement("group");
                    writer.WriteAttributeString("name", cond.CondCategoryDesc);
                    lastGroup = cond.CondCategoryDesc;
                    endGroup = true;
                }
                writer.WriteStartElement("condition");

                if (cond.CondCategoryDesc.ToLower().TrimWhitespaceAndBOM() != "notes")
                {
                    // 2/24/2005 kb - Only show done status when condition is
                    // not a note type task-condition.  We don't check if the
                    // broker has the lender default feature set turned on
                    // because we assume that all pml brokers have it.  If
                    // this is not so, add the check against the broker bit
                    // using a broker access info instance.



                    if (cond.CondStatus == E_CondStatus.Done)
                        writer.WriteElementString("date_done", cond.CondStatusDate.ToShortDateString());
                }

                writer.WriteElementString("subject", cond.CondDesc.Replace(Environment.NewLine, "<br>"));
                writer.WriteElementString("cleared_by", cond.CondCompletedByUserNm.ToUpper());
                writer.WriteElementString("number", countCondition.ToString());
                writer.WriteEndElement(); // </condition>
            }

            //since the last condition group will never be ended end it here 
            //if there are no categories this will never be run.
            if (endGroup)
            {
                endGroup = false;
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        #region Mapper methods

        private static string GetDescription(E_sQMStatusT sQMStatusT)
        {
            switch (sQMStatusT)
            {
                case E_sQMStatusT.Blank:
                    return string.Empty;
                case E_sQMStatusT.Eligible:
                    return "Eligible";
                case E_sQMStatusT.Ineligible:
                    return "Ineligible";
                case E_sQMStatusT.ProvisionallyEligible:
                    return "Provisionally Eligible";
                default:
                    throw new UnhandledEnumException(sQMStatusT);
            }
        }
        private static string GetDescription(E_DeliveryMethodT deliveryMethod)
        {
            switch (deliveryMethod)
            {
                case E_DeliveryMethodT.Email:
                    return "Email";
                case E_DeliveryMethodT.Fax:
                    return "Fax";
                case E_DeliveryMethodT.InPerson:
                    return "In Person";
                case E_DeliveryMethodT.LeaveEmpty:
                    return string.Empty;
                case E_DeliveryMethodT.Mail:
                    return "Mail";
                case E_DeliveryMethodT.Overnight:
                    return "Overnight";
                default:
                    throw new UnhandledEnumException(deliveryMethod);
            }
        }
        private static string GetDescription(E_RedisclosureMethodT sRedisclosureMethodT)
        {
            switch (sRedisclosureMethodT)
            {
                case E_RedisclosureMethodT.LeaveBlank:
                    return string.Empty;
                case E_RedisclosureMethodT.Email:
                    return "Email";
                case E_RedisclosureMethodT.Fax:
                    return "Fax";
                case E_RedisclosureMethodT.InPerson:
                    return "In Person";
                case E_RedisclosureMethodT.Mail:
                    return "Mail";
                case E_RedisclosureMethodT.Overnight:
                    return "Overnight";
                default:
                    throw new UnhandledEnumException(sRedisclosureMethodT);
            }
        }
        private static string GetProdBCitizenDescription(E_aProdCitizenT aProdCitizenT)
        {
            switch (aProdCitizenT)
            {
                case E_aProdCitizenT.ForeignNational: return "Non-Resident Alien (Foreign National)";
                case E_aProdCitizenT.NonpermanentResident: return "Non-permanent Resident";
                case E_aProdCitizenT.PermanentResident: return "Permanent Resident";
                case E_aProdCitizenT.USCitizen: return "US Citizen";
                default:
                    return "";
            }
        }
        private static string GetLPurposeDescription(E_sLPurposeT sLPurposeT)
        {
            switch (sLPurposeT)
            {
                case E_sLPurposeT.Construct: return "Construct";
                case E_sLPurposeT.ConstructPerm: return "Construct Perm";
                case E_sLPurposeT.Other: return "Other";
                case E_sLPurposeT.Purchase: return "Purchase";
                case E_sLPurposeT.Refin: return "Refin";
                case E_sLPurposeT.RefinCashout: return "Refin Cashout";
                case E_sLPurposeT.FhaStreamlinedRefinance: return "FHA Streamlined Refi";
                case E_sLPurposeT.VaIrrrl: return "VA IRRRL";
                case E_sLPurposeT.HomeEquity: return "Home Equity";
                default:
                    return "";
            }
        }

        private static string GetPropertyPurposeDescription(E_sOccT sOccT)
        {
            switch (sOccT)
            {
                case E_sOccT.Investment: return "Investment";
                case E_sOccT.PrimaryResidence: return "Primary Residence";
                case E_sOccT.SecondaryResidence: return "Secondary Residence";
            }
            return "";
        }

        private static string GetLienPositionDescription(E_sLienPosT sLienPosT)
        {
            switch (sLienPosT)
            {
                case E_sLienPosT.First: return "First Lien";
                case E_sLienPosT.Second: return "Second Lien";
            }
            return "";
        }

        private static string GetLoanProductTypeDescription(E_sLT sLT)
        {
            switch (sLT)
            {
                case E_sLT.Conventional:
                    return "Conventional";
                case E_sLT.FHA:
                    return "FHA";
                case E_sLT.UsdaRural:
                    return "USDA/Rural Housing";
                case E_sLT.Other:
                    return "Other";
                case E_sLT.VA:
                    return "VA";
            }
            return "";
        }
        private static string GetFinanceMethodDescription(E_sFinMethT sFinMethT)
        {
            switch (sFinMethT)
            {
                case E_sFinMethT.Fixed:
                    return "Fixed";
                case E_sFinMethT.ARM:
                    return "ARM";
                case E_sFinMethT.Graduated:
                    return "Graduated";
            }
            return "";
        }

        private static string GetMortgageInsuranceDescription(CPageData dataLoan, bool HasEnabledPMI)
        {
            switch (dataLoan.sLT)
            {
                case E_sLT.FHA:
                    return "FHA";
                case E_sLT.VA:
                    return "VA";
                case E_sLT.UsdaRural:
                    return "USDA/Rural Housing";
                case E_sLT.Conventional:
                case E_sLT.Other:
                default:
                    if (HasEnabledPMI)
                    {
                        return dataLoan.sProdConvMIOptionT_rep;
                    }
                    else
                    {
                        switch (dataLoan.sProdMIOptionT)
                        {
                            case E_sProdMIOptionT.BorrowerPdPmi:
                                return "Borrower Paid";
                            case E_sProdMIOptionT.NoPmi:
                                return "No MI / LPMI / Other";
                            case E_sProdMIOptionT.LeaveBlank:
                                return "N/A";
                            default:
                                throw new UnhandledEnumException(dataLoan.sProdMIOptionT);
                        }
                    }
            }
        }



        private static string GetCompensationSourceDescription(E_sOriginatorCompensationPaymentSourceT sOriginatorCompensationPaymentSourceT)
        {
            switch (sOriginatorCompensationPaymentSourceT)
            {
                case E_sOriginatorCompensationPaymentSourceT.SourceNotSpecified:
                    return "Not Specified";
                case E_sOriginatorCompensationPaymentSourceT.BorrowerPaid:
                    return "Borrower";
                case E_sOriginatorCompensationPaymentSourceT.LenderPaid:
                    return "Lender";
                default:
                    throw new UnhandledEnumException(sOriginatorCompensationPaymentSourceT);
            }
        }
        #endregion

    }
}
