﻿namespace LendersOffice.RatePrice.Helpers
{
    using System.Text;

    public class OutputBuilder
    {
        private StringBuilder m_sbMessages = new StringBuilder();
        private StringBuilder m_sbUntouchedPolicies = new StringBuilder();
        private StringBuilder m_sbUnmodifiedPolicies = new StringBuilder();
        private StringBuilder m_sbPoliciesAdded = new StringBuilder();
        private StringBuilder m_sbPoliciesUpdated = new StringBuilder();
        private StringBuilder m_sbIgnoredText = new StringBuilder();


        public void WriteToMessage(string sText)
        {
            Write(m_sbMessages, sText);
        }
        public void WriteToUntouchPolicies(string sText)
        {
            Write(m_sbUntouchedPolicies, sText);
        }
        public void WriteToUnmodifiedPolicies(string sText)
        {
            Write(m_sbUnmodifiedPolicies, sText);
        }
        public void WriteToPoliciesAdded(string sText)
        {
            Write(m_sbPoliciesAdded, sText);
        }
        public void WriteToPoliciesUpdated(string sText)
        {
            Write(m_sbPoliciesUpdated, sText);
        }
        public void WriteToIgnoredText(string sText)
        {
            Write(m_sbIgnoredText, sText);
        }

        public string MessageText
        {
            get { return m_sbMessages.ToString(); }
        }
        public string UntouchedPoliciesText
        {
            get { return m_sbUntouchedPolicies.ToString(); }
        }
        public string UnmodifiedPoliciesText
        {
            get { return m_sbUnmodifiedPolicies.ToString(); }
        }
        public string PoliciesAddedText
        {
            get { return m_sbPoliciesAdded.ToString(); }
        }
        public string PoliciesUpdatedText
        {
            get { return m_sbPoliciesUpdated.ToString(); }
        }
        public string IgnoredText
        {
            get { return m_sbIgnoredText.ToString(); }
        }
        private void Write(StringBuilder sb, string sText)
        {
            sb.Append(string.Format("{0}\n", sText));
        }
    }

}