﻿namespace LendersOffice.RatePrice.Helpers
{
    using System;

    /// <summary>
    /// Container for info for manual disabled investor products.
    /// </summary>
    public class ManualDisabledInvestorProductInfo
    {
        /// <summary>
        /// Gets the broker id.
        /// </summary>
        /// <value>The broker id.</value>
        public Guid BrokerId
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the investor name.
        /// </summary>
        /// <value>The investor name.</value>
        public string InvestorName
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the product code.
        /// </summary>
        /// <value>The product code.</value>
        public string ProductCode
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the disable status as a string.
        /// </summary>
        /// <value>The disable status as a string.</value>
        public string DisableStatus
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the message to submitting users.
        /// </summary>
        /// <value>The message to submitting users.</value>
        public string MessageToSubmittingUsers
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the date this product was disabled.
        /// </summary>
        /// <value>The date this product was disabled.</value>
        public DateTime DisableEntryModifiedDate
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the employee id of the one who disabled the product.
        /// </summary>
        /// <value>The employee id of the one who disabled the product.</value>
        public Guid DisableEntryModifiedByEmployeeId
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the user name of the employee who disabled the product.
        /// </summary>
        /// <value>The user name of the employee who disabled the product.</value>
        public string DisableEntryModifiedByUserName
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the disabled date time.
        /// </summary>
        /// <value>The disabled date time.</value>
        public DateTime DisableDateTime
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets a value indicating whether this should be hidden from results.
        /// </summary>
        /// <value>A value indicating whether this should be hidden from results.</value>
        public bool IsHiddenFromResult
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the notes for this product.
        /// </summary>
        /// <value>The notes for this product.</value>
        public string Notes
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the lock policy id.
        /// </summary>
        /// <value>The lock policy id.</value>
        public Guid LockPolicyId
        {
            get;
            internal set;
        }
    }
}
