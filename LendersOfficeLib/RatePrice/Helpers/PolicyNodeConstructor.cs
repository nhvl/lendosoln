﻿namespace LendersOffice.RatePrice.Helpers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOfficeApp.los.RatePrice;

    /// <summary>
    /// This class constructs PricePolicy objects
    /// </summary>
    public class PolicyNodeConstructor
    {
        public readonly int MaxNestedPoliciesDepth = 50;

        private Guid m_PolicyId = Guid.Empty;

        private bool m_IsTouched = false;
        private bool m_IsDirty = false;
        private bool m_IsNew = false;

        private Guid m_DataParentPolicyId = Guid.Empty;
        private string m_DataDescription = "";
        private bool m_DataIsPublished = false;
        private string m_DataMutualExclusiveExecSortedId = "";
        private IEnumerable<ParsedRuleInfo> m_DataRules;
        PolicyNodeConstructor m_pNode = null; //pointer to the Parent Node (if any).

        private CPricePolicy m_CachedPricePolicy = null;

        ArrayList m_ChildPolicyNodeList = new ArrayList();

        public static PolicyNodeConstructor GetRootPolicy(Guid policyId)
        {
            CPricePolicy pricePolicy = new CPricePolicy(policyId);
            return new PolicyNodeConstructor(pricePolicy);
        }

        public void UpdateChild(ParsedPolicyInfo parsedInfo)
        {
            UpdateChild(parsedInfo, 0);
        }

        public void UpdateDb(OutputBuilder output, Guid brokerId, bool bReadOnly)
        {
            if (!m_IsTouched)
                output.WriteToUntouchPolicies(string.Format("Policy {0}::{1}", m_PolicyId.ToString(), m_DataDescription));
            else if (!m_IsDirty)
                output.WriteToUnmodifiedPolicies(string.Format("Policy {0}::{1}", m_PolicyId.ToString(), m_DataDescription));
            else
            {
                // 10/24/2006 mf - OPM 7883 We should only prepare to save if we are
                // not in readonly (verify) mode.
                if (!bReadOnly)
                {
                    if (!m_IsNew)
                    {
                        if (m_CachedPricePolicy == null)
                            m_CachedPricePolicy = new CPricePolicy(m_PolicyId);
                    }
                    else
                        m_CachedPricePolicy = CPricePolicy.CreateNewPricePolicyWithPredfineId(m_PolicyId, brokerId, m_DataParentPolicyId);

                    m_CachedPricePolicy.PricePolicyDescription = m_DataDescription;
                    m_CachedPricePolicy.IsPublished = m_DataIsPublished;
                    m_CachedPricePolicy.MutualExclusiveExecSortedId = m_DataMutualExclusiveExecSortedId;

                    m_CachedPricePolicy.DeleteAllRules();

                    foreach (ParsedRuleInfo ruleInfo in m_DataRules)
                    {
                        CRule rule = m_CachedPricePolicy.AddNewRule(ruleInfo.RuleId);

                        rule.Description = ruleInfo.Description;
                        rule.Condition.UserExpression = ruleInfo.Condition;
                        rule.QBC = ruleInfo.QBC;    //opm 25872 fs 03/24/09
                        rule.Consequence.Skip = ruleInfo.Skip;
                        rule.Consequence.Disqualify = ruleInfo.Disqualify;
                        rule.Consequence.Stipulation = ruleInfo.Stip;
                        rule.Consequence.RateAdjustTarget.UserExpression = ruleInfo.Rate;
                        rule.Consequence.MarginAdjustTarget.UserExpression = ruleInfo.Margin;
                        rule.Consequence.FeeAdjustTarget.UserExpression = ruleInfo.Fees;
                        rule.Consequence.QltvAdjustTarget.UserExpression = ruleInfo.QLTV;
                        rule.Consequence.QcltvAdjustTarget.UserExpression = ruleInfo.QCLTV;
                        rule.Consequence.MaxFrontEndYspAdjustTarget.UserExpression = ruleInfo.MaxFrontEndYsp;
                        rule.Consequence.MaxYspAdjustTarget.UserExpression = ruleInfo.MaxBackEndYsp;
                        rule.Consequence.QscoreTarget.UserExpression = ruleInfo.QScore;
                        rule.Consequence.QLAmtAdjustTarget.UserExpression = ruleInfo.QLAmt;
                        rule.Consequence.MaxDtiTarget.UserExpression = ruleInfo.MaxDTI;
                        rule.Consequence.QrateAdjustTarget.UserExpression = ruleInfo.QRateAdj;
                        rule.Consequence.TeaserRateAdjustTarget.UserExpression = ruleInfo.TeaserRate;

                        rule.IsLenderRule = ruleInfo.LenderRule; //opm 32459 fs 09/10/09
                        rule.Consequence.LockDaysAdjustTarget.UserExpression = ruleInfo.LockDaysAdj; //opm 17988 fs 09/29/09
                    }

                    try
                    {
                        m_CachedPricePolicy.Save();
                    }
                    catch (SqlException exc)
                    {
                        //2627 is primary key violation error --> an existing policy with the same id exists.
                        if (exc.Number == 2627)
                        {
                            string dupPolicyName = "";
                            try
                            {
                                dupPolicyName = LoadDupPolicy(m_PolicyId);
                            }
                            catch { }

                            output.WriteToMessage(string.Format("Policy {0}::{1} NOT ADDED. Existing policy {2}has the same id.", m_PolicyId.ToString(), m_DataDescription, dupPolicyName + " "));
                        }
                        throw;
                    }

                }

                if (m_IsNew)
                    output.WriteToPoliciesAdded(string.Format("Policy {0}::{1}", m_PolicyId.ToString(), m_DataDescription));
                else
                    output.WriteToPoliciesUpdated(string.Format("Policy {0}::{1}", m_PolicyId.ToString(), m_DataDescription));
            }


            foreach (PolicyNodeConstructor child in m_ChildPolicyNodeList)
                child.UpdateDb(output, brokerId, bReadOnly);
        }
        public string Description
        {
            get { return m_DataDescription; }
        }

        public Guid policyId
        {
            get { return m_PolicyId; }
        }
        //opm 8084 fs 07/28/08
        public StringCollection namePath
        {
            get
            {
                StringCollection str = new StringCollection();
                str.Add(this.Description);
                PolicyNodeConstructor node = m_pNode;
                int times = 0;

                while (node != null && times < MaxNestedPoliciesDepth)
                {
                    str.Insert(0, node.Description);
                    node = node.parentNode;
                    times++;
                }
                if (times < MaxNestedPoliciesDepth)
                    return str;
                else
                    throw new CBaseException(string.Format("Error in the tree containing policy {0}.", this.policyId.ToString()), string.Format("Recursive 'namePath' loop containing policy {0} entered too many times. Only nested paths with a degree up to {2} are supported.", this.policyId.ToString(), MaxNestedPoliciesDepth));
            }
        }
        public PolicyNodeConstructor parentNode
        {
            get { return m_pNode; }
            set
            {
                if (value != this)
                    m_pNode = value;
            }
        }
        public PolicyNodeConstructor rootNode
        {
            get
            {
                int times = 0;
                PolicyNodeConstructor node = m_pNode;
                while (node != null && node.parentNode != null && times < MaxNestedPoliciesDepth)
                {
                    node = node.parentNode;
                    times++;
                }
                if (times < MaxNestedPoliciesDepth)
                    return node;
                else
                    throw new CBaseException(string.Format("Error in the tree containing policy {0}.", this.policyId.ToString()), string.Format("Recursive 'rootNode' loop containing policy {0} entered too many times. Only nested paths with a max degree up to {2} are supported.", this.policyId.ToString(), MaxNestedPoliciesDepth));
            }
        }

        private bool isPolicyNameUpdate(StringCollection existing, StringCollection current)
        {
            if (existing.Count != current.Count)
                return false;

            if (existing.Count > 1)
            {
                for (int i = 0; i < existing.Count - 1; i++)
                {
                    if (existing[i].ToUpper() != current[i].ToUpper())
                        return false;
                }
            }
            return true;
        }

        private bool isPolicyNameUpdate(PolicyNodeConstructor existing, ParsedPolicyInfo parsed)
        {
            PolicyNodeConstructor root = (existing.rootNode != null) ? existing.rootNode : existing;

            if (root != existing && root.Description.ToUpper() != parsed.PolicyNamePath[0].ToUpper())
            {
                //no root name included; insert it in the beginning
                StringCollection strCol = new StringCollection();
                strCol.Add(root.Description);
                foreach (string str in parsed.PolicyNamePath)
                    strCol.Add(str);

                return isPolicyNameUpdate(existing.namePath, strCol);
            }

            return isPolicyNameUpdate(existing.namePath, parsed.PolicyNamePath);
        }
        //end opm 8084 fs 07/28/08

        //opm 16523 fs 08/05/08
        private string LoadDupPolicy(Guid policyId)
        {
            var listParams = new SqlParameter[] { new SqlParameter("@policyId", policyId) };
            string sSql = "SELECT PricePolicyId, PricePolicyDescription, ParentPolicyId FROM PRICE_POLICY WHERE PricePolicyId = @policyId";

            string name = string.Empty;
			Action<IDataReader> readHandler = delegate (IDataReader sdr)
			{
				if (sdr.Read())
				{
					Guid id = new Guid(sdr["PricePolicyId"].ToString());
					name = sdr["PricePolicyDescription"].ToString();
					Guid parent = (sdr["ParentPolicyId"] != System.DBNull.Value) ? new Guid(sdr["ParentPolicyId"].ToString()) : Guid.Empty;

					if (parent != Guid.Empty)
						name = LoadDupPolicy(parent) + "::" + name;
				}
				else
				{
					throw new CBaseException(ErrorMessages.Generic, "Primary Key violation error found, but a duplicate could not be located in the PRICE_POLICY table.");
				}
			};

			DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sSql, null, listParams, readHandler);

			return name;
		}

		private PolicyNodeConstructor(Guid parentPolicyId, Guid policyId)
        {
            m_PolicyId = policyId;
            m_DataParentPolicyId = parentPolicyId;

            LoadChildren();
        }
        private PolicyNodeConstructor(Guid parentPolicyId, Guid policyId, string sDescription, bool IsPublished, string sMutualExclusiveExecSortedId)
        {
            m_PolicyId = policyId;
            m_DataParentPolicyId = parentPolicyId;
            m_DataDescription = sDescription.ToUpper().TrimWhitespaceAndBOM();
            m_DataIsPublished = IsPublished;
            m_DataMutualExclusiveExecSortedId = sMutualExclusiveExecSortedId;

            LoadChildren();
        }
        private PolicyNodeConstructor(CPricePolicy pricePolicy)
        {
            m_PolicyId = pricePolicy.PricePolicyId;
            m_DataParentPolicyId = pricePolicy.ParentPolicyId;
            m_DataDescription = pricePolicy.PricePolicyDescription;
            m_DataIsPublished = pricePolicy.IsPublished;
            m_DataMutualExclusiveExecSortedId = pricePolicy.MutualExclusiveExecSortedId;
            m_CachedPricePolicy = pricePolicy;

            LoadChildren();
        }
        private void UpdateChild(ParsedPolicyInfo parsedInfo, int nDepth)
        {
            if (parsedInfo.PolicyNamePath.Count - 1 != nDepth) // branch node
            {
                PolicyNodeConstructor child;

                if (parsedInfo.PolicyNamePath[nDepth].ToUpper() == this.Description.ToUpper())
                    child = this;
                else
                    child = FindChildByDescription(parsedInfo.PolicyNamePath[nDepth]);

                if (child == null)
                {
                    string sPath = null;
                    for (int i = 0; i <= nDepth; i++)
                    {
                        if (sPath == null)
                            sPath = parsedInfo.PolicyNamePath[i];
                        else
                            sPath += "::" + parsedInfo.PolicyNamePath[i];
                    }
                    throw new CBaseException(string.Format("Price Policy {0} does not exist", sPath), string.Format("Price Policy {0} does not exist", sPath));
                }

                child.parentNode = this;
                child.UpdateChild(parsedInfo, nDepth + 1);
            }
            else // leaf node
            {
                PolicyNodeConstructor child;
                if (parsedInfo.PolicyId == this.m_PolicyId)
                    child = this; // This statement should only be executed if we're updating the ROOT node
                else
                    child = FindChildByDescription(parsedInfo.PolicyNamePath[nDepth]);

                if (child != null)
                {
                    // DO NOT ADD 2 POLICY with the SAME NAME or MAJOR problems will occur because we navigate down to child nodes via name paths
                    if (child.m_PolicyId != parsedInfo.PolicyId)
                        throw new CBaseException(string.Format("Duplicate Price Policy {0} found in parent and cannot be added", parsedInfo.PolicyNamePath[nDepth]), string.Format("Duplicate Price Policy {0} found in parent and cannot be added", parsedInfo.PolicyNamePath[nDepth]));
                }
                else
                    child = FindChildById(parsedInfo.PolicyId);

                if (child == null)
                {
                    child = new PolicyNodeConstructor(m_PolicyId, parsedInfo.PolicyId);
                    child.m_IsNew = true;
                    child.m_IsDirty = true;
                    m_ChildPolicyNodeList.Add(child);
                }

                child.parentNode = this;

                //opm 8084 fs 07/26/08
                if (!isPolicyNameUpdate(child, parsedInfo))
                    throw new CBaseException(string.Format("Duplicate Price Policy {0} found in parent and cannot be added", parsedInfo.PolicyNamePath[parsedInfo.PolicyNamePath.Count - 1]), string.Format("Policy with id {0} cannot be added as another policy with the same id has been found in a different branch.", parsedInfo.PolicyId));

                child.m_IsTouched = true;
                if (child.m_DataDescription != parsedInfo.PolicyNamePath[parsedInfo.PolicyNamePath.Count - 1])
                {
                    child.m_IsDirty = true;
                    child.m_DataDescription = parsedInfo.PolicyNamePath[parsedInfo.PolicyNamePath.Count - 1];
                }
                if (child.m_DataIsPublished != parsedInfo.IsPublished)
                {
                    child.m_IsDirty = true;
                    child.m_DataIsPublished = parsedInfo.IsPublished;
                }
                if (child.m_DataMutualExclusiveExecSortedId != parsedInfo.MutualExclusiveExecSortedId)
                {
                    child.m_IsDirty = true;
                    child.m_DataMutualExclusiveExecSortedId = parsedInfo.MutualExclusiveExecSortedId;
                }

                child.m_DataRules = parsedInfo.GetRules();

                // check to see if this Policy is dirty at the Rules level (note: New Policies by default are dirty)
                if (!child.m_IsDirty)
                {
                    child.m_CachedPricePolicy = new CPricePolicy(child.m_PolicyId);

                    if (child.m_CachedPricePolicy.Rules.Count != child.m_DataRules.Count())
                        child.m_IsDirty = true;
                    else
                    {
                        for (int i = 0; i < child.m_CachedPricePolicy.Rules.Count; i++)
                        {
                            CRule rule = (CRule)child.m_CachedPricePolicy.Rules.GetRule(i);
                            ParsedRuleInfo parsedRuleInfo = child.m_DataRules.ElementAt(i);

                            if (parsedRuleInfo == null)
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.RuleId != rule.RuleId)
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.Condition != rule.Condition.UserExpression)
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.Description != rule.Description)
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.Disqualify != rule.Consequence.Disqualify)
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.Fees != rule.Consequence.FeeAdjustTarget.UserExpression)
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.Margin != rule.Consequence.MarginAdjustTarget.UserExpression)
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.MaxFrontEndYsp != rule.Consequence.MaxFrontEndYspAdjustTarget.UserExpression)
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.MaxBackEndYsp != rule.Consequence.MaxYspAdjustTarget.UserExpression)
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.QLTV != rule.Consequence.QltvAdjustTarget.UserExpression)
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.QCLTV != rule.Consequence.QcltvAdjustTarget.UserExpression)
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.QScore != rule.Consequence.QscoreTarget.UserExpression)
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.Rate != rule.Consequence.RateAdjustTarget.UserExpression)
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.Stip != rule.Consequence.Stipulation)
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.Skip != rule.Consequence.Skip)
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.QLAmt != rule.Consequence.QLAmtAdjustTarget.UserExpression)
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.MaxDTI != rule.Consequence.MaxDtiTarget.UserExpression)
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.QRateAdj != rule.Consequence.QrateAdjustTarget.UserExpression)
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.TeaserRate != rule.Consequence.TeaserRateAdjustTarget.UserExpression)
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.QBC != rule.QBC)    //opm 25872 fs 03/24/09
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.LenderRule != rule.IsLenderRule)   //opm 32459 fs 09/08/09
                                child.m_IsDirty = true;
                            else if (parsedRuleInfo.LockDaysAdj != rule.Consequence.LockDaysAdjustTarget.UserExpression)   //opm 17988 fs 09/29/09
                                child.m_IsDirty = true;

                            if (child.m_IsDirty)
                                break;
                        }
                    }
                }
            }
        }

        private void LoadChildren()
        {
            var listParams = new SqlParameter[] { new SqlParameter("@parentPolcyId", this.m_PolicyId) };
            string sSql = "SELECT PricePolicyId, PricePolicyDescription, IsPublished, MutualExclusiveExecSortedId FROM PRICE_POLICY WHERE ParentPolicyId = @parentPolcyId";

            Action<IDataReader> readHandler = delegate(IDataReader sdr)
            {
                while (sdr.Read())
                {
                    PolicyNodeConstructor child = new PolicyNodeConstructor(policyId, new Guid(sdr["PricePolicyId"].ToString()), sdr["PricePolicyDescription"].ToString(), Convert.ToInt32(sdr["IsPublished"]) == 1, sdr["MutualExclusiveExecSortedId"].ToString());
                    this.m_ChildPolicyNodeList.Add(child);
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sSql, null, listParams, readHandler);
        }

        private PolicyNodeConstructor FindChildByDescription(string sDescription)
        {
            foreach (PolicyNodeConstructor child in m_ChildPolicyNodeList)
            {
                if (child.m_DataDescription == sDescription)
                    return child;
            }
            return null;
        }
        private PolicyNodeConstructor FindChildById(Guid policyId)
        {
            foreach (PolicyNodeConstructor child in m_ChildPolicyNodeList)
            {
                if (child.m_PolicyId == policyId)
                    return child;
            }
            return null;
        }
    }
}