﻿namespace LendersOffice.RatePrice.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOfficeApp.los.RatePrice;

    public enum FlexError
    {
        None = 0,
        InvalidVar = 1,
        MissingOperator = 2, // missing " = " or " += "
        MissingVar = 3, // for example " = 0.125"
        MissingValue = 4, // for example "QRATE = "
        InvalidComma = 5, // for example "QRATE, QSCORE = 0.500"
        InvalidSemicolon = 6, // for example "QSCORE = 0.500;"
        AlreadySet = 7, // for example "QRATE = 0.500; QRATE = 0.500;"
        Unknow  = 8,
    }

    public class PolicyParser
    {
        //private fields
        private string m_textToParse;
        private OutputBuilder m_output;
        private Guid m_root;
        private Guid m_brokerId;
        private IEnumerable<ParsedPolicyInfo> m_parsedPolicyList;
        private bool m_WriteDB = false;

        private const char COMMA_SEPARATOR = ',';
        private const char TAB_SEPARATOR = '\t';
        private char m_separator;

        //Constructor
        public PolicyParser(string inputText, OutputBuilder output, Guid root, Guid broker, bool save)
        {
            if (inputText == null || output == null || root == Guid.Empty || broker == Guid.Empty)
                throw new ArgumentNullException();
            
            m_textToParse = inputText;
            m_output = output;
            m_root = root;
            m_brokerId = broker;
            m_WriteDB = save;
            m_separator = TAB_SEPARATOR;

        }

        public PolicyParser()
        {

        }

        public void Process()
        {
            m_parsedPolicyList = GetPolicies();

            if (m_parsedPolicyList == null)
            {
                m_output.WriteToMessage("!!!Processing aborted!!!");
                return;
            }
            
            BuildPolicyTree(m_root);

        }

        public IEnumerable<ParsedPolicyInfo> GetPolicies(StreamReader streamToParse, char separator, List<string> aErrorList, List<string> aIgnoredTextList)
        {
            string textToParse = "";
            if (!streamToParse.EndOfStream)
            {
                textToParse = streamToParse.ReadToEnd();
            }
            
            return GetPolicies(textToParse, separator, aErrorList, aIgnoredTextList);
        }

        private IEnumerable<ParsedPolicyInfo> GetPolicies(string textToParse, char separator, List<string> aErrorList, List<string> aIgnoredTextList)
        {
            List<ParsedPolicyInfo> policyList = new List<ParsedPolicyInfo>(); // processed Policies
            int nLineNo = 0;
            bool HasErrors = false;
            ParsedPolicyInfo currentPolicyInfo = null;

            string[] rgRecords = textToParse.Split('\n');

            //foreach line
            foreach (string sRecord in rgRecords)
            {
                nLineNo++;

                if (sRecord.Length == 0) continue;  //skip empty record
                string[] fields = ImportStreamReader.smartSplit(sRecord, separator);

                if (fields.Length <= 1) continue;   //skip record with single column
                
                string Type = fields[0].ToUpper();

                switch (Type)
                {
                    case "POLICY":
                        {
                            string error = "";
                            ParsedPolicyInfo parsedPolicyInfo = ParsePolicyEntry(fields, out error);
                            if (error != "" || parsedPolicyInfo == null)
                            {
                                string policyId = (parsedPolicyInfo == null) ? "" : parsedPolicyInfo.PolicyId.ToString();
                                if (aErrorList != null) aErrorList.Add(string.Format("Line {0}: Policy {1} has {2}.", nLineNo, policyId, error));
                                HasErrors = true;
                                continue; //Parse next line
                            }

                            policyList.Add(parsedPolicyInfo); // No error found: add Policy to the List
                            currentPolicyInfo = parsedPolicyInfo;
                            break;
                        }
                    case "RULE":
                        {
                            string error = "";

                            if (currentPolicyInfo == null) //Stray rules are not allowed
                            {
                                if (aErrorList != null) aErrorList.Add(string.Format("Line {0}: Cannot associate Rule {1} with a PricePolicy", nLineNo, fields[1]));
                                HasErrors = true;
                                continue; //Parse next line
                            }

                            ParsedRuleInfo parsedRuleInfo = ParseRuleEntry(fields, out error);

                            if (parsedRuleInfo == null || error != "")
                            {
                                if (aErrorList != null)  aErrorList.Add(string.Format("Line {0}: {1}", nLineNo, error));
                                HasErrors = true;
                                continue; //Parse next line
                            }

                            try
                            {
                                currentPolicyInfo.AddRule(parsedRuleInfo);
                            }
                            catch (CBaseException exc)
                            {
                                if (aErrorList != null) aErrorList.Add(string.Format("Line {0}: {1}", nLineNo, exc.UserMessage));
                                HasErrors = true;
                                continue;
                            }
                            break;
                        }
                    case "": //opm 8035 fs 11/03/09
                        {
                            string error = "TYPE column cannot be empty. Acceptable values are: RULE / POLICY / 'non empty comments'.";
                            if (aErrorList != null) aErrorList.Add(string.Format("Line {0}: {1}", nLineNo, error));
                            HasErrors = true;
                            continue; //Parse next line
                        }
                    default:
                            if (aIgnoredTextList != null) aIgnoredTextList.Add(string.Format("{0}: {1}", nLineNo, sRecord));
                        break;
                }
            }

            if (HasErrors)
                return null;
            else
                return policyList;
        }

        /// <summary>
        /// Parse the user input into an ArrayList of ParsedPolicyInfo
        /// </summary>
        private IEnumerable<ParsedPolicyInfo> GetPolicies()
        {
            List<string> aErrorList = new List<string>();
            List<string> aTextToIgnoreList = new List<string>();
            IEnumerable<ParsedPolicyInfo> policies = null;
            
            //Call actual implementation
            policies = GetPolicies(m_textToParse, m_separator, aErrorList, aTextToIgnoreList);

            foreach (string str in aErrorList)
            {
                m_output.WriteToMessage(str);
            }

            foreach (string str in aTextToIgnoreList)
            {
                m_output.WriteToIgnoredText(str);
            }

            return policies;
        }

        private void BuildPolicyTree(Guid rootNode)
        {
            if (rootNode == Guid.Empty)
                throw new ArgumentNullException();

            try
            {
                // ensure that Parent Policy Id is valid. Return error if it's not
                PolicyNodeConstructor root = PolicyNodeConstructor.GetRootPolicy(rootNode);

                foreach (ParsedPolicyInfo parsedPolicy in m_parsedPolicyList)
                {
                    root.UpdateChild(parsedPolicy);
                }

                root.UpdateDb(m_output, m_brokerId, m_WriteDB);
                m_output.WriteToMessage("!!!Processing completed SUCCESSFULLY!!!");
            }
            catch (Exception e)
            {
                m_output.WriteToMessage(e.Message);
                m_output.WriteToMessage("!!!Processing aborted!!!");
            }
        }


        /// <summary>
        /// Parse a single user input line into a policy ParsedPolicyInfo object.
        /// </summary>
        private ParsedPolicyInfo ParsePolicyEntry(string[] fields, out string error)
        {
            ParsedPolicyInfo parsedPolicyInfo = new ParsedPolicyInfo();
            StringCollection path = new StringCollection();

            Guid pId3 = Guid.Empty;
            Guid.TryParse(fields[3], out pId3);

            if (pId3 == Guid.Empty)
            {
                error = "an invalid PolicyID";

                Guid pId2 = Guid.Empty;
                Guid.TryParse(fields[2], out pId2);

                if (pId2 != Guid.Empty)
                {
                    error += ". A valid ID (" + pId2.ToString() + ") was found in the previous column. Is this template missing the QBC column?";
                }

                return null;
            }

            //new format
            //COLUMNS EXPECTED: TYPE, QBC, DESCR, POLICYID, AT, GRD               
            parsedPolicyInfo.PolicyId = new Guid(fields[3]);
            parsedPolicyInfo.PolicyNamePath = ParseNameWithPath(fields[2]);
            parsedPolicyInfo.IsPublished = SafeField(fields, 4).TrimWhitespaceAndBOM().ToUpper() == "X";
            parsedPolicyInfo.MutualExclusiveExecSortedId = SafeField(fields, 5).TrimWhitespaceAndBOM();

            // verify name validity
            if (parsedPolicyInfo.PolicyNamePath.Count == 0)
            {
                error = "an invalid name";
                return parsedPolicyInfo;
            }

            foreach (string sName in parsedPolicyInfo.PolicyNamePath)
            {
                if (sName.Length == 0)
                {
                    error = "a zero-length name in the path";
                    return parsedPolicyInfo;
                }
            }

            //No error found
            error = "";
            return parsedPolicyInfo;
        }

        static Dictionary<CAdjustTarget.E_Target, int> s_TargetToIdxDict = new Dictionary<CAdjustTarget.E_Target, int>()
        {
            { CAdjustTarget.E_Target.Rate, 6},
            { CAdjustTarget.E_Target.Fee, 7},
            { CAdjustTarget.E_Target.Margin, 8},
            { CAdjustTarget.E_Target.QrateAdjust, 9},
            { CAdjustTarget.E_Target.TeaserRateAdjust, 10},
            { CAdjustTarget.E_Target.MaxYsp, 11},
            { CAdjustTarget.E_Target.MaxFrontEndYsp, 12},
            { CAdjustTarget.E_Target.MaxDti, 14},
            { CAdjustTarget.E_Target.Qscore, 16},
            { CAdjustTarget.E_Target.Qltv, 17},
            { CAdjustTarget.E_Target.Qcltv, 18},
            { CAdjustTarget.E_Target.QLAmt, 19},
            { CAdjustTarget.E_Target.LockDaysAdjust, 22},
        };

        static Dictionary<CAdjustTarget.E_Target, int> s_FlexTargetToIdxDict = new Dictionary<CAdjustTarget.E_Target, int>()
        {
            { CAdjustTarget.E_Target.Rate, 6},
            { CAdjustTarget.E_Target.Fee, 7},
            { CAdjustTarget.E_Target.Margin, 8},
                // CAdjustTarget.E_Target.QrateAdjust
                // CAdjustTarget.E_Target.TeaserRateAdjust
            { CAdjustTarget.E_Target.MaxYsp, 9},
            { CAdjustTarget.E_Target.MaxFrontEndYsp, 10},
            { CAdjustTarget.E_Target.MaxDti, 12},
                // CAdjustTarget.E_Target.Qscore
                // CAdjustTarget.E_Target.Qltv
                // CAdjustTarget.E_Target.Qcltv
                // CAdjustTarget.E_Target.QLAmt
            { CAdjustTarget.E_Target.LockDaysAdjust, 16},
        };

        static Dictionary<string, CAdjustTarget.E_Target> s_flexVarToAdjTargetDict = new Dictionary<string, CAdjustTarget.E_Target>()
        {
            {"QRATE", CAdjustTarget.E_Target.QrateAdjust},
            {"TRATE" , CAdjustTarget.E_Target.TeaserRateAdjust},
            {"QSCORE", CAdjustTarget.E_Target.Qscore},
            {"QLTV", CAdjustTarget.E_Target.Qltv},
            {"QCLTV", CAdjustTarget.E_Target.Qcltv},
            {"QLAMT", CAdjustTarget.E_Target.QLAmt},
        };

        static HashSet<CAdjustTarget.E_Target> s_flexSupportedTargets = new HashSet<CAdjustTarget.E_Target>(s_flexVarToAdjTargetDict.Values);

        /// <summary>
        /// Parse a single user input line into a rule ParsedRuleInfo object.
        /// </summary>
        private ParsedRuleInfo ParseRuleEntry(string[] fields, out string error)
        {
            XmlDocument xmlDocEval = new XmlDocument();
            bool isFlex = false;
            Dictionary<CAdjustTarget.E_Target, string> flexDict = null;


            Func<CAdjustTarget.E_Target, string> getAdjustStr = (CAdjustTarget.E_Target target) =>
            {
                string fieldValue = string.Empty;
                var targetToIdxDict = isFlex ? s_FlexTargetToIdxDict : s_TargetToIdxDict;

                if (isFlex && s_flexSupportedTargets.Contains(target))
                {
                    if (flexDict.ContainsKey(target))
                    {
                        fieldValue = flexDict[target];
                    }
                }
                else
                {
                    try
                    {
                        int fldIdx = targetToIdxDict[target];
                        fieldValue = SafeField(fields, fldIdx);
                    }
                    catch
                    {
                        throw new CBaseException(ErrorMessages.Generic, $"Can not get data for adjust target {target}");
                    }
                }

                var adjustTargetObj = CAdjustTarget.CreateTestAdjustTarget(target, fieldValue);
                adjustTargetObj.EvaluateTest(xmlDocEval);

                return fieldValue;
            };

            ParsedRuleInfo parsedRuleInfo = new ParsedRuleInfo();

            try
            {
                // 10/09/06 mf OPM  7682. Two versions of input data.  
                // 12/01/06    OPM  7686. Added QRateBase & TeaserRate and use newer version only.
                // 03/24/09 fs opm 25872. Added QBC column. Two versions of input data.

                // Non-Flex version
                // LABEL FIELD
                // 0 A   TYPE
                // 1 B   QBC        //NEW COLUMN
                // 2 C	 DESCRIPTION
                // 3 D   POLICYID/CONDITION
                // 4 E   ATTACH/SKIP
                // 5 F   GRADE/DISQUAL
                // 6 G   RATE
                // 7 H   FEE
                // 8 I   MARGIN
                //
                // 9 J   QRATEADJST
                // 10 K  TEASERRATE
                // 11 L  BACKENDMAXYSP
                // 12 M  FRONTENDMAXYSP
                // 13 N  STIP
                // 14 O  MAXDTI
                // 15 P  DTI RANGE
                // 16 Q  QSCORE
                // 17 R  QLTV
                // 18 S  QCLTV
                // 19 T  QLAMT
                // 20 U  RULEID
                // 21 V  IS LENDER RULE
                // 22 W  LOCK DAYS ADJST

                // Flex version 
                // 9 first columns are same as non-flex version
                //
                //  9 J  BACK-END MAX YSP	
                // 10 K  FRONT-END MAX YSP	
                // 11 L  STIPS	
                // 12 M  MAXDTI	
                // 13 N  FLEX	
                // 14 O  RULEID	
                // 15 P  IS LENDER RULE	
                // 16 Q  LOCK DAYS	

                Guid ruleId = Guid.Empty;

                if (fields.Length <= 20 || !Guid.TryParse(SafeField(fields, 20), out ruleId))
                {
                    if (Guid.TryParse(SafeField(fields, 14), out ruleId))
                    {
                        isFlex = true;
                    }
                }

                if (ruleId == Guid.Empty)
                {
                    Guid pOld = Guid.Empty;
                    string errMsg = "RuleId not found. Guid should contain 32 digits with 4 dashes (xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx).";

                    if (Guid.TryParse(SafeField(fields, 19), out pOld))
                    {
                        errMsg += " A valid guid (" + pOld.ToString() + ") was found in the previous column. Is this template missing the FrontEndMaxYSP column?";
                    }

                    throw new CBaseException(ErrorMessages.Generic, errMsg);
                }

                int QbcIndex = 1,
                        descrIndex = 2,
                        condIndex = 3,
                        skipIndex = 4,
                        DisqIndex = 5,

                        //rateIndex = 6,
                        //feeIndex = 7,
                        //marginIndex = 8,
                        //    QrateAdjIndex = 9,
                        //    TeaserRateIndex = 10,
                        //MaxBackEndYspIndex = 11,
                        //MaxFrontEndYspIndex = 12,

                        stipIndex = 13,
                        //MaxDTIIndex = 14,
                        //DTIRangeIndex = 15, -- dd 11/11/2016 - We are no longer import DTIRangeIndex into rule object, but we still need to preserve the index for SAE import file.
                        //QScoreIndex = 16,
                        //QLTVIndex = 17,
                        //QCLTVIndex = 18,
                        //QLAmtIndex = 19,
                        ruleIDIndex = 20,
                        //After ruleID come Lender investor fields
                        LenderRuleIndex = 21;
                //LockDaysAdjIndex = 22;


                if (isFlex)
                {
                    stipIndex = 11;
                    ruleIDIndex = 14;
                    LenderRuleIndex = 15;

                    int flexIdx = 13;

                    try
                    {
                        flexDict = FlexParse( SafeField(fields, flexIdx));
                    }
                    catch(FlexParserException exc)
                    {
                        throw new CBaseException("Flex parser error: " + exc.Message, "Flex parser error: " + exc.Message + Environment.NewLine + "Flex field: " + SafeField(fields, flexIdx));
                    }

                }

                // ruleID
                parsedRuleInfo.RuleId = new Guid(SafeField(fields, ruleIDIndex));

                // description
                parsedRuleInfo.Description = SafeField(fields, descrIndex).TrimWhitespaceAndBOM();
                if (parsedRuleInfo.Description.Length == 0) throw new CBaseException("Description cannot be empty", "Description cannot be empty");

                // condition
                parsedRuleInfo.Condition = SafeField(fields, condIndex).TrimWhitespaceAndBOM();
                if (parsedRuleInfo.Condition.Length == 0) throw new CBaseException("Condition cannot be empty", "Condition cannot be empty");
                try
                {
                    CCondition condition = CCondition.CreateTestCondition(parsedRuleInfo.Condition);
                    condition.EvaluateTest(xmlDocEval);
                }
                catch
                {
                    throw new CBaseException("Invalid condition: " + parsedRuleInfo.Condition, "Invalid condition: " + parsedRuleInfo.Condition);
                }

                // skip
                parsedRuleInfo.sSkip = SafeField(fields, skipIndex).ToUpper();

                // disqualify
                parsedRuleInfo.sDisqualify = SafeField(fields, DisqIndex).ToUpper();


                // stip text
                parsedRuleInfo.Stip = SafeField(fields, stipIndex);

                // rate
                parsedRuleInfo.Rate = getAdjustStr(CAdjustTarget.E_Target.Rate);

                // margin
                parsedRuleInfo.Margin = getAdjustStr(CAdjustTarget.E_Target.Margin);

                // fee
                parsedRuleInfo.Fees = getAdjustStr(CAdjustTarget.E_Target.Fee);

                // QLTV
                parsedRuleInfo.QLTV = getAdjustStr(CAdjustTarget.E_Target.Qltv);

                // QCLTV
                parsedRuleInfo.QCLTV = getAdjustStr(CAdjustTarget.E_Target.Qcltv);

                // Max Back End YSP
                parsedRuleInfo.MaxBackEndYsp = getAdjustStr(CAdjustTarget.E_Target.MaxYsp);

                // Max Front End YSP
                parsedRuleInfo.MaxFrontEndYsp = getAdjustStr(CAdjustTarget.E_Target.MaxFrontEndYsp);

                // QSCORE
                parsedRuleInfo.QScore = getAdjustStr(CAdjustTarget.E_Target.Qscore);

                // QLAmt
                parsedRuleInfo.QLAmt = getAdjustStr(CAdjustTarget.E_Target.QLAmt);

                // MaxDTI
                parsedRuleInfo.MaxDTI = getAdjustStr(CAdjustTarget.E_Target.MaxDti);

                // DTIRange - 11/11/2016 - dd - DTIRange is not implement. Therefore remove.

                // QRateAdj
                parsedRuleInfo.QRateAdj = getAdjustStr(CAdjustTarget.E_Target.QrateAdjust);

                // TeaserRate
                parsedRuleInfo.TeaserRate = getAdjustStr(CAdjustTarget.E_Target.TeaserRateAdjust);

                //Is Lender Rule? 09/11/09 fs opm 32459
                parsedRuleInfo.sLenderRule = SafeField(fields, LenderRuleIndex).ToUpper();
                if (!parsedRuleInfo.isValidLenderRule)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Rule " + parsedRuleInfo.RuleId + " has an invalid 'Lender Rule' value. Accepted values are 'X' or '' (empty); found value: " + parsedRuleInfo.sLenderRule + ".");
                }
                

                //LockDaysAdj 09/29/09 fs opm 17988
                parsedRuleInfo.LockDaysAdj = getAdjustStr(CAdjustTarget.E_Target.LockDaysAdjust);

                if (!parsedRuleInfo.isValidLockDaysAdj)
                    throw new Exception("LockDaysAdj expression cannot contain 'LockDays' keyword (RuleId:" + parsedRuleInfo.RuleId + ").");


                // QBC
                //If using the new format gather the value from its column; otherwise assume an empty value.
                string sQBC = SafeField(fields, QbcIndex).TrimWhitespaceAndBOM();
                try
                {
                    parsedRuleInfo.parseQBC(sQBC);
                }
                catch (CBaseException)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Rule " + parsedRuleInfo.RuleId + " has an invalid QBC. Accepted values are 'P','A','C','blank'; found value: " + sQBC + ".");
                }

                //The QBC validation has to be done AFTER all other Rule fields have already been assigned.
                string errorMsg = "";
                if (!parsedRuleInfo.isQbcValid(out errorMsg))
                {
                    throw new CBaseException(ErrorMessages.Generic, errorMsg);
                }

                //Rule validation. At least one action field needs to be set, otherwise it's an invalid rule.
                errorMsg = "";
                if (!parsedRuleInfo.hasAction(out errorMsg))
                {
                    throw new CBaseException(ErrorMessages.Generic, errorMsg);
                }

                //Keywords that created excluded rules in step 3 should never be used in Qrules or in skip rules
                errorMsg = "";
                if (!parsedRuleInfo.skipAndQRulesLackExcludedKeywords(out errorMsg))
                {
                    throw new CBaseException(ErrorMessages.Generic, errorMsg);
                }

                // Case 102974 If a rules condition contains one or more of the q-keywords (QSCORE, QLAMT, 
                // QLTV, QCLTV), then don't validate the rule upload if rule is attempting to set a value in 
                // columns Q (QSCORE), R (QLTV), S (QCLTV), or T (QLAMT).
                errorMsg = "";
                if (!parsedRuleInfo.qKeywordsDontSetQColumns(out errorMsg))
                {
                    throw new CBaseException(ErrorMessages.Generic, errorMsg);
                }
            }
            catch (CBaseException e)
            {
                Tools.LogError(e);
                error = e.DeveloperMessage;
                return null;
            }
            catch (Exception e)
            {
                error = e.Message;
                Tools.LogError(e);
                return null;
            }

            error = "";
            return parsedRuleInfo;
        }

        private StringCollection ParseNameWithPath(string sNameWithPath)
        {
            StringCollection names = new StringCollection();
            int nStart = 0;
            do
            {
                int nPos = sNameWithPath.IndexOf("::", nStart);

                string sName;
                if (nPos > -1)
                    sName = sNameWithPath.Substring(nStart, nPos - nStart);
                else
                    sName = sNameWithPath.Substring(nStart);

                names.Add(sName.TrimWhitespaceAndBOM().ToUpper());
                nStart += sName.Length + 2;
            }
            while (nStart < sNameWithPath.Length);

            return names;
        }

        private string SafeField(string[] rg, int index)
        {
            if (rg.Length <= index)
                return "";

            string s = rg[index];
            if (s == null || s.Length == 0)
                return "";
            else if (s[s.Length - 1] == 13) // strip out a trailing carriage return (it screws up with our logic)
                return s.Substring(0, s.Length - 1);
            else
                return s;
        }

        static public Dictionary<CAdjustTarget.E_Target, string> FlexParse( string flexData)
        {
            Dictionary<CAdjustTarget.E_Target, string> result = new Dictionary<CAdjustTarget.E_Target, string>();

            flexData = (flexData ?? string.Empty).Trim();
            if (string.IsNullOrEmpty(flexData))
            {
                return result;
            }

            FlexError errCode = FlexError.Unknow;

            // Require space before and after comma, semicolon. The comma, semicolon cannot start or end of flexData.
            // Moreover the following code also detects the invalid string: ";   +" or ";   =" or ",   +" or ",   ="
            Match match = Regex.Match(flexData, @"\S+[;,]|[;,]\S+|^[;,]|[;,]$|;\s+;|,\s+,");
            if( match.Success )
            {
                if (match.Value[0] == ';' || match.Value[0] == ',')
                {
                    errCode = (match.Value[0] == ';') ? FlexError.InvalidSemicolon : FlexError.InvalidComma;
                }
                else
                {
                    char lastChar = match.Value[match.Value.Length - 1];
                    if (lastChar == ';' || lastChar == ',')
                    {
                        errCode = (lastChar == ';') ? FlexError.InvalidSemicolon : FlexError.InvalidComma;
                    }
                }

                throw new FlexParserException(errCode, $"Please review '{match.Value}' in '{flexData}'");
            }

            match = Regex.Match(flexData, @"[;,]\s+[+]*=|^\s*[+]*=");
            if (match.Success)
            {
                throw new FlexParserException(FlexError.MissingVar, $"Please review '{match.Value}' in '{flexData}'");

            }

            var statements = flexData.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var statement in statements)
            {
                string str = statement.Trim();
                if (string.IsNullOrEmpty(str))
                {
                    continue; // expect : never happen.
                }

                bool haveOp = false;
                string left = string.Empty, right = string.Empty;
                foreach (var delimiter in new string[] { " += ", " = " })
                {
                    int pos = str.IndexOf(delimiter);
                    if (pos > 0)
                    {
                        left = str.Substring(0, pos).Trim();
                        right = str.Substring(pos + delimiter.Length).Trim();
                        haveOp = true;
                        break;
                    }
                }

                if (!haveOp)
                {
                    throw new FlexParserException(FlexError.MissingOperator, $"Missing operator ' = ' or ' += ' in '{statement}'");
                }

                if (string.IsNullOrEmpty(right))
                {
                    throw new FlexParserException(FlexError.MissingValue, $"Missing value in '{statement}'");
                }

                var variables = left.Split(new[] { ',' , ' '}, StringSplitOptions.RemoveEmptyEntries);
                if (variables.Length == 0)
                {
                    throw new FlexParserException(FlexError.MissingVar, $"Missing data point in '{statement}'");
                }

                foreach (var variable in variables)
                {
                    CAdjustTarget.E_Target target;
                    if(!s_flexVarToAdjTargetDict.TryGetValue(variable, out target) )
                    {
                        throw new FlexParserException(FlexError.InvalidVar, $"Invalid data point '{variable}'  in '{statement}'");
                    }

                    if (result.ContainsKey(target))
                    {
                        throw new FlexParserException(FlexError.AlreadySet, $"The {variable} data point is being set to multiple values in  '{statement}'.");
                    }

                    result.Add(target, right);
                }
            }

            return result;
        }

        static public FlexError FlexTryParse(string flexData, out Dictionary<CAdjustTarget.E_Target, string> result, out string errMsg) // for unit test
        {
            try
            {
                result = FlexParse(flexData);
                errMsg = string.Empty;
                return FlexError.None;
            }
            catch(FlexParserException exc)
            {
                errMsg = exc.Message;
                result = null;
                return exc.ErrorCode;
            }
        }

        class FlexParserException : Exception
        {
            public FlexError ErrorCode { get; private set; }

            public FlexParserException(FlexError errorCode, string msg) : base(msg)
            {
                this.ErrorCode = errorCode;
            }
        }
    }
}
