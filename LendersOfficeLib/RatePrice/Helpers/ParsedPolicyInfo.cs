﻿namespace LendersOffice.RatePrice.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using DataAccess;

    public class ParsedPolicyInfo
    {
        public Guid PolicyId { get; set; }
        public StringCollection PolicyNamePath { get; set; }
        public bool IsPublished { get; set; }
        public string MutualExclusiveExecSortedId { get; set; }

        public IEnumerable<ParsedRuleInfo> GetRules() { return m_Rules; }
        public void AddRule(ParsedRuleInfo rule)
        {
            if (m_DupCheck.Contains(rule.RuleId))
                throw new CBaseException(string.Format("Duplicate RULEID '{0}' found in the same POLICY", rule.RuleId.ToString()), string.Format("Duplicate RULEID '{0}' found in the same POLICY", rule.RuleId.ToString()));

            m_Rules.Add(rule);
            m_DupCheck.Add(rule.RuleId);
        }

        private List<ParsedRuleInfo> m_Rules = new List<ParsedRuleInfo>(); // this is not a Hashtable because I want these items to be added in the order they are defined in the Excel file
        private HashSet<Guid> m_DupCheck = new HashSet<Guid>();
    }
}