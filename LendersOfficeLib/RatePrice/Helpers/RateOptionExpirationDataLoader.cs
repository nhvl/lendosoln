﻿namespace LendersOffice.RatePrice.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Xml.Linq;
    using Admin;
    using Constants;
    using DataAccess;
    using ObjLib.RatesheetExpiration;

    /// <summary>
    /// Class that loads data to determine rate option expiration.
    /// </summary>
    public class RateOptionExpirationDataLoader
    {
        /// <summary>
        /// The data loader for determining investor expiration.
        /// </summary>
        private InvestorExpirationDataLoader investorExpirationDataLoader = null;

        /// <summary>
        /// Mapping from rate sheet file id to rate sheet file info from LPE_ACCEPTABLE_RS_FILE.
        /// </summary>
        private Lazy<Dictionary<string, RateSheetFileInfo>> lazyRsFileIdToAcceptableRsFileInfo;

        /// <summary>
        /// Mapping from investor name/product code to rate sheet feature info.
        /// </summary>
        private Lazy<Dictionary<string, RateSheetExpirationFeatureInfo>> investorNameAndCodeToRateSheetFeatureInfo;

        /// <summary>
        /// The manual disabled investor products if they have been migrated from the broker.
        /// </summary>
        private Lazy<Dictionary<int, ManualDisabledInvestorProductInfo>> migratedManualDisabledInvestorProductInfo;

        /// <summary>
        /// Whether data should be bulk loaded or not.
        /// </summary>
        private bool shouldBulkLoad = true;

        /// <summary>
        /// The lender that the rate option expiration information belong to.
        /// </summary>
        private BrokerDB broker = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="RateOptionExpirationDataLoader"/> class.
        /// </summary>
        /// <param name="investorNamesForRateLockCutOff">The investor names used for loading the rate lock cut off info.</param>
        /// <param name="rateSheetFileIdsForFileInfo">The rate sheet file ids to pull file info.</param>
        /// <param name="investorCodePairs">The investor product code pairs for loading rate sheet expiration info. First item is the investor. Second is the product code.</param>
        /// <param name="broker">The broker.</param>
        /// <param name="shouldBulkLoad">Wether this loader should bulk load data or not.</param>
        public RateOptionExpirationDataLoader(
            IEnumerable<string> investorNamesForRateLockCutOff, 
            IEnumerable<string> rateSheetFileIdsForFileInfo, 
            IEnumerable<Tuple<string, string>> investorCodePairs, 
            BrokerDB broker,
            bool shouldBulkLoad = true)
        {
            this.broker = broker;
            this.shouldBulkLoad = shouldBulkLoad;
            this.investorExpirationDataLoader = new InvestorExpirationDataLoader(this.broker, investorNamesForRateLockCutOff, rateSheetFileIdsForFileInfo, shouldBulkLoad);
            this.investorNameAndCodeToRateSheetFeatureInfo = new Lazy<Dictionary<string, RateSheetExpirationFeatureInfo>>(() => this.InitializeRateSheetExpirationFeatureInfo(investorCodePairs));
            this.migratedManualDisabledInvestorProductInfo = new Lazy<Dictionary<int, ManualDisabledInvestorProductInfo>>(() => this.InitializeMigratedManualDisabledInvestorProductInfo(this.broker.BrokerID));
            this.lazyRsFileIdToAcceptableRsFileInfo = new Lazy<Dictionary<string, RateSheetFileInfo>>(() => this.InitializeAcceptableRsFileInfo(rateSheetFileIdsForFileInfo));
        }

        /// <summary>
        /// Gets the data loader for investor expiration.
        /// </summary>
        /// <value>The data loader for investor expiration.</value>
        public InvestorExpirationDataLoader InvestorExpirationDataLoader
        {
            get
            {
                return this.investorExpirationDataLoader;
            }
        }

        /// <summary>
        /// Gets the investor product info if they have been migrated.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="investorName">The investor name.</param>
        /// <param name="productCode">The product code.</param>
        /// <param name="lockPolicyId">The lock policy id.</param>
        /// <param name="shouldLog">Whether we should log missing entries.</param>
        /// <returns>The investor product info.</returns>
        public ManualDisabledInvestorProductInfo GetMigratedManualDisabledInvestorProductInfo(Guid brokerId, string investorName, string productCode, Guid lockPolicyId, bool shouldLog)
        {
            ManualDisabledInvestorProductInfo info = null;
            if (!this.shouldBulkLoad)
            {
                info = this.GetMigratedManualDisabledInvestorProductInfoSingleUse(brokerId, investorName, productCode, lockPolicyId);
            }
            else
            {
                if (lockPolicyId == Guid.Empty)
                {
                    info = this.migratedManualDisabledInvestorProductInfo.Value.Values.FirstOrDefault((prodInfo) => prodInfo.BrokerId == brokerId && prodInfo.InvestorName == investorName && prodInfo.ProductCode == productCode);
                }
                else
                {
                    if (!this.migratedManualDisabledInvestorProductInfo.Value.TryGetValue(GetKeyForMigratedManualDisabledProduct(brokerId, investorName, productCode, lockPolicyId), out info))
                    {
                        info = null;
                    }
                }
            }

            if (shouldLog && info == null)
            {
                Tools.LogWarning($"GetMigratedManualDisabledInvestorProductInfo. Info not found. BrokerId: {brokerId}, investorName: {investorName}, productCode: {productCode}, lockPolicyId: {lockPolicyId}");
            }

            return info;
        }

        /// <summary>
        /// Gets the rate sheet feature info.
        /// </summary>
        /// <param name="productCode">The product code.</param>
        /// <param name="investorName">The investor name.</param>
        /// <param name="shouldLog">Whether missing entries should be logged.</param>
        /// <returns>The rate sheet feature info if it exists. Null otherwise.</returns>
        public RateSheetExpirationFeatureInfo GetRateSheetExpirationFeatureInfo(string productCode, string investorName, bool shouldLog)
        {
            RateSheetExpirationFeatureInfo info = null;
            if (this.shouldBulkLoad)
            {
                if (!this.investorNameAndCodeToRateSheetFeatureInfo.Value.TryGetValue($"{productCode}_{investorName}", out info))
                {
                    info = null;
                }
            }
            else
            {
               info = this.GetRateSheetExpirationFeatureInfoSingleUse(productCode, investorName);
            }

            if (shouldLog && info == null)
            {
                Tools.LogWarning($"GetRateSheetExpirationFeatureInfo. Info not found. productCode: {productCode}, investorName: {investorName}");
            }

            return info;
        }

        /// <summary>
        /// Gets the rate sheet file info.
        /// </summary>
        /// <param name="rateSheetFileId">The rate sheet id.</param>
        /// <param name="shouldLog">Whether missing entries should be logged.</param>
        /// <returns>The rate sheet file info.</returns>
        public RateSheetFileInfo GetAcceptableRsFileInfo(string rateSheetFileId, bool shouldLog)
        {
            RateSheetFileInfo info = null;
            if (this.shouldBulkLoad && ConstStage.ShouldBulkLoadRateSheetInfoForROExpiration)
            {
                if (!this.lazyRsFileIdToAcceptableRsFileInfo.Value.TryGetValue(rateSheetFileId, out info))
                {
                    info = null;
                }
            }
            else
            {
                info = this.GetAcceptableRsFileInfoSingleUse(rateSheetFileId);
            }

            if (shouldLog && info == null)
            {
                Tools.LogWarning($"GetAcceptableRsFileInfo. Info not found. rateSheetFileId: {rateSheetFileId}");
            }

            return info;
        }

        /// <summary>
        /// Gets the key for migrated investor product info.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="investorName">The investor name.</param>
        /// <param name="productCode">The product code.</param>
        /// <param name="lockPolicyid">The lock policy id.</param>
        /// <returns>A hash for migrated products.</returns>
        private static int GetKeyForMigratedManualDisabledProduct(Guid brokerId, string investorName, string productCode, Guid lockPolicyid)
        {
            int key = 17;
            key = (key * 23) + brokerId.GetHashCode();
            key = (key * 23) + investorName.GetHashCode();
            key = (key * 23) + productCode.GetHashCode();
            key = (key * 23) + lockPolicyid.GetHashCode();
            return key;
        }

        /// <summary>
        /// Gets the rate sheet expiration feature for the given product code and investor name.
        /// Do not use this in a loop.
        /// </summary>
        /// <param name="productCode">The product code.</param>
        /// <param name="investorName">The investor name.</param>
        /// <returns>The rate sheet expiration feature info. Null if it doesn't exist.</returns>
        private RateSheetExpirationFeatureInfo GetRateSheetExpirationFeatureInfoSingleUse(string productCode, string investorName)
        {
            if (this.broker.ActualPricingBrokerId == Guid.Empty)
            {
                SqlParameter[] parameters =
                                            {
                                            new SqlParameter("@InvestorName", investorName),
                                            new SqlParameter("@ProductCode", productCode)
                                        };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "GetRateSheetExpirationFeatureByProductCodeInvestorName", parameters))
                {
                    if (reader.Read())
                    {
                        bool isRateSheetExpirationFeatureDeployed = (bool)reader["IsRateSheetExpirationFeatureDeployed"];
                        return new RateSheetExpirationFeatureInfo(productCode, investorName, isRateSheetExpirationFeatureDeployed);
                    }
                }
            }
            else
            {
                // The client is using file based pricing for all pricing run.
                List<Tuple<string, string>> investorProductCodePair = new List<Tuple<string, string>>();
                investorProductCodePair.Add(new Tuple<string, string>(investorName, productCode));

                var list = RateSheetExpirationFeatureInfo.List(this.broker, investorProductCodePair, isUseLpeDatabase: false);
                if (list.Any())
                {
                    return list.ElementAt(0);
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the manual disabled investor product info if it has been migrated.
        /// For single use only. Do not use in a loop.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="investorName">The investor name.</param>
        /// <param name="productCode">The product code.</param>
        /// <param name="lockPolicyId">The lock policy id.</param>
        /// <returns>The investor product info.</returns>
        private ManualDisabledInvestorProductInfo GetMigratedManualDisabledInvestorProductInfoSingleUse(Guid brokerId, string investorName, string productCode, Guid lockPolicyId)
        {
            string procedureName;
            var parameters = new List<SqlParameter>()
            {
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@InvestorName", investorName),
                    new SqlParameter("@ProductCode", productCode),
            };

            if (lockPolicyId == Guid.Empty)
            {
                procedureName = "RetrieveManualDisabledInvestorProductPerBrokerAllPolicies";
            }
            else
            {
                parameters.Add(new SqlParameter("@PolicyId", lockPolicyId));
                procedureName = "RetrieveManualDisabledInvestorProductPerPolicy";
            }

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, procedureName, parameters))
            {
                if (reader.Read())
                {
                    string invName = (string)reader["InvestorName"];
                    string code = (string)reader["ProductCode"];
                    string disableStatus = (string)reader["DisableStatus"];
                    string messagesToSubmittingUsers = (string)reader["MessagesToSubmittingUsers"];
                    DateTime disableEntryModifiedDate = (DateTime)reader["DisableEntryModifiedDate"];
                    Guid disableEntryModifiedByEmployeeId = (Guid)reader["DisableEntryModifiedByEmployeeId"];
                    string disableEntryModifiedByUserName = (string)reader["DisableEntryModifiedByUserName"];
                    DateTime disableDateTime = (DateTime)reader["DisableDateTime"];
                    bool isHiddenFromResult = (bool)reader["IsHiddenFromResult"];
                    string notes = (string)reader["Notes"];
                    Guid policyId = (Guid)reader["LockPolicyId"];

                    ManualDisabledInvestorProductInfo productInfo = new ManualDisabledInvestorProductInfo()
                    {
                        InvestorName = invName,
                        ProductCode = code,
                        DisableStatus = disableStatus,
                        MessageToSubmittingUsers = messagesToSubmittingUsers,
                        DisableEntryModifiedDate = disableEntryModifiedDate,
                        DisableEntryModifiedByEmployeeId = disableEntryModifiedByEmployeeId,
                        DisableEntryModifiedByUserName = disableEntryModifiedByUserName,
                        DisableDateTime = disableDateTime,
                        IsHiddenFromResult = isHiddenFromResult,
                        Notes = notes,
                        LockPolicyId = policyId
                    };

                    return productInfo;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the acceptable rs file info.
        /// For single use only. Do not use in a loop.
        /// </summary>
        /// <param name="rateSheetFileId">The rate sheet file id.</param>
        /// <returns>The rate sheet file info.</returns>
        private RateSheetFileInfo GetAcceptableRsFileInfoSingleUse(string rateSheetFileId)
        {
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "RS_File_GetById", new SqlParameter("@LpeAcceptableRsFileId", rateSheetFileId)))
            {
                if (reader.Read())
                {
                    string deploymentType = (string)reader["DeploymentType"];
                    bool isBothRsMapAndBotWorking = (bool)reader["IsBothRsMapAndBotWorking"];
                    long investorXlsFileId = (long)reader["InvestorXlsFileId"];
                    return new RateSheetFileInfo(rateSheetFileId, deploymentType, isBothRsMapAndBotWorking, investorXlsFileId);
                }
            }

            return null;
        }

        /// <summary>
        /// Initializes the rate sheet expiration feature dictionary.
        /// </summary>
        /// <param name="investorProductCodePair">The investor product code pairs.</param>
        /// <returns>The mapping from product code/investor name to rate sheet expiration feature info.</returns>
        private Dictionary<string, RateSheetExpirationFeatureInfo> InitializeRateSheetExpirationFeatureInfo(IEnumerable<Tuple<string, string>> investorProductCodePair)
        {
            Dictionary<string, RateSheetExpirationFeatureInfo> mapping = new Dictionary<string, RateSheetExpirationFeatureInfo>(StringComparer.OrdinalIgnoreCase);

            bool isUseLpeDatabase = this.broker.ActualPricingBrokerId == Guid.Empty;
            foreach (var item in RateSheetExpirationFeatureInfo.List(this.broker, investorProductCodePair, isUseLpeDatabase))
            {
                mapping.Add($"{item.ProductCode}_{item.InvestorName}", item);
            }

            return mapping;
        }

        /// <summary>
        /// Gets the migrated manual disabled investor product info from the DB.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <returns>A mapping from broker id/investor name/product code/lock policy id to the investor product info.</returns>
        private Dictionary<int, ManualDisabledInvestorProductInfo> InitializeMigratedManualDisabledInvestorProductInfo(Guid brokerId)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId.ToString())
            };

            Dictionary<int, ManualDisabledInvestorProductInfo> mapping = new Dictionary<int, ManualDisabledInvestorProductInfo>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "RetrieveMigratedManualDisabledInvestorProductForBroker", parameters))
            {
                while (reader.Read())
                {
                    string investorName = (string)reader["InvestorName"];
                    string productCode = (string)reader["ProductCode"];
                    string disableStatus = (string)reader["DisableStatus"];
                    string messagesToSubmittingUsers = (string)reader["MessagesToSubmittingUsers"];
                    DateTime disableEntryModifiedDate = (DateTime)reader["DisableEntryModifiedDate"];
                    Guid disableEntryModifiedByEmployeeId = (Guid)reader["DisableEntryModifiedByEmployeeId"];
                    string disableEntryModifiedByUserName = (string)reader["DisableEntryModifiedByUserName"];
                    DateTime disableDateTime = (DateTime)reader["DisableDateTime"];
                    bool isHiddenFromResult = (bool)reader["IsHiddenFromResult"];
                    string notes = (string)reader["Notes"];
                    Guid lockPolicyId = (Guid)reader["LockPolicyId"];

                    ManualDisabledInvestorProductInfo productInfo = new ManualDisabledInvestorProductInfo()
                    {
                        InvestorName = investorName,
                        ProductCode = productCode,
                        DisableStatus = disableStatus,
                        MessageToSubmittingUsers = messagesToSubmittingUsers,
                        DisableEntryModifiedDate = disableEntryModifiedDate,
                        DisableEntryModifiedByEmployeeId = disableEntryModifiedByEmployeeId,
                        DisableEntryModifiedByUserName = disableEntryModifiedByUserName,
                        DisableDateTime = disableDateTime,
                        IsHiddenFromResult = isHiddenFromResult,
                        Notes = notes,
                        LockPolicyId = lockPolicyId
                    };

                    mapping.Add(GetKeyForMigratedManualDisabledProduct(brokerId, investorName, productCode, lockPolicyId), productInfo);
                }
            }

            return mapping;
        }

        /// <summary>
        /// Initializes the RS file info from LPE_ACCEPTABLE_RS_FILE.
        /// </summary>
        /// <param name="rateSheetFileId">The rs file ids to look for.</param>
        /// <returns>The mapping from file id to info.</returns>
        private Dictionary<string, RateSheetFileInfo> InitializeAcceptableRsFileInfo(IEnumerable<string> rateSheetFileId)
        {
            string acceptableRsFileIdsXml = null;
            if (rateSheetFileId != null && rateSheetFileId.Any())
            {
                XElement rootElement = new XElement("root");
                foreach (string id in rateSheetFileId.Distinct())
                {
                    XElement investorElement = new XElement("file", new XAttribute("fileId", id));
                    rootElement.Add(investorElement);
                }

                acceptableRsFileIdsXml = rootElement.ToString(SaveOptions.DisableFormatting);
            }

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@RsFileIdsXml", acceptableRsFileIdsXml)
            };

            Dictionary<string, RateSheetFileInfo> mapping = new Dictionary<string, RateSheetFileInfo>(StringComparer.OrdinalIgnoreCase);
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "GetAllRsFilesById", parameters))
            {
                while (reader.Read())
                {
                    string acceptableRsFileId = (string)reader["LpeAcceptableRsFileId"];
                    string deploymentType = (string)reader["DeploymentType"];
                    bool isBothRsMapAndBotWorking = (bool)reader["IsBothRsMapAndBotWorking"];
                    long investorXlsFileId = (long)reader["InvestorXlsFileId"];

                    mapping.Add(acceptableRsFileId, new RateSheetFileInfo(acceptableRsFileId, deploymentType, isBothRsMapAndBotWorking, investorXlsFileId));
                }
            }

            return mapping;
        }
    }
}
