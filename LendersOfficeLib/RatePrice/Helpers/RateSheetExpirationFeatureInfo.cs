﻿namespace LendersOffice.RatePrice.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Xml.Linq;
    using DataAccess;
    using FileBasedPricing;
    using LendersOffice.Admin;

    /// <summary>
    /// Container for rate sheet expiration feature info.
    /// </summary>
    public class RateSheetExpirationFeatureInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RateSheetExpirationFeatureInfo"/> class.
        /// </summary>
        /// <param name="productCode">The product code.</param>
        /// <param name="investorName">The investor name.</param>
        /// <param name="isRatesheetExpirationDeployed">Whether the rate sheet expiration feature is deployed.</param>
        public RateSheetExpirationFeatureInfo(string productCode, string investorName, bool isRatesheetExpirationDeployed)
        {
            this.ProductCode = productCode;
            this.InvestorName = investorName;
            this.IsRateSheetExpirationFeatureDeployed = isRatesheetExpirationDeployed;
        }

        /// <summary>
        /// Gets the product code.
        /// </summary>
        /// <value>The product code.</value>
        public string ProductCode
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the investor name.
        /// </summary>
        /// <value>The investor name.</value>
        public string InvestorName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether the rate sheet expiration feature was deployed.
        /// </summary>
        /// <value>A value indicating whether the rate sheet expiration feature was deployed.</value>
        public bool IsRateSheetExpirationFeatureDeployed
        {
            get;
            private set;
        }

        /// <summary>
        /// Return a list of rate sheet expiration feature base on the investor product code pair. If
        /// investor product code pair is empty then return full list.
        /// </summary>
        /// <param name="broker">The broker account.</param>
        /// <param name="investorProductCodePair">List of investor product code pair.</param>
        /// <param name="isUseLpeDatabase">Whether to find information from Lpe database or file snapshot.</param>
        /// <returns>List of rate sheet expiration feature.</returns>
        public static IEnumerable<RateSheetExpirationFeatureInfo> List(BrokerDB broker, IEnumerable<Tuple<string, string>> investorProductCodePair, bool isUseLpeDatabase)
        {
            if (isUseLpeDatabase)
            {
                return ListFromDatabase(investorProductCodePair);
            }
            else
            {
                return ListFromSnapshot(broker, investorProductCodePair);
            }
        }

        /// <summary>
        /// Return a list of rate sheet expiration feature base on the investor product code pair. If
        /// investor product code pair is empty then return full list.
        /// </summary>
        /// <param name="investorProductCodePair">List of investor product code pair.</param>
        /// <returns>List of rate sheet expiration feature.</returns>
        private static IEnumerable<RateSheetExpirationFeatureInfo> ListFromDatabase(IEnumerable<Tuple<string, string>> investorProductCodePair)
        {
            string investorCodePairXml = null;
            if (investorProductCodePair != null && investorProductCodePair.Any())
            {
                XElement rootElement = new XElement("root");
                foreach (Tuple<string, string> pair in investorProductCodePair.Distinct())
                {
                    XElement pairElement = new XElement("pair", new XAttribute("investorName", pair.Item1), new XAttribute("code", pair.Item2));
                    rootElement.Add(pairElement);
                }

                investorCodePairXml = rootElement.ToString(SaveOptions.DisableFormatting);
            }

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@InvestorCodePairXml", investorCodePairXml)
            };

            List<RateSheetExpirationFeatureInfo> list = new List<RateSheetExpirationFeatureInfo>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "GetRateSheetExpirationFeatureForAll", parameters))
            {
                while (reader.Read())
                {
                    bool isRateSheetExpirationFeatureDeployed = (bool)reader["IsRateSheetExpirationFeatureDeployed"];
                    string productCode = (string)reader["ProductCode"];
                    string investorName = (string)reader["InvestorName"];

                    list.Add(new RateSheetExpirationFeatureInfo(productCode, investorName, isRateSheetExpirationFeatureDeployed));
                }
            }

            return list;
        }

        /// <summary>
        /// Return a list of rate sheet expiration feature base on the investor product code pair. If
        /// investor product code pair is empty then return full list.
        /// </summary>
        /// <param name="broker">The broker account.</param>
        /// <param name="investorProductCodePair">List of investor product code pair.</param>
        /// <returns>List of rate sheet expiration feature.</returns>
        private static IEnumerable<RateSheetExpirationFeatureInfo> ListFromSnapshot(BrokerDB broker, IEnumerable<Tuple<string, string>> investorProductCodePair)
        {
            HashSet<string> investorProductCodeSet = null;

            if (investorProductCodePair != null && investorProductCodePair.Any())
            {
                investorProductCodeSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

                foreach (Tuple<string, string> pair in investorProductCodePair.Distinct())
                {
                    // Item1 = investorName, Item2 = productCode;
                    investorProductCodeSet.Add(pair.Item1 + "::" + pair.Item2);
                }
            }

            var snapshot = broker == null ? FileBasedSnapshot.RetrieveLatestManualImport() : broker.RetrieveLatestManualImport();

            List<RateSheetExpirationFeatureInfo> list = new List<RateSheetExpirationFeatureInfo>();

            foreach (var item in snapshot.GetProductList())
            {
                if (investorProductCodeSet == null || investorProductCodeSet.Contains(item.InvestorName + "::" + item.ProductCode))
                {
                    list.Add(new RateSheetExpirationFeatureInfo(item.ProductCode, item.InvestorName, item.IsRateSheetExpirationFeatureDeployed));
                }
            }

            return list;
        }
    }
}