﻿namespace LendersOffice.RatePrice.Helpers
{
    using System;
    using DataAccess;
    using System.Text.RegularExpressions;
    using System.Text;
    using LendersOffice.Common;

    public class ParsedRuleInfo
    {
        public Guid RuleId;

        public string Description;
        public string Condition;
        public string sSkip;
        public string sDisqualify;
        public string Stip;
        public string Rate;
        public string Margin;
        public string Fees;
        public string QLTV;
        public string QCLTV;
        public string MaxFrontEndYsp;
        public string MaxBackEndYsp;
        public string QScore;
        public string QLAmt;
        public string MaxDTI;
        public string QRateAdj;
        public string TeaserRate;
        private string m_qbc;
        public string sLenderRule;
        public string LockDaysAdj;

        private const string BORROWER_NAME = "<BorrowerName>";

        public bool LenderRule
        {
            get
            {
                return (sLenderRule == "X");
            }
        }

        public bool Skip
        {
            get
            {
                return (sSkip == "X");
            }
        }

        public bool Disqualify
        {
            get
            {
                return (sDisqualify == "X");
            }
        }

        public string[] ExcludedKeywordsForSkipAndQRules
        {
            get
            {
                return new string[] {
                    "DebtRatioBottom", 
                    "DebtRatioTop", 
                    "ResidualIncomeMonthly", 
                    "AvailReserveMonths",
                    "NoteRateAfterLOCompAndRounding",
                    "NoteRateBeforeLOCompAndRounding",
                    "PointsBeforeLOCompAndRounding",
                    "NoteRateIsIntegerMultipleOfOneEighth",
                    "RateOptionExceedsMaxDTI",
                    "RateOptionFinalPrice",
                    "RateOptionPointsPreFEMaxYSP",
                    "CashToBorrower"
                };
            }
        }

        public E_sRuleQBCType QBC
        {
            get
            {
                switch (m_qbc)
                {
                    case "P":
                        return E_sRuleQBCType.PrimaryBorrower;
                    case "C":
                        return E_sRuleQBCType.OnlyCoborrowers;
                    case "A":
                        return E_sRuleQBCType.AllBorrowers;
                    default:
                        return E_sRuleQBCType.Legacy;
                }
            }
        }

        public void parseQBC(string str)
        {
            if (str == null)
                throw new CBaseException(ErrorMessages.Generic, "Error parsing QBC. Only values 'P', 'A', 'C' and 'blank' are accepted.");

            str = str.TrimWhitespaceAndBOM().ToUpper();
            if (!str.Equals("P") && !str.Equals("C") && !str.Equals("A") && !str.Equals(""))
                throw new CBaseException(ErrorMessages.Generic, "Error parsing QBC. Only values 'P', 'A', 'C' and 'blank' are accepted.");

            m_qbc = str;
        }

        public bool isValidLenderRule
        {
            get
            {
                return (sLenderRule == "" || sLenderRule == "X");
            }
        }

        public bool isValidLockDaysAdj
        {
            get
            {
                return !(Regex.Match(LockDaysAdj, @"\bLockDays\b", RegexOptions.IgnoreCase).Success);
            }
        }

        /*
         * opm 8036 fs 11/04/09
         2. The rule has no action. Current action fields are
         * AT/SKP, GRD/DIS, RATE, FEES, MARGIN, QRATE, MAXYSP,
         * STIPS, MAXDTI, DTI RANGE, QSCORE, QLTV, QCLTV, QLAMT
         * (i.e. everything but TYPE, DESCRIPTION, CONDITION, and RULEID).
         * Note: The QBC setting and the "Is Lender Adjustment?" flag are not action fields.
         * Rule-based lock period adjustment is an action field. 
         */
        //The rule action validation has to be done AFTER all other Rule fields have already been assigned.
        public bool hasAction(out string error)
        {
            bool bHasAction = false;

            if (sSkip != null && sSkip.Length != 0)
            {
                bHasAction = true;
            }
            else if (sDisqualify != null && sDisqualify.Length != 0)
            {
                bHasAction = true;
            }
            else if (Stip != null && Stip.Length != 0)
            {
                bHasAction = true;
            }
            else if (Rate != null && Rate.Length != 0)
            {
                bHasAction = true;
            }
            else if (Margin != null && Margin.Length != 0)
            {
                bHasAction = true;
            }
            else if (Fees != null && Fees.Length != 0)
            {
                bHasAction = true;
            }
            else if (QLTV != null && QLTV.Length != 0)
            {
                bHasAction = true;
            }
            else if (QCLTV != null && QCLTV.Length != 0)
            {
                bHasAction = true;
            }
            else if (MaxFrontEndYsp != null && MaxFrontEndYsp.Length != 0)
            {
                bHasAction = true;
            }
            else if (MaxBackEndYsp != null && MaxBackEndYsp.Length != 0)
            {
                bHasAction = true;
            }
            else if (QScore != null && QScore.Length != 0)
            {
                bHasAction = true;
            }
            else if (QLAmt != null && QLAmt.Length != 0)
            {
                bHasAction = true;
            }
            else if (MaxDTI != null && MaxDTI.Length != 0)
            {
                bHasAction = true;
            }
            else if (QRateAdj != null && QRateAdj.Length != 0)
            {
                bHasAction = true;
            }
            else if (TeaserRate != null && TeaserRate.Length != 0)
            {
                bHasAction = true;
            }
            else if (LockDaysAdj != null && LockDaysAdj.Length != 0)
            {
                bHasAction = true;
            }

            error = bHasAction ? "" : "Rule " + RuleId + " is invalid. No action field has been set.";
            return bHasAction;
        }

        public bool qKeywordsDontSetQColumns(out string error)
        {
            error = "";

            bool qKeywordInCondition = Condition.ToLower().Contains("qscore")
                || Condition.ToLower().Contains("qlamt")
                || Condition.ToLower().Contains("qltv")
                || Condition.ToLower().Contains("qcltv");

            bool qColumnsModified = !string.IsNullOrEmpty(QLTV)
                || !string.IsNullOrEmpty(QCLTV)
                || !string.IsNullOrEmpty(QScore)
                || !string.IsNullOrEmpty(QLAmt);

            if (qKeywordInCondition && qColumnsModified)
            {
                error = "Q-Keywords cannot be used to set the definition of Q-Keywords.";
                return false;
            }

            return true;
        }

        /*
         * opm 87119 vm 8/29/12
         * From the spec : The keywords that created excluded rules in step 3 should never be used in 
         * Qrules or in skip rules. This could cause unintended behavior. To prevent this unintended 
         * behavior, it would be safest to update the policy rule validation logic to throw an error 
         * if the SAE tries to upload a q/skip rule that depends on one of the rate option specific 
         * keywords. Additionally, the SAE will conduct a rules audit, to verify no such rules 
         * currently exist.
         * 
         * Updated in case 91554
         * In the parent case I had made the comment that in addition to preventing the setting 
         * of a qrule or a skip rule, the list of forbidden keywords should also be prevented from 
         * setting/adjusting any column with the exception of "disqual", “rate”, “fees”, “margin”, 
         * “qrate”, “trate”, and “is lender rule”. 
         * 
         * vm 9/14/12 - Removed back end and front end max ysp from protected columns
         */
        public bool skipAndQRulesLackExcludedKeywords(out string error)
        {
            if (string.IsNullOrEmpty(Condition))
            {
                error = "";
                return true;
            }

            // Looking for excluded keywords in the condition
            string excludedKeyword = string.Empty;
            foreach (string keyword in ExcludedKeywordsForSkipAndQRules)
            {
                if (Condition.ToLower().Contains(keyword.ToLower()) && excludedKeyword.Length == 0)
                {
                    // Making sure the detected keyword wasn't just a substring of a bigger keyword
                    string[] contents = Condition.ToLower().Split(' ');
                    foreach (string content in contents)
                    {
                        if (content.Equals(keyword.ToLower()))
                        {
                            excludedKeyword = keyword;
                        }
                    }
                }
            }

            // Checking protected columns
            bool modifiesProtectedColumn = false;
            if (!string.IsNullOrEmpty(sSkip)
                || !string.IsNullOrEmpty(Stip)
                || !string.IsNullOrEmpty(QLTV)
                || !string.IsNullOrEmpty(QCLTV)
                || !string.IsNullOrEmpty(QScore)
                || !string.IsNullOrEmpty(QLAmt)
                || !string.IsNullOrEmpty(MaxDTI)
                || !string.IsNullOrEmpty(LockDaysAdj))
            {
                modifiesProtectedColumn = true;
            }

            if (excludedKeyword.Length > 0 && modifiesProtectedColumn)
            {
                error = "A protected column was being modified using an excluded keyword: " + excludedKeyword;
                return false;
            }
            else
            {
                error = "";
                return true;
            }
        }

        //The QBC validation has to be done AFTER all other Rule fields have already been assigned.
        public bool isQbcValid(out string error)
        {
            bool hasError = false;
            StringBuilder sb = new StringBuilder();

            if (m_qbc != "")
            {
                if (Skip ||
                        Rate.Length != 0 ||
                        Fees.Length != 0 ||
                        Margin.Length != 0 ||
                        QRateAdj.Length != 0 ||
                        TeaserRate.Length != 0 ||
                        MaxFrontEndYsp.Length != 0 ||
                        MaxBackEndYsp.Length != 0 ||
                        QScore.Length != 0 ||
                        QLTV.Length != 0 ||
                        QCLTV.Length != 0 ||
                        QLAmt.Length != 0
                    )
                {
                    //If error detected, give a specific error message.
                    sb.AppendFormat("Rule {0} failed the QBC check. QBC='{1}' and ", RuleId, m_qbc);
                    if (Skip)
                    {
                        sb.Append("Skip = 'X'.");
                    }
                    else if (Rate != "")
                    {
                        sb.AppendFormat("Rate = '{0}'.", Rate);
                    }
                    else if (Fees != "")
                    {
                        sb.AppendFormat("Fees = '{0}'.", Fees);
                    }
                    else if (Margin != "")
                    {
                        sb.AppendFormat("Margin = '{0}'.", Margin);
                    }
                    else if (QRateAdj != "")
                    {
                        sb.AppendFormat("QRateAdj = '{0}'.", QRateAdj);
                    }
                    else if (TeaserRate != "")
                    {
                        sb.AppendFormat("TeaserRate = '{0}'.", TeaserRate);
                    }
                    else if (MaxFrontEndYsp != "")
                    {
                        sb.AppendFormat("MaxFrontEndYsp = '{0}'.", MaxFrontEndYsp);
                    }
                    else if (MaxBackEndYsp != "")
                    {
                        sb.AppendFormat("MaxYsp = '{0}'.", MaxBackEndYsp);
                    }
                    else if (QScore != "")
                    {
                        sb.AppendFormat("QScore = '{0}'.", QScore);
                    }
                    else if (QLTV != "")
                    {
                        sb.AppendFormat("QLTV = '{0}'.", QLTV);
                    }
                    else if (QCLTV != "")
                    {
                        sb.AppendFormat("QCLTV = '{0}'.", QCLTV);
                    }
                    else if (QLAmt != "")
                    {
                        sb.AppendFormat("QLAmt = '{0}'.", QLAmt);
                    }
                    sb.Append(" If QBC is set [Skip,Rate,Fees,Margin,QRateAdj,TeaserRate,MaxYsp,DTIRange,QScore,QLTV,QCLTV,QLAmt] need to remain blank.");
                    hasError = true;
                }
            }
            else // m_qbc = ""
            {
                if (Description.IndexOf(BORROWER_NAME) != -1)
                {
                    if (sb.Length != 0)
                        sb.Append("\n");
                    sb.AppendFormat("If QBC is blank, the rule description cannot contain the '{0}' keyword.", BORROWER_NAME);
                    hasError = true;
                }
                if (Stip.IndexOf(BORROWER_NAME) != -1)
                {
                    if (sb.Length != 0)
                        sb.Append("\n");
                    sb.AppendFormat("If QBC is blank, the rule stip cannot contain the '{0}' keyword.", BORROWER_NAME);
                    hasError = true;
                }
            }

            error = sb.ToString();
            return !hasError;
            
        }
    }
}