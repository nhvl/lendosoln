﻿namespace LendersOffice.RatePrice.CustomRatesheet
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Rate Margin Item.
    /// </summary>
    [DataContract]
    public class RatesMarginItem
    {
        /// <summary>
        /// Gets or sets Rate.
        /// </summary>
        [DataMember(Order = 1)]
        public string NoteRate { get; set; }

        /// <summary>
        /// Gets or sets Margin.
        /// </summary>
        [DataMember(Order = 2)]
        public string Margin { get; set; }
    }
}
