﻿namespace LendersOffice.RatePrice.CustomRatesheet
{
    /// <summary>
    /// Same as RateMarginItem but NoteRate/ Margin's types are decimals.
    /// </summary>
    public class RtRateMarginItem
    {
        /// <summary>
        /// Gets or sets Rate.
        /// </summary>
        public decimal NoteRate { get; set; }

        /// <summary>
        /// Gets or sets Margin.
        /// </summary>
        public decimal Margin { get; set; }
    }
}
