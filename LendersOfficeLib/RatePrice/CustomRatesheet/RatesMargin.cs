﻿namespace LendersOffice.RatePrice.CustomRatesheet
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Rate Margin Items for specific loan product.
    /// </summary>
    [DataContract]
    public class RatesMargin
    {
        /// <summary>
        /// Gets or sets the Loan program Id.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Match existing loan product.")]
        [DataMember(Order = 1)]
        public Guid lLpTemplateId { get; set; }

        /// <summary>
        /// Gets or sets the RatesMarginItem list.
        /// </summary>
        [DataMember(Order = 2)]
        public List<RatesMarginItem> Items { get; set; }
    }
}
