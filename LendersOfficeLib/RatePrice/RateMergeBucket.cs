namespace LendersOfficeApp.los.RatePrice
{
    using System.Collections;

    public class RateMergeBucket
	{
        private SortedList m_rateMergeGroupList = new SortedList();
        private bool m_isRateOptionsWorstThanLowerRateShown = false;
        private bool m_keepLoosers = false;

        public RateMergeBucket(bool isRateOptionsWorstThanLowerRateShown) : this(
            isRateOptionsWorstThanLowerRateShown, false)
        {

        }
        public RateMergeBucket(bool isRateOptionsWorstThanLowerRateShown, bool keepLoosers)
		{
            m_isRateOptionsWorstThanLowerRateShown = isRateOptionsWorstThanLowerRateShown;
            m_keepLoosers = keepLoosers;
		}

        public RateMergeGroup GetGroup(RateMergeGroupKey key)
        {
            return (RateMergeGroup)m_rateMergeGroupList[key];
        }

        public void Add(CApplicantPriceXml applicantPrice) 
        {
            if (null == applicantPrice)
                return;

            RateMergeGroupKey key = new RateMergeGroupKey(applicantPrice);

            RateMergeGroup list = null;

            if (!m_rateMergeGroupList.ContainsKey(key)) 
            {
                list = new RateMergeGroup(applicantPrice, m_isRateOptionsWorstThanLowerRateShown, m_keepLoosers);
                list.rateMergeGroupKey = key;
                m_rateMergeGroupList.Add(key, list);
            } 
            else 
            {
                list = (RateMergeGroup) m_rateMergeGroupList[key];
            }
            list.Add(applicantPrice);

        }

        public ICollection RateMergeGroups 
        {
            get 
            {
                return m_rateMergeGroupList.Values;
            }
        }

	}
}
