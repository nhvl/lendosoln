using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.LoanPrograms
{
    /// <summary>
    /// Each rate has an associated point cost according to
    /// the rate sheet for the associated product.
    /// </summary>

    [XmlRoot]
    public class LoanProductRate
    {
        /// <summary>
        /// Each rate has an associated point cost according to
        /// the rate sheet for the associated product.
        /// </summary>

        private string m_Rate = "";
        private string m_Point = "";
        private string m_Margin = "";
        private string m_qrateBase = "";
        private string m_teaserRate = "";

        #region ( Rate Point Elements )

        [XmlElement]
        public string Rate
        {
            set { m_Rate = value; }
            get { return m_Rate; }
        }

        [XmlElement]
        public string Point
        {
            set { m_Point = value; }
            get { return m_Point; }
        }

        [XmlElement]
        public string Margin
        {
            set { m_Margin = value; }
            get { return m_Margin; }
        }

        [XmlElement]
        public string QRateBase
        {
            get { return m_qrateBase; }
            set { m_qrateBase = value; }
        }

        [XmlElement]
        public string TeaserRate
        {
            get { return m_teaserRate; }
            set { m_teaserRate = value; }
        }

        #endregion

    }

    /// <summary>
    /// We capture the rate sheet entries using deserialization
    /// operators contained by this table.  A converter is needed
    /// to get the final object model to be fully importable.
    /// </summary>

    public class RateSheetXmlContentTable
    {
        /// <summary>
        /// We store parsed rate sheet entries in our export-ready
        /// array of product rates.
        /// </summary>

        private ArrayList m_Array = new ArrayList();

        #region ( Deserialization Operators )

        public String Document
        {
            // Access member.

            set
            {
                // Parse rate sheet parameters according to current
                // schema.  If schema changes, we need to update
                // this operator.

                XmlDocument xd = new XmlDocument();

                if (value.TrimWhitespaceAndBOM() == "")
                {
                    return;
                }

                xd.LoadXml(value);

                if (xd.HasChildNodes == false || xd.LastChild.Name != "XmlTable")
                {
                    return;
                }

                foreach (XmlNode node in xd.LastChild.ChildNodes)
                {
                    LoanProductRate rate = new LoanProductRate();

                    if (node.Name != "RateSheetXmlContent")
                    {
                        continue;
                    }

                    if (node["Rate"] != null)
                    {
                        rate.Rate = node["Rate"].InnerText;
                    }
                    else
                    {
                        rate.Rate = "0.0";
                    }

                    if (node["Point"] != null)
                    {
                        rate.Point = node["Point"].InnerText;
                    }
                    else
                    {
                        rate.Point = "0.0";
                    }

                    if (node["Margin"] != null)
                    {
                        rate.Margin = node["Margin"].InnerText;
                    }
                    else
                    {
                        rate.Margin = "0.0";
                    }

                    if (node["QRateBase"] != null)
                    {
                        rate.QRateBase = node["QRateBase"].InnerText;
                    }
                    else
                    {
                        rate.QRateBase = "0.0";
                    }

                    if (node["TeaserRate"] != null)
                    {
                        rate.TeaserRate = node["TeaserRate"].InnerText;
                    }
                    else
                    {
                        rate.TeaserRate = "0.0";
                    }

                    m_Array.Add(rate);
                }
            }
        }

        public ArrayList Array
        {
            // Access member.

            get
            {
                return m_Array;
            }
        }

        #endregion

        #region ( Constructors )

        /// <summary>
        /// Construct array from document.
        /// </summary>
        /// <param name="sDocument">
        /// Xml document of rates.
        /// </param>

        public RateSheetXmlContentTable(String sDocument)
        {
            // Initialize members.

            Document = sDocument;
        }

        #endregion

    }

    /// <summary>
    /// A loan product is a collection of parameters that govern
    /// the terms of a loan.  If a customer meets all the requirements
    /// according to a product's pricing rules, then the customer is
    /// eligible for this product.
    /// </summary>

    [XmlRoot]
    public class LoanProductItem
    {
        /// <summary>
        /// For now, we maintain the name and rate options in a
        /// single container.
        /// </summary>

        private Guid m_TemplateId;
        private Guid m_FolderId;
        private String m_ProductName;
        private String m_RateSheetLabel;
        //private String      m_ClosingCostName;
        private DateTime m_RateSheetEffective;
        private DateTime m_RateSheetExpires;
        private ArrayList m_RateOptions;

        #region ( Attributes and Elements )

        [XmlIgnore]
        public DataRow Contents
        {
            // Access member.

            set
            {
                Object o;

                o = value["lLpTemplateId"];

                if (o != null && o is DBNull == false)
                {
                    m_TemplateId = (Guid)o;
                }
                else
                {
                    m_TemplateId = Guid.Empty;
                }

                o = value["FolderId"];

                if (o != null && o is DBNull == false)
                {
                    m_FolderId = (Guid)o;
                }
                else
                {
                    m_FolderId = Guid.Empty;
                }

                o = value["lLpTemplateNm"];

                if (o != null && o is DBNull == false)
                {
                    m_ProductName = (String)o;
                }
                else
                {
                    m_ProductName = "";
                }

                o = value["lRateOptionBaseId"];

                if (o != null && o is DBNull == false)
                {
                    m_RateSheetLabel = (String)o;
                }
                else
                {
                    m_RateSheetLabel = "";
                }

                o = value["lRateSheetEffectiveD"];

                if (o != null && o is DBNull == false)
                {
                    m_RateSheetEffective = (DateTime)o;
                }
                else
                {
                    m_RateSheetEffective = DateTime.MinValue;
                }

                o = value["lRateSheetExpirationD"];

                if (o != null && o is DBNull == false)
                {
                    m_RateSheetExpires = (DateTime)o;
                }
                else
                {
                    m_RateSheetExpires = DateTime.MaxValue;
                }

                o = value["lRateSheetXmlContent"];

                if (o != null && o is DBNull == false)
                {
                    RateSheetXmlContentTable table = new RateSheetXmlContentTable((String)o);

                    foreach (LoanProductRate rate in table.Array)
                    {
                        m_RateOptions.Add(rate);
                    }
                }
                else
                {
                    m_RateOptions.Clear();
                }
            }
        }

        [XmlAttribute]
        public String ProductName
        {
            set { m_ProductName = value; }
            get { return m_ProductName; }
        }

        [XmlAttribute]
        public String RateSheetLabel
        {
            set { m_RateSheetLabel = value; }
            get { return m_RateSheetLabel; }
        }

        [XmlAttribute]
        public Guid TemplateId
        {
            set { m_TemplateId = value; }
            get { return m_TemplateId; }
        }

        [XmlAttribute]
        public Guid FolderId
        {
            set { m_FolderId = value; }
            get { return m_FolderId; }
        }

        [XmlElement]
        public DateTime RateSheetEffective
        {
            set { m_RateSheetEffective = value; }
            get { return m_RateSheetEffective; }
        }

        [XmlElement]
        public DateTime RateSheetExpiration
        {
            set { m_RateSheetExpires = value; }
            get { return m_RateSheetExpires; }
        }

        public LoanProductRate[] RateOptions
        {
            // Access member.

            set
            {
            }
            get
            {
                // Create a throw away array of policies.

                LoanProductRate[] array = new LoanProductRate[m_RateOptions.Count];

                for (int i = 0, n = m_RateOptions.Count; i < n; ++i)
                {
                    array[i] = m_RateOptions[i] as LoanProductRate;
                }

                return array;
            }
        }

        #endregion

        /// <summary>
        /// Append onto the tail of our options batch.  Once
        /// added, this rate will get streamed out in the
        /// order it was inserted.
        /// </summary>
        /// <param name="pRate">
        /// Rate to append and track.
        /// </param>

        public void Add(LoanProductRate pRate)
        {
            // Append to the tail if not null.  We should
            // check for duplicates too.

            if (pRate != null)
            {
                m_RateOptions.Add(pRate);
            }
        }

        #region ( Constructors )

        /// <summary>
        /// Construct default.
        /// </summary>

        public LoanProductItem()
        {
            // Initialize members.

            m_TemplateId = Guid.Empty;

            m_FolderId = Guid.Empty;

            m_ProductName = "";
            //m_ClosingCostName = "";

            m_RateSheetEffective = DateTime.MinValue;
            m_RateSheetExpires = DateTime.MaxValue;

            m_RateOptions = new ArrayList();
        }

        #endregion

    }

    /// <summary>
    /// Each group contains loan products and loan product groups.
    /// Thus, we store this tree as a composite of the two.
    /// </summary>

    [XmlRoot]
    public class LoanProductGroup
    {
        /// <summary>
        /// Each group has a name and can store other groups and
        /// inidividual loan products.
        /// </summary>

        private ArrayList m_Children = new ArrayList();

        private Guid m_FolderId;
        private Guid m_ParentFolderId;
        private String m_FolderName;

        #region ( Attributes and Nodes )

        [XmlIgnore]
        public DataRow Contents
        {
            // Access member.

            set
            {
                Object o;

                o = value["FolderId"];

                if (o != null && o is DBNull == false)
                {
                    m_FolderId = (Guid)o;
                }
                else
                {
                    m_FolderId = Guid.Empty;
                }

                o = value["ParentFolderId"];

                if (o != null && o is DBNull == false)
                {
                    m_ParentFolderId = (Guid)o;
                }
                else
                {
                    m_ParentFolderId = Guid.Empty;
                }

                o = value["FolderName"];

                if (o != null && o is DBNull == false)
                {
                    m_FolderName = (String)o;
                }
                else
                {
                    m_FolderName = "";
                }
            }
        }

        [XmlAttribute]
        public String FolderName
        {
            set { m_FolderName = value; }
            get { return m_FolderName; }
        }

        [XmlAttribute]
        public Guid FolderId
        {
            set { m_FolderId = value; }
            get { return m_FolderId; }
        }

        [XmlAttribute]
        public Guid ParentFolderId
        {
            set { m_ParentFolderId = value; }
            get { return m_ParentFolderId; }
        }

        [XmlArray]
        [XmlArrayItem(typeof(LoanProductGroup))]
        [XmlArrayItem(typeof(LoanProductItem))]
        public ArrayList Children
        {
            set { m_Children = value; }
            get { return m_Children; }
        }

        #endregion

        /// <summary>
        /// Append onto the tail of our export batch.  Once
        /// added, this group will get streamed out in the
        /// order it was inserted.
        /// </summary>
        /// <param name="pGroup">
        /// Product group to keep for export.
        /// </param>

        public void Add(LoanProductGroup pGroup)
        {
            // Append to the tail if not null.  We should
            // check for duplicates too.

            if (pGroup != null)
            {
                m_Children.Add(pGroup);
            }
        }

        /// <summary>
        /// Append onto the tail of our export batch.  Once
        /// added, this item will get streamed out in the
        /// order it was inserted.
        /// </summary>
        /// <param name="pItem">
        /// Product group to keep for export.
        /// </param>

        public void Add(LoanProductItem pItem)
        {
            // Append to the tail if not null.  We should
            // check for duplicates too.

            if (pItem != null)
            {
                m_Children.Add(pItem);
            }
        }

        #region ( Constructors )

        /// <summary>
        /// Construct default.
        /// </summary>

        public LoanProductGroup()
        {
            // Initialize members.

            m_FolderId = Guid.Empty;
            m_ParentFolderId = Guid.Empty;

            m_FolderName = "";
        }

        #endregion

    }

    /// <summary>
    /// We provide a container for exporting and importing
    /// a selected product folder with all its nested folders
    /// and contained products with their rates.
    /// </summary>

    [XmlRoot
    ]
    public class LoanProductSet
    {
        /// <summary>
        /// Keep a set of folders that we export in a batch.
        /// </summary>

        private ArrayList m_Set = new ArrayList();
        private Int32 m_Version = s_VersionStamp;

        #region ( Array of Folder Elements )

        [XmlArray]
        [XmlArrayItem(typeof(LoanProductGroup))]
        [XmlArrayItem(typeof(LoanProductItem))]
        public ArrayList Items
        {
            set { m_Set = value; }
            get { return m_Set; }
        }

        #endregion

        [XmlElement]
        public Int32 Verison
        {
            set { m_Version = value; }
            get { return m_Version; }
        }

        /// <summary>
        /// Append onto the tail of our export batch.  Once
        /// added, this group will get streamed out in the
        /// order it was inserted.
        /// </summary>
        /// <param name="pGroup">
        /// Product group to keep for export.
        /// </param>

        public void Add(LoanProductGroup pGroup)
        {
            // Append to the tail if not null.  We should
            // check for duplicates too.

            if (pGroup != null)
            {
                m_Set.Add(pGroup);
            }
        }

        /// <summary>
        /// Append onto the tail of our export batch.  Once
        /// added, this group will get streamed out in the
        /// order it was inserted.
        /// </summary>
        /// <param name="pItem">
        /// Product group to keep for export.
        /// </param>

        public void Add(LoanProductItem pItem)
        {
            // Append to the tail if not null.  We should
            // check for duplicates too.

            if (pItem != null)
            {
                m_Set.Add(pItem);
            }
        }

        /// <summary>
        /// Reset set state.
        /// </summary>

        public void Clear()
        {
            // Erase all stored objects and reset state.

            m_Set.Clear();
        }

        #region ( Streaming Operators )

        /// <summary>
        /// Translate instance to an xml document.
        /// </summary>
        /// <returns>
        /// Serialized instance.
        /// </returns>

        public override String ToString()
        {
            // Perform simple serialization according to hints.

            XmlSerializer xs = new XmlSerializer(typeof(LoanProductSet));
            StringWriter8 sw = new StringWriter8();

            m_Version = s_VersionStamp;

            xs.Serialize(sw, this);

            return sw.ToString();
        }

        #endregion

        #region ( Version Stamp )

        /// <summary>
        /// Each time we update this class' layout, in such a way
        /// that older versions don't conform to the new layout,
        /// then we need to bump up the version number by one.
        /// </summary>

        private static Int32 s_VersionStamp = 1;

        #endregion

    }

    /// <summary>
    /// Keep a handle on the export set we are wrapping.
    /// Once constructed, we let callers invoke our
    /// streaming operators to produce a simple comma
    /// separated value sheet.
    /// </summary>

    public class OptionWrapper
    {
        /// <summary>
        /// Keep a handle on the export set we are wrapping.
        /// </summary>

        private LoanProductSet m_Set;

        /// <summary>
        /// Write out the group using recursion.  If this group contains
        /// other groups, then we bounce right in.
        /// </summary>

        private void WriteGroup(LoanProductGroup lpGroup, Hashtable hLookup, StringWriter sWriter)
        {
            // Add this group to our lookup for folder path reconstruction
            // during writing items.

            hLookup.Add(lpGroup.FolderId, lpGroup);

            // Walk each entry and delegate to the proper handler.  Recursion
            // is the quickest implementation to pull this off.

            foreach (Object entry in lpGroup.Children)
            {
                if (entry is LoanProductItem == true)
                {
                    WriteItem(entry as LoanProductItem, lpGroup.FolderId, hLookup, sWriter);
                }

                if (entry is LoanProductGroup == true)
                {
                    WriteGroup(entry as LoanProductGroup, hLookup, sWriter);
                }
            }
        }

        /// <summary>
        /// Write out an individual item.  The csv sheet is composed
        /// of products; namely their rate options.
        /// </summary>

        private void WriteItem(LoanProductItem lpItem, Guid idFolder, Hashtable hLookup, StringWriter sWriter)
        {
            // Start by constructing the folder path using our lookup table
            // with the current folder as a seed.

            String path = "";

            for (LoanProductGroup group = hLookup[idFolder] as LoanProductGroup; group != null && group.FolderId != Guid.Empty; group = hLookup[group.ParentFolderId] as LoanProductGroup)
            {
                path = path.Insert(0, group.FolderName + "\\");
            }

            // Now write the individual entry.  Each product is listed
            // in a two-column format, showing name and id and rate
            // options for now.

            path += lpItem.ProductName;

            if (path.IndexOf(",") != -1)
            {
                path = "\"" + path + "\"";
            }

            sWriter.WriteLine("Product Name, " + path);

            sWriter.WriteLine("Rate Sheet Label, " + lpItem.RateSheetLabel);

            sWriter.WriteLine("Product Id, " + lpItem.TemplateId);

            foreach (LoanProductRate rate in lpItem.RateOptions)
            {
                string r = rate.Rate;
                if (r.IndexOf(",") != -1)
                {
                    r = "\"" + r + "\"";
                }

                string p = rate.Point;
                if (p.IndexOf(",") != -1)
                {
                    p = "\"" + p + "\"";
                }

                string m = rate.Margin;
                if (m.IndexOf(",") != -1)
                {
                    m = "\"" + m + "\"";
                }

                string qrateBase = rate.QRateBase;
                if (qrateBase.IndexOf(",") != -1)
                {
                    qrateBase = "\"" + qrateBase + "\"";
                }

                string teaserRate = rate.TeaserRate;
                if (teaserRate.IndexOf(",") != -1)
                {
                    teaserRate = "\"" + teaserRate + "\"";
                }

                sWriter.WriteLine(r + ", " + p + ", " + m + ", " + qrateBase + ", " + teaserRate);
            }

            sWriter.WriteLine("");
        }

        /// <summary>
        /// Translate instance to a csv document.
        /// </summary>
        /// <returns>
        /// Serialized instance.
        /// </returns>

        public override String ToString()
        {
            // Perform simple serialization according to hints.

            StringWriter8 sw = new StringWriter8();
            Hashtable lookup = new Hashtable();

            foreach (Object entry in m_Set.Items)
            {
                if (entry is LoanProductItem == true)
                {
                    WriteItem(entry as LoanProductItem, Guid.Empty, lookup, sw);
                }
                else
                if (entry is LoanProductGroup == true)
                {
                    WriteGroup(entry as LoanProductGroup, lookup, sw);
                }
            }

            return sw.ToString();
        }

        /// <summary>
        /// Construct wrapper.
        /// </summary>
        /// <param name="eSet">
        /// Set to wrap.
        /// </param>

        public OptionWrapper(LoanProductSet eSet)
        {
            // Initialize members.

            m_Set = eSet;
        }

    }

    /// <summary>
    /// Load the specified 
    /// </summary>

    public class RateOptionExport
    {
        /// <summary>
        /// Store what we load.
        /// </summary>

        LoanProductSet m_Set = new LoanProductSet();

        /// <summary>
        /// Pull broker's loan products from the database and store
        /// them in our exportable object model.
        /// </summary>
        /// <param name="brokerId">
        /// Broker to select on.
        /// </param>

        public void Load(Guid brokerId, Boolean isLpe, Guid folderId)
        {
            try
            {
                // Get all the products and folders for a broker and cache
                // it for quick lookup.  After we pull the individual entries,
                // we go through and fixup the linkage between groups and
                // their parents.

                ArrayList folders = new ArrayList();
                Hashtable groups = new Hashtable();
                ArrayList items = new ArrayList();
                DataSet qS = new DataSet();

                SqlParameter[] parameters = {
                                                new SqlParameter( "@BrokerId" , brokerId )
                                                , new SqlParameter( "@IsLpe"    , isLpe    )
                                            };
                DataSetHelper.Fill(qS, DataSrc.LpeSrc, "ListLoanProductFolderByBrokerID", parameters);

                foreach (DataRow row in qS.Tables[0].Rows)
                {
                    LoanProductGroup group = new LoanProductGroup();

                    group.Contents = row;

                    groups.Add(group.FolderId, group);
                }

                qS.Clear();

                parameters = new SqlParameter[] {
                                                    new SqlParameter( "@BrokerId" , brokerId )
                                                    , new SqlParameter( "@IsLpe"    , isLpe    )
                                                };
                DataSetHelper.Fill(qS, DataSrc.LpeSrc, "ListLoanProgramsUsingBrokerId", parameters);

                foreach (DataRow row in qS.Tables[0].Rows)
                {
                    LoanProductItem item = new LoanProductItem();

                    item.Contents = row;

                    items.Add(item);
                }

                qS.Clear();

                if (groups.Count > 0 || items.Count > 0)
                {
                    LoanProductGroup root = new LoanProductGroup();

                    root.ParentFolderId = Guid.Empty;
                    root.FolderId = Guid.Empty;

                    root.FolderName = "Root";

                    groups.Add(Guid.Empty, root);
                }

                foreach (LoanProductItem item in items)
                {
                    LoanProductGroup parent = groups[item.FolderId] as LoanProductGroup;

                    if (parent != null)
                    {
                        parent.Add(item);
                    }
                }

                foreach (LoanProductGroup group in groups.Values)
                {
                    LoanProductGroup parent = groups[group.ParentFolderId] as LoanProductGroup;

                    if (parent.FolderId == group.FolderId)
                    {
                        continue;
                    }

                    if (parent != null)
                    {
                        parent.Add(group);
                    }
                }

                // We have the user's broker's complete product folder
                // in memory.  Basically, we just pick what they picked
                // and add it to our export set.

                if (folderId == Guid.Empty)
                {
                    // Get the root from the cached tree.

                    LoanProductGroup root = groups[Guid.Empty] as LoanProductGroup;

                    if (root != null)
                    {
                        foreach (Object entry in root.Children)
                        {
                            if (entry is LoanProductGroup == true)
                            {
                                m_Set.Add(entry as LoanProductGroup);
                            }

                            if (entry is LoanProductItem == true)
                            {
                                m_Set.Add(entry as LoanProductItem);
                            }
                        }
                    }
                }
                else
                {
                    // Get the specified id from the query string.

                    LoanProductGroup folder = groups[folderId] as LoanProductGroup;

                    if (folder != null)
                    {
                        m_Set.Add(folder);
                    }
                }
            }
            catch (Exception e)
            {
                // Oops!

                throw new CBaseException(ErrorMessages.FailedToLoad, "Unable to load rate options. " + e.ToString());
            }
        }

        /// <summary>
        /// Pull broker's loan products from the database and store
        /// them in our exportable object model.  We assume that all
        /// products are to be loaded.
        /// </summary>
        /// <param name="brokerId">
        /// Broker to fetch.
        /// </param>

        public void Load(Guid brokerId, Boolean isLpe)
        {
            // Delegate to real implementation -- load up the house.
            Load(brokerId, isLpe, Guid.Empty);
        }

        /// <summary>
        /// Export out the loaded loan products in csv format.
        /// </summary>
        /// <returns>
        /// String representation of csv export file.
        /// </returns>

        public string Export()
        {
            // Send out the loaded state as a csv table-document.

            try
            {
                // Just wrap and stream out.

                OptionWrapper csvDoc = new OptionWrapper(m_Set);

                return csvDoc.ToString();
            }
            catch (Exception e)
            {
                // Oops!

                throw new CBaseException(ErrorMessages.GenericFailureToExportDataToCSV, "Unable to export rate options. " + e.ToString());
            }
        }
    }

    /// <summary>
    /// Convert string representation of an assortment of loan
    /// product's rate sheets into content that is loaded into
    /// the database.
    /// </summary>

    public class RateOptionImport
    {
        /// <summary>
        /// Walk the current input stream and load up the specified
        /// rate sheets.  We cache the broker's entire product set
        /// and on save, write back all the changes.
        /// </summary>
        /// <param name="sCsvDoc">
        /// Stream form of csv document containing rate options.
        /// </param>
        /// <param name="aErrorList">
        /// Container for all errors found during import.
        /// </param>
        /// <returns>
        /// True if import was successful.  Else, don't save.
        /// </returns>

        private class RateSheetLineItemsExt // POD : Plain Old Data 
        {
            internal RateSheetLineItemsExt(Guid productId, RateSheetLineItems item, int begline, int endline)
            {
                this.LineItem = item;
                this.StartLine = begline;
                this.EndLine = endline;
                this.ProductId = productId;
            }

            public Guid ProductId { get; private set; }
            public RateSheetLineItems LineItem { get; private set; }

            public int StartLine { get; private set; }

            public int EndLine { get; private set; }
        }

        /// <summary>
        /// Import rates option csv.
        /// </summary>
        /// <param name="csvStream"></param>
        /// <param name="lRateSheetDownloadStartD"></param>
        /// <param name="LpeAcceptableRsFileId"></param>
        /// <param name="LpeAcceptableRsFileVersionNumber"></param>
        /// <param name="errorList"></param>
        /// <returns>Number of rate options imported. Return -1 if there is error.</returns>
        public int Import(Stream csvStream, DateTime lRateSheetDownloadStartD, string LpeAcceptableRsFileId, long LpeAcceptableRsFileVersionNumber, out IEnumerable<string> errorList)
        {
            List<string> parseErrorList = new List<string>();
            errorList = parseErrorList;

            List<RateSheetLineItemsExt> rateSheetList = new List<RateSheetLineItemsExt>();

            if (!Parse(csvStream, rateSheetList, parseErrorList))
            {
                return -1;
            }

            if (rateSheetList.Count == 0)
            {
                return 0;
            }

            foreach (var ratesheetToImport in rateSheetList)
            {
                try
                {
                    SaveDirectlyToDatabase(ratesheetToImport.ProductId, ratesheetToImport.LineItem, lRateSheetDownloadStartD, LpeAcceptableRsFileId, LpeAcceptableRsFileVersionNumber);
                }
                catch (Exception e)
                {
                    e.Data.Add("ProductId", ratesheetToImport.ProductId);
                    e.Data.Add("LpeAcceptableRsFileId", LpeAcceptableRsFileId);
                    throw;
                }
            }

            return rateSheetList.Count;
        }

        private void SaveDirectlyToDatabase(Guid lLpTemplateId, RateSheetLineItems baseRates, DateTime lRateSheetDownloadStartD, string lpeAcceptableRsFileId, long lpeAcceptableRsFileVersionNumber)
        {
            using (PerformanceStopwatch.Start("SaveDirectlyToDatabase"))
            {
                string lRateSheetXmlContent = baseRates.ToString();

                var rateOptionList = CLoanProductBase.ConvertRateOption(lRateSheetXmlContent);

                var ratesProtobufContent = SerializationHelper.ProtobufSerialize(rateOptionList);
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@lLpTemplateId", lLpTemplateId));
                parameters.Add(new SqlParameter("@lRateSheetDownloadStartD", lRateSheetDownloadStartD));
                parameters.Add(new SqlParameter("@lRateSheetXmlContent", lRateSheetXmlContent));
                parameters.Add(new SqlParameter("@ContentKey", string.Empty)); // 7/23/2018 - dd - We are no longer store contains of Rate content in FileDB. Therefore ContentKey is not use. The column can be drop.
                parameters.Add(new SqlParameter("@RatesProtobufContent", ratesProtobufContent));

                if (!string.IsNullOrEmpty(lpeAcceptableRsFileId))
                {
                    parameters.Add(new SqlParameter("@LpeAcceptableRsFileId", lpeAcceptableRsFileId));
                }

                if (lpeAcceptableRsFileVersionNumber != -1)
                {
                    parameters.Add(new SqlParameter("@LpeAcceptableRsFileVersionNumber", lpeAcceptableRsFileVersionNumber));
                }

                if (baseRates.HasSpecialKeyword)
                {
                    // Only modify the Lock Period and PP period and IO period when there is special keyword in rate option.
                    int new_lLockedDays = baseRates.MaxProdRLckDays == int.MinValue ? -1 : baseRates.MaxProdRLckDays;
                    int new_lLockedDaysLowerSearch = baseRates.MinProdRLckdDays == int.MaxValue ? -1 : baseRates.MinProdRLckdDays;
                    int new_lPpmtPenaltyMon = baseRates.MaxProdPpmtPenaltyMon == int.MinValue ? -1 : baseRates.MaxProdPpmtPenaltyMon;
                    int new_lPpmtPenaltyMonLowerSearch = baseRates.MinProdPpmtPenaltyMon == int.MaxValue ? -1 : baseRates.MinProdPpmtPenaltyMon;
                    int new_lIOnlyMonLowerSearch = baseRates.MinIOnlyMon == int.MaxValue ? -1 : baseRates.MinIOnlyMon;
                    int new_lIOnlyMonUpperSearch = baseRates.MaxIOnlyMon == int.MinValue ? -1 : baseRates.MaxIOnlyMon;

                    parameters.Add(new SqlParameter("@new_lLockedDays", new_lLockedDays));
                    parameters.Add(new SqlParameter("@new_lLockedDaysLowerSearch", new_lLockedDaysLowerSearch));
                    parameters.Add(new SqlParameter("@new_lPpmtPenaltyMon", new_lPpmtPenaltyMon));
                    parameters.Add(new SqlParameter("@new_lPpmtPenaltyMonLowerSearch", new_lPpmtPenaltyMonLowerSearch));
                    parameters.Add(new SqlParameter("@new_lIOnlyMonLowerSearch", new_lIOnlyMonLowerSearch));
                    parameters.Add(new SqlParameter("@new_lIOnlyMonUpperSearch", new_lIOnlyMonUpperSearch));
                }
                parameters.Add(new SqlParameter("@isAllowUpdateLockRange", baseRates.HasSpecialKeyword));

                StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, "LpeUpload_UploadRateSheet", 1, parameters);
            }

        }
        private bool Parse(Stream csvStream, List<RateSheetLineItemsExt> rateSheetList, List<string> errorList)
        {
            // Load up the file from the stream that came with the
            // uploaded file.

            ImportStreamReader sr = new ImportStreamReader(csvStream);

            if (sr.IsValid == false)
            {
                errorList.Add("Invalid input file.  File may be in incorrect format or not exist.");

                return false;
            }


            HashSet<Guid> hashset = new HashSet<Guid>();
            try
            {
                // Walk the file and pull out a broker's product entries.
                //
                // 9/30/2004 kb - We previously looked for the broker's
                // id on its own line to show the start of a broker.
                // That requirement has been nixed.  We now update any
                // rate sheet.

                bool keepGoing = true;

                sr.ReadLine();

                while (keepGoing == true)
                {
                    // Now walk the content and pull out product entries.
                    // For now, the only thing we pull is rate prices for
                    // existing products within this broker.

                    Guid fileId = Guid.Empty;

                    if (sr.Parsed[0].TrimWhitespaceAndBOM().ToLower() != "product name")
                    {
                        if (sr.Parsed[0].TrimWhitespaceAndBOM() != "" && sr.Parsed[0].TrimWhitespaceAndBOM().ToLower().StartsWith("broker") == false && sr.Parsed[0].TrimWhitespaceAndBOM().StartsWith("//") == false)
                        {
                            // Only product entries may be listed.  Stray
                            // content is not allowed.

                            errorList.Add("Invalid line within rate sheet set (line# " + sr.LineNo + ").");
                        }

                        sr.ReadLine();

                        continue;
                    }

                    sr.ReadLine();

                    if (sr.Parsed[0].TrimWhitespaceAndBOM().ToLower() == "rate sheet label")
                    {
                        // Skip past this optional row.

                        sr.ReadLine();
                    }

                    if (sr.Parsed[0].TrimWhitespaceAndBOM().ToLower() != "product id" && sr.Parsed[0].TrimWhitespaceAndBOM().ToLower() != "program id")
                    {
                        // Only product entries may be listed at this scope.

                        errorList.Add("Product name not followed by product id (line# " + sr.LineNo + ").");

                        continue;
                    }

                    if (!Guid.TryParse(sr.Parsed[1].Trim(), out fileId))
                    {
                        // Unable to parse product id so punt and complain.

                        errorList.Add("Invalid product id (line# " + sr.LineNo + ").");

                        sr.ReadLine();
                        continue;
                    }

                    // Check if we've already seen this product.
                    if (hashset.Contains(fileId))
                    {
                        // Redundant product entry found.  Not supported.

                        errorList.Add("Redundant product entry " + fileId + " found.  Not supported (line# " + sr.LineNo + ").");

                        continue;
                    }


                    // Walk the following section and gather rate option
                    // pairs until the start of the next section.

                    RateSheetLineItems baseRates = new RateSheetLineItems();

                    // 09/27/07 mf. OPM 12259.  We need to parse based on the format of the item.
                    bool is2dFormat = false;
                    bool is2dFormatChecked = false;
                    StringBuilder sheet2dBuilder = new StringBuilder();

                    int beginLine = sr.LineNo;

                    try
                    {
                        bool hasBlankLines = false;
                        while (true)
                        {
                            string str = sr.ReadLine();
                            if (str.TrimWhitespaceAndBOM() == "")
                            {
                                hasBlankLines = true;
                                continue; // Skip Blank line.
                            }

                            if (sr.Parsed[0].TrimWhitespaceAndBOM().ToLower() == "product name")
                            {
                                break;
                            }

                            if (sr.Parsed[0].TrimWhitespaceAndBOM().ToLower().StartsWith("broker") == true || sr.Parsed[0].TrimWhitespaceAndBOM().StartsWith("//") == true)
                            {
                                continue;
                            }

                            if (!is2dFormatChecked)
                            {
                                if (CLoanProductBase.Is2dFormat(str))
                                {
                                    is2dFormat = true;
                                }
                                is2dFormatChecked = true;
                            }

                            if (sr.Parsed.Length < 2)
                            {
                                if (sr.Parsed[0].TrimWhitespaceAndBOM() != "")
                                {
                                    // When listing products, only blank lines and
                                    // valid product entries are allowed.

                                    errorList.Add("Invalid rate sheet option (line# " + sr.LineNo + ").");
                                }

                                hasBlankLines = true;
                                continue;
                            }

                            // additional check to see if this is a blank line or not
                            bool isBlankLine = true;
                            for (int i = 0; i < sr.Parsed.Length; i++)
                            {
                                if (sr.Parsed[i].TrimWhitespaceAndBOM() != "")
                                {
                                    isBlankLine = false;
                                    break;
                                }
                            }

                            if (hasBlankLines && isBlankLine)
                            {
                                continue;
                            }
                            else if (hasBlankLines && !isBlankLine)
                            {
                                // valid Rate entries may not be separated by blank lines
                                errorList.Add("Invalid rate sheet option (line# " + sr.LineNo + ").");
                            }
                            else if (isBlankLine)
                            {
                                hasBlankLines = true;
                                continue;
                            }


                            try
                            {
                                if (is2dFormat)
                                {
                                    // 09/27/07 mf. OPM 12259 For 2D Processing, we need to see
                                    // the whole sheet before we can process.  Accumulate all
                                    // valid lines first, then defer to the 2d processor for format
                                    // validation.
                                    sheet2dBuilder.Append(str + Environment.NewLine);
                                }
                                else
                                {
                                    // Attempt to parse the values.  If it fails, then we skip this row.
                                    // Rate could have following format.
                                    // LOCK, 0, 30
                                    // PP, 0, 12
                                    // 7, 0, 0
                                    // 8, 0
                                    // 11/30/2006 dd - OPM 7688 - We add two new values at the end QRateBase and TeaserRate
                                    // {Rate}, {Point}, {Margin}, {QrateBase}, {TeaserRate}

                                    string rate = sr.Parsed[0].Trim().ToUpper();
                                    string point = sr.Parsed[1].Trim();
                                    string margin = string.Empty;
                                    string qrateBase = string.Empty;
                                    string teaserRate = string.Empty;

                                    if (sr.Parsed.Length > 2)
                                    {
                                        margin = sr.Parsed[2].Trim();
                                    }

                                    // 11/30/2006 dd - QRateBase and TeaserRate must be valid.
                                    // 12/8/2006 mf - We need to accept 3, 4, or 5 columns.
                                    if (sr.Parsed.Length > 3)
                                    {
                                        qrateBase = sr.Parsed[3].Trim();
                                    }

                                    if (sr.Parsed.Length > 4)
                                    {
                                        teaserRate = sr.Parsed[4].Trim();
                                    }

                                    baseRates.Add(rate, point, margin, qrateBase, teaserRate);
                                }
                            }
                            catch (CBaseException exc)
                            {
                                errorList.Add("Invalid rate sheet option (line# " + sr.LineNo + "). ErrMsg: " + exc.DeveloperMessage);
                            }
                        }
                    }
                    finally
                    {
                        // Add the options to the specified product.  We blow away
                        // any previous options.
                        if (is2dFormat)
                        {
                            CLoanProductBase.RateSheet2D sheet2d = new CLoanProductBase.RateSheet2D();
                            sheet2d.Process(sheet2dBuilder.ToString());
                            sheet2d.ApplyToLineItems(baseRates);
                        }

                        if (!baseRates.IsValidRateOption() || baseRates.ToString().Length < 0 /* use for validate */ )
                        {
                            errorList.Add("Invalid rate sheet option in product " + fileId + ".");
                        }
                        else if (hashset.Contains(fileId))
                        {
                            errorList.Add("Redundant product entry " + fileId + " found.  Not supported (line# " + sr.LineNo + ").");
                        }
                        else
                        {
                            rateSheetList.Add(new RateSheetLineItemsExt(fileId, baseRates, beginLine, sr.LineNo));
                            hashset.Add(fileId);
                        }
                    }
                }
            }
            catch (EndOfStreamException)
            {
            }
            finally
            {
                sr.Close();
            }

            return errorList.Count == 0;
        }
    }

    /// <summary>
    /// We maintain line items in this working set.  When we
    /// serialize, we will convert to array.
    /// </summary>

    public class RateSheetLineItems
    {
        /// <summary>
        /// We maintain line items in this working set.  When we
        /// serialize, we will convert to array.
        /// </summary>

        private List<RateSheetXmlContent> m_Set = new List<RateSheetXmlContent>();

        private List<RateSheetXmlContent> m_finalSet = new List<RateSheetXmlContent>();

        private int m_min_sProdRLckdDays = int.MaxValue;
        private int m_max_sProdRLckdDays = int.MinValue;
        private int m_min_sProdPpmtPenaltyMon = int.MaxValue;
        private int m_max_sProdPpmtPenaltyMon = int.MinValue;
        private int m_min_sIOnlyMon = int.MaxValue;
        private int m_max_sIOnlyMon = int.MinValue;

        private bool m_hasSpecialKeyword = false;

        public int MinProdRLckdDays
        {
            get { return m_min_sProdRLckdDays; }
        }
        public int MaxProdRLckDays
        {
            get { return m_max_sProdRLckdDays; }
        }
        public int MinProdPpmtPenaltyMon
        {
            get { return m_min_sProdPpmtPenaltyMon; }
        }
        public int MaxProdPpmtPenaltyMon
        {
            get { return m_max_sProdPpmtPenaltyMon; }
        }
        public int MinIOnlyMon
        {
            get { return m_min_sIOnlyMon; }
        }
        public int MaxIOnlyMon
        {
            get { return m_max_sIOnlyMon; }
        }
        public bool HasSpecialKeyword
        {
            get { return m_hasSpecialKeyword; }
        }


        private class RateSheetXmlContent
        {
            public Guid RecordId { get; set; }

            public string Rate { get; set; }

            public string Point { get; set; }

            public string Margin { get; set; }

            public string QRateBase { get; set; }

            public string TeaserRate { get; set; }

            public decimal Rate_decimal
            {
                get
                {
                    try { return decimal.Parse(this.Rate); }
                    catch { return 0.0M; }
                }
            }
            public decimal Point_decimal
            {
                get
                {
                    try { return decimal.Parse(this.Point); }
                    catch { return 0.0M; }
                }
            }
            public decimal Margin_decimal
            {
                get
                {
                    try { return decimal.Parse(this.Margin); }
                    catch { return 0.0M; }
                }
            }
        }

        #region ( Serialization Operators )


        private static bool IsZeroOrEmpty(string str)
        {
            switch (str)
            {
                case "":
                case "0":
                case "0.":
                case "0.0":
                case "0.00":
                case "0.000":
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Write out the stored set as a xml doc that conforms
        /// to the current rate sheet content standard.
        /// </summary>
        /// <returns>
        /// Xml doc representation of array.
        /// </returns>

        public override string ToString()
        {
            // Serialize our set into a conforming xml document.

            LendersOffice.Common.StringWriter8 sw = new LendersOffice.Common.StringWriter8();

            if (m_finalSet != null)
            {
                // Write the list to our xml document.  We save it after
                // we have appended the list.

                XmlDocument xd = new XmlDocument();
                xd.PreserveWhitespace = true; //Thien :the output will not contain indents.

                XmlElement root = xd.CreateElement("XmlTable");

                foreach (RateSheetXmlContent item in m_finalSet)
                {
                    XmlElement attr, node = xd.CreateElement("RateSheetXmlContent");

                    attr = xd.CreateElement("RecordId");
                    attr.InnerText = item.RecordId.ToString();
                    node.AppendChild(attr);

                    attr = xd.CreateElement("Rate");
                    attr.InnerText = item.Rate;
                    node.AppendChild(attr);

                    attr = xd.CreateElement("Point");
                    attr.InnerText = item.Point;
                    node.AppendChild(attr);

                    attr = xd.CreateElement("Margin");
                    attr.InnerText = item.Margin;
                    node.AppendChild(attr);

                    if (IsZeroOrEmpty(item.QRateBase) == false)
                    {
                        attr = xd.CreateElement("QRateBase");
                        attr.InnerText = item.QRateBase;
                        node.AppendChild(attr);
                    }


                    if (IsZeroOrEmpty(item.TeaserRate) == false)
                    {
                        attr = xd.CreateElement("TeaserRate");
                        attr.InnerText = item.TeaserRate;
                        node.AppendChild(attr);
                    }

                    root.AppendChild(node);
                }

                // Now save the list as an xml document in a string.

                xd.AppendChild(root);

                xd.Save(sw);
            }

            return sw.ToString();
        }

        #endregion

        public bool IsValidRateOption()
        {

            List<RateSheetXmlContent> list = null;

            bool initialRate = true;

            bool isKeywordModeInitialize = false;
            int keyWordMode = 0;
            int lockKeyword = 1;
            int PPKeyword = 2;
            int ioKeyword = 4;

            int previousKeywordMode = 0;

            foreach (RateSheetXmlContent o in m_Set)
            {
                bool isNumeric = false;
                bool isKeyword = false;

                if (o.Rate == "LOCK")
                {
                    isKeyword = true;
                    keyWordMode = keyWordMode | (1 << lockKeyword);
                }
                else if (o.Rate == "PP")
                {
                    isKeyword = true;
                    keyWordMode = keyWordMode | (1 << PPKeyword);
                }
                else if (o.Rate == "IO")
                {
                    isKeyword = true;
                    keyWordMode = keyWordMode | (1 << ioKeyword);
                }
                else
                {
                    try
                    {
                        decimal.Parse(o.Rate);
                        isNumeric = true;
                    }
                    catch { }

                }

                if (isKeyword)
                {
                    try
                    {
                        if (int.Parse(o.Point) > int.Parse(o.Margin))
                            return false; // Min value is greater than max value.
                    }
                    catch
                    {
                        return false; // Invalid entry
                    }
                    initialRate = true;
                    m_finalSet.Add(o);
                }
                else if (isNumeric)
                {
                    if (initialRate)
                    {
                        list = new List<RateSheetXmlContent>();
                        if (!isKeywordModeInitialize)
                        {
                            previousKeywordMode = keyWordMode;
                            isKeywordModeInitialize = true;
                        }
                        else
                        {
                            // 7/16/2007 dd - We do not allow mixed keyword usage on each rate option.
                            // Example:
                            //  PP, 0, 30
                            //  8, 1, 0, 0, 0
                            //  LOCK, 0, 30
                            //  8, 1, 0, 0, 0
                            if (previousKeywordMode != keyWordMode)
                                return false; // MIXED MODE DETECTED.
                        }
                    }

                    // Reset keyword.
                    keyWordMode = 0;

                    initialRate = false;

                    bool isDuplicate = false;
                    foreach (RateSheetXmlContent entry in list)
                    {
                        if (entry.Rate_decimal == o.Rate_decimal && entry.Point_decimal == o.Point_decimal)
                        {
                            if (entry.Margin_decimal > o.Margin_decimal)
                            {
                                // 7/21/2005 dd - OPM #2125 - If Rate & Fee are the same then choose the lowest margin.
                                entry.Margin = o.Margin;


                            }
                            isDuplicate = true;
                            break;
                        }

                    }
                    if (!isDuplicate)
                    {
                        list.Add(o);
                        m_finalSet.Add(o);
                    }
                }
                else
                {
                    // Invalid entry detect.
                    return false;
                }

            }

            return true;
        }

        /// <summary>
        /// Add a new entry into the set.
        /// </summary>

        public void Add(string rate, string point, string margin, string qrateBase, string teaserRate)
        {
            if (rate == "N/A" || point == "N/A" || margin == "N/A")
            {
                // 5/23/2006 dd - OPM 4786 Skip N/A when import rate option.
                return;
            }
            // Append to the tail.

            RateSheetXmlContent item = new RateSheetXmlContent();

            item.RecordId = Guid.NewGuid();

            item.Rate = rate.Replace("%", "");
            item.Point = point == "" ? "0" : point.Replace("%", "");
            item.Margin = margin == "" ? "0" : margin.Replace("%", "");
            item.QRateBase = qrateBase == "" ? "0" : qrateBase.Replace("%", "");
            item.TeaserRate = teaserRate == "" ? "0" : teaserRate.Replace("%", "");

            // 2/9/2006 dd - OPM 4015 - Validate rate, point, margin. If it is invalid value then throw exception.
            try
            {
                if (rate != "LOCK" && rate != "PP" && rate != "IO")
                    decimal.Parse(item.Rate);
            }
            catch
            {
                throw new CBaseException("Rate is not a valid number.", "Rate = " + item.Rate + " is not a valid number.");
            }

            try
            {
                decimal.Parse(item.Point);
            }
            catch
            {
                throw new CBaseException("Point is not a valid number.", "Point = " + item.Point + " is not a valid number.");
            }

            try
            {
                decimal.Parse(item.Margin);
            }
            catch
            {
                throw new CBaseException("Margin is not a valid number.", "Margin = " + item.Margin + " is not a valid number.");
            }
            try
            {
                decimal.Parse(item.QRateBase);
            }
            catch
            {
                throw new CBaseException("QRateBase is not a valid number.", "QRateBase = " + item.QRateBase + " is not a valid number.");
            }

            try
            {
                decimal.Parse(item.TeaserRate);
            }
            catch
            {
                throw new CBaseException("TeaserRate is not a valid number.", "TeaserRate = " + item.TeaserRate + " is not a valid number.");
            }

            m_Set.Add(item);

            if (rate == "LOCK" || rate == "PP" || rate == "IO")
            {
                m_hasSpecialKeyword = true;
                // 9/13/2005 dd - Try to get the range of Lock Period and PP
                int min = int.MaxValue;
                int max = int.MinValue;
                try
                {
                    min = int.Parse(point);
                }
                catch { }
                try
                {
                    max = int.Parse(margin);
                }
                catch { }

                if (rate == "LOCK")
                {
                    if (min < m_min_sProdRLckdDays)
                        m_min_sProdRLckdDays = min;

                    if (max > m_max_sProdRLckdDays)
                        m_max_sProdRLckdDays = max;
                }
                else if (rate == "PP")
                {
                    if (min < m_min_sProdPpmtPenaltyMon)
                        m_min_sProdPpmtPenaltyMon = min;

                    if (max > m_max_sProdPpmtPenaltyMon)
                        m_max_sProdPpmtPenaltyMon = max;

                }
                else if (rate == "IO")
                {
                    if (min < m_min_sIOnlyMon)
                        m_min_sIOnlyMon = min;
                    if (max > m_max_sIOnlyMon)
                        m_max_sIOnlyMon = max;
                }
            }

        }

    }

}