using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using DataAccess;
using LendersOffice.Common;
using LendersOfficeApp.los.RatePrice;
using LendersOffice;

namespace Toolbox.LoanProgram
{
    internal class ProdDeriveCacheUpdate
    {	
		#region INTERNAL METHODS

        static internal void SpreadFromAllNonmasterProdsInThisBroker( Guid brokerId )
        {
            ArrayList rootIds = new ArrayList( 100 );

            SqlParameter[] pars = new SqlParameter[] { new SqlParameter("@BrokerId", brokerId ) };
            // This will get us the ids of all products where it's a first base or standalone products, no master
            using( DbDataReader reader = StoredProcedureHelper.ExecuteReader( DataSrc.LpeSrc, "ListRootBaseNonmasterProdIdsOfThisBroker_2", pars ) )
            {
                while( reader.Read() )
                {
                    rootIds.Add( new Guid( reader["lLpTemplateId"].ToString() ) );
                }
            }

            foreach( Guid rootId in rootIds )
            {
                RemoveInheritanceCacheThenSpreadThisNonmasterRootProd( rootId, true /*updateAssociateTable*/ );
            }
        }

		static internal void UpdateOnlyProgramsMarkedAsNeedToSpreadData()
		{
			ArrayList rootIds = new ArrayList( 100 );
			// This will get us the ids of all products where it's a first base or standalone products, no master
			using( DbDataReader reader = StoredProcedureHelper.ExecuteReader( DataSrc.LpeSrc, "ListLoanProgramsNeedProductDataSpreading" ) )
			{
				while( reader.Read() )
				{
					rootIds.Add( new Guid( reader["lLpTemplateId"].ToString() ) );
				}
			}

			foreach( Guid rootId in rootIds )
			{
				SpreadChangesFor( rootId,  /* updateAssociateTable */ false );
			}

			StoredProcedureHelper.ExecuteNonQuery(  DataSrc.LpeSrc, "ClearIsSpreadingNeededForProductDataBit", 3 );
		}

        static internal void UpdateAllNonmasterProds(bool updateAssociateTable)
        {
            ArrayList rootIds = new ArrayList( 100 );
            // This will get us the ids of all products where it's a first base or standalone products, no master
            using( DbDataReader reader = StoredProcedureHelper.ExecuteReader( DataSrc.LpeSrc, "ListRootBaseNonmasterProdIds_2" ) )
            {
                while( reader.Read() )
                {
                    rootIds.Add( new Guid( reader["lLpTemplateId"].ToString() ) );
                }
            }

            foreach( Guid rootId in rootIds )
            {
                RemoveInheritanceCacheThenSpreadThisNonmasterRootProd( rootId, updateAssociateTable );
            }

        }

        
        static internal void SpreadChangesFor( Guid baseProdId, bool updateAssociateTable )
        {
            RecursiveSpreadRegularChangesFor( baseProdId, updateAssociateTable ); //, ( int[] ) ITEMS_ONLY_OLD.Clone() );            
        }
   

        #endregion //INTERNAL METHODS
        #region PRIVATE METHODS

        // TODO: We eventually can use this mechanism to optimize how far we need to spread and minimize the changes
        private const int SPREAD_ONLY_OLD = 0; // all the old ones that didn't take advantage of this mechanism
        private const int SPREAD_8020_PAIRING = 1;
        private const int SPREAD_ITEM_COUNT = SPREAD_8020_PAIRING + 1; // keep this updated when adding new spread item

        private static int[] ITEMS_ONLY_OLD = new int[ SPREAD_ITEM_COUNT ];
        private static int[] ITEMS_NONE  = new int[ SPREAD_ITEM_COUNT ];
        private static int[] ITEMS_ONLY_8020_PAIRING = new int[ SPREAD_ITEM_COUNT ];

        static ProdDeriveCacheUpdate()
        {
            for( int i = 0; i < SPREAD_ITEM_COUNT; ++i )
            {
                ITEMS_ONLY_OLD[ i ] = 0;
                ITEMS_NONE[ i ] = 0;
                ITEMS_ONLY_8020_PAIRING[ i ] = 0;
            }
        
            ITEMS_ONLY_OLD[ SPREAD_ONLY_OLD ] = 1;
            ITEMS_ONLY_8020_PAIRING[ SPREAD_8020_PAIRING ] = 1;
        }

        // Opm 467620
        // This function uses partial code fromRecursiveSpreadRegularChangesFor( Guid baseProdId, bool updateAssociateTable ) with some modify.
        // In future, we refactor to remove duplicate.
        internal static void UpdateProductFromSomeBasedProduct(Guid derivedId, Guid baseProdId)
        {
            int nMaxTry = 3;

            #region PART 2 : creat the object : new CLoanProductData( baseProdId )

            CLoanProductData b = null; // assigning null to make the compiler happy.
            for (int i = 0; i < nMaxTry; ++i)
            {
                try
                {
                    b = new CLoanProductData(baseProdId);
                    b.InitSave(); // Note that: we are doing InitSave() to avoid loading up the list of folders to compute fullpath.

                    Tools.LogInfo(string.Format("ProdDeriveCacheUpdate.UpdateProductFromSomeBasedProduct() is executed on this product ={0}. id={1}",
                        b.lLpTemplateNm, b.lLpTemplateId.ToString()));
                    break;
                }
                catch (Exception ex)
                {
                    if (i < (nMaxTry - 1))
                    {
                        Tools.LogWarning(string.Format("UpdateProductFromSomeBasedProduct PART 2 is encountering error with product Id = {0}.  Basically it's doing InitLoad for CLoanProductData.  Sleeping for 5 seconds before retrying. i={1}",
                            baseProdId.ToString(), i.ToString()), ex);
                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(5000);
                    }
                    else
                    {
                        Tools.LogError(string.Format("UpdateProductFromSomeBasedProduct PART 2 is giving up on product Id = {0}. Basically it's doing InitLoad for CLoanProductData", baseProdId.ToString()), ex);
                        throw;
                    }
                }
            }
            #endregion // PART 2

            CLoanProductData d = null; // assigning null to make compiler happy
            for (int i = 0; i < nMaxTry; ++i)
            {
                try
                {
                    d = new CLoanProductData(derivedId);
                    d.InitSave();
                    E_sLienPosT lienPosT = d.lLienPosT;

                    #region UPDATE derived LOAN_PRODUCT_TEMPLATE
                    d.IsEnabledInherit = b.IsEnabled;
                    d.IsEnabled = b.IsEnabled;
                    d.IsEnabledOverrideBit = true;

                    d.lCcTemplateIdInherit = b.lCcTemplateId;
                    if (!d.lCcTemplateIdOverrideBit)
                        d.lCcTemplateId = b.lCcTemplateId;

                    d.lLockedDaysLowerSearchInherit = b.lLockedDaysLowerSearch;
                    d.lLockedDaysInherit = b.lLockedDays;
                    if (!d.lLockedDaysOverrideBit)
                    {
                        d.lLockedDaysLowerSearch = b.lLockedDaysLowerSearch;
                        d.lLockedDays = b.lLockedDays;
                    }

                    d.lLpeFeeMaxInherit = b.lLpeFeeMax;
                    d.lLpeFeeMax = b.lLpeFeeMax;
                    d.lLpeFeeMaxOverrideBit = true;

                    d.lLpeFeeMinInherit = b.lLpeFeeMin;
                    d.lLpeFeeMinParam = b.lLpeFeeMin;
                    d.lLpeFeeMin = b.lLpeFeeMin;
                    d.lLpeFeeMinOverrideBit = true;

                    d.lLpeFeeMinAddRuleAdjInheritBit = b.lLpeFeeMinAddRuleAdjBit;

                    d.lLpTemplateNmInherit = b.lLpTemplateNm;
                    if (!d.lLpTemplateNmOverrideBit)
                        d.lLpTemplateNm = b.lLpTemplateNm;

                    d.lPpmtPenaltyMonLowerSearchInherit = b.lPpmtPenaltyMonLowerSearch;
                    d.lPpmtPenaltyMonInherit = b.lPpmtPenaltyMon;
                    if (!d.lPpmtPenaltyMonOverrideBit)
                    {
                        d.lPpmtPenaltyMonLowerSearch = b.lPpmtPenaltyMonLowerSearch;
                        d.lPpmtPenaltyMon = b.lPpmtPenaltyMon;
                    }

                    d.lIOnlyMonLowerSearchInherit = b.lIOnlyMonLowerSearchInherit;
                    if (!d.lIOnlyMonLowerSearchOverrideBit)
                    {
                        d.lIOnlyMonLowerSearch = b.lIOnlyMonLowerSearch;
                    }

                    d.lIOnlyMonUpperSearchInherit = b.lIOnlyMonUpperSearchInherit;
                    if (!d.lIOnlyMonUpperSearchOverrideBit)
                    {
                        d.lIOnlyMonUpperSearch = b.lIOnlyMonUpperSearch;
                    }

                    d.lLpCustomCode1Inherit = b.lLpCustomCode1;
                    if (!d.lLpCustomCode1OverrideBit)
                    {
                        d.lLpCustomCode1 = b.lLpCustomCode1;
                    }

                    d.lLpCustomCode2Inherit = b.lLpCustomCode2;
                    if (!d.lLpCustomCode2OverrideBit)
                    {
                        d.lLpCustomCode2 = b.lLpCustomCode2;
                    }

                    d.lLpCustomCode3Inherit = b.lLpCustomCode3;
                    if (!d.lLpCustomCode3OverrideBit)
                    {
                        d.lLpCustomCode3 = b.lLpCustomCode3;
                    }

                    d.lLpCustomCode4Inherit = b.lLpCustomCode4;
                    if (!d.lLpCustomCode4OverrideBit)
                    {
                        d.lLpCustomCode4 = b.lLpCustomCode4;
                    }

                    d.lLpCustomCode5Inherit = b.lLpCustomCode5;
                    if (!d.lLpCustomCode5OverrideBit)
                    {
                        d.lLpCustomCode5 = b.lLpCustomCode5;
                    }

                    d.lLpInvestorCode1Inherit = b.lLpInvestorCode1;
                    if (!d.lLpInvestorCode1OverrideBit)
                    {
                        d.lLpInvestorCode1 = b.lLpInvestorCode1;
                    }

                    d.lLpInvestorCode2Inherit = b.lLpInvestorCode2;
                    if (!d.lLpInvestorCode2OverrideBit)
                    {
                        d.lLpInvestorCode2 = b.lLpInvestorCode2;
                    }

                    d.lLpInvestorCode3Inherit = b.lLpInvestorCode3;
                    if (!d.lLpInvestorCode3OverrideBit)
                    {
                        d.lLpInvestorCode3 = b.lLpInvestorCode3;
                    }

                    d.lLpInvestorCode4Inherit = b.lLpInvestorCode4;
                    if (!d.lLpInvestorCode4OverrideBit)
                    {
                        d.lLpInvestorCode4 = b.lLpInvestorCode4;
                    }

                    d.lLpInvestorCode5Inherit = b.lLpInvestorCode5;
                    if (!d.lLpInvestorCode5OverrideBit)
                    {
                        d.lLpInvestorCode5 = b.lLpInvestorCode5;
                    }

                    d.lHelocCalculatePrepaidInterest = b.lHelocCalculatePrepaidInterest;

                    if (!d.lRateSheetXmlContentOverrideBit)
                    {
#if NOUSE_RATE_OPTION_TABLE
                                d.lRateSheetDataSetWithoutDeltas = b.lRateSheetDataSetWithDeltas;
#endif

                    }

                    // 6/11/2018 Britt's suggested: "becoming independent programs" don't inherit lien data.
                    switch (b.lLienPosT)
                    {
                    case E_sLienPosT.First:
                        d.PairingProductIds = ""; // non-applicable to 1st lien products
                        d.PairIdFor1stLienProdGuidInherit = Guid.Empty;
                        d.PairIdFor1stLienProdGuid = d.lLpTemplateId; // its own product id

                        break;
                    case E_sLienPosT.Second:
                        d.PairingProductIdsInherit = "";
                        d.PairIdFor1stLienProdGuidInherit = Guid.Empty;
                        d.PairIdFor1stLienProdGuid = d.lLpTemplateId; // not used but just for consistency
                        d.PairingProductIds = "";
                        break;
                    default:
                        throw new CBaseException(ErrorMessages.EnumValueNotHandled, "Unhandled enum value for E_sLienPosT in RecursiveSpreadRegularChangesFor()");
                    }

                    d.PairingProductIdsOverrideBit = true;
                    d.lLendNmInherit = b.lLendNm;
                    if (!d.lLendNmOverrideBit)
                        d.lLendNm = b.lLendNm;

                    d.lLT = b.lLT;
                    d.lLienPosT = b.lLienPosT;
                    d.lQualR = b.lQualR;
                    d.lTerm = b.lTerm;
                    d.lDue = b.lDue;
                    d.lReqTopR = b.lReqTopR;
                    d.lReqBotR = b.lReqBotR;
                    d.lRadj1stCapR = b.lRadj1stCapR;
                    d.lRadj1stCapMon = b.lRadj1stCapMon;
                    d.lRAdjCapR = b.lRAdjCapR;
                    d.lRAdjCapMon = b.lRAdjCapMon;
                    d.lRAdjLifeCapR = b.lRAdjLifeCapR;
                    d.lRAdjMarginR = b.lRAdjMarginR;
                    d.lRAdjIndexR = b.lRAdjIndexR;
                    d.lRAdjFloorR = b.lRAdjFloorR;
                    d.lRAdjRoundT = b.lRAdjRoundT;
                    d.lPmtAdjCapR = b.lPmtAdjCapR;
                    d.lPmtAdjCapMon = b.lPmtAdjCapMon;
                    d.lPmtAdjRecastPeriodMon = b.lPmtAdjRecastPeriodMon;
                    d.lPmtAdjRecastStop = b.lPmtAdjRecastStop;
                    d.lBuydwnR1 = b.lBuydwnR1;
                    d.lBuydwnR2 = b.lBuydwnR2;
                    d.lBuydwnR3 = b.lBuydwnR3;
                    d.lBuydwnR4 = b.lBuydwnR4;
                    d.lBuydwnR5 = b.lBuydwnR5;
                    d.lBuydwnMon1 = b.lBuydwnMon1;
                    d.lBuydwnMon2 = b.lBuydwnMon2;
                    d.lBuydwnMon3 = b.lBuydwnMon3;
                    d.lBuydwnMon4 = b.lBuydwnMon4;
                    d.lBuydwnMon5 = b.lBuydwnMon5;
                    d.lGradPmtYrs = b.lGradPmtYrs;
                    d.lIOnlyMon = b.lIOnlyMon;
                    d.lHasVarRFeature = b.lHasVarRFeature;
                    d.lVarRNotes = b.lVarRNotes;
                    d.lAprIncludesReqDeposit = b.lAprIncludesReqDeposit;
                    d.lHasDemandFeature = b.lHasDemandFeature;
                    d.lLateDays = b.lLateDays;
                    d.lLateChargePc = b.lLateChargePc;
                    d.lPrepmtPenaltyT = b.lPrepmtPenaltyT;
                    d.lAssumeLT = b.lAssumeLT;
                    d.lFilingF = b.lFilingF;
                    d.lLateChargeBaseDesc = b.lLateChargeBaseDesc;
                    d.lPmtAdjMaxBalPc = b.lPmtAdjMaxBalPc;
                    d.lFinMethT = b.lFinMethT;
                    d.lFinMethDesc = b.lFinMethDesc;
                    d.lPrepmtRefundT = b.lPrepmtRefundT;
                    d.lRateSheetEffectiveD = b.lRateSheetEffectiveD;
                    d.lRateSheetExpirationD = b.lRateSheetExpirationD;

                    Tools.Assert(!d.IsMaster && !b.IsMaster,
                        string.Format("Product derivation should not be applicable for master products. base = {0}. derived = {1}",
                        b.lLpTemplateId.ToString(), d.lLpTemplateId.ToString()));

                    d.lArmIndexGuid = Guid.Empty;
                    if (d.lBaseLpId == b.lLpTemplateId || d.BrokerId == b.BrokerId || SystemArmIndex.ListSystemArmIndexes().Any(arm => arm.IndexIdGuid == b.lArmIndexGuid))
                    {
                        d.lArmIndexGuid = b.lArmIndexGuid;
                        d.lArmIndexBasedOnVstr = b.lArmIndexBasedOnVstr;
                        d.lArmIndexCanBeFoundVstr = b.lArmIndexCanBeFoundVstr;
                        d.lArmIndexAffectInitIRBit = b.lArmIndexAffectInitIRBit;
                        d.lArmIndexNotifyAtLeastDaysVstr = b.lArmIndexNotifyAtLeastDaysVstr;
                        d.lArmIndexNotifyNotBeforeDaysVstr = b.lArmIndexNotifyNotBeforeDaysVstr;
                        d.lArmIndexT = b.lArmIndexT;
                        d.lArmIndexNameVstr = b.lArmIndexNameVstr;
                        d.lArmIndexEffectiveD = b.lArmIndexEffectiveD;
                        d.lFreddieArmIndexT = b.lFreddieArmIndexT;
                    }

                    d.lRAdjRoundToR = b.lRAdjRoundToR;
                    d.lLpInvestorNm = b.lLpInvestorNm;

                    d.ProductCodeInherit = b.ProductCode;
                    if (!d.ProductCodeOverrideBit)
                    {
                        d.ProductCode = b.ProductCode;
                    }

                    d.ProductIdentifierInherited = b.ProductIdentifier;
                    if (!d.ProductIdentifierOverride)
                    {
                        d.ProductIdentifier = b.ProductIdentifier;
                    }

                    d.IsOptionArm = b.IsOptionArm;
                    d.lIsArmMarginDisplayed = b.lIsArmMarginDisplayed;
                    d.lLpDPmtT = b.lLpDPmtT;
                    d.lLpQPmtT = b.lLpQPmtT;
                    d.lHasQRateInRateOptions = b.lHasQRateInRateOptions;
                    d.lQualRateCalculationT = b.lQualRateCalculationT;
                    d.lQualRateCalculationFieldT1 = b.lQualRateCalculationFieldT1;
                    d.lQualRateCalculationFieldT2 = b.lQualRateCalculationFieldT2;
                    d.lQualRateCalculationAdjustment1 = b.lQualRateCalculationAdjustment1;
                    d.lQualRateCalculationAdjustment2 = b.lQualRateCalculationAdjustment2;
                    d.lQualTerm = b.lQualTerm;
                    d.lQualTermCalculationType = b.lQualTermCalculationType;
                    d.IsLpeDummyProgram = b.IsLpeDummyProgram;
                    d.IsPairedOnlyWithSameInvestor = b.IsPairedOnlyWithSameInvestor;
                    d.CanBeStandAlone2nd = b.CanBeStandAlone2nd;
                    d.lDtiUsingMaxBalPc = b.lDtiUsingMaxBalPc;
                    d.lLpProductType = b.lLpProductType;
                    d.IsConvertibleMortgage = b.IsConvertibleMortgage;
                    d.lRAdjFloorBaseT = b.lRAdjFloorBaseT;
                    d.lWholesaleChannelProgram = b.lWholesaleChannelProgram;
                    d.IsNonQmProgram = b.IsNonQmProgram;
                    d.IsDisplayInNonQmQuickPricer = b.IsDisplayInNonQmQuickPricer;

                    d.lLpmiSupportedOutsidePmiInherit = b.lLpmiSupportedOutsidePmi;
                    if (!d.lLpmiSupportedOutsidePmiOverrideBit)
                    {
                        d.lLpmiSupportedOutsidePmi = b.lLpmiSupportedOutsidePmi;
                    }

                    d.lHardPrepmtPeriodMonths = b.lHardPrepmtPeriodMonths;
                    d.lSoftPrepmtPeriodMonths = b.lSoftPrepmtPeriodMonths;

                    d.SuppressEventHandlerCall = true;

                    d.Save();
                    #endregion // UPDATE LOAN_PRODUCT_TEMPLATE
                    break; // breaking out of the retry loop
                } // try
                catch (Exception ex)
                {
                    if (i < (nMaxTry - 1))
                    {
                        Tools.LogWarning(string.Format("RecursiveSpreadRegularChangesFor PART 4 encounters error on derived product Id = {0}. Operations are CLoanProductData's InitSave and Save for derived product. Sleeping for 10 seconds before retrying. i={1} ",
                            derivedId.ToString(), i.ToString()), ex);
                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(5000);
                    }
                    else
                    {
                        Tools.LogError(string.Format("RecursiveSpreadRegularChangesFor PART 4 is giving up on derived product Id = {0}. Operations are CLoanProductData's InitSave and Save for derived product. ", derivedId.ToString()), ex);
                        throw;
                    }
                }
            }
        }

        static private void RecursiveSpreadRegularChangesFor( Guid baseProdId, bool updateAssociateTable )
        {
            ArrayList derivedIds = new ArrayList( 10 );

            int nMaxTry = 3;

            #region PART 1 : get derivedIds from baseProdId

            for( int i = 0; i < nMaxTry; ++i )
            {
                try
                {
                    // 1) Retrieve products deriving from baseProdId
                    SqlParameter[] pars = new SqlParameter[] { new SqlParameter("@BaseProdId", baseProdId ) };

                    using( DbDataReader reader = StoredProcedureHelper.ExecuteReader( DataSrc.LpeSrc, "ListDerivedProductsOf", pars ) )
                    {
                        while( reader.Read() )
                        {
                            derivedIds.Add( new Guid( reader["lLpTemplateId"].ToString() ) );
                        }
                    }

                    break;
                }
                catch( Exception ex )
                {
                    if( i < ( nMaxTry - 1 ) )
                    {
                        Tools.LogWarning( string.Format( "RecursiveSpreadRegularChangesFor PART 1 is encountering error with product Id = {0}.  Store proc involved is ListDerivedProductsOf. Sleeping for 5 seconds before retrying. i={1}", 
                            baseProdId.ToString(), i.ToString() ), ex );
                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep( 5000 );
                    }
                    else
                    {
                        Tools.LogError( string.Format( "RecursiveSpreadRegularChangesFor PART 1 is giving up on product Id = {0}", baseProdId.ToString() ), ex );
                        throw ex;
                    }
                }
            }

            if( derivedIds.Count <= 0 )
                return;
            #endregion // PART 1

            #region PART 2 : creat the object : new CLoanProductData( baseProdId )

            CLoanProductData  b = null; // assigning null to make the compiler happy.
            for( int i = 0; i < nMaxTry; ++i )
            {
                try
                {
                    b = new CLoanProductData( baseProdId );
                    b.InitSave(); // Note that: we are doing InitSave() to avoid loading up the list of folders to compute fullpath.

                    Tools.LogRegTest( string.Format( "ProdDeriveCacheUpdate.RecursiveSpreadRegularChangesFor() is executed on this product ={0}. id={1}",  
                        b.lLpTemplateNm, b.lLpTemplateId.ToString() ) );                   
                    break;
                }
                catch( Exception ex )
                {
                    if( i < ( nMaxTry - 1 ) )
                    {
                        Tools.LogWarning( string.Format( "RecursiveSpreadRegularChangesFor PART 2 is encountering error with product Id = {0}.  Basically it's doing InitLoad for CLoanProductData.  Sleeping for 5 seconds before retrying. i={1}", 
                            baseProdId.ToString(), i.ToString() ), ex );
                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep( 5000 );
                    }
                    else
                    {
                        Tools.LogError( string.Format( "RecursiveSpreadRegularChangesFor PART 2 is giving up on product Id = {0}. Basically it's doing InitLoad for CLoanProductData", baseProdId.ToString() ), ex );
                        throw ex;
                    }
                }
            }
            #endregion // PART 2


            CDataSet dsB = null; // assigning null to make the compiler happy.
            try // to dispose dsB in the "finally" block
            {            
                #region PART 3 : dsB = policies of the product baseProdId

                for( int i = 0; i < nMaxTry && updateAssociateTable; ++i )
                {
                    try
                    {
                        dsB = new CDataSet();

                        // 11/21/2007 ThienNguyen - Reviewed and safe
                        dsB.LoadWithSqlQueryDangerously( DataSrc.LpeSrc, string.Format( "select * from product_price_association where productid = '{0}'", baseProdId.ToString() ) );
                        break;
                    }
                    catch( Exception ex )
                    {
                        if( i < ( nMaxTry - 1 ) )
                        {
                            Tools.LogWarning( string.Format( "RecursiveSpreadRegularChangesFor PART 3 is encountering error with product Id = {0}.  The operation is: select * from product_price_association where productid = [ productid].  Sleeping for 5 seconds before retrying. i={1}", 
                                baseProdId.ToString(), i.ToString() ), ex );
                            LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep( 5000 );
                        }
                        else
                        {
                            Tools.LogError( string.Format( "RecursiveSpreadRegularChangesFor PART 3 is giving up on product Id = {0}. The operation is: select * from product_price_association where productid = [ productid]", baseProdId.ToString() ), ex );
                            throw ex;
                        }
                    } // catch
                } // for loop for retrying
           
                #endregion // PART 3

                CAssocCalc aCalcB = new CAssocCalc(); // Recycling for base's association
                CAssocCalc aCalcD = new CAssocCalc(); // recycling for derived's association

                // policy association tables
                CRowHashtable paB = (dsB != null ? new CRowHashtable( dsB.Tables[0].Rows, "PricePolicyId" ) : null);

                // 2) For each of the derived product
                foreach( Guid derivedId in derivedIds )
                {
                    // This one will be used in recursive call
                    // int[]  childSpreadItems = ( int[] ) spreadItems.Clone();
                    //		a) Update the cache for the product
                    // Derived product
                
                    // PART 4

                    CLoanProductData d = null; // assigning null to make compiler happy
                    for( int i = 0; i < nMaxTry; ++i )
                    {
                        try
                        {
                            d = new CLoanProductData( derivedId ); 
                            d.InitSave();
                            E_sLienPosT lienPosT              = d.lLienPosT;
                
                        #region UPDATE derived LOAN_PRODUCT_TEMPLATE
                            d.IsEnabledInherit = b.IsEnabled;
                            if( !d.IsEnabledOverrideBit )
                                d.IsEnabled = b.IsEnabled;
							
                            d.lCcTemplateIdInherit = b.lCcTemplateId;
                            if( !d.lCcTemplateIdOverrideBit )
                                d.lCcTemplateId = b.lCcTemplateId;

                            d.lLockedDaysLowerSearchInherit = b.lLockedDaysLowerSearch;
                            d.lLockedDaysInherit = b.lLockedDays;
                            if( !d.lLockedDaysOverrideBit )
                            {
                                d.lLockedDaysLowerSearch = b.lLockedDaysLowerSearch;
                                d.lLockedDays = b.lLockedDays;
                            }
					
                            d.lLpeFeeMaxInherit = b.lLpeFeeMax;
                            if( !d.lLpeFeeMaxOverrideBit )
                                d.lLpeFeeMax = b.lLpeFeeMax;

                            d.lLpeFeeMinInherit = b.lLpeFeeMin;
                            if( d.lLpeFeeMinOverrideBit )
                                d.lLpeFeeMin = d.lLpeFeeMinParam;
                            else
                                d.lLpeFeeMin = b.lLpeFeeMin + d.lLpeFeeMinParam;

                            d.lLpeFeeMinAddRuleAdjInheritBit = b.lLpeFeeMinAddRuleAdjBit;
                    
                            d.lLpTemplateNmInherit = b.lLpTemplateNm;
                            if( !d.lLpTemplateNmOverrideBit )
                                d.lLpTemplateNm = b.lLpTemplateNm;

                            d.lPpmtPenaltyMonLowerSearchInherit = b.lPpmtPenaltyMonLowerSearch;
                            d.lPpmtPenaltyMonInherit = b.lPpmtPenaltyMon;
                            if( !d.lPpmtPenaltyMonOverrideBit )
                            {
                                d.lPpmtPenaltyMonLowerSearch = b.lPpmtPenaltyMonLowerSearch;
                                d.lPpmtPenaltyMon = b.lPpmtPenaltyMon;
                            }

							d.lIOnlyMonLowerSearchInherit = b.lIOnlyMonLowerSearchInherit;
							if (!d.lIOnlyMonLowerSearchOverrideBit )
							{
								d.lIOnlyMonLowerSearch = b.lIOnlyMonLowerSearch;
							}

							d.lIOnlyMonUpperSearchInherit = b.lIOnlyMonUpperSearchInherit;
							if (!d.lIOnlyMonUpperSearchOverrideBit )
							{
								d.lIOnlyMonUpperSearch = b.lIOnlyMonUpperSearch;
							}

                            d.lLpCustomCode1Inherit = b.lLpCustomCode1;
                            if (!d.lLpCustomCode1OverrideBit)
                            {
                                d.lLpCustomCode1 = b.lLpCustomCode1;
                            }

                            d.lLpCustomCode2Inherit = b.lLpCustomCode2;
                            if (!d.lLpCustomCode2OverrideBit)
                            {
                                d.lLpCustomCode2 = b.lLpCustomCode2;
                            }

                            d.lLpCustomCode3Inherit = b.lLpCustomCode3;
                            if (!d.lLpCustomCode3OverrideBit)
                            {
                                d.lLpCustomCode3 = b.lLpCustomCode3;
                            }

                            d.lLpCustomCode4Inherit = b.lLpCustomCode4;
                            if (!d.lLpCustomCode4OverrideBit)
                            {
                                d.lLpCustomCode4 = b.lLpCustomCode4;
                            }

                            d.lLpCustomCode5Inherit = b.lLpCustomCode5;
                            if (!d.lLpCustomCode5OverrideBit)
                            {
                                d.lLpCustomCode5 = b.lLpCustomCode5;
                            }

                            d.lLpInvestorCode1Inherit = b.lLpInvestorCode1;
                            if (!d.lLpInvestorCode1OverrideBit)
                            {
                                d.lLpInvestorCode1 = b.lLpInvestorCode1;
                            }

                            d.lLpInvestorCode2Inherit = b.lLpInvestorCode2;
                            if (!d.lLpInvestorCode2OverrideBit)
                            {
                                d.lLpInvestorCode2 = b.lLpInvestorCode2;
                            }

                            d.lLpInvestorCode3Inherit = b.lLpInvestorCode3;
                            if (!d.lLpInvestorCode3OverrideBit)
                            {
                                d.lLpInvestorCode3 = b.lLpInvestorCode3;
                            }

                            d.lLpInvestorCode4Inherit = b.lLpInvestorCode4;
                            if (!d.lLpInvestorCode4OverrideBit)
                            {
                                d.lLpInvestorCode4 = b.lLpInvestorCode4;
                            }

                            d.lLpInvestorCode5Inherit = b.lLpInvestorCode5;
                            if (!d.lLpInvestorCode5OverrideBit)
                            {
                                d.lLpInvestorCode5 = b.lLpInvestorCode5;
                            }

                            d.lHelocCalculatePrepaidInterest = b.lHelocCalculatePrepaidInterest;

                            d.IsNonQmProgram = b.IsNonQmProgram;
                            d.IsDisplayInNonQmQuickPricer = b.IsDisplayInNonQmQuickPricer;

#if NOUSE_RATE_OPTION_TABLE
                            d.lRateSheetDataSetInherit    = b.lRateSheetDataSetWithDeltas;
#else
                            d.SrcRateOptionsProgIdInherit = b.EffectiveSrcRateOptionProgId;
                            d.lRateDeltaInherit           = b.effective_lRateDelta;
                            d.lFeeDeltaInherit            = b.effective_lFeeDelta;
#endif

                            if( !d.lRateSheetXmlContentOverrideBit )
                            {
#if NOUSE_RATE_OPTION_TABLE
                                d.lRateSheetDataSetWithoutDeltas = b.lRateSheetDataSetWithDeltas;
#endif

                            }

							d.lRateOptionBaseId              = b.lRateOptionBaseId;



                            switch( b.lLienPosT )
                            {
                                case E_sLienPosT.First:
                                    d.PairingProductIds = ""; // non-applicable to 1st lien products
                                    d.PairIdFor1stLienProdGuidInherit = b.PairIdFor1stLienProdGuid;
                                    if( d.PairingProductIdsOverrideBit )
                                        d.PairIdFor1stLienProdGuid = d.lLpTemplateId; // its own product id
                                    else
                                        d.PairIdFor1stLienProdGuid = b.PairIdFor1stLienProdGuid; // from its parent
                                    break;
                                case E_sLienPosT.Second:
                                    d.PairingProductIdsInherit = b.PairingProductIds;
                                    d.PairIdFor1stLienProdGuidInherit = Guid.Empty;
                                    d.PairIdFor1stLienProdGuid = d.lLpTemplateId; // not used but just for consistency
                                    if( !d.PairingProductIdsOverrideBit )
                                        d.PairingProductIds = b.PairingProductIds;
                                    break;
                                default:
                                    throw new CBaseException( ErrorMessages.EnumValueNotHandled, "Unhandled enum value for E_sLienPosT in RecursiveSpreadRegularChangesFor()" );
                            }
					
                            d.lLendNmInherit			= b.lLendNm;
                            if( !d.lLendNmOverrideBit )
                                d.lLendNm				= b.lLendNm;

                            d.lLT						= b.lLT;
                            d.lLienPosT					= b.lLienPosT;
                            d.lQualR					= b.lQualR;
                            d.lTerm						= b.lTerm;
                            d.lDue						= b.lDue;
                            d.lReqTopR					= b.lReqTopR;
                            d.lReqBotR					= b.lReqBotR;
                            d.lRadj1stCapR				= b.lRadj1stCapR;
                            d.lRadj1stCapMon			= b.lRadj1stCapMon;
                            d.lRAdjCapR					= b.lRAdjCapR;
                            d.lRAdjCapMon				= b.lRAdjCapMon;
                            d.lRAdjLifeCapR 			= b.lRAdjLifeCapR;
                            d.lRAdjMarginR  			= b.lRAdjMarginR;
                            d.lRAdjIndexR				= b.lRAdjIndexR;
                            d.lRAdjFloorR				= b.lRAdjFloorR;
                            d.lRAdjRoundT				= b.lRAdjRoundT;
                            d.lPmtAdjCapR				= b.lPmtAdjCapR;
                            d.lPmtAdjCapMon 			= b.lPmtAdjCapMon;
                            d.lPmtAdjRecastPeriodMon	= b.lPmtAdjRecastPeriodMon;
                            d.lPmtAdjRecastStop			= b.lPmtAdjRecastStop;
                            d.lBuydwnR1					= b.lBuydwnR1;
                            d.lBuydwnR2					= b.lBuydwnR2;
                            d.lBuydwnR3					= b.lBuydwnR3;
                            d.lBuydwnR4					= b.lBuydwnR4;
                            d.lBuydwnR5					= b.lBuydwnR5;
                            d.lBuydwnMon1				= b.lBuydwnMon1;
                            d.lBuydwnMon2				= b.lBuydwnMon2;
                            d.lBuydwnMon3				= b.lBuydwnMon3;
                            d.lBuydwnMon4				= b.lBuydwnMon4;
                            d.lBuydwnMon5				= b.lBuydwnMon5;
                            d.lGradPmtYrs				= b.lGradPmtYrs;
                            d.lIOnlyMon					= b.lIOnlyMon;
                            d.lHasVarRFeature			= b.lHasVarRFeature;
                            d.lVarRNotes				= b.lVarRNotes;
                            d.lAprIncludesReqDeposit	= b.lAprIncludesReqDeposit;
                            d.lHasDemandFeature			= b.lHasDemandFeature;
                            d.lLateDays					= b.lLateDays;
                            d.lLateChargePc				= b.lLateChargePc;
                            d.lPrepmtPenaltyT			= b.lPrepmtPenaltyT;
                            d.lAssumeLT					= b.lAssumeLT;
                            d.lFilingF					= b.lFilingF;
                            d.lLateChargeBaseDesc		= b.lLateChargeBaseDesc;
                            d.lPmtAdjMaxBalPc			= b.lPmtAdjMaxBalPc;
                            d.lFinMethT					= b.lFinMethT;
                            d.lFinMethDesc				= b.lFinMethDesc;
                            d.lPrepmtRefundT			= b.lPrepmtRefundT;
                            d.lRateSheetEffectiveD		= b.lRateSheetEffectiveD;
                            d.lRateSheetExpirationD		= b.lRateSheetExpirationD;
                    
                            Tools.Assert( !d.IsMaster && !b.IsMaster, 
                                string.Format( "Product derivation should not be applicable for master products. base = {0}. derived = {1}",
                                b.lLpTemplateId.ToString(), d.lLpTemplateId.ToString() ) );

                            d.lArmIndexGuid				= b.lArmIndexGuid;
                            d.lArmIndexBasedOnVstr		= b.lArmIndexBasedOnVstr;
                            d.lArmIndexCanBeFoundVstr	= b.lArmIndexCanBeFoundVstr;
                            d.lArmIndexAffectInitIRBit	= b.lArmIndexAffectInitIRBit;
                            d.lArmIndexNotifyAtLeastDaysVstr = b.lArmIndexNotifyAtLeastDaysVstr;
                            d.lArmIndexNotifyNotBeforeDaysVstr = b.lArmIndexNotifyNotBeforeDaysVstr;
                            d.lRAdjRoundToR				= b.lRAdjRoundToR;
                            d.lArmIndexT				= b.lArmIndexT;
                            d.lArmIndexNameVstr			= b.lArmIndexNameVstr;
                            d.lArmIndexEffectiveD		= b.lArmIndexEffectiveD;
                            d.lFreddieArmIndexT			= b.lFreddieArmIndexT;
                            d.lLpInvestorNm             = b.lLpInvestorNm;

                            d.ProductCodeInherit = b.ProductCode;
                            if (!d.ProductCodeOverrideBit)
                            {
                                d.ProductCode = b.ProductCode;
                            }

                            d.ProductIdentifierInherited = b.ProductIdentifier;
                            if (!d.ProductIdentifierOverride)
                            {
                                d.ProductIdentifier = b.ProductIdentifier;
                            }

                            d.IsOptionArm               = b.IsOptionArm;
                            d.lIsArmMarginDisplayed     = b.lIsArmMarginDisplayed;
							d.lLpDPmtT                  = b.lLpDPmtT;
							d.lLpQPmtT                  = b.lLpQPmtT;
							d.lHasQRateInRateOptions    = b.lHasQRateInRateOptions;
                            d.lQualRateCalculationT     = b.lQualRateCalculationT;
                            d.lQualRateCalculationFieldT1 = b.lQualRateCalculationFieldT1;
                            d.lQualRateCalculationFieldT2 = b.lQualRateCalculationFieldT2;
                            d.lQualRateCalculationAdjustment1 = b.lQualRateCalculationAdjustment1;
                            d.lQualRateCalculationAdjustment2 = b.lQualRateCalculationAdjustment2;
                            d.lQualTerm = b.lQualTerm;
                            d.lQualTermCalculationType = b.lQualTermCalculationType;
                            d.IsLpeDummyProgram         = b.IsLpeDummyProgram;
							d.IsPairedOnlyWithSameInvestor = b.IsPairedOnlyWithSameInvestor;
							d.CanBeStandAlone2nd        = b.CanBeStandAlone2nd;
							d.lDtiUsingMaxBalPc         = b.lDtiUsingMaxBalPc;
							d.lLpProductType            = b.lLpProductType;
                            d.IsConvertibleMortgage     = b.IsConvertibleMortgage;
                            d.lRAdjFloorBaseT           = b.lRAdjFloorBaseT;
                            d.lWholesaleChannelProgram  = b.lWholesaleChannelProgram;

                            d.lLpmiSupportedOutsidePmiInherit = b.lLpmiSupportedOutsidePmi;
                            if (!d.lLpmiSupportedOutsidePmiOverrideBit)
                            {
                                d.lLpmiSupportedOutsidePmi = b.lLpmiSupportedOutsidePmi;
                            }
                            
                            d.lHardPrepmtPeriodMonths   = b.lHardPrepmtPeriodMonths;
                            d.lSoftPrepmtPeriodMonths   = b.lSoftPrepmtPeriodMonths;

                            d.SuppressEventHandlerCall  = true;

                            d.Save();
                        #endregion // UPDATE LOAN_PRODUCT_TEMPLATE
                            break; // breaking out of the retry loop
                        } // try
                        catch( Exception ex )
                        {
                            if( i < ( nMaxTry - 1 ) )
                            {
                                Tools.LogWarning( string.Format( "RecursiveSpreadRegularChangesFor PART 4 encounters error on derived product Id = {0}. Operations are CLoanProductData's InitSave and Save for derived product. Sleeping for 10 seconds before retrying. i={1} ", 
                                    derivedId.ToString(), i.ToString() ), ex );
                                LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep( 5000 );
                            }
                            else
                            {
                                Tools.LogError( string.Format( "RecursiveSpreadRegularChangesFor PART 4 is giving up on derived product Id = {0}. Operations are CLoanProductData's InitSave and Save for derived product. ", derivedId.ToString() ), ex );
                                throw ex;
                            }
                        }                   
                    }    

                #region SPREAD PRICE POLICY ASSOCIATION
                    //We need to spread the policy association here.

                    //[PricePolicyId] [GuidNoDefault] NOT NULL ,
                    //[ProductId] [GuidNoDefault] NOT NULL ,
                    //[AssocOverrideTri] [tinyint] NOT NULL CONSTRAINT [DF__product_p__Assoc__2F9ADBB7] DEFAULT (1),
                    //[InheritVal] [bit] NOT NULL CONSTRAINT [DF__product_p__Inher__308EFFF0] DEFAULT (0),
                    //[InheritValFromBaseProd] [bit] NOT NULL CONSTRAINT [DF__product_p__Inher__068EA483] DEFAULT (0),

                    CDataSet dsD = null;
                    try // to dispose dsD in the "finally" block
                    {
                        // PART 5
                        
                        for( int i = 0; i < nMaxTry && updateAssociateTable; ++i )
                        {
                            try
                            {
                                dsD = new CDataSet();

                                // 11/21/2007 ThienNguyen - Reviewed and safe
                                dsD.LoadWithSqlQueryDangerously( DataSrc.LpeSrc, string.Format( "select * from product_price_association where productId = '{0}'", derivedId.ToString() ) );
						
                                //Policy association
                                CRowHashtable paD = new CRowHashtable( dsD.Tables[0].Rows, "PricePolicyId" );

                                foreach( DataRow rowB in dsB.Tables[0].Rows )
                                {
                                    // TRUTH TABLE:
                                    // Status	FromBase		InheritFromBase		Action
                                    // ----------------------------------------------------
                                    // done		no entry		no entry			none
                                    // done		no entry		0					none
                                    // done		no entry		1					set 0, we can remove entry only if it's not inheriting from the master either
                                    // done		0				no entry			none				
                                    // done		0				0					none, we can remove entry only if it's not inheriting from the master either
                                    // done		0				1					set 0, we can remove entry only if it's not inheriting from the master either
                                    // done		1				no entry			insert entry and set 1
                                    // done		1				0					set 1	
                                    // done		1				1					none
                                    aCalcB.RecycleFor( rowB );
                                    Guid pidB = aCalcB.PricePolicyId;

                                    if( aCalcB.IsAssocAfterMerge )
                                    {
                                        if( paD.HasRowOfKey( pidB ) )
                                        {
                                            aCalcD.RecycleFor( paD.GetRowByKey( pidB ) );
                                            if( !aCalcD.InheritValFromBaseProd )
                                                aCalcD.InheritValFromBaseProd = true;
                                        }
                                        else
                                            InsertNewAssocRow( dsD, pidB, d.lLpTemplateId, E_TriState.Blank, true ); 												
                                    }
                                    else
                                    {
                                        if( paD.HasRowOfKey( pidB ) )
                                        {
                                            aCalcD.RecycleFor( paD.GetRowByKey( pidB ) );
                                            if( aCalcD.InheritValFromBaseProd )
                                                aCalcD.InheritValFromBaseProd = false;
                                        }
                                    }				
                                }

                                foreach( DataRow rowD in dsD.Tables[0].Rows )
                                {
                                    CDataRow rD = new CDataRow( rowD );
                                    aCalcD.RecycleFor( rowD );
                                    Guid pidD = aCalcD.PricePolicyId;
                                    if( aCalcD.InheritValFromBaseProd && !paB.HasRowOfKey( pidD ) )
                                    {					
                                        // Perf: We can check to see if we can remove the derived's association entry from db table.
                                        // As of today, we will have to periodically go to clean out any rows returned from this query:
                                        // select * from product_price_association where inheritval = 0 and assocoverridetri = 0 and InheritValFromBaseProd = 0
                                        aCalcD.SetBoolField( "InheritValFromBaseProd", false );
                                    }
                                }
                            
                                dsD.Save();
                                break;
                            } // try
                            catch( Exception ex )
                            {
                                if( i < ( nMaxTry - 1 ) )
                                {
                                    Tools.LogWarning( string.Format( "RecursiveSpreadRegularChangesFor PART 5 encounters error on derived product Id = {0}. The operations are: select * from product_price_association where productId =[product id] into a dataset AND saving the dataset with changes for derived product. Sleep for 5 seconds before retrying. i={1} ", 
                                        derivedId.ToString(), i.ToString() ), ex );
                                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep( 5000 );
                                }
                                else
                                {
                                    Tools.LogError( string.Format( "RecursiveSpreadRegularChangesFor PART 5 is giving up on derived product Id = {0}. Operations are CLoanProductData's InitSave and Save for derived product. ", derivedId.ToString() ), ex );
                                    throw ex;
                                }
                            }
                        } // for loop retry loop PART 5
                    } // try block to catch to displose dsD
                    finally
                    {
                        if( dsD != null )
                        {
                            dsD.Dispose();
                            dsD = null;
                        }
                    }
					
                #endregion // SPREAD PRICE POLICY UPDATE
 
                }// for each derived product
            } // try block for catching to dispose dsB
            finally
            {
                if( dsB != null )
                {
                    dsB.Dispose();
                    dsB = null;
                }
            }

            foreach( Guid derivedId in derivedIds )
            {
                //		b) Call RecursiveSpreadChangesFor( the product id )
                RecursiveSpreadRegularChangesFor( derivedId, updateAssociateTable ); // Calling this on each of them outside of the loop so that we can free up dsB dataset first.
            }
        }

	    static private void RemoveInheritanceCacheThenSpreadThisNonmasterRootProd( Guid pid, bool updateAssociateTable )
        {
            int nMaxTry = 3;
            for( int i = 0; i < nMaxTry; ++i )
            {
                try
                {
                    CLoanProductData d = new CLoanProductData( pid );
                    d.InitSave();

					// 7/23/2007: The hardcoded values that we are setting in the following part are only true
					// for root (non-derived) programs. opm 17156. -tn 
					if( d.lBaseLpId != Guid.Empty )
					{
						throw new CBaseException( ErrorMessages.Generic, "ProdDeriveCacheUpdate.RemoveInheritanceCacheThenSpreadThisNonmasterRootProd() static method was called for a derived program. Consider ProdDeriveCacheUpdate.SpreadChangesFor() instead" );
					}

                    d.lLpTemplateNmOverrideBit = true;
                    d.lLpTemplateNmInherit = "";

                    d.lCcTemplateIdOverrideBit = true;
                    d.lCcTemplateIdInherit = Guid.Empty;

                    d.lLockedDaysOverrideBit = true;
                    d.lLockedDaysInherit = 0;

                    d.lLpeFeeMinOverrideBit = true;
                    d.lLpeFeeMinInherit = 0;

                    d.lLpeFeeMaxOverrideBit = true;
                    d.lLpeFeeMaxInherit = 0;

                    d.IsEnabledOverrideBit = true;
                    d.IsEnabledInherit = false;

                    d.lPpmtPenaltyMonOverrideBit = true;
                    d.lPpmtPenaltyMonInherit = 0;

                    d.lRateSheetXmlContentOverrideBit = true;

                    d.lHelocCalculatePrepaidInterest = false;

                    d.IsNonQmProgram = false;
                    d.IsDisplayInNonQmQuickPricer = false;

#if NOUSE_RATE_OPTION_TABLE
                    d.lRateSheetXmlContentInherit  = "";

#else
                    d.SrcRateOptionsProgIdInherit  = Guid.Empty;
                    d.lRateDeltaInherit            = 0;
                    d.lFeeDeltaInherit             = 0;
#endif


                    d.lLockedDaysOverrideBit = true;
                    d.lLockedDaysLowerSearchInherit = 0;
                    d.lLockedDaysInherit = 0;

                    d.lPpmtPenaltyMonOverrideBit = true;
                    d.lPpmtPenaltyMonLowerSearchInherit = 0;
                    d.lPpmtPenaltyMonInherit = 0;

					d.lIOnlyMonLowerSearchOverrideBit = true;
					d.lIOnlyMonLowerSearchInherit = 0;
					
					d.lIOnlyMonUpperSearchOverrideBit = true;
					d.lIOnlyMonUpperSearchInherit = 0;

                    d.SuppressEventHandlerCall = true;
                    d.Save();
                    break;
                } // try
                catch( Exception ex )
                {
                    if( i < ( nMaxTry - 1 ) )
                    {
                        Tools.LogWarning( string.Format( "ProdDeriveCacheUpdate::RemoveInheritanceCacheThenSpreadThisNonmasterRootProd. i={0}. Sleeping for 5 seconds", i.ToString() ), ex  );
                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep( 5000 );
                        continue;
                    }

                    Tools.LogErrorWithCriticalTracking( string.Format( "Error in maintaining product inheritance cache for base program spreading, need to manually update this root program = {0}", pid.ToString() ), 
                        ex );
                    throw ex;
                } // catch
            } // for loop to retry

            SpreadChangesFor( pid, updateAssociateTable );

        }


		static private DataRow InsertNewAssocRow( DataSet ds, Guid priceId, Guid prodId, E_TriState overrideTri, bool inheritValFromBaseProd )
		{
			DataRow newRow						= ds.Tables[0].NewRow();
			newRow["PricePolicyId"]				= priceId;
			newRow["ProductId"]					= prodId;
			newRow["AssocOverrideTri"]			= overrideTri;
			newRow["InheritValFromBaseProd"]	= inheritValFromBaseProd;

            ds.Tables[0].Rows.Add( newRow );
			return newRow;
		}
		#endregion // PRIVATE
	}
}