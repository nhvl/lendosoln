﻿namespace LendersOffice.RatePrice
{
    using System.IO;
    using LendersOffice.Drivers.Gateways;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Pricing engine storage that will store data in local computer file system. This class does not store to FileDB3 nor S3.
    /// This class will be use by LpeUpdate as a temporary storage before upload to persistent storage.
    /// Why LpeUpdate need to store data in local file system? Because a single LpeUpdate run could be slow if there are many rate sheets need to upload.
    /// </summary>
    public sealed class LocalFilePriceEngineStorage : IPriceEngineStorage
    {
        /// <summary>
        /// The root parent.
        /// </summary>
        private LocalFilePath rootPath;

        /// <summary>
        /// Initializes a new instance of the <see cref="LocalFilePriceEngineStorage" /> class.
        /// </summary>
        /// <param name="root">The root parent.</param>
        public LocalFilePriceEngineStorage(LocalFilePath root)
        {
            this.rootPath = root;
        }

        /// <summary>
        /// Retrieve the file from local root path storage.
        /// </summary>
        /// <param name="type">The type of file to retrieve.</param>
        /// <param name="key">The key of file to retrieve.</param>
        /// <returns>The local file path contains the file.</returns>
        public LocalFilePath Retrieve(PriceEngineStorageType type, SHA256Checksum key)
        {
            string destination = Path.Combine(this.rootPath.Value, type.ToString(), key.Value);

            var path = LocalFilePath.Create(destination);

            return path.Value;
        }

        /// <summary>
        /// Save the file in local path to local root path.
        /// </summary>
        /// <param name="type">The type of file to save.</param>
        /// <param name="key">The key of file to save.</param>
        /// <param name="path">The local path of the file save.</param>
        public void Save(PriceEngineStorageType type, SHA256Checksum key, LocalFilePath path)
        {
            string destination = Path.Combine(this.rootPath.Value, type.ToString(), key.Value);

            FileInfo fi = new FileInfo(destination);

            if (!fi.Exists)
            {
                string directoryName = Path.GetDirectoryName(destination);

                if (!Directory.Exists(directoryName))
                {
                    Directory.CreateDirectory(directoryName);
                }

                FileOperationHelper.Copy(path.Value, destination, false);
            }
        }
    }
}