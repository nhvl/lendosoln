using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Drivers.Gateways;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.RatePrice;
using LendersOffice.RatePrice.FileBasedPricing;
using LendersOfficeApp.los.RatePrice;
using LqbGrammar;
using LqbGrammar.DataTypes;
using LqbGrammar.Exceptions;
using Toolbox.Distributed;
using LendersOffice.Admin;
using System.Xml.Linq;

namespace LendersOfficeApp.los.RatePrice
{
    public static class Utils
    {
        private static PriceEngineStorageMode GetStorageMode()
        {
            if (ConstSite.PriceEngineStorageMode == "LocalFileOnly")
            {
                return PriceEngineStorageMode.LocalFileOnly;
            }
            else if (ConstSite.PriceEngineStorageMode == "PricingServerReadOnly")
            {
                return PriceEngineStorageMode.NormalWithCache;
            }
            else if (ConstStage.UseAwsS3Storage)
            {
                return PriceEngineStorageMode.Aws;
            }
            else
            {
                return PriceEngineStorageMode.Normal;
            }
        }

        public static void SaveToPriceEngineStorage(PriceEngineStorageType type, SHA256Checksum key, LocalFilePath path)
        {
            using (PerformanceStopwatch.Start("Utils.SaveToPricingEngineStorage - " + type))
            {
                var mode = GetStorageMode();

                var factory = GenericLocator<IPriceEngineStorageFactory>.Factory;
                var priceEngineStorage = factory.Create(mode);
                priceEngineStorage.Save(type, key, path);
            }
        }

        public static LocalFilePath RetrieveFromPriceEngineStorage(PriceEngineStorageType type, SHA256Checksum key)
        {
            var mode = GetStorageMode();

            var factory = GenericLocator<IPriceEngineStorageFactory>.Factory;
            var priceEngineStorage = factory.Create(mode);
            return priceEngineStorage.Retrieve(type, key);

        }

        /// <summary>
        /// Move the rateoptions and price policy stored in local file to persistent storage.
        /// Onle call this method from server running LpeUpdate. DO NOT USE this method for any other purpose.
        /// </summary>
        public static void MoveFromLocalToPersistent(string[] args)
        {
            var mode = GetStorageMode();

            if (mode != PriceEngineStorageMode.Normal)
            {
                throw new DeveloperException(ErrorMessage.BadConfiguration);
            }

            while (true)
            {
                string[] fileList = Directory.GetFiles(ConstSite.TempPriceEngineStorageLocalPath, "*.*", SearchOption.AllDirectories);

                if (fileList.Length == 0)
                {
                    return;
                }

                List<string> fileToDeleteList = new List<string>();

                StringBuilder debugLog = new StringBuilder();
                foreach (string file in fileList)
                {
                    string[] parts = file.Split(Path.DirectorySeparatorChar);

                    SHA256Checksum? key = SHA256Checksum.Create(parts[parts.Length - 1]); // The file name will be the last part.
                    string type = parts[parts.Length - 2]; // The folder name will be the file type.

                    PriceEngineStorageType storageType = (PriceEngineStorageType)Enum.Parse(typeof(PriceEngineStorageType), type);

                    LocalFilePath? localFilePath = LocalFilePath.Create(file);

                    Stopwatch sw = Stopwatch.StartNew();
                    SaveToPriceEngineStorage(storageType, key.Value, localFilePath.Value);
                    fileToDeleteList.Add(file);
                    FileInfo fi = new FileInfo(file);
                    long bytes = fi.Length;
                    debugLog.AppendLine($"{key.Value.Value} - {bytes:N0} bytes, {sw.ElapsedMilliseconds:N0} ms.");

                    try
                    {
                        // 8/21/2017 - Make a copy of the file to C:\LOTemp folder.
                        // This will allow separate process that upload rateoption, price policy and snapshot to AWS to use
                        // local file instead of pull from FileDB.
                        var loTempFile = TempFileUtils.Name2Path(key.Value.Value);
                        if (!FileOperationHelper.Exists(loTempFile))
                        {
                            FileOperationHelper.Copy(file, loTempFile, true);
                        }

                        FileOperationHelper.Delete(file);
                    }
                    catch (IOException exc)
                    {
                        Tools.LogWarning($"Unable to delete file {file}.", exc);
                    }
                }

                debugLog.AppendLine($"Total {fileList.Length}");
                Tools.LogInfo("MoveFromLocalToPersistent", debugLog.ToString());
            }
        }

        public static LendersOffice.ObjLib.PriceGroups.Model.LpePriceGroup RetrievePriceGroup(Guid brokerId, SHA256Checksum priceGroupRevision)
        {
            LendersOffice.ObjLib.PriceGroups.Model.LpePriceGroup priceGroup = null;

            Action<FileInfo> readHandler = delegate (FileInfo fi)
            {
                var filePath = LocalFilePath.Create(fi.FullName);
                priceGroup = SerializationHelper.JsonDeserializeFromFile<LendersOffice.ObjLib.PriceGroups.Model.LpePriceGroup>(filePath.Value);
            };

            try
            {
                FileDBTools.UseFile(E_FileDB.Normal, priceGroupRevision.Value, readHandler);
            }
            catch (FileNotFoundException)
            {
                throw new FileContentKeyNotFoundException($"Error: with LPE_PRICE_GROUP_REVISION broker:{brokerId}, the fileDB '{priceGroupRevision.Value}' doesn't exist.");
            }

            if (priceGroup.BrokerId != brokerId)
            {
                // Double check to make sure we are not loading the wrong price group by accident.
                throw new CBaseException("Bad data", "Bad data");
            }

            return priceGroup;
        }

        /// <summary>
        /// Retrieve the loan program information by id. If broker is using file based pricing then loan program will come from file based snapshot.
        /// </summary>
        /// <param name="broker"></param>
        /// <param name="lLpTemplateId"></param>
        /// <returns></returns>
        public static ILoanProgramTemplate RetrieveLoanProgramTemplate(BrokerDB broker, Guid lLpTemplateId)
        {
            if (broker.ActualPricingBrokerId == Guid.Empty)
            {
                // Retrieve from LPE database directly.
                CLoanProductData loanProductData = new CLoanProductData(lLpTemplateId);
                loanProductData.InitLoad();
                return loanProductData;
            }
            else
            {
                var snapshot = broker.RetrieveLatestManualImport();

                FileBasedLoanProgramTemplate item = null;

                if (!snapshot.TryGetLoanProgramTemplate(lLpTemplateId, out item))
                {
                    throw new NotFoundException("Loan product not found.");
                }

                return item;
            }
        }
    }
    public class Integer
    {
        public Integer(int num)
        {
            this.num = num;
        }

        public Integer() : this(0)
        {
        }

        public int num;
    }

    public class CountTable
    {
        Hashtable m_counters;

        public CountTable()
        {
            m_counters = new Hashtable();
        }

        public CountTable(int capacity)
        {
            m_counters = new Hashtable(capacity);
        }


        public void Clear()
        {
            m_counters.Clear();
        }

        public ICollection Keys
        {
            get { return m_counters.Keys; }
        }
        public IEnumerable<Guid> IdList
        {
            get
            {
                List<Guid> list = new List<Guid>();
                foreach (var o in m_counters.Keys)
                {
                    list.Add((Guid)o);
                }
                return list;
            }
        }
        public int Count
        {
            get { return m_counters.Count; }
        }
        public Integer this[object key]
        {
            get { return (Integer)m_counters[key]; }
            set { m_counters[key] = value; }
        }

        public void AccumulateCount(ArrayList arrayList)
        {
            foreach (object id in arrayList)
            {
                Integer repeat = (Integer)m_counters[id];
                if (repeat == null)
                    m_counters[id] = new Integer(1);
                else
                    repeat.num++;
            }
        }
    }


    internal class ArrayListAllocator
    {
        Stack m_cachedObjects = new Stack();
        int m_maxCached;
        public ArrayListAllocator(int maxCached)
        {
            if (maxCached <= 0)
                maxCached = 10;
            m_maxCached = maxCached;
        }
        public ArrayList Alloc()
        {
            lock (m_cachedObjects)
            {
                if (m_cachedObjects.Count > 0)
                {
                    ArrayList obj = (ArrayList)m_cachedObjects.Pop();
                    obj.Clear();
                    return obj;
                }
            }
            return new ArrayList();
        }

        public void Free(ref ArrayList obj)
        {
            if (obj != null)
            {
                obj.Clear();
                if (obj.Capacity > 300)
                    obj.Capacity = 50;

                lock (m_cachedObjects)
                {
                    if (m_cachedObjects.Count < m_maxCached)
                        m_cachedObjects.Push(obj);
                }
                obj = null;
            }

        }

    }

    internal class PriceGroupMemberArrayListAllocator
    {
        Stack m_cachedObjects = new Stack();
        int m_maxCached;
        public PriceGroupMemberArrayListAllocator(int maxCached)
        {
            if (maxCached <= 0)
                maxCached = 10;
            m_maxCached = maxCached;
        }
        public PriceGroupMemberArrayList Alloc()
        {
            lock (m_cachedObjects)
            {
                if (m_cachedObjects.Count > 0)
                {
                    PriceGroupMemberArrayList obj = (PriceGroupMemberArrayList)m_cachedObjects.Pop();
                    obj.Clear();
                    return obj;
                }
            }
            return new PriceGroupMemberArrayList();
        }

        public void Free(ref PriceGroupMemberArrayList obj)
        {
            if (obj != null)
            {
                obj.Clear();
                if (obj.Capacity > 300)
                    obj.Capacity = 50;

                lock (m_cachedObjects)
                {
                    if (m_cachedObjects.Count < m_maxCached)
                        m_cachedObjects.Push(obj);
                }
                obj = null;
            }

        }

    }

    public class PriceGroupMemberHashtable : Hashtable
    {
        public PriceGroupMemberHashtable() : base()
        {
        }

        public PriceGroupMemberHashtable(int capacity) : base(capacity)
        {
        }

        public override Object this[Object key]
        {
            get
            {
                return base[key];
            }

            set
            {
                if (key != null && (key is PriceGroupMember) == false)
                {
                    string msg = "Expect key's type is PriceGroupMember but its type = " + key.GetType().Name;
                    Tools.LogBug(msg);
                    throw new InvalidCastException(msg);
                }
                base[key] = value;
            }
        }

        public void Verify()
        {
            foreach (PriceGroupMember member in Keys)
                if (member.PriceGroupId != Guid.Empty)
                {
                }
        }

    }

    public class PriceGroupMemberArrayList : ArrayList
    {
        public PriceGroupMemberArrayList() : base()
        {
        }

        public PriceGroupMemberArrayList(int capacity) : base(capacity)
        {
        }

        public PriceGroupMemberArrayList(PriceGroupMemberArrayList other) : base(other)
        {
        }

        public override int Add(object ele)
        {
            if (ele != null && (ele is PriceGroupMember) == false)
            {
                string msg = "PriceGroupMemberArrayList> Expect element type is PriceGroupMember but its type = " + ele.GetType().Name;
                Tools.LogBug(msg);
                throw new InvalidCastException(msg);
            }

            return base.Add(ele);
        }
    }

    public class LpeTools
    {
        static public DateTime GetLpeDataLastModified(DataSrc dataSrc)
        {
            using (DbDataReader r = DataAccess.StoredProcedureHelper.ExecuteReader(dataSrc, "[GetPriceEngineVersionDate]"))
            {
                r.Read(); // a row must be there.

                return Convert.ToDateTime(r["LastUpdatedD"]);
            }
        }

        static public void CleanUpMemory(int delayTimeInMsBetweenTwoGetTotalMemory)
        {
            // dd 5/3/2017 - I believe this CleanUpMemory is not need at all. We should never call GC manually.
            // If OPM_456501_DisableCleanupMemory is active for a month without issue then we can remove this method completely.
            if (ConstStage.OPM_456501_DisableCleanupMemory)
            {
                return;
            }

            StringBuilder debugString = new StringBuilder();
            debugString.Append("[CleanUpMemory] DelayInMs=[" + delayTimeInMsBetweenTwoGetTotalMemory + "] ");
            Stopwatch sw = Stopwatch.StartNew();
            try
            {

                long allocatedMemory = GC.GetTotalMemory(true) / 1000;
                debugString.Append(" UsedMemory0=[" + allocatedMemory.ToString("N0") + " KB]");

                if (delayTimeInMsBetweenTwoGetTotalMemory > 0)
                {
                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(delayTimeInMsBetweenTwoGetTotalMemory);

                    allocatedMemory = GC.GetTotalMemory(true) / 1000;
                    debugString.Append(" UsedMemory1=[" + allocatedMemory.ToString("N0") + " KB]");

                }
            }
            finally
            {
                debugString.Append(" Execute in " + sw.ElapsedMilliseconds + "ms.");
                Tools.LogInfo(debugString.ToString());
            }
        }

        static public void ListLpeEnabledBrokers(Hashtable activeBrokerIds)
        {
            activeBrokerIds.Clear();

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "ListLpeEnabledBrokers", null))
                {
                    while (reader.Read())
                    {
                        activeBrokerIds[(Guid)reader["BrokerId"]] = null;
                    }
                }
            }
        }

        static public String GetCurThreadName(string defaultName)
        {
            if (Thread.CurrentThread.Name != null)
                return Thread.CurrentThread.Name;
            return defaultName;

        }


    }


    #region CalcOnlyServerInfo
    internal class CalcOnlyServerInfo
    {
        /// <summary>
        /// The volatile keyword is needed here so any running threads do not cache the value. Atomic reads
        /// only gurantee that the bool is read in one cpu cycle not necessarily that threads will not cache 
        /// the value. https://msdn.microsoft.com/en-us/library/x13ttww7(v=vs.140).aspx
        /// </summary>
        static volatile private bool  s_busy = false;

#if DEBUG
        static CalcOnlyServerInfo()
        {
            s_busy = true;
        }
#endif

        static internal bool Busy
        {
            get
            {
                return s_busy;
            }
        }

        // http://msdn.microsoft.com/library/default.asp?url=/library/en-us/csspec/html/vclrfCSharpSpec_5_5.asp?frame=true
        // Reads and writes of the following data types are atomic: 
        // bool, char, byte, sbyte, short, ushort, uint, int, float, and reference types. 

        static internal void SetBusyFlag(bool isBusy)
        {
            // we don't need thread-safe at here because Reads and writes of reference type are atomic
            if (DistributeUnderwritingSettings.ServerT == E_ServerT.CalcOnly)
                s_busy = isBusy;
            else
                s_busy = false;
        }

    }

    #endregion

    #region AccessTime
    public interface IGetIdleTimeInfo
    {
        string GetAbsIdleTimeInfo(Object id);
    }

    public class AccessTime : IGetIdleTimeInfo
    {
        protected class TimeCompare : IComparer
        {
            const double m_thresholdInSeconds = 3 * 60;
            DateTime m_currentTime = DateTime.Now;

            internal double GetIdle(ObjectAndTime xTime)
            {
                double xIdle = (m_currentTime - xTime.m_time).TotalSeconds;
                if (xIdle > m_thresholdInSeconds)
                    xIdle *= xTime.m_priority;
                return xIdle;
            }

            int IComparer.Compare(Object x, Object y)
            {
                double xIdle = GetIdle((ObjectAndTime)x);
                double yIdle = GetIdle((ObjectAndTime)y);

                if (xIdle > yIdle)
                    return 1;
                else
                    return xIdle == yIdle ? 0 : -1;
            }

            internal void SetCurrentTime()
            {
                lock (this)
                {
                    m_currentTime = DateTime.Now;
                }
            }


        }


        protected class ObjectAndTime // POD : Plain Old Data 
        {
            internal Object m_obj;
            internal DateTime m_time;
            internal int m_priority = 1;

            internal ObjectAndTime(Object obj, DateTime time)
            {
                m_obj = obj;
                m_time = time;
            }

            internal void Touch()
            {
                m_time = DateTime.Now;
            }

        }


        protected Hashtable m_timeTable = new Hashtable();
        protected TimeCompare m_compare = new TimeCompare();

        public void EntryEnters(Object id, DateTime accessTime)
        {
            ObjectAndTime time = m_timeTable[id] as ObjectAndTime;
            if (time == null)
                m_timeTable[id] = new ObjectAndTime(id, accessTime);
            else
            {
                time.Touch();
            }
        }

        virtual public string GetAbsIdleTimeInfo(Object id) // call SetMilestone() before using this method
        {
            ObjectAndTime time = m_timeTable[id] as ObjectAndTime;
            if (time == null)
                return "no idle information";
            else
                return m_compare.GetIdle(time).ToString("N0");
        }



        public string Report()
        {
            return Report(DateTime.Now);
        }

        public string Report(DateTime now)
        {
            ArrayList arr = new ArrayList();
            arr.AddRange(m_timeTable.Values);

            DateTime now_5 = now.AddMinutes(-5);
            DateTime now_10 = now.AddMinutes(-10);
            DateTime now_15 = now.AddMinutes(-15);
            DateTime now_20 = now.AddMinutes(-20);
            DateTime now_90 = now.AddMinutes(-90);

            int count_0 = 0;
            int count_5 = 0;
            int count_10 = 0;
            int count_15 = 0;
            int count_20 = 0;
            int count_90 = 0;

            foreach (ObjectAndTime obj in arr)
                if (obj.m_time < now_90)
                    count_90++;
                else if (obj.m_time < now_20)
                    count_20++;
                else if (obj.m_time < now_15)
                    count_15++;
                else if (obj.m_time < now_10)
                    count_10++;
                else if (obj.m_time < now_5)
                    count_5++;
                else
                    count_0++;

            return String.Format("[{0} {1} {2} {3} {4} {5}]. Note : #accessedElements during 90, 20, 15, 10, 5, 0 minutes ago.", count_90, count_20, count_15, count_10, count_5, count_0);
        }


        public void GetRecentEntries(ArrayList arr, int maxEntry)
        {
            GetRecentEntries(arr, maxEntry, DateTime.MinValue);
        }

        virtual public void GetRecentEntries(ArrayList arr, int maxEntry, DateTime min)
        {
            if (maxEntry <= 0)
                maxEntry = 1;

            arr.Clear();
            arr.AddRange(m_timeTable.Values);
            m_compare.SetCurrentTime();
            arr.Sort(m_compare);

            while (maxEntry < arr.Count)
            {
                //m_timeTable.Remove( ((ObjectAndTime)arr[maxEntry]).m_obj );
                arr.RemoveAt(maxEntry);
            }

            while (arr.Count > 1 && ((ObjectAndTime)arr[arr.Count - 1]).m_time < min)
                arr.RemoveAt(arr.Count - 1);

            if (CompilerConst.IsDebugModeVar)
            {
                StringBuilder sb = new StringBuilder();
                for (int i = (arr.Count <= 200) ? 0 : arr.Count - 200; i < arr.Count; i++)
                {
                    ObjectAndTime obj = (ObjectAndTime)arr[i];
                    PriceGroupMember member = obj.m_obj as PriceGroupMember;
                    object objInfo = member != null
                        ? "(" + member.PrgId.ToString() + ", " + member.PriceGroupId.ToString() + ")"
                        : obj.m_obj;

                    sb.AppendFormat("{0}. {1} *** {2} *** {3} *** {4}\n", i, objInfo, obj.m_priority, obj.m_time, m_compare.GetIdle(obj));
                }

                if (sb.Length > 0)
                {
                    Tools.LogRegTest("<Compare> ***\n" + sb.ToString());
                }
            }

            for (int i = 0; i < arr.Count; i++)
                arr[i] = ((ObjectAndTime)arr[i]).m_obj;
        }

        public void SetMilestone()
        {
            m_compare.SetCurrentTime();
        }
    }

    public class AccessTimeForPriceGroupMember : AccessTime
    {
        public void SetPriorityFromPriceGroupMembers(Hashtable priorities)
        {
            foreach (Object id in priorities.Keys)
            {

                ObjectAndTime time = m_timeTable[id] as ObjectAndTime;
                if (time != null)
                    time.m_priority = (int)priorities[id];
            }
        }

        public void SetPriorityFromLoanPrgs(Hashtable priorities)
        {
            foreach (PriceGroupMember member in m_timeTable.Keys)
            {
                ObjectAndTime time = m_timeTable[member] as ObjectAndTime;
                object priority = priorities[member.PrgId];
                if (priority != null)
                    time.m_priority = (int)priority;
            }
        }

        public void GetAccessTimeForLoanPrograms(ArrayList prgIds, Hashtable timeResult)
        {
            timeResult.Clear();
            foreach (Guid prgId in prgIds)
                timeResult[prgId] = null;

            foreach (PriceGroupMember member in m_timeTable.Keys)
            {
                if (timeResult.Contains(member.PrgId) == false)
                    continue;

                ObjectAndTime time = m_timeTable[member] as ObjectAndTime;
                object accessTime = timeResult[member.PrgId];
                if (accessTime == null)
                    timeResult[member.PrgId] = time.m_time;
                else if ((DateTime)accessTime < time.m_time)
                {
                    timeResult[member.PrgId] = time.m_time;
                }

            }

            foreach (Guid prgId in prgIds)
                if (timeResult[prgId] == null)
                    timeResult.Remove(prgId);
        }


    }

    #endregion


}


#region DbTools
public class DbTools
{
    private const int LpeDatabaseMaxSqlParameters = 2000;

    public static void RemoveTables(DataSet ds)
    {
        try
        {
            int idx = 0;
            while (ds.Tables.Count > 0 && idx < ds.Tables.Count)
            {
                DataTable t = ds.Tables[idx];
                if (ds.Tables.CanRemove(t))
                {
                    //Tools.LogRegTest("Remove table : " + t.TableName);
                    ds.Tables.Remove(t);
                    continue;
                }
                idx++;
            }
        }
        catch (Exception exc)
        {
            Tools.LogError(exc.Message);
        }
    }

    static public string InClauseForSql(ICollection ids)
    {
        StringBuilder selectSql = new StringBuilder(" in ( ");
        bool isFirst = true;
        foreach (Object id in ids)
        {
            if (isFirst)
                isFirst = false;
            else
                selectSql.Append(", ");
            selectSql.AppendFormat(" '{0}'", id);
        }
        selectSql.Append(" ) ");

        return selectSql.ToString();
    }


    // sql server 2005 supports maximun 2100 parameters
    // 11/15/2013 dd - OBSOLETE This method later.
    static public string CreateParameterized4InClauseSql(string prefixParaName, ICollection ids, ArrayList sqlParas)
    {
        sqlParas.Clear();

        StringBuilder sbName = new StringBuilder(200);
        prefixParaName = "@" + prefixParaName;


        // build string (@S1, @S2, @S3, @S4, @S5 ... , @Sn)
        StringBuilder selectSql = new StringBuilder(" in ( ");
        int count = 0;
        foreach (Object id in ids)
        {
            count++;

            // prefixParaName + count => @Sn
            sbName.Length = 0;
            sbName.Append(prefixParaName).Append(count);

            if (count > 1)
                selectSql.Append(", ");

            string name = sbName.ToString();
            selectSql.Append(name);

            sqlParas.Add(new SqlParameter(name, id));
            if (ids.Count == 1)
                return "= " + name;

        }
        selectSql.Append(" ) ");

        return selectSql.ToString();
    }

    public static string CreateParameterized4InClauseSql(string prefixParaName, IEnumerable<Guid> ids, List<SqlParameter> sqlParas)
    {
        sqlParas.Clear();

        if (ids.Count() > LpeDatabaseMaxSqlParameters)
        {
            // When the number of parameters in the IN clause exceeds the max
            // for SQL Server, switch to sending in a single XML parameter.
            // This situation should be fairly rare.
            var parameterName = $"@{prefixParaName}IdXml";
            var idXml = new XElement("r", ids.Select(id => new XElement("i", id))).ToString(SaveOptions.DisableFormatting);
            sqlParas.Add(new SqlParameter(parameterName, idXml) { SqlDbType = SqlDbType.Xml });

            return $" IN (SELECT T.Item.value('.', 'uniqueidentifier') FROM {parameterName}.nodes('r/i') as T(Item))";
        }

        StringBuilder sbName = new StringBuilder(200);
        prefixParaName = "@" + prefixParaName;


        // build string (@S1, @S2, @S3, @S4, @S5 ... , @Sn)
        StringBuilder selectSql = new StringBuilder(" in ( ");
        int count = 0;
        foreach (Guid id in ids)
        {
            count++;

            // prefixParaName + count => @Sn
            sbName.Length = 0;
            sbName.Append(prefixParaName).Append(count);

            if (count > 1)
                selectSql.Append(", ");

            string name = sbName.ToString();
            selectSql.Append(name);

            sqlParas.Add(new SqlParameter(name, id));
            if (ids.Count() == 1)
                return "= " + name;

        }
        selectSql.Append(" ) ");

        return selectSql.ToString();
    }

}

#endregion

namespace Toolbox
{
    public class SnapshotControler
    {
        static Object s_lock = new Object();

        static DateTime s_expectEngineVersion = DateTime.MinValue;
        static DataSrc s_dataSrc = DataSrc.LpeSrc;
        static readonly bool s_isUsingSnapshot;

        static E_LpeTaskAllowedT s_tasksAllowed = E_LpeTaskAllowedT.None;

        static SnapshotControler()
        {
            s_isUsingSnapshot = ConstSite.IsUsingDBSnapshotForLpe;
            s_tasksAllowed = E_LpeTaskAllowedT.All;

            if (s_isUsingSnapshot == false)
            {
                Tools.LogRegTest("<LPE> Not use DB snapshot for LPE");
                return;
            }

            s_dataSrc = DataSrc.LpeRelease1;

            while (true)
            {
                try
                {
                    SetExpectDataSrc();
                    break;
                }
                catch (Exception exc)
                {
                    Tools.LogError("Error -- CLpeReleaseInfo.GetLatestReleaseInfo() ", exc);
                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(5000);

                }
            }
        }

        static public DataSrc ExpectDataSrc
        {
            get
            {
                lock (s_lock) { return s_dataSrc; }
            }
        }

        static public void SetExpectDataSrc()
        {
            if (IsUsingSnapshot == false)
                return;

            CLpeReleaseInfo latestRelease = CLpeReleaseInfo.GetLatestReleaseInfo();

            lock (s_lock)
            {
                bool hasNewSnapshot = (s_expectEngineVersion != latestRelease.DataLastModifiedD);
                s_dataSrc = latestRelease.DataSrcForSnapshot;
                s_expectEngineVersion = latestRelease.DataLastModifiedD;

                if (hasNewSnapshot)
                {
                    string str = string.Format("<LPE> Detect the new snapshot ver = {0} {1}",
                                                DateTime2String(s_expectEngineVersion), s_dataSrc);
                    Tools.LogRegTest(str);
                }

            }

        }

        /// <summary>
        /// This boolean value is a constant (4/21/09 - Currently set in LendersOffice.Constants.ConstSite.IsUsingDBSnapshotForLpe)
        /// </summary>
        public static bool IsUsingSnapshot
        {
            get
            {
                return s_isUsingSnapshot;
            }
        }

        #region IsUpToDate functions
        public static bool IsUptodate(DateTime memoryVersion)
        {

            DataSrc latestDataSrc;
            return IsUptodate(memoryVersion, out latestDataSrc);
        }

        public static bool IsUptodate(DateTime memoryVersion, out DataSrc latestDataSrc)
        {
            if (SnapshotControler.ExpectDataSrc == DataSrc.LpeSrc)
            {
                latestDataSrc = DataSrc.LpeSrc;
                return Toolbox.RatePrice.IsUptodate(memoryVersion);
            }
            else
            {
                CLpeReleaseInfo latestRelease = CLpeReleaseInfo.GetLatestReleaseInfo();
                latestDataSrc = latestRelease.DataSrcForSnapshot;
                return IsUptodate(memoryVersion, latestRelease.DataLastModifiedD, latestRelease.DataSrcForSnapshot);
            }
        }

        private static bool IsUptodate(DateTime memoryVersion, DateTime dbVersion, DataSrc dataSrc)
        {
            int compareVal = memoryVersion.CompareTo(dbVersion);
            if (compareVal == 0)
                return true;

            if (compareVal < 0)
            {
                Tools.LogRegTest(String.Format("<LPE> LPE caching is out of date. dbVersionD= {0} {1}, memoryVersionD = {2}",
                                        DateTime2String(dbVersion), dataSrc, DateTime2String(memoryVersion)));
            }
            else
            {
                Tools.LogErrorWithCriticalTracking(string.Format(
                    "Lpe version in memory has a later datetime value, not possible, must be a bug. Memory Date={0} vs snapshot Db Date={1} {2}",
                    DateTime2String(memoryVersion), DateTime2String(dbVersion), dataSrc));
            }

            return false;
        }
        #endregion

        /// <summary>
        /// Will convert a DateTime object to the following format: Month/Day Year
        /// </summary>
        /// <param name="time">The time to format.</param>
        /// <returns>A string representing the given DateTime in the following format: Month/Day Year</returns>
        static string DateTime2String(DateTime time)
        {
            return string.Format("{0}/{1} {2}", time.Month, time.Day, time.ToLongTimeString());
        }


        #region SelfSnapshot

        public static E_LpeTaskAllowedT TasksAllowed
        {
            get
            {
                return s_tasksAllowed;
            }
            private set
            {
                s_tasksAllowed = value;
            }
        }

        /// <summary>
        /// Get the current data version from Lpe DB
        /// </summary>
        /// <returns></returns>
        private static DateTime GetCurrentLpeVersionDate(out DateTime dbTime)
        {
            DateTime version;
            using (DbDataReader r = DataAccess.StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "[GetPriceEngineVersionDate]"))
            {
                r.Read(); // a row must be there.
                version = Convert.ToDateTime(r["LastUpdatedD"]);
                dbTime = Convert.ToDateTime(r["CurrentDateTime"]);
            }

            return version;
        }

        private class HoldSingletonToolongSelfSnapshotException : CBaseException
        {
            public HoldSingletonToolongSelfSnapshotException(string errDevMsg) : base("Hold CSingletonDistributed too long", errDevMsg) { }
        }

        private static void ReadSomeDataFromSnapshot(DataSrc dataSrc)
        {
            const string readSomeDataQuery = "SELECT top 2 * FROM LOAN_PRODUCT_FOLDER ; " +
            "SELECT top 2 * FROM LOAN_PROGRAM_TEMPLATE ; " +
            "SELECT top 2 * FROM PRICE_POLICY ; " +
            "SELECT top 2 * FROM PRODUCT_PRICE_ASSOCIATION ; " +
            "SELECT top 2 * FROM RATE_OPTIONS ; ";

            LpeTools.GetLpeDataLastModified(dataSrc);
            DataSet ds = new DataSet();
            DBSelectUtility.FillDataSet(dataSrc, ds, readSomeDataQuery, null, null);
        }

        public static void CreateSnapshot(string ssId, string ssName)
        {
            DateTime reportTime = DateTime.Now;
            while (true)
            {
                try
                {
                    // Wait for the singleton
                    using (CSingletonDistributed singleton = CSingletonDistributed.RequestSingleton(CSingletonDistributed.SingletonObjectT.LpeDataAccessTicket, ssId, 30))
                    {
                        DateTime singletonStartD = DateTime.Now;

                        while (true)
                        {
                            Tools.LogRegTest(string.Format("<LpeSelfRelease> {0}: Attempting to create snapshot {1} now", ssId, ssName));

                            // Create snapshot  within the same serializable transaction if it found a 60 seconds idle in data.
                            object obj = StoredProcedureHelper.ExecuteScalar(DataSrc.LpeSrc, "CreateLpeSsIfDataOldEnough",
                                new SqlParameter[] { new SqlParameter("@SnapshotName", ssName) });
                            if (obj == null || obj == DBNull.Value)
                            {
                                String strangeErrMsg = string.Format("<LpeSelfRelease_Error> {0}: Strange thing happens in SelfSnapshotController code, CreateLpeSsIfDataOldEnough store proc returns null", ssId);
                                Tools.LogError(strangeErrMsg);
                                LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(60000); // sleeping for 60 seconds
                                continue;
                            }

                            int numberOfSecondsNeeded = (int)obj;

                            if (numberOfSecondsNeeded < 0)
                                throw new Exception(string.Format("{0}: Creating snapshot ran into error", ssId));
                            else if (numberOfSecondsNeeded > 0)
                            {
                                //Report(ssId, string.Format("<LpeRelease> {0}: Data is dirty in db in the last 60 seconds, ss wasn't created. Need to sleep for {1} seconds.", ssId, numberOfSecondsNeeded.ToString()), force, ref reportTime);
                                LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(1000 * numberOfSecondsNeeded);

                                if (singletonStartD.AddMinutes(5).CompareTo(DateTime.Now) < 0)
                                {
                                    //singletonStartD = DateTime.MaxValue; // to avoid sending too many emails. Singleton feeding code already shows the total time.
                                    //Tools.LogErrorAndSendMail( string.Format( "{0}: this controller is holding singleton for more than 30 min.", ssId ) );
                                    throw new HoldSingletonToolongSelfSnapshotException(string.Format("{0}: this controller is holding singleton for more than 5 min.", ssId));
                                }
                                continue;
                            }

                            try
                            {
                                LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(10 * 1000); // sleep for new created snapshot is stable                                

                                ReadSomeDataFromSnapshot(DataSrc.LpeServerSnapshot);
                                break;
                            }
                            catch (Exception ex)
                            {
                                Tools.LogError(string.Format("<LpeSelfRelease_Error> {0}: Although CreateLpeSsIfDataOldEnough was executed without error, the verfication process failed with an exception error. Sleeping for 10 seconds and try again.\r\nError:{1}\r\nStackTrace:{2}",
                                    ssId, ex.Message, ex.StackTrace));
                                LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(10 * 1000);
                            }
                        } // while( true )
                    } // using

                    // at here, we successly create new snapshot
                    DateTime dataModD = LpeTools.GetLpeDataLastModified(DataSrc.LpeServerSnapshot);
                    Tools.LogRegTest(string.Format("<LpeSelfRelease> {0}: Snapshot {1} has been created and verified. Its data-last-modified-date is {2} on {3}{4}",
                                        ssId, ssName, dataModD.ToLongTimeString(), dataModD.ToShortDateString(),
                                        ". Sleep 10 seconds")
                                    );
                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(10 * 1000);

                    return; // snapshot has been created

                } // try
                catch (Exception ex)
                {
                    if (ex is CLpeLoadingDeniedException)
                    {
                        Tools.LogRegTest(string.Format("<LpeRelease_Exception> {0} Attempt to create snapshot failed. Will try again in 10 seconds. Error: {1} ", ssId, ex.Message));
                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(10 * 1000);
                    }
                    else if (ex is HoldSingletonToolongSelfSnapshotException)
                    {
                        Tools.LogRegTest(string.Format("<LpeRelease_Exception> {0} Attempt to create snapshot failed. Will try again in 120 seconds. Error: {1} ", ssId, ex.Message));
                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(120 * 1000);
                    }
                    else
                    {
                        Tools.LogErrorWithCriticalTracking(string.Format("<LpeRelease_Exception> {0} Attempt to create snapshot failed. Will try again in 1 min. ", ssId), ex);
                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(60000);
                    }
                }
            } // while( true )
        }

        public static string GetSelfSnapshotName()
        {
            return "ssLpe" + ConstAppDavid.ServerName;
        }

        #endregion
    }

    /// <summary>
    /// This class handles:
    /// 1) Indicating that the data in the database has been changed by updating the pricing engine version date
    /// 2) Determining if the data is up to date by checking if the database's pricing engine version date matches
    ///    that of the pricing engine version date stored in memory
    /// 3) Returning the pricing engine's age in seconds (the number of seconds since data was last changed)
    /// </summary>
    public class RatePrice
    {
        /// <summary>
        /// The pricing engine age refers to the number of seconds since data in the database was last updated.
        /// </summary>
        /// <param name="dbVersion">The datetime that the data in the database was last changed.</param>
        /// <returns>The number of seconds since data was last updated in the database.</returns>
        public static int GetPriceEngineAgeInSeconds(out DateTime dbVersion)
        {
            return GetPriceEngineAgeInSeconds(DataSrc.LpeSrc, out dbVersion);
        }

        /// <summary>
        /// The pricing engine age refers to the number of seconds since data in the database was last updated.
        /// </summary>
        /// <param name="dataSrc">Must contain a stored procedure called "[GetPriceEngineVersionDate]"</param>
        /// <param name="dbVersion">The datetime that the data in the database was last changed.</param>
        /// <returns>The number of seconds since data was last updated in the database.</returns>
        public static int GetPriceEngineAgeInSeconds(DataSrc dataSrc, out DateTime dbVersion)
        {
            using (DbDataReader r = DataAccess.StoredProcedureHelper.ExecuteReader(dataSrc, "[GetPriceEngineVersionDate]"))
            {
                r.Read(); // a row must be there.

                dbVersion = Convert.ToDateTime(r["LastUpdatedD"]);
                DateTime dbMachineTime = Convert.ToDateTime(r["CurrentDateTime"]);
                double ageInSeconds = (dbMachineTime - dbVersion).TotalSeconds;

                if (ageInSeconds < -20)
                {
                    Tools.LogError(string.Format(
                        "PriceEngine age < 0, not possible, must be a bug. PriceEngine Date={0}, System time of sql server={1}",
                        dbVersion.ToShortDateString(), dbMachineTime.ToShortDateString()));

                    return int.MaxValue; // process as "senior"
                }
                else if (ageInSeconds < 0)
                {
                    // Some OS has the adjust time feature for synchronizing with standard time.
                    // it is ok if   -15 < differentTime < 0
                    ageInSeconds = 1;
                }
                else if (ageInSeconds > int.MaxValue)
                    return int.MaxValue;

                return (int)ageInSeconds;
            }
        }

        /// <summary>
        /// Determines if the pricing engine version in memory is out of date.  It determines this by
        /// comparing the date of the in-memory version to that of the current version in the database.
        /// </summary>
        /// <param name="memoryVersion">The DateTime value of the pricing engine version currently loaded in memory.</param>
        /// <returns>"False" if the pricing engine version in memory is older than the current version in the database,
        /// and "True" otherwise.</returns>
        public static bool IsUptodate(DateTime memoryVersion)
        {
            int dbAge;
            return IsUptodate(memoryVersion, out dbAge);
        }

        /// <summary>
        /// Determines if the pricing engine version in memory is out of date.  It determines this by
        /// comparing the date of the in-memory version to that of the current version in the database.
        /// </summary>
        /// <param name="memoryVersion">The DateTime value of the pricing engine version currently loaded in memory.</param>
        /// <param name="dbAge">An "out" parameter that indicates the age of the database pricing engine version (in seconds). </param>
        /// <returns>"False" if the pricing engine version in memory is older than the current version in the database,
        /// and "True" otherwise.</returns>
        public static bool IsUptodate(DateTime memoryVersion, out int dbAge)
        {
            DateTime dbVersion;
            dbAge = GetPriceEngineAgeInSeconds(out dbVersion);

            int compareVal = memoryVersion.CompareTo(dbVersion);
            if (compareVal < 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("<LPE> LPE caching is out of date. dbVersionD={0}, memoryVersionD={1}",
                    dbVersion.ToLongTimeString(), memoryVersion.ToLongTimeString());

#if DEBUG
                const int minAge = 10;
#else
                    const int minAge = 30;
#endif

                if (dbAge >= minAge)
                {
                    if (dbAge < 100)
                        sb.AppendFormat(", dbAge = {0} seconds", dbAge);
                    else
                        sb.AppendFormat(", dbAge = {0}", new TimeSpan(0, 0, dbAge));
                }

                Tools.LogRegTest(sb.ToString());
                return false;
            }

            if (compareVal > 0)
            {
                Tools.LogErrorWithCriticalTracking(string.Format(
                    "The Lpe version in memory has a later datetime value than that of the database.  This isn't possible, and must be a bug. Memory Date={0}, Db Date={1}",
                    memoryVersion.ToShortDateString(), dbVersion.ToShortDateString()));
            }
            return true;
        }

        /// <summary>
        /// This function indicates that data has been changed so a new snapshot will be taken.
        /// Some processes (LpeUpdate and the snapshot smoketest page for example) call this even when 
        /// data has not been changed to simulate a data change so that a snapshot switch will occur.
        /// </summary>
        public static void Invalidate()
        {
            int affectedRows = DataAccess.StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, "UpdatePriceEngineVersionDate", 5);
            if (affectedRows <= 0)
                throw new CBaseException(
                    LendersOffice.Common.ErrorMessages.UpdatePriceEngineVersionDateFailed,
                    "Failed to execute stored procedure UpdatePriceEngineVersionDate");
        }
    }

}
