/// Author : Thien Nguyen
using System;
using System.Collections.Generic;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.DistributeUnderwriting;

namespace LendersOfficeApp.los.RatePrice
{
    public class LpeDataProvider
	{
        static public string        s_USER_ERR_MSG = "The system pricing information is being updated.  Please try again in 2 minutes.  If this continues for more than 10 minutes, please notify system engineers.";
        static IGetLoanProgramSet   s_provider = null;
        const int                   OneMB = 1000 * 1000;
        
        // this class is intended to contain only static methods
        // prevent create this object by using private contructor
        private LpeDataProvider()
        {
        }
        
        static public int MinLpeMemory
        {
            get { return ConstSite.MinLpeMemory * OneMB; }
        }

        static public int MaxLpeMemory
        {
            get { return ConstSite.MaxLpeMemory * OneMB; } 
        }

        static public int MinLpePrograms
        {
            get { return ConstSite.MinLpePrograms; }
        }

        static public int MaxLpePrograms
        {
            get { return ConstSite.MaxLpePrograms; }
        }

        static LpeDataProvider()
        {
            s_provider = LoadLoanProgramOnDemand.Create();

            string paraStr = string.Format("<LPE> MinLpeMemory = {0} KB, MaxLpeMemory = {1} KB, MinLpePrograms = {2} , " +
                                "MaxLpePrograms = {3}  , LpeGetTotalMemory = {4} , GetLoanProgramSetTimeOut = {5} seconds.",
                                (MinLpeMemory/1000).ToString("N0"), 
                                (MaxLpeMemory/1000).ToString("N0"),
                                MinLpePrograms, MaxLpePrograms, ConstSite.LpeGetTotalMemory, ConstSite.GetLoanProgramSetTimeOut );

            Tools.LogInfo( paraStr );

            switch( DistributeUnderwritingSettings.ServerT )                                                                                     
            {                                                                                                                                    
                case E_ServerT.CalcOnly:                                                                          
                case E_ServerT.StandAlone:                                                                        
                    // we assume the expressions are correct => those services (CalcOnly, StandAlone) don't need to validate expressions again.  
                    CCondition.RestrictSyntax = false;                                                                                           
                    break;                                                                                                                       
            }
        }

        public static void StartUp()
        {
            //no op, triggering constructor
        }

        public static AbstractLoanProgramSet GetLoanProgramSet(Guid priceGroupId, IEnumerable<Guid> prgIds  )
        {
            AbstractLoanProgramSet result = GetLoanProgramSetFromIList(priceGroupId, prgIds);


            return result;
        }

        private static AbstractLoanProgramSet GetLoanProgramSetFromIList( Guid priceGroupId, IEnumerable<Guid> prgIds )
        {
            if( DistributeUnderwritingSettings.ServerT == E_ServerT.RequestOnly )
            {
                string errMsg = ConstAppDavid.ServerName + ": GetLoanProgramSetFromIList() should not be called from this server because it's in RequestOnly mode.";
                Tools.LogBug( errMsg );
                throw new CUnderwritingUnavailableException( errMsg );
            }

            try
            {
                return s_provider.GetLoanProgramSet( priceGroupId, prgIds );
            }            
            catch( Exception exc )
            {
                if ((exc as CUnderwritingUnavailableException) == null)
                {
                    Tools.LogError("<LPE_ERROR> GetLoanProgramSet(...) false ", exc);
                }
                throw;
            }
        }
	}
}
