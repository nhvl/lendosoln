﻿namespace LendersOffice.RatePrice
{
    using System;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using System.Web;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.FileSystem;
    using LqbGrammar.DataTypes;
    using Security;

    /// <summary>
    /// Upload pricing data in zip file.
    /// </summary>
    public class UploadLpeDataHandler : IHttpHandler
    {
        /// <summary>
        /// Gets a value indicating whether the handler can be reused to service multiple request.
        /// </summary>
        /// <value>Always true since it can.</value>
        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Processes the given request.
        /// </summary>
        /// <param name="context">An object that provides references to the intrinsic server objects.</param>
        public void ProcessRequest(HttpContext context)
        {
            this.Validate(context);

            string op = context.Request.QueryString["op"];

            // dd 2/27/2018 - This op will be retired once UploadSnapshotAndMarkReady is fully verify.
            if (op == "UploadLpeData")
            {
                this.UploadLpeData(context);
            }
            else if (op == "MarkNewLpeRelease")
            {
                // dd 2/27/2018 - This op will be retired once UploadSnapshotAndMarkReady is fully verify.
                this.MarkNewLpeRelease(context);
            }
            else if (op == "UploadSnapshotAndMarkReady")
            {
                this.UploadSnapshotAndMarkReady(context);
            }
            else if (op == "SyncRateSheetTables")
            {
                this.SyncRateSheetTables(context);
            }
        }

        /// <summary>
        /// Sync INVESTOR_XLS_FILE, LPE_ACCEPTABLE_RS_FILE, LPE_ACCEPTABLE_RS_FILE_VERSION tables.
        /// </summary>
        /// <param name="context">An object that provides references to the intrinsic server objects.</param>
        private void SyncRateSheetTables(HttpContext context)
        {
            string json = string.Empty;
            using (var streamReader = new StreamReader(context.Request.InputStream))
            {
                streamReader.BaseStream.Seek(0, SeekOrigin.Begin);
                json = streamReader.ReadToEnd();
            }

            var syncRequest = SerializationHelper.JsonNetDeserialize<Model.RatesheetDatabaseSyncRequest>(json);

            if (syncRequest.InvestorXlsFileList != null)
            {
                foreach (var record in syncRequest.InvestorXlsFileList)
                {
                    this.Sync(record);
                }
            }

            if (syncRequest.LpeAcceptableRsFileList != null)
            {
                foreach (var record in syncRequest.LpeAcceptableRsFileList)
                {
                    this.Sync(record);
                }
            }

            if (syncRequest.LpeAcceptableRsFileVersionList != null)
            {
                foreach (var record in syncRequest.LpeAcceptableRsFileVersionList)
                {
                    this.Sync(record);
                }
            }

            this.WriteResponse(context, 200, "OK");
        }

        /// <summary>
        /// Set status code and message to response.
        /// </summary>
        /// <param name="context">An object that provides references to the intrinsic server objects.</param>
        /// <param name="statusCode">HTTP status code.</param>
        /// <param name="message">Message return in response.</param>
        private void WriteResponse(HttpContext context, int statusCode, string message)
        {
            context.Response.Clear();
            context.Response.StatusCode = statusCode;
            context.Response.Write(message);
            context.Response.End();
        }

        /// <summary>
        /// Sync INVESTOR_XLS_FILE record.
        /// </summary>
        /// <param name="record">The record to sync.</param>
        private void Sync(Model.InvestorXlsFile record)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@InvestorXlsFileId", record.InvestorXlsFileId),
                new SqlParameter("@InvestorXlsFileName", record.InvestorXlsFileName),
                new SqlParameter("@EffectiveDateTimeWorksheetName", record.EffectiveDateTimeWorksheetName),
                new SqlParameter("@EffectiveDateTimeLandMarkText", record.EffectiveDateTimeLandMarkText),
                new SqlParameter("@EffectiveDateTimeRowOffset", record.EffectiveDateTimeRowOffset),
                new SqlParameter("@EffectiveDateTimeColumnOffset", record.EffectiveDateTimeColumnOffset),
                new SqlParameter("@FileVersionDateTime", record.FileVersionDateTimeInUtc.ToLocalTime())
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, "INVESTOR_XLS_FILE_SyncRecord", 2, parameters);
        }

        /// <summary>
        /// Sync LPE_ACCEPTABLE_RS_FILE record.
        /// </summary>
        /// <param name="record">The record to sync.</param>
        private void Sync(Model.LpeAcceptableRsFile record)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@LpeAcceptableRsFileId", record.LpeAcceptableRsFileId),
                new SqlParameter("@DeploymentType", record.DeploymentType),
                new SqlParameter("@IsBothRsMapAndBotWorking", record.IsBothRsMapAndBotWorking),
                new SqlParameter("@InvestorXlsFileId", record.InvestorXlsFileId)
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, "LPE_ACCEPTABLE_RS_FILE_SyncRecord", 2, parameters);
        }

        /// <summary>
        /// Sync LPE_ACCEPTABLE_RS_FILE_VERSION record.
        /// </summary>
        /// <param name="record">The record to sync.</param>
        private void Sync(Model.LpeAcceptableRsFileVersion record)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@LpeAcceptableRsFileId", record.LpeAcceptableRsFileId),
                new SqlParameter("@VersionNumber", record.VersionNumber),
                new SqlParameter("@FirstEffectiveDateTime", record.FirstEffectiveDateTimeInUtc.ToLocalTime()),
                new SqlParameter("@LatestEffectiveDateTime", record.LatestEffectiveDateTimeInUtc.ToLocalTime()),
                new SqlParameter("@IsExpirationIssuedByInvestor", record.IsExpirationIssuedByInvestor),
                new SqlParameter("@EntryCreatedD", record.EntryCreatedDInUtc.ToLocalTime())
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.RateSheet, "LPE_ACCEPTABLE_RS_FILE_VERSION_SyncRecord", 2, parameters);
        }

        /// <summary>
        /// Insert record into LPE_RELEASE table to indicate new release is available.
        /// </summary>
        /// <param name="context">An object that provides references to the intrinsic server objects.</param>
        private void MarkNewLpeRelease(HttpContext context)
        {
            string json = string.Empty;
            using (var streamReader = new StreamReader(context.Request.InputStream))
            {
                streamReader.BaseStream.Seek(0, SeekOrigin.Begin);
                json = streamReader.ReadToEnd();
            }

            var updateRequest = SerializationHelper.JsonNetDeserialize<LpeReleaseUpdateRequest>(json);

            SqlParameter[] parameters =
            {
                new SqlParameter("@ReleaseVersion", DateTime.Now),
                new SqlParameter("@SnapshotId", "ManualImport"),
                new SqlParameter("@DataLastModifiedD", updateRequest.DataLastModifiedDInUtc.ToLocalTime()),
                new SqlParameter("@FileKey", updateRequest.FileKey)
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "LPE_RELEASE_InsertManualImport", 1, parameters);
        }

        /// <summary>
        /// Accept a snapshot and put into persistent storage.
        /// </summary>
        /// <param name="context">An object that provides references to the intrinsic server objects.</param>
        private void UploadSnapshotAndMarkReady(HttpContext context)
        {
            string fileName = context.Request.Headers["x-lqb-file-name"];
            string dateString = context.Request.Headers["x-lqb-data-last-modified"];

            SHA256Checksum key = SHA256Checksum.Create(fileName).Value;
            DateTime dateLastModified = DateTime.Now;
            if (!DateTime.TryParse(dateString, out dateLastModified))
            {
                dateLastModified = DateTime.Now;
            }

            if (context.Request.Files.Count == 1)
            {
                string tempFile = TempFileUtils.NewTempFilePath();
                context.Request.Files[0].SaveAs(tempFile);
                FileDBTools.WriteFile(E_FileDB.Normal, key.Value, tempFile);
            }

            SqlParameter[] parameters =
                {
                    new SqlParameter("@ReleaseVersion", DateTime.Now),
                    new SqlParameter("@SnapshotId", "ManualImport"),
                    new SqlParameter("@DataLastModifiedD", dateLastModified),
                    new SqlParameter("@FileKey", key.Value)
                    };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "LPE_RELEASE_InsertManualImport", 1, parameters);

            this.WriteResponse(context, 200, "OK");
        }

        /// <summary>
        /// Accept a zip file contains LpeData Rate option, snapshot, and price policy. Unzip and put into persistent storage.
        /// </summary>
        /// <param name="context">An object that provides references to the intrinsic server objects.</param>
        private void UploadLpeData(HttpContext context)
        {
            StringBuilder responseText = new StringBuilder();
            if (context.Request.Files.Count == 1)
            {
                Stopwatch totalStopWath = Stopwatch.StartNew();
                string tempFile = TempFileUtils.NewTempFilePath();
                context.Request.Files[0].SaveAs(tempFile);

                string unzipPath = TempFileUtils.NewTempFilePath();
                Stopwatch sw = Stopwatch.StartNew();
                CompressionHelper.DecompressIOCompressedFile(new System.IO.FileInfo(tempFile), new System.IO.DirectoryInfo(unzipPath), true);
                sw.Stop();
                responseText.Append("Unzip " + sw.ElapsedMilliseconds.ToString("#,#") + "ms. ");
                FileStorageIdentifier location = FileStorageIdentifier.TryParse(ConstStage.AwsS3BucketForLpeData).Value;

                sw.Restart();
                foreach (var file in System.IO.Directory.GetFiles(unzipPath))
                {
                    FileInfo fi = new FileInfo(file);

                    SHA256Checksum key = SHA256Checksum.Create(fi.Name).Value;
                    LocalFilePath path = LocalFilePath.Create(file).Value;
                    this.UploadToS3(key, location, path);
                }

                sw.Stop();
                responseText.Append("UploadToS3=" + sw.ElapsedMilliseconds.ToString("#,#") + "ms. Total=" + totalStopWath.ElapsedMilliseconds.ToString("#,#") + "ms.");

                Tools.LogInfo(responseText.ToString());
            }
        }

        /// <summary>
        /// Upload file directly to S3.
        /// </summary>
        /// <param name="key">Key in S3.</param>
        /// <param name="location">S3 bucket name.</param>
        /// <param name="path">Local file path.</param>
        private void UploadToS3(SHA256Checksum key, FileStorageIdentifier location, LocalFilePath path)
        {
            FileIdentifier fileId = FileIdentifier.TryParse(key.Value).Value;

            Action<LocalFilePath> saveHandler = delegate(LocalFilePath target)
            {
                FileOperationHelper.Copy(path, target, true);
            };

            LendersOffice.Drivers.FileDB.FileDbHelper.SaveNewFile(location, fileId, saveHandler);
        }

        /// <summary>
        /// Read the partner secret key from Authorization header. If partner secret key does not match with
        /// value from our database then it will be reject.
        /// </summary>
        /// <param name="context">An object that provides references to the intrinsic server objects.</param>
        private void Validate(HttpContext context)
        {
            string secretKey = context.Request.Headers["x-lqb-secret-key"];

            if (string.IsNullOrEmpty(secretKey))
            {
                throw new AccessDenied();
            }

            string partnerName = AuthServiceHelper.CheckKey(secretKey);

            if (!partnerName.Equals("LpeUploadData"))
            {
                throw new AccessDenied();
            }
        }

        /// <summary>
        /// Request model for update LPE_RELEASE table.
        /// </summary>
        private class LpeReleaseUpdateRequest
        {
            /// <summary>
            /// Gets or sets the date snapshot modified.
            /// </summary>
            /// <value>The UTC date of snapshot modified.</value>
            public DateTime DataLastModifiedDInUtc { get; set; }

            /// <summary>
            /// Gets or sets the content key.
            /// </summary>
            /// <value>The content key.</value>
            public string FileKey { get; set; }
        }
    }
}