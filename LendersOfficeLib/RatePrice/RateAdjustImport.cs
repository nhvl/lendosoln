﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.RatePrice.Helpers;
using LendersOfficeApp.los.RatePrice;

namespace LendersOffice.LoanPrograms
{
    /// <summary>
    /// Convert string representation of an assortment of loan
    /// product's rate sheets into content that is loaded into
    /// the database.
    /// </summary>
    public class RateAdjustImport
    {
        private static char FILE_SEPARATOR_CHAR = ',';

        private class SimpleRuleInfo
        {
            public Guid RuleId { get; set; }
            public string RuleName { get; set; }

            public string RateAdjust { get; set; }

            public string FeeAdjust { get; set; }

            public string MarginAdjust { get; set; }

            public string MaxYspAdjust { get; set; }

            public string QrateAdjust { get; set; }

            public string TeaserRateAdjust { get; set; }

            public SimpleRuleInfo(Guid ruleId, string ruleName, string feeAdjust, string rateAdjust, string marginAdjust, string qrateAdjust, string trateAdjust, string maxYspAdjust)
            {
                AssertValidDecimal("Rate Adjustment", rateAdjust);
                AssertValidDecimal("Fee Adjustment", feeAdjust);
                AssertValidDecimal("Margin Adjustment", marginAdjust);
                AssertValidDecimal("QRate Adjustment", qrateAdjust);
                AssertValidDecimal("Teaser Rate Adjustment", trateAdjust);
                AssertValidDecimal("Max YSP Adjustment", maxYspAdjust);

                this.RuleId = ruleId;
                this.RuleName = ruleName;
                this.FeeAdjust = feeAdjust;
                this.RateAdjust = rateAdjust;
                this.MarginAdjust = marginAdjust;
                this.QrateAdjust = qrateAdjust;
                this.TeaserRateAdjust = trateAdjust;
                this.MaxYspAdjust = maxYspAdjust;
            }

            private void AssertValidDecimal(string fieldName, string value)
            {
                if (string.IsNullOrEmpty(value))
                {
                    // We allow empty string value.
                    return;
                }

                decimal v;
                if (!decimal.TryParse(value, out v))
                {
                    throw new CBaseException(fieldName + " is not a valid number.", fieldName + " = [" + value + "] is not a valid number.");
                }
            }
        }

        public bool Import(Stream csvStream, out IEnumerable<string> errorList)
        {
            errorList = null;

            Stopwatch sw = Stopwatch.StartNew();

            bool status = false;
            bool isOldFormat = CheckFileFormat(csvStream);

            List<string> list = new List<string>();
            List<CPricePolicy> pricePolicyList = null;

            if (isOldFormat)
            {
                status = ImportSchemaAdj(csvStream, list, out pricePolicyList);
            }
            else
            {
                status = ImportRMapProcAdj(csvStream, list, out pricePolicyList);
            }
            errorList = list;

            int numberOfRecords = pricePolicyList == null ? 0 : pricePolicyList.Count;

            if (status)
            {
                foreach (CPricePolicy pricePolicy in pricePolicyList)
                {
                    try
                    {
                        pricePolicy.Save();
                    }
                    catch (Exception exc)
                    {
                        exc.Data.Add("PricePolicyId", pricePolicy.PricePolicyId);
                        exc.Data.Add("PricePolicyDescription", pricePolicy.PricePolicyDescription);

                        throw;
                    }
                }
            }
            sw.Stop();
            Tools.LogInfo("Rate Adjust ImportAndSave executed in " + sw.ElapsedMilliseconds + "ms. IsSave=" + status + ", # of price policy=" + numberOfRecords + " Using Format: " + (isOldFormat ? "OLD" : "NEW"));

            return status;
        }

        private bool ImportRMapProcAdj(Stream sCsvDoc, List<string> aErrorList, out List<CPricePolicy> policyList)
        {
            policyList = new List<CPricePolicy>();

            IEnumerable<ParsedPolicyInfo> parsedPolicyList = null;
            StreamReader stream = new StreamReader(sCsvDoc);

            try
            {
                PolicyParser pp = new PolicyParser();
                parsedPolicyList = pp.GetPolicies(stream, FILE_SEPARATOR_CHAR, aErrorList, null);

            }
            catch (CBaseException exc)
            {
                aErrorList.Add(exc.ToString());
            }
            finally
            {
                stream.Close();
            }

            if (aErrorList.Count != 0)
            {
                return false;
            }

            foreach (ParsedPolicyInfo pInfo in parsedPolicyList)
            {
                try
                {
                    CPricePolicy currentPricePolicy = CPricePolicy.Retrieve(pInfo.PolicyId);
                    policyList.Add(currentPricePolicy);
                    //update the policy with info loaded from file
                    currentPricePolicy.PricePolicyDescription = (pInfo.PolicyNamePath.Count != 0) ? pInfo.PolicyNamePath[pInfo.PolicyNamePath.Count - 1] : pInfo.PolicyNamePath[0];
                    currentPricePolicy.IsPublished = pInfo.IsPublished;
                    currentPricePolicy.MutualExclusiveExecSortedId = pInfo.MutualExclusiveExecSortedId;

                    //DELETE ALL EXISTING RULES IN THE POLICY
                    currentPricePolicy.DeleteAllRules();

                    foreach (ParsedRuleInfo ruleInfo in pInfo.GetRules())
                    {
                        CRule rule = currentPricePolicy.AddNewRule(ruleInfo.RuleId);

                        rule.Description = ruleInfo.Description;
                        rule.Condition.UserExpression = ruleInfo.Condition;
                        rule.QBC = ruleInfo.QBC;
                        rule.Consequence.Skip = ruleInfo.Skip;
                        rule.Consequence.Disqualify = ruleInfo.Disqualify;
                        rule.Consequence.Stipulation = ruleInfo.Stip;
                        rule.Consequence.RateAdjustTarget.UserExpression = ruleInfo.Rate;
                        rule.Consequence.MarginAdjustTarget.UserExpression = ruleInfo.Margin;
                        rule.Consequence.FeeAdjustTarget.UserExpression = ruleInfo.Fees;
                        rule.Consequence.QltvAdjustTarget.UserExpression = ruleInfo.QLTV;
                        rule.Consequence.QcltvAdjustTarget.UserExpression = ruleInfo.QCLTV;
                        rule.Consequence.MaxFrontEndYspAdjustTarget.UserExpression = ruleInfo.MaxFrontEndYsp;
                        rule.Consequence.MaxYspAdjustTarget.UserExpression = ruleInfo.MaxBackEndYsp;
                        rule.Consequence.QscoreTarget.UserExpression = ruleInfo.QScore;
                        rule.Consequence.QLAmtAdjustTarget.UserExpression = ruleInfo.QLAmt;
                        rule.Consequence.MaxDtiTarget.UserExpression = ruleInfo.MaxDTI;
                        rule.Consequence.QrateAdjustTarget.UserExpression = ruleInfo.QRateAdj;
                        rule.Consequence.TeaserRateAdjustTarget.UserExpression = ruleInfo.TeaserRate;
                    }
                }
                catch (NotFoundException)
                {
                    aErrorList.Add("Error trying to import policy with id " + pInfo.PolicyId + ". The id could not be found within the existing policies.");
                }
            }

            return aErrorList.Count == 0;
        }

        public static bool CheckFileFormat(Stream stream)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            string sLine = "";
            bool bIdentified = false;
            bool retVal = false;
            FileStream fstream = stream as FileStream;
            string fileName = (fstream != null) ? fstream.Name : "";

            Tools.LogVerbose("Checking adjustments file " + fileName + " in order to determine its type.");
            StreamReader file = new StreamReader(stream);

            if (stream.Length <= 0)
                throw new IOException("Adjustment file " + fileName + " has an invalid length.");

            do
            {
                sLine = file.ReadLine().ToLower();
                int pos = sLine.IndexOf(FILE_SEPARATOR_CHAR);
                if (pos == -1)
                    continue;

                string firstField = sLine.Substring(0, pos).TrimWhitespaceAndBOM();

                if ((firstField.Equals("policy id") && sLine.Contains("policy name")) || (firstField.Equals("rule id") && sLine.Contains("rule name")))
                {
                    bIdentified = true;
                    retVal = true; //old format = true;
                    break;
                }
                else if (sLine.Contains("policy") || sLine.Contains("rule"))
                {

                    if (pos != -1)
                    {

                        if (firstField.Equals("policy") || firstField.Equals("rule"))
                        {
                            bIdentified = true;
                            retVal = false; //old format = false
                            break;
                        }
                    }

                }
            }
            while (!bIdentified);

            if (!bIdentified)
            {
                Tools.LogInfo("Could not determine adjustments file type: " + fileName + ". Assuming OLD format.");
            }

            stream.Position = 0; //reset stream to initial position.
            return retVal;
        }

        /// <summary>
        /// Walk the current input stream and load up the specified
        /// rate adjustments.  We cache the broker's entire product set
        /// and on save, write back all the changes.
        /// </summary>
        /// <param name="sCsvDoc">
        /// Stream form of csv document containing rate adjustments.
        /// </param>
        /// <param name="aErrorList">
        /// Container for all errors found during import.
        /// </param>
        /// <returns>
        /// True if import was successful.  Else, don't save.
        /// </returns>

        private bool ImportSchemaAdj(Stream sCsvDoc, List<string> aErrorList, out List<CPricePolicy> policyList)
        {
            bool isImportSuccess = true;
            policyList = new List<CPricePolicy>();

            try
            {
                // Walk the current input stream and load up the specified
                // price policies.  Here is where we cache the broker's
                // entire rule set.  For each price policy we update
                // using the imported details, we will maintain the result
                // in a single data set.  If policy hasn't changed since
                // export, and the import is just as what is already in
                // the database, then we expect the data set handling
                // code to be smart and not ping the db with a wasted
                // update to the table.
                Dictionary<Guid, List<SimpleRuleInfo>> policyDictionary = new Dictionary<Guid, List<SimpleRuleInfo>>();
                Dictionary<Guid, Tuple<int, int>> linesDictionary = new Dictionary<Guid, Tuple<int, int>>();
 
                // Load up the file from the stream that came with the
                // uploaded file.

                ImportStreamReader sr = new ImportStreamReader(sCsvDoc);

                if (sr.IsValid == false)
                {
                    aErrorList.Add("Invalid input file.  File may be in incorrect format or not exist.");
                    return false;
                }

                try
                {
                    // Walk the file and pull out policy entries.
                    sr.ReadLine();

                    int policyLineNo = -1;
                    while (true)
                    {
                        // Now walk the content and pull out policy entries.
                        // For now, the only thing we pull is price policies for
                        // existing policies within this stream.

                        Guid policyId = Guid.Empty;

                        string parsed0 = sr.Parsed[0].Trim().ToLower();
                        if (parsed0 != "policy id")
                        {
                            if (parsed0 != string.Empty)
                            {
                                // Only product entries may be listed.  Stray
                                // content is not allowed.

                                aErrorList.Add("Invalid line within policy section (line# " + sr.LineNo + ").");
                                isImportSuccess = false;
                            }

                            sr.ReadLine();
                            continue;
                        }

                        if (Guid.TryParse(sr.Parsed[1].Trim(), out policyId) == false)
                        {
                            aErrorList.Add("Invalid policy id (line# " + sr.LineNo + ").");
                            isImportSuccess = false;

                            sr.ReadLine();
                            continue;
                        }
                        policyLineNo = sr.LineNo;

                        // Check if we've already seen this price policy.

                        if (policyDictionary.ContainsKey(policyId))
                        {
                            // Redundant policy entry found.  Not supported.
                            aErrorList.Add("Redundant policy entry found.  Not supported (line# " + sr.LineNo + ").");
                            isImportSuccess = false;
                        }

                        // Walk the following section and gather rule adjustments
                        // until the start of the next section.

                        List<SimpleRuleInfo> ruleList = new List<SimpleRuleInfo>();
                        try
                        {
                            while (true) // process rules of a particular policy
                            {
                                String ruleNm = "";
                                Guid ruleId = Guid.Empty;

                                sr.ReadLine();

                                if (sr.Parsed[0].TrimWhitespaceAndBOM().ToLower() == "policy id")
                                {
                                    break;
                                }

                                if (sr.Parsed.Length < 4)
                                {
                                    if (sr.Parsed[0].TrimWhitespaceAndBOM() != "")
                                    {
                                        // When listing rules, only blank lines and
                                        // valid rule adjustment entries are allowed.
                                        aErrorList.Add("Invalid policy rule adjustments (line# " + sr.LineNo + ").");
                                        isImportSuccess = false;
                                    }

                                    continue;
                                }

                                if (sr.Parsed[0].TrimWhitespaceAndBOM().ToLower() == "rule id")
                                {
                                    if (Guid.TryParse(sr.Parsed[1].Trim(), out ruleId) == false)
                                    {
                                        // Rule adjustments must contain id and name to be a valid adjustment list.
                                        aErrorList.Add("Invalid policy rule id (line# " + sr.LineNo + ").");
                                        isImportSuccess = false;
                                    }
                                }
                                else
                                {
                                    // Rule adjustments must contain id and name to
                                    // be a valid adjustment list.
                                    aErrorList.Add("Invalid policy rule id (line# " + sr.LineNo + ").");
                                    isImportSuccess = false;
                                }

                                if (sr.Parsed[2].TrimWhitespaceAndBOM().ToLower() == "rule name")
                                {
                                    ruleNm = sr.Parsed[3].TrimWhitespaceAndBOM();
                                }
                                else
                                {
                                    // Rule adjustments must contain id and name to be a valid adjustment list.
                                    aErrorList.Add("Invalid policy rule name (line# " + sr.LineNo + ").");
                                    isImportSuccess = false;
                                }

                                try
                                {
                                    // Attempt to parse the values.  If it fails,
                                    // then we skip this row.

                                    string ratAdj = null;
                                    string feeAdj = null;
                                    string marAdj = null;
                                    string qrateAdj = null;
                                    string trateAdj = null;
                                    string maxYsp = null;

                                    if (sr.Parsed.Length > 4)
                                    {
                                        ratAdj = sr.Parsed[4].Trim();
                                    }

                                    if (sr.Parsed.Length > 5)
                                    {
                                        feeAdj = sr.Parsed[5].Trim();
                                    }

                                    if (sr.Parsed.Length > 6)
                                    {
                                        marAdj = sr.Parsed[6].Trim();
                                    }
                                    if (sr.Parsed.Length > 7)
                                    {
                                        qrateAdj = sr.Parsed[7].Trim();
                                    }

                                    if (sr.Parsed.Length > 8)
                                    {
                                        trateAdj = sr.Parsed[8].Trim();
                                    }

                                    if (sr.Parsed.Length > 9)
                                    {
                                        maxYsp = sr.Parsed[9].Trim();
                                    }

                                    ruleList.Add(new SimpleRuleInfo(ruleId, ruleNm, feeAdj, ratAdj, marAdj, qrateAdj, trateAdj, maxYsp));
                                }
                                catch (CBaseException exc)
                                {
                                    aErrorList.Add("Invalid rate sheet option (line# " + sr.LineNo + "). ErrMsg: " + exc.DeveloperMessage);
                                    isImportSuccess = false;
                                }
                            } // process rules of a particular policy
                        }
                        finally
                        {
                            if (policyId != Guid.Empty && policyDictionary.ContainsKey(policyId) == false)
                            {
                                policyDictionary.Add(policyId, ruleList);

                                // Store the start line and end line number for policy section.
                                linesDictionary.Add(policyId, new Tuple<int, int>(policyLineNo, sr.LineNo));
                            }
                        }
                    }
                }
                catch (EndOfStreamException)
                {
                }
                finally
                {
                    sr.Close();
                }

                foreach (Guid policyId in policyDictionary.Keys)
                {
                    Tuple<int, int> item = linesDictionary[policyId];
                    int startLine = item.Item1;
                    int endLine = item.Item2;

                    try
                    {
                        CPricePolicy pricePolicy = CPricePolicy.Retrieve(policyId);

                        policyList.Add(pricePolicy);

                        List<SimpleRuleInfo> ruleList = policyDictionary[policyId];

                        foreach (SimpleRuleInfo ruleInfo in ruleList)
                        {
                            CRule rule = pricePolicy.GetRule(ruleInfo.RuleId);
                            rule.Description = ruleInfo.RuleName;
                            rule.Consequence.FeeAdjustTarget.UserExpression = ruleInfo.FeeAdjust;
                            rule.Consequence.MarginAdjustTarget.UserExpression = ruleInfo.MarginAdjust;
                            rule.Consequence.MaxYspAdjustTarget.UserExpression = ruleInfo.MaxYspAdjust;
                            rule.Consequence.QrateAdjustTarget.UserExpression = ruleInfo.QrateAdjust;
                            rule.Consequence.RateAdjustTarget.UserExpression = ruleInfo.RateAdjust;
                            rule.Consequence.TeaserRateAdjustTarget.UserExpression = ruleInfo.TeaserRateAdjust;
                        }
                    }
                    catch (NotFoundException)
                    {
                        // This policy is not valid, so reject and fail to import.
                        aErrorList.Add("Non-existing policy id specified (line# " + startLine + ").");
                        isImportSuccess = false;
                    }
                    catch (CBaseException e)
                    {
                        aErrorList.Add(e.Message + " (line# " + endLine + ").");
                        isImportSuccess = false;
                    }
                }

                return isImportSuccess;
            }
            catch (Exception e)
            {
                throw new CBaseException(ErrorMessages.FailedToImportData, "Unable to import rate adjustments. " + e.ToString());
            }
        }

    }
}