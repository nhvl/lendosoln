/// Author: Thien Nguyen

using System;
using System.Text;
using System.Collections;
using System.Data;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice
{
    public class CLoanTemplateNotFoundException : CBaseException
    {
        public override string EmailSubjectCode
        {
            get { return "CLoanTemplateNotFound"; }
        }
        public CLoanTemplateNotFoundException(DateTime snapshotVersion, ArrayList invalidProgramIds)
            : base(LpeDataProvider.s_USER_ERR_MSG, "")
        {
            Hashtable prgNames = new Hashtable();
            try
            {
                // 11/21/2007 ThienNguyen - Reviewed and safe
                string sql = "SELECT lLpTemplateId, lLpTemplateNm FROM Loan_Program_Template WHERE lLpTemplateId "
                                + DbTools.InClauseForSql(invalidProgramIds);

                CDataSet ds = new CDataSet();
                ds.LoadWithSqlQueryDangerously(DataSrc.LpeSrc, sql);
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    string name = row["lLpTemplateNm"] as string;
                    if (name != null)
                        prgNames[row["lLpTemplateId"]] = name;
                }
            }
            catch (Exception exc)
            {
                Tools.LogError("Can not get template names. ", exc);
            }

            StringBuilder devMsg = new StringBuilder();
            devMsg.AppendFormat("The snapshot {0} {1} doesn't have the loan template(s) : "
                , snapshotVersion.ToLongTimeString(), snapshotVersion.ToShortDateString());

            bool isFirst = true;
            foreach (Guid prgId in invalidProgramIds)
            {
                if (isFirst == false)
                    devMsg.Append("\n");
                else
                    isFirst = false;

                if (prgNames.Contains(prgId))
                    devMsg.AppendFormat("{0} *** name = '{1}'", prgId, prgNames[prgId]);
                else
                    devMsg.AppendFormat("{0}", prgId);
            }

            StringBuilder userMsg = new StringBuilder();
            if (invalidProgramIds.Count == 1)
            {
                if (prgNames.Contains((Guid)invalidProgramIds[0]))
                {
                    userMsg.AppendFormat("The {0} loan program is currently not available. Contact your AE for more details.", prgNames[(Guid)invalidProgramIds[0]]);
                }
                else
                {
                    userMsg.Append("The previously selected loan program is currently not available. Contact your AE for more details.");
                }
            }
            else if (invalidProgramIds.Count > 1)
            {
                isFirst = true;
                foreach (Guid prgId in invalidProgramIds)
                {
                    if (prgNames.Contains(prgId))
                    {
                        if (isFirst == false)
                            userMsg.Append(", ");
                        else
                            isFirst = false;
                        userMsg.Append(prgNames[prgId]);
                    }
                }
                if (isFirst)
                {
                    // None of ids has name.
                    userMsg.Append("The previously selected loan program is currently not available. Contact your AE for more details.");
                }
                else
                {
                    userMsg.Append(" loan programs are currently not available. Contact your AE for more details.");
                }
            }
            DeveloperMessage = devMsg.ToString() + Environment.NewLine + Environment.NewLine + " - Note To Developers: Most of the time, this error is the result of the AE disabling the loan program in Price Groups.";
            UserMessage = userMsg.ToString();
            IsEmailDeveloper = false; // 8/10/2007 dd - We don't need to notify by email this error. MOst of the time it is because the AE disable LP in price group.

        }
    }

}
