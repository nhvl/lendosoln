﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.DistributeUnderwriting;
using Toolbox;
using TimeTool = CommonLib.TimeTool;
using LendersOffice.RatePrice.FileBasedPricing;

namespace LendersOfficeApp.los.RatePrice
{
    // the strategy for having LoanProgramSet object
    internal interface IGetLoanProgramSet
    {
        AbstractLoanProgramSet GetLoanProgramSet(Guid priceGroupId, IEnumerable<Guid> prgIds);
    }
    
    public class LoadLoanProgramOnDemand : IGetLoanProgramSet
    {
        #region Variables
        private static LoadLoanProgramOnDemand              s_instance;
        private static object s_lock = new object();
        private const int                                   SecondsPerMinute = 60;
        readonly int                                m_GetLoanProgramSetTimeOut = ConstSite.GetLoanProgramSetTimeOut;
        private bool                                m_lpeGetTotalMemory = ConstSite.LpeGetTotalMemory;
        private string                                      m_lpeCalcInstanceId = ConstAppDavid.ServerName;
        private PriceGroupMemberHashtable                   m_missingMembers = new PriceGroupMemberHashtable();
        private readonly ManualResetEvent                   m_requestSnapshotEvent = new ManualResetEvent(false);
        private readonly ManualResetEvent                   m_haveSnapshotEvent = new ManualResetEvent(false);
        private readonly ManualResetEvent                   m_missingProgramEvent = new ManualResetEvent(false);
        private bool                                        m_haveSnapshot = false;
        private CLpGlobal                                   m_lpGlobal = null;
        private AccessTimeForPriceGroupMember       m_accessTime = new AccessTimeForPriceGroupMember();
        private ArrayListAllocator                  m_arrayListAllocator = new ArrayListAllocator(40);
        private PriceGroupMemberArrayListAllocator  m_pgMembersAllocator = new PriceGroupMemberArrayListAllocator(40);
        private LpeReleaseMonitor                   m_lpeReleaseMonitor = new LpeReleaseMonitor(ConstSite.EmailIfDetectLpeReleaseError);
        private int                                 m_numTimeOut;
        private volatile bool isExiting = false;
        #endregion

        static internal LoadLoanProgramOnDemand Create()
        {
            lock (s_lock)
            {
                if (s_instance == null)
                {
                    Tools.LogRegTest(string.Format("<LPE> {0} DistributeUnderwritingSettings.ServerT = {1}",
                                            ConstAppDavid.ServerName,
                                            DistributeUnderwritingSettings.ServerT));

                    switch (DistributeUnderwritingSettings.ServerT)
                    {
                        case E_ServerT.AuthorTest:
                        case E_ServerT.CalcOnly:
                        case E_ServerT.StandAlone:
                            s_instance = new LoadLoanProgramOnDemand();

                            // will load 1st snapshot right away.
                            s_instance.m_requestSnapshotEvent.Set();
                            break;

                        case E_ServerT.DevMinimum:
                            s_instance = new LoadLoanProgramOnDemand();
                            // only load 1st snapshot after 1st GetLoanProgramSet(...) is called
                            break;

                        case E_ServerT.RequestOnly:
                        default:
                            return null;

                    } // end switch

                    Thread thread;
                    thread = new Thread(new ThreadStart(s_instance.LoadNewSnapshotThread));
                    thread.Name = $"LoadNewSnapshot.{ConstAppDavid.ServerName}";
                    thread.Start();
                }

                return s_instance;
            }
        }

        public static void TriggerExit()
        {
            if (s_instance != null)
            {
                s_instance.isExiting = true;
            }
        }

        private LoadLoanProgramOnDemand() // private constructor so no one can make instance of this class
        {
        }

        void WaitUntilHavingSnapshot()
        {
            if (m_lpGlobal == null)
            {
                string callerInfo = LpeTools.GetCurThreadName(m_lpeCalcInstanceId);
                Tools.LogRegTest(string.Format("<LPE> {0} : wait to load first snapshot ", callerInfo));
                m_requestSnapshotEvent.Set();
                m_haveSnapshotEvent.WaitOne(90 * 1000, false);

                if (m_lpGlobal == null)
                {
                    Tools.LogError(string.Format("<LPE_ERROR> {0} : false to get snapshot ", callerInfo));
                    throw new CUnderwritingUnavailableException("Lpe doesn't available");
                }
            }
        }

        ReqProgramsMsgAllocator m_requestAllocator = new ReqProgramsMsgAllocator(10);
        readonly Hashtable m_programSetRequestTable = new Hashtable();

        #region The class Statistic is used for GetLoanProgamSet.  It is used as POD (plain old data)
        private class Statistic
        {
            public DateTime ReportTime = DateTime.MinValue;
            public DateTime StartTime = DateTime.MinValue;
            public DateTime SnapshotTimeStamp = DateTime.MinValue;
            private bool m_justHaveNewSnapshot;

            public ulong TotalCall;
            public ulong MissingCounter;
            public ulong HitCounter;
            public ulong TimeoutCounter;
            public ulong SuccessCounter;

            public ulong LoanProgramCounter;
            public ulong MissingLoanProgramCounter;
            public double LoadDuration;
            public double TotalTimeInSeconds;

            public MissingCounters DirectlyLoadedFromDBCounter = new MissingCounters();

            private Statistic m_prevStatistic;

            public Statistic()
            {
                m_prevStatistic = new Statistic(true /* isPrivate */ );
            }


            private Statistic(bool isPrivate)
            {
            }

            public void SetSnapshotTimeStammp()
            {
                m_justHaveNewSnapshot = true;
                SnapshotTimeStamp = DateTime.Now;
                if (m_prevStatistic != null && m_prevStatistic.ReportTime == DateTime.MinValue)
                {
                    m_prevStatistic.ReportTime = StartTime = SnapshotTimeStamp;
                }
            }

            private string CallPercent(ulong num)
            {
                return (num * 100.0 / TotalCall).ToString("##.##");
            }

            private string Report4Last5Minutes()
            {
                try
                {
                    if (m_prevStatistic == null)
                        return "";

                    TimeSpan duration = ReportTime - m_prevStatistic.ReportTime;
                    if (duration.TotalSeconds < 10)
                        return "";

                    string result = "";
                    ulong calls = TotalCall - m_prevStatistic.TotalCall;
                    if (m_prevStatistic.ReportTime != DateTime.MinValue && calls != 0)
                    {
                        double ratio = 60 / duration.TotalSeconds;

                        ulong miss = MissingCounter - m_prevStatistic.MissingCounter;
                        ulong prgCounter = LoanProgramCounter - m_prevStatistic.LoanProgramCounter;
                        ulong missPrgCounter = MissingLoanProgramCounter - m_prevStatistic.MissingLoanProgramCounter;
                        double loadDuration = LoadDuration - m_prevStatistic.LoadDuration;
                        double avgTimePerCall = (TotalTimeInSeconds - m_prevStatistic.TotalTimeInSeconds) / calls;
                        ulong numTimeout = TimeoutCounter - m_prevStatistic.TimeoutCounter;

                        StringBuilder sb = new StringBuilder(300);
                        sb.AppendFormat(m_justHaveNewSnapshot ? "*" : " ");
                        sb.AppendFormat("{0:N3} seconds / call *** ", avgTimePerCall);
                        sb.AppendFormat("{0:N2} calls / min", calls * ratio);
                        sb.AppendFormat(" *** {0:N2} waiting calls / min [{1:##.##}%] ", miss * ratio, miss * 100.0 / calls);
                        sb.AppendFormat(" *** {0:N2} timeout / min ", numTimeout * ratio);

                        sb.AppendFormat(" *** Access : {0:N2} loan prgs / min", prgCounter * ratio);
                        if (prgCounter != 0)
                            sb.AppendFormat(" *** {0:N2} miss prgs / min [{1:##.##}%]", missPrgCounter * ratio, missPrgCounter * 100.0 / prgCounter);

                        sb.AppendFormat(" *** {0:N2} seconds [{1:##.##}%] for loading missing prgs", loadDuration, loadDuration * 100 / duration.TotalSeconds);
                        sb.AppendFormat(" *** Loaded from db information : {0:N2} loan programs/min *** {1:N2}  ratesheets/min *** {2:N2} policies/min",
                                         DirectlyLoadedFromDBCounter.NumLoanPrograms * ratio,
                                         DirectlyLoadedFromDBCounter.NumRateSheets * ratio,
                                         DirectlyLoadedFromDBCounter.NumPolicies * ratio);

                        sb.AppendFormat(" ( the GetLoanProgramSet's statistic for last {0:N0} seconds )", duration.TotalSeconds);
                        sb.AppendFormat(". Current snapshot is used during {0}", TimeTool.GetDurationString(SnapshotTimeStamp));


                        result = sb.ToString();
                        m_justHaveNewSnapshot = false;
                    }

                    m_prevStatistic.CopyFrom(this); ;
                    DirectlyLoadedFromDBCounter.Reset();

                    return result;
                }
                catch
                {
                    return "";
                }
            }

            private void CopyFrom(Statistic src)
            {
                ReportTime = src.ReportTime;
                TotalCall = src.TotalCall;
                MissingCounter = src.MissingCounter;
                HitCounter = src.HitCounter;
                TimeoutCounter = src.TimeoutCounter;
                SuccessCounter = src.SuccessCounter;
                LoanProgramCounter = src.LoanProgramCounter;
                MissingLoanProgramCounter = src.MissingLoanProgramCounter;
                LoadDuration = src.LoadDuration;
                TotalTimeInSeconds = src.TotalTimeInSeconds;
            }

            public string Report(out string last5MinutesReport)
            {
                last5MinutesReport = "";
                if (TotalCall == 0)
                    return "";

                ReportTime = DateTime.Now;
                StringBuilder sb = new StringBuilder(500);
                sb.AppendFormat("Total call = {0:N0}", TotalCall);
                sb.AppendFormat("  timeout = {0:N0}", TimeoutCounter);
                sb.AppendFormat("  miss = {0}% [{1:N0}]", CallPercent(MissingCounter), MissingCounter);
                sb.AppendFormat("  hit = {0}% [{1:N0}]", CallPercent(HitCounter), HitCounter);
                sb.AppendFormat("  success = {0}% [{1:N0}]", CallPercent(SuccessCounter), SuccessCounter);

                sb.AppendFormat(". Loan programs : #AccessCounter = {0:N0}", LoanProgramCounter);

                string missLoanPrgPercent = "0";
                if (LoanProgramCounter > 0)
                {
                    missLoanPrgPercent = (MissingLoanProgramCounter * 100.0 / LoanProgramCounter).ToString("##.###");
                }

                sb.AppendFormat("  #Missing = {0}% [{1:N0}]", missLoanPrgPercent, MissingLoanProgramCounter);

                int howlong = (int)((ReportTime - StartTime).TotalMinutes);

                if (howlong >= 100)
                {
                    sb.AppendFormat("  duration = {0} hours {1} minutes", howlong / 60, howlong % 60);
                }
                else if (howlong > 0)
                {
                    sb.AppendFormat("  duration = {0} minutes", howlong);
                }
                else
                {
                    sb.AppendFormat("  duration = {0:N0} seconds", (ReportTime - StartTime).TotalSeconds);
                }

                last5MinutesReport = Report4Last5Minutes();
                return sb.ToString();
            }
        }
        #endregion

        Statistic m_statistic = new Statistic();
        enum CallStatus { NoWait, Wait, Timeout };

        public AbstractLoanProgramSet GetLoanProgramSet(Guid priceGroupId, IEnumerable<Guid> prgIds)
        {
            DateTime startTime = DateTime.Now;

            LoanProgramSet result = null;
            CallStatus callStatus = CallStatus.NoWait;

            try
            {
                result = GetLoanProgramSet(priceGroupId, prgIds, out callStatus);
                return result;
            }
            finally
            {
                double totalSeconds = (DateTime.Now - startTime).TotalSeconds;

                lock (m_statistic)
                {
                    if (totalSeconds > 0)
                    {
                        m_statistic.TotalTimeInSeconds += totalSeconds;
                    }

                    m_statistic.TotalCall++;
                    if (result != null)
                    {
                        m_statistic.SuccessCounter++;
                        if (callStatus == CallStatus.NoWait)
                        {
                            m_statistic.HitCounter++;
                        }
                        else
                        {
                            m_statistic.MissingCounter++;
                        }
                    }
                    else
                    {
                        m_statistic.MissingCounter++;
                        if (callStatus == CallStatus.Timeout)
                        {
                            m_statistic.TimeoutCounter++;
                        }
                    }
                }
            }
        }

        private LoanProgramSet GetLoanProgramSet(Guid priceGroupId, IEnumerable<Guid> loanPrgIds, out CallStatus callStatus)
        {
            PriceGroupMemberArrayList missingMembers = null;
            PriceGroupMemberArrayList members = null;

            callStatus = CallStatus.NoWait;

            WaitUntilHavingSnapshot();

            missingMembers = m_pgMembersAllocator.Alloc();
            members = m_pgMembersAllocator.Alloc();

            foreach (Guid id in loanPrgIds)
            {
                members.Add(new PriceGroupMember(id, priceGroupId));
            }

            LoanProgramSet loanProgramSet;
            string currentGlobalId;

            ReqProgramsMsg reqMsg;

            lock (this)
            {
                DateTime now = DateTime.Now;
                foreach (PriceGroupMember member in members)
                {
                    m_accessTime.EntryEnters(member, now);
                }

                loanProgramSet = m_lpGlobal.CreateLoanProgramSet(members, missingMembers);
                if (loanProgramSet != null)
                {
                    lock (m_statistic)
                    {
                        m_statistic.LoanProgramCounter += (ulong)members.Count;
                    }
                    m_pgMembersAllocator.Free(ref missingMembers);
                    m_pgMembersAllocator.Free(ref members);
                    return loanProgramSet;
                }

                callStatus = CallStatus.Wait;

                lock (m_statistic)
                {
                    m_statistic.LoanProgramCounter += (ulong)members.Count;
                    m_statistic.MissingLoanProgramCounter += (ulong)missingMembers.Count;
                }

                foreach (PriceGroupMember id in missingMembers)
                {
                    m_missingMembers[id] = null;
                }

                reqMsg = m_requestAllocator.Alloc(members);
                m_programSetRequestTable[Thread.CurrentThread] = reqMsg;

                m_missingProgramEvent.Set();

                currentGlobalId = m_lpGlobal.Id;
            }
            using (PerformanceStopwatch.Start("LoadLoanProgramOnDemain.GetLoanProgramSet - Verify Missing"))
            {
                ArrayList invalidIds = m_arrayListAllocator.Alloc();
                try
                {
                    string callerInfo = LpeTools.GetCurThreadName(m_lpeCalcInstanceId);
                    object firstMissingId = missingMembers.Count > 0 ? missingMembers[0] : (object)Guid.Empty;
                    string missingMsg = string.Format("{0} : 'missing' {1}/{2}  LoanPrograms {3} .... : ",
                        callerInfo, missingMembers.Count, members.Count, firstMissingId);

                    if (ConstSite.VerboseLogEnable4Pricing)
                    {
                        Tools.LogRegTest(missingMsg + "wait to load");
                    }

                    DateTime startTime = DateTime.Now;
                    DateTime dueTime = DateTime.Now.AddSeconds(m_GetLoanProgramSetTimeOut);

                    while (null == loanProgramSet)
                    {
                        if (DateTime.Now > dueTime)
                        {
                            callStatus = CallStatus.Timeout;

                            int numTimeout;
                            lock (this)
                            {
                                numTimeout = ++m_numTimeOut;
                            }

                            object firstLoanPrg = Guid.Empty;
                            if (missingMembers.Count >= 1)
                                firstLoanPrg = missingMembers[0];
                            string msg = string.Format("{0}: <LPE_TIMEOUT> {3}. GetLoanProgramSet(...) can not get " +
                                                        "the loan program Id = {1} during {2}"
                                                        , callerInfo, firstLoanPrg, TimeTool.GetDurationString(startTime),
                                                        numTimeout);
                            Tools.LogRegTest(msg);
                            throw new CUnderwritingUnavailableException(msg);
                        }

                        reqMsg.AnswerEvent.WaitOne(CompilerConst.IsDebugMode ? 10 * 1000 : 2 * 1000, false);

                        lock (this)
                        {
                            m_lpGlobal.GetMissingPriceGroupMembers(members, missingMembers, invalidIds);
                            bool noMissing = (missingMembers.Count == 0);

                            if (invalidIds.Count > 0)
                            {

                                PriceGroupMemberArrayList tmp = new PriceGroupMemberArrayList(members.Count);
                                foreach (PriceGroupMember member in members)
                                {
                                    if (invalidIds.Contains(member.PrgId) == false)
                                    {
                                        tmp.Add(member);
                                    }
                                    else
                                    {
                                        Tools.LogRegTest(string.Format("{0}: <LPE_IGNORE> The current snapshot doesn't contain the loan program with Id = {1}", callerInfo, member.PrgId));
                                    }
                                }

                                members = tmp;
                                reqMsg.Programs = members;
                                reqMsg.AnswerEvent.Reset();
                                m_programSetRequestTable[Thread.CurrentThread] = reqMsg;
                            }

                            if (noMissing)
                            {

                                loanProgramSet = m_lpGlobal.CreateLoanProgramSet(members, missingMembers);
                            }
                            else if (currentGlobalId != m_lpGlobal.Id)
                            {

                                currentGlobalId = m_lpGlobal.Id;

                                m_lpGlobal.GetMissingPriceGroupMembers(members, missingMembers, invalidIds);

                                foreach (PriceGroupMember id in missingMembers)
                                {
                                    m_missingMembers[id] = null;
                                }

                                reqMsg.Programs = members;
                                reqMsg.AnswerEvent.Reset();
                                m_programSetRequestTable[Thread.CurrentThread] = reqMsg;

                                m_missingProgramEvent.Set();
                            }
                        }
                    }

                    if (ConstSite.VerboseLogEnable4Pricing || (DateTime.Now - startTime).TotalSeconds >= 10)
                    {
                        Tools.LogRegTest(missingMsg + " finish to load *** " + CommonLib.TimeTool.GetDurationStringInMs(startTime));
                    }
                }
                finally
                {
                    lock (this)
                    {
                        if (m_programSetRequestTable.Contains(Thread.CurrentThread))
                        {
                            m_programSetRequestTable.Remove(Thread.CurrentThread);
                        }
                    }
                    m_pgMembersAllocator.Free(ref missingMembers);
                    m_pgMembersAllocator.Free(ref members);
                    m_arrayListAllocator.Free(ref invalidIds);
                    m_requestAllocator.Free(ref reqMsg);
                }
            }
            return loanProgramSet;
        } // GetLoanProgramSet( Guid priceGroupId, ICollection loanPrgIds, out CallStatus callStatus )


        // important note : only "LoadProgramOndemandThread()" thread can call LoadMissingPriceGroupMembers()
        Hashtable m_programPriorities = new Hashtable(); // only is used by LoadMissingPriceGroupMembers
        private bool LoadMissingPriceGroupMembers()
        {
            CLpGlobal curLpGlobal = m_lpGlobal;

            // if LpGlobal object is "new", need exchange information between this object va new LpGlobal object 
            if (curLpGlobal.HasOwner == false)
            {
                // exchange information between this object va new LpGlobal object 
                curLpGlobal.GetCurProgramsPriorities(m_programPriorities);
                lock (this)
                {
                    m_accessTime.SetPriorityFromLoanPrgs(m_programPriorities);
                }

                curLpGlobal.HasOwner = true;
                curLpGlobal.SendReportToWebservice();

                lock (m_statistic)
                {
                    m_statistic.SetSnapshotTimeStammp();

                }
            }

            PriceGroupMemberArrayList loadingMembers = null;

            lock (this)
            {
                if (m_missingMembers.Count == 0)
                {
                    m_missingProgramEvent.Reset();
                    return true;
                }

                loadingMembers = m_pgMembersAllocator.Alloc();
                loadingMembers.AddRange(m_missingMembers.Keys);
            }

            DateTime startTime = DateTime.Now;
            MissingCounters loadFromDBCounter = curLpGlobal.LoadPriceGroupMembers(loadingMembers, m_programPriorities);
            
            lock (m_statistic)
            {
                m_statistic.DirectlyLoadedFromDBCounter.Add(loadFromDBCounter);
            }

            PriceGroupMemberArrayList missingMembers = m_pgMembersAllocator.Alloc();
            ArrayList invalidPrgIds = m_arrayListAllocator.Alloc();

            bool retCode;
            lock (this)
            {
                m_accessTime.SetPriorityFromPriceGroupMembers(m_programPriorities);

                loadingMembers.Clear();
                loadingMembers.AddRange(m_missingMembers.Keys);
                curLpGlobal.GetMissingPriceGroupMembers(loadingMembers, missingMembers, invalidPrgIds);

                m_missingMembers.Clear();
                foreach (PriceGroupMember member in missingMembers)
                {
                    m_missingMembers[member] = null;
                }

                foreach (Guid prgId in invalidPrgIds)
                {
                    Tools.LogRegTest("<LPE_ERROR> ignore invalid loanProgramId " + prgId);
                }

                ArrayList remove = m_arrayListAllocator.Alloc();
                foreach (object thread in m_programSetRequestTable.Keys)
                {
                    try
                    {
                        ReqProgramsMsg reqMsg = (ReqProgramsMsg)m_programSetRequestTable[thread];

                        curLpGlobal.GetMissingPriceGroupMembers(reqMsg.Programs, missingMembers, invalidPrgIds);
                        if (missingMembers.Count == 0)
                        {
                            reqMsg.AnswerEvent.Set();
                            remove.Add(thread);
                        }

                    }
                    catch (Exception exc)
                    {
                        if (CompilerConst.IsDebugModeVar)
                        {
                            Tools.LogError(exc);
                        }
                    }
                }

                foreach (object thread in remove)
                {
                    m_programSetRequestTable.Remove(thread);
                }

                m_pgMembersAllocator.Free(ref loadingMembers);
                m_pgMembersAllocator.Free(ref missingMembers);
                m_arrayListAllocator.Free(ref invalidPrgIds);

                if (m_missingMembers.Count == 0)
                {
                    m_missingProgramEvent.Reset();
                    retCode = true;
                }
                else
                {
                    m_missingProgramEvent.Set();
                    retCode = false;
                }
            }
            double totalSeconds = (DateTime.Now - startTime).TotalSeconds;
            if (totalSeconds > 0)
            {
                lock (m_statistic)
                {
                    m_statistic.LoadDuration += totalSeconds;
                }
            }

            curLpGlobal.SendReportToWebservice();
            return retCode;
        }

        private void LoadProgramOndemandThread()
        {
            string callerInfo = LpeTools.GetCurThreadName("LoadProgramOndemandThread." + m_lpeCalcInstanceId);
            Tools.LogRegTest(string.Format("<LPE> {0}  : thread starting", callerInfo));

            DateTime watchMemoryTime = DateTime.MinValue;

            // never end this loop
            while (!this.isExiting)
            {
                bool success = false;
                try
                {
                    if (!m_missingProgramEvent.WaitOne(TimeSpan.FromSeconds(1)))
                    {
                        continue;
                    }

                    ReportSchedule();

                    LoadMissingPriceGroupMembers();

                    if (watchMemoryTime < DateTime.Now)
                    {
                        lock (this)
                        {
                            m_accessTime.SetMilestone();
                            m_lpGlobal.ReduceMemoryIfNeed(m_accessTime);
                        }
                        watchMemoryTime = DateTime.Now.AddSeconds(10);
                    }

                    CLpGlobal curLpGlobal = m_lpGlobal;
                    if (curLpGlobal.HasOwner)
                    {
                        curLpGlobal.SendReportToWebservice();
                    }
                    curLpGlobal = null;

                    success = true;

                } // end try
                catch (CBaseException ex)
                {
                    Tools.LogError(string.Format("<LPE_Exception> {0}:", callerInfo), ex);
                } 
                catch (Exception ex)
                {
                    Tools.LogError(string.Format("<LPE_Exception> {0} : LoadProgramOndemandThread error.", callerInfo), ex);
                }

                if (success == false)
                {
                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(2 * 1000); // for safety
                }

            } // end while( true )
        }

        private CLpGlobal LoadNewSnapshot()
        {
            CLpGlobal lpGlobal = null;
            while (m_haveSnapshot == false)
            {
                CLpeReleaseInfo latestRelease = CLpeReleaseInfo.GetLatestReleaseInfo();

                bool hasIncorrectAge = false;
                int releaseAge = (int)latestRelease.ReleaseAge.TotalSeconds;
                if (releaseAge < 0)
                {
                    hasIncorrectAge = true;

                    if (-releaseAge >= 6)
                    {
                        Tools.LogRegTest(string.Format("<Lpe> Warning : ReleaseAge = {0} seconds < 0 ", releaseAge));
                    }
                    if (-releaseAge < 20)
                    {
                        releaseAge = 0;
                    }
                }

                if (releaseAge >= ConstSite.LpeLoadDelayXSeconds)
                {
                    Tools.LogRegTest(string.Format("<Lpe> LpeLoadDelayXSeconds = {0} seconds, ReleaseSnapshotAge = {1} seconds=> don't need to delay to load lpe data",
                        ConstSite.LpeLoadDelayXSeconds, releaseAge.ToString("N0")));
                    break;
                }

                int sleepTime = (ConstSite.LpeLoadDelayXSeconds - releaseAge);

                Tools.LogRegTest(string.Format("<Lpe> LpeLoadDelayXSeconds = {0}, ReleaseSnapshotAge = {1} => Sleep = {2} seconds before load new snapshot",
                    ConstSite.LpeLoadDelayXSeconds, releaseAge, sleepTime));

                LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(sleepTime * 1000);
                if (hasIncorrectAge)
                {
                    break;
                }
            }

            SnapshotControler.SetExpectDataSrc();
            lpGlobal = CLpGlobal.UpdatedInstance;
            return lpGlobal;
        }

        private void GetRecentPriceGroupMembersForNewSnapshot(ArrayList candidatePgMembers, bool optimizeLoading)
        {
            candidatePgMembers.Clear();

            lock (this)
            {
                const int MinPrograms = 5000; //diana change back to 500
                const int MaxPrograms = 10000; //diana change back to 1000

                int numPreloadPrograms = m_missingMembers.Count + 50;

                if (numPreloadPrograms > MaxPrograms)
                {
                    numPreloadPrograms = MaxPrograms;
                }

                if (optimizeLoading)
                {
                    if (numPreloadPrograms < 200)
                    {
                        numPreloadPrograms = 200;
                    }
                }
                else if (numPreloadPrograms < MinPrograms)
                {
                    numPreloadPrograms = MinPrograms;
                }

                int recentIntervalInSeconds = optimizeLoading ? 3 * SecondsPerMinute / 2 : 5 * SecondsPerMinute;
                if (CompilerConst.IsDebugModeVar)
                {
                    recentIntervalInSeconds = optimizeLoading ? 3 * SecondsPerMinute : 50 * SecondsPerMinute;
                }

                m_accessTime.GetRecentEntries(candidatePgMembers, numPreloadPrograms, DateTime.Now.AddSeconds(-recentIntervalInSeconds));
                if (optimizeLoading && candidatePgMembers.Count < 200)
                {
                    candidatePgMembers.Clear();
                    m_accessTime.GetRecentEntries(candidatePgMembers, 200, DateTime.Now.AddSeconds(-5 * SecondsPerMinute));
                }
            }
        }

        internal void LoadNewSnapshotThread()
        {
            string methodName = LpeTools.GetCurThreadName("LoadNewSnapshotThread");
            Tools.LogWarning(methodName + ":Starting");
            Tools.LogRegTest(string.Format("<LPE> {0} LoadNewSnapshotThread : thread starting for {1}",
                                ConstAppDavid.ServerName, DistributeUnderwritingSettings.ServerT));

            m_requestSnapshotEvent.WaitOne();
            Tools.LogRegTest("<LPE> LoadNewSnapshotThread starts to load lpe.");

            DateTime memoryVersion = DateTime.MinValue;
            String statusMsg = " This server is not loaded at this time.";
            String otherStatusMsg = statusMsg;

            bool justUpdateNewEngine = false;
            int numTry = 0;

            ArrayList loadingMembers = new ArrayList();
            ArrayList prgIds = new ArrayList();
            Hashtable accessTimeForPrgs = new Hashtable();
            Thread loadOndemandThread = null;


            // never end this loop
            while (!this.isExiting)
            {
                try
                {
                    if (justUpdateNewEngine)
                    {
                        justUpdateNewEngine = false;
                        if (memoryVersion != DateTime.MinValue)
                        {
                            statusMsg = otherStatusMsg;
                        }
                    }

                    if (memoryVersion == DateTime.MinValue || SnapshotControler.IsUptodate(memoryVersion) == false)
                    {
                        Tools.LogRegTest(String.Format("<LPE> {0} : Need Update", methodName));

                        bool loadSuccess = false;

                        numTry++;
                        CLpGlobal tmpGlobal = null;
                        try
                        {
                            DateTime beginTime = DateTime.Now;
                            tmpGlobal = LoadNewSnapshot();

                            if (ConstStage.LpeReuseDataOfPrevSnapshot)
                            {
                                tmpGlobal.StealSomeRateSheetsAndPolicies(m_lpGlobal);
                            }

                            //diana1
                            GetRecentPriceGroupMembersForNewSnapshot(loadingMembers, true /* optimizeLoading */ );
                            CalcOnlyServerInfo.SetBusyFlag(true);
                            tmpGlobal.LoadPriceGroupMembers(loadingMembers, null);

                            try
                            {
                                GetRecentPriceGroupMembersForNewSnapshot(loadingMembers, false /* optimizeLoading */ );
                                tmpGlobal.LoadPriceGroupMembers(loadingMembers, null);
                            }
                            catch (Exception otherExc)
                            {
                                Tools.LogWarning(string.Format("<LPE_Exception> {0}'s LoadNewSnapshotThread only " +
                                                                 "successfully reloaded a small recent programs, but we still have a new snapshot.", m_lpeCalcInstanceId),
                                                  otherExc);
                            }
                            CalcOnlyServerInfo.SetBusyFlag(false);

                            tmpGlobal.GetCurProgramIds(prgIds);

                            Tools.LogRegTest(string.Format("<Lpe> {0} : Created  CLpGlobal {1} and loaded {2} programs in {3} ",
                                LpeTools.GetCurThreadName(m_lpeCalcInstanceId),
                                tmpGlobal.SnapshotStr(), prgIds.Count,
                                CommonLib.TimeTool.GetDurationStringInMs(beginTime)));

                            lock (this)
                            {
                                m_accessTime.GetAccessTimeForLoanPrograms(prgIds, accessTimeForPrgs);
                            }

                            tmpGlobal.SetAccessTimeForPrograms(accessTimeForPrgs);

                            // success load new version
                            justUpdateNewEngine = true;
                            numTry = 0;

                            string strNewVersion = tmpGlobal.SnapshotStr();

                            if (m_haveSnapshot == false)
                            {
                                Tools.LogRegTest("First snapshot is loaded. Version = " + strNewVersion);

                                if (loadOndemandThread == null)
                                {
                                    loadOndemandThread = new Thread(new ThreadStart(LoadProgramOndemandThread));
                                    loadOndemandThread.Name = $"LoadOndemand.{m_lpeCalcInstanceId}"; 
                                    loadOndemandThread.Start();
                                }
                            }

                            tmpGlobal.SetOnlyEditableFrom(loadOndemandThread);
                            lock (this)
                            {
                                m_lpGlobal = tmpGlobal;
                            }

                            if (m_haveSnapshot == false)
                            {
                                m_haveSnapshotEvent.Set();
                                m_haveSnapshot = true;
                            }

                            memoryVersion = m_lpGlobal.VersionD;
                            statusMsg = " Just load new version = " + m_lpGlobal.SnapshotStr() + ".";
                            otherStatusMsg = " The snapshot version = " + m_lpGlobal.SnapshotStr() + ".";

                            Tools.LogInfo("SnapshotSwitched", $"<LPE> Pricing on this calculation server switched to data snapshot {strNewVersion}. Estimate #FilebasedSnapshot {FileBasedSnapshot.GetCounter()}");
                            Tools.LogRegTest(string.Format("{0} : Done update and replacement of serving objects, breaking out of the update loop. {1}", methodName, strNewVersion));

                            loadSuccess = true;
                            Utilities.DetectTimeDifferentWithDB();

                        } // end try
                        catch (Exception ex)
                        {
                            Tools.LogWarning(string.Format("<LPE_Exception> {0} is loading up Lpe info and runs into an exception, " +
                                                             "it will clear its memory and start loading again.", methodName), ex);
                        }
                        finally
                        {
                            tmpGlobal = null;
                            CalcOnlyServerInfo.SetBusyFlag(false);

                            const int delayTimeInMsBetweenTwoGetTotalMemory = 2000;
                            LpeTools.CleanUpMemory(delayTimeInMsBetweenTwoGetTotalMemory);
                        }

                        // This would allow other servers, who might be the first-timers, or the LpeUpdate process to take the singleton lock.
                        // So we only keep trying if this one is a first-timer.
                        if (loadSuccess == false && m_haveSnapshot == false)
                        {
                            Tools.LogRegTest(string.Format("<LPE_Exception> {0} : It's a first-timer so it will sleep for 5 seconds and try again.", methodName));
                            LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(5000); // There must be some editing activity going on, wait 5 seconds before trying again.
                            continue;
                        }
                    }

                    m_lpeReleaseMonitor.Run();

                } // end try
                catch (Exception ex)
                {
                    Tools.LogError(string.Format("<LPE_Exception> {0}", methodName), ex);
                } 

                const int delayForJustUpdate = CompilerConst.IsDebugMode ? 60 * 1000 : 2 * 60 * 1000;
                const int delayForNormalCase = 30 * 1000;

                int sleepTime = delayForNormalCase;

                if (justUpdateNewEngine)
                {
                    sleepTime = delayForJustUpdate;
                }
                else
                {
                    sleepTime = delayForNormalCase;
                }
                m_missingProgramEvent.Set(); // for ReportSchedule();

                LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(sleepTime);

            } // end while( true )
        }


        const int IntervalReportInMinutes = 5;
        DateTime m_nextReportTime = DateTime.Now.AddMinutes(IntervalReportInMinutes);

        protected void ReportSchedule()
        {
            lock (this)
            {
                if (DateTime.Now < m_nextReportTime)
                {
                    return;
                }
            }

            RunReportAccessTime();
        }

        protected void RunReportAccessTime()
        {
            string report, productReport, statisticReport, statisticReport4Last5Minutes;
            string reportCaching = m_lpGlobal.ReportCaching(out productReport);

            lock (this)
            {
                report = m_accessTime.Report();
                m_nextReportTime = DateTime.Now.AddMinutes(IntervalReportInMinutes);
            }

            lock (m_statistic)
            {
                statisticReport = m_statistic.Report(out statisticReport4Last5Minutes);
            }

            if (m_lpeGetTotalMemory)
            {
                long allocatedMemory = GC.GetTotalMemory(true) / 1000;
                report = report + String.Format(" Memory = {0} KB", allocatedMemory.ToString("N0"));
            }
        }
    }
}