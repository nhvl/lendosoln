/// Author: David Dao

using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

using LendersOffice.Common;
using LendersOffice.Constants;
using DataAccess;
namespace LendersOfficeApp.los.RatePrice
{
	public class DisabledBySaeTable
	{
        private static object s_lock = new object();

        public static DisabledBySaeItem Retrieve(string investorName, string productCode) 
        {
            if (null == investorName)
                investorName = "";
            if (null == productCode)
                productCode = "";

            lock (s_lock) 
            {
                Hashtable table = RetrieveDisabledBySaeHash();

                return (DisabledBySaeItem) table[investorName.ToLower() + "::" + productCode.ToLower()];
            }
        }

        private static Hashtable RetrieveDisabledBySaeHash() 
        {
            string key = "DisabledBySaeTable";
            Hashtable hashTable = (Hashtable) LOCache.Get(key);

            if (null == hashTable) 
            {
                hashTable = RetrieveFromDB();
                LOCache.Set(key, hashTable, DateTime.Now.AddSeconds(ConstAppDavid.StageConfigExpirationSeconds));
            }		
            
            return hashTable;
            
        }
        private static Hashtable RetrieveFromDB() 
        {
            Hashtable hashTable = new Hashtable();
            
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListDisabledBySae")) 
            {
                while (reader.Read()) 
                {
                    string investorName = (string) reader["InvestorName"];
                    string productCode = (string) reader["ProductCode"];
                    string disableStatus = (string) reader["DisableStatus"];
                    string saeName = (string) reader["SaeName"];
                    hashTable.Add(investorName.ToLower() + "::" + productCode.ToLower(), new DisabledBySaeItem(investorName, productCode, disableStatus, saeName));
                }
            }
            return hashTable;
        }
	}

    public class DisabledBySaeItem 
    {
        private string m_investorName;
        private string m_productCode;
        private string m_disableStatus;
        private string m_saeName;

        public DisabledBySaeItem(string investorName, string productCode, string disableStatus, string saeName) 
        {
            m_investorName = investorName;
            m_productCode = productCode;
            m_disableStatus = disableStatus;
            m_saeName = saeName;
        }
        public string InvestorName 
        {
            get { return m_investorName; }
        }
        public string ProductCode 
        {
            get { return m_productCode; }
        }
        public string DisableStatus 
        {
            get { return m_disableStatus; }
        }
		public string SaeName
		{
			get { return m_saeName; }
		}
    }
}
