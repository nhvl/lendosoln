﻿namespace LendersOffice.RatePrice
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// An inteface for saving and retrieving pricing related immutable file content from persistent storage.
    /// </summary>
    public interface IPriceEngineStorage
    {
        /// <summary>
        /// Retrieve the file from persistent storage. Throw FileNotFoundException if key is not in storage.
        /// </summary>
        /// <param name="type">The type of file to retrieve.</param>
        /// <param name="key">The key of file to retrieve.</param>
        /// <returns>The local file path contains the file.</returns>
        LocalFilePath Retrieve(PriceEngineStorageType type, SHA256Checksum key);

        /// <summary>
        /// Save the file in local path to persistent storage.
        /// </summary>
        /// <param name="type">The type of file to save.</param>
        /// <param name="key">The key of file to save.</param>
        /// <param name="path">The local path of the file save.</param>
        void Save(PriceEngineStorageType type, SHA256Checksum key, LocalFilePath path);
    }
}