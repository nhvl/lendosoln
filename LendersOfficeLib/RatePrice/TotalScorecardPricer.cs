﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.Security;
using LendersOffice.Common;
using LendersOffice.Constants;
using System.Threading;
using LendersOffice.RatePrice;

namespace LendersOfficeApp.los.RatePrice
{
    public class TotalScorecardPricer
    {
        // OPM 41231. 10/22/09 mf.  Pricing a loan against the special template for Total Scorecard
        public static CApplicantPriceXml PriceLoan( Guid LoanId )
        {
            if (LoanId == null)
                throw new CBaseException( ErrorMessages.Generic, "No LoanId provided for Total Scorecard pricing" );

            // Build a pricing request for this loan with the only applicable program being the Total program
           LoanProgramByInvestorSet lpSet = new LoanProgramByInvestorSet();
            lpSet.Add( ConstStage.TotalScorecardLoanProgramId, "N/A", null ); // Special PML Master TOTAL Scorecard Program

            AbstractUserPrincipal principal = (AbstractUserPrincipal)Thread.CurrentPrincipal;
            if (principal == null)
                throw new CBaseException( ErrorMessages.Generic, "Null user principal in Total Scorecard pricer." );
            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
            Guid requestID = DistributeUnderwritingEngine.SubmitToEngine( 
                principal
                , LoanId
                , lpSet
                , E_sLienQualifyModeT.ThisLoan
                , Guid.Empty
                , string.Empty
                , E_RenderResultModeT.Regular
                , E_sPricingModeT.RegularPricing
                , Guid.Empty
                , 0
                , 0
                , 0
                , 0
                , options
                );

            // Pricing one program, so we go ahead and wait for it.
            UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultsSynchronous( requestID );

            if (resultItem != null && resultItem.Results.Count > 0)
                return resultItem.Results[0] as CApplicantPriceXml;
            else
                throw new CBaseException( ErrorMessages.Generic, "No Pricing Result in Total Scorecard pricer." );
        }

    }
}
