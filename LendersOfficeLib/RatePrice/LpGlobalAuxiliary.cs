﻿using System;
using System.Collections;

namespace LendersOfficeApp.los.RatePrice
{
    internal class ProductIdProvider
    {
        // Investor, Product, Lender
        CaseInsensitiveHashtable m_Investors = new CaseInsensitiveHashtable();

        internal Guid GetProductId(string investor, string product, Guid lenderId /*brokerId*/ )
        {
            Hashtable products = m_Investors[investor] as Hashtable;
            if (products == null)
                m_Investors[investor] = products = new Hashtable();

            Hashtable lenders = products[product] as Hashtable;
            if (lenders == null)
                products[product] = lenders = new Hashtable();

            Object productId = lenders[lenderId];
            if (productId == null)
                lenders[lenderId] = productId = Guid.NewGuid();

            return (Guid)productId;
        }
    }

    public struct MissingCounters // POD : Plain Old Data 
    {
        public int NumPriceGroupMembers, NumLoanPrograms, NumPolicies, NumRateSheets;

        public void Add(MissingCounters other)
        {
            NumPriceGroupMembers += other.NumPriceGroupMembers;
            NumLoanPrograms += other.NumLoanPrograms;
            NumPolicies += other.NumPolicies;
            NumRateSheets += other.NumRateSheets;
        }

        public void Reset()
        {
            NumPriceGroupMembers = 0;
            NumLoanPrograms = 0;
            NumPolicies = 0;
            NumRateSheets = 0;
        }
    }

    public class CLpGlobalSizeInfo // pod
    {
        public int PoliciesSize;
        public int ProgramsSize;
        public int PrgAndPolicyRelationshipSize;

        public int FrozenRateSheetsSize;
        public int FrozenPoliciesSize;

        public int TotalSize
        {
            get { return PoliciesSize + PrgAndPolicyRelationshipSize + ProgramsSize; }
        }

        public string PoliciesSizeInKB
        {
            get { return (PoliciesSize / 1000).ToString("N0") + " KB"; }
        }
        public string ProgramsSizeInKB
        {
            get { return (ProgramsSize / 1000).ToString("N0") + " KB"; }
        }

        public string FrozenRateSheetsSizeInKB
        {
            get { return (FrozenRateSheetsSize / 1000).ToString("N0") + " KB"; }
        }
        public string FrozenPoliciesSizeInKB
        {
            get { return (FrozenPoliciesSize / 1000).ToString("N0") + " KB"; }
        }

        public string TotalSizeInKB
        {
            get { return (TotalSize / 1000).ToString("N0") + " KB"; }
        }

    }

    public class PriceGroupMember
    {
        public Guid PrgId { get; private set;}
        public Guid PriceGroupId { get; private set; }

        public PriceGroupMember(Guid prgId, Guid priceGroupId)
        {
            PrgId = prgId;
            PriceGroupId = priceGroupId;
        }

        public override int GetHashCode()
        {
            return PrgId.GetHashCode() ^ PriceGroupId.GetHashCode();
        }

        public override bool Equals(Object o)
        {
            PriceGroupMember other = o as PriceGroupMember;
            if (other == null)
                return false;
            return other.PrgId == PrgId && other.PriceGroupId == PriceGroupId;
        }
    }
}
