///
/// Author: Thien Nguyen
/// 

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice
{
    public class PostfixExpression 
    {
        static public readonly RtOperator  Less           = new RtOperator( OperatorName.Less          );
        static public readonly RtOperator  LessEqual      = new RtOperator( OperatorName.LessEqual     );
        static public readonly RtOperator  Equal          = new RtOperator( OperatorName.Equal         );
        static public readonly RtOperator  NotEqual       = new RtOperator( OperatorName.NotEqual      );
        static public readonly RtOperator  Greater        = new RtOperator( OperatorName.Greater       );
        static public readonly RtOperator  GreaterEqual   = new RtOperator( OperatorName.GreaterEqual  );
        static public readonly RtOperator  And            = new RtOperator( OperatorName.And           );
        static public readonly RtOperator  Or             = new RtOperator( OperatorName.Or            );
        static public readonly RtOperator  Plus           = new RtOperator( OperatorName.Plus          );
        static public readonly RtOperator  Minus          = new RtOperator( OperatorName.Minus         );
        static public readonly RtOperator  Multiply       = new RtOperator( OperatorName.Multiply      );
        static public readonly RtOperator  Divide         = new RtOperator( OperatorName.Divide        );

        private ArrayList m_elements = new ArrayList();

        public void AddOperator( RtOperator rtOperator )
        {
            m_elements.Add( rtOperator );
        }

        public void AddNumber( decimal number )
        {
            m_elements.Add( new RtOperandNumber( number ) );
        }

        internal void AddFieldName( CSymbolFieldName symbol )
        {
            m_elements.Add( RtOperandFieldName.Create( symbol ) );
        }

       

        internal void AddFieldValueSymbol( string fieldValueSymbol )
        {
            m_elements.Add( RtOperandFieldValueSymbol.Create( fieldValueSymbol ) );
        }

        internal void Add( CSymbolFunction funct, int intPara )
        {
            m_elements.Add( new RtFunctionIntParams( funct.InputKeyword, intPara ) );
        }

        internal void Add( CSymbolFunctionStrParams funct, string stringPara )
        {
            m_elements.Add( new RtFunctionStrParams( funct.InputKeyword, stringPara ) );
        }

        internal int Count 
        {
            get { return m_elements.Count; }
        }

        override public string ToString()
        {
            StringBuilder sb = new StringBuilder( 8 * m_elements.Count  );
            foreach( object obj in m_elements )
            {
                if( sb.Length > 0 )
                    sb.Append( ' ');
                sb.Append( obj != null ? obj.ToString() : "???" );
            }
            return sb.ToString();

        }
        public decimal Evaluate( CSymbolTable symbolTable, EvalStack stack, StringBuilder debugInfo, bool isLogDetailInfo )
        {
            if( Count == 0 )
                return 0;

            HashSet<string> debugKeywordSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            Stopwatch sw = Stopwatch.StartNew();

            stack.Init( Count );

            foreach( object obj in m_elements )
            {
                IRtOperand operand = obj as IRtOperand;
                if (operand != null)
                {
                    decimal val = operand.Evaluate(symbolTable);

                    if (isLogDetailInfo)
                    {
                        if (operand.GetType().Name == "RtOperandFieldValueSymbol" ||
                            operand.GetType().Name == "RtOperandNumber")
                        {
                            // 1/15/2014 dd - NO-OP. No need to log constant or symbol value.
                        }
                        else
                        {
                            debugKeywordSet.Add(operand.ToString() + "=[" + val + "]");
                        }
                    }
                    stack.Push(val);
                }
                else
                {
                    RtOperator op = obj as RtOperator;
                    op.Evaluate(symbolTable, stack);
                }
            }
            if( stack.Count != 1 )
                throw new System.Exception("Postfix Evaluate error, expect stack.Size = 1 but " + stack.Count);

            decimal ret = stack.Top();
            sw.Stop();
            if (isLogDetailInfo)
            {
                foreach (var str in debugKeywordSet.OrderBy(o => o))
                {
                    debugInfo.AppendLine("        " + str);
                }
            }

            return ret;

        }

        public void TrimToSize()
        {
            m_elements.TrimToSize();
        }

        public static decimal Evaluate(PostfixExpression expr, CSymbolTable symbolTable, StringBuilder debugInfo, bool isLogDetailInfo)
        {
            EvalStack stack = new EvalStack( expr.Count +1 );
            return expr.Evaluate( symbolTable, stack, debugInfo, isLogDetailInfo );
        }

    }

    #region Postfix Converter

    public class PostfixConvertUtility
    {
        static public ArrayList GetObjects( Hashtable hashtable, Type requiredType )
        {
            ArrayList arr = new ArrayList();
            foreach( object obj in hashtable.Values )
                if( requiredType.IsAssignableFrom ( obj.GetType() )  )
                    arr.Add( obj );
            return arr;
        }

        static public ArrayList GetCSymbolEntries( Type requiredType )
        {
            return GetObjects( CSymbolTable.CreateSymbolTableWithoutNewKeywords().Entries,  requiredType );
        }

        static public CaseInsensitiveHashtable GetInputKeywordsOf(Type requiredType)
        {
            CaseInsensitiveHashtable table  = new CaseInsensitiveHashtable();
            foreach( CSymbolEntry symbol in PostfixConvertUtility.GetCSymbolEntries( requiredType) )
                table[ symbol.InputKeyword ] = symbol.InputKeyword;
            return table;
        }

    }
    public interface IRtOperand
    {
        decimal Evaluate( CSymbolTable symbolTable );
    };

    public class RtOperandNumber      : IRtOperand
    {
        decimal m_number;

        public RtOperandNumber(decimal number )
        {
            m_number = number;
        }

        public decimal Evaluate( CSymbolTable symbolTable )
        {
            return m_number;
        }

        public override string ToString()
        {
            return m_number.ToString();
        }

    }

    public class RtOperandFieldValueSymbol : IRtOperand
    {
        string  m_symbol;
        decimal m_value;

        private RtOperandFieldValueSymbol(string symbol, decimal value )
        {
            m_symbol = symbol;
            m_value  = value;
        }

        public decimal Evaluate( CSymbolTable symbolTable )
        {
            return m_value;
        }

        public override string ToString()
        {
            return m_symbol;
        }


        static readonly Hashtable s_fldValueSymbols;

        static RtOperandFieldValueSymbol()
        {
            s_fldValueSymbols = new CaseInsensitiveHashtable();

            CSymbolTable symbolTable = new CSymbolTable(null);
            foreach( string key in symbolTable.Entries.Keys )
            {
                CSymbolFieldValue symbol = symbolTable.Entries[ key] as CSymbolFieldValue;
                if( symbol == null )
                    continue;

                s_fldValueSymbols[ symbol.InputKeyword ] = new RtOperandFieldValueSymbol( symbol.InputKeyword, symbol.ConstValue );
            }

        }

        static public RtOperandFieldValueSymbol Create( string fieldValueSymbol )
        {
            RtOperandFieldValueSymbol obj = (RtOperandFieldValueSymbol)s_fldValueSymbols[ fieldValueSymbol ];
            if( obj == null )
                throw new CBaseException(ErrorMessages.Generic, "RtOperandFieldValueSymbol error : invalid fieldname '" + fieldValueSymbol + "'");
            return obj;
        }

    }

    public class RtOperandFieldName : IRtOperand
    {
        string m_fieldName;
        string m_keyword;

        protected RtOperandFieldName(string fieldName )
        {
            m_fieldName  = fieldName;
            m_keyword    = fieldName.ToLower();
        }

        public decimal Evaluate( CSymbolTable symbolTable )
        {
            try
            {
                CSymbolFieldName entry = symbolTable.Entries[m_keyword] as CSymbolFieldName;
                if( entry == null )
                {
                    string errMsg = m_keyword + " is not a valid keyword for CSymbolFieldName.";
                    if( symbolTable.Entries[m_keyword] == null )
                        errMsg += " Maybe it was removed.";
                    else
                        errMsg += " It uses for " + symbolTable.Entries[m_keyword].GetType().Name;
                    throw new CEvaluationException( CEvaluationException.ErrorTypeEnum.InvalidToken, errMsg);
                }

                return entry.Get();
            }
            catch ( CEvaluationException  )
            {
                throw;
            }
            catch( System.Exception exc )
            {
                throw CRuleEvalTool.ConvertToCEvaluationException(m_fieldName + " doesn't have a value for the given loan file.",  exc);
            }
        }

        public override string ToString()
        {
            return m_fieldName.ToString();
        }

        static readonly Hashtable s_rtOperandFieldNames;
        static readonly Hashtable s_rtOperandFieldNamesExt;

        static RtOperandFieldName()
        {
            s_rtOperandFieldNames    = new CaseInsensitiveHashtable();
            s_rtOperandFieldNamesExt = new CaseInsensitiveHashtable();


            CSymbolTable symbolTable = CSymbolTable.CreateSymbolTableWithoutNewKeywords();
            foreach( string key in symbolTable.Entries.Keys )
            {
                CSymbolFieldName symbol = symbolTable.Entries[ key] as CSymbolFieldName;
                if( symbol == null )
                    continue;

                s_rtOperandFieldNames[ symbol.InputKeyword ] = new RtOperandFieldName( symbol.InputKeyword );
            }
        }

        static public RtOperandFieldName Create( CSymbolFieldName symbol )
        {
            string fieldName = symbol.InputKeyword;
            RtOperandFieldName obj = (RtOperandFieldName)s_rtOperandFieldNames[ fieldName ];
            if( obj == null )
            {
                // get from cache s_rtOperandFieldNamesExt
                lock( s_rtOperandFieldNamesExt )
                {
                    obj = (RtOperandFieldName)s_rtOperandFieldNamesExt[ fieldName ];
                    if( obj != null )
                        return obj;

                    s_rtOperandFieldNames[ symbol.InputKeyword ] = obj = new RtOperandFieldName( fieldName );
                }

            }
            return obj;
        }
    }

    
    public class RtFunctionStrParams : IRtOperand
    {
        string m_functionName;
        string m_param;

        public RtFunctionStrParams(string functionName, string param)
        {
            m_functionName = s_poolString.GetString( functionName );
            if( m_functionName == null )
                throw new CBaseException(ErrorMessages.Generic, "RtFunctionStrParams error : function name can not be null");
            
            
            m_param        = param;
        }

        public decimal Evaluate( CSymbolTable symbolTable )
        {
            try
            {
                CSymbolFunctionStrParams entry = symbolTable.Entries[m_functionName] as CSymbolFunctionStrParams;
                if( entry == null )
                {
                    string errMsg = m_functionName + " is not a valid keyword for CSymbolFunctionStrParams.";
                    if( symbolTable.Entries[m_functionName] == null )
                        errMsg += " Maybe it was removed.";
                    else
                        errMsg += " It uses for " + symbolTable.Entries[m_functionName].GetType().Name;
                    throw new CEvaluationException( CEvaluationException.ErrorTypeEnum.InvalidToken, errMsg);
                }

                return entry.Call(m_param);

            }
            catch ( CEvaluationException  )
            {
                throw;
            }
            catch ( System.Exception exc )
            {
                throw CRuleEvalTool.ConvertToCEvaluationException("Can not evaluate the function '" + m_functionName +"' .",  exc);
            }			
        }

        public override string ToString()
        {
            string functionName = m_functionName;
            int idx = functionName.IndexOf( ":" );
            if( idx > 0 )
                functionName = functionName.Substring(0, idx);

            return functionName + "(" + m_param.ToString() + ")";
        }


        static readonly PoolStringTable4SymbolName s_poolString;
        static RtFunctionStrParams()
        {
            const int cacheSize = 50;
            CaseInsensitiveHashtable officialFunctionNames = PostfixConvertUtility.GetInputKeywordsOf( typeof( CSymbolFunctionStrParams ) );
            s_poolString = new PoolStringTable4SymbolName( officialFunctionNames, cacheSize );
        }

    }

    public class PoolStringTable4SymbolName // support thread-safe
    {
        CaseInsensitiveHashtable m_officialStrings;
        CaseInsensitiveHashtable m_cache;
        int                      m_cacheSize;

        public PoolStringTable4SymbolName( CaseInsensitiveHashtable officialStrings, int cacheSize )
        {
            m_officialStrings = officialStrings;
            m_cache           = new CaseInsensitiveHashtable();
            m_cacheSize       = cacheSize; 
        }

        public string GetString(string str)
        {
            string result = (string)m_officialStrings[ str ];
            if( result == null )
            {
                lock( m_cache )
                {
                    result = (string)m_cache[ str ];
                    if( result == null )
                    {
                        if( m_cache.Count >= m_cacheSize )
                            m_cache.Clear();

                        m_cache[ str ] = result = str; 
                    }

                }
            }
            return result;
        }

    }

    public class RtFunctionIntParams : IRtOperand
    {
        string m_functionName;
        int    m_param;

        public RtFunctionIntParams(string functionName, int param)
        {
            m_functionName = s_poolString.GetString( functionName );
            if( m_functionName == null )
                throw new CBaseException(ErrorMessages.Generic, "RtFunctionIntParams error : function name can not be null");
            m_param        = param;
        }
        public decimal Evaluate( CSymbolTable symbolTable )
        {
            try
            {
                CSymbolFunction entry = symbolTable.Entries[m_functionName] as CSymbolFunction;
                if( entry == null )
                {
                    string errMsg = m_functionName + " is not a valid keyword for CSymbolFunction.";
                    if( symbolTable.Entries[m_functionName] == null )
                        errMsg += " Maybe it was removed.";
                    else
                        errMsg += " It uses for " + symbolTable.Entries[m_functionName].GetType().Name;
                    throw new CEvaluationException( CEvaluationException.ErrorTypeEnum.InvalidToken, errMsg);
                }


                return entry.Call(m_param);

            }
            catch ( CEvaluationException  )
            {
                throw;
            }
            catch ( System.Exception exc )
            {
                throw CRuleEvalTool.ConvertToCEvaluationException("Can not evaluate the function '" + m_functionName +"' .",  exc);
            }			
        }

        public override string ToString()
        {
            string functionName = m_functionName;
            int idx = functionName.IndexOf( ":" );
            if( idx > 0 )
                functionName = functionName.Substring(0, idx);

            return functionName + "(" + m_param.ToString() + ")";
        }

        static readonly PoolStringTable4SymbolName s_poolString;
        static RtFunctionIntParams()
        {
            const int cacheSize = 50;
            CaseInsensitiveHashtable officialFunctionNames = PostfixConvertUtility.GetInputKeywordsOf( typeof( CSymbolFunction ) );
            s_poolString = new PoolStringTable4SymbolName( officialFunctionNames, cacheSize );
        }
    }


    public class EvalStack
    {
        private decimal []m_data;
        private int       m_pos;

        public EvalStack(int size )
        {
            Init( size );
        }

        public void Init( int requireSize )
        {
            if( m_data == null || m_data.Length < requireSize )
                m_data = new decimal[ requireSize + 20 ];
            m_pos  = -1;

        }

        void Verify()
        {
            if( m_pos < -1 )
                throw new CBaseException(ErrorMessages.Generic, "EvalStack Underflow"); 
        }

        public void Push(decimal value)
        {
            m_data[++m_pos] = value;
        }

        public decimal Top()
        {
            return m_data[m_pos];
        }

        public void Pop()
        {
            m_pos--;
        }

        public int Count
        {
            get { return m_pos+1; }
        }

        public void Add()
        {
            m_data[m_pos-1] += m_data[m_pos];
            m_pos--;
            Verify();
        }

        public void Sub()
        {
            m_data[m_pos-1] -= m_data[m_pos];
            m_pos--;
            Verify();
        }

        public void Mul()
        {
            m_data[m_pos-1] *= m_data[m_pos];
            m_pos--;
            Verify();
        }

        public void Div()
        {
            m_data[m_pos-1] /= m_data[m_pos];
            m_pos--;
            Verify();
        }

        public void And()
        {
            // verify data = 0 or 1

            m_data[m_pos-1] = (m_data[m_pos-1] != 0 && m_data[m_pos] != 0) ? 1 : 0;
            m_pos--;
            Verify();
        }

        public void Or()
        {
            // verify data = 0 or 1

            m_data[m_pos-1] = (m_data[m_pos-1] != 0 || m_data[m_pos] != 0) ? 1 : 0;
            m_pos--;
            Verify();
        }

        public void Less()
        {
            m_data[m_pos-1] = (m_data[m_pos-1] < m_data[m_pos] ) ? 1 : 0;
            m_pos--;
            Verify();
        }

        public void LessEqual()
        {
            m_data[m_pos-1] = (m_data[m_pos-1] <= m_data[m_pos] ) ? 1 : 0;
            m_pos--;
            Verify();
        }

        public void Equal()
        {
            m_data[m_pos-1] = (m_data[m_pos-1] == m_data[m_pos] ) ? 1 : 0;
            m_pos--;
            Verify();
        }

        public void NotEqual()
        {
            m_data[m_pos-1] = (m_data[m_pos-1] != m_data[m_pos] ) ? 1 : 0;
            m_pos--;
            Verify();
        }

        public void Greater()
        {
            m_data[m_pos-1] = (m_data[m_pos-1] > m_data[m_pos] ) ? 1 : 0;
            m_pos--;
            Verify();
        }

        public void GreaterEqual()
        {
            m_data[m_pos-1] = (m_data[m_pos-1] >= m_data[m_pos] ) ? 1 : 0;
            m_pos--;
            Verify();
        }
    }

    public class RtOperator
    {
        static RtOperator()
        {

        }

        public RtOperator(string operatorName )
        {
            m_operator = operatorName;
        }

        public string Name
        {
            get { return m_operator; }
        }

        public override string ToString()
        {
            return OperatorName.GetOperatorSymbol( m_operator );

        }


        public void Evaluate( CSymbolTable symbolTable, EvalStack stack)
        {
            switch( m_operator )
            {
                case "Less"         : stack.Less(); return;
                case "LessEqual"    : stack.LessEqual(); return;
                case "Equal"        : stack.Equal(); return;
                case "NotEqual"     : stack.NotEqual(); return;
                case "Greater"      : stack.Greater(); return;
                case "GreaterEqual" : stack.GreaterEqual(); return;

                case "And"          : stack.And(); return;
                case "Or"           : stack.Or(); return;
                            
                case "Plus"         : stack.Add(); return;
                case "Minus"        : stack.Sub(); return;
                case "Multiply"     : stack.Mul(); return;
                case "Divide"       : stack.Div(); return;
            }

            throw new CBaseException(ErrorMessages.Generic, "RtOperator : unknown operator +" + m_operator);

        }

        private string  m_operator;
    }


    #endregion

}
