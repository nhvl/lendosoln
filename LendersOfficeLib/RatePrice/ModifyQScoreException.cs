﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using DataAccess;

namespace LendersOfficeApp.los.RatePrice
{
    public class ModifyQScoreException : CBaseException
    {
        string m_prevRuleInfo, m_ruleInfo;
        public ModifyQScoreException(string prevRuleInfo, string ruleInfo)
            : base(ErrorMessages.Generic, ErrorMessages.Generic)
        {
            m_prevRuleInfo = prevRuleInfo;
            m_ruleInfo = ruleInfo;
        }

        public string RuleInfo
        {
            get { return m_ruleInfo; }
        }

        public string PrevRuleInfo
        {
            get { return m_prevRuleInfo; }
        }

    }
}
