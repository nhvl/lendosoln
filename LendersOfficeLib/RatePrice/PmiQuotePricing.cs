﻿namespace LendersOffice.RatePrice
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Integration.MortgageInsurance;
    using LendersOffice.ObjLib.Extensions;
    using LendersOffice.Security;
    using LendersOfficeApp.los.RatePrice;
    using Mismo231.MI;

    /// <summary>
    /// Helper class for engine to call MI Vendors for quotes.
    /// </summary>
    internal class PmiQuotePricing
    {
        /// <summary>
        ///  Contains a mapping of the MI company to vendor identifier.
        /// </summary>
        private static readonly LqbSingleThreadInitializeLazy<Dictionary<E_PmiCompanyT, Guid>> MiCompanyMap =
            new LqbSingleThreadInitializeLazy<Dictionary<E_PmiCompanyT, Guid>>(LoadMiCompanyMap);

        /// <summary>
        /// The loan file.
        /// </summary>
        private CPageData dataLoan;

        /// <summary>
        /// The pricing cache to set data to.
        /// </summary>
        private CacheInvariantPolicyResults pricingCache;

        /// <summary>
        /// The master policy number of this lender.
        /// </summary>
        private Dictionary<E_PmiCompanyT, string> masterPolicyNumbers;

        /// <summary>
        /// The time at which we should consider this run unsalvageable.
        /// Our UIs have a polling timeout.  We do not want to tie up a pricing 
        /// thread and continue to hit our MI partners if we know the result will 
        /// not ever be rendered.
        /// </summary>
        private DateTime giveUpTime;

        /// <summary>
        /// Initializes a new instance of the <see cref="PmiQuotePricing"/> class.
        /// </summary>
        /// <param name="dataLoan">The dataloan.</param>
        /// <param name="pricingCache">The pricing run cache.</param>
        private PmiQuotePricing(CPageData dataLoan, CacheInvariantPolicyResults pricingCache)
        {
            this.dataLoan = dataLoan;
            this.pricingCache = pricingCache;
            this.giveUpTime = pricingCache.PricingRunStartTime + TimeSpan.FromMinutes(3);
            this.masterPolicyNumbers = this.GetMasterPolicyNumbers();
        }

        /// <summary>
        /// If the system should continue processing quotes.
        /// </summary>
        private bool ShouldContinue => DateTime.Now < this.giveUpTime;

        /// <summary>
        /// Create an instance of PmiQuotePricing.
        /// </summary>
        /// <param name="dataLoan">The dataloan.</param>
        /// <param name="pricingCache">The pricing run cache.</param>
        /// <returns>The new instance.</returns>
        public static PmiQuotePricing Create(CPageData dataLoan, CacheInvariantPolicyResults pricingCache)
        {
            return new PmiQuotePricing(dataLoan, pricingCache);
        }

        /// <summary>
        /// Gets the result for a particular in-memory loan file.
        /// </summary>
        /// <returns>The pmi result.</returns>
        public PmiResult GetResult()
        {
            if (!this.ShouldContinue)
            {
                // We have timed out for this run.
                // No result.
                return null;
            }

            PmiResult pmiWinner = null;
            string processLog = Environment.NewLine;
            Dictionary<int, List<E_PmiCompanyT>> rankList = this.dataLoan.BrokerDB.PmiRankList;
            var startTime = DateTime.Now;
            List<PmiResult> pmiResults = new List<PmiResult>();

            foreach (var rank in rankList)
            {
                // 1. Get the list of all calls needed for this rank.
                // 2. Make the job create calls in parallel.
                var timeout = DateTime.Now + TimeSpan.FromSeconds(60);
                Dictionary<E_PmiCompanyT, MIRequestResult> requestsToRun = new Dictionary<E_PmiCompanyT, MIRequestResult>();
                foreach (var company in rank.Value)
                {
                    var requestData = this.CreateQuoteOrder(company);
                    if (requestData != null)
                    {
                        MIRequestHandler requestHandler = new MIRequestHandler(requestData);
                        MIRequestResult requestResult = requestHandler.SubmitToProcessor(checkWorkflow: false);
                        requestsToRun.Add(company, requestResult);

                        this.LogMiRequest(company);
                    }
                }

                // 3. Poll for results with a timeout and attempt limit.
                Dictionary<E_PmiCompanyT, MIRequestResult> finalResult = new Dictionary<E_PmiCompanyT, MIRequestResult>();
                var attemptCount = 0;
                while (requestsToRun.Values.Any(r => r.Status == MIRequestResultStatus.Processing) && this.ShouldContinue)
                {
                    foreach (var company in requestsToRun.Keys.ToList())
                    {
                        if (finalResult.ContainsKey(company))
                        {
                            // This one is ready.
                            continue;
                        }

                        var currentResult = requestsToRun[company];
                        var result = MIRequestHandler.CheckForRequestResults(currentResult.PublicJobId);
                        requestsToRun[company] = result;

                        if (result.Status != MIRequestResultStatus.Processing)
                        {
                            // Either a failure or success
                            finalResult[company] = result;
                        }
                    }
                    
                    // If we have reached the timeout or too many retries, we quit.
                    if (++attemptCount >= LendersOffice.Constants.ConstStage.MIFrameworkRequestJobPollingMaxAttempts
                        || DateTime.Now >= timeout)
                    {
                        break;
                    }

                    Tools.SleepAndWakeupRandomlyWithin(500, 2000, logWarning: false);
                }

                processLog += this.BuildResultSummary(requestsToRun);

                // At this point, we have the final result list.
                // We need to go through each and create results out of them.
                foreach (var company in finalResult.Keys)
                {
                    var result = finalResult[company];
                    if (result.Status == MIRequestResultStatus.Success)
                    {
                         MIOrderInfo quote = MIOrderInfo.RetrieveTransaction(
                             result.TransactionId,
                             result.OrderNumber,
                             this.dataLoan.sLId,
                             this.dataLoan.sBrokerId,
                             this.dataLoan.GetAppData(0).aAppId);

                        PmiResult pmiResult = this.GetPmiResultFromQuote(quote, company, rank.Key);
                        pmiResults.Add(pmiResult);
                    }
                }

                // 4. Merge and return the result as a PMI Result.
                if (pmiResults.Any(r => !r.IsDenied))
                {
                    this.pricingCache.PmiQuoteWaitTime += DateTime.Now - startTime;
                    pmiResults.Sort();
                    pmiWinner = pmiResults.First();
                    processLog += "Winner: " + pmiWinner.PmiCompany.ToString() + ", " + pmiWinner.MonthlyPct + ", " + pmiWinner.SinglePct + Environment.NewLine;
                    break;
                }
            }

            var rankTotalTime = DateTime.Now - startTime;

            // For the final logging.
            this.pricingCache.PmiQuoteWaitTime += rankTotalTime;
            processLog += rankTotalTime.TotalSeconds + " seconds at this rank." + Environment.NewLine;

            if (!this.ShouldContinue)
            {
                processLog += "MI pricing has timed out.  No more calls.";
            }

            this.pricingCache.PmiQuoteSummary += processLog;

            // Found no result at any rank.
            return pmiWinner;
        }

        /// <summary>
        /// Load the MiCompanies into the map dictionary.
        /// </summary>
        /// <returns>The mapping dictionary.</returns>
        private static Dictionary<E_PmiCompanyT, Guid> LoadMiCompanyMap()
        {
            var activeVendors = MortgageInsuranceVendorConfig.ListActiveVendors();
            Dictionary<E_PmiCompanyT, Guid> map = new Dictionary<E_PmiCompanyT, Guid>();

            // We only seek out the vendors in the hard-coded list of 
            // pricing-supported providers.
            foreach (var vendor in activeVendors)
            {
                switch (vendor.VendorType)
                {
                    case E_sMiCompanyNmT.Essent:
                        map.Add(E_PmiCompanyT.Essent, vendor.VendorId);
                        break;
                    case E_sMiCompanyNmT.Genworth:
                        map.Add(E_PmiCompanyT.Genworth, vendor.VendorId);
                        break;
                    case E_sMiCompanyNmT.MGIC:
                        map.Add(E_PmiCompanyT.MGIC, vendor.VendorId);
                        break;
                    case E_sMiCompanyNmT.Radian:
                        map.Add(E_PmiCompanyT.Radian, vendor.VendorId);
                        break;
                    case E_sMiCompanyNmT.NationalMI:
                        map.Add(E_PmiCompanyT.NationalMI, vendor.VendorId);
                        break;
                    case E_sMiCompanyNmT.Arch:
                        map.Add(E_PmiCompanyT.Arch, vendor.VendorId);
                        break;
                    case E_sMiCompanyNmT.MassHousing:
                        map.Add(E_PmiCompanyT.MassHousing, vendor.VendorId);
                        break;
                    case E_sMiCompanyNmT.LeaveBlank:
                    case E_sMiCompanyNmT.CMG:
                    case E_sMiCompanyNmT.UnitedGuaranty:
                    case E_sMiCompanyNmT.Amerin:
                    case E_sMiCompanyNmT.CAHLIF:
                    case E_sMiCompanyNmT.CMGPre94:
                    case E_sMiCompanyNmT.Commonwealth:
                    case E_sMiCompanyNmT.MDHousing:
                    case E_sMiCompanyNmT.MIF:
                    case E_sMiCompanyNmT.PMI:
                    case E_sMiCompanyNmT.RMIC:
                    case E_sMiCompanyNmT.RMICNC:
                    case E_sMiCompanyNmT.SONYMA:
                    case E_sMiCompanyNmT.Triad:
                    case E_sMiCompanyNmT.Verex:
                    case E_sMiCompanyNmT.WiscMtgAssr:
                    case E_sMiCompanyNmT.FHA:
                    case E_sMiCompanyNmT.VA:
                    case E_sMiCompanyNmT.USDA:
                        // These are not pricing-supported MI providers.
                        // They would need to be added to the lender priority
                        // setup before being legal call outs in pricing.
                        continue;
                    default:
                        throw new UnhandledEnumException(vendor.VendorType);
                }
            }

            return map;
        }

        /// <summary>
        /// Build a simple summary for reporting.
        /// </summary>
        /// <param name="requests">The request list.</param>
        /// <returns>A string representing summary.</returns>
        private string BuildResultSummary(Dictionary<E_PmiCompanyT, MIRequestResult> requests)
        {
            var summary = string.Empty;
            foreach (var company in requests.Keys)
            {
                var result = requests[company];
                string statusDesc = Environment.NewLine;
                switch (result.Status)
                {
                    case MIRequestResultStatus.Processing:
                        statusDesc += "Timed out." + Environment.NewLine;
                        break;
                    case MIRequestResultStatus.Failure:
                        statusDesc += "Failed. ";
                        result.Errors.ToList().ForEach(e => statusDesc += e + Environment.NewLine);
                        break;
                    case MIRequestResultStatus.Success:
                        statusDesc += "Success." + Environment.NewLine;
                        break;
                    default:
                        throw new UnhandledEnumException(result.Status);
                }

                summary += company.ToString() + ":" + statusDesc + Environment.NewLine;
            }

            return summary;
        }

        /// <summary>
        /// Log in our overall counts that a company was used.
        /// </summary>
        /// <param name="company">The company.</param>
        private void LogMiRequest(E_PmiCompanyT company)
        {
            var counts = this.pricingCache.pmiCompanyUseCount;
            if (!counts.ContainsKey(company))
            {
                counts[company] = 1;
            }
            else
            {
                counts[company]++;
            }
        }

        /// <summary>
        /// Get an engine-usable Pmi result from a quote from a PMI vendor.
        /// </summary>
        /// <param name="quote">Quote result.</param>
        /// <param name="company">Chosen MI company.</param>
        /// <param name="rank">Chosen MI rank.</param>
        /// <returns>The result.</returns>
        private PmiResult GetPmiResultFromQuote(MIOrderInfo quote, E_PmiCompanyT company, int rank)
        {
            var result = new PmiResult(quote, this.dataLoan, rank, company);
            return result;
        }

        /// <summary>
        /// Create an MI quote order.
        /// </summary>
        /// <param name="company">The MI company to use.</param>
        /// <returns>The MI Request.</returns>
        private MIRequestData CreateQuoteOrder(E_PmiCompanyT company)
        {
            if (!this.masterPolicyNumbers.ContainsKey(company))
            {
                // Lender does not have branch or lender-level
                // credentials for this lender.
                return null;
            }

            Guid loanID = this.dataLoan.sLId;
            Guid applicationID = this.dataLoan.GetAppData(0).aAppId;
            Guid vendorID = MiCompanyMap.Value[company];
            Guid branchID = this.dataLoan.sBranchId;
            E_sProdConvMIOptionT premiumType = this.dataLoan.sProdConvMIOptionT;
            bool ufmipFinanced = this.dataLoan.sFfUfMipIsBeingFinanced;

            // Using the MI vendor, look up the Master Policy Number from the lender's
            // corporate /branch settings in the same manner that the MI framework does
            // when the user selects a vendor (when pulling a quote), i.e.:
            // If the loan's branch has an MPN configured for the vendor then use it
            // Else get the MPN from the corporate level configuration for that vendor
            var masterPolicyNumber = this.masterPolicyNumbers[company];

            // Using the pricing engine data on a per loan program basis, 
            // look - up the MI coverage percent using the 
            // PmiManager.GetStandardCoverage method implemented in case 241546
            decimal? coveragePercent = PmiManager.GetStandardCoverage(
                this.dataLoan.sLpProductT,
                this.dataLoan.sLtvRForMi,
                this.dataLoan.sTerm,
                this.dataLoan.sProdSpT,
                this.dataLoan.sProd3rdPartyUwResultT,
                this.dataLoan.sFinMethT);

            // These defaults are provided in case 473112.
            // Set Premium Refundability to Non-Refundable as that's the case on 99.6% of orders in the DB
            MI_MIPremiumRefundableTypeEnumerated refundability = MI_MIPremiumRefundableTypeEnumerated.NotRefundable;

            // Set Renewal Option to Constant as that's the case on 84.6% of orders in the DB
            MI_MIRenewalCalculationTypeEnumerated renewalType = MI_MIRenewalCalculationTypeEnumerated.Constant;

            // Set Premium at Closing to Deferred as that's the case on 82.3% of orders in the DB
            MI_MIInitialPremiumAtClosingTypeEnumerated premiumAtClosing = MI_MIInitialPremiumAtClosingTypeEnumerated.Deferred;

            // Set Relocation loan to No as that's the case for nearly all orders in the DB
            bool relocation = false;

            // Set MIRequest.MIApplicationType to RateQuote so that the request is for a quote and not a policy.
            MIRequestData requestData = MIRequestData.CreateQuoteRequestData(
                PrincipalFactory.CurrentPrincipal, 
                loanID, 
                applicationID, 
                vendorID, 
                branchID, 
                premiumType,
                MISplitPremiumOption.DefaultMismoValue,
                refundability, 
                ufmipFinanced, 
                masterPolicyNumber,
                coveragePercent ?? -1, 
                renewalType,
                premiumAtClosing, 
                relocation,
                LendersOffice.Constants.ConstStage.MIFrameworkRequestJobPollingInterval,
                E_MiRequestType.PricingEngine);
            return requestData;
        }

        /// <summary>
        /// Get the Master policy numbers from the branch and broker settings.
        /// </summary>
        /// <returns>The master policy mapping.</returns>
        private Dictionary<E_PmiCompanyT, string> GetMasterPolicyNumbers()
        {
            var policyNumbers = new Dictionary<E_PmiCompanyT, string>();
            var brokerDb = this.dataLoan.BrokerDB;

            Guid brokerID = PrincipalFactory.CurrentPrincipal.BrokerId;
            Guid branchID = this.dataLoan.sBranchId;

            var branchCredentials = MortgageInsuranceVendorBranchSettings.ListCredentialsByBranchId(brokerID, branchID);
            var brokerCredentials = MortgageInsuranceVendorBrokerSettings.ListActiveVendorByBrokerId(brokerID);

            foreach (var credential in brokerCredentials)
            {
                if (MiCompanyMap.Value.ContainsValue(credential.VendorId))
                {
                    var companyT = MiCompanyMap.Value.Where(p => p.Value == credential.VendorId).First().Key;
                    policyNumbers[companyT] = credential.PolicyId;
                }
            }

            foreach (var credential in branchCredentials)
            {
                if (MiCompanyMap.Value.ContainsValue(credential.VendorId))
                {
                    var companyT = MiCompanyMap.Value.Where(p => p.Value == credential.VendorId).First().Key;
                    policyNumbers[companyT] = credential.PolicyId;
                }
            }

            return policyNumbers;
        }
    }
}
