﻿namespace LendersOffice.RatePrice
{
    using System;
    using Constants;
    using DataAccess;
    using Drivers.FileSystem;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Pricing engine storage that will store data share AWS S3 bucket. Only use this in AWS environment.
    /// </summary>
    public class AwsPriceEngineStorage : IPriceEngineStorage
    {
        /// <summary>
        /// Retrieve the file from persistent storage. Throw FileNotFoundException if key is not in storage.
        /// </summary>
        /// <param name="type">The type of file to retrieve.</param>
        /// <param name="key">The key of file to retrieve.</param>
        /// <returns>The local file path contains the file.</returns>
        public LocalFilePath Retrieve(PriceEngineStorageType type, SHA256Checksum key)
        {
            LocalFilePath tempFilePath = TempFileUtils.NewTempFile();

            Action<LocalFilePath> readHandler = delegate(LocalFilePath path)
            {
                FileOperationHelper.Move(path, tempFilePath);
            };

            FileIdentifier fileId = FileIdentifier.TryParse(key.Value).Value;
            FileStorageIdentifier location = FileStorageIdentifier.TryParse(ConstStage.AwsS3BucketForLpeData).Value;
            LendersOffice.Drivers.FileDB.FileDbHelper.RetrieveFile(location, fileId, readHandler);

            return tempFilePath;
        }

        /// <summary>
        /// Save the file in local path to persistent storage.
        /// </summary>
        /// <param name="type">The type of file to save.</param>
        /// <param name="key">The key of file to save.</param>
        /// <param name="path">The local path of the file save.</param>
        public void Save(PriceEngineStorageType type, SHA256Checksum key, LocalFilePath path)
        {
            FileIdentifier fileId = FileIdentifier.TryParse(key.Value).Value;
            FileStorageIdentifier location = FileStorageIdentifier.TryParse(ConstStage.AwsS3BucketForLpeData).Value;

            Action<LocalFilePath> saveHandler = delegate(LocalFilePath target)
            {
                FileOperationHelper.Copy(path, target, true);
            };

            LendersOffice.Drivers.FileDB.FileDbHelper.SaveNewFile(location, fileId, saveHandler);
        }
    }
}