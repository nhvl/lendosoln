﻿namespace LendersOfficeApp.los.RatePrice
{
    public class CAdjustItem
    {
        public CAdjustItem(string margin, string rate, string fee, string qrate, string teaserRate, string description, string debugString)
        {
            Margin = margin;
            Rate = rate;
            Fee = fee;
            Description = description;
            QRate = qrate;
            TeaserRate = teaserRate;

            DebugString = debugString;

        }

        public CAdjustItem(string debugString)
            : this("", "", "", "", "", "", debugString)
        {
        }

        public CAdjustItem(string margin, string rate, string fee, string qrate, string teaserRate, string description, string debugString, bool isLenderAdj)
            : this(margin, rate, fee, qrate, teaserRate, description, debugString)
        {
            IsLenderAdjustment = isLenderAdj;
        }

        public CAdjustItem(string margin, string rate, string fee, string qrate, string teaserRate, string description, string debugString, bool isLenderAdj, bool isPersist)
        : this(margin, rate, fee, qrate, teaserRate, description, debugString, isLenderAdj)
        {
            IsPersist = isPersist;
        }

        public string OptionCode { get; set;}

        public bool IsPerRateAdjForAllRateOptions { get; set; }

        public string Margin { get; private set;}

        public string Rate { get; private set;}

        public string Fee { get; private set;}

        public string Description { get; private set;}

        public string DebugString { get; set;}

        public string QRate { get; private set;}

        public string TeaserRate { get; private set;}

        public bool IsLenderAdjustment { get; private set;}

        public bool IsPersist { get; private set; }

        public static string AdjustFeeInResult(bool displayIn100, string fee)
        {
            try
            {
                // 1/23/2008 dd - OPM 19525 - If DisplayPmlFeeIn100Format option is turn on then we need to revert the sign of fee. Example fee of .375 will become -.375
                if (displayIn100)
                {
                    decimal v = decimal.Parse(fee.Replace("%", "")); // Strip out percent sign before convert to decimal.
                    return string.Format("{0:N3}", -1 * v);
                }
            }
            catch { }
            return fee;
        }

        public bool HasAdjustment
        {
            get
            {
                return ((Rate != string.Empty && Rate.Replace("%", "") != "0.000")
                   || (Fee != string.Empty && Fee.Replace("%", "") != "0.000")
                   || (Margin != string.Empty && Margin.Replace("%", "") != "0.000")
                   || (QRate != string.Empty && QRate.Replace("%", "") != "0.000")
                   || (TeaserRate != string.Empty && TeaserRate.Replace("%", "") != "0.000")
                   || IsPersist
                   );
            }
        }

    }
}
