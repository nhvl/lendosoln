﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;

namespace LendersOfficeApp.los.RatePrice
{
    public class RateMergeGroup
    {

        private List<CApplicantPriceXml> m_applicantPriceList = new List<CApplicantPriceXml>();

        private int m_lTerm = 0;
        private int m_lDue = 0;
        private int m_lRAdj1stCapMon = 0;
        private int m_lRAdjCapMon = 0;
        private E_sFinMethT m_lFinMethT;
        private string m_lProductType = "";
        private int m_lBuydwnR1 = 0;
        private int m_lBuydwnR2 = 0;
        private int m_lBuydwnR3 = 0;
        private int m_lBuydwnR4 = 0;
        private int m_lBuydwnR5 = 0;
        private bool m_isRateOptionsWorstThanLowerRateShown = false;
        private bool m_isDuRefiFilterEnabled = false;
        private E_sLT m_lLT = E_sLT.Conventional;
        private int m_lIOnlyMon = 0;

        private string m_lArmIndexNameVstr = string.Empty; // 3/26/2010 dd - OPM 44173
        private decimal m_lRAdj1stCapR = 0; // 3/26/2010 dd - OPM 44173
        private decimal m_lRAdjCapR = 0; // 3/26/2010 dd - OPM 44173
        private decimal m_lRAdjLifeCapR = 0; // 3/26/2010 dd - OPM 44173
        private int m_lPrepmtPeriod = 0; // 2/28/2013 mf. OPM 105510
        private bool m_includeLoosers = false;

        public int lTerm { get { return m_lTerm; } }
        public int lDue { get { return m_lDue; } }
        public int lRAdj1stCapMon { get { return m_lRAdj1stCapMon; } }
        public E_sFinMethT lFinMethT { get {return m_lFinMethT;} }
        public string lProductType { get { return m_lProductType; } }
        public E_sLT lLT { get { return m_lLT; } }
        public int lIOnlyMon { get { return m_lIOnlyMon; } }
        public int lHelocDraw { get; private set; }
        public int lHelocRepay { get; private set; }
        public RateMergeGroupKey rateMergeGroupKey { get; set; }

        public string lRateSheetDownloadEndD_rep { get; private set; }

        public string lArmIndexNameVstr
        {
            get; private set;
        }
        public int lRAdjCapMon
        {
            get; private set;
        }
        public string lRadj1stCapR_rep
        {
            get; private set;
        }
        public string lRAdjCapR_rep
        {
            get; private set;
        }
        public string lRAdjLifeCapR_rep
        {
            get; private set;
        }
        public string lRAdjIndexR_rep
        {
            get; private set;
        }

        public RateMergeGroup(CApplicantPriceXml applicantPrice, bool isRateOptionsWorstThanLowerRateShown, bool includeLoosers)
        {
            lHelocDraw = ParseInt(applicantPrice.lHelocDraw_rep, 0);
            lHelocRepay = ParseInt(applicantPrice.lHelocRepay_rep, 0);
            m_lTerm = ParseInt(applicantPrice.lTerm_rep, 0);
            m_lDue = ParseInt(applicantPrice.lDue_rep, 0);
            m_lRAdj1stCapMon = ParseInt(applicantPrice.lRadj1stCapMon_rep, 0);
            m_lRAdjCapMon = ParseInt(applicantPrice.lRAdjCapMon_rep, 0);
            m_lBuydwnR1 = ParseBuydown(applicantPrice.lBuydwnR1_rep);
            m_lBuydwnR2 = ParseBuydown(applicantPrice.lBuydwnR2_rep);
            m_lBuydwnR3 = ParseBuydown(applicantPrice.lBuydwnR3_rep);
            m_lBuydwnR4 = ParseBuydown(applicantPrice.lBuydwnR4_rep);
            m_lBuydwnR5 = ParseBuydown(applicantPrice.lBuydwnR5_rep);
            m_lFinMethT = applicantPrice.lFinMethT;
            m_lProductType = applicantPrice.lLpProductType;
            x_isInitialized = false;
            m_isRateOptionsWorstThanLowerRateShown = isRateOptionsWorstThanLowerRateShown;
            m_lRAdj1stCapR = ParseRate(applicantPrice.lRadj1stCapR_rep, 0);
            m_lRAdjCapR = ParseRate(applicantPrice.lRAdjCapR_rep, 0);
            m_lRAdjLifeCapR = ParseRate(applicantPrice.lRAdjLifeCapR_rep, 0);
            m_lLT = applicantPrice.lLT;
            m_lIOnlyMon = ParseInt(applicantPrice.lIOnlyMon_rep, 0);
            m_lPrepmtPeriod = ParseInt(applicantPrice.lPrepmtPeriod_rep, 0);

            m_lArmIndexNameVstr = applicantPrice.lArmIndexNameVstr;
            m_isDuRefiFilterEnabled = applicantPrice.IsDuRefiFilterEnabled;
            m_includeLoosers = includeLoosers;
            lRAdjIndexR_rep = applicantPrice.lRAdjIndexR_rep;
            lArmIndexNameVstr = applicantPrice.lArmIndexNameVstr;
            lRAdjCapMon = ParseInt(applicantPrice.lRAdjCapMon_rep, 0);
            lRadj1stCapR_rep = DecimalToString(ParseRate(applicantPrice.lRadj1stCapR_rep, 0));
            lRAdjCapR_rep = DecimalToString(ParseRate(applicantPrice.lRAdjCapR_rep, 0));
            lRAdjLifeCapR_rep = DecimalToString(ParseRate(applicantPrice.lRAdjLifeCapR_rep, 0));
        }
        public RateMergeGroup(CApplicantPriceXml applicantPrice, bool isRateOptionsWorstThanLowerRateShown) :
            this(applicantPrice, isRateOptionsWorstThanLowerRateShown, false)
        {
            
        }
        public RateMergeGroup(RateMergeGroupKey key)
        {
            lHelocDraw = key.lHelocDraw;
            lHelocRepay = key.lHelocRepay;
            m_lTerm = key.Term;
            m_lDue = key.Due;
            m_lRAdj1stCapMon = key.RAdj1stCapMon;
            m_lRAdjCapMon = key.RAdjCapMon;
            m_lBuydwnR1 = key.BuydwnR1;
            m_lBuydwnR2 = key.BuydwnR2;
            m_lBuydwnR3 = key.BuydwnR3;
            m_lBuydwnR4 = key.BuydwnR4;
            m_lBuydwnR5 = key.BuydwnR5;
            m_lFinMethT = key.FinMethT;
            m_lProductType = key.ProductType;
            x_isInitialized = false;

            m_lRAdj1stCapR = key.RAdj1stCapR;
            m_lRAdjCapR = key.RAdjCapR;
            m_lRAdjLifeCapR = key.RAdjLifeCapR; ;
            m_lPrepmtPeriod = key.PrepmtPeriod;
        }
        private int ParseInt(string s, int defaultValue)
        {
            int result;
            if (!int.TryParse(s, out result))
            {
                result = defaultValue;
            }
            return result;
        }
        private decimal ParseRate(string s, decimal defaultValue)
        {
            if (string.IsNullOrEmpty(s))
            {
                return defaultValue;
            }
            decimal result;
            if (!decimal.TryParse(s.Replace("%", ""), out result))
            {
                result = defaultValue;
            }
            return result;
        }
        private int ParseBuydown(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return 0;
            }
            decimal result;
            if (!decimal.TryParse(s.Replace("%", ""), out result))
            {
                result = 0;
            }

            // 3/9/2010 dd - If buydown rate is not a whole number than throw exception.
            if (Math.Round(result, 0) != result)
            {
                throw new CBaseException(ErrorMessages.Generic, "Buydown is not a whole number. Buydown=" + result);
            }
            return (int)result;

        }

        public decimal GetQMParRate(E_OriginatorCompPointSetting compSetting)
        {
            foreach (var item in RateOptions.Where(p=>p.IsRateMergeWinner).OrderBy(p => p.Rate))
            {
                if (item.IsDisqualified && !ConstStage.AllowDisqualifiedRateOptionsInParRateCalc)
                {
                    var debugElement = item.ToXmlElement(new System.Xml.XmlDocument());
                    debugElement.SetAttribute("CompSetting", compSetting.ToString("G"));
                    
                    Tools.LogInfo("[OPM 430909] Skipping disqualified rate option. Debug info: " + debugElement.OuterXml);
                    continue;
                }

                Func<decimal> getPoint;

                switch (compSetting)
                {
                    case E_OriginatorCompPointSetting.UsePoint:
                        getPoint = () => item.Point;
                        break;
                    case E_OriginatorCompPointSetting.UsePointLessOriginatorComp:
                        getPoint = () => item.PointLessOriginatorComp;
                        break;
                    case E_OriginatorCompPointSetting.UsePointWithOriginatorComp:
                        getPoint = () => item.PointIncludingOriginatorComp;
                        break;
                    default:
                        throw new UnhandledEnumException(compSetting);
                }

                if (getPoint() <= 0)
                {
                    return item.Rate;
                }
            }

            return 0;
        }

        private string DecimalToString(decimal value)
        {
            if (Math.Round(value, 0) == value)
            {
                return ((int)Math.Round(value, 0)).ToString();
            }
            else
            {
                return value.ToString();
            }
        }
        public string Name
        {
            get
            {
                if (m_lProductType.Equals("HELOC", StringComparison.OrdinalIgnoreCase) && lHelocDraw != 0)
                {
                    // 10/25/2013 dd - Per OPM 7776;
                    return string.Format("{0} YR DRAW / {1} YR REPAY HELOC", lHelocDraw / 12, lHelocRepay / 12);
                }
                string productType = m_isDuRefiFilterEnabled ? m_lProductType.Replace("CONFORMING", "DU REFI PLUS") : m_lProductType;
                string buyDownDesc = string.Empty;
                string prepayDesc = m_lPrepmtPeriod == 0 ? "" : " " + m_lPrepmtPeriod + "MO PPP";

                if (m_lBuydwnR1 > 0)
                {
                    buyDownDesc = " " + m_lBuydwnR1.ToString();
                    if (m_lBuydwnR2 > 0)
                    {
                        buyDownDesc += "-" + m_lBuydwnR2.ToString();
                        if (m_lBuydwnR3 > 0)
                        {
                            buyDownDesc += "-" + m_lBuydwnR3.ToString();
                            if (m_lBuydwnR4 > 0)
                            {
                                buyDownDesc += "-" + m_lBuydwnR4.ToString();
                                if (m_lBuydwnR5 > 0)
                                {
                                    buyDownDesc += "-" + m_lBuydwnR5.ToString();
                                }
                            }
                        }
                    }
                    buyDownDesc += " BUYDOWN";
                }
                if (m_lFinMethT == E_sFinMethT.Fixed)
                {
                    if (m_lTerm == m_lDue)
                    {
                        return string.Format("{0} YR FIXED {1}{2}{3}", m_lTerm / 12, productType, prepayDesc, buyDownDesc);
                    }
                    else
                    {
                        return string.Format("{0}/{1} BALLOON FIXED {2}{3}{4}", m_lTerm / 12, m_lDue / 12, productType, prepayDesc, buyDownDesc);
                    }
                }
                else if (m_lFinMethT == E_sFinMethT.ARM || m_lFinMethT == E_sFinMethT.Graduated)
                {
                    string capDescription = string.Empty;
                    if (m_lRAdj1stCapR != 0 || m_lRAdjCapR != 0 || m_lRAdjLifeCapR != 0)
                    {
                        capDescription = string.Format("{0}/{1}/{2}", 
                                                            DecimalToString(m_lRAdj1stCapR), // 4
                                DecimalToString(m_lRAdjCapR), // 5
                                DecimalToString(m_lRAdjLifeCapR) // 6
                                );
                    }
                    if (m_lTerm == m_lDue)
                    {
                        if (m_lRAdj1stCapMon >= 12)
                        {
                            int lRAdj1stCapMonInYr = m_lRAdj1stCapMon / 12;
                            int lRAdjCapMonInYr = m_lRAdjCapMon / 12;
                            int remainingTerm = m_lTerm / 12 - lRAdj1stCapMonInYr;
                            // 3/26/2010 dd - Fixed the naming scheme base on OPM 44173
                            return string.Format("{0}/{6} {1} {2} {7} {4} {5} YR ARM{3}", 
                                lRAdj1stCapMonInYr, // 0
                                m_lArmIndexNameVstr, //1
                                productType, // 2
                                buyDownDesc, //3
                                capDescription, // 4
                                m_lTerm / 12, // 5
                                lRAdjCapMonInYr, // 6
                                prepayDesc // 7
                                );
                            //return string.Format("{0}/{1} ARM {2}{3}", lRAdj1stCapMonInYr, remainingTerm, m_lProductType, buyDownDesc);
                        }
                        else
                        {
                            // 3/26/2010 dd - Fixed naming scheme base on OPM 44173.
                            if (m_lRAdj1stCapMon == m_lRAdjCapMon)
                            {
                                return string.Format("{0} {1} {5} {2} {3} YR ARM{4}",
                                    m_lArmIndexNameVstr, // 0
                                    productType, // 1
                                capDescription, // 2

                                m_lTerm / 12, // 3
                                buyDownDesc, // 4
                                prepayDesc // 5
                                );

                            }
                            else
                            {
                                return string.Format("{0} MO/{4} {2} {6} {5} {1} YR ARM{3}",
                                    m_lRAdj1stCapMon, // 0
                                    m_lTerm / 12,  // 1
                                    productType, //2
                                    buyDownDesc, // 3
                                    m_lArmIndexNameVstr, // 4
                                capDescription, // 5
                                prepayDesc // 6


                                    );
                            }

                            //return string.Format("{0} MO {1} YR ARM {2}{3}", 
                            //    m_lRAdj1stCapMon, m_lTerm / 12, m_lProductType, buyDownDesc);
                        }
                    }
                    else
                    {
                        if (m_lRAdj1stCapMon >= 12)
                        {
                            int lRAdj1stCapMonInYr = m_lRAdj1stCapMon / 12;
                            int remainingTerm = m_lTerm / 12 - lRAdj1stCapMonInYr;
                            // 3/26/2010 dd - Fixed the naming scheme base on OPM 44173
                            return string.Format("{0}/{1} {2} {7} {4} {5}/{6} BALLOON ARM{3}",
                                lRAdj1stCapMonInYr, // 0
                                m_lArmIndexNameVstr, //1
                                productType, // 2
                                buyDownDesc, //3
                                capDescription, // 4

                                m_lTerm / 12, // 5
                                m_lDue / 12, // 6
                                prepayDesc // 7
                                );

                            //return string.Format("{0}/{1} {2}/{3} BALLOON ARM {4}{5}", lRAdj1stCapMonInYr, remainingTerm, m_lTerm / 12, m_lDue / 12, m_lProductType, buyDownDesc);
                        }
                        else
                        {
                            // 3/26/2010 dd - Fixed naming scheme base on OPM 44173.
                            if (m_lRAdj1stCapMon == m_lRAdjCapMon)
                            {
                                return string.Format("{0} {1} {6} {2} {3}/{5} BALLOON ARM{4}",
                                    m_lArmIndexNameVstr, // 0
                                    productType, // 1
                                capDescription, // 2

                                m_lTerm / 12, // 3
                                buyDownDesc, // 4
                                m_lDue / 12, //5
                                m_lPrepmtPeriod // 6
                                );

                            }
                            else
                            {
                                return string.Format("{0} MO/{4} {2} {5} {1}/{6} BALLOON ARM{3}",
                                    m_lRAdj1stCapMon, // 0
                                    m_lTerm / 12,  // 1
                                    productType, //2
                                    buyDownDesc, // 3
                                    m_lArmIndexNameVstr, // 4
                                capDescription, // 5

                                m_lDue / 12, // 6
                                prepayDesc // 7

                                    );
                            }

                            //return string.Format("{0} MO {1}/{2} BALLOON ARM {3}{4}", m_lRAdj1stCapMon, m_lTerm / 12, m_lDue / 12, m_lProductType, buyDownDesc);
                        }

                    }
                }
                else
                {
                    throw new UnhandledEnumException(m_lFinMethT);
                }

            }
        }

        private DateTime m_currentRateSheetDownloadEndD = DateTime.MaxValue;
        public void Add(CApplicantPriceXml applicantPrice)
        {
            if (applicantPrice != null)
            {
                DateTime dt;
                if (DateTime.TryParse(applicantPrice.lRateSheetDownloadEndD_rep, out dt))
                {
                    // 11/13/2013 dd - Let be pessimistic and use the oldest download date.
                    if (dt < m_currentRateSheetDownloadEndD)
                    {
                        lRateSheetDownloadEndD_rep = applicantPrice.lRateSheetDownloadEndD_rep;
                        m_currentRateSheetDownloadEndD = dt;
                    }
                }
            }
            m_applicantPriceList.Add(applicantPrice);
            x_isInitialized = false;

        }



        private int x_representativeRateIndex = 0;
        private List<CApplicantRateOption> x_rateOptions = null;
        private bool x_isInitialized = false;

        public int RepresentativeRateIndex
        {

            get
            {
                if (!x_isInitialized)
                    Calculate();
                // 
                return x_representativeRateIndex;
            }
        }
        private void Calculate()
        {
            SortedList<decimal, List<CApplicantRateOption>> unexpiredRateList = new SortedList<decimal, List<CApplicantRateOption>>();
            SortedList<decimal, List<CApplicantRateOption>> expiredRateList = new SortedList<decimal, List<CApplicantRateOption>>();
         
            SortedList<decimal, List<CApplicantRateOption>> tempList = null;
        
            foreach (CApplicantPriceXml applicantPrice in m_applicantPriceList)
            {
                if (applicantPrice.IsBlockedRateLockSubmission)
                {
                    tempList = expiredRateList;
                }
                else
                {
                    tempList = unexpiredRateList;
                }
                foreach (CApplicantRateOption rateOption1 in applicantPrice.ApplicantRateOptions)
                {
                    decimal key = rateOption1.Rate;
                    rateOption1.SetApplicantPrice(applicantPrice);
                    if (tempList.ContainsKey(key))
                    {
                        List<CApplicantRateOption> _list = tempList[key];
                        bool rateOption1ViolateMaxDti = rateOption1.IsViolateMaxDti;

                        bool hasDelete = true;
                        // 6/18/2008 dd - Always assume we will add the new rate option. Once we decide we are not add new rate (set IsAddRateOption1=false) then do not turn it on again.
                        bool isAddRateOption1 = true;
                        // Truth Table for deciding which rate option to retain.
                        //
                        // Price Comparision      | RateOption1 DTI    | RateOption2 DTI    | OUTPUT
                        // -----------------------------------------------------------------------------------
                        // RO1.Point < RO2.Point  | Violate MaxDTI     | Violate MaxDTI     | Keep RateOption1
                        // RO1.Point < RO2.Point  | Violate MaxDTI     | DTI OK             | Keep Both Rates
                        // RO1.Point < RO2.Point  | DTI OK             | Violate MaxDTI     | Keep RateOption1
                        // RO1.Point < RO2.Point  | DTI OK             | DTI OK             | Keep RateOption1

                        // RO1.Point > RO2.Point  | Violate MaxDTI     | Violate MaxDTI     | Keep RateOption2
                        // RO1.Point > RO2.Point  | Violate MaxDTI     | DTI OK             | Keep RateOption2
                        // RO1.Point > RO2.Point  | DTI OK             | Violate MaxDTI     | Keep Both Rates
                        // RO1.Point > RO2.Point  | DTI OK             | DTI OK             | Keep RateOption2

                        // RO1.Point = RO2.Point  | Violate MaxDTI     | Violate MaxDTI     | Keep Both Rates
                        // RO1.Point = RO2.Point  | Violate MaxDTI     | DTI OK             | Keep RateOption2
                        // RO1.Point = RO2.Point  | DTI OK             | Violate MaxDTI     | Keep RateOption1
                        // RO1.Point = RO2.Point  | DTI OK             | DTI OK             | Keep Both Rates

                        for (int i = 0; i < _list.Count; i++)
                        {
                            CApplicantRateOption rateOption2 = _list[i] as CApplicantRateOption;
                            bool rateOption2ViolateMaxDti = rateOption2.IsViolateMaxDti;

                            if (!rateOption1.IsDisqualified && rateOption2.IsDisqualified)
                            {
                                // 10/02/12 OPM 91637.
                                rateOption2.MarkedAsDelete = true;
                            }
                            else if (rateOption1.IsDisqualified && !rateOption2.IsDisqualified)
                            {
                                // 10/02/12 OPM 91637.
                                isAddRateOption1 = false;
                            }
                            else if (rateOption1.Point < rateOption2.Point)
                            {
                                if (!rateOption1ViolateMaxDti || rateOption2ViolateMaxDti)
                                    rateOption2.MarkedAsDelete = true;
                            }
                            else if (rateOption1.Point > rateOption2.Point)
                            {
                                if (rateOption1ViolateMaxDti || !rateOption2ViolateMaxDti)
                                    isAddRateOption1 = false;
                            }
                            else if (rateOption1.Point == rateOption2.Point)
                            {
                                if (rateOption1ViolateMaxDti && !rateOption2ViolateMaxDti)
                                {
                                    isAddRateOption1 = false;
                                }
                                else if (!rateOption1ViolateMaxDti && rateOption2ViolateMaxDti)
                                {
                                    rateOption2.MarkedAsDelete = true;
                                }
                                else
                                {
                                    // 5/23/2008 dd - For now to break a tie we will compare the loan program name. 
                                    int ret = rateOption1.LpTemplateNm.CompareTo(rateOption2.LpTemplateNm);
                                    if (ret < 0)
                                    {
                                        rateOption2.MarkedAsDelete = true;
                                    }
                                    else if (ret >= 0)
                                    {
                                        isAddRateOption1 = false;
                                    }
                                }
                            }

                        }

                        if (isAddRateOption1)
                        {
                            _list.Add(rateOption1);
                        }
                        if (hasDelete)
                        {
                            for (int i = _list.Count - 1; i >= 0; i--)
                            {
                                CApplicantRateOption ro = _list[i] as CApplicantRateOption;
                                if (ro.MarkedAsDelete)
                                {
                                    _list.RemoveAt(i);
                                }

                            }
                        }

                    }
                    else
                    {
                        List<CApplicantRateOption> _list = new List<CApplicantRateOption>();
                        _list.Add(rateOption1);
                        tempList.Add(key, _list);
                    }
                }

            }


            // 6/2/2008 dd - If there are no unexpired rate then use the expire rate for this group.
            SortedList<decimal, List<CApplicantRateOption>> sortedList = unexpiredRateList.Count > 0 ? unexpiredRateList : expiredRateList;
            List<CApplicantRateOption> list = new List<CApplicantRateOption>();

            decimal previousNormalPoint = decimal.MaxValue;
            decimal previousViolateDtiPoint = decimal.MaxValue;

            foreach (List<CApplicantRateOption> _rateOptionList in sortedList.Values)
            {
                _rateOptionList.Sort(new DescendingPointRateOptionComparer());

                foreach (CApplicantRateOption o in _rateOptionList)
                {
                    if (o.IsViolateMaxDti)
                    {
                        if (m_isRateOptionsWorstThanLowerRateShown)
                        {
                            list.Insert(0, o);
                        }
                        else
                        {
                            if (o.Point < previousViolateDtiPoint && o.Point < previousNormalPoint)
                            {
                                list.Insert(0, o);
                                previousViolateDtiPoint = o.Point;
                            }
                        }
                    }
                    else
                    {
                        if (m_isRateOptionsWorstThanLowerRateShown)
                        {
                            // 7/1/2009 dd - OPM 24899 - Do not filter out higher rate with higher fee.
                            list.Insert(0, o);
                        }
                        else
                        {
                            if (o.Point < previousNormalPoint)
                            {
                                // 7/1/2009 dd - OPM 24899 - Filter out higher rate with higher fee.
                                list.Insert(0, o);
                                previousNormalPoint = o.Point;
                            }
                        }
                    }
                }
            }

            x_representativeRateIndex = 0;
            decimal absolutePoint = decimal.MaxValue;
            for (int i = 0; i < list.Count; i++)
            {
                CApplicantRateOption o = list[i] as CApplicantRateOption;
                o.IsRateMergeWinner = true;
                if (Math.Abs(o.Point) < absolutePoint)
                {
                    x_representativeRateIndex = i;
                    absolutePoint = Math.Abs(o.Point);
                }

            }



            if (m_includeLoosers)
            {
                HashSet<CApplicantRateOption> winner = new HashSet<CApplicantRateOption>(list);
                Dictionary<decimal, int> counts = new Dictionary<decimal,int>();

                foreach (CApplicantPriceXml applicantPrice in m_applicantPriceList)
                {
                    foreach (CApplicantRateOption rateOption in applicantPrice.ApplicantRateOptions)
                    {
                        //dont include loosers who have no winner.
                        if (winner.Contains(rateOption) || winner.All(p=>p.Rate != rateOption.Rate))
                        {
                            continue;
                        }
                        int count;

                        if (!counts.TryGetValue(rateOption.Rate, out count))
                        {
                            count = 1;
                            counts.Add(rateOption.Rate, count);
                        }
                        else
                        {
                            counts[rateOption.Rate] += 1;
                        }
                      
                        list.Add(rateOption);
                    }
                }

                list = list.OrderByDescending(p => p.Rate).ThenByDescending(p=>p.PointIn100).ThenByDescending(p => p.IsRateMergeWinner).ToList();

                foreach (var rateOption in winner)
                {
                    int count;
                    if (!counts.TryGetValue(rateOption.Rate, out count))
                    {
                        count = 0;
                    }
                    rateOption.NumberOfRateMergeCompetitors = count;
                }
            }

            x_rateOptions = list;

            x_isInitialized = true;

        }
        public List<CApplicantRateOption> RateOptions
        {
            get
            {
                if (!x_isInitialized)
                    Calculate();

                return x_rateOptions;
            }
        }

        internal List<CApplicantRateOption> AllRateOptions
        {
            get
            {
                List<CApplicantRateOption> allOptions = new List<CApplicantRateOption>();
                foreach(CApplicantPriceXml ap in m_applicantPriceList)
                {
                    allOptions.AddRange(ap.ApplicantRateOptions);
                }

                return allOptions;
            }
        }
    }
}
