﻿using System;
using System.Collections.Generic;
using System.Xml;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice
{
    public class RtConsequence
    {
        private RtAdjustTarget m_feeAdjustTarget = SimpleRtAdjustTarget.Zero;
        private RtAdjustTarget m_marginAdjustTarget = SimpleRtAdjustTarget.Zero;
        private RtAdjustTarget m_rateAdjustTarget = SimpleRtAdjustTarget.Zero;
        private RtAdjustTarget m_baseMarginTarget = SimpleRtAdjustTarget.Zero;
        private RtAdjustTarget m_qltvAdjustTarget = SimpleRtAdjustTarget.Zero;
        private RtAdjustTarget m_qcltvAdjustTarget = SimpleRtAdjustTarget.Zero;
        private RtAdjustTarget m_qscoreTarget = SimpleRtAdjustTarget.Zero;
        private RtAdjustTarget m_maxFrontEndYspAdjustTarget = SimpleRtAdjustTarget.Zero;
        private RtAdjustTarget m_maxYspAdjustTarget = SimpleRtAdjustTarget.Zero;
        private RtAdjustTarget m_qrateTarget = SimpleRtAdjustTarget.Zero;
        private RtAdjustTarget m_maxDtiTarget = SimpleRtAdjustTarget.Zero;
        private RtAdjustTarget m_qlamtAdjustTarget = SimpleRtAdjustTarget.Zero;

        // OPM 7686 
        private RtAdjustTarget m_qrateAdjustTarget = SimpleRtAdjustTarget.Zero;
        private RtAdjustTarget m_teaserRateAdjustTarget = SimpleRtAdjustTarget.Zero;

        // OPM 17988
        private RtAdjustTarget m_lockDaysAdjustTarget = SimpleRtAdjustTarget.Zero;


        public RtConsequence(XmlNode xmlNode)
        {

            CAdjustTarget.E_Target target;
            XmlElement xmlElement = (XmlElement)xmlNode;

            HashSet<string> used = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

            Category = LpeKeywordCategory.Invariant;
            foreach (XmlNode targetElement in xmlElement.SelectNodes(".//Adjust"))
            {
                string targetName = targetElement.Attributes["Target"].Value;
                if (used.Contains(targetName))
                    continue;

                used.Add(targetName);


                RtAdjustTarget adjustTarget = RtAdjustTargetFactory.Create(targetName, targetElement.InnerText, out target);
                if (adjustTarget.IsConstant() == false)
                {
                    LpeKeywordCategory curValue = LpeKeywordUtil.GetLpeKeywordCategory(targetElement.InnerText);
                    if (Category < curValue)
                        Category = curValue;
                }


                switch (target)
                {
                    case CAdjustTarget.E_Target.Fee:
                        m_feeAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.Margin:
                        m_marginAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.Rate:
                        m_rateAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.BaseMargin:
                        m_baseMarginTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.Qltv:
                        m_qltvAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.Qcltv:
                        m_qcltvAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.Qscore:
                        m_qscoreTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.MaxFrontEndYsp:
                        m_maxFrontEndYspAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.MaxYsp:
                        m_maxYspAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.Qrate:
                        m_qrateTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.MaxDti:
                        m_maxDtiTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.QLAmt:
                        m_qlamtAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.QrateAdjust:
                        m_qrateAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.TeaserRateAdjust:
                        m_teaserRateAdjustTarget = adjustTarget;
                        break;
                    case CAdjustTarget.E_Target.LockDaysAdjust:
                        m_lockDaysAdjustTarget = adjustTarget;
                        break;

                    default:
                        throw new UnhandledEnumException(target);
                }

            } // for loop

            HasQConsequence = !QltvAdjustTarget.IsBlank ||
                                !QcltvAdjustTarget.IsBlank ||
                                !QscoreTarget.IsBlank ||
                                !QLAmtAdjustTarget.IsBlank ||
                                !LockDaysAdjustTarget.IsBlank;

            XmlNodeList stipulations = xmlElement.SelectNodes(".//Stipulation"); // selects the "Stipulation" element descendants of the context node

            if (0 == stipulations.Count)
                Stipulation = "";
            else
                Stipulation = stipulations[0].InnerText.TrimWhitespaceAndBOM();

            XmlNodeList disqualifies = xmlElement.SelectNodes(".//Disqualify");
            Disqualify = disqualifies.Count > 0;

            XmlNodeList skips = xmlElement.SelectNodes(".//Skip");
            Skip = skips.Count > 0;

            HasOnlyStipConsequence =
                QltvAdjustTarget.IsBlank &&
                QcltvAdjustTarget.IsBlank &&
                QscoreTarget.IsBlank &&
                QLAmtAdjustTarget.IsBlank &&
                QrateAdjustTarget.IsBlank &&
                LockDaysAdjustTarget.IsBlank &&
                FeeAdjustTarget.IsBlank &&
                MaxFrontEndYspAdjustTarget.IsBlank &&
                MaxYspAdjustTarget.IsBlank &&
                MaxDtiTarget.IsBlank &&
                MarginAdjustTarget.IsBlank &&
                RateAdjustTarget.IsBlank &&
                MarginBaseTarget.IsBlank &&
                TeaserRateAdjustTarget.IsBlank
                && MarginBaseTarget.IsBlank
                && Disqualify == false
                && Skip == false;

        }

        public RtConsequence(LendersOffice.RatePrice.Model.PricePolicyRule ruleModel)
        {
            CAdjustTarget.E_Target target;
            Category = LpeKeywordCategory.Invariant;

            Action<string, string> setAdjust = delegate (string targetName, string data)
            {
                if( data == null)
                {
                    return;
                }

                RtAdjustTarget adjustTarget = RtAdjustTargetFactory.Create(targetName, data, out target);
                if (adjustTarget.IsConstant() == false)
                {
                    LpeKeywordCategory curValue = LpeKeywordUtil.GetLpeKeywordCategory(data);
                    if (Category < curValue)
                        Category = curValue;
                }

                switch (target)
                {
                    case CAdjustTarget.E_Target.Fee:
                        m_feeAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.Margin:
                        m_marginAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.Rate:
                        m_rateAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.BaseMargin:
                        m_baseMarginTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.Qltv:
                        m_qltvAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.Qcltv:
                        m_qcltvAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.Qscore:
                        m_qscoreTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.MaxFrontEndYsp:
                        m_maxFrontEndYspAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.MaxYsp:
                        m_maxYspAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.Qrate:
                        m_qrateTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.MaxDti:
                        m_maxDtiTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.QLAmt:
                        m_qlamtAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.QrateAdjust:
                        m_qrateAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.TeaserRateAdjust:
                        m_teaserRateAdjustTarget = adjustTarget;
                        break;
                    case CAdjustTarget.E_Target.LockDaysAdjust:
                        m_lockDaysAdjustTarget = adjustTarget;
                        break;

                    default:
                        throw new UnhandledEnumException(target);
                }
            };

            setAdjust("Fee", ruleModel.AdjustFee);
            setAdjust("Margin", ruleModel.AdjustMargin);
            setAdjust("Rate", ruleModel.AdjustRate);
            setAdjust("BaseMargin", ruleModel.AdjustBaseMagin);
            setAdjust("Qltv", ruleModel.AdjustQltv);
            setAdjust("Qcltv", ruleModel.AdjustQcltv);
            setAdjust("Qscore", ruleModel.AdjustQscore);
            setAdjust("MaxFrontEndYsp", ruleModel.AdjustMaxFrontEndYsp);
            setAdjust("MaxYsp", ruleModel.AdjustMaxYsp);
            setAdjust("Qrate", ruleModel.AdjustQrate);
            setAdjust("MaxDti", ruleModel.AdjustMaxDti);
            setAdjust("QLAmt", ruleModel.AdjustQLAmt);
            setAdjust("QrateAdjust", ruleModel.AdjustQrateAdjust);
            setAdjust("TeaserRateAdjust", ruleModel.AdjustTeaserRateAdjust);
            setAdjust("LockDaysAdjust", ruleModel.AdjustLockDaysAdjust);

            HasQConsequence = !QltvAdjustTarget.IsBlank ||
                                !QcltvAdjustTarget.IsBlank ||
                                !QscoreTarget.IsBlank ||
                                !QLAmtAdjustTarget.IsBlank ||
                                !LockDaysAdjustTarget.IsBlank;

            Stipulation = (ruleModel.Stipulation ?? string.Empty).TrimWhitespaceAndBOM();
            Disqualify = ruleModel.Disqualify;
            Skip = ruleModel.Skip;

            HasOnlyStipConsequence =
                QltvAdjustTarget.IsBlank &&
                QcltvAdjustTarget.IsBlank &&
                QscoreTarget.IsBlank &&
                QLAmtAdjustTarget.IsBlank &&
                QrateAdjustTarget.IsBlank &&
                LockDaysAdjustTarget.IsBlank &&
                FeeAdjustTarget.IsBlank &&
                MaxFrontEndYspAdjustTarget.IsBlank &&
                MaxYspAdjustTarget.IsBlank &&
                MaxDtiTarget.IsBlank &&
                MarginAdjustTarget.IsBlank &&
                RateAdjustTarget.IsBlank &&
                MarginBaseTarget.IsBlank &&
                TeaserRateAdjustTarget.IsBlank
                && MarginBaseTarget.IsBlank
                && Disqualify == false
                && Skip == false;

        }

        public RtConsequence(LendersOffice.RatePrice.Model.PricePolicyRuleV2 ruleModel)
        {
            CAdjustTarget.E_Target target;
            Category = LpeKeywordCategory.Invariant;

            Action<string, string> setAdjust = delegate (string targetName, string data)
            {
                if (data == null)
                {
                    return;
                }

                RtAdjustTarget adjustTarget = RtAdjustTargetFactory.Create(targetName, data, out target);
                if (adjustTarget.IsConstant() == false)
                {
                    LpeKeywordCategory curValue = LpeKeywordUtil.GetLpeKeywordCategory(data);
                    if (Category < curValue)
                        Category = curValue;
                }

                switch (target)
                {
                    case CAdjustTarget.E_Target.Fee:
                        m_feeAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.Margin:
                        m_marginAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.Rate:
                        m_rateAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.BaseMargin:
                        m_baseMarginTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.Qltv:
                        m_qltvAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.Qcltv:
                        m_qcltvAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.Qscore:
                        m_qscoreTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.MaxFrontEndYsp:
                        m_maxFrontEndYspAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.MaxYsp:
                        m_maxYspAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.Qrate:
                        m_qrateTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.MaxDti:
                        m_maxDtiTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.QLAmt:
                        m_qlamtAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.QrateAdjust:
                        m_qrateAdjustTarget = adjustTarget;
                        break;

                    case CAdjustTarget.E_Target.TeaserRateAdjust:
                        m_teaserRateAdjustTarget = adjustTarget;
                        break;
                    case CAdjustTarget.E_Target.LockDaysAdjust:
                        m_lockDaysAdjustTarget = adjustTarget;
                        break;

                    default:
                        throw new UnhandledEnumException(target);
                }
            };

            setAdjust("Fee", ruleModel.AdjustFee);
            setAdjust("Margin", ruleModel.AdjustMargin);
            setAdjust("Rate", ruleModel.AdjustRate);
            setAdjust("BaseMargin", ruleModel.AdjustBaseMagin);
            setAdjust("Qltv", ruleModel.AdjustQltv);
            setAdjust("Qcltv", ruleModel.AdjustQcltv);
            setAdjust("Qscore", ruleModel.AdjustQscore);
            setAdjust("MaxFrontEndYsp", ruleModel.AdjustMaxFrontEndYsp);
            setAdjust("MaxYsp", ruleModel.AdjustMaxYsp);
            setAdjust("Qrate", ruleModel.AdjustQrate);
            setAdjust("MaxDti", ruleModel.AdjustMaxDti);
            setAdjust("QLAmt", ruleModel.AdjustQLAmt);
            setAdjust("QrateAdjust", ruleModel.AdjustQrateAdjust);
            setAdjust("TeaserRateAdjust", ruleModel.AdjustTeaserRateAdjust);
            setAdjust("LockDaysAdjust", ruleModel.AdjustLockDaysAdjust);

            HasQConsequence = !QltvAdjustTarget.IsBlank ||
                                !QcltvAdjustTarget.IsBlank ||
                                !QscoreTarget.IsBlank ||
                                !QLAmtAdjustTarget.IsBlank ||
                                !LockDaysAdjustTarget.IsBlank;

            Stipulation = (ruleModel.Stipulation ?? string.Empty).TrimWhitespaceAndBOM();
            Disqualify = ruleModel.Disqualify;
            Skip = ruleModel.Skip;

            HasOnlyStipConsequence =
                QltvAdjustTarget.IsBlank &&
                QcltvAdjustTarget.IsBlank &&
                QscoreTarget.IsBlank &&
                QLAmtAdjustTarget.IsBlank &&
                QrateAdjustTarget.IsBlank &&
                LockDaysAdjustTarget.IsBlank &&
                FeeAdjustTarget.IsBlank &&
                MaxFrontEndYspAdjustTarget.IsBlank &&
                MaxYspAdjustTarget.IsBlank &&
                MaxDtiTarget.IsBlank &&
                MarginAdjustTarget.IsBlank &&
                RateAdjustTarget.IsBlank &&
                MarginBaseTarget.IsBlank &&
                TeaserRateAdjustTarget.IsBlank
                && MarginBaseTarget.IsBlank
                && Disqualify == false
                && Skip == false;

        }

        public RtAdjustTarget FeeAdjustTarget
        {
            get { return m_feeAdjustTarget; }
        }

        public RtAdjustTarget QltvAdjustTarget
        {
            get
            {
                return m_qltvAdjustTarget;
            }
        }

        public RtAdjustTarget QcltvAdjustTarget
        {
            get
            {
                return m_qcltvAdjustTarget;
            }
        }

        public RtAdjustTarget QLAmtAdjustTarget
        {
            get
            {
                return m_qlamtAdjustTarget;
            }
        }

        public RtAdjustTarget MaxFrontEndYspAdjustTarget
        {
            get
            {
                return m_maxFrontEndYspAdjustTarget;
            }
        }


        public RtAdjustTarget MaxYspAdjustTarget
        {
            get
            {
                return m_maxYspAdjustTarget;
            }
        }


        public RtAdjustTarget QscoreTarget
        {
            get
            {
                return m_qscoreTarget;
            }
        }

        public RtAdjustTarget MaxDtiTarget
        {
            get
            {
                return m_maxDtiTarget;
            }

        }

        public RtAdjustTarget MarginAdjustTarget
        {
            get
            {
                return m_marginAdjustTarget;
            }
        }

        public RtAdjustTarget RateAdjustTarget
        {
            get
            {
                return m_rateAdjustTarget;
            }
        }

        public RtAdjustTarget MarginBaseTarget
        {
            get
            {
                return m_baseMarginTarget;
            }
        }

        public RtAdjustTarget QrateAdjustTarget
        {
            get
            {
                return m_qrateAdjustTarget;
            }
        }

        public RtAdjustTarget TeaserRateAdjustTarget
        {
            get
            {
                return m_teaserRateAdjustTarget;
            }
        }
        public RtAdjustTarget LockDaysAdjustTarget
        {
            get
            {
                return m_lockDaysAdjustTarget;
            }
        }


        public bool HasQConsequence { get; private set;}

        // OPM 128219
        public bool HasOnlyStipConsequence { get; private set;}

        public string Stipulation { get; private set;}

        public bool Disqualify { get; private set; }
        public bool Skip { get; private set;}

        public int EstimateSize()
        {
            int size = 80;
            if (m_feeAdjustTarget != SimpleRtAdjustTarget.Zero) size += 32;
            if (m_marginAdjustTarget != SimpleRtAdjustTarget.Zero) size += 32;
            if (m_rateAdjustTarget != SimpleRtAdjustTarget.Zero) size += 32;
            if (m_baseMarginTarget != SimpleRtAdjustTarget.Zero) size += 32;
            if (m_qltvAdjustTarget != SimpleRtAdjustTarget.Zero) size += 32;
            if (m_qcltvAdjustTarget != SimpleRtAdjustTarget.Zero) size += 32;
            if (m_qscoreTarget != SimpleRtAdjustTarget.Zero) size += 32;
            if (m_maxFrontEndYspAdjustTarget != SimpleRtAdjustTarget.Zero) size += 32;
            if (m_maxYspAdjustTarget != SimpleRtAdjustTarget.Zero) size += 32;
            if (m_qrateTarget != SimpleRtAdjustTarget.Zero) size += 32;
            if (m_maxDtiTarget != SimpleRtAdjustTarget.Zero) size += 32;
            if (m_qlamtAdjustTarget != SimpleRtAdjustTarget.Zero) size += 32;

            return size;
        }

        public LpeKeywordCategory Category { get; private set;}


    }
}
