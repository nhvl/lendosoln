namespace LendersOfficeApp.los.RatePrice
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.ManualInvestorProductExpiration;
    using LendersOffice.ObjLib.PriceGroups;
    using LendersOffice.RatePrice;
    using LendersOffice.RatePrice.FileBasedPricing;

    public class CLoanProgramsFromDb
    {
        public enum E_Option
        {
            All = 0,
            Only1stLien = 1,
            Only2ndLien = 2
        }

        static public DbDataReader ListByBrokerIdForNonlpe(Guid brokerId)
        {
            //ListNonlpeLoanProgramsByBrokerId
            SqlParameter[] pars = new SqlParameter[] { new SqlParameter("@BrokerId", brokerId) };
            return StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListNonlpeLoanProgramsByBrokerId", pars);
        }

        public static LoanProgramByInvestorSet ListByBrokerIdToRunLpe(BrokerDB broker, Guid priceGroupId, Guid firstLienLpId, LoanProgramFilterParameters parameters, bool preserveProductCode = false)
        {
            if (broker.ActualPricingBrokerId != Guid.Empty)
            {
                var priceGroup = PriceGroup.RetrieveByID(priceGroupId, broker.BrokerID);
                var snapshot = broker.RetrieveLatestManualImport();
                return ListLoanProgramsFromFile.ListUsingSnapshot(snapshot, priceGroup, firstLienLpId, parameters, preserveProductCode);
            }
            else
            {
                return ListByBrokerIdToRunLpe(broker.BrokerID, priceGroupId, firstLienLpId, parameters, preserveProductCode);
            }
        }
        /// <summary>
        /// Gets the loan program set based on the passed in paramters.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="priceGroupId"></param>
        /// <param name="firstLienLpId">Pass Guid.Empty if you are running pricing for first lien.</param>
        /// <param name="parameters">The filter parameters for picking the loan programs.</param>
        /// <returns>Loan program set.</returns>
        private static LoanProgramByInvestorSet ListByBrokerIdToRunLpe(Guid brokerId, Guid priceGroupId, Guid firstLienLpId, LoanProgramFilterParameters parameters, bool preserveProductCode = false)
        {
            /*order of filter precedence:
             * 0)manualExcludeProductList 
             * 1)onlyIncludeLpProductType (only programs of this 1 product type will be included)
             * 2)
             *  -onlyIncludeProductTypes (only programs of these product types will be included)
             *  -ignoreProductTypes (programs of these types will not be included)
             * 3)includeProductTypes
             * 4)ignoreLoanTypes (unless includeProductTypes (but not onlyIncludeProductTypes) includes it.)
            */

            E_sLienPosT lienPosTIncluded1;
            E_sLienPosT lienPosTIncluded2;
            switch (parameters.LienOption)
            {
                case E_Option.All:
                    lienPosTIncluded1 = E_sLienPosT.First;
                    lienPosTIncluded2 = E_sLienPosT.Second;
                    break;
                case E_Option.Only1stLien:
                    lienPosTIncluded1 = E_sLienPosT.First;
                    lienPosTIncluded2 = E_sLienPosT.First;
                    break;
                case E_Option.Only2ndLien:
                    lienPosTIncluded1 = E_sLienPosT.Second;
                    lienPosTIncluded2 = E_sLienPosT.Second;
                    break;
                default:
                    throw new UnhandledEnumException(parameters.LienOption);
            }

            ArrayList pars = new ArrayList(6);
            pars.Add(new SqlParameter("@BrokerID", brokerId));
            pars.Add(new SqlParameter("@LienPosTIncluded1", (int)lienPosTIncluded1));
            pars.Add(new SqlParameter("@LienPosTIncluded2", (int)lienPosTIncluded2));

            pars.Add(new SqlParameter("@FilterDue10Yrs", parameters.Due10Years));
            pars.Add(new SqlParameter("@FilterDue15Yrs", parameters.Due15Years));
            pars.Add(new SqlParameter("@FilterDue20Yrs", parameters.Due20Years));
            pars.Add(new SqlParameter("@FilterDue25Yrs", parameters.Due25Years));
            pars.Add(new SqlParameter("@FilterDue30Yrs", parameters.Due30Years));
            pars.Add(new SqlParameter("@FilterDueOther", parameters.DueOther));
            pars.Add(new SqlParameter("@FilterFinMethFixed", parameters.FinMethFixed));
            pars.Add(new SqlParameter("@FilterFinMeth3YrsArm", parameters.FinMeth3YearsArm));
            pars.Add(new SqlParameter("@FilterFinMeth5YrsArm", parameters.FinMeth5YearsArm));
            pars.Add(new SqlParameter("@FilterFinMeth7YrsArm", parameters.FinMeth7YearsArm));
            pars.Add(new SqlParameter("@FilterFinMeth10YrsArm", parameters.FinMeth10YearsArm));
            pars.Add(new SqlParameter("@FilterFinMethOther", parameters.FinMethOther));
            pars.Add(new SqlParameter("@FilterOnlyCanBeStandAlone2nd", parameters.CanOnlyBeStandalone2nd));
            pars.Add(new SqlParameter("@Include10yrAnd25yrInOtherDueFilter", parameters.Include10And25YearTermsInOtherDue));
            pars.Add(new SqlParameter("@PriceGroupId", (Guid.Empty == priceGroupId) ? (object)DBNull.Value : (object)priceGroupId));
            pars.Add(new SqlParameter("@FilterPaymentTypePI", parameters.PaymentTypePI));
            pars.Add(new SqlParameter("@FilterPaymentTypeIOnly", parameters.PaymentTypeIOnly));
            pars.Add(new SqlParameter("@IsNonQm", parameters.IsNonQm));

            if (string.IsNullOrEmpty(parameters.MatchingProductType) == false)
            {
                pars.Add(new SqlParameter("@FilterLpProductType", parameters.MatchingProductType));
            }

            if (!string.IsNullOrEmpty(parameters.MatchingInvestorName))
            {
                pars.Add(new SqlParameter("@FilterLpInvestorName", parameters.MatchingInvestorName));
            }

            ProductPairingTable productPairingTable = null;

            if (Guid.Empty != firstLienLpId)
            {
                pars.Add(new SqlParameter("@FirstLienLpId", firstLienLpId));

                // Need to perform product pairing
                productPairingTable = ProductPairingTable.RetrieveProductPairingTable();
            }

            List<InvestorProductItem> manualExcludeProductList = GetManualExcludeProductList(brokerId, PriceGroup.RetrieveByID(priceGroupId, brokerId).LockPolicyID.Value);

            LoanProgramByInvestorSet lpSet = new LoanProgramByInvestorSet();

            StringBuilder debugString = new StringBuilder();
            debugString.AppendLine("exec ListNonMasterLoanProgramsByBrokerIDToRunLpe");
            foreach (SqlParameter p in pars)
            {
                debugString.AppendLine("   " + p.ParameterName + "=" + p.Value);
            }
            if (parameters.ProductTypesToIgnore != null && parameters.ProductTypesToIgnore.Length > 0)
            {
                debugString.AppendLine();
                debugString.AppendLine("IgnoreProductTypes");
                foreach (var s in parameters.ProductTypesToIgnore)
                {
                    debugString.AppendLine("   " + s);
                }
            }
            if (parameters.LoanTypesToIgnore != null && parameters.LoanTypesToIgnore.Length > 0)
            {
                debugString.AppendLine();
                debugString.AppendLine("IgnoreLoanTypes");
                foreach (var s in parameters.LoanTypesToIgnore)
                {
                    debugString.AppendLine("   " + s);
                }
            }
            if (parameters.ProductTypesToInclude != null && parameters.ProductTypesToInclude.Length > 0)
            {
                debugString.AppendLine();
                debugString.AppendLine("IncludeProductTypes");
                foreach (var s in parameters.ProductTypesToInclude)
                {
                    debugString.AppendLine("   " + s);
                }
            }
            if (parameters.ProductTypesToConsider != null && parameters.ProductTypesToConsider.Length > 0)
            {
                debugString.AppendLine();
                debugString.AppendLine("onlyIncludeProductTypes");
                foreach (var s in parameters.ProductTypesToConsider)
                {
                    debugString.AppendLine("   " + s);
                }
            }
            if (parameters.RequiredProductCodes!= null && parameters.RequiredProductCodes.Any())
            {
                debugString.AppendLine();
                debugString.AppendLine("RequiredProductCodes");
                foreach (var s in parameters.RequiredProductCodes)
                {
                    debugString.AppendLine("   " + s);
                }
            }

            PriceGroup priceGroup = null;

            HashSet<Guid> includeSet = new HashSet<Guid>();
            if (priceGroupId != Guid.Empty)
            {
                priceGroup = PriceGroup.RetrieveByID(priceGroupId, brokerId);

                if (priceGroup.ExternalPriceGroupEnabled == false)
                {
                    return lpSet;
                }
                foreach (var o in PriceGroupProduct.ListByPriceGroupId(brokerId, priceGroupId))
                {
                    if (o.IsValid)
                    {
                        includeSet.Add(o.LpTemplateId);
                    }
                }
            }
            Tools.LogInfo(debugString.ToString());
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReaderWithRetries(DataSrc.LpeSrc, "ListNonMasterLoanProgramsByBrokerIDToRunLpe", 3, (SqlParameter[])pars.ToArray(typeof(SqlParameter))))
            {
                while (reader.Read())
                {
                    string lLpProductType = (string)reader["lLpProductType"];
                    string lLpInvestorNm = (string)reader["lLpInvestorNm"];
                    string productCode = (string)reader["ProductCode"];
                    Guid lLpTemplateId = (Guid)reader["lLpTemplateId"];
                    E_sLT lLpLoanType = (E_sLT)reader["lLT"];

                    if (priceGroupId != Guid.Empty)
                    {
                        // 7/24/2014 dd - Must check to see if lLpTemplateId is include in the price group.
                        if (includeSet.Contains(lLpTemplateId) == false)
                        {
                            continue;
                        }
                    }
                    // 5/26/2009 dd - Check to see if investor name and product code contains in user manual exclude list.
                    if (IsContains(manualExcludeProductList, lLpInvestorNm, productCode))
                    {
                        continue; // skip this product.
                    }

                    if (parameters.ProductTypesToConsider.Length > 0)
                    {
                        if (false == parameters.ProductTypesToConsider.Contains(lLpProductType, StringComparer.OrdinalIgnoreCase))
                        {
                            continue; // skip all products that aren't in the "only include product type filter", if it exists.
                        }
                    }
                    if (string.IsNullOrEmpty(parameters.MatchingProductType))
                    {
                        if (parameters.ProductTypesToIgnore != null && parameters.ProductTypesToIgnore.Contains(lLpProductType, StringComparer.OrdinalIgnoreCase))
                        {
                            continue; // skip this product.
                        }
                        if (parameters.ProductTypesToInclude != null && parameters.ProductTypesToInclude.Contains(lLpProductType, StringComparer.OrdinalIgnoreCase))
                        {
                            // don't skip this product.
                        }
                        else
                        {
                            if (parameters.LoanTypesToIgnore != null && parameters.LoanTypesToIgnore.Contains(lLpLoanType))
                            {
                                continue; // skip this product
                            }
                        }
                    }
                    else
                    {
                        // include all results when of sql query when it has a nonempty onlyIncludeLpProductType
                    }

                    // Don't filter by product code if there are no required product codes set.
                    if (parameters.RequiredProductCodes != null &&
                        parameters.RequiredProductCodes.Count > 0 &&
                        !parameters.RequiredProductCodes.Contains(productCode, StringComparer.OrdinalIgnoreCase))
                    {
                        // Don't include this loan program if it does not contain the correct product code.
                        continue;
                    }

                    if (null != productPairingTable)
                    {
                        int pairingProductIdsLength = (int)reader["PairingProductIdsLength"];

                        if (pairingProductIdsLength == 0)
                        {
                            // 7/19/2007 dd - Only Proceed with pairing at product level IF AND ONLY IF there is no explicit pairing at LP level.
                            string firstInvestorName = (string)reader["FirstInvestorName"];
                            string firstProductCode = (string)reader["FirstProductCode"];
                            if (!productPairingTable.IsAllowPairing(firstInvestorName, firstProductCode, lLpInvestorNm, productCode))
                            {
                                continue; // Skip this product.
                            }
                        }
                    }

                    lpSet.Add(lLpTemplateId, lLpInvestorNm, productCode, preserveProductCode, lLpLoanType);
                }
            }

            return lpSet;
        }

        public static bool IsContains(List<InvestorProductItem> list, string investorName, string productCode)
        {
            bool ret = false;

            foreach (var o in list)
            {
                if (o.InvestorName.Equals(investorName, StringComparison.OrdinalIgnoreCase) && o.ProductCode.Equals(productCode, StringComparison.OrdinalIgnoreCase))
                {
                    ret = true;
                    break;
                }
            }
            return ret;
        }

        public static List<InvestorProductItem> GetManualExcludeProductList(Guid brokerId, Guid lockPolicyId)
        {
            // 5/26/2009 dd - OPM 27772 - Return a list of user manual exclude product list.
            List<InvestorProductItem> list = new List<InvestorProductItem>();

            var manualDisabledList = InvestorProductManager.ListManualDisabledInvestorProductItem(brokerId, lockPolicyId);
            foreach (InvestorProductItem o in manualDisabledList)
            {
                // 6/23/2009 dd - If user choose DisableUntilNextLpeUpdate then we handle in CApplicantPrice. See case 31884.
                if (o.IsHiddenFromResult && o.DisableStatus == E_InvestorProductDisabledStatus.Disabled)
                {
                    list.Add(o); // 5/26/2009 dd - Only add the product that is marked as hidden from result.
                }
            }

            
            return list;
        }

    }
}