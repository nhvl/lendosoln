/// Author: David Dao

using System;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice
{
    public class CUnderwritingUnavailableException : CBaseException
    {
        public override string EmailSubjectCode 
        {
            get { return "UNDERWRITING_UNAVAILABLE"; }
        }
        public CUnderwritingUnavailableException( string devMsg )
            : base ( LpeDataProvider.s_USER_ERR_MSG, devMsg  )
        {}
    }
}
