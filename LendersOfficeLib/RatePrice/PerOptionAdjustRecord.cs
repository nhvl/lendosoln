﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using Toolbox;
using DataAccess;

namespace LendersOfficeApp.los.RatePrice
{
    // Helping class to help apply the logic in case 87119
    public class PerOptionAdjustRecord
    {
        private PriceAdjustRecord m_thisRatePricing;
        private Dictionary<string, string> m_rateOptionAdjusts = new Dictionary<string, string>();
        private bool m_haveMaxIterationStip = false;

        private Dictionary<Guid, List<CApplicantRateOption>> m_displayedAdjusts = new Dictionary<Guid, List<CApplicantRateOption>>();
        private Dictionary<Guid, List<CApplicantRateOption>> m_hiddenAdjusts = new Dictionary<Guid, List<CApplicantRateOption>>();

        // SAEs want a debug entry for each visble rate on the cert
        private Dictionary<CApplicantRateOption, string> m_debugCache = new Dictionary<CApplicantRateOption, string>();

        private HashSet<decimal> m_snappedDownOptions = new HashSet<decimal>();

        private int m_iterations = 0;

        private decimal? m_programMaxYsp = null;
        public decimal? ProgramMaxYsp
        {
            get { return m_programMaxYsp; }
        }

        public string DebugLog
        {
            get
            {
                StringBuilder ret = new StringBuilder();
                ret.Append("Per Rate Evaluation (Iterations: " + m_iterations
                    + ", ProgramMaxYsp:" + (m_programMaxYsp.HasValue ? m_programMaxYsp.Value.ToString() : "NONE") + " )<br />");
                foreach (CApplicantRateOption option in m_debugCache.Keys.Reverse())
                {
                    ret.Append(m_debugCache[option] + "<br />");
                }

                return ret.ToString();
            }
        }

        public PriceAdjustRecord CurrentRateAdjust
        {
            get { return m_thisRatePricing; }
            set
            {
                m_thisRatePricing = value;
            }
        }

        private Dictionary<Guid, CAdjustItem> ruleMap = new Dictionary<Guid, CAdjustItem>();

        // Add this adjustment/rate option combo to our list
        // and add it to the list if new
        public void AddAdjustment(CAdjustItem adj, CApplicantRateOption option, bool isDisplayed, List<CAdjustItem> adjCollection, Guid ruleId)
        {
            if (!ruleMap.ContainsKey(ruleId)) ruleMap.Add(ruleId, adj);

            var adjList = isDisplayed ? m_displayedAdjusts : m_hiddenAdjusts;

            if (!adjList.Keys.Contains(ruleId))
            {
                adjList[ruleId] = new List<CApplicantRateOption>(new CApplicantRateOption[] { option });

                adjCollection.Add(adj);
            }
            else
            {
                adjList[ruleId].Add(option);
            }
        }

        public void AddFilteredOptionDebug(string debugStr, int iterations)
        {
            // Option was filtered out, but SAEs want to see it per OPM 94323.
            m_debugCache.Add(new CApplicantRateOption(Guid.Empty, debugStr, "", "", "", "", "", 0, "", "", "", "", "", "", "", "", "", 0, 0, 0), debugStr + "[OUT]");

            if (iterations > m_iterations)
                m_iterations = iterations;
        }

        public void AddOption(CApplicantRateOption option, string debugStr, int iterations)
        {
            m_debugCache.Add(option, debugStr);

            if (iterations > m_iterations)
                m_iterations = iterations;
        }

        public void RemoveOption(CApplicantRateOption option, bool maxYspFiltered = true)
        {
            // Due to MaxYSP filtering, this option was removed.
            // To keep its adjustments from being added to the list,
            // it must be removed.

            foreach (var adjusts in m_displayedAdjusts.Union(m_hiddenAdjusts))
            {
                adjusts.Value.Remove(option);
            }

            if (maxYspFiltered && m_debugCache.ContainsKey(option))
            {
                m_debugCache[option] += "[OUT]";
            }

        }

        public bool IsAdjustmentReferencedByOption(CAdjustItem adjustment)
        {
           foreach (var adj in ruleMap.Values)
            {
                if ( adj.Description == adjustment.Description
                    && adj.Fee == adjustment.Fee)
                {
                    return true;
                }
            }
            return false;
        }

        // 91390. We have to do this last when we have all adjustments:
        // assign codes to the adjustments using visible first
        // Also, if an adjustment applies to every visible rate, it should become a regular adjustment
        // There is a little string concatination here.  Overhead of StringBuilder would not be overcome using it here.

        public void FinalizeAdjustmentList(List<CAdjustItem> displayedAdj, List<CAdjustItem> hiddenAdj, ArrayList rateOptions)
        {
            char code = 'a';
            foreach (Guid adjRuleId in m_displayedAdjusts.Keys)
            {
                CAdjustItem item = ruleMap[adjRuleId];

                List<CApplicantRateOption> optionsReceivingThisAdjust = m_displayedAdjusts[adjRuleId];

                if (optionsReceivingThisAdjust.Count == 0)
                {
                    // 94381 - This adjustment applies to no rates.  Do not add.
                    displayedAdj.Remove(item);
                    continue;
                }

                if (rateOptions.Count == optionsReceivingThisAdjust.Count)
                {
                    // This adjustment applies to every visible rate option => Treat it as regular adjustment
                    item.IsPerRateAdjForAllRateOptions = true;
                    continue;
                }

                if (string.IsNullOrEmpty(item.OptionCode))
                    item.OptionCode = code++.ToString();

                foreach (CApplicantRateOption option in optionsReceivingThisAdjust)
                {
                    //option.PerOptionAdjStr += item.OptionCode;
                    option.PerOptionAdjStr += option.PerOptionAdjStr == "" ? item.OptionCode : "," + item.OptionCode;
                }
            }

            foreach (Guid adjRuleId in m_hiddenAdjusts.Keys)
            {
                CAdjustItem item = ruleMap[adjRuleId];

                List<CApplicantRateOption> optionsReceivingThisAdjust = m_hiddenAdjusts[adjRuleId];

                if (optionsReceivingThisAdjust.Count == 0)
                {
                    // 94381 - This adjustment applies to no rates.  Do not add.
                    hiddenAdj.Remove(item);
                    continue;
                }

                if (rateOptions.Count == optionsReceivingThisAdjust.Count)
                {
                    // This adjustment applies to every visible rate option => Treat it as regular adjustment
                    item.IsPerRateAdjForAllRateOptions = true;
                    continue;
                }

                if (string.IsNullOrEmpty(item.OptionCode))
                    item.OptionCode = code++.ToString();

                foreach (CApplicantRateOption option in m_hiddenAdjusts[adjRuleId])
                {

                    if (option.PerOptionAdjHiddenStr != string.Empty || option.PerOptionAdjStr != string.Empty)
                    {
                        option.PerOptionAdjHiddenStr += "," + item.OptionCode;
                    }
                    else
                    {
                        option.PerOptionAdjHiddenStr += item.OptionCode;
                    }

                }
            }

            if (code > 'z')
            {
                // SAEs say this should be almost impossible to do, but we cannot allow.
                throw new CBaseException(ErrorMessages.Generic, "Too many rate-specific adjustments.  We are out of letters");
            }
        }

        // Add the max iteration violation only once.
        public void AddIterationStip(CSortedListOfGroups stipCollection)
        {
            if (m_haveMaxIterationStip == false)
            {

                stipCollection.Add(
                        "WARNING"
                        , new CStipulation(
                            "PRICING MAY BE OFF FOR CERTAIN NOTE RATES DUE TO CIRCULAR DEPENDENCY."
                            , ""));
                m_haveMaxIterationStip = true;
            }
        }

        public void AddSnapDown(decimal snapdown, decimal rate)
        {
            if (m_programMaxYsp.HasValue == false || m_programMaxYsp.Value > snapdown)
            {
                m_programMaxYsp = snapdown;
                m_snappedDownOptions = new HashSet<decimal>(new decimal[] { rate }); 
            }
            else
            {
                if ( m_programMaxYsp.Value == snapdown )
                    m_snappedDownOptions.Add(rate); 
            }
        }

        public bool IsSnappedDownByProgramMaxYsp(decimal rate)
        {
            return m_snappedDownOptions.Contains(rate);
        }

    }
}
