using System;
using System.Collections;
using System.Text;
using System.Xml;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.CreditReport;
using System.Collections.Generic;

namespace LendersOfficeApp.los.RatePrice
{
    //CSymbolEntry
    public class CaseInsensitiveHashtable : Hashtable
    {
        public CaseInsensitiveHashtable() : base(StringComparer.CurrentCultureIgnoreCase)
        {
        }

        public CaseInsensitiveHashtable(int size) : base(size, StringComparer.CurrentCultureIgnoreCase)
        {
        }
    }

    // when adding or removing a method in this class, 
    // please updating the mapping string [,] Keyword2FieldName
    //
    // When modifying keywords, or their methods/properties
    // please examine the output of Dynamic Call method ReportVariantKeywords in LOAdmin
    // It will check which keywords are currently dependent on variant fields.
    // We must keep the variant lists in LpeKeywordUtil up to date.
    // See OPM 1353
    //
    // When adding new keyword, add dependency to sSymbolTableForPriceRule rather than sfRunLpe.
    // This is because there are other ways rules might need to be run.
    
    public class LoanDataWrapper
    {
        static LoanDataWrapper()
        {
            try
            {
                VerifyMapping();
            }
            catch( Exception exec )
            {
                Tools.LogErrorWithCriticalTracking( exec.ToString() );
            }
        }

        private CPageBase m_loanData; // New field needs to be added to this calculated field in LoanFileFields.cs: sSymbolTableForPriceRule
        ICreditReport creditData = null;

        CAppBase appData = null;

        void Init(CPageBase loanData, int appIndex)
        {
            m_loanData = loanData;
            if( null != m_loanData )
            {
                appData    = m_loanData.GetAppData(appIndex);
                creditData = appData.CreditReportData.Value;
            }
            else
            {
                appData    = null;
                creditData = null;
            }

        }

        public LoanDataWrapper(CPageBase loanData, int appIndex)
        {
            Init(loanData, appIndex);
        }

        #region wrapper properties
        /// ////////////////////////////////////////////////////////////////////
        // wrapper properties
        /// ////////////////////////////////////////////////////////////////////

        // Note, per OPM 50487 avoid using a default value of 0 or 1 for keyword
        // validation.  When app or loandata is null, it means we are in validation
        // mode (upload or rule edit). A single keyword without an operator should
        // not evaluate to 1 or 0.

        // For OPM 477479.  We are caching some keyword values that show as expensive
        // when calculated.  The cache is only used per-rate in PMI execution currently.
        private bool useCache = false;
        public void StartCache()
        {
            useCache = true;
            ClearCache();
        }

        public void EndCache()
        {
            useCache = false;
            ClearCache();
        }
        private void ClearCache()
        {
            x_Score2 = null;
            x_Score2Soft = null;
            x_HasCobor = null;
            x_HasNonOccupantCoborrower = null;
            x_DebtRatioBottom = null;
            x_IsBaseLAMTConformingLAmt = null;
            x_IsBaseLAMTHighBalanceConformingLAMT = null;
            x_IsConformingLAmt = null;
            x_LTV = null;
            x_CLTV = null;
            x_HCLTV = null;
        }

        private decimal? x_Score2;
        private decimal? x_Score2Soft;
        private decimal? x_HasCobor;
        private decimal? x_HasNonOccupantCoborrower;
        private decimal? x_DebtRatioBottom;
        private decimal? x_IsBaseLAMTConformingLAmt;
        private decimal? x_IsBaseLAMTHighBalanceConformingLAMT;
        private decimal? x_IsConformingLAmt;
        private decimal? x_LTV;
        private decimal? x_CLTV;
        private decimal? x_HCLTV;

        public decimal GetScore2()
        {
            try
            {
                if (null == m_loanData)
                {
                    return 610;
                }

                if (useCache)
                {
                    if (x_Score2.HasValue)
                    {
                        return x_Score2.Value;
                    }
                
                    x_Score2 = m_loanData.sCreditScoreType2;
                    return x_Score2.Value;
                }

                return m_loanData.sCreditScoreType2; 
            }
            catch
            {
                if( m_loanData.IsRunningPricingEngine )
                    return 0;
                throw;
            }
        }

        public decimal GetScore1()                             
        { 
            try
            {
                return (null==m_loanData) ? 620 : m_loanData.sCreditScoreType1;
            }
            catch
            {
                if( m_loanData.IsRunningPricingEngine )
                    return 0;
                throw;
            }

        }


        public decimal GetLienPosition()                       
        { 
            return (int)( (null==m_loanData) ? E_sLienPosT.Second : m_loanData.sLienPosT ) ;
        }

        public decimal GetPropertyValue()                       
        {
            return (null == m_loanData) ? 350000 : m_loanData.sPropertyValueKeyword;
        }



        public decimal GetCashoutAmt()                         
        { 
            if( null==m_loanData) 
                return 10000;

            return decimal.Parse( m_loanData.sProdCashoutAmt_rep.Replace("$","") );
        }

        public decimal GetIs1stTimeHomeBuyer()                 
        { 
            return (null==m_loanData) ? 3 : (m_loanData.sHas1stTimeBuyer ? 1 : 0); 
        }

        public decimal Get2ndWageScore()                       
        { 
            try
            {
                return (null==m_loanData) ? 620 : m_loanData.sCreditScoreType4;
            }
            catch
            {
                if( m_loanData.IsRunningPricingEngine )
                    return 0;
                throw;
            }

        }

        public decimal GetNumberOfUnits()                      
        { 
            return (null==m_loanData) ? 2 : m_loanData.sProdNumberOfUnits; 
        }

        public decimal GetMIOption()                           
        { 
            return (int)((null == m_loanData) ? E_sProdMIOptionT.NoPmi : m_loanData.sProdMIOptionT);
        }

        public decimal GetUnits()                              
        { 
            return (null==m_loanData) ? 2 : m_loanData.sProdNumberOfUnits;         
        }

        public decimal GetScore2Soft()                         
        {
            try
            {
                if (null == m_loanData)
                {
                    return 610;
                }

                if (useCache)
                {
                    if (x_Score2Soft.HasValue)
                    {
                        return x_Score2Soft.Value;
                    }

                    x_Score2Soft = m_loanData.sCreditScoreType2Soft;
                    return x_Score2Soft.Value;
                }

                return m_loanData.sCreditScoreType2Soft;
            }
            catch
            {
                if (m_loanData.IsRunningPricingEngine)
                    return 0;
                throw;
            }
        }

        public decimal GetBkHasUnpaidAndNondischargedUnknownTypeBk()    
        { 
            return (null==creditData) ? 3 : ( creditData.HasUnpaidAndNondischargedUnknownTypeBk ? 1 : 0);
        }

        public decimal GetTradeCount()                         
        { 
            return (null==creditData) ? 3 : creditData.TradeCount;
        }

        public decimal GetSelfEmployed()
        {
            return (null == appData) ? 3 : (appData.aIsSelfEmployed ? 1 : 0);  
        }

        public decimal GetIsInCreditCounseling()               
        { 
            return (null==creditData) ? 3 : (creditData.IsInCreditCounseling ? 1 : 0); 
        }

        public decimal GetBkHasUnpaidAndNondischarged13Bk()    
        { 
            return (null==creditData) ? 3 : (creditData.HasUnpaidAndNondischarged13Bk ? 1 : 0 );
        }

        public decimal GetPpmtPenaltyMonths()                  
        { 
            return (null==m_loanData) ? 10 : m_loanData.sProdPpmtPenaltyMonKeyword; 
        }

        public decimal GetInterestOnlyOtherLoan()              
        { 
            return (null==m_loanData) ? 3 : (m_loanData.sIsIOnlyForSubFin ? 1 : 0); 
        }

        public decimal GetTradesCountNonMajorDerog()           
        { 
            return (null==creditData) ? 3 : creditData.TradesCountNonMajorDerog; 
        }

        public decimal GetQcltv()                              
        { 
            return (null==m_loanData) ? 90	: m_loanData.sCltvLpeQual;         
        }

        public decimal GetQhcltv()
        {
            return (null==m_loanData) ? 90	: m_loanData.sHcltvLpeQual;         
        }

        public decimal GetSpCountyFips()
        {
            // 6059 is Orange County, CA.
            return (null==m_loanData) ? 6059 : m_loanData.sSpCountyFips;
        }

        public decimal GetDocType()                            
        { 
            return (int)((null==m_loanData) ? E_sProdDocT.Full : m_loanData.sProdDocT );       
        }

        public decimal GetPropertyType()                       
        { 
            return (int)((null==m_loanData) ? E_sProdSpT.SFR : m_loanData.sProdSpT);  
        }

        public decimal GetIsMhAdvantageHome()
        {
            return (int)(this.m_loanData?.sHomeIsMhAdvantageTri ?? E_TriState.Blank);
        }

        public decimal GetIsSameInvestorFor8020()              
        { 
            return (null==m_loanData) ? 3 : (m_loanData.sLp8020HaveSameInvestor ? 1 : 0); 
        }

        public decimal GetIsStandAlone2ndLien() 
        {
            return (null == m_loanData) ? 3 : (m_loanData.sIsStandAlone2ndLien ? 1 : 0);
        }

        public decimal GetIsPpAllowedInSubjPropState()         
        { 
            return ( null == m_loanData || m_loanData.IsSpStateAllowPp ) ? 1 : 0; 
        }

        public decimal GetLTV()
        {
            if (null == m_loanData)
            {
                return 80;
            }

            if (useCache)
            {
                if (x_LTV.HasValue)
                {
                    return x_LTV.Value;
                }

                x_LTV = m_loanData.sPricingLtvR;
                return x_LTV.Value;
            }

            return m_loanData.sPricingLtvR;
        }

        public decimal GetFannieLTV()
        {
            return (null == m_loanData) ? 80 : m_loanData.sFannieLtvR;
        }

        public decimal GetLeadSourceId()
        {
            return (null == m_loanData) ? 3 : m_loanData.sLeadSrcId; 
        }

        public decimal GetAvailReserveMonths()                 
        { 
            return (null==m_loanData) ? 3 : m_loanData.sProdAvailReserveMonthsKeyword; 
        }

        public decimal GetArmFixedTerm()                       
        { 
            return (null==m_loanData) ? 24 : m_loanData.sRAdj1stCapMon;  
        }

        public decimal GetMinScoreCount()                      
        { 
            return (null==m_loanData) ? 2 : m_loanData.sMinimumBorrowerCreditScoresCount; 
        }

        public decimal GetIsConformingLAmt()
        {
            if (null == m_loanData)
            {
                return 3;
            }

            if (useCache)
            {
                if (x_IsConformingLAmt.HasValue)
                {
                    return x_IsConformingLAmt.Value;
                }

                x_IsConformingLAmt = (m_loanData.sIsConformingLAmt ? 1 : 0);
                return x_IsConformingLAmt.Value;
            }

            return (m_loanData.sIsConformingLAmt ? 1 : 0);
        }

        public decimal GetIsHighBalanceConformingLAmt()
        {
            return (null == m_loanData) ? 3 : (m_loanData.sIsHighBalanceConformingLAmt ? 1 : 0);
        }

        public decimal GetIsBaseLAmtConformingLAmt()
        {
            if (null == m_loanData)
            {
                return 3;
            }

            if (useCache)
            {
                if (x_IsBaseLAMTConformingLAmt.HasValue)
                {
                    return x_IsBaseLAMTConformingLAmt.Value;
                }

                x_IsBaseLAMTConformingLAmt = (m_loanData.sIsBaseLAmtConformingLAmt ? 1 : 0);
                return x_IsBaseLAMTConformingLAmt.Value;
            }

            return (m_loanData.sIsBaseLAmtConformingLAmt ? 1 : 0);
        }

        public decimal GetIsBaseLAmtHighBalanceConformingLAmt()
        {
            if (null == m_loanData)
            {
                return 3;
            }

            if (useCache)
            {
                if (x_IsBaseLAMTHighBalanceConformingLAMT.HasValue)
                {
                    return x_IsBaseLAMTHighBalanceConformingLAMT.Value;
                }

                x_IsBaseLAMTHighBalanceConformingLAMT = (m_loanData.sIsBaseLAmtHighBalanceConformingLAmt ? 1 : 0);
                return x_IsBaseLAMTHighBalanceConformingLAMT.Value;
            }

            return (m_loanData.sIsBaseLAmtHighBalanceConformingLAmt ? 1 : 0);
        }

        public decimal GetAmortizationType()                   
        { 
            return (int)((null==m_loanData) ? E_sFinMethT.Fixed : m_loanData.sFinMethT ); 
        }

        public decimal GetTotalScoreFhaProduct()
        {
            return (int)((null == m_loanData) ? E_sTotalScoreFhaProductT.LeaveBlank : m_loanData.sTotalScoreFhaProductT);
        }

        public decimal GetLoanFileType()
        {
            if (this.m_loanData == null)
            {
                return (int)E_sLoanFileT.Loan;
            }

            return (int)this.m_loanData.sLoanFileT;
        }

        public decimal GetIsNegAmortOtherLien()                
        {
            return (null==m_loanData) ? 3 : (m_loanData.sLpIsNegAmortOtherLien ?1 : 0); 
        }


        public decimal GetLockDays()                           
        { 
            return (null==m_loanData) ? 30 : m_loanData.sRLckdDaysFromInvestor;      
        }

        public decimal GetLockDaysUnadjusted()
        {
            return (null == m_loanData) ? 30 : m_loanData.sProdRLckdDays;
        }

        public decimal GetPurposeType()                        
        { 
            return (int)((null==m_loanData) ? E_sLPurposeT.Purchase : m_loanData.sLPurposeT);   
        }

        public decimal GetDebtRatioBottom()
        {
            if (null == m_loanData)
            {
                return 50;
            }

            if (useCache)
            {
                if (x_DebtRatioBottom.HasValue)
                {
                    return x_DebtRatioBottom.Value;
                }

                x_DebtRatioBottom = decimal.Parse(m_loanData.sQualBottomR_rep.Replace("%", ""));
                return x_DebtRatioBottom.Value;
            }

            return decimal.Parse(m_loanData.sQualBottomR_rep.Replace("%", ""));
        }


        public decimal GetIncomeMonthly()                      
        { 
            return (null==m_loanData) ? 300000	: m_loanData.sPrimAppTotNonspI; 
        }

        public decimal GetIsNonwarrantableProj()               
        { 
            return (null==m_loanData) ? 3 : (m_loanData.sProdIsNonwarrantableProj ? 1 : 0); 
        }

        public decimal GetBkHasUnpaidAndNondischarged12Bk()    
        { 
            return (null==creditData) ? 3 : (creditData.HasUnpaidAndNondischarged12Bk ? 1 : 0); 
        }

        public decimal GetPropertyStructType()                 
        { 
            return (int)((null==m_loanData) ? E_sProdSpStructureT.Attached : m_loanData.sProdSpStructureT ); 
        }

        public decimal GetCreditReportIsOnFile()               
        { 
            return (null==appData) ? 3 : (appData.aIsCreditReportOnFile ? 1 : 0); 
        }

        public decimal GetIsCreditScoresDifferentWithCreditReport() 
        {
            return (null == appData) ? 3 : (appData.aIsCreditScoresDifferentWithCreditReport ? 1 : 0);
        }

        public decimal GetState()                              
        { 
            return int.Parse( CSymbolTable.StateValue( (null==m_loanData) ? "CA" : m_loanData.sSpState )  );         
        }

        public decimal GetPropertyPurpose()                    
        { 
            return (int)((null==m_loanData) ? E_sOccT.PrimaryResidence : m_loanData.sOccT); 
        }

        public decimal GetBkHasUnpaidAndNondischarged()        
        { 
            return (null==creditData) ? 3 : (creditData.HasUnpaidAndNondischargedAnyBk ? 1 : 0); 
        }

		public int GetHasMortgageIncludedInBKWithin(int nMonths)  //av 25329
		{
			if ( creditData == null ) 
			{
				return 3; 
			}
			return creditData.HasMortgageIncludedInBKWithin( nMonths)?1:0; 
		}
		public int GetHighestUnpaidBalOfMedicalCollectionWithin(int nMonths) 
		{
			if ( creditData == null ) 
			{
				return 3;
			}
			return creditData.GetHighestUnpaidBalOfMedicalCollectionWithin(nMonths);
		}
        public decimal GetDebtRatioTop()                       
        { 
            return (null==m_loanData) ? 50 : decimal.Parse( m_loanData.sQualTopR_rep.Replace("%", "") );
        }

        public decimal GetCLTV()
        {
            if (null == m_loanData)
            {
                return 80;
            }

            if (useCache)
            {
                if (x_CLTV.HasValue)
                {
                    return x_CLTV.Value;
                }

                x_CLTV = m_loanData.sPricingCltvR;
                return x_CLTV.Value;
            }

            return m_loanData.sPricingCltvR;
        }
        public decimal GetFannieCLTV()
        {
            return (null == m_loanData) ? 80 : m_loanData.sFannieCltvR;
        }

        public decimal GetFannieHCLTV()
        {
            return (null == m_loanData) ? 80 : m_loanData.sFannieHcltvR;
        }

        // update these to the correct renovation LTV values after they are completed
        public decimal GetRenovationLTV()
        {
            return (null == m_loanData) ? 80 : m_loanData.sLtvR;
        }

        public decimal GetRenovationCLTV()
        {
            return (null == m_loanData) ? 80 : m_loanData.sCltvR;
        }

        public decimal GetRenovationHCLTV()
        {
            return (null == m_loanData) ? 80 : m_loanData.sHcltvR;
        }

        public decimal GetFha203KType()
        {
            return (int)((null == m_loanData) ? E_sFHA203kType.NA : m_loanData.sFHA203kType);
        }

        public decimal GetHasTaxOpenTaxLien()                  
        { 
            return (null==creditData) ? 3 : (creditData.HasTaxOpenTaxLien ? 1 : 0); 
        }

        public decimal GetCostConstrRepairsRehab()
        {
            return (null == m_loanData) ? 3 : m_loanData.sCostConstrRepairsRehab;
        }

        public decimal GetArchitectEngineerFee()
        {
            return (null == m_loanData) ? 3 : m_loanData.sArchitectEngineerFee;
        }

        public decimal GetConsultantFee()
        {
            return (null == m_loanData) ? 3 : m_loanData.sConsultantFee;
        }

        public decimal GetRenovationConstrInspectFee()
        {
            return (null == m_loanData) ? 3 : m_loanData.sRenovationConstrInspectFee;
        }

        public decimal GetTitleUpdateFee()
        {
            return (null == m_loanData) ? 3 : m_loanData.sTitleUpdateFee;
        }

        public decimal GetPermitFee()
        {
            return (null == m_loanData) ? 3 : m_loanData.sPermitFee;
        }

        public decimal GetFeasibilityStudyFee()
        {
            return (null == m_loanData) ? 3 : m_loanData.sFeasibilityStudyFee;
        }

        public decimal GetRepairImprovementCostFee()
        {
            return (null == m_loanData) ? 3 : m_loanData.sRepairImprovementCostFee;
        }

        public decimal GetFinanceContingencyReserve()
        {
            return (null == m_loanData) ? 3 : m_loanData.sFinanceContingencyRsrv;
        }

        public decimal GetQLAmt()                              
        { 
            return (null==m_loanData) ? 350000	: m_loanData.sLAmtCalcLpeQual;         
        }

        public decimal GetDue()                                
        { 
            return (null==m_loanData) ? 180 : m_loanData.sDue;           
        }

        public decimal GetQscore()                             
        { 
            return (null==m_loanData) ? 720	: m_loanData.sCreditScoreLpeQual;        
        }

        public decimal GetIsCondotel()                         
        { 
            return (null==m_loanData) ? 3 : (m_loanData.sProdIsCondotel ? 1 : 0);    
        }

        public decimal GetQltv()                               
        { 
            return (null==m_loanData) ? 80 : m_loanData.sLtvLpeQual;          
        }

        public decimal GetThirdPartyUwResultType()             
        { 
            return (int)((null==m_loanData) ? E_sProd3rdPartyUwResultT.DU_ApproveIneligible : m_loanData.sProd3rdPartyUwResultT); 
        }

        public static decimal GetThirdPartyUwProcessingType() 
        {
            return 999; // Return Invalid value.
        }

        public decimal GetForeclosureUnpaidCount()             
        { 
            return (null==creditData) ? 2 : creditData.ForeclosureUnpaidCount; 
        }

        public decimal GetIsTexas50a6Loan()
        {
            return (null == m_loanData) ? 3 : (m_loanData.sProdIsTexas50a6Loan ? 1 : 0);
        }

        public decimal GetIsPriorLoanTexas50a6Loan()
        {
            return (null == m_loanData) ? 3 : (m_loanData.sPreviousLoanIsTexas50a6Loan ? 1 : 0);
        }

        public decimal GetImpound()                            
        {
            return (null==m_loanData) ? 3 : (m_loanData.sProdImpound ? 1 : 0);       
        }

        public decimal GetInterestOnly()                       
        { 
            return (null==m_loanData) ? 3 : (m_loanData.sIsIOnly ? 1 : 0);  
        }

        public decimal GetPropertyInRuralArea()                
        { 
            return (null==m_loanData) ? 3 : m_loanData.sProdIsSpInRuralArea ? 1 : 0; 
        }

        public decimal GetLOCCompanyId()
        {
            return (null == m_loanData) ? 1 : m_loanData.sEmployeeLoanRepOriginatingCompanyId_FirstFourDigits;
        }

        public decimal GetHasMortgage()                        
        { 
            return (null==m_loanData) ? 3 : (appData.aHasMortgageLia ? 1 : 0);   
        }

        public decimal GetBkHasUnpaidAndNondischarged7Bk()     
        { 
            return (null==creditData) ? 3 : (creditData.HasUnpaidAndNondischarged7Bk ? 1 : 0); 
        }

        public decimal GetBkHasUnpaidAndNondischarged11Bk()    
        { 
            return (null==creditData) ? 3 : (creditData.HasUnpaidAndNondischarged11Bk ? 1 : 0); 
        }

        public decimal GetDerogatoryAmountTotal()              
        { 
            return (null==creditData) ? 2000 : creditData.DerogatoryAmountTotal; 
        }

        public decimal GetBkMultipleType()                     
        { 
            return (int)((null==creditData) ? E_BkMultipleT.Multiple : creditData.BkMultipleType ); 
        }

        public decimal GetAmortTypeOtherLoan()                 
        { 
            return (int)((null==m_loanData) ? E_sFinMethT.Fixed : m_loanData.sOtherLFinMethT  ); 
        }

        public decimal GetTerm()                               
        { 
            return (null==m_loanData) ? 3 : m_loanData.sTerm;          
        }

        public decimal GetCitizenshipType()
        {
            return (int)((null == appData) ? E_aProdCitizenT.USCitizen : appData.aCitizenshipType );
        }

        public decimal GetTotBalOfMedCollectionsUnpaid()       
        { 
            return (null==creditData) ? 3 : creditData.TotBalOfMedCollectionsUnpaid; 
        }

        public decimal GetLoanAmount()                         
        {
            return (null==m_loanData) ? 300000	: m_loanData.sLAmtCalc;    
        }

        public decimal GetLtv1stLien()                         
        { 
            return (null==m_loanData) ? 80	: m_loanData.sLtv1stLien;    
        }

        public decimal GetPrimaryWageEarnerCreditScoresCount()    
        { 
            return (null==m_loanData) ? 2 : m_loanData.sPrimaryWageEarnerCreditScoresCount; 
        }

        public decimal GetLoanAmount1stLien()                  
        { 
            return (null==m_loanData) ? 100000	: m_loanData.sLAmt1stLien; 
        }

        public decimal GetLoanAmount2ndLien() 
        {
            return (null == m_loanData) ? 100000 : m_loanData.sLAmt2ndLien;
        }

        public decimal GetNumberOfCondoStories()               
        { 
            return (null==m_loanData) ? 5 : m_loanData.sProdCondoStories; 
        }

        public decimal GetLAmt()                               
        { 
            return (null==m_loanData) ? 300000	: m_loanData.sLAmtCalc;          
        }
        public decimal GetTLAmt()
        {
            return (null == m_loanData) ? 300000 : m_loanData.sFinalLAmt;
        }

        public decimal GetHasHousingHistory() 
        {
            return (null == m_loanData) ? 1 : (m_loanData.sProdHasHousingHistory ? 1 : 0);
        }

        public decimal GetHasCobor()
        {
            if (null == m_loanData)
            {
                return 3;
            }

            if (useCache)
            {
                if (x_HasCobor.HasValue)
                {
                    return x_HasCobor.Value;
                }

                x_HasCobor = (m_loanData.sHasCoborrower ? 1 : 0);
                return x_HasCobor.Value;
            }

            return (m_loanData.sHasCoborrower ? 1 : 0);
        }

        public decimal GetResidualIncomeMonthly() 
        {
            return (null == m_loanData) ? 3 : m_loanData.sProdEstimatedResidualI;
        }
        public decimal GetIsAllPrepaymentPenaltyAllowed() 
        {
            return (null == m_loanData) ? 3 : (m_loanData.IsAllPrepaymentPenaltyAllowed ? 1 : 0);
        }
        public decimal GetIsNegAmort() 
        {
            return (null == m_loanData) ? 3 : (m_loanData.sIsNegAmort ? 1 : 0);
        }
        public decimal GetIsWithinFhaLoanLimits() 
        {
            return (null == m_loanData) ? 3 : (m_loanData.sIsWithinFhaLoanLimits ? 1 : 0);
        }
        // 5/28/2009 dd - OPM 30923 - ScoreQBC
        public decimal GetScoreQBC()
        {
            return (null == appData) ? 3 : appData.aProdRepresentativeScore; 
        }

        // 11/17/2009 dd - OPM 42581
        public decimal GetIsCreditQualifying()
        {
            return (null == m_loanData) ? 3 : (m_loanData.sIsCreditQualifying ? 1 : 0);
        }

        public decimal GetOriginatingCompanyTierID()
        {
            return (null == m_loanData) ? -1 : m_loanData.sPmlCompanyTierId;
        }
        /// ////////////////////////////////////////////////////////////////////
        // wrapper functions
        /// ////////////////////////////////////////////////////////////////////

        //6/28/07 db OPM 16602 - New MaxHighCreditAmountX keyword
        public decimal MaxHighCreditAmountX(string parameters)             
        { 
            return (null==creditData) ? 3 : creditData.MaxHighCreditAmountX(parameters); 
        }

        public decimal FcCountX(string parameters)
        {
            return creditData.FcCountX(parameters);
        }

        public decimal HasFcX(string parameters)             
        { 
            return creditData.HasFcX(parameters); 
        }

        public decimal TradeCountX(string parameters)             
        { 
            return creditData.TradeCountX(parameters); 
        }

        public decimal ChargeOffCountX(string parameters)
        {
            return this.creditData.ChargeOffCountX(parameters);
        }

        public decimal GetCreditReportAge()
        {
            // 10/13/2011 dd - OPM 72613
            return (null == creditData) ? 3 : creditData.CreditReportAge;
        }
        public decimal HasContiguousMortgageHistory(string parameters)             
        { 
            return creditData.HasContiguousMortgageHistory(parameters); 
        }

        public decimal MajorDerogCountX(string parameters)             
        { 
            return creditData.MajorDerogCountX(parameters); 
        }

        public int MaxEmploymentGapWithin( int months )
        {
            return (null == appData) ? 3 : appData.MaxEmploymentGapWithin( months );
        }

        public decimal GetYearsOnJob()
        {
            return (null == appData) ? 3 : appData.YearsOnJob;
        }

        public decimal GetYearsInProfession()
        {
            return (null == appData) ? 3 : appData.YearsInProfession;
        }

        static public decimal GetCertTimeHhMm() 
        {
            // 3/31/2008 dd - Return the current time in military format. HHMM.
            return decimal.Parse(DateTime.Now.ToString("HHmm"));
        }

        static public decimal GetCertTimeDayOfWeek() 
        {
            // 3/31/2008 dd - Return the day of the week of today.
            // 1 - Sunday, 2 - Monday, 3 - Tuesday, 4 - Wednesday, 5 - Thursday, 6 - Friday, 7 - Saturday.
            int dayOfWeek = (int) DateTime.Now.DayOfWeek + 1; // C# return start Sunday=0.
            return dayOfWeek;
        }

        public decimal AssetTotalX(string parameters)
        {
            return (null == m_loanData) ? 3 : m_loanData.sAssetTotalX(parameters);
        }
        public decimal BorrIncomeTotalX(string parameters)
        {
            return (null == appData) ? 3 : appData.aBorrIncomeTotalX(parameters);
        }

        public decimal GetIsRSEFeatureEnable() 
        {
            return (null == m_loanData) ? 3 : (m_loanData.sLockPolicy.IsUsingRateSheetExpirationFeature ? 1 : 0);
        }

        public decimal GetIncludeMyCommunityProc() 
        {
            return (null == m_loanData) ? 3 : (m_loanData.sProdIncludeMyCommunityProc ? 1 : 0);
        }
        public decimal GetIncludeHomePossibleProc() 
        {
            return (null == m_loanData) ? 3 : (m_loanData.sProdIncludeHomePossibleProc ? 1 : 0);
        }
        public decimal GetIncludeNormalProc() 
        {
            return (null == m_loanData) ? 3 : (m_loanData.sProdIncludeNormalProc ? 1 : 0);
        }
        public decimal GetIncludeFHATOTALProc() 
        {
            return (null == m_loanData) ? 3 : (m_loanData.sProdIncludeFHATotalProc ? 1 : 0);
        }
        public decimal GetIncludeUSDARuralProc()
        {
            return (null == m_loanData) ? 3 : (m_loanData.sProdIncludeUSDARuralProc ? 1 : 0);
        }
        public decimal GetIncludeVAProc() 
        {
            return (null == m_loanData) ? 3 : (m_loanData.sProdIncludeVAProc ? 1 : 0);
        }

        public decimal GetLenderID()
        {
            return (null == m_loanData) ? -1 : m_loanData.sLenderID;
        }

        public decimal GetIsDuRefiPlus()
        {
            return (null == m_loanData) ? 3 : (m_loanData.sProdIsDuRefiPlus ? 1 : 0);
        }
        public decimal GetHasNonOccupantCoborrower()
        {
            if (null == m_loanData)
            {
                return 3;
            }

            if (useCache)
            {
                if (x_HasNonOccupantCoborrower.HasValue)
                {
                    return x_HasNonOccupantCoborrower.Value;
                }

                x_HasNonOccupantCoborrower = (m_loanData.sHasNonOccupantCoborrower ? 1 : 0);
                return x_HasNonOccupantCoborrower.Value;
            }

            return (m_loanData.sHasNonOccupantCoborrower ? 1 : 0);
        }
        public decimal GetH4HHasNonOccupantCoborrower()
        {
            return (null == m_loanData) ? 3 : (m_loanData.sH4HHasNonOccupantCoborrower ? 1 : 0);
        }
        public decimal GetHasTempBuydown() 
        {
            return (null == m_loanData) ? 3 : (m_loanData.sHasTempBuydown ? 1 : 0);
        }
        public decimal GetInterestedPartyContribPercent()
        {
            return (null == m_loanData) ? 3 : m_loanData.sInterestedPartyContribPc;
        }
        public decimal GetIsIdentityOfInterestTransaction()
        {
            return (null == m_loanData) ? 3 : m_loanData.sTotalScoreIsIdentityOfInterest ? 1 : 0;
        }
        public decimal GetIsIdentityOfInterestTransactionException()
        {
            return (null == m_loanData) ? 3 : m_loanData.sTotalScoreIsIdentityOfInterestException ? 1 : 0;
        }
        public decimal GetEnergyEfficientImprovementsAmt()
        {
            return (null == m_loanData) ? 3 : m_loanData.sFHAEnergyEffImprov;
        }
        public decimal GetSubFinType()
        {
            return (int)(null == m_loanData ? E_sFHASecondaryFinancingSourceT.LeaveBlank : m_loanData.sFHASecondaryFinancingSourceT);
        }
        public decimal GetIsWithinFHA3Or4UnitMaxLoanAmount()
        {
            return (null == m_loanData) ? 3 : m_loanData.sTotalScoreIsWithinFha3Or4UnitMaxLoanAmount ? 1 : 0;
        }
        public decimal GetHasClearCaivrsAuthcode()
        {
            return (null == appData) ? 3 : appData.aHasClearCaivrsAuthCode ? 1 : 0;
        }
        public decimal GetIsHUDApprovedFTHBCounselingComplete()
        {
            return (null == appData) ? 3 : appData.aIsHudApprovedFTHBCounselingComplete ? 1 : 0;
        }
        public decimal GetLdpOrGsaExclusion()
        {
            return (null == appData) ? 3 : (int) appData.aTotalScoreLdpOrGsaExclusionT;
        }
        public decimal GetTotalCreditRiskResultType()
        {
            return (null == m_loanData) ? 3 : (int) m_loanData.sTotalScoreCreditRiskResultT;
        }
        public decimal GetLoanProductType()
        {
            return (null == m_loanData) ? 3 : (int)m_loanData.sLpProductT;
        }
        public decimal GetIsAllReoInherited()
        {
            return (null == m_loanData) ? 3 : (int)m_loanData.sH4HIsAllReoInheritedTri;
        }
        public decimal GetPresentDebtRatioTop()
        {
            try
            {
                return (null == m_loanData) ? 3 : m_loanData.sH4HPresentMortgageTopRatio;
            }
            catch
            {
                // 12/7/2009 dd - It could throw exception if income is 0.
                return 0;
            }
        }
        public decimal GetNetworthLessRetirement()
        {
            return (null == appData) ? 3 : appData.aH4HNetworthLessRetirementKeyword;
        }
        public decimal GetTotalAvailReserveMonths()
        {
            return (null == m_loanData) ? 3 : m_loanData.sTotalScoreReserves;
        }
        public decimal GetAssetsAfterClosing()
        {
            return (null == m_loanData) ? 3 : m_loanData.sTotalScoreAssetsAfterClosing;
        }
        public decimal GetNonOccCoBorrOnOrigNote()
        {
            return (null == m_loanData) ? 3 : (int) m_loanData.sNonOccCoBorrOnOrigNoteT;
        }
        public decimal GetH4HOriginationReqMet()
        {
            return (null == m_loanData) ? 3 : (int) m_loanData.sH4HOriginationReqMetT;
        }
        public decimal GetBorrowerHasFraudConviction()
        {
            return (null == appData) ? 3 : (int) appData.aHasFraudConvictionT;
        }
        public decimal GetOutstandingPrincipalBalance()
        {
            return (null == m_loanData) ? 3 : m_loanData.sSpLien;
        }
        public decimal GetFhaCondoApprovalStatus()
        {
            return (null == m_loanData) ? 3 : (int)m_loanData.sFhaCondoApprovalStatusT;
        }
        public decimal GetFhaLoanLimit()
        {
            return (null == m_loanData) ? 729750 : m_loanData.sFhaLoanLimit;
        }

        public decimal GetIsTpoLoan()
        {
            return (null == m_loanData) ? 3 : (m_loanData.sIsTPO_UseInKeyword ? 1 : 0);
        }
        public decimal GetUFMIPRefund()
        {
            return (null == m_loanData) ? 3 : (int)m_loanData.sFHASalesConcessions;
        }
        public decimal GetHasAppraisal()
        {
            return (null == m_loanData) ? 3 : m_loanData.sHasAppraisal ? 1 : 0;
        }
        public decimal GetOriginalAppraisedValue()
        {
            return (null == m_loanData) ? 3 : m_loanData.sOriginalAppraisedValue;
        }
        public decimal GetOriginatorCompensationPaymentSource()
        {
            return (null == m_loanData) ? 3 : (int)m_loanData.sOriginatorCompensationPaymentSourceT;
        }

        public decimal GetHasSubjPropPriorSaleData()
        {
            return (null == m_loanData) ? 3 : m_loanData.sHasSubjPropPriorSaleData ? 1: 0;
        }

        public decimal GetPropertySeasoningMonths()
        {
            return (null == m_loanData) ? 3 : m_loanData.sPropertySeasoningMonths;
        }

        public decimal GetPropertySeasoningDays()
        {
            return (null == m_loanData) ? 3 : m_loanData.sPropertySeasoningDays;
        }

        public decimal GetPriorSalesPrice()
        {
            return (null == m_loanData) ? 3 : m_loanData.sPriorSalesPrice;
        }

        public decimal GetPropertySeller()
        {
            return (null == m_loanData) ? 3 : (int)m_loanData.sPriorSalesPropertySellerT  ;
        }

        public decimal GetSalesPrice()
        {
            return (null == m_loanData) ? 3 : m_loanData.sHouseVal;  
        }

        public decimal GetLTV_USDA()
        {
            return (null == m_loanData) ? 3 : m_loanData.sLtvRUsda;
        }

        public decimal GetCLTV_USDA()
        {
            return (null == m_loanData) ? 3 : m_loanData.sCLtvRUsda;
        }
        public decimal GetLoanStatus()
        {
            return (int)((null == m_loanData) ? 3 : (int) m_loanData.sStatusT);
        }
        public decimal GetInvestorID()
        {
            return (null == m_loanData) ? 0 : m_loanData.sLpInvestorId;
        }
        public decimal GetIsFinancedUFMIPIncludedInLTV()
        {
            return (null == m_loanData) ? 0 : m_loanData.sIncludeUfmipInLtvCalc  ? 1: 0;
        }
        public decimal GetNumberOfFinancedProperties()
        {
            return (null == m_loanData) ? 1 : m_loanData.sNumFinancedProperties;
        }

        public decimal GetHCLTV()
        {
            if (null == m_loanData)
            {
                return 3;
            }

            if (useCache)
            {
                if (x_HCLTV.HasValue)
                {
                    return x_HCLTV.Value;
                }

                x_HCLTV = m_loanData.sPricingHcltvR;
                return x_HCLTV.Value;
            }
            return m_loanData.sPricingHcltvR; 
        }

        public decimal GetHELOCLineAmount()
        {
            return (null == m_loanData) ? 3 : m_loanData.sSubFinPe;
        }

        public decimal GetRequestCommunityOrAffordableSeconds()
        {
            return (null == m_loanData) ? 0 : m_loanData.sRequestCommunityOrAffordableSeconds ? 1 : 0;
        }

        public decimal GetIsRenovation()
        {
            return (null == m_loanData) ? 0 : m_loanData.sIsRenovationLoan ? 1 : 0;
        }
        public decimal GetRenovationCosts()
        {
            return (null == m_loanData) ? 3 : m_loanData.sTotalRenovationCosts;
        }
        public decimal GetAsCompletedValue()
        {
            return (null == m_loanData) ? 3 : m_loanData.sAsCompletedValueKeyword;
        }


        public decimal GetARM1stAdjCap()
        {
            return (null == m_loanData) ? 3 : m_loanData.sRAdj1stCapR;
        }

        public decimal GetARMNextAdjCap()
        {
            return (null == m_loanData) ? 3 : m_loanData.sRAdjCapR;
        }

        public decimal GetARMLifeAdjCap()
        {
            return (null == m_loanData) ? 3 : m_loanData.sRAdjLifeCapR;
        }

        public decimal GetARMAdjPeriod()
        {
            return (null == m_loanData) ? 3 : m_loanData.sRAdjCapMon;
        }

        public decimal GetARMIndex()
        {
            return (null == m_loanData) ? 3 : (int) m_loanData.sMasterArmIndexT;
        }

        public decimal GetAssumption()
        {
            return (null == m_loanData) ? 3 : (int)m_loanData.sAssumeLT;
        }

        public decimal GetIsConvertibleMortgage()
        {
            return (null == m_loanData) ? 3 : m_loanData.sIsConvertibleMortgage ? 1 : 0;
        }

        public decimal GetInterestOnlyMonths()
        {
            return (null == m_loanData) ? 3 : m_loanData.sIOnlyMon;
        }

        public decimal GetNoteRateAfterLOCompAndRounding()
        {
            return (null == m_loanData) ? 3 : m_loanData.sNoteRateAfterLOCompAndRounding;
        }
        public decimal GetRateOptionFinalPrice()
        {
            return (null == m_loanData) ? 3 : m_loanData.sRateOptionFinalPrice;
        }
        public decimal GetNoteRateBeforeLOCompAndRounding()
        {
            return (null == m_loanData) ? 3 : m_loanData.sNoteRateBeforeLOCompAndRounding;
        }
        public decimal GetPointsBeforeLOCompAndRounding()
        {
            return (null == m_loanData) ? 3 : m_loanData.sPointsBeforeLOCompAndRounding;
        }
        public decimal GetNoteRateIsIntegerMultipleOfOneEighth()
        {
            return (null == m_loanData) ? 3 : m_loanData.sNoteRateIsIntegerMultipleOfOneEighth ? 1 : 0;
        }
        public decimal GetCashToBorrower()
        {
            return (null == m_loanData) ? 3 : m_loanData.sCashToBorrower;
        }
        public decimal GetHasMortgageLiabilityThatWillBePaidOffAtClosing()
        {
            return (null == m_loanData) ? 3 : m_loanData.sHasMortgageLiabilityThatWillBePaidOffAtClosing ? 1 : 0;
        }

        public decimal GetRateOptionPointsPreFEMaxYSP()
        {
            return (null == m_loanData) ? 3 : m_loanData.sRateOptionPointsPreFEMaxYSP;
        }
        public decimal GetZipCode()
        {
            return (null == m_loanData) ? 3 : m_loanData.sSpZipKeyword;
        }
        public decimal GetMaxNoteRateOnLoanFile()
        {
            return (null == m_loanData) ? 3 : m_loanData.sMaxRKeyword;
        }
        public decimal GetPMI_Type()
        {
            return (null == m_loanData) ? 3 : (int)m_loanData.sProdConvMIOptionT;
        }
        public decimal GetSplitMI_Upfront()
        {
            return (null == m_loanData) ? 3 : (int)m_loanData.sConvSplitMIRT;
        }
        public decimal GetRunPmlRequestSourceType()
        {
            return (null == m_loanData) ? 0 : (int)m_loanData.RunPmlRequestSourceT;
        }
        public decimal GetLenderFeeBuyout()
        {
            return (null == m_loanData) ? 3 : (int)m_loanData.sLenderFeeBuyoutRequestedT;
        }
        public decimal GetBranchChannel()
        {
            return (null == m_loanData) ? 0 : (int)m_loanData.sBranchChannelT;
        }
        public decimal GetCorrespondentProcess()
        {
            return (null == m_loanData) ? 0 : (int)m_loanData.sCorrespondentProcessT;
        }
        public decimal GetIsUFMIPorFFFinanced()
        {
            return (null == m_loanData) ? 3 : m_loanData.sProdIsFhaMipFinanced ? 1 : 0;
        }
        public decimal GetLenderIsACreditUnion()
        {
            return (null == m_loanData) ? 3 : m_loanData.BrokerDB.IsCreditUnion ? 1 : 0;
        }
        public decimal GetVAEligibleBorrOnFile()
        {
            return (null == m_loanData) ? 3 : m_loanData.sVAEligibleBorrOnFile ? 1 : 0;
        }
        public decimal GetCustomPMLField1()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPMLFieldKeyword(1);
        }
        public decimal GetCustomPMLField2()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPMLFieldKeyword(2);
        }
        public decimal GetCustomPMLField3()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPMLFieldKeyword(3);
        }
        public decimal GetCustomPMLField4()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPMLFieldKeyword(4);
        }
        public decimal GetCustomPMLField5()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPMLFieldKeyword(5);
        }
        public decimal GetCustomPMLField6()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPMLFieldKeyword(6);
        }
        public decimal GetCustomPMLField7()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPMLFieldKeyword(7);
        }
        public decimal GetCustomPMLField8()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPMLFieldKeyword(8);
        }
        public decimal GetCustomPMLField9()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPMLFieldKeyword(9);
        }
        public decimal GetCustomPMLField10()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPMLFieldKeyword(10);
        }
        public decimal GetCustomPMLField11()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPMLFieldKeyword(11);
        }
        public decimal GetCustomPMLField12()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPMLFieldKeyword(12);
        }
        public decimal GetCustomPMLField13()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPMLFieldKeyword(13);
        }
        public decimal GetCustomPMLField14()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPMLFieldKeyword(14);
        }
        public decimal GetCustomPMLField15()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPMLFieldKeyword(15);
        }
        public decimal GetCustomPMLField16()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPMLFieldKeyword(16);
        }
        public decimal GetCustomPMLField17()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPMLFieldKeyword(17);
        }
        public decimal GetCustomPMLField18()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPMLFieldKeyword(18);
        }
        public decimal GetCustomPMLField19()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPMLFieldKeyword(19);
        }
        public decimal GetCustomPMLField20()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPMLFieldKeyword(20);
        }
        public decimal GetTotalLiquidAssets()
        {
            return (null == m_loanData) ? 3 : m_loanData.sTotLiquidAssets;
        }
        public decimal GetRateOptionExceedsMaxDti()
        {
            return (null == m_loanData) ? 3 : m_loanData.sRateOptionExceedsMaxDti ? 1 : 0;
        }
        public decimal GetIsEmployeeLoan()
        {
            return (null == m_loanData) ? 3 : m_loanData.sIsEmployeeLoan ? 1 : 0;
        }
        public decimal GetLoanRegisteredDate()
        {
            return (null == m_loanData) ? 3 : m_loanData.sLoanRegisteredDateForPricing;
        }
        public decimal GetCompPlanEffectiveDate()
        {
            return (null == m_loanData) ? 3 : m_loanData.sCompPlanEffectiveDateForPricing;
        }
        public decimal GetsProdIsLoanEndorsedBeforeJune09()
        {
            return (null == m_loanData) ? 3 : m_loanData.sProdIsLoanEndorsedBeforeJune09 ? 1 : 0;
        }
        public decimal GetAPOR()
        {
            return (null == m_loanData) ? 3 : m_loanData.sQMAveragePrimeOfferRCalc;
        }

        public decimal GetAppraisedValue()
        {
            return (null == m_loanData) ? 3 : m_loanData.sAppraisedValueKeyword;
        }

        public decimal GetLoanHasNonExpiredApproval()
        {
            return (null == m_loanData) ? 3 : m_loanData.sLoanHasNonExpiredApproval ? 1 : 0;
        }
        public decimal GetCustomPricingPolicyField1()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPricingPolicyFieldKeyword(1);
        }
        public decimal GetCustomPricingPolicyField2()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPricingPolicyFieldKeyword(2);
        }
        public decimal GetCustomPricingPolicyField3()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPricingPolicyFieldKeyword(3);
        }
        public decimal GetCustomPricingPolicyField4()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPricingPolicyFieldKeyword(4);
        }
        public decimal GetCustomPricingPolicyField5()
        {
            return (null == m_loanData) ? 3 : m_loanData.GetCustomPricingPolicyFieldKeyword(5);
        }

        public decimal GetOCIsDelegatedCorrespondent()
        {
            return (null == this.m_loanData) ? 3 : this.m_loanData.sIsOriginatingCompanyDelegatedCorrespondent ? 1 : 0;
        }

        public decimal GetBorrowerType()
        {
            return (null == appData) ? 3 : (int) appData.aBorrowerTypeT;
        }

        public decimal GetCustomLoanProgramField1(string str)
        {
            string actual = string.Empty;
            if (null != m_loanData && m_loanData.sPricingLoanProgramTemplate != null)
            {
                actual = m_loanData.sPricingLoanProgramTemplate.lLpCustomCode1;
            }
            return CompareString(str, actual);
        }

        public decimal GetCustomLoanProgramField2(string str)
        {
            string actual = string.Empty;
            if (null != m_loanData && m_loanData.sPricingLoanProgramTemplate != null)
            {
                actual = m_loanData.sPricingLoanProgramTemplate.lLpCustomCode2;
            }
            return CompareString(str, actual);
        }
        public decimal GetCustomLoanProgramField3(string str)
        {
            string actual = string.Empty;
            if (null != m_loanData && m_loanData.sPricingLoanProgramTemplate != null)
            {
                actual = m_loanData.sPricingLoanProgramTemplate.lLpCustomCode3;
            }
            return CompareString(str, actual);
        }
        public decimal GetCustomLoanProgramField4(string str)
        {
            string actual = string.Empty;
            if (null != m_loanData && m_loanData.sPricingLoanProgramTemplate != null)
            {
                actual = m_loanData.sPricingLoanProgramTemplate.lLpCustomCode4;
            }
            return CompareString(str, actual);
        }
        public decimal GetCustomLoanProgramField5(string str)
        {
            string actual = string.Empty;
            if (null != m_loanData && m_loanData.sPricingLoanProgramTemplate != null)
            {
                actual = m_loanData.sPricingLoanProgramTemplate.lLpCustomCode5;
            }
            return CompareString(str, actual);
        }

        public decimal GetInvestorLoanProgramField1(string str)
        {
            string actual = string.Empty;
            if (null != m_loanData && m_loanData.sPricingLoanProgramTemplate != null)
            {
                actual = m_loanData.sPricingLoanProgramTemplate.lLpInvestorCode1;
            }
            return CompareString(str, actual);
        }

        public decimal GetInvestorLoanProgramField2(string str)
        {
            string actual = string.Empty;
            if (null != m_loanData && m_loanData.sPricingLoanProgramTemplate != null)
            {
                actual = m_loanData.sPricingLoanProgramTemplate.lLpInvestorCode2;
            }
            return CompareString(str, actual);
        }

        public decimal GetInvestorLoanProgramField3(string str)
        {
            string actual = string.Empty;
            if (null != m_loanData && m_loanData.sPricingLoanProgramTemplate != null)
            {
                actual = m_loanData.sPricingLoanProgramTemplate.lLpInvestorCode3;
            }
            return CompareString(str, actual);
        }

        public decimal GetInvestorLoanProgramField4(string str)
        {
            string actual = string.Empty;
            if (null != m_loanData && m_loanData.sPricingLoanProgramTemplate != null)
            {
                actual = m_loanData.sPricingLoanProgramTemplate.lLpInvestorCode4;
            }
            return CompareString(str, actual);
        }

        public decimal GetInvestorLoanProgramField5(string str)
        {
            string actual = string.Empty;
            if (null != m_loanData && m_loanData.sPricingLoanProgramTemplate != null)
            {
                actual = m_loanData.sPricingLoanProgramTemplate.lLpInvestorCode5;
            }
            return CompareString(str, actual);
        }

        public decimal GetScoreCBS(string parameters)
        {
            return (null == appData) ? 3 : appData.ScoreCBS(parameters);
        }

        public decimal GetScoreCBSKeyword()
        {
            return (null == appData) ? 3 : appData.aScoreCBS;
        }

        public decimal GetLoanType()
        {
            return (null == m_loanData) ? 3 : (int) m_loanData.sLT;
        }

        public decimal GetBranchIdNumber()
        {
            return (null == m_loanData) ? 3 : m_loanData.sBranchIdNumber;
        }

        public decimal GetOriginatingCompanyIndexNumber()
        {
            return this.m_loanData?.sOriginatingCompanyIndexNumber ?? -1;
        }

        public decimal GetTotalCashDeposit()
        {
            return (null == m_loanData ) ? 3 : m_loanData.sTotCashDeposit;
        }

        public decimal Get2ndLienIsHeloc()
        {
            return (null == m_loanData) ? 3 : m_loanData.s2ndLienIsHeloc ? 1 : 0;
        }

        public decimal GetFannieHomebuyerEducationType()
        {
            return (null == m_loanData) ? 3 : (int)m_loanData.sFannieHomebuyerEducationT;
        }

        public decimal Get2ndFinancingInclusionType()
        {
            return (null == m_loanData) ? 3 : (int)m_loanData.sSecondFinancingInclusionT;
        }
        public decimal GetIsStudentLoanCashoutRefi()
        {
            return (null == m_loanData) ? 3 : m_loanData.sIsStudentLoanCashoutRefi ? 1 : 0;
        }

        public decimal GetNotPermanentlyAffixed()
        {
            return (null == m_loanData) ? 3 : m_loanData.sIsNotPermanentlyAffixed ? 1 : 0;
        }

        public decimal GetProdConvLoanLtvForMiNeed()
        {
            return (null == m_loanData) ? 3 : m_loanData.sConvLoanLtvForMiNeed;
        }

        private decimal CompareString(string expected, string actual)
        {
            // 1/24/2014 dd - This method compare two strings using case insensitive.
            // Since we cannot pass 'space' in Price Policy rule, therefore we replace
            // space with underscore before comparison.
            if (expected == null)
            {
                expected = string.Empty;
            }
            if (actual == null)
            {
                actual = string.Empty;
            }

            expected = expected.TrimWhitespaceAndBOM().Replace(' ', '_'); // Trim whitespace before replace space with underscore
            actual = actual.TrimWhitespaceAndBOM().Replace(' ', '_'); // Trim whitespace before replace space with underscore

            return expected.Equals(actual, StringComparison.OrdinalIgnoreCase) ? 1 : 0;
        }

        #endregion wrapper properties



        public static void VerifyMapping()
        {
            StringBuilder sb = new StringBuilder();

            // Verify Keywords
            // Predicate : if CSymbolTable contains a CSymbolFieldName object, then Keyword2FieldName has to contain CSymbolFieldName.Inputkeyword 
            Hashtable ourKeywords = new CaseInsensitiveHashtable();
            for( int i =0; i < Keyword2FieldName.GetLength(0); i++ )
            {
                string key = Keyword2FieldName[i, 0];
                ourKeywords[ key ] = null;
            }

            CSymbolTable symbolTable = CSymbolTable.CreateSymbolTableWithoutNewKeywords();
            foreach( string key in symbolTable.Entries.Keys )
            {
                CSymbolFieldName fieldName = symbolTable.Entries[ key] as CSymbolFieldName;
                if( fieldName == null )
                    continue;

                if( ourKeywords.Contains( fieldName.InputKeyword ) == false && fieldName.DependonLoanData() )
                    sb.AppendFormat("\nMissing the keyword {1} in this mapping.", key, fieldName.InputKeyword );

            }

            //Verify fieldName
            Hashtable properties = new CaseInsensitiveHashtable();

            foreach( System.Reflection.PropertyInfo  property in typeof(CPageBase).GetProperties() )
                properties[property.Name] = null;

            foreach( System.Reflection.PropertyInfo  property in typeof(CAppData).GetProperties() )
                properties["appData."+property.Name] = null;

            foreach( System.Reflection.PropertyInfo property in typeof(ICreditReport).GetProperties() )
                properties["creditData."+property.Name] = null;


            for( int i =0; i < Keyword2FieldName.GetLength(0); i++ )
            {
                string propertyName = Keyword2FieldName[i, 1];
                if( properties.Contains( propertyName ) == false )
                    sb.AppendFormat("\nThe entry [ keyword {0} -> property {1} ]  is not correct, because don't exist that property",
                        Keyword2FieldName[i, 0], Keyword2FieldName[i, 1] );                                     
 
            }
            
            if( sb.Length > 0 )
            {
                sb.Insert(0, "The mapping LoanDataWrapper.Keyword2FieldName is not correct\n\n" );
                throw new CBaseException(ErrorMessages.Generic, sb.ToString());
            }

        }



        static internal readonly string [,] Keyword2FieldName =
        {
            { "Score2",             "sCreditScoreType2"     },
            { "Score1",             "sCreditScoreType1"     },
            { "LienPosition",       "sLienPosT"             },
            { "PropertyValue",      "sPropertyValueKeyword" },
            { "CashoutAmt",         "sProdCashoutAmt"       },
            { "Is1stTimeHomeBuyer", "sHas1stTimeBuyer"      },
            { "2ndWageScore",       "sCreditScoreType4"     },
            { "NumberOfUnits",      "sProdNumberOfUnits"    },
            { "MIOption",           "sProdMIOptionT"        },
            { "Units",              "sProdNumberOfUnits"    },
            { "Score2Soft",         "sCreditScoreType2Soft_rep"},
            { "BkHasUnpaidAndNondischargedUnknownTypeBk", "creditData.HasUnpaidAndNondischargedUnknownTypeBk"},
            { "TradeCount",         "creditData.TradeCount"            },
            { "SelfEmployed",       "appData.aIsSelfEmployed"       },
            { "IsInCreditCounseling", "creditData.IsInCreditCounseling" },
            { "BkHasUnpaidAndNondischarged13Bk", "creditData.HasUnpaidAndNondischarged13Bk" },        
            { "PpmtPenaltyMonths", "sProdPpmtPenaltyMonKeyword"    },
            { "InterestOnlyOtherLoan", "sIsIOnlyForSubFin"  },
            { "TradesCountNonMajorDerog", "creditData.TradesCountNonMajorDerog"},
            { "Qcltv",              "sCltvLpeQual"          },
            { "Qhcltv",             "sHcltvLpeQual"        },
            { "DocType",            "sProdDocT"             },
            { "PropertyType",       "sProdSpT"              },
            { "IsMhAdvantageHome", "sHomeIsMhAdvantageTri" },
            { "IsSameInvestorFor8020",  "sLp8020HaveSameInvestor"   },
            { "IsPpAllowedInSubjPropState", "IsSpStateAllowPp"  }, 
            { "LTV",                "sLtvR"                     },    
            { "AvailReserveMonths", "sProdAvailReserveMonthsKeyword"   },
            { "ArmFixedTerm",       "sRAdj1stCapMon"            },
            { "MinScoreCount",      "sMinimumBorrowerCreditScoresCount"}, 
            { "IsConformingLAmt",   "sIsConformingLAmt"         },
            { "AmortizationType",   "sFinMethT"                 }, 
            { "IsNegAmortOtherLien","sLpIsNegAmortOtherLien"},
            { "LockDays",           "sRLckdDaysFromInvestor"    },
            { "PurposeType",        "sLPurposeT"                },
            { "DebtRatioBottom",    "sQualBottomR"              },
            { "IncomeMonthly",      "sPrimAppTotNonspI"         },
            { "IsNonwarrantableProj",    "sProdIsNonwarrantableProj" },
            { "BkHasUnpaidAndNondischarged12Bk", "creditData.HasUnpaidAndNondischarged12Bk" },
		
            { "PropertyStructType", "sProdSpStructureT"         },
            { "CreditReportIsOnFile", "appData.aIsCreditReportOnFile"},
            { "IsCreditScoresDifferentWithCreditReport",    "appData.aIsCreditScoresDifferentWithCreditReport" },
            { "State",              "sSpState"                  },
            { "PropertyPurpose",    "sOccT"                     },
            { "BkHasUnpaidAndNondischarged", "creditData.HasUnpaidAndNondischargedAnyBk" },
            { "DebtRatioTop",       "sQualTopR"                 },
            { "CLTV",               "sCltvR"                    },
            { "HasTaxOpenTaxLien",  "creditData.HasTaxOpenTaxLien" },
            { "QLAmt",              "sLAmtCalcLpeQual"          },
            { "Due",                "sDue"                      },
            { "Qscore",             "sCreditScoreLpeQual"       },
            { "IsCondotel",         "sProdIsCondotel"           },
            { "Qltv",               "sLtvLpeQual"               },
            { "ThirdPartyUwResultType",     "sProd3rdPartyUwResultT"  },
            { "ForeclosureUnpaidCount", "creditData.ForeclosureUnpaidCount" },
            { "Impound",            "sProdImpound"              },
            { "InterestOnly",       "sIsIOnly"       },
            { "PropertyInRuralArea", "sProdIsSpInRuralArea"     },
            { "HasMortgage",         "appData.aHasMortgageLia"   },
            { "BkHasUnpaidAndNondischarged7Bk", "creditData.HasUnpaidAndNondischarged7Bk"   },
            { "BkHasUnpaidAndNondischarged11Bk", "creditData.HasUnpaidAndNondischarged11Bk" },
            { "DerogatoryAmountTotal",  "creditData.DerogatoryAmountTotal"  },
            { "BkMultipleType",     "creditData.BkMultipleType" }, 
            { "AmortTypeOtherLoan", "sOtherLFinMethT"           },
            { "Term",               "sTerm"                     },
            { "CitizenshipType",    "appData.aCitizenshipType"    },
            { "TotBalOfMedCollectionsUnpaid", "creditData.TotBalOfMedCollectionsUnpaid" },
            { "LoanAmount",         "sLAmtCalc"                 },
            { "Ltv1stLien",         "sLtv1stLien"               },
            { "PrimaryWageEarnerCreditScoresCount", "sPrimaryWageEarnerCreditScoresCount" },
            { "LoanAmount1stLien",  "sLAmt1stLien"              },
            { "LoanAmount2ndLien",  "sLAmt2ndLien"              },
            { "NumberOfCondoStories", "sProdCondoStories"       },
            { "LAmt",               "sLAmtCalc"                 },
            { "HasHousingHistory",  "sProdHasHousingHistory"    },
            { "HasCobor",           "sHasCoborrower"      },
            { "IsStandAlone2ndLien", "sIsStandAlone2ndLien"},
            { "ResidualIncomeMonthly", "sProdEstimatedResidualI"},        
            { "IsAllPrepaymentPenaltyAllowed", "IsAllPrepaymentPenaltyAllowed"},
            { "IsNegAmort",         "sIsNegAmort"},
            { "CountyFIPS",         "sSpCountyFips"},
            { "IsWithinFhaLoanLimits", "sIsWithinFhaLoanLimits"},
            { "IncludeMyCommunityProc", "sProdIncludeMyCommunityProc"},
            { "IncludeHomePossibleProc", "sProdIncludeHomePossibleProc"},
            { "IncludeNormalProc", "sProdIncludeNormalProc"},
            { "IncludeFHATOTALProc", "sProdIncludeFHATotalProc"},
            { "IncludeVAProc", "sProdIncludeVAProc"},
            { "IncludeUSDARuralProc", "sProdIncludeUSDARuralProc"},
            { "IsRSEFeatureEnable", "BrokerDB"},
            { "IsHighBalanceConformingLAmt", "sIsHighBalanceConformingLAmt" },
            { "TLAmt", "sFinalLAmt"},
            { "IsDuRefiPlus", "sProdIsDuRefiPlus"},
            {"ScoreQBC", "appData.aProdRepresentativeScore"},
            {"IsTexas50a6Loan", "sProdIsTexas50a6Loan"},
            {"IsPriorLoanTexas50a6Loan", "sPreviousLoanIsTexas50a6Loan"},
            {"FhaProductType", "sTotalScoreFhaProductT"},
            {"HasNonOccupantCoborrower", "sHasNonOccupantCoborrower"},
            {"H4HHasNonOccupantCoborrower","sH4HHasNonOccupantCoborrower"},
            {"HasTempBuydown", "sHasTempBuydown"},
            {"InterestedPartyContribPercent", "sInterestedPartyContribPc"},
            {"IsIdentityOfInterestTransaction","sTotalScoreIsIdentityOfInterest"},
            {"IsIdentityOfInterestTransactionException", "sTotalScoreIsIdentityOfInterestException"},
            {"EnergyEfficientImprovementsAmt", "sFHAEnergyEffImprov"},
            {"SubFinType", "sFHASecondaryFinancingSourceT"},
            {"IsWithinFHA3Or4UnitMaxLoanAmount","sTotalScoreIsWithinFha3Or4UnitMaxLoanAmount"},
            {"YearsOnJob", "appData.YearsOnJob"},
            {"YearsInProfession", "appData.YearsInProfession"},
            {"TotalCreditRiskResultType", "sTotalScoreCreditRiskResultT"},
            {"LdpOrGsaExclusion","appData.aTotalScoreLdpOrGsaExclusionT"},
            {"HasClearCaivrsAuthcode","appData.aHasClearCaivrsAuthCode"},
            {"IsHUDApprovedFTHBCounselingComplete","appData.aIsHudApprovedFTHBCounselingComplete"},
            {"LoanProductType", "sLpProductT"},
            {"LockDaysUnadjusted","sRLckdDays"},
            {"IsCreditQualifying", "sIsCreditQualifying"},
            {"IsAllReoInherited", "sH4HIsAllReoInheritedTri"},
            {"PresentDebtRatioTop", "sH4HPresentMortgageTopRatio"},
            {"NetworthLessRetirement", "appData.aH4HNetworthLessRetirementKeyword"},
            {"TotalAvailReserveMonths", "sTotalScoreReserves"},
            {"AssetsAfterClosing", "sTotalScoreAssetsAfterClosing"},
            {"NonOccCoBorrOnOrigNote", "sNonOccCoBorrOnOrigNoteT"},
            {"H4HOriginationReqMet", "sH4HOriginationReqMetT"},
            {"BorrowerHasFraudConviction", "appData.aHasFraudConvictionT"},
            {"OutstandingPrincipalBalance", "sSpLien"},
            {"FhaCondoApprovalStatus", "sFhaCondoApprovalStatusT"},
            {"FhaLoanLimit", "sFhaLoanLimit"},
            {"FannieLTV", "sFannieLtvR"},
            {"FannieCLTV", "sFannieCltvR"},
            {"FannieHCLTV", "sFannieHcltvR"},
            {"IsTpoLoan", "sIsTPO_UseInKeyword"},
            {"UFMIPRefund","sFHASalesConcessions"},
            {"HasAppraisal","sHasAppraisal"},
            {"OriginalAppraisedValue","sOriginalAppraisedValue"},
            {"OriginatorCompSource", "sOriginatorCompensationPaymentSourceT"},
            {"HasSubjPropPriorSaleData", "sHasSubjPropPriorSaleData" },
            {"PropertySeasoningDays", "sPropertySeasoningDays"},
            {"PropertySeasoningMonths", "sPropertySeasoningMonths"}, 
            {"PriorSalesPrice", "sPriorSalesPrice"},
            {"PropertySeller", "sPriorSalesPropertySellerT"},
            {"SalesPrice", "sHouseVal" },
            {"LTV_USDA", "sLtvRUsda"},
            {"CLTV_USDA", "sCLtvRUsda" },
            {"CreditReportAge", "creditData.CreditReportAge"},
            {"LoanStatus", "sStatusT"},
            {"InvestorID", "sLpInvestorId"},
            {"IsFinancedUFMIPIncludedInLTV", "sIncludeUfmipInLtvCalc"},
            {"ARM1stAdjCap","sRAdj1stCapR"},
            {"ARMNextAdjCap","sRAdjCapR"},
            {"ARMLifeAdjCap","sRAdjLifeCapR"},
            {"ARMAdjPeriod","sRAdjCapMon"},
            {"ARMIndex","sMasterArmIndexT"},
            {"Assumption", "sAssumeLT"},
            {"IsConvertibleMortgage", "sIsConvertibleMortgage"},
            {"InterestOnlyMonths", "sIOnlyMon"},
            {"NumberOfFinancedProperties", "sNumFinancedProperties"},
            {"HELOCLineAmount", "sSubFinPe"},
            {"HCLTV", "sHCLTVRPe"},
            {"IsRenovation", "sIsRenovationLoan"},
            {"RenovationCosts", "sTotalRenovationCosts"},
            {"AsCompletedValue", "sAsCompletedValueKeyword"},
            {"NoteRateAfterLOCompAndRounding", "sNoteRateAfterLOCompAndRounding"},
            {"NoteRateBeforeLOCompAndRounding", "sNoteRateBeforeLOCompAndRounding"},
            {"PointsBeforeLOCompAndRounding", "sPointsBeforeLOCompAndRounding"},
            {"NoteRateIsIntegerMultipleOfOneEighth", "sNoteRateIsIntegerMultipleOfOneEighth"},
            {"LenderID", "sLenderID" },
            {"ZipCode", "sSpZipKeyword" },
            {"MaxNoteRateOnLoanFile", "sMaxRKeyword" },
            { "PMI_Type", "sProdConvMIOptionT" },
            { "SplitMI_Upfront","sConvSplitMIRT" },
            { "IsUFMIPorFFFinanced","sProdIsFhaMipFinanced" },
            { "LenderIsACreditUnion", "BrokerDB"},
            { "VAEligibleBorrOnFile", "sVAEligibleBorrOnFile"},
            { "CustomPMLField_1", "sCustomPMLField1" },
            { "CustomPMLField_2", "sCustomPMLField2" },
            { "CustomPMLField_3", "sCustomPMLField3" },
            { "CustomPMLField_4", "sCustomPMLField4" },
            { "CustomPMLField_5", "sCustomPMLField5" },
            { "CustomPMLField_6", "sCustomPMLField6" },
            { "CustomPMLField_7", "sCustomPMLField7" },
            { "CustomPMLField_8", "sCustomPMLField8" },
            { "CustomPMLField_9", "sCustomPMLField9" },
            { "CustomPMLField_10", "sCustomPMLField10" },
            { "CustomPMLField_11", "sCustomPMLField11" },
            { "CustomPMLField_12", "sCustomPMLField12" },
            { "CustomPMLField_13", "sCustomPMLField13" },
            { "CustomPMLField_14", "sCustomPMLField14" },
            { "CustomPMLField_15", "sCustomPMLField15" },
            { "CustomPMLField_16", "sCustomPMLField16" },
            { "CustomPMLField_17", "sCustomPMLField17" },
            { "CustomPMLField_18", "sCustomPMLField18" },
            { "CustomPMLField_19", "sCustomPMLField19" },
            { "CustomPMLField_20", "sCustomPMLField20" },
            { "TotalLiquidAssets", "sAppTotLiqAsset" },
            { "RateOptionExceedsMaxDTI", "sRateOptionExceedsMaxDti"},
            { "LeadSourceId", "sLeadSrcId" },
            { "IsEmployeeLoan", "sIsEmployeeLoan" },
            { "RunPmlRequestSourceType", "RunPmlRequestSourceT"},
            { "LoanRegisteredDate", "sLoanRegisteredDateForPricing"},
            { "CompPlanEffectiveDate", "sCompPlanEffectiveDateForPricing"},
            { "APOR", "sQMAveragePrimeOfferRCalc" },
            { "IsBaseLAMTConformingLAmt", "sIsBaseLAmtConformingLAmt" },
            { "IsBaseLAMTHighBalanceConformingLAMT", "sIsBaseLAmtHighBalanceConformingLAmt" },
            { "LoanHasNonExpiredApproval", "sLoanHasNonExpiredApproval" },
            { "CustomLOPPField_1", "sCustomPricingPolicyField1" },
            { "CustomLOPPField_2", "sCustomPricingPolicyField2" },
            { "CustomLOPPField_3", "sCustomPricingPolicyField3" },
            { "CustomLOPPField_4", "sCustomPricingPolicyField4" },
            { "CustomLOPPField_5", "sCustomPricingPolicyField5" },
            { "CustomLoanProgramField_1", "sLpTemplateId"}, // 6/28/2014 dd - The LP Custom Keyword is variant keyword.
            { "CustomLoanProgramField_2", "sLpTemplateId"},
            { "CustomLoanProgramField_3", "sLpTemplateId"},
            { "CustomLoanProgramField_4", "sLpTemplateId"},
            { "CustomLoanProgramField_5", "sLpTemplateId"},
            { "InvestorLoanProgramField_1", "sLpTemplateId"},
            { "InvestorLoanProgramField_2", "sLpTemplateId"},
            { "InvestorLoanProgramField_3", "sLpTemplateId"},
            { "InvestorLoanProgramField_4", "sLpTemplateId"},
            { "InvestorLoanProgramField_5", "sLpTemplateId"},
            { "BranchChannel", "sBranchChannelT"},
            { "CorrespondentProcessType", "sCorrespondentProcessT"},
            { "LenderFeeBuyout", "sLenderFeeBuyoutRequestedT"},
            { "AppraisedValue", "sAppraisedValueKeyword"},
            { "RateOptionFinalPrice", "sRateOptionFinalPrice"},
            { "RateOptionPointsPreFEMaxYSP", "sRateOptionPointsPreFEMaxYSP"},
            { "LOOCCompanyIdFirstFourDigits", "sEmployeeLoanRepOriginatingCompanyId_FirstFourDigits"},
            { "FHAEndorsedBeforeJune2009", "sProdIsLoanEndorsedBeforeJune09" },
            { "OCIsDelegatedCorrespondent", "sIsOriginatingCompanyDelegatedCorrespondent" },
            { "OriginatingCompanyTierID", "sPmlCompanyTierId" },
            { "BorrowerType", "appData.aBorrowerTypeT" },
            { "CashToBorrower", "sCashToBorrower" },
            { "HasMortgageLiabilityThatWillBePaidOffAtClosing", "sHasMortgageLiabilityThatWillBePaidOffAtClosing" },
            { "ScoreCBS", "appData.aScoreCBS" },
            { "LoanType", "sLT" },
            { "BranchIDNumber", "sBranchIdNumber" },
            { "TotalCashDeposit", "sTotCashDeposit" },
            { "2ndLienIsHeloc", "s2ndLienIsHeloc" },
            { "FannieHomebuyerEducationType", "sFannieHomebuyerEducationT" },
            { "2ndFinancingInclusionType", "sSecondFinancingInclusionT" },
            { "IsStudentLoanCashoutRefi", "sIsStudentLoanCashoutRefi" },
            { "NotPermanentlyAffixed", "sIsNotPermanentlyAffixed" },

            { "RenovationLTV", "sLtvR" }, 
            { "RenovationCLTV", "sCltvR" },
            { "RenovationHCLTV", "sHcltvR" },

            { "Fha203KType", "sFha203KType" },
            { "CostConstrRepairsRehab", "sCostConstrRepairsRehab" },
            { "ArchitectEngineerFee", "sArchitectEngineerFee" },
            { "ConsultantFee", "sConsultantFee" },
            { "RenovationConstrInspectFee", "sRenovationConstrInspectFee" },
            { "TitleUpdateFee", "sTitleUpdateFee" },
            { "PermitFee", "sPermitFee" },
            { "FeasibilityStudyFee", "sFeasibilityStudyFee" },
            { "RepairImprovementCostFee", "sRepairImprovementCostFee" },
            { "FinanceContingencyReserve", "sFinanceContingencyRsrv" },
            { "LoanFileType", "sLoanFileT" },
            { "OriginatingCompanyIndexNumber", "sOriginatingCompanyIndexNumber" },
            { "RequestCommunityOrAffordableSeconds", "sRequestCommunityOrAffordableSeconds" },

            { "ConventionalLtvForMiNeed", "sConvLoanLtvForMiNeed" }

        };

    }

    
    public interface IGetSymbolTable
    {
        CSymbolTable sSymbolTableForPriceRule
        {
            get;
        }
    }

    public class CSymbolTable
    {
        public delegate decimal Evaluator(XmlNode element, int indent, CSymbolTable symbolTable, PostfixExpression postfixExpr);
        public delegate int		Function( int months );
        public delegate decimal	FunctionStrParams( string str );
        public delegate decimal	Get();  // This one helps us delaying the calculation in loan data object unless we hit a rule that needs the particular keyword.

        static private CaseInsensitiveHashtable s_preloadEntries; 
        static private CaseInsensitiveHashtable s_evaluatorEntries;
        static private CaseInsensitiveHashtable s_OfficialKeywords = new CaseInsensitiveHashtable();    
        static private bool                     s_fastAccessEvaluators   = true;

        readonly
            static private int       s_DefaultSymbolTableSize = 332; // readonly (except static constructor can edit it)


        private Hashtable m_entriesTable = new Hashtable();
        private Hashtable m_inputDataTable = new Hashtable();
        private int m_activeApplication = 0;

        private CPageBase       m_loanData; //  basically this is CPriceEngineData, the field to edit the dependency list is sSymbolTableForPriceRule

        private static void AddEnumConstants(Hashtable hash, string keyword, object[,] enumMappings) 
        {
            for (int i = 0; i < enumMappings.Length / 2; i++) 
            {
                Enum e = (Enum) enumMappings[i, 0];
                string v = (string) enumMappings[i, 1];
                AddEntry(hash, new CSymbolFieldValue((int) E_Purpose.Condition, v, e, keyword));
            }
        }

        #region Enum Mappings
        private static object[,] E_sTotalScoreLdpOrGsaExclusion_SymbolMap = {
                                                                                { E_sTotalScoreLdpOrGsaExclusionT.Unknown, "LdpOrGsaExclusion_Unknown"},
                                                                                { E_sTotalScoreLdpOrGsaExclusionT.Excluded, "LdpOrGsaExclusion_Excluded"},
                                                                                { E_sTotalScoreLdpOrGsaExclusionT.NotExcluded, "LdpOrGsaExclusion_NotExcluded"}
                                                                            };
        private static object[,] E_sTotalScoreCreditRiskResultT_SymbolMap = {
                                                                                {E_sTotalScoreCreditRiskResultT.Unknown, "TotalCreditRiskResultType_Unknown"},
                                                                                {E_sTotalScoreCreditRiskResultT.Approve, "TotalCreditRiskResultType_Approve"},
                                                                                {E_sTotalScoreCreditRiskResultT.Refer, "TotalCreditRiskResultType_Refer"}
                                                                            };
        private static object[,] E_sTotalScoreFhaProductT_SymbolMap = {
                                                                          { E_sTotalScoreFhaProductT.LeaveBlank, "FhaProductType_LeaveBlank"},
                                                                          { E_sTotalScoreFhaProductT.Standard, "FhaProductType_Standard"},
                                                                          { E_sTotalScoreFhaProductT.Rehabilitation, "FhaProductType_Rehabilitation203k251"},
                                                                          { E_sTotalScoreFhaProductT.HOPEForHomeowners, "FhaProductType_HopeForHomeowners257"},
                                                                          { E_sTotalScoreFhaProductT.DisasterVictims, "FhaProductType_DisasterVictims203h"},
                                                                          { E_sTotalScoreFhaProductT.Other, "FhaProductType_Other"}
                                                                      };
        private static object[,] E_sProdDocT_SymbolMap = {
                                                               {E_sProdDocT.Full   , "DocType_Full"}
                                                             , {E_sProdDocT.Alt    , "DocType_Alt"}
                                                             , {E_sProdDocT.Light  , "DocType_Light"}
                                                             , {E_sProdDocT.SIVA   , "DocType_SIVA"}
                                                             , {E_sProdDocT.VISA   , "DocType_VISA"}
                                                             , {E_sProdDocT.SISA   , "DocType_SISA"}
                                                             , {E_sProdDocT.NIVA   , "DocType_NIVA"}
                                                             , {E_sProdDocT.NINA   , "DocType_NINA"}
                                                             , {E_sProdDocT.NINANE , "DocType_NINANE"}
                                                             , {E_sProdDocT.NISA   , "DocType_NISA"}
                                                             , {E_sProdDocT.NIVANE , "DocType_NIVANE"}
                                                             , {E_sProdDocT.VINA   , "DocType_VINA"}
                                                             , {E_sProdDocT.Streamline, "DocType_Streamline"}
                                                             , {E_sProdDocT._12MoPersonalBankStatements, "DocType_12MoPersonalBankStatements"}
                                                             , {E_sProdDocT._24MoPersonalBankStatements, "DocType_24MoPersonalBankStatements"}
                                                             , {E_sProdDocT._12MoBusinessBankStatements, "DocType_12MoBusinessBankStatements"}
                                                             , {E_sProdDocT._24MoBusinessBankStatements, "DocType_24MoBusinessBankStatements"}
                                                             , {E_sProdDocT.OtherBankStatements, "DocType_OtherBankStatements"}
                                                             , {E_sProdDocT._1YrTaxReturns, "DocType_1YrTaxReturns"}
                                                             , {E_sProdDocT.Voe, "DocType_VOE"}
                                                             , {E_sProdDocT.AssetUtilization, "DocType_AssetUtilization"}
                                                             , {E_sProdDocT.DebtServiceCoverage, "DocType_DebtServiceCoverage"}
                                                             , {E_sProdDocT.NoIncome, "DocType_NoIncome"}
                                                         };
        private static object[,] E_sFHASecondaryFinancingSourceT_SymbolMap = {
                                                                                 { E_sFHASecondaryFinancingSourceT.LeaveBlank, "SubFinType_LeaveBlank"},
                                                                                 { E_sFHASecondaryFinancingSourceT.Government, "SubFinType_Government"},
                                                                                 { E_sFHASecondaryFinancingSourceT.Family, "SubFinType_Family"},
                                                                                 { E_sFHASecondaryFinancingSourceT.NonProfit, "SubFinType_NonProfit"},
                                                                                 { E_sFHASecondaryFinancingSourceT.Other, "SubFinType_Other"},
                                                                             };

        private static object[,] E_sProdMIOptionT_SymbolMap = {
                                                                    {E_sProdMIOptionT.LeaveBlank, "MIOption_LeaveBlank"}
                                                                  , {E_sProdMIOptionT.NoPmi, "MIOption_NoPmi"}
                                                                  , {E_sProdMIOptionT.BorrowerPdPmi, "MIOption_BorrowerPdPmi"}
                                                              };

        private static object[,] E_sLienPosT_SymbolMap = {
                                                               {E_sLienPosT.First  , "LienPosition_First"}
                                                             , {E_sLienPosT.Second , "LienPosition_Second"}
                                                         };

        private static object[,] E_aOccT_SymbolMap = {
                                                           {E_aOccT.Investment         , "PropertyPurpose_Investment"}
                                                         , {E_aOccT.PrimaryResidence   , "PropertyPurpose_PrimaryResidence"}
                                                         , {E_aOccT.SecondaryResidence , "PropertyPurpose_SecondaryResidence"}
                                                     };

        private static object[,] E_sFinMethT_SymbolMap = {
                                                               {E_sFinMethT.ARM       ,  "AmortizationType_ARM"}
                                                             , {E_sFinMethT.Fixed     ,  "AmortizationType_Fixed"}
                                                             , {E_sFinMethT.Graduated ,  "AmortizationType_Graduated"}
                                                         };

        private static object[,] E_sLoanFileT_SymbolMap = {
                                                               {E_sLoanFileT.Loan       ,  "LoanFileType_LoanFile"}
                                                             , {E_sLoanFileT.Sandbox     ,  "LoanFileType_SandboxFile"}
                                                             , {E_sLoanFileT.QuickPricer2Sandboxed ,  "LoanFileType_QP2File"}
                                                             , {E_sLoanFileT.Test, "LoanFileType_TestFile" }
                                                         };

        private static object[,] E_sFinMethodOtherLoan_SymbolMap = {
                                                                         {E_sFinMethT.ARM   , "AmortTypeOtherLoan_ARM"}
                                                                       , {E_sFinMethT.Fixed , "AmortTypeOtherLoan_Fixed"}
                                                                       , {E_sFinMethT.Graduated, "AmortTypeOtherLoan_Graduated"}
                                                                   };

        private static object[,] E_BkMultipleT_SymbolMap = {
                                                               {E_BkMultipleT.None             , "BkMultipleType_None"}
                                                               ,{E_BkMultipleT.PossibleRollOver , "BkMultipleType_PossibleRollOver"}
                                                               ,{E_BkMultipleT.Multiple         , "BkMultipleType_Multiple"}
                                                           };

        private static object[,] E_aProdCitizenT_SymbolMap = {
                                                                 {E_aProdCitizenT.USCitizen            , "CitizenshipType_USCitizen"}
                                                                 ,{E_aProdCitizenT.PermanentResident    , "CitizenshipType_PermanentResident"}
                                                                 ,{E_aProdCitizenT.NonpermanentResident , "CitizenshipType_NonpermanentResident"}
                                                                 ,{E_aProdCitizenT.ForeignNational      , "CitizenshipType_ForeignNational"}
                                                             };

        private static object[,] E_sProdSpStructureT_SymbolMap = {
                                                                      {E_sProdSpStructureT.Attached , "PropertyStructType_Attached"}
                                                                     ,{E_sProdSpStructureT.Detached , "PropertyStructType_Detached"}
                                                                 };

        private static object[,] E_sProdSpT_SymbolMap = {
                                                             {E_sProdSpT.SFR          , "PropertyType_SFR"}
                                                            ,{E_sProdSpT.PUD          , "PropertyType_PUD"}
                                                            ,{E_sProdSpT.Condo        , "PropertyType_Condo"}
                                                            ,{E_sProdSpT.CoOp         , "PropertyType_CoOp"}
                                                            ,{E_sProdSpT.Manufactured , "PropertyType_Manufactured"}
                                                            ,{E_sProdSpT.Townhouse    , "PropertyType_Townhouse"}
                                                            ,{E_sProdSpT.Commercial   , "PropertyType_Commercial"}
                                                            ,{E_sProdSpT.MixedUse     , "PropertyType_MixedUse"}
                                                            ,{E_sProdSpT.TwoUnits     , "PropertyType_2Units"}
                                                            ,{E_sProdSpT.ThreeUnits   , "PropertyType_3Units"}
                                                            ,{E_sProdSpT.FourUnits    , "PropertyType_4Units"}
                                                            ,{E_sProdSpT.Modular      , "PropertyType_Modular"}
                                                            ,{E_sProdSpT.Rowhouse     , "PropertyType_Rowhouse"}
                                                        };

        private static object[,] E_IsMhAdvantageHome_SymbolMap = {
                                                            {E_TriState.Blank, "IsMhAdvantageHome_NA"},
                                                            {E_TriState.Yes, "IsMhAdvantageHome_Yes"},
                                                            {E_TriState.No, "IsMhAdvantageHome_No"}
        };

        private static object[,] E_sLPurposeT_SymbolMap = {
                                                              {E_sLPurposeT.Purchase       ,"PurposeType_Purchase"}
                                                              ,{E_sLPurposeT.Refin         , "PurposeType_Refin"}
                                                              ,{E_sLPurposeT.RefinCashout  , "PurposeType_RefinCashout"}
                                                              ,{E_sLPurposeT.Construct     , "PurposeType_Construct"}
                                                              ,{E_sLPurposeT.ConstructPerm , "PurposeType_ConstructPerm"}
                                                              ,{E_sLPurposeT.Other         , "PurposeType_Other"}
                                                              ,{E_sLPurposeT.FhaStreamlinedRefinance, "PurposeType_FHAStreamlinedRefinance"}
                                                              ,{E_sLPurposeT.VaIrrrl       , "PurposeType_VAIRRRL"}
                                                              ,{E_sLPurposeT.HomeEquity, "PurposeType_HomeEquity"}
                                                          };

        private static object[,] E_sProd3rdPartyUwResultT_SymbolMap = {
                                                                          {E_sProd3rdPartyUwResultT.NA                         , "ThirdPartyUwResultT_NA"}
                                                                          ,{E_sProd3rdPartyUwResultT.DU_ApproveEligible         , "ThirdPartyUwResultT_DUApproveEligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.DU_ApproveIneligible       , "ThirdPartyUwResultT_DUApproveIneligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.DU_ReferEligible           , "ThirdPartyUwResultT_DUReferEligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.DU_ReferIneligible         , "ThirdPartyUwResultT_DUReferIneligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible   , "ThirdPartyUwResultT_DUReferWCautionEligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible , "ThirdPartyUwResultT_DUReferWCautionIneligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.DU_EAIEligible             , "ThirdPartyUwResultT_DUEAIEligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.DU_EAIIEligible           , "ThirdPartyUwResultT_DUEAIIEligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.DU_EAIIIEligible           , "ThirdPartyUwResultT_DUEAIIIEligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.LP_AcceptEligible          , "ThirdPartyUwResultT_LPAcceptEligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.LP_AcceptIneligible        , "ThirdPartyUwResultT_LPAcceptIneligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.Lp_AMinus_Level1           , "ThirdPartyUwResultT_LpAMinusLevel1"}
                                                                          ,{E_sProd3rdPartyUwResultT.Lp_AMinus_Level2           , "ThirdPartyUwResultT_LpAMinusLevel2"}
                                                                          ,{E_sProd3rdPartyUwResultT.Lp_AMinus_Level3           , "ThirdPartyUwResultT_LpAMinusLevel3"}
                                                                          ,{E_sProd3rdPartyUwResultT.Lp_AMinus_Level4           , "ThirdPartyUwResultT_LpAMinusLevel4"}
                                                                          ,{E_sProd3rdPartyUwResultT.Lp_AMinus_Level5           , "ThirdPartyUwResultT_LpAMinusLevel5"}
                                                                          ,{E_sProd3rdPartyUwResultT.LP_CautionEligible         , "ThirdPartyUwResultT_LPCautionEligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.LP_CautionIneligible       , "ThirdPartyUwResultT_LPCautionIneligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.OutOfScope                 , "ThirdPartyUwResultT_OutOfScope"}
                                                                          ,{E_sProd3rdPartyUwResultT.Lp_Refer                   , "ThirdPartyUwResultT_LpRefer"}
                                                                          ,{E_sProd3rdPartyUwResultT.Total_ApproveEligible, "ThirdPartyUwResultT_TotalApproveEligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.Total_ApproveIneligible, "ThirdPartyUwResultT_TotalApproveIneligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.Total_ReferEligible, "ThirdPartyUwResultT_TotalReferEligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.Total_ReferIneligible, "ThirdPartyUwResultT_TotalReferIneligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.GUS_AcceptEligible, "ThirdPartyUwResultT_GUSAcceptEligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.GUS_AcceptIneligible, "ThirdPartyUwResultT_GUSAcceptIneligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.GUS_ReferEligible, "ThirdPartyUwResultT_GUSReferEligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.GUS_ReferIneligible, "ThirdPartyUwResultT_GUSReferIneligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.GUS_ReferWCautionEligible, "ThirdPartyUwResultT_GUSReferWCautionEligible"}
                                                                          ,{E_sProd3rdPartyUwResultT.GUS_ReferWCautionIneligible, "ThirdPartyUwResultT_GUSReferWCautionIneligible"}
                                                                      };

        private static object[,] E_sProd3rdPartyUwProcessingT_SymbolMap = {
                                                                              {E_sProd3rdPartyUwProcessingT.Normal, "ThirdPartyUwProcessingT_Normal"}
                                                                              ,{E_sProd3rdPartyUwProcessingT.DU_MyCommunity, "ThirdPartyUwProcessingT_DUMyCommunity"}
                                                                              ,{E_sProd3rdPartyUwProcessingT.LP_HomePossible, "ThirdPartyUwProcessingT_LPHomePossible"}
                                                                          };
        private static object[,] E_sLpProductT_SymbolMap = {
                                                               {E_sLpProductT.LeaveBlank, "LoanProductType_LeaveBlank"},
                                                               {E_sLpProductT.Conforming, "LoanProductType_Conforming"},
                                                               {E_sLpProductT.Subprime, "LoanProductType_Subprime"},
                                                               {E_sLpProductT.Rural, "LoanProductType_Rural"},
                                                               {E_sLpProductT.Fha, "LoanProductType_Fha"},
                                                               {E_sLpProductT.CES, "LoanProductType_CES"},
                                                               {E_sLpProductT.NonConforming, "LoanProductType_NonConforming"},
                                                               {E_sLpProductT.ManualUW, "LoanProductType_ManualUW"},
                                                               {E_sLpProductT.HomePath, "LoanProductType_HomePath"},
                                                               {E_sLpProductT.VA, "LoanProductType_VA"},
                                                               {E_sLpProductT.AltA, "LoanProductType_AltA"},
                                                               {E_sLpProductT.Heloc, "LoanProductType_Heloc"},
                                                               {E_sLpProductT.Second, "LoanProductType_Second"},
                                                               {E_sLpProductT.FhlmcReliefRefi, "LoanProductType_FhlmcReliefRefi"},
                                                               {E_sLpProductT.Fha203k, "LoanProductType_Fha203k"},
                                                               {E_sLpProductT.Flex97, "LoanProductType_Flex97"},
                                                               {E_sLpProductT.Alt97, "LoanProductType_Alt97"},
                                                               {E_sLpProductT.Fha203ks, "LoanProductType_Fha203ks"},
                                                               {E_sLpProductT.HomePathRenovation, "LoanProductType_HomePathRenovation"},
                                                               {E_sLpProductT.Fha100Down, "LoanProductType_Fha100Down"},
                                                               {E_sLpProductT.Fha203k100Down, "LoanProductType_Fha203k100Down"},
                                                               {E_sLpProductT.Fha203ks100Down, "LoanProductType_Fha203ks100Down"},
                                                               {E_sLpProductT.MyCommunity, "LoanProductType_MyCommunity"},
                                                               {E_sLpProductT.HomePossible, "LoanProductType_HomePossible"},
                                                               {E_sLpProductT.Fha1xClose, "LoanProductType_Fha1xClose" },
                                                               {E_sLpProductT.HomestyleRenovation, "LoanProductType_HomestyleRenovation"},
                                                               {E_sLpProductT.HomeEquity, "LoanProductType_HomeEquity" },
                                                               {E_sLpProductT.ConformingTVLB, "LoanProductType_ConformingTVLB" },
                                                               {E_sLpProductT.FhaTVLB, "LoanProductType_FhaTVLB" },
                                                               {E_sLpProductT.VaTVLB, "LoanProductType_VaTVLB" },
                                                               {E_sLpProductT.FhaRepairEscrow, "LoanProductType_FhaRepairEscrow"},
                                                               //{E_sLpProductT.FhaPurchase90, "LoanProductType_FhaPurchase90"}      // if they need it in future then remember to say "I told you so" in the OPM.
                                                               {E_sLpProductT.AIO, "LoanProductType_AIO"}, // 2/25/2014 - dd - OPM 169704
                                                               {E_sLpProductT.ZeroInterestProgram, "LoanProductType_ZeroInterestProgram" },
                                                               {E_sLpProductT.HOMEREADY, "LoanProductType_HOMEREADY"},
                                                               {E_sLpProductT.Bond, "LoanProductType_Bond"},
                                                               {E_sLpProductT.ConformingCEMA, "LoanProductType_ConformingCEMA"},
                                                               {E_sLpProductT.ConstructionEndLoan, "LoanProductType_ConstructionEndLoan"},
                                                               {E_sLpProductT.ConstructionDraw, "LoanProductType_ConstructionDraw"},
                                                               {E_sLpProductT.YESMortgage, "LoanProductType_YESMortgage"},
                                                               {E_sLpProductT.WESLENDSELECT, "LoanProductType_WESLENDSELECT"},
                                                               {E_sLpProductT.ConventionalBond, "LoanProductType_ConventionalBond"},
                                                               {E_sLpProductT.FHABond, "LoanProductType_FHABond"},
                                                               {E_sLpProductT.VABond, "LoanProductType_VABond"},
                                                               {E_sLpProductT.USDABond, "LoanProductType_USDABond"},
                                                               {E_sLpProductT.Va1xClose, "LoanProductType_VA1xClose"},
                                                               {E_sLpProductT.COMBO, "LoanProductType_COMBO"},
                                                               {E_sLpProductT.UsdaStreamlineAssistRefi, "LoanProductType_USDAStreamlineAssistRefi"},
                                                               {E_sLpProductT.FHAEEM, "LoanProductType_FHAEEM" }, // ejm opm 246231
                                                               {E_sLpProductT.VAEEM, "LoanProductType_VAEEM" }, // ejm opm 246231
                                                               {E_sLpProductT.ExpandedPlus, "LoanProductType_ExpandedPlus" }, // jl - opm 246584
                                                               {E_sLpProductT.JumboFlex, "LoanProductType_JumboFlex" }, // jl - opm 247122
                                                               {E_sLpProductT.Fha203kBond, "LoanProductType_Fha203kBond" }, // jl - opm 245595
                                                               {E_sLpProductT.AltAPlatinum, "LoanProductType_AltAPlatinum" }, // jl - opm 247338
                                                               {E_sLpProductT.AltADiamond, "LoanProductType_AltADiamond" },
                                                               {E_sLpProductT.AltADiamondPlus, "LoanProductType_AltADiamondPlus" },
                                                               {E_sLpProductT.AltAUltraPrime, "LoanProductType_AltAUltraPrime" },
                                                               {E_sLpProductT.ConvMcc, "LoanProductType_CONVMCC" },
                                                               {E_sLpProductT.NonconfNewport, "LoanProductType_NONCONFNEWPORT" },
                                                               {E_sLpProductT.Fha203h, "LoanProductType_Fha203h" },
                                                               {E_sLpProductT.DelegatedJumbo, "LoanProductType_DelegatedJumbo" },
                                                               {E_sLpProductT.NonprimeBridgeResidential, "LoanProductType_NonprimeBridgeResidential"},
                                                               {E_sLpProductT.NonprimeBridgeCommercial, "LoanProductType_NonprimeBridgeCommercial"},
                                                               {E_sLpProductT.NonprimeBusinessResidential, "LoanProductType_NonprimeBusinessResidential"},
                                                               {E_sLpProductT.NonprimeBusinessCommercial, "LoanProductType_NonprimeBusinessCommercial"},
                                                               {E_sLpProductT.NonprimeConsumer, "LoanProductType_NonprimeConsumer"},
                                                               {E_sLpProductT.HomestyleEnergy, "LoanProductType_HomestyleEnergy" },
                                                               {E_sLpProductT.ServiceRetained, "LoanProductType_ServiceRetained" },
                                                               {E_sLpProductT.AltASilver, "LoanProductType_AltASilver" },
                                                               {E_sLpProductT.SixMoSeasoningVa, "LoanProductType_6MoSeasoningVa" },
                                                               {E_sLpProductT.ConvMedicalProfessional, "LoanProductType_ConvMedicalProfessional" },
                                                               {E_sLpProductT._1ServiceRetained, "LoanProductType_1ServiceRetained" },
                                                               {E_sLpProductT.HomePossAdvPlus1PctGrant, "LoanProductType_HomePossAdvPlus1PctGrant" },
                                                               {E_sLpProductT.HomePossAdvPlus2PctGrant, "LoanProductType_HomePossAdvPlus2PctGrant" },
                                                               {E_sLpProductT.HomestyleEnergyImprovements, "LoanProductType_HomestyleEnergyImprovements" },
                                                               {E_sLpProductT.ConformingDeluxe, "LoanProductType_ConformingDeluxe" },
                                                               {E_sLpProductT.Section184, "LoanProductType_Section184" },
                                                               {E_sLpProductT.VaRenovation, "LoanProductType_VARenovation" },
                                                               {E_sLpProductT.ConfFthbValorBond, "LoanProductType_ConfFthbValorBond" },
                                                               {E_sLpProductT.CalhfaEem, "LoanProductType_CalhfaEem" },
                                                               {E_sLpProductT.PiggybackCombo, "LoanProductType_PiggybackCombo" },
                                                               {E_sLpProductT.EdgeMortgage, "LoanProductType_EdgeMortgage" },
                                                               {E_sLpProductT.Usda1xClose, "LoanProductType_Usda1xClose" },
                                                               {E_sLpProductT.Advantage, "LoanProductType_Advantage" },
                                                               {E_sLpProductT.HomeFront, "LoanProductType_HomeFront" },
                                                               {E_sLpProductT.UsdaRepairEscrow, "LoanProductType_UsdaRepairEscrow" },
                                                               {E_sLpProductT.ElitePlus, "LoanProductType_ELITEPLUS" },
                                                               {E_sLpProductT.DpaAdvantage203k, "LoanProductType_DpaAdvantage203k" },
                                                               {E_sLpProductT.DpaAdvantage203ks, "LoanProductType_DpaAdvantage203ks" },
                                                               {E_sLpProductT.DpaAdvantageRepairEscrow, "LoanProductType_DpaAdvantageRepairEscrow" },
                                                               {E_sLpProductT.TxA6Fhlmc, "LoanProductType_TxA6Fhlmc" },
                                                               {E_sLpProductT.TxA6Fnma, "LoanProductType_TxA6Fnma" },
                                                               {E_sLpProductT.TxA4Fhlmc, "LoanProductType_TxA4Fhlmc" },
                                                               {E_sLpProductT.TxA4Fnma, "LoanProductType_TxA4Fnma" },
                                                               {E_sLpProductT.Tx50A6, "LoanProductType_Tx50A6" },
                                                               {E_sLpProductT.Tx50A4, "LoanProductType_Tx50A4" },
                                                               {E_sLpProductT.DpaAdvantage, "LoanProductType_DpaAdvantage" },
                                                               {E_sLpProductT.HardmoneyCommercial, "LoanProductType_HardmoneyCommercial" },
                                                               {E_sLpProductT.HardmoneyResidential, "LoanProductType_HardmoneyResidential" },
                                                               {E_sLpProductT.HardmoneyBackEastResidential, "LoanProductType_HardmoneyBackEastResidential" },
                                                               {E_sLpProductT.AltABusinessResidential, "LoanProductType_AltABusinessResidential" },
                                                               {E_sLpProductT.Portfolio, "LoanProductType_Portfolio" },
                                                               {E_sLpProductT.HomeOne, "LoanProductType_HomeOne" },
                                                               {E_sLpProductT.Pivot, "LoanProductType_Pivot" },
                                                               {E_sLpProductT.Simple, "LoanProductType_Simple" },
                                                               {E_sLpProductT.HbBiscayne, "LoanProductType_HbBiscayne" },
                                                               {E_sLpProductT.LotLoan, "LoanProductType_LotLoan" },
                                                               {E_sLpProductT.Renovation, "LoanProductType_Renovation" },
                                                               {E_sLpProductT.HighLtvRefi, "LoanProductType_HighLtvRefi" },
                                                               {E_sLpProductT.EnhancedReliefRefi, "LoanProductType_EnhancedReliefRefi" },
                                                               {E_sLpProductT.FthbValorHfaPreferred, "LoanProductType_FthbValorHfaPreferred" },
                                                               {E_sLpProductT.Brokered, "LoanProductType_Brokered" },
                                                               {E_sLpProductT.HfaPreferred, "LoanProductType_HfaPreferred" },
                                                               {E_sLpProductT.FhaDpaOtc, "LoanProductType_FhaDpaOtc" },
                                                           };
        private static object[,] E_sH4HIsAllReoInheritedTri_SymbolMap = {
                                                                            {E_TriState.Blank, "IsAllReoInherited_NA"},
                                                                            {E_TriState.Yes, "IsAllReoInherited_Yes"},
                                                                            {E_TriState.No, "IsAllReoInherited_No"}
                                                                        };
        private static object[,] E_aHasFraudConvictionT_SymbolMap = {
                                                                            {E_aHasFraudConvictionT.Unknown, "BorrowerHasFraudConviction_Unknown"},
                                                                            {E_aHasFraudConvictionT.Yes, "BorrowerHasFraudConviction_Yes"},
                                                                            {E_aHasFraudConvictionT.No, "BorrowerHasFraudConviction_No"},
                                                                            {E_aHasFraudConvictionT.Blank, "BorrowerHasFraudConviction_Blank"}
                                                                        };
        private static object[,] E_sH4HOriginationReqMetT_SymbolMap = {
                                                                            {E_sH4HOriginationReqMetT.No, "H4HOriginationReqMet_No"},
                                                                            {E_sH4HOriginationReqMetT.Yes, "H4HOriginationReqMet_Yes"},
                                                                            {E_sH4HOriginationReqMetT.Blank, "H4HOriginationReqMet_Blank"},
                                                                        };
        private static object[,] E_sNonOccCoBorrOnOrigNoteT_SymbolMap = {
                                                                            {E_sNonOccCoBorrOnOrigNoteT.Unknown, "NonOccCoBorrOnOrigNote_Unknown"},
                                                                            {E_sNonOccCoBorrOnOrigNoteT.Yes, "NonOccCoBorrOnOrigNote_Yes"},
                                                                            {E_sNonOccCoBorrOnOrigNoteT.No, "NonOccCoBorrOnOrigNote_No"},
                                                                            {E_sNonOccCoBorrOnOrigNoteT.Blank, "NonOccCoBorrOnOrigNote_Blank"}
                                                                        };
        private static object[,] E_sFhaCondoApprovalStatusT_SymbolMap = {
                                                                         { E_sFhaCondoApprovalStatusT.LeaveBlank, "FhaCondoApprovalStatus_Blank"},
                                                                         { E_sFhaCondoApprovalStatusT.Approved, "FhaCondoApprovalStatus_Approved"},
                                                                         { E_sFhaCondoApprovalStatusT.Pending, "FhaCondoApprovalStatus_Pending"},
                                                                         { E_sFhaCondoApprovalStatusT.Rejected, "FhaCondoApprovalStatus_Rejected"},
                                                                         { E_sFhaCondoApprovalStatusT.Withdrawn, "FhaCondoApprovalStatus_Withdrawn"}
                                                                     };
        private static object[,] E_sOriginatorCompensationPaymentSourceT_SymbolMap = {
                                                                                         { E_sOriginatorCompensationPaymentSourceT.SourceNotSpecified, "OriginatorCompSource_SourceNotSpecified"},
                                                                                         { E_sOriginatorCompensationPaymentSourceT.BorrowerPaid, "OriginatorCompSource_BorrowerPaid"},
                                                                                         { E_sOriginatorCompensationPaymentSourceT.LenderPaid, "OriginatorCompSource_LenderPaid"}
                                                                                     };

        private static object[,] E_sPriorSalesPropertySellerT_SymbolMap = { 
                                                                                { E_sPriorSalesPropertySellerT.Blank, "PropertySeller_NA"},
                                                                                { E_sPriorSalesPropertySellerT.Private, "PropertySeller_Private"},
                                                                                { E_sPriorSalesPropertySellerT.BankREO, "PropertySeller_BankREO"},
                                                                                { E_sPriorSalesPropertySellerT.FNMA, "PropertySeller_FNMA"},
                                                                                { E_sPriorSalesPropertySellerT.FHLMC, "PropertySeller_FHLMC"},
                                                                                { E_sPriorSalesPropertySellerT.GNMA, "PropertySeller_GNMA"},
                                                                                { E_sPriorSalesPropertySellerT.HUD, "PropertySeller_HUD"},
                                                                                { E_sPriorSalesPropertySellerT.VA, "PropertySeller_VA"},
                                                                                { E_sPriorSalesPropertySellerT.OtherGovEntity, "PropertySeller_OtherGovEntity"},
                                                                                { E_sPriorSalesPropertySellerT.Inheritance, "PropertySeller_Inheritance"},
                                                                                { E_sPriorSalesPropertySellerT.USDA, "PropertySeller_USDA"},
                                                                                { E_sPriorSalesPropertySellerT.Corporation, "PropertySeller_Corporation"},
                                                                          };

        private static object[,] E_sStatusT_SymbolMap = { 
                                                                                { E_sStatusT.Loan_Open, "LoanStatus_Loan_Open"},
                                                                                { E_sStatusT.Loan_Prequal, "LoanStatus_Loan_Prequal"},
                                                                                { E_sStatusT.Loan_Preapproval, "LoanStatus_Loan_Preapproval"},
                                                                                { E_sStatusT.Loan_Registered, "LoanStatus_Loan_Registered"}, // Note the rename to match LO nomenclature
                                                                                { E_sStatusT.Loan_Approved, "LoanStatus_Loan_Approved"},
                                                                                { E_sStatusT.Loan_Docs, "LoanStatus_Loan_Docs"},
                                                                                { E_sStatusT.Loan_Funded, "LoanStatus_Loan_Funded"},
                                                                                { E_sStatusT.Loan_OnHold, "LoanStatus_Loan_OnHold"},
                                                                                { E_sStatusT.Loan_Suspended, "LoanStatus_Loan_Suspended"},
                                                                                { E_sStatusT.Loan_Canceled, "LoanStatus_Loan_Canceled"},
                                                                                { E_sStatusT.Loan_Rejected, "LoanStatus_Loan_Denied"}, // opm 241591 ejm - Renaming to match Loan Denied status.
                                                                                { E_sStatusT.Loan_Closed, "LoanStatus_Loan_Closed"},
                                                                                { E_sStatusT.Lead_New, "LoanStatus_Lead_New"},
                                                                                { E_sStatusT.Loan_Underwriting, "LoanStatus_Loan_Underwriting"},
                                                                                { E_sStatusT.Loan_WebConsumer, "LoanStatus_Loan_WebConsumer"},
                                                                                { E_sStatusT.Lead_Canceled, "LoanStatus_Lead_Canceled"},
                                                                                { E_sStatusT.Lead_Declined, "LoanStatus_Lead_Declined"},
                                                                                { E_sStatusT.Lead_Other, "LoanStatus_Lead_Other"},
                                                                                { E_sStatusT.Loan_Other, "LoanStatus_Loan_Other"},
                                                                                { E_sStatusT.Loan_Recorded, "LoanStatus_Loan_Recorded"},
                                                                                { E_sStatusT.Loan_Shipped, "LoanStatus_Loan_Shipped"},
                                                                                { E_sStatusT.Loan_ClearToClose, "LoanStatus_Loan_ClearToClose"},
                                                                                { E_sStatusT.Loan_Processing, "LoanStatus_Loan_Processing"},
                                                                                { E_sStatusT.Loan_FinalUnderwriting, "LoanStatus_Loan_FinalUnderwriting"},
                                                                                { E_sStatusT.Loan_DocsBack, "LoanStatus_Loan_DocsBack"},
                                                                                { E_sStatusT.Loan_FundingConditions, "LoanStatus_Loan_FundingConditions"},
                                                                                { E_sStatusT.Loan_FinalDocs, "LoanStatus_Loan_FinalDocs"},
                                                                                { E_sStatusT.Loan_LoanPurchased, "LoanStatus_Loan_Sold"}, // opm 241591 ejm - Renaming to match the Loan Sold status.
                                                                                { E_sStatusT.Loan_LoanSubmitted, "LoanStatus_Loan_LoanSubmitted"},
                                                                                // start sk 1/6/2014 opm 145251
                                                                                { E_sStatusT.Loan_PreProcessing , "LoanStatus_Loan_PreProcessing"}, // opm 241591 ejm - Renaming to match the format of the other keywords.    
                                                                                { E_sStatusT.Loan_DocumentCheck , "LoanStatus_Loan_DocumentCheck"},
                                                                                { E_sStatusT.Loan_DocumentCheckFailed , "LoanStatus_Loan_DocumentCheckFailed"},
                                                                                { E_sStatusT.Loan_PreUnderwriting , "LoanStatus_Loan_PreUnderwriting"},
                                                                                { E_sStatusT.Loan_ConditionReview , "LoanStatus_Loan_ConditionReview"},
                                                                                { E_sStatusT.Loan_PreDocQC , "LoanStatus_Loan_PreDocQC"},
                                                                                { E_sStatusT.Loan_DocsOrdered , "LoanStatus_Loan_DocsOrdered"},
                                                                                { E_sStatusT.Loan_DocsDrawn , "LoanStatus_Loan_DocsDrawn"},
                                                                                { E_sStatusT.Loan_InvestorConditions , "LoanStatus_Loan_InvestorConditions"},       //  tied to sSuspendedByInvestorD
                                                                                { E_sStatusT.Loan_InvestorConditionsSent , "LoanStatus_Loan_InvestorConditionsSent"},   //  tied to sCondSentToInvestorD
                                                                                { E_sStatusT.Loan_ReadyForSale , "LoanStatus_Loan_ReadyForSale"},
                                                                                { E_sStatusT.Loan_SubmittedForPurchaseReview , "LoanStatus_Loan_SubmittedForPurchaseReview"},
                                                                                { E_sStatusT.Loan_InPurchaseReview , "LoanStatus_Loan_InPurchaseReview"},
                                                                                { E_sStatusT.Loan_PrePurchaseConditions , "LoanStatus_Loan_PrePurchaseConditions"},
                                                                                { E_sStatusT.Loan_SubmittedForFinalPurchaseReview , "LoanStatus_Loan_SubmittedForFinalPurchaseReview"},
                                                                                { E_sStatusT.Loan_InFinalPurchaseReview , "LoanStatus_Loan_InFinalPurchaseReview"},
                                                                                { E_sStatusT.Loan_ClearToPurchase , "LoanStatus_Loan_ClearToPurchase"},
                                                                                { E_sStatusT.Loan_Purchased , "LoanStatus_Loan_Purchased"},        // don't confuse with the old { E_sStatusT.Loan_LoanPurchased, which is now Loan Sold in the UI.
                                                                                { E_sStatusT.Loan_CounterOffer , "LoanStatus_Loan_CounterOffer"},
                                                                                { E_sStatusT.Loan_Withdrawn , "LoanStatus_Loan_Withdrawn"},
                                                                                { E_sStatusT.Loan_Archived , "LoanStatus_Loan_Archived"  }  
                                                                            };

        private static object[,] E_MasterArmIndexT_SymbolMap = {
                                                                    { E_MasterArmIndexT.OneMonthLibor, "ARMIndex_1MOLIBOR" },
                                                                    { E_MasterArmIndexT.OneYearCmt, "ARMIndex_1YRCMT" },
                                                                    { E_MasterArmIndexT.OneYearLibor, "ARMIndex_1YRLIBOR" },
                                                                    { E_MasterArmIndexT.TenYearCmt, "ARMIndex_10YRCMT" },
                                                                    { E_MasterArmIndexT.EleventhDistrictCofi, "ARMIndex_11DISTCOFI" },
                                                                    { E_MasterArmIndexT.TwoYearCmt, "ARMIndex_2YRCMT" },
                                                                    { E_MasterArmIndexT.ThreeMonthLibor, "ARMIndex_3MOLIBOR" },
                                                                    { E_MasterArmIndexT.ThreeYearCmt, "ARMIndex_3YRCMT" },
                                                                    { E_MasterArmIndexT.FiveYearCmt, "ARMIndex_5YRCMT" },
                                                                    { E_MasterArmIndexT.SixMonthCmt, "ARMIndex_6MOCMT" },
                                                                    { E_MasterArmIndexT.SixMonthLibor, "ARMIndex_6MOLIBOR" },
                                                                    { E_MasterArmIndexT.SevenYearCmt, "ARMIndex_7YRCMT" },
                                                                    { E_MasterArmIndexT.MTA, "ARMIndex_MTA" },
                                                                    { E_MasterArmIndexT.PrimeRate, "ARMIndex_PRIMERATE" }
                                                               };

        private static object[,] E_sAssumeLT_SymbolMap = {
                                                            //{ E_sAssumeLT.LeaveBlank, "Assumption_LeaveBlank" },
                                                            { E_sAssumeLT.MayNot, "Assumption_MayNot" },
                                                            { E_sAssumeLT.May, "Assumption_May" },
                                                            { E_sAssumeLT.MaySubjectToCondition, "Assumption_MayWithCond" },
                                                        };

        private static object[,] E_sProdConvMIOptionT_SymbolMap = {
                                                            { E_sProdConvMIOptionT.NoMI, "PMI_Type_NoPMI" },
                                                            { E_sProdConvMIOptionT.BorrPaidMonPrem, "PMI_Type_BPMonthly" },
                                                            { E_sProdConvMIOptionT.BorrPaidSinglePrem, "PMI_Type_BPSingle" },
                                                            { E_sProdConvMIOptionT.BorrPaidSplitPrem, "PMI_Type_BPSplit" },
                                                            { E_sProdConvMIOptionT.LendPaidSinglePrem, "PMI_Type_LPSingle" },
                                                            { E_sProdConvMIOptionT.Blank, "PMI_Type_NoPMI_4" }
                                                        };

        private static object[,] E_sConvSplitMIRT_SymbolMap = {
                                                            { E_sConvSplitMIRT.PointFivePercent, "SplitMI_Upfront_50" },
                                                            { E_sConvSplitMIRT.PointSevenFivePercent, "SplitMI_Upfront_75" },
                                                            { E_sConvSplitMIRT.OnePercent, "SplitMI_Upfront_100" },
                                                            { E_sConvSplitMIRT.OnePointTwoFivePercent, "SplitMI_Upfront_125" },
                                                            { E_sConvSplitMIRT.OnePointFivePercent, "SplitMI_Upfront_150" },
                                                            { E_sConvSplitMIRT.OnePointSevenFivePercent, "SplitMI_Upfront_175" },
                                                            { E_sConvSplitMIRT.TwoPercent, "SplitMI_Upfront_200" },
                                                            { E_sConvSplitMIRT.TwoPointTwoFivePercent, "SplitMI_Upfront_225" },

                                                        };

        private static object[,] E_RunPmlRequestSourceT_SymbolMap = {
                                                                        { E_RunPmlRequestSourceT.Regular, "RunPmlRequestSourceType_Regular"},
                                                                        { E_RunPmlRequestSourceT.ConsumerPortal, "RunPmlRequestSourceType_ConsumerPortal"}
                                                                    };

        private static object[,] E_sBranchChannelT_SymbolMap = {
                                                                   { E_BranchChannelT.Blank, "BranchChannel_Blank"},
                                                                   { E_BranchChannelT.Broker, "BranchChannel_Broker"},
                                                                   { E_BranchChannelT.Correspondent, "BranchChannel_Correspondent"},
                                                                   { E_BranchChannelT.Retail, "BranchChannel_Retail"},
                                                                   { E_BranchChannelT.Wholesale, "BranchChannel_Wholesale"}
                                                               };

        private static object[,] E_sCorrespondentProcessT_SymbolMap = {
                                                                       { E_sCorrespondentProcessT.Blank, "CorrespondentProcessType_Blank"},
                                                                       { E_sCorrespondentProcessT.Delegated, "CorrespondentProcessType_Delegated"},
                                                                       { E_sCorrespondentProcessT.PriorApproved, "CorrespondentProcessType_PriorApproved"},
                                                                       { E_sCorrespondentProcessT.MiniCorrespondent, "CorrespondentProcessType_MiniCorrespondent"},
                                                                       { E_sCorrespondentProcessT.Bulk, "CorrespondentProcessType_Bulk"},
                                                                       { E_sCorrespondentProcessT.MiniBulk, "CorrespondentProcessType_MiniBulk"}
                                                                   };

        private static object[,] E_sLenderFeeBuyoutRequestedT_SymbolMap = {
                                                                   { E_sLenderFeeBuyoutRequestedT.No, "LenderFeeBuyout_No"},
                                                                   { E_sLenderFeeBuyoutRequestedT.Yes, "LenderFeeBuyout_Yes"},
                                                                   { E_sLenderFeeBuyoutRequestedT.Waived, "LenderFeeBuyout_Waived"}
                                                               };

        private static object[,] E_aBorrowerTypeT_SymbolMap = {
                                                                   { E_aTypeT.Individual, "BorrowerType_Individual"},
                                                                   { E_aTypeT.CoSigner, "BorrowerType_CoSigner"},
                                                                   { E_aTypeT.TitleOnly, "BorrowerType_TitleOnly"},
                                                                   { E_aTypeT.NonTitleSpouse, "BorrowerType_NonTitleSpouse"},
                                                                   { E_aTypeT.CurrentTitleOnly, "BorrowerType_CurrentTitleOnly"}
                                                               };
        private static object[,] E_sLT_SymbolMap = {
                                                                   { E_sLT.Conventional, "LoanType_Conventional"},
                                                                   { E_sLT.FHA, "LoanType_FHA"},
                                                                   { E_sLT.VA, "LoanType_VA"},
                                                                   { E_sLT.UsdaRural, "LoanType_Usda"},
                                                                   { E_sLT.Other, "LoanType_Other"},
                                                               };

        private static object[,] E_sFannieHomebuyerEducationT_SymbolMap = 
        {
            { E_FannieHomebuyerEducation.LeaveBlank, "FannieHomebuyerEducationType_Blank"},
            { E_FannieHomebuyerEducation.EducationComplete, "FannieHomebuyerEducationType_HomeBuyerEducationComplete"},
            { E_FannieHomebuyerEducation.OneOnOneCounselingComplete, "FannieHomebuyerEducationType_OneOnOneCounselingComplete"}
        };

        private static object[,] SecondFinancingInclusionT_SymbolMap = 
        {
            { SecondFinancingInclusionT.None, "2ndFinancingInclusionType_None"},
            { SecondFinancingInclusionT.New, "2ndFinancingInclusionType_New"},
            { SecondFinancingInclusionT.Existing, "2ndFinancingInclusionType_Existing"}
        };

        private static object[,] Fha203K_SymbolMap =
        {
            { E_sFHA203kType.NA, "Fha203KType_NA" },
            { E_sFHA203kType.Limited, "Fha203KType_Limited" },
            { E_sFHA203kType.Standard, "Fha203KType_Standard" }
        };

        #endregion
        static CSymbolTable( )
        {
            s_evaluatorEntries = new CaseInsensitiveHashtable(20);
            s_preloadEntries   = new CaseInsensitiveHashtable(150);

            #region initialize s_preloadEntries
            Hashtable m_entries = s_preloadEntries; // hack


            AddEntry( m_entries, new CSymbolNonOpSyntax( (int) E_Purpose.ALL, "(", "<Group>" ) );
            AddEntry( m_entries, new CSymbolNonOpSyntax( (int) E_Purpose.ALL, ")", "</Group>" ) );
            AddEntry( m_entries, new CSymbolOperator( (int) E_Purpose.ALL, "+",	"Plus",			new Evaluator(CRuleEvalTool.EvaluatePlusExpression) ) );
            AddEntry( m_entries, new CSymbolOperator( (int) E_Purpose.ALL, "-",	"Minus",		new Evaluator(CRuleEvalTool.EvaluateMinusExpression) ) );
            AddEntry( m_entries, new CSymbolOperator( (int) E_Purpose.ALL, "*",	"Multiply",		new Evaluator(CRuleEvalTool.EvaluateMultiplyExpression) ) );
            AddEntry( m_entries, new CSymbolOperator( (int) E_Purpose.ALL, "/",	"Divide",		new Evaluator(CRuleEvalTool.EvaluateDivideExpression) ) );
            AddEntry( m_entries, new CSymbolOperator( (int) E_Purpose.Condition, "<",	"Less",			new Evaluator(CRuleEvalTool.EvaluateLessExpression) ) );
            AddEntry( m_entries, new CSymbolOperator( (int) E_Purpose.Condition, "<=",	"LessEqual",    new Evaluator(CRuleEvalTool.EvaluateLessEqualExpression) ) );
            AddEntry( m_entries, new CSymbolOperator( (int) E_Purpose.Condition, ">",	"Greater",		new Evaluator(CRuleEvalTool.EvaluateGreaterExpression) ) );
            AddEntry( m_entries, new CSymbolOperator( (int) E_Purpose.Condition, ">=",	"GreaterEqual",	new Evaluator(CRuleEvalTool.EvaluateGreaterEqualExpression) ) );
            AddEntry( m_entries, new CSymbolOperator( (int) E_Purpose.Condition, "=",	"Equal",		new Evaluator(CRuleEvalTool.EvaluateEqualExpression) ) );
            AddEntry( m_entries, new CSymbolOperator( (int) E_Purpose.Condition, "<>",	"NotEqual",		new Evaluator(CRuleEvalTool.EvaluateNotEqualExpression) ) );
            AddEntry( m_entries, new CSymbolOperator( (int) E_Purpose.Condition, "or",	"Or",			new Evaluator(CRuleEvalTool.EvaluateOrExpression) ) );
            AddEntry( m_entries, new CSymbolOperator( (int) E_Purpose.Condition, "and",	"And",			new Evaluator(CRuleEvalTool.EvaluateAndExpression) ) );

            AddEnumConstants(m_entries, "DocType",                    E_sProdDocT_SymbolMap);
            AddEnumConstants(m_entries, "MIOption",                   E_sProdMIOptionT_SymbolMap);
            AddEnumConstants(m_entries, "LienPosition",               E_sLienPosT_SymbolMap);
            AddEnumConstants(m_entries, "PropertyPurpose",            E_aOccT_SymbolMap);
            AddEnumConstants(m_entries, "AmortizationType",           E_sFinMethT_SymbolMap);
            AddEnumConstants(m_entries, "AmortTypeOtherLoan",         E_sFinMethodOtherLoan_SymbolMap);
            AddEnumConstants(m_entries, "BkMultipleType",             E_BkMultipleT_SymbolMap);
            AddEnumConstants(m_entries, "CitizenshipType",            E_aProdCitizenT_SymbolMap);
            AddEnumConstants(m_entries, "PropertyStructType",         E_sProdSpStructureT_SymbolMap);
            AddEnumConstants(m_entries, "PropertyType",               E_sProdSpT_SymbolMap);
            AddEnumConstants(m_entries, "IsMhAdvantageHome", E_IsMhAdvantageHome_SymbolMap);
            AddEnumConstants(m_entries, "PurposeType",                E_sLPurposeT_SymbolMap);
            AddEnumConstants(m_entries, "ThirdPartyUwResultType",     E_sProd3rdPartyUwResultT_SymbolMap);
            AddEnumConstants(m_entries, "ThirdPartyUwProcessingType", E_sProd3rdPartyUwProcessingT_SymbolMap);
            AddEnumConstants(m_entries, "FhaProductType", E_sTotalScoreFhaProductT_SymbolMap);
            AddEnumConstants(m_entries, "SubFinType", E_sFHASecondaryFinancingSourceT_SymbolMap);
            AddEnumConstants(m_entries, "TotalCreditRiskResultType", E_sTotalScoreCreditRiskResultT_SymbolMap);
            AddEnumConstants(m_entries, "LdpOrGsaExclusion", E_sTotalScoreLdpOrGsaExclusion_SymbolMap);
            AddEnumConstants(m_entries, "LoanProductType", E_sLpProductT_SymbolMap);
            AddEnumConstants(m_entries, "IsAllReoInherited", E_sH4HIsAllReoInheritedTri_SymbolMap);
            AddEnumConstants(m_entries, "BorrowerHasFraudConviction", E_aHasFraudConvictionT_SymbolMap);
            AddEnumConstants(m_entries, "H4HOriginationReqMet", E_sH4HOriginationReqMetT_SymbolMap);
            AddEnumConstants(m_entries, "NonOccCoBorrOnOrigNote", E_sNonOccCoBorrOnOrigNoteT_SymbolMap);
            AddEnumConstants(m_entries, "FhaCondoApprovalStatus", E_sFhaCondoApprovalStatusT_SymbolMap);
            AddEnumConstants(m_entries, "OriginatorCompSource", E_sOriginatorCompensationPaymentSourceT_SymbolMap);
            AddEnumConstants(m_entries, "PropertySeller", E_sPriorSalesPropertySellerT_SymbolMap);
            AddEnumConstants(m_entries, "LoanStatus", E_sStatusT_SymbolMap);
            AddEnumConstants(m_entries, "ARMIndex", E_MasterArmIndexT_SymbolMap);
            AddEnumConstants(m_entries, "Assumption", E_sAssumeLT_SymbolMap);
            AddEnumConstants(m_entries, "PMI_Type", E_sProdConvMIOptionT_SymbolMap);
            AddEnumConstants(m_entries, "SplitMI_Upfront", E_sConvSplitMIRT_SymbolMap);
            AddEnumConstants(m_entries, "RunPmlRequestSourceType", E_RunPmlRequestSourceT_SymbolMap);
            AddEnumConstants(m_entries, "BranchChannel", E_sBranchChannelT_SymbolMap);
            AddEnumConstants(m_entries, "CorrespondentProcessType", E_sCorrespondentProcessT_SymbolMap);
            AddEnumConstants(m_entries, "LenderFeeBuyout", E_sLenderFeeBuyoutRequestedT_SymbolMap);
            AddEnumConstants(m_entries, "BorrowerType", E_aBorrowerTypeT_SymbolMap);
            AddEnumConstants(m_entries, "LoanType", E_sLT_SymbolMap);
            AddEnumConstants(m_entries, "FannieHomebuyerEducationType", E_sFannieHomebuyerEducationT_SymbolMap);
            AddEnumConstants(m_entries, "2ndFinancingInclusionType", SecondFinancingInclusionT_SymbolMap);
            AddEnumConstants(m_entries, "Fha203KType", Fha203K_SymbolMap);
            AddEnumConstants(m_entries, "LoanFileType", E_sLoanFileT_SymbolMap);

            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "Impound_Yes",			true , "Impound") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "Impound_No",			false, "Impound") ); 


            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "Bool_No",		false ) );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "Bool_Yes",		true ) );


            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "SelfEmployed_No",		false, "SelfEmployed") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "SelfEmployed_Yes",		true , "SelfEmployed") );

            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "HasHousingHistory_No", false, "HasHousingHistory"));
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "HasHousingHistory_Yes", true, "HasHousingHistory"));

            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_AL", StateValue( "AL" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_AK", StateValue( "AK" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_AZ", StateValue( "AZ" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_AR", StateValue( "AR" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_CA", StateValue( "CA" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_CO", StateValue( "CO" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_CT", StateValue( "CT" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_DC", StateValue( "DC" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_DE", StateValue( "DE" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_FL", StateValue( "FL" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_GA", StateValue( "GA" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_HI", StateValue( "HI" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_ID", StateValue( "ID" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_IL", StateValue( "IL" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_IN", StateValue( "IN" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_IA", StateValue( "IA" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_KS", StateValue( "KS" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_KY", StateValue( "KY" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_LA", StateValue( "LA" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_ME", StateValue( "ME" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_MD", StateValue( "MD" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_MA", StateValue( "MA" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_MI", StateValue( "MI" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_MN", StateValue( "MN" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_MS", StateValue( "MS" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_MO", StateValue( "MO" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_MT", StateValue( "MT" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_NE", StateValue( "NE" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_NV", StateValue( "NV" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_NH", StateValue( "NH" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_NJ", StateValue( "NJ" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_NM", StateValue( "NM" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_NY", StateValue( "NY" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_NC", StateValue( "NC" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_ND", StateValue( "ND" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_OH", StateValue( "OH" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_OK", StateValue( "OK" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_OR", StateValue( "OR" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_PA", StateValue( "PA" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_RI", StateValue( "RI" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_SC", StateValue( "SC" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_SD", StateValue( "SD" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_TN", StateValue( "TN" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_TX", StateValue( "TX" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_UT", StateValue( "UT" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_VT", StateValue( "VT" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_VA", StateValue( "VA" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_WA", StateValue( "WA" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_WV", StateValue( "WV" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_WI", StateValue( "WI" ) , "State") );
            AddEntry( m_entries, new CSymbolFieldValue( (int) E_Purpose.Condition, "State_WY", StateValue( "WY" ) , "State") );

            AddEntry(m_entries, new CSymbolFieldValue((int)E_Purpose.Condition, "State_PR", StateValue("PR"), "State"));
            AddEntry(m_entries, new CSymbolFieldValue((int)E_Purpose.Condition, "State_VI", StateValue("VI"), "State"));
            AddEntry(m_entries, new CSymbolFieldValue((int)E_Purpose.Condition, "State_GU", StateValue("GU"), "State"));
            AddEntry(m_entries, new CSymbolFieldValue((int)E_Purpose.Condition, "State_AS", StateValue("AS"), "State"));
            AddEntry(m_entries, new CSymbolFieldValue((int)E_Purpose.Condition, "State_MP", StateValue("MP"), "State"));

            // 3/31/2008 dd
            AddEntry( m_entries, new CSymbolFieldNameAsPureMethod( (int) E_Purpose.Condition, "CertTime_HHMM", new Get(LoanDataWrapper.GetCertTimeHhMm)));
            AddEntry( m_entries, new CSymbolFieldNameAsPureMethod( (int) E_Purpose.Condition, "CertTime_DayOfWeek", new Get(LoanDataWrapper.GetCertTimeDayOfWeek)));
            AddEntry( m_entries, new CSymbolFieldNameAsPureMethod( (int) E_Purpose.Condition, "ThirdPartyUwProcessingType", new Get (LoanDataWrapper.GetThirdPartyUwProcessingType)));

            #endregion


            // create dummy CSymbolTable object to count how many items it has
            try
            {
                const int EstimateMaxNewKeywords = 20;

                CSymbolTable dummy = new CSymbolTable(null, new Hashtable() /* don't include new keywords */);
                s_DefaultSymbolTableSize = dummy.Count + EstimateMaxNewKeywords;
                Tools.LogRegTest( "CSymbolTable's capacity = " + dummy.Count.ToString() + " ( official keywords) + " + 
                                  EstimateMaxNewKeywords.ToString() + " (EstimateMaxNewKeywords) = " + 
                                  s_DefaultSymbolTableSize.ToString() );

                foreach( CSymbolEntry entry in dummy.Entries.Values )
                    s_OfficialKeywords[entry.InputKeyword] = entry.InputKeyword;
            }
            catch( Exception exc )
            {
                Tools.LogErrorWithCriticalTracking( "static CSymbolTable( ) error", exc );
            }
        }

        Hashtable m_newKeywords;

        /// <summary>
        /// Read new keywords from db
        /// </summary>
        /// <param name="loanData"></param>
		public CSymbolTable( CPageBase loanData ) : this(loanData, null /* read new keywords from db */)
		{
		}

        /// <summary>
        /// Read new keywords from newKeywords if newKeywords != null. Otherwise, read from db.
        /// </summary>
        /// <param name="loanData"></param>
        public CSymbolTable( CPageBase loanData, Hashtable newKeywords ) 
        {
            m_loanData    = loanData;

            if (m_loanData != null)
            {
                for (int appId = 0; appId < loanData.nApps; appId++)
                    m_inputDataTable[appId] = new LoanDataWrapper(loanData, appId);
            }
            else
                m_inputDataTable[0] = new LoanDataWrapper(loanData, 0); // We just need the entries set up

            m_newKeywords = newKeywords;
        }

        public static CaseInsensitiveHashtable OfficialKeywords
        {
            get { return s_OfficialKeywords; }
        }

        static internal int TestFunction( int months )
		{
			return 2;
		}

		static internal int TestFunctionStrParams( string pars  )
		{
			return 3;
		}

        static internal decimal TestTradeCountX( string pars  )
        {
			// if string parameters are bad, exception will be thrown within this call
			int count = AbstractCreditReport.EvaluateTradeCountX( pars,  null /*AbstractCreditReport*/ );

			if( count < 0 )
				throw new CBaseException( ErrorMessages.Generic, string.Format( "{0} is an invalid return value for the keyword TradeCountX", count ) );

			// this will prevent a return value of 0 being treated as bool_no and 1 as bool_yes
			return 3;
        }

		static internal decimal TestMaxHighCreditAmountX( string pars  )
		{
			// if string parameters are bad, exception will be thrown within this call
			decimal MaxHighCreditAmountValue = 0;
			AbstractCreditReport.EvaluateTradeCountX( pars,  null /*AbstractCreditReport*/, ref MaxHighCreditAmountValue, true /*MaxHighCreditAmountMode*/ );

			if( MaxHighCreditAmountValue < 0 )
				throw new CBaseException( ErrorMessages.Generic, string.Format( "{0} is an invalid return value for the keyword MaxHighCreditAmountX", MaxHighCreditAmountValue ) );

			// this will prevent a return value of 0 being treated as bool_no and 1 as bool_yes
			return 3;
		}

		static internal decimal TestHasContiguousMortgageHistory( string pars  )
		{
			// if string parameters are bad, exception will be thrown within this call
			int count = AbstractCreditReport.EvaluateHasContiguousMortgageHistory( pars,  null /*AbstractCreditReport*/ );

			if( count < 0 )
				throw new CBaseException( ErrorMessages.Generic, string.Format( "{0} is an invalid return value for the keyword HasContiguousMortgageHistory", count ) );

			// this will prevent a return value of 0 being treated as bool_no and 1 as bool_yes
			return 3;
		}

        static internal decimal TestMajorDerogCountX( string pars  )
        {
			// if string parameters are bad, exception will be thrown within this call
			int count = AbstractCreditReport.EvaluateMajorDerogCountX( pars,  null /*AbstractCreditReport*/ );

			if( count < 0 )
				throw new CBaseException( ErrorMessages.Generic, string.Format( "{0} is an invalid return value for the keyword MajorDerogCountX", count ) );

			// this will prevent a return value of 0 being treated as bool_no and 1 as bool_yes
			return 3;
        }

		
        static internal decimal TestHasFcX( string pars  )
        {
			// if string parameters are bad, exception will be thrown within this call
			int count = AbstractCreditReport.EvaluateHasFcX( pars,  null /*AbstractCreditReport*/ );

			if( count < 0 )
				throw new CBaseException( ErrorMessages.Generic, string.Format( "{0} is an invalid return value for the keyword HasFcX", count ) );

			// this will prevent a return value of 0 being treated as bool_no and 1 as bool_yes
			return 3;
        }

        static internal decimal TestFcCountX(string pars)
        {
            // if string parameters are bad, exception will be thrown within this call
            int count = AbstractCreditReport.EvaluateFcCountX(pars, null /*AbstractCreditReport*/ );

            if (count < 0)
                throw new CBaseException(ErrorMessages.Generic, string.Format("{0} is an invalid return value for the keyword FcCountX", count));

            // this will prevent a return value of 0 being treated as bool_no and 1 as bool_yes
            return 3;
        }

        internal static decimal TestChargeOffCountX(string parameters)
        {
            int count = AbstractCreditReport.EvaluateChargeOffCountX(parameters, null);

            if (count < 0)
            {
                throw new CBaseException(ErrorMessages.Generic, string.Format("{0} is an invalid return value for the keyword ChargeOffCountX", count));
            }

            return 3;
        }

        static internal int TestFunctionForBool( int months )
        {
            return 1;
        }

        static internal decimal TestScoreCBS(string parameters)
        {
            // OPM 201266.
            // Attempt to parse the params.  Care not of the result.
            // It will throw exception in parsing error.
            bool equifax, experian, transUnion, pwe, soft;
            string b2, b3, cScore;

           CAppBase.ScoreCBSParseParameters(parameters,
                true,           // verifyParams
                out equifax,
                out experian,
                out transUnion,
                out b2,          // Individual Score Determiner
                out b3,          // Individual Score Determiner
                out pwe,         // Primary Wage Earner
                out cScore,      // Combined Score
                out soft);       // Soft Score
           
            // this will prevent a return value of 0 being treated as bool_no and 1 as bool_yes
            return 3;
        }
		
		static private void AddEntry( Hashtable hashtable, CSymbolEntry entry )
		{
            string key = entry.InputKeyword;
  
            hashtable.Add(key, entry);

            if( entry.XmlOperator != "" )
            {
                if( Object.ReferenceEquals(hashtable, s_preloadEntries) == true)
                    s_evaluatorEntries.Add(entry.XmlOperator, entry);                
                else if( s_preloadEntries.Contains(key) == false)
                {
                    s_fastAccessEvaluators = false;
                    string errMsg = "Internal error:  developer needs to rewrite SymbolTable.LookupEvaluator(...) method.";
#if DEBUG
                    throw new CBaseException( ErrorMessages.Generic, errMsg);
#else
                    Tools.LogBug( errMsg );
#endif
                }
            }
		}

        public void StartCache()
        {
            foreach (LoanDataWrapper inputData in m_inputDataTable.Values)
            {
                inputData.StartCache();
            }
        }

        public void EndCache()
        {
            foreach (LoanDataWrapper inputData in m_inputDataTable.Values)
            {
                inputData.EndCache();
            }
        }

        private CaseInsensitiveHashtable AllocateEntries(int appIndex)
        {
            CaseInsensitiveHashtable m_entries = null; // hide the member m_entries
            FormatTarget storedFormatTarget = FormatTarget.Webform;

            try
            {
                m_entries = new CaseInsensitiveHashtable(s_DefaultSymbolTableSize);
                LoanDataWrapper m_inputData = (LoanDataWrapper) m_inputDataTable[appIndex];

                CAppBase appData = null;
                ICreditReport creditData = null;

                if (null != m_loanData)
                {
                    appData = m_loanData.GetAppData(appIndex);
                    creditData = appData.CreditReportData.Value;
                }

                // TODO: when this list gets very long, make each entry evaluated on demand using reflection


                if (null != m_loanData)
                {
                    storedFormatTarget = m_loanData.m_convertLos.FormatTargetCurrent;
                    m_loanData.SetFormatTarget(FormatTarget.PriceEngineEval);
                }

                foreach (CSymbolEntry entry in s_preloadEntries.Values)
                    AddEntry(m_entries, entry);

                #region Add Symbol Function
                AddEntry(m_entries, new CSymbolFunctionStrParams("AssetTotalX", new FunctionStrParams(m_inputData.AssetTotalX)));
                AddEntry(m_entries, new CSymbolFunctionStrParams("BorrIncomeTotalX", new FunctionStrParams(m_inputData.BorrIncomeTotalX)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "Bankruptcy", (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetBankruptcy)));

                //NEW BK METHODS	
                // DISCHARGED BK

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasDischargedBkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasDischargedBkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasDischarged7BkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasDischarged7BkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasDischarged11BkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasDischarged11BkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasDischarged12BkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasDischarged12BkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasDischarged13BkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasDischarged13BkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasDischargedUnknownTypeBkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasDischargedUnknownTypeBkWithin)));


                // 7/3/2006 nw - OPM 5752 - New Sets of BK Keywords
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasClosedBkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasClosedBkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasClosed7BkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasClosed7BkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasClosed11BkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasClosed11BkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasClosed12BkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasClosed12BkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasClosed13BkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasClosed13BkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasClosedUnknownTypeBkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasClosedUnknownTypeBkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasDismissedBkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasDismissedBkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasDismissed7BkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasDismissed7BkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasDismissed11BkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasDismissed11BkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasDismissed12BkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasDismissed12BkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasDismissed13BkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasDismissed13BkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasDismissedUnknownTypeBkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasDismissedUnknownTypeBkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasDischBkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasDischBkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasDisch7BkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasDisch7BkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasDisch11BkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasDisch11BkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasDisch12BkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasDisch12BkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasDisch13BkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasDisch13BkWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasDischUnknownTypeBkWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasDischUnknownTypeBkWithin)));
                // End OPM 5752 - New Sets of BK Keywords


                // BK FILED DATE	

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasBkFiledWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasBkFiledWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHas7BkFiledWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.Has7BkFiledWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHas11BkFiledWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.Has11BkFiledWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHas12BkFiledWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.Has12BkFiledWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHas13BkFiledWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.Has13BkFiledWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "BkHasUnknownTypeBkFiledWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasUnknownTypeBkFiledWithin)));


                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "ChargeOff",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetChargeOff)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "HighestUnpaidBalOfMedicalCollectionWithin", //av 18561
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(m_inputData.GetHighestUnpaidBalOfMedicalCollectionWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "HasMortgageIncludedInBKWithin", //av 25329
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(m_inputData.GetHasMortgageIncludedInBKWithin)));
                //6/28/07 db OPM 16602 - New MaxHighCreditAmountX keyword
                AddEntry(m_entries, new CSymbolFunctionStrParams("MaxHighCreditAmountX",
                    (null == creditData) ? ((null == m_loanData) ? new FunctionStrParams(TestMaxHighCreditAmountX) : null) : new FunctionStrParams(m_inputData.MaxHighCreditAmountX)));

                AddEntry(m_entries, new CSymbolFunctionStrParams("HasFcX",
                    (null == creditData) ? ((null == m_loanData) ? new FunctionStrParams(TestHasFcX) : null) : new FunctionStrParams(m_inputData.HasFcX)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "HasDerogCountEffectiveWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasDerogCountEffectiveWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "HasMajorDerogCountEffectiveWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HasMajorDerogCountEffectiveWithin)));


                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "HasTaxLienWithin", (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.BankImpactHasTaxLienWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "HasTaxLienFiledWithin", (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.BankImpactHasTaxLienFiledWithin)));

                AddEntry(m_entries, new CSymbolFunctionStrParams("TradeCountX",
                    (null == creditData) ? ((null == m_loanData) ? new FunctionStrParams(TestTradeCountX) : null) : new FunctionStrParams(m_inputData.TradeCountX)));

                AddEntry(m_entries, new CSymbolFunctionStrParams("HasContiguousMortgageHistory",
                    (null == creditData) ? ((null == m_loanData) ? new FunctionStrParams(TestHasContiguousMortgageHistory) : null) : new FunctionStrParams(m_inputData.HasContiguousMortgageHistory)));

                AddEntry(m_entries, new CSymbolFunctionStrParams("FcCountX",
                    (null == creditData) ? ((null == m_loanData) ? new FunctionStrParams(TestFcCountX) : null) : new FunctionStrParams(m_inputData.FcCountX)));


                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "HighestUnpaidBalOfJudgmentWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetHighestUnpaidBalOfJudgmentWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "HighestUnpaidBalOfJudgmentOlderThan",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetHighestUnpaidBalOfJudgmentOlderThan)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "HighestUnpaidBalOfChargeOffOrCollectionWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HighestUnpaidBalOfChargeOffOrCollectionWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "HighestUnpaidBalOfChargeOffOrCollectionOlderThan",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.HighestUnpaidBalOfChargeOffOrCollectionOlderThan)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "HighestUnpaidBalOfCollectionWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetHighestUnpaidBalOfCollectionWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "HighestUnpaidBalOfChargeOffWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetHighestUnpaidBalOfChargeOffWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "TotUnpaidBalOfCollectionWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetTotUnpaidBalOfCollectionWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "TotUnpaidBalOfChargeOffWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetTotUnpaidBalOfChargeOffWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "HighestUnpaidBalOfCollectionOlderThan",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetHighestUnpaidBalOfCollectionOlderThan)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "HighestUnpaidBalOfChargeOffOlderThan",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetHighestUnpaidBalOfChargeOffOlderThan)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "HighestUnpaidBalOfTaxLienWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetHighestUnpaidBalOfTaxLienWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "HighestUnpaidPastDueWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetHighestUnpaidPastDueWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "HighestUnpaidPastDueOlderThan",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetHighestUnpaidPastDueOlderThan)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "TotUnpaidBalOfJudgmentWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetTotUnpaidBalOfJudgmentWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "TotUnpaidBalOfAllDerogWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.TotUnpaidBalOfAllDerogWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "TotUnpaidBalOfChargeOffAndCollectionWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.TotUnpaidBalOfChargeOffAndCollectionWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "TotUnpaidBalOfTaxLienWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetTotUnpaidBalOfTaxLienWithin)));


                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "InstallTradeCountOlderThan", (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.InstallTradeCountOlderThan)));


                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateInstall30IntermitProgressRollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetInstallmentLates30IntermittentProgressiveRollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateInstall60IntermitProgressRollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetInstallmentLates60IntermittentProgressiveRollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateInstall90IntermitProgressRollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetInstallmentLates90IntermittentProgressiveRollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateInstall120IntermitProgressRollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetInstallmentLates120IntermittentProgressiveRollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateInstall150IntermitProgressRollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetInstallmentLates150IntermittentProgressiveRollingCountWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateInstall30IntermitProgressNonrollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetInstallmentLates30IntermittentProgressiveNonrollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateInstall60IntermitProgressNonrollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetInstallmentLates60IntermittentProgressiveNonrollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateInstall90IntermitProgressNonrollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetInstallmentLates90IntermittentProgressiveNonrollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateInstall120IntermitProgressNonrollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetInstallmentLates120IntermittentProgressiveNonrollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateInstall150IntermitProgressNonrollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetInstallmentLates150IntermittentProgressiveNonrollingCountWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateMortgage30IntermitProgressRollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetMortgageLates30IntermittentProgressiveRollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateMortgage60IntermitProgressRollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetMortgageLates60IntermittentProgressiveRollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateMortgage90IntermitProgressRollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetMortgageLates90IntermittentProgressiveRollingCountWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateRevolve30IntermitProgressRollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetRevolvingLates30IntermittentProgressiveRollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateRevolve60IntermitProgressRollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetRevolvingLates60IntermittentProgressiveRollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateRevolve90IntermitProgressRollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetRevolvingLates90IntermittentProgressiveRollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateRevolve120IntermitProgressRollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetRevolvingLates120IntermittentProgressiveRollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateRevolve150IntermitProgressRollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetRevolvingLates150IntermittentProgressiveRollingCountWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateRevolve30IntermitProgressNonrollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetRevolvingLates30IntermittentProgressiveNonrollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateRevolve60IntermitProgressNonrollingCountWithin", (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetRevolvingLates60IntermittentProgressiveNonrollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateRevolve90IntermitProgressNonrollingCountWithin", (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetRevolvingLates90IntermittentProgressiveNonrollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateRevolve120IntermitProgressNonrollingCountWithin", (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetRevolvingLates120IntermittentProgressiveNonrollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateRevolve150IntermitProgressNonrollingCountWithin", (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetRevolvingLates150IntermittentProgressiveNonrollingCountWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateLease30IntermitProgressRollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetLeaseLates30IntermittentProgressiveRollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateLease60IntermitProgressRollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetLeaseLates60IntermittentProgressiveRollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateLease90IntermitProgressRollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetLeaseLates90IntermittentProgressiveRollingCountWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateLease30IntermitProgressNonrollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetLeaseLates30IntermittentProgressiveNonrollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateLease60IntermitProgressNonrollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetLeaseLates60IntermittentProgressiveNonrollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateLease90IntermitProgressNonrollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetLeaseLates90IntermittentProgressiveNonrollingCountWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateHeloc30IntermitProgressRollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetHelocLates30IntermittentProgressiveRollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateHeloc60IntermitProgressRollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetHelocLates60IntermittentProgressiveRollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateHeloc90IntermitProgressRollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetHelocLates90IntermittentProgressiveRollingCountWithin)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateHeloc30IntermitProgressNonrollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetHelocLates30IntermittentProgressiveNonrollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateHeloc60IntermitProgressNonrollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetHelocLates60IntermittentProgressiveNonrollingCountWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "LateHeloc90IntermitProgressNonrollingCountWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetHelocLates90IntermittentProgressiveNonrollingCountWithin)));

                AddEntry(m_entries, new CSymbolFunctionStrParams("MajorDerogCountX",
                    (null == creditData) ? ((null == m_loanData) ? new FunctionStrParams(TestMajorDerogCountX) : null) : new FunctionStrParams(m_inputData.MajorDerogCountX)));

                // 6/15/2006 - nw - OPM 4822
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "MightHaveUndated120Within",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.MightHaveUndated120Within)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "MortgageLates", (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetMortgageLates)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "Mortgage30Lates", (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetMortgage30Lates)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "Mortgage60Lates", (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetMortgage60Lates)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "Mortgage90Lates", (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetMortgage90Lates)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "Mortgage120Lates", (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetMortgage120Lates)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "Mortgage150PlusLates", (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetMortgage150PlusLates)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "ShortSales", (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetShortSales)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "RevolveTradeCountOlderThan",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.RevolveTradeCountOlderThan)));
                    AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsDuRefiPlus", new Get(m_inputData.GetIsDuRefiPlus)));


                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "TotUnpaidBalOfMedCollectionsWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.TotUnpaidBalOfMedCollectionsWithin)));


                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "TradesCountActiveWithin",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetTradesCountActiveWithin)));
                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "TradesCountActiveWithHighCreditOf",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetTradesCountActiveWithHighCreditOf)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "TradesCountOlderThan", (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetTradesCountOlderThan)));


                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "TradesCountWithHighCreditOf",
                    (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.GetTradesCountWithHighCreditOf)));

                AddEntry(m_entries, new CSymbolFunction((int)E_Purpose.Condition, "TradesOpenCountOlderThan", (null == creditData) ? ((null == m_loanData) ? new Function(TestFunction) : null) : new Function(creditData.TradesOpenCountOlderThan)));

                AddEntry( m_entries, new CSymbolFunction( (int) E_Purpose.Condition, "MaxEmploymentGapWithin", (null == creditData) ? ((null == m_loanData) ? new Function( TestFunction ) : null) : new Function( m_inputData.MaxEmploymentGapWithin ) ) );
                AddEntry( m_entries, new CSymbolFieldName( (int) E_Purpose.Condition, "YearsOnJob", new Get( m_inputData.GetYearsOnJob ) ) );
                AddEntry( m_entries, new CSymbolFieldName( (int) E_Purpose.Condition, "YearsInProfession", new Get( m_inputData.GetYearsOnJob ) ) );

                AddEntry(m_entries, new CSymbolFunctionStrParams("ChargeOffCountX",
                    (null == creditData) ? ((null == m_loanData) ? new FunctionStrParams(TestChargeOffCountX) : null) : new FunctionStrParams(m_inputData.ChargeOffCountX)));

                // OPM 201266
                AddEntry(m_entries, new CSymbolFunctionStrParams("ScoreCBS",
                    (null == m_loanData) ? new FunctionStrParams(TestScoreCBS) : new FunctionStrParams(m_inputData.GetScoreCBS)));
                
                #endregion

                #region Add Symbol Field Name
                // 3/7/2007 dd - OPM 10585 - Add Residual Income keyword
                AddEntry(m_entries, new CSymbolFieldName("ResidualIncomeMonthly", new Get(m_inputData.GetResidualIncomeMonthly)));

                AddEntry(m_entries, new CSymbolFieldName("Qcltv", new Get(m_inputData.GetQcltv)));
                AddEntry(m_entries, new CSymbolFieldName("Qhcltv", new Get(m_inputData.GetQhcltv)));
                AddEntry(m_entries, new CSymbolFieldName("Qltv", new Get(m_inputData.GetQltv)));
                AddEntry(m_entries, new CSymbolFieldName("Qscore", new Get(m_inputData.GetQscore)));
                AddEntry(m_entries, new CSymbolFieldName("QLAmt", new Get(m_inputData.GetQLAmt)));


                AddEntry(m_entries, new CSymbolFieldName((int)(E_Purpose.Condition | E_Purpose.TargetQscore), "Score2", new Get(m_inputData.GetScore2)));
                AddEntry(m_entries, new CSymbolFieldName((int)(E_Purpose.Condition | E_Purpose.TargetQscore), "Score2Soft", new Get(m_inputData.GetScore2Soft)));

                AddEntry(m_entries, new CSymbolFieldName("AmortizationType", new Get(m_inputData.GetAmortizationType)));
                AddEntry(m_entries, new CSymbolFieldName("FhaProductType", new Get(m_inputData.GetTotalScoreFhaProduct)));
                AddEntry(m_entries, new CSymbolFieldName("AmortTypeOtherLoan", new Get(m_inputData.GetAmortTypeOtherLoan)));
                AddEntry(m_entries, new CSymbolFieldName("ArmFixedTerm", new Get(m_inputData.GetArmFixedTerm)));
                AddEntry(m_entries, new CSymbolFieldName("AvailReserveMonths", new Get(m_inputData.GetAvailReserveMonths)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "BkHasUnpaidAndNondischarged", new Get(m_inputData.GetBkHasUnpaidAndNondischarged)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "BkHasUnpaidAndNondischarged7Bk", new Get(m_inputData.GetBkHasUnpaidAndNondischarged7Bk)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "BkHasUnpaidAndNondischarged11Bk", new Get(m_inputData.GetBkHasUnpaidAndNondischarged11Bk)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "BkHasUnpaidAndNondischarged12Bk", new Get(m_inputData.GetBkHasUnpaidAndNondischarged12Bk)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "BkHasUnpaidAndNondischarged13Bk", new Get(m_inputData.GetBkHasUnpaidAndNondischarged13Bk)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "BkHasUnpaidAndNondischargedUnknownTypeBk", new Get(m_inputData.GetBkHasUnpaidAndNondischargedUnknownTypeBk)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "BkMultipleType", new Get(m_inputData.GetBkMultipleType)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CashoutAmt", new Get(m_inputData.GetCashoutAmt)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CitizenshipType", new Get(m_inputData.GetCitizenshipType)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CLTV", new Get(m_inputData.GetCLTV)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CountyFIPS", new Get(m_inputData.GetSpCountyFips)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CreditReportIsOnFile", new Get(m_inputData.GetCreditReportIsOnFile)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsCreditScoresDifferentWithCreditReport", new Get(m_inputData.GetIsCreditScoresDifferentWithCreditReport)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "DebtRatioBottom", new Get(m_inputData.GetDebtRatioBottom)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "DebtRatioTop", new Get(m_inputData.GetDebtRatioTop)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "DerogatoryAmountTotal", new Get(m_inputData.GetDerogatoryAmountTotal)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "DocType", new Get(m_inputData.GetDocType)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "Due", new Get(m_inputData.GetDue)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsAllPrepaymentPenaltyAllowed", new Get(m_inputData.GetIsAllPrepaymentPenaltyAllowed)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "ForeclosureUnpaidCount", new Get(m_inputData.GetForeclosureUnpaidCount)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "HasMortgage", new Get(m_inputData.GetHasMortgage)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "HasTaxOpenTaxLien", new Get(m_inputData.GetHasTaxOpenTaxLien)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "Impound", new Get(m_inputData.GetImpound)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "MIOption", new Get(m_inputData.GetMIOption)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IncomeMonthly", new Get(m_inputData.GetIncomeMonthly)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "InterestOnly", new Get(m_inputData.GetInterestOnly)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "InterestOnlyOtherLoan", new Get(m_inputData.GetInterestOnlyOtherLoan)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "Is1stTimeHomeBuyer", new Get(m_inputData.GetIs1stTimeHomeBuyer)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsCondotel", new Get(m_inputData.GetIsCondotel)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsConformingLAmt", new Get(m_inputData.GetIsConformingLAmt)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsHighBalanceConformingLAmt", new Get(m_inputData.GetIsHighBalanceConformingLAmt)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsInCreditCounseling", new Get(m_inputData.GetIsInCreditCounseling)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsNonwarrantableProj", new Get(m_inputData.GetIsNonwarrantableProj)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsNegAmortOtherLien", new Get(m_inputData.GetIsNegAmortOtherLien)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsNegAmort", new Get(m_inputData.GetIsNegAmort)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsPpAllowedInSubjPropState", new Get(m_inputData.GetIsPpAllowedInSubjPropState)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsSameInvestorFor8020", new Get(m_inputData.GetIsSameInvestorFor8020)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "PpmtPenaltyMonths", new Get(m_inputData.GetPpmtPenaltyMonths)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "NumberOfUnits", new Get(m_inputData.GetNumberOfUnits)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "NumberOfCondoStories", new Get(m_inputData.GetNumberOfCondoStories)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "PrimaryWageEarnerCreditScoresCount", new Get(m_inputData.GetPrimaryWageEarnerCreditScoresCount)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "MinScoreCount", new Get(m_inputData.GetMinScoreCount)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "PropertyInRuralArea", new Get(m_inputData.GetPropertyInRuralArea)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "PropertyPurpose", new Get(m_inputData.GetPropertyPurpose)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "PropertyStructType", new Get(m_inputData.GetPropertyStructType)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "PropertyType", new Get(m_inputData.GetPropertyType)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsMhAdvantageHome", new Get(m_inputData.GetIsMhAdvantageHome)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "PropertyValue", new Get(m_inputData.GetPropertyValue)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "PurposeType", new Get(m_inputData.GetPurposeType)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "Score1", new Get(m_inputData.GetScore1)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "2ndWageScore", new Get(m_inputData.Get2ndWageScore)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "SelfEmployed", new Get(m_inputData.GetSelfEmployed)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "State", new Get(m_inputData.GetState)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "Term", new Get(m_inputData.GetTerm)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "ThirdPartyUwResultType", new Get(m_inputData.GetThirdPartyUwResultType)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "TotBalOfMedCollectionsUnpaid", new Get(m_inputData.GetTotBalOfMedCollectionsUnpaid)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "TradeCount", new Get(m_inputData.GetTradeCount)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "TradesCountNonMajorDerog", new Get(m_inputData.GetTradesCountNonMajorDerog)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "Units", new Get(m_inputData.GetUnits)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "HasHousingHistory", new Get(m_inputData.GetHasHousingHistory)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "HasCobor", new Get(m_inputData.GetHasCobor)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsStandAlone2ndLien", new Get(m_inputData.GetIsStandAlone2ndLien)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsWithinFhaLoanLimits", new Get(m_inputData.GetIsWithinFhaLoanLimits)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IncludeMyCommunityProc", new Get(m_inputData.GetIncludeMyCommunityProc)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IncludeHomePossibleProc", new Get(m_inputData.GetIncludeHomePossibleProc)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IncludeNormalProc", new Get(m_inputData.GetIncludeNormalProc)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IncludeFHATOTALProc", new Get(m_inputData.GetIncludeFHATOTALProc)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IncludeVAProc", new Get(m_inputData.GetIncludeVAProc)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IncludeUSDARuralProc", new Get(m_inputData.GetIncludeUSDARuralProc)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsRSEFeatureEnable", new Get(m_inputData.GetIsRSEFeatureEnable)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "LienPosition", new Get(m_inputData.GetLienPosition)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "LenderID", new Get(m_inputData.GetLenderID)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "LockDays", new Get(m_inputData.GetLockDays)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "LTV", new Get(m_inputData.GetLTV)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "Ltv1stLien", new Get(m_inputData.GetLtv1stLien)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "LoanAmount", new Get(m_inputData.GetLoanAmount)));// TODO: LoanAmount is the obsolete version of LAmt. 
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "LAmt", new Get(m_inputData.GetLAmt)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "LoanAmount1stLien", new Get(m_inputData.GetLoanAmount1stLien)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "LoanAmount2ndLien", new Get(m_inputData.GetLoanAmount2ndLien)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "TLAmt", new Get(m_inputData.GetTLAmt)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "ScoreQBC", new Get(m_inputData.GetScoreQBC)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsTexas50a6Loan", new Get(m_inputData.GetIsTexas50a6Loan))); // 8/26/2009 dd - OPM 28525
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsPriorLoanTexas50a6Loan", new Get(m_inputData.GetIsPriorLoanTexas50a6Loan))); // 01/09/2018 je - OPM 465629
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "HasNonOccupantCoborrower", new Get(m_inputData.GetHasNonOccupantCoborrower))); // 10/22/2009 dd - OPM 41241
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "H4HHasNonOccupantCoborrower", new Get(m_inputData.GetH4HHasNonOccupantCoborrower))); // 7/15/2010 dd - OPM 54013
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "HasTempBuydown", new Get(m_inputData.GetHasTempBuydown))); // 10/22/2009 dd - OPM 41242
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "InterestedPartyContribPercent", new Get(m_inputData.GetInterestedPartyContribPercent))); // 10/22/2009 dd - OPM 41243
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsIdentityOfInterestTransaction", new Get(m_inputData.GetIsIdentityOfInterestTransaction))); // 10/22/2009 dd - OPM 41245
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsIdentityOfInterestTransactionException", new Get(m_inputData.GetIsIdentityOfInterestTransactionException))); // 12/1/2009 dd - OPM 43080
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "EnergyEfficientImprovementsAmt", new Get(m_inputData.GetEnergyEfficientImprovementsAmt))); // 10/23/2009 dd - OPM 41238
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "SubFinType", new Get(m_inputData.GetSubFinType)));// 10/23/2009 dd - OPM 41249.
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsWithinFHA3Or4UnitMaxLoanAmount", new Get(m_inputData.GetIsWithinFHA3Or4UnitMaxLoanAmount))); // 10/23/2009 dd - OPM 41247
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "HasClearCaivrsAuthcode", new Get(m_inputData.GetHasClearCaivrsAuthcode))); // 10/23/2009 dd - OPM 41240
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsHUDApprovedFTHBCounselingComplete", new Get(m_inputData.GetIsHUDApprovedFTHBCounselingComplete))); // 10/23/2009 dd - OPM 41244
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "LdpOrGsaExclusion", new Get(m_inputData.GetLdpOrGsaExclusion))); // 10/23/2009 dd - OPM 41246
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "TotalCreditRiskResultType", new Get(m_inputData.GetTotalCreditRiskResultType))); // 10/23/2009 dd - OPM 41250
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "LockDaysUnadjusted", new Get( m_inputData.GetLockDaysUnadjusted ) ) );
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "LoanProductType", new Get(m_inputData.GetLoanProductType))); // 11/10/2009 dd - OPM 23038
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsCreditQualifying", new Get(m_inputData.GetIsCreditQualifying))); // 11/17/2009 dd - OPM 42581
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsAllReoInherited", new Get(m_inputData.GetIsAllReoInherited))); // 12/7/2009 dd - OPM 43148
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "PresentDebtRatioTop", new Get(m_inputData.GetPresentDebtRatioTop))); // 12/7/2009 dd - OPM 43150
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "NetWorthLessRetirement", new Get(m_inputData.GetNetworthLessRetirement))); // 12/8/2009 dd - OPM 43149
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "TotalAvailReserveMonths", new Get(m_inputData.GetTotalAvailReserveMonths))); // 12/22/2009 dd - OPM 43733
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "AssetsAfterClosing", new Get(m_inputData.GetAssetsAfterClosing))); // 2/17/2010 dd - OPM 45309
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "NonOccCoBorrOnOrigNote", new Get(m_inputData.GetNonOccCoBorrOnOrigNote))); // 3/24/2010 mf - OPM 48192
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "H4HOriginationReqMet", new Get(m_inputData.GetH4HOriginationReqMet))); // 3/24/2010 mf - OPM 48192
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "BorrowerHasFraudConviction", new Get(m_inputData.GetBorrowerHasFraudConviction))); // 3/24/2010 mf - OPM 48192
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "OutstandingPrincipalBalance", new Get(m_inputData.GetOutstandingPrincipalBalance))); // 4/6/2010 fs - OPM 28103
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "FhaCondoApprovalStatus", new Get(m_inputData.GetFhaCondoApprovalStatus))); // 4/8/2010 dd - OPM 45321
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "FhaLoanLimit", new Get(m_inputData.GetFhaLoanLimit))); // 8/24/2010 mF - OPM 55766

                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "FannieLTV", new Get(m_inputData.GetFannieLTV))); // 8/30/2010 dd - OPM 50817
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "FannieCLTV", new Get(m_inputData.GetFannieCLTV))); // 8/30/2010 dd - OPM 50817
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "FannieHCLTV", new Get(m_inputData.GetFannieHCLTV))); // opm 224873
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsTpoLoan", new Get(m_inputData.GetIsTpoLoan))); // 10/13/2010 dd - OPM 41485
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "UFMIPRefund", new Get(m_inputData.GetUFMIPRefund))); // 08/07/2010 mf. OPM 27822
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "HasAppraisal", new Get(m_inputData.GetHasAppraisal))); // 08/07/2010 mf. OPM 27822
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "OriginalAppraisedValue", new Get(m_inputData.GetOriginalAppraisedValue))); // 08/07/2010 mf. OPM 27822
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "OriginatorCompSource", new Get(m_inputData.GetOriginatorCompensationPaymentSource))); // 5/6/2011 dd - OPM 66366

                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "HasSubjPropPriorSaleData", new Get(m_inputData.GetHasSubjPropPriorSaleData)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "PropertySeasoningMonths", new Get(m_inputData.GetPropertySeasoningMonths)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "PropertySeasoningDays", new Get(m_inputData.GetPropertySeasoningDays)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "PriorSalesPrice", new Get(m_inputData.GetPriorSalesPrice)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "PropertySeller", new Get(m_inputData.GetPropertySeller)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "SalesPrice", new Get(m_inputData.GetSalesPrice)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "LTV_USDA", new Get(m_inputData.GetLTV_USDA)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CLTV_USDA", new Get(m_inputData.GetCLTV_USDA)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CreditReportAge", new Get(m_inputData.GetCreditReportAge)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "LoanStatus", new Get(m_inputData.GetLoanStatus)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "InvestorID", new Get(m_inputData.GetInvestorID))); // 3/5/2012 dd - OPM 55761
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsFinancedUFMIPIncludedInLTV", new Get(m_inputData.GetIsFinancedUFMIPIncludedInLTV)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "ARM1stAdjCap", new Get(m_inputData.GetARM1stAdjCap))); // 4/4/2012 mf - OPM 13035
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "ARMNextAdjCap", new Get(m_inputData.GetARMNextAdjCap))); // 4/4/2012 mf - OPM 13035
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "ARMLifeAdjCap", new Get(m_inputData.GetARMLifeAdjCap))); // 4/4/2012 mf - OPM 13035
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "ARMAdjPeriod", new Get(m_inputData.GetARMAdjPeriod))); // 4/4/2012 mf - OPM 13035
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "ARMIndex", new Get(m_inputData.GetARMIndex))); // 4/4/2012 mf - OPM 13035
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "Assumption", new Get(m_inputData.GetAssumption))); // 4/4/2012 mf - OPM 13035
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsConvertibleMortgage", new Get(m_inputData.GetIsConvertibleMortgage))); // 4/4/2012 mf - OPM 13035
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "InterestOnlyMonths", new Get(m_inputData.GetInterestOnlyMonths))); // 4/4/2012 mf - OPM 13035

                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "NumberOfFinancedProperties", new Get(m_inputData.GetNumberOfFinancedProperties)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "HCLTV", new Get(m_inputData.GetHCLTV)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "HELOCLineAmount", new Get(m_inputData.GetHELOCLineAmount)));

                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "RequestCommunityOrAffordableSeconds", new Get(m_inputData.GetRequestCommunityOrAffordableSeconds)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsRenovation", new Get(m_inputData.GetIsRenovation)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "RenovationCosts", new Get(m_inputData.GetRenovationCosts)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "AsCompletedValue", new Get(m_inputData.GetAsCompletedValue)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "NoteRateAfterLOCompAndRounding", new Get(m_inputData.GetNoteRateAfterLOCompAndRounding))); // OPM 87644
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "NoteRateBeforeLOCompAndRounding", new Get(m_inputData.GetNoteRateBeforeLOCompAndRounding))); // OPM 87644
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "PointsBeforeLOCompAndRounding", new Get(m_inputData.GetPointsBeforeLOCompAndRounding))); // OPM 87644
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "ZipCode", new Get(m_inputData.GetZipCode))); // OPM 81571
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "MaxNoteRateOnLoanFile", new Get(m_inputData.GetMaxNoteRateOnLoanFile))); // OPM 104085
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "PMI_Type", new Get(m_inputData.GetPMI_Type))); // OPM 26013
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "SplitMI_Upfront", new Get(m_inputData.GetSplitMI_Upfront))); // OPM 26013
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsUFMIPorFFFinanced", new Get(m_inputData.GetIsUFMIPorFFFinanced))); // OPM 26013
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "LenderIsACreditUnion", new Get(m_inputData.GetLenderIsACreditUnion))); // OPM 26013
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "VAEligibleBorrOnFile", new Get(m_inputData.GetVAEligibleBorrOnFile))); // OPM 32222
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomPMLField_1", new Get(m_inputData.GetCustomPMLField1))); // OPM 108066
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomPMLField_2", new Get(m_inputData.GetCustomPMLField2))); // OPM 108066
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomPMLField_3", new Get(m_inputData.GetCustomPMLField3))); // OPM 108066
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomPMLField_4", new Get(m_inputData.GetCustomPMLField4))); // OPM 108066
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomPMLField_5", new Get(m_inputData.GetCustomPMLField5))); // OPM 108066
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomPMLField_6", new Get(m_inputData.GetCustomPMLField6))); // OPM 108066
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomPMLField_7", new Get(m_inputData.GetCustomPMLField7))); // OPM 108066
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomPMLField_8", new Get(m_inputData.GetCustomPMLField8))); // OPM 108066
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomPMLField_9", new Get(m_inputData.GetCustomPMLField9))); // OPM 108066
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomPMLField_10", new Get(m_inputData.GetCustomPMLField10))); // OPM 108066
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomPMLField_11", new Get(m_inputData.GetCustomPMLField11))); // OPM 471186
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomPMLField_12", new Get(m_inputData.GetCustomPMLField12))); // OPM 471186
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomPMLField_13", new Get(m_inputData.GetCustomPMLField13))); // OPM 471186
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomPMLField_14", new Get(m_inputData.GetCustomPMLField14))); // OPM 471186
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomPMLField_15", new Get(m_inputData.GetCustomPMLField15))); // OPM 471186
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomPMLField_16", new Get(m_inputData.GetCustomPMLField16))); // OPM 471186
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomPMLField_17", new Get(m_inputData.GetCustomPMLField17))); // OPM 471186
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomPMLField_18", new Get(m_inputData.GetCustomPMLField18))); // OPM 471186
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomPMLField_19", new Get(m_inputData.GetCustomPMLField19))); // OPM 471186
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomPMLField_20", new Get(m_inputData.GetCustomPMLField20))); // OPM 471186
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "TotalLiquidAssets", new Get(m_inputData.GetTotalLiquidAssets))); // OPM 32222
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "NoteRateIsIntegerMultipleOfOneEighth", new Get(m_inputData.GetNoteRateIsIntegerMultipleOfOneEighth))); // OPM 82902
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "RateOptionExceedsMaxDTI", new Get(m_inputData.GetRateOptionExceedsMaxDti))); // OPM 82902
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "LeadSourceId", new Get(m_inputData.GetLeadSourceId))); //OPM 124981
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsEmployeeLoan", new Get(m_inputData.GetIsEmployeeLoan))); //OPM 132054
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "RunPmlRequestSourceType", new Get(m_inputData.GetRunPmlRequestSourceType))); // 9/4/2013 dd - OPM 137164
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "LoanRegisteredDate", new Get(m_inputData.GetLoanRegisteredDate))); // OPM 146934
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CompPlanEffectiveDate", new Get(m_inputData.GetCompPlanEffectiveDate))); // OPM 146934
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "APOR", new Get(m_inputData.GetAPOR))); //149232
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsBaseLAMTConformingLAmt", new Get(m_inputData.GetIsBaseLAmtConformingLAmt)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "IsBaseLAMTHighBalanceConformingLAMT", new Get(m_inputData.GetIsBaseLAmtHighBalanceConformingLAmt)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "LoanHasNonExpiredApproval", new Get(m_inputData.GetLoanHasNonExpiredApproval)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomLOPPField_1", new Get(m_inputData.GetCustomPricingPolicyField1)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomLOPPField_2", new Get(m_inputData.GetCustomPricingPolicyField2)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomLOPPField_3", new Get(m_inputData.GetCustomPricingPolicyField3)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomLOPPField_4", new Get(m_inputData.GetCustomPricingPolicyField4)));
                AddEntry(m_entries, new CSymbolFieldName((int)E_Purpose.Condition, "CustomLOPPField_5", new Get(m_inputData.GetCustomPricingPolicyField5)));

                AddEntry(m_entries, new CSymbolFunctionStrParams("CustomLoanProgramField_1", new FunctionStrParams(m_inputData.GetCustomLoanProgramField1))); // OPM 150430
                AddEntry(m_entries, new CSymbolFunctionStrParams("CustomLoanProgramField_2", new FunctionStrParams(m_inputData.GetCustomLoanProgramField2))); // OPM 150430
                AddEntry(m_entries, new CSymbolFunctionStrParams("CustomLoanProgramField_3", new FunctionStrParams(m_inputData.GetCustomLoanProgramField3))); // OPM 150430
                AddEntry(m_entries, new CSymbolFunctionStrParams("CustomLoanProgramField_4", new FunctionStrParams(m_inputData.GetCustomLoanProgramField4))); // OPM 150430
                AddEntry(m_entries, new CSymbolFunctionStrParams("CustomLoanProgramField_5", new FunctionStrParams(m_inputData.GetCustomLoanProgramField5))); // OPM 150430

                AddEntry(m_entries, new CSymbolFunctionStrParams("InvestorLoanProgramField_1", new FunctionStrParams(m_inputData.GetInvestorLoanProgramField1)));
                AddEntry(m_entries, new CSymbolFunctionStrParams("InvestorLoanProgramField_2", new FunctionStrParams(m_inputData.GetInvestorLoanProgramField2)));
                AddEntry(m_entries, new CSymbolFunctionStrParams("InvestorLoanProgramField_3", new FunctionStrParams(m_inputData.GetInvestorLoanProgramField3)));
                AddEntry(m_entries, new CSymbolFunctionStrParams("InvestorLoanProgramField_4", new FunctionStrParams(m_inputData.GetInvestorLoanProgramField4)));
                AddEntry(m_entries, new CSymbolFunctionStrParams("InvestorLoanProgramField_5", new FunctionStrParams(m_inputData.GetInvestorLoanProgramField5)));

                AddEntry(m_entries, new CSymbolFieldName("BranchChannel", new Get(m_inputData.GetBranchChannel))); // OPM 150240
                AddEntry(m_entries, new CSymbolFieldName("CorrespondentProcessType", new Get(m_inputData.GetCorrespondentProcess))); // OPM 150240
                AddEntry(m_entries, new CSymbolFieldName("LenderFeeBuyout", new Get(m_inputData.GetLenderFeeBuyout)));
                AddEntry(m_entries, new CSymbolFieldName("AppraisedValue", new Get(m_inputData.GetAppraisedValue))); // 08/07/2010 mf. OPM 27822
                AddEntry(m_entries, new CSymbolFieldName("ScoreCBS", new Get(m_inputData.GetScoreCBSKeyword)));
                AddEntry(m_entries, new CSymbolFieldName("RateOptionFinalPrice", new Get(m_inputData.GetRateOptionFinalPrice))); // OPM 126752
                AddEntry(m_entries, new CSymbolFieldName("RateOptionPointsPreFEMaxYSP", new Get(m_inputData.GetRateOptionPointsPreFEMaxYSP))); // OPM 126752
                AddEntry(m_entries, new CSymbolFieldName("LOOCCompanyIdFirstFourDigits", new Get(m_inputData.GetLOCCompanyId)));

                AddEntry(m_entries, new CSymbolFieldName("FHAEndorsedBeforeJune2009", new Get(m_inputData.GetsProdIsLoanEndorsedBeforeJune09))); 
                AddEntry(m_entries, new CSymbolFieldName("OCIsDelegatedCorrespondent", new Get(m_inputData.GetOCIsDelegatedCorrespondent))); 
                AddEntry(m_entries, new CSymbolFieldName("OriginatingCompanyTierID", new Get(m_inputData.GetOriginatingCompanyTierID))); // OPM 190542
                AddEntry(m_entries, new CSymbolFieldName("BorrowerType", new Get(m_inputData.GetBorrowerType))); // OPM 184017
                AddEntry(m_entries, new CSymbolFieldName("CashToBorrower", new Get(m_inputData.GetCashToBorrower)));  // opm 233638
                AddEntry(m_entries, new CSymbolFieldName("HasMortgageLiabilityThatWillBePaidOffAtClosing", new Get(m_inputData.GetHasMortgageLiabilityThatWillBePaidOffAtClosing))); // opm 233638
                AddEntry(m_entries, new CSymbolFieldName("LoanType", new Get(m_inputData.GetLoanType)));  // opm 238102
                AddEntry(m_entries, new CSymbolFieldName("BranchIDNumber", new Get(m_inputData.GetBranchIdNumber)));  // opm 246124
                AddEntry(m_entries, new CSymbolFieldName("TotalCashDeposit", new Get(m_inputData.GetTotalCashDeposit)));  // opm 245755
                AddEntry(m_entries, new CSymbolFieldName("2ndLienIsHeloc", new Get(m_inputData.Get2ndLienIsHeloc)));  // opm 246943
                AddEntry(m_entries, new CSymbolFieldName("FannieHomebuyerEducationType", new Get(m_inputData.GetFannieHomebuyerEducationType)));  // opm 350347
                AddEntry(m_entries, new CSymbolFieldName("2ndFinancingInclusionType", new Get(m_inputData.Get2ndFinancingInclusionType)));  // opm 57233
                AddEntry(m_entries, new CSymbolFieldName("IsStudentLoanCashoutRefi", new Get(m_inputData.GetIsStudentLoanCashoutRefi)));  // opm 456304
                AddEntry(m_entries, new CSymbolFieldName("NotPermanentlyAffixed", new Get(m_inputData.GetNotPermanentlyAffixed))); // opm 462879

                AddEntry(m_entries, new CSymbolFieldName("RenovationLTV", new Get(m_inputData.GetRenovationLTV)));      // opm 465371
                AddEntry(m_entries, new CSymbolFieldName("RenovationCLTV", new Get(m_inputData.GetRenovationCLTV)));    // opm 465371
                AddEntry(m_entries, new CSymbolFieldName("RenovationHCLTV", new Get(m_inputData.GetRenovationHCLTV)));  // opm 465371

                AddEntry(m_entries, new CSymbolFieldName("Fha203KType", new Get(m_inputData.GetFha203KType)));  // opm 466774 start
                AddEntry(m_entries, new CSymbolFieldName("CostConstrRepairsRehab", new Get(m_inputData.GetCostConstrRepairsRehab)));
                AddEntry(m_entries, new CSymbolFieldName("ArchitectEngineerFee", new Get(m_inputData.GetArchitectEngineerFee)));
                AddEntry(m_entries, new CSymbolFieldName("ConsultantFee", new Get(m_inputData.GetConsultantFee)));
                AddEntry(m_entries, new CSymbolFieldName("RenovationConstrInspectFee", new Get(m_inputData.GetRenovationConstrInspectFee)));
                AddEntry(m_entries, new CSymbolFieldName("TitleUpdateFee", new Get(m_inputData.GetTitleUpdateFee)));
                AddEntry(m_entries, new CSymbolFieldName("PermitFee", new Get(m_inputData.GetPermitFee)));
                AddEntry(m_entries, new CSymbolFieldName("FeasibilityStudyFee", new Get(m_inputData.GetFeasibilityStudyFee)));
                AddEntry(m_entries, new CSymbolFieldName("RepairImprovementCostFee", new Get(m_inputData.GetRepairImprovementCostFee)));
                AddEntry(m_entries, new CSymbolFieldName("FinanceContingencyReserve", new Get(m_inputData.GetFinanceContingencyReserve))); // opm 466774 end

                // OPM 471311
                AddEntry(m_entries, new CSymbolFieldName("LoanFileType", new Get(m_inputData.GetLoanFileType)));

                AddEntry(m_entries, new CSymbolFieldName("OriginatingCompanyIndexNumber", new Get(m_inputData.GetOriginatingCompanyIndexNumber)));

                AddEntry(m_entries, new CSymbolFieldName("ConventionalLtvForMiNeed", new Get(m_inputData.GetProdConvLoanLtvForMiNeed)));
                #endregion
            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking("Error : can not build SymbolTable.", exc);
                return null;
            }
            finally
            {
                if (null != m_loanData)
                    m_loanData.SetFormatTarget(storedFormatTarget);
            }

            // append unofficial keywords
            try
            {
                if (m_newKeywords == null)
                {
                    // Note : NewLpeKeywordCenter.NewLpeKeywords only gives the "correct" keywords.
                    //        In other words, NewLpeKeywordCenter.NewLpeKeywords ready removes invalid keywords
                    m_newKeywords = NewLpeKeywordCenter.NewLpeKeywords;
                }

                foreach (CSymbolEntry symbol in m_newKeywords.Values)
                    AddEntry(m_entries, symbol);
            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking("Fail to append new keywords", exc);

            }

            return m_entries;

        }

        public int ActiveApplication
        {
            set 
            { 
                m_activeApplication = value;
            }
        }

        public CaseInsensitiveHashtable Entries
        {
            get
            {
                return GetEntries(m_activeApplication);
            }
        }

        private CaseInsensitiveHashtable GetEntries(int appIndex)
        {
            if (m_entriesTable[appIndex] == null)
            {
                m_entriesTable[appIndex] = AllocateEntries(appIndex);
            }

            return (CaseInsensitiveHashtable)m_entriesTable[appIndex];
        }


		internal Evaluator LookupEvaluator( string xmlOperator )
		{
            if( s_fastAccessEvaluators )
            {
                CSymbolEntry entry = (CSymbolEntry)s_evaluatorEntries[xmlOperator];
                if( entry != null )
                    return entry.ExpressionEvaluator;
            }
            else
            {
                Hashtable entries = Entries;
                foreach( DictionaryEntry obj in entries )
                {
                    CSymbolEntry entry = (CSymbolEntry) obj.Value;
                    if( entry.XmlOperator == xmlOperator )
                        return entry.ExpressionEvaluator;
                }
            }
			throw new CBaseException( ErrorMessages.Generic, "Programming error:  Cannot find evaluator method for operator named: " + xmlOperator );
		}
		static internal string StateValue( string state )
		{
            #region State Name to State value
			switch( state.TrimWhitespaceAndBOM().ToUpper() )
			{
				case "": return "NaN";
				case "AL": return "1";
				case "AK": return "2";
				case "AZ": return "3";
				case "AR": return "4";
				case "CA": return "5";
				case "CO": return "6";
				case "CT": return "7";
				case "DC": return "8";
				case "DE": return "9";
				case "FL": return "10";
				case "GA": return "11";
				case "HI": return "12";
				case "ID": return "13";
				case "IL": return "14";
				case "IN": return "15";
				case "IA": return "16";
				case "KS": return "17";
				case "KY": return "18";
				case "LA": return "19";
				case "ME": return "20";
				case "MD": return "21";
				case "MA": return "22";
				case "MI": return "23";
				case "MN": return "24";
				case "MS": return "25";
				case "MO": return "26";
				case "MT": return "27";
				case "NE": return "28";
				case "NV": return "29";
				case "NH": return "30";
				case "NJ": return "31";
				case "NM": return "32";
				case "NY": return "33";
				case "NC": return "34";
				case "ND": return "35";
				case "OH": return "36";
				case "OK": return "37";
				case "OR": return "38";
				case "PA": return "39";
				case "RI": return "40";
				case "SC": return "41";
				case "SD": return "42";
				case "TN": return "43";
				case "TX": return "44";
				case "UT": return "45";
				case "VT": return "46";
				case "VA": return "47";
				case "WA": return "48";
				case "WV": return "49";
				case "WI": return "50";
				case "WY": return "51";
                    // 4/11/2012 dd - OPM 65673 - Add US Territory
                case "PR": return "52";
                case "VI": return "53";
                case "GU": return "54";
                case "AS": return "55";
                case "MP": return "56";
				default: return "NaN";
			}
            #endregion
		}
		internal int Count
		{
			get { return Entries.Count; }
		}

        public string[] GetValidSymbols( E_Purpose purpose )
        {
            return GetValidSymbols( purpose, null );
        }

		public string[] GetValidSymbols( E_Purpose purpose, Hashtable outNewKeywords  )
		{
            if( outNewKeywords != null )
                outNewKeywords.Clear();

			int countAll = Count;
			ArrayList validSymbols = new ArrayList( countAll );
			foreach( DictionaryEntry obj in Entries )
			{
				CSymbolEntry entry = (CSymbolEntry) obj.Value;

                if( CRuleEvalTool.HasPurpose( entry.Purposes, purpose ) )
                {
                    string keyword = entry.InputKeyword;
                    validSymbols.Add( keyword );
                    if( outNewKeywords != null && entry.IsPeudoKeyword )
                        outNewKeywords[ keyword ] = keyword;
                }
			}
			
			validSymbols.Sort();
			return ( string[] ) validSymbols.ToArray( typeof(string) );
		}

        public string Dump()
        {
            StringBuilder sb = new StringBuilder();
            int count = 0;
            int numOp = 0;
            bool onlyDisplayOp = false;
            foreach( CSymbolEntry entry in Entries.Values)
            {
                count++;
                if( onlyDisplayOp )
                {
                    if( entry.XmlOperator != "")
                    {
                        numOp++;
                        sb.AppendFormat("{0}.  {1}    ======   {2}\n", numOp, entry. InputKeyword, entry.XmlOperator);
                    }
                }
                else
                    sb.AppendFormat("{0}.  {1}\n", count, entry. InputKeyword);
            }
            return sb.ToString();
        }

        public Dictionary<string, decimal> GetValues()
        {
            // For the purpose of debugging, pull all resulting values from keywords

            Dictionary<string, decimal> values = new Dictionary<string, decimal>();

            foreach (CSymbolEntry entry in Entries.Values)
            {
                if (entry is CSymbolFieldName)
                {
                    var fieldEntry = (CSymbolFieldName)entry;
                    try
                    {
                        values.Add(fieldEntry.InputKeyword, fieldEntry.Get());
                    }
                    catch (InvalidCastException exc)
                    {
                        Tools.LogWarning("Could not get debug keyword value of:" + fieldEntry.InputKeyword, exc);
                    }
                    catch (CBaseException exc)
                    {
                        // Some keywords require specific loan/lender setup.
                        // Let them be missed.  Nothing we can do at this point.
                        Tools.LogWarning("Could not get debug keyword value of:" + fieldEntry.InputKeyword, exc);
                    }
                }
            }

            return values;
        }

        static readonly Hashtable NewKeywordEmpty = new Hashtable();
        static public CSymbolTable CreateSymbolTableWithoutNewKeywords()
        {
            return new CSymbolTable( null, NewKeywordEmpty /* not include new keywords */ );
        }

	}
}