using System;
using System.Collections;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;
using LendersOfficeApp.los.RatePrice;


namespace Toolbox.LoanProgram
{
    public class EventHandlers
	{
		static private object s_lockPad  = new object();
		#region PUBLIC METHODS
		static public void AfterModifOfThisMaster( Guid masterProdId, Guid brokerId )
		{
			lock( s_lockPad )
			{
				try
				{
                    CAssocInheritUpdate updator = CAssocInheritUpdate.GetUpdatorForExistingMasterNoUiAction( masterProdId, brokerId );
					Hashtable affectedNonmasterProds = updator.UpdateAndSpread( true );

                    if( null == affectedNonmasterProds || affectedNonmasterProds.Count <= 0 )
                        return;

                    // We want to spread from the products that have at least one product derives from it.
                    // We still have redudancy here. The optimal way is to spread only from the root of each
                    // derivation branch  that got affected.
                    object baseProdMarker = new object();
                    using( DbDataReader r = DataAccess.StoredProcedureHelper.ExecuteReader( DataSrc.LpeSrc, "ListIdsOfProductsHaveDerivedProds" ) )
                    {
                        while( r.Read() )
                        {
                            Guid readId = new Guid( r[ "lBaseLpId" ].ToString() );
            
                            if( affectedNonmasterProds.ContainsKey( readId ) )
                                affectedNonmasterProds[ readId ] = baseProdMarker;
                        }
                    }
                    
                    foreach( Guid baseProdId in affectedNonmasterProds.Keys )
                    {
                        if( baseProdMarker == affectedNonmasterProds[ baseProdId ] )
                            ProdDeriveCacheUpdate.SpreadChangesFor( baseProdId, true /*updateAssociateTable*/ );
                     }
				}
				catch( Exception ex )
				{
					Tools.LogErrorWithCriticalTracking( string.Format( "AfterModifOfThisMaster failed. BrokerId={0}. MasterProdId={1}", 
						brokerId.ToString(), masterProdId.ToString() ), ex );
					throw ex;
				}
			}
		}

		static public void AfterCreationOfThisMaster( Guid masterProdId, Guid brokerId )
		{
			lock( s_lockPad )
			{
				try
				{
					Tools.LogRegTest( string.Format( "EventHandlers:AfterCreationOfThisMaster() is called for product with id = {0}",
						masterProdId.ToString() ) );
                    // TODO: There is still redundancy, brand new master never doesn't change any of its products because
                    // it could only have what's the master above has and its products already got those associations.
                    // The fix is to make sure the Optimization On/Off is honored, right now we always turns it off.
                    CAssocInheritUpdate updator = CAssocInheritUpdate.GetUpdatorForNewProd( masterProdId, brokerId );
                    updator.UpdateAndSpread( true );

                    // NOTE: I don't need to spread from the affected non-master prodcuts because new master should inherit
                    // straight everything from its above master so the affected products shouldn't have any net-result association changes.
				}
				catch( Exception ex )
				{
					Tools.LogErrorWithCriticalTracking( string.Format( "AfterCreationOfThisMaster failed. BrokerId={0}. MasterProdId={1}", 
						brokerId.ToString(), masterProdId.ToString() ), ex );
					throw ex;
				}
			}
		}

		static public void AfterModifForThisNonmasterProd( Guid prodId, Guid brokerId )
		{
			// TODO: Do one batch at a time to be safe, hm, I might have to do this exclusive protection
			// at a time. What about if some users is using the engine while 
			// we are doing this.
			lock( s_lockPad )
			{
				try
				{
					ProdDeriveCacheUpdate.SpreadChangesFor( prodId, true /* updateAssociateTable */ );
				}
				catch( Exception ex )
				{
					Tools.LogErrorWithCriticalTracking( string.Format( "AfterModifForThisNonmasterProd failed. BrokerId={0}. prodId={1}", 
						brokerId.ToString(), prodId.ToString() ), ex );
					throw ex;
				}
			}
		}

        static public void IndependentAfterDerivationForThisNonmasterProd(Guid prodId, Guid baseProdId, Guid brokerId)
        {
            lock (s_lockPad)
            {
                try
                {
                    Tools.LogInfo($"EventHandlers:AfterCreationForThisNonmasterProd() is called for product with id = {prodId}");
                    ProdDeriveCacheUpdate.UpdateProductFromSomeBasedProduct(prodId, baseProdId);
                    AfterModifForThisNonmasterProd(prodId, brokerId); // call for consistent. I expect this product has no child product at this time.
                }
                catch (Exception ex)
                {
                    Tools.LogErrorWithCriticalTracking($"IndependentAfterDerivationForThisNonmasterProd failed. BrokerId={brokerId}, prodId={prodId}", ex);
                    throw;
                }
            }
        }

        static public void AfterCreationForThisNonmasterProd( Guid prodId, Guid brokerId )
		{
			lock( s_lockPad )
			{
				try
				{                   
					Tools.LogRegTest( string.Format( "EventHandlers:AfterCreationForThisNonmasterProd() is called for product with id = {0}",
						prodId.ToString() ) );

                    CAssocInheritUpdate updator = CAssocInheritUpdate.GetUpdatorForNewProd( prodId, brokerId );
                    updator.UpdateAndSpread( true );
				}
				catch( Exception ex )
				{
					Tools.LogErrorWithCriticalTracking( string.Format( "AfterCreationForThisNonmasterProd failed. BrokerId={0}. prodId={1}", 
						brokerId.ToString(), prodId.ToString() ), ex );
					throw ex;
				}
			}
		}

		static public void AfterDerivationForThisNonmasterProd( Guid baseProdId, Guid destinationBrokerId )
		{
			lock( s_lockPad )
			{
				try
				{
					// TODO: We need to trim down and just let the base spread to the newly derived product
					LoanProgram.ProdDeriveCacheUpdate.SpreadChangesFor( baseProdId, true /* updateAssociateTable */ ); 
				}
				catch( Exception ex )
				{
					Tools.LogErrorWithCriticalTracking( string.Format( "AfterDerivationForThisNonmasterProd failed. Destination BrokerId={0}. baseProdId={1}", 
						destinationBrokerId.ToString(), baseProdId.ToString() ), ex );
					throw ex;
				}
			}
		}

		static public void AfterLoadminRequestFixupAllLoanProgramsInSystem()
		{
			lock( s_lockPad )
			{
				try
				{
					// step 1: Remove all the product and master inheritance cached bit, this is to avoid
					// scenarios where orphan products who still get an assoction inherited somehow (bug etc..)

					DataAccess.StoredProcedureHelper.ExecuteNonQuery( DataSrc.LpeSrc, "ClearAllAssocInheritanceCachedVal", 3 );

					// step 2
					MasterInheritCacheUpdate.SpreadFromAllMastersInSystem();
			
					// step 3: Force to add them all back in, that is calling to spread for the nonmaster products
					// with derived products and doesn't have base, that's how we are going to 
					// start from only the roots.
					LoanProgram.ProdDeriveCacheUpdate.UpdateAllNonmasterProds( true /* updateAssociateTable */);
				}
				catch( Exception ex )
				{
					Tools.LogErrorWithCriticalTracking( string.Format( "AfterLoadminRequestFixupAllLoanProgramsInSystem failed.", ex ) );
					throw ex;
				}
			}
		}


        static public void AfterUpdateAllNonmasterProds()
        {
            lock( s_lockPad )
            {
                try
                {
                    LoanProgram.ProdDeriveCacheUpdate.UpdateAllNonmasterProds(true /* updateAssociateTable */);
                }
                catch( Exception ex )
                {
                    Tools.LogErrorWithCriticalTracking( ex );
                    throw ex;
                }
            }
        }

        static public void AfterLpeUpdateProjectIsRun()
        {
            // LpeUpdate only modifies data related to rateoptions and some adjustments
            // we don't need to spread the policy association. Thinh 9/26/06
            AfterRateOptionsBatchUpdate();
        }


		static public void AfterRateOptionsBatchUpdate()
		{
            DateTime startTime = DateTime.Now;

			// 2/17/05 tn: LpeUpdate does 2 things worth considering handling.
			// 1) Update the rate options of existing non-master loan products. Need to handle this. There might 
			//			be many calls to the RateOptionImport::Store() methods for each execution of LpeUpdate
			// 2) Update the consequence of the existing rules via RateAdjustImport object. No handling is needed.
			int tryMax = 2;
			for( int i = 0; i < tryMax; i++ )
			{
				try
				{
					lock( s_lockPad )
					{
						// Only rates, pp, lock period, and individual rules have been changed, no association should be affected.
						LoanProgram.ProdDeriveCacheUpdate.UpdateOnlyProgramsMarkedAsNeedToSpreadData();
                    }
					return;
				}
				catch( Exception ex )
				{
					if( i >= ( tryMax - 1 ) )
					{
                        CBaseException exception = new CBaseException(ErrorMessages.SpreadRateOptionFailed, ex);
                        Tools.LogErrorWithCriticalTracking(exception);
                        return;
					}
					Tools.LogWarning( string.Format( "Call to LoanProgram.ProdDeriveCacheUpdate.AfterRateOptionsBatchUpdate() fails, i = {0} retrying...in 10 seconds", i.ToString() ), ex );
                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep( 10000 );
				}
			}   
            Tools.LogWarning("AfterLpeUpdateProjectIsRun() finish : " +  CommonLib.TimeTool.GetDurationString(startTime) );

		}

		#endregion // PUBLIC METHODS
	}
}