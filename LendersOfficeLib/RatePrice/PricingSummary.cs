﻿// <copyright file="PricingSummary.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Matthew Flynn 
//  Date:   01/27/2016
// </summary>

namespace LendersOffice.RatePrice
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using LendersOffice.Common;

    /// <summary>
    /// This class stores debug data for a pricing execution run.
    /// It is hoped that this class can be a "dumb" collector class
    /// so that its callers can actually do whatever processing with
    /// the data.  Inital design calls for keywords values, but we
    /// can also add in additional data about the pricing run.
    /// </summary>
    [DataContract]
    public class PricingSummary
    {
        /// <summary>
        /// Gets a dictonary containing the keyword value pairs for Phase 0 of pricing execution.
        /// </summary>
        /// <value>The keyword-value pairs from Phase 0 execution.</value>
        [DataMember]
        public Dictionary<string, decimal> PhaseZeroKeywords { get; private set; }

        /// <summary>
        /// Gets a dictonary containing the keyword value pairs for main pricing execution.
        /// </summary>
        /// <value>The keyword-value pairs from main execution.</value>
        [DataMember]
        public Dictionary<string, decimal> MainExecutionKeywords { get; private set; }

        /// <summary>
        /// Gets a dictonary containing the keyword value pairs for per-rate pricing execution.
        /// </summary>
        /// <value>The keyword-value pairs from per-rate execution.</value>
        [DataMember]
        public Dictionary<string, List<decimal>> PerRateExecutionKeywords { get; private set; }

        /// <summary>
        /// Gets a Pricing summary from the json string.
        /// </summary>
        /// <param name="json">The json string.</param>
        /// <returns>A new instance based on the json.</returns>
        public static PricingSummary FromJson(string json)
        {
            return ObsoleteSerializationHelper.JsonDeserialize<PricingSummary>(json);
        }

        /// <summary>
        /// Pulls phase 0 values into object.
        /// </summary>
        /// <param name="keywordValues">The keyword-value pairs.</param>
        public void DumpPhaseZero(IDictionary<string, decimal> keywordValues)
        {
            if (this.PhaseZeroKeywords != null && this.PhaseZeroKeywords.Count != 0)
            {
                throw new GenericUserErrorMessageException("Do not expect Phase 0 multiple times.");
            }

            this.PhaseZeroKeywords = new Dictionary<string, decimal>(keywordValues);
        }

        /// <summary>
        /// Pulls main execution values into object.
        /// </summary>
        /// <param name="keywordValues">The keyword-value pairs.</param>
        public void DumpMainExecution(IDictionary<string, decimal> keywordValues)
        {
            if (this.MainExecutionKeywords != null && this.MainExecutionKeywords.Count != 0)
            {
                throw new GenericUserErrorMessageException("Do not expect main execution multiple times.");
            }

            this.MainExecutionKeywords = new Dictionary<string, decimal>(keywordValues);
        }

        /// <summary>
        /// Pulls per-rate values into object.
        /// </summary>
        /// <param name="keywordValues">The keyword-value pairs.</param>
        public void DumpPerRate(IDictionary<string, decimal> keywordValues)
        {
            if (this.PerRateExecutionKeywords == null)
            {
                this.PerRateExecutionKeywords = new Dictionary<string, List<decimal>>();
            }

            foreach (var keyword in keywordValues.Keys)
            {
                List<decimal> phaseValues;
                if (this.PerRateExecutionKeywords.TryGetValue(keyword, out phaseValues) == false)
                {
                    phaseValues = new List<decimal>();
                    this.PerRateExecutionKeywords.Add(keyword, phaseValues);
                }

                this.PerRateExecutionKeywords[keyword].Add(keywordValues[keyword]);
            }
        }

        /// <summary>
        /// Json string export of this object.
        /// </summary>
        /// <returns>Json string of object.</returns>
        public string ToJson()
        {
            return ObsoleteSerializationHelper.JsonSerialize(this);
        }
    }
}
