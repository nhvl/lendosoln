﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.RatePrice.FileBasedPricing;
using LendersOfficeApp.los.RatePrice;

namespace LendersOffice.RatePrice
{
    /// <summary>
    /// The hybrid loan program template return LOAN_PROGRAM_TEMPLATE_ from current snapshot
    /// But rate options from historical price
    /// </summary>
    public class HybridLoanProgramTemplate : ILoanProgramTemplate
    {
        private CLoanProductData m_snapshotLoanProgramTemplate;
        private FileBasedLoanProgramTemplate m_fileBasedLoanProgramTemplate;

        public HybridLoanProgramTemplate(CLoanProductData snapshotLoanProgramTemplate, FileBasedLoanProgramTemplate fileBasedLoanProgramTemplate)
        {
            m_snapshotLoanProgramTemplate = snapshotLoanProgramTemplate;
            m_fileBasedLoanProgramTemplate = fileBasedLoanProgramTemplate;
        }
        #region ILoanProgramTemplate Members

        public Guid lLpTemplateId
        {
            get { return m_snapshotLoanProgramTemplate.lLpTemplateId; }
        }

        public Guid BrokerId
        {
            get { return m_snapshotLoanProgramTemplate.BrokerId; }
        }

        public string lLpTemplateNm
        {
            get { return m_snapshotLoanProgramTemplate.lLpTemplateNm; }
        }

        public string lLendNm
        {
            get { return m_snapshotLoanProgramTemplate.lLendNm; }
        }

        public E_sLT lLT
        {
            get { return m_snapshotLoanProgramTemplate.lLT; }
        }

        public E_sLienPosT lLienPosT
        {
            get { return m_snapshotLoanProgramTemplate.lLienPosT; }
        }

        public decimal lQualR
        {
            get { return m_snapshotLoanProgramTemplate.lQualR; }
        }

        public int lTerm
        {
            get { return m_snapshotLoanProgramTemplate.lTerm; }
        }

        public string lTerm_rep
        {
            get { return m_snapshotLoanProgramTemplate.lTerm_rep; }
        }

        public int lDue
        {
            get { return m_snapshotLoanProgramTemplate.lDue; }
        }

        public string lDue_rep
        {
            get { return m_snapshotLoanProgramTemplate.lDue_rep; }
        }

        public int lLockedDays
        {
            get { return m_snapshotLoanProgramTemplate.lLockedDays; }
        }

        public string lLockedDays_rep
        {
            get { return m_snapshotLoanProgramTemplate.lLockedDays_rep; }
        }

        public decimal lReqTopR
        {
            get { return m_snapshotLoanProgramTemplate.lReqTopR; }
        }

        public string lReqTopR_rep
        {
            get { return m_snapshotLoanProgramTemplate.lReqTopR_rep; }
        }

        public decimal lReqBotR
        {
            get { return m_snapshotLoanProgramTemplate.lReqBotR; }
        }

        public string lReqBotR_rep
        {
            get { return m_snapshotLoanProgramTemplate.lReqBotR_rep; }
        }

        public decimal lRadj1stCapR
        {
            get { return m_snapshotLoanProgramTemplate.lRadj1stCapR; }
        }

        public string lRadj1stCapR_rep
        {
            get { return m_snapshotLoanProgramTemplate.lRadj1stCapR_rep; }
        }

        public int lRadj1stCapMon
        {
            get { return m_snapshotLoanProgramTemplate.lRadj1stCapMon; }
        }

        public string lRadj1stCapMon_rep
        {
            get { return m_snapshotLoanProgramTemplate.lRadj1stCapMon_rep; }
        }

        public decimal lRAdjCapR
        {
            get { return m_snapshotLoanProgramTemplate.lRAdjCapR; }
        }

        public string lRAdjCapR_rep
        {
            get { return m_snapshotLoanProgramTemplate.lRAdjCapR_rep; }
        }

        public int lRAdjCapMon
        {
            get { return m_snapshotLoanProgramTemplate.lRAdjCapMon; }
        }

        public string lRAdjCapMon_rep
        {
            get { return m_snapshotLoanProgramTemplate.lRAdjCapMon_rep; }
        }

        public decimal lRAdjLifeCapR
        {
            get { return m_snapshotLoanProgramTemplate.lRAdjLifeCapR; }
        }

        public string lRAdjLifeCapR_rep
        {
            get { return m_snapshotLoanProgramTemplate.lRAdjLifeCapR_rep; }
        }

        public decimal lRAdjMarginR
        {
            get
            {
                return m_snapshotLoanProgramTemplate.lRAdjMarginR;
            }
            set
            {
                m_snapshotLoanProgramTemplate.lRAdjMarginR = value;
            }
        }

        public string lRAdjMarginR_rep
        {
            get { return m_snapshotLoanProgramTemplate.lRAdjMarginR_rep; }
        }

        public decimal lRAdjIndexR
        {
            get { return m_snapshotLoanProgramTemplate.lRAdjIndexR; }
        }

        public string lRAdjIndexR_rep
        {
            get { return m_snapshotLoanProgramTemplate.lRAdjIndexR_rep; }
        }

        public decimal lRAdjFloorR
        {
            get { return m_snapshotLoanProgramTemplate.lRAdjFloorR; }
        }

        public string lRAdjFloorR_rep
        {
            get { return m_snapshotLoanProgramTemplate.lRAdjFloorR_rep; }
        }

        public E_sRAdjRoundT lRAdjRoundT
        {
            get { return m_snapshotLoanProgramTemplate.lRAdjRoundT; }
        }

        public decimal lPmtAdjCapR
        {
            get { return m_snapshotLoanProgramTemplate.lPmtAdjCapR; }
        }

        public string lPmtAdjCapR_rep
        {
            get { return m_snapshotLoanProgramTemplate.lPmtAdjCapR_rep; }
        }

        public int lPmtAdjCapMon
        {
            get { return m_snapshotLoanProgramTemplate.lPmtAdjCapMon; }
        }

        public string lPmtAdjCapMon_rep
        {
            get { return m_snapshotLoanProgramTemplate.lPmtAdjCapMon_rep; }
        }

        public int lPmtAdjRecastPeriodMon
        {
            get { return m_snapshotLoanProgramTemplate.lPmtAdjRecastPeriodMon; }
        }

        public string lPmtAdjRecastPeriodMon_rep
        {
            get { return m_snapshotLoanProgramTemplate.lPmtAdjRecastPeriodMon_rep; }
        }

        public int lPmtAdjRecastStop
        {
            get { return m_snapshotLoanProgramTemplate.lPmtAdjRecastStop; }
        }

        public string lPmtAdjRecastStop_rep
        {
            get { return m_snapshotLoanProgramTemplate.lPmtAdjRecastStop_rep; }
        }

        public decimal lBuydwnR1
        {
            get { return m_snapshotLoanProgramTemplate.lBuydwnR1; }
        }

        public string lBuydwnR1_rep
        {
            get { return m_snapshotLoanProgramTemplate.lBuydwnR1_rep; }
        }

        public decimal lBuydwnR2
        {
            get { return m_snapshotLoanProgramTemplate.lBuydwnR2; }
        }

        public string lBuydwnR2_rep
        {
            get { return m_snapshotLoanProgramTemplate.lBuydwnR2_rep; }
        }

        public decimal lBuydwnR3
        {
            get { return m_snapshotLoanProgramTemplate.lBuydwnR3; }
        }

        public string lBuydwnR3_rep
        {
            get { return m_snapshotLoanProgramTemplate.lBuydwnR3_rep; }
        }

        public decimal lBuydwnR4
        {
            get { return m_snapshotLoanProgramTemplate.lBuydwnR4; }
        }

        public string lBuydwnR4_rep
        {
            get { return m_snapshotLoanProgramTemplate.lBuydwnR4_rep; }
        }

        public decimal lBuydwnR5
        {
            get { return m_snapshotLoanProgramTemplate.lBuydwnR5; }
        }

        public string lBuydwnR5_rep
        {
            get { return m_snapshotLoanProgramTemplate.lBuydwnR5_rep; }
        }

        public int lBuydwnMon1
        {
            get { return m_snapshotLoanProgramTemplate.lBuydwnMon1; }
        }

        public string lBuydwnMon1_rep
        {
            get { return m_snapshotLoanProgramTemplate.lBuydwnMon1_rep; }
        }

        public int lBuydwnMon2
        {
            get { return m_snapshotLoanProgramTemplate.lBuydwnMon2; }
        }

        public string lBuydwnMon2_rep
        {
            get { return m_snapshotLoanProgramTemplate.lBuydwnMon2_rep; }
        }

        public int lBuydwnMon3
        {
            get { return m_snapshotLoanProgramTemplate.lBuydwnMon3; }
        }

        public string lBuydwnMon3_rep
        {
            get { return m_snapshotLoanProgramTemplate.lBuydwnMon3_rep; }
        }

        public int lBuydwnMon4
        {
            get { return m_snapshotLoanProgramTemplate.lBuydwnMon4; }
        }

        public string lBuydwnMon4_rep
        {
            get { return m_snapshotLoanProgramTemplate.lBuydwnMon4_rep; }
        }

        public int lBuydwnMon5
        {
            get { return m_snapshotLoanProgramTemplate.lBuydwnMon5; }
        }

        public string lBuydwnMon5_rep
        {
            get { return m_snapshotLoanProgramTemplate.lBuydwnMon5_rep; }
        }

        public int lGradPmtYrs
        {
            get { return m_snapshotLoanProgramTemplate.lGradPmtYrs; }
        }

        public string lGradPmtYrs_rep
        {
            get { return m_snapshotLoanProgramTemplate.lGradPmtYrs_rep; }
        }

        public decimal lGradPmtR
        {
            get { return m_snapshotLoanProgramTemplate.lGradPmtR; }
        }

        public string lGradPmtR_rep
        {
            get { return m_snapshotLoanProgramTemplate.lGradPmtR_rep; }
        }

        public int lIOnlyMon
        {
            get { return m_snapshotLoanProgramTemplate.lIOnlyMon; }
        }

        public string lIOnlyMon_rep
        {
            get { return m_snapshotLoanProgramTemplate.lIOnlyMon_rep; }
        }

        public bool lHasVarRFeature
        {
            get { return m_snapshotLoanProgramTemplate.lHasVarRFeature; }
        }

        public string lVarRNotes
        {
            get { return m_snapshotLoanProgramTemplate.lVarRNotes; }
        }

        public bool lAprIncludesReqDeposit
        {
            get { return m_snapshotLoanProgramTemplate.lAprIncludesReqDeposit; }
        }

        public bool lHasDemandFeature
        {
            get { return m_snapshotLoanProgramTemplate.lHasDemandFeature; }
        }

        public string lLateDays
        {
            get { return m_snapshotLoanProgramTemplate.lLateDays; }
        }

        public string lLateChargePc
        {
            get { return m_snapshotLoanProgramTemplate.lLateChargePc; }
        }

        public E_sPrepmtPenaltyT lPrepmtPenaltyT
        {
            get { return m_snapshotLoanProgramTemplate.lPrepmtPenaltyT; }
        }

        public E_sAssumeLT lAssumeLT
        {
            get { return m_snapshotLoanProgramTemplate.lAssumeLT; }
        }

        public Guid lCcTemplateId
        {
            get { return m_snapshotLoanProgramTemplate.lCcTemplateId; }
        }

        public string lFilingF
        {
            get { return m_snapshotLoanProgramTemplate.lFilingF; }
        }

        public string lLateChargeBaseDesc
        {
            get { return m_snapshotLoanProgramTemplate.lLateChargeBaseDesc; }
        }

        public decimal lPmtAdjMaxBalPc
        {
            get { return m_snapshotLoanProgramTemplate.lPmtAdjMaxBalPc; }
        }

        public string lPmtAdjMaxBalPc_rep
        {
            get { return m_snapshotLoanProgramTemplate.lPmtAdjMaxBalPc_rep; }
        }

        public E_sFinMethT lFinMethT
        {
            get { return m_snapshotLoanProgramTemplate.lFinMethT; }
        }

        public string lFinMethDesc
        {
            get { return m_snapshotLoanProgramTemplate.lFinMethDesc; }
        }

        public E_sPrepmtRefundT lPrepmtRefundT
        {
            get { return m_snapshotLoanProgramTemplate.lPrepmtRefundT; }
        }

        public Guid FolderId
        {
            get { return m_snapshotLoanProgramTemplate.FolderId; }
        }

        public CDateTime lRateSheetEffectiveD
        {
            get { return m_snapshotLoanProgramTemplate.lRateSheetEffectiveD; }
        }

        public CDateTime lRateSheetExpirationD
        {
            get { return m_snapshotLoanProgramTemplate.lRateSheetExpirationD; }
        }

        public bool IsMaster
        {
            get { return m_snapshotLoanProgramTemplate.IsMaster; }
        }

        public decimal lLpeFeeMin
        {
            get { return m_snapshotLoanProgramTemplate.lLpeFeeMin; }
        }

        public string lLpeFeeMin_rep
        {
            get { return m_snapshotLoanProgramTemplate.lLpeFeeMin_rep; }
        }

        public decimal lLpeFeeMax
        {
            get { return m_snapshotLoanProgramTemplate.lLpeFeeMax; }
        }

        public string lLpeFeeMax_rep
        {
            get { return m_snapshotLoanProgramTemplate.lLpeFeeMax_rep; }
        }

        public string PairingProductIds
        {
            get { return m_snapshotLoanProgramTemplate.PairingProductIds; }
        }

        public Guid lBaseLpId
        {
            get { return m_snapshotLoanProgramTemplate.lBaseLpId; }
        }

        public bool IsEnabled
        {
            get { return m_snapshotLoanProgramTemplate.IsEnabled; }
        }

        public Guid lArmIndexGuid
        {
            get { return m_snapshotLoanProgramTemplate.lArmIndexGuid; }
        }

        public string lArmIndexBasedOnVstr
        {
            get { return m_snapshotLoanProgramTemplate.lArmIndexBasedOnVstr; }
        }

        public string lArmIndexCanBeFoundVstr
        {
            get { return m_snapshotLoanProgramTemplate.lArmIndexCanBeFoundVstr; }
        }

        public bool lArmIndexAffectInitIRBit
        {
            get { return m_snapshotLoanProgramTemplate.lArmIndexAffectInitIRBit; }
        }

        public decimal lRAdjRoundToR
        {
            get { return m_snapshotLoanProgramTemplate.lRAdjRoundToR; }
        }

        public CDateTime lArmIndexEffectiveD
        {
            get { return m_snapshotLoanProgramTemplate.lArmIndexEffectiveD; }
        }

        public string lArmIndexEffectiveD_rep
        {
            get { return m_snapshotLoanProgramTemplate.lArmIndexEffectiveD_rep; }
        }

        public E_sFreddieArmIndexT lFreddieArmIndexT
        {
            get { return m_snapshotLoanProgramTemplate.lFreddieArmIndexT; }
        }

        public string lArmIndexNameVstr
        {
            get { return m_snapshotLoanProgramTemplate.lArmIndexNameVstr; }
        }

        public E_sArmIndexT lArmIndexT
        {
            get { return m_snapshotLoanProgramTemplate.lArmIndexT; }
        }

        public string lArmIndexNotifyAtLeastDaysVstr
        {
            get { return m_snapshotLoanProgramTemplate.lArmIndexNotifyAtLeastDaysVstr; }
        }

        public string lArmIndexNotifyNotBeforeDaysVstr
        {
            get { return m_snapshotLoanProgramTemplate.lArmIndexNotifyNotBeforeDaysVstr; }
        }

        public string lLpTemplateNmInherit
        {
            get { return m_snapshotLoanProgramTemplate.lLpTemplateNmInherit; }
        }

        public bool lLpTemplateNmOverrideBit
        {
            get { return m_snapshotLoanProgramTemplate.lLpTemplateNmOverrideBit; }
        }

        public Guid lCcTemplateIdInherit
        {
            get { return m_snapshotLoanProgramTemplate.lCcTemplateIdInherit; }
        }

        public bool lCcTemplateIdOverrideBit
        {
            get { return m_snapshotLoanProgramTemplate.lCcTemplateIdOverrideBit; }
        }

        public int lLockedDaysInherit
        {
            get { return m_snapshotLoanProgramTemplate.lLockedDaysInherit; }
        }

        public bool lLockedDaysOverrideBit
        {
            get { return m_snapshotLoanProgramTemplate.lLockedDaysOverrideBit; }
        }

        public decimal lLpeFeeMinInherit
        {
            get { return m_snapshotLoanProgramTemplate.lLpeFeeMinInherit; }
        }

        public bool lLpeFeeMinOverrideBit
        {
            get { return m_snapshotLoanProgramTemplate.lLpeFeeMinOverrideBit; }
        }

        public decimal lLpeFeeMaxInherit
        {
            get { return m_snapshotLoanProgramTemplate.lLpeFeeMaxInherit; }
        }

        public bool lLpeFeeMaxOverrideBit
        {
            get { return m_snapshotLoanProgramTemplate.lLpeFeeMaxOverrideBit; }
        }

        public bool IsEnabledInherit
        {
            get { return m_snapshotLoanProgramTemplate.IsEnabledInherit; }
        }

        public bool IsEnabledOverrideBit
        {
            get { return m_snapshotLoanProgramTemplate.IsEnabledOverrideBit; }
        }

        public int lPpmtPenaltyMon
        {
            get { return m_snapshotLoanProgramTemplate.lPpmtPenaltyMon; }
        }

        public int lPpmtPenaltyMonInherit
        {
            get { return m_snapshotLoanProgramTemplate.lPpmtPenaltyMonInherit; }
        }

        public bool lPpmtPenaltyMonOverrideBit
        {
            get { return m_snapshotLoanProgramTemplate.lPpmtPenaltyMonOverrideBit; }
        }

        public decimal lRateDelta
        {
            get { return m_snapshotLoanProgramTemplate.lRateDelta; }
        }

        public string lRateDelta_rep
        {
            get { return m_snapshotLoanProgramTemplate.lRateDelta_rep; }
        }

        public decimal lFeeDelta
        {
            get { return m_snapshotLoanProgramTemplate.lFeeDelta; }
        }

        public string lFeeDelta_rep
        {
            get { return m_snapshotLoanProgramTemplate.lFeeDelta_rep; }
        }

        public int lPpmtPenaltyMonLowerSearch
        {
            get { return m_snapshotLoanProgramTemplate.lPpmtPenaltyMonLowerSearch; }
        }

        public int lLockedDaysLowerSearch
        {
            get { return m_snapshotLoanProgramTemplate.lLockedDaysLowerSearch; }
        }

        public int lLockedDaysLowerSearchInherit
        {
            get { return m_snapshotLoanProgramTemplate.lLockedDaysLowerSearchInherit; }
        }

        public int lPpmtPenaltyMonLowerSearchInherit
        {
            get { return m_snapshotLoanProgramTemplate.lPpmtPenaltyMonLowerSearchInherit; }
        }

        public string PairingProductIdsInherit
        {
            get { return m_snapshotLoanProgramTemplate.PairingProductIdsInherit; }
        }

        public bool PairingProductIdsOverrideBit
        {
            get { return m_snapshotLoanProgramTemplate.PairingProductIdsOverrideBit; }
        }

        public string lLpInvestorNm
        {
            get { return m_snapshotLoanProgramTemplate.lLpInvestorNm; }
        }

        public string lLendNmInherit
        {
            get { return m_snapshotLoanProgramTemplate.lLendNmInherit; }
        }

        public bool lLendNmOverrideBit
        {
            get { return m_snapshotLoanProgramTemplate.lLendNmOverrideBit; }
        }

        public string lRateOptionBaseId
        {
            get { return m_snapshotLoanProgramTemplate.lRateOptionBaseId; }
        }

        public Guid PairIdFor1stLienProdGuid
        {
            get { return m_snapshotLoanProgramTemplate.PairIdFor1stLienProdGuid; }
        }

        public Guid PairIdFor1stLienProdGuidInherit
        {
            get { return m_snapshotLoanProgramTemplate.PairIdFor1stLienProdGuidInherit; }
        }

        public decimal lLpeFeeMinParam
        {
            get { return m_snapshotLoanProgramTemplate.lLpeFeeMinParam; }
        }

        public bool lLpeFeeMinAddRuleAdjInheritBit
        {
            get { return m_snapshotLoanProgramTemplate.lLpeFeeMinAddRuleAdjInheritBit; }
        }

        public bool IsLpe
        {
            get { return m_snapshotLoanProgramTemplate.IsLpe; }
        }

        public bool lIsArmMarginDisplayed
        {
            get { return m_snapshotLoanProgramTemplate.lIsArmMarginDisplayed; }
        }

        public string ProductCode
        {
            get { return m_snapshotLoanProgramTemplate.ProductCode; }
        }

        public string ProductIdentifier
        {
            get
            {
                return m_snapshotLoanProgramTemplate.ProductIdentifier;
            }
        }

        public bool IsOptionArm
        {
            get { return m_snapshotLoanProgramTemplate.IsOptionArm; }
        }

        public decimal lRateDeltaInherit
        {
            get { return m_snapshotLoanProgramTemplate.lRateDeltaInherit; }
        }

        public decimal lFeeDeltaInherit
        {
            get { return m_snapshotLoanProgramTemplate.lFeeDeltaInherit; }
        }

        public Guid SrcRateOptionsProgIdInherit
        {
            get { return m_snapshotLoanProgramTemplate.SrcRateOptionsProgIdInherit; }
        }

        public Guid SrcRateOptionsProgId
        {
            get { return m_snapshotLoanProgramTemplate.SrcRateOptionsProgId; }
        }

        public E_sLpDPmtT lLpDPmtT
        {
            get { return m_snapshotLoanProgramTemplate.lLpDPmtT; }
        }

        public E_sLpQPmtT lLpQPmtT
        {
            get { return m_snapshotLoanProgramTemplate.lLpQPmtT; }
        }

        public bool lHasQRateInRateOptions
        {
            get
            {
                return m_snapshotLoanProgramTemplate.lHasQRateInRateOptions;
            }
            set
            {
                m_snapshotLoanProgramTemplate.lHasQRateInRateOptions = value;
            }
        }

        public bool IsLpeDummyProgram
        {
            get { return m_snapshotLoanProgramTemplate.IsLpeDummyProgram; }
        }

        public bool IsPairedOnlyWithSameInvestor
        {
            get { return m_snapshotLoanProgramTemplate.IsPairedOnlyWithSameInvestor; }
        }

        public int lIOnlyMonLowerSearch
        {
            get { return m_snapshotLoanProgramTemplate.lIOnlyMonLowerSearch; }
        }

        public int lIOnlyMonLowerSearchInherit
        {
            get { return m_snapshotLoanProgramTemplate.lIOnlyMonLowerSearchInherit; }
        }

        public bool lIOnlyMonLowerSearchOverrideBit
        {
            get { return m_snapshotLoanProgramTemplate.lIOnlyMonLowerSearchOverrideBit; }
        }

        public int lIOnlyMonUpperSearch
        {
            get { return m_snapshotLoanProgramTemplate.lIOnlyMonUpperSearch; }
        }

        public int lIOnlyMonUpperSearchInherit
        {
            get { return m_snapshotLoanProgramTemplate.lIOnlyMonUpperSearchInherit; }
        }

        public bool lIOnlyMonUpperSearchOverrideBit
        {
            get { return m_snapshotLoanProgramTemplate.lIOnlyMonUpperSearchOverrideBit; }
        }

        public bool CanBeStandAlone2nd
        {
            get { return m_snapshotLoanProgramTemplate.CanBeStandAlone2nd; }
        }

        public bool lDtiUsingMaxBalPc
        {
            get { return m_snapshotLoanProgramTemplate.lDtiUsingMaxBalPc; }
        }

        public bool IsMasterPriceGroup
        {
            get { return m_snapshotLoanProgramTemplate.IsMasterPriceGroup; }
        }

        public string lLpProductType
        {
            get { return m_snapshotLoanProgramTemplate.lLpProductType; }
        }

        public E_sRAdjFloorBaseT lRAdjFloorBaseT
        {
            get { return m_snapshotLoanProgramTemplate.lRAdjFloorBaseT; }
        }

        public bool IsConvertibleMortgage
        {
            get { return m_snapshotLoanProgramTemplate.IsConvertibleMortgage; }
        }

        public bool lLpmiSupportedOutsidePmi
        {
            get { return m_snapshotLoanProgramTemplate.lLpmiSupportedOutsidePmi; }
        }

        public bool lWholesaleChannelProgram
        {
            get { return m_snapshotLoanProgramTemplate.lWholesaleChannelProgram; }
        }

        public int lHardPrepmtPeriodMonths
        {
            get { return m_snapshotLoanProgramTemplate.lHardPrepmtPeriodMonths; }
        }

        public string lHardPrepmtPeriodMonths_rep
        {
            get { return m_snapshotLoanProgramTemplate.lHardPrepmtPeriodMonths_rep; }
        }

        public int lSoftPrepmtPeriodMonths
        {
            get { return m_snapshotLoanProgramTemplate.lSoftPrepmtPeriodMonths; }
        }

        public string lSoftPrepmtPeriodMonths_rep
        {
            get { return m_snapshotLoanProgramTemplate.lSoftPrepmtPeriodMonths_rep; }
        }

        public E_sHelocPmtBaseT lHelocQualPmtBaseT
        {
            get { return m_snapshotLoanProgramTemplate.lHelocQualPmtBaseT; }
        }

        public E_sHelocPmtFormulaT lHelocQualPmtFormulaT
        {
            get { return m_snapshotLoanProgramTemplate.lHelocQualPmtFormulaT; }
        }

        public E_sHelocPmtFormulaRateT lHelocQualPmtFormulaRateT
        {
            get { return m_snapshotLoanProgramTemplate.lHelocQualPmtFormulaRateT; }
        }
        public E_sHelocPmtAmortTermT lHelocQualPmtAmortTermT
        {
            get { return m_snapshotLoanProgramTemplate.lHelocQualPmtAmortTermT; }
        }

        public E_sHelocPmtPcBaseT lHelocQualPmtPcBaseT
        {
            get { return m_snapshotLoanProgramTemplate.lHelocQualPmtPcBaseT; }
        }

        public decimal lHelocQualPmtMb
        {
            get { return m_snapshotLoanProgramTemplate.lHelocQualPmtMb; }
        }

        public string lHelocQualPmtMb_rep
        {
            get { return m_snapshotLoanProgramTemplate.lHelocQualPmtMb_rep; }
        }

        public E_sHelocPmtBaseT lHelocPmtBaseT
        {
            get { return m_snapshotLoanProgramTemplate.lHelocPmtBaseT; }
        }

        public E_sHelocPmtFormulaT lHelocPmtFormulaT
        {
            get { return m_snapshotLoanProgramTemplate.lHelocPmtFormulaT; }
        }

        public E_sHelocPmtFormulaRateT lHelocPmtFormulaRateT
        {
            get { return m_snapshotLoanProgramTemplate.lHelocPmtFormulaRateT; }
        }
        public E_sHelocPmtAmortTermT lHelocPmtAmortTermT
        {
            get { return m_snapshotLoanProgramTemplate.lHelocPmtAmortTermT; }
        }

        public E_sHelocPmtPcBaseT lHelocPmtPcBaseT
        {
            get { return m_snapshotLoanProgramTemplate.lHelocPmtPcBaseT; }
        }

        public decimal lHelocPmtMb
        {
            get { return m_snapshotLoanProgramTemplate.lHelocPmtMb; }
        }

        public string lHelocPmtMb_rep
        {
            get { return m_snapshotLoanProgramTemplate.lHelocPmtMb_rep; }
        }

        public int lHelocDraw
        {
            get { return m_snapshotLoanProgramTemplate.lHelocDraw; }
        }

        public string lHelocDraw_rep
        {
            get { return m_snapshotLoanProgramTemplate.lHelocDraw_rep; }
        }

        public bool lHelocCalculatePrepaidInterest
        {
            get { return m_snapshotLoanProgramTemplate.lHelocCalculatePrepaidInterest; }
        }

        public bool IsHeloc
        {
            get { return m_snapshotLoanProgramTemplate.IsHeloc; }
        }

        public string lPrepmtPeriod_rep
        {
            get { return m_snapshotLoanProgramTemplate.lPrepmtPeriod_rep; }
        }

        public decimal lNoteR
        {
            get { return m_snapshotLoanProgramTemplate.lNoteR; }
        }

        public decimal lLOrigFPc
        {
            get { return m_snapshotLoanProgramTemplate.lLOrigFPc; }
        }

        public string lHelocRepay_rep
        {
            get { return m_snapshotLoanProgramTemplate.lHelocRepay_rep; }
        }

        public DateTime lRateSheetDownloadEndD
        {
            get { return m_fileBasedLoanProgramTemplate.lRateSheetDownloadEndD; }
        }

        public DateTime lRateSheetDownloadStartD
        {
            get { return m_fileBasedLoanProgramTemplate.lRateSheetDownloadStartD; }
        }

        public CCcTemplateData CcTemplate
        {
            get { return m_snapshotLoanProgramTemplate.CcTemplate; }
        }

        public bool HasCcTemplate
        {
            get { return m_snapshotLoanProgramTemplate.HasCcTemplate; }
        }

        public string FullName
        {
            get { return m_snapshotLoanProgramTemplate.FullName; }
        }

        public IRateItem[] GetRawRateSheetWithDeltas()
        {
            return m_fileBasedLoanProgramTemplate.GetRawRateSheetWithDeltas();
        }

        public IRateItem GetRateOptionKey(int sProdRLckdDays, int sProdPpmtPenaltyMon, int sIOnlyMon)
        {
            return m_fileBasedLoanProgramTemplate.GetRateOptionKey(sProdRLckdDays, sProdPpmtPenaltyMon, sIOnlyMon);
        }

        public bool lLpeFeeMinAddRuleAdjBit
        {
            get { return m_snapshotLoanProgramTemplate.lLpeFeeMinAddRuleAdjBit; }
        }

        public long LpeAcceptableRsFileVersionNumber
        {
            get { return m_fileBasedLoanProgramTemplate.LpeAcceptableRsFileVersionNumber; }
        }
        public string LpeAcceptableRsFileId
        {
            get { return m_fileBasedLoanProgramTemplate.LpeAcceptableRsFileId; }
        }

        public string lLpCustomCode1
        {
            get { return m_snapshotLoanProgramTemplate.lLpCustomCode1; }
        }
        public string lLpCustomCode2
        {
            get { return m_snapshotLoanProgramTemplate.lLpCustomCode2; }
        }

        public string lLpCustomCode3
        {
            get { return m_snapshotLoanProgramTemplate.lLpCustomCode3; }
        }

        public string lLpCustomCode4
        {
            get { return m_snapshotLoanProgramTemplate.lLpCustomCode4; }
        }

        public string lLpCustomCode5
        {
            get { return m_snapshotLoanProgramTemplate.lLpCustomCode5; }
        }

        public string lLpInvestorCode1
        {
            get { return m_snapshotLoanProgramTemplate.lLpInvestorCode1; }
        }
        public string lLpInvestorCode2
        {
            get { return m_snapshotLoanProgramTemplate.lLpInvestorCode2; }
        }

        public string lLpInvestorCode3
        {
            get { return m_snapshotLoanProgramTemplate.lLpInvestorCode3; }
        }

        public string lLpInvestorCode4
        {
            get { return m_snapshotLoanProgramTemplate.lLpInvestorCode4; }
        }

        public string lLpInvestorCode5
        {
            get { return m_snapshotLoanProgramTemplate.lLpInvestorCode5; }
        }

        public QualRateCalculationT lQualRateCalculationT
        {
            get { return this.m_snapshotLoanProgramTemplate.lQualRateCalculationT; }
        }

        public QualRateCalculationFieldT lQualRateCalculationFieldT1
        {
            get { return this.m_snapshotLoanProgramTemplate.lQualRateCalculationFieldT1; }
        }

        public QualRateCalculationFieldT lQualRateCalculationFieldT2
        {
            get { return this.m_snapshotLoanProgramTemplate.lQualRateCalculationFieldT2; }
        }

        public decimal lQualRateCalculationAdjustment1
        {
            get { return this.m_snapshotLoanProgramTemplate.lQualRateCalculationAdjustment1; }
        }

        public decimal lQualRateCalculationAdjustment2
        {
            get { return this.m_snapshotLoanProgramTemplate.lQualRateCalculationAdjustment2; }
        }

        public int lQualTerm
        {
            get { return this.m_snapshotLoanProgramTemplate.lQualTerm; }
        }

        public QualTermCalculationType lQualTermCalculationType
        {
            get { return this.m_snapshotLoanProgramTemplate.lQualTermCalculationType; }
        }


        #endregion
    }
}
