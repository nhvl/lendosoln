﻿namespace LendersOffice.RatePrice
{
    using LendersOffice.Constants;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Default implementation of price engine storage factory.
    /// </summary>
    public class PriceEngineStorageFactory : IPriceEngineStorageFactory
    {
        /// <summary>
        /// Create a default implementation of price engine storage.
        /// </summary>
        /// <param name="mode">A type of price engine storage to create.</param>
        /// <returns>Implementation of price engine storage.</returns>
        public IPriceEngineStorage Create(PriceEngineStorageMode mode)
        {
            switch (mode)
            {
                case PriceEngineStorageMode.Normal:
                    return new PriceEngineStorage();
                case PriceEngineStorageMode.NormalWithCache:
                    LocalFilePath tempFolderRootForNormal = LocalFilePath.Create(ConstStage.LpeDataCachePath).Value;

                    return new CachePriceEngineStorage(tempFolderRootForNormal, new PriceEngineStorage());
                case PriceEngineStorageMode.LocalFileOnly:
                    LocalFilePath? root = LocalFilePath.Create(ConstSite.TempPriceEngineStorageLocalPath);

                    if (root == null)
                    {
                        throw new DeveloperException(ErrorMessage.BadConfiguration);
                    }

                    return new LocalFilePriceEngineStorage(root.Value);
                case PriceEngineStorageMode.Aws:
                    LocalFilePath tempFolderRoot = LocalFilePath.Create(ConstStage.LpeDataCachePath).Value;

                    return new CachePriceEngineStorage(tempFolderRoot, new AwsPriceEngineStorage());
                default:
                    throw new DataAccess.UnhandledEnumException(mode);
            }
        }
    }
}