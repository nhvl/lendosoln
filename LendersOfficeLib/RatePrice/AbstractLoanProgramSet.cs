﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LqbGrammar.DataTypes;
using LendersOffice.RatePrice.CustomRatesheet;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice
{
    public abstract class AbstractLoanProgramSet
    {
        public abstract bool IsCurrentSnapshot { get; }
        public abstract DateTime VersionD { get; }

        public abstract SHA256Checksum FileKey { get; }

        public abstract AbstractRtPricePolicy GetPricePolicy(Guid policyId);

        public virtual AbstractRtPricePolicy GetPmiPricePolicy(Guid policyId)
        {
            return GetPricePolicy(policyId);
        }

        public abstract IEnumerable<Guid> GetApplicablePolicies(Guid lLpTemplateId);

        public abstract IPolicyRelationDb PolicyRelation { get; }

        public abstract PmiManager PmiManager { get; }

        public abstract int Count { get; }

        public abstract ILoanProgramTemplate RetrieveByIndex(int index);
        public abstract ILoanProgramTemplate RetrieveByKey(Guid lLpTemplateId);

        public abstract string FriendlyInfo { get; }

        public void Debug()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Class::" + this.GetType().FullName);
            sb.AppendLine("VersionD::" + this.VersionD);
            sb.AppendLine("FriendlyInfo::" + this.FriendlyInfo);
            sb.AppendLine("Program Count::" + this.Count);

            for (int i = 0; i < this.Count; i++)
            {
                ILoanProgramTemplate loanProgramTemplate = RetrieveByIndex(i);

                sb.AppendLine();
                sb.AppendLine("    lLpTemplateId:" + loanProgramTemplate.lLpTemplateId);
                sb.AppendLine("    lLpTemplateNm:" + loanProgramTemplate.lLpTemplateNm);

                IEnumerable<Guid> applicablePolicyList = GetApplicablePolicies(loanProgramTemplate.lLpTemplateId);

                sb.AppendLine("    ApplicablePolicyCount=" + applicablePolicyList.Count());

                foreach (Guid policyId in applicablePolicyList)
                {
                    sb.AppendLine("        " + policyId);
                }
                sb.AppendLine();
            }

            Tools.LogInfo(sb.ToString());
        }

        public virtual IEnumerable<RtRateMarginItem> GetTpoRateMargin(Guid brokerId, int pmlBrokerIdx, Guid productId)
        {
            return null;
        }
    }
}
