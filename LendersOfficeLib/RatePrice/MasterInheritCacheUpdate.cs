using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOfficeApp.los.RatePrice;


namespace Toolbox.LoanProgram
{

    internal class MasterInheritCacheUpdate
	{
		#region INTERNAL METHODS

        /// <summary>
		/// TODO: This guy doesn't belong to this file and scope
		/// </summary>
		/// <param name="brokerId"></param>
		static internal void SpreadFromAllMastersInThisBroker( Guid brokerId )
		{
			ArrayList masterIds = new ArrayList( 100 );
			SqlParameter[] pars = new SqlParameter[] { new SqlParameter( "@brokerid", brokerId ) };
			// This will get us the ids of all products where it's a first base or standalone products, no master
			using( DbDataReader reader = StoredProcedureHelper.ExecuteReader( DataSrc.LpeSrc, "ListMasterProdIdsOfBroker", pars ) )
			{
				while( reader.Read() )
				{
					masterIds.Add( new Guid( reader["lLpTemplateId"].ToString() ) );
				}
			}

			foreach( Guid masterId in masterIds )
			{
                CAssocInheritUpdate updator = CAssocInheritUpdate.GetUpdatorForExistingMasterNoUiAction( masterId, brokerId );
                updator.UpdateAndSpread( true );
			}
		}

		/// <summary>
		/// TODO: This guy doesn't belong to this file and scope
		/// </summary>
		/// <param name="brokerId"></param>
		static internal void SpreadFromAllMastersInSystem()
		{
            List<Guid> brokerIds = (List<Guid>)Tools.GetAllActiveBrokers();

			foreach( Guid brokerId in brokerIds )
			{
				SpreadFromAllMastersInThisBroker( brokerId );
			}
		}

		#endregion // internal METHODS
	}
}