﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Xml;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice
{
    public abstract class AbstractRtPricePolicy
    {
        protected RtRuleList m_ruleList = new RtRuleList(20);

        public RtRuleList Rules
        {
            get { return m_ruleList; }
        }

        public abstract Guid PricePolicyId { get; set; }
        public abstract string PricePolicyDescription { get; set; }
        public abstract Guid ParentPolicyId { get; set; }

        public abstract string MutualExclusiveExecSortedId { get; set; }

        public abstract int EstimateSize();
        public abstract void LoadRules();

        public void AccumulateCategoryCounter(ref RuleCategoryCounter counter)
        {
            RtRuleList ruleList = m_ruleList;
            if (ruleList == null)
                counter.PolicyNotCallLoadRules_Counter++;
            else
            {
                counter.LoadedRuleCounter += ruleList.Count;
                counter.InvariantRuleCounter += InvariantCount;
                counter.QValueRuleCounter += this.LastIdxOfQValue - this.FirstIdxOfQValue;
                counter.VariantRuleCounter += this.LastIdxOfVariant - this.FirstIdxOfVariant;
                counter.DtiRuleCounter += this.LastIdxOfDti - this.FirstIdxOfDti;

            }


        }

        public int InvariantCount { get { return LastIdxOfInvariant; } }

        public int FirstIdxOfInvariant { get { return 0; } }
        public int LastIdxOfInvariant { get { return FirstIdxOfQValue; } }

        public int FirstIdxOfQValue { get; protected set; }
        public int LastIdxOfQValue { get { return FirstIdxOfVariant; } }

        public int FirstIdxOfVariant { get; protected set; }
        public int LastIdxOfVariant { get { return FirstIdxOfDti; } }

        public int FirstIdxOfDti { get; protected set; }

        public int LastIdxOfDti
        {
            get { return m_ruleList == null ? 0 : m_ruleList.Count; }
        }

    }
    public class RtPricePolicy : AbstractRtPricePolicy
    {
        private bool m_loadedRules = false;
        private int m_xmlSize = 0;

        // save memory by converting string to byte[] in UTF8
        private byte[] m_pricePolicyXmlUTF8;
        private string PricePolicyXml
        {
            get
            {
                if (m_pricePolicyXmlUTF8 == null)
                    return null;

                return System.Text.Encoding.UTF8.GetString(m_pricePolicyXmlUTF8);
            }

            set
            {
                if (value == null)
                    m_pricePolicyXmlUTF8 = null;
                else
                {
                    m_pricePolicyXmlUTF8 = System.Text.Encoding.UTF8.GetBytes(value);
                    m_xmlSize = value.Length;
                }
            }
        }

        public override Guid PricePolicyId
        {
            get;
            set;
        }

        public override Guid ParentPolicyId
        {
            get; set;
        }

        public override string MutualExclusiveExecSortedId
        {
            get; set;
        }

        public override string PricePolicyDescription
        {
            get; set;
        }
        /// <summary>
        /// This constructor will return an object that 
        /// is a result of a merge of all price policies
        /// applicable to the loan product.  The multiple
        /// policies features will be done in the future
        /// by allowing groups and policies applied to group.
        /// </summary>
        /// <param name="LoanProduct"></param>
        /// 
        public RtPricePolicy(Guid pricePolicyId, DataRow rowPolicy)
        {
            this.PricePolicyId = pricePolicyId;

            string pricePolicyXml = (string)rowPolicy["PricePolicyXmlText"];
            this.PricePolicyDescription = (string)rowPolicy["PricePolicyDescription"];
            this.MutualExclusiveExecSortedId = (string)rowPolicy["MutualExclusiveExecSortedId"];
            if (!(rowPolicy["ParentPolicyId"] is DBNull))
            {
                this.ParentPolicyId = (Guid)rowPolicy["ParentPolicyId"];
            }

            if (0 == pricePolicyXml.TrimWhitespaceAndBOM().Length)
                pricePolicyXml = @"<PricePolicy></PricePolicy>";

            PricePolicyXml = pricePolicyXml;
        }



        public override void LoadRules()
        {


            int i = 0;
            int count = 0;
            lock (this)
            {
                if (m_loadedRules)
                    return;

                RtRuleList tmpRuleList = null;
                try
                {
                    List<AbstractRtRule> dtiRules = new List<AbstractRtRule>();
                    List<AbstractRtRule> variantRules = new List<AbstractRtRule>();
                    List<AbstractRtRule> qvalueRules = new List<AbstractRtRule>();


                    XmlDocument m_xmlDoc = new XmlDocument();
                    m_xmlDoc.LoadXml(PricePolicyXml);

                    XmlNodeList ruleNodes = m_xmlDoc.SelectNodes(".//Rule");

                    count = ruleNodes.Count;
                    tmpRuleList = new RtRuleList(count);

                    for (i = 0; i < count; ++i)
                    {
                        RtRule rtRule = new RtRule((XmlElement)ruleNodes[i] /*, loanData */);
                        switch (rtRule.Category)
                        {
                            case LpeKeywordCategory.Invariant: tmpRuleList.Add(rtRule); break;
                            case LpeKeywordCategory.Variant: variantRules.Add(rtRule); break;
                            case LpeKeywordCategory.Dti: dtiRules.Add(rtRule); break;
                            case LpeKeywordCategory.QValue: qvalueRules.Add(rtRule); break;
                        }
                    }


                    FirstIdxOfQValue = tmpRuleList.Count;
                    tmpRuleList.AddRange(qvalueRules);

                    FirstIdxOfVariant = tmpRuleList.Count;
                    tmpRuleList.AddRange(variantRules);

                    FirstIdxOfDti = tmpRuleList.Count;
                    tmpRuleList.AddRange(dtiRules);

                    Tools.Assert(tmpRuleList.Count == count, "RtPricePolicy.LoadRules : expect tmpRuleList.Count == count");

                    //ThienChange
                    PricePolicyXml = null;
                    m_ruleList = tmpRuleList;

                    m_loadedRules = true;
                }
                catch (Exception ex)
                {
                    int ruleListCount = (tmpRuleList == null) ? 0 : tmpRuleList.Count;
                    Tools.LogError(string.Format("ruleNodes.Count = {0}, i = {1}, m_ruleList.Count={2}",
                                                   count, i, ruleListCount), ex);
                    throw;
                }
            }
        }

        public override int EstimateSize()
        {
            const int ObjectFixSize = 74 + 12;

            int size = ObjectFixSize;
            if (PricePolicyDescription != null)
                size += 2 * PricePolicyDescription.Length;

            if (MutualExclusiveExecSortedId != null)
                size += 2 * MutualExclusiveExecSortedId.Length;

            if (m_ruleList != null)
                size += m_ruleList.EstimateSize();
            else
                size += 20 + m_xmlSize;

            return size;
        }
    }
}