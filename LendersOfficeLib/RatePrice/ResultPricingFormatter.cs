﻿namespace LendersOffice.RatePrice
{
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using DataAccess.LoanComparison;
    using LendersOffice.Admin;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.RatePrice.Model;
    using LendersOfficeApp.los.RatePrice;
    using Security;

    /// <summary>
    /// The purpose of this class is to translate a raw pricing result
    /// into a format that can be serialized for the UI to parse. 
    /// </summary>
    public class ResultPricingFormatter
    {
        /// <summary>
        /// Mapping of rate options to qm result.
        /// </summary>
        private Dictionary<CApplicantRateOption, string> rateQmMap = new Dictionary<CApplicantRateOption, string>();

        /// <summary>
        /// Mapping of rate options to Hpm result.
        /// </summary>
        private Dictionary<CApplicantRateOption, string> rateHpmMap = new Dictionary<CApplicantRateOption, string>();

        /// <summary>
        /// Prevents a default instance of the <see cref="ResultPricingFormatter"/> class from being created.
        /// </summary>
        private ResultPricingFormatter()
        {
        }

        /// <summary>
        /// Gets or sets the Broker value.
        /// </summary>
        /// <value>The Broker value.</value>
        private BrokerDB Broker { get; set; }

        /// <summary>
        /// Gets or sets the Principal value.
        /// </summary>
        /// <value>The Principal value.</value>
        private AbstractUserPrincipal Principal { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether originator comp is lender paid and in addition to fees.
        /// </summary>
        /// <value>The originator comp lender paid and in addition to fees value.</value>
        private bool IsLenderPaidOriginatorCompAndAdditionToFees { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether originator comp is borrower paid and included in fees.
        /// </summary>
        /// <value>The originator comp borrower paid and included in fees value.</value>
        private bool IsBorrowerPaidOriginatorCompAndIncludedInFees { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the loan's total income is zero.
        /// </summary>
        /// <value>Whether the total income is zero.</value>
        private bool IsTotalIncomeZero { get; set; }

        /// <summary>
        /// Gets or sets a value indicating the pricing mode.
        /// </summary>
        /// <value>Pricing mode.</value>
        private E_sPricingModeT PricingModeT { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is rate merge.
        /// </summary>
        /// <value>The is rate merge value.</value>
        private bool IsRateMerge { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether QM should be processed.
        /// </summary>
        /// <value>The QM should be processed value.</value>
        private bool ProcessQM { get; set; } = true;

        /// <summary>
        /// Gets or sets point setting to use with QM.
        /// </summary>
        /// <value>Qm point setting.</value>
        private E_OriginatorCompPointSetting QmPointSetting { get; set; }

        /// <summary>
        /// Gets or sets the pricing states.
        /// </summary>
        /// <value>The pricing states.</value>
        private List<PricingState> PricingStates { get; set; }

        /// <summary>
        /// Creates an instance of ResultPricingFormatter.
        /// </summary>
        /// <param name="broker">BrokerDB object.</param>
        /// <param name="principal">Principal of running user.</param>
        /// <param name="pricingModeT">Pricing Mode.</param>
        /// <param name="originatorCompensationPaymentSourceT">Comp payment source.</param>
        /// <param name="originatorCompensationLenderFeeOptionT">Comp fee mode.</param>
        /// <param name="pricingStates">The loan's pricing states.</param>
        /// <param name="isTotalIncomeZero">Whether the loan's total income is zero.</param>
        /// <returns>A new formatter object.</returns>
        public static ResultPricingFormatter GetFormatter(
            BrokerDB broker, 
            AbstractUserPrincipal principal,
            E_sPricingModeT pricingModeT,
            E_sOriginatorCompensationPaymentSourceT originatorCompensationPaymentSourceT,
            E_sOriginatorCompensationLenderFeeOptionT originatorCompensationLenderFeeOptionT,
            List<PricingState> pricingStates = null,
            bool isTotalIncomeZero = false)
        {
            var ret = new ResultPricingFormatter();
            ret.Broker = broker;
            ret.Principal = principal;
            ret.PricingModeT = pricingModeT;
            ret.IsLenderPaidOriginatorCompAndAdditionToFees = originatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                                                                && originatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;
            ret.IsBorrowerPaidOriginatorCompAndIncludedInFees = originatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid
                                                                && originatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.IncludedInLenderFees;

            ret.PricingStates = pricingStates;
            ret.IsTotalIncomeZero = isTotalIncomeZero;
            return ret;
        }

        /// <summary>
        /// Get a single (non-merged) pricing result from the result item.
        /// </summary>
        /// <param name="resultItem">The pricing result item.</param>
        /// <returns>Formatted result item.</returns>
        public ResultPricing GetSingleResult(UnderwritingResultItem resultItem)
        {
            var ret = new ResultPricing();

            this.ProcessQmData(resultItem);

            ret.EligibleGroups = this.ParseLoanPrograms(resultItem.GetEligibleLoanProgramList());
            ret.InsufficientGroups = this.ParseLoanPrograms(resultItem.GetInsufficientLoanProgramList());
            ret.IneligibleGroups = this.ParseLoanPrograms(resultItem.GetIneligibleLoanProgramList());
            return ret;
        }

        /// <summary>
        /// Get a single (merged) pricing result from the result item.
        /// </summary>
        /// <param name="resultItem">The pricing result item.</param>
        /// <returns>Formatted result item.</returns>
        public ResultPricing GetMergedSingleResult(UnderwritingResultItem resultItem)
        {
            this.IsRateMerge = true;
            var ret = new ResultPricing();

            this.ProcessQmData(resultItem);

            ret.EligibleGroups = this.ParseMergedPrograms(resultItem.GetRateMergeResult(this.Broker));
            ret.InsufficientGroups = this.ParseLoanPrograms(resultItem.GetInsufficientLoanProgramList());
            ret.IneligibleGroups = this.ParseLoanPrograms(resultItem.GetIneligibleLoanProgramList());
            return ret;
        }

        /// <summary>
        /// Get a combined pricing result from the result items.
        /// </summary>
        /// <param name="currentResultItem">The current pricing result item.</param>
        /// <param name="historicalResultItem">The historical pricing result item.</param>
        /// <returns>Formatted result item.</returns>
        public ResultPricing GetCombinedResult(UnderwritingResultItem currentResultItem, UnderwritingResultItem historicalResultItem)
        {
            var ret = new ResultPricing();
            this.ProcessQmData(currentResultItem);
            this.ProcessQmData(historicalResultItem);

            var eligibleGroups = this.ParseLoanPrograms(currentResultItem.GetEligibleLoanProgramList());
            var historicalEligibleGroups = this.ParseLoanPrograms(historicalResultItem.GetEligibleLoanProgramList());
            this.CombineCurrentAndHistoricalPricing(eligibleGroups, historicalEligibleGroups);
            ret.EligibleGroups = eligibleGroups;

            var ineligibleGroups = this.ParseLoanPrograms(currentResultItem.GetIneligibleLoanProgramList());
            var historicalIneligibleGroups = this.ParseLoanPrograms(historicalResultItem.GetIneligibleLoanProgramList());
            this.CombineCurrentAndHistoricalIneligiblePricing(ineligibleGroups, historicalIneligibleGroups);
            ret.IneligibleGroups = ineligibleGroups;

            return ret;
        }

        /// <summary>
        /// This method's goal is to pull a historical result into
        /// a current result.
        /// </summary>
        /// <param name="currentGroups">Current result groups.</param>
        /// <param name="historicalGroups">Historical result groups.</param>
        private void CombineCurrentAndHistoricalPricing(List<ResultGroup> currentGroups, List<ResultGroup> historicalGroups)
        {
            foreach (var currentGroup in currentGroups)
            {
                var historicalGroup = historicalGroups.FirstOrDefault(group => group.GroupName == currentGroup.GroupName);
                if (historicalGroup != null)
                {
                    foreach (var currentProgram in currentGroup.ResultLoanPrograms)
                    {
                        var historicalProgram = historicalGroup.ResultLoanPrograms.FirstOrDefault(p => p.lLpTemplateId == currentProgram.lLpTemplateId);
                        if (historicalProgram != null)
                        {
                            currentProgram.ResultRateOptions = this.CombineCurrentAndHistoricalRateOptions(currentProgram.ResultRateOptions, historicalProgram.ResultRateOptions);
                            currentProgram.RepresentativeHistoricRateOption = historicalProgram.RepresentativeRateOption;

                            bool hasCurrent, hasHistorical;
                            decimal currentRate = 0, historicalRate = 0;

                            hasCurrent = currentProgram.RepresentativeRateOption != null && decimal.TryParse(currentProgram.RepresentativeRateOption.Rate, out currentRate);
                            hasHistorical = currentProgram.RepresentativeHistoricRateOption != null && decimal.TryParse(currentProgram.RepresentativeHistoricRateOption.Rate, out historicalRate);

                            if (hasCurrent && hasHistorical)
                            {
                                if (currentRate > historicalRate)
                                {
                                    currentProgram.RepresentativeHistoricRateOption = this.FindPairedRate(currentProgram.RepresentativeRateOption, currentProgram.ResultRateOptions, false);
                                }
                                else
                                {
                                    currentProgram.RepresentativeRateOption = this.FindPairedRate(currentProgram.RepresentativeHistoricRateOption, currentProgram.ResultRateOptions, true);
                                }
                            }
                            else if (hasCurrent)
                            {
                                currentProgram.RepresentativeHistoricRateOption = this.FindPairedRate(currentProgram.RepresentativeRateOption, currentProgram.ResultRateOptions, false);
                            }
                            else if (hasHistorical)
                            {
                                currentProgram.RepresentativeRateOption = this.FindPairedRate(currentProgram.RepresentativeHistoricRateOption, currentProgram.ResultRateOptions, true);
                            }

                            this.MarkRateOptionChanges(currentProgram.RepresentativeRateOption, currentProgram.RepresentativeHistoricRateOption);
                        }
                        else
                        {
                            // This program was not in the past result.  She is left alone.
                            foreach (var rateOption in currentProgram.ResultRateOptions)
                            {
                                this.MarkSingleRateOption(rateOption, isCurrent: true);
                            }
                        }
                    }
                }
                else
                {
                    // This entire group did not exist historically.  Mark all as single and move on.
                    foreach (var program in currentGroup.ResultLoanPrograms)
                    {
                        foreach (var rateOption in program.ResultRateOptions)
                        {
                            this.MarkSingleRateOption(rateOption, isCurrent: true);
                        }
                    }
                }
            }

            // Above we merged every matching historical program's rates into 
            // Historicals that do not correspond to current also need to be in the merged result.
            foreach (var historicalGroup in historicalGroups)
            {
                var currentGroup = currentGroups.FirstOrDefault(group => group.GroupName == historicalGroup.GroupName);
                if (currentGroup != null)
                {
                    // This group corresponds to one that exists in current.  It is possible some of these were missed.
                    // Go though each program and check if it already has been added 
                    foreach (var historicalProgram in historicalGroup.ResultLoanPrograms)
                    {
                        if (currentGroup.ResultLoanPrograms.Any(p => p.lLpTemplateId == historicalProgram.lLpTemplateId))
                        {
                            // We already have this one.
                            continue;
                        }

                        // This program exists historically, but there was no current match (it probably changed in eligibility).
                        // Add this historical to the result set.
                        historicalProgram.IsHistoric = true;
                        if (historicalProgram.RepresentativeRateOption != null)
                        {
                            this.MarkSingleRateOption(historicalProgram.RepresentativeRateOption, isCurrent: false);
                        }

                        foreach (var rateOption in historicalProgram.ResultRateOptions)
                        {
                            this.MarkSingleRateOption(rateOption, isCurrent: false);
                        }

                        currentGroup.ResultLoanPrograms.Add(historicalProgram);
                    }
                }
                else
                {
                    // This group exists in historical, but not in current.  All programs here should have
                    // no corresponding current ones.
                    foreach (var historicalProgram in historicalGroup.ResultLoanPrograms)
                    {
                        historicalProgram.IsHistoric = true;
                        if (historicalProgram.RepresentativeRateOption != null)
                        {
                            this.MarkSingleRateOption(historicalProgram.RepresentativeRateOption, isCurrent: false);
                        }

                        foreach (var rateOption in historicalProgram.ResultRateOptions)
                        {
                            this.MarkSingleRateOption(rateOption, isCurrent: false);
                        }
                    }
                    
                    currentGroups.Add(historicalGroup);
                }
            }
        }

        /// <summary>
        /// Combines historical ineligible pricing, which does not fully merge like eligible.
        /// </summary>
        /// <param name="currentGroups">Current ineligible groups.</param>
        /// <param name="historicalGroups">Current historical groups.</param>
        private void CombineCurrentAndHistoricalIneligiblePricing(List<ResultGroup> currentGroups, List<ResultGroup> historicalGroups)
        {
            // Because ineligibles can have different disqual reasons,
            // we do not merge historical and current here.
            // We need to just put them in the list ajacent to eachother.
            foreach (var currentGroup in currentGroups)
            {
                foreach (var currentProgram in currentGroup.ResultLoanPrograms)
                {
                    currentProgram.IsHistoric = false;
                    foreach (var rateOption in currentProgram.ResultRateOptions)
                    {
                        this.MarkSingleRateOption(rateOption, isCurrent: true);
                    }
                }
            }

            foreach (var historicalGroup in historicalGroups)
            {
                foreach (var historicalProgram in historicalGroup.ResultLoanPrograms)
                {
                    historicalProgram.IsHistoric = true;
                    foreach (var rateOption in historicalProgram.ResultRateOptions)
                    {
                        this.MarkSingleRateOption(rateOption, isCurrent: false);
                    }

                    if (historicalProgram.RepresentativeRateOption != null)
                    {
                        this.MarkSingleRateOption(historicalProgram.RepresentativeRateOption, isCurrent: false);
                    }

                    var matchingCurrentGroup = currentGroups.FirstOrDefault(p => p.GroupName == historicalProgram.MergeGroupName);
                    if (matchingCurrentGroup != null)
                    {
                        matchingCurrentGroup.ResultLoanPrograms.Add(historicalProgram);
                    }
                    else
                    {
                        var newGroup = new ResultGroup() { GroupName = historicalProgram.MergeGroupName };
                        var loanProgramList = new List<ResultLoanProgram>();
                        loanProgramList.Add(historicalProgram);
                        newGroup.ResultLoanPrograms = loanProgramList;
                        currentGroups.Add(newGroup);
                    }
                }
            }

            foreach (var group in currentGroups)
            {
                group.ResultLoanPrograms = group.ResultLoanPrograms.OrderBy(p => p.MergeGroupName).ThenBy(p => p.lLpTemplateNm).ThenBy(p => p.IsHistoric).ToList();
            }
        }

        /// <summary>
        /// Returns the corresponding rate option that the given rate option has been grouped with in worst-case pricing.
        /// </summary>
        /// <param name="rateOption">The rate option whose pair we are finding.</param>
        /// <param name="mergedRateOptions">The list of Rate Options to search through.</param>
        /// <param name="isHistoric">True if we are finding the pair of a historical rate option, false if current.</param>
        /// <returns>The paired rate Option, or null if it doesn't exist.</returns>
        private ResultRateOption FindPairedRate(ResultRateOption rateOption, List<ResultRateOption> mergedRateOptions, bool isHistoric)
        {
            var currentIndex = mergedRateOptions.FindIndex(rate => rate.IsHistorical == isHistoric && string.Equals(rate.Price, rateOption.Price) && string.Equals(rate.RawRate, rateOption.RawRate));

            if (currentIndex <= (isHistoric ? -1 : 0))
            {
                return null;
            }
            else
            { 
                var pairedIndex = currentIndex + (isHistoric ? 1 : -1);
                if (pairedIndex < mergedRateOptions.Count)
                {
                    var pairedRate = mergedRateOptions.ElementAt(pairedIndex);

                    if (pairedRate.Rate == rateOption.Rate && pairedRate.IsHistorical != isHistoric)
                    {
                        return mergedRateOptions.ElementAt(pairedIndex);
                    }
                }

                return null;
            }
        }

        /// <summary>
        /// Pulls in the historical rate options into a current rate option
        /// also calculating which is worse.
        /// </summary>
        /// <param name="currentRateOptions">Current Rate options.</param>
        /// <param name="historicalRateOptions">Historical rate options.</param>
        /// <returns>The combined list.</returns>
        private List<ResultRateOption> CombineCurrentAndHistoricalRateOptions(List<ResultRateOption> currentRateOptions, List<ResultRateOption> historicalRateOptions)
        {
            var ret = new List<ResultRateOption>();

            foreach (var currentRateOption in currentRateOptions)
            {
                var historicalRateOption = historicalRateOptions.Where(p => p.RawRate == currentRateOption.RawRate).FirstOrDefault();
                if (historicalRateOption != null)
                {
                    // Is in both.  Mark and add both
                    historicalRateOptions.Remove(historicalRateOption);
                    this.MarkRateOptionChanges(currentRateOption, historicalRateOption);
                    ret.Add(historicalRateOption);
                    ret.Add(currentRateOption);
                }
                else
                {
                    // Rate is in current, but does not exist in historical.  Add it alone
                    this.MarkSingleRateOption(currentRateOption, isCurrent: true);
                    ret.Add(currentRateOption);
                }
            }

            foreach (var historicalRate in historicalRateOptions)
            {
                historicalRate.IsHistorical = true;
                historicalRate.IsCurrent = false;
            }

            ret = ret.Concat(historicalRateOptions).ToList();
            return ret;
        }

        /// <summary>
        /// Calculate and mark the changes between options.
        /// </summary>
        /// <param name="currentRateOption">Current Rate Option.</param>
        /// <param name="historicalRateOption">Historical Rate Option.</param>
        private void MarkRateOptionChanges(ResultRateOption currentRateOption, ResultRateOption historicalRateOption)
        {
            if (currentRateOption == null)
            {
                if (historicalRateOption != null)
                {
                    historicalRateOption.IsCurrent = false;
                    historicalRateOption.IsHistorical = true;
                    historicalRateOption.IsEqual = false;
                    historicalRateOption.IsWorse = false;
                }

                return;
            }

            if (historicalRateOption == null)
            {
                currentRateOption.IsCurrent = true;
                currentRateOption.IsHistorical = false;
                currentRateOption.IsEqual = false;
                currentRateOption.IsWorse = false;
                return;
            }

            var currentPointValue = decimal.Parse(currentRateOption.Point);
            var historicalPointValue = decimal.Parse(historicalRateOption.Point);

            historicalRateOption.IsCurrent = false;
            historicalRateOption.IsHistorical = true;
            historicalRateOption.IsEqual = currentPointValue == historicalPointValue;
            historicalRateOption.IsWorse = currentPointValue < historicalPointValue;
            historicalRateOption.IsPaired = true;

            currentRateOption.IsCurrent = true;
            currentRateOption.IsHistorical = false;
            currentRateOption.IsEqual = currentPointValue == historicalPointValue;
            currentRateOption.IsWorse = currentPointValue > historicalPointValue;
            currentRateOption.IsPaired = true;
        }

        /// <summary>
        /// Mark a single rate option as alone.
        /// </summary>
        /// <param name="rateOption">The rate option.</param>
        /// <param name="isCurrent">If the rate is current.</param>
        private void MarkSingleRateOption(ResultRateOption rateOption, bool isCurrent)
        {
            rateOption.IsCurrent = isCurrent;
            rateOption.IsHistorical = !isCurrent;
            rateOption.IsEqual = false;
            rateOption.IsWorse = false;
        }

        /// <summary>
        /// Parse loan program nodes from result list of programs.
        /// </summary>
        /// <param name="programs">List of programs.</param>
        /// <returns>Programs grouped for result.</returns>
        private List<ResultGroup> ParseLoanPrograms(IEnumerable<CApplicantPriceXml> programs)
        {
            var ret = new List<ResultGroup>();

            foreach (var program in programs)
            {
                var groupName = this.ParseMergeGroupName(program);
                var matchingGroup = ret.FirstOrDefault(p => p.GroupName == groupName);

                if (matchingGroup != null)
                {
                    matchingGroup.ResultLoanPrograms.Add(this.ParseLoanProgram(program));
                }
                else
                {
                    var newGroup = new ResultGroup() { GroupName = groupName };
                    var loanProgramList = new List<ResultLoanProgram>();
                    loanProgramList.Add(this.ParseLoanProgram(program));
                    newGroup.ResultLoanPrograms = loanProgramList;
                    ret.Add(newGroup);
                }
            }

            return ret;
        }

        /// <summary>
        /// Parse merged programs.
        /// </summary>
        /// <param name="groups">Merged program group.</param>
        /// <returns>Result groups.</returns>
        private List<ResultGroup> ParseMergedPrograms(IEnumerable<RateMergeGroup> groups)
        {
            var ret = new List<ResultGroup>();

            E_OriginatorCompPointSetting pointSetting;
            if (this.Broker.IsUsePriceIncludingCompensationInPricingResult && this.IsLenderPaidOriginatorCompAndAdditionToFees)
            {
                // OPM 64253
                pointSetting = E_OriginatorCompPointSetting.UsePointWithOriginatorComp;
            }
            else
            {
                pointSetting = E_OriginatorCompPointSetting.UsePoint;
            }

            foreach (var group in groups)
            {
                var newGroup = new ResultGroup() { GroupName = group.Name };
                newGroup.ParRate = group.GetQMParRate(pointSetting).ToString();
                newGroup.ResultRateOptions = this.ParseRateOptions(group);
                ret.Add(newGroup);
            }

            return ret;
        }

        /// <summary>
        /// Calculate the merge group name from the program data.
        /// </summary>
        /// <param name="program">The program.</param>
        /// <returns>The merge group name.</returns>
        private string ParseMergeGroupName(CApplicantPriceXml program)
        {
            RateMergeGroupKeyParameters groupKeyParameters = new RateMergeGroupKeyParameters();
            groupKeyParameters.lArmIndexNameVstr = program.lArmIndexNameVstr;
            groupKeyParameters.lTerm_rep = program.lTerm_rep;
            groupKeyParameters.lDue_rep = program.lDue_rep;
            groupKeyParameters.lFinMethT = program.lFinMethT;
            groupKeyParameters.lBuydwnR1_rep = program.lBuydwnR1_rep;
            groupKeyParameters.lBuydwnR2_rep = program.lBuydwnR2_rep;
            groupKeyParameters.lBuydwnR3_rep = program.lBuydwnR3_rep;
            groupKeyParameters.lBuydwnR4_rep = program.lBuydwnR4_rep;
            groupKeyParameters.lBuydwnR5_rep = program.lBuydwnR5_rep;
            groupKeyParameters.lLpProductType = program.lLpProductType;
            groupKeyParameters.lRAdj1stCapMon_rep = program.lRadj1stCapMon_rep;
            groupKeyParameters.lRAdj1stCapR_rep = program.lRadj1stCapR_rep;
            groupKeyParameters.lRAdjCapR_rep = program.lRAdjCapR_rep;
            groupKeyParameters.lRAdjLifeCapR_rep = program.lRAdjLifeCapR_rep;
            groupKeyParameters.lRAdjCapMon_rep = program.lRAdjCapMon_rep;
            groupKeyParameters.lPrepmtPeriod_rep = program.lPrepmtPeriod_rep;

            RateMergeGroup rateMergeGroup = new RateMergeGroup(new RateMergeGroupKey(groupKeyParameters));
            return rateMergeGroup.Name;
        }

        /// <summary>
        /// Parse the Loan program node from a result loan program data.
        /// </summary>
        /// <param name="program">The program result.</param>
        /// <returns>The result program.</returns>
        private ResultLoanProgram ParseLoanProgram(CApplicantPriceXml program)
        {
            var ret = new ResultLoanProgram();

            ret.Version = program.Version;
            ret.IsBlockedRateLockSubmission = program.IsBlockedRateLockSubmission;
            ret.RateLockSubmissionUserWarningMessage = program.RateLockSubmissionUserWarningMessage;

            ret.lLpTemplateId = program.lLpTemplateId;
            ret.lLpTemplateNm = program.lLpTemplateNm;
            ret.lLpInvestorNm = program.lLpInvestorNm;
            ret.ProductCode = program.ProductCode;
            ret.MaxDti = program.MaxDti_rep;
            ret.UniqueChecksum = program.UniqueChecksum;
            ret.AreRatesExpired = program.AreRatesExpired;
            ret.IsHistoric = false;
            ret.MergeGroupName = this.ParseMergeGroupName(program);
            ret.ResultRateOptions = this.ParseRateOptions(program);
            ret.DisqualifiedRuleList = program.DisqualifiedRuleList.Select(x => x.Replace("<br />", System.Environment.NewLine));

            var repRateOptionList = program.RepresentativeRateOption;
            if (repRateOptionList != null && repRateOptionList.Length > 0)
            {
                var repRateOption = repRateOptionList[0];
                repRateOption.SetApplicantPrice(program);
                var parsedRepRateOption = this.ParseRateOption(repRateOption);
                this.ProcessQmDataRepRate(ret, parsedRepRateOption);

                ret.RepresentativeRateOption = parsedRepRateOption;
            }
            else
            {
                ret.RepresentativeRateOption = this.GetRepresentativeRate(ret.ResultRateOptions);
            }

            return ret;
        }

        /// <summary>
        /// Get a representative rate option from a rate option list.
        /// </summary>
        /// <param name="options">List of rate options.</param>
        /// <returns>A representative rate option.</returns>
        private ResultRateOption GetRepresentativeRate(List<ResultRateOption> options)
        {
            // OPM 450583.  The engine does not provide a rep rate in some cases, namely
            // when all rates are denied due to per-rate denials.   Case 175387 introduced 
            // a hack to the results displays so they will use the first rateoption in the list
            // when one was not provided, however results using this method for non-merge were
            // described as having a "janky" appearance.  The long term plan is to change the
            // core engine to be smarter, but for this hotfix, we use the simple par rate
            // algorithm to show the correct rep rate.
            ResultRateOption ret = null;
            if (options.Count > 0)
            {
                // If all results are dq'd, use the entire list,
                // Otherwise use only the qualified.
                var disqualifiedOptions = options.Where(o => !string.IsNullOrEmpty(o.DisqualReason));
                var applicableOptions = (disqualifiedOptions.Count() == options.Count()) ? disqualifiedOptions : options;

                ret = applicableOptions.Last();

                decimal retPoint = decimal.Parse(ret.Point);
                foreach (var option in applicableOptions.Reverse())
                {
                    var currentPoint = decimal.Parse(option.Point);
                    if ((ret == null)
                        || (retPoint > 0 && (currentPoint <= 0 || currentPoint < retPoint)))
                    {
                        ret = option;
                        retPoint = currentPoint;
                    }
                }
            }

            return ret;
        }

        /// <summary>
        /// Parse the rateoptions from a program.
        /// </summary>
        /// <param name="program">The program.</param>
        /// <returns>Parsed rate option list.</returns>
        private List<ResultRateOption> ParseRateOptions(CApplicantPriceXml program)
        {
            var ret = new List<ResultRateOption>();
            foreach (var rateOption in program.ApplicantRateOptions)
            {
                if (this.IsRateMerge && rateOption.ApplicantPrice == null)
                {
                    rateOption.SetApplicantPrice(program);
                }

                ret.Add(this.ParseRateOption(rateOption));
            }

            return ret;
        }

        /// <summary>
        /// Parse rate options from merge group.
        /// </summary>
        /// <param name="group">The merge group.</param>
        /// <returns>Parsed rate option list.</returns>
        private List<ResultRateOption> ParseRateOptions(RateMergeGroup group)
        {
            var ret = new List<ResultRateOption>();
            var overDtiRateWinners = group.RateOptions.Where(rateOption => rateOption.IsViolateMaxDti && rateOption.IsRateMergeWinner).ToList();
            var underDtiRateWinners = group.RateOptions.Where(rateOption => !rateOption.IsViolateMaxDti && rateOption.IsRateMergeWinner);

            foreach (var rateOption in underDtiRateWinners)
            {
                var parsedRateOption = this.ParseRateOption(rateOption);
                this.CreateRateStack(parsedRateOption, rateOption, group.AllRateOptions, overDtiRateWinners);
                ret.Add(parsedRateOption);
            }

            var overDtiStacks = new List<ResultRateOption>();
            foreach (var rateOption in overDtiRateWinners)
            {
                var parsedRateOption = this.ParseRateOption(rateOption);
                this.CreateRateStack(parsedRateOption, rateOption, group.AllRateOptions);
                overDtiStacks.Add(parsedRateOption);
            }

            return overDtiStacks.Concat(ret).ToList();
        }

        /// <summary>
        /// This method determines which rates are priced worse than the given rate option, and adds them to the list of worst stacks.
        /// </summary>
        /// <param name="parsedRateOption">The parsed rate option that will take in the rate stack..</param>
        /// <param name="rawRateOption">The raw rate option.  Used for decimal values.</param>
        /// <param name="allRawRates">The list of all rates priced in the program.</param>
        /// <param name="overDtiWinners">The list of rates that have the same rate as the parsedRateOption, but are over the max DTI.</param>
        private void CreateRateStack(ResultRateOption parsedRateOption, CApplicantRateOption rawRateOption, List<CApplicantRateOption> allRawRates, List<CApplicantRateOption> overDtiWinners = null)
        {
            var betterRateDtiViolationsRaw = allRawRates.Where(rate =>
                rate.Rate_rep.Equals(rawRateOption.Rate_rep)
                && rate.Point < rawRateOption.Point_
                && 
                (rate.IsViolateMaxDti && !rate.IsBlockedRateLockSubmission));

            if (betterRateDtiViolationsRaw.Any())
            {
                parsedRateOption.TrueBestOption = this.ParseRateOption(betterRateDtiViolationsRaw.OrderBy(rate => rate.Point).First());
            }

            if (!this.Broker.IsAlwaysUseBestPrice || this.Principal.HasPermission(Permission.AllowBypassAlwaysUseBestPrice))
            {
                var worseRatesRaw = allRawRates.Where(rate => rate.Rate_rep.Equals(rawRateOption.Rate_rep));
                var worseRates = worseRatesRaw.Select(rate => this.ParseRateOption(rate)).ToList();

                if (overDtiWinners != null)
                {
                    overDtiWinners.RemoveAll(rate => worseRatesRaw.Contains(rate));
                }

                if (worseRates.Count > 1)
                {
                    parsedRateOption.WorseRates = worseRates;
                }
            }
        }

        /// <summary>
        /// Parse the rate option data from a rate option.
        /// </summary>
        /// <param name="rateOption">The rate option result.</param>
        /// <returns>The rate option node.</returns>
        private ResultRateOption ParseRateOption(CApplicantRateOption rateOption)
        {
            if (rateOption == null)
            {
                return new ResultRateOption();
            }

            var ret = new ResultRateOption();

            ret.Rate = rateOption.Rate_rep;
            ret.RawRate = rateOption.RawRate_rep;
            ret.IsRateExpired = rateOption.IsRateExpired;
            ret.PinId = this.PricingStates?.FirstOrDefault(pricingState => pricingState.GetLoanTemplateId(rateOption.ApplicantPrice.lLienPosT) == rateOption.LpTemplateId && pricingState.GetNoteRate(rateOption.ApplicantPrice.lLienPosT).Equals(rateOption.Rate_rep))?.Id.ToString();
            ret.TeaserRate = rateOption.TeaserRate_rep;
            ret.BankedAmount = rateOption.BankAmount_rep;
            ret.RateOptionId = rateOption.RateOptionId;
            ret.IsViolateMaxDti = rateOption.IsViolateMaxDti;

            if (this.PricingModeT == E_sPricingModeT.InternalInvestorPricing)
            {
                // 7/8/2011 dd - OPM 67783 - Add Total Adjustments to internal pricing investor mode results screen.
                ret.TotalPriceAdj = rateOption.TotalPriceAdj_rep;
            }

            // To be more specific on the intent here:
            // - Eligibility Modes: Price is unused, no reason to set it to the model.
            // - Internal Front-End (Broker) and "Regular": Honor the comp settings
            //      as it does apply to the front-end transaction.
            // - Internal Back-End (Investor): Ignore the comp settings, and show 
            //      pricing always without comp.  Back-end pricing is what will be
            //      sold to the investor, so the LO's comp should not be included.
            if (this.PricingModeT != E_sPricingModeT.EligibilityBrokerPricing && this.PricingModeT != E_sPricingModeT.EligibilityInvestorPricing)
            {
                if (this.PricingModeT != E_sPricingModeT.InternalInvestorPricing
                    && this.IsLenderPaidOriginatorCompAndAdditionToFees
                    && this.Broker.IsUsePriceIncludingCompensationInPricingResult)
                {
                    // OPM 114645
                    ret.Price = rateOption.PointIncludingOriginatorCompIn100_rep;
                    ret.Point = rateOption.PointIncludingOriginatorComp_rep;
                }
                else
                {
                    ret.Price = rateOption.PointIn100_rep;
                    ret.Point = rateOption.Point_rep;
                }
            }

            ret.Payment = rateOption.FirstPmtAmt_rep;
            ret.QualRate = rateOption.QRate_rep;
            ret.QualPmt = rateOption.QualPmt_rep;
            ret.Margin = rateOption.Margin_rep;
            ret.Dti = this.IsTotalIncomeZero ? string.Empty : rateOption.BottomRatio_rep;
            ret.MaxDti = this.ParseDti(rateOption.MaxDti_rep);

            if (this.IsRateMerge)
            {
                // 1/8/2011 dd - For rate merge, we need loan program name and program id since each rate in the group can come from different loan program.
                ret.LpInvestorNm = rateOption.LpInvestorNm;
                ret.ProductCode = rateOption.ApplicantPrice.ProductCode;
                ret.LpTemplateId = rateOption.LpTemplateId.ToString();
                ret.LpTemplateNm = rateOption.LpTemplateNm;
                ret.UniqueChecksum = rateOption.ApplicantPrice.UniqueChecksum;
                ret.IsBlockedRateLockSubmission = rateOption.ApplicantPrice.IsBlockedRateLockSubmission;
                ret.RateLockSubmissionUserWarningMessage = rateOption.ApplicantPrice.RateLockSubmissionUserWarningMessage;
                ret.Version = rateOption.ApplicantPrice.Version;
            }

            ret.Apr = rateOption.Apr_rep;
            ret.ClosingCosts = rateOption.ClosingCost;
            ret.CashToClose = rateOption.CashToClose;
            ret.Reserves = this.ParseReserves(rateOption.Reserves);
            ret.BreakEvenMonths = this.ParseReserves(rateOption.BreakEvenPoint);
            ret.PrepaidCharges = rateOption.PrepaidCharges;
            ret.NonPrepaidCharges = rateOption.NonPrepaidCharges;
       
            if (rateOption.IsDisqualified)
            {
                // For old cert the engine would add br tags to the disqual reason.
                // Here in new UI formatter, we should use regular line breaks.
                ret.DisqualReason = rateOption.DisqualReason.Replace("<br />", System.Environment.NewLine);
            }

            if (rateOption.LoanData != null)
            {
                ret.sTransNetCashNonNegative = rateOption.LoanData.sTransNetCashNonNegative;
                ret.sPurchPrice = rateOption.LoanData.sPurchPrice;
                ret.sAltCost = rateOption.LoanData.sAltCost;
                ret.sLandCost = rateOption.LoanData.sLandCost;
                ret.sRefPdOffAmt1003 = rateOption.LoanData.sRefPdOffAmt1003;
                ret.sTotEstPp1003 = rateOption.LoanData.sTotEstPp1003;
                ret.sTotEstCcNoDiscnt1003 = rateOption.LoanData.sTotEstCcNoDiscnt1003;
                ret.sFfUfmip1003 = rateOption.LoanData.sFfUfmip1003;
                ret.sLDiscnt1003 = rateOption.LoanData.sLDiscnt1003;
                ret.sTotTransC = rateOption.LoanData.sTotTransC;
                ret.sONewFinBal = rateOption.LoanData.sONewFinBal;
                ret.sTotCcPbs = rateOption.LoanData.sTotCcPbs;
                ret.sOCredit1Desc = rateOption.LoanData.sOCredit1Desc;
                ret.sOCredit1Amt = rateOption.LoanData.sOCredit1Amt;
                ret.sOCredit2Desc = rateOption.LoanData.sOCredit2Desc;
                ret.sOCredit2Amt = rateOption.LoanData.sOCredit2Amt;
                ret.sOCredit3Desc = rateOption.LoanData.sOCredit3Desc;
                ret.sOCredit3Amt = rateOption.LoanData.sOCredit3Amt;
                ret.sOCredit4Desc = rateOption.LoanData.sOCredit4Desc;
                ret.sOCredit4Amt = rateOption.LoanData.sOCredit4Amt;
                ret.sOCredit5Amt = rateOption.LoanData.sOCredit5Amt;
                ret.sONewFinCc = rateOption.LoanData.sONewFinCc;
                ret.sONewFinCcInverted = rateOption.LoanData.sONewFinCcInverted;
                ret.sTotLiquidAssets = rateOption.LoanData.sTotLiquidAssets;
                ret.sLAmtCalc = rateOption.LoanData.sLAmtCalc;
                ret.sFfUfmipFinanced = rateOption.LoanData.sFfUfmipFinanced;
                ret.sFinalLAmt = rateOption.LoanData.sFinalLAmt;
                ret.sTransNetCash = rateOption.LoanData.sTransNetCash;
                ret.sIsIncludeProrationsIn1003Details = rateOption.LoanData.sIsIncludeProrationsIn1003Details;
                ret.sIsIncludeProrationsInTotPp = rateOption.LoanData.sIsIncludeProrationsInTotPp;
                ret.sTotEstPp = rateOption.LoanData.sTotEstPp;
                ret.sTotalBorrowerPaidProrations = rateOption.LoanData.sTotalBorrowerPaidProrations;
                ret.sIsIncludeONewFinCcInTotEstCc = rateOption.LoanData.sIsIncludeONewFinCcInTotEstCc;
                ret.sTotEstCcNoDiscnt = rateOption.LoanData.sTotEstCcNoDiscnt;
                ret.sMonthlyPmt = rateOption.LoanData.sMonthlyPmt;

                if (rateOption.LoanData.sIsTargetingUlad2019)
                {
                    ret.sAltCostLckd = rateOption.LoanData.sAltCostLckd;
                    ret.sLandIfAcquiredSeparately1003 = rateOption.LoanData.sLandIfAcquiredSeparately1003;
                    ret.sLDiscnt1003Lckd = rateOption.LoanData.sLDiscnt1003Lckd;
                    ret.sPurchasePrice1003 = rateOption.LoanData.sPurchasePrice1003;
                    ret.sRefNotTransMortgageBalancePayoffAmt = rateOption.LoanData.sRefNotTransMortgageBalancePayoffAmt;
                    ret.sRefTransMortgageBalancePayoffAmt = rateOption.LoanData.sRefTransMortgageBalancePayoffAmt;
                    ret.sSellerCreditsUlad = rateOption.LoanData.sSellerCreditsUlad;
                    ret.sTotCcPbsLocked = rateOption.LoanData.sTotCcPbsLocked;
                    ret.sTotCreditsUlad = rateOption.LoanData.sTotCreditsUlad;
                    ret.sTotEstBorrCostUlad = rateOption.LoanData.sTotEstBorrCostUlad;
                    ret.sTotEstCc1003Lckd = rateOption.LoanData.sTotEstCc1003Lckd;
                    ret.sTotEstPp1003Lckd = rateOption.LoanData.sTotEstPp1003Lckd;
                    ret.sTotMortLAmtUlad = rateOption.LoanData.sTotMortLAmtUlad;
                    ret.sTotMortLTotCreditUlad = rateOption.LoanData.sTotMortLTotCreditUlad;
                    ret.sTotOtherCreditsUlad = rateOption.LoanData.sTotOtherCreditsUlad;
                    ret.sTotTransCUlad = rateOption.LoanData.sTotTransCUlad;
                    ret.sTransNetCashUlad = rateOption.LoanData.sTransNetCashUlad;
                    ret.sTRIDSellerCredits = rateOption.LoanData.sTRIDSellerCredits;
                }
            }

            string modeQm;
            if (this.rateQmMap.TryGetValue(rateOption, out modeQm))
            {
                ret.QmMode = modeQm;
                ret.HpmMode = this.rateHpmMap[rateOption];
            }

            ret.ClosingCostBreakdown = this.ParseClosingCosts(rateOption);

            return ret;
        }

        /// <summary>
        /// Parse a closing cost list from a rate option.
        /// </summary>
        /// <param name="rateOption">The rate option.</param>
        /// <returns>The closing cost list.</returns>
        private List<ResultClosingCost> ParseClosingCosts(CApplicantRateOption rateOption)
        {
            var ret = new List<ResultClosingCost>();

            foreach (var feeItem in rateOption.ClosingCostBreakdown)
            {
                ret.Add(this.ParseClosingCost(feeItem));
            }

            return ret;
        }

        /// <summary>
        /// Parses a respa fee item into a closing cost node.
        /// </summary>
        /// <param name="feeItem">The fee item.</param>
        /// <returns>The closing cost list.</returns>
        private ResultClosingCost ParseClosingCost(RespaFeeItem feeItem)
        {
            var converter = new LosConvert();
            var ret = new ResultClosingCost();
            ret.Amount = converter.ToMoneyString(feeItem.Fee, FormatDirection.ToRep);
            ret.Description = feeItem.Description;
            ret.Section = ClosingCostSetUtils.MapIntegratedDisclosureSectionToString(feeItem.DisclosureSectionT);
            ret.Source = feeItem.DescriptionSource;

            return ret;
        }

        /// <summary>
        /// Parse reserves value.
        /// </summary>
        /// <param name="reserves">Reserves value.</param>
        /// <returns>Parsed reserves.</returns>
        private string ParseReserves(string reserves)
        {
            return reserves.Replace(" months", string.Empty);
        }

        /// <summary>
        /// Parse dti value.
        /// </summary>
        /// <param name="dti">Dti value.</param>
        /// <returns>Dti value parsed.</returns>
        private string ParseDti(string dti)
        {
            decimal value = 0;
            if (string.IsNullOrEmpty(dti) == false)
            {
                if (decimal.TryParse(dti.Replace("%", string.Empty), out value) == false)
                {
                    value = -1;
                }
            }

            if (value > 0)
            {
                return value.ToString();
            }
            else
            {
                return "N/A";
            }
        }

        /// <summary>
        /// Process the QM data.
        /// </summary>
        /// <param name="resultItem">Result to pull out the QM data.</param>
        private void ProcessQmData(UnderwritingResultItem resultItem)
        {
            if (this.ProcessQM)
            {
                // The intent here is to process the QM result per rate option,
                // so that QM and HPM can be known when parsing rate results.
                bool includeAllPrograms = !this.Broker.IsAlwaysUseBestPrice || this.Principal.HasPermission(Permission.AllowBypassAlwaysUseBestPrice);

                E_OriginatorCompPointSetting pointSetting;
                if (this.Broker.IsUsePriceIncludingCompensationInPricingResult && this.IsLenderPaidOriginatorCompAndAdditionToFees)
                {
                    // OPM 64253
                    pointSetting = E_OriginatorCompPointSetting.UsePointWithOriginatorComp;
                }
                else
                {
                    pointSetting = E_OriginatorCompPointSetting.UsePoint;
                }

                LosConvert losConvert = new LosConvert();
                var overrideParRate = ApplyLoanWorkerUnit.GetParRateOverride(new List<UnderwritingResultItem>(new[] { resultItem }), this.Broker, includeAllPrograms, pointSetting);

                foreach (var programs in new IEnumerable<CApplicantPriceXml>[] { resultItem.GetEligibleLoanProgramList(), resultItem.GetIneligibleLoanProgramList() })
                {
                    // 7/1/2009 dd - OPM 24899 - Pass in option whether to filter out higher rate with worst price.
                    RateMergeBucket rateMergeBucket = new RateMergeBucket(this.Broker.IsShowRateOptionsWorseThanLowerRate, includeAllPrograms);

                    foreach (var program in programs)
                    {
                        rateMergeBucket.Add(program);
                    }

                    foreach (RateMergeGroup mergeGroup in rateMergeBucket.RateMergeGroups)
                    {
                        if (mergeGroup.RateOptions.Count == 0)
                        {
                            continue;
                        }

                        decimal parRate = ApplyLoanWorkerUnit.GetParRate(overrideParRate, mergeGroup, pointSetting);

                        foreach (var rateOption in mergeGroup.AllRateOptions)
                        {
                            if (rateOption.QMData != null)
                            {
                                QMStatusCalculator calculator = new QMStatusCalculator(parRate, rateOption.Apr_rep, rateOption.QMData, losConvert, rateOption.Rate);
                                this.rateQmMap.Add(rateOption, calculator.GetQMStatus().ToString("d"));
                                this.rateHpmMap.Add(rateOption, calculator.GetHPMLStatus().ToString("d"));
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Processes the QM data for a representative rate.
        /// </summary>
        /// <param name="program">The parsed program.</param>
        /// <param name="rateOption">The parsed rep rate.</param>
        private void ProcessQmDataRepRate(ResultLoanProgram program, ResultRateOption rateOption)
        {
            if (this.ProcessQM)
            {
                var matchingOption = program.ResultRateOptions.FirstOrDefault(option => option.Rate == rateOption.Rate && option.Point == rateOption.Point);

                if (matchingOption != null)
                {
                    rateOption.QmMode = matchingOption.QmMode;
                    rateOption.HpmMode = matchingOption.HpmMode;
                }
            }
        }
    }
}
