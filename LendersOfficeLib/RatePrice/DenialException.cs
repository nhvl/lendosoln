﻿// <copyright file="DenialException.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Flynn, Matthew
//    Date:   7/27/2014
// </summary>

namespace DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// POD class representing a denial with a user-provided exception for the denial.
    /// </summary>
    public class DenialException
    {
        /// <summary>
        /// Gets or sets denial reason typically provided by the engine.
        /// </summary>
        /// <value>Denial reason typically provided by the engine.</value>
        public string DenialReason { get; set; }

        /// <summary>
        /// Gets or sets exception reason typically provided by the user.
        /// </summary>
        /// <value>Exception reason typically provided by the user.</value>
        public string ExceptionReason { get; set; }

        /// <summary>
        /// Gets or sets the lien position the Denial is associated with.
        /// </summary>
        /// <value>The lien position the Denial is associated with.  Because typically a single
        /// list is provided for combo submission, engine needs to know which loan caused the denial.</value>
        public E_sLienPosT LienPosT { get; set; }
    }
}
