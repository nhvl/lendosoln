﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOfficeApp.los.RatePrice;
using System.Data.Common;
using System.Data.SqlClient;
using CommonProjectLib.Common.Lib;
using System.Data;
using System.Collections;
using LendersOffice.Common;
using LendersOffice.RatePrice.FileBasedPricing;
using LendersOffice.Integration.MortgageInsurance;

namespace LendersOfficeApp.los.RatePrice
{

    // POD. A single PMI prod
    public class RtPmiProgram
    {
        public Guid PmiProgramId { get; set; }
        public E_PmiCompanyT Company { get; set; }
        public E_sProdConvMIOptionT PmiType { get; set; }
        public HashSet<Guid> Policies { get; set; }
        public bool UfmipIsRefundableOnProRataBasis { get; set; }
    }

    //POD. Information about a disqualified policy.  Needed to implement logic in 107423.
    public class PmiDenial : IComparable
    {
        public int NumOfDenials;    // Pick the one with fewest denials (regardless of rank)
        public int Rank;            // If multiple have the same number of denials then use rank as a tie-breaker
        public string Company;      // If there is still a tie (ie equal denials and equal rank) then just go in alphabetical order.
        public string DenialText;

        #region IComparable Members

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            PmiDenial otherPmiResult = obj as PmiDenial;
            if (otherPmiResult != null)
            {
                if (otherPmiResult.NumOfDenials != NumOfDenials)
                    return NumOfDenials.CompareTo(otherPmiResult.NumOfDenials); 
                
                if ( Rank != otherPmiResult.Rank )
                    return Rank.CompareTo(otherPmiResult.Rank);

                return Company.CompareTo(otherPmiResult.Company);
                
            }
            else
                throw new CBaseException(ErrorMessages.Generic, "Cannot compare PmiDenial");

        }

        #endregion
    }


    public class PmiResult : IComparable
    {
        public PmiResult(int rank, RtPmiProgram program, PriceAdjustRecord result, CPageData dataLoan)
        {
            Rank = rank;
            PmiType = program.PmiType;
            PmiCompany = program.Company;
            MonthlyPct = result.m_rateDeltaDisclosed + result.m_rateDeltaHidden;

            if (program.PmiType == E_sProdConvMIOptionT.BorrPaidSplitPrem)
            {
                SinglePct = Tools.Get_sConvSplitMIRT(dataLoan.sConvSplitMIRT) ?? 0m;
            }
            else
            {
                SinglePct = result.m_feeDeltaDisclosed + result.m_feeDeltaHidden;    
            }

            UfmipIsRefundableOnProRataBasis = program.UfmipIsRefundableOnProRataBasis;
            
            decimal loanAmt = dataLoan.sLAmtCalc;            

            switch (program.PmiType)
            {
                case E_sProdConvMIOptionT.BorrPaidMonPrem:
                    MonthlyPmt = (MonthlyPct / 100 * loanAmt )/ 12 ;
                    SinglePmt = SinglePct = 0;
                    break;
                case E_sProdConvMIOptionT.BorrPaidSinglePrem:
                case E_sProdConvMIOptionT.LendPaidSinglePrem:
                    MonthlyPmt = MonthlyPct = 0;
                    SinglePmt = SinglePct / 100 * loanAmt;
                    break;
                case E_sProdConvMIOptionT.BorrPaidSplitPrem:
                    MonthlyPmt = ( MonthlyPct / 100 * loanAmt ) / 12;
                    SinglePmt = SinglePct / 100 * loanAmt;
                    break;
                default:
                    throw new UnhandledEnumException(PmiType);
            }
            
            MonthlyPmt = Math.Round(MonthlyPmt, 2);
            SinglePmt = Math.Round(SinglePmt, 2);
        }

        public PmiResult(MIOrderInfo miQuote, CPageData dataLoan, int rank, E_PmiCompanyT company)
        {
            Rank = rank;
            PmiType = dataLoan.sProdConvMIOptionT;
            PmiCompany = company;

            if (!miQuote.HasResponse)
            {
                var denial = new PmiDenial() { DenialText = "No response from MI Provider" };
                this.DenialReasons.Add(denial);
                return;
            }

            var responseInfo = miQuote.ResponseProvider.ResponseInfo;
            var loanAmt = dataLoan.sLAmtCalc;
            Rank = rank;

            decimal initialPremium = MIUtil.ConvertPremium(responseInfo.MIInitialPremiumRatePercent, false);
            decimal initialPremiumAmount = MIUtil.ConvertDollarAmount(responseInfo.MIInitialPremiumAmount);
             decimal monthlyBasis = initialPremium == 0 ? 0 : initialPremiumAmount / initialPremium;
            if (PmiType == E_sProdConvMIOptionT.BorrPaidSplitPrem)
            {
                monthlyBasis = monthlyBasis / 12;
            }

            if (PmiType == E_sProdConvMIOptionT.BorrPaidSplitPrem)
            {
                SinglePct = Tools.Get_sConvSplitMIRT(dataLoan.sConvSplitMIRT) ?? 0m;
            }
            else
            {
                SinglePct = monthlyBasis;
            }

            MonthlyPct = monthlyBasis;

            switch (dataLoan.sProdConvMIOptionT)
            {
                case E_sProdConvMIOptionT.BorrPaidMonPrem:
                    MonthlyPmt = (MonthlyPct / 100 * loanAmt) / 12;
                    SinglePmt = SinglePct = 0;
                    break;
                case E_sProdConvMIOptionT.BorrPaidSinglePrem:
                case E_sProdConvMIOptionT.LendPaidSinglePrem:
                    MonthlyPmt = MonthlyPct = 0;
                    SinglePmt = SinglePct / 100 * loanAmt;
                    break;
                case E_sProdConvMIOptionT.BorrPaidSplitPrem:
                    MonthlyPmt = (MonthlyPct / 100 * loanAmt) / 12;
                    SinglePmt = SinglePct / 100 * loanAmt;
                    break;
                default:
                    throw new UnhandledEnumException(PmiType);
            }

            MonthlyPmt = Math.Round(MonthlyPmt, 2);
            SinglePmt = Math.Round(SinglePmt, 2);
        }

        public int Rank { get; private set; }
        public E_PmiCompanyT PmiCompany { get; private set; }
        public E_sProdConvMIOptionT PmiType { get; private set; }
        public decimal MonthlyPmt { get; private set; }
        public decimal SinglePmt { get; private set; }
        public decimal MonthlyPct { get; private set; }
        public decimal SinglePct { get; private set; }
        public bool UfmipIsRefundableOnProRataBasis { get; private set; }

        // OPM 471515 Needs the concept of an ineligible MI price
        // so we can applied denied MI back to the loanfile.
        public bool IsDenied => DenialReasons.Count() != 0;
        public List<PmiDenial> DenialReasons { get; private set; } = new List<PmiDenial>();


        public static string PmiTypeDescription (E_sProdConvMIOptionT option)
        {
                switch (option)
                {
                    case E_sProdConvMIOptionT.BorrPaidMonPrem:
                        return "BPMI-Monthly";
                    case E_sProdConvMIOptionT.BorrPaidSinglePrem:
                        return "BPMI-Single";
                    case E_sProdConvMIOptionT.BorrPaidSplitPrem:
                        return "BPMI-Split";
                    case E_sProdConvMIOptionT.LendPaidSinglePrem:
                        return "LPMI-Single";
                    case E_sProdConvMIOptionT.NoMI:
                        return "No MI";
                    case E_sProdConvMIOptionT.Blank:
                        return string.Empty;
                    default:
                        throw new UnhandledEnumException(option);
            }
        }

        public static E_sProdConvMIOptionT GetPmiType(string desc)
        {
            if (desc.StartsWith("BPMI-Monthly")) return E_sProdConvMIOptionT.BorrPaidMonPrem;
            if (desc.StartsWith("BPMI-Single")) return E_sProdConvMIOptionT.BorrPaidSinglePrem;
            if (desc.StartsWith("BPMI-Split")) return E_sProdConvMIOptionT.BorrPaidSplitPrem;
            if (desc.StartsWith("LPMI-Single")) return E_sProdConvMIOptionT.LendPaidSinglePrem;
            if (string.IsNullOrEmpty(desc)) return E_sProdConvMIOptionT.Blank;
            return E_sProdConvMIOptionT.NoMI;
        }

        #region IComparable Members

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            PmiResult otherPmiResult = obj as PmiResult;
            if (otherPmiResult != null)
            {

                // If one is denied, but the other is not, 
                // the eligible one wins. regardless of data.
                if (IsDenied != otherPmiResult.IsDenied )
                {
                    return IsDenied.CompareTo(otherPmiResult.IsDenied);
                }

                    switch (PmiType)
                {
                    case E_sProdConvMIOptionT.BorrPaidMonPrem:
                    case E_sProdConvMIOptionT.BorrPaidSplitPrem:
                        return MonthlyPmt.CompareTo(otherPmiResult.MonthlyPmt);
                    case E_sProdConvMIOptionT.BorrPaidSinglePrem:
                    case E_sProdConvMIOptionT.LendPaidSinglePrem:
                        return SinglePmt.CompareTo(otherPmiResult.SinglePmt);

                    default:
                        throw new UnhandledEnumException(PmiType);
                }
            }
            else
                throw new CBaseException(ErrorMessages.Generic, "Cannot compare PmiResult");
        }

        #endregion
    }


    /// <summary>
    /// Note that we expect this to be very simplified pricing with very few
    /// policies, and not very many PMI objects.
    /// </summary>
    public class PmiManager
    {
        private Dictionary<E_PmiCompanyT, List<RtPmiProgram>> m_programCache = new Dictionary<E_PmiCompanyT, List<RtPmiProgram>>();
        private HashSet<Guid> m_specialPmiPolicyIds = new HashSet<Guid>();

        public IEnumerable<Guid> PmiPolicyIds
        {
            get { return m_specialPmiPolicyIds; }
        }

        public IPolicyRelationDb PolicyRelationCenter { get; private set; }

        private static E_sProdConvMIOptionT MapTosProdConvMIOptionT(E_PmiTypeT pmiTypeT)
        {
            switch (pmiTypeT)
            {
                case E_PmiTypeT.BorrowerPaidMonthlyPremium: return E_sProdConvMIOptionT.BorrPaidMonPrem;
                case E_PmiTypeT.BorrowerPaidSinglePremium: return E_sProdConvMIOptionT.BorrPaidSinglePrem;
                case E_PmiTypeT.BorrowerPaidSplitPremium: return E_sProdConvMIOptionT.BorrPaidSplitPrem;
                case E_PmiTypeT.LenderPaidSinglePremium: return E_sProdConvMIOptionT.LendPaidSinglePrem;
                default:
                    throw new UnhandledEnumException(pmiTypeT);
            }
        }

        public PmiManager(IEnumerable<FileBasedPmiPolicyAssociation> pmiPolicyAssoications, 
            IEnumerable<FileBasedPmiProduct> pmiPrograms,
            IPolicyRelationDb policyRelationCenter)
        {
            this.PolicyRelationCenter = policyRelationCenter;
            foreach (var row in pmiPrograms)
            {
                E_PmiCompanyT pmiCompany = row.MICompany;
                Guid pmiProductId = row.ProductId;
                E_sProdConvMIOptionT pmiType = MapTosProdConvMIOptionT ( row.MIType );
                bool ufmipIsRefundableOnProRataBasis = row.UfmipIsRefunableOnProRataBasis;

                if (!m_programCache.ContainsKey(pmiCompany))
                    m_programCache[pmiCompany] = new List<RtPmiProgram>(5);

                m_programCache[pmiCompany].Add(new RtPmiProgram() { PmiProgramId = pmiProductId, PmiType = pmiType, Company = pmiCompany, Policies = new HashSet<Guid>(), UfmipIsRefundableOnProRataBasis = ufmipIsRefundableOnProRataBasis });
            }

            foreach (var row in pmiPolicyAssoications)
            {
                Guid pmiProductId = row.PmiProductId; 
                Guid pricePolicyId = row.PricePolicyId;

                HashSet<Guid> policyList = new HashSet<Guid>(new Guid[] { pricePolicyId });

                ArrayList ancestors = policyRelationCenter.GetAncestors(pricePolicyId);
                if ( ancestors != null && ancestors.Count > 0 )
                    policyList.UnionWith(ancestors.Cast<Guid>());

                m_specialPmiPolicyIds.UnionWith(policyList);

                foreach (var company in m_programCache)
                {
                    foreach (var program in company.Value)
                    {
                        if (program.PmiProgramId == pmiProductId)
                        {
                            program.Policies.UnionWith(policyList);
                        }
                    }
                }
            }
        }

        public RtPmiProgram GetPmiProgram(E_PmiCompanyT company, E_sProdConvMIOptionT type)
        {
            // Per PDE, there should be either 0 or 1 programs of each type per company
            // (No duplicates)

            foreach (var pmi in m_programCache.Where(p => p.Key == company))
            {
                return pmi.Value.FirstOrDefault(o => o.PmiType == type);
            }

            return null;
        }

        public static decimal? GetStandardCoverage(E_sLpProductT loanProductType, decimal ltv, int term, E_sProdSpT propertyType, E_sProd3rdPartyUwResultT uwResultType, E_sFinMethT amortizationType)
        {
            var coverage = CoverageMap.FirstOrDefault((item) =>
            {
                return item.DoesLoanProductTypeMatch(loanProductType) &&
                       ltv > item.LtvMinimum && ltv <= item.LtvMaximum &&
                       item.DoesLoanTermMatch(term) &&
                       item.DoesPropTypeAndUwResultTypeMatch(propertyType, uwResultType) &&
                       item.DoesAmortizationTypeMatch(amortizationType);
            });

            return coverage == null ? (decimal?)null : coverage.CoveragePercent;
        }

        private static List<CoverageItem> CoverageMap = new List<CoverageItem>()
        {
            // The following is the legend for the comments above each CoverageItem. 
            // Each comment was copy pasted from the excel sheet in case 241546.
            // An ANY value indicates that we don't care about that parameter.
            // [Accepted loan product types] [ltv range] [loan term range] [See below] [Amortization Type] [Standard Coverage Percent]
            // The value in the comment indicates the expected result of the check below.
            // PropertyType = "Manufactured" AND ThirdPartyUwResultType = "DUApproveEligible" or "DUApproveIneligible" or "DUReferEligible" or "DUReferIneligible" or "DUReferWCautionEligible" or "DUReferWCautionIneligible"

            // "MY COMMUNITY"	>80%-85%	ANY	ANY	ANY	6%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lptype) => lptype == E_sLpProductT.MyCommunity,
                LtvMinimum = 80m,
                LtvMaximum = 85m,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = null,
                DoesAmortizationTypeMatch = (amorType) => true,
                CoveragePercent = 6.0m
            },
            //"MY COMMUNITY"	>85%-90%	ANY	ANY	ANY	12%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lptype) => lptype == E_sLpProductT.MyCommunity,
                LtvMinimum = 85m,
                LtvMaximum = 90m,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = null,
                DoesAmortizationTypeMatch = (amorType) => true,
                CoveragePercent = 12.0m
            },
            //"MY COMMUNITY"	>90%-95%	ANY	ANY	ANY	16%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lptype) => lptype == E_sLpProductT.MyCommunity,
                LtvMinimum = 90m,
                LtvMaximum = 95m,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = null,
                DoesAmortizationTypeMatch = (amorType) => true,
                CoveragePercent = 16.0m
            },
            //"MY COMMUNITY"	>95%	ANY	ANY	ANY	18%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lptype) => lptype == E_sLpProductT.MyCommunity,
                LtvMinimum = 95m,
                LtvMaximum = decimal.MaxValue,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = null,
                DoesAmortizationTypeMatch = (amorType) => true,
                CoveragePercent = 18.0m
            },
            //"HOMEREADY" or "HOME POSSIBLE"	>80%-85%	Term > 240 Months	NO	FIXED	12%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.HomePossible,
                LtvMinimum = 80m,
                LtvMaximum = 85m,
                DoesLoanTermMatch = (loanTerm) => loanTerm > 240,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.Fixed,
                CoveragePercent = 12.0m
            },
            // "HOMEREADY" or "HOME POSSIBLE"	>85%-90%	Term > 240 Months	NO	FIXED	25%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.HomePossible,
                LtvMinimum = 85m,
                LtvMaximum = 90m,
                DoesLoanTermMatch = (loanTerm) => loanTerm > 240,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.Fixed,
                CoveragePercent = 25.0m
            },
            //"HOMEREADY" or "HOME POSSIBLE"	>90%-95%	Term > 240 Months	NO	FIXED	25%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.HomePossible,
                LtvMinimum = 90m,
                LtvMaximum = 95m,
                DoesLoanTermMatch = (loanTerm) => loanTerm > 240,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.Fixed,
                CoveragePercent = 25.0m
            },
            //"HOMEREADY" or "HOME POSSIBLE"	>95%	Term > 240 Months	NO	FIXED	25%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.HomePossible,
                LtvMinimum = 95m,
                LtvMaximum = decimal.MaxValue,
                DoesLoanTermMatch = (loanTerm) => loanTerm > 240,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.Fixed,
                CoveragePercent = 25.0m
            },
            //"HOMEREADY" or "HOME POSSIBLE"	>80%-85%	Term  <= 240 Months	NO	FIXED	6%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.HomePossible,
                LtvMinimum = 80m,
                LtvMaximum = 85m,
                DoesLoanTermMatch = (loanTerm) => loanTerm <= 240,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.Fixed,
                CoveragePercent = 6.0m
            },
            //"HOMEREADY" or "HOME POSSIBLE"	>85%-90%	Term  <= 240 Months	NO	FIXED	12%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.HomePossible,
                LtvMinimum = 85m,
                LtvMaximum = 90m,
                DoesLoanTermMatch = (loanTerm) => loanTerm <= 240,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.Fixed,
                CoveragePercent = 12.0m
            },
            //"HOMEREADY" or "HOME POSSIBLE"	>90%-95%	Term  <= 240 Months	NO	FIXED	25%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.HomePossible,
                LtvMinimum = 90m,
                LtvMaximum = 95m,
                DoesLoanTermMatch = (loanTerm) => loanTerm <= 240,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.Fixed,
                CoveragePercent = 25.0m
            },
            //"HOMEREADY" or "HOME POSSIBLE"	>95%	Term  <= 240 Months	NO	FIXED	25%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.HomePossible,
                LtvMinimum = 95m,
                LtvMaximum = decimal.MaxValue,
                DoesLoanTermMatch = (loanTerm) => loanTerm <= 240,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.Fixed,
                CoveragePercent = 25.0m
            },
            //"HOMEREADY" or "HOME POSSIBLE"	>80%-85%	ANY	NO	ARM	12%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.HomePossible,
                LtvMinimum = 80m,
                LtvMaximum = 85m,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.ARM,
                CoveragePercent = 12.0m
            },
            //"HOMEREADY" or "HOME POSSIBLE"	>85%-90%	ANY	NO	ARM	25%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.HomePossible,
                LtvMinimum = 85m,
                LtvMaximum = 90m,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.ARM,
                CoveragePercent = 25.0m
            },
            //"HOMEREADY" or "HOME POSSIBLE"	>90%-95%	ANY	NO	ARM	25%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.HomePossible,
                LtvMinimum = 90m,
                LtvMaximum = 95m,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.ARM,
                CoveragePercent = 25.0m
            },
            //"HOMEREADY" or "HOME POSSIBLE"	>95%	ANY	NO	ARM	25%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.HomePossible,
                LtvMinimum = 95m,
                LtvMaximum = decimal.MaxValue,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.ARM,
                CoveragePercent = 25.0m
            },
            //"HOMEREADY" or "HOME POSSIBLE"	>80%-85%	ANY	YES	ANY	12%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.HomePossible,
                LtvMinimum = 80m,
                LtvMaximum = 85m,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = true,
                DoesAmortizationTypeMatch = (amortType) => true,
                CoveragePercent = 12.0m
            },
            //"HOMEREADY" or "HOME POSSIBLE"	>85%-90%	ANY	YES	ANY	25%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.HomePossible,
                LtvMinimum = 85m,
                LtvMaximum = 90m,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = true,
                DoesAmortizationTypeMatch = (amortType) => true,
                CoveragePercent = 25.0m
            },
            // "HOMEREADY" or "HOME POSSIBLE"	>90%-95%	ANY	YES	ANY	25%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.HomePossible,
                LtvMinimum = 90m,
                LtvMaximum = 95m,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = true,
                DoesAmortizationTypeMatch = (amortType) => true,
                CoveragePercent = 25.0m
            },
            // "HOMEREADY" or "HOME POSSIBLE"	>95%	ANY	YES	ANY	25%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.HomePossible,
                LtvMinimum = 95m,
                LtvMaximum = decimal.MaxValue,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = true,
                DoesAmortizationTypeMatch = (amortType) => true,
                CoveragePercent = 25.0m
            },
            // "CONVENTIONAL BOND"	>80%-85%	ANY	ANY	ANY	6%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lptype) => lptype == E_sLpProductT.ConventionalBond,
                LtvMinimum = 80m,
                LtvMaximum = 85m,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = null,
                DoesAmortizationTypeMatch = (amorType) => true,
                CoveragePercent = 6.0m
            },
            //"CONVENTIONAL BOND"	>85%-90%	ANY	ANY	ANY	12%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lptype) => lptype == E_sLpProductT.ConventionalBond,
                LtvMinimum = 85m,
                LtvMaximum = 90m,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = null,
                DoesAmortizationTypeMatch = (amorType) => true,
                CoveragePercent = 12.0m
            },
            //"CONVENTIONAL BOND"	>90%-95%	ANY	ANY	ANY	16%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lptype) => lptype == E_sLpProductT.ConventionalBond,
                LtvMinimum = 90m,
                LtvMaximum = 95m,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = null,
                DoesAmortizationTypeMatch = (amorType) => true,
                CoveragePercent = 16.0m
            },
            //"CONVENTIONAL BOND"	>95%	ANY	ANY	ANY	18%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lptype) => lptype == E_sLpProductT.ConventionalBond,
                LtvMinimum = 95m,
                LtvMaximum = decimal.MaxValue,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = null,
                DoesAmortizationTypeMatch = (amorType) => true,
                CoveragePercent = 18.0m
            },
            // NOT ("MY COMMUNITY" or "HOME POSSIBLE" or "HOMEREADY" or "CONVENTIONAL BOND")	>80%-85%	Term > 240 Months	NO	FIXED	12%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => !(lpType == E_sLpProductT.MyCommunity || lpType == E_sLpProductT.HomePossible || lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.ConventionalBond),
                LtvMinimum = 80m,
                LtvMaximum = 85m,
                DoesLoanTermMatch = (loanTerm) => loanTerm > 240,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.Fixed,
                CoveragePercent = 12.0m
            },
            // NOT ("MY COMMUNITY" or "HOME POSSIBLE" or "HOMEREADY" or "CONVENTIONAL BOND")	>85%-90%	Term > 240 Months	NO	FIXED	25%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => !(lpType == E_sLpProductT.MyCommunity || lpType == E_sLpProductT.HomePossible || lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.ConventionalBond),
                LtvMinimum = 85m,
                LtvMaximum = 90m,
                DoesLoanTermMatch = (loanTerm) => loanTerm > 240,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.Fixed,
                CoveragePercent = 25.0m
            },
            //NOT ("MY COMMUNITY" or "HOME POSSIBLE" or "HOMEREADY" or "CONVENTIONAL BOND")	>90%-95%	Term > 240 Months	NO	FIXED	30%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => !(lpType == E_sLpProductT.MyCommunity || lpType == E_sLpProductT.HomePossible || lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.ConventionalBond),
                LtvMinimum = 90m,
                LtvMaximum = 95m,
                DoesLoanTermMatch = (loanTerm) => loanTerm > 240,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.Fixed,
                CoveragePercent = 30.0m
            },
            //NOT ("MY COMMUNITY" or "HOME POSSIBLE" or "HOMEREADY" or "CONVENTIONAL BOND")	>95%	Term > 240 Months	NO	FIXED	35%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => !(lpType == E_sLpProductT.MyCommunity || lpType == E_sLpProductT.HomePossible || lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.ConventionalBond),
                LtvMinimum = 95m,
                LtvMaximum = decimal.MaxValue,
                DoesLoanTermMatch = (loanTerm) => loanTerm > 240,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.Fixed,
                CoveragePercent = 35.0m
            },
            //NOT ("MY COMMUNITY" or "HOME POSSIBLE" or "HOMEREADY" or "CONVENTIONAL BOND")	>80%-85%	Term  <= 240 Months	NO	FIXED	6%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => !(lpType == E_sLpProductT.MyCommunity || lpType == E_sLpProductT.HomePossible || lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.ConventionalBond),
                LtvMinimum = 80m,
                LtvMaximum = 85m,
                DoesLoanTermMatch = (loanTerm) => loanTerm <= 240,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.Fixed,
                CoveragePercent = 6.0m
            },
            //NOT ("MY COMMUNITY" or "HOME POSSIBLE" or "HOMEREADY" or "CONVENTIONAL BOND")	>85%-90%	Term  <= 240 Months	NO	FIXED	12%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => !(lpType == E_sLpProductT.MyCommunity || lpType == E_sLpProductT.HomePossible || lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.ConventionalBond),
                LtvMinimum = 85m,
                LtvMaximum = 90m,
                DoesLoanTermMatch = (loanTerm) => loanTerm <= 240,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.Fixed,
                CoveragePercent = 12.0m
            },
            //NOT ("MY COMMUNITY" or "HOME POSSIBLE" or "HOMEREADY" or "CONVENTIONAL BOND")	>90%-95%	Term  <= 240 Months	NO	FIXED	25%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => !(lpType == E_sLpProductT.MyCommunity || lpType == E_sLpProductT.HomePossible || lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.ConventionalBond),
                LtvMinimum = 90m,
                LtvMaximum = 95m,
                DoesLoanTermMatch = (loanTerm) => loanTerm <= 240,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.Fixed,
                CoveragePercent = 25.0m
            },
            // NOT ("MY COMMUNITY" or "HOME POSSIBLE" or "HOMEREADY" or "CONVENTIONAL BOND")	>95%	Term  <= 240 Months	NO	FIXED	35%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => !(lpType == E_sLpProductT.MyCommunity || lpType == E_sLpProductT.HomePossible || lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.ConventionalBond),
                LtvMinimum = 95m,
                LtvMaximum = decimal.MaxValue,
                DoesLoanTermMatch = (loanTerm) => loanTerm <= 240,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.Fixed,
                CoveragePercent = 35.0m
            },
            //NOT ("MY COMMUNITY" or "HOME POSSIBLE" or "HOMEREADY" or "CONVENTIONAL BOND")	>80%-85%	ANY	NO	ARM	12%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => !(lpType == E_sLpProductT.MyCommunity || lpType == E_sLpProductT.HomePossible || lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.ConventionalBond),
                LtvMinimum = 80m,
                LtvMaximum = 85m,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.ARM,
                CoveragePercent = 12.0m
            },
            //NOT ("MY COMMUNITY" or "HOME POSSIBLE" or "HOMEREADY" or "CONVENTIONAL BOND")	>85%-90%	ANY	NO	ARM	25%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => !(lpType == E_sLpProductT.MyCommunity || lpType == E_sLpProductT.HomePossible || lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.ConventionalBond),
                LtvMinimum = 85m,
                LtvMaximum = 90m,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.ARM,
                CoveragePercent = 25.0m
            },
            //NOT ("MY COMMUNITY" or "HOME POSSIBLE" or "HOMEREADY" or "CONVENTIONAL BOND")	>90%-95%	ANY	NO	ARM	30%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => !(lpType == E_sLpProductT.MyCommunity || lpType == E_sLpProductT.HomePossible || lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.ConventionalBond),
                LtvMinimum = 90m,
                LtvMaximum = 95m,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.ARM,
                CoveragePercent = 30.0m
            },
            //NOT ("MY COMMUNITY" or "HOME POSSIBLE" or "HOMEREADY" or "CONVENTIONAL BOND")	>95%	ANY	NO	ARM	35%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => !(lpType == E_sLpProductT.MyCommunity || lpType == E_sLpProductT.HomePossible || lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.ConventionalBond),
                LtvMinimum = 95m,
                LtvMaximum = decimal.MaxValue,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = false,
                DoesAmortizationTypeMatch = (amortType) => amortType == E_sFinMethT.ARM,
                CoveragePercent = 35.0m
            },
            //NOT ("MY COMMUNITY" or "HOME POSSIBLE" or "HOMEREADY" or "CONVENTIONAL BOND")	>80%-85%	ANY	YES	ANY	12%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => !(lpType == E_sLpProductT.MyCommunity || lpType == E_sLpProductT.HomePossible || lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.ConventionalBond),
                LtvMinimum = 80m,
                LtvMaximum = 85m,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = true,
                DoesAmortizationTypeMatch = (amortType) => true,
                CoveragePercent = 12.0m
            },
            //NOT ("MY COMMUNITY" or "HOME POSSIBLE" or "HOMEREADY" or "CONVENTIONAL BOND")	>85%-90%	ANY	YES	ANY	25%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => !(lpType == E_sLpProductT.MyCommunity || lpType == E_sLpProductT.HomePossible || lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.ConventionalBond),
                LtvMinimum = 85m,
                LtvMaximum = 90m,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = true,
                DoesAmortizationTypeMatch = (amortType) => true,
                CoveragePercent = 25.0m
            },
            //NOT ("MY COMMUNITY" or "HOME POSSIBLE" or "HOMEREADY" or "CONVENTIONAL BOND")	>90%-95%	ANY	YES	ANY	30%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => !(lpType == E_sLpProductT.MyCommunity || lpType == E_sLpProductT.HomePossible || lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.ConventionalBond),
                LtvMinimum = 90m,
                LtvMaximum = 95m,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = true,
                DoesAmortizationTypeMatch = (amortType) => true,
                CoveragePercent = 30.0m
            },
            //NOT ("MY COMMUNITY" or "HOME POSSIBLE" or "HOMEREADY" or "CONVENTIONAL BOND")	>95%	ANY	YES	ANY	35%
            new CoverageItem()
            {
                DoesLoanProductTypeMatch = (lpType) => !(lpType == E_sLpProductT.MyCommunity || lpType == E_sLpProductT.HomePossible || lpType == E_sLpProductT.HOMEREADY || lpType == E_sLpProductT.ConventionalBond),
                LtvMinimum = 95m,
                LtvMaximum = decimal.MaxValue,
                DoesLoanTermMatch = (loanTerm) => true,
                ExpectedResultForPropTypeAndUwResultTypeCheck = true,
                DoesAmortizationTypeMatch = (amortType) => true,
                CoveragePercent = 35.0m
            }
        };

        private class CoverageItem
        {
            public decimal LtvMaximum
            {
                get;
                set;
            }

            public decimal LtvMinimum
            {
                get;
                set;
            }

            public Func<E_sLpProductT, bool> DoesLoanProductTypeMatch
            {
                get;
                set;
            }

            public Func<int, bool> DoesLoanTermMatch
            {
                get;
                set;
            }

            public bool? ExpectedResultForPropTypeAndUwResultTypeCheck
            {
                get;
                set;
            }

            public Func<E_sFinMethT, bool> DoesAmortizationTypeMatch
            {
                get;
                set;
            }

            public decimal CoveragePercent
            {
                get;
                set;
            }

            public bool DoesPropTypeAndUwResultTypeMatch(E_sProdSpT propertyType, E_sProd3rdPartyUwResultT resultType)
            {
                if (!this.ExpectedResultForPropTypeAndUwResultTypeCheck.HasValue)
                {
                    return true;
                }

                bool expectedResult = this.ExpectedResultForPropTypeAndUwResultTypeCheck.Value;

                bool checkTypes = propertyType == E_sProdSpT.Manufactured &&
                                  (resultType == E_sProd3rdPartyUwResultT.DU_ApproveEligible || resultType == E_sProd3rdPartyUwResultT.DU_ApproveIneligible ||
                                   resultType == E_sProd3rdPartyUwResultT.DU_ReferEligible || resultType == E_sProd3rdPartyUwResultT.DU_ReferIneligible ||
                                   resultType == E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible || resultType == E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible);

                return checkTypes == expectedResult;
            }
        }
    }

}
