/// Author: David Dao

using System;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice
{
    public class CInvalidRateVersionException : CBaseException 
    {

        private Guid m_sLId;
        public override string EmailSubjectCode 
        {
            get { return "INVALID_RATE_VERSION - " + m_sLId; }
        }
        public string RateOptionChange { get; set; }

        public CInvalidRateVersionException(string expectedVersion, string actualVersion, Guid sLId, DateTime resultDisplayTime) : 
            base(ErrorMessages.InvalidRateOptionVersion, "Invalid Rate Version. Expected=" + expectedVersion + ", Actual Version=" + actualVersion) 
        {
            Init(expectedVersion, actualVersion, sLId, resultDisplayTime);
            RateOptionChange = string.Empty;
            IsEmailDeveloper = false;
        }

        public CInvalidRateVersionException(string expectedVersion, string actualVersion, Guid sLId, DateTime resultDisplayTime, string rateOptionChange) :
            base(ErrorMessages.InvalidRateOptionVersion, "Invalid Rate Version. Expected=" + expectedVersion + ", Actual Version=" + actualVersion)
        {
            Init(expectedVersion, actualVersion, sLId, resultDisplayTime);
            RateOptionChange = rateOptionChange;
            IsEmailDeveloper = false;
        }


        private void Init(string expectedVersion, string actualVersion, Guid sLId, DateTime resultDisplayTime) 
        {
            m_sLId = sLId;

            TimeSpan ts = DateTime.Now.Subtract(resultDisplayTime);

            string duration = string.Format("{0} hr {1} min {2} secs", ts.Hours, ts.Minutes, ts.Seconds);

            this.DeveloperMessage = string.Format("Invalid Rate Version. Expected=[{0}], Actual Version=[{1}].{4} Result First Display={2}.{4} Interval From First To Exception is {3}", 
                expectedVersion, actualVersion, resultDisplayTime, duration, Environment.NewLine);
        }


    }
}
