﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using DataAccess;
using LendersOffice.RatePrice;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice
{
    public class CApplicantRateOption
    {
        private string m_rate;
        private string m_point;
        private string m_apr;
        private string m_firstPmtAmt;
        private decimal m_pointVal;
        private string m_margin;
        private string m_term;
        private string m_due;
        //private string m_topRatio;
        private string m_bottomRatio;
        private Guid m_lLpTemplateId;
        private string m_lLpTemplateNm;
        private string m_rawRate;
        private string m_rawPoint;

        // Temporary variables
        private bool m_isRateChange = false;
        private string m_oldRate;
        private string m_oldPoint;
        private bool m_markedAsDelete = false;
        private CApplicantPriceXml m_applicantPrice = null;
        // End temporary variables

        private string m_qrate;
        private string m_teaserRate;
        private string m_qualPmt;
        private string m_perOptionAdjStr = ""; // OPM 87119 - So cert can list which per-rate adjustments apply to this one
        private string m_perOptionAdjHiddenStr = "";
        private string m_disqualReason = "";
        private string m_disqualReasonDebug = "";
        private string m_reserves = ""; // opm 32222 - Purchase money scenarios will have a new "RESERVES" column.
        private string m_breakEvenPoint = ""; // opm 32222 - Refinance money scenarios will have a new “BREAK EVEN POINT” column.
        private string m_prepaidCharges = ""; // opm 108720 - Now including prepaid charges as part of the closing cost breakdown
        private string m_nonPrepaidCharges = ""; // opm 108720 - Now including non-prepaid charges as part of the closing cost breakdown
        private string m_closingCost = ""; // opm 32222 - Closing costs column is determined using the automated GFE less seller credits (for purchase money).
        private bool m_isWinner = false;
        private string m_disableLockReason = ""; // OPM 130497.
        private int m_numberOfCompetitors = 0;

        private QMPricingFields m_QMData = null;

        public CApplicantRateOption(Guid lLpTemplateId, string lLpTemplateNm, string lpInvestorNm, string rawRate, string rawPoint, string rate, string point,
            decimal pointVal, string apr, string firstPmtAmt, string margin, string term, string due, /*string topRatio,*/ string bottomRatio,
            string qrate, string teaserRate, string qualPmt, decimal bankAmount, decimal originatorComp, decimal originatorBasePoint)
        {
            m_lLpTemplateId = lLpTemplateId;
            m_lLpTemplateNm = lLpTemplateNm;
            this.LpInvestorNm = lpInvestorNm;
            m_rate = rate.Replace("%", "");
            m_point = point.Replace("%", "");
            m_rawRate = rawRate.Replace("%", "");
            m_rawPoint = rawPoint.Replace("%", "");
            m_pointVal = pointVal;
            m_apr = apr.Replace("%", "");
            m_firstPmtAmt = firstPmtAmt.Replace("$", "");
            m_margin = margin.Replace("%", "");
            m_term = term;
            m_due = due;
            //m_topRatio = topRatio.Replace("%", "");
            m_bottomRatio = bottomRatio.Replace("%", "");

            m_qrate = qrate.Replace("%", "");
            m_teaserRate = teaserRate.Replace("%", "");
            m_qualPmt = qualPmt.Replace("%", "");

            this.BankAmount = bankAmount;
            // 4/28/2011 dd - Need to round OriginatorComp to 3 decimal places otherwise PointIncludingOriginatorComp_rep will not be correct in certain scenario.
            OriginatorComp = Math.Round(originatorComp, 3);
            this.OriginatorBasePoint = originatorBasePoint;
            m_closingCostBreakdown = new List<RespaFeeItem>();
        }

        /// <summary>
        /// // 1/6/2014 dd - 
        /// This is the raw point + hidden adjustment. Base Point is what display to originator and will be use to determine market change in lock policy.
        /// </summary>
        public decimal OriginatorBasePoint { get; private set; }

        /// <summary>
        /// Only accurate when the rate option is a winner. Only populated by rate merge code.
        /// </summary>
        public int NumberOfRateMergeCompetitors
        {
            get { return m_numberOfCompetitors; }
            set { m_numberOfCompetitors = value; }
        }

        public QMPricingFields QMData
        {
            get { return m_QMData; }
            set { m_QMData = value; }
        }


        /// <summary>
        /// Populated by rate merge code.
        /// </summary>
        public bool IsRateMergeWinner
        {
            get
            {
                return m_isWinner;
            }

            set
            {
                m_isWinner = value;
            }
        }
        public string TotalPriceAdj_rep
        {
            get
            {
                if (m_applicantPrice != null)
                {
                    decimal total = 0;
                    if (decimal.TryParse(m_applicantPrice.TotalFeeDelta_rep.Replace("%", ""), out total))
                    {
                        // 7/28/2011 dd - Total Price is opposite with Fee. (see OPM 67783 entry 7/28)
                        total = -1 * total;
                        return total.ToString("+0.000;-0.000");
                    }

                }
                return string.Empty;
            }
        }
        public decimal BankAmount { get; private set; }
        public string BankAmount_rep
        {
            get { return string.Format("{0:N2}", Math.Round(BankAmount, 2)); }
        }
        public decimal OriginatorComp { get; private set; }
        public string OriginatorComp_rep
        {
            get { return string.Format("{0:N3}", Math.Round(OriginatorComp, 3)); }
        }

        public CApplicantPriceXml ApplicantPrice
        {
            get { return m_applicantPrice; }
        }
        public void SetApplicantPrice(CApplicantPriceXml applicantPrice)
        {
            m_applicantPrice = applicantPrice;
        }
        public bool MarkedAsDelete
        {
            get { return m_markedAsDelete; }
            set { m_markedAsDelete = value; }
        }
        public int ApplicantPriceTempIndex
        {
            get
            {
                if (null == m_applicantPrice)
                    return -1;

                return m_applicantPrice.TempIndex;
            }
        }
        public string MaxDti_rep
        {
            get
            {
                if (null == m_applicantPrice)
                {
                    return "";
                }
                return m_applicantPrice.MaxDti_rep;
            }
        }
        public bool IsBlockedRateLockSubmission
        {
            get
            {
                if (null == m_applicantPrice)
                {
                    return false;
                }
                return m_applicantPrice.IsBlockedRateLockSubmission;
            }
        }
        public bool IsRateExpired
        {
            get
            {
                if (null == m_applicantPrice)
                {
                    return false;
                }
                return m_applicantPrice.AreRatesExpired;
            }
        }

        public bool IsViolateMaxDti
        {
            get
            {
                if (null == m_applicantPrice)
                    return false;

                if ("" == m_applicantPrice.MaxDti_rep || "" == m_bottomRatio)
                    return false;

                try
                {
                    decimal maxDti = decimal.Parse(m_applicantPrice.MaxDti_rep.Replace("%", ""));
                    decimal dti = decimal.Parse(m_bottomRatio.Replace("%", ""));
                    return dti > maxDti;
                }
                catch { }

                return false;
            }
        }
        public bool IsRateChange
        {
            get { return m_isRateChange; }
            set { m_isRateChange = value; }
        }
        public string OldRate
        {
            get { return m_oldRate; }
            set { m_oldRate = value; }
        }
        public string OldPoint
        {
            get { return m_oldPoint; }
            set { m_oldPoint = value; }
        }

        /// <summary>
        /// Gets the raw rate of the rate option. 
        /// </summary>
        public string RawRate_rep
        {
            get
            {
                return m_rawRate;
            }
        }

        public string RateOptionId
        {
            get
            {
                return string.Format("{0},{1}", m_rawRate, m_rawPoint);
            }
        }

        public string RequestedRateOption
        {
            get { return string.Format($"{Rate}:{Point}:{RateOptionId}"); }
        }

        public string Rate_rep { get { return m_rate; } }
        public decimal Rate
        {
            get
            {
                try
                {
                    return decimal.Parse(m_rate);
                }
                catch { }
                return 0.0M;
            }
        }

        public string Point_rep { get { return m_point; } }
        public decimal Point_
        {
            // Not sure what is the different between m_point and pointVal.
            get
            {
                try
                {
                    return decimal.Parse(m_point);
                }
                catch { }
                return 0.0M;
            }
        }
        public decimal PointIn100
        {
            get { return 100 - Point_; }
        }
        public string PointIn100_rep { get { return string.Format("{0:N3}", PointIn100); } }

        public string Apr_rep { 
            get { return m_apr; }
            set { m_apr = value;}
        
        }
        public string FirstPmtAmt_rep { get { return m_firstPmtAmt; } }
        public string Margin_rep { get { return m_margin; } }
        public decimal Margin
        {
            get
            {
                try
                {
                    return decimal.Parse(m_margin);
                }
                catch { }
                return 0.0M;
            }
        }

        public string QRate_rep
        {
            get { return m_qrate; }
        }
        public decimal QRate
        {
            get
            {
                // OPM 108042. This one is most often blank.  Exiting early faster then attempting to parse and hitting format exception.
                if (m_qrate.Length == 0) return 0.0M;

                try
                {
                    return decimal.Parse(m_qrate);
                }
                catch { }
                return 0.0M;
            }
        }

        public string QualPmt_rep
        {
            get { return m_qualPmt; }
        }
        public string TeaserRate_rep
        {
            get { return m_teaserRate; }
        }

        public decimal TeaserRate
        {
            get
            {
                try
                {
                    return decimal.Parse(m_teaserRate);
                }
                catch { }
                return 0.0M;
            }
        }

        public bool IsDisqualified
        {
            get { return !String.IsNullOrEmpty(m_disqualReason); }
        }

        public string DisqualReason
        {
            get { return m_disqualReason; }
            set { m_disqualReason = value; }
        }

        public string DisqualReasonDebug
        {
            get { return m_disqualReasonDebug; }
            set { m_disqualReasonDebug = value; }
        }

        public bool IsDisableLock
        {
            get { return m_disableLockReason != string.Empty; }
        }

        public string DisableLockReason
        {
            get { return m_disableLockReason; }
            set { m_disableLockReason = value; }
        }

        public string PerOptionAdjStr
        {
            get { return m_perOptionAdjStr ?? ""; }
            set { m_perOptionAdjStr = value; }
        }

        public HashSet<String> PerOptionCodes
        {
            get 
            { 
                HashSet<string> opcodes = new HashSet<string>();
                foreach (char a in PerOptionAdjStr)
                {
                    opcodes.Add(a.ToString());
                }

                foreach (char a in PerOptionAdjHiddenStr)
                {
                    opcodes.Add(a.ToString());
                }

                return opcodes;
            }
        }

        public string PerOptionAdjHiddenStr
        {
            get { return m_perOptionAdjHiddenStr ?? ""; }
            set { m_perOptionAdjHiddenStr = value; }
        }

        public string Reserves
        {
            get { return m_reserves; }
            set { m_reserves = value; }
        }

        public string BreakEvenPoint
        {
            get { return m_breakEvenPoint; }
            set { m_breakEvenPoint = value; }
        }

        public string PrepaidCharges
        {
            get { return m_prepaidCharges; }
            set { m_prepaidCharges = value; }
        }

        public string NonPrepaidCharges
        {
            get { return m_nonPrepaidCharges; }
            set { m_nonPrepaidCharges = value; }
        }

        public string ClosingCost
        {
            get { return m_closingCost; }
            set { m_closingCost = value; }
        }

        public decimal ClosingCostTemp
        {
            get;
            set;
        }
        

        private List<RespaFeeItem> m_closingCostBreakdown;
        public List<RespaFeeItem> ClosingCostBreakdown
        {
            get { return m_closingCostBreakdown; }
        }

        // TODO: This bit is very ugly, clean it up.  Refactor. Better OOP is that each rateoption will know its "Display Point"
        // Only done this way for time, and because did not have whole design at initial implementation.
        public decimal PointIncludingOriginatorComp
        {
           get { return Point_ + OriginatorComp; }
        }

        public string PointIncludingOriginatorComp_rep
        {
            // 64253 
            get { return Math.Round(PointIncludingOriginatorComp, 3).ToString(); }
        }

        public decimal PointIncludingOriginatorCompIn100
        {
            get { return 100 - (Point_ + OriginatorComp); }
        }

        public string PointIncludingOriginatorCompIn100_rep
        {
            // 64253 
            get { return Math.Round(PointIncludingOriginatorCompIn100, 3).ToString(); }

        }

        public decimal PointLessOriginatorComp
        {
            //64256 
            get { return Point_ - OriginatorComp; }
        }
        public string PointLessOriginatorComp_rep
        {
            // 64253 
            get { return Math.Round(PointLessOriginatorComp, 3).ToString(); }

        }
        public decimal PointLessOriginatorCompIn100
        {
            //64256 
            get { return 100 - (Point_ - OriginatorComp); }
        }
        public string PointLessOriginatorCompIn100_rep
        {
            // 64253 
            get { return Math.Round(PointLessOriginatorCompIn100, 3).ToString(); }

        }


        public string Term_rep { get { return m_term; } }
        public string Due_rep { get { return m_due; } }
        //public string TopRatio_rep { get { return m_topRatio; } }
        public string BottomRatio_rep { get { return m_bottomRatio; } }
        internal decimal Point { get { return m_pointVal; } }
        public static string PointFeeInResult(bool displayPmlFeeIn100Format, string point)
        {
            try
            {
                if (displayPmlFeeIn100Format)
                {
                    decimal v = decimal.Parse(point.Replace("%", ""));
                    return string.Format("{0:N3}", 100 - v);
                }
            }
            catch { }
            return point;
        }

        public void TemporaryIncreasePoint()
        {
            try
            {
                m_point = (decimal.Parse(m_point) + .05M).ToString();
            }
            catch { }
        }

        public void SetToParRate(decimal appliedComp)
        {
            m_pointVal = -1 * appliedComp;
            m_point = string.Format("{0:N3}", Math.Round(m_pointVal, 3));
        }

        // 10/12/2009 dd - Add extra cap information for Google pricing
        #region Extra cap information for Google Pricing
        public string lRadj1stCapMon_rep { get; private set; }
        public string lRadj1stCapR_rep { get; private set; }
        public string lRAdjCapMon_rep { get; private set; }
        public decimal lRAdjCapR
        {
            get
            {
                decimal v = 0;
                decimal.TryParse(lRAdjCapR_rep, out v);

                return v;
            }
        }
        public string lRAdjCapR_rep { get; private set; }
        public decimal lRAdjLifeCapR
        {
            get
            {
                decimal v = 0;
                decimal.TryParse(lRAdjLifeCapR_rep, out v);

                return v;

            }
        }
        public string lRAdjLifeCapR_rep { get; private set; }
        public string lArmIndexNameVstr { get; private set; }
        public void SetArmCapInformation(string _lRadj1stCapMon_rep, string _lRadj1stCapR_rep, string _lRAdjCapMon_rep, string _lRAdjCapR_rep, string _lRAdjLifeCapR_rep, string _lArmIndexNameVstr)
        {
            lRadj1stCapMon_rep = _lRadj1stCapMon_rep;
            lRadj1stCapR_rep = _lRadj1stCapR_rep;
            lRAdjCapMon_rep = _lRAdjCapMon_rep;
            lRAdjCapR_rep = _lRAdjCapR_rep;
            lRAdjLifeCapR_rep = _lRAdjLifeCapR_rep;
            lArmIndexNameVstr = _lArmIndexNameVstr;
        }
        #endregion

        public Guid LpTemplateId { get { return m_lLpTemplateId; } }
        public string LpTemplateNm { get { return m_lLpTemplateNm; } }
        public string LpInvestorNm { get; private set; }

        public decimal GetPoint(E_OriginatorCompPointSetting setting)
        {
            switch (setting)
            {
                case E_OriginatorCompPointSetting.UsePoint:
                    return Point;
                case E_OriginatorCompPointSetting.UsePointLessOriginatorComp:
                    return PointLessOriginatorComp;
                case E_OriginatorCompPointSetting.UsePointWithOriginatorComp:
                    return PointIncludingOriginatorComp;
                default:
                    throw new UnhandledEnumException(setting);
            }
        }

        public string GetPoint_rep(E_OriginatorCompPointSetting setting)
        {
            return Math.Round(GetPoint(setting), 3).ToString(); 
        }

        #region ComparisonFields
        // Per OPM 32222 These fields need to be pulled from the XML for Comparison Report Cert

        //Base Loan Amount (sLAmtCalcPe)
        public string sLAmtCalcPe { get; set; }
        
        //Upfront MIP / Funding Fee Financed (sFfUfmipFinanced)
        public string sFfUfmipFinanced { get; set; }

        //Total Loan Amount (sFinalLAmt)
        public string sFinalLAmt { get; set; }

        //Impound Taxes&Insurance (sProdImpound)
        public string sProdImpound { get; set; }

        //Mortgage Insurance (sProMIns)
        public string sProMIns { get; set; }

        //Total Cash to Close(Down Payment + Borrower Paid Closing Costs)
        public string CashToClose { get; set; }

        //Estimated Reserves Dollars (Total liquid assets – down payment – closing costs)
        public string EstimatedReservesDollars { get; set; }

        //Monthly PITIA Savings (Current PITIA Payment – sMonthlyPmt )
        public string PitiaSavings { get; set; }

        //Months to Recoup Closing Costs ( Closing Costs / (Current PITIA Payment – sMonthlyPmt ) )
        public string RecoupClosingCostsMonths { get; set; }

        //Lifetime Finance Charge (sFinCharge)
        public string sFinCharge { get; set; }

        //Principal Paid (Sum of principal payments over horizon term.)
        public string PrincipalPaidOverHorizon { get; set; }
        //Finance Charge Paid (Sum of interest payments, mortgage insurance, and closing costs over horizon term)
        public string FinanceChargesPaidOverHorizon { get; set; }
        //Remaining Balance (principal balance at end of horizon term)
        public string RemainingBalanceAfterHorizon { get; set; }
        //Savings Versus Current Loan
        public string SavingsVersusCurrentLoan { get; set; }

        //Current PITI Payment
        public string CurrentPITIPayment { get; set; }

        // sMonthlyPmtPe
        public string sMonthlyPmtPe { get; set; }

        public string sProRealETx { get; set; }
        public string sProHazIns { get; set; }
        public string sProHoAssocDues { get; set; }
        public string sProOHExp { get; set; }

        #endregion

        // OPM 233679.
        public RateOptionLoanData LoanData { get; set; }

        public XmlElement ToXmlElement(XmlDocument doc)
        {
            XmlElement el = doc.CreateElement("CApplicantRateOption");
            el.SetAttribute("Rate", m_rate);
            el.SetAttribute("Point", m_point);
            el.SetAttribute("Apr", m_apr);
            el.SetAttribute("FirstPmtAmt", m_firstPmtAmt);
            el.SetAttribute("Margin", m_margin);
            el.SetAttribute("Term", m_term);
            el.SetAttribute("Due", m_due);
            //el.SetAttribute("TopRatio", m_topRatio);
            el.SetAttribute("BottomRatio", m_bottomRatio);
            el.SetAttribute("PointVal", m_pointVal.ToString());
            el.SetAttribute("LpTemplateId", m_lLpTemplateId.ToString());
            el.SetAttribute("LpTemplateNm", m_lLpTemplateNm.ToString());
            el.SetAttribute("LpInvestorNm", LpInvestorNm);
            el.SetAttribute("RawRate", m_rawRate);
            el.SetAttribute("RawPoint", m_rawPoint);
            el.SetAttribute("QRate", m_qrate);
            el.SetAttribute("TeaserRate", m_teaserRate);
            el.SetAttribute("QualPmt", m_qualPmt);
            el.SetAttribute("BankAmount", BankAmount_rep);
            el.SetAttribute("OriginatorComp", OriginatorComp_rep);
            el.SetAttribute("DisqualReason", m_disqualReason);
            el.SetAttribute("DisqualReasonDebug", m_disqualReasonDebug);
            el.SetAttribute("OptionAdjusts", m_perOptionAdjStr);
            el.SetAttribute("OptionAdjustsHidden", m_perOptionAdjHiddenStr);
            el.SetAttribute("BreakEvenPoint", m_breakEvenPoint);
            el.SetAttribute("Reserves", m_reserves);
            el.SetAttribute("PrepaidCharges", m_prepaidCharges);
            el.SetAttribute("NonPrepaidCharges", m_nonPrepaidCharges);
            el.SetAttribute("ClosingCost", m_closingCost);
            el.SetAttribute("sLAmtCalcPe", sLAmtCalcPe  );
            el.SetAttribute("sFfUfmipFinanced", sFfUfmipFinanced );
            el.SetAttribute("sFinalLAmt", sFinalLAmt );
            el.SetAttribute("sProdImpound", sProdImpound );
            el.SetAttribute("sProMIns", sProMIns );
            el.SetAttribute("CashToClose", CashToClose );
            el.SetAttribute("EstimatedReservesDollars", EstimatedReservesDollars );
            el.SetAttribute("PitiaSavings", PitiaSavings );
            el.SetAttribute("RecoupClosingCostsMonths", RecoupClosingCostsMonths );
            el.SetAttribute("sFinCharge", sFinCharge );
            el.SetAttribute("PrincipalPaidOverHorizon", PrincipalPaidOverHorizon );
            el.SetAttribute("FinanceChargesPaidOverHorizon", FinanceChargesPaidOverHorizon );
            el.SetAttribute("RemainingBalanceAfterHorizon", RemainingBalanceAfterHorizon );
            el.SetAttribute("SavingsVersusCurrentLoan", SavingsVersusCurrentLoan);
            el.SetAttribute("sMonthlyPmtPe", sMonthlyPmtPe);
            el.SetAttribute("CurrentPITIPayment", CurrentPITIPayment);
            el.SetAttribute("OriginatorBasePoint", OriginatorBasePoint.ToString());
            el.SetAttribute("sProRealETx", sProRealETx);
            el.SetAttribute("sProHazIns", sProHazIns);
            el.SetAttribute("sProHoAssocDues", sProHoAssocDues);
            el.SetAttribute("sProOHExp", sProOHExp);

            if (QMData != null)
            {
                el.SetAttribute("QMData", ObsoleteSerializationHelper.JavascriptJsonSerialize(QMData));
            }
            el.SetAttribute("DisableLockReason", m_disableLockReason);
            if (LoanData != null)
            {
                el.SetAttribute("LoanData", ObsoleteSerializationHelper.JavascriptJsonSerialize(LoanData));
            }


            XmlElement closingCostBreakdown = doc.CreateElement("ClosingCostBreakdown");
            foreach (var cost in m_closingCostBreakdown)
            {
                XmlElement costElement = doc.CreateElement("ClosingCost");
                costElement.SetAttribute("HudLine", cost.HudLine);
                costElement.SetAttribute("Description", cost.Description);
                costElement.SetAttribute("Fee", cost.Fee.ToString());
                costElement.SetAttribute("GfeSectionT", cost.GfeSectionT.ToString("D"));
                costElement.SetAttribute("PaidTo", cost.PaidTo);
                costElement.SetAttribute("Source", cost.Source.ToString("D"));
                costElement.SetAttribute("LoanSec", Enum.GetName(typeof(E_IntegratedDisclosureSectionT), cost.DisclosureSectionT));
                closingCostBreakdown.AppendChild(costElement);
            }
            el.AppendChild(closingCostBreakdown);
            return el;
        }

        public static CApplicantRateOption Parse(XmlElement el)
        {
            string rate = el.GetAttribute("Rate");
            string point = el.GetAttribute("Point");
            string apr = el.GetAttribute("Apr");
            string firstPmtAmt = el.GetAttribute("FirstPmtAmt");
            string margin = el.GetAttribute("Margin");
            string term = el.GetAttribute("Term");
            string due = el.GetAttribute("Due");
            //string topRatio = el.GetAttribute("TopRatio");
            string bottomRatio = el.GetAttribute("BottomRatio");
            string rawRate = el.GetAttribute("RawRate");
            string rawPoint = el.GetAttribute("RawPoint");
            string qrate = el.GetAttribute("QRate");
            string teaserRate = el.GetAttribute("TeaserRate");
            string qualPmt = el.GetAttribute("QualPmt");
            decimal pointVal = 0.0M;
            try
            {
                pointVal = decimal.Parse(el.GetAttribute("PointVal"));
            }
            catch { }
            Guid lpTemplateId = new Guid(el.GetAttribute("LpTemplateId"));
            string lpTemplateNm = el.GetAttribute("LpTemplateNm");
            string lpInvestorNm = el.GetAttribute("LpInvestorNm");
            decimal bankAmount = 0.0M;
            if (decimal.TryParse(el.GetAttribute("BankAmount"), out bankAmount) == false) 
            {
                bankAmount = 0.0M;
            }

            decimal originatorComp = 0.0M;
            if (decimal.TryParse(el.GetAttribute("OriginatorComp"), out originatorComp) == false)
            {
                originatorComp = 0.0M;
            }

            string disqualReason = el.GetAttribute("DisqualReason");
            string disqualReasonDebug = el.GetAttribute("DisqualReasonDebug");
            
            string OptionAdjusts = el.GetAttribute("OptionAdjusts");
            string OptionAdjustsHidden = el.GetAttribute("OptionAdjustsHidden");
            string BreakEvenPoint = el.GetAttribute("BreakEvenPoint");
            string Reserves = el.GetAttribute("Reserves");
            string PrepaidCharges = el.GetAttribute("PrepaidCharges");
            string NonPrepaidCharges = el.GetAttribute("NonPrepaidCharges");
            string ClosingCost = el.GetAttribute("ClosingCost");

            var closingCostBreakdownNodes = el.SelectNodes("ClosingCostBreakdown/ClosingCost");
            List<RespaFeeItem> ClosingCostBreakdown = new List<RespaFeeItem>();
            foreach (XmlElement node in closingCostBreakdownNodes)
            {
                string hudline = node.GetAttribute("HudLine");
                string description = node.GetAttribute("Description");
                string fee = node.GetAttribute("Fee");
                string gfeSectionT = node.GetAttribute("GfeSectionT");
                string paidTo = node.GetAttribute("PaidTo");
                string source = node.GetAttribute("Source");
                E_IntegratedDisclosureSectionT loanSec = (E_IntegratedDisclosureSectionT)Enum.Parse(typeof(E_IntegratedDisclosureSectionT), node.GetAttribute("LoanSec"), true);
                ClosingCostBreakdown.Add(
                    new RespaFeeItem(
                        hudline,
                        description,
                        decimal.Parse(fee),
                        (E_GfeSectionT) Enum.Parse(typeof(E_GfeSectionT), gfeSectionT),
                        0,
                        paidTo,
                        (E_ClosingFeeSource) Enum.Parse(typeof(E_ClosingFeeSource), source),
                        loanSec
                        ));
            }

            string sLAmtCalcPe = el.GetAttribute("sLAmtCalcPe");
            string sFfUfmipFinanced = el.GetAttribute("sFfUfmipFinanced");
            string sFinalLAmt = el.GetAttribute("sFinalLAmt");
            string sProdImpound = el.GetAttribute("sProdImpound");
            string sProMIns = el.GetAttribute("sProMIns");
            string CashToClose = el.GetAttribute("CashToClose");
            string EstimatedReservesDollars = el.GetAttribute("EstimatedReservesDollars");
            string PitiaSavings = el.GetAttribute("PitiaSavings");
            string RecoupClosingCostsMonths = el.GetAttribute("RecoupClosingCostsMonths");
            string sFinCharge = el.GetAttribute("sFinCharge");
            string PrincipalPaidOverHorizon = el.GetAttribute("PrincipalPaidOverHorizon");
            string FinanceChargesPaidOverHorizon = el.GetAttribute("FinanceChargesPaidOverHorizon");
            string RemainingBalanceAfterHorizon = el.GetAttribute("RemainingBalanceAfterHorizon");
            string SavingsVersusCurrentLoan = el.GetAttribute("SavingsVersusCurrentLoan");
            string sMonthlyPmtPe = el.GetAttribute("sMonthlyPmtPe");
            string CurrentPITIPayment = el.GetAttribute("CurrentPITIPayment");
            string disableLockReason = el.GetAttribute("DisableLockReason");
            string sProRealETx = el.GetAttribute("sProRealETx");
            string sProHazIns = el.GetAttribute("sProHazIns");
            string sProHoAssocDues = el.GetAttribute("sProHoAssocDues");
            string sProOHExp = el.GetAttribute("sProOHExp");

            QMPricingFields data = null;
            if (el.HasAttribute("QMData"))
            {
                data = ObsoleteSerializationHelper.JavascriptJsonDeserializer<QMPricingFields>(el.GetAttribute("QMData"));
            }

            RateOptionLoanData loanData = null;
            if (el.HasAttribute("LoanData"))
            {
                loanData = ObsoleteSerializationHelper.JavascriptJsonDeserializer<RateOptionLoanData>(el.GetAttribute("LoanData"));
            }

            decimal originatorBasePoint = SafeParseDecimal(el, "OriginatorBasePoint"); 

            return new CApplicantRateOption(lpTemplateId, lpTemplateNm, lpInvestorNm, rawRate, rawPoint, rate, point, pointVal, apr, firstPmtAmt, margin, 
                term, due, /*topRatio, */bottomRatio, qrate, teaserRate, qualPmt, bankAmount, originatorComp, originatorBasePoint) 
            { DisqualReason = disqualReason, PerOptionAdjStr = OptionAdjusts, PerOptionAdjHiddenStr = OptionAdjustsHidden, Reserves = Reserves, BreakEvenPoint = BreakEvenPoint, ClosingCost = ClosingCost,
              sLAmtCalcPe = sLAmtCalcPe
              , sFfUfmipFinanced = sFfUfmipFinanced
              , sFinalLAmt = sFinalLAmt
              , sProdImpound = sProdImpound
              , sProMIns = sProMIns
              , CashToClose = CashToClose 
              , EstimatedReservesDollars = EstimatedReservesDollars
              , PitiaSavings = PitiaSavings
              , RecoupClosingCostsMonths = RecoupClosingCostsMonths
              , sFinCharge = sFinCharge
              , PrincipalPaidOverHorizon = PrincipalPaidOverHorizon
              , FinanceChargesPaidOverHorizon = FinanceChargesPaidOverHorizon
              , RemainingBalanceAfterHorizon = RemainingBalanceAfterHorizon
              , SavingsVersusCurrentLoan =  SavingsVersusCurrentLoan
              , sMonthlyPmtPe = sMonthlyPmtPe
              , CurrentPITIPayment = CurrentPITIPayment
              , m_closingCostBreakdown = ClosingCostBreakdown
              , m_prepaidCharges = PrepaidCharges
              , m_nonPrepaidCharges = NonPrepaidCharges
              , DisqualReasonDebug = disqualReasonDebug
              , QMData = data
              , m_disableLockReason = disableLockReason
              , LoanData = loanData
              , sProRealETx = sProRealETx
              , sProHazIns = sProHazIns
              , sProHoAssocDues = sProHoAssocDues
              , sProOHExp = sProOHExp
            };
        }

        private static decimal SafeParseDecimal(XmlElement el, string attrName)
        {
            decimal v = 0;

            if (decimal.TryParse(el.GetAttribute(attrName), out v) == false)
            {
                return 0;
            }
            return v;
        }
    }

}
