﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.RatePrice.FileBasedPricing;
using ClosedXML.Excel;
using System.IO;

namespace LendersOffice.RatePrice
{
    public static class LoanProgramTemplateExport
    {
        private class __LoanProductFolderItem
        {
            public Guid FolderId { get; set; }
            public string FolderName { get; set; }
            public Guid ParentFolderId { get; set; }
            public string FullName { get; set; }


        }

        private class __LoanProgramTemplateItem
        {
            public Guid lLpTemplateId { get; set; }
            public Guid FolderId { get; set; }
            public string FolderFullName { get; set; }
            public string lLpTemplateNm { get; set; }
            public bool IsEnabled { get; set; }

            public E_sLT lLT { get; set; }
            public int lTerm { get; set; }
            public int lDue { get; set; }
            public E_sFinMethT lFinMethT { get; set; }
            public bool IsMaster { get; set; }
            public Guid lBaseLpId { get; set; }
            public string Type
            {
                get
                {
                    if (IsMaster)
                    {
                        return lBaseLpId == Guid.Empty ? "Master" : "N/A";
                    }
                    else
                    {
                        return lBaseLpId == Guid.Empty ? "" : "Derived";
                    }
                }
            }

            public string ProductCode { get; set; }
            public string lLpProductType { get; set; }
            public string lLpInvestorNm { get; set; }
            public string lLpCustomCode1 { get; set; }
            public string lLpCustomCode2 { get; set; }
            public string lLpCustomCode3 { get; set; }
            public string lLpCustomCode4 { get; set; }
            public string lLpCustomCode5 { get; set; }
            public string lLpInvestorCode1 { get; set; }
            public string lLpInvestorCode2 { get; set; }
            public string lLpInvestorCode3 { get; set; }
            public string lLpInvestorCode4 { get; set; }
            public string lLpInvestorCode5 { get; set; }
            public string lLpTemplateNmInherit { get; set; }
            public bool lLpTemplateNmOverrideBit { get; set; }
            public bool ProductCodeOverrideBit { get; set; }
            public decimal lLpeFeeMax { get; set; }
            public decimal lRateDelta { get; set; }
            public decimal lFeeDelta { get; set; }
            public DateTime CreatedD { get; set; }
            public E_sLienPosT lLienPosT { get; set; }
            public bool CanBeStandAlone2nd { get; set; }
            public int lLockedDays { get; set; }
            public bool lRateSheetxmlContentOverrideBit { get; set; }
            public string lRateOptionBaseId { get; set; }
            public bool lLpmiSupportedOutsidePmi { get; set; }
            public bool lLpmiSupportedOutsidePmiOverrideBit { get; set; }
            public E_sLpDPmtT lLpDPmtT { get; set; }
            public E_sLpQPmtT lLpQPmtT { get; set; }
            public bool lHasQRateInRateOptions { get; set; }
            public int lRadj1stCapMon { get; set; }
            public int lRAdjCapMon { get; set; }
            public decimal lRadj1stCapR { get; set; }
            public decimal lRAdjCapR { get; set; }
            public decimal lRAdjLifeCapR { get; set; }
            public E_sRAdjFloorBaseT lRAdjFloorBaseT { get; set; }
            public decimal lRAdjFloorR { get; set; }
            public E_sRAdjRoundT lRAdjRoundT { get; set; }
            public decimal lRAdjRoundToR { get; set; }
            public string lArmIndexNameVstr { get; set; }
            public int lHardPrepmtPeriodMonths { get; set; }
            public int lSoftPrepmtPeriodMonths { get; set; }
            public int lIOnlyMon { get; set; }
            public int lLateDays { get; set; }
            public string lLateChargePc { get; set; }
            public string lLateChargeBaseDesc { get; set; }
            public E_sPrepmtPenaltyT lPrepmtPenaltyT { get; set; }
            public E_sPrepmtRefundT lPrepmtRefundT { get; set; }
            public E_sAssumeLT lAssumeLT { get; set; }
            public QualRateCalculationT lQualRateCalculationT { get; set; }
            public QualRateCalculationFieldT lQualRateCalculationFieldT1 { get; set; }
            public QualRateCalculationFieldT lQualRateCalculationFieldT2 { get; set; }
            public decimal lQualRateCalculationAdjustment1 { get; set; }
            public decimal lQualRateCalculationAdjustment2 { get; set; }

            // from RateOption
            public string LpeAcceptableRsFileId { get; set; }

            public bool lHelocCalculatePrepaidInterest { get; set; }

            public bool IsNonQmProgram { get; set; }
            public bool IsDisplayInNonQmQuickPricer { get; set; }

            public int lQualTerm { get; set; }

            public QualTermCalculationType lQualTermCalculationType { get; set; }
        }

        public static byte[] ExportToExcel(Guid brokerId, Guid folderId, string type)
        {

            Dictionary<Guid, __LoanProductFolderItem> folderDictionary = new Dictionary<Guid, __LoanProductFolderItem>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@IsLpe", true)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListLoanProductFolderByBrokerID", parameters))
            {
                while (reader.Read())
                {
                    __LoanProductFolderItem item = new __LoanProductFolderItem();
                    item.FolderId = reader.SafeGuid("FolderId");
                    item.FolderName = (string)reader["FolderName"];
                    item.ParentFolderId = reader.SafeGuid("ParentFolderId");

                    folderDictionary.Add(item.FolderId, item);

                }
            }

            DependencySet<Guid> pricePolicyDependencySet = new DependencySet<Guid>(new GuidComparer());

            foreach (var o in folderDictionary)
            {
                pricePolicyDependencySet.Add(o.Value.ParentFolderId, o.Value.FolderId);
                // Compute Full Name
                o.Value.FullName = ComputeFullName(folderDictionary, o.Key);
            }

            HashSet<Guid> allowableFolderSet = new HashSet<Guid>(pricePolicyDependencySet.GetDependsOn(new Guid[] { folderId }));
            allowableFolderSet.Add(folderId);

            List<__LoanProgramTemplateItem> programList = new List<__LoanProgramTemplateItem>();

            FillProgramList(brokerId, folderDictionary, allowableFolderSet, programList);

            #region Export To Excel

            if (type.Equals("batchrename", StringComparison.OrdinalIgnoreCase))
            {
                return ExportExcelBatchRename(programList);
            }
            else
            {
                return ExportExcelDefaultType(programList);
            }
            #endregion

        }

        private static void FillProgramList(Guid brokerId, Dictionary<Guid, __LoanProductFolderItem> folderDictionary, HashSet<Guid> allowableFolderSet, List<__LoanProgramTemplateItem> programList)
        {
            string sql = "SELECT LOAN_PROGRAM_TEMPLATE.*, r.LpeAcceptableRsFileId FROM LOAN_PROGRAM_TEMPLATE WITH(NOLOCK) " +
                         "  join Rate_Options as r on LOAN_PROGRAM_TEMPLATE.SrcRateOptionsProgId = r.SrcProgId " +
                         " WHERE BrokerID=@BrokerID AND IsLpe=1 AND IsMasterPriceGroup=0";

            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@BrokerId", brokerId) };

            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    if (allowableFolderSet.Contains(reader.SafeGuid("FolderId")) == false)
                    {
                        continue;
                    }

                    __LoanProgramTemplateItem item = new __LoanProgramTemplateItem();
                    item.FolderId = reader.SafeGuid("FolderId");
                    item.lLpTemplateId = reader.SafeGuid("lLpTemplateId");
                    item.lLpTemplateNm = reader.SafeString("lLpTemplateNm");
                    item.IsEnabled = reader.SafeBool("IsEnabled");
                    if (item.FolderId == Guid.Empty)
                    {
                        item.FolderFullName = "Root";
                    }
                    else
                    {
                        item.FolderFullName = folderDictionary[item.FolderId].FullName;
                    }

                    item.lLT = (E_sLT)reader.SafeInt("lLT");
                    item.lTerm = reader.SafeInt("lTerm");
                    item.lDue = reader.SafeInt("lDue");
                    item.lFinMethT = (E_sFinMethT)reader.SafeInt("lFinMethT");
                    item.IsMaster = reader.SafeBool("IsMaster");
                    item.lBaseLpId = reader.SafeGuid("lBaseLpId");
                    item.ProductCode = reader.SafeString("ProductCode");
                    item.lLpProductType = reader.SafeString("lLpProductType");
                    item.lLpInvestorNm = reader.SafeString("lLpInvestorNm");
                    item.lLpCustomCode1 = reader.SafeString("lLpCustomCode1");
                    item.lLpCustomCode2 = reader.SafeString("lLpCustomCode2");
                    item.lLpCustomCode3 = reader.SafeString("lLpCustomCode3");
                    item.lLpCustomCode4 = reader.SafeString("lLpCustomCode4");
                    item.lLpCustomCode5 = reader.SafeString("lLpCustomCode5");
                    item.lLpInvestorCode1 = reader.SafeString("lLpInvestorCode1");
                    item.lLpInvestorCode2 = reader.SafeString("lLpInvestorCode2");
                    item.lLpInvestorCode3 = reader.SafeString("lLpInvestorCode3");
                    item.lLpInvestorCode4 = reader.SafeString("lLpInvestorCode4");
                    item.lLpInvestorCode5 = reader.SafeString("lLpInvestorCode5");
                    item.lLpTemplateNmInherit = reader.SafeString("lLpTemplateNmInherit");
                    item.lLpTemplateNmOverrideBit = reader.SafeBool("lLpTemplateNmOverrideBit");
                    item.ProductCodeOverrideBit = reader.SafeBool("ProductCodeOverrideBit");
                    item.lLpeFeeMax = reader.SafeDecimal("lLpeFeeMax");
                    item.lRateDelta = reader.SafeDecimal("lRateDelta");
                    item.lFeeDelta = reader.SafeDecimal("lFeeDelta");
                    item.CreatedD = reader.SafeDateTime("CreatedD");
                    item.lLienPosT = (E_sLienPosT)reader.SafeInt("lLienPosT");
                    item.CanBeStandAlone2nd = reader.SafeBool("CanBeStandAlone2nd");
                    item.lLockedDays = reader.SafeInt("lLockedDays");
                    item.lRateSheetxmlContentOverrideBit = reader.SafeBool("lRateSheetxmlContentOverrideBit");
                    item.lRateOptionBaseId = reader.SafeString("lRateOptionBaseId");
                    item.lLpmiSupportedOutsidePmi = reader.SafeBool("lLpmiSupportedOutsidePmi");
                    item.lLpmiSupportedOutsidePmiOverrideBit = reader.SafeBool("lLpmiSupportedOutsidePmiOverrideBit");
                    item.lLpDPmtT = (E_sLpDPmtT)reader.SafeInt("lLpDPmtT");
                    item.lLpQPmtT = (E_sLpQPmtT)reader.SafeInt("lLpQPmtT");
                    item.lHasQRateInRateOptions = reader.SafeBool("lHasQRateInRateOptions");
                    item.lRadj1stCapMon = reader.SafeInt("lRadj1stCapMon");
                    item.lRAdjCapMon = reader.SafeInt("lRAdjCapMon");
                    item.lRadj1stCapR = reader.SafeDecimal("lRadj1stCapR");
                    item.lRAdjCapR = reader.SafeDecimal("lRAdjCapR");
                    item.lRAdjLifeCapR = reader.SafeDecimal("lRAdjLifeCapR");
                    item.lRAdjFloorBaseT = (E_sRAdjFloorBaseT)reader.SafeInt("lRAdjFloorBaseT");
                    item.lRAdjFloorR = reader.SafeDecimal("lRAdjFloorR");
                    item.lRAdjRoundT = (E_sRAdjRoundT)reader.SafeInt("lRAdjRoundT");
                    item.lRAdjRoundToR = reader.SafeDecimal("lRAdjRoundToR");
                    item.lArmIndexNameVstr = reader.SafeString("lArmIndexNameVstr");
                    item.lHardPrepmtPeriodMonths = reader.SafeInt("lHardPrepmtPeriodMonths");
                    item.lSoftPrepmtPeriodMonths = reader.SafeInt("lSoftPrepmtPeriodMonths");
                    item.lIOnlyMon = reader.SafeInt("lIOnlyMon");
                    item.lLateDays = reader.SafeInt("lLateDays");
                    item.lLateChargePc = reader.SafeString("lLateChargePc");
                    item.lLateChargeBaseDesc = reader.SafeString("lLateChargeBaseDesc");
                    item.lPrepmtPenaltyT = (E_sPrepmtPenaltyT)reader.SafeInt("lPrepmtPenaltyT");
                    item.lPrepmtRefundT = (E_sPrepmtRefundT)reader.SafeInt("lPrepmtRefundT");
                    item.lAssumeLT = (E_sAssumeLT)reader.SafeInt("lAssumeLT");
                    item.lQualRateCalculationT = (QualRateCalculationT)reader.SafeInt("lQualRateCalculationT");
                    item.lQualRateCalculationFieldT1 = (QualRateCalculationFieldT)reader.SafeInt("lQualRateCalculationFieldT1");
                    item.lQualRateCalculationFieldT2 = (QualRateCalculationFieldT)reader.SafeInt("lQualRateCalculationFieldT2");
                    item.lQualRateCalculationAdjustment1 = reader.SafeDecimal("lQualRateCalculationAdjustment1");
                    item.lQualRateCalculationAdjustment2 = reader.SafeDecimal("lQualRateCalculationAdjustment2");
                    item.LpeAcceptableRsFileId = reader.SafeString("LpeAcceptableRsFileId");
                    item.lHelocCalculatePrepaidInterest = reader.SafeBool("lHelocCalculatePrepaidInterest");
                    item.IsNonQmProgram = reader.SafeBool("IsNonQmProgram");
                    item.IsDisplayInNonQmQuickPricer = reader.SafeBool("IsDisplayInNonQmQuickPricer");
                    item.lQualTerm = reader.SafeInt("lHelocCalculatePrepaidInterest");
                    item.lQualTermCalculationType = (QualTermCalculationType)reader.SafeInt("lQualTermCalculationType");
                    programList.Add(item);
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, sql, null, parameters, readHandler);
        }

        private static byte[] ExportExcelBatchRename(IEnumerable<__LoanProgramTemplateItem> programList)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                XLWorkbook workbook = new XLWorkbook(XLEventTracking.Disabled);
                IXLWorksheet worksheet = workbook.AddWorksheet("Main");

                string[] headers = {
                                       "lLpTemplateId", "Current Name", "New Name"
                                   };
                for (int i = 0; i < headers.Length; i++)
                {
                    worksheet.Cell(1, i + 1).SetValue(headers[i]);

                }
                worksheet.Column(1).Width = 37;
                worksheet.Column(2).Width = 60;
                worksheet.Column(3).Width = 60;
                int row = 2;

                string previousFolderName = string.Empty;

                foreach (var program in programList.OrderBy(o => o.FolderFullName).ThenBy(o => o.lLpTemplateNm))
                {
                    if (program.IsMaster)
                    {
                        continue;
                    }
                    if (program.FolderFullName != previousFolderName)
                    {
                        if (row != 2)
                        {
                            row += 3;
                        }
                        worksheet.Cell(row, 2).SetValue(program.FolderFullName);
                        worksheet.Row(row).Style.Fill.BackgroundColor = XLColor.Blue;
                        worksheet.Row(row).Style.Font.FontColor = XLColor.White;
                        row++;
                        previousFolderName = program.FolderFullName;
                    }
                    worksheet.Cell(row, 1).SetValue(program.lLpTemplateId.ToString());
                    worksheet.Cell(row, 2).SetValue(program.lLpTemplateNm);


                    row++;
                }
                workbook.SaveAs(stream);

                return stream.ToArray();
            }
        }

        private static byte[] ExportExcelDefaultType(IEnumerable<__LoanProgramTemplateItem> programList)
        {
            Tuple<string, Func<__LoanProgramTemplateItem, string>>[] columns = new Tuple<string, Func<__LoanProgramTemplateItem, string>>[]
            {
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Program GUID (lLpTemplateId)", program => program.lLpTemplateId.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Folder", program => program.FolderFullName),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Type", program => program.Type),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Enabled?", program => program.IsEnabled ? "Yes" : "No"),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Investor Name (lLpInvestorNm)", program => program.lLpInvestorNm),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Loan Program Name(lLpTemplateNm)", program => program.lLpTemplateNm),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Original Loan Program Name (lLpTemplateNmInherit)", program => program.lLpTemplateNmInherit),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Original Loan Program Name Overriden?", program => program.lLpTemplateNmOverrideBit ? "Yes" : "No"),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Amortization Type (lFinMethT)", program => program.lFinMethT.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Product Code", program => program.ProductCode),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Product Code Overridden?", program => program.ProductCodeOverrideBit ? "Yes" : "No"),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Loan Product Type (lLpProductType)", program => program.lLpProductType),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Loan Type (lLT)", program => program.lLT.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Min YSP", program => program.lLpeFeeMax.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("CustomLoanProgramField 1 (lLpCustomCode1)", program => program.lLpCustomCode1),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("CustomLoanProgramField 2 (lLpCustomCode2)", program => program.lLpCustomCode2),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("CustomLoanProgramField 3 (lLpCustomCode3)", program => program.lLpCustomCode3),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("CustomLoanProgramField 4 (lLpCustomCode4)", program => program.lLpCustomCode4),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("CustomLoanProgramField 5 (lLpCustomCode5)", program => program.lLpCustomCode5),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("InvestorLoanProgramField 1 (lLpInvestorCode1)", program => program.lLpInvestorCode1),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("InvestorLoanProgramField 2 (lLpInvestorCode2)", program => program.lLpInvestorCode2),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("InvestorLoanProgramField 3 (lLpInvestorCode3)", program => program.lLpInvestorCode3),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("InvestorLoanProgramField 4 (lLpInvestorCode4)", program => program.lLpInvestorCode4),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("InvestorLoanProgramField 5 (lLpInvestorCode5)", program => program.lLpInvestorCode5),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Rate Delta", program => program.lRateDelta.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Fee Delta", program => program.lFeeDelta.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Created Date", program => program.CreatedD.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Lien Position", program => program.lLienPosT.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Can Be Stand Alone 2nd?", program => program.CanBeStandAlone2nd ? "Yes" : "No"),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("TERM", program => program.lTerm.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("DUE", program => program.lDue.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Rate Lock Period", program => program.lLockedDays.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Rate Sheet Overridden?", program => program.lRateSheetxmlContentOverrideBit ? "Yes" : "No"),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Rate sheet Identifier (LoRatesheetId)", program => program.lRateOptionBaseId),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("LpeAcceptableRsFileId", program => program.LpeAcceptableRsFileId),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("LPMI Pricing Support Outside of PMI Program Pairing?", program => program.lLpmiSupportedOutsidePmi ? "Yes" : "No"),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("\"LPMI Pricing Support Outside of PMI Program Pairing?\" Overridden?", program => program.lLpmiSupportedOutsidePmiOverrideBit ? "Yes" : "No"),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Display Payment", program => program.lLpDPmtT.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Qualify Payment", program => program.lLpQPmtT.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Use Q Rate", program => program.lHasQRateInRateOptions ? "Yes" : "No"),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("ARM 1st Change", program => program.lRadj1stCapMon.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("ARM Adj Period", program => program.lRAdjCapMon.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("ARM 1st Adj Cap", program => program.lRadj1stCapR.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("ARM Adj Cap", program => program.lRAdjCapR.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("ARM Life Adj Cap", program => program.lRAdjLifeCapR.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("ARM Rate Floor (dropdown)", program => program.lRAdjFloorBaseT.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("ARM Rate Floor (Free Fill)", program => program.lRAdjFloorR.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("ARM Round (dropdown)", program => program.lRAdjRoundT.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("ARM Round (Free fill)", program => program.lRAdjRoundToR.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("ARM Index", program => program.lArmIndexNameVstr),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Prepayment Penalty (mths) Hard", program => program.lHardPrepmtPeriodMonths.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Prepayment Penalty (mths) Soft", program => program.lSoftPrepmtPeriodMonths.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Interest only (mths)", program => program.lIOnlyMon.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Late Days (lLateDays)", program => program.lLateDays.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Late Charge % (lLateChargePc)", program => program.lLateChargePc),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Late Charge Desc (lLateChargeBaseDesc)", program => program.lLateChargeBaseDesc),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Pay Prepay Penalty (lPrepmtPenaltyT)", program => program.lPrepmtPenaltyT.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Pay Prepay Refund (lPrepmtRefundT)", program => program.lPrepmtRefundT.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Assumption (lAssumeLT)", program => program.lAssumeLT.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Q Rate Calculation Type", program => program.lHasQRateInRateOptions ? program.lQualRateCalculationT.ToString() : string.Empty),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Q Rate Calculation Field 1", program => program.lHasQRateInRateOptions ? program.lQualRateCalculationFieldT1.ToString() : string.Empty),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Q Rate Calculation Adjustment 1", program => program.lHasQRateInRateOptions ? program.lQualRateCalculationAdjustment1.ToString() : string.Empty),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Q Rate Calculation Field 2", program => program.lHasQRateInRateOptions && program.lQualRateCalculationT == QualRateCalculationT.MaxOf ? program.lQualRateCalculationFieldT2.ToString() : string.Empty),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Q Rate Calculation Adjustment 2", program => program.lHasQRateInRateOptions && program.lQualRateCalculationT == QualRateCalculationT.MaxOf ? program.lQualRateCalculationAdjustment2.ToString() : string.Empty),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Calculate and collect prepaid interest (HELOC ONLY)", program => program.lHelocCalculatePrepaidInterest ? "Yes" : "No"),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Q Term", program => program.lQualTermCalculationType == QualTermCalculationType.Manual ? program.lQualTerm.ToString() : string.Empty),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Is this a non-QM program?", program => program.IsNonQmProgram.ToString()),
                Tuple.Create<string, Func<__LoanProgramTemplateItem, string>>("Display in Non-QM OP?", program => program.IsDisplayInNonQmQuickPricer.ToString())
            };

            using (MemoryStream stream = new MemoryStream())
            {
                XLWorkbook workbook = new XLWorkbook(XLEventTracking.Disabled);
                IXLWorksheet worksheet = workbook.AddWorksheet("Main");

                for (int i = 0; i < columns.Length; i++)
                {
                    worksheet.Cell(1, i + 1).SetValue(columns[i].Item1);
                }
                int row = 2;
                foreach (var program in programList.OrderBy(o => o.FolderFullName).ThenBy(o => o.lLpTemplateNm))
                {
                    for (int i = 0; i < columns.Length; i++)
                    {
                        worksheet.Cell(row, i + 1).SetValue(columns[i].Item2(program));
                    }

                    if (program.IsMaster)
                    {
                        worksheet.Row(row).Style.Fill.BackgroundColor = XLColor.DarkBlue;
                        worksheet.Row(row).Style.Font.FontColor = XLColor.White;
                    }

                    row++;
                }
                workbook.SaveAs(stream);

                return stream.ToArray();
            }
        }

        private static string ComputeFullName(Dictionary<Guid, __LoanProductFolderItem> dictionary, Guid folderId)
        {
            List<string> parts = new List<string>();

            while (folderId != Guid.Empty)
            {
                __LoanProductFolderItem o = null;

                if (dictionary.TryGetValue(folderId, out o) == true)
                {
                    parts.Add(o.FolderName);

                    folderId = o.ParentFolderId;
                }
            }
            parts.Add("Root");
            parts.Reverse();
            string fullName = string.Empty;
            for (int i = 0; i < parts.Count; i++)
            {
                if (i != 0)
                {
                    fullName += " / ";
                }
                fullName += parts[i];

            }
            return fullName;
        }
    }
}
