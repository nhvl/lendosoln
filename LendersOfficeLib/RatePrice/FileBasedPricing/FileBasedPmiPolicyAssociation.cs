﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    public sealed partial class FileBasedPmiPolicyAssociation
    {
        public static IEnumerable<FileBasedPmiPolicyAssociation> CreateFrom(DataRowCollection collection)
        {
            List<FileBasedPmiPolicyAssociation> list = new List<FileBasedPmiPolicyAssociation>();

            foreach (DataRow row in collection)
            {
                FileBasedPmiPolicyAssociation item = new FileBasedPmiPolicyAssociation();
                item.PmiProductId = (Guid)row["PmiProductId"];
                item.PricePolicyId = (Guid)row["PricePolicyId"];
                list.Add(item);
            }
            return list;
        }
        public static FileBasedPmiPolicyAssociation Create(IDataReader reader)
        {
                FileBasedPmiPolicyAssociation item = new FileBasedPmiPolicyAssociation();
                item.PmiProductId = (Guid)reader["PmiProductId"];
                item.PricePolicyId = (Guid)reader["PricePolicyId"];

                return item;
        }
    }
}
