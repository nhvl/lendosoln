﻿using System;
using LendersOfficeApp.los.RatePrice;
using ProtoBuf;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    [ProtoContract]
    public sealed partial class FileBasedRtPricePolicy : AbstractRtPricePolicy
    {
        [ProtoMember(1)]
        public override Guid PricePolicyId
        {
            get; set;
        }

        [ProtoMember(2)]
        public string ContentKeyString
        {
            get; set;
        }


        [ProtoMember(3)]
        public override Guid ParentPolicyId
        {
            get;
            set;
        }
    }
}