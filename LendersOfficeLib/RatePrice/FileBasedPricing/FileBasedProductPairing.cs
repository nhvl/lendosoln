﻿namespace LendersOffice.RatePrice.FileBasedPricing
{
    using System.Data;

    /// <summary>
    /// Represent data in PRODUCT_PAIRING table.
    /// </summary>
    public sealed partial class FileBasedProductPairing
    {
        /// <summary>
        /// Create object from sql.
        /// </summary>
        /// <param name="reader">Sql row contains data.</param>
        /// <returns>An object.</returns>
        public static FileBasedProductPairing Create(IDataReader reader)
        {
            FileBasedProductPairing item = new FileBasedProductPairing();
            item.InvestorName1stLien = (string)reader["InvestorName1stLien"];
            item.ProductCode1stLien = (string)reader["ProductCode1stLien"];
            item.InvestorName2ndLien = (string)reader["InvestorName2ndLien"];
            item.ProductCode2ndLien = (string)reader["ProductCode2ndLien"];
            return item;
        }
    }
}