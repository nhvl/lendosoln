﻿namespace LendersOffice.RatePrice.FileBasedPricing
{
    using ProtoBuf;

    /// <summary>
    /// Represent data in PRODUCT_PAIRING table.
    /// </summary>
    [ProtoContract]
    public sealed partial class FileBasedProductPairing
    {
        /// <summary>
        /// Gets or sets investor name of 1st lien product.
        /// </summary>
        /// <value>The investor name of 1st lien product.</value>
        [ProtoMember(1)]
        public string InvestorName1stLien { get; set; }

        /// <summary>
        /// Gets or sets product code of 1st lien product.
        /// </summary>
        /// <value>The product code of 1st lien product.</value>
        [ProtoMember(2)]
        public string ProductCode1stLien { get; set; }

        /// <summary>
        /// Gets or sets investor name of 2nd lien product.
        /// </summary>
        /// <value>The investor name of 2nd lien product.</value>
        [ProtoMember(3)]
        public string InvestorName2ndLien { get; set; }

        /// <summary>
        /// Gets or sets product code of 2nd lien product.
        /// </summary>
        /// <value>The product code of 2nd lien product.</value>
        [ProtoMember(4)]
        public string ProductCode2ndLien { get; set; }
    }
}