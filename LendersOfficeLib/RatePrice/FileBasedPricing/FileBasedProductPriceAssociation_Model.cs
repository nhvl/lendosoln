﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using ProtoBuf;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    [ProtoContract]
    public sealed partial class FileBasedProductPriceAssociation
    {
        [ProtoMember(1)]
        public Guid ProductId { get; set; }

        [ProtoMember(2)]
        public List<FileBasedProductPriceAssociationItem> Items { get; set;}
    }

    [ProtoContract]
    public sealed partial class FileBasedProductPriceAssociationItem
    {
        [ProtoMember(1)]
        public Guid PricePolicyId { get; set; }

        [ProtoMember(2)]
        public E_TriState AssocOverrideTri { get; set; }

        [ProtoMember(3)]
        public bool InheritVal { get; set; }

        [ProtoMember(4)]
        public bool InheritValFromBaseProd { get; set; }

        [ProtoMember(5)]
        public bool IsForcedNo { get; set; }
    }
}
