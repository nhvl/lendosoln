﻿namespace LendersOffice.RatePrice.FileBasedPricing
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.IO;
    using System.IO.Compression;
    using System.Text;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOfficeApp.los.RatePrice;
    using LqbGrammar.DataTypes;
    using Model;
    using ProtoBuf;

    public class FileBasedSnapshotSummaryInfo
    {
        public DateTime DataLastModifiedDateInUtc { get; set; }
    }

    class GuidComparer : IEqualityComparer<Guid>
    {
        #region IEqualityComparer<Guid> Members

        public bool Equals(Guid x, Guid y)
        {
            return x.Equals(y);

        }

        public int GetHashCode(Guid obj)
        {
            return obj.GetHashCode();
        }

        #endregion
    }

    public static class FileBasedPricingUtilities
    {
        static private string SnapshotRootFolder = ConstApp.TempFolder;

        public static string GetFileDBFileName(DateTime dt)
        {
            // ToDo: map date time to filebased snapshot
            return dt.ToString("yyyyMMddHHmmss") + @"_LPEPricing";
        }

        private static void RemoveZipFileAndTmpFolder(string zipFile, string tmpFolder)
        {
            if (FileOperationHelper.Exists(zipFile))
            {
                FileOperationHelper.Delete(zipFile);
            }

            if (Directory.Exists(tmpFolder))
            {
                DirectoryInfo folderInfo = new DirectoryInfo(tmpFolder);
                folderInfo.Delete(true);
            }

        }

        public static SHA256Checksum CreateSnapshot()
        {
            CLpeReleaseInfo releaseInfo = CLpeReleaseInfo.GetLatestReleaseInfo();
            DataSrc dataSrc = releaseInfo.DataSrcForSnapshot;
            DateTime dataLastModifiedD = releaseInfo.DataLastModifiedD;
            return CreateSnapshot(dataSrc, dataLastModifiedD);
        }

        public static SHA256Checksum CreateSnapshot(DataSrc dataSrc, DateTime dataLastModifiedD)
        {
            StringBuilder debugString = new StringBuilder();
            Stopwatch sw = Stopwatch.StartNew();
            debugString.AppendLine($"FileBasedPricingUtilities.CreateSnapshot( {dataSrc}, {dataLastModifiedD})");
            bool hasError = false;
            string dataFolderName = dataLastModifiedD.ToString("yyyyMMddHHmmss") + @"_LPEPricing";

            try
            {
                string path = Path.Combine(SnapshotRootFolder, dataFolderName);
                string zipFile = Path.Combine(SnapshotRootFolder, dataFolderName + ".zip");
                RemoveZipFileAndTmpFolder(zipFile, path);

                FileBasedSnapshotSummaryInfo summaryInfo = new FileBasedSnapshotSummaryInfo();
                summaryInfo.DataLastModifiedDateInUtc = dataLastModifiedD.ToUniversalTime();


                DirectoryInfo di = Directory.CreateDirectory(path);
                debugString.AppendLine("Created Path=[" + path + "].");

                Copy_PRICE_POLICY_v2(debugString, path, dataSrc);

                Copy_PRODUCT_PRICE_ASSOCIATION(debugString, path, dataSrc);
                Copy_PRICEGROUP_PROG_POLICY_ASSOC(debugString, path, dataSrc);
                Copy_LOAN_PROGRAM_TEMPLATE(debugString, path, dataSrc);
                Copy_RATE_OPTIONS_v2(debugString, path, dataSrc);

                Copy_PMI_PRODUCT(debugString, path, dataSrc);
                Copy_PMI_POLICY_ASSOC(debugString, path, dataSrc);
                Copy_LOAN_PRODUCT_FOLDER(debugString, path, dataSrc);
                Copy_PMI_PRODUCT_FOLDER(debugString, path, dataSrc);
                Copy_RATE_OPTION_PRICEGROUP_PROGRAM(debugString, path, dataSrc);
                Copy_PRODUCT(debugString, path, dataSrc);
                Copy_PRODUCT_PAIRING(debugString, path, dataSrc);
                Copy_INVESTOR_NAME(debugString, path, dataSrc);
                Copy_DOC_MAGIC_PLAN_CODE(debugString, path, dataSrc);
                GenerateSummaryFile(path, summaryInfo);
                debugString.AppendFormat("[{0}] Finished Create Snapshot.", msToString(sw.ElapsedMilliseconds)).AppendLine();

                long before = sw.ElapsedMilliseconds;
                ZipFile.CreateFromDirectory(path, zipFile);
                FileInfo fileInfo = new FileInfo(zipFile);
                debugString.AppendFormat("[{0}] Zip File: size = {1:N0} bytes, excute time {2:N0} ms.", 
                                         msToString(sw.ElapsedMilliseconds), fileInfo.Length, sw.ElapsedMilliseconds-before).AppendLine();

                before = sw.ElapsedMilliseconds;
                LocalFilePath? zipPath = LocalFilePath.Create(zipFile);
                if(zipPath == null)
                {
                     throw CBaseException.GenericException($"zipFile '{zipFile}' is not a valid path.");
                }

                var checksum = EncryptionHelper.ComputeSHA256Hash(zipPath.Value);
                Utils.SaveToPriceEngineStorage(PriceEngineStorageType.PricingSnapshot, checksum, zipPath.Value);

                debugString.AppendFormat("[{0}] Put to FileDB. KeyDB = {1}, excute time {2:N0} ms.",
                                         msToString(sw.ElapsedMilliseconds), checksum.Value, sw.ElapsedMilliseconds - before).AppendLine();

                RemoveZipFileAndTmpFolder(zipFile, path);

                return checksum;
            }
            catch (Exception exc)
            {
                hasError = true;
                debugString.AppendLine(exc.ToString());
                throw;
            }
            finally
            {
                if (hasError)
                {
                    Tools.LogError(debugString.ToString());
                }
                else
                {
                    Tools.LogInfo(debugString.ToString());
                }
            }
        }

        private static void LogExeTime(StringBuilder debugString, string msg, long totalTime, long loadTime, int numRecords, string fname, string extraInfo = "")
        {

            string str = msg + totalTime.ToString("N0") + " ms." +
                         Environment.NewLine + "   load time " + loadTime.ToString("N0") +
                         " ms, write time " + (totalTime - loadTime).ToString("N0") + " ms";
            if (numRecords > 0)
            {
                str += ", " + numRecords.ToString("N0") + " records";
            }

            if (string.IsNullOrEmpty(extraInfo) == false)
            {
                str += ", " + extraInfo;
            }

            if (!string.IsNullOrEmpty(fname))
            {
                FileInfo fileInfo = new FileInfo(fname);
                str += Environment.NewLine + "   " + Path.GetFileName(fname) + ": " + fileInfo.Length.ToString("N0") + " bytes";
            }

            debugString.AppendLine(str);
        }

        private static void Copy_LOAN_PRODUCT_FOLDER(StringBuilder debugString, string path, DataSrc dataSrc)
        {
            Stopwatch sw = Stopwatch.StartNew();

            string fileName = Path.Combine(path, "LOAN_PRODUCT_FOLDER.bin");

            string sql = "SELECT * FROM LOAN_PRODUCT_FOLDER WITH(NOLOCK)";

            List<FileBasedLoanProductFolder> list = new List<FileBasedLoanProductFolder>();
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    FileBasedLoanProductFolder item = FileBasedLoanProductFolder.Create(reader);
                    list.Add(item);
                }
            };

            DBSelectUtility.ProcessDBData(dataSrc, sql, null, null, readHandler);

            long loadTime = sw.ElapsedMilliseconds;
            Action<BinaryFileHelper.LqbBinaryStream> writeHandler = delegate (BinaryFileHelper.LqbBinaryStream stream)
            {
                Serializer.Serialize(stream.Stream, list);
            };

            BinaryFileHelper.OpenNew(fileName, writeHandler);
            LogExeTime(debugString, "Copy_LOAN_PRODUCT_FOLDER executed in ", sw.ElapsedMilliseconds, loadTime, list.Count, fileName);
        }

        private static void Copy_PRODUCT(StringBuilder debugString, string path, DataSrc dataSrc)
        {
            string sql = "SELECT * FROM PRODUCT";
            Copy(debugString, path, dataSrc, sql, "PRODUCT.bin", FileBasedProduct.Create);
        }

        private static void Copy_PRODUCT_PAIRING(StringBuilder debugString, string path, DataSrc dataSrc)
        {
            string sql = "SELECT * FROM PRODUCT_PAIRING";
            Copy(debugString, path, dataSrc, sql, "PRODUCT_PAIRING.bin", FileBasedProductPairing.Create);
        }

        private static void Copy_INVESTOR_NAME(StringBuilder debugString, string path, DataSrc dataSrc)
        {
            string sql = "SELECT * FROM INVESTOR_NAME";
            Copy(debugString, path, dataSrc, sql, "INVESTOR_NAME.bin", FileBasedInvestorName.Create);
        }

        private static void Copy_DOC_MAGIC_PLAN_CODE(StringBuilder debugString, string path, DataSrc dataSrc)
        {
            string sql = "SELECT * FROM DOC_MAGIC_PLAN_CODE";
            Copy(debugString, path, dataSrc, sql, "DOC_MAGIC_PLAN_CODE.bin", FileBasedDocMagicPlanCode.Create);
        }

        private static void Copy<T>(StringBuilder debugString, string path, DataSrc dataSrc, string sql, string outputFileName, Func<IDataReader, T> createMethod)
        {
            Stopwatch sw = Stopwatch.StartNew();

            string fileName = Path.Combine(path, outputFileName);

            List<T> list = new List<T>();
            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    T item = createMethod(reader);
                    list.Add(item);
                }
            };

            DBSelectUtility.ProcessDBData(dataSrc, sql, null, null, readHandler);

            long loadTime = sw.ElapsedMilliseconds;
            Action<BinaryFileHelper.LqbBinaryStream> writeHandler = delegate (BinaryFileHelper.LqbBinaryStream stream)
            {
                Serializer.Serialize(stream.Stream, list);
            };

            BinaryFileHelper.OpenNew(fileName, writeHandler);
            LogExeTime(debugString, $"Copy {outputFileName} executed in ", sw.ElapsedMilliseconds, loadTime, list.Count, fileName);
        }

        private static void Copy_PMI_PRODUCT_FOLDER(StringBuilder debugString, string path, DataSrc dataSrc)
        {
            Stopwatch sw = Stopwatch.StartNew();

            string fileName = Path.Combine(path, "PMI_PRODUCT_FOLDER.bin");

            string sql = "SELECT * FROM PMI_PRODUCT_FOLDER WITH(NOLOCK)";

            List<FileBasedPmiProductFolder> list = new List<FileBasedPmiProductFolder>();
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    FileBasedPmiProductFolder item = FileBasedPmiProductFolder.Create(reader);
                    list.Add(item);
                }
            };

            DBSelectUtility.ProcessDBData(dataSrc, sql, null, null, readHandler);

            long loadTime = sw.ElapsedMilliseconds;
            Action<BinaryFileHelper.LqbBinaryStream> writeHandler = delegate(BinaryFileHelper.LqbBinaryStream stream)
            {
                Serializer.Serialize(stream.Stream, list);
            };

            BinaryFileHelper.OpenNew(fileName, writeHandler);
            LogExeTime(debugString, "Copy_PMI_PRODUCT_FOLDER executed in ", sw.ElapsedMilliseconds, loadTime, list.Count, fileName);
        }

        private static void Copy_RATE_OPTION_PRICEGROUP_PROGRAM(StringBuilder debugString, string path, DataSrc dataSrc)
        {
            Stopwatch sw = Stopwatch.StartNew();

            string fileName = Path.Combine(path, "RATE_OPTION_PRICEGROUP_PROGRAM.bin");

            string sql = @"SELECT o.LpePriceGroupId, o.lLpTemplateId, o.lRateSheetXmlContent, o.VersionTimestamp, o.LpeAcceptableRsFileId,
                        o.LpeAcceptableRsFileVersionNumber, t.lRateSheetDownloadStartD, t.lRateSheetDownloadEndD
                           FROM RATE_OPTION_PRICEGROUP_PROGRAM o WITH(NOLOCK) JOIN RATE_OPTION_PRICEGROUP_PROGRAM_DOWNLOAD_TIME t WITH(NOLOCK)
                            ON o.LpePriceGroupId = t.LpePriceGroupId AND o.lLpTemplateId = t.lLpTemplateId";

            List<FileBasedRateOptionPriceGroupProgram> list = new List<FileBasedRateOptionPriceGroupProgram>();
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    FileBasedRateOptionPriceGroupProgram item = FileBasedRateOptionPriceGroupProgram.Create(reader);
                    list.Add(item);
                }
            };

            DBSelectUtility.ProcessDBData(dataSrc, sql, null, null, readHandler);

            long loadTime = sw.ElapsedMilliseconds;
            Action<BinaryFileHelper.LqbBinaryStream> writeHandler = delegate(BinaryFileHelper.LqbBinaryStream stream)
            {
                Serializer.Serialize(stream.Stream, list);
            };

            BinaryFileHelper.OpenNew(fileName, writeHandler);
            LogExeTime(debugString, "Copy_FileBasedRateOptionPriceGroupProgram executed in ", sw.ElapsedMilliseconds, loadTime, list.Count, fileName);
        }


        private static void Copy_PMI_PRODUCT(StringBuilder debugString, string path, DataSrc dataSrc)
        {
            Stopwatch sw = Stopwatch.StartNew();

            string fileName = Path.Combine(path, "PMI_PRODUCT.bin");

            string sql = "SELECT ProductId, MICompany, MIType, UfmipIsRefundableOnProRataBasis FROM PMI_PRODUCT WHERE IsEnabled = 1 AND MIType <> 0;";

            List<FileBasedPmiProduct> list = new List<FileBasedPmiProduct>();
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    FileBasedPmiProduct item = FileBasedPmiProduct.Create(reader);
                    list.Add(item);
                }
            };

            DBSelectUtility.ProcessDBData(dataSrc, sql, null, null, readHandler);

            long loadTime = sw.ElapsedMilliseconds;
            Action<BinaryFileHelper.LqbBinaryStream> writeHandler = delegate(BinaryFileHelper.LqbBinaryStream stream)
            {
                Serializer.Serialize(stream.Stream, list);
            };

            BinaryFileHelper.OpenNew(fileName, writeHandler);
            LogExeTime(debugString, "Copy_PMI_PRODUCT executed in ", sw.ElapsedMilliseconds, loadTime, list.Count, fileName);
        }

        private static void Copy_PMI_POLICY_ASSOC(StringBuilder debugString, string path, DataSrc dataSrc)
        {
            Stopwatch sw = Stopwatch.StartNew();

            string fileName = Path.Combine(path, "PMI_POLICY_ASSOC.bin");
            string sql = "SELECT PricePolicyId, PmiProductId from PMI_POLICY_ASSOC WHERE AssocOverrideTri = 1 OR ( AssocOverrideTri = 0 AND InheritVal = 1 );";

            List<FileBasedPmiPolicyAssociation> list = new List<FileBasedPmiPolicyAssociation>();
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    FileBasedPmiPolicyAssociation item = FileBasedPmiPolicyAssociation.Create(reader);
                    list.Add(item);
                }
            };

            DBSelectUtility.ProcessDBData(dataSrc, sql, null, null, readHandler);

            long loadTime = sw.ElapsedMilliseconds;
            Action<BinaryFileHelper.LqbBinaryStream> writeHandler = delegate(BinaryFileHelper.LqbBinaryStream stream)
            {
                Serializer.Serialize(stream.Stream, list);
            };

            BinaryFileHelper.OpenNew(fileName, writeHandler);
            LogExeTime(debugString, "Execute Copy_PMI_POLICY_ASSOC in ", sw.ElapsedMilliseconds, loadTime, list.Count, fileName);
        }

        private static void Copy_PRICEGROUP_PROG_POLICY_ASSOC(StringBuilder debugString, string path, DataSrc dataSrc)
        {
            Stopwatch sw = Stopwatch.StartNew();
            string fileName = Path.Combine(path, "PRICEGROUP_PROG_POLICY_ASSOC.bin");
            string sql = "SELECT * FROM PRICEGROUP_PROG_POLICY_ASSOC WITH(NOLOCK)";

            List<FileBasedPriceGroupProgPolicyAssoc> list = new List<FileBasedPriceGroupProgPolicyAssoc>();
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    FileBasedPriceGroupProgPolicyAssoc item = new FileBasedPriceGroupProgPolicyAssoc();
                    item.LpePriceGroupId = (Guid)reader["LpePriceGroupId"];
                    item.ProgId = (Guid)reader["ProgId"];
                    item.PricePolicyId = (Guid)reader["PricePolicyId"];
                    item.AssociateType = (E_TriState)((byte)reader["AssociateType"]);

                    list.Add(item);
                }
            };

            DBSelectUtility.ProcessDBData(dataSrc, sql, null, null, readHandler);

            long loadTime = sw.ElapsedMilliseconds;
            Action<BinaryFileHelper.LqbBinaryStream> writeHandler = delegate(BinaryFileHelper.LqbBinaryStream stream)
            {
                Serializer.Serialize(stream.Stream, list);
            };

            BinaryFileHelper.OpenNew(fileName, writeHandler);
            LogExeTime(debugString, "Execute Copy_PRICEGROUP_PROG_POLICY_ASSOC in ", sw.ElapsedMilliseconds, loadTime, list.Count, fileName);           
        }

        static void LoadDataFrom_PRODUCT_PRICE_ASSOCIATION(DataSrc dataSrc, string sql, Dictionary<Guid, FileBasedProductPriceAssociation> dictionary, ref int recordCount)
        {
            int recordCountLocal = recordCount;
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {

                    Guid productId = (Guid)reader["ProductId"];
                    Guid pricePolicyId = (Guid)reader["PricePolicyId"];
                    E_TriState assocOverrideTri = (E_TriState)((byte)reader["AssocOverrideTri"]);
                    bool inheritVal = (bool)reader["InheritVal"];
                    bool inheritValFromBaseProd = (bool)reader["InheritValFromBaseProd"];
                    bool isForcedNo = (bool)reader["IsForcedNo"];

                    FileBasedProductPriceAssociation item;
                    if (dictionary.TryGetValue(productId, out item) == false)
                    {
                        item = new FileBasedProductPriceAssociation();
                        item.ProductId = productId;
                        dictionary.Add(productId, item);
                    }
                    item.Add(pricePolicyId, assocOverrideTri, inheritVal, inheritValFromBaseProd, isForcedNo);
                    recordCountLocal++;
                }
            };

            DBSelectUtility.ProcessDBData(dataSrc, sql, null, null, readHandler);
            recordCount = recordCountLocal;
        }

        private static void Copy_PRODUCT_PRICE_ASSOCIATION(StringBuilder debugString, string path, DataSrc dataSrc)
        {
            Stopwatch sw = Stopwatch.StartNew();
            string fileName = Path.Combine(path, "PRODUCT_PRICE_ASSOCIATION.bin");

            List<FileBasedProductPriceAssociation> list = new List<FileBasedProductPriceAssociation>();
            Dictionary<Guid, FileBasedProductPriceAssociation> dictionary = new Dictionary<Guid, FileBasedProductPriceAssociation>();
            int recordCount = 0;

            /*  Price Policies Associated To Price Group

                Reference CLpGlobal.GetAssocPriceGroupWidePoliciesFromDb(...)
                         string firstSql = "SELECT PricePolicyId, ProductId, AssocOverrideTri FROM Product_Price_Association WHERE AssocOverrideTri != 0 AND (" + firstWhereClause.ToString() + ")";
            */
            string associatedToPriceGroupSql = "SELECT pa.* " +
                        "FROM ( Product_Price_Association pa join loan_program_template product on pa.productId = product.lLpTemplateId ) " +
                        "WHERE                                      \n" +
                        "           product.IsMasterPriceGroup = 1  \n" +
                        "   and     product.IsMaster = 1    \n" +
                        "   and     pa.AssocOverrideTri != 0 \n";

            LoadDataFrom_PRODUCT_PRICE_ASSOCIATION(dataSrc, associatedToPriceGroupSql, dictionary, ref recordCount);

            // "Price Policies Associated To Price Group" records comes first for optimzing FileBasedSnapshot.PopulateApplicablesPricePolicy(...) in future
            foreach (var o in dictionary.Values)
            {
                list.Add(o);
            }

            dictionary.Clear();

            /*  Price Policies Directly Associated To Loan Product

                    Reference  CLpGlobal.GetLoanProgramsFromDb(...)  

                        ";\nSELECT pa.ProductId, p.PricePolicyId " +
                        "FROM ( Product_Price_Association pa JOIN Price_Policy p ON pa.PricePolicyID = p.PricePolicyId )              " +
                        "   join loan_program_template product on pa.productId = product.lLpTemplateId                                " +
                        "WHERE                               \n" +
                        "           product.IsMaster = 0     \n" +
                        "   and     product.IsEnabled = 1    \n" +
                        "   and     product.IsLpe = 1        \n" +
                        "   and     pa.AssocOverrideTri != 2 \n" +
                        "   and ( pa.InheritVal = 1 or pa.AssocOverrideTri = 1 or pa.InheritValFromBaseProd = 1 ) \n" +
                        "   and product.lLpTemplateId ");
            */

            string directlyAssociatedToProductSql = "SELECT pa.* " +
                        "FROM ( Product_Price_Association pa join loan_program_template product on pa.productId = product.lLpTemplateId ) " +
                        "WHERE                               \n" +
                        "           product.IsMaster = 0     \n" +
                        "   and     product.IsEnabled = 1    \n" +
                        "   and     product.IsLpe = 1        \n" +
                        "   and     pa.AssocOverrideTri != 2 \n" +
                        "   and ( pa.InheritVal = 1 or pa.AssocOverrideTri = 1 or pa.InheritValFromBaseProd = 1 )";

            LoadDataFrom_PRODUCT_PRICE_ASSOCIATION(dataSrc, directlyAssociatedToProductSql, dictionary, ref recordCount);
            foreach (var o in dictionary.Values)
            {
                list.Add(o);
            }

            long loadTime = sw.ElapsedMilliseconds;
            Action<BinaryFileHelper.LqbBinaryStream> writeHandler = delegate(BinaryFileHelper.LqbBinaryStream stream)
            {
                Serializer.Serialize(stream.Stream, list);
            };

            BinaryFileHelper.OpenNew(fileName, writeHandler);
            LogExeTime(debugString, "Execute Copy_PRODUCT_PRICE_ASSOCIATION in ", sw.ElapsedMilliseconds, loadTime, recordCount, fileName,
                       string.Format("#distinct ProductId value = {0:N0}", list.Count));
        }

        private static void Copy_PRICE_POLICY_v2(StringBuilder debugString, string path, DataSrc dataSrc)
        {
            Stopwatch sw = Stopwatch.StartNew();

            string fileName = Path.Combine(path, "PRICE_POLICY_v2.bin");

            // 2/21/2018 - dd - Select the full table in one shot may cause SQL timeout. Need to break in chunk.
            string sql = "SELECT PricePolicyId FROM PRICE_POLICY WITH(NOLOCK)";

            List<Guid> idList = new List<Guid>();
            Action<IDataReader> idReadHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    idList.Add((Guid)reader["PricePolicyId"]);
                }
            };
            DBSelectUtility.ProcessDBData(dataSrc, sql, null, null, idReadHandler);

            sql = @"SELECT PricePolicyId, PricePolicyDescription, ParentPolicyId, MutualExclusiveExecSortedId, RulesProtobufContent FROM PRICE_POLICY WITH(NOLOCK) 
                    WHERE PricePolicyId IN (@p0,@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8,@p9,@p10,@p11,@p12,@p13,@p14,@p15,@p16,@p17,@p18,@p19,@p20,@p21,@p22,@p23,@p24,@p25,
                      @p26,@p27,@p28,@p29,@p30,@p31,@p32,@p33,@p34,@p35,@p36,@p37,@p38,@p39,@p40,@p41,@p42,@p43,@p44,@p45,@p46,@p47,@p48,@p49,@p50,@p51,@p52,@p53,@p54,@p55,
                      @p56,@p57,@p58,@p59,@p60,@p61,@p62,@p63,@p64,@p65,@p66,@p67,@p68,@p69,@p70,@p71,@p72,@p73,@p74,@p75,@p76,@p77,@p78,@p79,@p80,@p81,@p82,@p83,@p84,@p85,
                      @p86,@p87,@p88,@p89,@p90,@p91,@p92,@p93,@p94,@p95,@p96,@p97,@p98,@p99,@p100,@p101,@p102,@p103,@p104,@p105,@p106,@p107,@p108,@p109,@p110,@p111,@p112,@p113,
                      @p114,@p115,@p116,@p117,@p118,@p119,@p120,@p121,@p122,@p123,@p124,@p125,@p126,@p127,@p128,@p129,@p130,@p131,@p132,@p133,@p134,@p135,@p136,@p137,@p138,@p139,
                      @p140,@p141,@p142,@p143,@p144,@p145,@p146,@p147,@p148,@p149,@p150,@p151,@p152,@p153,@p154,@p155,@p156,@p157,@p158,@p159,@p160,@p161,@p162,@p163,@p164,@p165,
                      @p166,@p167,@p168,@p169,@p170,@p171,@p172,@p173,@p174,@p175,@p176,@p177,@p178,@p179,@p180,@p181,@p182,@p183,@p184,@p185,@p186,@p187,@p188,@p189,@p190,@p191,
                      @p192,@p193,@p194,@p195,@p196,@p197,@p198,@p199,@p200,@p201,@p202,@p203,@p204,@p205,@p206,@p207,@p208,@p209,@p210,@p211,@p212,@p213,@p214,@p215,@p216,@p217,
                      @p218,@p219,@p220,@p221,@p222,@p223,@p224,@p225,@p226,@p227,@p228,@p229,@p230,@p231,@p232,@p233,@p234,@p235,@p236,@p237,@p238,@p239,@p240,@p241,@p242,@p243,
                      @p244,@p245,@p246,@p247,@p248,@p249,@p250,@p251,@p252,@p253,@p254,@p255,@p256,@p257,@p258,@p259,@p260,@p261,@p262,@p263,@p264,@p265,@p266,@p267,@p268,@p269,
                      @p270,@p271,@p272,@p273,@p274,@p275,@p276,@p277,@p278,@p279,@p280,@p281,@p282,@p283,@p284,@p285,@p286,@p287,@p288,@p289,@p290,@p291,@p292,@p293,@p294,@p295,
                      @p296,@p297,@p298,@p299,@p300,@p301,@p302,@p303,@p304,@p305,@p306,@p307,@p308,@p309,@p310,@p311,@p312,@p313,@p314,@p315,@p316,@p317,@p318,@p319,@p320,@p321,
                      @p322,@p323,@p324,@p325,@p326,@p327,@p328,@p329,@p330,@p331,@p332,@p333,@p334,@p335,@p336,@p337,@p338,@p339,@p340,@p341,@p342,@p343,@p344,@p345,@p346,@p347,
                      @p348,@p349,@p350,@p351,@p352,@p353,@p354,@p355,@p356,@p357,@p358,@p359,@p360,@p361,@p362,@p363,@p364,@p365,@p366,@p367,@p368,@p369,@p370,@p371,@p372,@p373,
                      @p374,@p375,@p376,@p377,@p378,@p379,@p380,@p381,@p382,@p383,@p384,@p385,@p386,@p387,@p388,@p389,@p390,@p391,@p392,@p393,@p394,@p395,@p396,@p397,@p398,@p399,
                      @p400,@p401,@p402,@p403,@p404,@p405,@p406,@p407,@p408,@p409,@p410,@p411,@p412,@p413,@p414,@p415,@p416,@p417,@p418,@p419,@p420,@p421,@p422,@p423,@p424,@p425,
                      @p426,@p427,@p428,@p429,@p430,@p431,@p432,@p433,@p434,@p435,@p436,@p437,@p438,@p439,@p440,@p441,@p442,@p443,@p444,@p445,@p446,@p447,@p448,@p449,@p450,@p451,
                      @p452,@p453,@p454,@p455,@p456,@p457,@p458,@p459,@p460,@p461,@p462,@p463,@p464,@p465,@p466,@p467,@p468,@p469,@p470,@p471,@p472,@p473,@p474,@p475,@p476,@p477,
                      @p478,@p479,@p480,@p481,@p482,@p483,@p484,@p485,@p486,@p487,@p488,@p489,@p490,@p491,@p492,@p493,@p494,@p495,@p496,@p497,@p498,@p499,@p500,@p501,@p502,@p503,
                      @p504,@p505,@p506,@p507,@p508,@p509,@p510,@p511,@p512,@p513,@p514,@p515,@p516,@p517,@p518,@p519,@p520,@p521,@p522,@p523,@p524,@p525,@p526,@p527,@p528,@p529,
                      @p530,@p531,@p532,@p533,@p534,@p535,@p536,@p537,@p538,@p539,@p540,@p541,@p542,@p543,@p544,@p545,@p546,@p547,@p548,@p549,@p550,@p551,@p552,@p553,@p554,@p555,
                      @p556,@p557,@p558,@p559,@p560,@p561,@p562,@p563,@p564,@p565,@p566,@p567,@p568,@p569,@p570,@p571,@p572,@p573,@p574,@p575,@p576,@p577,@p578,@p579,@p580,@p581,
                      @p582,@p583,@p584,@p585,@p586,@p587,@p588,@p589,@p590,@p591,@p592,@p593,@p594,@p595,@p596,@p597,@p598,@p599,@p600,@p601,@p602,@p603,@p604,@p605,@p606,@p607,
                      @p608,@p609,@p610,@p611,@p612,@p613,@p614,@p615,@p616,@p617,@p618,@p619,@p620,@p621,@p622,@p623,@p624,@p625,@p626,@p627,@p628,@p629,@p630,@p631,@p632,@p633,
                      @p634,@p635,@p636,@p637,@p638,@p639,@p640,@p641,@p642,@p643,@p644,@p645,@p646,@p647,@p648,@p649,@p650,@p651,@p652,@p653,@p654,@p655,@p656,@p657,@p658,@p659,
                      @p660,@p661,@p662,@p663,@p664,@p665,@p666,@p667,@p668,@p669,@p670,@p671,@p672,@p673,@p674,@p675,@p676,@p677,@p678,@p679,@p680,@p681,@p682,@p683,@p684,@p685,
                      @p686,@p687,@p688,@p689,@p690,@p691,@p692,@p693,@p694,@p695,@p696,@p697,@p698,@p699,@p700,@p701,@p702,@p703,@p704,@p705,@p706,@p707,@p708,@p709,@p710,@p711,
                      @p712,@p713,@p714,@p715,@p716,@p717,@p718,@p719,@p720,@p721,@p722,@p723,@p724,@p725,@p726,@p727,@p728,@p729,@p730,@p731,@p732,@p733,@p734,@p735,@p736,@p737,
                      @p738,@p739,@p740,@p741,@p742,@p743,@p744,@p745,@p746,@p747,@p748,@p749,@p750,@p751,@p752,@p753,@p754,@p755,@p756,@p757,@p758,@p759,@p760,@p761,@p762,@p763,
                      @p764,@p765,@p766,@p767,@p768,@p769,@p770,@p771,@p772,@p773,@p774,@p775,@p776,@p777,@p778,@p779,@p780,@p781,@p782,@p783,@p784,@p785,@p786,@p787,@p788,@p789,
                      @p790,@p791,@p792,@p793,@p794,@p795,@p796,@p797,@p798,@p799,@p800,@p801,@p802,@p803,@p804,@p805,@p806,@p807,@p808,@p809,@p810,@p811,@p812,@p813,@p814,@p815,
                      @p816,@p817,@p818,@p819,@p820,@p821,@p822,@p823,@p824,@p825,@p826,@p827,@p828,@p829,@p830,@p831,@p832,@p833,@p834,@p835,@p836,@p837,@p838,@p839,@p840,@p841,
                      @p842,@p843,@p844,@p845,@p846,@p847,@p848,@p849,@p850,@p851,@p852,@p853,@p854,@p855,@p856,@p857,@p858,@p859,@p860,@p861,@p862,@p863,@p864,@p865,@p866,@p867,
                      @p868,@p869,@p870,@p871,@p872,@p873,@p874,@p875,@p876,@p877,@p878,@p879,@p880,@p881,@p882,@p883,@p884,@p885,@p886,@p887,@p888,@p889,@p890,@p891,@p892,@p893,
                      @p894,@p895,@p896,@p897,@p898,@p899,@p900,@p901,@p902,@p903,@p904,@p905,@p906,@p907,@p908,@p909,@p910,@p911,@p912,@p913,@p914,@p915,@p916,@p917,@p918,@p919,
                      @p920,@p921,@p922,@p923,@p924,@p925,@p926,@p927,@p928,@p929,@p930,@p931,@p932,@p933,@p934,@p935,@p936,@p937,@p938,@p939,@p940,@p941,@p942,@p943,@p944,@p945,
                      @p946,@p947,@p948,@p949,@p950,@p951,@p952,@p953,@p954,@p955,@p956,@p957,@p958,@p959,@p960,@p961,@p962,@p963,@p964,@p965,@p966,@p967,@p968,@p969,@p970,@p971,
                      @p972,@p973,@p974,@p975,@p976,@p977,@p978,@p979,@p980,@p981,@p982,@p983,@p984,@p985,@p986,@p987,@p988,@p989,@p990,@p991,@p992,@p993,@p994,@p995,@p996,@p997,@p998,@p999)";

            List<PricePolicyV2> list = new List<PricePolicyV2>();
            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    PricePolicyV2 item = new PricePolicyV2();
                    item.PricePolicyId = (Guid)reader["PricePolicyId"];
                    item.PricePolicyDescription = (string)reader["PricePolicyDescription"];
                    item.ParentPolicyId = reader.SafeGuid("ParentPolicyId");
                    item.MutualExclusiveExecSortedId = (string)reader["MutualExclusiveExecSortedId"];

                    object rulesProtobufContent = reader["RulesProtobufContent"];
                    if (rulesProtobufContent != DBNull.Value)
                    {
                        item.Rules = SerializationHelper.ProtobufDeserialize<List<PricePolicyRuleV2>>((byte[])rulesProtobufContent);
                    }

                    list.Add(item);
                }
            };

            int startIndex = 0;
            while (startIndex < idList.Count)
            {
                List<SqlParameter> parameters = new List<SqlParameter>();

                for (int i = 0; i < 1000; i++)
                {
                    Guid value = (i + startIndex) < idList.Count ? idList[i + startIndex] : Guid.Empty;
                    parameters.Add(new SqlParameter("@p" + i, value));
                }
                DBSelectUtility.ProcessDBData(dataSrc, sql, null, parameters, readHandler);
                startIndex += 1000;
            }

            long loadTime = sw.ElapsedMilliseconds;
            Action<BinaryFileHelper.LqbBinaryStream> writeHandler = delegate (BinaryFileHelper.LqbBinaryStream stream)
            {
                Serializer.Serialize(stream.Stream, list);
            };

            for (int retry = 0; retry < 5; retry++)
            {
                try
                {
                    BinaryFileHelper.OpenNew(fileName, writeHandler);
                    LogExeTime(debugString, "Execute Copy_PRICE_POLICY_v2 in ", sw.ElapsedMilliseconds, loadTime, list.Count, fileName);
                    return;
                }
                catch (DirectoryNotFoundException)
                {
                    retry++;
                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(100);
                    if (Directory.Exists(path) == false)
                    {
                        Directory.CreateDirectory(path);
                    }
                    debugString.AppendLine("    Retry #" + retry + ". File=" + fileName);
                }
            }

            throw new CBaseException("Unable to create " + fileName + " after 5 tries", "Unable to create " + fileName + " after 5 tries");
        }

        private static void GenerateSummaryFile(string path, FileBasedSnapshotSummaryInfo info)
        {
            string fileName = Path.Combine(path, "summary.json");

            var tempFilePath = SerializationHelper.JsonSerializeToFile(info);

            FileOperationHelper.Move(tempFilePath.Value, fileName);
        }

        private static string InClauses(List<Guid> idList, List<SqlParameter> listParams, int startIndex, int count)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = startIndex; (i < startIndex + count) && i < idList.Count; i++)
            {
                if (i != startIndex)
                {
                    sb.Append(",");
                }

                string paramName = "@p" + i.ToString();
                sb.Append(paramName);
                listParams.Add(new SqlParameter(paramName, idList[i]));
            }
            return sb.ToString();
        }

        private static void Copy_RATE_OPTIONS_v2(StringBuilder debugString, string path, DataSrc dataSrc)
        {
            Stopwatch sw = Stopwatch.StartNew();

            string fileName = Path.Combine(path, "RATE_OPTIONS_v2.bin");

            // 2/21/2018 - dd - Select the full table in one shot may cause SQL timeout. Need to break in chunk.
            string sql = "SELECT DISTINCT SrcRateOptionsProgId FROM LOAN_PROGRAM_TEMPLATE WHERE  (IsLpe=1 AND IsEnabled=1) or (IsMasterPriceGroup=1 AND IsMaster=1)";

            List<Guid> idList = new List<Guid>();
            Action<IDataReader> idReadHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    idList.Add((Guid)reader["SrcRateOptionsProgId"]);
                }
            };
            DBSelectUtility.ProcessDBData(dataSrc, sql, null, null, idReadHandler);

            // 2/21/2018 - dd - I don't see any reason why download time column is in separate table. Future optimization should  move it to RATE_OPTIONS table.
            sql = @"SELECT r.SrcProgId, r.LpeAcceptableRsFileId, r.LpeAcceptableRsFileVersionNumber, r.RatesProtobufContent, r1.lRateSheetDownloadStartD
                    FROM RATE_OPTIONS r WITH(NOLOCK) JOIN RATE_OPTIONS_DOWNLOAD_TIME r1 WITH(NOLOCK) ON r.SrcProgId = r1.SrcProgId
                    WHERE r.SrcProgId IN (@p0,@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8,@p9,@p10,@p11,@p12,@p13,@p14,@p15,@p16,@p17,@p18,@p19,@p20,@p21,@p22,@p23,@p24,@p25,
                      @p26,@p27,@p28,@p29,@p30,@p31,@p32,@p33,@p34,@p35,@p36,@p37,@p38,@p39,@p40,@p41,@p42,@p43,@p44,@p45,@p46,@p47,@p48,@p49,@p50,@p51,@p52,@p53,@p54,@p55,
                      @p56,@p57,@p58,@p59,@p60,@p61,@p62,@p63,@p64,@p65,@p66,@p67,@p68,@p69,@p70,@p71,@p72,@p73,@p74,@p75,@p76,@p77,@p78,@p79,@p80,@p81,@p82,@p83,@p84,@p85,
                      @p86,@p87,@p88,@p89,@p90,@p91,@p92,@p93,@p94,@p95,@p96,@p97,@p98,@p99,@p100,@p101,@p102,@p103,@p104,@p105,@p106,@p107,@p108,@p109,@p110,@p111,@p112,@p113,
                      @p114,@p115,@p116,@p117,@p118,@p119,@p120,@p121,@p122,@p123,@p124,@p125,@p126,@p127,@p128,@p129,@p130,@p131,@p132,@p133,@p134,@p135,@p136,@p137,@p138,@p139,
                      @p140,@p141,@p142,@p143,@p144,@p145,@p146,@p147,@p148,@p149,@p150,@p151,@p152,@p153,@p154,@p155,@p156,@p157,@p158,@p159,@p160,@p161,@p162,@p163,@p164,@p165,
                      @p166,@p167,@p168,@p169,@p170,@p171,@p172,@p173,@p174,@p175,@p176,@p177,@p178,@p179,@p180,@p181,@p182,@p183,@p184,@p185,@p186,@p187,@p188,@p189,@p190,@p191,
                      @p192,@p193,@p194,@p195,@p196,@p197,@p198,@p199,@p200,@p201,@p202,@p203,@p204,@p205,@p206,@p207,@p208,@p209,@p210,@p211,@p212,@p213,@p214,@p215,@p216,@p217,
                      @p218,@p219,@p220,@p221,@p222,@p223,@p224,@p225,@p226,@p227,@p228,@p229,@p230,@p231,@p232,@p233,@p234,@p235,@p236,@p237,@p238,@p239,@p240,@p241,@p242,@p243,
                      @p244,@p245,@p246,@p247,@p248,@p249,@p250,@p251,@p252,@p253,@p254,@p255,@p256,@p257,@p258,@p259,@p260,@p261,@p262,@p263,@p264,@p265,@p266,@p267,@p268,@p269,
                      @p270,@p271,@p272,@p273,@p274,@p275,@p276,@p277,@p278,@p279,@p280,@p281,@p282,@p283,@p284,@p285,@p286,@p287,@p288,@p289,@p290,@p291,@p292,@p293,@p294,@p295,
                      @p296,@p297,@p298,@p299,@p300,@p301,@p302,@p303,@p304,@p305,@p306,@p307,@p308,@p309,@p310,@p311,@p312,@p313,@p314,@p315,@p316,@p317,@p318,@p319,@p320,@p321,
                      @p322,@p323,@p324,@p325,@p326,@p327,@p328,@p329,@p330,@p331,@p332,@p333,@p334,@p335,@p336,@p337,@p338,@p339,@p340,@p341,@p342,@p343,@p344,@p345,@p346,@p347,
                      @p348,@p349,@p350,@p351,@p352,@p353,@p354,@p355,@p356,@p357,@p358,@p359,@p360,@p361,@p362,@p363,@p364,@p365,@p366,@p367,@p368,@p369,@p370,@p371,@p372,@p373,
                      @p374,@p375,@p376,@p377,@p378,@p379,@p380,@p381,@p382,@p383,@p384,@p385,@p386,@p387,@p388,@p389,@p390,@p391,@p392,@p393,@p394,@p395,@p396,@p397,@p398,@p399,
                      @p400,@p401,@p402,@p403,@p404,@p405,@p406,@p407,@p408,@p409,@p410,@p411,@p412,@p413,@p414,@p415,@p416,@p417,@p418,@p419,@p420,@p421,@p422,@p423,@p424,@p425,
                      @p426,@p427,@p428,@p429,@p430,@p431,@p432,@p433,@p434,@p435,@p436,@p437,@p438,@p439,@p440,@p441,@p442,@p443,@p444,@p445,@p446,@p447,@p448,@p449,@p450,@p451,
                      @p452,@p453,@p454,@p455,@p456,@p457,@p458,@p459,@p460,@p461,@p462,@p463,@p464,@p465,@p466,@p467,@p468,@p469,@p470,@p471,@p472,@p473,@p474,@p475,@p476,@p477,
                      @p478,@p479,@p480,@p481,@p482,@p483,@p484,@p485,@p486,@p487,@p488,@p489,@p490,@p491,@p492,@p493,@p494,@p495,@p496,@p497,@p498,@p499,@p500,@p501,@p502,@p503,
                      @p504,@p505,@p506,@p507,@p508,@p509,@p510,@p511,@p512,@p513,@p514,@p515,@p516,@p517,@p518,@p519,@p520,@p521,@p522,@p523,@p524,@p525,@p526,@p527,@p528,@p529,
                      @p530,@p531,@p532,@p533,@p534,@p535,@p536,@p537,@p538,@p539,@p540,@p541,@p542,@p543,@p544,@p545,@p546,@p547,@p548,@p549,@p550,@p551,@p552,@p553,@p554,@p555,
                      @p556,@p557,@p558,@p559,@p560,@p561,@p562,@p563,@p564,@p565,@p566,@p567,@p568,@p569,@p570,@p571,@p572,@p573,@p574,@p575,@p576,@p577,@p578,@p579,@p580,@p581,
                      @p582,@p583,@p584,@p585,@p586,@p587,@p588,@p589,@p590,@p591,@p592,@p593,@p594,@p595,@p596,@p597,@p598,@p599,@p600,@p601,@p602,@p603,@p604,@p605,@p606,@p607,
                      @p608,@p609,@p610,@p611,@p612,@p613,@p614,@p615,@p616,@p617,@p618,@p619,@p620,@p621,@p622,@p623,@p624,@p625,@p626,@p627,@p628,@p629,@p630,@p631,@p632,@p633,
                      @p634,@p635,@p636,@p637,@p638,@p639,@p640,@p641,@p642,@p643,@p644,@p645,@p646,@p647,@p648,@p649,@p650,@p651,@p652,@p653,@p654,@p655,@p656,@p657,@p658,@p659,
                      @p660,@p661,@p662,@p663,@p664,@p665,@p666,@p667,@p668,@p669,@p670,@p671,@p672,@p673,@p674,@p675,@p676,@p677,@p678,@p679,@p680,@p681,@p682,@p683,@p684,@p685,
                      @p686,@p687,@p688,@p689,@p690,@p691,@p692,@p693,@p694,@p695,@p696,@p697,@p698,@p699,@p700,@p701,@p702,@p703,@p704,@p705,@p706,@p707,@p708,@p709,@p710,@p711,
                      @p712,@p713,@p714,@p715,@p716,@p717,@p718,@p719,@p720,@p721,@p722,@p723,@p724,@p725,@p726,@p727,@p728,@p729,@p730,@p731,@p732,@p733,@p734,@p735,@p736,@p737,
                      @p738,@p739,@p740,@p741,@p742,@p743,@p744,@p745,@p746,@p747,@p748,@p749,@p750,@p751,@p752,@p753,@p754,@p755,@p756,@p757,@p758,@p759,@p760,@p761,@p762,@p763,
                      @p764,@p765,@p766,@p767,@p768,@p769,@p770,@p771,@p772,@p773,@p774,@p775,@p776,@p777,@p778,@p779,@p780,@p781,@p782,@p783,@p784,@p785,@p786,@p787,@p788,@p789,
                      @p790,@p791,@p792,@p793,@p794,@p795,@p796,@p797,@p798,@p799,@p800,@p801,@p802,@p803,@p804,@p805,@p806,@p807,@p808,@p809,@p810,@p811,@p812,@p813,@p814,@p815,
                      @p816,@p817,@p818,@p819,@p820,@p821,@p822,@p823,@p824,@p825,@p826,@p827,@p828,@p829,@p830,@p831,@p832,@p833,@p834,@p835,@p836,@p837,@p838,@p839,@p840,@p841,
                      @p842,@p843,@p844,@p845,@p846,@p847,@p848,@p849,@p850,@p851,@p852,@p853,@p854,@p855,@p856,@p857,@p858,@p859,@p860,@p861,@p862,@p863,@p864,@p865,@p866,@p867,
                      @p868,@p869,@p870,@p871,@p872,@p873,@p874,@p875,@p876,@p877,@p878,@p879,@p880,@p881,@p882,@p883,@p884,@p885,@p886,@p887,@p888,@p889,@p890,@p891,@p892,@p893,
                      @p894,@p895,@p896,@p897,@p898,@p899,@p900,@p901,@p902,@p903,@p904,@p905,@p906,@p907,@p908,@p909,@p910,@p911,@p912,@p913,@p914,@p915,@p916,@p917,@p918,@p919,
                      @p920,@p921,@p922,@p923,@p924,@p925,@p926,@p927,@p928,@p929,@p930,@p931,@p932,@p933,@p934,@p935,@p936,@p937,@p938,@p939,@p940,@p941,@p942,@p943,@p944,@p945,
                      @p946,@p947,@p948,@p949,@p950,@p951,@p952,@p953,@p954,@p955,@p956,@p957,@p958,@p959,@p960,@p961,@p962,@p963,@p964,@p965,@p966,@p967,@p968,@p969,@p970,@p971,
                      @p972,@p973,@p974,@p975,@p976,@p977,@p978,@p979,@p980,@p981,@p982,@p983,@p984,@p985,@p986,@p987,@p988,@p989,@p990,@p991,@p992,@p993,@p994,@p995,@p996,@p997,@p998,@p999)";

            List<RateOptionsV2> list = new List<RateOptionsV2>();
            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    RateOptionsV2 item = new RateOptionsV2();
                    item.Id = (Guid)reader["SrcProgId"];
                    item.RatesheetFileId = (string)reader["LpeAcceptableRsFileId"];
                    item.RatesheetFileVersionNumber = (long)reader["LpeAcceptableRsFileVersionNumber"];
                    item.DownloadTime = (DateTime)reader["lRateSheetDownloadStartD"];

                    object ratesProtobufContent = reader["RatesProtobufContent"];
                    if (ratesProtobufContent != DBNull.Value)
                    {
                        item.Rates = SerializationHelper.ProtobufDeserialize<List<RateOptionV2>>((byte[])ratesProtobufContent);
                    }

                    list.Add(item);
                }
            };

            int startIndex = 0;
            while (startIndex < idList.Count)
            {
                List<SqlParameter> parameters = new List<SqlParameter>();

                for (int i = 0; i < 1000; i++)
                {
                    Guid value = (i + startIndex) < idList.Count ? idList[i + startIndex] : Guid.Empty;
                    parameters.Add(new SqlParameter("@p" + i, value));
                }
                DBSelectUtility.ProcessDBData(dataSrc, sql, null, parameters, readHandler);
                startIndex += 1000;
            }

            long loadTime = sw.ElapsedMilliseconds;
            Action<BinaryFileHelper.LqbBinaryStream> writeHandler = delegate (BinaryFileHelper.LqbBinaryStream stream)
            {
                Serializer.Serialize(stream.Stream, list);
            };

            for (int retry = 0; retry < 5; retry++)
            {
                try
                {
                    BinaryFileHelper.OpenNew(fileName, writeHandler);
                    LogExeTime(debugString, "Execute Copy_RATE_OPTIONS_v2 in ", sw.ElapsedMilliseconds, loadTime, list.Count, fileName);
                    return;
                }
                catch (DirectoryNotFoundException)
                {
                    retry++;
                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(100);
                    if (Directory.Exists(path) == false)
                    {
                        Directory.CreateDirectory(path);
                    }
                    debugString.AppendLine("    Retry #" + retry + ". File=" + fileName);
                }
            }

            throw new CBaseException("Unable to create " + fileName + " after 5 tries", "Unable to create " + fileName + " after 5 tries");
        }

        private static void Copy_LOAN_PROGRAM_TEMPLATE(StringBuilder debugString, string path, DataSrc dataSrc)
        {
            string fileName = Path.Combine(path, "LOAN_PROGRAM_TEMPLATE.bin");

            Stopwatch sw = Stopwatch.StartNew();
            string sql = "SELECT * FROM LOAN_PROGRAM_TEMPLATE WITH(NOLOCK) WHERE (IsLpe=1 AND IsEnabled=1) or (IsMasterPriceGroup=1 AND IsMaster=1)";

            List<FileBasedLoanProgramTemplate> list = new List<FileBasedLoanProgramTemplate>();
            Dictionary<Guid, FileBasedLoanProgramTemplate> dictionary = new Dictionary<Guid, FileBasedLoanProgramTemplate>();
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {

                    FileBasedLoanProgramTemplate item = FileBasedLoanProgramTemplate.Create(reader);

                    list.Add(item);
                    dictionary.Add(item.lLpTemplateId, item);
                }
            };

            DBSelectUtility.ProcessDBData(dataSrc, sql, null, null, readHandler);

            long loadTime = sw.ElapsedMilliseconds;
            Action<BinaryFileHelper.LqbBinaryStream> writeHandler = delegate (BinaryFileHelper.LqbBinaryStream stream)
            {
                Serializer.Serialize(stream.Stream, list);
            };

            BinaryFileHelper.OpenNew(fileName, writeHandler);
            LogExeTime(debugString, "Execute Copy_LOAN_PROGRAM_TEMPLATE in ", sw.ElapsedMilliseconds, loadTime, list.Count, fileName);
        }

        private static string msToString(long ms)
        {
            TimeSpan ts = new TimeSpan(ms * 10000); // 10,000 = 1 ms
            if (ts.Days > 0)
            {
                if (ts.Hours + ts.Minutes + ts.Seconds == 0)
                {
                    return $"{ts.Days} day(s)";
                }

                return $"{ts.Days} day(s) {ts.Hours:00}:{ts.Minutes:00}:{ts.Seconds:00}";
            }
            return $"{ts.Hours:00}:{ts.Minutes:00}:{ts.Seconds:00}";
        }
    }
}
