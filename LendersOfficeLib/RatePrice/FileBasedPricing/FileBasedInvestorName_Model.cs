﻿namespace LendersOffice.RatePrice.FileBasedPricing
{
    using System;
    using ProtoBuf;

    /// <summary>
    /// Represent data in INVESTOR_NAME table.
    /// </summary>
    [ProtoContract]
    public sealed partial class FileBasedInvestorName
    {
        /// <summary>
        /// Gets or sets the investor name.
        /// </summary>
        /// <value>The investor name.</value>
        [ProtoMember(1)]
        public string InvestorName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether item is valid.
        /// </summary>
        /// <value>Item is valid.</value>
        [ProtoMember(2)]
        public bool IsValid { get; set; }

        /// <summary>
        /// Gets or sets rate lock cut off time.
        /// </summary>
        /// <value>Rate lock cut off time.</value>
        [ProtoMember(3)]
        public DateTime RateLockCutOffTime { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether use lender time zone for ratesheet expiration.
        /// </summary>
        /// <value>Use lender time zone for ratesheet expiration.</value>
        [ProtoMember(4)]
        public bool UseLenderTimezoneForRsExpiration { get; set; }

        /// <summary>
        /// Gets or sets investor id.
        /// </summary>
        /// <value>Investor id.</value>
        [ProtoMember(5)]
        public int InvestorId { get; set; }
    }
}