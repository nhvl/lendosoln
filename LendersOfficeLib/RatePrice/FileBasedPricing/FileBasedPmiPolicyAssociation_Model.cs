﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    [ProtoContract]
    public sealed partial class FileBasedPmiPolicyAssociation
    {

        [ProtoMember(1)]
        public Guid PmiProductId { get; set; }

        [ProtoMember(2)]
        public Guid PricePolicyId { get; set; }
    }
}
