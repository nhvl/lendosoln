﻿namespace LendersOffice.RatePrice.FileBasedPricing
{
    using System;
    using System.Collections.Generic;
    using Common;
    using DataAccess;
    using LendersOfficeApp.los.RatePrice;
    using Model;

    /// <summary>
    /// Price policy class. This class use PricePolicyV2Optimized as backend. PricePolicyV2Optimized stored the rules as an array of byte.
    /// Before use rule, must call LoadRules to deserialize.
    /// </summary>
    public class FileBasedPricePolicyV2 : AbstractRtPricePolicy
    {
        /// <summary>
        /// An actual content of price policy.
        /// </summary>
        private PricePolicyV2Optimized pricePolicy;

        /// <summary>
        /// A lock object.
        /// </summary>
        private object lockObject = new object();

        /// <summary>
        /// Indicate whether the rule list is parse and populate.
        /// </summary>
        private bool isRuleLoaded = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileBasedPricePolicyV2"/> class.
        /// </summary>
        /// <param name="o">An actual content of price policy.</param>
        public FileBasedPricePolicyV2(PricePolicyV2Optimized o)
        {
            this.pricePolicy = o;
        }

        /// <summary>
        /// Gets or sets mutual exclusive sorted id.
        /// </summary>
        public override string MutualExclusiveExecSortedId
        {
            get { return this.pricePolicy.MutualExclusiveExecSortedId; }
            set { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Gets or sets parent id.
        /// </summary>
        public override Guid ParentPolicyId
        {
            get { return this.pricePolicy.ParentPolicyId; }
            set { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public override string PricePolicyDescription
        {
            get { return this.pricePolicy.PricePolicyDescription; }
            set { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public override Guid PricePolicyId
        {
            get { return this.pricePolicy.PricePolicyId; }
            set { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Gets the size. This is not used.
        /// </summary>
        /// <returns>This method always return 0.</returns>
        public override int EstimateSize()
        {
            return 0;
        }

        /// <summary>
        /// Load the rules from byte array.
        /// </summary>
        public override void LoadRules()
        {
            lock (this.lockObject)
            {
                if (this.isRuleLoaded)
                {
                    return;
                }

                RtRuleList tmpRuleList = null;

                if (this.pricePolicy.Rules != null && this.pricePolicy.Rules.Count > 0)
                {
                    tmpRuleList = new RtRuleList(this.pricePolicy.Rules.Count);
                }
                else
                {
                    tmpRuleList = new RtRuleList(0);
                }

                if (this.pricePolicy.Rules != null && this.pricePolicy.Rules.Count > 0)
                {
                    List<AbstractRtRule> dtiRules = new List<AbstractRtRule>();
                    List<AbstractRtRule> variantRules = new List<AbstractRtRule>();
                    List<AbstractRtRule> qvalueRules = new List<AbstractRtRule>();

                    foreach (var bytes in this.pricePolicy.Rules)
                    {
                        var ruleModel = SerializationHelper.ProtobufDeserialize<PricePolicyRuleV2>(bytes);

                        RtRule rule = new RtRule(ruleModel);
                        switch (rule.Category)
                        {
                            case LpeKeywordCategory.Invariant:
                                tmpRuleList.Add(rule);
                                break;
                            case LpeKeywordCategory.Variant:
                                variantRules.Add(rule);
                                break;
                            case LpeKeywordCategory.Dti:
                                dtiRules.Add(rule);
                                break;
                            case LpeKeywordCategory.QValue:
                                qvalueRules.Add(rule);
                                break;
                            default:
                                throw new UnhandledEnumException(rule.Category);
                        }
                    }

                    this.FirstIdxOfQValue = tmpRuleList.Count;
                    tmpRuleList.AddRange(qvalueRules);

                    this.FirstIdxOfVariant = tmpRuleList.Count;
                    tmpRuleList.AddRange(variantRules);

                    this.FirstIdxOfDti = tmpRuleList.Count;
                    tmpRuleList.AddRange(dtiRules);
                }

                this.m_ruleList = tmpRuleList;
                this.isRuleLoaded = true;
            }
        }
    }
}
