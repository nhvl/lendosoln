﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;
using DataAccess;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    [ProtoContract]
    public sealed partial class FileBasedPmiProduct
    {
        [ProtoMember(1)]
        public Guid ProductId { get; set; }

        [ProtoMember(2)]
        public E_PmiCompanyT MICompany { get; set; }

        [ProtoMember(3)]
        public E_PmiTypeT MIType { get; set; }

        [ProtoMember(4)]
        public bool UfmipIsRefunableOnProRataBasis { get; set; }

    }
}
