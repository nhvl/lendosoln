﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using DataAccess;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    public sealed partial class FileBasedPmiProductFolder
    {
        public static FileBasedPmiProductFolder Create(IDataReader reader)
        {

            FileBasedPmiProductFolder item = new FileBasedPmiProductFolder();
            item.FolderId = reader.SafeGuid("FolderId");
            item.FolderName = reader.SafeString("FolderName");
            item.ParentFolderId = reader.SafeGuid("ParentFolderId");

            return item;
        }
    }
}
