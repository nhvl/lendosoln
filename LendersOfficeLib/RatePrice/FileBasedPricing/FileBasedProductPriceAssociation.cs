﻿using System;
using System.Collections.Generic;
using DataAccess;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    public sealed partial class FileBasedProductPriceAssociation
    {
        public void Add(Guid pricePolicyId, E_TriState assocOverrideTri, bool inheritVal, bool inheritValFromBaseProd, bool isForcedNo)
        {
            if (this.Items == null)
            {
                this.Items = new List<FileBasedProductPriceAssociationItem>();
            }

            FileBasedProductPriceAssociationItem o = new FileBasedProductPriceAssociationItem();
            o.PricePolicyId = pricePolicyId;
            o.AssocOverrideTri = assocOverrideTri;
            o.InheritVal = inheritVal;
            o.InheritValFromBaseProd = inheritValFromBaseProd;
            o.IsForcedNo = isForcedNo;

            this.Items.Add(o);
        }
    }

    public sealed partial class FileBasedProductPriceAssociationItem
    {
        public bool IsAssociate()
        {
            switch (this.AssocOverrideTri)
            {
                case E_TriState.Blank:
                    return this.InheritVal || this.InheritValFromBaseProd;
                case E_TriState.Yes:
                    return true;
                case E_TriState.No:
                    return false;
                default:
                    throw new UnhandledEnumException(this.AssocOverrideTri);
            }
        }
    }
}