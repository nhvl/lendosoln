﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccess;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    public sealed partial class FileBasedPmiProduct
    {
        public static IEnumerable<FileBasedPmiProduct> CreateFrom(DataRowCollection collection)
        {
            List<FileBasedPmiProduct> list = new List<FileBasedPmiProduct>();

            foreach (DataRow row in collection)
            {
                FileBasedPmiProduct item = new FileBasedPmiProduct();
                item.ProductId = (Guid)row["ProductId"];
                item.MIType = (E_PmiTypeT)row["MIType"];
                item.MICompany = (E_PmiCompanyT)row["MICompany"];
                item.UfmipIsRefunableOnProRataBasis = (bool)row["UfmipIsRefundableOnProRataBasis"];

                list.Add(item);

            }
            return list;
        }

        public static FileBasedPmiProduct Create(IDataReader reader)
        {
            FileBasedPmiProduct item = new FileBasedPmiProduct();
            item.ProductId = (Guid)reader["ProductId"];
            item.MIType = (E_PmiTypeT)((int)reader["MIType"]);
            item.MICompany = (E_PmiCompanyT)((int)reader["MICompany"]);
            item.UfmipIsRefunableOnProRataBasis = (bool)reader["UfmipIsRefundableOnProRataBasis"];
            return item;
        }
    }
}
