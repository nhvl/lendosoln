﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    [ProtoContract]
    public sealed partial class FileBasedPmiProductFolder
    {
        [ProtoMember(1)]
        public Guid FolderId { get; set; }

        [ProtoMember(2)]
        public string FolderName { get; set; }

        [ProtoMember(3)]
        public Guid ParentFolderId { get; set; }
    }
}
