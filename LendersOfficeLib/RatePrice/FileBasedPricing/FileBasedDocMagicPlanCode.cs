﻿namespace LendersOffice.RatePrice.FileBasedPricing
{
    using System;
    using System.Data;
    using DataAccess;

    /// <summary>
    /// Represent data in PRODUCT table.
    /// </summary>
    public sealed partial class FileBasedDocMagicPlanCode
    {
        /// <summary>
        /// Create object from sql.
        /// </summary>
        /// <param name="reader">Sql row contains data.</param>
        /// <returns>An object.</returns>
        public static FileBasedDocMagicPlanCode Create(IDataReader reader)
        {
            FileBasedDocMagicPlanCode item = new FileBasedDocMagicPlanCode();

            item.InvestorNm = reader.SafeString("InvestorNm");
            item.PlanCodeDesc = reader.SafeString("PlanCodeDesc");
            item.PlanCodeNm = reader.SafeString("PlanCodeNm");
            item.PlanCodeId = (Guid)reader["PlanCodeId"];
            item.RuleXmlContent = reader.SafeString("RuleXmlContent");

            return item;
        }
    }
}