﻿using System;
using System.Collections;
using System.Collections.Generic;
using LendersOffice.Common;
using LendersOfficeApp.los.RatePrice;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    public class FileBasedPolicyRelationCenter : IPolicyRelationDb
    {
        Dictionary<Guid, AbstractRtPricePolicy> m_policyDictionary;
        DependencySet<Guid> m_pricePolicyDependencySet = new DependencySet<Guid>(new GuidComparer());

        public FileBasedPolicyRelationCenter(IEnumerable<AbstractRtPricePolicy> policyList)
        {
            m_policyDictionary = new Dictionary<Guid, AbstractRtPricePolicy>();

            foreach (var o in policyList) {
                m_policyDictionary.Add(o.PricePolicyId, o);
                m_pricePolicyDependencySet.Add(o.ParentPolicyId, o.PricePolicyId);
            }
        }

        #region IPolicyRelationDb Members

        public void AddDescendantsToSet(Guid policyId, ICollection<Guid> descendants)
        {
            descendants.Add(policyId);
            foreach (var o in m_pricePolicyDependencySet.GetDependsOn(new Guid[] { policyId }))
            {
                descendants.Add(o);
            }
        }

        public ArrayList GetAncestors(Guid policyId)
        {
            ArrayList list = new ArrayList();

            foreach (var id in m_pricePolicyDependencySet.GetUsedBy(new Guid[] { policyId }))
            {
                if (id != Guid.Empty)
                {
                    list.Add(id);
                }
            }
            return list;

        }
        #endregion

        public AbstractRtPricePolicy GetPricePolicy(Guid policyId)
        {
            return m_policyDictionary[policyId];
        }
    }
}
