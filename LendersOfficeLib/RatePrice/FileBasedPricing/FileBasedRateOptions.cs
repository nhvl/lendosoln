﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using LendersOffice.Common;
using LendersOffice.RatePrice.Model;
using LendersOfficeApp.los.RatePrice;
using LqbGrammar.DataTypes;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    public sealed partial class FileBasedRateOptions
    {
        public static FileBasedRateOptions Create(FileBasedRateOptionPriceGroupProgram readonlyItem)
        {
            // expect: after call this function, nobody will modifies readonlyItem's member data. Otherwise, we need clone RateItemList
            FileBasedRateOptions rateOptions = new FileBasedRateOptions();
            rateOptions.LpeAcceptableRsFileVersionNumber = readonlyItem.LpeAcceptableRsFileVersionNumber;
            rateOptions.LpeAcceptableRsFileId = readonlyItem.LpeAcceptableRsFileId;
            rateOptions.lRateSheetDownloadStartD = readonlyItem.lRateSheetDownloadStartD;
            rateOptions.lRateSheetDownloadEndD = readonlyItem.lRateSheetDownloadEndD;
            rateOptions.RateItemList = readonlyItem.RateItemList;
            rateOptions.loadedContent = true;

            return rateOptions;
        }

        public static FileBasedRateOptions Create(RateOptionsV2 item)
        {
            FileBasedRateOptions rateOptions = new FileBasedRateOptions();
            rateOptions.LpeAcceptableRsFileVersionNumber = item.RatesheetFileVersionNumber;
            rateOptions.LpeAcceptableRsFileId = item.RatesheetFileId;
            rateOptions.lRateSheetDownloadStartD = item.DownloadTime;
            rateOptions.lRateSheetDownloadEndD = item.DownloadTime;

            if (item.Rates != null)
            {
                rateOptions.RateItemList = new List<FileBasedRateItem>();

                foreach (var rate in item.Rates)
                {
                    rateOptions.RateItemList.Add(new FileBasedRateItem(rate));
                }
            }

            rateOptions.loadedContent = true;

            return rateOptions;
        }

        /// <summary>
        /// return true if need to load content.
        /// </summary>
        public bool LoadContentIfNeed(ConcurrentDictionary<string, Model.RateOptions> cache)
        {
            if (this.loadedContent)
            {
                return false;
            }

            // dd 7/23/2018 - Only get here if the snapshot contains old version of RateOptions (using FileDB to store actual data). These old versions only show up in snapshot before March 2018.
            using (PerformanceStopwatch.Start("FileBasedRateOptions.LoadContentIfNeed"))
            {
                lock (this.lockObject)
                {
                    if (this.loadedContent)
                    {
                        return false;
                    }

                    Model.RateOptions rateOptionsModel = null;
                    if (cache == null || !cache.TryGetValue(this.ContentKeyString, out rateOptionsModel))
                    {
                        var contentKey = SHA256Checksum.Create(this.ContentKeyString);
                        if (contentKey == null)
                        {
                            throw new FileContentKeyNotFoundException($"The FileBasedRateOptions {this.SrcProgId} has invalid contentKey '{this.ContentKeyString}'.");
                        }

                        LocalFilePath rateOptFname = Utils.RetrieveFromPriceEngineStorage(PriceEngineStorageType.RateOptions, contentKey.Value);
                        rateOptionsModel = SerializationHelper.JsonDeserializeFromFile<Model.RateOptions>(rateOptFname);
                        if (cache != null)
                        {
                            cache.TryAdd(this.ContentKeyString, rateOptionsModel);
                        }
                    }

                    this.LpeAcceptableRsFileVersionNumber = rateOptionsModel.RatesheetFileVersionNumber;
                    this.LpeAcceptableRsFileId = rateOptionsModel.RatesheetFileId;
                    this.lRateSheetDownloadStartD = rateOptionsModel.DownloadTimeInUtc.ToLocalTime();
                    this.lRateSheetDownloadEndD = this.lRateSheetDownloadStartD;
                    this.RateItemList = new List<FileBasedRateItem>();

                    foreach (var rateModel in rateOptionsModel.Rates)
                    {
                        RateItemList.Add(new FileBasedRateItem(rateModel));
                    }

                    this.loadedContent = true;
                    return true;
                }
            }
        }

        private bool loadedContent;
        private object lockObject = new object();
    }
}
