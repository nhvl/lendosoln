﻿namespace LendersOffice.RatePrice.FileBasedPricing
{
    using System;
    using ProtoBuf;

    /// <summary>
    /// Represent data in PRODUCT table.
    /// </summary>
    [ProtoContract]
    public sealed partial class FileBasedDocMagicPlanCode
    {
        /// <summary>
        /// Gets or sets the plan code id.
        /// </summary>
        /// <value>The plan code id.</value>
        [ProtoMember(1)]
        public Guid PlanCodeId { get; set; }

        /// <summary>
        /// Gets or sets the plan code name.
        /// </summary>
        /// <value>The plan code name.</value>
        [ProtoMember(2)]
        public string PlanCodeNm { get; set; }

        /// <summary>
        /// Gets or sets the plan code description.
        /// </summary>
        /// <value>The plan code description.</value>
        [ProtoMember(3)]
        public string PlanCodeDesc { get; set; }

        /// <summary>
        /// Gets or sets the investor name.
        /// </summary>
        /// <value>The investor name.</value>
        [ProtoMember(4)]
        public string InvestorNm { get; set; }

        /// <summary>
        /// Gets or sets the rule xml content.
        /// </summary>
        /// <value>The rule xml content.</value>
        [ProtoMember(5)]
        public string RuleXmlContent { get; set; }
    }
}