﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    [ProtoContract]
    public sealed partial class FileBasedRateOptionPriceGroupProgram
    {
        [ProtoMember(1)]
        public Guid LpePriceGroupId { get; set; }
        
        [ProtoMember(2)]
        public Guid lLpTemplateId { get; set; }

        [ProtoMember(3)]
        public List<FileBasedRateItem> RateItemList { get; set; }

        [ProtoMember(4)]
        public byte[] VersionTimestamp { get; set; }

        [ProtoMember(5)]
        public long LpeAcceptableRsFileVersionNumber { get; set; }

        [ProtoMember(6)]
        public string LpeAcceptableRsFileId { get; set; }

        [ProtoMember(7)]
        public DateTime lRateSheetDownloadStartD { get; set; }

        [ProtoMember(8)]
        public DateTime lRateSheetDownloadEndD { get; set; }
    }
}
