﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Data;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    public sealed partial class FileBasedRateOptionPriceGroupProgram
    {

        public static FileBasedRateOptionPriceGroupProgram Create(IDataReader reader)
        {
            FileBasedRateOptionPriceGroupProgram item = new FileBasedRateOptionPriceGroupProgram();

            item.LpePriceGroupId = reader.SafeGuid("LpePriceGroupId");
            item.lLpTemplateId = reader.SafeGuid("lLpTemplateId");
            item.RateItemList = FileBasedRateItem.Parse_lRateSheetXmlContent(reader.SafeString("lRateSheetXmlContent"));
            item.VersionTimestamp = (byte[])reader["VersionTimestamp"];
            item.LpeAcceptableRsFileId = reader.SafeString("LpeAcceptableRsFileId");
            item.LpeAcceptableRsFileVersionNumber = (long)reader["LpeAcceptableRsFileVersionNumber"];

            item.lRateSheetDownloadStartD = reader.SafeDateTime("lRateSheetDownloadStartD");
            item.lRateSheetDownloadEndD = reader.SafeDateTime("lRateSheetDownloadEndD");
            return item;
        }
    }
}
