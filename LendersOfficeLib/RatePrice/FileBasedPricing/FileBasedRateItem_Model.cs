﻿using DataAccess;
using ProtoBuf;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    [ProtoContract]
    public sealed partial class FileBasedRateItem
    {

        [ProtoMember(1)]
        public string __Rate { get; set; }
        [ProtoMember(2)]
        public string __Point { get; set; }
        [ProtoMember(3)]
        public string __Margin { get; set; }
        [ProtoMember(4)]
        public string __QRateBase { get; set; }
        [ProtoMember(5)]
        public string __TeaserRate { get; set; }

        public FileBasedRateItem(Model.RateOption rateOptModel)
        {
            this.__Rate = rateOptModel.Rate;
            this.__Point = rateOptModel.Point;
            this.__Margin = rateOptModel.Margin;
            this.__QRateBase = rateOptModel.QRateBase;
            this.__TeaserRate = rateOptModel.TeaserRate;
        }

        public FileBasedRateItem()
        {
        }

        internal FileBasedRateItem(Model.RateOptionV2 rateModel)
        {
            if (rateModel.ItemType == E_RateSheetKeywordT.Lock)
            {
                this.__Rate = "LOCK";
            }
            else if (rateModel.ItemType == E_RateSheetKeywordT.IO)
            {
                this.__Rate = "IO";
            }
            else if (rateModel.ItemType == E_RateSheetKeywordT.Prepay)
            {
                this.__Rate = "PP";
            }
            else
            {
                this.__Rate = rateModel.Rate.ToString();
            }

            this.__Point = rateModel.Point.ToString();
            this.__Margin = rateModel.Margin.ToString();
            this.__QRateBase = rateModel.QRateBase.ToString();
            this.__TeaserRate = rateModel.TeaserRate.ToString();
        }
    }
}
