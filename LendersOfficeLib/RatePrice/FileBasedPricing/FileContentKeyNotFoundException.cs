﻿namespace LendersOffice.RatePrice.FileBasedPricing
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    // ThienToDo: base class will be CBaseException when "remove"  ConstSite.EnableFilebasedCmp
    // Note: UnderwritingWorkerUnit.CmpWithHistoryOption() will rethrow any exception that is not  SystemException subclass => "main" pricing failed.

    /// <summary>
    /// FileBasedRtPricePolicy, FileBasedRateOptions ... have contentKey.
    /// Throw this exception if contentKey is invalid or can not load fileDB from contentKey.
    /// </summary>
    internal class FileContentKeyNotFoundException : FileNotFoundException  // will replace FileNotFoundException by CBaseException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FileContentKeyNotFoundException" /> class.
        /// </summary>
        /// <param name="msg"> The error message.</param>
        public FileContentKeyNotFoundException(string msg) : base(msg)
        {
        }
    }
}
