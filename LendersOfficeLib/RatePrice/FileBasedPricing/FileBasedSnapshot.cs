﻿namespace LendersOffice.RatePrice.FileBasedPricing
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;

    using CommonProjectLib.Caching;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOfficeApp.los.RatePrice;
    using LqbGrammar.DataTypes;
    using ProtoBuf;
    using System.Threading;
    using Model;
    using System.IO.Compression;

    /// <summary>
    /// 1/16/2017 - dd - Store all the possibles of price policy + association combo in dictionary.
    /// This will help with memory usage compare with create a new object for each price policy + association.
    /// </summary>
    internal sealed class SimplifyProductPriceAssociationItem
    {
        private static ConcurrentDictionary<Guid, SimplifyProductPriceAssociationItem> AssociateDictionary = new ConcurrentDictionary<Guid, SimplifyProductPriceAssociationItem>();
        private static ConcurrentDictionary<Guid, SimplifyProductPriceAssociationItem> UnassociateDictionary = new ConcurrentDictionary<Guid, SimplifyProductPriceAssociationItem>();

        public Guid PricePolicyId { get; private set; }
        public bool IsAssociate { get; private set; }

        public static SimplifyProductPriceAssociationItem Create(Guid pricePolicyId, bool isAssociate)
        {
            ConcurrentDictionary<Guid, SimplifyProductPriceAssociationItem> dictionary = isAssociate ? AssociateDictionary : UnassociateDictionary;

            SimplifyProductPriceAssociationItem item = null;
            if (!dictionary.TryGetValue(pricePolicyId, out item))
            {
                item = new SimplifyProductPriceAssociationItem();
                item.PricePolicyId = pricePolicyId;
                item.IsAssociate = isAssociate;

                dictionary.TryAdd(pricePolicyId, item);
            }

            return item;
        }
    }

    internal sealed class SimplifyProductPriceAssociation
    {
        public Guid ProductId { get; set; }

        public List<SimplifyProductPriceAssociationItem> Items { get; set; }

        internal static SimplifyProductPriceAssociation CreateFrom(FileBasedProductPriceAssociation productPriceAssociation)
        {
            SimplifyProductPriceAssociation obj = new SimplifyProductPriceAssociation();
            obj.ProductId = productPriceAssociation.ProductId;
            obj.Items = new List<SimplifyProductPriceAssociationItem>();

            foreach (var o in productPriceAssociation.Items)
            {
                obj.Items.Add(SimplifyProductPriceAssociationItem.Create(o.PricePolicyId, o.IsAssociate()));
            }

            return obj;
        }
    }

    internal sealed class ReadOnlyPricePolicyDependencySet
    {
        private DependencySet<Guid> internalDependencySet = null;
        private ConcurrentDictionary<Guid, IEnumerable<Guid>> cache = new ConcurrentDictionary<Guid, IEnumerable<Guid>>();

        public ReadOnlyPricePolicyDependencySet(DependencySet<Guid> set)
        {
            this.internalDependencySet = set;
        }

        public IEnumerable<Guid> GetUsedBy(Guid id)
        {
            IEnumerable<Guid> list = null;

            if (cache.TryGetValue(id, out list) == false)
            {
                list = this.internalDependencySet.GetUsedBy(new Guid[] { id });
                this.cache.TryAdd(id, list);
            }

            return list;
        }
    }

    public sealed class FileBasedSnapshot
    {
        private static readonly TimeSpan SlidingExpiration = TimeSpan.FromMinutes(60); // 2017-01-05 - dd - Default for cache sliding period.
        static private int objCounter = 0;

        static private int DecrementCounter()
        {
            return Interlocked.Decrement(ref objCounter);
        }

        static internal int GetCounter()
        {
            return objCounter;
        }

        private FileBasedSnapshotGoodBye goodbyeMsg;

        private bool isUsePricePolicyVersion2 = false;
        private bool isUseRateOptionVersion2 = false;
        private Guid brokerId;

        public DateTime Version { get; private set; }

        public SHA256Checksum FileKey { get; private set; }

        private FileBasedSnapshot(SHA256Checksum fileKey, Guid brokerId)
        {
            using (PerformanceStopwatch.Start($"FileBasedSnapshot.ctor: {fileKey.Value}"))
            {
                Stopwatch sw = Stopwatch.StartNew();
                this.FileKey = fileKey;
                this.brokerId = brokerId;

                Initialize();
                sw.Stop();
                int numObj = Interlocked.Increment(ref objCounter);
                goodbyeMsg = new FileBasedSnapshotGoodBye($"Destroy FilebasedSnapshot {fileKey.Value}, broker {brokerId}");

                Tools.LogInfo($"FileBasedSnapshot initialized in {sw.ElapsedMilliseconds:N0}ms: {fileKey.Value}. #FileBasedSnapshot: {numObj}");
            }
        }

        private void Initialize()
        {
            string path = Path.Combine(ConstApp.TempFolder, Guid.NewGuid().ToString());
            SHA256Checksum snapshotId = SHA256Checksum.Invalid;

            try
            {
                // load snapshot/subsnapshot
                if (this.brokerId == Guid.Empty)
                {
                    LocalFilePath snapshotLocalPath;

                    using (PerformanceStopwatch.Start($"FileBasedSnapshot::RetrieveFromPriceEngineStorage: {this.FileKey.Value}"))
                    {
                        snapshotLocalPath = Utils.RetrieveFromPriceEngineStorage(PriceEngineStorageType.PricingSnapshot, this.FileKey);
                    }

                    using (PerformanceStopwatch.Start("FileBasedSnapshot::DecompressIOCompressedFile()"))
                    {
                        CompressionHelper.DecompressIOCompressedFile(new FileInfo(snapshotLocalPath.Value), new DirectoryInfo(path), deleteExisting: true);
                    }
                }
                else
                {
                    snapshotId = GetSubsnapshotId(this.FileKey.Value, brokerId);
                    LocalFilePath snapshotLocalPath;

                    using (PerformanceStopwatch.Start($"FileBasedSnapshot::Retrieve Subsnapshot FromPriceEngineStorage: {snapshotId.Value}"))
                    {
                        snapshotLocalPath = Utils.RetrieveFromPriceEngineStorage(PriceEngineStorageType.PerLenderSnapshot, snapshotId);
                    }

                    using (PerformanceStopwatch.Start("FileBasedSnapshot::DecompressIOCompressedFile()"))
                    {
                        CompressionHelper.DecompressIOCompressedFile(new FileInfo(snapshotLocalPath.Value), new DirectoryInfo(path), deleteExisting: true);
                    }

                    // load PRICE_POLICY_v2.bin by PricePolicySnapshotLoader

                    var snapshotInfoFname = LocalFilePath.Create(Path.Combine(path, "SnapshotInfo.json"));
                    SnapshotInfo snapshotInfo = SerializationHelper.JsonDeserializeFromFile<SnapshotInfo>(snapshotInfoFname.Value);

                    string policyCacheKey = snapshotInfo.PolicyChecksum + "_policySnapshot";
                    var policyLoader = MemoryCacheUtilities.GetOrAddExisting(policyCacheKey,
                                                                                () => new PricePolicySnapshotLoader(SHA256Checksum.Create(snapshotInfo.PolicyChecksum).Value),
                                                                                SlidingExpiration);
                    m_pricePolicyDictionary = policyLoader.PricePolicyDictionary;
                }

                if (File.Exists(Path.Combine(path, "PRICE_POLICY_v2.bin")))
                {
                    isUsePricePolicyVersion2 = true;
                }

                if (File.Exists(Path.Combine(path, "RATE_OPTIONS_v2.bin")))
                {
                    isUseRateOptionVersion2 = true;
                }

                using (PerformanceStopwatch.Start($"FileBasedSnapshot::LoadToMemory: {this.FileKey.Value}"))
                {
                    ReadVersion(path);

                    using (PerformanceStopwatch.Start("FileBasedSnapshot::Load(LOAN_PROGRAM_TEMPLATE)"))
                    {
                        ExtractToRawDictionary(path, "LOAN_PROGRAM_TEMPLATE.bin", m_loanProgramDictionary);
                    }

                    if (this.brokerId != Guid.Empty)
                    {
                        // already load by PricePolicySnapshotLoader
                    }
                    else if (isUsePricePolicyVersion2)
                    {
                        using (PerformanceStopwatch.Start("FileBasedSnapshot::Load(PRICE_POLICY_v2)"))
                        {
                            ExtractPricePolicyVersion2(path, "PRICE_POLICY_v2.bin", m_pricePolicyDictionary);
                        }
                    }
                    else
                    {
                        using (PerformanceStopwatch.Start("FileBasedSnapshot::Load(PRICE_POLICY)"))
                        {
                            ExtractPricePolicyVersion1(path, "PRICE_POLICY.bin", m_pricePolicyDictionary);
                        }
                    }

                    if (isUseRateOptionVersion2)
                    {
                        using (PerformanceStopwatch.Start("FileBasedSnapshot::Load(RATE_OPTIONS_v2)"))
                        {
                            m_rawRateOptionsV2Dictionary = new Dictionary<Guid, byte[]>();
                            ExtractToRawDictionary(path, "RATE_OPTIONS_v2.bin", m_rawRateOptionsV2Dictionary);
                        }
                    }
                    else
                    {
                        using (PerformanceStopwatch.Start("FileBasedSnapshot::Load(RATE_OPTIONS)"))
                        {
                            ExtractToDictionary(path, "RATE_OPTIONS.bin", m_rateOptionsDictionary, o => o.SrcProgId);
                        }
                    }

                    ExtractToDictionary(path, "PMI_PRODUCT.bin", m_pmiProductDictionary, o => o.ProductId);

                    using (PerformanceStopwatch.Start($"FileBasedSnapshot::Load(PRODUCT_PRICE_ASSOCIATION)"))
                    {
                        if (this.brokerId == Guid.Empty)
                        {
                            ExtractPricePolicyAssociation(path, "PRODUCT_PRICE_ASSOCIATION.bin", m_productPriceAssociationDictionary);
                        }
                        else
                        {
                            ExtractPricePolicyAssociationV2(path, "PRODUCT_PRICE_ASSOCIATION_v2.bin", m_productPriceAssociationDictionary);
                        }
                    }

                    ExtractToList(path, "PMI_POLICY_ASSOC.bin", m_pmiPolicyAssociationList);
                    ExtractToList(path, "PRICEGROUP_PROG_POLICY_ASSOC.bin", m_priceGroupProgPolicyAssocList);
                    ExtractToList(path, "RATE_OPTION_PRICEGROUP_PROGRAM.bin", m_rateOtionPriceGroupProgramList);
                    ExtractToList(path, "PRODUCT.bin", m_productList);
                    ExtractToList(path, "INVESTOR_NAME.bin", m_investorNameList);
                    ExtractToList(path, "PRODUCT_PAIRING.bin", m_productPairingList);
                    ExtractToList(path, "DOC_MAGIC_PLAN_CODE.bin", m_docMagicPlanCodeList);
                }

                PrecomputeReadOnlyPricePolicySet();
            }
            catch (Exception exc)
            {
                Tools.LogError($"FilebasedSnapshot::Initialize() Error: snapshotId '{this.FileKey.Value}', brokerId '{this.brokerId}', subSnapshotId '{snapshotId.Value}'", exc);
                throw;
            }

            try
            {
                Directory.Delete(path, true);
            }
            catch(SystemException)
            {
            }
        }

        private void PrecomputeReadOnlyPricePolicySet()
        {
            // Precompute price policy dependency for performance.
            DependencySet<Guid> dependencySet = new DependencySet<Guid>(new GuidComparer());
            foreach (var o in this.m_pricePolicyDictionary.Values)
            {
                dependencySet.Add(o.ParentPolicyId, o.PricePolicyId);
            }

            this.m_pricePolicyDependencySet = new ReadOnlyPricePolicyDependencySet(dependencySet);
        }

        private Dictionary<Guid, byte[]> m_loanProgramDictionary = new Dictionary<Guid, byte[]>();
        private Dictionary<Guid, byte[]> m_rawRateOptionsV2Dictionary = null;

        private Dictionary<Guid, AbstractRtPricePolicy> m_pricePolicyDictionary = new Dictionary<Guid, AbstractRtPricePolicy>();
        private Dictionary<Guid, FileBasedRateOptions> m_rateOptionsDictionary = new Dictionary<Guid, FileBasedRateOptions>();
        private Dictionary<Guid, FileBasedPmiProduct> m_pmiProductDictionary = new Dictionary<Guid, FileBasedPmiProduct>();
        private Dictionary<Guid, SimplifyProductPriceAssociation> m_productPriceAssociationDictionary = new Dictionary<Guid, SimplifyProductPriceAssociation>();

        private List<FileBasedRateOptionPriceGroupProgram> m_rateOtionPriceGroupProgramList = new List<FileBasedRateOptionPriceGroupProgram>();
        private List<FileBasedPriceGroupProgPolicyAssoc> m_priceGroupProgPolicyAssocList = new List<FileBasedPriceGroupProgPolicyAssoc>();
        private List<FileBasedPmiPolicyAssociation> m_pmiPolicyAssociationList = new List<FileBasedPmiPolicyAssociation>();
        private List<FileBasedProduct> m_productList = new List<FileBasedProduct>();
        private List<FileBasedInvestorName> m_investorNameList = new List<FileBasedInvestorName>();
        private List<FileBasedProductPairing> m_productPairingList = new List<FileBasedProductPairing>();
        private List<FileBasedDocMagicPlanCode> m_docMagicPlanCodeList = new List<FileBasedDocMagicPlanCode>();
        private ReadOnlyPricePolicyDependencySet m_pricePolicyDependencySet = null;

        private static void ExtractPricePolicyVersion1(string path, string fileName, Dictionary<Guid, AbstractRtPricePolicy> dictionary)
        {
            string fullPath = Path.Combine(path, fileName);

            Action<BinaryFileHelper.LqbBinaryStream> readHandler = delegate (BinaryFileHelper.LqbBinaryStream stream)
            {
                foreach (var o in Serializer.DeserializeItems<FileBasedRtPricePolicy>(stream.Stream, PrefixStyle.Base128, 1))
                {
                    dictionary.Add(o.PricePolicyId, o);
                }
            };

            BinaryFileHelper.OpenRead(fullPath, readHandler);
        }

        private static void ExtractPricePolicyVersion2(string path, string fileName, Dictionary<Guid, AbstractRtPricePolicy> dictionary)
        {
            string fullPath = Path.Combine(path, fileName);

            Action<BinaryFileHelper.LqbBinaryStream> readHandler = delegate (BinaryFileHelper.LqbBinaryStream stream)
            {
                foreach (var o in Serializer.DeserializeItems<PricePolicyV2Optimized>(stream.Stream, PrefixStyle.Base128, 1))
                {

                    dictionary.Add(o.PricePolicyId, new FileBasedPricePolicyV2(o));
                }
            };

            BinaryFileHelper.OpenRead(fullPath, readHandler);
        }

        private static void ExtractToDictionary<T>(string path, string fileName, Dictionary<Guid, T> dictionary, Func<T, Guid> getKey)
        {
            string fullPath = Path.Combine(path, fileName);

            Action<BinaryFileHelper.LqbBinaryStream> readHandler = delegate (BinaryFileHelper.LqbBinaryStream stream)
            {
                foreach (var o in Serializer.DeserializeItems<T>(stream.Stream, PrefixStyle.Base128, 1))
                {
                    dictionary.Add(getKey(o), o);
                }
            };

            BinaryFileHelper.OpenRead(fullPath, readHandler);
        }

        private void ExtractPricePolicyAssociation(string path, string fileName, Dictionary<Guid, SimplifyProductPriceAssociation> dictionary)
        {
            string fullPath = Path.Combine(path, fileName);

            Action<BinaryFileHelper.LqbBinaryStream> readHandler = delegate(BinaryFileHelper.LqbBinaryStream stream)
            {
                foreach (var o in Serializer.DeserializeItems<FileBasedProductPriceAssociation>(stream.Stream, PrefixStyle.Base128, 1))
                {
                    dictionary.Add(o.ProductId, SimplifyProductPriceAssociation.CreateFrom(o));
                }
            };

            BinaryFileHelper.OpenRead(fullPath, readHandler);
        }

        private void ExtractPricePolicyAssociationV2(string path, string fileName, Dictionary<Guid, SimplifyProductPriceAssociation> dictionary)
        {
            var table = ExtractToObject<ProductPriceAssociationTable>(path, fileName);
            foreach(ProductPriceAssociationV2 item in table.Items)
            {
                var obj = item.ToFileBasedProductPriceAssociation(table.Idx2Guid);
                dictionary.Add(obj.ProductId, SimplifyProductPriceAssociation.CreateFrom(obj));
            }
        }

        static private void ExtractToList<T>(string path, string fileName, List<T> list)
        {
            string fullPath = Path.Combine(path, fileName);

            if (!FileOperationHelper.Exists(fullPath))
            {
                return;
            }

            Action<BinaryFileHelper.LqbBinaryStream> readHandler = delegate(BinaryFileHelper.LqbBinaryStream stream)
            {
                foreach (var o in Serializer.DeserializeItems<T>(stream.Stream, PrefixStyle.Base128, 1))
                {
                    list.Add(o);
                }
            };

            BinaryFileHelper.OpenRead(fullPath, readHandler);
        }

        static private T ExtractToObject<T>(string path, string fileName)
        {
            string fullPath = Path.Combine(path, fileName);
            T item = default(T);

            Action<BinaryFileHelper.LqbBinaryStream> readHandler = delegate (BinaryFileHelper.LqbBinaryStream stream)
            {
                item = Serializer.Deserialize<T>(stream.Stream);
            };

            BinaryFileHelper.OpenRead(fullPath, readHandler);
            return item;
        }

        private void ReadVersion(string path)
        {
            string summaryJsonFile = Path.Combine(path, "summary.json");

            if (!FileOperationHelper.Exists(summaryJsonFile))
            {
                this.Version = DateTime.MinValue;
                return;
            }

            var jsonPath = LocalFilePath.Create(summaryJsonFile);
            FileBasedSnapshotSummaryInfo info = SerializationHelper.JsonDeserializeFromFile<FileBasedSnapshotSummaryInfo>(jsonPath.Value);
            this.Version = info.DataLastModifiedDateInUtc.ToLocalTime();
        }

        private static void ExtractToRawDictionary(string path, string fileName, Dictionary<Guid, byte[]> dictionary) // will convert to generic method if need.
        {
            string fullPath = Path.Combine(path, fileName);

            Action<BinaryFileHelper.LqbBinaryStream> readHandler = delegate(BinaryFileHelper.LqbBinaryStream stream)
            {
                Guid id;
                byte[] protoBufData;
                while (ReadRawBytes(stream.Stream, out id, out protoBufData))
                {
                    dictionary.Add(id, protoBufData);
                }
            };

            BinaryFileHelper.OpenRead(fullPath, readHandler);
        }

        /// <summary>
        /// Read object as raw bytes of the protobuf. The first field must be a guid id.
        /// </summary>
        /// <param name="stream">Protobuf stream source.</param>
        /// <param name="id">The id of the object.</param>
        /// <param name="buffer">The content of the object.</param>
        /// <returns>Whether parse was successful.</returns>
        private static bool ReadRawBytes(Stream stream, out Guid id, out byte[] buffer)
        {
            id = Guid.Empty;
            buffer = null;

            int fieldNumber;

            int length = ProtoReader.ReadLengthPrefix(stream, true /* expect header */, PrefixStyle.Base128, out fieldNumber);
            if (length < 0)
            {
                return false;
            }

            buffer = new byte[length];
            ProtoReader.DirectReadBytes(stream, buffer, 0, length);

            var typeModel = ProtoBuf.Meta.RuntimeTypeModel.Default;
            using (MemoryStream ms = new MemoryStream(buffer))
            {
                using (var protoReader = new ProtoReader(ms, typeModel, null))
                {
                    protoReader.ReadFieldHeader(); // Id must be the first field header with type guid.
                    id = BclHelpers.ReadGuid(protoReader);
                }
            }

            return true;
        }

        private static T DeserializeFromMemory<T>(byte[] buf)
        {
            using (var stream = new MemoryStream(buf))
            {
                return Serializer.Deserialize<T>(stream); ;
            }
        }
        

        internal IEnumerable<Guid> GetAncestorPricePolicyIdList(Guid pricePolicyId)
        {
            return this.m_pricePolicyDependencySet.GetUsedBy(pricePolicyId);
        }

        public AbstractRtPricePolicy GetPricePolicy(Guid pricePolicyId)
        {
            return m_pricePolicyDictionary[pricePolicyId];
        }

        internal bool TryGetLoanProgramTemplate(Guid id, out FileBasedLoanProgramTemplate item)
        {
            byte[] buf;
            if (!this.m_loanProgramDictionary.TryGetValue(id, out buf))
            {
                item = null;
                return false;
            }

            item = DeserializeFromMemory<FileBasedLoanProgramTemplate>(buf);
            return true;
        }

        internal bool TryGetRateOptions(Guid id, out FileBasedRateOptions item)
        {
            if (this.isUseRateOptionVersion2)
            {
                byte[] buf;

                if (!this.m_rawRateOptionsV2Dictionary.TryGetValue(id, out buf))
                {
                    item = null;
                    return false;
                }
                var rateOption = DeserializeFromMemory<RateOptionsV2>(buf);

                item = FileBasedRateOptions.Create(rateOption);
                return true;
            }
            else
            {
                return this.m_rateOptionsDictionary.TryGetValue(id, out item);
            }
        }

        internal IEnumerable<FileBasedRateOptionPriceGroupProgram> GetRateOptionPriceGroupProgramList()
        {
            return this.m_rateOtionPriceGroupProgramList;
        }

        internal IEnumerable<AbstractRtPricePolicy> GetPricePolicyList()
        {
            return this.m_pricePolicyDictionary.Values;
        }

        internal IEnumerable<FileBasedPmiProduct> GetPmiProductList()
        {
            return this.m_pmiProductDictionary.Values;
        }

        internal IEnumerable<FileBasedPmiPolicyAssociation> GetPmiPolicyAssociationList()
        {
            return this.m_pmiPolicyAssociationList;
        }

        internal IEnumerable<FileBasedProduct> GetProductList()
        {
            return this.m_productList;
        }

        internal IEnumerable<FileBasedProductPairing> GetProductPairingList()
        {
            return this.m_productPairingList;
        }

        internal IEnumerable<FileBasedDocMagicPlanCode> GetDocMagicPlanCodeList()
        {
            return this.m_docMagicPlanCodeList;
        }

        internal IEnumerable<FileBasedInvestorName> GetInvestorNameList()
        {
            return this.m_investorNameList;
        }

        internal IEnumerable<FileBasedLoanProgramTemplate> GetLoanProgramTemplateByBrokerId(Guid brokerId)
        {
            List<FileBasedLoanProgramTemplate> list = new List<FileBasedLoanProgramTemplate>();

            var typeModel = ProtoBuf.Meta.RuntimeTypeModel.Default;

            foreach (var obj in this.m_loanProgramDictionary)
            {
                using (MemoryStream ms = new MemoryStream(obj.Value))
                {
                    using (var protoReader = new ProtoReader(ms, typeModel, null))
                    {
                        protoReader.ReadFieldHeader(); // lLpTemplateId is locate in first property.
                        protoReader.SkipField(); // We do not need the llptemplateid
                        protoReader.ReadFieldHeader(); // brokerId is locate in second property.
                        Guid id = BclHelpers.ReadGuid(protoReader);
                        if (id == brokerId)
                        {
                            var item = DeserializeFromMemory<FileBasedLoanProgramTemplate>(obj.Value);

                            list.Add(item);
                        }
                    }
                }
            }
            return list;
        }

        internal IEnumerable<FileBasedPriceGroupProgPolicyAssoc> GetPriceGroupProgPolicyAssocList()
        {
            return this.m_priceGroupProgPolicyAssocList;
        }

        internal bool TryGetProductPriceAssociation(Guid id, out SimplifyProductPriceAssociation item)
        {
            return this.m_productPriceAssociationDictionary.TryGetValue(id, out item);
        }

        public static FileBasedSnapshot Retrieve(SHA256Checksum fileKey, Guid brokerId, bool? useSubsnapshot = null)
        {
            bool enablePerLenderSnapshot = ConstSite.EnablePerLenderSnapshot;
            if(useSubsnapshot.HasValue)
            {
                enablePerLenderSnapshot = useSubsnapshot.Value;
            }

            using (PerformanceStopwatch.Start($"FileBasedSnapshot.Retrieve(): {fileKey.Value}"))
            {
                Stopwatch sw = Stopwatch.StartNew();

                if (enablePerLenderSnapshot && brokerId != Guid.Empty)
                {
                    var tmpId = GetSubsnapshotId(fileKey.Value, brokerId);
                    bool keyExists = MemoryCacheUtilities.GetOrAddExisting($"{tmpId.Value}_PricingSnapshotExists", () => FileDBTools.DoesFileExist(E_FileDB.PricingSnapshot, tmpId.Value), SlidingExpiration);
                    if (!keyExists)
                    {
                        brokerId = Guid.Empty;
                    }
                }
                else
                {
                    brokerId = Guid.Empty;
                }                

                string cacheKey = brokerId == Guid.Empty ? fileKey.Value + "_Snapshot" : fileKey.Value + "_" + brokerId.ToString() + "_Snapshot";

                FileBasedSnapshot snapshot = MemoryCacheUtilities.GetOrAddExisting(cacheKey, () => new FileBasedSnapshot(fileKey, brokerId), SlidingExpiration);

                using (PerformanceStopwatch.Start($"FileBasedSnapshotCounter: {objCounter}"))
                {
                }

                Tools.LogInfo("FileBasedSnapshot.Retrieve", $"FileBasedSnapshot loaded {fileKey.Value}, brokerId = {brokerId}, duration ={sw.ElapsedMilliseconds:N0} ms. #FileBasedSnapshot: {objCounter}");
                return snapshot;
            }
        }

        private static FileBasedSnapshot RetrieveLatestManualImport_Private(Guid brokerId, bool? useSubsnapshot = null)
        {
            SHA256Checksum fileKey;

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "LPE_RELEASE_RetrieveLatestManualImport"))
            {
                if (reader.Read())
                {
                    string v = (string)reader["FileKey"];
                    fileKey = SHA256Checksum.Create(v).ForceValue();
                }
                else
                {
                    throw CBaseException.GenericException("Unable to locate manual import snapshot.");
                }
            }

            return Retrieve(fileKey, brokerId, useSubsnapshot);
        }


        /// <summary>
        /// Retrieve the latest file based snapshot that import into system. This will not retrieve latest snapshot generate by LpeRelease controller.
        /// </summary>
        /// <returns>File based snapshot.</returns>
        public static FileBasedSnapshot RetrieveLatestManualImport()
        {
            return RetrieveLatestManualImport_Private(Guid.Empty);
        }

        public static FileBasedSnapshot RetrieveLatestManualImport(Guid brokerId, bool? useSubsnapshot = null)
        {
            return RetrieveLatestManualImport_Private(brokerId, useSubsnapshot);
        }


        public static int SnapshotSplit(string snapshotId)
        {
            var sw = Stopwatch.StartNew();
            Tools.LogInfo($"SnapshotSplit({snapshotId}) start");
            string path = Path.Combine(ConstApp.TempFolder, Guid.NewGuid().ToString());

            try
            {
                var key = SHA256Checksum.Create(snapshotId).Value;
                var snapshotLocalPath = Utils.RetrieveFromPriceEngineStorage(PriceEngineStorageType.PricingSnapshot, key);

                CompressionHelper.DecompressIOCompressedFile(new FileInfo(snapshotLocalPath.Value), new DirectoryInfo(path), deleteExisting: true);
                int subSnapshotCount = PartitionByLender(path, snapshotId);
                Tools.LogInfo($"SnapshotSplit({snapshotId}) end. Created {subSnapshotCount} sub-snapshots in {sw.ElapsedMilliseconds/1000:N0} seconds.");

                return subSnapshotCount;
            }
            finally
            {
                if (Directory.Exists(path))
                {
                    Directory.Delete(path, true);
                }
            }
        }

        private static int TouchFolder(string path)
        {
            int count = 0;
            DateTime now = DateTime.Now;

            var di = new DirectoryInfo(path);
            foreach (var fi in di.GetFiles())
            {
                if(fi.LastAccessTime < now)
                {
                    fi.LastAccessTime = now;
                }

                count++;
            }

            return count;
        }


        public static int PartitionByLender(string path, string snapshotId)
        {
            string tmpPath = Path.Combine(path, Guid.NewGuid().ToString());
            Directory.CreateDirectory(tmpPath);

            LocalFilePath pricePolicyFname = LocalFilePath.Create(Path.Combine(path, "PRICE_POLICY_v2.bin")).Value;
            if (!File.Exists(pricePolicyFname.Value))
            {
                throw new ArgumentException("The file PRICE_POLICY_v2.bin does not exist.");
            }

            var policyChecksum = EncryptionHelper.ComputeSHA256Hash(pricePolicyFname);
            Utils.SaveToPriceEngineStorage(PriceEngineStorageType.PricePolicySnapshot, policyChecksum, pricePolicyFname);

            LocalFilePath folderFname = LocalFilePath.Create(Path.Combine(path, "LOAN_PRODUCT_FOLDER.bin")).Value;
            var loanProductFolderChecksum = EncryptionHelper.ComputeSHA256Hash(folderFname);

            var snapshotInfo = new SnapshotInfo()
            {
                MainSnapshot = snapshotId,
                PolicyChecksum = policyChecksum.Value,
                LoanProductFolderChecksum = loanProductFolderChecksum.Value
            };

            var snapshotInfoPath = SerializationHelper.JsonSerializeToFile(snapshotInfo);
            FileOperationHelper.Move(snapshotInfoPath.Value, Path.Combine(tmpPath, "SnapshotInfo.json"));

            string partitionPath = Path.Combine(path, "Subsnapshots");
            if (!Directory.Exists(partitionPath))
            {
                Directory.CreateDirectory(partitionPath);
            }

            string[] commonFnames =
            {
                "PMI_PRODUCT.bin"                   ,
                "PMI_PRODUCT_FOLDER.bin"            ,
                "summary.json"                      ,
                "RATE_OPTION_PRICEGROUP_PROGRAM.bin",
                "PRICEGROUP_PROG_POLICY_ASSOC.bin"  ,
                "PMI_POLICY_ASSOC.bin"              ,
                "PRODUCT.bin"                       ,
                "INVESTOR_NAME.bin"                 ,
                "PRODUCT_PAIRING.bin"               ,
                "DOC_MAGIC_PLAN_CODE.bin"
            };

            foreach (var name in commonFnames)
            {
                string src = Path.Combine(path, name);
                string dest = Path.Combine(tmpPath, name);

                File.Copy(src, dest, true);
            }


            var loanPrograms = new List<FileBasedLoanProgramTemplate>();
            ExtractToList(path, "LOAN_PROGRAM_TEMPLATE.bin", loanPrograms);

            var rateOpts = new List<RateOptionsV2>();
            ExtractToList(path, "RATE_OPTIONS_v2.bin", rateOpts);
            var rateOpDict = rateOpts.ToDictionary(r => r.Id, r => r);

            var productPriceAssocList = new List<FileBasedProductPriceAssociation>();
            ExtractToList(path, "PRODUCT_PRICE_ASSOCIATION.bin", productPriceAssocList);
            var productPriceAssocDict = productPriceAssocList.ToDictionary(a => a.ProductId, a => a);

            var loanPartitions = loanPrograms.GroupBy(i => i.BrokerId);

            // Total files = commonFnames.Length + 4 new files: SnapshotInfo.json LOAN_PROGRAM_TEMPLATE.bin,  PRODUCT_PRICE_ASSOCIATION_v2.bin RATE_OPTIONS_v2.bin
            int totalFiles = commonFnames.Length + 4;
            int counter = 0;
            foreach (var partition in loanPartitions)
            {
                var productList = partition.ToList();
                Action<BinaryFileHelper.LqbBinaryStream> writeHandler = delegate (BinaryFileHelper.LqbBinaryStream stream)
                {
                    Serializer.Serialize(stream.Stream, productList);
                };
                BinaryFileHelper.OpenNew(Path.Combine(tmpPath, "LOAN_PROGRAM_TEMPLATE.bin"), writeHandler);


                var rateIdSet = new HashSet<Guid>(productList.Select(i => i.SrcRateOptionsProgId));

                var subRateOpts = new List<RateOptionsV2>();
                foreach (var id in rateIdSet)
                {
                    if (rateOpDict.ContainsKey(id))
                    {
                        subRateOpts.Add(rateOpDict[id]);
                    }
                }

                Action<BinaryFileHelper.LqbBinaryStream> writeRateOptHandler = delegate (BinaryFileHelper.LqbBinaryStream stream)
                {
                    Serializer.Serialize(stream.Stream, subRateOpts);
                };
                BinaryFileHelper.OpenNew(Path.Combine(tmpPath, "RATE_OPTIONS_v2.bin"), writeRateOptHandler);

                var productPriceAssocSubList = new List<FileBasedProductPriceAssociation>();
                foreach (var product in productList)
                {
                    if (productPriceAssocDict.ContainsKey(product.lLpTemplateId))
                    {
                        productPriceAssocSubList.Add(productPriceAssocDict[product.lLpTemplateId]);
                    }
                }

                Copy_PRODUCT_PRICE_ASSOCIATION_V2(tmpPath, productPriceAssocSubList);
                int numFiles = TouchFolder(tmpPath);
                if (numFiles < totalFiles )
                {
                    throw CBaseException.GenericException($"PartitionByLenderError: require {totalFiles} files but only have {numFiles} files.");
                }

                string zipFile = Path.Combine(partitionPath, "Snapshot-broker-" + partition.Key.ToString() + ".zip");
                ZipFile.CreateFromDirectory(tmpPath, zipFile);

                var subSnapshotId = GetSubsnapshotId(snapshotId, partition.Key);
                Utils.SaveToPriceEngineStorage(PriceEngineStorageType.PerLenderSnapshot, subSnapshotId, LocalFilePath.Create(zipFile).Value);

                if ((++counter % 50) == 0 || counter == 1)
                {
                    Tools.LogInfo($"PartitionByLender({snapshotId}) create {counter} subsnapshots");
                }
            }

            Directory.Delete(tmpPath, true);
            return loanPartitions.Count();
        }

        private class SnapshotInfo
        {
            public string MainSnapshot;
            public string PolicyChecksum;
            public string LoanProductFolderChecksum;
        }

        private static void Copy_PRODUCT_PRICE_ASSOCIATION_V2(string path, List<FileBasedProductPriceAssociation> productPriceAssocList)
        {
            var guidToId = new Dictionary<Guid, int>();
            var compactList = new List<ProductPriceAssociationV2>();
            foreach (var item in productPriceAssocList)
            {
                compactList.Add(ProductPriceAssociationV2.Create(item, guidToId));
            }

            var id2Guid = guidToId.ToDictionary(i => i.Value, i => i.Key);
            List<Guid> guids = new List<Guid>();
            for (int i = 0; i < id2Guid.Count; i++)
            {
                guids.Add(id2Guid[i]);
            }

            ProductPriceAssociationTable obj = new ProductPriceAssociationTable()
            {
                Idx2Guid = guids,
                Items = compactList
            };

            Action<BinaryFileHelper.LqbBinaryStream> writeHandler = delegate (BinaryFileHelper.LqbBinaryStream stream)
            {
                Serializer.Serialize(stream.Stream, obj);
            };
            BinaryFileHelper.OpenNew(Path.Combine(path, "PRODUCT_PRICE_ASSOCIATION_v2.bin"), writeHandler);
        }


        private static SHA256Checksum GetSubsnapshotId(string snapshotId, Guid brokerId)
        {
            return EncryptionHelper.ComputeSHA256Hash(System.Text.Encoding.UTF8.GetBytes($"{snapshotId}_{brokerId}"));
        }

        private static bool needSummaryHeader = true;

        internal static void WriteToSummaryFile(string msg)
        {
            try
            {
                if (ConstSite.EnableFilebasedCmp == false || Directory.Exists(ConstSite.FilebasedFolder) == false)
                {
                    return;
                }

                string fname = Path.Combine(ConstSite.FilebasedFolder, "summary.txt");

                Action<TextFileHelper.LqbTextWriter> writeHandler = delegate(TextFileHelper.LqbTextWriter sw)
                {
                    string timeStamp = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff");

                    if (needSummaryHeader)
                    {
                        sw.AppendLine(string.Format("{0}************* {1}{0}", Environment.NewLine, timeStamp));
                        needSummaryHeader = false;
                    }

                    sw.AppendLine("[" + timeStamp + "] - " + msg);
                };

                TextFileHelper.OpenForAppend(fname, writeHandler);
            }
            catch (SystemException)
            {
            }
        }

        internal class FileBasedSnapshotGoodBye : Goodbye
        {
            public FileBasedSnapshotGoodBye(string msg) : base(msg)
            {
            }

            ~FileBasedSnapshotGoodBye()
            {
                int numObj = FileBasedSnapshot.DecrementCounter();
                m_msg = m_msg + $", estimate FilebasedSnapshotCounter: {numObj}";
            }

        }

        private class PricePolicySnapshotLoader
        {
            public PricePolicySnapshotLoader(SHA256Checksum policyFileKey)
            {
                using (PerformanceStopwatch.Start($"PolicySnapshotLoader.ctor: {policyFileKey.Value}"))
                {
                    LocalFilePath policyLocalPath = Utils.RetrieveFromPriceEngineStorage(PriceEngineStorageType.PricePolicySnapshot, policyFileKey);
                    var fi = new FileInfo(policyLocalPath.Value);

                    Dictionary<Guid, AbstractRtPricePolicy> pricePolicyDictionary = new Dictionary<Guid, AbstractRtPricePolicy>();
                    ExtractPricePolicyVersion2(Path.GetDirectoryName(policyLocalPath.Value), fi.Name, pricePolicyDictionary);
                    PricePolicyDictionary = pricePolicyDictionary;
                }
            }

            public Dictionary<Guid, AbstractRtPricePolicy> PricePolicyDictionary { get; private set; }
        }
    }
}