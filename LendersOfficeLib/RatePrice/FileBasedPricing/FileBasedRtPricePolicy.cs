﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccess;
using LendersOffice.Common;
using LendersOfficeApp.los.RatePrice;
using LqbGrammar.DataTypes;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    //public class FileBasedRtPricePolicyComparer : IEqualityComparer<FileBasedRtPricePolicy>
    //{
    //    #region IEqualityComparer<FileBasedRtPricePolicy> Members

    //    public bool Equals(FileBasedRtPricePolicy x, FileBasedRtPricePolicy y)
    //    {
    //        return x.PricePolicyId == y.PricePolicyId;
    //    }

    //    public int GetHashCode(FileBasedRtPricePolicy obj)
    //    {
    //        return obj.PricePolicyId.GetHashCode();
    //    }

    //    #endregion
    //}
    public sealed partial class FileBasedRtPricePolicy
    {
        private string mutualExclusiveExecSortedId;
        private string pricePolicyDescription;

        public override int EstimateSize()
        {
            return 0;
        }


        public override string MutualExclusiveExecSortedId
        {
            get
            {
                this.LoadContentIfNeed();
                return this.mutualExclusiveExecSortedId;
            }

            set
            {
                throw new NotImplementedException(); // This value set from LoadContentIfNeed() method.
            }
        }

        public override string PricePolicyDescription
        {
            get
            {
                this.LoadContentIfNeed();
                return this.pricePolicyDescription;
            }

            set
            {
                throw new NotImplementedException(); // This value set from LoadContentIfNeed() method.
            }
        }

        public override void LoadRules()
        {
            this.LoadContentIfNeed(); // 2016/12/08: this used for PMI policy. With other policies, their contents already loaded.
        }

        public static FileBasedRtPricePolicy Create(IDataReader reader)
        {
            FileBasedRtPricePolicy item = new FileBasedRtPricePolicy();
            item.PricePolicyId = reader.SafeGuid("PricePolicyId");
            item.ContentKeyString = (string)reader["ContentKey"];
            item.ParentPolicyId = reader.SafeGuid("ParentPolicyId");
            item.loadedContent = false;

            return item;
        }

        private void LoadContentIfNeed()
        {
            if (this.loadedContent)
            {
                return;
            }

            using (PerformanceStopwatch.Start("FileBasedRtPricePolicy.LoadContentIfNeed"))
            {
                lock (this.lockObject)
                {
                    if (this.loadedContent)
                    {
                        return;
                    }

                    var contentKey = SHA256Checksum.Create(this.ContentKeyString);
                    if (contentKey == null)
                    {
                        throw new FileContentKeyNotFoundException($"The FileBasedRtPricePolicy {this.PricePolicyId} has invalid contentKey '{this.ContentKeyString}'.");
                    }

                    LocalFilePath policyFname = Utils.RetrieveFromPriceEngineStorage(PriceEngineStorageType.PricePolicy, contentKey.Value);
                    Model.PricePolicy policyModel = SerializationHelper.JsonDeserializeFromFile<Model.PricePolicy>(policyFname);
                    this.mutualExclusiveExecSortedId = policyModel.MutualExclusiveExecSortedId;
                    this.pricePolicyDescription = policyModel.Description;

                    Tools.Assert(this.PricePolicyId == policyModel.Id, "Price Policy Id does not match.");
                    Tools.Assert(this.ParentPolicyId == policyModel.ParentId, "Parent Priec Policy Id does not match.");

                    RtRuleList tmpRuleList = null;

                    List<AbstractRtRule> dtiRules = new List<AbstractRtRule>();
                    List<AbstractRtRule> variantRules = new List<AbstractRtRule>();
                    List<AbstractRtRule> qvalueRules = new List<AbstractRtRule>();

                    tmpRuleList = new RtRuleList(policyModel.Rules.Count);

                    foreach (var ruleModel in policyModel.Rules)
                    {
                        RtRule rtRule = new RtRule(ruleModel);
                        switch (rtRule.Category)
                        {
                        case LpeKeywordCategory.Invariant: tmpRuleList.Add(rtRule); break;
                        case LpeKeywordCategory.Variant: variantRules.Add(rtRule); break;
                        case LpeKeywordCategory.Dti: dtiRules.Add(rtRule); break;
                        case LpeKeywordCategory.QValue: qvalueRules.Add(rtRule); break;
                        }
                    }

                    FirstIdxOfQValue = tmpRuleList.Count;
                    tmpRuleList.AddRange(qvalueRules);

                    FirstIdxOfVariant = tmpRuleList.Count;
                    tmpRuleList.AddRange(variantRules);

                    FirstIdxOfDti = tmpRuleList.Count;
                    tmpRuleList.AddRange(dtiRules);

                    m_ruleList = tmpRuleList;

                    this.loadedContent = true;
                }
            }
        }

        private bool loadedContent;
        private object lockObject = new object();
    }
}
