﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;
using DataAccess;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    [ProtoContract]
    public sealed partial class FileBasedPriceGroupProgPolicyAssoc
    {
        [ProtoMember(1)]
        public Guid LpePriceGroupId { get; set;}

        [ProtoMember(2)]
        public Guid ProgId { get; set;}

        [ProtoMember(3)]
        public Guid PricePolicyId { get; set;}

        [ProtoMember(4)]
        public E_TriState AssociateType { get; set; }
    }
}
