﻿namespace LendersOffice.RatePrice.FileBasedPricing
{
    using System.Data;
    using DataAccess;

    /// <summary>
    /// Represent data in INVESTOR_NAME table.
    /// </summary>
    public sealed partial class FileBasedInvestorName
    {
        /// <summary>
        /// Create object from sql.
        /// </summary>
        /// <param name="reader">Sql row contains data.</param>
        /// <returns>An object.</returns>
        public static FileBasedInvestorName Create(IDataReader reader)
        {
            FileBasedInvestorName item = new FileBasedInvestorName();

            item.InvestorName = (string)reader["InvestorName"];
            item.IsValid = (bool)reader["IsValid"];
            item.InvestorId = (int)reader["InvestorId"];
            item.UseLenderTimezoneForRsExpiration = (bool)reader["UseLenderTimezoneForRsExpiration"];
            item.RateLockCutOffTime = reader.SafeDateTime("RateLockCutOffTime");
            return item;
        }
    }
}
