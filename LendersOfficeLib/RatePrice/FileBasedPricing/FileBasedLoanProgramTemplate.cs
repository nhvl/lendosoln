﻿using System;
using System.Collections.Generic;
using System.Data;
using DataAccess;
using LendersOffice.Common;
using LendersOfficeApp.los.RatePrice;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    public sealed partial class FileBasedLoanProgramTemplate
    {
        private static LosConvert s_losConvertInstance = new LosConvert();

        /// <summary>
        /// 1/10/2017 - dd - Create the LosConvert object on demand to save memory.
        /// File based snapshot hold FileBasedLoanProgramTemplate in memory. As of this writing
        /// if I do not create the LosConvert on demand the memory consumption will be in 500MB. 
        /// With ondemand the initial memory demand is around 200MB.
        /// </summary>
        private LosConvert m_losConvert = s_losConvertInstance;

        public string lTerm_rep
        {
            get { return m_losConvert.ToCountString(lTerm); }
        }

        public string lDue_rep
        {
            get { return m_losConvert.ToCountString(lDue); }
        }

        public string lLockedDays_rep
        {
            get { return m_losConvert.ToCountString(lLockedDays); }
        }

        public string lReqTopR_rep
        {
            get { return m_losConvert.ToRateString(lReqTopR); }
        }

        public string lReqBotR_rep
        {
            get { return m_losConvert.ToRateString(lReqBotR); }
        }


        public string lRadj1stCapR_rep 
        {
            get { return m_losConvert.ToRateString(lRadj1stCapR); } 
        }

        public string lRadj1stCapMon_rep
        {
            get { return m_losConvert.ToCountString(lRadj1stCapMon); }
        }

        public string lRAdjCapR_rep
        {
            get { return m_losConvert.ToRateString(lRAdjCapR); }
        }

        public string lRAdjCapMon_rep
        {
            get { return m_losConvert.ToCountString(lRAdjCapMon); }
        }

        public string lRAdjLifeCapR_rep
        {
            get { return m_losConvert.ToRateString(lRAdjLifeCapR); }
        }

        public string lRAdjMarginR_rep
        {
            get { return m_losConvert.ToRateString(lRAdjMarginR); }
        }

        public string lRAdjIndexR_rep
        {
            get { return m_losConvert.ToRateString(lRAdjIndexR); }
        }

        public string lRAdjFloorR_rep
        {
            get { return m_losConvert.ToRateString(lRAdjFloorR); }
        }

        public string lPmtAdjCapR_rep
        {
            get { return m_losConvert.ToRateString(lPmtAdjCapR); }
        }

        public string lPmtAdjCapMon_rep 
        { 
            get { return m_losConvert.ToCountString(lPmtAdjCapMon); }
        }

        public string lPmtAdjRecastPeriodMon_rep 
        {
            get { return m_losConvert.ToCountString(lPmtAdjRecastPeriodMon); }
        }

        public string lPmtAdjRecastStop_rep
        {
            get { return m_losConvert.ToCountString(lPmtAdjRecastStop); }
        }


        public string lBuydwnR1_rep
        {
            get { return m_losConvert.ToRateString(lBuydwnR1); }
        }

        public string lBuydwnR2_rep
        {
            get { return m_losConvert.ToRateString(lBuydwnR2); }
        }

        public string lBuydwnR3_rep
        {
            get { return m_losConvert.ToRateString(lBuydwnR3); }
        }

        public string lBuydwnR4_rep
        {
            get { return m_losConvert.ToRateString(lBuydwnR4); }
        }

        public string lBuydwnR5_rep
        {
            get { return m_losConvert.ToRateString(lBuydwnR5); }
        }

        public string lBuydwnMon1_rep 
        {
            get { return m_losConvert.ToCountString(lBuydwnMon1); }
        }

        public string lBuydwnMon2_rep
        {
            get { return m_losConvert.ToCountString(lBuydwnMon2); }
        }
        public string lBuydwnMon3_rep
        {
            get { return m_losConvert.ToCountString(lBuydwnMon3); }
        }
        public string lBuydwnMon4_rep
        {
            get { return m_losConvert.ToCountString(lBuydwnMon4); }
        }
        public string lBuydwnMon5_rep
        {
            get { return m_losConvert.ToCountString(lBuydwnMon5); }
        }

        public string lGradPmtYrs_rep
        {
            get { return m_losConvert.ToCountString(lGradPmtYrs); }
        }

        public string lGradPmtR_rep
        {
            get { return m_losConvert.ToRateString(lGradPmtR); }
        }

        public string lIOnlyMon_rep
        {
            get { return m_losConvert.ToCountString(lIOnlyMon); }
        }

        public string lPmtAdjMaxBalPc_rep
        {
            get { return m_losConvert.ToRateString(lPmtAdjMaxBalPc); }
        }

        public CDateTime lRateSheetEffectiveD 
        {
            get { return CDateTime.Create(lRateSheetEffectiveD_internal); }
        }

        public CDateTime lRateSheetExpirationD 
        {
            get { return CDateTime.Create(lRateSheetExpirationD_internal); }
        }

        public string lLpeFeeMin_rep
        {
            get { return m_losConvert.ToRateString(lLpeFeeMin); }
        }

        public string lLpeFeeMax_rep
        {
            get { return m_losConvert.ToRateString(lLpeFeeMax); }
        }

        public CDateTime lArmIndexEffectiveD
        {
            get { return CDateTime.Create(lArmIndexEffectiveD_internal); }
        }
        public string lArmIndexEffectiveD_rep
        {
            get { return m_losConvert.ToDateTimeString(lArmIndexEffectiveD_internal); }
        }

        public string lRateDelta_rep
        {
            get { return m_losConvert.ToRateString(lRateDelta); }
        }

        public string lFeeDelta_rep
        {
            get { return m_losConvert.ToRateString(lFeeDelta); }
        }

        public string lHardPrepmtPeriodMonths_rep
        {
            get { return m_losConvert.ToCountString(lHardPrepmtPeriodMonths); }
        }

        public string lSoftPrepmtPeriodMonths_rep
        {
            get { return m_losConvert.ToCountString(lSoftPrepmtPeriodMonths); }
        }

        public string lHelocPmtMb_rep
        {
            get { return m_losConvert.ToMoneyString(lHelocPmtMb, FormatDirection.ToRep); }
        }

        public string lHelocDraw_rep
        {
            get { return m_losConvert.ToCountString(lHelocDraw); }
        }



        #region ILoanProgramTemplate Members



        public bool IsHeloc
        {
            get {
                // 10/24/2013 dd - Use lLpProductType = HELOC to determine if the program a HELOC.
                return lLpProductType.Equals("HELOC", StringComparison.OrdinalIgnoreCase);
            }
        }

        public string lPrepmtPeriod_rep
        {
            get 
            {
                // 12/5/2013 dd - How to sync logic with LoanProgramTemplateData??
                return m_losConvert.ToCountString(lSoftPrepmtPeriodMonths + lHardPrepmtPeriodMonths);
            }
        }

        public decimal lNoteR
        {
            get 
            {
                foreach (var o in GetRawRateSheetWithDeltas())
                {
                    if (o.IsSpecialKeyword == false)
                    {
                        return o.Rate;
                    }
                }
                return 0;
            }
        }

        public decimal lLOrigFPc
        {
            get 
            {
                foreach (var o in GetRawRateSheetWithDeltas())
                {
                    //Note: remove condition "if (o.IsSpecialKeyword == false)" for matching with CLoanProductDataForRunningPricing.lLOrigFPc 

                    return o.Point;
                }
                return 0;
            }
        }
        private int lHelocRepay
        {
            get { return lDue - lHelocDraw; }
        }
        public string lHelocRepay_rep
        {
            get { 
                return m_losConvert.ToCountString(lHelocRepay); 
            }
        }



        private CCcTemplateData m_ccTemplate = null;
        public CCcTemplateData CcTemplate
        {
            get 
            {
                if (!HasCcTemplate)
                    throw new CBaseException(ErrorMessages.Generic, "No cc template associated with this loan program");

                if (null == m_ccTemplate)
                {

                    m_ccTemplate = new CCcTemplateData(this.BrokerId, lCcTemplateId);
                    m_ccTemplate.InitLoad();

                }

                return m_ccTemplate;
            }
        }

        public bool HasCcTemplate
        {
            get { return lCcTemplateId != Guid.Empty; }
        }

        public string FullName
        {
            get {
                return lLpTemplateNm; //"TOBE IMPLEMENT FULL NAME";
            }
        }

        public DateTime lRateSheetDownloadEndD
        {
            get 
            {
                if (m_rateOptions != null)
                {
                    return m_rateOptions.lRateSheetDownloadEndD;
                }
                return DateTime.MinValue;
            }
        }

        public DateTime lRateSheetDownloadStartD
        {
            get 
            {
                if (m_rateOptions != null)
                {
                    return m_rateOptions.lRateSheetDownloadStartD;
                }
                return DateTime.MinValue;
            }
        }
        private decimal effective_lRateDelta
        {
            get
            {
                return lRateDelta + (lRateSheetXmlContentOverrideBit ? 0 : lRateDeltaInherit);
            }
        }
        private decimal effective_lFeeDelta
        {
            get
            {
                return lFeeDelta + (lRateSheetXmlContentOverrideBit ? 0 : lFeeDeltaInherit);
            }
        }
        private List<FileBasedRateItem> m_rateItemList = null;
        public IRateItem[] GetRawRateSheetWithDeltas()
        {
            if (m_rateItemList == null)
            {
                m_rateItemList = new List<FileBasedRateItem>();

                if (m_rateOptions != null)
                {
                    if (m_rateOptions.RateItemList != null)
                    {
                        foreach (var o in m_rateOptions.RateItemList)
                        {
                            if (o.IsSpecialKeyword)
                            {
                                m_rateItemList.Add(o);
                            }
                            else
                            {
                                FileBasedRateItem clone = new FileBasedRateItem();
                                clone.__Rate = string.Format("{0:N3}%", o.Rate + effective_lRateDelta);
                                clone.__Point = string.Format("{0:N3}%", o.Point + effective_lFeeDelta);
                                clone.__Margin = o.__Margin;
                                clone.__QRateBase = o.__QRateBase;
                                clone.__TeaserRate = o.__TeaserRate;
                                m_rateItemList.Add(clone);
                            }
                        }
                    }
                }
            }
            return m_rateItemList.ToArray() ;
        }

        public string LpeAcceptableRsFileId
        {
            get { return m_rateOptions.LpeAcceptableRsFileId; }
        }
        public long LpeAcceptableRsFileVersionNumber
        {
            get { return m_rateOptions.LpeAcceptableRsFileVersionNumber; }
        }
        private FileBasedRateOptions m_rateOptions = null;
        public void SetRateOptions(FileBasedRateOptions rateOptions)
        {
            m_rateOptions = rateOptions;
        }

        public LendersOfficeApp.los.RatePrice.IRateItem GetRateOptionKey(int sProdRLckdDays, int sProdPpmtPenaltyMon, int sIOnlyMon)
        {
            // 12/10/2013 dd - Logic is implement in CApplicantPrices
            return null;

        }


        /// <summary>
        /// If true, add static setting of max ysp to max ysp adjustment from rules
        /// If false, just use static setting of max ysp
        /// </summary>
        public bool lLpeFeeMinAddRuleAdjBit
        {
            // 12/5/2013 dd - How to sync logic
            get
            {
                if (Guid.Empty == lBaseLpId)
                    return true;
                if (lLpeFeeMinOverrideBit)
                    return false;
                return lLpeFeeMinAddRuleAdjInheritBit;
            }
        }

        public bool lIsArmMarginDisplayed
        {
            get
            {
                if (lFinMethT == E_sFinMethT.ARM)
                    return true; // per opm 13035
                                 //return GetBoolField( "lIsArmMarginDisplayed" ); 
                return false;
            }
        }

        public int lDue
        {
            get
            {
                if (lDue_internal <= 0)
                    return lTerm;
                return lDue_internal;
            }
        }

        public E_sLpDPmtT lLpDPmtT
        {
            get { return lLpDPmtT_internal;  }
        }

        public E_sLpQPmtT lLpQPmtT
        {
            get { return lLpQPmtT_internal; }
        }

        public E_sHelocPmtBaseT lHelocQualPmtBaseT
        {
            get
            {
                if (lHasQRateInRateOptions)
                {
                    return lHelocQualPmtBaseT_internal;
                }
                else
                {
                    return lHelocPmtBaseT;
                }
            }
        }

        public E_sHelocPmtFormulaT lHelocQualPmtFormulaT
        {
            get
            {
                if (lHasQRateInRateOptions)
                {
                    return lHelocQualPmtFormulaT_internal;
                }
                else
                {
                    return lHelocPmtFormulaT;
                }
            }
        }
        public E_sHelocPmtFormulaRateT lHelocQualPmtFormulaRateT
        {
            get
            {
                if (lHasQRateInRateOptions)
                {
                    return lHelocQualPmtFormulaRateT_internal;
                }
                else
                {
                    return lHelocPmtFormulaRateT;
                }
            }
        }
        public E_sHelocPmtAmortTermT lHelocQualPmtAmortTermT
        {
            get
            {
                if (lHasQRateInRateOptions)
                {
                    return lHelocQualPmtAmortTermT_internal;
                }
                else
                {
                    return lHelocPmtAmortTermT;
                }
            }
        }


        public E_sHelocPmtPcBaseT lHelocQualPmtPcBaseT
        {
            get
            {
                if (lHasQRateInRateOptions)
                {
                    return lHelocQualPmtPcBaseT_internal;
                }
                else
                {
                    return lHelocPmtPcBaseT;
                }
            }
        }

        public decimal lHelocQualPmtMb
        {
            get
            {
                if (lHasQRateInRateOptions)
                {
                    return lHelocQualPmtMb_internal;
                }
                else
                {
                    return lHelocPmtMb;
                }
            }
        }

        public string lHelocQualPmtMb_rep
        {
            get
            {
                if (lHasQRateInRateOptions)
                {
                    return m_losConvert.ToMoneyString(lHelocQualPmtMb, FormatDirection.ToRep);
                }
                else
                {
                    return lHelocPmtMb_rep;
                }
            }
        }

        public int lTerm
        {
            get
            {
                return (lTerm_internal <= 0) ? 360 : lTerm_internal;
            }
        }

        #endregion


        public static FileBasedLoanProgramTemplate Create(IDataReader reader)
        {
            FileBasedLoanProgramTemplate item = new FileBasedLoanProgramTemplate();
            #region Set DB Fields
            item.lLpTemplateId = reader.SafeGuid("lLpTemplateId");
            item.BrokerId = reader.SafeGuid("BrokerId");
            item.lLpTemplateNm = reader.SafeString("lLpTemplateNm");
            item.lLendNm = reader.SafeString("lLendNm");
            item.lLT = (E_sLT) reader.SafeInt("lLT");
            item.lLienPosT = (E_sLienPosT) reader.SafeInt("lLienPosT");
            item.lQualR = reader.SafeDecimal("lQualR");
            item.lTerm_internal = reader.SafeInt("lTerm");
            item.lDue_internal = reader.SafeInt("lDue");
            item.lLockedDays = reader.SafeInt("lLockedDays");
            item.lReqTopR = reader.SafeDecimal("lReqTopR");
            item.lReqBotR = reader.SafeDecimal("lReqBotR");
            item.lRadj1stCapR = reader.SafeDecimal("lRadj1stCapR");
            item.lRadj1stCapMon = reader.SafeInt("lRadj1stCapMon");
            item.lRAdjCapR = reader.SafeDecimal("lRAdjCapR");
            item.lRAdjCapMon = reader.SafeInt("lRAdjCapMon");
            item.lRAdjLifeCapR = reader.SafeDecimal("lRAdjLifeCapR");
            item.lRAdjMarginR = reader.SafeDecimal("lRAdjMarginR");
            item.lRAdjIndexR = reader.SafeDecimal("lRAdjIndexR");
            item.lRAdjFloorR = reader.SafeDecimal("lRAdjFloorR");
            item.lRAdjRoundT = (E_sRAdjRoundT) reader.SafeInt("lRAdjRoundT");
            item.lPmtAdjCapR = reader.SafeDecimal("lPmtAdjCapR");
            item.lPmtAdjCapMon = reader.SafeInt("lPmtAdjCapMon");
            item.lPmtAdjRecastPeriodMon = reader.SafeInt("lPmtAdjRecastPeriodMon");
            item.lPmtAdjRecastStop = reader.SafeInt("lPmtAdjRecastStop");
            item.lBuydwnR1 = reader.SafeDecimal("lBuydwnR1");
            item.lBuydwnR2 = reader.SafeDecimal("lBuydwnR2");
            item.lBuydwnR3 = reader.SafeDecimal("lBuydwnR3");
            item.lBuydwnR4 = reader.SafeDecimal("lBuydwnR4");
            item.lBuydwnR5 = reader.SafeDecimal("lBuydwnR5");
            item.lBuydwnMon1 = reader.SafeInt("lBuydwnMon1");
            item.lBuydwnMon2 = reader.SafeInt("lBuydwnMon2");
            item.lBuydwnMon3 = reader.SafeInt("lBuydwnMon3");
            item.lBuydwnMon4 = reader.SafeInt("lBuydwnMon4");
            item.lBuydwnMon5 = reader.SafeInt("lBuydwnMon5");
            item.lGradPmtYrs = reader.SafeInt("lGradPmtYrs");
            item.lGradPmtR = reader.SafeDecimal("lGradPmtR");
            item.lIOnlyMon = reader.SafeInt("lIOnlyMon");
            item.lHasVarRFeature = reader.SafeBool("lHasVarRFeature");
            item.lVarRNotes = reader.SafeString("lVarRNotes");
            item.lAprIncludesReqDeposit = reader.SafeBool("lAprIncludesReqDeposit");
            item.lHasDemandFeature = reader.SafeBool("lHasDemandFeature");
            item.lLateDays = reader.SafeInt("lLateDays").ToString();
            item.lLateChargePc = reader.SafeString("lLateChargePc");
            item.lPrepmtPenaltyT = (E_sPrepmtPenaltyT) reader.SafeInt("lPrepmtPenaltyT");
            item.lAssumeLT = (E_sAssumeLT) reader.SafeInt("lAssumeLT");
            item.lCcTemplateId = reader.SafeGuid("lCcTemplateId");
            item.lFilingF = reader.SafeString("lFilingF");
            item.lLateChargeBaseDesc = reader.SafeString("lLateChargeBaseDesc");
            item.lPmtAdjMaxBalPc = reader.SafeDecimal("lPmtAdjMaxBalPc");
            item.lFinMethT = (E_sFinMethT) reader.SafeInt("lFinMethT");
            item.lFinMethDesc = reader.SafeString("lFinMethDesc");
            item.lPrepmtRefundT = (E_sPrepmtRefundT) reader.SafeInt("lPrepmtRefundT");
            item.FolderId = reader.SafeGuid("FolderId");
            item.lRateSheetEffectiveD_internal = reader.SafeDateTime("lRateSheetEffectiveD");
            item.lRateSheetExpirationD_internal = reader.SafeDateTime("lRateSheetExpirationD");
            item.IsMaster = reader.SafeBool("IsMaster");
            item.lLpeFeeMin = reader.SafeDecimal("lLpeFeeMin");
            item.lLpeFeeMax = reader.SafeDecimal("lLpeFeeMax");
            item.PairingProductIds = reader.SafeString("PairingProductIds");
            item.lBaseLpId = reader.SafeGuid("lBaseLpId");
            item.IsEnabled = reader.SafeBool("IsEnabled");
            item.lArmIndexGuid = reader.SafeGuid("lArmIndexGuid");
            item.lArmIndexBasedOnVstr = reader.SafeString("lArmIndexBasedOnVstr");
            item.lArmIndexCanBeFoundVstr = reader.SafeString("lArmIndexCanBeFoundVstr");
            item.lArmIndexAffectInitIRBit = reader.SafeBool("lArmIndexAffectInitIRBit");
            item.lRAdjRoundToR = reader.SafeDecimal("lRAdjRoundToR");
            item.lArmIndexEffectiveD_internal = reader.SafeDateTime("lArmIndexEffectiveD");
            item.lFreddieArmIndexT = (E_sFreddieArmIndexT) reader.SafeInt("lFreddieArmIndexT");
            item.lArmIndexNameVstr = reader.SafeString("lArmIndexNameVstr");
            item.lArmIndexT = (E_sArmIndexT) reader.SafeInt("lArmIndexT");
            item.lArmIndexNotifyAtLeastDaysVstr = reader.SafeString("lArmIndexNotifyAtLeastDaysVstr");
            item.lArmIndexNotifyNotBeforeDaysVstr = reader.SafeString("lArmIndexNotifyNotBeforeDaysVstr");
            item.lLpTemplateNmInherit = reader.SafeString("lLpTemplateNmInherit");
            item.lLpTemplateNmOverrideBit = reader.SafeBool("lLpTemplateNmOverrideBit");
            item.lCcTemplateIdInherit = reader.SafeGuid("lCcTemplateIdInherit");
            item.lCcTemplateIdOverrideBit = reader.SafeBool("lCcTemplateIdOverrideBit");
            item.lLockedDaysInherit = reader.SafeInt("lLockedDaysInherit");
            item.lLockedDaysOverrideBit = reader.SafeBool("lLockedDaysOverrideBit");
            item.lLpeFeeMinInherit = reader.SafeDecimal("lLpeFeeMinInherit");
            item.lLpeFeeMinOverrideBit = reader.SafeBool("lLpeFeeMinOverrideBit");
            item.lLpeFeeMaxInherit = reader.SafeDecimal("lLpeFeeMaxInherit");
            item.lLpeFeeMaxOverrideBit = reader.SafeBool("lLpeFeeMaxOverrideBit");
            item.IsEnabledInherit = reader.SafeBool("IsEnabledInherit");
            item.IsEnabledOverrideBit = reader.SafeBool("IsEnabledOverrideBit");
            item.lPpmtPenaltyMon = reader.SafeInt("lPpmtPenaltyMon");
            item.lPpmtPenaltyMonInherit = reader.SafeInt("lPpmtPenaltyMonInherit");
            item.lPpmtPenaltyMonOverrideBit = reader.SafeBool("lPpmtPenaltyMonOverrideBit");
            item.lRateDelta = reader.SafeDecimal("lRateDelta");
            item.lFeeDelta = reader.SafeDecimal("lFeeDelta");
            item.lPpmtPenaltyMonLowerSearch = reader.SafeInt("lPpmtPenaltyMonLowerSearch");
            item.lLockedDaysLowerSearch = reader.SafeInt("lLockedDaysLowerSearch");
            item.lLockedDaysLowerSearchInherit = reader.SafeInt("lLockedDaysLowerSearchInherit");
            item.lPpmtPenaltyMonLowerSearchInherit = reader.SafeInt("lPpmtPenaltyMonLowerSearchInherit");
            item.PairingProductIdsInherit = reader.SafeString("PairingProductIdsInherit");
            item.PairingProductIdsOverrideBit = reader.SafeBool("PairingProductIdsOverrideBit");
            item.lLpInvestorNm = reader.SafeString("lLpInvestorNm");
            item.lLendNmInherit = reader.SafeString("lLendNmInherit");
            item.lLendNmOverrideBit = reader.SafeBool("lLendNmOverrideBit");
            item.lRateOptionBaseId = reader.SafeString("lRateOptionBaseId");
            item.PairIdFor1stLienProdGuid = reader.SafeGuid("PairIdFor1stLienProdGuid");
            item.PairIdFor1stLienProdGuidInherit = reader.SafeGuid("PairIdFor1stLienProdGuidInherit");
            item.lLpeFeeMinParam = reader.SafeDecimal("lLpeFeeMinParam");
            item.lLpeFeeMinAddRuleAdjInheritBit = reader.SafeBool("lLpeFeeMinAddRuleAdjInheritBit");
            item.IsLpe = reader.SafeBool("IsLpe");
            item.lIsArmMarginDisplayed_internal = reader.SafeBool("lIsArmMarginDisplayed");
            item.ProductCode = reader.SafeString("ProductCode");
            item.ProductIdentifier = reader.SafeString("ProductIdentifier");
            item.IsOptionArm = reader.SafeBool("IsOptionArm");
            item.lRateDeltaInherit = reader.SafeDecimal("lRateDeltaInherit");
            item.lFeeDeltaInherit = reader.SafeDecimal("lFeeDeltaInherit");
            item.SrcRateOptionsProgIdInherit = reader.SafeGuid("SrcRateOptionsProgIdInherit");
            item.SrcRateOptionsProgId = reader.SafeGuid("SrcRateOptionsProgId");
            item.lLpDPmtT_internal = (E_sLpDPmtT)  reader.SafeInt("lLpDPmtT");
            item.lLpQPmtT_internal = (E_sLpQPmtT) reader.SafeInt("lLpQPmtT");
            item.lHasQRateInRateOptions = reader.SafeBool("lHasQRateInRateOptions");
            item.IsLpeDummyProgram = reader.SafeBool("IsLpeDummyProgram");
            item.IsPairedOnlyWithSameInvestor = reader.SafeBool("IsPairedOnlyWithSameInvestor");
            item.lIOnlyMonLowerSearch = reader.SafeInt("lIOnlyMonLowerSearch");
            item.lIOnlyMonLowerSearchInherit = reader.SafeInt("lIOnlyMonLowerSearchInherit");
            item.lIOnlyMonLowerSearchOverrideBit = reader.SafeBool("lIOnlyMonLowerSearchOverrideBit");
            item.lIOnlyMonUpperSearch = reader.SafeInt("lIOnlyMonUpperSearch");
            item.lIOnlyMonUpperSearchInherit = reader.SafeInt("lIOnlyMonUpperSearchInherit");
            item.lIOnlyMonUpperSearchOverrideBit = reader.SafeBool("lIOnlyMonUpperSearchOverrideBit");
            item.CanBeStandAlone2nd = reader.SafeBool("CanBeStandAlone2nd");
            item.lDtiUsingMaxBalPc = reader.SafeBool("lDtiUsingMaxBalPc");
            item.IsMasterPriceGroup = reader.SafeBool("IsMasterPriceGroup");
            item.lLpProductType = reader.SafeString("lLpProductType");
            item.lRAdjFloorBaseT = (E_sRAdjFloorBaseT) reader.SafeInt("lRAdjFloorBaseT");
            item.IsConvertibleMortgage = reader.SafeBool("IsConvertibleMortgage");
            item.lLpmiSupportedOutsidePmi = reader.SafeBool("lLpmiSupportedOutsidePmi");
            item.lWholesaleChannelProgram = reader.SafeBool("lWholesaleChannelProgram");
            item.lHardPrepmtPeriodMonths = reader.SafeInt("lHardPrepmtPeriodMonths");
            item.lSoftPrepmtPeriodMonths = reader.SafeInt("lSoftPrepmtPeriodMonths");
            item.lHelocQualPmtBaseT_internal = (E_sHelocPmtBaseT)reader.SafeInt("lHelocQualPmtBaseT");
            item.lHelocQualPmtPcBaseT_internal = (E_sHelocPmtPcBaseT)reader.SafeInt("lHelocQualPmtPcBaseT");
            item.lHelocQualPmtMb_internal = reader.SafeDecimal("lHelocQualPmtMb");
            item.lHelocPmtBaseT = (E_sHelocPmtBaseT)reader.SafeInt("lHelocPmtBaseT");
            item.lHelocPmtPcBaseT = (E_sHelocPmtPcBaseT)reader.SafeInt("lHelocPmtPcBaseT");
            item.lHelocPmtMb = reader.SafeDecimal("lHelocPmtMb");
            item.lHelocPmtFormulaT = (E_sHelocPmtFormulaT)reader.SafeInt("lHelocPmtFormulaT");
            item.lHelocPmtFormulaRateT = (E_sHelocPmtFormulaRateT)reader.SafeInt("lHelocPmtFormulaRateT");
            item.lHelocPmtAmortTermT = (E_sHelocPmtAmortTermT)reader.SafeInt("lHelocPmtAmortTermT");
            item.lHelocQualPmtFormulaT_internal = (E_sHelocPmtFormulaT)reader.SafeInt("lHelocPmtFormulaT");
            item.lHelocQualPmtFormulaRateT_internal = (E_sHelocPmtFormulaRateT)reader.SafeInt("lHelocPmtFormulaRateT");
            item.lHelocQualPmtAmortTermT_internal = (E_sHelocPmtAmortTermT)reader.SafeInt("lHelocPmtAmortTermT");
            item.lHelocDraw = reader.SafeInt("lHelocDraw");
            item.lHelocCalculatePrepaidInterest = reader.SafeBool("lHelocCalculatePrepaidInterest");

            item.lRateSheetXmlContentOverrideBit = reader.SafeBool("lRateSheetxmlContentOverrideBit");

            item.lLpCustomCode1 = reader.SafeString("lLpCustomCode1");
            item.lLpCustomCode1Inherit = reader.SafeString("lLpCustomCode1Inherit");
            item.lLpCustomCode1OverrideBit = reader.SafeBool("lLpCustomCode1OverrideBit");

            item.lLpCustomCode2 = reader.SafeString("lLpCustomCode2");
            item.lLpCustomCode2Inherit = reader.SafeString("lLpCustomCode2Inherit");
            item.lLpCustomCode2OverrideBit = reader.SafeBool("lLpCustomCode2OverrideBit");

            item.lLpCustomCode3 = reader.SafeString("lLpCustomCode3");
            item.lLpCustomCode3Inherit = reader.SafeString("lLpCustomCode3Inherit");
            item.lLpCustomCode3OverrideBit = reader.SafeBool("lLpCustomCode3OverrideBit");

            item.lLpCustomCode4 = reader.SafeString("lLpCustomCode4");
            item.lLpCustomCode4Inherit = reader.SafeString("lLpCustomCode4Inherit");
            item.lLpCustomCode4OverrideBit = reader.SafeBool("lLpCustomCode4OverrideBit");

            item.lLpCustomCode5 = reader.SafeString("lLpCustomCode5");
            item.lLpCustomCode5Inherit = reader.SafeString("lLpCustomCode5Inherit");
            item.lLpCustomCode5OverrideBit = reader.SafeBool("lLpCustomCode5OverrideBit");

            item.lLpInvestorCode1 = reader.SafeString("lLpInvestorCode1");
            item.lLpInvestorCode1Inherit = reader.SafeString("lLpInvestorCode1Inherit");
            item.lLpInvestorCode1OverrideBit = reader.SafeBool("lLpInvestorCode1OverrideBit");

            item.lLpInvestorCode2 = reader.SafeString("lLpInvestorCode2");
            item.lLpInvestorCode2Inherit = reader.SafeString("lLpInvestorCode2Inherit");
            item.lLpInvestorCode2OverrideBit = reader.SafeBool("lLpInvestorCode2OverrideBit");

            item.lLpInvestorCode3 = reader.SafeString("lLpInvestorCode3");
            item.lLpInvestorCode3Inherit = reader.SafeString("lLpInvestorCode3Inherit");
            item.lLpInvestorCode3OverrideBit = reader.SafeBool("lLpInvestorCode3OverrideBit");

            item.lLpInvestorCode4 = reader.SafeString("lLpInvestorCode4");
            item.lLpInvestorCode4Inherit = reader.SafeString("lLpInvestorCode4Inherit");
            item.lLpInvestorCode4OverrideBit = reader.SafeBool("lLpInvestorCode4OverrideBit");

            item.lLpInvestorCode5 = reader.SafeString("lLpInvestorCode5");
            item.lLpInvestorCode5Inherit = reader.SafeString("lLpInvestorCode5Inherit");
            item.lLpInvestorCode5OverrideBit = reader.SafeBool("lLpInvestorCode5OverrideBit");

            item.lQualRateCalculationT = (QualRateCalculationT)reader.SafeInt("lQualRateCalculationT");
            item.lQualRateCalculationFieldT1 = (QualRateCalculationFieldT)reader.SafeInt("lQualRateCalculationFieldT1");
            item.lQualRateCalculationFieldT2 = (QualRateCalculationFieldT)reader.SafeInt("lQualRateCalculationFieldT2");
            item.lQualRateCalculationAdjustment1 = reader.SafeDecimal("lQualRateCalculationAdjustment1");
            item.lQualRateCalculationAdjustment2 = reader.SafeDecimal("lQualRateCalculationAdjustment2");

            item.lQualTerm = reader.SafeInt("lQualTerm");
            item.lQualTermCalculationType = (QualTermCalculationType)reader.SafeInt("lQualTermCalculationType");

            #endregion

            return item;
        }
















    }
}
