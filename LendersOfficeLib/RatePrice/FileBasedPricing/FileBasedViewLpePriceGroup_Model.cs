﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;
using DataAccess;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    [ProtoContract]
    public sealed partial class FileBasedViewLpePriceGroup
    {
        [ProtoMember(1)]
        public Guid LpePriceGroupId { get; set;}

        [ProtoMember(2)]
	    public Guid BrokerId { get; set;}

        [ProtoMember(3)]
	    public E_LenderPaidOriginatorCompensationOptionT LenderPaidOriginatorCompensationOptionT { get; set;}

        [ProtoMember(4)]
	    public Guid LockPolicyID { get; set;}

        [ProtoMember(5)]
	    public bool DisplayPmlFeeIn100Format { get; set;}
	
        [ProtoMember(6)]
        public bool IsRoundUpLpeFee { get; set;}

        [ProtoMember(7)]
        public decimal RoundUpLpeFeeToInterval { get; set; }

        [ProtoMember(8)]
        public bool IsAlwaysDisplayExactParRateOption { get; set; }
    }
}
