﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    [ProtoContract]
    public sealed partial class FileBasedLpePriceGroupProgram
    {
        [ProtoMember(1)]
        public Guid lLpTemplateId { get; set; }

        [ProtoMember(2)]
        public List<Guid> ApplicablePricePolicyList { get; set; }
    }
}
