﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.ObjLib.PriceGroups.Model;
using LendersOfficeApp.los.RatePrice;
using LqbGrammar.DataTypes;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    public class FileBasedLoanProgramSet : AbstractLoanProgramSet
    {
        private IEnumerable<FileBasedLoanProgramTemplate> m_list = null;
        private FileBasedPolicyRelationCenter m_policyCenter = null;
        private PmiManager m_tempPmiManager;
        private List<FileBasedLpePriceGroupProgram> m_priceGroupProgramList;
        private SHA256Checksum m_fileKey;

        private FileBasedLoanProgramSet(SHA256Checksum fileKey, DateTime version, IEnumerable<FileBasedLoanProgramTemplate> list,
            FileBasedPolicyRelationCenter policyCenter,
            Dictionary<Guid, FileBasedRateOptions> rateOptionsDictionary,
            List<FileBasedLpePriceGroupProgram> priceGroupProgramList, PmiManager pmiManager)
        {
            m_fileKey = fileKey;
            m_versionD = version;
            m_list = list;

            m_policyCenter = policyCenter;
            m_priceGroupProgramList = priceGroupProgramList;
            m_tempPmiManager = pmiManager;
            foreach (var o in m_list)
            {
                FileBasedRateOptions rateOptions;
                if (rateOptionsDictionary.TryGetValue(o.lLpTemplateId, out rateOptions))
                {
                    o.SetRateOptions(rateOptions);

                }
            }

        }
        public override bool IsCurrentSnapshot
        {
            get { return false; }
        }
        private DateTime m_versionD;
        public override DateTime VersionD
        {
            get { return m_versionD; }
        }

        public override SHA256Checksum FileKey
        {
            get { return m_fileKey; }
        }
        public override AbstractRtPricePolicy GetPricePolicy(Guid policyId)
        {
            return m_policyCenter.GetPricePolicy(policyId);
        }
        public override IEnumerable<Guid> GetApplicablePolicies(Guid lLpTemplateId)
        {
            foreach (var o in m_priceGroupProgramList)
            {
                if (o.lLpTemplateId == lLpTemplateId)
                {
                    if (o.ApplicablePricePolicyList != null)
                    {
                        return o.ApplicablePricePolicyList;
                    }
                    else
                    {
                        return new List<Guid>();
                    }
                }
            }
            return new List<Guid>();

        }

        public override IPolicyRelationDb PolicyRelation
        {
            get { return m_policyCenter; }
        }

        public override PmiManager PmiManager
        {
            get { return m_tempPmiManager; }
        }

        public override int Count
        {
            get { return m_list.Count(); }
        }

        public override ILoanProgramTemplate RetrieveByIndex(int index)
        {
            return m_list.ElementAt(index);
        }

        public override ILoanProgramTemplate RetrieveByKey(Guid lLpTemplateId)
        {
            foreach (var o in m_list)
            {
                if (o.lLpTemplateId == lLpTemplateId)
                {
                    return o;
                }
            }
            throw new KeyNotFoundException("lLpTemplateId=" + lLpTemplateId);
        }

        public override string FriendlyInfo
        {
            get { return "FileBasedLoanProgramSet VersionD=[" + VersionD.ToString("yyyy-MM-dd HH:mm:ss") + "]"; }
        }

        public static AbstractLoanProgramSet Create(BrokerDB broker, SHA256Checksum priceGroupRevision, FileBasedSnapshot snapshot, IEnumerable<Guid> requestProgramIdList)
        {

            using (PerformanceStopwatch.Start("FileBasedLoanProgramSet.Create"))
            {
                HashSet<Guid> validProgramIdSet = new HashSet<Guid>();

                List<FileBasedLpePriceGroupProgram> lpePriceGroupProgramList = new List<FileBasedLpePriceGroupProgram>();
                List<FileBasedLoanProgramTemplate> loanProgramList = new List<FileBasedLoanProgramTemplate>();

                LpePriceGroup priceGroup = Utils.RetrievePriceGroup(broker.BrokerID, priceGroupRevision);

                foreach (var o in priceGroup.ProductList)
                {
                    if (o.IsValid && requestProgramIdList.Contains(o.LoanProgramTemplateId))
                    {
                        FileBasedLpePriceGroupProgram prog = new FileBasedLpePriceGroupProgram();
                        prog.lLpTemplateId = o.LoanProgramTemplateId;


                        lpePriceGroupProgramList.Add(prog);

                        validProgramIdSet.Add(o.LoanProgramTemplateId);
                    }
                }

                foreach (var id in requestProgramIdList)
                {
                    FileBasedLoanProgramTemplate o = null;
                    if (validProgramIdSet.Contains(id) && snapshot.TryGetLoanProgramTemplate(id, out o))
                    {
                        // dd 3/9/2018 - Make sure the loan program belong to same ActualBrokerId if set.
                        if (broker.ActualPricingBrokerId != Guid.Empty && broker.ActualPricingBrokerId != o.BrokerId)
                        {
                            continue;
                        }

                        loanProgramList.Add(o);
                    }
                }

                Dictionary<Guid, FileBasedRateOptions> rateOptionsDictionary = new Dictionary<Guid, FileBasedRateOptions>();
                foreach (var program in loanProgramList)
                {
                    FileBasedRateOptions rate;

                    if (!rateOptionsDictionary.TryGetValue(program.SrcRateOptionsProgId, out rate))
                    {
                        if (!snapshot.TryGetRateOptions(program.SrcRateOptionsProgId, out rate))
                        {
                            throw new NotFoundException("Could not find rate for the program.", $"Rate {program.SrcRateOptionsProgId} is not found in snapshot.");
                        }

                        rateOptionsDictionary.Add(program.SrcRateOptionsProgId, rate);
                    }
                }

                Dictionary<Guid, FileBasedRateOptions> rateOptionOverrides = new Dictionary<Guid, FileBasedRateOptions>();
                foreach (var o in snapshot.GetRateOptionPriceGroupProgramList())
                {
                    if (o.LpePriceGroupId != priceGroup.LpePriceGroupId || validProgramIdSet.Contains(o.lLpTemplateId) == false)
                    {
                        continue;
                    }

                    rateOptionOverrides.Add(o.lLpTemplateId, FileBasedRateOptions.Create(o));
                }

                int loadFileDbRateOptionCount = 0;
                Dictionary<Guid, FileBasedRateOptions> refineRateOptionsDict = new Dictionary<Guid, FileBasedRateOptions>(loanProgramList.Count);
                ConcurrentDictionary<string, Model.RateOptions> cacheRateOptionsDict = new ConcurrentDictionary<string, Model.RateOptions>();
                foreach (var o in loanProgramList)
                {
                    FileBasedRateOptions rateOptions = null;

                    if (rateOptionOverrides.TryGetValue(o.lLpTemplateId, out rateOptions) == true) // key: lLpTemplateId
                    {
                        rateOptions.SrcProgId = o.SrcRateOptionsProgId;
                    }
                    else
                    {
                        rateOptionsDictionary.TryGetValue(o.SrcRateOptionsProgId, out rateOptions); // key: SrcRateOptionsProgId
                    }

                    if (rateOptions != null)
                    {
                        if (rateOptions.LoadContentIfNeed(cacheRateOptionsDict))
                        {
                            loadFileDbRateOptionCount++;
                        }

                        // Thien's note:
                        // Don't use SrcRateOptionsProgId as key, because 2 loan products A & B can have same SrcRateOptionsProgId
                        //
                        // In "general case", maybe override A's policies but not B (I am not sure it can happen or not)
                        refineRateOptionsDict.Add(o.lLpTemplateId, rateOptions);
                    }
                }

                Tools.LogInfo($"Load {loadFileDbRateOptionCount} RateItem Lists from FileDB. {refineRateOptionsDict.Count - loadFileDbRateOptionCount} RateItem Lists are already in memory");

                Guid actualPriceGroupId = priceGroup.LpePriceGroupId;

                if (broker.ActualPricingBrokerId != Guid.Empty)
                {
                    // 7/14/2017 - dd - When the client use pricing import from production LPE, then we need to use the
                    //     price group id that on production LPE instead of current price group id.
                    //     The price group id on production LPE is need to find policy attach to the price group.
                    actualPriceGroupId = priceGroup.ActualPriceGroupId;
                }

                PopulateApplicablesPricePolicy(actualPriceGroupId, lpePriceGroupProgramList, snapshot);

                FileBasedPolicyRelationCenter policyCenter = new FileBasedPolicyRelationCenter(snapshot.GetPricePolicyList());

                PmiManager pmiManager = new PmiManager(snapshot.GetPmiPolicyAssociationList(), snapshot.GetPmiProductList(), policyCenter);
                return new FileBasedLoanProgramSet(snapshot.FileKey, snapshot.Version, loanProgramList, policyCenter, refineRateOptionsDict, lpePriceGroupProgramList, pmiManager);
            }
        }

        private static void PopulateApplicablesPricePolicy(Guid lpePriceGroupId, IEnumerable<FileBasedLpePriceGroupProgram> list, FileBasedSnapshot snapshot)
        {
            Dictionary<Guid, IEnumerable<SimplifyProductPriceAssociationItem>> directProgramDictionary = new Dictionary<Guid, IEnumerable<SimplifyProductPriceAssociationItem>>();

            foreach (var o in list)
            {
                SimplifyProductPriceAssociation item = null;

                if (snapshot.TryGetProductPriceAssociation(o.lLpTemplateId, out item))
                {
                    directProgramDictionary.Add(o.lLpTemplateId, item.Items);
                }
                else
                {
                    directProgramDictionary.Add(o.lLpTemplateId, null);
                }
            }

            IEnumerable<SimplifyProductPriceAssociationItem> priceGroupAssociation = null;

            {
                SimplifyProductPriceAssociation item = null;
                if (snapshot.TryGetProductPriceAssociation(lpePriceGroupId, out item))
                {
                    priceGroupAssociation = item.Items;
                }
            }

            Dictionary<Guid, List<FileBasedPriceGroupProgPolicyAssoc>> priceGroupProgramDictionary = new Dictionary<Guid, List<FileBasedPriceGroupProgPolicyAssoc>>();

            HashSet<Guid> lLpTemplateIdSet = new HashSet<Guid>();
            foreach (var o in list)
            {
                lLpTemplateIdSet.Add(o.lLpTemplateId);
            }

            foreach (var o in snapshot.GetPriceGroupProgPolicyAssocList())
            {
                if (o.LpePriceGroupId == lpePriceGroupId && lLpTemplateIdSet.Contains(o.ProgId))
                {
                    List<FileBasedPriceGroupProgPolicyAssoc> assocList = null;

                    if (priceGroupProgramDictionary.TryGetValue(o.ProgId, out assocList) == false)
                    {
                        assocList = new List<FileBasedPriceGroupProgPolicyAssoc>();
                        priceGroupProgramDictionary.Add(o.ProgId, assocList);
                    }
                    assocList.Add(o);
                }
            }

            foreach (var program in list)
            {
                // 1) Direct Attach Policy
                IEnumerable<SimplifyProductPriceAssociationItem> directProgramList;
                if (directProgramDictionary.TryGetValue(program.lLpTemplateId, out directProgramList) == true)
                {
                    if (directProgramList != null)
                    {
                        foreach (var o in directProgramList)
                        {
                            if (o.IsAssociate)
                            {
                                program.AddTempPolicyId(o.PricePolicyId, true);
                            }
                        }
                    }
                }

                // 2) Attach Policy From Price Group

                if (priceGroupAssociation != null)
                {
                    foreach (var o in priceGroupAssociation)
                    {
                        program.AddTempPolicyId(o.PricePolicyId, o.IsAssociate);
                    }
                }

                // 3) Policy from Price Group program.
                List<FileBasedPriceGroupProgPolicyAssoc> priceGroupProgramAssocList = null;
                if (priceGroupProgramDictionary.TryGetValue(program.lLpTemplateId, out priceGroupProgramAssocList) == true)
                {
                    if (priceGroupProgramAssocList != null)
                    {
                        foreach (var o in priceGroupProgramAssocList)
                        {
                            bool isAssoc = false;
                            if (o.AssociateType == E_TriState.Yes)
                            {
                                isAssoc = true;
                            }
                            program.AddTempPolicyId(o.PricePolicyId, isAssoc);
                        }
                    }
                }

                program.FinalizeApplicablePolicyList(snapshot);
            }
        }

    }
}
