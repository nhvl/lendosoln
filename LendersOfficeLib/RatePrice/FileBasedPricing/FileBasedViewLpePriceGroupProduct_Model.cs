﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    [ProtoContract]
    public sealed partial class FileBasedViewLpePriceGroupProduct
    {
        [ProtoMember(1)]
        public Guid LpePriceGroupId { get; set; }

        [ProtoMember(2)]
        public Guid lLpTemplateId { get; set; }

        [ProtoMember(3)]
        public decimal LenderProfitMargin { get; set; }

        [ProtoMember(4)]
        public decimal LenderMaxYspAdjust { get; set; }

        [ProtoMember(5)]
        public decimal LenderRateMargin { get; set; }

        [ProtoMember(6)]
        public decimal LenderMarginMargin { get; set; }
    }
}
