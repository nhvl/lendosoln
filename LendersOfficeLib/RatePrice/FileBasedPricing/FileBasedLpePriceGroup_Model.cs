﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    [ProtoContract]
    public sealed partial class FileBasedLpePriceGroup
    {
        [ProtoMember(1)]
        public Guid LpePriceGroupId { get; set; }

        [ProtoMember(2)]
        public string LpePriceGroupName { get; set; }

        [ProtoMember(3)]
        public List<FileBasedLpePriceGroupProgram> ProgramList { get; set; }
    }
}
