﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOfficeApp.los.RatePrice;
using DataAccess;
using System.Xml.Linq;
using LendersOffice.Common;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    public sealed partial class FileBasedRateItem : IRateItem
    {
        public bool IsSpecialKeyword
        {
            get { return __Rate == "LOCK" || __Rate == "PP" || __Rate == "IO"; }
        }
        /// <summary>
        /// Should only invoke this property when IsSpecialKeyword == true
        /// </summary>
        public E_RateSheetKeywordT RateSheetKeyword
        {
            get
            {
                switch (__Rate)
                {
                    case "LOCK": return E_RateSheetKeywordT.Lock;
                    case "PP": return E_RateSheetKeywordT.Prepay;
                    case "IO": return E_RateSheetKeywordT.IO;
                    default:
                        throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Unsupport [" + __Rate + "] for RateSheetKeyword");
                }
            }
        }
        /// <summary>
        /// Should only invoke this property when IsSpecialKeyword == true
        /// </summary>
        public int LowerLimit
        {
            get { return GetIntValue(__Point); }
        }

        /// <summary>
        /// Should only invoke this property when IsSpecialKeyword == true
        /// </summary>
        public int UpperLimit
        {
            get { return GetIntValue(__Margin); }
        }
        public decimal Rate
        {
            get { return GetDecimalValue(__Rate); }
        }
        public string Rate_rep
        {
            get { return GetRep(__Rate); }

        }
        public decimal Point
        {
            get { return GetDecimalValue(__Point); }
        }
        public string Point_rep
        {
            get { return GetRep(__Point); }
        }

        public decimal Margin { get { return GetDecimalValue(__Margin); } }
        public string Margin_rep { get { return GetRep(__Margin); } }

        public decimal QRateBase { get { return GetDecimalValue(__QRateBase); } }
        public string QRateBase_rep { get { return GetRep(__QRateBase); } }

        public decimal TeaserRate { get { return GetDecimalValue(__TeaserRate); } }
        public string TeaserRate_rep { get { return GetRep(__TeaserRate); } }

        private string GetRep(string s)
        {
            if (IsSpecialKeyword)
            {
                return s;
            }
            else
            {
                return string.Format("{0:N3}%", GetDecimalValue(s));
            }

        }
        private decimal GetDecimalValue(string s)
        {


            if (string.IsNullOrEmpty(s))
            {
                return 0;
            }
            s = s.Replace("%", "").TrimWhitespaceAndBOM();
            decimal v;

            if (decimal.TryParse(s, out v) == false)
            {
                return 0;
            }
            return v;
        }
        private int GetIntValue(string s)
        {
            int v;
            if (int.TryParse(s, out v) == false)
            {
                return 0;
            }
            return v;
        }


        public static List<FileBasedRateItem> Parse_lRateSheetXmlContent(string xml)
        {
            List<FileBasedRateItem> list = new List<FileBasedRateItem>();
            if (string.IsNullOrEmpty(xml))
            {
                return list;
            }
            if (xml.TrimWhitespaceAndBOM() == string.Empty)
            {
                return list;
            }
            try
            {

                XDocument xdoc = XDocument.Parse(xml);
                foreach (XElement rateSheetXmlContentEl in xdoc.Root.Elements("RateSheetXmlContent"))
                {
                    FileBasedRateItem item = new FileBasedRateItem();
                    foreach (XElement el in rateSheetXmlContentEl.Elements())
                    {
                        switch (el.Name.LocalName)
                        {
                            case "RecordId": break;
                            case "Rate":
                                item.__Rate = CleanDecimalValue(el.Value);
                                break;
                            case "Point":
                                item.__Point = CleanDecimalValue(el.Value);
                                break;
                            case "Margin":
                                item.__Margin = CleanDecimalValue(el.Value);
                                break;
                            case "QRateBase":
                                item.__QRateBase = CleanDecimalValue(el.Value);
                                break;
                            case "TeaserRate":
                                item.__TeaserRate = CleanDecimalValue(el.Value);
                                break;
                            default:
                                throw new Exception("Unhandle element=[" + el.Name.LocalName + "]");
                        }
                    }
                    list.Add(item);
                }
            }
            catch (Exception exc)
            {
                throw new Exception("Xml=[" + xml + "]", exc);
            }
            return list;
        }

        private static string CleanDecimalValue(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            s = s.Replace("%", "").TrimWhitespaceAndBOM();

            decimal v = 0;
            if (decimal.TryParse(s, out v) == true)
            {
                if (v == 0)
                {
                    return string.Empty; // 12/19/2013 dd - Use empty string to represent zero value.
                }
                else
                {
                    // 1/15/2014 dd - Round the decimal value to 6 digits and remove all trailing zeros to save space.
                    //s = decimal.Round(v, 6).ToString("#.######");

                    // 5/23/2016 Thien Nguyen - New requirement:
                    //      Input: "-3.79649999999999"
                    //      Expected output:  "-3.796499"

                    s = v.ToString();
                    int idx = s.IndexOf(".");
                    if (idx > 0)
                    {
                        int newLen = idx + 1 + 6;  //  (substring before character .) + (one character .)  +  (6 digits after .)
                        if (newLen < s.Length)
                        {
                            s = s.Substring(0, newLen);
                        }

                        s = s.TrimEnd('0');
                        if (s[s.Length - 1] == '.')
                        {
                            s = s.Substring(0, s.Length - 1);
                        }

                    }
                }
            }
            return s;
        }

    }
}
