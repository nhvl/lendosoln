﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    [ProtoContract]
    public sealed partial class FileBasedBroker
    {
        [ProtoMember(1)]
        public Guid BrokerId { get; set; }

        [ProtoMember(2)]
        public List<FileBasedLpePriceGroup> PriceGroupList { get; set;}
    }
}
