﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    public sealed partial class FileBasedLpePriceGroup
    {
        public void Add(FileBasedLpePriceGroupProgram program)
        {
            if (ProgramList == null)
            {
                ProgramList = new List<FileBasedLpePriceGroupProgram>();
            }
            ProgramList.Add(program);
        }
    }
}
