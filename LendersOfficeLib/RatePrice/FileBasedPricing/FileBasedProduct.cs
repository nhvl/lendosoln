﻿namespace LendersOffice.RatePrice.FileBasedPricing
{
    using System.Data;

    /// <summary>
    /// Represent data in PRODUCT table.
    /// </summary>
    public sealed partial class FileBasedProduct
    {
        /// <summary>
        /// Create object from sql.
        /// </summary>
        /// <param name="reader">Sql row contains data.</param>
        /// <returns>An object.</returns>
        public static FileBasedProduct Create(IDataReader reader)
        {
            FileBasedProduct item = new FileBasedProduct();
            item.InvestorName = (string)reader["InvestorName"];
            item.ProductCode = (string)reader["ProductCode"];
            item.IsValid = (bool)reader["IsValid"];
            item.IsRateSheetExpirationFeatureDeployed = (bool)reader["IsRateSheetExpirationFeatureDeployed"];
            item.PairingModeFor2ndLienProduct = (string)reader["PairingModeFor2ndLienProduct"];
            return item;
        }
    }
}