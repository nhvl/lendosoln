﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    public sealed partial class FileBasedBroker
    {
        public void Add(FileBasedLpePriceGroup priceGroup)
        {
            if (PriceGroupList == null)
            {
                PriceGroupList = new List<FileBasedLpePriceGroup>();
            }
            PriceGroupList.Add(priceGroup);
        }
    }
}
