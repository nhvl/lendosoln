﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    [ProtoContract]
    public sealed partial class FileBasedLoanProductFolder
    {
        [ProtoMember(1)]
        public Guid FolderId { get; set; }

        [ProtoMember(2)]
        public string FolderName { get; set; }

        [ProtoMember(3)]
        public Guid BrokerId { get; set; }

        [ProtoMember(4)]
        public Guid ParentFolderId { get; set; }

        [ProtoMember(5)]
        public bool IsLpe { get; set; }

        [ProtoMember(6)]
        public DateTime CreatedD { get; set; }
    }
}
