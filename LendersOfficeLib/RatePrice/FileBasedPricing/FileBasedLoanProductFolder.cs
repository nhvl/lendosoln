﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccess;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    public sealed partial class FileBasedLoanProductFolder
    {
        public static FileBasedLoanProductFolder Create(IDataReader reader)
        {
            FileBasedLoanProductFolder item = new FileBasedLoanProductFolder();
            item.FolderId = reader.SafeGuid("FolderId");
            item.FolderName = reader.SafeString("FolderName");
            item.BrokerId = reader.SafeGuid("BrokerId");
            item.ParentFolderId = reader.SafeGuid("ParentFolderId");
            item.IsLpe = reader.SafeBool("IsLpe");
            item.CreatedD = reader.SafeDateTime("CreatedD");

            return item;
        }
    }
}
