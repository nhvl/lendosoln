﻿namespace LendersOffice.RatePrice.FileBasedPricing
{
    using ProtoBuf;

    /// <summary>
    /// Represent data in PRODUCT table.
    /// </summary>
    [ProtoContract]
    public sealed partial class FileBasedProduct
    {
        /// <summary>
        /// Gets or sets the investor name.
        /// </summary>
        /// <value>The investor name.</value>
        [ProtoMember(1)]
        public string InvestorName { get; set; }

        /// <summary>
        /// Gets or sets the product code.
        /// </summary>
        /// <value>The product code.</value>
        [ProtoMember(2)]
        public string ProductCode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether item is valid.
        /// </summary>
        /// <value>Item is valid.</value>
        [ProtoMember(3)]
        public bool IsValid { get; set; }

        /// <summary>
        /// Gets or sets the pairing mode.
        /// </summary>
        /// <value>The pairing mode.</value>
        [ProtoMember(4)]
        public string PairingModeFor2ndLienProduct { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether rate sheet expiration feature is deployed.
        /// </summary>
        /// <value>Rate sheet expiration feature is deployed.</value>
        [ProtoMember(5)]
        public bool IsRateSheetExpirationFeatureDeployed { get; set; }
    }
}