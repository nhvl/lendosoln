﻿using System;
using DataAccess;
using ProtoBuf;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    /// <summary>
    /// // 11/25/2013 dd - Only define the property that need to serialize / deserialize for ProtoBuff.
    /// </summary>
    [ProtoContract]
    public sealed partial class FileBasedLoanProgramTemplate : ILoanProgramTemplate
    {
        [ProtoMember(1)]
        public Guid lLpTemplateId { get; set;  }

        [ProtoMember(2)]
        public Guid BrokerId { get; set;  }

        [ProtoMember(3)]
        public string lLpTemplateNm { get; set;  }

        [ProtoMember(4)]
        public string lLendNm { get; set;  }

        [ProtoMember(5)]
        public E_sLT lLT { get; set;  }

        [ProtoMember(6)]
        public E_sLienPosT lLienPosT { get; set;  }

        [ProtoMember(7)]
        public decimal lQualR { get; set;  }

        [ProtoMember(8)]
        public int lTerm_internal { get; set;  }

        [ProtoMember(9)]
        public int lDue_internal { get; set;  }

        [ProtoMember(10)]
        public int lLockedDays { get; set;  }

        [ProtoMember(11)]
        public decimal lReqTopR { get; set;  }


        [ProtoMember(12)]
        public decimal lReqBotR { get; set;  }

        [ProtoMember(13)]
        public decimal lRadj1stCapR { get; set;  }

        [ProtoMember(14)]
        public int lRadj1stCapMon { get; set;  }

        [ProtoMember(15)]
        public decimal lRAdjCapR { get; set;  }

        [ProtoMember(16)]
        public int lRAdjCapMon { get; set;  }

        [ProtoMember(17)]
        public decimal lRAdjLifeCapR { get; set;  }

        [ProtoMember(18)]
        public decimal lRAdjMarginR { get; set; }

        [ProtoMember(19)]
        public decimal lRAdjIndexR { get; set;  }

        [ProtoMember(20)]
        public decimal lRAdjFloorR { get; set;  }


        [ProtoMember(21)]
        public E_sRAdjRoundT lRAdjRoundT { get; set; }

        [ProtoMember(22)]
        public int lPmtAdjCapMon { get; set;  }

        [ProtoMember(23)]
        public int lPmtAdjRecastPeriodMon { get; set;  }

        [ProtoMember(24)]
        public int lPmtAdjRecastStop { get; set;  }

        [ProtoMember(25)]
        public decimal lBuydwnR1 { get; set;  }

        [ProtoMember(26)]
        public decimal lBuydwnR2 { get; set;  }

        [ProtoMember(27)]
        public decimal lBuydwnR3 { get; set;  }

        [ProtoMember(28)]
        public decimal lBuydwnR4 { get; set;  }

        [ProtoMember(29)]
        public decimal lBuydwnR5 { get; set;  }

        [ProtoMember(30)]
        public int lBuydwnMon1 { get; set;  }

        [ProtoMember(31)]
        public int lBuydwnMon2 { get; set;  }

        [ProtoMember(32)]
        public int lBuydwnMon3 { get; set;  }

        [ProtoMember(33)]
        public int lBuydwnMon4 { get; set;  }

        [ProtoMember(34)]
        public int lBuydwnMon5 { get; set;  }

        [ProtoMember(35)]
        public int lGradPmtYrs { get; set;  }


        [ProtoMember(36)]
        public decimal lGradPmtR { get; set;  }

        [ProtoMember(37)]
        public int lIOnlyMon { get; set;  }

        [ProtoMember(38)]
        public bool lHasVarRFeature { get; set;  }

        [ProtoMember(39)]
        public string lVarRNotes { get; set;  }

        [ProtoMember(40)]
        public bool lAprIncludesReqDeposit { get; set;  }

        [ProtoMember(41)]
        public bool lHasDemandFeature { get; set;  }

        [ProtoMember(42)]
        public string lLateDays { get; set;  }

        [ProtoMember(43)]
        public string lLateChargePc { get; set;  }

        [ProtoMember(44)]
        public E_sPrepmtPenaltyT lPrepmtPenaltyT { get; set;  }

        [ProtoMember(45)]
        public E_sAssumeLT lAssumeLT { get; set;  }

        [ProtoMember(46)]
        public Guid lCcTemplateId { get; set;  }

        [ProtoMember(47)]
        public string lFilingF { get; set;  }

        [ProtoMember(48)]
        public string lLateChargeBaseDesc { get; set;  }

        [ProtoMember(49)]
        public decimal lPmtAdjMaxBalPc { get; set;  }

        [ProtoMember(50)]
        public E_sFinMethT lFinMethT { get; set;  }

        [ProtoMember(51)]
        public string lFinMethDesc { get; set;  }

        [ProtoMember(52)]
        public E_sPrepmtRefundT lPrepmtRefundT { get; set;  }

        [ProtoMember(53)]
        public Guid FolderId { get; set;  }


        [ProtoMember(54)]
        public DateTime lRateSheetEffectiveD_internal { get; set; }


        [ProtoMember(55)]
        public DateTime lRateSheetExpirationD_internal { get; set; }


        [ProtoMember(56)]
        public bool IsMaster { get; set;  }

        [ProtoMember(57)]
        public decimal lLpeFeeMin { get; set;  }


        [ProtoMember(58)]
        public decimal lLpeFeeMax { get; set;  }

        [ProtoMember(59)]
        public string PairingProductIds { get; set;  }

        [ProtoMember(60)]
        public Guid lBaseLpId { get; set;  }

        [ProtoMember(61)]
        public bool IsEnabled { get; set;  }

        [ProtoMember(62)]
        public Guid lArmIndexGuid { get; set; }

        [ProtoMember(63)]
        public string lArmIndexBasedOnVstr { get; set; }

        [ProtoMember(64)]
        public string lArmIndexCanBeFoundVstr { get; set; }

        [ProtoMember(65)]
        public bool lArmIndexAffectInitIRBit { get; set; }

        [ProtoMember(66)]
        public decimal lRAdjRoundToR { get; set; }

        [ProtoMember(67)]
        public DateTime lArmIndexEffectiveD_internal { get; set; }

        [ProtoMember(68)]
        public E_sFreddieArmIndexT lFreddieArmIndexT { get; set; }

        [ProtoMember(69)]
        public string lArmIndexNameVstr { get; set; }

        [ProtoMember(70)]
        public E_sArmIndexT lArmIndexT { get; set; }

        [ProtoMember(71)]
        public string lArmIndexNotifyAtLeastDaysVstr { get; set; }

        [ProtoMember(72)]
        public string lArmIndexNotifyNotBeforeDaysVstr { get; set; }

        [ProtoMember(73)]
        public string lLpTemplateNmInherit { get; set;  }

        [ProtoMember(74)]
        public bool lLpTemplateNmOverrideBit { get; set;  }

        [ProtoMember(75)]
        public Guid lCcTemplateIdInherit { get; set;  }

        [ProtoMember(76)]
        public bool lCcTemplateIdOverrideBit { get; set;  }

        [ProtoMember(78)]
        public int lLockedDaysInherit { get; set;  }

        [ProtoMember(79)]
        public bool lLockedDaysOverrideBit { get; set;  }

        [ProtoMember(80)]
        public decimal lLpeFeeMinInherit { get; set;  }

        [ProtoMember(81)]
        public bool lLpeFeeMinOverrideBit { get; set;  }

        [ProtoMember(82)]
        public decimal lLpeFeeMaxInherit { get; set;  }

        [ProtoMember(83)]
        public bool lLpeFeeMaxOverrideBit { get; set;  }

        [ProtoMember(84)]
        public bool IsEnabledInherit { get; set;  }

        [ProtoMember(85)]
        public bool IsEnabledOverrideBit { get; set;  }

        [ProtoMember(86)]
        public int lPpmtPenaltyMon { get; set;  }

        [ProtoMember(87)]
        public int lPpmtPenaltyMonInherit { get; set;  }

        [ProtoMember(88)]
        public bool lPpmtPenaltyMonOverrideBit { get; set;  }

        [ProtoMember(89)]
        public decimal lRateDelta { get; set;  }

        [ProtoMember(90)]
        public decimal lFeeDelta { get; set;  }

        [ProtoMember(91)]
        public int lPpmtPenaltyMonLowerSearch { get; set;  }

        [ProtoMember(92)]
        public int lLockedDaysLowerSearch { get; set;  }

        [ProtoMember(93)]
        public int lLockedDaysLowerSearchInherit { get; set;  }

        [ProtoMember(94)]
        public int lPpmtPenaltyMonLowerSearchInherit { get; set;  }

        [ProtoMember(95)]
        public string PairingProductIdsInherit { get; set;  }

        [ProtoMember(96)]
        public bool PairingProductIdsOverrideBit { get; set;  }

        [ProtoMember(97)]
        public string lLpInvestorNm { get; set;  }

        [ProtoMember(98)]
        public string lLendNmInherit { get; set;  }

        [ProtoMember(99)]
        public bool lLendNmOverrideBit { get; set;  }

        [ProtoMember(100)]
        public string lRateOptionBaseId { get; set;  }

        [ProtoMember(101)]
        public Guid PairIdFor1stLienProdGuid { get; set;  }

        [ProtoMember(102)]
        public Guid PairIdFor1stLienProdGuidInherit { get; set;  }

        [ProtoMember(103)]
        public decimal lLpeFeeMinParam { get; set;  }

        [ProtoMember(104)]
        public bool lLpeFeeMinAddRuleAdjInheritBit { get; set;  }

        [ProtoMember(105)]
        public bool IsLpe { get; set;  }

        [ProtoMember(106)]
        public bool lIsArmMarginDisplayed_internal { get; set;  }

        [ProtoMember(107)]
        public string ProductCode { get; set;  }

        [ProtoMember(108)]
        public bool IsOptionArm { get; set;  }

        [ProtoMember(109)]
        public decimal lRateDeltaInherit { get; set;  }

        [ProtoMember(110)]
        public decimal lFeeDeltaInherit { get; set;  }

        [ProtoMember(111)]
        public Guid SrcRateOptionsProgIdInherit { get; set;  }

        [ProtoMember(112)]
        public Guid SrcRateOptionsProgId { get; set;  }

        [ProtoMember(113)]
        public E_sLpDPmtT lLpDPmtT_internal { get; set;  }

        [ProtoMember(114)]
        public E_sLpQPmtT lLpQPmtT_internal { get; set;  }

        [ProtoMember(115)]
        public bool lHasQRateInRateOptions { get; set; }

        [ProtoMember(116)]
        public bool IsLpeDummyProgram { get; set;  }

        [ProtoMember(117)]
        public bool IsPairedOnlyWithSameInvestor { get; set;  }

        [ProtoMember(118)]
        public int lIOnlyMonLowerSearch { get; set;  }

        [ProtoMember(119)]
        public int lIOnlyMonLowerSearchInherit { get; set;  }

        [ProtoMember(120)]
        public bool lIOnlyMonLowerSearchOverrideBit { get; set;  }

        [ProtoMember(121)]
        public int lIOnlyMonUpperSearch { get; set;  }

        [ProtoMember(122)]
        public int lIOnlyMonUpperSearchInherit { get; set;  }

        [ProtoMember(123)]
        public bool lIOnlyMonUpperSearchOverrideBit { get; set;  }

        [ProtoMember(124)]
        public bool CanBeStandAlone2nd { get; set;  }

        [ProtoMember(125)]
        public bool lDtiUsingMaxBalPc { get; set;  }

        [ProtoMember(126)]
        public bool IsMasterPriceGroup { get; set;  }

        [ProtoMember(127)]
        public string lLpProductType { get; set;  }


        [ProtoMember(128)]
        public E_sRAdjFloorBaseT lRAdjFloorBaseT { get; set;  }

        [ProtoMember(129)]
        public bool IsConvertibleMortgage { get; set;  }

        [ProtoMember(130)]
        public bool lLpmiSupportedOutsidePmi { get; set;  }

        [ProtoMember(131)]
        public bool lWholesaleChannelProgram { get; set;  }

        [ProtoMember(132)]
        public int lHardPrepmtPeriodMonths { get; set;  }

        [ProtoMember(133)]
        public int lSoftPrepmtPeriodMonths { get; set;  }

        [ProtoMember(134)]
        public E_sHelocPmtBaseT lHelocQualPmtBaseT_internal { get; set;  }

        [ProtoMember(135)]
        public E_sHelocPmtPcBaseT lHelocQualPmtPcBaseT_internal { get; set;  }

        [ProtoMember(136)]
        public decimal lHelocQualPmtMb_internal { get; set;  }


        [ProtoMember(137)]
        public E_sHelocPmtBaseT lHelocPmtBaseT { get; set;  }

        [ProtoMember(138)]
        public E_sHelocPmtPcBaseT lHelocPmtPcBaseT { get; set;  }

        [ProtoMember(139)]
        public decimal lHelocPmtMb { get; set;  }

        [ProtoMember(140)]
        public int lHelocDraw { get; set;  }

        [ProtoMember(141)]
        public decimal lPmtAdjCapR { get; set; }

        [ProtoMember(142)]
        public bool lRateSheetXmlContentOverrideBit { get; set; }

        [ProtoMember(143)]
        public string lLpCustomCode1 { get; set; }

        [ProtoMember(144)]
        public string lLpCustomCode1Inherit { get; set; }

        [ProtoMember(145)]
        public bool lLpCustomCode1OverrideBit { get; set; }

        [ProtoMember(146)]
        public string lLpCustomCode2 { get; set; }

        [ProtoMember(147)]
        public string lLpCustomCode2Inherit { get; set; }

        [ProtoMember(148)]
        public bool lLpCustomCode2OverrideBit { get; set; }

        [ProtoMember(149)]
        public string lLpCustomCode3 { get; set; }

        [ProtoMember(150)]
        public string lLpCustomCode3Inherit { get; set; }

        [ProtoMember(151)]
        public bool lLpCustomCode3OverrideBit { get; set; }

        [ProtoMember(152)]
        public string lLpCustomCode4 { get; set; }

        [ProtoMember(153)]
        public string lLpCustomCode4Inherit { get; set; }

        [ProtoMember(154)]
        public bool lLpCustomCode4OverrideBit { get; set; }

        [ProtoMember(155)]
        public string lLpCustomCode5 { get; set; }

        [ProtoMember(156)]
        public string lLpCustomCode5Inherit { get; set; }

        [ProtoMember(157)]
        public bool lLpCustomCode5OverrideBit { get; set; }

        [ProtoMember(158)]
        public E_sHelocPmtFormulaT lHelocQualPmtFormulaT_internal { get; set; }

        [ProtoMember(159)]
        public E_sHelocPmtFormulaRateT lHelocQualPmtFormulaRateT_internal { get; set; }

        [ProtoMember(160)]
        public E_sHelocPmtAmortTermT lHelocQualPmtAmortTermT_internal { get; set; }

        [ProtoMember(161)]
        public E_sHelocPmtFormulaT lHelocPmtFormulaT { get; set; }

        [ProtoMember(162)]
        public E_sHelocPmtFormulaRateT lHelocPmtFormulaRateT { get; set; }

        [ProtoMember(163)]
        public E_sHelocPmtAmortTermT lHelocPmtAmortTermT { get; set; }

        [ProtoMember(164)]
        public string lLpInvestorCode1 { get; set; }

        [ProtoMember(165)]
        public string lLpInvestorCode1Inherit { get; set; }

        [ProtoMember(166)]
        public bool lLpInvestorCode1OverrideBit { get; set; }

        [ProtoMember(167)]
        public string lLpInvestorCode2 { get; set; }

        [ProtoMember(168)]
        public string lLpInvestorCode2Inherit { get; set; }

        [ProtoMember(169)]
        public bool lLpInvestorCode2OverrideBit { get; set; }

        [ProtoMember(170)]
        public string lLpInvestorCode3 { get; set; }

        [ProtoMember(171)]
        public string lLpInvestorCode3Inherit { get; set; }

        [ProtoMember(172)]
        public bool lLpInvestorCode3OverrideBit { get; set; }

        [ProtoMember(173)]
        public string lLpInvestorCode4 { get; set; }

        [ProtoMember(174)]
        public string lLpInvestorCode4Inherit { get; set; }

        [ProtoMember(175)]
        public bool lLpInvestorCode4OverrideBit { get; set; }

        [ProtoMember(176)]
        public string lLpInvestorCode5 { get; set; }

        [ProtoMember(177)]
        public string lLpInvestorCode5Inherit { get; set; }

        [ProtoMember(178)]
        public bool lLpInvestorCode5OverrideBit { get; set; }

        [ProtoMember(179)]
        public QualRateCalculationT lQualRateCalculationT { get; set; } = QualRateCalculationT.FlatValue;

        [ProtoMember(180)]
        public QualRateCalculationFieldT lQualRateCalculationFieldT1 { get; set; } = QualRateCalculationFieldT.LpeUploadValue;

        [ProtoMember(181)]
        public QualRateCalculationFieldT lQualRateCalculationFieldT2 { get; set; } = QualRateCalculationFieldT.LpeUploadValue;

        [ProtoMember(182)]
        public decimal lQualRateCalculationAdjustment1 { get; set; }

        [ProtoMember(183)]
        public decimal lQualRateCalculationAdjustment2 { get; set; }

        [ProtoMember(184)]
        public bool lHelocCalculatePrepaidInterest { get; set; }

        [ProtoMember(185)]
        public int lQualTerm { get; set; }

        [ProtoMember(186)]
        public QualTermCalculationType lQualTermCalculationType { get; set; } = QualTermCalculationType.Standard;

        [ProtoMember(187)]
        public string ProductIdentifier { get; set; }

        [ProtoMember(188)]
        public bool IsNonQmProgram { get; set; }

        [ProtoMember(189)]
        public bool IsDisplayInNonQmQuickPricer { get; set; }

    }
}
