﻿using System;
using System.Collections.Generic;
using Toolbox;

namespace LendersOffice.RatePrice.FileBasedPricing
{
    public sealed partial class FileBasedLpePriceGroupProgram
    {
        private HashSet<Guid> m_tempPolicyList = new HashSet<Guid>();

        public void AddTempPolicyId(Guid policyId, bool isAssociate) 
        {
            if (isAssociate)
            {
                m_tempPolicyList.Add(policyId);
            }
            else
            {
                m_tempPolicyList.Remove(policyId);
            }
        }

        internal void FinalizeApplicablePolicyList(FileBasedSnapshot snapshot)
        {
            HashSet<Guid> list = new HashSet<Guid>();

            foreach (var id in m_tempPolicyList)
            {
                if (list.Contains(id) == false)
                {
                    list.Add(id);
                }

                IEnumerable<Guid> ancestorList = snapshot.GetAncestorPricePolicyIdList(id);

                // Get all parents
                foreach (var parentPolicy in ancestorList)
                {
                    if (parentPolicy == Guid.Empty)
                    {
                        continue;
                    }

                    list.Add(parentPolicy);
                }
            }

            CSortedListOfGroups sortedList = new CSortedListOfGroups(5);
            foreach (Guid id in list)
            {
                var pricePolicy = snapshot.GetPricePolicy(id);
                sortedList.Add(pricePolicy.MutualExclusiveExecSortedId, id);
            }

            ApplicablePricePolicyList = new List<Guid>();

            foreach (Guid id in sortedList.MergedArrayBySortedOrder)
            {
                ApplicablePricePolicyList.Add(id);
            }
        }
    }
}