﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtoBuf;
using LqbGrammar.DataTypes;

namespace LendersOffice.RatePrice.FileBasedPricing
{


    [ProtoContract]
    public sealed partial class FileBasedRateOptions
    {
        [ProtoMember(1)]
        public Guid SrcProgId { get; set; }

        [ProtoMember(2)]
        public string ContentKeyString { get; set; }

        public long LpeAcceptableRsFileVersionNumber { get; set; }

        public string LpeAcceptableRsFileId { get; set; }

        public List<FileBasedRateItem> RateItemList { get; set; }

        public DateTime lRateSheetDownloadStartD { get; set; }

        public DateTime lRateSheetDownloadEndD { get; set; }
    }
}
