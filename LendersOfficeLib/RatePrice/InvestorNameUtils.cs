﻿namespace LendersOffice.RatePrice
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using Admin;
    using Constants;
    using DataAccess;
    using FileBasedPricing;

    /// <summary>
    /// Utilities class for retrieve investor name and invetor product.
    /// </summary>
    public static class InvestorNameUtils
    {
        /// <summary>
        /// Return a list of investor name and product code that has rate sheet expiration feature.
        /// </summary>
        /// <param name="broker">The broker.</param>
        /// <returns>List of investor name and product code that has rate sheet expiration feature.</returns>
        public static List<KeyValuePair<string, string>> ListInvestorProductHasRateSheetExpirationFeature(BrokerDB broker)
        {
            if (broker.ActualPricingBrokerId == Guid.Empty)
            {
                return ListInvestorProductHasRateSheetExpirationFeatureFromDatabase();
            }
            else
            {
                return ListInvestorProductHasRateSheetExpirationFeatureFromActualPricingBroker(broker);
            }
        }

        /// <summary>
        /// Return a list of investor name and product code belong to the broker acocunt.
        /// </summary>
        /// <param name="broker">The broker account.</param>
        /// <returns>List of investor name and product code.</returns>
        public static IEnumerable<KeyValuePair<string, string>> ListInvestorProductByBroker(BrokerDB broker)
        {
            if (broker.ActualPricingBrokerId == Guid.Empty)
            {
                return ListInvestorProductByBrokerFromDatabase(broker.BrokerID);
            }
            else
            {
                return ListInvestorProductByBrokerFromActualPricingBroker(broker);
            }
        }

        /// <summary>
        /// Return a list of active investor name and id belong to the broker acocunt.
        /// </summary>
        /// <param name="broker">The broker account.</param>
        /// <returns>List of investor id and name.</returns>
        public static IEnumerable<KeyValuePair<int, string>> ListActiveLpeInvestor(BrokerDB broker)
        {
            if (broker.ActualPricingBrokerId == Guid.Empty)
            {
                return ListActiveLpeInvestorFromDatabase(broker.BrokerID);
            }
            else
            {
                return ListActiveLpeInvestorFromFromActualPricingBroker(broker);
            }
        }

        /// <summary>
        /// Return a list of all investor name.
        /// </summary>
        /// <param name="broker">The broker account.</param>
        /// <returns>List of all investor name.</returns>
        public static IEnumerable<IInvestorName> ListAllInvestorNames(BrokerDB broker)
        {
            IEnumerable<IInvestorName> list = null;

            if (ConstStage.UseLpeDatabaseForPricing)
            {
                list = ListInvestorNamesFromLpeDatabase(isActiveFilter: null /* all */);
            }
            else
            {
                list = ListInvestorNamesFromSnapshot(broker, isActiveFilter: null /* all */);
            }

            return list.OrderBy(o => o.InvestorName);
        }

        /// <summary>
        /// Return a list of active investor name.
        /// </summary>
        /// <param name="broker">The broker account.</param>
        /// <returns>List of actictive investor name.</returns>
        public static IEnumerable<IInvestorName> ListActiveInvestorNames(BrokerDB broker)
        {
            IEnumerable<IInvestorName> list = null;

            if (ConstStage.UseLpeDatabaseForPricing)
            {
                list = ListInvestorNamesFromLpeDatabase(isActiveFilter: true /* active only */);
            }
            else
            {
                list = ListInvestorNamesFromSnapshot(broker, isActiveFilter: true /* active only */);
            }

            return list.OrderBy(o => o.InvestorName);
        }

        /// <summary>
        /// Return a list of investor name and product code that has rate sheet expiration feature.
        /// </summary>
        /// <returns>List of investor name and product code that has rate sheet expiration feature.</returns>
        private static List<KeyValuePair<string, string>> ListInvestorProductHasRateSheetExpirationFeatureFromDatabase()
        {
            var list = new List<KeyValuePair<string, string>>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListInvestorProductHasRSE"))
            {
                while (reader.Read())
                {
                    list.Add(new KeyValuePair<string, string>((string)reader["InvestorName"], (string)reader["ProductCode"]));
                }
            }

            return list;
        }

        /// <summary>
        /// Return a list of investor name and product code that has rate sheet expiration feature.
        /// </summary>
        /// <param name="broker">The broker account.</param>
        /// <returns>List of investor name and product code that has rate sheet expiration feature.</returns>
        private static List<KeyValuePair<string, string>> ListInvestorProductHasRateSheetExpirationFeatureFromActualPricingBroker(BrokerDB broker)
        {
            var list = new List<KeyValuePair<string, string>>();

            var snapshot = broker.RetrieveLatestManualImport();

            foreach (var product in snapshot.GetProductList())
            {
                if (product.IsValid && product.IsRateSheetExpirationFeatureDeployed)
                {
                    list.Add(new KeyValuePair<string, string>(product.InvestorName, product.ProductCode));
                }
            }

            return list;
        }

        /// <summary>
        /// Return a list of active investor name and product code belong to the broker account.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <returns>List of investor name and product code.</returns>
        private static IEnumerable<KeyValuePair<string, string>> ListInvestorProductByBrokerFromDatabase(Guid brokerId)
        {
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListInvestorProductByBrokerId", new SqlParameter("@sBrokerId", brokerId)))
            {
                while (reader.Read())
                {
                    list.Add(new KeyValuePair<string, string>((string)reader["lLpInvestorNm"], (string)reader["ProductCode"]));
                }
            }

            return list;
        }

        /// <summary>
        /// Return a list of active investor name and product code belong to the broker account.
        /// </summary>
        /// <param name="broker">The broker account.</param>
        /// <returns>List of investor name and product code.</returns>
        private static IEnumerable<KeyValuePair<string, string>> ListInvestorProductByBrokerFromActualPricingBroker(BrokerDB broker)
        {
            // Based from stored procedure ListInvestorProductByBrokerId.
            var snapshot = broker.RetrieveLatestManualImport();

            var programList = snapshot.GetLoanProgramTemplateByBrokerId(broker.ActualPricingBrokerId);

            HashSet<string> investorNameSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();

            foreach (var program in programList)
            {
                if (program.IsMaster == false && program.IsEnabled && investorNameSet.Contains(program.lLpInvestorNm + "::" + program.ProductCode) == false)
                {
                    investorNameSet.Add(program.lLpInvestorNm + "::" + program.ProductCode);

                    list.Add(new KeyValuePair<string, string>(program.lLpInvestorNm, program.ProductCode));
                }
            }

            return list;
        }

        /// <summary>
        /// Return a list of active investor name and id belong to the broker account.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <returns>List of investor id and name.</returns>
        private static IEnumerable<KeyValuePair<int, string>> ListActiveLpeInvestorFromDatabase(Guid brokerId)
        {
            List<KeyValuePair<int, string>> list = new List<KeyValuePair<int, string>>();

            SqlParameter[] parameters = 
            {
                    new SqlParameter("@BrokerId", brokerId)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListInvestorByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    list.Add(new KeyValuePair<int, string>((int)reader["InvestorId"], (string)reader["InvestorName"]));
                }
            }

            return list;
        }

        /// <summary>
        /// Return a list of active investor name and id belong to the broker account.
        /// </summary>
        /// <param name="broker">The broker account.</param>
        /// <returns>List of investor id and name.</returns>
        private static IEnumerable<KeyValuePair<int, string>> ListActiveLpeInvestorFromFromActualPricingBroker(BrokerDB broker)
        {
            // Based from stored procedure ListInvestorByBrokerId.
            var snapshot = broker.RetrieveLatestManualImport();

            Dictionary<string, int> dictionary = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);

            foreach (var investorName in snapshot.GetInvestorNameList())
            {
                dictionary[investorName.InvestorName] = investorName.InvestorId;
            }

            var programList = snapshot.GetLoanProgramTemplateByBrokerId(broker.ActualPricingBrokerId);

            HashSet<string> investorNameSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

            List<KeyValuePair<int, string>> list = new List<KeyValuePair<int, string>>();

            foreach (var program in programList)
            {
                if (program.IsMaster == false && program.IsEnabled && investorNameSet.Contains(program.lLpInvestorNm) == false)
                {
                    investorNameSet.Add(program.lLpInvestorNm);

                    int investorId;

                    if (dictionary.TryGetValue(program.lLpInvestorNm, out investorId))
                    {
                        list.Add(new KeyValuePair<int, string>(investorId, program.lLpInvestorNm));
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Retrieve a list of investor name from snapshot.
        /// </summary>
        /// <param name="broker">The broker account.</param>
        /// <param name="isActiveFilter">Filter for is active investor name.</param>
        /// <returns>List of investor name.</returns>
        private static IEnumerable<IInvestorName> ListInvestorNamesFromSnapshot(BrokerDB broker, bool? isActiveFilter)
        {
            var snapshot = broker == null ? FileBasedSnapshot.RetrieveLatestManualImport() : broker.RetrieveLatestManualImport();

            List<IInvestorName> list = new List<IInvestorName>();

            foreach (var o in snapshot.GetInvestorNameList())
            {
                if (isActiveFilter.HasValue && o.IsValid != isActiveFilter.Value)
                {
                    continue; // Skip filter does not match.
                }

                list.Add(new InternalInvestorName(o));
            }

            return list;
        }

        /// <summary>
        /// Retrieve a list of investor name from Lpe database.
        /// </summary>
        /// <param name="isActiveFilter">Filter for is active investor name.</param>
        /// <returns>List of investor name.</returns>
        private static IEnumerable<IInvestorName> ListInvestorNamesFromLpeDatabase(bool? isActiveFilter)
        {
            SqlParameter[] parameters = null;

            if (isActiveFilter.HasValue)
            {
                parameters = new SqlParameter[] { new SqlParameter("@IsValid", isActiveFilter.Value) };
            }

            List<IInvestorName> list = new List<IInvestorName>();
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListInvestorNames", parameters))
            {
                while (reader.Read())
                {
                    list.Add(new InternalInvestorName(reader));
                }
            }

            return list;
        }

        /// <summary>
        /// Implementation of investor name.
        /// </summary>
        private class InternalInvestorName : IInvestorName
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="InternalInvestorName" /> class.
            /// </summary>
            /// <param name="reader">Database record.</param>
            internal InternalInvestorName(IDataReader reader)
            {
                this.InvestorId = reader.SafeInt("InvestorId");
                this.InvestorName = reader.SafeString("InvestorName");
                this.IsValid = reader.SafeBool("IsValid");
                this.RateLockCutOffTime = reader.SafeDateTimeNullable("RateLockCutoffTime");
                this.UseLenderTimezoneForRsExpiration = reader.SafeBool("UseLenderTimezoneForRsExpiration");
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="InternalInvestorName" /> class.
            /// </summary>
            /// <param name="item">File base record.</param>
            internal InternalInvestorName(FileBasedInvestorName item)
            {
                // InvestorName, IsValid, RateLockCutOffTime, UseLenderTimezoneForRsExpiration, InvestorId
                this.InvestorId = item.InvestorId;
                this.InvestorName = item.InvestorName;
                this.IsValid = item.IsValid;

                if (item.RateLockCutOffTime == DateTime.MinValue)
                {
                    this.RateLockCutOffTime = null;
                }
                else
                {
                    this.RateLockCutOffTime = item.RateLockCutOffTime;
                }

                this.UseLenderTimezoneForRsExpiration = item.UseLenderTimezoneForRsExpiration;
            }

            /// <summary>
            /// Gets investor id.
            /// </summary>
            /// <value>Investor id.</value>
            public int InvestorId { get; private set; }

            /// <summary>
            /// Gets the investor name.
            /// </summary>
            /// <value>The investor name.</value>
            public string InvestorName { get; private set; }

            /// <summary>
            /// Gets a value indicating whether item is valid.
            /// </summary>
            /// <value>Item is valid.</value>
            public bool IsValid { get; private set; }

            /// <summary>
            /// Gets rate lock cut off time.
            /// </summary>
            /// <value>Rate lock cut off time.</value>
            public DateTime? RateLockCutOffTime { get; private set; }

            /// <summary>
            /// Gets a value indicating whether use lender time zone for ratesheet expiration.
            /// </summary>
            /// <value>Use lender time zone for ratesheet expiration.</value>
            public bool UseLenderTimezoneForRsExpiration { get; private set; }
        }
    }
}