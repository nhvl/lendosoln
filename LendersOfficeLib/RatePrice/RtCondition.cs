﻿using System.Text;
using System.Xml;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice
{
    public class RtCondition
    {

        private string m_exprString;
        private object m_expr; // XmlNode or PostfixExpression
        private XmlNode m_debugExprNode;
        private int m_xmlSize;

        public RtCondition(string exprString)
        {
            m_exprString = exprString;

            if (exprString != null)
            {
                m_xmlSize = 20 + 2 * exprString.Length;
                Category = LpeKeywordUtil.GetLpeKeywordCategory(m_exprString);
            }
        }

        public bool Evaluate(XmlDocument xmlDocEval, CSymbolTable symbolTable, EvalStack stack, StringBuilder debugInfo, bool isLogDetailInfo)
        {
            XmlNode exprNode;

            if (m_expr == null)
            {
                lock (this)
                {
                    if (m_expr == null)
                    {

                        exprNode = null;
                        object tmpExpr = CRuleEvalTool.ParseToXmlNodeOrPostfixExpr(symbolTable, xmlDocEval, m_exprString, ref exprNode);
                        if (exprNode != null)
                            m_xmlSize = 20 + 4 * exprNode.InnerXml.Length;


                        m_exprString = null; // remove it when don't need it anymore to save memory.
                        if (tmpExpr is XmlNode)
                        {
                            // "convert to postfix" error => using old method to evaluate expression
                            m_debugExprNode = exprNode;
                            m_expr = exprNode;
                        }
                        else
                        {
                            // typeof(tmpExpr) is PostfixExpression
                            m_debugExprNode = null;
                            m_expr = tmpExpr;
                        }
                    }

                }
            }
 
            decimal rawResult;
            object expr = m_expr;
            exprNode = m_debugExprNode;

            if (exprNode != null)
                rawResult = CRuleEvalTool.ExpressionDispatch(exprNode, 0, symbolTable);
            else
                rawResult = ((PostfixExpression)expr).Evaluate(symbolTable, stack, debugInfo, isLogDetailInfo);

            if (0 == rawResult)
                return false;
            else if (1 == rawResult)
                return true;
            else
                throw new CBaseException(ErrorMessages.Generic, "Invalid expression.  True or false expression expected.");
        }

        public int EstimateSize()
        {
            return 24 + m_xmlSize;
        }

        public LpeKeywordCategory Category { get; private set; }

        public string RecoverExpr()
        {
            string str = m_exprString;
            if (str != null && str.Length > 0)
                return str;

            lock (this)
            {
                XmlNode xmlNode = m_debugExprNode;
                if (xmlNode != null)
                    return CRuleEvalTool.ErrorExpr2Str(xmlNode);

                PostfixExpression postfixExpr = m_expr as PostfixExpression;
                if (postfixExpr != null)
                    return postfixExpr.ToString();

            }

            return "Can not recover the expression.";
        }
    }
}