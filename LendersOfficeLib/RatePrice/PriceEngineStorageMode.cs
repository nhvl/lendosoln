﻿namespace LendersOffice.RatePrice
{
    /// <summary>
    /// Use to determine which price engine storage class to instantiate.
    /// </summary>
    public enum PriceEngineStorageMode
    {
        /// <summary>
        /// Store the content to persistent storage (FileDB or S3).
        /// </summary>
        Normal = 0,

        /// <summary>
        /// Store the content on local computer only. Use this in LpeUpdate executable.
        /// </summary>
        LocalFileOnly = 1,

        /// <summary>
        /// The actual content is store in FileDB but it can be cache in local file. Should only be use in pricing.
        /// </summary>
        NormalWithCache = 2,

        /// <summary>
        /// The actual content is store in S3. Only use on AWS instance.
        /// </summary>
        Aws = 3
    }
}
