﻿namespace LendersOfficeApp.los.RatePrice
{
    using DataAccess;
    using System;
    using LendersOffice.Common;

    public class RateMergeGroupKey : IComparable
    {
        private int m_lTerm = 0;
        private int m_lDue = 0;
        private int m_lRAdj1stCapMon = 0;
        private int m_lRAdjCapMon = 0;
        private E_sFinMethT m_lFinMethT;
        private string m_lProductType = "";
        private int m_lBuydwnR1 = 0;
        private int m_lBuydwnR2 = 0;
        private int m_lBuydwnR3 = 0;
        private int m_lBuydwnR4 = 0;
        private int m_lBuydwnR5 = 0;
        private string m_lArmIndexNameVstr = string.Empty; // 3/26/2010 dd - OPM 44173
        private decimal m_lRAdj1stCapR = 0; // 3/26/2010 dd - OPM 44173
        private decimal m_lRAdjCapR = 0; // 3/26/2010 dd - OPM 44173
        private decimal m_lRAdjLifeCapR = 0; // 3/26/2010 dd - OPM 44173
        private int m_lPrepmtPeriod = 0; // 2/28/2012 mf - OPM 105510

        public int Term { get { return m_lTerm; } }
        public int Due { get { return m_lDue; } }
        public int RAdj1stCapMon { get { return m_lRAdj1stCapMon; } }
        public int RAdjCapMon { get { return m_lRAdjCapMon; } }
        public E_sFinMethT FinMethT { get { return m_lFinMethT; } }
        public string ProductType { get { return m_lProductType; } }
        public int BuydwnR1 { get { return m_lBuydwnR1; } }
        public int BuydwnR2 { get { return m_lBuydwnR2; } }
        public int BuydwnR3 { get { return m_lBuydwnR3; } }
        public int BuydwnR4 { get { return m_lBuydwnR4; } }
        public int BuydwnR5 { get { return m_lBuydwnR5; } }

        public string ArmIndexNameVstr { get { return m_lArmIndexNameVstr; } }
        public decimal RAdj1stCapR { get { return m_lRAdj1stCapR; } }
        public decimal RAdjCapR { get { return m_lRAdjCapR; } }
        public decimal RAdjLifeCapR { get { return m_lRAdjLifeCapR; } }
        public int PrepmtPeriod { get { return m_lPrepmtPeriod; } }

        public int lHelocDraw { get; private set; }
        public int lHelocRepay { get; private set; }


        private int ParseInt(string s, int defaultValue)
        {
            if (string.IsNullOrEmpty(s))
                return defaultValue;

            int result;
            if (!int.TryParse(s.Replace("%", ""), out result))
            {
                result = defaultValue;
            }
            return result;
        }
        private int ParseBuydown(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return 0;
            }
            decimal result;
            if (!decimal.TryParse(s.Replace("%", ""), out result))
            {
                result = 0;
            }

            // 3/9/2010 dd - If buydown rate is not a whole number than throw exception.
            if (Math.Round(result, 0) != result)
            {
                throw new CBaseException(ErrorMessages.Generic, "Buydown is not a whole number. Buydown=" + result);
            }
            return (int)result;

        }
        private decimal ParseRate(string s, decimal defaultValue)
        {
            if (string.IsNullOrEmpty(s))
            {
                return defaultValue;
            }
            decimal result;
            if (!decimal.TryParse(s.Replace("%", ""), out result))
            {
                result = defaultValue;
            }
            return result;
        }
        public RateMergeGroupKey(CApplicantPriceXml applicantPrice)
        {
            lHelocDraw = ParseInt(applicantPrice.lHelocDraw_rep, 0);
            lHelocRepay = ParseInt(applicantPrice.lHelocRepay_rep, 0);
            m_lTerm = ParseInt(applicantPrice.lTerm_rep, 0);
            m_lDue = ParseInt(applicantPrice.lDue_rep, 0);

            m_lRAdj1stCapMon = ParseInt(applicantPrice.lRadj1stCapMon_rep, 0);
            m_lRAdj1stCapR = ParseRate(applicantPrice.lRadj1stCapR_rep, 0);
            m_lRAdjCapR = ParseRate(applicantPrice.lRAdjCapR_rep, 0);
            m_lRAdjLifeCapR = ParseRate(applicantPrice.lRAdjLifeCapR_rep, 0);
            m_lRAdjCapMon = ParseInt(applicantPrice.lRAdjCapMon_rep, 0);

            m_lArmIndexNameVstr = applicantPrice.lArmIndexNameVstr;

            m_lBuydwnR1 = ParseBuydown(applicantPrice.lBuydwnR1_rep);
            m_lBuydwnR2 = ParseBuydown(applicantPrice.lBuydwnR2_rep);
            m_lBuydwnR3 = ParseBuydown(applicantPrice.lBuydwnR3_rep);
            m_lBuydwnR4 = ParseBuydown(applicantPrice.lBuydwnR4_rep);
            m_lBuydwnR5 = ParseBuydown(applicantPrice.lBuydwnR5_rep);

            m_lFinMethT = applicantPrice.lFinMethT;
            m_lProductType = applicantPrice.lLpProductType;
            m_lPrepmtPeriod = ParseInt(applicantPrice.lPrepmtPeriod_rep, 0);

        }

        public RateMergeGroupKey(RateMergeGroupKeyParameters parameters)
        {
            lHelocDraw = ParseInt(parameters.lHelocDraw_rep, 0);
            lHelocRepay = ParseInt(parameters.lHelocRepay_rep, 0);

            m_lTerm = ParseInt(parameters.lTerm_rep, 0);
            m_lDue = ParseInt(parameters.lDue_rep, 0);

            m_lRAdj1stCapMon = ParseInt(parameters.lRAdj1stCapMon_rep, 0);
            m_lRAdj1stCapR = ParseRate(parameters.lRAdj1stCapR_rep, 0);
            m_lRAdjCapR = ParseRate(parameters.lRAdjCapR_rep, 0);
            m_lRAdjLifeCapR = ParseRate(parameters.lRAdjLifeCapR_rep, 0);
            m_lRAdjCapMon = ParseInt(parameters.lRAdjCapMon_rep, 0);

            m_lArmIndexNameVstr = parameters.lArmIndexNameVstr;

            m_lBuydwnR1 = ParseBuydown(parameters.lBuydwnR1_rep);
            m_lBuydwnR2 = ParseBuydown(parameters.lBuydwnR2_rep);
            m_lBuydwnR3 = ParseBuydown(parameters.lBuydwnR3_rep);
            m_lBuydwnR4 = ParseBuydown(parameters.lBuydwnR4_rep);
            m_lBuydwnR5 = ParseBuydown(parameters.lBuydwnR5_rep);

            m_lFinMethT = parameters.lFinMethT;
            m_lProductType = parameters.lLpProductType;
            m_lPrepmtPeriod = ParseInt(parameters.lPrepmtPeriod_rep, 0);
        }

        public int CompareTo(object x)
        {
            RateMergeGroupKey _key = x as RateMergeGroupKey;

            if (null == _key)
                return 1;

            int ret = m_lFinMethT.CompareTo(_key.m_lFinMethT);
            if (ret == 0)
            {
                ret = m_lTerm.CompareTo(_key.m_lTerm);
                if (ret == 0)
                {
                    ret = m_lDue.CompareTo(_key.m_lDue);
                    if (ret == 0)
                    {
                        ret = m_lRAdj1stCapMon.CompareTo(_key.m_lRAdj1stCapMon);
                        if (ret == 0)
                        {
                            ret = m_lRAdjCapMon.CompareTo(_key.m_lRAdjCapMon);
                            if (ret == 0)
                            {

                                ret = m_lProductType.CompareTo(_key.m_lProductType);
                                if (ret == 0)
                                {
                                    if (m_lProductType.Equals("HELOC", StringComparison.OrdinalIgnoreCase))
                                    {
                                        // 10/25/2013 dd - Only compare HELOC Draw and Repay period on HELOC program
                                        ret = lHelocDraw.CompareTo(_key.lHelocDraw);
                                        if (ret == 0)
                                        {
                                            ret = lHelocRepay.CompareTo(_key.lHelocRepay);
                                        }
                                    }
                                    if (ret == 0 && (m_lFinMethT == E_sFinMethT.ARM || m_lFinMethT == E_sFinMethT.Graduated))
                                    {
                                        // 3/26/2010 dd - Only perform comparison on ARM.
                                        ret = m_lArmIndexNameVstr.CompareTo(_key.m_lArmIndexNameVstr);
                                    }

                                    if (ret == 0)
                                    {
                                        ret = m_lPrepmtPeriod.CompareTo(_key.m_lPrepmtPeriod);
                                        if (ret == 0)
                                        {
                                            ret = _key.m_lRAdj1stCapR.CompareTo(m_lRAdj1stCapR);

                                            if (ret == 0)
                                            {
                                                ret = _key.m_lRAdjCapR.CompareTo(m_lRAdjCapR);
                                                if (ret == 0)
                                                {
                                                    ret = _key.m_lRAdjLifeCapR.CompareTo(m_lRAdjLifeCapR);
                                                    if (ret == 0)
                                                    {
                                                        // 3/9/2010 dd - OPM 47755 - Buydown rate will sort in descending order.
                                                        ret = CompareBuydown(m_lBuydwnR1, _key.m_lBuydwnR1);
                                                        if (ret == 0)
                                                        {
                                                            ret = CompareBuydown(m_lBuydwnR2, _key.m_lBuydwnR2);
                                                            if (ret == 0)
                                                            {
                                                                ret = CompareBuydown(m_lBuydwnR3, _key.m_lBuydwnR3);
                                                                if (ret == 0)
                                                                {
                                                                    ret = CompareBuydown(m_lBuydwnR4, _key.m_lBuydwnR4);
                                                                    if (ret == 0)
                                                                    {
                                                                        ret = CompareBuydown(m_lBuydwnR5, _key.m_lBuydwnR5);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    } // ret = _key.m_lRAdjLifeCapR.CompareTo(m_lRAdjLifeCapR);
                                                } // ret = _key.m_lRAdjCapR.CompareTo(m_lRAdjCapR);
                                            } // ret = _key.m_lRAdj1stCapR.CompareTo(m_lRAdj1stCapR);
                                        } // ret = m_lArmIndexNameVstr.CompareTo(_key.m_lArmIndexNameVstr);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// Return 1 if a less than b and a <> 0 b <> 0
        /// Return 0 if a = b
        /// Return -1 if a greater than b.
        /// Return -1 if a is zero and b<> 0.
        /// Return 1 if b <> 0 and b is zero.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        private int CompareBuydown(int a, int b)
        {
            int ret = b.CompareTo(a);
            if (ret != 0)
            {
                if (a == 0 || b == 0)
                {
                    ret = ret * -1;
                }
            }
            return ret;
        }

        //http://stackoverflow.com/questions/263400/what-is-the-best-algorithm-for-an-overridden-system-object-gethashcode
        public override int GetHashCode()
        {

            unchecked
            {
                int hash = 17;
                hash = hash * 23 + m_lTerm.GetHashCode();
                hash = hash * 23 + m_lDue.GetHashCode();
                hash = hash * 23 + m_lRAdj1stCapMon.GetHashCode();
                hash = hash * 23 + m_lRAdjCapMon.GetHashCode();
                hash = hash * 23 + m_lFinMethT.GetHashCode();
                hash = hash * 23 + m_lProductType.GetHashCode();
                hash = hash * 23 + m_lBuydwnR1.GetHashCode();
                hash = hash * 23 + m_lBuydwnR2.GetHashCode();
                hash = hash * 23 + m_lBuydwnR3.GetHashCode();
                hash = hash * 23 + m_lBuydwnR4.GetHashCode();
                hash = hash * 23 + m_lBuydwnR5.GetHashCode();
                hash = hash * 23 + m_lArmIndexNameVstr.GetHashCode();
                hash = hash * 23 + m_lRAdj1stCapR.GetHashCode();
                hash = hash * 23 + m_lRAdjCapR.GetHashCode();
                hash = hash * 23 + m_lRAdjLifeCapR.GetHashCode();
                hash = hash * 23 + m_lPrepmtPeriod.GetHashCode();
                return hash;
            }

        }

        public override bool Equals(object obj)
        {
            var item = obj as RateMergeGroupKey;

            if (obj == null)
            {
                return false;
            }
            //or should I call compareto(obj) == 0
            return m_lTerm == item.m_lTerm &&
            m_lDue == item.m_lDue &&
            m_lRAdj1stCapMon == item.m_lRAdj1stCapMon &&
            m_lRAdjCapMon == item.m_lRAdjCapMon &&
            m_lFinMethT == item.m_lFinMethT &&
            m_lProductType == item.m_lProductType &&
            m_lBuydwnR1 == item.m_lBuydwnR1 &&
            m_lBuydwnR2 == item.m_lBuydwnR2 &&
            m_lBuydwnR3 == item.m_lBuydwnR3 &&
            m_lBuydwnR4 == item.m_lBuydwnR4 &&
            m_lBuydwnR5 == item.m_lBuydwnR5 &&
            m_lArmIndexNameVstr == item.m_lArmIndexNameVstr &&
            m_lRAdj1stCapR == item.m_lRAdj1stCapR &&
            m_lRAdjCapR == item.m_lRAdjCapR &&
            m_lRAdjLifeCapR == item.m_lRAdjLifeCapR &&
            m_lPrepmtPeriod == item.m_lPrepmtPeriod;
        }
    }
}
