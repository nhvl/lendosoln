﻿namespace LendersOffice.RatePrice
{
    /// <summary>
    /// Interface for pricing engine storage factory.
    /// </summary>
    public interface IPriceEngineStorageFactory
    {
        /// <summary>
        /// Create pricing engine storage implementation.
        /// </summary>
        /// <param name="mode">A type of price engine storage to create.</param>
        /// <returns>The implementation of pricing engine storage.</returns>
        IPriceEngineStorage Create(PriceEngineStorageMode mode);
    }
}