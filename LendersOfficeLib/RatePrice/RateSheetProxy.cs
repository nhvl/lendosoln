//Author : Thien Nguyen

using System;
using System.Collections;
using System.Data;
using System.Text;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice
{
    public enum LpeRateSheetProxyType
    {
        Legacy,
        LegacyProxy,
        DataSetCached,
        ArrayItemsCached
    }

    abstract public class AbstractRateSheetProxy
    {
        public delegate   DataSet LoadXmlDelegate( string xml );

        protected byte[]  m_rateSheetXmlContent;
        protected int     m_xmlSize;
        protected byte[]  m_versionTimestamp;

        public    string  LpeAcceptableRsFileId;
        public    long    LpeAcceptableRsFileVersionNumber;

        static byte[] StringToBytes( string str)
        {
            if( str == null )
                str = "";
            else
                str = str.TrimWhitespaceAndBOM();

            return str.Length == 0 ? s_StringEmpty : Encoding.ASCII.GetBytes(str);


        }

        protected AbstractRateSheetProxy( string rateSheetXmlContent, byte[] versionTimestamp, 
                                          string akaLpeAcceptableRsFileId, long akaLpeAcceptableRsFileVersionNumber )
            :this( StringToBytes( rateSheetXmlContent ), versionTimestamp, akaLpeAcceptableRsFileId, 
                  akaLpeAcceptableRsFileVersionNumber )
        {
        }

        static byte[] s_StringEmpty = Encoding.ASCII.GetBytes("");
        protected AbstractRateSheetProxy( byte[] rateSheetXmlContent, byte[] versionTimestamp,
                                          string akaLpeAcceptableRsFileId, long akaLpeAcceptableRsFileVersionNumber)
        {
            if( rateSheetXmlContent == null )
                rateSheetXmlContent = s_StringEmpty;


            m_rateSheetXmlContent = rateSheetXmlContent;
            m_xmlSize             = m_rateSheetXmlContent.Length;
            m_versionTimestamp    = versionTimestamp;

            LpeAcceptableRsFileId            = akaLpeAcceptableRsFileId;
            LpeAcceptableRsFileVersionNumber = akaLpeAcceptableRsFileVersionNumber;

        }


        abstract public DataSet GetDataSet( LoadXmlDelegate loadXml );
        abstract public AbstractRateSheetProxy CloneIfNoChoice(Hashtable stringTable);

        internal bool SameTimeStamp( byte[] timeStamp )
        {
            return m_versionTimestamp != null && TimeStampUtility.Equals( m_versionTimestamp, timeStamp );
        }

        virtual public int EstimateSize()
        {
            int size = 20 + m_xmlSize; 
            if( m_rateSheetXmlContent != null )
                size += 16;
            return size;
        }


        static protected DataSet Load( LoadXmlDelegate loadXml, string rateSheetStr, Hashtable  stringTable)
        {
            DataSet ds            = loadXml(rateSheetStr);

            if( ds == null )
                return null;

            if( stringTable != null && ds != null && ds.Tables.Count > 0)
            {
                DataTable table = ds.Tables[0];
                int numCols = table.Columns.Count;
                lock( stringTable )
                {
                    foreach(DataRow row in table.Rows )
                    {
                        for( int idx = 0; idx < numCols; idx++)
                        {
                            string str = row[idx] as string;
                            if( str == null )
                                continue;

                            Object found = stringTable[ str ];
                                
                            if( found == null )
                                stringTable[ str ] = str;
                            else if(  Object.ReferenceEquals(found, str ) == false )
                                row[idx] = found;
                        }
                        row.AcceptChanges();
                    }
                }
            }
            return ds;
        }

        public static AbstractRateSheetProxy Create(string rateSheetXmlContent, byte[] versionTimestamp, 
                                                    string akaLpeAcceptableRsFileId, long akaLpeAcceptableRsFileVersionNumber,  
                                                    Hashtable stringTable)
        {
            return new ArrayItemsCachedRateSheetProxy(rateSheetXmlContent, versionTimestamp, 
                                                      akaLpeAcceptableRsFileId, akaLpeAcceptableRsFileVersionNumber,
                                                      stringTable);

        }

    }

    public class ArrayItemsCachedRateSheetProxy : AbstractRateSheetProxy
    {
        static Object  s_lock = new Object();
        static DataSet s_DataSetWithRateSheetSchema = null;

        private Object  [] m_rows; 
        private bool       m_done;
        private Hashtable  m_stringTable;

        public ArrayItemsCachedRateSheetProxy(string rateSheetXmlContent, byte[] versionTimestamp, 
                                              string akaLpeAcceptableRsFileId, long akaLpeAcceptableRsFileVersionNumber,
                                              Hashtable stringTable) 
               : base(rateSheetXmlContent, versionTimestamp, akaLpeAcceptableRsFileId, akaLpeAcceptableRsFileVersionNumber)
        {
            m_stringTable  = stringTable;
        }

        public ArrayItemsCachedRateSheetProxy(byte[] rateSheetXmlContent, byte[] versionTimestamp, 
                                              string akaLpeAcceptableRsFileId, long akaLpeAcceptableRsFileVersionNumber,  
                                              Hashtable stringTable) 
             : base(rateSheetXmlContent, versionTimestamp, akaLpeAcceptableRsFileId, akaLpeAcceptableRsFileVersionNumber)
        {
            m_stringTable  = stringTable;
        }

        override public DataSet GetDataSet( LoadXmlDelegate loadXml )
        {
            if( m_done == false )
            {
                lock( this )
                {
                    if( m_done == false )
                    {
                        string rateSheetStr   = Encoding.ASCII.GetString( m_rateSheetXmlContent );
                        DataSet ds = Load( loadXml, rateSheetStr, m_stringTable);

                        if( ds == null )
                            return null;

                        if( ds.Tables.Count == 0 )
                            return ds;

                        if( s_DataSetWithRateSheetSchema == null )
                        {
                            lock( s_lock )
                            {
                                if( s_DataSetWithRateSheetSchema == null )
                                {
                                    DataSet otherDs = ds.Copy();
                                    otherDs.Tables[0].Clear();
                                    otherDs.AcceptChanges();

                                    s_DataSetWithRateSheetSchema = otherDs;                                    
                                }
                            }

                        }

                        m_rows = new Object[ ds.Tables[0].Rows.Count];
                        int idx  = 0;
                        foreach(DataRow row in ds.Tables[0].Rows )
                        {
                            m_rows[idx] = row.ItemArray;
                            idx++;
                        }

                        m_rateSheetXmlContent = null; // save memory
                        m_stringTable         = null;
                        m_done                = true;                        
                    }
                }
            }

            if( m_rows == null )
                return null;

            DataSet resultDS;
            lock( s_DataSetWithRateSheetSchema )
            {
                resultDS = s_DataSetWithRateSheetSchema.Copy();
            }

            try
            {
                if( resultDS.Tables[0].Rows.Count > 0 )
                {
                    throw new CBaseException(ErrorMessages.Generic, "BinaryRateSheetProxy error : expect #rows = 0");
                }
                foreach( Object []arrItems in m_rows )
                    resultDS.Tables[0].LoadDataRow(arrItems, true);
            }
            catch( Exception exc )
            {
                Tools.LogError("BinaryRateSheetProxy.GetDataSet() error", exc);
                return null;

            }

            return resultDS;
        }

        override public AbstractRateSheetProxy CloneIfNoChoice(Hashtable stringTable)
        {
            if( m_stringTable == null )
            {
                CollectItemString( stringTable );
                return this;
            }

            lock( this )
            {
                if( m_stringTable != null )
                    return new ArrayItemsCachedRateSheetProxy(m_rateSheetXmlContent, m_versionTimestamp, 
                                                              LpeAcceptableRsFileId, LpeAcceptableRsFileVersionNumber,
                                                              stringTable);
            }

            CollectItemString( stringTable );
            return this;

        }


        override public int EstimateSize()
        {
            int size = 36 + m_xmlSize + 16; 
            return size;
        }

        static int s_errorCounter = 0;
        void CollectItemString( Hashtable stringTable )
        {
            if( m_done == false || m_rows == null || stringTable == null )
                return;

            lock( stringTable )
            {
                foreach( object[] arrItem in m_rows )
                    foreach(object obj in arrItem )
                    {
                        string str = obj as string; // obj can be DBNull
                        if( str == null )
                            continue;

                        Object found = stringTable[ str ];
                                
                        if( found == null )
                            stringTable[ str ] = str;
                        else if(  Object.ReferenceEquals(found, str ) == false && s_errorCounter++ <= 100)
                            Tools.LogBug("Bug : RateSheet's values are not atomized.");
                    }
            }
        }

    }


}
