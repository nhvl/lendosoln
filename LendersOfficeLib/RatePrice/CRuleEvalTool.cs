
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Xml;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice
{
    public class OperatorName
    {
        public const string  Less           = "Less"         ;
        public const string  LessEqual      = "LessEqual"    ;
        public const string  Equal          = "Equal"        ;
        public const string  NotEqual       = "NotEqual"     ;
        public const string  Greater        = "Greater"      ;
        public const string  GreaterEqual   = "GreaterEqual" ;
        public const string  And            = "And"          ;
        public const string  Or             = "Or"           ;
        public const string  Plus           = "Plus"         ;
        public const string  Minus          = "Minus"        ;
        public const string  Multiply       = "Multiply"     ;
        public const string  Divide         = "Divide"       ;

        static Hashtable s_operatorSymbol = new Hashtable();

        static OperatorName()
        {
            s_operatorSymbol[ Less        ] = "<";
            s_operatorSymbol[ LessEqual   ] = "<=";
            s_operatorSymbol[ Equal       ] = "=";
            s_operatorSymbol[ NotEqual    ] = "<>";
            s_operatorSymbol[ Greater     ] = ">";
            s_operatorSymbol[ GreaterEqual] = ">=";
            s_operatorSymbol[ And         ] = "And";
            s_operatorSymbol[ Or          ] = "Or";
            s_operatorSymbol[ Plus        ] = "+";
            s_operatorSymbol[ Minus       ] = "-";
            s_operatorSymbol[ Multiply    ] = "*";
            s_operatorSymbol[ Divide      ] = "/";
        }

        static public string GetOperatorSymbol( string operatorName )
        {
            return s_operatorSymbol[operatorName] as string;
        }

    }

    public class CRuleEvalTool
    {

        static CRuleEvalTool()
        {
            //for thread-safe, we force s_defaultSymbolTable to create the hashtable Entries
            CSymbolTable s_defaultSymbolTable = new CSymbolTable(null);
            Hashtable table = s_defaultSymbolTable.Entries;
            if( table == null )
                Tools.LogError("static CRuleEvalTool() error : can not create s_defaultSymbolTable.Entries");
        }

        static public CSymbolTable DefaultTable
        {
            get { return new CSymbolTable(null); } 
        }

        static private bool s_restrictSyntax = true;

        static public void SetRestrictSyntax(bool onOff)
        {
            s_restrictSyntax = onOff;
        }

        static public bool HasPurpose( int purposes, E_Purpose purpose )
        {
            return 0 != ( ((int) purpose) & purposes );
        }

        static public XmlNode CreateAnEvalNode( XmlDocument xmlDocEval, string xmlText )
        {
            XmlTextReader rdr = new XmlTextReader(xmlText,XmlNodeType.Element,null); 
            return xmlDocEval.ReadNode(rdr); 
        }

        static private string LookupReplacement( string key, CSymbolTable symTable )
        {	
            Hashtable entries = symTable.Entries;
			
            string[] parts = key.Split( ':' );

            //Handling functions separately
            if( parts.Length > 1 )
            {
                // Check for parameter
                string param = parts[1];

                int first = key.IndexOf(":");
                if( parts.Length > 2 &&  key.LastIndexOf(":") != first  ) // opm 18952
                    throw new CConditionException( CEvaluationException.ErrorTypeEnum.UnexpectColonChar,  "The argument of method " +parts[0] + " can not contain the character ':'   *** actual argument is '" +  key.Substring( first+1)  + "'" );

                Match match = System.Text.RegularExpressions.Regex.Match(param, "[<>=&a-zA-Z]");
                if( match.Success)
                {
                    key = CSymbolFunctionStrParams.BuildFunctionKeyword( parts[0] );
                    CSymbolEntry entry = entries[key] as CSymbolEntry;
                    if( null == entry )
                        throw new CEvaluationException( CEvaluationException.ErrorTypeEnum.InvalidToken, parts[0] + " is not a valid keyword before ':'");
					
                    CSymbolFunctionStrParams func  = entry as CSymbolFunctionStrParams;
                    if( func == null )
                        throw new CEvaluationException( CEvaluationException.ErrorTypeEnum.InvalidFunctionStrParams,"Keyword " + parts[0] + " : can not convert type '" + entry.GetType().Name + "' to CSymbolFunctionStrParams.");

                    return func.CalculateXmlFunctionReplacement( param );
                } // if( match.Success )
                else
                {
                    int number = 0;
                    try
                    {
                        number = int.Parse( param );
                    }
                    catch
                    {
                        throw new CEvaluationException("Expecting a non-negative integer after ':'.  " + param + " is not an integer." );
                    }

                    if( number < 0 )
                    {
                        throw new CEvaluationException("Expecting a non-negative integer after ':'.  " + param + " is a negative integer.");
                    }

                    // Check for function name
                    key = CSymbolFunction.BuildFunctionKeyword( parts[0] );
					
                    CSymbolEntry entry = (CSymbolEntry) entries[key];
                    if( null == entry )
                        throw new CEvaluationException(CEvaluationException.ErrorTypeEnum.InvalidToken, parts[0] + " is not a valid keyword before ':'");

                    return ((CSymbolFunction)entry).CalculateXmlFunctionReplacement( param );
                } // else
            } // if( parts.Length > 1 )


            CSymbolEntry found = (CSymbolEntry) entries[key];
            // See if it's a number
            if( null == found )
            {
                try
                {
                    decimal number = decimal.Parse( key );
                    return @"<Number>" + key + @"</Number>";
                }
                catch
                {
                    throw new CEvaluationException( CEvaluationException.ErrorTypeEnum.InvalidToken, key + " is neither a number nor a valid keyword");

                }
            }

            return found.XmlReplacement;

        }

        static public string ReplaceForEval( string userExpression, CSymbolTable symTalbe )
        {
            string evalText = userExpression.Insert( 0, @"( " );;
            evalText = evalText.Insert( evalText.Length, @" )" );

            StringBuilder sBuilder = new StringBuilder( evalText.Length * 2 );
			
            string[] words = evalText.Split( null ); // splitting by whitespace

            for( int iWord = 0; iWord < words.Length; ++iWord )
            {
                string word = words[iWord].TrimWhitespaceAndBOM();
                if( word != "" )
                {
                    sBuilder.Append( LookupReplacement( word, symTalbe ) );
                }
            }
            return sBuilder.ToString();
        }


        static internal decimal EvaluateNumberExpression( XmlNode node, int indent, CSymbolTable symbolTable, PostfixExpression postfixExpr )
        {
            string key = node.InnerText;
            try
            {
                decimal result = decimal.Parse( key );
                if( postfixExpr != null )
                {
                    string fieldValueName = GetFieldValueName( node );
                    if( fieldValueName.Length == 0 )
                        postfixExpr.AddNumber( result );
                    else
                        postfixExpr.AddFieldValueSymbol( fieldValueName );

                }

                return result;

            }
            catch
            {
                string id = "";
                if( node.Attributes.Count > 0 )
                    id = node.Attributes[0].InnerText;
                throw new CEvaluationException(key + " doesn't have a value for the given loan file.");
            }
			
        }

        static internal CEvaluationException ConvertToCEvaluationException(string prompt, System.Exception exc)
        {
            if( exc is CEvaluationException  )
                return (CEvaluationException)exc;

            CBaseException baseExc = exc as CBaseException;
            if( baseExc != null )
                return new CEvaluationException(baseExc.UserMessage,  prompt + "\r\n"  + baseExc.DeveloperMessage );
            return new CEvaluationException( prompt + " [ " + exc.Message + " ] ", exc);
        }

        static internal decimal EvaluateFieldName( XmlNode node, int indent, CSymbolTable symbolTable, PostfixExpression postfixExpr )
        {
            string key = node.InnerText;
            CSymbolFieldName entry = null;
            try
            {
                entry = symbolTable.Entries[key] as CSymbolFieldName;
                if( entry == null )
                {
                    string errMsg = key + " is not a valid keyword for CSymbolFieldName.";
                    if( symbolTable.Entries[key] == null )
                        errMsg += " Maybe it was removed.";
                    throw new CEvaluationException( CEvaluationException.ErrorTypeEnum.InvalidToken, errMsg);
                }

                if( postfixExpr != null )
                    postfixExpr.AddFieldName( entry );

                return entry.Get();
            }
            catch ( CEvaluationException  )
            {
                throw;
            }
            catch( System.Exception exc )
            {

                throw ConvertToCEvaluationException(key + " doesn't have a value for the given loan file.",  exc);
            }
			
        }

        // http://opm/default.asp?8357
        static internal decimal EvaluateFunctionStringParams( XmlNode node, int indent, CSymbolTable symbolTable, PostfixExpression postfixExpr )
        {
            string key = node.InnerText;
            CSymbolFunctionStrParams entry = null;
            try
            {
                entry = symbolTable.Entries[key] as CSymbolFunctionStrParams;
                if( entry == null )
                {
                    string errMsg = key + " is not a valid keyword for CSymbolFunctionStrParams.";
                    if( symbolTable.Entries[key] == null )
                        errMsg += " Maybe it was removed.";
                    throw new CEvaluationException( CEvaluationException.ErrorTypeEnum.InvalidToken, errMsg);
                }

                string para = node.Attributes["param"].Value;
                if( postfixExpr != null )
                    postfixExpr.Add(entry, para);

                return entry.Call(para);
            }
            catch ( CEvaluationException  )
            {
                throw;
            }
            catch ( System.Exception exc )
            {

                throw ConvertToCEvaluationException("Can not evaluate the function '" + key +"' .",  exc);
            }			
        }

        static internal decimal EvaluateFunctionIntParams( XmlNode node, int indent, CSymbolTable symbolTable, PostfixExpression postfixExpr )
        {
            string key            = node.InnerText;
            try
            {
	            CSymbolFunction entry = symbolTable.Entries[key] as CSymbolFunction;
                if( entry == null )
                {
                    string errMsg = key + " is not a valid keyword for CSymbolFunction.";
                    if( symbolTable.Entries[key] == null )
                        errMsg += " Maybe it was removed.";
                    throw new CEvaluationException( CEvaluationException.ErrorTypeEnum.InvalidToken, errMsg);
                }
                
                string para           = node.Attributes["param"].Value;
	
	            int intValue          = int.Parse(para);
	
	            if( postfixExpr != null )
	                postfixExpr.Add(entry, intValue);
	
	            return entry.Call( intValue );
            }
            catch ( CEvaluationException  )
            {
                throw;
            }
            catch ( System.Exception exc )
            {
                throw ConvertToCEvaluationException("Can not evaluate the function '" + key +"' .",  exc);
            }			
        }

        static string GetFieldValueName( XmlNode node )
        {
            XmlAttribute attribute = node.Attributes["FieldValue"];
            return ( attribute != null && node.Name.TrimWhitespaceAndBOM() == "Number" ) ? attribute.Value : string.Empty;
        }
        
        static public decimal ExpressionDispatch( XmlNode node, int indent, CSymbolTable symbolTable )
        {
            return ExpressionDispatch( node, indent, symbolTable, null );
        }

        static public decimal ExpressionDispatch( XmlNode node, int indent, CSymbolTable symbolTable, PostfixExpression postfixExpr )
        {
            if( indent > 20 )
                throw new CEvaluationException("20 is the max nested level for a condition.");
			
            ++indent;

            switch( node.Name.TrimWhitespaceAndBOM() )
            {
                case "Number": 
                    return EvaluateNumberExpression( node, indent, symbolTable, postfixExpr );

                case "FunctionStrParams": 
                    return EvaluateFunctionStringParams( node, indent, symbolTable, postfixExpr );

                case "FunctionIntParams": 
                    return EvaluateFunctionIntParams( node, indent, symbolTable, postfixExpr );

                case "FieldName": 
                    return EvaluateFieldName( node, indent, symbolTable, postfixExpr );

                case "Op":
                    throw new CEvaluationException("Programming error:  'Op' node is not a valid node to be passed to EvaluateExpressionDispatch method.  " + node.OuterXml);
                default:
                    break;
            }

            XmlNodeList nodeList = node.ChildNodes;
	
            if( 1 == nodeList.Count )
            {
                switch( GetFieldValueName( node.FirstChild ) )
                {
                    case "Bool_Yes" :
                    case "Bool_No"  :
                    case ""         :
                        break;

                    default : 
                        throw new CConditionException( CEvaluationException.ErrorTypeEnum.MissingVariableName, 
                            "Missing some variable in the expression " + ErrorExpr2Str(node) );
                }

                return ExpressionDispatch( node.FirstChild, indent, symbolTable, postfixExpr );
            }

            if( 0 == nodeList.Count )
                return 0; // This is the default value for empty expression (false for boolean group, 0 for decimal group)

            for( int i=0; i< nodeList.Count; ++i )
            {
                XmlNode child = (XmlNode) nodeList[i];
                if( child.Name.TrimWhitespaceAndBOM() == "Op" )
                {
                    // ThienChange : remove the method OperatorDispatch( ... )
                    //return OperatorDispatch( node, indent, symbolTable );
                    return symbolTable.LookupEvaluator( child.InnerText )(node, indent, symbolTable , postfixExpr );

                }
            }// end of for

            throw new CEvaluationException("Encountering a group with no operator:  " + node.OuterXml);
        }

        static public void DontAllowFieldValueInExpression( XmlNode node )
        {
            if( s_restrictSyntax == false )
                return;

            XmlNodeList nodeList = node.ChildNodes;
            for( int i=0; i < nodeList.Count; i++)
            {
                XmlNode operand =( XmlNode) nodeList[i]; 
                string fieldValueName = GetFieldValueName( operand );

                if( fieldValueName != string.Empty )
                {
                    throw new CConditionException( CEvaluationException.ErrorTypeEnum.DontAllowFieldValue, 
                        string.Format("Don't allow field value {0} in the expression {1}",
                        fieldValueName, ErrorExpr2Str(node) ) );
                }
            }
        }


        static void RequireIfIsFieldValue(XmlNode node, CSymbolTable symbolTable )
        {
            if( s_restrictSyntax == false )
                return;

            string fieldValueName = string.Empty;
            XmlNode other = null;

            XmlNodeList nodeList = node.ChildNodes;
            if( nodeList.Count != 3 )
                return;

            for(int i = 0; i < 2; i++)
            {
                fieldValueName = GetFieldValueName( i == 0 ? ( XmlNode) nodeList[0] : ( XmlNode) nodeList[2]);
                if( fieldValueName != string.Empty )
                {
                    if( fieldValueName.IndexOf("Bool_") >= 0 )
                        return;

                    other = ( i == 0) ? ( XmlNode) nodeList[2] : ( XmlNode) nodeList[0];
                    break;
                }
            }
            if( other == null )
                return;

            if( other.Name != "FieldName" )
                throw new CConditionException( CEvaluationException.ErrorTypeEnum.DontAllowFieldValue, 
                    "Missing some variable in the expression " + ErrorExpr2Str(node) +
                    "\nThe error data : " +   ErrorExpr2Str(other) );

            CSymbolFieldValue symbolFieldValue = (CSymbolFieldValue) symbolTable.Entries[fieldValueName];
            if( symbolFieldValue.FieldName == string.Empty )
                return;

            if( other.InnerText != symbolFieldValue.FieldName )
                throw new CConditionException( CEvaluationException.ErrorTypeEnum.MismatchFieldAndFieldValue, 
                    "The variable and the value are mismatch : " +  ErrorExpr2Str(node));

        }

        // Case 16350:   Show reason why having the exception "= operator with incorrect number of operands." for validating an expression. 
        static void AssertNumOfOperandsAndOperators(XmlNode subExprNode, int expectNumChildren, string errMsg )
        {
            XmlNodeList nodeList = subExprNode.ChildNodes;
            if( nodeList.Count == expectNumChildren )
                return;

            if( s_restrictSyntax )
            {
                try
                {
                    string detailErrMsg = string.Format("{0}\n *. The invalid subexpression is {1}.",
                                                            errMsg, ErrorExpr2Str(subExprNode) );
                    errMsg  = detailErrMsg;

                    for(int i=0; i < nodeList.Count && i < expectNumChildren; i++)
                    {
                        detailErrMsg = string.Format( "\n *. data {0} : {1} ", i+1,  ErrorExpr2Str(( XmlNode) nodeList[i]) );
                        errMsg  += detailErrMsg;
                    }

                    if( nodeList.Count > expectNumChildren )
                        errMsg = errMsg + "\n *. The 'extra-data' : " + ErrorExpr2Str(( XmlNode) nodeList[expectNumChildren]);

                }
                catch
                {
                }
            }
            throw new CEvaluationException( errMsg ); 
        }

        static internal decimal EvaluatePlusExpression( XmlNode node, int indent, CSymbolTable symbolTable, PostfixExpression postfixExpr )
        {
            XmlNodeList nodeList = node.ChildNodes;

            AssertNumOfOperandsAndOperators( node, 3, "+ operator with incorrect number of operands." );

            DontAllowFieldValueInExpression( node );

            XmlNode firstOperand =( XmlNode) nodeList[0]; 
            XmlNode secondOperand =( XmlNode) nodeList[2];

            decimal result = ExpressionDispatch( firstOperand, indent, symbolTable, postfixExpr) + ExpressionDispatch( secondOperand, indent, symbolTable, postfixExpr );
            if( postfixExpr != null )
                postfixExpr.AddOperator( PostfixExpression.Plus );

            return result;            
        }
	
        static internal decimal EvaluateMinusExpression( XmlNode node, int indent, CSymbolTable symbolTable, PostfixExpression postfixExpr  )
        {
            XmlNodeList nodeList = node.ChildNodes;
            AssertNumOfOperandsAndOperators( node, 3, "- operator with incorrect number of operands." );


            DontAllowFieldValueInExpression( node );

            XmlNode firstOperand = ( XmlNode)nodeList[0];
            XmlNode secondOperand = ( XmlNode)nodeList[2];

            decimal result = ExpressionDispatch( firstOperand, indent, symbolTable, postfixExpr ) - ExpressionDispatch( secondOperand, indent, symbolTable, postfixExpr );
            if( postfixExpr != null )
                postfixExpr.AddOperator( PostfixExpression.Minus );

            return result;            

        }

        static internal decimal EvaluateDivideExpression( XmlNode node, int indent, CSymbolTable symbolTable, PostfixExpression postfixExpr )
        {
            XmlNodeList nodeList = node.ChildNodes;
            AssertNumOfOperandsAndOperators( node, 3, "/ operator with incorrect number of operands." );


            DontAllowFieldValueInExpression( node );

            XmlNode firstOperand = ( XmlNode)nodeList[0];
            XmlNode secondOperand = ( XmlNode)nodeList[2];

            decimal result = ExpressionDispatch( firstOperand, indent, symbolTable, postfixExpr ) / ExpressionDispatch( secondOperand, indent, symbolTable, postfixExpr );
            if( postfixExpr != null )
                postfixExpr.AddOperator( PostfixExpression.Divide );

            return result;            

        }

        //static internal decimal EvaluateMultiplyExpression( XmlNode node, int indent, CSymbolTable symbolTable )
        //{
        //    return EvaluateMultiplyExpression( node, indent, symbolTable, null );
        //}
        static internal decimal EvaluateMultiplyExpression( XmlNode node, int indent, CSymbolTable symbolTable, PostfixExpression postfixExpr )
        {
            XmlNodeList nodeList = node.ChildNodes;
            AssertNumOfOperandsAndOperators( node, 3, "* operator with incorrect number of operands." );


            DontAllowFieldValueInExpression( node );

            XmlNode firstOperand = ( XmlNode)nodeList[0];
            XmlNode secondOperand = ( XmlNode)nodeList[2];

            decimal result = ExpressionDispatch( firstOperand, indent, symbolTable, postfixExpr ) * ExpressionDispatch( secondOperand, indent, symbolTable, postfixExpr );

            if( postfixExpr != null )
                postfixExpr.AddOperator( PostfixExpression.Multiply );

            return result;            
        }

        static internal decimal EvaluateLessExpression( XmlNode node, int indent, CSymbolTable symbolTable, PostfixExpression postfixExpr )
        {
            XmlNodeList nodeList = node.ChildNodes;
            AssertNumOfOperandsAndOperators( node, 3, "< operator with incorrect number of operands." );


            DontAllowFieldValueInExpression( node );

            XmlNode firstOperand =( XmlNode) nodeList[0]; 
            XmlNode secondOperand =( XmlNode) nodeList[2];
			
            bool result = ( ExpressionDispatch( firstOperand, indent, symbolTable, postfixExpr ) < ExpressionDispatch( secondOperand, indent, symbolTable, postfixExpr ) );
            if( postfixExpr != null )
                postfixExpr.AddOperator( PostfixExpression.Less );
                
            return result ? 1 : 0;
        }

        static internal decimal EvaluateLessEqualExpression( XmlNode node, int indent, CSymbolTable symbolTable, PostfixExpression postfixExpr )
        {
            XmlNodeList nodeList = node.ChildNodes;
            AssertNumOfOperandsAndOperators( node, 3, "<= operator with incorrect number of operands." );

            DontAllowFieldValueInExpression( node );

            XmlNode firstOperand = ( XmlNode)nodeList[0];
            XmlNode secondOperand = ( XmlNode)nodeList[2];

            bool result = ( ExpressionDispatch( firstOperand, indent, symbolTable, postfixExpr ) <= ExpressionDispatch( secondOperand, indent, symbolTable, postfixExpr ) );
            if( postfixExpr != null )
                postfixExpr.AddOperator( PostfixExpression.LessEqual );
                
            return result ? 1 : 0;
        }

        static internal decimal EvaluateGreaterExpression( XmlNode node, int indent, CSymbolTable symbolTable, PostfixExpression postfixExpr )
        {
            XmlNodeList nodeList = node.ChildNodes;
            AssertNumOfOperandsAndOperators( node, 3, "> operator with incorrect number of operands." );

            DontAllowFieldValueInExpression( node );

            XmlNode firstOperand = ( XmlNode)nodeList[0];
            XmlNode secondOperand = ( XmlNode)nodeList[2];

            bool result = ( ExpressionDispatch( firstOperand, indent, symbolTable, postfixExpr ) > ExpressionDispatch( secondOperand, indent, symbolTable, postfixExpr ) );
            if( postfixExpr != null )
                postfixExpr.AddOperator( PostfixExpression.Greater );
                
            return result ? 1 : 0;
        }
        static internal decimal EvaluateGreaterEqualExpression( XmlNode node, int indent, CSymbolTable symbolTable, PostfixExpression postfixExpr )
        {
            XmlNodeList nodeList = node.ChildNodes;
            AssertNumOfOperandsAndOperators( node, 3, ">= operator with incorrect number of operands." );

            DontAllowFieldValueInExpression( node );

            XmlNode firstOperand = ( XmlNode)nodeList[0];
            XmlNode secondOperand = ( XmlNode)nodeList[2];
            bool result = ( ExpressionDispatch( firstOperand, indent, symbolTable, postfixExpr ) >= ExpressionDispatch( secondOperand, indent, symbolTable, postfixExpr ) );
            if( postfixExpr != null )
                postfixExpr.AddOperator( PostfixExpression.GreaterEqual );
                
            return result ? 1 : 0;
        }

        const  bool s_throwExceptionIfMissingAndOrOp = true; //ThienChange : will remove soon.

        //static public string DebugDump( XmlNode exprNode )
        //{
        //    StringBuilder sb = new StringBuilder();
        //    ErrorExpr2Str(exprNode, sb);
        //    return sb.ToString();
        //}

        static private bool s_DisplayParenthesesOfOneEleGroup = true;

        //static public bool DisplayParenthesesOfOneEleGroup
        //{
        //    get { return s_DisplayParenthesesOfOneEleGroup; }
        //    set { s_DisplayParenthesesOfOneEleGroup = value; }
        //}
            
        static void ErrorExpr2Str(XmlNode node, StringBuilder sb)
        {
            if( node == null )
                return;
            string postfix = "";
            string name = node.Name.TrimWhitespaceAndBOM();
            XmlAttribute attribute = node.Attributes.Count > 0 ? node.Attributes[0] : null;


            switch( name )
            {
                case "Op" : // <Op>And</Op>
                switch( node.InnerText.TrimWhitespaceAndBOM() )
                {
                    case "Less"         : sb.Append(" < "); return;
                    case "LessEqual"    : sb.Append(" <= "); return;
                    case "Equal"        : sb.Append(" = "); return;
                    case "NotEqual"     : sb.Append(" <> "); return;
                    case "Greater"      : sb.Append(" > "); return;
                    case "GreaterEqual" : sb.Append(" >= "); return;

                    case "And"          : sb.Append(" AND "); return;
                    case "Or"           : sb.Append(" OR "); return;
                            
                    case "Plus"         : sb.Append(" + "); return;
                    case "Minus"        : sb.Append(" - "); return;
                    case "Multiply"     : sb.Append(" * "); return;
                    case "Divide"       : sb.Append(" / "); return;

                }
                    sb.Append( node.InnerXml ).Append(" ******* Unknown Op*********");
                    return;

                case "Group" :
                    if( s_DisplayParenthesesOfOneEleGroup == false && node.ChildNodes.Count == 1 )
                        postfix = "";
                    else
                    {
                        sb.Append(" ( ");
                        postfix = " ) ";
                    }
                    break;

                case "Number"        :      // <Number FieldValue="State_AL"> 1 </Number>
                    // <Number>25</Number>
                    if( attribute == null || attribute.Value == "")
                        sb.Append( node.InnerText );
                    else                    
                        sb.AppendFormat(" {0} ", attribute.Value ); 
                    return;

                case "FieldName" : // <FieldName>Qltv</FieldName>
                    sb.Append( node.InnerXml );
                    return;

                case "FunctionIntParams" : // <FunctionIntParams param="36">Bankruptcy:#OfMonths</FunctionIntParams>
                case "FunctionStrParams" : // <FunctionStrParams param="reviewed&gt;=24">TradeCountX:StrParams</FunctionStrParams>
                {
                    string functName = node.InnerText;
                    int idx          = functName.IndexOf(":");
                    if( idx > 0 )
                        functName = functName.Substring(0, idx);

                    if( attribute == null || attribute.Value == "")
                        sb.AppendFormat(" {0} ", functName ); 
                    else                    
                        sb.AppendFormat(" {0}:{1} ", functName, attribute.Value); 
                    return;
                }

                default :
                    sb.Append( node.InnerXml );
                    return;
                    //throw Exception("Don't understand node : "  + name);
            }
            foreach( XmlNode child in node.ChildNodes )
                ErrorExpr2Str(child, sb);
            if( postfix != "" )
                sb.Append(postfix);
        }

        public static string ErrorExpr2Str(XmlNode node)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                ErrorExpr2Str(node, sb);
                if( sb.Length > 1 && sb[sb.Length - 1] == ' ' )
                    sb.Length = sb.Length - 1;
                sb.Replace("  ", " ");

                return sb.ToString();

            }
            catch
            {
                return node.InnerXml;
            }
        }


        static internal decimal EvaluateOrExpression( XmlNode node, int indent, CSymbolTable symbolTable, PostfixExpression postfixExpr )
        {
            XmlNodeList nodeList = node.ChildNodes;
            if( nodeList.Count < 3 ) 
                throw new CEvaluationException( "OR operator with incorrect number of operands." ); 
			
            bool bResult = false;
            bool isFirst = true;
            for( int i = 0; i < nodeList.Count; ++i )
            {
                XmlNode child = ( XmlNode)nodeList[i];

                bool expectOp = (i % 2) == 1 ? true : false; 
                bool isOp     = "Op" == child.Name.TrimWhitespaceAndBOM();

                if( expectOp != isOp && s_throwExceptionIfMissingAndOrOp)
                {
                    if( isOp )
                        throw new CConditionException( CEvaluationException.ErrorTypeEnum.UnexpectOp, 
                            "Found unexpected operator  : " + child.InnerText);
                    else
                        throw new CConditionException( CEvaluationException.ErrorTypeEnum.MissingOrOp, 
                            "Missing some OR operator.\nThe error data : " + ErrorExpr2Str(child) );
                }

                if( isOp ) 
                {
                    if( "Or" != child.InnerText.TrimWhitespaceAndBOM() )
                        throw new CEvaluationException( "Mixed operation with OR operator is found in one group.  " + node.InnerXml );
                }
                else
                {		
                    decimal childResult = ExpressionDispatch( child, indent, symbolTable, postfixExpr );
                    if( childResult != 0 && childResult != 1  )
                        throw new CEvaluationException( "OR operator is used on a non-boolean type group.  " + node.InnerXml );
                    bResult = bResult || ( 1 == childResult );

                    if( isFirst == false && postfixExpr != null )
                        postfixExpr.AddOperator( PostfixExpression.Or );
                    isFirst = false;

                }
            }
            return bResult ? 1 : 0;
        }
        static internal decimal EvaluateAndExpression( XmlNode node, int indent, CSymbolTable symbolTable, PostfixExpression postfixExpr )
        {
            XmlNodeList nodeList = node.ChildNodes;
            if( nodeList.Count < 3 ) 
                throw new CEvaluationException( "AND operator with incorrect number of operands." ); 
			
            bool bResult = true;
            bool isFirst = true;
            for( int i = 0; i < nodeList.Count; ++i )
            {
                XmlNode child = ( XmlNode)nodeList[i];

                bool expectOp = (i % 2) == 1 ? true : false; 
                bool isOp     = "Op" == child.Name.TrimWhitespaceAndBOM();

                if( expectOp != isOp && s_throwExceptionIfMissingAndOrOp)
                {
                    if( isOp )
                        throw new CConditionException( CEvaluationException.ErrorTypeEnum.UnexpectOp, 
                            "Found unexpected operator  : " + child.InnerText);
                    else
                        throw new CConditionException( CEvaluationException.ErrorTypeEnum.MissingAndOp , 
                            "Missing some AND operator.\nThe error data : " + ErrorExpr2Str(child) );
                }

                if( isOp ) // "Op" == child.Name.TrimWhitespaceAndBOM() 
                {
                    if( "And" != child.InnerText.TrimWhitespaceAndBOM() )
                        throw new CEvaluationException( "Mixed operation with AND operator is found in one group.  " + node.InnerXml );

                }
                else
                {		
                    decimal childResult = ExpressionDispatch( child, indent, symbolTable, postfixExpr );
                    if( childResult != 0 && childResult != 1  )
                        throw new CEvaluationException( "AND operator is used on a non-boolean type group.  " + node.InnerXml );
                    bResult = bResult && ( 1 == childResult );

                    if( isFirst == false && postfixExpr != null )
                        postfixExpr.AddOperator( PostfixExpression.And );
                    isFirst = false;

                }
            }
            return bResult ? 1 : 0;
        }
        static internal decimal EvaluateEqualExpression( XmlNode node, int indent, CSymbolTable symbolTable, PostfixExpression postfixExpr )
        {
            XmlNodeList nodeList = node.ChildNodes;

            AssertNumOfOperandsAndOperators( node, 3, "Equal operator with incorrect number of operands." );

            RequireIfIsFieldValue(node, symbolTable );

            XmlNode firstOperand =( XmlNode) nodeList[0]; 
            XmlNode secondOperand =( XmlNode) nodeList[2];
			
            bool result = ( ExpressionDispatch( firstOperand, indent, symbolTable, postfixExpr ) == ExpressionDispatch( secondOperand, indent, symbolTable, postfixExpr ) );
            if( postfixExpr != null )
                postfixExpr.AddOperator( PostfixExpression.Equal );
                
            return result ? 1 : 0;
        }
        static internal decimal EvaluateNotEqualExpression( XmlNode node, int indent, CSymbolTable symbolTable, PostfixExpression postfixExpr )
        {
            XmlNodeList nodeList = node.ChildNodes;

            AssertNumOfOperandsAndOperators( node, 3, "Equal operator with incorrect number of operands." );

            RequireIfIsFieldValue(node, symbolTable );

            XmlNode firstOperand =( XmlNode) nodeList[0]; 
            XmlNode secondOperand =( XmlNode) nodeList[2];
			
            bool result = ( ExpressionDispatch( firstOperand, indent, symbolTable, postfixExpr ) != ExpressionDispatch( secondOperand, indent, symbolTable, postfixExpr ) );
            if( postfixExpr != null )
                postfixExpr.AddOperator( PostfixExpression.NotEqual );
                
            return result ? 1 : 0;
        }

        static public object ParseToXmlNodeOrPostfixExpr(CSymbolTable symTable, XmlDocument xmlDoc, string exprString, ref XmlNode exprNode)
        {
            string  xmlEval = ReplaceForEval( exprString, symTable );
            exprNode        = CreateAnEvalNode( xmlDoc, xmlEval );

            try
            {
                PostfixExpression postfixExpr =  new PostfixExpression();
                CRuleEvalTool.ExpressionDispatch( exprNode, 0, symTable, postfixExpr );
                postfixExpr.TrimToSize();
                return postfixExpr;
            }
            catch( System.Exception exc)
            {
                Tools.LogError("Can not convert '" + exprString + "' to postfix form", exc);
                return exprNode;
            }
        }


    }
}
