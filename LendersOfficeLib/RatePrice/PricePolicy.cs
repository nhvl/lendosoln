using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Xml;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice
{

    public class CRuleList : IEnumerable<CRule>
	{
        private List<CRule> m_list = null;

		public CRuleList( int capacity )
		{
            this.m_list = new List<CRule>(capacity);
		}

		public CRule GetRule( int index )
		{
            return m_list[index];
		}

        public int Count
        {
            get { return this.m_list.Count; }
        }

        public void Add(CRule item)
        {
            this.m_list.Add(item);
        }

        public void RemoveAt(int index)
        {
            this.m_list.RemoveAt(index);
        }

        public IEnumerator<CRule> GetEnumerator()
        {
            return this.m_list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.m_list.GetEnumerator();

        }
    }

	/// <summary>
	/// Get an object of this class from loan product object.
	/// </summary>
	public class CPricePolicy
	{

		private XmlDocument m_xmlDoc;
        private Guid m_useThisId = Guid.Empty;

        public Guid PricePolicyId { get; private set; }

        public string PricePolicyDescription { get; set; }

        public string Notes { get; set; }

        public bool IsPublished { get; set; }

        public Guid ParentPolicyId { get; private set; }

        public Guid BrokerId { get; private set; }

        public string MutualExclusiveExecSortedId { get; set; }

        public CRuleList Rules { get; private set; }

        public DateTime CreatedDate { get; private set; }

        /// <summary>
        /// This will only return the value from the db since the last it loads, not the current edit session
        /// </summary>
        public bool HasQualifiedRule { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="parentPolicyId">Pass Guid.Empty if it doesn't have parent</param>
        /// <returns></returns>
        public static CPricePolicy CreateNewPricePolicy( Guid brokerId, Guid parentPolicyId )
		{
			return new CPricePolicy( brokerId, parentPolicyId, Guid.NewGuid() );
		}

        public static CPricePolicy CreateNewPricePolicyWithPredfineId(Guid predefinePolicyId, Guid brokerId, Guid parentPolicyId) 
        {
            CPricePolicy ret = new CPricePolicy(brokerId, parentPolicyId, predefinePolicyId);
            return ret;
        }

        /// <summary>
        /// Retrieve price policy by id from database. Will throw NotFoundException if price policy id does not exists.
        /// </summary>
        /// <param name="pricePolicyId">Id to retrieve.</param>
        /// <returns>The price policy object.</returns>
        public static CPricePolicy Retrieve(Guid pricePolicyId)
        {
            CPricePolicy pricePolicy = new CPricePolicy();

            if (!pricePolicy.RetrieveImpl(pricePolicyId))
            {
                throw new NotFoundException("Price Policy Not Found.", "PricePolicyId=[" + pricePolicyId + "] does not exists.");
            }

            return pricePolicy;
        }

        private CPricePolicy()
        {

        }
		private CPricePolicy( Guid brokerId, Guid parentPolicyId, Guid useThisId )
		{
            m_xmlDoc = Tools.CreateXmlDoc(@"<PricePolicy></PricePolicy>");

            this.m_useThisId = useThisId;
			this.PricePolicyId = Guid.Empty;
            this.PricePolicyDescription = string.Empty;
			this.Notes = string.Empty;
			this.IsPublished = false;
			this.Rules = new CRuleList(0);
            this.MutualExclusiveExecSortedId = string.Empty;
			this.ParentPolicyId = parentPolicyId;
			this.BrokerId = brokerId;
			this.HasQualifiedRule = false;
		}

		public CPricePolicy(Guid pricePolicyId)
		{
            RetrieveImpl(pricePolicyId);
        }

        private bool RetrieveImpl(Guid pricePolicyId)
        {
            bool hasRecord = false;
            this.PricePolicyId = pricePolicyId;

            string pricePolicyXml = string.Empty;
            SqlParameter[] parameters = { new SqlParameter("@PricePolicyId", this.PricePolicyId) };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "RetrievePricePolicy", parameters))
            {
                if (reader.Read())
                {
                    pricePolicyXml = (string)reader["PricePolicyXmlText"];
                    this.PricePolicyDescription = (string)reader["PricePolicyDescription"];
                    this.Notes = (string)reader["Notes"];
                    this.IsPublished = (bool)reader["IsPublished"];
                    this.MutualExclusiveExecSortedId = (string)reader["MutualExclusiveExecSortedId"];
                    this.HasQualifiedRule = (bool)reader["HasQualifiedRule"];
                    this.CreatedDate = (DateTime)reader["CreateDate"];
                    if (!(reader["ParentPolicyId"] is DBNull))
                    {
                        this.ParentPolicyId = (Guid)reader["ParentPolicyId"];
                    }
                    this.BrokerId = (Guid)reader["BrokerId"];

                    hasRecord = true;
                }
            }

            if (string.IsNullOrEmpty(pricePolicyXml))
            {
                pricePolicyXml = @"<PricePolicy></PricePolicy>";
            }

            m_xmlDoc = new XmlDocument();
            m_xmlDoc.LoadXml(pricePolicyXml);

            LoadRules();
            return hasRecord;

        }
		private void LoadRules()
		{
			int i = 0;
			int count = 0;
			try
			{
                XmlNodeList ruleNodes = m_xmlDoc.SelectNodes( ".//Rule" );
				
				count = ruleNodes.Count;
				this.Rules = new CRuleList( count );

				for( i = 0; i < count; ++i )
				{
                    this.Rules.Add( new CRule( (XmlElement) ruleNodes[i]) );
				}
			}
			catch( Exception ex )
			{
				Tools.LogError( string.Format( "ruleNodes.Count = {0}, i = {1}, m_ruleList.Count={2}", 
					count.ToString(), i.ToString(), this.Rules.Count.ToString() ), ex );
				throw;
			}
		}

		public void Save()
		{
			bool bHasQualifiedRule = false; // value to be cached
			if (this.Rules != null)
			{
                foreach (var rule in this.Rules)
                {
                    if (rule.Consequence.HasQConsequence)
                    {
                        bHasQualifiedRule = true;
                        break;
                    }
                }
			}

            string pricePolicyXmlText = PricePolicyTools.RemoveEmptyAdjustTargets( m_xmlDoc.OuterXml );

            var pricePolicyRules = Convert(this.m_xmlDoc);
            var rulesProtobufContent = SerializationHelper.ProtobufSerialize(pricePolicyRules);

            if( this.PricePolicyId == Guid.Empty )
            {
                SqlParameter outParm = new SqlParameter( "@PricePolicyId", SqlDbType.UniqueIdentifier  );
                outParm.Direction = ParameterDirection.Output;

                SqlParameter[] pars = { new SqlParameter( "@BrokerId", BrokerId ), 
                                          new SqlParameter( "@ParentPolicyId", this.ParentPolicyId == Guid.Empty ? (object) DBNull.Value : (object) this.ParentPolicyId  ),
                                          new SqlParameter("@PricePolicyXmlText", pricePolicyXmlText),
                                          new SqlParameter("@PricePolicyDescription", PricePolicyDescription ),
                                          new SqlParameter("@IsPublished", IsPublished ),
                                          new SqlParameter("@UseThisId", m_useThisId),
                                          new SqlParameter("@Notes", Notes ),
                                          new SqlParameter("@MutualExclusiveExecSortedId", MutualExclusiveExecSortedId ),
                                          new SqlParameter("@HasQualifiedRule", bHasQualifiedRule ),
                                          new SqlParameter("@ContentKey", string.Empty), // dd 7/23/2018 - No longer in use.
                                          new SqlParameter("@RulesProtobufContent", rulesProtobufContent),
                                          outParm };
			
                int n = StoredProcedureHelper.ExecuteNonQuery( DataSrc.LpeSrc, "CreatePricePolicy" , 3 , pars );
                if( n <= 0 ) 
                {
                    string errMsg = string.Format( "Create new price policy failed for pricepolicy with id {0}. Name={1}", 
                        m_useThisId.ToString(),PricePolicyDescription );
                    CBaseException exc = new CBaseException(ErrorMessages.Generic, errMsg);
                    throw exc;
                }
                this.PricePolicyId = (Guid) outParm.Value;
            }
            else
            {
                SqlParameter[] pars = {new SqlParameter("@PricePolicyId", PricePolicyId ),
                                          new SqlParameter("@PricePolicyXmlText", pricePolicyXmlText),
                                          new SqlParameter("@PricePolicyDescription", PricePolicyDescription ),
                                          new SqlParameter("@IsPublished", IsPublished ),
                                          new SqlParameter("@PricePolicyNotes", Notes ),
                                          new SqlParameter("@MutualExclusiveExecSortedId", MutualExclusiveExecSortedId ),
                                          new SqlParameter("@HasQualifiedRule", bHasQualifiedRule ),
                                          new SqlParameter("@ContentKey", string.Empty), // dd 7/23/2018 - No longer in use.
                                          new SqlParameter("@RulesProtobufContent", rulesProtobufContent)
                };
                int n = StoredProcedureHelper.ExecuteNonQuery( DataSrc.LpeSrc, "SavePricePolicy" , 3 , pars );
                if( n <= 0 ) 
                {
                    CBaseException e = new CBaseException(ErrorMessages.Generic, "Update price policy failed.  Price policy id = " + PricePolicyId.ToString());
                    throw e ;
                }
            }
		}

		public CRule AddNewRule()
		{
            return this.AddNewRule(Guid.NewGuid());
		}

		public CRule AddNewRule(Guid ruleID)
		{
			CRule newRule = new CRule( m_xmlDoc, ruleID );
            this.Rules.Add( newRule );
			m_xmlDoc.FirstChild.AppendChild( newRule.XmlNode );
			return newRule;
		}

		public void DeleteRule( Guid RuleId )
		{
			for( int i = 0; i < this.Rules.Count; ++i )
			{
				CRule rule = this.Rules.GetRule( i );
				if( rule.RuleId == RuleId )
				{
					m_xmlDoc.FirstChild.RemoveChild( rule.XmlNode );
					this.Rules.RemoveAt( i );
					break;
				}
			}
		}

		public CRule GetRule( Guid ruleId )
		{
            foreach (var rule in this.Rules)
            {
                if (rule.RuleId == ruleId)
                {
                    return rule;
                }
            }

			throw new CBaseException( ErrorMessages.Generic, "Cannot find the rule specified.  PricePolicyId = " + this.PricePolicyId.ToString() + ".  RuleId = " + ruleId.ToString() );
		}

		/// <summary>
		/// Note: The simplest but most inefficient way to delete all rules.
		/// </summary>
		public void DeleteAllRules()
		{
			while (this.Rules.Count > 0)
			{
				CRule rule = this.Rules.GetRule(0) ;
				m_xmlDoc.FirstChild.RemoveChild(rule.XmlNode) ;
				this.Rules.RemoveAt(0) ;
			}
		}

        /// <summary>
        /// Protobuf will optimize for null value of string protobuf compare with string empty. Therefore if value is string empty then return null.
        /// </summary>
        /// <param name="value">Value to optimize for protobuf.</param>
        /// <returns>Return null if string is empty.</returns>
        private static string ProtobufString(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            return value;
        }

        private static List<LendersOffice.RatePrice.Model.PricePolicyRuleV2> Convert(XmlDocument xmlDoc)
        {
            var ruleList = new List<LendersOffice.RatePrice.Model.PricePolicyRuleV2>();

            var ruleElementList = xmlDoc.DocumentElement.ChildNodes;

            foreach (XmlElement ruleElement in ruleElementList)
            {
                if (ruleElement.Name != "Rule")
                {
                    throw CBaseException.GenericException("Unexpected element name [" + ruleElement.Name + "]");
                }
                var rule = new LendersOffice.RatePrice.Model.PricePolicyRuleV2();

                foreach (XmlAttribute attr in ruleElement.Attributes)
                {
                    if (attr.Name == "RuleId")
                    {
                        rule.Id = new Guid(attr.Value);
                    }
                    else if (attr.Name == "Description")
                    {
                        rule.Description = ProtobufString(attr.Value);
                    }
                    else if (attr.Name == "LenderRule")
                    {
                        rule.IsLenderRule = attr.Value == "T";
                    }
                    else if (attr.Name == "QBC")
                    {
                        rule.QBC = ProtobufString(attr.Value);
                    }
                    else
                    {
                        throw CBaseException.GenericException("Unhandle attribute [" + attr.Name + "]");
                    }
                }

                foreach (XmlElement ruleChildElement in ruleElement.ChildNodes)
                {
                    if (ruleChildElement.Name == "Condition")
                    {
                        rule.Condition = ProtobufString(ruleChildElement.InnerText);
                    }
                    else if (ruleChildElement.Name == "Consequence")
                    {
                        foreach (XmlElement consequenceChild in ruleChildElement.ChildNodes)
                        {
                            if (consequenceChild.Name == "Skip")
                            {
                                rule.Skip = true;
                            }
                            else if (consequenceChild.Name == "Disqualify")
                            {
                                rule.Disqualify = true;
                            }
                            else if (consequenceChild.Name == "Adjust")
                            {
                                switch (consequenceChild.GetAttribute("Target"))
                                {
                                    case "Fee":
                                        rule.AdjustFee = ProtobufString(consequenceChild.InnerText);
                                        break;
                                    case "Margin":
                                        rule.AdjustMargin = ProtobufString(consequenceChild.InnerText);
                                        break;
                                    case "Rate":
                                        rule.AdjustRate = ProtobufString(consequenceChild.InnerText);
                                        break;
                                    case "BaseMargin":
                                        rule.AdjustBaseMagin = ProtobufString(consequenceChild.InnerText);
                                        break;
                                    case "Qltv":
                                        rule.AdjustQltv = ProtobufString(consequenceChild.InnerText);
                                        break;
                                    case "Qcltv":
                                        rule.AdjustQcltv = ProtobufString(consequenceChild.InnerText);
                                        break;
                                    case "Qscore":
                                        rule.AdjustQscore = ProtobufString(consequenceChild.InnerText);
                                        break;
                                    case "MaxFrontEndYsp":
                                        rule.AdjustMaxFrontEndYsp = ProtobufString(consequenceChild.InnerText);
                                        break;
                                    case "MaxYsp":
                                        rule.AdjustMaxYsp = ProtobufString(consequenceChild.InnerText);
                                        break;
                                    case "Qrate":
                                        rule.AdjustQrate = ProtobufString(consequenceChild.InnerText);
                                        break;
                                    case "MaxDti":
                                        rule.AdjustMaxDti = ProtobufString(consequenceChild.InnerText);
                                        break;
                                    case "QLAmt":
                                        rule.AdjustQLAmt = ProtobufString(consequenceChild.InnerText);
                                        break;
                                    case "QrateAdjust":
                                        rule.AdjustQrateAdjust = ProtobufString(consequenceChild.InnerText);
                                        break;
                                    case "TeaserRateAdjust":
                                        rule.AdjustTeaserRateAdjust = ProtobufString(consequenceChild.InnerText);
                                        break;
                                    case "LockDaysAdjust":
                                        rule.AdjustLockDaysAdjust = ProtobufString(consequenceChild.InnerText);
                                        break;
                                    default:
                                        throw CBaseException.GenericException("Unsupport Rule Adjust Target=[" + consequenceChild.GetAttribute("Target") + "]");
                                }
                            }
                            else if (consequenceChild.Name == "Stipulation")
                            {
                                if (consequenceChild.IsEmpty == false)
                                {
                                    rule.Stipulation = ProtobufString(consequenceChild.InnerText);
                                }
                            }
                            else
                            {
                                throw CBaseException.GenericException("Unexpected child of consequence [" + consequenceChild.Name + "]");
                            }
                        }
                    }
                    else if (ruleChildElement.Name == "ConditionEx")
                    {
                        // No-OP
                    }
                    else
                    {
                        throw CBaseException.GenericException("Unexpected child element [" + ruleChildElement.Name + "]");
                    }
                }

                ruleList.Add(rule);
            }

            return ruleList;
        }

    }
}