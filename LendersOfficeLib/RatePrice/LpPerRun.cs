namespace LendersOfficeApp.los.RatePrice
{
    using System;
    using System.Collections;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.ObjLib.PriceGroups;
    using LendersOffice.ObjLib.TitleProvider;
    using LendersOffice.QuickPricer;
    using LendersOffice.RatePrice;
    using LendersOffice.RatePrice.FileBasedPricing;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using System.Threading;

    public class CPriceGenerator
    {
        private CPageData  m_loanFileData;					// per run
        private SortedList m_sortedLoanProducts;            // per run. dd 2/22/2017 - Do we need to order by product name? Can we just return simple List<T>?

        public string PricingDebugLog { get; private set; }

        private CPriceGenerator()
        {

        }

        public CPriceGenerator(BrokerDB brokerDB, Guid loanFileId, IEnumerable<Guid> onlyTheseProdIds, CLpRunOptions options, E_sLienQualifyModeT lienQualifyModeT,
            QuickPricerLoanItem quickPricerLoanItem, PricingQuotes titleQuotes)
        {
            using (PerformanceStopwatch.Start("CPriceGenerator..ctor - CPriceEngineData.InitLoad"))
            {
                if (quickPricerLoanItem != null)
                {
                    if (quickPricerLoanItem.IsHELOCRequest)
                    {
                        if (brokerDB.IsEnableHELOC)
                        {
                            m_loanFileData = CPriceEngineData.CreatePricingEngineHeloc(loanFileId);
                        }
                    }
                }

                if (m_loanFileData == null)
                {
                    m_loanFileData = new CPriceEngineData(loanFileId);
                }
                m_loanFileData.AllowLoadWhileQP2Sandboxed = true;
                m_loanFileData.InitLoad();
                
                if (quickPricerLoanItem != null)
                {
                    m_loanFileData.PrepareQuickPricerLoan(quickPricerLoanItem);
                }
            }

            Init(onlyTheseProdIds, options, lienQualifyModeT, titleQuotes);
        }

        public CPriceGenerator(CPageData loanFile, IEnumerable<Guid> onlyTheseProdIds, CLpRunOptions options, E_sLienQualifyModeT lienQualifyModeT)
        {
            m_loanFileData = loanFile;
            Init(onlyTheseProdIds, options, lienQualifyModeT, null);
        }

        private AbstractLoanProgramSet GetLoanProgramSet(CLpRunOptions options, IEnumerable<Guid> onlyTheseProdIds)
        {
            SHA256Checksum pricingSnapshotFileKey = SHA256Checksum.Invalid;

            HybridLoanProgramSetOption hybridOptions = null;
            var broker = BrokerDB.RetrieveById(options.BrokerId);

            if (options.HistoricalPricingOption == null)
            {
                // Default to current snapshot.
                hybridOptions = HybridLoanProgramSetOption.GetCurrentSnapshotOption(broker);
            }
            else
            {
                hybridOptions = options.HistoricalPricingOption;
            }

            bool isHistoricalMode = hybridOptions.IsHistoricalMode();

            var snapshotKey = SHA256Checksum.Create(hybridOptions.SnapshotContentKey);

            if (snapshotKey != null)
            {
                pricingSnapshotFileKey = snapshotKey.Value;
            }

            if (isHistoricalMode == false)
            {
                AbstractLoanProgramSet snapShotLoanProgramSet = null;

                if (broker.ActualPricingBrokerId == Guid.Empty)
                {
                    snapShotLoanProgramSet = LpeDataProvider.GetLoanProgramSet(options.PriceGroupId, onlyTheseProdIds);
                }
                else
                {
                    // For broker using file based pricing then current snapshot is the latest import snapshot.
                    var currentLatestImportSnapshot = broker.RetrieveLatestManualImport();

                    SHA256Checksum priceGroupRevision = SHA256Checksum.Invalid;

                    PriceGroup priceGroup = PriceGroup.RetrieveByID(options.PriceGroupId, options.BrokerId);

                    snapShotLoanProgramSet = FileBasedLoanProgramSet.Create(broker, priceGroup.ContentKey, currentLatestImportSnapshot, onlyTheseProdIds);

                }

                return snapShotLoanProgramSet;
            }
            else
            {
                Stopwatch sw = Stopwatch.StartNew();

                Guid useBrokerId = broker.ActualPricingBrokerId == Guid.Empty ? options.BrokerId : broker.ActualPricingBrokerId;
                FileBasedSnapshot fileBasedSnapshot = FileBasedSnapshot.Retrieve(pricingSnapshotFileKey, useBrokerId, hybridOptions.UseSubsnapshot);

                if (fileBasedSnapshot == null)
                {
                    throw CBaseException.GenericException("Unable to locate snapshot of [" + pricingSnapshotFileKey + "]");
                }

                SHA256Checksum? priceGroupContentKey = SHA256Checksum.Create(options.PriceGroupContentKey); // Assume latest revision of price group.

                if (hybridOptions.PriceGroupOptions == E_LoanProgramSetPricingT.Historical)
                {
                    priceGroupContentKey = SHA256Checksum.Create(hybridOptions.PriceGroupContentKey);
                }
                
                if (priceGroupContentKey == null)
                {
                    throw CBaseException.GenericException("PriceGroupContentKey is missing or invalid.");
                }

                AbstractLoanProgramSet snapShotLoanProgramSet = null;

                if (broker.ActualPricingBrokerId == Guid.Empty)
                {
                    snapShotLoanProgramSet = LpeDataProvider.GetLoanProgramSet(options.PriceGroupId, onlyTheseProdIds);
                }
                else
                {
                    // For broker using file based pricing then current snapshot is the latest import snapshot.
                    var currentLatestImportSnapshot = broker.RetrieveLatestManualImport();
                    snapShotLoanProgramSet = FileBasedLoanProgramSet.Create(broker, priceGroupContentKey.Value, currentLatestImportSnapshot, onlyTheseProdIds);
                }

                FileBasedLoanProgramSet fileBasedLoanProgramSet = FileBasedLoanProgramSet.Create(broker, priceGroupContentKey.Value, fileBasedSnapshot, onlyTheseProdIds) as FileBasedLoanProgramSet;

                HybridLoanProgramSet hybridLoanProgramSet = new HybridLoanProgramSet(snapShotLoanProgramSet, fileBasedLoanProgramSet, hybridOptions);


                sw.Stop();
                string logMsg = $"FileBasedPricingUtilities.GetLoanProgramSet() in {sw.ElapsedMilliseconds:N0} ms";
                Tools.LogInfo(logMsg);

                if( ConstSite.EnableFilebasedCmp)
                {
                    string diagnosticMsg;
                    hybridLoanProgramSet.PartialCompareWithDbSnapshot(out diagnosticMsg);

                    this.PricingDebugLog = logMsg + Environment.NewLine + Environment.NewLine +
                                        "***** HybridLoanProgramSet.PartialCompareWithDbSnapshot: start" + Environment.NewLine +                                         
                                        diagnosticMsg +
                                        "***** HybridLoanProgramSet.PartialCompareWithDbSnapshot: end" + Environment.NewLine;
                }

                return hybridLoanProgramSet;
            }
        }

        private static void PrepareLoanForPricing(CPageData dataLoan, CLpRunOptions options)
        {
            if (dataLoan.DataState != E_DataState.InitLoad)
            {
                throw new CBaseException(ErrorMessages.Generic, "Pricing only accept dataloan object that is in InitLoad state");
            }

            dataLoan.DisableFieldEnforcement();
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            dataLoan.RunPmlRequestSourceT = options.RunPmlRequestSourceT; // 9/4/2013 dd - OPM 137164

            if (options.PricingState != null)
            {
                dataLoan.SetPricingState(options.PricingState);
            }
        }

        private static CPageData PrepareLoanFromLinkedSecond(CPageData sourceLoan, CLpRunOptions options, PricingQuotes titleQuotes)
        {
            // Ideally, we would:
            //   1. Load the 2nd lien from the DB
            //   2. Apply the PML scenario data in the same way we apply it for the 1st
            //   3. Run the engine
            // Unfortunately, we currently do step 2 by saving the scenario data to the 1st lien
            // from the UI, which we aren't doing for the 2nd (though arguably we should...).
            // Thus, we instead do the hacky thing and pull the data that we want to use from the
            // 2nd (and hope we can remember which data to pull) and then apply it to the transformed 1st.
            CPageData secondLoan = new CFullAccessPageData(sourceLoan.sLinkedLoanInfo.LinkedLId, new string[] { nameof(CPageBase.sClosingCostSetJsonContent), nameof(CPageBase.sFeeServiceApplicationHistoryXmlContent), nameof(CPageBase.sBrokerLockAdjustments) });
            secondLoan.InitLoad();

            string closingCostSetJson = secondLoan.GetsClosingCostSetJson();
            string feeServiceHistoryXml = secondLoan.sFeeServiceApplicationHistoryXmlContent;

            bool secondLoanOldByPassFieldsBrokerLockAdjustXmlContent = secondLoan.ByPassFieldsBrokerLockAdjustXmlContent;
            secondLoan.ByPassFieldsBrokerLockAdjustXmlContent = true;
            var sBrokerLockAdjustments = secondLoan.sBrokerLockAdjustments.Where(adj => adj.IsPersist).ToList();
            secondLoan.ByPassFieldsBrokerLockAdjustXmlContent = secondLoanOldByPassFieldsBrokerLockAdjustXmlContent;

            CPageData preparedLoan = PrepareFirstAsSecond(sourceLoan, options, titleQuotes);

            preparedLoan.CopysClosingCostSetJson(closingCostSetJson);
            preparedLoan.sFeeServiceApplicationHistoryXmlContent = feeServiceHistoryXml;

            bool preparedLoanLoanOldByPassFieldsBrokerLockAdjustXmlContent = preparedLoan.ByPassFieldsBrokerLockAdjustXmlContent;
            preparedLoan.ByPassFieldsBrokerLockAdjustXmlContent = true;
            preparedLoan.sBrokerLockAdjustments = sBrokerLockAdjustments;
            preparedLoan.ByPassFieldsBrokerLockAdjustXmlContent = preparedLoanLoanOldByPassFieldsBrokerLockAdjustXmlContent;

            return preparedLoan;
        }

        private static CPageData PrepareFirstAsSecond(CPageData sourceLoan, CLpRunOptions options, PricingQuotes titleQuotes)
        {
            PrepareLoanForPricing(sourceLoan, options);

            decimal sNoteIR = 0;
            if (decimal.TryParse(sourceLoan.sNoteIR_rep.Replace("%", ""), out sNoteIR) == false)
            {
                sNoteIR = 0;
            }

            options.OriginalNoteRate = sNoteIR;

            if (sourceLoan.sSubFinT == E_sSubFinT.Heloc)
            {
                // We need to extract the data that will be applied to the
                // HELOC from the loan BEFORE it is loaded as a HELOC.
                // Otherwise, the data retrieved will use the HELOC calculations
                // and be incorrect.
                var data = sourceLoan.GetDataForSecondLien();

                sourceLoan = CPriceEngineData.CreatePricingEngineHeloc(sourceLoan.sLId);
                sourceLoan.AllowLoadWhileQP2Sandboxed = true;
                sourceLoan.InitLoad();

                PrepareLoanForPricing(sourceLoan, options);

                sourceLoan.PrepareCreditReportDataForLpe();
                sourceLoan.MakeThisInto2ndLienForQualifying(options, data);
            }
            else
            {
                sourceLoan.PrepareCreditReportDataForLpe();
                sourceLoan.MakeThisInto2ndLienForQualifying(options);
            }

            return sourceLoan;
        }

        private static CPageData PrepareFirstLien(CPageData sourceLoan, CLpRunOptions options, PricingQuotes titleQuotes)
        {
            PrepareLoanForPricing(sourceLoan, options);
            sourceLoan.PrepareCreditReportDataForLpe();

            decimal sNoteIR = 0;
            if (decimal.TryParse(sourceLoan.sNoteIR_rep.Replace("%", ""), out sNoteIR) == false)
            {
                sNoteIR = 0;
            }

            options.OriginalNoteRate = sNoteIR;

            if (options.SecondLienMPmt > 0)
            {
                // 3/4/2011 dd - We need to set the second lien payment when running 80/20 scenario that trigger rate/fee change due
                // to DTI rule from original run.
                // SecondLienMPmt should only be greater than zero during the rerun and should not be set during the first run.
                sourceLoan.sProOFinPmtPe_rep = sourceLoan.m_convertLos.ToMoneyString(options.SecondLienMPmt, FormatDirection.ToRep);

                // 10/25/2017 dt - Need to set this too, so that the setting of the value survives the PML transform that gets called
                //                 in the middle of the pricing run.
                sourceLoan.sSubFinPmt_rep = sourceLoan.sProOFinPmtPe_rep;
            }

            return sourceLoan;
        }

        private void Init(IEnumerable<Guid> onlyTheseProdIds, CLpRunOptions options, E_sLienQualifyModeT lienQualifyModeT, PricingQuotes titleQuotes)
        {
            CPageData preparedLoan;

            using (PerformanceStopwatch.Start("CPriceGenerator.Init"))
            {
                if (lienQualifyModeT == E_sLienQualifyModeT.ThisLoan)
                {
                    preparedLoan = PrepareFirstLien(this.m_loanFileData, options, titleQuotes);
                }
                else if (lienQualifyModeT == E_sLienQualifyModeT.OtherLoan && !this.m_loanFileData.sLinkedLoanInfo.IsLoanLinked)
                {
                    preparedLoan = PrepareFirstAsSecond(this.m_loanFileData, options, titleQuotes);
                    bool preparedLoanLoanOldByPassFieldsBrokerLockAdjustXmlContent = preparedLoan.ByPassFieldsBrokerLockAdjustXmlContent;
                    preparedLoan.ByPassFieldsBrokerLockAdjustXmlContent = true;
                    preparedLoan.sBrokerLockAdjustments = new List<PricingAdjustment>();
                    preparedLoan.ByPassFieldsBrokerLockAdjustXmlContent = preparedLoanLoanOldByPassFieldsBrokerLockAdjustXmlContent;
                }
                else if (lienQualifyModeT == E_sLienQualifyModeT.OtherLoan && this.m_loanFileData.sLinkedLoanInfo.IsLoanLinked)
                {
                    preparedLoan = PrepareLoanFromLinkedSecond(this.m_loanFileData, options, titleQuotes);
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, "Whoops, logic fail in CPriceGenerator.Init, unreachable condition reached. Congratulations!");
                }
            }

            preparedLoan.SetPricingQuotes(titleQuotes);
            this.m_sortedLoanProducts = ComputeListAndSortByProductFullName(preparedLoan, onlyTheseProdIds, options);
        }

        public PricingQuotes GetPricingQuotes()
        {
            return this.m_loanFileData.GetPricingQuotes();
        }

        // TODO: We might have some security issue here with the product ids coming from UI
        // we might have to mask them
        // Note that this method is executed per run instance.
        private SortedList ComputeListAndSortByProductFullName(CPageData dataLoan, IEnumerable<Guid> onlyTheseProdIds, CLpRunOptions options)
        {
            CacheInvariantPolicyResults cacheResults = new CacheInvariantPolicyResults();

            AbstractLoanProgramSet loanProgramSet = GetLoanProgramSet(options, onlyTheseProdIds);
            int count = loanProgramSet.Count;
            SortedList result = new SortedList(count);

            using (PerformanceStopwatch.Start("CPriceGenerator.ComputeListAndSortByProductFullName"))
            {
                for (int i = 0; i < count; ++i)
                {
                    Guid prodId = Guid.Empty;
                    if (null != onlyTheseProdIds)
                    {
                        prodId = loanProgramSet.RetrieveByIndex(i).lLpTemplateId;
                        if (!onlyTheseProdIds.Contains(prodId))
                            continue;
                    }

                    CApplicantPrice product;
                    string key;

                    try
                    {
                        product = ComputeApplicantPrice(dataLoan, loanProgramSet, prodId, options, cacheResults);
                        key = product.lLpTemplateNm;

                        // Log the process sequence. Note: we lost the sequence information because "sort by fullname".
                        product.ExecutedSequence = i;
                    }
                    catch (CRateOptionNotFoundException exc)
                    {
                        product = null;
                        key = "A product with invalid rate option";
                        if (exc != null)
                            Tools.LogRegTest("CRateOptionNotFoundException : " + exc.DeveloperMessage); //ThienChange : opm 4969
                    }


                    try
                    {
                        result.Add(key, product);
                    }
                    catch
                    {
                        if (result.Contains(key))
                        {
                            for (int iExt = 2; iExt < 200; ++iExt)
                            {
                                string newKey = key + "_" + iExt.ToString();
                                if (!result.Contains(newKey))
                                {
                                    result.Add(newKey, product);
                                    break;
                                }
                            }
                        }
                    }
                }

                // To avoid email spamming when there is an SAE mistake live on production, 
                // they are accumulated in the engine and sent here once per-run.
                // This method is also using the deduping framework.
                foreach (var email in cacheResults.SaeErrors)
                {
                    EmailUtilities.SendSaeError(email.Key, email.Value);
                }

                // Case 471515 So we can track usage:
                if (dataLoan.BrokerDB.UseRateOptionVariantMI && cacheResults.pmiCompanyUseCount.Count != 0)
                {
                    var usageStr = $"Case 471515 / 473112 {Environment.NewLine}Loan Programs: {count}{Environment.NewLine}{Environment.NewLine}";

                    foreach (var company in cacheResults.pmiCompanyUseCount.Keys)
                    {
                        usageStr += company + ": " + cacheResults.pmiCompanyUseCount[company] + " times" + Environment.NewLine;
                    }

                    if (dataLoan.BrokerDB.UseMiVendorIntegrationsInPricing)
                    {
                        usageStr += cacheResults.PmiQuoteSummary + Environment.NewLine;
                        usageStr += $"Seconds in quotes: {cacheResults.PmiQuoteWaitTime.TotalSeconds.ToString()}";
                    }

                    Tools.LogInfo(usageStr.ToString());
                }

            }

            return result;
        }

        private static CApplicantPrice ComputeApplicantPrice(CPageData dataLoan, AbstractLoanProgramSet loanProgramSet, Guid lLpTemplateId, CLpRunOptions options, CacheInvariantPolicyResults cacheResults)
        {
            CApplicantPrice price = CApplicantPrice.CreateApplicantPrice(
                dataLoan,
                loanProgramSet.RetrieveByKey(lLpTemplateId),
                loanProgramSet,
                options,
                cacheResults);

            return price;
        }

        public IEnumerable<CApplicantPrice> GetApplicantPriceList()
        {
            return m_sortedLoanProducts.Values.Cast<CApplicantPrice>();
        }

        /// <summary>
        /// This method will execute pricing using the best approach. The method can determine if it should run pricing in parallel or keep it same thread.
        /// </summary>
        /// <param name="brokerDB">The broker object.</param>
        /// <param name="task">The pricing task.</param>
        /// <param name="onlyTheseProdIds">A list of product ids to execute.</param>
        /// <returns>A pricing result.</returns>
        public static CPriceGenerator ExecutePricing(BrokerDB brokerDB, LpeTaskMessage task, IEnumerable<Guid> onlyTheseProdIds)
        {
            int maxNumberOfProducsInParallelTask = ConstSite.MaxNumberOfProductsInParallelTask;

            bool isExecuteInParallel = IsExecuteInParallel(brokerDB, onlyTheseProdIds, maxNumberOfProducsInParallelTask);

            CPriceGenerator priceGenerator = null;

            if (isExecuteInParallel)
            {
                priceGenerator = ExecuteInParallel(brokerDB, task, onlyTheseProdIds);
            }
            else
            {
                priceGenerator = new CPriceGenerator(brokerDB, task.RequestData.LoanId, onlyTheseProdIds, task.Options, task.RequestData.sLienQualifyModeT, task.QuickPricerLoanItem, titleQuotes: null);
                using (PerformanceStopwatch.Start($"Normal Execute, #products {onlyTheseProdIds.Count()}."))
                {
                }
            }

            return priceGenerator;
        }

        private static bool IsExecuteInParallel(BrokerDB brokerDB, IEnumerable<Guid> onlyTheseProdIds, int maxNumberOfProducsInParallelTask)
        {
            if (onlyTheseProdIds.Count() <= maxNumberOfProducsInParallelTask)
            {
                return false; // Number of products to run is less than a threshold.
            }

            return true;
        }
        /// <summary>
        /// Break a list of onlyTheseProdids into multiple smaller list and execute in parallel.
        /// </summary>
        /// <param name="brokerDB">The broker.</param>
        /// <param name="task">The original task.</param>
        /// <param name="onlyTheseProdIds">A list of product ids to execute.</param>
        /// <returns>A merge pricing result.</returns>
        private static CPriceGenerator ExecuteInParallel(BrokerDB brokerDB, LpeTaskMessage task, IEnumerable<Guid> onlyTheseProdIds)
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

            // Load title quote so it can be share across many tasks. It is to prevent each task order title 
            PricingQuotes titleQuotes = null;
            Guid firstProductId = onlyTheseProdIds.First();
            long pricingQuoteTime = 0;
            Stopwatch sw = new Stopwatch();
            
            if (brokerDB.IsTitleRunRequiredForPricing(principal.IsNewPmlUIEnabled, firstProductId))
            {
                sw.Start();

                task.Options.PrepareLoanOnly = true;
                CPriceGenerator titleGenerator = new CPriceGenerator(brokerDB, task.RequestData.LoanId, new Guid[] { firstProductId }, task.Options, task.RequestData.sLienQualifyModeT, task.QuickPricerLoanItem, null);
                task.Options.PrepareLoanOnly = false;

                titleQuotes = titleGenerator.GetPricingQuotes();
                sw.Stop();
                pricingQuoteTime = sw.ElapsedMilliseconds;
            }

            var executingThreadId = Thread.CurrentThread.ManagedThreadId;

            sw.Restart();
            var workList = PartitionList(onlyTheseProdIds, ConstSite.MaxNumberOfProductsInParallelTask);

            var tempDebugList = new ConcurrentQueue<Tuple<string /*log*/, int /*executeTime*/>>();

            ConcurrentQueue<CPriceGenerator> tempResultList = new ConcurrentQueue<CPriceGenerator>();

            Parallel.ForEach(workList, item =>
            {
                // https://referencesource.microsoft.com/#System/services/monitoring/system/diagnosticts/Stopwatch.cs,c8f79874d4d2130c,references
                // Stopwatch object is thread-safe if don't use Start(),Restart() and Stop() in multiple-threads

                PerformanceStopwatch.ResetAll();

                int startTime = (int)sw.ElapsedMilliseconds;
                var p = new CPriceGenerator(brokerDB, task.RequestData.LoanId, item,
                                            task.Options,
                                            task.RequestData.sLienQualifyModeT, task.QuickPricerLoanItem, titleQuotes);
                int endTime = (int)sw.ElapsedMilliseconds;

                int executionTime = (endTime-startTime);

                // "In certain circumstances, the Task Parallel Library will inline a task, which means it runs on the task on the currently executing thread. "
                string inlineTaskInfo = executingThreadId == Thread.CurrentThread.ManagedThreadId ? "*" : string.Empty;
                using (PerformanceStopwatch.Start($"ParallelPricing: startAt {startTime:N0} ms, endAt {endTime:N0} ms, " +
                                                  $"execution time {executionTime:N0} ms, {inlineTaskInfo}ManagedThreadId = {Thread.CurrentThread.ManagedThreadId}"))
                {
                }

                tempDebugList.Enqueue(Tuple.Create(PerformanceStopwatch.ReportOutput, executionTime));
                tempResultList.Enqueue(p);
            });

            CPriceGenerator priceGenerator = Merge(tempResultList.ToArray());

            sw.Stop();

            string category = string.Empty;

            if (sw.ElapsedMilliseconds > 20000)
            {
                // dd 3/17/2017 - This category is only for logging purpose. By having unique string I can search in PB log for timing over 20 seconds.
                category = "[TimeOver20s] ";
            }
            StringBuilder sb1 = new StringBuilder();
            Guid parallelId = Guid.NewGuid();

            sb1.AppendLine($"{category}Execute in parallel. # of products: {onlyTheseProdIds.Count()}. # of small tasks: {workList.Count()}. GetPricingQuote: {pricingQuoteTime:N0}ms. Execution: {sw.ElapsedMilliseconds:N0}ms. ParallelId {parallelId}");            
            foreach( var pair in tempDebugList.ToList().OrderByDescending(pair => pair.Item2 /* executionTime*/) )
            {
                sb1.AppendLine(pair.Item1 /*log*/);
                sb1.AppendLine();
            }

            Tools.LogInfo("PricingTiming", sb1.ToString(), (int)sw.ElapsedMilliseconds);

            using (PerformanceStopwatch.Start($"ParallelId {parallelId}, # products {onlyTheseProdIds.Count()}, #sub-tasks {workList.Count()}, execution: {sw.ElapsedMilliseconds:N0}ms."
                                              + $" Before parallel, ran GetPricingQuote: {pricingQuoteTime:N0}ms."))
            {
            }

            return priceGenerator;
        }
        /// <summary>
        /// Merge multiple price generator objects into single object.
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private static CPriceGenerator Merge(IEnumerable<CPriceGenerator> list)
        {
            using (PerformanceStopwatch.Start("CPriceGenerator.Merger"))
            {
                SortedList sortLoanProductsList = new SortedList();
                CPageData dataLoan = null;

                foreach (var item in list)
                {
                    if (dataLoan == null)
                    {
                        // Use the first CPageData
                        dataLoan = item.m_loanFileData;
                    }

                    foreach (var product in item.GetApplicantPriceList())
                    {
                        string key = product.lLpTemplateNm;
                        try
                        {
                            sortLoanProductsList.Add(key, product);
                        }
                        catch (ArgumentException)
                        {
                            if (sortLoanProductsList.Contains(key))
                            {
                                for (int iExt = 2; iExt < 200; ++iExt)
                                {
                                    string newKey = key + "_" + iExt.ToString();
                                    if (!sortLoanProductsList.Contains(newKey))
                                    {
                                        sortLoanProductsList.Add(newKey, product);
                                        break;
                                    }
                                }
                            }
                        }
                    }

                }

                CPriceGenerator resultPriceGenerator = new CPriceGenerator();

                resultPriceGenerator.m_sortedLoanProducts = sortLoanProductsList;
                resultPriceGenerator.m_loanFileData = dataLoan;

                return resultPriceGenerator;
            }
        }

        private static IEnumerable<IEnumerable<Guid>> PartitionList(IEnumerable<Guid> list, int maxItemPerPartition)
        {
            using (PerformanceStopwatch.Start("CPriceGenerator.PartitionList"))
            {
                if (list == null)
                {
                    return null;
                }

                List<IEnumerable<Guid>> result = new List<IEnumerable<Guid>>();

                List<Guid> subsetList = null;

                foreach (var item in list)
                {
                    if (subsetList == null || subsetList.Count == maxItemPerPartition)
                    {
                        subsetList = new List<Guid>();
                        result.Add(subsetList);
                    }

                    subsetList.Add(item);
                }

                return result;
            }
        }
    }
}