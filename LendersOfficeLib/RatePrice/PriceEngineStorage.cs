﻿namespace LendersOffice.RatePrice
{
    using System;
    using System.Data.SqlClient;
    using System.IO;
    using DataAccess;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Pricing engine storage that will store data in persistent storage. Persistent storage could be FileDB3 or S3.
    /// </summary>
    public class PriceEngineStorage : IPriceEngineStorage
    {
        /// <summary>
        /// Retrieve the file from persistent storage. Throw FileNotFoundException if key is not in storage.
        /// </summary>
        /// <param name="type">The type of file to retrieve.</param>
        /// <param name="key">The key of file to retrieve.</param>
        /// <returns>The local file path contains the file.</returns>
        public LocalFilePath Retrieve(PriceEngineStorageType type, SHA256Checksum key)
        {
            string path = null;

            try
            {
                path = FileDBTools.CreateCopy(E_FileDB.PricingSnapshot, key.Value);
            }
            catch (FileNotFoundException)
            {
                path = FileDBTools.CreateCopy(E_FileDB.Normal, key.Value);
            }

            LocalFilePath? o = LocalFilePath.Create(path);
            if (o == null)
            {
                throw CBaseException.GenericException("Invalid temp path");
            }

            return o.Value;
        }

        /// <summary>
        /// Save the file in local path to persistent storage.
        /// </summary>
        /// <param name="type">The type of file to save.</param>
        /// <param name="key">The key of file to save.</param>
        /// <param name="path">The local path of the file save.</param>
        public void Save(PriceEngineStorageType type, SHA256Checksum key, LocalFilePath path)
        {
            FileDBTools.WriteFile(E_FileDB.PricingSnapshot, key.Value, path.Value);

            SqlParameter[] parameters =
            {
                new SqlParameter("@CreatedDate", DateTime.Now),
                new SqlParameter("@FileKey", key.Value),
                new SqlParameter("@FileType", this.GetTypeDescription(type))
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, "LPE_FILE_KEY_LIST_Insert", 3, parameters);
        }

        /// <summary>
        /// Get the string description of type.
        /// </summary>
        /// <param name="type">Storage type.</param>
        /// <returns>String description of type.</returns>
        private string GetTypeDescription(PriceEngineStorageType type)
        {
            switch (type)
            {
                case PriceEngineStorageType.PricePolicy:
                    return "PRICE_POLICY";
                case PriceEngineStorageType.RateOptions:
                    return "RATE_OPTIONS";
                case PriceEngineStorageType.PricingSnapshot:
                    return "PRICING_SNAPSHOT";
                case PriceEngineStorageType.PerLenderSnapshot:
                    return "PERLENDER_SNAPSHOT";
                case PriceEngineStorageType.PricePolicySnapshot:
                    return "PRICE_POLICY_SNAPSHOT";
                default:
                    throw new UnhandledEnumException(type);
            }
        }
    }
}