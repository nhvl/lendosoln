﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOfficeApp.los.RatePrice;

using DataAccess;
using LendersOffice.RatePrice.FileBasedPricing;
using LendersOffice.Common;
using LqbGrammar.DataTypes;
using LendersOffice.ObjLib.HistoricalPricing;
using LendersOffice.Admin;

namespace LendersOffice.RatePrice
{
    public enum E_LoanProgramSetPricingT
    {
        Undefined = 0,
        Snapshot = 1,
        Historical = 2
    }

    public class HybridLoanProgramSetOption
    {

        public HybridLoanProgramSetOption()
        {
            // 1/8/2014 dd - Only separate historical pricing on two main buckets: Policy & RateOptions.
            // PmiManager and loan program template will always use current info.

            LoanProgramTemplate = E_LoanProgramSetPricingT.Snapshot;
            PmiManager = E_LoanProgramSetPricingT.Snapshot;
        }

        /// <summary>
        /// Gets or sets the content key of pricing snapshot to use.
        /// Note: The reason this property a string instead of SHA256Checksum is because of Xml serialization.
        /// </summary>
        public string SnapshotContentKey { get; set; } = SHA256Checksum.Invalid.Value;

        public E_LoanProgramSetPricingT LoanProgramTemplate { get; set; }
        public E_LoanProgramSetPricingT ApplicablePolicies { get; set; }
        public E_LoanProgramSetPricingT RateOptions { get; set; }
        public E_LoanProgramSetPricingT PmiManager { get; set; }

        public E_LoanProgramSetPricingT PriceGroupOptions { get; set; }
        public string PriceGroupContentKey { get; set; } = SHA256Checksum.Invalid.Value;

        public E_LoanProgramSetPricingT FeeServiceOptions { get; set; }
        public int FeeServiceRevisionId { get; set; }

        public bool UsingSamePolicyOrderEnabled { get; set; }
        public E_LoanProgramSetPricingT LenderFeeOptions { get; set; }

        public bool IsHistoricalMode()
        {
            if (string.IsNullOrEmpty(this.SnapshotContentKey))
            {
                return false;
            }

            return this.ApplicablePolicies == E_LoanProgramSetPricingT.Historical || this.RateOptions == E_LoanProgramSetPricingT.Historical ||
                this.PmiManager == E_LoanProgramSetPricingT.Historical || this.LoanProgramTemplate == E_LoanProgramSetPricingT.Historical ||
                this.PriceGroupOptions == E_LoanProgramSetPricingT.Historical || this.FeeServiceOptions == E_LoanProgramSetPricingT.Historical;
        }

        // opm 474737 verify subsnapshot pricing. 
        // Thien 9/30/2018 this property will remove soon.
        public bool? UseSubsnapshot { get; set; } = null;

        /// <summary>
        /// Return settings for using pricing in LPE database or from import snapshot file.
        /// </summary>
        /// <param name="broker">The broker to retrieve current snapshot settings.</param>
        /// <returns></returns>
        public static HybridLoanProgramSetOption GetCurrentSnapshotOption(BrokerDB broker)
        {
            if (broker.ActualPricingBrokerId == Guid.Empty)
            {
                // Use pricing from LPE database.
                return new HybridLoanProgramSetOption()
                {
                    LoanProgramTemplate = E_LoanProgramSetPricingT.Snapshot,
                    ApplicablePolicies = E_LoanProgramSetPricingT.Snapshot,
                    RateOptions = E_LoanProgramSetPricingT.Snapshot,
                    PmiManager = E_LoanProgramSetPricingT.Snapshot,
                    UsingSamePolicyOrderEnabled = false
                };
            }
            else
            {
                var snapshot = broker.RetrieveLatestManualImport();

                return new HybridLoanProgramSetOption()
                {
                    LoanProgramTemplate = E_LoanProgramSetPricingT.Historical,
                    PmiManager = E_LoanProgramSetPricingT.Historical,
                    UsingSamePolicyOrderEnabled = false,

                    SnapshotContentKey = snapshot.FileKey.Value,
                    ApplicablePolicies = E_LoanProgramSetPricingT.Historical,
                    RateOptions = E_LoanProgramSetPricingT.Historical,
                    PriceGroupOptions = E_LoanProgramSetPricingT.Snapshot,
                    FeeServiceOptions = E_LoanProgramSetPricingT.Snapshot
                };
            }
        }

        public static HybridLoanProgramSetOption 
        CreateHistoricalPricingOption(string snapshotContentKey, 
                                      E_LoanProgramSetPricingT priceGroupOptions, string priceGroupContentKey,
                                      E_LoanProgramSetPricingT feeServiceOptions, int feeServiceRevisionId)
        {
            return new HybridLoanProgramSetOption()
            {
                LoanProgramTemplate = E_LoanProgramSetPricingT.Historical,
                PmiManager = E_LoanProgramSetPricingT.Snapshot,
                UsingSamePolicyOrderEnabled = false,

                SnapshotContentKey = snapshotContentKey,
                ApplicablePolicies = E_LoanProgramSetPricingT.Historical,
                RateOptions = E_LoanProgramSetPricingT.Historical,
                PriceGroupOptions = priceGroupOptions,
                PriceGroupContentKey = priceGroupContentKey,
                FeeServiceOptions = feeServiceOptions,
                FeeServiceRevisionId = feeServiceRevisionId,
            };
        }

        public static HybridLoanProgramSetOption
        CreateHistoricalPricingOption(HistoricalPricingSettings settings, SubmissionSnapshot snapshot)
        {
            int feeServiceRevisionId = CApplicantPrice.InvalidFeeServiceRevisionId;

            if (settings.FeeService == HistoricalSetting.Historical)
            {
                if (snapshot.FeeServiceRevisionId.HasValue)
                {
                    feeServiceRevisionId = snapshot.FeeServiceRevisionId.Value;
                }
                else
                {
                    // Camron mentioned that if we don't have a historical fee service revision, 
                    // we don't need to error.
                    Tools.LogWarning(
                        "Lender is configured to use historical fee service, but SubmissionSnapshot "
                        + "did not have a historical fee service revision id.");
                }
            }

            return HybridLoanProgramSetOption.CreateHistoricalPricingOption(
                        snapshot.SnapshotFileKey.Value,
                        settings.PriceGroup == HistoricalSetting.Current ? E_LoanProgramSetPricingT.Snapshot : E_LoanProgramSetPricingT.Historical,
                        snapshot.PriceGroupFileKey.Value,
                        settings.FeeService == HistoricalSetting.Current ? E_LoanProgramSetPricingT.Snapshot : E_LoanProgramSetPricingT.Historical,
                        feeServiceRevisionId);
        }

    }
    public class HybridLoanProgramSet : AbstractLoanProgramSet
    {
        private AbstractLoanProgramSet m_snapShotLoanProgramSet;
        private FileBasedLoanProgramSet m_fileBasedLoanProgramSet;
        private HybridLoanProgramSetOption m_options;

        private List<HybridLoanProgramTemplate> m_hybridLoanProgramTemplateList = null;

        public override bool IsCurrentSnapshot
        {
            get 
            {
                if (m_options.RateOptions == E_LoanProgramSetPricingT.Historical)
                {
                    return false;
                }
                if (m_options.ApplicablePolicies == E_LoanProgramSetPricingT.Historical)
                {
                    return false;
                }
                if (m_options.PmiManager == E_LoanProgramSetPricingT.Historical)
                {
                    return false;
                }
                if (m_options.LoanProgramTemplate == E_LoanProgramSetPricingT.Historical)
                {
                    return false;
                }
                return true;
            }
        }

        public HybridLoanProgramSet(AbstractLoanProgramSet snapShotLoanProgramSet, FileBasedLoanProgramSet fileBasedLoanProgramSet,
            HybridLoanProgramSetOption options)
        {
            if (snapShotLoanProgramSet == null && fileBasedLoanProgramSet == null)
            {
                throw CBaseException.GenericException("snapShotLoanProgramSet and fileBasedLoanProgramSet CANNOT be both NULL");
            }
            if (options == null)
            {
                throw CBaseException.GenericException("options cannot be null");
            }
            Assert("LoanProgramTemplate", snapShotLoanProgramSet, fileBasedLoanProgramSet, options.LoanProgramTemplate);
            Assert("ApplicablePolicies", snapShotLoanProgramSet, fileBasedLoanProgramSet, options.ApplicablePolicies);
            Assert("RateOptions", snapShotLoanProgramSet, fileBasedLoanProgramSet, options.RateOptions);
            Assert("PmiManager", snapShotLoanProgramSet, fileBasedLoanProgramSet, options.PmiManager);


            m_snapShotLoanProgramSet = snapShotLoanProgramSet;
            m_fileBasedLoanProgramSet = fileBasedLoanProgramSet;
            m_options = options;

            if (options.LoanProgramTemplate == E_LoanProgramSetPricingT.Snapshot && options.RateOptions == E_LoanProgramSetPricingT.Historical)
            {
                m_hybridLoanProgramTemplateList = new List<HybridLoanProgramTemplate>();

                for (int i = 0; i < snapShotLoanProgramSet.Count; i++)
                {
                    CLoanProductData snapShotLp = snapShotLoanProgramSet.RetrieveByIndex(i) as CLoanProductData;
                    FileBasedLoanProgramTemplate historicalLp = fileBasedLoanProgramSet.RetrieveByKey(snapShotLp.lLpTemplateId) as FileBasedLoanProgramTemplate;

                    m_hybridLoanProgramTemplateList.Add(new HybridLoanProgramTemplate(snapShotLp, historicalLp));
                }
            }
        }

        private void Assert(string desc, AbstractLoanProgramSet snapShotLoanProgramSet, 
            FileBasedLoanProgramSet fileBasedLoanProgramSet,
            E_LoanProgramSetPricingT setting)
        {
            switch (setting)
            {
                case E_LoanProgramSetPricingT.Undefined:
                    throw CBaseException.GenericException(desc + " cannot be Undefined");
                case E_LoanProgramSetPricingT.Snapshot:
                    if (snapShotLoanProgramSet == null)
                    {
                        throw CBaseException.GenericException(desc + " is defined to use Snapshot. However snapshot is not available.");
                    }
                    break;
                case E_LoanProgramSetPricingT.Historical:
                    if (fileBasedLoanProgramSet == null)
                    {
                        throw CBaseException.GenericException(desc + " is defined to use Historical. However historical is not available.");
                    }
                    break;
                default:
                    throw new UnhandledEnumException(setting);
            }
        }
        private AbstractLoanProgramSet GetLoanProgramSet(E_LoanProgramSetPricingT settings)
        {
            switch (settings)
            {
                case E_LoanProgramSetPricingT.Snapshot:
                    return m_snapShotLoanProgramSet;
                case E_LoanProgramSetPricingT.Historical:
                    return m_fileBasedLoanProgramSet;
                case E_LoanProgramSetPricingT.Undefined:
                default:
                    throw new UnhandledEnumException(settings);
            }
        }
        public override DateTime VersionD
        {
            get 
            {
                // 12/20/2013 dd - Not sure which version date we should return.
                // I just assume use version date of the rate option.
                return GetLoanProgramSet(m_options.RateOptions).VersionD;
            }
        }

        public override SHA256Checksum FileKey
        {
            get
            {
                return GetLoanProgramSet(m_options.RateOptions).FileKey;
            }
        }
        public override AbstractRtPricePolicy GetPricePolicy(Guid policyId)
        {
            return GetLoanProgramSet(m_options.ApplicablePolicies).GetPricePolicy(policyId);
        }

        public override AbstractRtPricePolicy GetPmiPricePolicy(Guid policyId)
        {
            return GetLoanProgramSet(m_options.PmiManager).GetPricePolicy(policyId);
        }

        public override IEnumerable<Guid> GetApplicablePolicies(Guid lLpTemplateId)
        {
            IEnumerable<Guid> policies = GetLoanProgramSet(m_options.ApplicablePolicies).GetApplicablePolicies(lLpTemplateId);

            if(m_options.UsingSamePolicyOrderEnabled)
            {
                IEnumerable<Guid> dbPolicies = m_snapShotLoanProgramSet.GetApplicablePolicies(lLpTemplateId);

                string dbPolicyIdsOnly = string.Empty;
                string hybridPolicyIdsOnly = string.Empty;
                if( AreSetsEqual(dbPolicies, policies, out dbPolicyIdsOnly, out hybridPolicyIdsOnly) )
                {
                    return dbPolicies;
                }
            }

            return policies;
        }

        public override IPolicyRelationDb PolicyRelation
        {
            get 
            {
                return GetLoanProgramSet(m_options.ApplicablePolicies).PolicyRelation;
            }
        }

        public override PmiManager PmiManager
        {
            get 
            {
                return GetLoanProgramSet(m_options.PmiManager).PmiManager;
            }
        }

        public override int Count
        {
            get { return GetLoanProgramSet(m_options.LoanProgramTemplate).Count; }
        }

        public override ILoanProgramTemplate RetrieveByIndex(int index)
        {
            if (m_hybridLoanProgramTemplateList != null)
            {
                return m_hybridLoanProgramTemplateList[index];
            }
            else
            {
                return GetLoanProgramSet(m_options.LoanProgramTemplate).RetrieveByIndex(index);
            }
        }

        public override ILoanProgramTemplate RetrieveByKey(Guid lLpTemplateId)
        {
            return this.RetrieveByKey(lLpTemplateId, forceCurrent: false);
        }

        public ILoanProgramTemplate RetrieveByKey(Guid lLpTemplateId, bool forceCurrent)
        {
            if (forceCurrent)
            {
                return this.m_snapShotLoanProgramSet.RetrieveByKey(lLpTemplateId);
            }
            else if (m_hybridLoanProgramTemplateList != null)
            {
                foreach (var o in m_hybridLoanProgramTemplateList)
                {
                    if (o.lLpTemplateId == lLpTemplateId)
                    {
                        return o;
                    }
                }
                throw new NotFoundException(lLpTemplateId + " is not found.");
            }
            else
            {
                return GetLoanProgramSet(m_options.LoanProgramTemplate).RetrieveByKey(lLpTemplateId);
            }
        }

        public override string FriendlyInfo
        {
            get 
            {

                return string.Format("HybridLoanProgramSet: RateOptions=[{0}], PricePolicy=[{1}], HistoricalVersionD=[{2}], CurrentVersionD=[{3}]",
                    m_options.RateOptions, m_options.ApplicablePolicies, m_fileBasedLoanProgramSet.VersionD, m_snapShotLoanProgramSet.VersionD);
            }
        }

        static private bool AreSetsEqual(IEnumerable<Guid> listA, IEnumerable<Guid> listB, out string aMinusB, out string bMinusA, bool onlylogFirstDiff = true)
        {
            listA = listA ?? Enumerable.Empty<Guid>();
            listB = listB ?? Enumerable.Empty<Guid>();
            aMinusB = string.Join(", ", listA.Except(listB));
            bMinusA = string.Join(", ", listB.Except(listA));

            if (onlylogFirstDiff)
            {
                int pos = aMinusB.IndexOf(",");
                if( pos > 0)
                {
                    aMinusB = aMinusB.Substring(0, pos) + " ...";
                }

                pos = bMinusA.IndexOf(",");
                if (pos > 0)
                {
                    bMinusA = bMinusA.Substring(0, pos) + " ...";
                }
            }

            return string.IsNullOrEmpty(aMinusB) && string.IsNullOrEmpty(bMinusA);
        }

        static bool AreDecimalsEqual(decimal x, decimal y)
        {
            return x == y;
        }

        public bool PartialCompareWithDbSnapshot(out string logMsg, bool ignorePolicyOrder = false)
        {

            if (this.m_snapShotLoanProgramSet == null)
            {
                logMsg = "Error: database snapshot's LoanProgramSet is null. Don't have data to compare.";
                return false;
            }

            AbstractLoanProgramSet dbLoanProgramSet = this.m_snapShotLoanProgramSet;
            StringBuilder sb = new StringBuilder();
            bool result = true;
            bool sameLoansOrder = true;
            string dbPolicyIdsOnly = string.Empty;
            string hybridPolicyIdsOnly = string.Empty;

            try
            {
                sb.AppendFormat("{1}Filebased Snapshot Id: {0}{1}", FileBasedPricingUtilities.GetFileDBFileName(this.m_fileBasedLoanProgramSet.VersionD), Environment.NewLine);

                if( dbLoanProgramSet.VersionD != this.VersionD )
                {
                    sb.AppendFormat("LoanProgramSet.VersionD {0} != HybridLoanProgramSet.VersionD {1}",
                                     dbLoanProgramSet.VersionD.ToString("MM/dd/yyyy HH:mm:ss"), this.VersionD.ToString("MM/dd/yyyy HH:mm:ss")).AppendLine();

                }

                if (dbLoanProgramSet.Count != this.Count)
                {
                    sb.AppendFormat("LoanProgramSet.Count = {0} != HybridLoanProgramSet.Count {1}",
                                     dbLoanProgramSet.Count, this.Count).AppendLine();
                    logMsg = sb.ToString();
                    return false;
                }

                sb.AppendFormat("DbSnapshot LoanProgramSet have {0} product(s)", dbLoanProgramSet.Count).AppendLine();

                Guid strangeProductId = Guid.Empty;
                for (int i = 0; i < dbLoanProgramSet.Count; i++)
                {
                    ILoanProgramTemplate dbLoanProgramTemplate = dbLoanProgramSet.RetrieveByIndex(i);
                    Guid lLpTemplateId = dbLoanProgramTemplate.lLpTemplateId;
                    ILoanProgramTemplate loanProgramTemplate = this.RetrieveByKey(lLpTemplateId);

                    IEnumerable<Guid> dbPolicies = dbLoanProgramSet.GetApplicablePolicies(lLpTemplateId);
                    IEnumerable<Guid> policies = this.GetApplicablePolicies(lLpTemplateId);

                    IRateItem[] dbRateItems = dbLoanProgramTemplate.GetRawRateSheetWithDeltas();
                    IRateItem[] rateItems = loanProgramTemplate.GetRawRateSheetWithDeltas();

                    if (i == 0)
                    {
                        sb.AppendLine($"   LoanProgramTemplate type: {dbLoanProgramTemplate.GetType()} (its counterpart {loanProgramTemplate.GetType()})");
                    }

                    if (lLpTemplateId != this.RetrieveByIndex(i).lLpTemplateId)
                    {
                        sameLoansOrder = false;
                    }

                    string policyInfo = string.Empty;
                    bool policyEqual = AreSetsEqual(dbPolicies, policies, out dbPolicyIdsOnly, out hybridPolicyIdsOnly);

                    if (policyEqual == false)
                    {
                        result = false;
                        strangeProductId = lLpTemplateId;
                        policyInfo = " (counterpart: different)";
                    }
                    else if( dbPolicies.SequenceEqual(policies) == false)
                    {
                        policyInfo = " (counterpart: different order)";
                        if (!ignorePolicyOrder)
                        {
                            result = false;
                        }
                        // Thien's note: it is normal if the policies order for db snapshot and filebased snapshot are different.
                    }

                    sb.AppendFormat("   {0}, SrcRateOptionsProgId {1}, #rateItems {2},  # applicablePolicies {3}{4}", 
                                    lLpTemplateId, dbLoanProgramTemplate.SrcRateOptionsProgId, dbRateItems.Length, dbPolicies.Count(), policyInfo).AppendLine();

                    if (policyEqual == false)
                    {
                        if (dbPolicies.Count() != policies.Count())
                        {
                            sb.AppendFormat("       ***** PoliciesDiff: HybridLoanProgramSet  #applicablePolicies {0}.", policies.Count()).AppendLine();
                        }

                        if (string.IsNullOrEmpty(hybridPolicyIdsOnly) == false)
                        {
                            sb.AppendFormat("       ***** PoliciesDiff: LoanProgramSet policies don't contain {0} .", hybridPolicyIdsOnly).AppendLine();
                        }

                        if (string.IsNullOrEmpty(dbPolicyIdsOnly) == false)
                        {
                            sb.AppendFormat("       ***** PoliciesDiff: HybridLoanProgramSet policies don't contain {0}.", dbPolicyIdsOnly).AppendLine();
                        }
                    }

                    if (dbLoanProgramTemplate.SrcRateOptionsProgId != loanProgramTemplate.SrcRateOptionsProgId)
                    {
                        sb.AppendFormat("       ***** RateOptionDiff: HybridLoanProgramSet SrcRateOptionsProgId {0}.", loanProgramTemplate.SrcRateOptionsProgId).AppendLine();
                        result = false;
                    }

                    if (dbRateItems.Length != rateItems.Length)
                    {
                        sb.AppendFormat("       ***** RateOptionDiff: HybridLoanProgramSet #rateItems {0}.", rateItems.Length).AppendLine();
                        result = false;
                    }

                    string errMsg;
                    if (!PartialCompare(dbLoanProgramTemplate, loanProgramTemplate, out errMsg))
                    {
                        sb.AppendLine($"       ***** LoanProgramTemplate: {errMsg}").AppendLine();
                        result = false;
                    }
                }

                sb.AppendLine().AppendFormat("DBSnapshot LoanProgramSet and HybridLoanProgramSet are {0} order.", sameLoansOrder ? "same" : "different").AppendLine().AppendLine();

                if (sameLoansOrder == false)
                {
                    sb.AppendFormat("HybridLoanProgramSet have {0} product(s)", dbLoanProgramSet.Count).AppendLine();
                    for (int i = 0; i < Count; i++)
                    {
                        sb.AppendFormat("   {0}", this.RetrieveByIndex(i).lLpTemplateId).AppendLine();
                    }
                }

                if (strangeProductId != Guid.Empty)
                {
                    logMsg = sb.ToString();
                    return false;
                }

                //compare rateOptions
                for (int i = 0; i < dbLoanProgramSet.Count; i++)
                {
                    ILoanProgramTemplate dbLoanProgramTemplate = dbLoanProgramSet.RetrieveByIndex(i);
                    Guid lLpTemplateId = dbLoanProgramTemplate.lLpTemplateId;
                    ILoanProgramTemplate loanProgramTemplate = this.RetrieveByKey(lLpTemplateId);

                    if (dbLoanProgramTemplate.SrcRateOptionsProgId != loanProgramTemplate.SrcRateOptionsProgId)
                    {
                        sb.AppendFormat("RateOptionDiff: with lLpTemplateId = {0} their SrcRateOptionsProgId are different: LoanProgramSet '{1}' != HybridLoanProgramSet {2}.",
                                         lLpTemplateId, dbLoanProgramTemplate.SrcRateOptionsProgId, loanProgramTemplate.SrcRateOptionsProgId).AppendLine();
                        result = false;
                    }

                    IRateItem[] dbRateItems = dbLoanProgramTemplate.GetRawRateSheetWithDeltas();
                    IRateItem[] rateItems = loanProgramTemplate.GetRawRateSheetWithDeltas();

                    if (dbRateItems.Length != dbRateItems.Length)
                    {
                        sb.AppendFormat("RateOptionDiff: with lLpTemplateId = {0}, their GetRawRateSheetWithDeltas() are different #rateItems:  LoanProgramSet {1} rateItems != HybridLoanProgramSet {2} rateItems.",
                                         lLpTemplateId, dbRateItems.Length, rateItems.Length).AppendLine();
                        result = false;
                        continue;
                    }

                    for (int k = 0; k < dbRateItems.Length; k++)
                    {
                        IRateItem dbRateItem = dbRateItems[k];
                        IRateItem rateItem = rateItems[k];

                        if (dbRateItem.IsSpecialKeyword != rateItem.IsSpecialKeyword)
                        {
                            sb.AppendFormat("RateOptionDiff: with lLpTemplateId = {0}, their RateItem[{1}]'s IsSpecialKeyword are different: {2} vs {3}",
                                             lLpTemplateId, k, dbRateItem.IsSpecialKeyword, rateItem.IsSpecialKeyword).AppendLine();
                            result = false;
                            break;
                        }

                        if (dbRateItem.IsSpecialKeyword)
                        {
                            if (dbRateItem.RateSheetKeyword != rateItem.RateSheetKeyword)
                            {
                                sb.AppendFormat("RateOptionDiff: with lLpTemplateId = {0}, their RateItem[{1}]'s RateSheetKeyword are different: {2} vs {3}",
                                                 lLpTemplateId, k, dbRateItem.RateSheetKeyword, rateItem.RateSheetKeyword).AppendLine();
                                result = false;
                                break;
                            }

                            continue;
                        }

                        if (AreDecimalsEqual(dbRateItem.Rate, rateItem.Rate) == false)
                        {
                            sb.AppendFormat("RateOptionDiff: with lLpTemplateId = {0}, their RateItem[{1}]'s Rate are different: {2} vs {3}",
                                             lLpTemplateId, k, dbRateItem.Rate, rateItem.Rate).AppendLine();
                            result = false;
                            break;
                        }

                        if (AreDecimalsEqual(dbRateItem.Point, rateItem.Point) == false)
                        {
                            sb.AppendFormat("RateOptionDiff: with lLpTemplateId = {0}, their RateItem[{1}]'s Point are different: {2} vs {3}",
                                             lLpTemplateId, k, dbRateItem.Point, rateItem.Point).AppendLine();
                            result = false;
                            break;
                        }

                        if (AreDecimalsEqual(dbRateItem.Margin, rateItem.Margin) == false)
                        {
                            sb.AppendFormat("RateOptionDiff: with lLpTemplateId = {0}, their RateItem[{1}]'s Margin are different: {2} vs {3}",
                                             lLpTemplateId, k, dbRateItem.Margin, rateItem.Margin).AppendLine();
                            result = false;
                            break;
                        }


                        if (AreDecimalsEqual(dbRateItem.QRateBase, rateItem.QRateBase) == false)
                        {
                            sb.AppendFormat("RateOptionDiff: with lLpTemplateId = {0}, their RateItem[{1}]'s QRateBase are different: {2} vs {3}",
                                             lLpTemplateId, k, dbRateItem.QRateBase, rateItem.QRateBase).AppendLine();
                            result = false;
                            break;
                        }

                        if (AreDecimalsEqual(dbRateItem.TeaserRate, rateItem.TeaserRate) == false)
                        {
                            sb.AppendFormat("RateOptionDiff: with lLpTemplateId = {0}, their RateItem[{1}]'s TeaserRate are different: {2} vs {3}",
                                             lLpTemplateId, k, dbRateItem.TeaserRate, rateItem.TeaserRate).AppendLine();
                            result = false;
                            break;
                        }
                    }

                }
            }
            catch(SystemException ex)
            {
                sb.AppendFormat("Exception in PartialCompareWithDbSnapshot " + Environment.NewLine  + ex.Message + Environment.NewLine + ex.StackTrace);
                result = false;
            }

            logMsg = sb.ToString();
            return result;
        }

        #region PartialCompare for ILoanProgramTemplate
        private bool PartialCompare(ILoanProgramTemplate x, ILoanProgramTemplate y, out string errMsg)
        {
            StringBuilder sb = new StringBuilder();
            bool retVal = true;
            errMsg = string.Empty;

            if (x.lLpTemplateId != y.lLpTemplateId)
            {
                sb.AppendFormat("    lLpTemplateId: '{0}' vs '{1}'", x.lLpTemplateId, y.lLpTemplateId).AppendLine();
                retVal = false;
            }

            if (x.BrokerId != y.BrokerId)
            {
                sb.AppendFormat("    BrokerId: '{0}' vs '{1}'", x.BrokerId, y.BrokerId).AppendLine();
                retVal = false;
            }

            if (x.lLpTemplateNm != y.lLpTemplateNm)
            {
                sb.AppendFormat("    lLpTemplateNm: '{0}' vs '{1}'", x.lLpTemplateNm, y.lLpTemplateNm).AppendLine();
                retVal = false;
            }

            if (x.lLendNm != y.lLendNm)
            {
                sb.AppendFormat("    lLendNm: '{0}' vs '{1}'", x.lLendNm, y.lLendNm).AppendLine();
                retVal = false;
            }

            if (x.lLT != y.lLT)
            {
                sb.AppendFormat("    lLT: '{0}' vs '{1}'", x.lLT, y.lLT).AppendLine();
                retVal = false;
            }

            if (x.lLienPosT != y.lLienPosT)
            {
                sb.AppendFormat("    lLienPosT: '{0}' vs '{1}'", x.lLienPosT, y.lLienPosT).AppendLine();
                retVal = false;
            }

            if (x.lQualR != y.lQualR)
            {
                sb.AppendFormat("    lQualR: '{0}' vs '{1}'", x.lQualR, y.lQualR).AppendLine();
                retVal = false;
            }

            if (x.lTerm != y.lTerm)
            {
                sb.AppendFormat("    lTerm: '{0}' vs '{1}'", x.lTerm, y.lTerm).AppendLine();
                retVal = false;
            }

            if (x.lTerm_rep != y.lTerm_rep)
            {
                sb.AppendFormat("    lTerm_rep: '{0}' vs '{1}'", x.lTerm_rep, y.lTerm_rep).AppendLine();
                retVal = false;
            }

            if (x.lDue != y.lDue)
            {
                sb.AppendFormat("    lDue: '{0}' vs '{1}'", x.lDue, y.lDue).AppendLine();
                retVal = false;
            }

            if (x.lDue_rep != y.lDue_rep)
            {
                sb.AppendFormat("    lDue_rep: '{0}' vs '{1}'", x.lDue_rep, y.lDue_rep).AppendLine();
                retVal = false;
            }

            if (x.lLockedDays != y.lLockedDays)
            {
                sb.AppendFormat("    lLockedDays: '{0}' vs '{1}'", x.lLockedDays, y.lLockedDays).AppendLine();
                retVal = false;
            }

            if (x.lLockedDays_rep != y.lLockedDays_rep)
            {
                sb.AppendFormat("    lLockedDays_rep: '{0}' vs '{1}'", x.lLockedDays_rep, y.lLockedDays_rep).AppendLine();
                retVal = false;
            }

            if (x.lReqTopR != y.lReqTopR)
            {
                sb.AppendFormat("    lReqTopR: '{0}' vs '{1}'", x.lReqTopR, y.lReqTopR).AppendLine();
                retVal = false;
            }

            if (x.lReqTopR_rep != y.lReqTopR_rep)
            {
                sb.AppendFormat("    lReqTopR_rep: '{0}' vs '{1}'", x.lReqTopR_rep, y.lReqTopR_rep).AppendLine();
                retVal = false;
            }

            if (x.lReqBotR != y.lReqBotR)
            {
                sb.AppendFormat("    lReqBotR: '{0}' vs '{1}'", x.lReqBotR, y.lReqBotR).AppendLine();
                retVal = false;
            }

            if (x.lReqBotR_rep != y.lReqBotR_rep)
            {
                sb.AppendFormat("    lReqBotR_rep: '{0}' vs '{1}'", x.lReqBotR_rep, y.lReqBotR_rep).AppendLine();
                retVal = false;
            }

            if (x.lRadj1stCapR != y.lRadj1stCapR)
            {
                sb.AppendFormat("    lRadj1stCapR: '{0}' vs '{1}'", x.lRadj1stCapR, y.lRadj1stCapR).AppendLine();
                retVal = false;
            }

            if (x.lRadj1stCapR_rep != y.lRadj1stCapR_rep)
            {
                sb.AppendFormat("    lRadj1stCapR_rep: '{0}' vs '{1}'", x.lRadj1stCapR_rep, y.lRadj1stCapR_rep).AppendLine();
                retVal = false;
            }

            if (x.lRadj1stCapMon != y.lRadj1stCapMon)
            {
                sb.AppendFormat("    lRadj1stCapMon: '{0}' vs '{1}'", x.lRadj1stCapMon, y.lRadj1stCapMon).AppendLine();
                retVal = false;
            }

            if (x.lRadj1stCapMon_rep != y.lRadj1stCapMon_rep)
            {
                sb.AppendFormat("    lRadj1stCapMon_rep: '{0}' vs '{1}'", x.lRadj1stCapMon_rep, y.lRadj1stCapMon_rep).AppendLine();
                retVal = false;
            }

            if (x.lRAdjCapR != y.lRAdjCapR)
            {
                sb.AppendFormat("    lRAdjCapR: '{0}' vs '{1}'", x.lRAdjCapR, y.lRAdjCapR).AppendLine();
                retVal = false;
            }

            if (x.lRAdjCapR_rep != y.lRAdjCapR_rep)
            {
                sb.AppendFormat("    lRAdjCapR_rep: '{0}' vs '{1}'", x.lRAdjCapR_rep, y.lRAdjCapR_rep).AppendLine();
                retVal = false;
            }

            if (x.lRAdjCapMon != y.lRAdjCapMon)
            {
                sb.AppendFormat("    lRAdjCapMon: '{0}' vs '{1}'", x.lRAdjCapMon, y.lRAdjCapMon).AppendLine();
                retVal = false;
            }

            if (x.lRAdjCapMon_rep != y.lRAdjCapMon_rep)
            {
                sb.AppendFormat("    lRAdjCapMon_rep: '{0}' vs '{1}'", x.lRAdjCapMon_rep, y.lRAdjCapMon_rep).AppendLine();
                retVal = false;
            }

            if (x.lRAdjLifeCapR != y.lRAdjLifeCapR)
            {
                sb.AppendFormat("    lRAdjLifeCapR: '{0}' vs '{1}'", x.lRAdjLifeCapR, y.lRAdjLifeCapR).AppendLine();
                retVal = false;
            }

            if (x.lRAdjLifeCapR_rep != y.lRAdjLifeCapR_rep)
            {
                sb.AppendFormat("    lRAdjLifeCapR_rep: '{0}' vs '{1}'", x.lRAdjLifeCapR_rep, y.lRAdjLifeCapR_rep).AppendLine();
                retVal = false;
            }

            if (x.lRAdjMarginR != y.lRAdjMarginR)
            {
                sb.AppendFormat("    lRAdjMarginR: '{0}' vs '{1}'", x.lRAdjMarginR, y.lRAdjMarginR).AppendLine();
                retVal = false;
            }

            if (x.lRAdjMarginR_rep != y.lRAdjMarginR_rep)
            {
                sb.AppendFormat("    lRAdjMarginR_rep: '{0}' vs '{1}'", x.lRAdjMarginR_rep, y.lRAdjMarginR_rep).AppendLine();
                retVal = false;
            }

            if (x.lRAdjIndexR != y.lRAdjIndexR)
            {
                sb.AppendFormat("    lRAdjIndexR: '{0}' vs '{1}'", x.lRAdjIndexR, y.lRAdjIndexR).AppendLine();
                retVal = false;
            }

            if (x.lRAdjIndexR_rep != y.lRAdjIndexR_rep)
            {
                sb.AppendFormat("    lRAdjIndexR_rep: '{0}' vs '{1}'", x.lRAdjIndexR_rep, y.lRAdjIndexR_rep).AppendLine();
                retVal = false;
            }

            if (x.lRAdjFloorR != y.lRAdjFloorR)
            {
                sb.AppendFormat("    lRAdjFloorR: '{0}' vs '{1}'", x.lRAdjFloorR, y.lRAdjFloorR).AppendLine();
                retVal = false;
            }

            if (x.lRAdjFloorR_rep != y.lRAdjFloorR_rep)
            {
                sb.AppendFormat("    lRAdjFloorR_rep: '{0}' vs '{1}'", x.lRAdjFloorR_rep, y.lRAdjFloorR_rep).AppendLine();
                retVal = false;
            }

            if (x.lRAdjRoundT != y.lRAdjRoundT)
            {
                sb.AppendFormat("    lRAdjRoundT: '{0}' vs '{1}'", x.lRAdjRoundT, y.lRAdjRoundT).AppendLine();
                retVal = false;
            }

            if (x.lPmtAdjCapR != y.lPmtAdjCapR)
            {
                sb.AppendFormat("    lPmtAdjCapR: '{0}' vs '{1}'", x.lPmtAdjCapR, y.lPmtAdjCapR).AppendLine();
                retVal = false;
            }

            if (x.lPmtAdjCapR_rep != y.lPmtAdjCapR_rep)
            {
                sb.AppendFormat("    lPmtAdjCapR_rep: '{0}' vs '{1}'", x.lPmtAdjCapR_rep, y.lPmtAdjCapR_rep).AppendLine();
                retVal = false;
            }

            if (x.lPmtAdjCapMon != y.lPmtAdjCapMon)
            {
                sb.AppendFormat("    lPmtAdjCapMon: '{0}' vs '{1}'", x.lPmtAdjCapMon, y.lPmtAdjCapMon).AppendLine();
                retVal = false;
            }

            if (x.lPmtAdjCapMon_rep != y.lPmtAdjCapMon_rep)
            {
                sb.AppendFormat("    lPmtAdjCapMon_rep: '{0}' vs '{1}'", x.lPmtAdjCapMon_rep, y.lPmtAdjCapMon_rep).AppendLine();
                retVal = false;
            }

            if (x.lPmtAdjRecastPeriodMon != y.lPmtAdjRecastPeriodMon)
            {
                sb.AppendFormat("    lPmtAdjRecastPeriodMon: '{0}' vs '{1}'", x.lPmtAdjRecastPeriodMon, y.lPmtAdjRecastPeriodMon).AppendLine();
                retVal = false;
            }

            if (x.lPmtAdjRecastPeriodMon_rep != y.lPmtAdjRecastPeriodMon_rep)
            {
                sb.AppendFormat("    lPmtAdjRecastPeriodMon_rep: '{0}' vs '{1}'", x.lPmtAdjRecastPeriodMon_rep, y.lPmtAdjRecastPeriodMon_rep).AppendLine();
                retVal = false;
            }

            if (x.lPmtAdjRecastStop != y.lPmtAdjRecastStop)
            {
                sb.AppendFormat("    lPmtAdjRecastStop: '{0}' vs '{1}'", x.lPmtAdjRecastStop, y.lPmtAdjRecastStop).AppendLine();
                retVal = false;
            }

            if (x.lPmtAdjRecastStop_rep != y.lPmtAdjRecastStop_rep)
            {
                sb.AppendFormat("    lPmtAdjRecastStop_rep: '{0}' vs '{1}'", x.lPmtAdjRecastStop_rep, y.lPmtAdjRecastStop_rep).AppendLine();
                retVal = false;
            }

            if (x.lBuydwnR1 != y.lBuydwnR1)
            {
                sb.AppendFormat("    lBuydwnR1: '{0}' vs '{1}'", x.lBuydwnR1, y.lBuydwnR1).AppendLine();
                retVal = false;
            }

            if (x.lBuydwnR1_rep != y.lBuydwnR1_rep)
            {
                sb.AppendFormat("    lBuydwnR1_rep: '{0}' vs '{1}'", x.lBuydwnR1_rep, y.lBuydwnR1_rep).AppendLine();
                retVal = false;
            }

            if (x.lBuydwnR2 != y.lBuydwnR2)
            {
                sb.AppendFormat("    lBuydwnR2: '{0}' vs '{1}'", x.lBuydwnR2, y.lBuydwnR2).AppendLine();
                retVal = false;
            }

            if (x.lBuydwnR2_rep != y.lBuydwnR2_rep)
            {
                sb.AppendFormat("    lBuydwnR2_rep: '{0}' vs '{1}'", x.lBuydwnR2_rep, y.lBuydwnR2_rep).AppendLine();
                retVal = false;
            }

            if (x.lBuydwnR3 != y.lBuydwnR3)
            {
                sb.AppendFormat("    lBuydwnR3: '{0}' vs '{1}'", x.lBuydwnR3, y.lBuydwnR3).AppendLine();
                retVal = false;
            }

            if (x.lBuydwnR3_rep != y.lBuydwnR3_rep)
            {
                sb.AppendFormat("    lBuydwnR3_rep: '{0}' vs '{1}'", x.lBuydwnR3_rep, y.lBuydwnR3_rep).AppendLine();
                retVal = false;
            }

            if (x.lBuydwnR4 != y.lBuydwnR4)
            {
                sb.AppendFormat("    lBuydwnR4: '{0}' vs '{1}'", x.lBuydwnR4, y.lBuydwnR4).AppendLine();
                retVal = false;
            }

            if (x.lBuydwnR4_rep != y.lBuydwnR4_rep)
            {
                sb.AppendFormat("    lBuydwnR4_rep: '{0}' vs '{1}'", x.lBuydwnR4_rep, y.lBuydwnR4_rep).AppendLine();
                retVal = false;
            }

            if (x.lBuydwnR5 != y.lBuydwnR5)
            {
                sb.AppendFormat("    lBuydwnR5: '{0}' vs '{1}'", x.lBuydwnR5, y.lBuydwnR5).AppendLine();
                retVal = false;
            }

            if (x.lBuydwnR5_rep != y.lBuydwnR5_rep)
            {
                sb.AppendFormat("    lBuydwnR5_rep: '{0}' vs '{1}'", x.lBuydwnR5_rep, y.lBuydwnR5_rep).AppendLine();
                retVal = false;
            }

            if (x.lBuydwnMon1 != y.lBuydwnMon1)
            {
                sb.AppendFormat("    lBuydwnMon1: '{0}' vs '{1}'", x.lBuydwnMon1, y.lBuydwnMon1).AppendLine();
                retVal = false;
            }

            if (x.lBuydwnMon1_rep != y.lBuydwnMon1_rep)
            {
                sb.AppendFormat("    lBuydwnMon1_rep: '{0}' vs '{1}'", x.lBuydwnMon1_rep, y.lBuydwnMon1_rep).AppendLine();
                retVal = false;
            }

            if (x.lBuydwnMon2 != y.lBuydwnMon2)
            {
                sb.AppendFormat("    lBuydwnMon2: '{0}' vs '{1}'", x.lBuydwnMon2, y.lBuydwnMon2).AppendLine();
                retVal = false;
            }

            if (x.lBuydwnMon2_rep != y.lBuydwnMon2_rep)
            {
                sb.AppendFormat("    lBuydwnMon2_rep: '{0}' vs '{1}'", x.lBuydwnMon2_rep, y.lBuydwnMon2_rep).AppendLine();
                retVal = false;
            }

            if (x.lBuydwnMon3 != y.lBuydwnMon3)
            {
                sb.AppendFormat("    lBuydwnMon3: '{0}' vs '{1}'", x.lBuydwnMon3, y.lBuydwnMon3).AppendLine();
                retVal = false;
            }

            if (x.lBuydwnMon3_rep != y.lBuydwnMon3_rep)
            {
                sb.AppendFormat("    lBuydwnMon3_rep: '{0}' vs '{1}'", x.lBuydwnMon3_rep, y.lBuydwnMon3_rep).AppendLine();
                retVal = false;
            }

            if (x.lBuydwnMon4 != y.lBuydwnMon4)
            {
                sb.AppendFormat("    lBuydwnMon4: '{0}' vs '{1}'", x.lBuydwnMon4, y.lBuydwnMon4).AppendLine();
                retVal = false;
            }

            if (x.lBuydwnMon4_rep != y.lBuydwnMon4_rep)
            {
                sb.AppendFormat("    lBuydwnMon4_rep: '{0}' vs '{1}'", x.lBuydwnMon4_rep, y.lBuydwnMon4_rep).AppendLine();
                retVal = false;
            }

            if (x.lBuydwnMon5 != y.lBuydwnMon5)
            {
                sb.AppendFormat("    lBuydwnMon5: '{0}' vs '{1}'", x.lBuydwnMon5, y.lBuydwnMon5).AppendLine();
                retVal = false;
            }

            if (x.lBuydwnMon5_rep != y.lBuydwnMon5_rep)
            {
                sb.AppendFormat("    lBuydwnMon5_rep: '{0}' vs '{1}'", x.lBuydwnMon5_rep, y.lBuydwnMon5_rep).AppendLine();
                retVal = false;
            }

            if (x.lGradPmtYrs != y.lGradPmtYrs)
            {
                sb.AppendFormat("    lGradPmtYrs: '{0}' vs '{1}'", x.lGradPmtYrs, y.lGradPmtYrs).AppendLine();
                retVal = false;
            }

            if (x.lGradPmtYrs_rep != y.lGradPmtYrs_rep)
            {
                sb.AppendFormat("    lGradPmtYrs_rep: '{0}' vs '{1}'", x.lGradPmtYrs_rep, y.lGradPmtYrs_rep).AppendLine();
                retVal = false;
            }

            if (x.lGradPmtR != y.lGradPmtR)
            {
                sb.AppendFormat("    lGradPmtR: '{0}' vs '{1}'", x.lGradPmtR, y.lGradPmtR).AppendLine();
                retVal = false;
            }

            if (x.lGradPmtR_rep != y.lGradPmtR_rep)
            {
                sb.AppendFormat("    lGradPmtR_rep: '{0}' vs '{1}'", x.lGradPmtR_rep, y.lGradPmtR_rep).AppendLine();
                retVal = false;
            }

            if (x.lIOnlyMon != y.lIOnlyMon)
            {
                sb.AppendFormat("    lIOnlyMon: '{0}' vs '{1}'", x.lIOnlyMon, y.lIOnlyMon).AppendLine();
                retVal = false;
            }

            if (x.lIOnlyMon_rep != y.lIOnlyMon_rep)
            {
                sb.AppendFormat("    lIOnlyMon_rep: '{0}' vs '{1}'", x.lIOnlyMon_rep, y.lIOnlyMon_rep).AppendLine();
                retVal = false;
            }

            if (x.lHasVarRFeature != y.lHasVarRFeature)
            {
                sb.AppendFormat("    lHasVarRFeature: '{0}' vs '{1}'", x.lHasVarRFeature, y.lHasVarRFeature).AppendLine();
                retVal = false;
            }

            if (x.lVarRNotes != y.lVarRNotes)
            {
                sb.AppendFormat("    lVarRNotes: '{0}' vs '{1}'", x.lVarRNotes, y.lVarRNotes).AppendLine();
                retVal = false;
            }

            if (x.lAprIncludesReqDeposit != y.lAprIncludesReqDeposit)
            {
                sb.AppendFormat("    lAprIncludesReqDeposit: '{0}' vs '{1}'", x.lAprIncludesReqDeposit, y.lAprIncludesReqDeposit).AppendLine();
                retVal = false;
            }

            if (x.lHasDemandFeature != y.lHasDemandFeature)
            {
                sb.AppendFormat("    lHasDemandFeature: '{0}' vs '{1}'", x.lHasDemandFeature, y.lHasDemandFeature).AppendLine();
                retVal = false;
            }

            if (x.lLateDays != y.lLateDays)
            {
                sb.AppendFormat("    lLateDays: '{0}' vs '{1}'", x.lLateDays, y.lLateDays).AppendLine();
                retVal = false;
            }

            if (x.lLateChargePc != y.lLateChargePc)
            {
                sb.AppendFormat("    lLateChargePc: '{0}' vs '{1}'", x.lLateChargePc, y.lLateChargePc).AppendLine();
                retVal = false;
            }

            if (x.lPrepmtPenaltyT != y.lPrepmtPenaltyT)
            {
                sb.AppendFormat("    lPrepmtPenaltyT: '{0}' vs '{1}'", x.lPrepmtPenaltyT, y.lPrepmtPenaltyT).AppendLine();
                retVal = false;
            }

            if (x.lAssumeLT != y.lAssumeLT)
            {
                sb.AppendFormat("    lAssumeLT: '{0}' vs '{1}'", x.lAssumeLT, y.lAssumeLT).AppendLine();
                retVal = false;
            }

            if (x.lCcTemplateId != y.lCcTemplateId)
            {
                sb.AppendFormat("    lCcTemplateId: '{0}' vs '{1}'", x.lCcTemplateId, y.lCcTemplateId).AppendLine();
                retVal = false;
            }

            if (x.lFilingF != y.lFilingF)
            {
                sb.AppendFormat("    lFilingF: '{0}' vs '{1}'", x.lFilingF, y.lFilingF).AppendLine();
                retVal = false;
            }

            if (x.lLateChargeBaseDesc != y.lLateChargeBaseDesc)
            {
                sb.AppendFormat("    lLateChargeBaseDesc: '{0}' vs '{1}'", x.lLateChargeBaseDesc, y.lLateChargeBaseDesc).AppendLine();
                retVal = false;
            }

            if (x.lPmtAdjMaxBalPc != y.lPmtAdjMaxBalPc)
            {
                sb.AppendFormat("    lPmtAdjMaxBalPc: '{0}' vs '{1}'", x.lPmtAdjMaxBalPc, y.lPmtAdjMaxBalPc).AppendLine();
                retVal = false;
            }

            if (x.lPmtAdjMaxBalPc_rep != y.lPmtAdjMaxBalPc_rep)
            {
                sb.AppendFormat("    lPmtAdjMaxBalPc_rep: '{0}' vs '{1}'", x.lPmtAdjMaxBalPc_rep, y.lPmtAdjMaxBalPc_rep).AppendLine();
                retVal = false;
            }

            if (x.lFinMethT != y.lFinMethT)
            {
                sb.AppendFormat("    lFinMethT: '{0}' vs '{1}'", x.lFinMethT, y.lFinMethT).AppendLine();
                retVal = false;
            }

            if (x.lFinMethDesc != y.lFinMethDesc)
            {
                sb.AppendFormat("    lFinMethDesc: '{0}' vs '{1}'", x.lFinMethDesc, y.lFinMethDesc).AppendLine();
                retVal = false;
            }

            if (x.lPrepmtRefundT != y.lPrepmtRefundT)
            {
                sb.AppendFormat("    lPrepmtRefundT: '{0}' vs '{1}'", x.lPrepmtRefundT, y.lPrepmtRefundT).AppendLine();
                retVal = false;
            }

            if (x.FolderId != y.FolderId)
            {
                sb.AppendFormat("    FolderId: '{0}' vs '{1}'", x.FolderId, y.FolderId).AppendLine();
                retVal = false;
            }

            if (x.IsMaster != y.IsMaster)
            {
                sb.AppendFormat("    IsMaster: '{0}' vs '{1}'", x.IsMaster, y.IsMaster).AppendLine();
                retVal = false;
            }

            if (x.lLpeFeeMin != y.lLpeFeeMin)
            {
                sb.AppendFormat("    lLpeFeeMin: '{0}' vs '{1}'", x.lLpeFeeMin, y.lLpeFeeMin).AppendLine();
                retVal = false;
            }

            if (x.lLpeFeeMin_rep != y.lLpeFeeMin_rep)
            {
                sb.AppendFormat("    lLpeFeeMin_rep: '{0}' vs '{1}'", x.lLpeFeeMin_rep, y.lLpeFeeMin_rep).AppendLine();
                retVal = false;
            }

            if (x.lLpeFeeMax != y.lLpeFeeMax)
            {
                sb.AppendFormat("    lLpeFeeMax: '{0}' vs '{1}'", x.lLpeFeeMax, y.lLpeFeeMax).AppendLine();
                retVal = false;
            }

            if (x.lLpeFeeMax_rep != y.lLpeFeeMax_rep)
            {
                sb.AppendFormat("    lLpeFeeMax_rep: '{0}' vs '{1}'", x.lLpeFeeMax_rep, y.lLpeFeeMax_rep).AppendLine();
                retVal = false;
            }

            if (x.PairingProductIds != y.PairingProductIds)
            {
                sb.AppendFormat("    PairingProductIds: '{0}' vs '{1}'", x.PairingProductIds, y.PairingProductIds).AppendLine();
                retVal = false;
            }

            if (x.lBaseLpId != y.lBaseLpId)
            {
                sb.AppendFormat("    lBaseLpId: '{0}' vs '{1}'", x.lBaseLpId, y.lBaseLpId).AppendLine();
                retVal = false;
            }

            if (x.IsEnabled != y.IsEnabled)
            {
                sb.AppendFormat("    IsEnabled: '{0}' vs '{1}'", x.IsEnabled, y.IsEnabled).AppendLine();
                retVal = false;
            }

            if (x.lArmIndexGuid != y.lArmIndexGuid)
            {
                sb.AppendFormat("    lArmIndexGuid: '{0}' vs '{1}'", x.lArmIndexGuid, y.lArmIndexGuid).AppendLine();
                retVal = false;
            }

            if (x.lArmIndexBasedOnVstr != y.lArmIndexBasedOnVstr)
            {
                sb.AppendFormat("    lArmIndexBasedOnVstr: '{0}' vs '{1}'", x.lArmIndexBasedOnVstr, y.lArmIndexBasedOnVstr).AppendLine();
                retVal = false;
            }

            if (x.lArmIndexCanBeFoundVstr != y.lArmIndexCanBeFoundVstr)
            {
                sb.AppendFormat("    lArmIndexCanBeFoundVstr: '{0}' vs '{1}'", x.lArmIndexCanBeFoundVstr, y.lArmIndexCanBeFoundVstr).AppendLine();
                retVal = false;
            }

            if (x.lArmIndexAffectInitIRBit != y.lArmIndexAffectInitIRBit)
            {
                sb.AppendFormat("    lArmIndexAffectInitIRBit: '{0}' vs '{1}'", x.lArmIndexAffectInitIRBit, y.lArmIndexAffectInitIRBit).AppendLine();
                retVal = false;
            }

            if (x.lRAdjRoundToR != y.lRAdjRoundToR)
            {
                sb.AppendFormat("    lRAdjRoundToR: '{0}' vs '{1}'", x.lRAdjRoundToR, y.lRAdjRoundToR).AppendLine();
                retVal = false;
            }

            if (x.lArmIndexEffectiveD_rep != y.lArmIndexEffectiveD_rep)
            {
                sb.AppendFormat("    lArmIndexEffectiveD_rep: '{0}' vs '{1}'", x.lArmIndexEffectiveD_rep, y.lArmIndexEffectiveD_rep).AppendLine();
                retVal = false;
            }

            if (x.lFreddieArmIndexT != y.lFreddieArmIndexT)
            {
                sb.AppendFormat("    lFreddieArmIndexT: '{0}' vs '{1}'", x.lFreddieArmIndexT, y.lFreddieArmIndexT).AppendLine();
                retVal = false;
            }

            if (x.lArmIndexNameVstr != y.lArmIndexNameVstr)
            {
                sb.AppendFormat("    lArmIndexNameVstr: '{0}' vs '{1}'", x.lArmIndexNameVstr, y.lArmIndexNameVstr).AppendLine();
                retVal = false;
            }

            if (x.lArmIndexT != y.lArmIndexT)
            {
                sb.AppendFormat("    lArmIndexT: '{0}' vs '{1}'", x.lArmIndexT, y.lArmIndexT).AppendLine();
                retVal = false;
            }

            if (x.lArmIndexNotifyAtLeastDaysVstr != y.lArmIndexNotifyAtLeastDaysVstr)
            {
                sb.AppendFormat("    lArmIndexNotifyAtLeastDaysVstr: '{0}' vs '{1}'", x.lArmIndexNotifyAtLeastDaysVstr, y.lArmIndexNotifyAtLeastDaysVstr).AppendLine();
                retVal = false;
            }

            if (x.lArmIndexNotifyNotBeforeDaysVstr != y.lArmIndexNotifyNotBeforeDaysVstr)
            {
                sb.AppendFormat("    lArmIndexNotifyNotBeforeDaysVstr: '{0}' vs '{1}'", x.lArmIndexNotifyNotBeforeDaysVstr, y.lArmIndexNotifyNotBeforeDaysVstr).AppendLine();
                retVal = false;
            }

            if (x.lLpTemplateNmInherit != y.lLpTemplateNmInherit)
            {
                sb.AppendFormat("    lLpTemplateNmInherit: '{0}' vs '{1}'", x.lLpTemplateNmInherit, y.lLpTemplateNmInherit).AppendLine();
                retVal = false;
            }

            if (x.lLpTemplateNmOverrideBit != y.lLpTemplateNmOverrideBit)
            {
                sb.AppendFormat("    lLpTemplateNmOverrideBit: '{0}' vs '{1}'", x.lLpTemplateNmOverrideBit, y.lLpTemplateNmOverrideBit).AppendLine();
                retVal = false;
            }

            if (x.lCcTemplateIdInherit != y.lCcTemplateIdInherit)
            {
                sb.AppendFormat("    lCcTemplateIdInherit: '{0}' vs '{1}'", x.lCcTemplateIdInherit, y.lCcTemplateIdInherit).AppendLine();
                retVal = false;
            }

            if (x.lCcTemplateIdOverrideBit != y.lCcTemplateIdOverrideBit)
            {
                sb.AppendFormat("    lCcTemplateIdOverrideBit: '{0}' vs '{1}'", x.lCcTemplateIdOverrideBit, y.lCcTemplateIdOverrideBit).AppendLine();
                retVal = false;
            }

            if (x.lLockedDaysInherit != y.lLockedDaysInherit)
            {
                sb.AppendFormat("    lLockedDaysInherit: '{0}' vs '{1}'", x.lLockedDaysInherit, y.lLockedDaysInherit).AppendLine();
                retVal = false;
            }

            if (x.lLockedDaysOverrideBit != y.lLockedDaysOverrideBit)
            {
                sb.AppendFormat("    lLockedDaysOverrideBit: '{0}' vs '{1}'", x.lLockedDaysOverrideBit, y.lLockedDaysOverrideBit).AppendLine();
                retVal = false;
            }

            if (x.lLpeFeeMinInherit != y.lLpeFeeMinInherit)
            {
                sb.AppendFormat("    lLpeFeeMinInherit: '{0}' vs '{1}'", x.lLpeFeeMinInherit, y.lLpeFeeMinInherit).AppendLine();
                retVal = false;
            }

            if (x.lLpeFeeMinOverrideBit != y.lLpeFeeMinOverrideBit)
            {
                sb.AppendFormat("    lLpeFeeMinOverrideBit: '{0}' vs '{1}'", x.lLpeFeeMinOverrideBit, y.lLpeFeeMinOverrideBit).AppendLine();
                retVal = false;
            }

            if (x.lLpeFeeMaxInherit != y.lLpeFeeMaxInherit)
            {
                sb.AppendFormat("    lLpeFeeMaxInherit: '{0}' vs '{1}'", x.lLpeFeeMaxInherit, y.lLpeFeeMaxInherit).AppendLine();
                retVal = false;
            }

            if (x.lLpeFeeMaxOverrideBit != y.lLpeFeeMaxOverrideBit)
            {
                sb.AppendFormat("    lLpeFeeMaxOverrideBit: '{0}' vs '{1}'", x.lLpeFeeMaxOverrideBit, y.lLpeFeeMaxOverrideBit).AppendLine();
                retVal = false;
            }

            if (x.IsEnabledInherit != y.IsEnabledInherit)
            {
                sb.AppendFormat("    IsEnabledInherit: '{0}' vs '{1}'", x.IsEnabledInherit, y.IsEnabledInherit).AppendLine();
                retVal = false;
            }

            if (x.IsEnabledOverrideBit != y.IsEnabledOverrideBit)
            {
                sb.AppendFormat("    IsEnabledOverrideBit: '{0}' vs '{1}'", x.IsEnabledOverrideBit, y.IsEnabledOverrideBit).AppendLine();
                retVal = false;
            }

            if (x.lPpmtPenaltyMon != y.lPpmtPenaltyMon)
            {
                sb.AppendFormat("    lPpmtPenaltyMon: '{0}' vs '{1}'", x.lPpmtPenaltyMon, y.lPpmtPenaltyMon).AppendLine();
                retVal = false;
            }

            if (x.lPpmtPenaltyMonInherit != y.lPpmtPenaltyMonInherit)
            {
                sb.AppendFormat("    lPpmtPenaltyMonInherit: '{0}' vs '{1}'", x.lPpmtPenaltyMonInherit, y.lPpmtPenaltyMonInherit).AppendLine();
                retVal = false;
            }

            if (x.lPpmtPenaltyMonOverrideBit != y.lPpmtPenaltyMonOverrideBit)
            {
                sb.AppendFormat("    lPpmtPenaltyMonOverrideBit: '{0}' vs '{1}'", x.lPpmtPenaltyMonOverrideBit, y.lPpmtPenaltyMonOverrideBit).AppendLine();
                retVal = false;
            }

            if (x.lRateDelta != y.lRateDelta)
            {
                sb.AppendFormat("    lRateDelta: '{0}' vs '{1}'", x.lRateDelta, y.lRateDelta).AppendLine();
                retVal = false;
            }

            if (x.lRateDelta_rep != y.lRateDelta_rep)
            {
                sb.AppendFormat("    lRateDelta_rep: '{0}' vs '{1}'", x.lRateDelta_rep, y.lRateDelta_rep).AppendLine();
                retVal = false;
            }

            if (x.lFeeDelta != y.lFeeDelta)
            {
                sb.AppendFormat("    lFeeDelta: '{0}' vs '{1}'", x.lFeeDelta, y.lFeeDelta).AppendLine();
                retVal = false;
            }

            if (x.lFeeDelta_rep != y.lFeeDelta_rep)
            {
                sb.AppendFormat("    lFeeDelta_rep: '{0}' vs '{1}'", x.lFeeDelta_rep, y.lFeeDelta_rep).AppendLine();
                retVal = false;
            }

            if (x.lPpmtPenaltyMonLowerSearch != y.lPpmtPenaltyMonLowerSearch)
            {
                sb.AppendFormat("    lPpmtPenaltyMonLowerSearch: '{0}' vs '{1}'", x.lPpmtPenaltyMonLowerSearch, y.lPpmtPenaltyMonLowerSearch).AppendLine();
                retVal = false;
            }

            if (x.lLockedDaysLowerSearch != y.lLockedDaysLowerSearch)
            {
                sb.AppendFormat("    lLockedDaysLowerSearch: '{0}' vs '{1}'", x.lLockedDaysLowerSearch, y.lLockedDaysLowerSearch).AppendLine();
                retVal = false;
            }

            if (x.lLockedDaysLowerSearchInherit != y.lLockedDaysLowerSearchInherit)
            {
                sb.AppendFormat("    lLockedDaysLowerSearchInherit: '{0}' vs '{1}'", x.lLockedDaysLowerSearchInherit, y.lLockedDaysLowerSearchInherit).AppendLine();
                retVal = false;
            }

            if (x.lPpmtPenaltyMonLowerSearchInherit != y.lPpmtPenaltyMonLowerSearchInherit)
            {
                sb.AppendFormat("    lPpmtPenaltyMonLowerSearchInherit: '{0}' vs '{1}'", x.lPpmtPenaltyMonLowerSearchInherit, y.lPpmtPenaltyMonLowerSearchInherit).AppendLine();
                retVal = false;
            }

            if (x.PairingProductIdsInherit != y.PairingProductIdsInherit)
            {
                sb.AppendFormat("    PairingProductIdsInherit: '{0}' vs '{1}'", x.PairingProductIdsInherit, y.PairingProductIdsInherit).AppendLine();
                retVal = false;
            }

            if (x.PairingProductIdsOverrideBit != y.PairingProductIdsOverrideBit)
            {
                sb.AppendFormat("    PairingProductIdsOverrideBit: '{0}' vs '{1}'", x.PairingProductIdsOverrideBit, y.PairingProductIdsOverrideBit).AppendLine();
                retVal = false;
            }

            if (x.lLpInvestorNm != y.lLpInvestorNm)
            {
                sb.AppendFormat("    lLpInvestorNm: '{0}' vs '{1}'", x.lLpInvestorNm, y.lLpInvestorNm).AppendLine();
                retVal = false;
            }

            if (x.lLendNmInherit != y.lLendNmInherit)
            {
                sb.AppendFormat("    lLendNmInherit: '{0}' vs '{1}'", x.lLendNmInherit, y.lLendNmInherit).AppendLine();
                retVal = false;
            }

            if (x.lLendNmOverrideBit != y.lLendNmOverrideBit)
            {
                sb.AppendFormat("    lLendNmOverrideBit: '{0}' vs '{1}'", x.lLendNmOverrideBit, y.lLendNmOverrideBit).AppendLine();
                retVal = false;
            }

            if (x.lRateOptionBaseId != y.lRateOptionBaseId)
            {
                sb.AppendFormat("    lRateOptionBaseId: '{0}' vs '{1}'", x.lRateOptionBaseId, y.lRateOptionBaseId).AppendLine();
                retVal = false;
            }

            if (x.PairIdFor1stLienProdGuid != y.PairIdFor1stLienProdGuid)
            {
                sb.AppendFormat("    PairIdFor1stLienProdGuid: '{0}' vs '{1}'", x.PairIdFor1stLienProdGuid, y.PairIdFor1stLienProdGuid).AppendLine();
                retVal = false;
            }

            if (x.PairIdFor1stLienProdGuidInherit != y.PairIdFor1stLienProdGuidInherit)
            {
                sb.AppendFormat("    PairIdFor1stLienProdGuidInherit: '{0}' vs '{1}'", x.PairIdFor1stLienProdGuidInherit, y.PairIdFor1stLienProdGuidInherit).AppendLine();
                retVal = false;
            }

            if (x.lLpeFeeMinParam != y.lLpeFeeMinParam)
            {
                sb.AppendFormat("    lLpeFeeMinParam: '{0}' vs '{1}'", x.lLpeFeeMinParam, y.lLpeFeeMinParam).AppendLine();
                retVal = false;
            }

            if (x.lLpeFeeMinAddRuleAdjInheritBit != y.lLpeFeeMinAddRuleAdjInheritBit)
            {
                sb.AppendFormat("    lLpeFeeMinAddRuleAdjInheritBit: '{0}' vs '{1}'", x.lLpeFeeMinAddRuleAdjInheritBit, y.lLpeFeeMinAddRuleAdjInheritBit).AppendLine();
                retVal = false;
            }

            if (x.IsLpe != y.IsLpe)
            {
                sb.AppendFormat("    IsLpe: '{0}' vs '{1}'", x.IsLpe, y.IsLpe).AppendLine();
                retVal = false;
            }

            if (x.lIsArmMarginDisplayed != y.lIsArmMarginDisplayed)
            {
                sb.AppendFormat("    lIsArmMarginDisplayed: '{0}' vs '{1}'", x.lIsArmMarginDisplayed, y.lIsArmMarginDisplayed).AppendLine();
                retVal = false;
            }

            if (x.ProductCode != y.ProductCode)
            {
                sb.AppendFormat("    ProductCode: '{0}' vs '{1}'", x.ProductCode, y.ProductCode).AppendLine();
                retVal = false;
            }

            if (x.ProductIdentifier != y.ProductIdentifier)
            {
                sb.AppendFormat("    ProductIdentifier: '{0}' vs '{1}'", x.ProductIdentifier, y.ProductIdentifier).AppendLine();
                retVal = false;
            }

            if (x.IsOptionArm != y.IsOptionArm)
            {
                sb.AppendFormat("    IsOptionArm: '{0}' vs '{1}'", x.IsOptionArm, y.IsOptionArm).AppendLine();
                retVal = false;
            }

            if (x.lRateDeltaInherit != y.lRateDeltaInherit)
            {
                sb.AppendFormat("    lRateDeltaInherit: '{0}' vs '{1}'", x.lRateDeltaInherit, y.lRateDeltaInherit).AppendLine();
                retVal = false;
            }

            if (x.lFeeDeltaInherit != y.lFeeDeltaInherit)
            {
                sb.AppendFormat("    lFeeDeltaInherit: '{0}' vs '{1}'", x.lFeeDeltaInherit, y.lFeeDeltaInherit).AppendLine();
                retVal = false;
            }

            if (x.SrcRateOptionsProgIdInherit != y.SrcRateOptionsProgIdInherit)
            {
                sb.AppendFormat("    SrcRateOptionsProgIdInherit: '{0}' vs '{1}'", x.SrcRateOptionsProgIdInherit, y.SrcRateOptionsProgIdInherit).AppendLine();
                retVal = false;
            }

            if (x.SrcRateOptionsProgId != y.SrcRateOptionsProgId)
            {
                sb.AppendFormat("    SrcRateOptionsProgId: '{0}' vs '{1}'", x.SrcRateOptionsProgId, y.SrcRateOptionsProgId).AppendLine();
                retVal = false;
            }

            if (x.lLpDPmtT != y.lLpDPmtT)
            {
                sb.AppendFormat("    lLpDPmtT: '{0}' vs '{1}'", x.lLpDPmtT, y.lLpDPmtT).AppendLine();
                retVal = false;
            }

            if (x.lLpQPmtT != y.lLpQPmtT)
            {
                sb.AppendFormat("    lLpQPmtT: '{0}' vs '{1}'", x.lLpQPmtT, y.lLpQPmtT).AppendLine();
                retVal = false;
            }

            if (x.lHasQRateInRateOptions != y.lHasQRateInRateOptions)
            {
                sb.AppendFormat("    lHasQRateInRateOptions: '{0}' vs '{1}'", x.lHasQRateInRateOptions, y.lHasQRateInRateOptions).AppendLine();
                retVal = false;
            }

            if (x.lQualRateCalculationT != y.lQualRateCalculationT)
            {
                sb.AppendFormat("    lQualRateCalculationT: '{0}' vs '{1}'", x.lQualRateCalculationT, y.lQualRateCalculationT).AppendLine();
                retVal = false;
            }

            if (x.lQualRateCalculationFieldT1 != y.lQualRateCalculationFieldT1)
            {
                sb.AppendFormat("    lQualRateCalculationFieldT1: '{0}' vs '{1}'", x.lQualRateCalculationFieldT1, y.lQualRateCalculationFieldT1).AppendLine();
                retVal = false;
            }

            if (x.lQualRateCalculationFieldT2 != y.lQualRateCalculationFieldT2)
            {
                sb.AppendFormat("    lQualRateCalculationFieldT2: '{0}' vs '{1}'", x.lQualRateCalculationFieldT2, y.lQualRateCalculationFieldT2).AppendLine();
                retVal = false;
            }

            if (x.lQualRateCalculationAdjustment1 != y.lQualRateCalculationAdjustment1)
            {
                sb.AppendFormat("    lQualRateCalculationAdjustment1: '{0}' vs '{1}'", x.lQualRateCalculationAdjustment1, y.lQualRateCalculationAdjustment1).AppendLine();
                retVal = false;
            }

            if (x.lQualRateCalculationAdjustment2 != y.lQualRateCalculationAdjustment2)
            {
                sb.AppendFormat("    lQualRateCalculationAdjustment2: '{0}' vs '{1}'", x.lQualRateCalculationAdjustment2, y.lQualRateCalculationAdjustment2).AppendLine();
                retVal = false;
            }

            if (x.lQualTerm != y.lQualTerm)
            {
                sb.AppendFormat("    lQualTerm: '{0}' vs '{1}'", x.lQualTerm, y.lQualTerm).AppendLine();
                retVal = false;
            }

            if (x.lQualTermCalculationType != y.lQualTermCalculationType)
            {
                sb.AppendFormat("    lQualTermCalculationType: '{0}' vs '{1}'", x.lQualTermCalculationType, y.lQualTermCalculationType).AppendLine();
                retVal = false;
            }

            if (x.IsLpeDummyProgram != y.IsLpeDummyProgram)
            {
                sb.AppendFormat("    IsLpeDummyProgram: '{0}' vs '{1}'", x.IsLpeDummyProgram, y.IsLpeDummyProgram).AppendLine();
                retVal = false;
            }

            if (x.IsPairedOnlyWithSameInvestor != y.IsPairedOnlyWithSameInvestor)
            {
                sb.AppendFormat("    IsPairedOnlyWithSameInvestor: '{0}' vs '{1}'", x.IsPairedOnlyWithSameInvestor, y.IsPairedOnlyWithSameInvestor).AppendLine();
                retVal = false;
            }

            if (x.lIOnlyMonLowerSearch != y.lIOnlyMonLowerSearch)
            {
                sb.AppendFormat("    lIOnlyMonLowerSearch: '{0}' vs '{1}'", x.lIOnlyMonLowerSearch, y.lIOnlyMonLowerSearch).AppendLine();
                retVal = false;
            }

            if (x.lIOnlyMonLowerSearchInherit != y.lIOnlyMonLowerSearchInherit)
            {
                sb.AppendFormat("    lIOnlyMonLowerSearchInherit: '{0}' vs '{1}'", x.lIOnlyMonLowerSearchInherit, y.lIOnlyMonLowerSearchInherit).AppendLine();
                retVal = false;
            }

            if (x.lIOnlyMonLowerSearchOverrideBit != y.lIOnlyMonLowerSearchOverrideBit)
            {
                sb.AppendFormat("    lIOnlyMonLowerSearchOverrideBit: '{0}' vs '{1}'", x.lIOnlyMonLowerSearchOverrideBit, y.lIOnlyMonLowerSearchOverrideBit).AppendLine();
                retVal = false;
            }

            if (x.lIOnlyMonUpperSearch != y.lIOnlyMonUpperSearch)
            {
                sb.AppendFormat("    lIOnlyMonUpperSearch: '{0}' vs '{1}'", x.lIOnlyMonUpperSearch, y.lIOnlyMonUpperSearch).AppendLine();
                retVal = false;
            }

            if (x.lIOnlyMonUpperSearchInherit != y.lIOnlyMonUpperSearchInherit)
            {
                sb.AppendFormat("    lIOnlyMonUpperSearchInherit: '{0}' vs '{1}'", x.lIOnlyMonUpperSearchInherit, y.lIOnlyMonUpperSearchInherit).AppendLine();
                retVal = false;
            }

            if (x.lIOnlyMonUpperSearchOverrideBit != y.lIOnlyMonUpperSearchOverrideBit)
            {
                sb.AppendFormat("    lIOnlyMonUpperSearchOverrideBit: '{0}' vs '{1}'", x.lIOnlyMonUpperSearchOverrideBit, y.lIOnlyMonUpperSearchOverrideBit).AppendLine();
                retVal = false;
            }

            if (x.CanBeStandAlone2nd != y.CanBeStandAlone2nd)
            {
                sb.AppendFormat("    CanBeStandAlone2nd: '{0}' vs '{1}'", x.CanBeStandAlone2nd, y.CanBeStandAlone2nd).AppendLine();
                retVal = false;
            }

            if (x.lDtiUsingMaxBalPc != y.lDtiUsingMaxBalPc)
            {
                sb.AppendFormat("    lDtiUsingMaxBalPc: '{0}' vs '{1}'", x.lDtiUsingMaxBalPc, y.lDtiUsingMaxBalPc).AppendLine();
                retVal = false;
            }

            if (x.IsMasterPriceGroup != y.IsMasterPriceGroup)
            {
                sb.AppendFormat("    IsMasterPriceGroup: '{0}' vs '{1}'", x.IsMasterPriceGroup, y.IsMasterPriceGroup).AppendLine();
                retVal = false;
            }

            if (x.lLpProductType != y.lLpProductType)
            {
                sb.AppendFormat("    lLpProductType: '{0}' vs '{1}'", x.lLpProductType, y.lLpProductType).AppendLine();
                retVal = false;
            }

            if (x.lRAdjFloorBaseT != y.lRAdjFloorBaseT)
            {
                sb.AppendFormat("    lRAdjFloorBaseT: '{0}' vs '{1}'", x.lRAdjFloorBaseT, y.lRAdjFloorBaseT).AppendLine();
                retVal = false;
            }

            if (x.IsConvertibleMortgage != y.IsConvertibleMortgage)
            {
                sb.AppendFormat("    IsConvertibleMortgage: '{0}' vs '{1}'", x.IsConvertibleMortgage, y.IsConvertibleMortgage).AppendLine();
                retVal = false;
            }

            if (x.lLpmiSupportedOutsidePmi != y.lLpmiSupportedOutsidePmi)
            {
                sb.AppendFormat("    lLpmiSupportedOutsidePmi: '{0}' vs '{1}'", x.lLpmiSupportedOutsidePmi, y.lLpmiSupportedOutsidePmi).AppendLine();
                retVal = false;
            }

            if (x.lWholesaleChannelProgram != y.lWholesaleChannelProgram)
            {
                sb.AppendFormat("    lWholesaleChannelProgram: '{0}' vs '{1}'", x.lWholesaleChannelProgram, y.lWholesaleChannelProgram).AppendLine();
                retVal = false;
            }

            if (x.lHardPrepmtPeriodMonths != y.lHardPrepmtPeriodMonths)
            {
                sb.AppendFormat("    lHardPrepmtPeriodMonths: '{0}' vs '{1}'", x.lHardPrepmtPeriodMonths, y.lHardPrepmtPeriodMonths).AppendLine();
                retVal = false;
            }

            if (x.lHardPrepmtPeriodMonths_rep != y.lHardPrepmtPeriodMonths_rep)
            {
                sb.AppendFormat("    lHardPrepmtPeriodMonths_rep: '{0}' vs '{1}'", x.lHardPrepmtPeriodMonths_rep, y.lHardPrepmtPeriodMonths_rep).AppendLine();
                retVal = false;
            }

            if (x.lSoftPrepmtPeriodMonths != y.lSoftPrepmtPeriodMonths)
            {
                sb.AppendFormat("    lSoftPrepmtPeriodMonths: '{0}' vs '{1}'", x.lSoftPrepmtPeriodMonths, y.lSoftPrepmtPeriodMonths).AppendLine();
                retVal = false;
            }

            if (x.lSoftPrepmtPeriodMonths_rep != y.lSoftPrepmtPeriodMonths_rep)
            {
                sb.AppendFormat("    lSoftPrepmtPeriodMonths_rep: '{0}' vs '{1}'", x.lSoftPrepmtPeriodMonths_rep, y.lSoftPrepmtPeriodMonths_rep).AppendLine();
                retVal = false;
            }

            if (x.lHelocQualPmtBaseT != y.lHelocQualPmtBaseT)
            {
                sb.AppendFormat("    lHelocQualPmtBaseT: '{0}' vs '{1}'", x.lHelocQualPmtBaseT, y.lHelocQualPmtBaseT).AppendLine();
                retVal = false;
            }

            if (x.lHelocQualPmtFormulaT != y.lHelocQualPmtFormulaT)
            {
                sb.AppendFormat("    lHelocQualPmtBaseT: '{0}' vs '{1}'", x.lHelocQualPmtFormulaT, y.lHelocQualPmtFormulaT).AppendLine();
                retVal = false;
            }

            if (x.lHelocQualPmtFormulaRateT != y.lHelocQualPmtFormulaRateT)
            {
                sb.AppendFormat("    lHelocQualPmtFormulaRateT: '{0}' vs '{1}'", x.lHelocQualPmtFormulaRateT, y.lHelocQualPmtFormulaRateT).AppendLine();
                retVal = false;
            }

            if (x.lHelocQualPmtAmortTermT != y.lHelocQualPmtAmortTermT)
            {
                sb.AppendFormat("    lHelocQualPmtAmortTermT: '{0}' vs '{1}'", x.lHelocQualPmtAmortTermT, y.lHelocQualPmtAmortTermT).AppendLine();
                retVal = false;
            }

            if (x.lHelocQualPmtPcBaseT != y.lHelocQualPmtPcBaseT)
            {
                sb.AppendFormat("    lHelocQualPmtPcBaseT: '{0}' vs '{1}'", x.lHelocQualPmtPcBaseT, y.lHelocQualPmtPcBaseT).AppendLine();
                retVal = false;
            }

            if (x.lHelocQualPmtMb != y.lHelocQualPmtMb)
            {
                sb.AppendFormat("    lHelocQualPmtMb: '{0}' vs '{1}'", x.lHelocQualPmtMb, y.lHelocQualPmtMb).AppendLine();
                retVal = false;
            }

            if (x.lHelocQualPmtMb_rep != y.lHelocQualPmtMb_rep)
            {
                sb.AppendFormat("    lHelocQualPmtMb_rep: '{0}' vs '{1}'", x.lHelocQualPmtMb_rep, y.lHelocQualPmtMb_rep).AppendLine();
                retVal = false;
            }

            if (x.lHelocPmtBaseT != y.lHelocPmtBaseT)
            {
                sb.AppendFormat("    lHelocPmtBaseT: '{0}' vs '{1}'", x.lHelocPmtBaseT, y.lHelocPmtBaseT).AppendLine();
                retVal = false;
            }

            if (x.lHelocPmtFormulaT != y.lHelocPmtFormulaT)
            {
                sb.AppendFormat("    lHelocPmtFormulaT: '{0}' vs '{1}'", x.lHelocPmtFormulaT, y.lHelocPmtFormulaT).AppendLine();
                retVal = false;
            }

            if (x.lHelocPmtFormulaRateT != y.lHelocPmtFormulaRateT)
            {
                sb.AppendFormat("    lHelocPmtFormulaRateT: '{0}' vs '{1}'", x.lHelocPmtFormulaRateT, y.lHelocPmtFormulaRateT).AppendLine();
                retVal = false;
            }

            if (x.lHelocPmtAmortTermT != y.lHelocPmtAmortTermT)
            {
                sb.AppendFormat("    lHelocPmtAmortTermT: '{0}' vs '{1}'", x.lHelocPmtAmortTermT, y.lHelocPmtAmortTermT).AppendLine();
                retVal = false;
            }

            if (x.lHelocPmtPcBaseT != y.lHelocPmtPcBaseT)
            {
                sb.AppendFormat("    lHelocPmtPcBaseT: '{0}' vs '{1}'", x.lHelocPmtPcBaseT, y.lHelocPmtPcBaseT).AppendLine();
                retVal = false;
            }

            if (x.lHelocPmtMb != y.lHelocPmtMb)
            {
                sb.AppendFormat("    lHelocPmtMb: '{0}' vs '{1}'", x.lHelocPmtMb, y.lHelocPmtMb).AppendLine();
                retVal = false;
            }

            if (x.lHelocPmtMb_rep != y.lHelocPmtMb_rep)
            {
                sb.AppendFormat("    lHelocPmtMb_rep: '{0}' vs '{1}'", x.lHelocPmtMb_rep, y.lHelocPmtMb_rep).AppendLine();
                retVal = false;
            }

            if (x.lHelocDraw != y.lHelocDraw)
            {
                sb.AppendFormat("    lHelocDraw: '{0}' vs '{1}'", x.lHelocDraw, y.lHelocDraw).AppendLine();
                retVal = false;
            }

            if (x.lHelocDraw_rep != y.lHelocDraw_rep)
            {
                sb.AppendFormat("    lHelocDraw_rep: '{0}' vs '{1}'", x.lHelocDraw_rep, y.lHelocDraw_rep).AppendLine();
                retVal = false;
            }

            if (x.IsHeloc != y.IsHeloc)
            {
                sb.AppendFormat("    IsHeloc: '{0}' vs '{1}'", x.IsHeloc, y.IsHeloc).AppendLine();
                retVal = false;
            }

            if (x.lLpCustomCode1 != y.lLpCustomCode1)
            {
                sb.AppendFormat("    lLpCustomCode1: '{0}' vs '{1}'", x.lLpCustomCode1, y.lLpCustomCode1).AppendLine();
                retVal = false;
            }

            if (x.lLpCustomCode2 != y.lLpCustomCode2)
            {
                sb.AppendFormat("    lLpCustomCode2: '{0}' vs '{1}'", x.lLpCustomCode2, y.lLpCustomCode2).AppendLine();
                retVal = false;
            }

            if (x.lLpCustomCode3 != y.lLpCustomCode3)
            {
                sb.AppendFormat("    lLpCustomCode3: '{0}' vs '{1}'", x.lLpCustomCode3, y.lLpCustomCode3).AppendLine();
                retVal = false;
            }

            if (x.lLpCustomCode4 != y.lLpCustomCode4)
            {
                sb.AppendFormat("    lLpCustomCode4: '{0}' vs '{1}'", x.lLpCustomCode4, y.lLpCustomCode4).AppendLine();
                retVal = false;
            }

            if (x.lLpCustomCode5 != y.lLpCustomCode5)
            {
                sb.AppendFormat("    lLpCustomCode5: '{0}' vs '{1}'", x.lLpCustomCode5, y.lLpCustomCode5).AppendLine();
                retVal = false;
            }

            if (x.lLpInvestorCode1 != y.lLpInvestorCode1)
            {
                sb.AppendFormat("    lLpInvestorCode1: '{0}' vs '{1}'", x.lLpInvestorCode1, y.lLpInvestorCode1).AppendLine();
                retVal = false;
            }

            if (x.lLpInvestorCode2 != y.lLpInvestorCode2)
            {
                sb.AppendFormat("    lLpInvestorCode2: '{0}' vs '{1}'", x.lLpInvestorCode2, y.lLpInvestorCode2).AppendLine();
                retVal = false;
            }

            if (x.lLpInvestorCode3 != y.lLpInvestorCode3)
            {
                sb.AppendFormat("    lLpInvestorCode3: '{0}' vs '{1}'", x.lLpInvestorCode3, y.lLpInvestorCode3).AppendLine();
                retVal = false;
            }

            if (x.lLpInvestorCode4 != y.lLpInvestorCode4)
            {
                sb.AppendFormat("    lLpInvestorCode4: '{0}' vs '{1}'", x.lLpInvestorCode4, y.lLpInvestorCode4).AppendLine();
                retVal = false;
            }

            if (x.lLpInvestorCode5 != y.lLpInvestorCode5)
            {
                sb.AppendFormat("    lLpInvestorCode5: '{0}' vs '{1}'", x.lLpInvestorCode5, y.lLpInvestorCode5).AppendLine();
                retVal = false;
            }

            if (x.lPrepmtPeriod_rep != y.lPrepmtPeriod_rep)
            {
                sb.AppendFormat("    lPrepmtPeriod_rep: '{0}' vs '{1}'", x.lPrepmtPeriod_rep, y.lPrepmtPeriod_rep).AppendLine();
                retVal = false;
            }

            if (x.lNoteR != y.lNoteR)
            {
                sb.AppendFormat("    lNoteR: '{0}' vs '{1}'", x.lNoteR, y.lNoteR).AppendLine();
                retVal = false;
            }

            if (x.lLOrigFPc != y.lLOrigFPc)
            {
                sb.AppendFormat("    lLOrigFPc: '{0}' vs '{1}'", x.lLOrigFPc, y.lLOrigFPc).AppendLine();
                retVal = false;
            }

            if (x.lHelocRepay_rep != y.lHelocRepay_rep)
            {
                sb.AppendFormat("    lHelocRepay_rep: '{0}' vs '{1}'", x.lHelocRepay_rep, y.lHelocRepay_rep).AppendLine();
                retVal = false;
            }

            if (x.lRateSheetDownloadEndD != y.lRateSheetDownloadEndD)
            {
                sb.AppendFormat("    lRateSheetDownloadEndD: '{0}' vs '{1}'", x.lRateSheetDownloadEndD, y.lRateSheetDownloadEndD).AppendLine();
                retVal = false;
            }

            if (x.lRateSheetDownloadStartD != y.lRateSheetDownloadStartD)
            {
                sb.AppendFormat("    lRateSheetDownloadStartD: '{0}' vs '{1}'", x.lRateSheetDownloadStartD, y.lRateSheetDownloadStartD).AppendLine();
                retVal = false;
            }

            if (x.HasCcTemplate != y.HasCcTemplate)
            {
                sb.AppendFormat("    HasCcTemplate: '{0}' vs '{1}'", x.HasCcTemplate, y.HasCcTemplate).AppendLine();
                retVal = false;
            }

            if (x.lLpeFeeMinAddRuleAdjBit != y.lLpeFeeMinAddRuleAdjBit)
            {
                sb.AppendFormat("    lLpeFeeMinAddRuleAdjBit: '{0}' vs '{1}'", x.lLpeFeeMinAddRuleAdjBit, y.lLpeFeeMinAddRuleAdjBit).AppendLine();
                retVal = false;
            }

            if (x.LpeAcceptableRsFileId != y.LpeAcceptableRsFileId)
            {
                sb.AppendFormat("    LpeAcceptableRsFileId: '{0}' vs '{1}'", x.LpeAcceptableRsFileId, y.LpeAcceptableRsFileId).AppendLine();
                retVal = false;
            }


            errMsg = sb.ToString();
            return retVal;
        }
        #endregion
    }
}
