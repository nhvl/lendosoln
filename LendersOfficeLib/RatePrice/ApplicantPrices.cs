namespace LendersOfficeApp.los.RatePrice
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.CreditReport;
    using LendersOffice.ManualInvestorProductExpiration;
    using LendersOffice.ObjLib.Extensions;
    using LendersOffice.ObjLib.Loan;
    using LendersOffice.ObjLib.LockPolicies;
    using LendersOffice.ObjLib.PriceGroups;
    using LendersOffice.RatePrice;
    using LendersOffice.RatePrice.FileBasedPricing;
    using LendersOffice.RatePrice.Helpers;
    using LendersOffice.Security;
    using LendersOfficeApp.ObjLib.RatesheetExpiration;
    using LqbGrammar.DataTypes;
    using Toolbox;
    using MyCondition = LendersOfficeApp.los.RatePrice.RtCondition;
    using MyConsequence = LendersOfficeApp.los.RatePrice.RtConsequence;
    using MyPricePolicy = LendersOfficeApp.los.RatePrice.AbstractRtPricePolicy;
    using MyRule = LendersOfficeApp.los.RatePrice.AbstractRtRule;
    using MyRuleList = LendersOfficeApp.los.RatePrice.RtRuleList;
    using static DataAccess.FeeService.FeeServiceApplication;
    using LendersOffice.RatePrice.CustomRatesheet;

    public enum E_EvalStatus
    {
        Eval_Eligible = 0,
        Eval_Ineligible = 1,
        Eval_InsufficientInfo = 2,
        Eval_HideFromResult = 3
    }


    public abstract class CApplicantPrice : ILoanProgramTemplate
    {
        #region STATIC
        private static readonly LqbSingleThreadInitializeLazy<Dictionary<string, string>> s_errMsgMap;
        static CApplicantPrice()
        {
            s_errMsgMap = new LqbSingleThreadInitializeLazy<Dictionary<string, string>>(RetrieveLpeErrorMessageMap);
        }

        static public string GetUserMsg(string origMsg)
        {
            string errorMessage;
            if (!s_errMsgMap.Value.TryGetValue(origMsg, out errorMessage))
            {
                errorMessage = origMsg;
            }

            return errorMessage;
        }

        private static Dictionary<string, string> RetrieveLpeErrorMessageMap()
        {
            var map = new Dictionary<string, string>();
            using (var r = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "ListLpeUserErrMsgMapping"))
            {
                while (r.Read())
                {
                    map.Add(r["OrigMsg"].ToString(), r["UserMsg"].ToString());
                }
            }

            return map;
        }
        #endregion // STATIC

        public static string HIDE_STIP_PREFIX_STR = "HIDE::";
        public static string MISC_CATEGORY_STR = "MISC";
        public static string HIDE_PREFIX_STR = "HIDE";
        public static string BORROWER_NAME_ALIAS = "<BorrowerName>";
        public static string LEGACY_BORR_NAME_REPLACEMENT = "BORROWER";
        public static string DISABLE_LOCK_PREFIX_STR = "DISABLELOCK";

        public static CApplicantPrice CreateApplicantPrice(
            CPageData loanFileData,
            ILoanProgramTemplate loanProgramTemplate,
            AbstractLoanProgramSet loanProgramSet,
            CLpRunOptions options,
            CacheInvariantPolicyResults cachResult)
        {
            if (loanProgramSet.GetType() == typeof(LoanProgramSet))
            {
                return CApplicantPriceOLD.Create(loanFileData, loanProgramTemplate, loanProgramSet,
                    options, cachResult);
            }
            else if (loanProgramSet.GetType() == typeof(FileBasedLoanProgramSet))
            {
                return ApplicantPriceFileBased.Create(loanFileData, loanProgramTemplate,
                    loanProgramSet, options, cachResult);
            }
            else if (loanProgramSet.GetType() == typeof(HybridLoanProgramSet))
            {
                return ApplicantPriceFileBased.Create(loanFileData, loanProgramTemplate,
                   loanProgramSet, options, cachResult);

            }
            else
            {
                throw new NotSupportedException(loanProgramSet.GetType() + " is not support");
            }
        }

        private CPageData x_loanFileData;
        protected CPageData m_loanFileData
        {
            get { return x_loanFileData; }
        }

        protected ILoanProgramTemplate CurrentProgramForHistoricalPricing { get; set; }

        private const string NoRateOptionMatch = "THIS LOAN PROGRAM DOES NOT HAVE A RATE OPTION MATCHING THE NOTE RATE";

        private const string MissingLoanOfficerLicenseStip = "THE LOAN OFFICER MAY NOT HAVE A VALID LICENSE FOR THE SUBJECT PROPERTY STATE. PLEASE VERIFY.";
        private const string MissingOriginationLicenseStip = "THE ORIGINATING COMPANY MAY NOT HAVE A VALID LICENSE FOR THE SUBJECT PROPERTY STATE. PLEASE VERIFY.";

        public const int InvalidFeeServiceRevisionId = -1;

        private const int RateDecimalDigits = 3;

        private E_EvalStatus m_evalStatus;
        private decimal m_marginBaseAllROptions;
        private bool m_isMarginBaseAllRateOptions = false;// true if entire product will get a uniform base margin (that is for all rate options)
        private decimal m_marginDeltaDisclosed;
        private decimal m_marginDeltaHidden;
        private decimal m_feeDeltaDisclosed;
        private decimal m_feeDeltaHidden;
        private decimal m_rateDeltaDisclosed;
        private decimal m_rateDeltaHidden;
        private decimal m_teaserRDeltaDisclosed;
        private decimal m_teaserRDeltaHidden;
        private decimal m_qualRDeltaDisclosed;
        private decimal m_qualRDeltaHidden;

        private CSortedListOfGroups m_stipCollection;
        private CSortedListOfGroups m_hiddenStipCollection;
        private List<CStipulation> m_reasonArray;
        private string m_disqualifiedRules;
        private List<CAdjustItem> m_adjDescsDisclosed;
        private List<CAdjustItem> m_adjDescsHidden;
        private Hashtable m_errors;
        private Hashtable m_developerErrors;
        private CApplicantRateOption[] m_applicantRateOptions;
        private CApplicantRateOption m_repRateOption;
        private PmiManager m_pmiManager;
        private IPolicyRelationDb m_parentInfo;
        private LosConvert m_losConvert;
        private bool m_bMinMaxFeeViolation = false;
        private CLpRunOptions m_runOptions;
        private decimal m_maxDti = 0;
        private decimal? m_maxFrontEndYsp = null; // OPM 43151
        private decimal m_originalBackEndMaxYsp = 0; // For debugging
        private bool m_isBlockedRateLockSubmission = false;
        private bool m_areRatesExpired = false; // 20882
        private string m_rateLockSubmissionUserWarningMessage = "";
        private string m_rateLockSubmissionDevWarningMessage = "";
        private LoanApplicationList m_applications;
        private int m_qScore = 0;
        private decimal m_rateRounding = 0;
        private decimal m_feeRounding = 0;
        private decimal m_frontEndSnapDown = 0;
        private decimal m_backEndSnapDown = 0;
        private string m_mortgageInsuranceProvider = "";
        private string m_mortgageInsuranceDesc = ""; // OPM 26013
        private string m_mortgageInsuranceSinglePmt = "";
        private string m_mortgageInsuranceMonthlyPmt = "";
        private string m_mortgageInsuranceSingleRate = "";
        private string m_mortgageInsuranceMonthlyRate = "";
        private bool m_PmiDisqual = false;
        private string m_lockDisabledReason = "";
        private PricingSummary m_debugSummary;

        private string m_qbcDisqualRule = string.Empty; // OPM 62684

        private decimal m_PhaseZeroLockDaysAdj = 0; // OPM 229841

        private RateVariantPmi m_RateVariantPmi;

        public int lLockedDaysNeededByLender
        {
            get { return m_runOptions.LockPeriodAdj + (int)m_PhaseZeroLockDaysAdj; }
        }

        public bool IsTotalOrH4HSpecialProgram
        {
            get { return lLpTemplateId == ConstStage.TotalScorecardLoanProgramId || lLpTemplateId == ConstStage.H4HLoanProgramId; }
        }
        protected CApplicantPrice(CPageData loanFileData,
            ILoanProgramTemplate loanProgramTemplate,
            AbstractLoanProgramSet loanProgramSet,
            CLpRunOptions options

            )
        {

            m_losConvert = new LosConvert();
            x_loanFileData = loanFileData;
            x_loanFileData.ByPassFieldSecurityCheck = true;
            m_pmiManager = loanProgramSet.PmiManager;
            m_loanProgramTemplate = loanProgramTemplate;
            m_RateVariantPmi = RateVariantPmi.Create(options);

            this.LoanProgramSet = loanProgramSet;

            m_parentInfo = loanProgramSet.PolicyRelation;
            m_runOptions = options;
            m_stipCollection = new Toolbox.CSortedListOfGroups(new CStipulationCategoryComparer(), 20);
            m_hiddenStipCollection = new Toolbox.CSortedListOfGroups(new CStipulationCategoryComparer(), 20);
            m_reasonArray = new List<CStipulation>(20);

            m_applications = new LoanApplicationList(loanFileData);
            IsDuRefiFilterEnabled = loanFileData.sProdIsDuRefiPlus;
            m_loanFileData.sPricingModeT = options.PricingModeT;
        }

        private void PrepareInsufficientInfoStatus(Exception exc, string regTestMsg, Guid ruleId, Guid policyId)
        {
            CBaseException baseExc = exc as CBaseException;

            string userMsg = "";
            string devMsg = "";

            if (null != baseExc)
            {
                userMsg = baseExc.UserMessage;
                devMsg = baseExc.DeveloperMessage;
            }
            else
            {
                userMsg = exc.Message;
                devMsg = exc.Message;
            }
            if (!m_errors.Contains(userMsg))
            {
                Tools.LogRegTest(regTestMsg);
                m_errors.Add(userMsg, null);

                string msg = string.Format("{0}. RuleId={1}, PricePolicyId={2}", devMsg, ruleId, policyId);
                if (!m_developerErrors.Contains(msg))
                {
                    m_developerErrors.Add(msg, null);
                    LogDebug("    => DevErrMsg: " + msg);
                }
            }
            m_evalStatus = E_EvalStatus.Eval_InsufficientInfo;

        }


        private void PrepareInsufficientInfoStatus(string logMsg, string errorMsg, string errorMsg4Developer)
        {
            if (!m_errors.Contains(errorMsg))
            {
                Tools.LogRegTest(logMsg);
                m_errors.Add(errorMsg, null);

                if (!m_developerErrors.Contains(errorMsg4Developer))
                {
                    m_developerErrors.Add(errorMsg4Developer, null);
                    LogDebug("    => DevErrMsg: " + errorMsg4Developer);
                }
            }
            m_evalStatus = E_EvalStatus.Eval_InsufficientInfo;

        }

        public enum DtiFilterEnum
        {
            AllowDtiOnly,
            AllowNonDtiOnly,
            AllowAll

        }

        const int EvalStackSize = 400;

        public string FriendlyInfo { get { return LoanProgramSet.FriendlyInfo; } }

        private StringBuilder m_debugInfo = new StringBuilder();
        private StringBuilder m_logInfo = new StringBuilder();
        private Stopwatch m_debugStopwatch = Stopwatch.StartNew();

        public string DebugInfo
        {
            get
            {
                // Thien notes: I expect the following code will work
                //      return IsLogDetailDebugInfo ? m_debugInfo.ToString() : m_logInfo.ToString();
                // However for safe and backward compatible, I use the following code.

                return (!ConstSite.EnableFilebasedCmp || IsLogDetailDebugInfo) ? m_debugInfo.ToString() : m_logInfo.ToString();
            }
        }

        public string InternalDebugInfo // at this time, only use when ConstSite.EnableFilebasedCmp = true
        {
            get { return m_debugInfo.ToString(); }
        }


        private bool? x_isLogDetailDebugInfo;
        private bool IsLogDetailDebugInfo
        {
            get
            {
                if (x_isLogDetailDebugInfo.HasValue == false)
                {
                    AbstractUserPrincipal currentPrincipal = PrincipalFactory.CurrentPrincipal;
                    // 2014-02-13 - dd - Allow user with internal admin to see detail debugging information.
                    //    This will help SAE to investigate pricing issue with CMG hosting solution.
                    // 4/17/2014 dd - OPM 173155 - When we are hosting the site, only display debug info
                    //                             on LOAUTH.
                    if (ConstStage.IsLendingQBHostingServer)
                    {
                        x_isLogDetailDebugInfo = ConstSite.DisplayPolicyAndRuleIdOnLpeResult;
            }
                    else
                    {
                        // 4/17/2014 dd - For CMG hosting
                        x_isLogDetailDebugInfo = ConstSite.DisplayPolicyAndRuleIdOnLpeResult || (currentPrincipal != null && currentPrincipal.HasPermission(Permission.CanModifyLoanPrograms));
        }
                }
                return x_isLogDetailDebugInfo.Value;

            }
        }

        // if( IsLogDetailDebugInfo = true) 
        //    generate debug log and potential show debug info to client using m_adjDescsHidden.Add(new CAdjustItem(dtiExplain));.
        // else if( IsLogDebugInfo )
        //    generate debug log but don't show debug info to client. Moreover the debug log is used in internal.
        private bool IsLogDebugInfo
        {
            get
            {
                return IsLogDetailDebugInfo ? true : ConstSite.EnableFilebasedCmp;
            }
        }

        private void LogDebug(string msg)
        {
            m_debugInfo.AppendLine(string.Format("[{0,5}] {1}", m_debugStopwatch.ElapsedMilliseconds, msg));
        }

        private void LogInfo(string msg)
        {
            string str = string.Format("[{0,5}] {1}", m_debugStopwatch.ElapsedMilliseconds, msg);
            m_debugInfo.AppendLine(str);
            m_logInfo.AppendLine(str);
        }

        public PricingSummary DebugPricingSummary
        {
            get { return m_debugSummary; }
        }

        private CApplicantRateOption GetParRate(ArrayList list)
        {
            CApplicantRateOption repRate = null;
            decimal appliedOriginatorCompPoint = 0;

            decimal sOriginatorCompPoint = m_loanFileData.sOriginatorCompPoint; // 3/28/2011 dd - Cache this value.
            if (m_loanFileData.sIsQualifiedForOriginatorCompensation && m_loanFileData.sHasOriginatorCompensationPlan)
            {
                if (m_loanFileData.BrokerDB.IsUsePriceIncludingCompensationInPricingResult
                    && m_loanFileData.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                    && m_loanFileData.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees)
                {
                    // In result we are adding to point value
                    appliedOriginatorCompPoint = m_loanFileData.sOriginatorCompPoint;
                }
            }

            foreach (CApplicantRateOption option in list)
            {
                if ((null == repRate)
                                       || (repRate.Point + appliedOriginatorCompPoint > 0 &&
                                             (option.Point + appliedOriginatorCompPoint <= 0
                                               || option.Point + appliedOriginatorCompPoint < repRate.Point + appliedOriginatorCompPoint)))
                {
                    repRate = option;
                }
            }

            return repRate;

        }
        private string GetFullPolicyName(Guid policyId)
        {
            if (policyId == Guid.Empty)
            {
                return string.Empty;
            }
            // 1/15/2014 dd - Only use for debugging.
            List<string> list = new List<string>();

            MyPricePolicy policy = LoanProgramSet.GetPricePolicy(policyId);
            list.Add(policy.PricePolicyDescription);

            while (policy != null)
            {
                if (policy.ParentPolicyId != Guid.Empty)
                {
                    policy = LoanProgramSet.GetPricePolicy(policy.ParentPolicyId);
                    list.Add(policy.PricePolicyDescription);
                }
                else
                {
                    policy = null; // Break out of loop.
                }
            }
            list.Reverse();

            StringBuilder fullName = null;
            foreach (var str in list)
            {
                if (fullName == null)
                {
                    fullName = new StringBuilder();
                }
                else
                {
                    fullName.Append(@" / ");
                }
                fullName.Append(str);
            }
            return fullName.ToString();
        }

        protected void ComputePrices(CacheInvariantPolicyResults cachResult)
        {
            LogInfo("ComputePrices FriendlyInfo=[" + FriendlyInfo + "]");
            LogInfo("");
            LogInfo("lLpTemplateId=[" + lLpTemplateId + "], Full Name=[" + FullName + "]");

            EvalStack stack = null;
            XmlDocument xmlDocEval = new XmlDocument(); // for recycling reading of an eval node.
            HashSet<Guid> skipList = new HashSet<Guid>();
            Hashtable mutualExclExeList = new Hashtable(20);
            m_debugSummary = new PricingSummary();

            var isTargeting2019Ulad = m_loanFileData.sIsTargeting2019Ulad;

            decimal maxYsp;

            CPriceGroupDetails priceGroupDetails; //Program-PG specific data (Point, Margin, etc.)
            // The data about the pricegroup itself not specific to program (lock policies, global settings).
            Lazy<PriceGroup> priceGroup = new Lazy<PriceGroup>(() => PriceGroup.RetrieveByID(m_runOptions.PriceGroupId, m_loanFileData.BrokerDB.BrokerID));

            bool bGetAllReasons;
            bool bImproveParRate;
            DataAccess.FeeService.FeeServiceApplication.FeeServiceResult feeServiceResult = null;
            this.FeeServiceRevisionId = InvalidFeeServiceRevisionId;
            bool appliedAutoCCTemplate = false;
            bool calculateclosingCostInPML = m_loanFileData.BrokerDB.CalculateclosingCostInPML && !IsTotalOrH4HSpecialProgram;

            bool UsePerRatePricing = m_loanFileData.BrokerDB.IsUsePerRatePricing;
            bool useRateOptionVariantMI = m_loanFileData.BrokerDB.UseRateOptionVariantMI;

            bool UseNewPml = m_loanFileData.IsNewPMLEnabled;

            // OPM 190542 - Update sPmlCompanyTierId - 11/18/14 je
            m_loanFileData.sPmlCompanyTierId = m_loanFileData.ComputePmlCompanyTierId();

            IEnumerable<Guid> applicablePolicies = LoanProgramSet.GetApplicablePolicies(lLpTemplateId); // This list is sorted by MutualExclusiveExecSortedId

            if (null == applicablePolicies)
            {
                PrepareInsufficientInfoStatus("No policy", "No Policy", "No applicable policy for lLpTemplateId=" + lLpTemplateId);
                return;
            }

            if (IsLogDebugInfo)
            {
                LogDebug("Applicable Policies List:");
                int count = 0;
                foreach (var o in applicablePolicies)
                {

                    LogDebug("    " + count + " -PolicyId:" + o + " - [" + GetFullPolicyName(o) + "]");
                    count++;
                }
                LogDebug("");
            }
            E_sProd3rdPartyUwResultT old_sProd3rdPartyUwResultT;

            do
            {
                using (PerformanceStopwatch.Start("Preparation Phase"))
                {
                    stack = new EvalStack(EvalStackSize);

                    m_errors = new Hashtable(30);
                    m_developerErrors = new Hashtable(30);
                    m_PhaseZeroLockDaysAdj = 0; // OPM 229841
                    m_maxDti = 0;
                    m_qScore = 0;

                    // Internal opm# 16132 and 10242, we want to allow SAE to disable an entire product from a particular investor. -tn 6/21/07
                    DisabledBySaeItem disabledByItem = DisabledBySaeTable.Retrieve(lLpInvestorNm, ProductCode);
                    if (disabledByItem != null)
                    {
                        string regTestMsg = string.Format("This program name={0}, id={1} has been disabled by SAE: {2} with mode:{3}",
                                                            lLpTemplateNm, lLpTemplateId.ToString(), disabledByItem.SaeName,
                                                            disabledByItem.DisableStatus);
                        Exception ex = new CBaseException(ErrorMessages.LP_DisabledBySAEMessage,
                                                                                        regTestMsg);

                        PrepareInsufficientInfoStatus(ex, regTestMsg, Guid.Empty, Guid.Empty);
                        return; // stop the execution immediately.
                    }

                    #region Preparation

                    m_marginBaseAllROptions = 0; // lRAdjMarginR; // base;
                    m_marginDeltaDisclosed = 0;
                    m_marginDeltaHidden = 0;
                    m_feeDeltaDisclosed = 0;
                    m_feeDeltaHidden = 0;
                    m_rateDeltaDisclosed = 0;
                    m_rateDeltaHidden = 0;
                    m_qualRDeltaDisclosed = 0;
                    m_qualRDeltaHidden = 0;
                    m_teaserRDeltaDisclosed = 0;
                    m_teaserRDeltaHidden = 0;

                    m_disqualifiedRules = "";
                    m_adjDescsDisclosed = new List<CAdjustItem>();
                    m_adjDescsHidden = new List<CAdjustItem>();
                    m_evalStatus = E_EvalStatus.Eval_Eligible;

                    m_loanFileData.InvalidateCache(); // 11/24/04: Fixing bug 2393
                    m_loanFileData.sPricingModeT = m_runOptions.PricingModeT;
                    // 9/20/2010 dd - Need to store the original value of sProd3rdPartyUwResultT. In the ApplyLoanProductTemplate method
                    // it is possible for the sProd3rdPartyUwResultT to be modify (due to OPM 48940). As the result after we finish running
                    // pricing on this loan product we need to revert back the settings.
                    old_sProd3rdPartyUwResultT = m_loanFileData.sProd3rdPartyUwResultT;

                    int? historicalRev = null;
                    var historicalOption = this.m_runOptions.HistoricalPricingOption;
                    if (historicalOption.IsHistoricalMode() && historicalOption.FeeServiceOptions == E_LoanProgramSetPricingT.Historical)
                    {
                        historicalRev = historicalOption.FeeServiceRevisionId;
                    }

                    m_loanFileData.ApplyLoanProductTemplate(this, false /* bApplyCcTemplate */, historicalRev); // this method will indirectly load FeeServiceRevision
                    if (m_loanFileData.LoanProductMatchingTemplate != null)
                        m_loanFileData.LoanProductMatchingTemplate.ByPassFieldSecurityCheck = true;

                    // OPM 476789. Now that the program is applied, we can disable per-rate mi when
                    // it is not applicable.
                    if(m_loanFileData.sLT != E_sLT.Conventional)
                    {
                        useRateOptionVariantMI = false;
                    }

                    // OPM 471694 - QTerm cannot be 0 (results in divide by 0 exception in QPayment Calc).
                    if (m_loanFileData.sQualTerm == 0M)
                    {
                        string msg = "QTerm is 0 which is causing division by zero";
                        string devMsg = $"QTerm is 0 for lLpTemplateId = {lLpTemplateId}";
                        PrepareInsufficientInfoStatus(msg, msg, devMsg);

                        return; // Can stop here.
                    }

                    if (m_runOptions.RateLockDays.HasValue)
                    {
                        // OPM 119928.  Means the pricing caller wants to price with this lock period
                        m_loanFileData.sProdRLckdDays = m_runOptions.RateLockDays.Value;
                    }


                    m_loanFileData.SetAprItemForLOrigFProps(true); // override existing data and the data from CC template.

                    DtiFilterEnum dtiFilter = ConstStage.AdjustParRateOptionEnable && (m_loanFileData.CalcModeT == E_CalcModeT.PriceMyLoan)
                                               ? DtiFilterEnum.AllowNonDtiOnly : DtiFilterEnum.AllowAll;


                    // If calling code specifically specify the rate and fee to be used, do it - 
                    // that means overwriting whatever ApplyLoanProductTemplate() did for rate and fee.
                    if (m_runOptions.SelectedRate > 0)
                    {
                        m_loanFileData.sNoteIR = m_runOptions.SelectedRate;
                        m_loanFileData.sBrokComp1Pc = m_runOptions.SelectedFee;
                        dtiFilter = DtiFilterEnum.AllowAll;

                    }

                    bImproveParRate = (dtiFilter == DtiFilterEnum.AllowNonDtiOnly);


                    m_loanFileData.sRAdjWorstIndex = false;// to avoid display the worst case scenario for APR calculation.

                    if (IsTotalOrH4HSpecialProgram) // Special PML Master TOTAL Scorecard Program
                        m_runOptions.IsGetAllReasons = true;

                    bGetAllReasons = m_runOptions.IsGetAllReasons || useRateOptionVariantMI;

                    ForceTransform();

                    // OPM. 471515. If the lender uses rate-variant MI, the Closing Cost application
                    // needs to occur further down the line (after non-DTI Phase 2).
                    if (!useRateOptionVariantMI
                        && calculateclosingCostInPML
                        && UseNewPml) // OPM 109662. AutoCC Option might be on at lender, but this user does not use new UI.
                    {
                        var matchedClosingCosts = ExecuteClosingCost(ref feeServiceResult, ref appliedAutoCCTemplate);
                        if ( !matchedClosingCosts )
                        {
                            // ExecuteClosingCost() will have set invalid status in this case. 
                            return;
                        }
                    }

                    #endregion // Preparation

                }

                // The loan is going to be inspected now for fee service / title changes only. Pricing is not being fully run.
                // Hacky, but this is a sensitive hotfix.
                if (this.m_runOptions.PrepareLoanOnly)
                {
                    return;
                }

                PriceAdjustRecord.Args priceAdjustRecordArgs;
                decimal? m_lpmiAdjust = null;
                bool recalculateMortgageInsurancePerRate = false;
                if (UsePerRatePricing && !IsTotalOrH4HSpecialProgram)
                {
                    if (useRateOptionVariantMI)
                    {
                        m_reasonArray = new List<Toolbox.CStipulation>(20);

                        // OPM 471515.
                        // Branching gets way too tricky between the old PMI calc/apply, 
                        // so we have a seperate method while this flag is in beta.
                        var miResult = ExecutePMI_Variant(
                            ref m_lpmiAdjust,
                            xmlDocEval,
                            cachResult,
                            stack,
                            ref recalculateMortgageInsurancePerRate,
                            m_RateVariantPmi.ForcedMIProgram,
                            ignoreDtiRules: true,
                            applyWinner: true);

                        m_RateVariantPmi.SetBaselineMiResult(miResult);
                    }
                    else
                    {
                        // Classic PMI
                        using (PerformanceStopwatch.Start("PMI Execution"))
                        {
                            LogInfo("");
                            LogInfo("PMI Execution");
                            #region PMI
                            // Per PDE, this does not need to be done per-rate.  Do it early here.
                            if (m_loanFileData.BrokerDB.HasEnabledPMI)
                            {

                                if (m_loanFileData.sLT == E_sLT.Conventional
                                    && m_loanFileData.sProdConvMIOptionT == E_sProdConvMIOptionT.LendPaidSinglePrem)
                                {
                                    // OPM 189515.
                                    m_loanFileData.sProMInsCancelLtv = 0;
                                    m_loanFileData.sProMInsMidptCancel = false;
                                    m_loanFileData.sProMInsCancelMinPmts = 0;
                                }

                                if (lLpmiSupportedOutsidePmi == true && m_loanFileData.sProdConvMIOptionT == E_sProdConvMIOptionT.LendPaidSinglePrem)
                                {
                                    // OPM 180150 - Special Program setting--SAES use adjustments for LPMI instead of AutoMI.
                                    AdjustMIForLPMI();
                                    m_mortgageInsuranceProvider = "";

                                    // After we have gathered all adjustments, we also set sLenderUfmipR = [Sum of SAE adjustments with "LPMI" text contained within adjustment description.]
                                }
                                else if (m_loanFileData.sLT == E_sLT.Conventional)
                                {
                                    bool useMi = m_loanFileData.sProdConvMIOptionT != E_sProdConvMIOptionT.NoMI && m_loanFileData.sProdConvMIOptionT != E_sProdConvMIOptionT.Blank;

                                    // Special hardcode requested for OPM 107793.
                                    if (m_loanFileData.sLpProductT == E_sLpProductT.HomePath || m_loanFileData.sLpProductT == E_sLpProductT.HomePathRenovation)
                                    {
                                        // "Homepath loans are conventional loans that never have mortgage insurance. 
                                        // Therefore we need to exclude homepath loans from seeking a PMI program partner in PML."
                                        useMi = false;
                                    }

                                    PmiResult pmiProgram = null;
                                    string execLog = "";
                                    string pmiDisqualReason = ""; // OPM 107423

                                    if (useMi == true)
                                    {
                                        priceAdjustRecordArgs = new PriceAdjustRecord.Args(xmlDocEval, m_loanFileData.sSymbolTableForPriceRule, m_runOptions, this, cachResult, stack);
                                        pmiProgram = SetPMIProduct(priceAdjustRecordArgs, out execLog, out pmiDisqualReason, cachResult);
                                    }

                                    m_loanFileData.sProMInsLckd = false;

                                    if (pmiProgram != null)
                                    {
                                        string selectRateInfo = string.Format("Picked PMI = {0}, Type = {1}, MonthlyPmt = {2}, SinglePmt = {3}"
                                            , pmiProgram.PmiCompany
                                            , pmiProgram.PmiType
                                            , pmiProgram.MonthlyPmt
                                            , pmiProgram.SinglePmt
                                            );

                                        m_mortgageInsuranceProvider = pmiProgram.PmiCompany.ToString();

                                        selectRateInfo += "<br /><br /> Execution log: <br />" + execLog;
                                        if (IsLogDetailDebugInfo)
                                            m_adjDescsHidden.Add(new CAdjustItem(selectRateInfo));

                                        m_mortgageInsuranceDesc = PmiResult.PmiTypeDescription(pmiProgram.PmiType) + ", "
                                            + (pmiProgram.SinglePct != 0m ? pmiProgram.SinglePct + "% Upfront, " : "")
                                            + (pmiProgram.MonthlyPct != 0m ? pmiProgram.MonthlyPct + "% Monthly " : "");

                                        if (this.IsLogDebugInfo)
                                        {

                                            this.LogDebug("PmiSummaryLog:\r\n" + execLog.Replace("<br />* ", "\r\n    ").Replace("<br />", "\r\n        ")
                                                         + "\r\n    " + m_mortgageInsuranceDesc + "\r\n");
                                        }

                                        // OPM 110220.  We don't reuse these MI from previous result.  DEFAULT them.
                                        m_loanFileData.sProMInsMon = 0;
                                        m_loanFileData.sProMIns2Mon = 0;
                                        m_loanFileData.sProMInsR2 = 0;

                                        // OPM 143132 - This is for QM.
                                        m_loanFileData.sUfmipIsRefundableOnProRataBasis = pmiProgram.UfmipIsRefundableOnProRataBasis;

                                        if (m_loanFileData.sProdConvMIOptionT != E_sProdConvMIOptionT.LendPaidSinglePrem)
                                        {
                                            // OPM 144616 requests this.  Fannie Mae likes it this way.
                                            // OPM 212375 updated
                                            if (m_loanFileData.sOccTPe == E_sOccT.PrimaryResidence
                                                && m_loanFileData.sUnitsNum < 2)
                                            {
                                                m_loanFileData.sProMInsCancelLtv = 78;
                                                m_loanFileData.sProMInsMidptCancel = true;
                                                m_loanFileData.sProMInsCancelMinPmts = 0;
                                            }
                                            else
                                            {
                                                m_loanFileData.sProMInsCancelLtv = 0;
                                                m_loanFileData.sProMInsMidptCancel = false;
                                                m_loanFileData.sProMInsCancelMinPmts = 0;
                                            }
                                        }
                                        // Outlined in section 3.3.1 of Spec for case 26013
                                        switch (pmiProgram.PmiType)
                                        {
                                            case E_sProdConvMIOptionT.BorrPaidMonPrem:
                                                m_loanFileData.sProMInsR = pmiProgram.MonthlyPct;
                                                m_loanFileData.sProMInsPe_rep = pmiProgram.MonthlyPmt.ToString();
                                                m_loanFileData.sFfUfmipR = 0m;
                                                m_loanFileData.sFfUfmip1003 = 0m;
                                                m_mortgageInsuranceMonthlyPmt = pmiProgram.MonthlyPmt.ToString();
                                                m_mortgageInsuranceMonthlyRate = pmiProgram.MonthlyPct.ToString();

                                                // OPM 110220 explains this logic.
                                                if (m_loanFileData.sProMInsR != 0)
                                                {
                                                    m_loanFileData.sProMInsMon = 120;
                                                    m_loanFileData.sProMIns2Mon = m_loanFileData.sDue - m_loanFileData.sProMInsMon;
                                                    m_loanFileData.sProMInsR2 = (m_loanFileData.sProMInsR > 0.2m) ? 0.2m : m_loanFileData.sProMInsR;
                                                }

                                                // case 144618 data clearing
                                                m_loanFileData.sProMInsMb = 0;
                                                m_loanFileData.sProMInsLckd = false;
                                                m_loanFileData.sLenderUfmipR = 0;
                                                m_loanFileData.sLenderUfmipLckd = false;

                                                break;
                                            case E_sProdConvMIOptionT.BorrPaidSinglePrem:
                                                m_loanFileData.sFfUfmipR = pmiProgram.SinglePct;
                                                m_loanFileData.sFfUfmip1003 = pmiProgram.SinglePmt;
                                                m_mortgageInsuranceSinglePmt = pmiProgram.SinglePmt.ToString();
                                                m_mortgageInsuranceSingleRate = pmiProgram.SinglePct.ToString();

                                                // case 144618 data clearing
                                                m_loanFileData.sProMInsR = 0;
                                                m_loanFileData.sProMInsMb = 0;
                                                m_loanFileData.sProMInsLckd = false;
                                                m_loanFileData.sProMInsMon = 0;
                                                m_loanFileData.sProMIns2Mon = 0;
                                                m_loanFileData.sProMInsR2 = 0;
                                                m_loanFileData.sLenderUfmipR = 0;
                                                m_loanFileData.sLenderUfmipLckd = false;
                                                m_loanFileData.sUfCashPdLckd = false;

                                                break;
                                            case E_sProdConvMIOptionT.BorrPaidSplitPrem:

                                                if (m_loanFileData.sConvSplitMIRT == E_sConvSplitMIRT.Blank)
                                                {
                                                    m_evalStatus = E_EvalStatus.Eval_InsufficientInfo;

                                                    Tools.LogError(string.Format("BPMI Split, but upfront MI is blank. Program:{0}({1}). Loan:{2}({3})."
                                                        , lLpTemplateNm
                                                        , lLpTemplateId
                                                        , m_loanFileData.sLNm
                                                        , m_loanFileData.sLId));

                                                    return;
                                                }

                                                // OPM 110220 explains this logic.
                                                if (m_loanFileData.sProMInsR != 0)
                                                {
                                                    m_loanFileData.sProMInsMon = 120;
                                                    m_loanFileData.sProMIns2Mon = m_loanFileData.sDue - m_loanFileData.sProMInsMon;
                                                    m_loanFileData.sProMInsR2 = (m_loanFileData.sProMInsR > 0.2m) ? 0.2m : m_loanFileData.sProMInsR;
                                                }

                                                m_loanFileData.sProMInsR = pmiProgram.MonthlyPct;
                                                m_loanFileData.sFfUfmipR = pmiProgram.SinglePct;
                                                m_loanFileData.sProMInsPe_rep = pmiProgram.MonthlyPmt.ToString();
                                                m_loanFileData.sFfUfmip1003 = pmiProgram.SinglePmt;
                                                m_mortgageInsuranceMonthlyPmt = pmiProgram.MonthlyPmt.ToString();
                                                m_mortgageInsuranceSinglePmt = pmiProgram.SinglePmt.ToString();
                                                m_mortgageInsuranceMonthlyRate = pmiProgram.MonthlyPct.ToString();
                                                m_mortgageInsuranceSingleRate = pmiProgram.SinglePct.ToString();

                                                // case 144618 data clearing
                                                m_loanFileData.sProMInsMb = 0;
                                                m_loanFileData.sProMInsLckd = false;
                                                m_loanFileData.sLenderUfmipR = 0;
                                                m_loanFileData.sLenderUfmipLckd = false;

                                                break;
                                            case E_sProdConvMIOptionT.LendPaidSinglePrem:

                                                if (m_loanFileData.sPricingModeT != E_sPricingModeT.InternalInvestorPricing)
                                                {
                                                    PriceAdjustRecord.AdjustItem lpmiItem = new PriceAdjustRecord.AdjustItem(0, 0, pmiProgram.SinglePct, 0, 0, "LPMI Premium", "", true, Guid.Empty);
                                                    CAdjustItem lpmiAdjustment = lpmiItem.ToCAdjustItem(m_losConvert, lHasQRateInRateOptions, IsOptionArm);
                                                    if (lpmiAdjustment != null)
                                                        m_adjDescsDisclosed.Add(lpmiAdjustment);

                                                    m_feeDeltaDisclosed += pmiProgram.SinglePct;

                                                    m_lpmiAdjust = pmiProgram.SinglePct;
                                                }

                                                // OPM 122017.
                                                m_mortgageInsuranceSinglePmt = pmiProgram.SinglePmt.ToString();
                                                m_mortgageInsuranceSingleRate = pmiProgram.SinglePct.ToString();
                                                m_loanFileData.sLenderUfmipR = pmiProgram.SinglePct; // OPM 132071

                                                // case 144618 data clearing
                                                AdjustMIForLPMI();

                                                break;
                                        }

                                    }
                                    else
                                    {
                                        if (useMi == true)
                                        {
                                            // PMI is enabled and required and this is a Conventional loan, but we were denied all PMI programs
                                            // Means this entire program is ineligible.
                                            m_evalStatus = E_EvalStatus.Eval_Ineligible;
                                            m_PmiDisqual = true;
                                            m_reasonArray.Add(new CStipulation(pmiDisqualReason, ""));
                                            if (m_disqualifiedRules.Length > 0)
                                                m_disqualifiedRules += "<br>";
                                            m_disqualifiedRules += " * " + pmiDisqualReason;
                                        }
                                        else
                                        {
                                            execLog = "THIS IS A NO-MI SCENARIO";
                                            m_mortgageInsuranceDesc = "None";
                                        }

                                        if (IsLogDetailDebugInfo)
                                            m_adjDescsHidden.Add(new CAdjustItem("NO PMI SELECTED" + "<br /><br /> Execution log: <br />" + execLog));

                                        m_loanFileData.sProMInsLckd = false;
                                        m_loanFileData.sProMInsR = 0m;
                                        m_loanFileData.sFfUfmipR = 0m;
                                        m_loanFileData.sProMInsPe_rep = "0";
                                        m_loanFileData.sFfUfmip1003 = 0;
                                        m_mortgageInsuranceMonthlyPmt = "0";
                                        m_mortgageInsuranceSinglePmt = "0";
                                        m_mortgageInsuranceMonthlyRate = "0";
                                        m_mortgageInsuranceSingleRate = "0";

                                    }
                                } // Conventional
                                else if (m_loanFileData.sLT == E_sLT.FHA)
                                {
                                    // OPM. 114052.  AverageOutstandingBalance calculation needs note rate to calculate payment correctly in comparison.
                                    if (m_runOptions.PricingState != null)
                                    {
                           
                                        m_loanFileData.sNoteIR_rep = m_runOptions.PricingState.GetNoteRate(m_runOptions.GetMode());
                                    }

                                    m_loanFileData.Update_sProMInsFieldsPer110559();

                                    m_mortgageInsuranceDesc = "FHA" + ", " + m_loanFileData.sProdFhaUfmip_rep + " upfront, " + m_loanFileData.sProMInsR_rep + " monthly";
                                    if (this.m_loanFileData.sProMInsT != E_PercentBaseT.LoanAmount)
                                    {
                                        m_loanFileData.sProMInsT = E_PercentBaseT.AverageOutstandingBalance; // Per OPM 114052
                                    }
                                    m_loanFileData.sProMInsPe_rep = Math.Round((m_loanFileData.sProMInsR * m_loanFileData.sProMInsBaseAmt / 1200.00M), 2).ToString();
                                    m_loanFileData.sProMInsLckd = false;
                                    m_mortgageInsuranceMonthlyPmt = m_loanFileData.sProMIns_rep;
                                    m_mortgageInsuranceSinglePmt = m_loanFileData.sProdFhaUfmip_rep;

                                    // case 144618 data clearing
                                    m_loanFileData.sLenderUfmipR = 0;
                                    m_loanFileData.sLenderUfmipLckd = false;
                                    m_loanFileData.sProMInsMidptCancel = false;
                                    m_loanFileData.sProMIns2Mon = 0;
                                    m_loanFileData.sProMInsR2 = 0;

                                    recalculateMortgageInsurancePerRate = true;
                                }
                                else if (m_loanFileData.sLT == E_sLT.UsdaRural)
                                {
                                    // OPM. 114052.  AverageOutstandingBalance calculation needs note rate to calculate payment correctly in comparison.
                                    if (m_runOptions.PricingState != null)
                                    {
                                        m_loanFileData.sNoteIR_rep = m_runOptions.PricingState.GetNoteRate(m_runOptions.GetMode());
                                    }

                                    DateTime dateToUse = DateTime.Now;
                                    if (m_loanFileData.sMiCommitmentReceivedD.IsValid)
                                    {
                                        dateToUse = m_loanFileData.sMiCommitmentReceivedD.DateTimeForComputation;
                                    }

                                    if (dateToUse <= new DateTime(2016, 9, 30))
                                    {
                                        m_loanFileData.sProMInsR = .5m;
                                    }
                                    else
                                    {
                                        m_loanFileData.sProMInsR = .35m;
                                    }

                                    m_mortgageInsuranceDesc = "USDA" + ", " + m_loanFileData.sProdUSDAGuaranteeFee_rep + " upfront, " + m_loanFileData.sProMInsR_rep + " monthly";
                                    m_loanFileData.sProMInsCancelLtv = 0;
                                    m_loanFileData.sProMInsCancelMinPmts = 0;

                                    m_loanFileData.sProMInsT = E_PercentBaseT.AverageOutstandingBalance; // Per OPM 114052
                                    m_loanFileData.sProMInsPe_rep = Math.Round((m_loanFileData.sProMInsR * m_loanFileData.sProMInsBaseAmt / 1200.00M), 2).ToString();
                                    m_loanFileData.sProMInsLckd = false;
                                    m_mortgageInsuranceMonthlyPmt = m_loanFileData.sProMIns_rep;
                                    m_mortgageInsuranceSinglePmt = m_loanFileData.sProdUSDAGuaranteeFee_rep;

                                    // case 144618 data clearing
                                    m_loanFileData.sLenderUfmipR = 0;
                                    m_loanFileData.sLenderUfmipLckd = false;
                                    m_loanFileData.sProMInsMidptCancel = false;
                                    m_loanFileData.sProMInsMb = 0;
                                    m_loanFileData.sProMInsMon = m_loanFileData.sDue;
                                    m_loanFileData.sFfUfmip1003Lckd = false;
                                    m_loanFileData.sUfCashPdLckd = false;
                                    m_loanFileData.sProMIns2Mon = 0;
                                    m_loanFileData.sProMInsR2 = 0;

                                    recalculateMortgageInsurancePerRate = true;
                                }
                                else if (m_loanFileData.sLT == E_sLT.VA)
                                {
                                    m_mortgageInsuranceDesc = "VA" + ", " + m_loanFileData.sProdVaFundingFee_rep + " upfront ";
                                    m_loanFileData.sProMInsPe_rep = "0"; // VA has no monthly
                                    m_loanFileData.sProMInsLckd = false;
                                    m_mortgageInsuranceMonthlyPmt = m_loanFileData.sProMIns_rep;
                                    m_mortgageInsuranceSinglePmt = m_loanFileData.sProdVaFundingFee_rep;

                                    // case 144618 data clearing
                                    m_loanFileData.sProMInsR = 0;
                                    m_loanFileData.sProMInsMb = 0;
                                    m_loanFileData.sProMInsLckd = false;
                                    m_loanFileData.sLenderUfmipR = 0;
                                    m_loanFileData.sLenderUfmipLckd = false;
                                    m_loanFileData.sProMInsMidptCancel = false;
                                    m_loanFileData.sUfCashPdLckd = false;
                                    m_loanFileData.sProMInsMon = 0;
                                    m_loanFileData.sProMIns2Mon = 0;
                                    m_loanFileData.sProMInsR2 = 0;

                                }
                            }
                            #endregion
                        }
                    }
                }


                using (PerformanceStopwatch.Start("Phase 0"))
                {
                    if (IsLogDebugInfo)
                    {
                        LogDebug("Execute Phase 0 - Compute Q (qualified) values based on rules");
                    }
                    #region Phase 0 to compute the Q (qualified) values based on rules

                    if (m_runOptions.IsUsePricingSummaryDebug)
                    {
                        m_debugSummary.DumpPhaseZero(m_loanFileData.sSymbolTableForPriceRule.GetValues());
                    }
                    PhaseZeroAdjustRecord.PhaseZeroValues4Applicant phaseZeroValues = new PhaseZeroAdjustRecord.PhaseZeroValues4Applicant(m_loanFileData, this, cachResult.adjustQscoreRecords);

                    try
                    {
                        // 04/15/09 mf - OPM 25872. QRules (Phase 0) are always Primary App joint credit
                        if (m_runOptions.UseQbc)
                            m_applications.SetPricingContext(m_applications.PrimaryBorrower.Application.AppIndex, E_aBorrowerCreditModeT.Both);

                        foreach (Guid policyId in applicablePolicies)
                        {
                            MyPricePolicy pricePolicy = LoanProgramSet.GetPricePolicy(policyId);
                            PhaseZeroAdjustRecord.PhaseZeroValues4Applicant.ComputePhaseZeroValues(pricePolicy, xmlDocEval, m_loanFileData.sSymbolTableForPriceRule,
                                stack, phaseZeroValues, m_loanFileData.sPricingModeT,
                                m_debugInfo, IsLogDebugInfo);

                        } // for
                    }
                    catch (PricingCalcException exc)
                    {
                        try
                        {
                            phaseZeroValues.CopyPhaseZeroValuesToLoanData(m_loanFileData, m_adjDescsHidden, false /* isComplete */, out m_PhaseZeroLockDaysAdj);
                        }
                        finally { }

                        PrepareInsufficientInfoStatus(exc.LogMsg, exc.ErrorMsg, exc.ErrorMsg4Developer);
                        return; // stop the execution immediately.
                    }
                    catch (ModifyQScoreException exc)
                    {
                        try
                        {
                            phaseZeroValues.CopyPhaseZeroValuesToLoanData(m_loanFileData, m_adjDescsHidden, false /* isComplete */, out m_PhaseZeroLockDaysAdj);
                        }
                        finally { }

                        string errStr = "System error when evaluating QScore. System administrator has been notified.";

                        if (!m_errors.Contains(errStr))
                        {
                            // We can't do this more than once, it would result in undeterministic behavior for QScore
                            Tools.LogErrorWithCriticalTracking(string.Format("Error encountered in phase 0 (Q values), Qscore is being modified more than once, " +
                                                                      "that would result in undeterministic result for Qscore, executing rule with {0}.  " +
                                                                      "Product Id={1}. Product Nm={2}. Error msg={3}\nNote : first time, qscore was changed by executing {4}",
                                exc.RuleInfo, lLpTemplateId.ToString(), FullName, errStr, exc.PrevRuleInfo));

                            m_errors.Add(errStr, null);

                            string errorMsg4Developer = string.Format("{0}. Qscore are modified by current {1} and previous {2}", errStr, exc.RuleInfo, exc.PrevRuleInfo);
                            m_developerErrors.Add(errorMsg4Developer, null);
                            LogDebug("    => DevErrMsg: " + errorMsg4Developer);
                        }
                        m_evalStatus = E_EvalStatus.Eval_InsufficientInfo;
                        return;
                    }

                    string qvalueId = phaseZeroValues.CopyPhaseZeroValuesToLoanData(m_loanFileData, m_adjDescsHidden, true /* isComplete */, out m_PhaseZeroLockDaysAdj);
                    cachResult.SetQValueId(qvalueId);
                    cachResult.numPhaseZeroSuccess++;
                    LogInfo("    qValueId=[" + qvalueId + "]");

                    #endregion // Phase 0 to compute the Q (qualified) values based on rules

                    #region Verify Pricing Segment
                    // Per OPM 17988, rules can set adjustments to the lock days in Phase 0,
                    // so we terminate early if there will be no applicable segment in the final pricing.

                    // 6/22/09 mf. OPM 2950. We want to make sure the submitted LOCK, PP, and IO
                    // segments (groups) exist on the ratesheet, and display the appropriate disqual message
                    // if they do not.
                    string disqualMsg = string.Empty;
                    if (!IsTotalOrH4HSpecialProgram
                        && VerifyRateOptionPricingSegment(m_loanFileData.sRLckdDaysFromInvestor, m_loanFileData.sProdPpmtPenaltyMon, m_loanFileData.sIOnlyMon, ref disqualMsg) == false)
                    {
                        m_evalStatus = E_EvalStatus.Eval_Ineligible;
                        string reason = disqualMsg;
                        m_reasonArray.Add(new CStipulation(reason, ""));
                        if (m_disqualifiedRules.Length > 0)
                            m_disqualifiedRules += "<br>";
                        m_disqualifiedRules += " * " + reason;
                        LogInfo("=> *** RateOption Disqualify: " + disqualMsg);

                        return; // Stop right away--cannot price without base rates.
                    }
                    #endregion

                    #region LockDays Modify Stip
                    if (m_loanFileData.sRLckdDaysFromInvestor != m_loanFileData.sProdRLckdDays)
                    {
                        // 1/16/2011 dd - When running pricing in Internal Investor mode, we do not need to include the hidden adjustment since
                        // we always use the lock days from user specification.

                        if (m_loanFileData.sPricingModeT != E_sPricingModeT.InternalInvestorPricing && m_loanFileData.sPricingModeT != E_sPricingModeT.EligibilityInvestorPricing)
                        {
                            // 11/20/09 mf. OPM 17988: If we are modifying the rate lock period, add a hidden adjustment
                            // At this point, sRLckdDaysFromInvestor = [Lender Static Lock Buffer] + [Sum of Phase 0 Adjustments]

                            int lockDaysTotalAdjustment = m_loanFileData.sRLckdDaysFromInvestor - m_loanFileData.sProdRLckdDays;

                            string lockInfo = string.Format("Add {0} to LockDays, Pricing Corresponds to Investor {1} Day Lock", lockDaysTotalAdjustment, m_loanFileData.sRLckdDaysFromInvestor);
                            m_adjDescsHidden.Add(new CAdjustItem("", "", "", "", "", lockInfo, ""));
                        }
                    }
                    #endregion
                }

                using (PerformanceStopwatch.Start("Phase 1"))
                {
                    LogInfo("");
                    LogInfo("Execute Phase 1 - Handle the If-Else (Skip) && Mutual Exclusive Execution statement for a policy and its children");

                    #region Phase 1 to handle the If-Else (Skip) && Mutual Exclusive Execution statement for a policy and its children
                    foreach (Guid policyId in applicablePolicies)
                    {

                        //11/20/04-BD-Does this work? Yes-tn
                        if (skipList.Contains(policyId))
                        {
                            if (!ConstSite.PricingCompactLogEnabled)
                            {
                                LogInfo("    Skip PolicyId=[" + policyId + "]");
                            }
                            continue;
                        }

                        E_EvalStatus thisPolicyStatus = E_EvalStatus.Eval_Eligible;

                        MyPricePolicy pricePolicy = LoanProgramSet.GetPricePolicy(policyId);
                        Tools.Assert(policyId == pricePolicy.PricePolicyId, "Expecting the hashtable m_policyManager to keep the policy and its key policy id correctly.");
                        bool bMutualExclusiveExe = "" != pricePolicy.MutualExclusiveExecSortedId;
                        Guid parentPolicyId = pricePolicy.ParentPolicyId;

                        if (bMutualExclusiveExe && mutualExclExeList.ContainsKey(parentPolicyId))
                        {
                            // TODO: For scalability, we need a mapping from parent as key to children as list of values tree store in the database.

                            // A preceding branch has been selected and will be executed in phase 2, skipping this sibling entire branch.
                            LogInfo("    Add descendant of " + policyId + " to skip list.");
                            m_parentInfo.AddDescendantsToSet(policyId, skipList);

                            skipList.Add(policyId); // skipping the policy group itself too, not just its children
                            continue;
                        }

                        pricePolicy.LoadRules();

                        MyRuleList rules = pricePolicy.Rules;

                        // See task#1865,we no longer try to get all insufficient data info, just fail it on the first one encountered.

                        for (int i = 0; i < rules.Count; ++i)
                        {
                            MyRule rule = rules.GetRule(i);

                            if (rule.IsLenderRule == true && (m_loanFileData.sPricingModeT == E_sPricingModeT.InternalInvestorPricing || m_loanFileData.sPricingModeT == E_sPricingModeT.EligibilityInvestorPricing))
                            {
                                LogInfo("    Skip Lender RuleId:" + rule.RuleId + " when running internal pricing mode.");
                                // 1/15/2011 dd - OPM 30649 - When run pricing in Investor Pricing Mode we do not include rule that flag as "Is Lender Rule"
                                continue;
                            }

                            MyConsequence consequence = rule.Consequence;
                            bool bSkippingRule = consequence.Skip;
                            bool bDisqualifyRule = consequence.Disqualify;

                            if (bMutualExclusiveExe && bSkippingRule)
                            {
                                string errMsg = string.Format("PolicyId={0}. Hm, we are having a skip (if-then) and " +
                                                       "mutual exclusive execution in the same price policy, this is not supported",
                                                       policyId.ToString());
                                Tools.LogErrorWithCriticalTracking(errMsg);
                                throw new CBaseException(ErrorMessages.Generic, errMsg);
                            }

                            // Read it as: !( bSkippingRule || ( bMutualExclusiveExe && bDisqualifyRule ) )
                            if (!bSkippingRule && !(bMutualExclusiveExe && bDisqualifyRule)) // TODO: We can cache this info as "Needed for 1st phase" to optimize perf by avoid SetNewLoanData() calls (see 10-20 lines above) for the ones we don't need
                            {
                                continue;
                            }

                            // When we get to this point, it's either skipping rule or the policy is mutual exclusive and the rule is disqualifying.

                            // TODO: What to do if the mutual exclusive execution policy results in insufficient data??
                            // Answer: The entire program is "undefined" (=insufficient data), we don't need to worry about it here.
                            // What about branching mutual exclusive and if-then-else feature force some 
                            // branch no need to have sufficient data, right now since we are not executing in the tree order, this is
                            // not avoidable. Technically we should execute it in the depth-first-search from top to bottom, however
                            // there is no sibling order guarantee unless we start loading up policies based on Name sorting
                            // Well, we can save the list of policies require data loan file doesn't have && is not MutualExclusiveExe 
                            // to the back. After other policies get a chance to process, we'll get to these saved ones, if 
                            // they are not skipped by then we'll know for sure that the loan program really needs to know the 
                            // data to accurately determine qualification. Hm, not in this loop though, as only special ones
                            // are executed in this loop, namely the skipping rule and mutual exclusive exe. We are not conclusive
                            // about this yet, just have to make the whole thing "insufficient data" for now.

                            MyCondition condition = rule.Condition;
                            bool bEvalVal; // The value that the condition is evaluated to

                            try
                            {
                                // PERFORMANCE: We do not cache invariant result here in phase 1

                                // 04/15/09 mf - OPM 25872
                                StringBuilder subDebugInfo = new StringBuilder();
                                if (m_runOptions.UseQbc && rule.QBC != E_sRuleQBCType.Legacy)
                                {
                                    // We do not care which borrower causes the disqual of the m-e branch
                                    bEvalVal = EvalWithQBC(rule, xmlDocEval, m_loanFileData, stack, m_applications, subDebugInfo, IsLogDebugInfo);
                                }
                                else
                                {
                                    if (m_runOptions.UseQbc)
                                        m_applications.SetPricingContext(m_applications.PrimaryBorrower.Application.AppIndex, E_aBorrowerCreditModeT.Both);

                                    bEvalVal = condition.Evaluate(xmlDocEval, m_loanFileData.sSymbolTableForPriceRule, stack, subDebugInfo, IsLogDebugInfo);
                                }
                                if (IsLogDebugInfo && (bEvalVal || !ConstSite.PricingCompactLogEnabled))
                                {
                                    LogDebug("    Eval: PolicyId:" + policyId + ", RuleId:" + rule.RuleId + ", Rule Desc:[" + rule.Description + "]. Result: " + bEvalVal);
                                    LogDebug(subDebugInfo.ToString());
                                }

                                if (bEvalVal == false)
                                    continue;

                            }
                            catch (Exception ex)
                            {   // We are dealing with a loan program requiring data that the loan file doesn't have
                                bEvalVal = false; // try to flush out as many errors as possible, giving it false won't skip over anything if insufficient data encountered

                                string regTestMsg = string.Format("Error encountered in phase 1 for rule with id={0}, title={1}, pricepolicy id={2}, pricepolicy title={3}. Error msg={4}",
                                        rule.RuleId.ToString(), rule.Description, policyId, pricePolicy.PricePolicyDescription, ex.Message);
                                PrepareInsufficientInfoStatus(ex, regTestMsg, rule.RuleId, policyId);

                                return; // We can stop the execution now, the program's result is "insufficient data"
                            }

                            bool bSkipThisBranch = false;

                            if (bSkippingRule)
                                bSkipThisBranch = true;

                            if (bDisqualifyRule)
                            {
                                thisPolicyStatus = E_EvalStatus.Eval_Ineligible;

                                if (bMutualExclusiveExe)
                                    bSkipThisBranch = true;
                                else
                                {
                                    // Even thisPolicyStatus is Eval_Ineligible, we can not say the loan is ineligible.
                                    // Reason : maybe one of this policy's ancestors is a "skip policy", 
                                    //          therefore we don't consider this rule.

                                }
                            }

                            if (bSkipThisBranch)
                            {
                                // We found a skipper
                                // Anyone has policy 'policyId' as a parent will be skipped as well.
                                m_parentInfo.AddDescendantsToSet(policyId, skipList);
                                //Tools.LogWarning( string.Format( "****** Mark to skip policy {0} either because it has skipping rule or itself exclusive branch failed.", priceTest.PricePolicyDescription ) );
                                skipList.Add(policyId); // skipping the policy group itself too, not just its children
                            }

                        } // for ...rules

#if( DEBUG )
                        Tools.Assert((E_EvalStatus.Eval_InsufficientInfo != m_evalStatus && E_EvalStatus.Eval_Ineligible != m_evalStatus) || m_PmiDisqual, "Assert: E_EvalStatus.Eval_InsufficientInfo != m_evalStatus && E_EvalStatus.Eval_Ineligible != m_evalStatus. m_evalStatus is not accurate at this point, don't expect to set anything else other than 'elligible' because we don't execute them in the right order to honor skipping");
#endif// DEBUG

                        if (bMutualExclusiveExe && E_EvalStatus.Eval_Eligible == thisPolicyStatus)
                        {   // We found a MutualExlusiveExe branch that returns true, now we need to skip all of its siblings
                            // by marking the parent id (group id) as done.

                            mutualExclExeList[pricePolicy.ParentPolicyId] = null;
                        }
                    }

                    #endregion // Phase 1 to handle the If-Else (Skip) && Mutual Exclusive Execution statement for a policy and its children

                    /*
                if m_evalStatus from phase 1 is not an accurate result because if it's:
                    E_EvalStatus.Eval_Eligible: This doesn't tell us much, we only run only a few policies in phase 1. An "eligible"
                        must require all policies (non-skipping) to be run and return "eligible".
                    E_EvalStatus.Eval_Ineligible: This is not accurate because we might get this status from a branch that will
                        be skipped in phase 2.
                    E_EvalStatus.Eval_InsufficientInfo: Should get here at all.
                }
                */
                }

                // 2/5/2010 dd - Save the QScore so we can return in result. OPM 44242. At this point QScore already available and set in sCreditScoreLpeQual.
                m_qScore = m_loanFileData.sCreditScoreLpeQual;


                using (PerformanceStopwatch.Start("Phase 2"))
                {
                    LogInfo("");
                    LogInfo("Execute Phase 2 - The Main Computation Loop");
                    if (m_runOptions.IsUsePricingSummaryDebug)
                    {
                        m_debugSummary.DumpMainExecution(m_loanFileData.sSymbolTableForPriceRule.GetValues());
                    }
                    #region Phase 2 The main computation loop

                    // OPM 109393.  When PMI engine fails to pair Conv with MI, need to keep the disqual.
                    if (m_PmiDisqual == false)
                        m_evalStatus = E_EvalStatus.Eval_Eligible;

                    maxYsp = lLpeFeeMin;
                    bool bUseMaxYspAdjRules = lLpeFeeMinAddRuleAdjBit;

                    priceGroupDetails = m_runOptions.GetPriceGroupDetailsFor(lLpTemplateId);
                    if (m_runOptions.PriceGroupId != Guid.Empty)
                    {
                        if (priceGroupDetails == null)
                        {
                            // program can not come to this point.
                            Tools.LogBug(string.Format("Bug : m_runOptions.GetPriceGroupDetailsFor( LoanProgramId = {0} ) == null with PriceGroupId = {1}",
                                lLpTemplateId, m_runOptions.PriceGroupId));
                            PrepareInsufficientInfoStatus("No Price Group Details for " + lLpTemplateNm, "No Price Group Details for " + lLpTemplateNm, "No Price Group Details for " + lLpTemplateNm);
                            return;
                        }

                        maxYsp += priceGroupDetails.PriceGroupMaxYspAdj; // adding meaning reducing the maximum of the ysp broker can have

                        m_feeDeltaHidden += priceGroupDetails.PriceGroupProfitMargin; // adding meaning allow lender to keep the profit margin
                        m_rateDeltaHidden += priceGroupDetails.PriceGroupRateMargin;
                        m_marginDeltaHidden += priceGroupDetails.PriceGroupMarginMargin;

                        LogInfo("    PriceGroupMaxYspAdj:" + priceGroupDetails.PriceGroupMaxYspAdj + ", PriceGroupProfitMargin:" + priceGroupDetails.PriceGroupProfitMargin
                            + ", PriceGroupRateMargin:" + priceGroupDetails.PriceGroupRateMargin + ", PriceGroupMarginMargin:" + priceGroupDetails.PriceGroupMarginMargin);
                    }

                    // OPM 365561
                    // We need to have persisting adjustments pull from the loan file as if 
                    // they came from pricing.  If this pricing is chosen, they will be kept.
                    this.AddPersistAdjustments();

                    priceAdjustRecordArgs = new PriceAdjustRecord.Args(xmlDocEval, m_loanFileData.sSymbolTableForPriceRule, m_runOptions, this, cachResult, stack);

                    // PERF: QP never exposes stips/conditions, so we do not waste execution on rules that only stip.
                    priceAdjustRecordArgs.IgnoreStips = m_loanFileData.sIsQuickPricerLoan;

                    // This is what I'd actually like to do here, but a bit more refactor than should be done for hotfix is needed:  
                    // priceAdjustRecordArgs.IgnoreStips = (m_runOptions.LpeTaskT == LendersOffice.DistributeUnderwriting.E_LpeTaskT.RunEngine && IsTotalOrH4HSpecialProgram == false);
                    // (The non-quick pricing webservice runs in RunEngine/GetResult mode, but actually does parse stips/conditions.)

                    if (IsLogDetailDebugInfo)
                    {
                        string selectRateInfo = string.Format("Step 1: selected rate = {0}, point = {1}", m_loanFileData.sNoteIR.ToString("0.000"), m_loanFileData.sBrokComp1Pc.ToString("0.000"));
                        m_adjDescsHidden.Add(new CAdjustItem(selectRateInfo));
                    }

                    bool[] needUpdate = new bool[] { false, false };
                    int dtiRuleCounter = 0;
                    PriceAdjustRecord finalAdjust = new PriceAdjustRecord(false);

                    try
                    {
                        for (int step = 0; step < 2; step++)
                        {
                            //  step 0 : process invariant or nonDtiDepend rules
                            //  step 1 : process dti rules  
                            bool processDtiOnly = (step == 1);
                            needUpdate[step] = true;

                            if (IsLogDebugInfo)
                            {
                                if (step == 0)
                                {
                                    LogDebug("    Process Invariant or Non-DTI depend rules");
                                }
                                else if (step == 1)
                                {
                                    LogDebug("    Process DTI rules");

                                }
                            }
                            #region inside phase 2, travel policies for adjusting rate, fee ...
                            if (bGetAllReasons || m_evalStatus == E_EvalStatus.Eval_Eligible)
                            {
                                foreach (Guid policyId in applicablePolicies)
                                {

                                    if (skipList.Contains(policyId))
                                    {
                                        if (!ConstSite.PricingCompactLogEnabled)
                                        {
                                            LogInfo("    Skip PolicyId:" + policyId);
                                        }

                                        continue;
                                    }

                                    MyPricePolicy pricePolicy = LoanProgramSet.GetPricePolicy(policyId);
                                    PriceAdjustRecord.Compute(pricePolicy, ref priceAdjustRecordArgs, finalAdjust, processDtiOnly, m_applications, this.m_debugInfo, m_loanFileData.sPricingModeT, IsLogDebugInfo, m_runOptions.IsGetAllReasons);

                                    if (finalAdjust.Disqualify)
                                    {
                                        if (E_EvalStatus.Eval_Eligible == m_evalStatus) // right now, we just take the first one.
                                        {
                                            m_evalStatus = E_EvalStatus.Eval_Ineligible;
                                            LogInfo("    => *** 1st disqualify policy:" + policyId + Environment.NewLine);
                                        }
                                    }

                                    if (finalAdjust.m_disallowLock)
                                    {
                                        m_lockDisabledReason = finalAdjust.m_disallowLockReason;
                                    }


                                }// for ...policies
                            }
                            #endregion inside phase 2, travel policies for adjusting rate, fee ...

                            needUpdate[step] = false;
                            ReceiveAndClearAccumulateData(ref maxYsp, finalAdjust, (step == 1 || UsePerRatePricing) /* receiveAll */  );

                            // For OPM 43151, if a front end maxysp was set, we set maxYsp to be the MAX (FrontEndYsp, BackEndYsp)

                            if (step == 0)
                                m_originalBackEndMaxYsp = maxYsp;

                            if (m_maxFrontEndYsp.HasValue)
                            {
                                maxYsp = Math.Max(maxYsp, m_maxFrontEndYsp.Value);
                            }

                            if (UsePerRatePricing)
                                break; // For per-rate, we apply DTI rules by rate option. Not here.

                            if (bImproveParRate == false)
                                continue;

                            if (step == 0 && !IsTotalOrH4HSpecialProgram) // TOTAL pricing makes no adjustments and has no rates.
                            {
                                // MaxYsp = (Program MaxYsp) + (PriceGroup MaxYsp) + (MaxYspAdj from rules)

                                // set par rate option : Case 16907 Improve estimate of par rate that DTI depends on
                                IRateItem keyOption = m_loanFileData.ApplyParRateOption(this, m_rateDeltaDisclosed + m_rateDeltaHidden, m_feeDeltaDisclosed + m_feeDeltaHidden, maxYsp, m_runOptions);

                                if (IsLogDetailDebugInfo)
                                {
                                    string dtiExplain;

                                    if (keyOption != null)
                                        dtiExplain = string.Format("Step 2 : par rate = {0} + {1} = {2}, point = {3} + {4} = {5} for {6} DTI-rules",
                                            keyOption.Rate, // 0
                                            m_rateDeltaDisclosed + m_rateDeltaHidden, //1 
                                            m_loanFileData.sNoteIR,  // 2
                                            keyOption.Point, // 3
                                            m_feeDeltaDisclosed + m_feeDeltaHidden, //4
                                            m_loanFileData.sBrokComp1Pc, //5
                                            dtiRuleCounter);// 6
                                    else
                                        dtiExplain = string.Format("Step 2 : no modify par rate for {2} DTI-rules when rateAdj = {0}, feeAdj = {1}",
                                            m_rateDeltaDisclosed + m_rateDeltaHidden,
                                            m_feeDeltaDisclosed + m_feeDeltaHidden, dtiRuleCounter);

                                    m_adjDescsHidden.Add(new CAdjustItem(dtiExplain));
                                }
                            }

                            //if( dtiRuleCounter == 0 ) break;

                        } // end of while
                    }
                    catch (PricingCalcException exc)
                    {
                        for (int step = 0; step < 2; step++)
                            if (needUpdate[step])
                            {
                                ReceiveAndClearAccumulateData(ref maxYsp, finalAdjust, true);
                                break;
                            }

                        PrepareInsufficientInfoStatus(exc.LogMsg, exc.ErrorMsg, exc.ErrorMsg4Developer);
                        return; // stop the execution immediately.
                    }

                    Tools.Assert(E_EvalStatus.Eval_InsufficientInfo != m_evalStatus,
                        "Assert: E_EvalStatus.Eval_InsufficientInfo != m_evalStatus. Insufficient data scenario should return long ago");

                    if (IsTotalOrH4HSpecialProgram)
                    {
                        // TOTAL/H4H has no rates/adjustments.  It only disquals and adds stipulations
                        // Add the appropriate auto-stips, then quit pricing.  
                        this.AddCreditStips();

                        // OPM 105626.  Provide user with more information.

                        Dictionary<string, string> replacements = new Dictionary<string, string>()
                     {
                        { "<sFinalLAmt>", m_loanFileData.sFinalLAmt_rep },
                        { "<sApprVal>", m_loanFileData.sApprVal_rep },
                        { "<sTotalScoreAssetsAfterClosing>", m_loanFileData.sTotalScoreAssetsAfterClosing_rep },
                        { "<sRetirementAssetTotal>", m_loanFileData.sRetirementAssetTotal_rep },
                     };

                        foreach (CStipulation reason in m_reasonArray)
                        {
                            reason.UpdateDescriptionForTotal(replacements);
                        }
                        return;
                    }

                    // PER OPM 459147, we round the accumulated maxYSP because it will
                    // later be compared with the rounded fees.
                    if (m_maxFrontEndYsp.HasValue)
                    {
                        m_maxFrontEndYsp = Math.Round(m_maxFrontEndYsp.Value, RateDecimalDigits);
                    }
                    maxYsp = Math.Round(maxYsp, RateDecimalDigits);
                    m_originalBackEndMaxYsp = Math.Round(m_originalBackEndMaxYsp, RateDecimalDigits);

                    if (IsLogDebugInfo)
                    {
                        // debug information
                        string frontEndMaxYsp = m_maxFrontEndYsp.HasValue ? m_maxFrontEndYsp.ToString() : "NONE";

                        string debugInformation = "Phase 2' result : " +
                            "* m_maxDti                      = " + m_maxDti.ToString() +
                            "* m_marginDeltaHidden           = " + m_marginDeltaHidden.ToString() +
                            "* m_marginDeltaDisclosed        = " + m_marginDeltaDisclosed.ToString() +
                            "* m_feeDeltaHidden              = " + m_feeDeltaHidden.ToString() +
                            "* m_feeDeltaDisclosed           = " + m_feeDeltaDisclosed.ToString() +
                            "* m_rateDeltaHidden             = " + m_rateDeltaHidden.ToString() +
                            "* m_rateDeltaDisclosed          = " + m_rateDeltaDisclosed.ToString() +
                            "* m_teaserRDeltaHidden          = " + m_teaserRDeltaHidden.ToString() +
                            "* m_teaserRDeltaDisclosed       = " + m_teaserRDeltaDisclosed.ToString() +
                            "* m_qualRDeltaHidden            = " + m_qualRDeltaHidden.ToString() +
                            "* m_qualRDeltaDisclosed         = " + m_qualRDeltaDisclosed.ToString() +
                            "* m_marginBaseAllROptions       = " + m_marginBaseAllROptions.ToString() +
                            "* m_isMarginBaseAllRateOptions  = " + m_isMarginBaseAllRateOptions.ToString() +
                            (UsePerRatePricing ? "" : ("* maxYsp  (frontEnd, backEnd)   = " + maxYsp.ToString() + " ( " + frontEndMaxYsp + ", " + m_originalBackEndMaxYsp + " )"));

                        if (IsLogDetailDebugInfo)
                        {
                            m_adjDescsHidden.Add(new CAdjustItem(debugInformation));
                        }

                        LogDebug(debugInformation);
                    }


                    #endregion // Phase 2
                }
                // 9/20/2010 dd - Revert back the original value of sProd3rdPartyUwResultT.
                m_loanFileData.sProd3rdPartyUwResultT = old_sProd3rdPartyUwResultT;
                using (PerformanceStopwatch.Start("Putting Result Together"))
                {
                    LogInfo("");
                    LogInfo("Puting Result Together");
                    #region Putting together the result set
                    IRateItem[] baseRates = null;

                    // 2/17/2010 dd - For performance reason, we do not generate stipulation list when run under pricing mode.
                    // 3/11/2014 dd - OPM 172788 Always return stipulation even if it is running pricing. PML0225 is run pricing through webservice and
                    //                want stipulations to match.
                    if (m_runOptions.LpeTaskT != LendersOffice.DistributeUnderwriting.E_LpeTaskT.RunEngine || m_loanFileData.BrokerDB.CustomerCode == "PML0225")
                    {

                        using (PerformanceStopwatch.Start("Add Stipulations"))
                        {

                            if (m_runOptions.PriceGroupId != Guid.Empty)
                            {
                                CAdjustItem feeAdjDesc = new CAdjustItem(m_losConvert.ToRateString(priceGroupDetails.PriceGroupMarginMargin),  // margin adj
                                    m_losConvert.ToRateString(priceGroupDetails.PriceGroupRateMargin),    // rate adj
                                    m_losConvert.ToRateString(priceGroupDetails.PriceGroupProfitMargin),  // fee adj
                                    "", // qrate
                                    "", // teaser rate
                                    string.Format("Price Group (\"{0}\")", m_runOptions.PriceGroupNm), "", true /*IsLenderAdj*/);
                                m_adjDescsHidden.Add(feeAdjDesc);
                            }

                            // 04/15/09 mf - OPM 25872
                            this.AddCreditStips();

                            this.AddLendingLicenseStips(); // 6/15/2009 dd - OPM 2703 - I am adding stipulation warning if loan officer does not have lending license.
                        }
                    }
                    if (m_isMarginBaseAllRateOptions)
                    {
                        decimal baseMargin = m_marginBaseAllROptions + m_marginDeltaDisclosed + m_marginDeltaHidden;
                        m_loanFileData.sRAdjMarginR = baseMargin;
                        lRAdjMarginR = baseMargin; // this will affect product applying.
                    }
                    using (PerformanceStopwatch.Start("GetRateSheetWithDeltas"))
                    {
                        baseRates = GetRateSheetWithDeltas(m_loanFileData.sRLckdDaysFromInvestor, m_loanFileData.sProdPpmtPenaltyMon, m_loanFileData.sIOnlyMon);
                        // 4/11/2011 dd - Make sure base rates are order in descending order.
                        baseRates = baseRates.OrderByDescending(o => o.Rate).ToArray<IRateItem>();
                        if (!m_runOptions.IsRateOptionsWorseThanLowerRateShown)
                        {
                            // 7/1/2009 dd - OPM 24899 Filter out higher rate with worst price.
                            List<IRateItem> newList = new List<IRateItem>();
                            decimal lastPoint = decimal.MaxValue;
                            for (int i = baseRates.Length - 1; i >= 0; i--)
                            {
                                IRateItem o = baseRates[i];
                                if (o.Point < lastPoint)
                                {
                                    newList.Add(o);
                                    lastPoint = o.Point;
                                }
                            }
                            newList.Reverse();
                            baseRates = newList.ToArray();
                        }
                    }
                    int count = baseRates.GetLength(0);

                    if (count > 20)
                    {
                        // 3/6/2017 - dd - Since we are doing per rate option pricing, therefore a large number of rate options in single program
                        //                 will cause a pricing for the program to slow down. On average a loan program has about 15-20 rates.
                        //                 Have a warning will help us to let client know to trim down the rate to improve their overall pricing.
                        Tools.LogWarning($"lLpTemplateId=[{this.lLpTemplateId}] - lLpTemplateNm=[{this.lLpTemplateNm}] has too many rates. Rates Count=[{count}]");
                    }

                    decimal minYsp = lLpeFeeMax;

                    if (minYsp > 0
                        && m_loanFileData.sGfeIsTPOTransaction == true
                        && m_loanFileData.BrokerDB.OriginatorCompensationApplyMinYSPForTPOLoan == true
                        && m_loanFileData.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                        )
                    {
                        // 2/23/11 mf. OPM 62085. Section 6.2.4. Lender is not compliant if borrower is paying
                        // points and originator is getting compensated by lender.
                        minYsp = 0;
                    }

                    bool bSnapupFee = m_runOptions.RoundUpLpeFee;
                    decimal snapupFeeInterval = m_runOptions.RoundUpLpeFeeInterval;
                    bool bSnapUpRate = m_runOptions.IsSnapupRate1Eighth;
                    bool bApplyCompensationBeforeRounding = m_loanFileData.BrokerDB.IsApplyPricingEngineRoundingAfterLoComp;

                    if (m_loanFileData.sPricingModeT == E_sPricingModeT.InternalInvestorPricing || m_loanFileData.sPricingModeT == E_sPricingModeT.EligibilityInvestorPricing)
                    {
                        // 1/16/2011 dd - Base on the spec for OPM 30649, when run pricing in investor mode, do not round result.
                        bSnapupFee = false;
                        bSnapUpRate = false;
                    }
                    if (bSnapupFee && !bApplyCompensationBeforeRounding)
                    {
                        maxYsp = Snapup(maxYsp, snapupFeeInterval);
                        minYsp = Snapup(minYsp, snapupFeeInterval);
                    }




                    #region Loop to see if there is at least one rate option is <= minYsp
                    // Here are the cases related to ysp:
                    // if maxYsp > minYsp: this is a weird case # 6748, maxYsp <= minYsp is the typical case. In this situation, disqualify and display rates at preview only
                    // else if ysp < maxYsp < minYsp: display just the last rateoption and make the ysp to be maxYsp. External case 13904

                    m_bMinMaxFeeViolation = true;

                    if (UsePerRatePricing)
                    {
                        // For per-rate, we cannot filter YSP yet. We do not know the final points of each rate.
                        // Additionally, per rate pricing can modify maxYSP at the rate-option level--it is not constant
                        m_bMinMaxFeeViolation = false;
                    }
                    else // Not per rate
                    {
                        if (maxYsp <= minYsp) // internal opm#6748, if maxYsp > minYsp, then we have minMaxFeeViolation=true, disqualify and display rates at the preview only
                        {
                            for (int i = count - 1; i >= 0; --i)
                            {
                                IRateItem baseOption = baseRates[i];

                                decimal r = baseOption.Rate + m_rateDeltaDisclosed + m_rateDeltaHidden;
                                if (bSnapUpRate)
                                    r = SnapupOneEighth(r);

                                m_loanFileData.sNoteIR = r;

                                decimal ysp = baseOption.Point + m_feeDeltaDisclosed + m_feeDeltaHidden;

                                if (bSnapupFee && !bApplyCompensationBeforeRounding)
                                    ysp = Snapup(ysp, snapupFeeInterval);

                                // Note: users can always accept higher fee if they want.  More max means users get more rebate. They
                                // shouldn't be disqualified if they want to sacrifice some rebate here.  If ysp < maxYsp we would just 
                                // display one rateoption and have it to be maxYsp. 
                                if (ysp <= minYsp)
                                {
                                    m_bMinMaxFeeViolation = false; // At least 1 rate option doesn't violate the range check.
                                    break;
                                }
                            } // for loop, looping through the array of base rates
                        }
                    }
                    #endregion // Loop to see if there is at least one rate option is <= minYsp

                    decimal lastFee = -1999;

                    string sTerm = m_loanFileData.sTerm_rep;
                    string sDue = m_loanFileData.sDue_rep;
                    Guid _lLpTemplateId = this.lLpTemplateId;
                    string _lLpTemplateNm = this.lLpTemplateNm;
                    string _lLpInvestorNm = this.lLpInvestorNm;

                    ArrayList list = new ArrayList();
                    decimal rateDelta = m_rateDeltaDisclosed + m_rateDeltaHidden;
                    decimal feeDelta = m_feeDeltaDisclosed + m_feeDeltaHidden;
                    decimal marginDelta = m_marginDeltaDisclosed + m_marginDeltaHidden;
                    decimal teaserRDelta = m_teaserRDeltaDisclosed + m_teaserRDeltaHidden;
                    decimal qrateDelta = m_qualRDeltaDisclosed + m_qualRDeltaHidden;

                    decimal _loanOfficerCommissionPointOfGrossProfit = 0;
                    decimal _loanOfficerCommissionPointOfLoanAmount = 0;
                    decimal _loanOfficerCommissionMinBase = 0;
                    decimal _sOriginatorCompensationBaseAmt = 0;
                    decimal _sOriginatorCompensationTotalAmount = 0;

                    if (!m_loanFileData.sHasOriginatorCompensationPlan)
                    {
                        // 3/25/2011 dd - If there is no originator compensation plan set yet then we temporary apply the compensation
                        // from the current assigned orignator to calculate the total compensation amount.
                        // WARNING: This is only temporary settings during pricing, the compensation should not be save here.
                        Tools.Assert(m_loanFileData.DataState == E_DataState.InitLoad, "Only allow to temporary set compensation plan in InitMOde");
                        if (!m_loanFileData.BrokerDB.ApplyCompWhenDocCheckDateEntered)
                        {
                            if (m_loanFileData.sEmployeeLoanRepId != Guid.Empty)
                            {
                                // If there is an LO, go ahead and get their comp plan
                                m_loanFileData.ApplyCompensationFromCurrentOriginator();
                            }
                            else if (m_loanFileData.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                            {
                                // OPM 237202. mf.  There is no LO, but under this condition, we can take the comp from the OC directly.
                                // For wholesale lenders/loans, the process can involve inital pricing before the LO is known via QP.
                                // This will not be set via submission.
                                m_loanFileData.ApplyCompensationFromCurrentOriginatingCompany();
                            }
                        }
                    }

                    if (m_loanFileData.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                    {
                        // 3/25/2011 dd - Only calculate performance account amount when compensation is lender paid.
                        EmployeeDB employee = m_loanFileData.sEmployeeLoanRepEmployeeDB;
                        if (employee != null)
                        {
                            // 3/8/2011 dd - Dollar bank amount need to key off the Gross Profit field on assign loan officer.
                            _loanOfficerCommissionPointOfGrossProfit = employee.CommissionPointOfGrossProfit;
                            _loanOfficerCommissionPointOfLoanAmount = employee.CommissionPointOfLoanAmount;
                            _loanOfficerCommissionMinBase = employee.CommissionMinBase;
                        }

                        _sOriginatorCompensationBaseAmt = m_loanFileData.sOriginatorCompensationBaseAmt;
                        _sOriginatorCompensationTotalAmount = m_loanFileData.sOriginatorCompensationTotalAmount;

                    }

                    // 3/26/2011 mf. OPM 63535.  Basically, when we are going to modify the result point 
                    // for originator comp, we need to apply the add/substract (opms 64253/64256) before determine par rate.
                    decimal appliedOriginatorCompPoint = 0;

                    decimal sOriginatorCompPoint = m_loanFileData.sOriginatorCompPoint; // 3/28/2011 dd - Cache this value.
                    if (m_loanFileData.sIsQualifiedForOriginatorCompensation && m_loanFileData.sHasOriginatorCompensationPlan)
                    {
                        if (m_loanFileData.BrokerDB.IsUsePriceIncludingCompensationInPricingResult
                            && m_loanFileData.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                            && m_loanFileData.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees)
                        {
                            // In result we are adding to point value
                            appliedOriginatorCompPoint = m_loanFileData.sOriginatorCompPoint;
                        }
                    }

                    PerOptionAdjustRecord perOptionAdjust = null;

                    if (UsePerRatePricing)
                    {
                        // OPM 471515, We need to recycle the rate adjust object
                        // so we can merge previous adjustments into later ones.
                        perOptionAdjust = useRateOptionVariantMI 
                            ? m_RateVariantPmi.PerOptionAdjust
                            : new PerOptionAdjustRecord();

                        // OPM 170643 - If we chose an LPMI program earlier, need to reduce maxYSP.
                        if (m_lpmiAdjust.HasValue)
                        {
                            // For LPMI we have to reduce (add) from backend maxysp per 107734.
                            // Then re-calculate maxYsp
                            m_originalBackEndMaxYsp = m_originalBackEndMaxYsp + m_lpmiAdjust.Value;
                            maxYsp = m_originalBackEndMaxYsp;

                            if (m_maxFrontEndYsp.HasValue)
                            {
                                maxYsp = Math.Max(maxYsp, m_maxFrontEndYsp.Value);
                            }

                            if (m_runOptions.RoundUpLpeFee && !m_loanFileData.BrokerDB.IsApplyPricingEngineRoundingAfterLoComp)
                                maxYsp = Snapup(maxYsp, m_runOptions.RoundUpLpeFeeInterval);
                        }
                    }

                    // OPM 471515. With rate variant mi on, we delay CC until post-DTI for this option.
                    if (useRateOptionVariantMI
                        && calculateclosingCostInPML
                        && UseNewPml) // OPM 109662. AutoCC Option might be on at lender, but this user does not use new UI.
                    {
                        ExecuteClosingCost(ref feeServiceResult, ref appliedAutoCCTemplate);
                    }

                    decimal sLTotI = m_loanFileData.sLTotI;
                    int minYspFilterCounter = 0;

                    // Current design of pricing and closing cost applying does not work well together.
                    // We have to cache out these to allow pricing and CC template apply to work until 
                    // we can redesign so the CC cost apply and PP can set fee/discount without stomping on eachother.

                    decimal _sLOrigFPc = m_loanFileData.sLOrigFPc;
                    decimal __sLOrigFPc = m_loanFileData.sLOrigFPc;

                    if (appliedAutoCCTemplate && m_loanFileData.LoanProductMatchingTemplate != null)
                    {
                        if (m_loanFileData.LoanProductMatchingTemplate.cPricingEngineCostT == E_cPricingEngineCostT._801LoanOriginationFee
                            || m_loanFileData.LoanProductMatchingTemplate.cPricingEngineCreditT == E_cPricingEngineCreditT._801LoanOriginationFee)
                        {
                            //OPM 113193.
                            _sLOrigFPc = m_loanFileData.LoanProductMatchingTemplate.cLOrigFPc;
                            __sLOrigFPc = m_loanFileData.LoanProductMatchingTemplate.cLOrigFPc;
                        }
                    }
                    bool perRateParFound = false;
                    bool usesLOrigFPcBackup = calculateclosingCostInPML && UseNewPml && UsePerRatePricing;

                    bool dumpJsonFees = IsLogDebugInfo;
                    for (int i = count - 1; i >= 0; --i)
                    {
                        if (usesLOrigFPcBackup)
                            m_loanFileData.sLOrigFPc = __sLOrigFPc;

                        IRateItem baseOption = baseRates[i];

                        decimal r = baseOption.Rate + rateDelta;
                        if (bSnapUpRate)
                            r = SnapupOneEighth(r);

                        // OPM 48007
                        if (m_runOptions.SelectedRate != 0 && r == m_runOptions.SelectedRate)
                            m_rateRounding = r - (baseOption.Rate + rateDelta);

                        m_loanFileData.sNoteIR = r;
                        decimal ysp = baseOption.Point + feeDelta;

                        decimal origFee = ysp;
                        bool maxYspSnapped = false;

                        if (bSnapupFee)
                            ysp = Snapup(ysp, snapupFeeInterval);

                        if (IsLogDebugInfo)
                        {
                            LogDebug(string.Format("    BaseOption.Rate=[{0}], RateDelta=[{1}], sNoteIR=[{2}], RateRounding=[{3}], BaseOption.Point=[{4}], FeeDelta=[{5}], YSP=[{6}]",
                                baseOption.Rate,
                                rateDelta,
                                r,
                                m_rateRounding,
                                baseOption.Point,
                                feeDelta,
                                ysp));

                        }
                        // If there is at least one rate option within the min-max range (no violation), then we want to show only those valid ones
                        // else, we want to show all the rate options without altering or filtering them out. OPM case #1202
                        if (!m_bMinMaxFeeViolation)
                        {
                            if (ysp > minYsp
                                && !UsePerRatePricing // Per rate does not do minYSP check until end.
                                && m_loanFileData.sPricingModeT != E_sPricingModeT.InternalBrokerPricing // Internal pricing does not filter.
                                && m_loanFileData.sPricingModeT != E_sPricingModeT.InternalInvestorPricing
                                )
                                continue;    // unacceptable, skip           

                            if (ysp < maxYsp) // AKA "MaxYSP snapdown"
                            {
                                maxYspSnapped = true;
                                ysp = maxYsp; // external case 13904
                            }
                        }

                        // OPM 67572
                        if (bSnapupFee && bApplyCompensationBeforeRounding)
                            ysp = CalculatePriceWithoutComp(origFee, appliedOriginatorCompPoint, snapupFeeInterval);

                        // OPM 48007
                        if (!UsePerRatePricing && m_runOptions.SelectedRate != 0 && r == m_runOptions.SelectedRate)
                            m_feeRounding = ysp - (baseOption.Point + feeDelta);

                        // This would help us avoid showing many rate options with the same fee. OPM# 1821.
                        // Because we are iterating from lowest rate to highest rate, the lowest rate will be shown
                        // among the ones with the same fee.

                        // 4/8/2011 dd - OPM 65160 - We do not want to filter out any rate even if they have same point.
                        lastFee = ysp;

                        using (PerformanceStopwatch.Start("Set LoanFile Data and Create CApplicantRateOption"))
                        {
                            if (usesLOrigFPcBackup)
                                _sLOrigFPc = m_loanFileData.sLOrigFPc;

                            // Snapup again as it could get adjusted to maxYsp or something else in the future when this
                            // block of code gets changed.
                            using (PerformanceStopwatch.Start("Block 1"))
                            {
                                m_loanFileData.sLOrigFPc = ysp;

                                if (!m_isMarginBaseAllRateOptions)
                                    m_loanFileData.sRAdjMarginR = baseOption.Margin + marginDelta;

                                if (IsOptionArm)
                                    m_loanFileData.sOptionArmTeaserR = baseOption.TeaserRate + teaserRDelta;
                                else
                                    m_loanFileData.sOptionArmTeaserR = 0;

                                if (!ConstStage.EnableLegacyQualRateCalculation &&
                                    (this.lQualRateCalculationT != QualRateCalculationT.FlatValue ||
                                    this.lQualRateCalculationFieldT1 != QualRateCalculationFieldT.LpeUploadValue))
                                {
                                    var calculationOptions = new QualRateCalculationOptions(this, this.m_loanFileData.sNoteIR, baseOption.Margin + marginDelta);
                                    this.m_loanFileData.sQualIR = Tools.CalculateQualRate(calculationOptions);
                                    this.m_loanFileData.sQualIRLckd = false;
                                }
                                else if (lHasQRateInRateOptions)
                                {
                                    this.m_loanFileData.sQualIR = baseOption.QRateBase + qrateDelta;
                                    this.m_loanFileData.sQualIRLckd = true;
                                }
                                else
                                {
                                    this.m_loanFileData.sQualIR = 0;
                                    this.m_loanFileData.sQualIRLckd = true;
                                }

                                this.m_loanFileData.sUseQualRate = this.lHasQRateInRateOptions;
                                this.m_loanFileData.sQualRateCalculationT = this.lQualRateCalculationT;
                                this.m_loanFileData.sQualRateCalculationFieldT1 = this.lQualRateCalculationFieldT1;
                                this.m_loanFileData.sQualRateCalculationFieldT2 = this.lQualRateCalculationFieldT2;
                                this.m_loanFileData.sQualRateCalculationAdjustment1 = this.lQualRateCalculationAdjustment1;
                                this.m_loanFileData.sQualRateCalculationAdjustment2 = this.lQualRateCalculationAdjustment2;

                                m_loanFileData.InvalidateCache();
                            }

                            PriceAdjustRecord thisRateAdjust = null;
                            decimal? sTransNetCashUsedForKeywords = null;
                            int iteration = 0;
                            int iterationLimit = ConstStage.PerRatePricingIterationLimit;
                            string rateDebugStr = "";
                            if (UsePerRatePricing)
                            {

                                rateDebugStr =
                                    m_loanFileData.sNoteIR_rep
                                    + ", " + m_loanFileData.sLOrigFPc_rep + (maxYspSnapped ? "[X]" : "")
                                    + ". MaxYSP (f,b) = " + maxYsp
                                    + "(" + (m_maxFrontEndYsp.HasValue ? m_maxFrontEndYsp.ToString() : "NONE")
                                    + ", " + m_originalBackEndMaxYsp + ")"
                                    ;


                                // Here, we run the DTI rules that we skipped earlier for this current rate.
                                // Should use a new AdjustResult because we may not apply it

                                Dictionary<Guid, Tuple<bool, string>> hitRules = null;
                                Dictionary<Guid, Tuple<bool, string>> previousHitRules = null;

                                thisRateAdjust = null;
                                decimal thisRateMaxYsp = maxYsp;
                                // DTI keywords for 87644.
                                m_loanFileData.sNoteRateAfterLOCompAndRounding = r;
                                m_loanFileData.sRateOptionFinalPrice = m_loanFileData.sPointsAfterLOCompAndRounding = ysp + appliedOriginatorCompPoint;
                                m_loanFileData.sNoteRateBeforeLOCompAndRounding = baseOption.Rate + rateDelta;
                                m_loanFileData.sPointsBeforeLOCompAndRounding = (origFee < maxYsp) ? origFee : maxYsp;

                                // OPM 113089. 
                                if (m_loanFileData.sLTotI != 0)
                                {
                                    try
                                    {
                                        if (m_maxDti == 0) // aka. never set
                                        {
                                            m_loanFileData.sRateOptionExceedsMaxDti = false;
                                        }
                                        else
                                        {
                                            // It would be better to lazy calculate this to avoid extra calculation of sQualBottomR
                                            m_loanFileData.sRateOptionExceedsMaxDti = m_maxDti < m_loanFileData.sQualBottomR;
                                        }
                                    }
                                    catch (DivideByZeroException)
                                    {
                                        m_loanFileData.sRateOptionExceedsMaxDti = false;
                                    }
                                    catch (InvalidCalculationException)
                                    {
                                        m_loanFileData.sRateOptionExceedsMaxDti = false;
                                    }
                                }
                                else
                                {
                                    // No-income scenario
                                    m_loanFileData.sRateOptionExceedsMaxDti = false;
                                }
                                // Per rate pricing iteration.
                                for (iteration = 1 /*DW counting system*/; iteration < iterationLimit; iteration++)
                                {
                                    if (m_runOptions.IsUsePricingSummaryDebug)
                                    {
                                        m_debugSummary.DumpPerRate(m_loanFileData.sSymbolTableForPriceRule.GetValues());
                                    }
                                    previousHitRules = hitRules;
                                    perOptionAdjust.CurrentRateAdjust = thisRateAdjust = new PriceAdjustRecord(false);
                                    thisRateAdjust.DtiRuleResultCache = new Dictionary<Guid, Tuple<bool, string>>();


                                    // Apply CC discount per-rate
                                    using (PerformanceStopwatch.Start("Apply CC Discount per-rate"))
                                    {
                                        if (calculateclosingCostInPML && UseNewPml)
                                        {
                                            if (feeServiceResult != null)
                                            {
                                                decimal _ysp = m_loanFileData.sLOrigFPc;
                                                //av 6/19/2014 - We dont need to add compensation in this case
                                                if (m_loanFileData.sGfeOriginatorCompF == 0)
                                                {
                                                    _ysp += appliedOriginatorCompPoint;
                                                }

                                                // Pull these from result object if they are supposed to be set.
                                                m_loanFileData.sLOrigFMb = feeServiceResult.sLOrigFMb ?? 0;
                                                m_loanFileData.sLDiscntPc = feeServiceResult.sLDiscntPc ?? 0;
                                                m_loanFileData.sLDiscntFMb = feeServiceResult.sLDiscntFMb ?? 0;
                                                m_loanFileData.SetGFEDiscountForAutomaticClosingCosts(_ysp, ref _sLOrigFPc, feeServiceResult);
                                                m_loanFileData.InvalidateCache();

                                                if (m_loanFileData.sLenderCreditCalculationMethodT == E_sLenderCreditCalculationMethodT.CalculateFromFrontEndRateLock)
                                                {
                                                    // OPM 217420. mf.
                                                    m_loanFileData.LoadBrokerLockData();
                                                }
                                            }
                                            else if (m_loanFileData.LoanProductMatchingTemplate != null)
                                            {

                                                decimal _ysp = m_loanFileData.sLOrigFPc + appliedOriginatorCompPoint;

                                                CCcTemplateData matchingTemplate = m_loanFileData.LoanProductMatchingTemplate;

                                                if (appliedAutoCCTemplate)
                                                {
                                                    m_loanFileData.sLOrigFMb = matchingTemplate.cLOrigFMb;
                                                    m_loanFileData.sLDiscntPc = matchingTemplate.cLDiscntPc;
                                                    m_loanFileData.sLDiscntFMb = matchingTemplate.cLDiscntFMb;
                                                }
                                                else
                                                {
                                                    // OPM 113193
                                                    if (matchingTemplate.cPricingEngineCostT == E_cPricingEngineCostT._802CreditOrCharge
                                                        || matchingTemplate.cPricingEngineCreditT == E_cPricingEngineCreditT._802CreditOrCharge)
                                                    {
                                                        m_loanFileData.sLDiscntPc = matchingTemplate.cLDiscntPc;
                                                        m_loanFileData.sLDiscntFMb = matchingTemplate.cLDiscntFMb;
                                                    }

                                                    if (matchingTemplate.cPricingEngineCostT == E_cPricingEngineCostT._801LoanOriginationFee
                                                        || matchingTemplate.cPricingEngineCreditT == E_cPricingEngineCreditT._801LoanOriginationFee)
                                                    {
                                                        m_loanFileData.sLOrigFMb = matchingTemplate.cLOrigFMb;
                                                    }
                                                }

                                                m_loanFileData.SetGFEDiscountForAutomaticClosingCosts(_ysp, ref _sLOrigFPc, feeServiceResult);
                                                m_loanFileData.InvalidateCache();

                                                if (m_loanFileData.sLenderCreditCalculationMethodT == E_sLenderCreditCalculationMethodT.CalculateFromFrontEndRateLock)
                                                {
                                                    // OPM 217420. mf.
                                                    m_loanFileData.LoadBrokerLockData();
                                                }
                                            }
                                        }
                                    }

                                    sTransNetCashUsedForKeywords = null;

                                    // Calculate reserve months per-rate in new PML.
                                    // (Old pml uses user-input)
                                    if (UseNewPml)
                                    {
                                        using (PerformanceStopwatch.Start("Calculate sAvailReserveMonthsQual"))
                                        {
                                            bool needToCalculatesTransNetCash = !sTransNetCashUsedForKeywords.HasValue;
                                            decimal tmpsLOrigFPc = m_loanFileData.sLOrigFPc;
                                            decimal tmpsBrokComp1Pc = this.m_loanFileData.sBrokComp1Pc;

                                            if (needToCalculatesTransNetCash)
                                            {
                                                if (m_loanFileData.sLenderCreditCalculationMethodT == E_sLenderCreditCalculationMethodT.CalculateFromFrontEndRateLock)
                                                {
                                                    m_loanFileData.sBrokComp1Pc = m_loanFileData.sLOrigFPc;
                                                    m_loanFileData.LoadBrokerLockData();
                                                    m_loanFileData.InvalidateCache();
                                                }

                                                if (usesLOrigFPcBackup)
                                                {
                                                    m_loanFileData.sLOrigFPc = _sLOrigFPc;
                                                }

                                                m_loanFileData.StartCaching();
                                                sTransNetCashUsedForKeywords = m_loanFileData.sTransNetCash;
                                                m_loanFileData.StopCaching();
                                            }

                                            decimal nonNegativesTransNetCash = sTransNetCashUsedForKeywords.Value < 0 ? 0 : sTransNetCashUsedForKeywords.Value;

                                            try
                                            {
                                                decimal sMonthlyPmtQual = m_loanFileData.sMonthlyPmtQual;
                                                if (sMonthlyPmtQual > 0)
                                                {
                                                    // OPM 126520. OLD CALCULATION: [[Total Liquid Assets (all applications)] �[Borrower Paid Non-Financed Closing Costs (when total is greater than $0.00)] � Downpayment]/[PITIA Payment]
                                                    // New Calcuation: [[Tot. Liquid Assets all Apps] � [Max( $0.00 , sTransNetCash )]]/[Qualifying PITIA Payment]
                                                    m_loanFileData.sAvailReserveMonthsQual = ((m_loanFileData.sTotLiquidAssets - nonNegativesTransNetCash) / sMonthlyPmtQual);

                                                }
                                                else
                                                {
                                                    m_loanFileData.sAvailReserveMonthsQual = 0;
                                                }
                                            }
                                            catch (CBaseException)
                                            {
                                                // Too early to mark it as bad, because rate could be adjusted
                                                // to be non 0 in this iteration.
                                                m_loanFileData.sAvailReserveMonthsQual = 0;
                                            }

                                            // OPM 471515.
                                            // Need to store this so later mi iterations can access.
                                            if (useRateOptionVariantMI)
                                            {
                                                m_RateVariantPmi.SetReserves(m_loanFileData.sNoteIR, m_loanFileData.sAvailReserveMonthsQual);
                                            }

                                            if (needToCalculatesTransNetCash)
                                            {
                                                if (this.m_loanFileData.sLenderCreditCalculationMethodT == E_sLenderCreditCalculationMethodT.CalculateFromFrontEndRateLock)
                                                {
                                                    m_loanFileData.sBrokComp1Pc = tmpsBrokComp1Pc;
                                                    m_loanFileData.InvalidateCache();
                                                    m_loanFileData.LoadBrokerLockData();
                                                }


                                                if (usesLOrigFPcBackup)
                                                    m_loanFileData.sLOrigFPc = tmpsLOrigFPc;
                                            }
                                        }
                                    }

                                    if (recalculateMortgageInsurancePerRate)
                                    {
                                        // The monthly MI amount for FHA and USDA files depends on the
                                        // average outstanding balance, which varies based on the note
                                        // rate. Update the monthly MI amount per rate to ensure more
                                        // accurate DTI. See opm 225160 for more details. gf
                                        if (m_loanFileData.sLT == E_sLT.FHA)
                                        {
                                            m_loanFileData.sProMInsPe_rep = Math.Round((m_loanFileData.sProMInsR * m_loanFileData.sProMInsBaseAmt / 1200.00M), 2).ToString();
                                            m_loanFileData.sProMInsLckd = false;

                                            // If we are running for a selected rate, then we only want to update the monthly MI
                                            // we return for that rate.
                                            if (m_loanFileData.sNoteIR == m_runOptions.SelectedRate || m_runOptions.SelectedRate == 0)
                                            {
                                                m_mortgageInsuranceMonthlyPmt = m_loanFileData.sProMIns_rep;
                                                m_mortgageInsuranceSinglePmt = m_loanFileData.sProdFhaUfmip_rep;
                                            }
                                        }
                                        else if (m_loanFileData.sLT == E_sLT.UsdaRural)
                                        {
                                            m_loanFileData.sProMInsPe_rep = Math.Round((m_loanFileData.sProMInsR * m_loanFileData.sProMInsBaseAmt / 1200.00M), 2).ToString();
                                            m_loanFileData.sProMInsLckd = false;

                                            // If we are running for a selected rate, then we only want to update the monthly MI
                                            // we return for that rate.
                                            if (m_loanFileData.sNoteIR == m_runOptions.SelectedRate || m_runOptions.SelectedRate == 0)
                                            {
                                                m_mortgageInsuranceMonthlyPmt = m_loanFileData.sProMIns_rep;
                                                m_mortgageInsuranceSinglePmt = m_loanFileData.sProdUSDAGuaranteeFee_rep;
                                            }
                                        }
                                    }

                                    /**** Begin calculation for CashToBorrower keyword ****/
                                    // opm 233638 ejm - Calculation for CashToBorrower keyword. Done here so that sLOrigFPc gets reset before calculating sCashToBorrower.
                                    decimal currentsLOrigFPc = this.m_loanFileData.sLOrigFPc;
                                    decimal currentsBrokComp1Pc = this.m_loanFileData.sBrokComp1Pc;
                                    bool needTransNetCashForCtB = !sTransNetCashUsedForKeywords.HasValue;

                                    if (needTransNetCashForCtB)
                                    {
                                        if (m_loanFileData.sLenderCreditCalculationMethodT == E_sLenderCreditCalculationMethodT.CalculateFromFrontEndRateLock)
                                        {
                                            this.m_loanFileData.sBrokComp1Pc = this.m_loanFileData.sLOrigFPc;
                                            this.m_loanFileData.LoadBrokerLockData();
                                            this.m_loanFileData.InvalidateCache();

                                        }

                                        if (usesLOrigFPcBackup)
                                        {
                                            this.m_loanFileData.sLOrigFPc = _sLOrigFPc;
                                            this.m_loanFileData.InvalidateCache();
                                        }

                                        this.m_loanFileData.StartCaching();
                                        sTransNetCashUsedForKeywords = this.m_loanFileData.sTransNetCash;
                                        this.m_loanFileData.StopCaching();
                                    }

                                    // If you change this calculation, considering changing the same calculation in LFF's sCashToBorrower property.
                                    this.m_loanFileData.sCashToBorrowerForPricing = -1 * Math.Min(sTransNetCashUsedForKeywords.Value, 0);

                                    if (needTransNetCashForCtB)
                                    {
                                        // Reset everything back
                                        if (m_loanFileData.sLenderCreditCalculationMethodT == E_sLenderCreditCalculationMethodT.CalculateFromFrontEndRateLock)
                                        {
                                            this.m_loanFileData.sBrokComp1Pc = currentsBrokComp1Pc;
                                            this.m_loanFileData.InvalidateCache();
                                            this.m_loanFileData.LoadBrokerLockData(); // Reset the broker lock data.
                                        }

                                        if (usesLOrigFPcBackup)
                                        {
                                            this.m_loanFileData.sLOrigFPc = currentsLOrigFPc;
                                            this.m_loanFileData.InvalidateCache();
                                        }
                                    }
                                    /**** End calculation for CashToBorrower keyword ****/

                                    // Cannot process disquals until we have (otherwise) final per-rate result.
                                    priceAdjustRecordArgs.dtiDisqualProcessing = PriceAdjustRecord.E_DtiDisqualProcessing.IgnoreAllDisquals;

                                    foreach (Guid policyId in applicablePolicies)
                                    {

                                        if (skipList.Contains(policyId))
                                            continue;

                                        MyPricePolicy pricePolicy = LoanProgramSet.GetPricePolicy(policyId);

                                        try
                                        {
                                            PriceAdjustRecord.Compute(pricePolicy, ref priceAdjustRecordArgs, thisRateAdjust, true /*DTI ONLY */, m_applications, m_debugInfo, m_loanFileData.sPricingModeT, IsLogDebugInfo, m_runOptions.IsGetAllReasons);
                                        }
                                        catch (PricingCalcException exc)
                                        {
                                            PrepareInsufficientInfoStatus(exc.LogMsg, exc.ErrorMsg, exc.ErrorMsg4Developer);
                                            return; // stop the execution immediately.
                                        }
                                    }// for ...policies

                                    hitRules = thisRateAdjust.DtiRuleResultCache;


                                    // Pickup changes to maxYsp 
                                    decimal newBackEndMaxYsp = m_originalBackEndMaxYsp;
                                    if (thisRateAdjust.m_adjMaxYsp != 0)
                                    {
                                        var adjMaxYsp = Decimal.Round(thisRateAdjust.m_adjMaxYsp, RateDecimalDigits);
                                        newBackEndMaxYsp += adjMaxYsp;
                                    }

                                    var thisRateAdjustMaxFrontEndYsp = thisRateAdjust.m_maxFrontEndYsp;
                                    if (thisRateAdjustMaxFrontEndYsp != decimal.MinValue)
                                    {
                                        thisRateAdjustMaxFrontEndYsp = Decimal.Round(thisRateAdjustMaxFrontEndYsp, RateDecimalDigits);
                                    }

                                    decimal? newFrontEndMaxYsp = m_maxFrontEndYsp;

                                    if (thisRateAdjustMaxFrontEndYsp != decimal.MinValue
                                         && (!m_maxFrontEndYsp.HasValue || thisRateAdjustMaxFrontEndYsp > m_maxFrontEndYsp))
                                    {
                                        newFrontEndMaxYsp = thisRateAdjustMaxFrontEndYsp;
                                    }
                                    thisRateMaxYsp = (newFrontEndMaxYsp.HasValue) ? Math.Max(newBackEndMaxYsp, newFrontEndMaxYsp.Value) : newBackEndMaxYsp;

                                    // We need to apply the adjustments from thisRateAdjust to this base rate
                                    // for the next run. We add the DTI based adjustments from this iteration
                                    // to the original non-dti adjusted rate options.
                                    ApplyAdjustmentsAndRound_DynamicFee(baseOption
                                        , thisRateAdjust.m_rateDeltaHidden + thisRateAdjust.m_rateDeltaDisclosed + rateDelta
                                        , thisRateAdjust.m_feeDeltaHidden + thisRateAdjust.m_feeDeltaDisclosed + feeDelta
                                        , thisRateAdjust.m_marginDeltaHidden + thisRateAdjust.m_marginDeltaDisclosed + marginDelta
                                        , thisRateAdjust.m_teaserRDeltaHidden + thisRateAdjust.m_teaserRDeltaDisclosed + teaserRDelta
                                        , thisRateAdjust.m_qualRDeltaHidden + thisRateAdjust.m_qualRDeltaDisclosed + qrateDelta
                                        , bSnapUpRate
                                        , bSnapupFee
                                        , snapupFeeInterval
                                        , newBackEndMaxYsp
                                        , newFrontEndMaxYsp
                                        , out maxYspSnapped
                                        , bApplyCompensationBeforeRounding
                                        , appliedOriginatorCompPoint
                                        );


                                    rateDebugStr =
                                        m_loanFileData.sNoteIR_rep
                                        + ", " + m_loanFileData.sLOrigFPc_rep + (maxYspSnapped ? "[X]" : "")
                                        + ". MaxYSP(f,b) = " + thisRateMaxYsp
                                        + "(" + (newFrontEndMaxYsp.HasValue ? newFrontEndMaxYsp.ToString() : "NONE")
                                        + ", " + newBackEndMaxYsp + ")"
                                        ;

                                    List<string> hits = new List<string>();
                                    if (IsSameDtiResult(previousHitRules, hitRules, hits))
                                    {
                                        // We are done.  The result is the same.
                                        break;
                                    }

                                } // Iteration of re-applying DTI rules.

                                // 07/14/14 OPM 186390.  Have to calculate the exact par option before
                                // disquals are run so sRateOptionFinalPrice is fully adjusted for them.  
                                if (priceGroup.Value.IsAlwaysDisplayExactParRateOption
                                    && m_loanFileData.sPricingModeT != E_sPricingModeT.InternalInvestorPricing)
                                {
                                    if (perRateParFound == false && m_loanFileData.sRateOptionFinalPrice <= 0)
                                    {
                                        // Found what looks like the par-rate.
                                        perRateParFound = true;

                                        if (m_loanFileData.sRateOptionFinalPrice == 0)
                                        {
                                            // OPM 192387. This a natural par-rate.
                                            // No need to muck with data to force one.
                                        }
                                        else
                                        {
                                            // Set the point value as true par and correct the rounding.
                                            m_loanFileData.sLOrigFPc = -1 * appliedOriginatorCompPoint;

                                            if (m_runOptions.SelectedRate != 0 && m_loanFileData.sNoteIR == m_runOptions.SelectedRate)
                                            {
                                                m_feeRounding -= m_loanFileData.sRateOptionFinalPrice;
                                            }
                                            m_loanFileData.sRateOptionFinalPrice = 0;

                                            m_loanFileData.InvalidateCache();
                                        }
                                    }
                                }

                                // Look for disquals only at end after per-rate iteration done.
                                priceAdjustRecordArgs.dtiDisqualProcessing = PriceAdjustRecord.E_DtiDisqualProcessing.OnlyDisquals;
                                foreach (Guid policyId in applicablePolicies)
                                {

                                    if (skipList.Contains(policyId))
                                        continue;

                                    MyPricePolicy pricePolicy = LoanProgramSet.GetPricePolicy(policyId);
                                    try
                                    {
                                        PriceAdjustRecord.Compute(pricePolicy, ref priceAdjustRecordArgs, thisRateAdjust, true /*DTI ONLY */, m_applications, m_debugInfo, m_loanFileData.sPricingModeT, IsLogDebugInfo, m_runOptions.IsGetAllReasons);
                                    }
                                    catch (PricingCalcException exc)
                                    {
                                        PrepareInsufficientInfoStatus(exc.LogMsg, exc.ErrorMsg, exc.ErrorMsg4Developer);
                                        return; // stop the execution immediately.
                                    }

                                }

                                // We'll reset sCashToBorrowerForPricing here since it should only be used for the keyword evaluation.
                                this.m_loanFileData.sCashToBorrowerForPricing = null;

                                // for ...policies
                                priceAdjustRecordArgs.dtiDisqualProcessing = PriceAdjustRecord.E_DtiDisqualProcessing.Normal;

                                if (maxYspSnapped)
                                {
                                    // Per OPM 91898, we choose a "program maxYsp", which is defined as the minimum maxYSP
                                    // that did cause a maxysp snapdown.  We won't know until we've processed all rates.
                                    perOptionAdjust.AddSnapDown(thisRateMaxYsp, m_loanFileData.sNoteIR);
                                }

                                if (m_loanFileData.sLOrigFPc + appliedOriginatorCompPoint > minYsp)
                                {
                                    // OPM 67253
                                    if (m_loanFileData.sPricingModeT != E_sPricingModeT.InternalBrokerPricing
                                            && m_loanFileData.sPricingModeT != E_sPricingModeT.InternalInvestorPricing)
                                    {
                                        perOptionAdjust.AddFilteredOptionDebug(rateDebugStr, iteration);
                                        minYspFilterCounter++;
                                        continue; // MinYSP violation
                                    }
                                }

                                if (m_loanFileData.sLOrigFPc + appliedOriginatorCompPoint < thisRateMaxYsp)
                                {
                                    if (IsLogDebugInfo)
                                    {
                                        // OPM 233317 logging.
                                        LogDebug(string.Format("[Remove Rate] :{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}"
                                            , rateDebugStr
                                            , m_loanFileData.sLOrigFPc
                                            , m_loanFileData.sRateOptionFinalPrice
                                            , appliedOriginatorCompPoint
                                            , thisRateMaxYsp
                                            , _sLOrigFPc, __sLOrigFPc
                                            , maxYspSnapped));
                                    }
                                    continue; // MaxYSP violation
                                }
                            }

                            if (m_loanFileData.BrokerDB.HasEnabledPMI && lLpmiSupportedOutsidePmi == true && m_loanFileData.sProdConvMIOptionT == E_sProdConvMIOptionT.LendPaidSinglePrem)
                            {
                                // 180150.  This is a type of program that gets its LPMI adjustments
                                // from rules rather than MI.
                                decimal lpmiAdjTot = 0;

                                foreach (var lpmiAdj in m_adjDescsDisclosed.Union(m_adjDescsHidden).Where(adj => adj.Description.StartsWith("LPMI", StringComparison.OrdinalIgnoreCase)))
                                {
                                    decimal adjustment;
                                    if (decimal.TryParse(lpmiAdj.Fee.Replace("%", ""), out adjustment))
                                    {
                                        lpmiAdjTot += adjustment;
                                    }
                                }

                                string lpmiAdjTotStr = m_losConvert.ToRateString(lpmiAdjTot);
                                m_loanFileData.sLenderUfmipR = lpmiAdjTot;
                                m_mortgageInsuranceDesc = "LPMI-Single, " + lpmiAdjTotStr + " Upfront";
                                m_mortgageInsuranceSingleRate = lpmiAdjTotStr;
                            }

                            decimal sProThisMPmt = 0;
                            string sProThisMPmt_rep = "";
                            using (PerformanceStopwatch.Start("Block 2"))
                            {
                                try
                                {
                                    sProThisMPmt = m_loanFileData.sProThisMPmt;
                                    sProThisMPmt_rep = m_loanFileData.m_convertLos.ToMoneyString(sProThisMPmt, FormatDirection.ToRep);
                                }
                                catch (CBaseException)
                                {
                                }

                                m_loanFileData.StartCaching(); // Start Caching Loan File Field for performance.

                                string sProThisMQual_rep = sProThisMPmt_rep; // By default this value is equivalent to monthly payment.
                                if (m_loanFileData.sIsProThisMQualDiffsProThisMPmt || m_loanFileData.sIsLineOfCredit)
                                {
                                    // 11/20/2009 dd - Qualify monthly payment is different than regular payment. Go to datalayer for new
                                    // calculation.
                                    // 10/24/2013 dd - HELOC loan may have different qualifying payment and displaying payment
                                    try
                                    {
                                        sProThisMQual_rep = m_loanFileData.sProThisMQual_rep;
                                    }
                                    catch (InvalidCalculationException e)
                                    {
                                        // 08.21.14 OPM 190261. If not getting all reasons, the rate may not be fully-adjusted
                                        // and is possibly 0 because of it.  Allow to continue in that situation.
                                        if (bGetAllReasons && m_runOptions.LpeTaskT != LendersOffice.DistributeUnderwriting.E_LpeTaskT.RenderCertificate)
                                        {
                                            throw e;
                                        }
                                    }
                                }
                                string apr = "N/A";
                                if (UseNewPml)
                                {
                                    // No-op.  We calculate it after apply CC template.
                                }
                                else if (m_runOptions.LpeTaskT != LendersOffice.DistributeUnderwriting.E_LpeTaskT.RunEngine &&
                                    m_runOptions.LpeTaskT != LendersOffice.DistributeUnderwriting.E_LpeTaskT.PerformInternalPricingAction)
                                {
                                    // 3/15/2011 dd - If we want to compute APR for pricing then we need to implement caching mechanism
                                    // for sFinancedAmt.
                                    decimal sFinancedAmt = 0;
                                    try
                                    {
                                        // 12/22/2009 dd - I cannot move sFinancedAmt out of this loop because it is depend on sLOrigFPc
                                        sFinancedAmt = m_loanFileData.sFinancedAmt;
                                    }
                                    catch (CBaseException)
                                    {
                                    }

                                    // 3/4/2010 dd - Since we do not display APR in the result screen, we can avoid compute APR for performing reason.
                                    apr = m_loanFileData.PricingEngineApr_rep(sFinancedAmt, sProThisMPmt);
                                }
                                string sQualBottomR_rep;
                                try
                                {
                                    if (m_loanFileData.sLienPosT == E_sLienPosT.Second && m_loanFileData.sSubFinT == E_sSubFinT.Heloc)
                                    {
                                        if (m_loanFileData.sLpProductT != E_sLpProductT.Heloc)
                                            lHasQRateInRateOptions = false;
                                        sQualBottomR_rep = m_loanFileData.Calculate_sQualBottomR_rep(sLTotI, m_loanFileData.sSubFinPe * (qrateDelta / 100));
                                    }
                                    else
                                    {
                                        sQualBottomR_rep = m_loanFileData.Calculate_sQualBottomR_rep(sLTotI);
                                    }
                                }
                                catch (InvalidCalculationException e)
                                {
                                    // 08.21.14 OPM 190261. If not getting all reasons, the rate may not be fully-adjusted
                                    // and is possibly 0 because of it.  Allow to continue in that situation.

                                    if (bGetAllReasons && m_runOptions.LpeTaskT != LendersOffice.DistributeUnderwriting.E_LpeTaskT.RenderCertificate)
                                    {
                                        throw e;
                                    }

                                    sQualBottomR_rep = "0";
                                }
                                CApplicantRateOption newOption = new CApplicantRateOption(
                                    _lLpTemplateId,
                                    _lLpTemplateNm,
                                    _lLpInvestorNm,
                                    baseOption.Rate_rep,
                                    baseOption.Point_rep,
                                    m_loanFileData.sNoteIR_rep,
                                    m_loanFileData.sLOrigFPc_rep,
                                    m_loanFileData.sLOrigFPc,
                                    apr, //m_loanFileData.sApr_rep,
                                    sProThisMPmt_rep,
                                    m_loanFileData.sRAdjMarginR_rep,
                                    sTerm,
                                    sDue,
                                    // Note that these ratios are not useful until we allow PML users edit the liability list.
                                    //"",//m_loanFileData.sQualTopR_rep,
                                    sQualBottomR_rep, //m_loanFileData.sQualBottomR_rep,
                                    lHasQRateInRateOptions ? m_loanFileData.sQualIR_rep : "",
                                    IsOptionArm ? m_loanFileData.sOptionArmTeaserR_rep : "",
                                    sProThisMQual_rep,
                                    CPageBase.CalculateBankedAmount(m_loanFileData.sLOrigFPc, _loanOfficerCommissionPointOfLoanAmount, _loanOfficerCommissionPointOfGrossProfit, _loanOfficerCommissionMinBase, _sOriginatorCompensationBaseAmt, _sOriginatorCompensationTotalAmount),
                                    sOriginatorCompPoint,
                                    baseOption.Point + m_feeDeltaHidden
                                    );

                                list.Insert(0, newOption);

                                if (UsePerRatePricing)
                                {
                                    ProcessFinalOption(newOption, thisRateAdjust, perOptionAdjust, iteration, iterationLimit, rateDebugStr);
                                }



                                if (UseNewPml)
                                {
                                    decimal sMonthlyPmt = 0;
                                    try
                                    {
                                        sMonthlyPmt = m_loanFileData.sMonthlyPmt;
                                    }
                                    catch (CBaseException exc)
                                    {

                                        // 08.21.14 OPM 190261. If not getting all reasons, the rate may not be fully-adjusted
                                        // and is possibly 0 because of it.  Allow to continue in that situation.

                                        if (bGetAllReasons && m_runOptions.LpeTaskT != LendersOffice.DistributeUnderwriting.E_LpeTaskT.RenderCertificate)
                                        {
                                            // Per SAE in case 107899
                                            // "A note rate of 0.000% would never be valid. 
                                            // It probably should be an evaluation error for the offending program."

                                            // Even if only one rate returns 0, it still probably means
                                            // something went wrong and SAEs need to look at the program.
                                            Tools.LogError("Cannot calculate monthlypmt for  " + lLpTemplateNm + ". Removing from result.", exc);
                                            m_evalStatus = E_EvalStatus.Eval_InsufficientInfo;
                                            return;
                                        }
                                    }

                                    if (m_loanFileData.sLenderCreditCalculationMethodT == E_sLenderCreditCalculationMethodT.CalculateFromFrontEndRateLock)
                                    {
                                        // OPM 217420. mf.
                                        m_loanFileData.sBrokComp1Pc = newOption.Point;
                                        m_loanFileData.LoadBrokerLockData();
                                        m_loanFileData.InvalidateCache(); // We just set value.
                                    }

                                    if (usesLOrigFPcBackup)
                                    {
                                        m_loanFileData.sLOrigFPc = _sLOrigFPc;
                                        m_loanFileData.InvalidateCache(); // We just set value.
                                    }


                                    // No need to calculate for when in comparison mode and this not a needed rate
                                    // Also not for Zillow.  We do not use these in Zillow, but unfortunately
                                    // we cannot disable for all quick pricer runs.  LQB UI does not use them, but
                                    // the quickpricer API exposes them.  Another optimization point is to not do
                                    // this for manual QP.
                                    if (
                                         (m_runOptions.PricingState == null
                                        || (m_runOptions.PricingState.GetNoteRate(m_runOptions.GetMode()) == newOption.Rate_rep || m_runOptions.PricingState.IsForRateMonitor)))
                                    {

                                        bool isDisclosureRegulationT_TRID2015 = m_loanFileData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;

                                        // (OPM 108720) Include the nonzero fees that make up the closing costs
                                        // These are intended to be the SAME fees used to calculate closingCostCache
                                        foreach (var fee in m_loanFileData.sRespaFeeItemList)
                                        {
                                            if (dumpJsonFees)
                                            {
                                                dumpJsonFees = false;
                                                LogDebug("jsonFees = " + Environment.NewLine + this.GetJsonFeesLog());
                                            }

                                            E_ClosingFeeSource source = E_ClosingFeeSource.LoanFile;
                                            if (feeServiceResult != null && feeServiceResult.AffectedLines.ContainsKey(fee.HudLine))
                                            {
                                                source = feeServiceResult.AffectedLines[fee.HudLine];
                                            }
                                            else if (appliedAutoCCTemplate)
                                            {
                                                source = E_ClosingFeeSource.ClosingCostTemplate;

                                                if (m_loanFileData.LoanProductMatchingTemplate != null && m_loanFileData.LoanProductMatchingTemplate.PricingQuote != null)
                                                {
                                                    // Centralize this.  Manual map because of time.
                                                    var quote = m_loanFileData.LoanProductMatchingTemplate.PricingQuote;
                                                    if (quote.IsCountyRtcSet && fee.HudLine == "1204") source = E_ClosingFeeSource.FirstAmerican;
                                                    if (quote.IsEscrowFSet && fee.HudLine == "1102") source = E_ClosingFeeSource.FirstAmerican;
                                                    if (quote.IsNotaryFSet && fee.HudLine == "1110") source = E_ClosingFeeSource.FirstAmerican;
                                                    if (quote.IsOwnerTitleInsFSet && fee.HudLine == "1103") source = E_ClosingFeeSource.FirstAmerican;
                                                    if (quote.IsRecFSet && fee.HudLine == "1201") source = E_ClosingFeeSource.FirstAmerican;
                                                    if (quote.IsStateRtcSet && fee.HudLine == "1205") source = E_ClosingFeeSource.FirstAmerican;
                                                    if (quote.IsTitleInsFSet && fee.HudLine == "1104") source = E_ClosingFeeSource.FirstAmerican;
                                                }
                                            }

                                            decimal feeAmount = fee.BorrowerFee;

                                            // OPM 229111 - Allow all fee's to be entered as long as they have a non-zero BorrowerFee.  Having that means the fee has at least one
                                            // payment that's paid by the borrower.
                                            if (LosConvert.GfeItemProps_Payer(fee.Props) < 2 || m_loanFileData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy)
                                            {
                                                RespaFeeItem newItem;
                                                string commonLog = (isDisclosureRegulationT_TRID2015 ? "BorrowerFee=" : "Fee=") + feeAmount;

                                                // Only include fees to be paid by the buyer
                                                if (fee.HudLine == "801")
                                                {
                                                    if (feeAmount != 0)
                                                    {
                                                        newOption.ClosingCostBreakdown.Add(new RespaFeeItem(fee.HudLine, fee.Description, feeAmount, fee.GfeSectionT, fee.Props, fee.PaidTo, source, fee.DisclosureSectionT));

                                                        if (this.IsLogDebugInfo)
                                                        {
                                                            newItem = newOption.ClosingCostBreakdown[newOption.ClosingCostBreakdown.Count - 1];
                                                            LogDebug("    => ClosingCostBreakdown (1): HudLine=" + newItem.HudLine + ", Description=" + newItem.Description +
                                                                     ", " + commonLog);
                                                        }
                                                    }

                                                }
                                                else if (fee.HudLine == "802"
                                                    && (feeAmount != 0 || (fee.PricingDisplayOverrideValue.HasValue && fee.PricingDisplayOverrideValue.Value != 0)))
                                                {
                                                    // 6/16/2014 AV - 183501 Correct PML 2.0 Closing Cost Breakdown for Line 802
                                                    if (fee.PricingDisplayOverrideValue.HasValue && !string.IsNullOrEmpty(fee.PricingDescription))
                                                    {
                                                        if (fee.PricingDisplayOverrideValue.Value == 0)
                                                        {
                                                            continue;
                                                        }
                                                        newOption.ClosingCostBreakdown.Add(new RespaFeeItem(fee.HudLine, fee.PricingDescription, fee.PricingDisplayOverrideValue.Value, fee.GfeSectionT, fee.Props, fee.PaidTo, source, fee.DisclosureSectionT));

                                                        if (this.IsLogDebugInfo)
                                                        {
                                                            newItem = newOption.ClosingCostBreakdown[newOption.ClosingCostBreakdown.Count - 1];
                                                            LogDebug("    => ClosingCostBreakdown (2): HudLine=" + newItem.HudLine + ", Description=" + newItem.Description +
                                                                     ", PricingDisplayOverrideValue=" + newItem.Fee + ", " + commonLog);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        newOption.ClosingCostBreakdown.Add(new RespaFeeItem(fee.HudLine, feeAmount < 0 ? "Lender Credit" : "Lender Charge", fee.BorrowerFee, fee.GfeSectionT, fee.Props, fee.PaidTo, source, fee.DisclosureSectionT));

                                                        if (this.IsLogDebugInfo)
                                                        {
                                                            newItem = newOption.ClosingCostBreakdown[newOption.ClosingCostBreakdown.Count - 1];
                                                            LogDebug("    => ClosingCostBreakdown (3): HudLine=" + newItem.HudLine + ", Description=" + newItem.Description +
                                                                     ", *BorrowerFee=" + newItem.Fee + ", " + commonLog);
                                                        }
                                                    }
                                                }
                                                else if (fee.HudLine == "902") // Ln.902 = MIP
                                                {
                                                    decimal miPaidByBorrower = feeAmount - m_loanFileData.sFfUfmipFinanced;
                                                    if (miPaidByBorrower != 0 && feeAmount != 0)
                                                    {
                                                        newOption.ClosingCostBreakdown.Add(new RespaFeeItem(fee.HudLine, fee.Description, miPaidByBorrower, fee.GfeSectionT, fee.Props, fee.PaidTo, source, fee.DisclosureSectionT));

                                                        if (this.IsLogDebugInfo)
                                                        {
                                                            newItem = newOption.ClosingCostBreakdown[newOption.ClosingCostBreakdown.Count - 1];
                                                            LogDebug("    => ClosingCostBreakdown (4): HudLine=" + newItem.HudLine + ", Description=" + newItem.Description +
                                                                     ", miPaidByBorrower=" + newItem.Fee + ", " + commonLog);
                                                        }
                                                    }
                                                    // Otherwise, this is a $0 fee because either the borrower is financing all UFMIP or there is no UFMIP.

                                                }
                                                else if (fee.HudLine == "905") // Ln.902 = VA Funding Fee
                                                {
                                                    decimal vaFFPaidByBorrower = feeAmount - m_loanFileData.sFfUfmipFinanced;
                                                    if (vaFFPaidByBorrower != 0 && feeAmount != 0)
                                                    {
                                                        newOption.ClosingCostBreakdown.Add(new RespaFeeItem(fee.HudLine, fee.Description, vaFFPaidByBorrower, fee.GfeSectionT, fee.Props, fee.PaidTo, source, fee.DisclosureSectionT));

                                                        if (this.IsLogDebugInfo)
                                                        {
                                                            newItem = newOption.ClosingCostBreakdown[newOption.ClosingCostBreakdown.Count - 1];
                                                            LogDebug("    => ClosingCostBreakdown (5): HudLine=" + newItem.HudLine + ", Description=" + newItem.Description +
                                                                     ", vaFFPaidByBorrower=" + newItem.Fee + ", " + commonLog);
                                                        }
                                                    }
                                                    // Otherwise, this is a $0 fee because either the borrower is financing all VAFF or there is no VAFF.

                                                }
                                                else if (feeAmount != 0)
                                                {
                                                    // Otherwise just include the fee
                                                    fee.Source = source;
                                                    newOption.ClosingCostBreakdown.Add(new RespaFeeItem(fee.HudLine, fee.Description, feeAmount, fee.GfeSectionT, fee.Props, fee.PaidTo, source, fee.DisclosureSectionT));
                                                    if (this.IsLogDebugInfo)
                                                    {
                                                        newItem = newOption.ClosingCostBreakdown[newOption.ClosingCostBreakdown.Count - 1];
                                                        LogDebug("    => ClosingCostBreakdown (6): HudLine=" + newItem.HudLine + ", Description=" + newItem.Description +
                                                                 ", " + commonLog);
                                                    }
                                                }
                                                // else it's zero, so ignore the fee as it does not contribute to the closing cost breakdown
                                            }
                                        }

                                        // (OPM 108720) Show the total of prepaid items (any GFE with line number between 900-1009)
                                        //   and non-prepaid items (any GFE except with line between 900-1009)
                                        decimal prepaidCharges = 0;
                                        decimal nonPrepaidCharges = 0;
                                        foreach (var fee in newOption.ClosingCostBreakdown)
                                        {
                                            int hudline;
                                            if (int.TryParse(fee.HudLine, out hudline))
                                            {
                                                if (900 <= hudline && hudline <= 1009)
                                                {
                                                    prepaidCharges += fee.Fee;
                                                }
                                                else
                                                {
                                                    nonPrepaidCharges += fee.Fee;
                                                }
                                            }
                                            else
                                            {
                                                Tools.LogBug("Could not parse hudline for closing cost fee: " + fee.Description + ". Fee was not added to closing cost breakdown.");
                                            }
                                        }
                                        newOption.PrepaidCharges = prepaidCharges.ToString("C");
                                        newOption.NonPrepaidCharges = nonPrepaidCharges.ToString("C");

                                        decimal closingCost = prepaidCharges + nonPrepaidCharges;

                                        decimal nonNegativeClosingCost = closingCost < 0 ? 0 : closingCost;
                                        newOption.ClosingCostTemp = closingCost;
                                        newOption.ClosingCost = closingCost.ToString("C");

                                        if (m_loanFileData.sIsRefinancing)
                                        {
                                            if (m_loanFileData.sLpProductT == E_sLpProductT.Heloc ||
                                                ((bGetAllReasons == false || m_runOptions.LpeTaskT == LendersOffice.DistributeUnderwriting.E_LpeTaskT.RenderCertificate) && m_evalStatus != E_EvalStatus.Eval_Eligible)
                                                 )
                                            {
                                                // OPM 141171.  Calculation not applicable because $0 payment.

                                                // 08.21.14 OPM 190261. Also, if not getting all reasons, the rate may not be fully-adjusted
                                                // and is possibly 0 because of it.  Allow to continue in that situation.

                                                newOption.BreakEvenPoint = "N/A";
                                            }
                                            else
                                            {
                                                // Refi Scenarios:
                                                // Latest version of Breakeven point calculation is here:
                                                //https://opm/default.asp?113787#BugEvent.871107

                                                decimal currentPI = m_loanFileData.sProdCurrPIPmt + m_loanFileData.sProdCurrMIPMo;
                                                decimal newPI = m_loanFileData.sProThisMPmt + m_loanFileData.sProMIns;
                                                if (currentPI < newPI)
                                                {
                                                    //If (current P&I + current MIP) < (new P&I + new MIP) then display "N/A"
                                                    newOption.BreakEvenPoint = "N/A";
                                                }
                                                else if (closingCost <= 0)
                                                {
                                                    //Else, if "Costs" column <= $0.00 then display "0.0".
                                                    newOption.BreakEvenPoint = "0.0";
                                                }
                                                else
                                                {
                                                    decimal monthlyDiff = currentPI - newPI;
                                                    decimal breakEvenPoint = monthlyDiff != 0 ?
                                                            Tools.AlwaysRoundUp(closingCost / monthlyDiff, 1)
                                                        : 0m;

                                                    if (breakEvenPoint > m_loanFileData.sDue)
                                                    {
                                                        // If break even months > loan program's due then display "N/A".
                                                        newOption.BreakEvenPoint = "N/A";
                                                    }
                                                    else
                                                    {
                                                        newOption.BreakEvenPoint = breakEvenPoint.ToString();
                                                    }
                                                }
                                            }
                                        }

                                        if ((bGetAllReasons == false || m_runOptions.LpeTaskT == LendersOffice.DistributeUnderwriting.E_LpeTaskT.RenderCertificate) && m_evalStatus != E_EvalStatus.Eval_Eligible)
                                        {
                                            // 08.21.14 OPM 190261. If not getting all reasons, the rate may not be fully-adjusted
                                            // and is possibly 0 because of it.  Allow to continue in that situation.

                                            newOption.Reserves = "N/A";
                                        }
                                        else
                                        {
                                            //RESERVES= ((Total Liquid Assets-Downpayment-Total Borrower Paid Closing Costs))/(PITIA Payment)
                                            decimal sMonthlyPmtQual = m_loanFileData.sMonthlyPmtQual;
                                            if (sMonthlyPmtQual != 0)
                                            {

                                                decimal sTransNetCash = m_loanFileData.sTransNetCash;

                                                decimal nonNegativesTransNetCash = sTransNetCash < 0 ? 0 : sTransNetCash;
                                                decimal reservesNotRounded = 0;

                                                reservesNotRounded = ((m_loanFileData.sTotLiquidAssets - nonNegativesTransNetCash) / sMonthlyPmtQual);

                                                decimal reservesRounded = Math.Round(0.1M * Math.Floor(10 * reservesNotRounded), 1);
                                                newOption.Reserves = reservesRounded.ToString() + " months";
                                            }
                                            else
                                            {
                                                // OPM 141171.
                                                newOption.Reserves = (m_loanFileData.sLpProductT == E_sLpProductT.Heloc) ? "N/A" : "0 months";
                                            }
                                        }

                                        if (m_evalStatus != E_EvalStatus.Eval_Ineligible ||
                                            useRateOptionVariantMI) 
                                        {
                                            // For opm 471515, we will not know until the very end if there will be an ineligibility.
                                            // It is possible that this iteration is denied, but the overall result will subsequently 
                                            // become eligible due to other iterations.

                                            m_loanFileData.BypassEngineAmortizationCheck = true;
                                            using (PerformanceStopwatch.Start("sApr"))
                                            {
                                                newOption.Apr_rep = m_loanFileData.sApr_rep;
                                            }
                                            m_loanFileData.BypassEngineAmortizationCheck = false;
                                            if (!m_loanFileData.sIsLineOfCredit)
                                            {
                                                newOption.QMData = m_loanFileData.GetQMPricingData();
                                            }
                                        }
                                    }


                                    else if (m_runOptions.PricingState != null)
                                    {
                                        if (!m_loanFileData.sIsLineOfCredit)
                                        {
                                            newOption.QMData = m_loanFileData.GetQMPricingData();
                                        }
                                    }
                                }

                                // 2018-05 tj 467908 - web services needs to always export sProMIns so I'm pulling it out of the pin state only fields
                                // There's potential for a bug here with BypassEngineAmortizationCheck, but after reviewing the code briefly with Doug,
                                // it appears the calculation for sProMIns has its own Amortization, and will not be directly affected.
                                //// Mortgage Insurance (sProMIns)
                                newOption.sProMIns = m_loanFileData.sProMIns_rep;

                                // If we have a pin state with the matching rate or we're the rate monitor
                                if (m_runOptions.PricingState != null && (m_runOptions.PricingState.GetNoteRate(m_runOptions.GetMode()) == newOption.Rate_rep || m_runOptions.PricingState.IsForRateMonitor))
                                {

                                    decimal closingCost = newOption.ClosingCostTemp;
                                    decimal nonNegativeClosingCost = closingCost < 0 ? 0 : closingCost;

                                    m_loanFileData.BypassEngineAmortizationCheck = true;

                                    decimal currentPITIPayment = (m_loanFileData.sProdCurrPIPmt
                                                  + m_loanFileData.sProdCurrMIPMo
                                                  + m_loanFileData.sProRealETxPe
                                                  + m_loanFileData.sProHazInsPe
                                                  + m_loanFileData.sProHoAssocDuesPe
                                                  + m_loanFileData.sProOHExpPe);

                                    //// Base Loan Amount (sLAmtCalcPe)
                                    newOption.sLAmtCalcPe = m_loanFileData.sLAmtCalcPe_rep;

                                    //// Upfront MIP / Funding Fee Financed (sFfUfmipFinanced)
                                    newOption.sFfUfmipFinanced = m_loanFileData.sFfUfmipFinanced_rep;

                                    //// Total Loan Amount (sFinalLAmt)
                                    newOption.sFinalLAmt = m_loanFileData.sFinalLAmt_rep;

                                    //// Impound Taxes&Insurance (sProdImpound)
                                    newOption.sProdImpound = m_loanFileData.sProdImpound ? "1" : "0";

                                    //// Total Cash to Close (sTransNetCash)
                                    newOption.CashToClose = m_loanFileData.sTransNetCash_rep;

                                    //// Estimated Reserves Dollars (Total liquid assets � non-negative cash to close)
                                    newOption.EstimatedReservesDollars = (m_loanFileData.sTotLiquidAssets - Math.Max(0, m_loanFileData.sTransNetCash)).ToString("C");

                                    //// Monthly PITIA Savings (Current PITIA Payment � sMonthlyPmt )
                                    newOption.PitiaSavings = (currentPITIPayment - m_loanFileData.sMonthlyPmt).ToString("C");

                                    //// RecoupClosingCostsMonths = BreakEvenPoint.  Likely obsolete.
                                    newOption.RecoupClosingCostsMonths = newOption.BreakEvenPoint;

                                    //// Lifetime Finance Charge (sFinCharge)
                                    newOption.sFinCharge = m_loanFileData.sFinCharge_rep;

                                    AmortTable table = m_loanFileData.GetAmortTable(E_AmortizationScheduleT.Standard, false);

                                    int horizon = m_loanFileData.sHorizonOfBorrowerInterestInMonths; // Future: user enters this
                                    decimal principalPaid;
                                    decimal financePaid;
                                    decimal remainingBalance;
                                    table.CalculateHorizon(horizon, out principalPaid, out financePaid, out remainingBalance);
                                    financePaid += m_loanFileData.sAprIncludedCc;
                                    ////	Principal Paid (Sum of principal payments over horizon term.)
                                    newOption.PrincipalPaidOverHorizon = principalPaid.ToString("C");

                                    //// Finance Charge Paid (Sum of interest payments, mortgage insurance, and closing costs over horizon term)
                                    newOption.FinanceChargesPaidOverHorizon = financePaid.ToString("C");

                                    //// Remaining Balance (principal balance at end of horizon term)
                                    newOption.RemainingBalanceAfterHorizon = remainingBalance.ToString("C");

                                    //// Savings Versus Current Loan (m_loanFileData.sProdCurrPIPmt - m_loanFileData.sMonthlyPmt * horizon - est closing cost 
                                    newOption.SavingsVersusCurrentLoan = (((currentPITIPayment - m_loanFileData.sMonthlyPmt) * horizon) - (nonNegativeClosingCost)).ToString("C");

                                    // sMonthlyPmtPe
                                    newOption.sMonthlyPmtPe = m_loanFileData.sMonthlyPmt_rep;

                                    newOption.CurrentPITIPayment = currentPITIPayment.ToString("C");

                                    // OPM 244915.
                                    newOption.sProRealETx = m_loanFileData.sProRealETx_rep;
                                    newOption.sProHazIns = m_loanFileData.sProHazIns_rep;
                                    newOption.sProHoAssocDues = m_loanFileData.sProHoAssocDues_rep;
                                    newOption.sProOHExp = m_loanFileData.sProOHExp_rep;

                                    m_loanFileData.BypassEngineAmortizationCheck = false;


                                }

                                // OPM 233679.
                                if (m_loanFileData.BrokerDB.EnableCashToCloseAndReservesDebugInPML)
                                {
                                    // Expensive field.
                                    // Take advantage of sTransNetCaching if if we already have it for this rate.
                                    decimal sTransNetCash = sTransNetCashUsedForKeywords.HasValue ? sTransNetCashUsedForKeywords.Value : m_loanFileData.sTransNetCash;
                                    //newOption.LoanData.sTransNetCash = sTransNetCash;

                                    newOption.LoanData = new RateOptionLoanData()
                                    {
                                        sPurchPrice = m_loanFileData.sPurchPrice_rep,
                                        sAltCost = m_loanFileData.sAltCost_rep,
                                        sLandCost = m_loanFileData.sLandCost_rep,
                                        sRefPdOffAmt1003 = m_loanFileData.sRefPdOffAmt1003_rep,
                                        sTotEstPp1003 = m_loanFileData.sTotEstPp1003_rep,
                                        sTotEstCcNoDiscnt1003 = m_loanFileData.sTotEstCcNoDiscnt1003_rep,
                                        sFfUfmip1003 = m_loanFileData.sFfUfmip1003_rep,
                                        sLDiscnt1003 = m_loanFileData.sLDiscnt1003_rep,
                                        sTotTransC = m_loanFileData.sTotTransC_rep,
                                        sONewFinBal = m_loanFileData.sONewFinBal_rep,
                                        sTotCcPbs = m_loanFileData.sTotCcPbs_rep,
                                        sOCredit1Desc = m_loanFileData.sOCredit1Desc,
                                        sOCredit1Amt = m_loanFileData.sOCredit1Amt_rep,
                                        sOCredit2Desc = m_loanFileData.sOCredit2Desc,
                                        sOCredit2Amt = m_loanFileData.sOCredit2Amt_rep,
                                        sOCredit3Desc = m_loanFileData.sOCredit3Desc,
                                        sOCredit3Amt = m_loanFileData.sOCredit3Amt_rep,
                                        sOCredit4Desc = m_loanFileData.sOCredit4Desc,
                                        sOCredit4Amt = m_loanFileData.sOCredit4Amt_rep,
                                        sOCredit5Amt = m_loanFileData.sOCredit5Amt_rep,
                                        sONewFinCc = m_loanFileData.sONewFinCcAsOCreditAmt_rep,
                                        sONewFinCcInverted = m_loanFileData.sONewFinCc_rep,
                                        sTotLiquidAssets = m_loanFileData.sTotLiquidAssets_rep,
                                        sLAmtCalc = m_loanFileData.sLAmtCalc_rep,
                                        sFfUfmipFinanced = m_loanFileData.sFfUfmipFinanced_rep,
                                        sFinalLAmt = m_loanFileData.sFinalLAmt_rep,
                                        sMonthlyPmt = m_loanFileData.sMonthlyPmtQual_rep,
                                        sTransNetCash = m_loanFileData.m_convertLos.ToMoneyString(sTransNetCash, FormatDirection.ToRep),
                                        sTransNetCashNonNegative = m_loanFileData.m_convertLos.ToMoneyString(sTransNetCash < 0 ? 0 : sTransNetCash, FormatDirection.ToRep),
                                        sIsIncludeProrationsIn1003Details = m_loanFileData.sIsIncludeProrationsIn1003Details,
                                        sIsIncludeProrationsInTotPp = m_loanFileData.sIsIncludeProrationsInTotPp,
                                        sTotalBorrowerPaidProrations = m_loanFileData.sTotalBorrowerPaidProrations_rep,
                                        sTotEstPp = m_loanFileData.sTotEstPp_rep,
                                        sIsIncludeONewFinCcInTotEstCc = m_loanFileData.sIsIncludeONewFinCcInTotEstCc,
                                        sTotEstCcNoDiscnt = m_loanFileData.sTotEstCcNoDiscnt_rep,
                                    };

                                    if (isTargeting2019Ulad)
                                    {
                                        newOption.LoanData.sIsTargetingUlad2019 = isTargeting2019Ulad;
                                        newOption.LoanData.sAltCostLckd = m_loanFileData.sAltCostLckd;
                                        newOption.LoanData.sLandIfAcquiredSeparately1003 = m_loanFileData.sLandIfAcquiredSeparately1003_rep;
                                        newOption.LoanData.sLDiscnt1003Lckd = m_loanFileData.sLDiscnt1003Lckd;
                                        newOption.LoanData.sPurchasePrice1003 = m_loanFileData.sPurchasePrice1003_rep;
                                        newOption.LoanData.sRefNotTransMortgageBalancePayoffAmt = m_loanFileData.sRefNotTransMortgageBalancePayoffAmt_rep;
                                        newOption.LoanData.sRefTransMortgageBalancePayoffAmt = m_loanFileData.sRefTransMortgageBalancePayoffAmt_rep;
                                        newOption.LoanData.sSellerCreditsUlad = m_loanFileData.sSellerCreditsUlad_rep;
                                        newOption.LoanData.sTotCcPbsLocked = m_loanFileData.sTotCcPbsLocked;
                                        newOption.LoanData.sTotCreditsUlad = m_loanFileData.sTotCreditsUlad_rep;
                                        newOption.LoanData.sTotEstBorrCostUlad = m_loanFileData.sTotEstBorrCostUlad_rep;
                                        newOption.LoanData.sTotEstCc1003Lckd = m_loanFileData.sTotEstCc1003Lckd;
                                        newOption.LoanData.sTotEstPp1003Lckd = m_loanFileData.sTotEstPp1003Lckd;
                                        newOption.LoanData.sTotMortLAmtUlad = m_loanFileData.sTotMortLAmtUlad_rep;
                                        newOption.LoanData.sTotMortLTotCreditUlad = m_loanFileData.sTotMortLTotCreditUlad_rep;
                                        newOption.LoanData.sTotOtherCreditsUlad = m_loanFileData.sTotOtherCreditsUlad_rep;
                                        newOption.LoanData.sTotTransCUlad = m_loanFileData.sTotTransCUlad_rep;
                                        newOption.LoanData.sTransNetCashUlad = m_loanFileData.sTransNetCashUlad_rep;
                                        newOption.LoanData.sTRIDSellerCredits = m_loanFileData.sTRIDSellerCredits_rep;
                                    }
                                }

                                m_loanFileData.StopCaching();
                                // 02/03/09 mf - OPM 19182.  We sometimes get ratesheets that switch between a broker
                                // fee and rebate as rate ascends.  Previous algorithm made assumption that we cross
                                // the par threshold at most once. SAEs decided that the displayed rate option
                                // should be the lowest rate that has a broker rebate (negative point). 
                                if ((null == m_repRateOption)
                                    || (m_repRateOption.Point + appliedOriginatorCompPoint > 0 &&
                                          (newOption.Point + appliedOriginatorCompPoint <= 0
                                            || newOption.Point + appliedOriginatorCompPoint < m_repRateOption.Point + appliedOriginatorCompPoint)))
                                {
                                    if (UsePerRatePricing == false || (UsePerRatePricing && thisRateAdjust.Disqualify == false)) // Rep rate cannot be disqual'd
                                        m_repRateOption = newOption;
                                }
                            }
                        }
                        // We only allow at most one extra rate option with give back the amount outside of range.
                        if (ysp <= maxYsp
                            && !m_bMinMaxFeeViolation
                            && !UsePerRatePricing
                            // OPM 67253
                            && !m_loanFileData.BrokerDB.IsDisplayRateOptionsAboveLowestTriggeringMaxYSP
                            && m_loanFileData.sPricingModeT != E_sPricingModeT.InternalBrokerPricing
                            && m_loanFileData.sPricingModeT != E_sPricingModeT.InternalInvestorPricing
                            )
                        {
                            break;
                        }
                        if (usesLOrigFPcBackup)
                            m_loanFileData.sLOrigFPc = _sLOrigFPc;
                    } // for loop, it's looping through the array of base rates

                    #region FilterRatesForOPM130011
                    if (ConstStage.IsEnableRateFilterForOPM_130011 && m_runOptions.IsRateOptionsWorseThanLowerRateShown == false)
                    {
                        decimal lastPoint = decimal.MaxValue;
                        for (int i = list.Count - 1; i >= 0; i--)
                        {
                            CApplicantRateOption option = (CApplicantRateOption)list[i];

                            if (option.Point_ >= lastPoint)
                            {
                                // This point is higher than one at lower rate.  It cannot stay.
                                list.Remove(option);

                                if (UsePerRatePricing)
                                    perOptionAdjust.RemoveOption(option);

                                continue;
                            }

                            lastPoint = option.Point_;
                        }
                    }

                    #endregion


                    if (UsePerRatePricing)
                    {
                        // If there are higher rate options above the maxYsp, remove
                        bool allRatesInvalid = true;
                        bool sawProgramMaxYsp = false;
                        for (int i = list.Count - 1; i >= 0; i--)
                        {
                            CApplicantRateOption option = (CApplicantRateOption)list[i];

                            if (perOptionAdjust.ProgramMaxYsp.HasValue)
                            {
                                if (perOptionAdjust.IsSnappedDownByProgramMaxYsp(option.Rate))
                                {
                                    if (sawProgramMaxYsp == false)
                                    {
                                        // Keep only one
                                        if (!m_loanFileData.BrokerDB.IsDisplayRateOptionsAboveLowestTriggeringMaxYSP
                                                && m_loanFileData.sPricingModeT != E_sPricingModeT.InternalBrokerPricing
                                                && m_loanFileData.sPricingModeT != E_sPricingModeT.InternalInvestorPricing
                                            )
                                            sawProgramMaxYsp = true;
                                    }
                                    else
                                    {
                                        // We already took one at this point
                                        list.Remove(option);
                                        perOptionAdjust.RemoveOption(option);
                                        continue;
                                    }
                                }
                                else
                                {
                                    if (sawProgramMaxYsp)
                                    {
                                        // Higher rate after we saw 1st maxYsp
                                        list.Remove(option);
                                        perOptionAdjust.RemoveOption(option);
                                        continue;
                                    }
                                }
                            }

                            if (option.DisqualReason == "")
                            {
                                // found a valid rate
                                allRatesInvalid = false;
                            }
                        }

                        if (lLockedDaysNeededByLender != 0)
                        {
                            // 1/16/2011 dd - When running pricing in Internal Investor mode, we do not need to include the hidden adjustment since
                            // we always use the lock days from user specification.

                            if (m_loanFileData.sPricingModeT != E_sPricingModeT.InternalInvestorPricing && m_loanFileData.sPricingModeT != E_sPricingModeT.EligibilityInvestorPricing)
                            {
                                string zeroDot000 = m_losConvert.ToRateString(0); // 0.000  

                                CAdjustItem feeAdjDesc = new CAdjustItem(zeroDot000, // margin adj
                                    zeroDot000,  // rate adj
                                    zeroDot000, // fee adj
                                    zeroDot000, // qrate
                                    zeroDot000, // teaser rate
                                    string.Format("{0} Day Lock Buffer.", lLockedDaysNeededByLender), "");
                                m_adjDescsHidden.Add(feeAdjDesc);
                            }
                        }

                        if (!useRateOptionVariantMI)
                        {
                            // Set up the codes for the per rate options.
                            perOptionAdjust.FinalizeAdjustmentList(m_adjDescsDisclosed, m_adjDescsHidden, list);
                        }
                        else
                        {
                            // OPM 471493.
                            // Here the goal is to run PMI (without setting the result back to the loan file),
                            // for each rate option to determine if we received a different MI result than our baseline
                            // that was applied at the start.
                            try
                            {
                                // We only care about the options greater than or equal to the last triggering rate.
                                ArrayList reprocessRates = this.m_RateVariantPmi.GetRatesToProcess(list, m_adjDescsDisclosed, m_adjDescsHidden, m_reasonArray);

                                var foundMiDelta = false;
                                var finalNote = m_loanFileData.sNoteIR;
                                try
                                {
                                    // Notice we want to go in ascending order here to catch lowest rate with a difference.
                                    for (int i = reprocessRates.Count - 1; i >= 0; i--)
                                    {
                                        CApplicantRateOption option = (CApplicantRateOption)reprocessRates[i];

                                        var currentRate = option.Rate;
                                        m_loanFileData.sAvailReserveMonthsQual = m_RateVariantPmi.GetReserves(currentRate);

                                        m_loanFileData.sNoteIR = currentRate;
                                        m_loanFileData.sSymbolTableForPriceRule.StartCache();

                                        var result = ExecutePMI_Variant(
                                            ref m_lpmiAdjust,
                                            xmlDocEval,
                                            cachResult,
                                            stack,
                                            ref recalculateMortgageInsurancePerRate,
                                            forcedWinner: null,
                                            ignoreDtiRules: false,
                                            applyWinner: false,
                                            allowDisqualified: true);

                                        if (this.m_RateVariantPmi.ProcessMiEvaluation(currentRate, result, reprocessRates))
                                        {
                                            // This means we found a difference and need to potentially iterate again.
                                            break;
                                        }

                                    }
                                }
                                finally
                                {
                                    m_loanFileData.sNoteIR = finalNote;
                                }

                                // If we have selected a rate, set the PMI appropriately.
                                if (m_runOptions.SelectedRate > 0 && !foundMiDelta)
                                {
                                    var pmiResult = m_RateVariantPmi.GetPmiOfRate(m_runOptions.SelectedRate);

                                    if (pmiResult != null)
                                    {
                                        if (!pmiResult.IsDenied)
                                        {
                                            var result = ExecutePMI_Variant(
                                                ref m_lpmiAdjust,
                                                xmlDocEval,
                                                cachResult,
                                                stack,
                                                ref recalculateMortgageInsurancePerRate,
                                                forcedWinner: pmiResult,
                                                ignoreDtiRules: false,
                                                applyWinner: true);
                                        }
                                        else
                                        {
                                            // The selected rate option was denied PMI. 
                                            // We cannot leave the previous baseline MI.
                                        }
                                    }
                                }

                                // Handle the per-rate and per-program MI management.
                                list = m_RateVariantPmi.ProcessEndOfIteration(list, ref m_reasonArray, ref m_adjDescsDisclosed, ref m_adjDescsHidden, ref m_evalStatus);
                            }
                            finally
                            {
                                m_loanFileData.sSymbolTableForPriceRule.EndCache();
                            }
                        }

                        // If all rates are ineligible, mark the entire program as ineligible
                        if (allRatesInvalid)
                        {
                            string reason = "ALL RATES ARE UNAVAILABLE"; //91382 av 11 09 2012 show the par rate disqual reason
                            string reasonDebug = "";

                            if (minYspFilterCounter == count)
                            {
                                reason = "ALL RATE OPTIONS HAVE COST EXCEEDING MIN YSP";
                            }

                            //it turns out rep rate can be null sometimes and even if its not max ysp can filter it out so 
                            //always calculate it again.
                            var parRate = GetParRate(list);
                            if (parRate != null)
                            {
                                reason = parRate.DisqualReason;
                                reasonDebug = parRate.DisqualReasonDebug;

                                // OPM 450967.  In the all-rates-denied situation, 
                                // we can consider the rep rate from all rates,
                                // including those that were denied.
                                m_repRateOption = parRate;
                            }

                            m_evalStatus = E_EvalStatus.Eval_Ineligible;
                            m_reasonArray.Add(new CStipulation(reason, reasonDebug));
                            if (m_disqualifiedRules.Length > 0)
                                m_disqualifiedRules += "<br>";
                            m_disqualifiedRules += " * " + reason;
                        }

                        // Debug Adj
                        if (IsLogDetailDebugInfo)
                        {
                            m_adjDescsHidden.Add(new CAdjustItem(perOptionAdjust.DebugLog));
                        }

                    }

                    if (UsePerRatePricing == false)
                    {
                        // OPM 63535. Special setting to show negative par rate as 0.
                        if (priceGroup.Value.IsAlwaysDisplayExactParRateOption &&
                            m_loanFileData.sPricingModeT != E_sPricingModeT.InternalInvestorPricing &&
                            null != m_repRateOption &&
                            m_repRateOption.Point + appliedOriginatorCompPoint < 0)
                        {

                            // If this is the chosen option, need to make sure the Pricing Engine rounding 
                            // adjustment gets the change.
                            if (m_runOptions.SelectedRate != 0 && m_repRateOption.Rate == m_runOptions.SelectedRate)
                            {
                                m_feeRounding -= m_repRateOption.Point + appliedOriginatorCompPoint;
                            }


                            m_repRateOption.SetToParRate(appliedOriginatorCompPoint);
                        }
                    }

                    m_applicantRateOptions = (CApplicantRateOption[])list.ToArray(typeof(CApplicantRateOption));

                    if (m_bMinMaxFeeViolation)
                    {
                        //if( m_runOptions.IsLpeDisqualificationEnabled ) with opm 16132, this value is always true 
                        {
                            // Disqualifying product with no valid rateoption 
                            m_evalStatus = E_EvalStatus.Eval_Ineligible;
                            string reason = "No rate options between Max and Min price for this product.  Please contact your Lock Desk for pricing.";
                            m_reasonArray.Add(new CStipulation(reason, ""));
                            if (m_disqualifiedRules.Length > 0)
                                m_disqualifiedRules += "<br>";
                            m_disqualifiedRules += " * " + reason;
                        }
                    }

                    #endregion // Putting together the result set

                    using (PerformanceStopwatch.Start("Determine RSE"))
                    {
                        LogInfo("Determine RSE");

                        #region OPM 18661 - Determine if the rate option is expired or block user from submission
                        m_isBlockedRateLockSubmission = false;
                        m_areRatesExpired = false;
                        m_rateLockSubmissionUserWarningMessage = "";
                        m_rateLockSubmissionDevWarningMessage = "";

                        AbstractUserPrincipal currentPrincipal = PrincipalFactory.CurrentPrincipal;

                        if (null == currentPrincipal)
                        {
                            Tools.LogBug("Principal is null in CApplicantPriceOLD.ComputePrices(). Could not DetermineRateOptionExpiration");
                            return;
                        }
                        RateOptionExpirationResult result;
                        RateOptionExpirationResult normalUserResult;
                        string rseCacheKey = string.Format("{0}::{1}::{2}::{3}", this.LpeAcceptableRsFileId, this.LpeAcceptableRsFileVersionNumber, this.lLpInvestorNm, this.ProductCode);
                        // This isn't done in a loop so it may be better to not load all the data.
                        RateOptionExpirationDataLoader dataLoader = new RateOptionExpirationDataLoader(null, null, null, m_loanFileData.BrokerDB, shouldBulkLoad: false);
                        if (!cachResult.RateOptionExpirationResultCache.TryGetValue(rseCacheKey, out result))
                        {
                            var pg = priceGroup.Value;
                            if (pg == null || pg.LockPolicyID.HasValue == false)
                            {
                                Tools.LogError("Did not find a lock policy or price group for pg id " + m_runOptions.PriceGroupId + " BrokerId " + BrokerId);
                            }
                            var lp = LockPolicy.Retrieve(m_loanFileData.sBrokerId, pg.LockPolicyID.Value);
                            result = DetermineRateOptionExpiration(lp,
                                currentPrincipal.LpeRsExpirationByPassPermission,
                                this.LpeAcceptableRsFileId,
                                this.LpeAcceptableRsFileVersionNumber,
                                this.lLpInvestorNm,
                                this.ProductCode,
                                false /*byPassManualDisabled*/,
                                dataLoader);
                            cachResult.RateOptionExpirationResultCache.Add(rseCacheKey, result);
                        }

                        // OPM 20882
                        if (currentPrincipal.LpeRsExpirationByPassPermission != E_LpeRsExpirationByPassPermissionT.NoByPass)
                        {
                            // This user has some bypass privilidges. Per case 20882, result rates need to show expiration based on NORMAL user's result.
                            if (!cachResult.RateOptionExpirationNormalUserResultCache.TryGetValue(rseCacheKey, out normalUserResult))
                            {
                                var pg = PriceGroup.RetrieveByID(m_runOptions.PriceGroupId, m_loanFileData.BrokerDB.BrokerID);
                                if (pg == null || pg.LockPolicyID.HasValue == false)
                                {
                                    Tools.LogError("Did not find a lock policy or price group for pg id " + m_runOptions.PriceGroupId + " BrokerId " + BrokerId);
                                }
                                var lp = LockPolicy.Retrieve(m_loanFileData.sBrokerId, pg.LockPolicyID.Value);
                                normalUserResult = DetermineRateOptionExpiration(
                                    lp,
                                    E_LpeRsExpirationByPassPermissionT.NoByPass,
                                    this.LpeAcceptableRsFileId,
                                    this.LpeAcceptableRsFileVersionNumber,
                                    this.lLpInvestorNm,
                                    this.ProductCode,
                                    false, /*byPassManualDisabled*/
                                    dataLoader);
                                cachResult.RateOptionExpirationNormalUserResultCache.Add(rseCacheKey, normalUserResult);
                            }
                        }
                        else
                        {
                            normalUserResult = result;
                        }

                        m_isBlockedRateLockSubmission = result.IsBlockedRateLockSubmission;
                        m_areRatesExpired = normalUserResult.IsBlockedRateLockSubmission;
                        m_rateLockSubmissionUserWarningMessage = result.RateLockSubmissionUserWarningMessage;
                        m_rateLockSubmissionDevWarningMessage = result.RateLockSubmissionDevWarningMessage;

                        // 6/23/2009 dd - OPM 31884 - If RateOptionExpirationResult is HiddenFromResult then change eval status.
                        if (result.IsHiddenFromResult)
                        {
                            m_evalStatus = E_EvalStatus.Eval_HideFromResult;
                        }

                        if (m_loanFileData.sPricingModeT == E_sPricingModeT.InternalBrokerPricing ||
                            m_loanFileData.sPricingModeT == E_sPricingModeT.InternalInvestorPricing ||
                            m_loanFileData.sPricingModeT == E_sPricingModeT.EligibilityBrokerPricing ||
                            m_loanFileData.sPricingModeT == E_sPricingModeT.EligibilityInvestorPricing)
                        {
                            // 4/19/2011 dd - OPM 61767 - For internal pricing mode we only consider rate expire only
                            // if it does not due to outside of lock desk hour (see specs)
                            m_areRatesExpired = normalUserResult.IsBlockedRateLockSubmission && normalUserResult.IsExpiredDueToOutsideLockDeskHourOnly == false;
                        }
                        #endregion

                        if (IsLogDebugInfo)
                        {
                            LogDebug("    IsBlockedRateLockSubmission=" + m_isBlockedRateLockSubmission);
                            LogDebug("    m_areRatesExpired=" + m_areRatesExpired);
                            LogDebug("    m_rateLockSubmissionUserWarningMessage=" + m_rateLockSubmissionUserWarningMessage);
                            LogDebug("    m_rateLockSubmissionDevWarningMessage=" + m_rateLockSubmissionDevWarningMessage);
                            LogDebug("    m_evalStatus=" + m_evalStatus);
                        }
                    }

                    using (PerformanceStopwatch.Start("Determine Internal Pricing Restriction"))
                    {
                        if (m_loanFileData.sPricingModeT == E_sPricingModeT.InternalBrokerPricing ||
                            m_loanFileData.sPricingModeT == E_sPricingModeT.InternalInvestorPricing ||
                            m_loanFileData.sPricingModeT == E_sPricingModeT.EligibilityBrokerPricing ||
                            m_loanFileData.sPricingModeT == E_sPricingModeT.EligibilityInvestorPricing)
                        {
                            if (m_evalStatus == E_EvalStatus.Eval_Eligible)
                            {
                                // 1/16/2011 dd - OPM 30649 - For the internal pricing mode, we need to handle filter "Restrict Results to current not rate"
                                if (m_loanFileData.sProdFilterDisplayUsingCurrentNoteRate || m_runOptions.IsAutoProcess)
                                {
                                    decimal originalRate = m_runOptions.OriginalNoteRate;
                                    CApplicantRateOption selectedRateOption = null;
                                    if (originalRate > 0)
                                    {
                                        bool matchFound = false;

                                        foreach (var rateoption in m_applicantRateOptions)
                                        {
                                            if (rateoption.Rate == m_runOptions.OriginalNoteRate)
                                            {
                                                selectedRateOption = rateoption;
                                                matchFound = true;
                                                break;
                                            }
                                        }

                                        if (matchFound == false)
                                        {
                                            m_evalStatus = E_EvalStatus.Eval_Ineligible;
                                            m_disqualifiedRules = NoRateOptionMatch;
                                        }
                                        else
                                        {
                                            m_repRateOption = selectedRateOption;
                                            m_applicantRateOptions = new CApplicantRateOption[] { selectedRateOption };
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } while (useRateOptionVariantMI && this.m_RateVariantPmi.ShouldContinue()); 

            if (useRateOptionVariantMI && m_RateVariantPmi.IsMaxedOutIterations)
            {
                m_stipCollection.Add(
                    "WARNING"
                        , new CStipulation(
                        "PRICING MAY BE OFF DUE TO CIRCULAR DEPENDENCY."
                    , ""));
            }

            if (IsLogDebugInfo && !ConstSite.PricingCompactLogEnabled)
            {
                    LogDebug("");
                LogDebug("Timing Info");
                LogDebug(PerformanceStopwatch.ReportOutput);
            }

        }

        private void AdjustMIForLPMI()
        {
            m_loanFileData.sFfUfmipR = 0;
            m_loanFileData.sProMInsR = 0;
            m_loanFileData.sProMInsPe_rep = "0";
            m_loanFileData.sProMInsMb = 0;
            m_loanFileData.sProMInsLckd = false;
            m_loanFileData.sProMInsMon = 0;
            m_loanFileData.sProMIns2Mon = 0;
            m_loanFileData.sProMInsR2 = 0;
            m_loanFileData.sLenderUfmipLckd = false;
            m_loanFileData.sUfCashPdLckd = false;
        }

        private void ForceTransform()
        {
            E_CalcModeT previous = m_loanFileData.CalcModeT;
            m_loanFileData.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
            m_loanFileData.TransformDataToPml(E_TransformToPmlT.FromScratch);
            m_loanFileData.CalcModeT = previous;
        }

        // OPM 471515. This was methodized so the call could happen at different points in 
        // pricing based on a broker bit.
        private bool ExecuteClosingCost(ref FeeServiceResult feeServiceResult, ref bool appliedAutoCCTemplate )
        {
            // Opm 244710 - We only want to unlock these fields if we're not dealing with correspondent channel loans. 
            // OPM 251650 - Also prevent unlocking these fields for non-Correspondent loans based on lender setting.
            if (m_loanFileData.sBranchChannelT != E_BranchChannelT.Correspondent &&
                this.m_loanFileData.BrokerDB.IsAllowUnlockingDotFieldsDuringPmlSubmission)
            {
                // OPM 225530 - Set Details of Transaction lock boxes to false.                
                m_loanFileData.sTotEstPp1003Lckd = false;
                m_loanFileData.sTotEstCc1003Lckd = false;
                m_loanFileData.sLDiscnt1003Lckd = false;
                m_loanFileData.sTotCcPbsLocked = false;
                m_loanFileData.sTransNetCashLckd = false;
            }

            if (m_loanFileData.IsFeeServiceValid)
            {
                if (!m_loanFileData.sDisableFeeServiceForQuickPricer)
                {
                    LogInfo("ApplyFeeService");
                    feeServiceResult = m_loanFileData.ApplyFeeService(this);
                    if (feeServiceResult != null && !string.IsNullOrEmpty(feeServiceResult.DebugInfo))
                    {
                        LogDebug(Environment.NewLine + feeServiceResult.DebugInfo);
                    }

                    if (feeServiceResult != null)
                    {
                        this.FeeServiceRevisionId = feeServiceResult.RevisionId;
                    }

                    ForceTransform();  // Force for fields that were just updated.
                }
            }
            else
            {
                if (m_loanFileData.LoanProductMatchingTemplate != null)
                {
                    // If the condition that set the current template is still true, allow it.

                    if (m_loanFileData.ShouldApplyAutoClosingTemplate)
                    {
                        var template = m_loanFileData.LoanProductMatchingTemplate;
                        m_loanFileData.ApplyCCTemplate(template, true);
                        ForceTransform();
                        appliedAutoCCTemplate = true;

                        LogInfo("Apply Auto Closing Cost Template");
                    }
                }
                else
                {
                    // Lender uses the Auto-CC feature, but there was no matching CC Template.
                    // We have to quit per OPM 109254.
                    string errorText = "Could not determine closing costs for this program or scenario: No matching closing cost template found.";
                    string debugText = " sLId:" + m_loanFileData.sLId.ToString() + ", llpTemplateId:" + lLpTemplateId.ToString();
                    PrepareInsufficientInfoStatus(
                        errorText + debugText  // Log
                        , errorText // Error
                        , errorText + debugText // Dev Error
                        );

                    return false;
                }
            }

            return true;
        }

        // OPM 471493.  We need to be able to run these actions multiple times, so this 
        // has been methodized.  Note that given the sensitivity of this code, we are 
        // trying to minimize code change to the main path, so there is some temporary
        // code duplication while this feature is in beta. Eventually this will be the 
        // main path.
        private PmiResult ExecutePMI_Variant(ref decimal? m_lpmiAdjust, XmlDocument xmlDocEval, CacheInvariantPolicyResults cachResult, EvalStack stack, ref bool recalculateMortgageInsurancePerRate, PmiResult forcedWinner = null, bool ignoreDtiRules = true, bool applyWinner = true, bool allowDisqualified = false) 
        {
            PriceAdjustRecord.Args priceAdjustRecordArgs;
            PmiResult pmiProgram = null;

            using (PerformanceStopwatch.Start("PMI Execution"))
            {
                LogInfo("");
                LogInfo("PMI Execution");
                #region PMI
                // Per PDE, this does not need to be done per-rate.  Do it early here.
                if (m_loanFileData.BrokerDB.HasEnabledPMI)
                {

                    if (m_loanFileData.sLT == E_sLT.Conventional
                        && m_loanFileData.sProdConvMIOptionT == E_sProdConvMIOptionT.LendPaidSinglePrem)
                    {
                        // OPM 189515.
                        m_loanFileData.sProMInsCancelLtv = 0;
                        m_loanFileData.sProMInsMidptCancel = false;
                        m_loanFileData.sProMInsCancelMinPmts = 0;
                    }

                    if (lLpmiSupportedOutsidePmi == true && m_loanFileData.sProdConvMIOptionT == E_sProdConvMIOptionT.LendPaidSinglePrem)
                    {
                        // OPM 180150 - Special Program setting--SAES use adjustments for LPMI instead of AutoMI.
                        AdjustMIForLPMI();
                        m_mortgageInsuranceProvider = "";

                        // After we have gathered all adjustments, we also set sLenderUfmipR = [Sum of SAE adjustments with "LPMI" text contained within adjustment description.]
                    }
                    else if (m_loanFileData.sLT == E_sLT.Conventional)
                    {
                        bool useMi = m_loanFileData.sProdConvMIOptionT != E_sProdConvMIOptionT.NoMI && m_loanFileData.sProdConvMIOptionT != E_sProdConvMIOptionT.Blank;

                        // Special hardcode requested for OPM 107793.
                        if (m_loanFileData.sLpProductT == E_sLpProductT.HomePath || m_loanFileData.sLpProductT == E_sLpProductT.HomePathRenovation)
                        {
                            // "Homepath loans are conventional loans that never have mortgage insurance. 
                            // Therefore we need to exclude homepath loans from seeking a PMI program partner in PML."
                            useMi = false;
                        }

                        string execLog = "";
                        string pmiDisqualReason = ""; // OPM 107423

                        if (useMi == true)
                        {
                            if (m_loanFileData.BrokerDB.UseRateOptionVariantMI && forcedWinner != null)
                            {
                                // We have a winner we need to apply rather than calculate.
                                
                                pmiProgram = forcedWinner;
                                if (pmiProgram.IsDenied)
                                {
                                    // Have to simulate what would come out of SetPMIProduct
                                    pmiDisqualReason = "PMI NOT ELIGIBLE.";

                                    if (pmiProgram.DenialReasons.Count() > 0)
                                    {
                                        pmiProgram.DenialReasons.Sort();
                                        pmiDisqualReason += " <br />" + pmiProgram.DenialReasons[0].DenialText;
                                    }
                                }
                            }
                            else
                            {
                                if (m_loanFileData.BrokerDB.UseMiVendorIntegrationsInPricing)
                                {
                                    var pmiQuotePricing = PmiQuotePricing.Create(m_loanFileData, cachResult);
                                    pmiProgram = pmiQuotePricing.GetResult();
                                }
                                else
                                {
                                    priceAdjustRecordArgs = new PriceAdjustRecord.Args(xmlDocEval, m_loanFileData.sSymbolTableForPriceRule, m_runOptions, this, cachResult, stack);
                                        pmiProgram = SetPMIProduct_Variant(priceAdjustRecordArgs, out execLog, out pmiDisqualReason, cachResult, ignoreDtiRules, allowDisqualified);
                                }
                            }
                            
                            // Previously DTI rules were not allowed, so it did not matter, 
                            // but since it does now and we run multiople times.
                            if (m_loanFileData.BrokerDB.UseRateOptionVariantMI && !applyWinner)
                            {
                                // In new flow, we only apply the winner in baseline.
                                // Otherwise we we just need to know who the winner is.
                                
                                return pmiProgram;
                            }
                        }

                        m_loanFileData.sProMInsLckd = false;

                        if (pmiProgram != null)
                        {
                            string selectRateInfo = string.Format("Picked PMI = {0}, Type = {1}, MonthlyPmt = {2}, SinglePmt = {3}"
                                , pmiProgram.PmiCompany
                                , pmiProgram.PmiType
                                , pmiProgram.MonthlyPmt
                                , pmiProgram.SinglePmt
                                );

                            m_mortgageInsuranceProvider = pmiProgram.PmiCompany.ToString();

                            selectRateInfo += "<br /><br /> Execution log: <br />" + execLog;
                            if (IsLogDetailDebugInfo)
                                m_adjDescsHidden.Add(new CAdjustItem(selectRateInfo));

                            m_mortgageInsuranceDesc = PmiResult.PmiTypeDescription(pmiProgram.PmiType) + ", "
                                + (pmiProgram.SinglePct != 0m ? pmiProgram.SinglePct + "% Upfront, " : "")
                                + (pmiProgram.MonthlyPct != 0m ? pmiProgram.MonthlyPct + "% Monthly " : "");

                            if (this.IsLogDebugInfo)
                            {

                                this.LogDebug("PmiSummaryLog:\r\n" + execLog.Replace("<br />* ", "\r\n    ").Replace("<br />", "\r\n        ")
                                             + "\r\n    " + m_mortgageInsuranceDesc + "\r\n");
                            }

                            // OPM 110220.  We don't reuse these MI from previous result.  DEFAULT them.
                            m_loanFileData.sProMInsMon = 0;
                            m_loanFileData.sProMIns2Mon = 0;
                            m_loanFileData.sProMInsR2 = 0;

                            // OPM 143132 - This is for QM.
                            m_loanFileData.sUfmipIsRefundableOnProRataBasis = pmiProgram.UfmipIsRefundableOnProRataBasis;

                            if (m_loanFileData.sProdConvMIOptionT != E_sProdConvMIOptionT.LendPaidSinglePrem)
                            {
                                // OPM 144616 requests this.  Fannie Mae likes it this way.
                                // OPM 212375 updated
                                if (m_loanFileData.sOccTPe == E_sOccT.PrimaryResidence
                                    && m_loanFileData.sUnitsNum < 2)
                                {
                                    m_loanFileData.sProMInsCancelLtv = 78;
                                    m_loanFileData.sProMInsMidptCancel = true;
                                    m_loanFileData.sProMInsCancelMinPmts = 0;
                                }
                                else
                                {
                                    m_loanFileData.sProMInsCancelLtv = 0;
                                    m_loanFileData.sProMInsMidptCancel = false;
                                    m_loanFileData.sProMInsCancelMinPmts = 0;
                                }
                            }
                            // Outlined in section 3.3.1 of Spec for case 26013
                            switch (pmiProgram.PmiType)
                            {
                                case E_sProdConvMIOptionT.BorrPaidMonPrem:
                                    m_loanFileData.sProMInsR = pmiProgram.MonthlyPct;
                                    m_loanFileData.sProMInsPe_rep = pmiProgram.MonthlyPmt.ToString();
                                    m_loanFileData.sFfUfmipR = 0m;
                                    m_loanFileData.sFfUfmip1003 = 0m;
                                    m_mortgageInsuranceMonthlyPmt = pmiProgram.MonthlyPmt.ToString();
                                    m_mortgageInsuranceMonthlyRate = pmiProgram.MonthlyPct.ToString();

                                    // OPM 110220 explains this logic.
                                    if (m_loanFileData.sProMInsR != 0)
                                    {
                                        m_loanFileData.sProMInsMon = 120;
                                        m_loanFileData.sProMIns2Mon = m_loanFileData.sDue - m_loanFileData.sProMInsMon;
                                        m_loanFileData.sProMInsR2 = (m_loanFileData.sProMInsR > 0.2m) ? 0.2m : m_loanFileData.sProMInsR;
                                    }

                                    // case 144618 data clearing
                                    m_loanFileData.sProMInsMb = 0;
                                    m_loanFileData.sProMInsLckd = false;
                                    m_loanFileData.sLenderUfmipR = 0;
                                    m_loanFileData.sLenderUfmipLckd = false;

                                    break;
                                case E_sProdConvMIOptionT.BorrPaidSinglePrem:
                                    m_loanFileData.sFfUfmipR = pmiProgram.SinglePct;
                                    m_loanFileData.sFfUfmip1003 = pmiProgram.SinglePmt;
                                    m_mortgageInsuranceSinglePmt = pmiProgram.SinglePmt.ToString();
                                    m_mortgageInsuranceSingleRate = pmiProgram.SinglePct.ToString();

                                    // case 144618 data clearing
                                    m_loanFileData.sProMInsR = 0;
                                    m_loanFileData.sProMInsMb = 0;
                                    m_loanFileData.sProMInsLckd = false;
                                    m_loanFileData.sProMInsMon = 0;
                                    m_loanFileData.sProMIns2Mon = 0;
                                    m_loanFileData.sProMInsR2 = 0;
                                    m_loanFileData.sLenderUfmipR = 0;
                                    m_loanFileData.sLenderUfmipLckd = false;
                                    m_loanFileData.sUfCashPdLckd = false;

                                    break;
                                case E_sProdConvMIOptionT.BorrPaidSplitPrem:

                                    if (m_loanFileData.sConvSplitMIRT == E_sConvSplitMIRT.Blank)
                                    {
                                        m_evalStatus = E_EvalStatus.Eval_InsufficientInfo;

                                        Tools.LogError(string.Format("BPMI Split, but upfront MI is blank. Program:{0}({1}). Loan:{2}({3})."
                                            , lLpTemplateNm
                                            , lLpTemplateId
                                            , m_loanFileData.sLNm
                                            , m_loanFileData.sLId));

                                        return null;
                                    }

                                    // OPM 110220 explains this logic.
                                    if (m_loanFileData.sProMInsR != 0)
                                    {
                                        m_loanFileData.sProMInsMon = 120;
                                        m_loanFileData.sProMIns2Mon = m_loanFileData.sDue - m_loanFileData.sProMInsMon;
                                        m_loanFileData.sProMInsR2 = (m_loanFileData.sProMInsR > 0.2m) ? 0.2m : m_loanFileData.sProMInsR;
                                    }

                                    m_loanFileData.sProMInsR = pmiProgram.MonthlyPct;
                                    m_loanFileData.sFfUfmipR = pmiProgram.SinglePct;
                                    m_loanFileData.sProMInsPe_rep = pmiProgram.MonthlyPmt.ToString();
                                    m_loanFileData.sFfUfmip1003 = pmiProgram.SinglePmt;
                                    m_mortgageInsuranceMonthlyPmt = pmiProgram.MonthlyPmt.ToString();
                                    m_mortgageInsuranceSinglePmt = pmiProgram.SinglePmt.ToString();
                                    m_mortgageInsuranceMonthlyRate = pmiProgram.MonthlyPct.ToString();
                                    m_mortgageInsuranceSingleRate = pmiProgram.SinglePct.ToString();

                                    // case 144618 data clearing
                                    m_loanFileData.sProMInsMb = 0;
                                    m_loanFileData.sProMInsLckd = false;
                                    m_loanFileData.sLenderUfmipR = 0;
                                    m_loanFileData.sLenderUfmipLckd = false;

                                    break;
                                case E_sProdConvMIOptionT.LendPaidSinglePrem:

                                    if (m_loanFileData.sPricingModeT != E_sPricingModeT.InternalInvestorPricing)
                                    {

                                        PriceAdjustRecord.AdjustItem lpmiItem = new PriceAdjustRecord.AdjustItem(0, 0, pmiProgram.SinglePct, 0, 0, "LPMI Premium", "", true, Guid.Empty);
                                        //bool addAdjustment = true;


                                        if (m_loanFileData.BrokerDB.UseRateOptionVariantMI && applyWinner)
                                        {
                                            if (ignoreDtiRules) // Meaning we are in a baseline
                                            {
                                                m_adjDescsDisclosed.RemoveAll(p => p.Description == "LPMI Premium");

                                                CAdjustItem lpmiAdjustment = lpmiItem.ToCAdjustItem(m_losConvert, lHasQRateInRateOptions, IsOptionArm);
                                                if (lpmiAdjustment != null)
                                                    m_adjDescsDisclosed.Add(lpmiAdjustment);

                                                m_feeDeltaDisclosed += pmiProgram.SinglePct;

                                                m_lpmiAdjust = pmiProgram.SinglePct;
                                            }
                                        }
                                    }

                                    // OPM 122017.
                                    m_mortgageInsuranceSinglePmt = pmiProgram.SinglePmt.ToString();
                                    m_mortgageInsuranceSingleRate = pmiProgram.SinglePct.ToString();
                                    m_loanFileData.sLenderUfmipR = pmiProgram.SinglePct; // OPM 132071

                                    // case 144618 data clearing
                                    AdjustMIForLPMI();

                                    break;
                            }

                            // if ( pmiProgram.IsDenied)
                            if (pmiDisqualReason != string.Empty
                                && !m_reasonArray.Any(p => p.Description.Contains(pmiDisqualReason)))
                            {
                                m_evalStatus = E_EvalStatus.Eval_Ineligible;
                                m_PmiDisqual = true;
                                m_reasonArray.Add(new CStipulation(pmiDisqualReason, ""));
                                if (m_disqualifiedRules.Length > 0)
                                    m_disqualifiedRules += "<br>";
                                m_disqualifiedRules += " * " + pmiDisqualReason;
                            }
                        }
                        else
                        {
                            if (useMi == true)
                            {
                                // PMI is enabled and required and this is a Conventional loan, but we were denied all PMI programs
                                // Means this entire program is ineligible.

                                if (m_loanFileData.BrokerDB.UseRateOptionVariantMI && !ignoreDtiRules)
                                {
                                    // We are not in "baseline" pricing, we should save this disqual rather 
                                    // than apply it to the loanfile.
                                }
                                else
                                {
                                    m_evalStatus = E_EvalStatus.Eval_Ineligible;
                                    m_PmiDisqual = true;
                                    m_reasonArray.Add(new CStipulation(pmiDisqualReason, ""));
                                    if (m_disqualifiedRules.Length > 0)
                                        m_disqualifiedRules += "<br>";
                                    m_disqualifiedRules += " * " + pmiDisqualReason;
                                }
                            }
                            else
                            {
                                execLog = "THIS IS A NO-MI SCENARIO";
                                m_mortgageInsuranceDesc = "None";
                            }

                            if (IsLogDetailDebugInfo)
                                m_adjDescsHidden.Add(new CAdjustItem("NO PMI SELECTED" + "<br /><br /> Execution log: <br />" + execLog));

                            m_loanFileData.sProMInsLckd = false;
                            m_loanFileData.sProMInsR = 0m;
                            m_loanFileData.sFfUfmipR = 0m;
                            m_loanFileData.sProMInsPe_rep = "0";
                            m_loanFileData.sFfUfmip1003 = 0;
                            m_mortgageInsuranceMonthlyPmt = "0";
                            m_mortgageInsuranceSinglePmt = "0";
                            m_mortgageInsuranceMonthlyRate = "0";
                            m_mortgageInsuranceSingleRate = "0";

                        }
                    } // Conventional
                    else if (m_loanFileData.sLT == E_sLT.FHA)
                    {
                        // OPM. 114052.  AverageOutstandingBalance calculation needs note rate to calculate payment correctly in comparison.
                        if (m_runOptions.PricingState != null)
                        {

                            m_loanFileData.sNoteIR_rep = m_runOptions.PricingState.GetNoteRate(m_runOptions.GetMode());
                        }

                        m_loanFileData.Update_sProMInsFieldsPer110559();

                        m_mortgageInsuranceDesc = "FHA" + ", " + m_loanFileData.sProdFhaUfmip_rep + " upfront, " + m_loanFileData.sProMInsR_rep + " monthly";
                        if (this.m_loanFileData.sProMInsT != E_PercentBaseT.LoanAmount)
                        {
                            m_loanFileData.sProMInsT = E_PercentBaseT.AverageOutstandingBalance; // Per OPM 114052
                        }
                        m_loanFileData.sProMInsPe_rep = Math.Round((m_loanFileData.sProMInsR * m_loanFileData.sProMInsBaseAmt / 1200.00M), 2).ToString();
                        m_loanFileData.sProMInsLckd = false;
                        m_mortgageInsuranceMonthlyPmt = m_loanFileData.sProMIns_rep;
                        m_mortgageInsuranceSinglePmt = m_loanFileData.sProdFhaUfmip_rep;

                        // case 144618 data clearing
                        m_loanFileData.sLenderUfmipR = 0;
                        m_loanFileData.sLenderUfmipLckd = false;
                        m_loanFileData.sProMInsMidptCancel = false;
                        m_loanFileData.sProMIns2Mon = 0;
                        m_loanFileData.sProMInsR2 = 0;

                        recalculateMortgageInsurancePerRate = true;
                    }
                    else if (m_loanFileData.sLT == E_sLT.UsdaRural)
                    {
                        // OPM. 114052.  AverageOutstandingBalance calculation needs note rate to calculate payment correctly in comparison.
                        if (m_runOptions.PricingState != null)
                        {
                            m_loanFileData.sNoteIR_rep = m_runOptions.PricingState.GetNoteRate(m_runOptions.GetMode());
                        }

                        DateTime dateToUse = DateTime.Now;
                        if (m_loanFileData.sMiCommitmentReceivedD.IsValid)
                        {
                            dateToUse = m_loanFileData.sMiCommitmentReceivedD.DateTimeForComputation;
                        }

                        if (dateToUse <= new DateTime(2016, 9, 30))
                        {
                            m_loanFileData.sProMInsR = .5m;
                        }
                        else
                        {
                            m_loanFileData.sProMInsR = .35m;
                        }

                        m_mortgageInsuranceDesc = "USDA" + ", " + m_loanFileData.sProdUSDAGuaranteeFee_rep + " upfront, " + m_loanFileData.sProMInsR_rep + " monthly";
                        m_loanFileData.sProMInsCancelLtv = 0;
                        m_loanFileData.sProMInsCancelMinPmts = 0;

                        m_loanFileData.sProMInsT = E_PercentBaseT.AverageOutstandingBalance; // Per OPM 114052
                        m_loanFileData.sProMInsPe_rep = Math.Round((m_loanFileData.sProMInsR * m_loanFileData.sProMInsBaseAmt / 1200.00M), 2).ToString();
                        m_loanFileData.sProMInsLckd = false;
                        m_mortgageInsuranceMonthlyPmt = m_loanFileData.sProMIns_rep;
                        m_mortgageInsuranceSinglePmt = m_loanFileData.sProdUSDAGuaranteeFee_rep;

                        // case 144618 data clearing
                        m_loanFileData.sLenderUfmipR = 0;
                        m_loanFileData.sLenderUfmipLckd = false;
                        m_loanFileData.sProMInsMidptCancel = false;
                        m_loanFileData.sProMInsMb = 0;
                        m_loanFileData.sProMInsMon = m_loanFileData.sDue;
                        m_loanFileData.sFfUfmip1003Lckd = false;
                        m_loanFileData.sUfCashPdLckd = false;
                        m_loanFileData.sProMIns2Mon = 0;
                        m_loanFileData.sProMInsR2 = 0;

                        recalculateMortgageInsurancePerRate = true;
                    }
                    else if (m_loanFileData.sLT == E_sLT.VA)
                    {
                        m_mortgageInsuranceDesc = "VA" + ", " + m_loanFileData.sProdVaFundingFee_rep + " upfront ";
                        m_loanFileData.sProMInsPe_rep = "0"; // VA has no monthly
                        m_loanFileData.sProMInsLckd = false;
                        m_mortgageInsuranceMonthlyPmt = m_loanFileData.sProMIns_rep;
                        m_mortgageInsuranceSinglePmt = m_loanFileData.sProdVaFundingFee_rep;

                        // case 144618 data clearing
                        m_loanFileData.sProMInsR = 0;
                        m_loanFileData.sProMInsMb = 0;
                        m_loanFileData.sProMInsLckd = false;
                        m_loanFileData.sLenderUfmipR = 0;
                        m_loanFileData.sLenderUfmipLckd = false;
                        m_loanFileData.sProMInsMidptCancel = false;
                        m_loanFileData.sUfCashPdLckd = false;
                        m_loanFileData.sProMInsMon = 0;
                        m_loanFileData.sProMIns2Mon = 0;
                        m_loanFileData.sProMInsR2 = 0;

                    }
                }
                #endregion
            }

            return pmiProgram;
        }

        private PmiResult SetPMIProduct(PriceAdjustRecord.Args priceAdjustRecordArgs, out string execLog, out string disqualText, CacheInvariantPolicyResults cache)
        {
            PmiResult winningResult = null;
            List<PmiResult> tieBreakers = new List<PmiResult>();
            disqualText = "PMI NOT ELIGIBLE.";
            List<PmiDenial> denials = new List<PmiDenial>();
            StringBuilder log = new StringBuilder();

            // TEMP.  While we tune this feature.
            System.Diagnostics.Stopwatch debugTimer = new System.Diagnostics.Stopwatch();

            HashSet<Guid> totalLoadedPolicies = new HashSet<Guid>();

            Dictionary<int, List<E_PmiCompanyT>> rankList = m_loanFileData.BrokerDB.PmiRankList; // Cached in broker
            foreach (var rank in rankList)
            {

                if (winningResult != null && rank.Key > winningResult.Rank)
                    continue;  // Winner at a lower rank.

                foreach (var company in rank.Value)
                {
                    debugTimer.Reset();
                    debugTimer.Start();

                    var pmiProgram = m_pmiManager.GetPmiProgram(company, m_loanFileData.sProdConvMIOptionT);
                    if (pmiProgram == null)
                    {
                        log.AppendFormat("<br />* Rank{0}: No PMI program for company {1} with type {2}. <br />"
                            , rank.Key
                            , company
                            , m_loanFileData.sProdConvMIOptionT
                            );
                        if (IsLogDebugInfo)
                        {
                            LogDebug(string.Format("Rank{0}: No PMI program for company {1} with type {2}.",
                                rank.Key, company, m_loanFileData.sProdConvMIOptionT));
                        }
                        continue;
                    }
                    if (IsLogDebugInfo)
                    {
                        LogDebug("Rank" + rank.Key + ". PMI Company=" + pmiProgram.Company + ". PmiProgramId:" + pmiProgram.PmiProgramId);
                    }

                    PriceAdjustRecord thisPricing = new PriceAdjustRecord(false);

                    int policyCount = 0;
                    int skippedCount = 0;

                    //StringBuilder debugInfo = new StringBuilder();

                    HashSet<Guid> skipSet = new HashSet<Guid>();

                    EvalStack stack = new EvalStack(EvalStackSize);
                    XmlDocument xmlDocEval = new XmlDocument();

                    // OPM 150035. 01/30/14 mf. PMI evaluation is BEFORE Q-Rules are run.  Any rule that references
                    // a Q value keyword here is an SAE mistake and makes execution for this PMI program invalid.  PDE advised
                    // these ones should be pulled out of the winning PMI progams pool and email SAE.  
                    // (Pricing still goes on if there is a different valid winner).
                    Func<AbstractRtPricePolicy, bool> PolicyUsesQValues = (policy) => { return policy.FirstIdxOfQValue != policy.LastIdxOfQValue; };
                    string invalidQAccessPolicy = string.Empty;

                    foreach (Guid policyId in pmiProgram.Policies)
                    {
                        if (skipSet.Contains(policyId))
                        {
                            if (!ConstSite.PricingCompactLogEnabled)
                            {
                                LogInfo("    Skip PMI PolicyId:" + policyId);
                            }
                            continue; // This policy was already skipped
                        }

                        totalLoadedPolicies.Add(policyId);

                        MyPricePolicy pricePolicy = LoanProgramSet.GetPmiPricePolicy(policyId);

                        pricePolicy.LoadRules();

                        if (PolicyUsesQValues(pricePolicy))
                        {
                            invalidQAccessPolicy = pricePolicy.PricePolicyDescription + "(policyid:" + pricePolicy.PricePolicyId + ")";
                            break; // We are done.  This result is invalid.
                        }

                        MyRuleList rules = pricePolicy.Rules;

                        foreach (MyRule rule in rules)
                        {
                            MyConsequence consequence = rule.Consequence;
                            if (consequence.Skip)
                            {
                                StringBuilder subDebugInfo = new StringBuilder();
                                bool result = rule.Condition.Evaluate(xmlDocEval, m_loanFileData.sSymbolTableForPriceRule, stack, subDebugInfo, IsLogDebugInfo);

                                if (IsLogDebugInfo && (result || !ConstSite.PricingCompactLogEnabled))
                                {
                                    LogDebug("    Eval: PMI PolicyId:" + policyId + ", RuleId:" + rule.RuleId + ", Rule Desc:[" + rule.Description + "]. Result: " + result);
                                    LogDebug(subDebugInfo.ToString());
                                }
                                if (result == true)
                                {
                                    // Skip this policy and all its children
                                    m_pmiManager.PolicyRelationCenter.AddDescendantsToSet(policyId, skipSet);
                                    skipSet.Add(policyId);
                                    break;
                                }
                            }
                        }
                    }

                    if (invalidQAccessPolicy.Length == 0)
                    {
                        foreach (Guid policyId in pmiProgram.Policies)
                        {
                            if (skipSet.Contains(policyId))
                            {
                                if (!ConstSite.PricingCompactLogEnabled)
                                {
                                    LogInfo("    Skip PMI PolicyId:" + policyId);
                                }
                                skippedCount++;
                                continue;
                            }

                            MyPricePolicy pricePolicy = LoanProgramSet.GetPmiPricePolicy(policyId);

                            if (PolicyUsesQValues(pricePolicy))
                            {
                                invalidQAccessPolicy = pricePolicy.PricePolicyDescription + "(policyid:" + pricePolicy.PricePolicyId + ")";
                                break; // We are done.  This result is invalid.
                            }

                            PriceAdjustRecord.Compute(pricePolicy, ref priceAdjustRecordArgs, thisPricing, false /*DTI ONLY */, m_applications, m_debugInfo, m_loanFileData.sPricingModeT, IsLogDebugInfo, m_runOptions.IsGetAllReasons);
                            policyCount++;
                        }
                    }

                    if (thisPricing.Disqualify == false)
                    {

                        // Potential Winner here, but might need to be Best-X'd with others at this rank.
                        PmiResult result = new PmiResult(rank.Key, pmiProgram, thisPricing, m_loanFileData);

                        StringBuilder policyDebug = new StringBuilder();
                        foreach (LendersOfficeApp.los.RatePrice.PriceAdjustRecord.AdjustItem adjDesc in thisPricing.m_adjDescsDisclosed)
                        {
                            CAdjustItem adjust = adjDesc.ToCAdjustItem(m_losConvert, lHasQRateInRateOptions, IsOptionArm);
                            policyDebug.Append(" |_" + adjust.Description + " Rate: " + adjust.Rate + ",  Fee: " + adjust.Fee + ". " + adjust.DebugString + "<br />");
                        }
                        foreach (LendersOfficeApp.los.RatePrice.PriceAdjustRecord.AdjustItem adjDesc in thisPricing.m_adjDescsHidden)
                        {
                            CAdjustItem adjust = adjDesc.ToCAdjustItem(m_losConvert, lHasQRateInRateOptions, IsOptionArm);
                            policyDebug.Append(" |_" + adjust.Description + " Rate: " + adjust.Rate + ",  Fee: " + adjust.Fee + ". " + adjust.DebugString + "<br />");
                        }

                        log.AppendFormat("{3}* Rank:{0} Company: {1}, Type: {2} {6} {3} SinglePmt: {4} {3} MonthlyPmt: {5} {3} {7}"
                            , result.Rank
                            , result.PmiCompany
                            , result.PmiType
                            , "<br />"
                            , result.SinglePmt
                            , result.MonthlyPmt
                            , " ( " + policyCount + " policies executed,  " + skippedCount + " skipped )"
                            , policyDebug.ToString()
                            );

                        if (this.IsLogDebugInfo)
                        {
                            LogDebug($"    => Rank{result.Rank} Company:{result.PmiCompany}, Type:{result.PmiType} ({policyCount} policies executed, {skippedCount} skipped)\r\n"
                                     + (result.SinglePmt != 0 ? "    SinglePmt: {result.SinglePmt}\r\n" : string.Empty)
                                     + (result.MonthlyPmt != 0 ? "    MonthlyPmt: {result.MonthlyPmt}\r\n" : string.Empty)
                                     );
                        }

                        // 108602.  If any eligible MI program evaluates to a 0 premium, it is an SAE rule error.
                        // We should email.  To avoid spamming external OPM and creating tons of cases,
                        // we add one email per pricing run (instead of every program), and use the email-dedupe mechanism.

                        bool isValidPmiResult = true;
                        if (((result.PmiType == E_sProdConvMIOptionT.BorrPaidMonPrem || result.PmiType == E_sProdConvMIOptionT.BorrPaidSplitPrem) && result.MonthlyPct == 0)
                            || result.PmiType == E_sProdConvMIOptionT.LendPaidSinglePrem && result.SinglePct == 0)
                        {
                            isValidPmiResult = false;

                            if ( !m_loanFileData.BrokerDB.UseRateOptionVariantMI // OPM 475358. In rate variant MI, $0 premium can happen in iterations.
                                && result.PmiType != E_sProdConvMIOptionT.BorrPaidSplitPrem) // Case 183756.  0$ monthly can happen with Split in expected result.  Only remove as eligible--do not email eOPM.
                            {
                                string Subject = "MI Provider " + result.PmiCompany + " evaluated to $0 premium for " + PmiResult.PmiTypeDescription(result.PmiType);

                                // Dedupe by subject here--this could repeat per program.
                                if (cache.SaeErrors.ContainsKey(Subject) == false)
                                {
                                    string Body = string.Format("Loan: {0}, Program: {1}, Lender: {2}.",
                                        m_loanFileData.sLNm + "::" + m_loanFileData.sLId
                                        , lLpTemplateNm + "::" + lLpTemplateId
                                        , m_loanFileData.BrokerDB.Name + "::" + m_loanFileData.BrokerDB.BrokerID
                                            );

                                    cache.SaeErrors.Add(Subject, Body);
                                }
                            }
                        }

                        // 150035.  Another reason to notify SAEs.  Error when QValues accessed in PMI phase.
                        if (invalidQAccessPolicy.Length != 0)
                        {
                            isValidPmiResult = false;
                            string Subject = "MI Provider execution for " + result.PmiCompany + " accessed QValues for " + PmiResult.PmiTypeDescription(result.PmiType);

                            // Dedupe by subject here--this could repeat per program.
                            if (cache.SaeErrors.ContainsKey(Subject) == false)
                            {
                                string Body = string.Format("Loan: {0}, Program: {1}, Lender: {2}. " + invalidQAccessPolicy,
                                    m_loanFileData.sLNm + "::" + m_loanFileData.sLId
                                    , lLpTemplateNm + "::" + lLpTemplateId
                                    , m_loanFileData.BrokerDB.Name + "::" + m_loanFileData.BrokerDB.BrokerID
                                        );

                                cache.SaeErrors.Add(Subject, Body);
                            }

                        }


                        // OPM 135689.
                        int direction = m_loanFileData.BrokerDB.IsHighestPmiProviderWins ? -1 : 1;

                        if (isValidPmiResult)
                        {
                            var comparison = result.CompareTo(winningResult);
                            var isCurrentMiProvider = m_loanFileData.sMiCompanyNmT == m_loanFileData.MapMiProviderToCompanyNmT(result.PmiCompany.ToString());

                            if (winningResult == null || direction * comparison < 0)
                            {
                            winningResult = result;
                                tieBreakers = isCurrentMiProvider ? null : new List<PmiResult>() { result };
                        }
                            else if(comparison == 0 && isCurrentMiProvider)
                            {
                                winningResult = result;
                                tieBreakers = null;
                    }
                            else if(comparison == 0 && tieBreakers != null)
                            {
                                tieBreakers.Add(result);
                            }
                        }
                    }
                    else
                    {
                        StringBuilder policyDebug = new StringBuilder();
                        foreach (CStipulation disqualReason in thisPricing.m_reasonArray)
                        {
                            policyDebug.Append(" |_" + disqualReason.Description + " " + disqualReason.DebugString + "<br />");
                        }

                        log.AppendFormat("<br />* Rank:{0}: Company {1} with type {2} was disqualified. {3} <br /> {4}"
                            , rank.Key
                            , company
                            , m_loanFileData.sProdConvMIOptionT
                            , " ( " + policyCount + " policies,  " + skippedCount + " skipped )"
                            , policyDebug.ToString()
                            );

                        denials.Add(new PmiDenial() { NumOfDenials = thisPricing.m_reasonArray.Count, Rank = rank.Key, Company = company.ToString(), DenialText = thisPricing.m_disqualifiedRules });

                        if (this.IsLogDebugInfo)
                        {
                            LogDebug($"    ===> Rank{rank.Key}: Company {company} with type {m_loanFileData.sProdConvMIOptionT} was disqualified. ({policyCount} policies,  {skippedCount} skipped).");
                        }
                    }
                    debugTimer.Stop();
                    log.Append("( " + company + " total execution time:" + debugTimer.Elapsed.TotalMilliseconds.ToString() + "ms )  <br /> ");
                }
            }

            log.AppendLine($"<br />* Total touched xmlPolicy: {totalLoadedPolicies.Count}");

            execLog = log.ToString();

            if(tieBreakers != null && tieBreakers.Count > 1)
            {
                Random randomGenerator = new Random(m_loanFileData.sLId.GetHashCode());
                winningResult = tieBreakers[randomGenerator.Next(tieBreakers.Count)];
            }

            if (winningResult != null)
            {
                disqualText = "";
            }
            else
            {
                if (denials.Count > 0)
                {
                    denials.Sort();
                    disqualText += " <br />" + denials[0].DenialText;
                }
            }

            return winningResult;
        }

        // Rate-Variant PMI version of application.  While this bit flag is in beta,
        // we have to duplicate this code.  Once we can retire the bit we would only
        // use this one.
        private PmiResult SetPMIProduct_Variant(PriceAdjustRecord.Args priceAdjustRecordArgs, out string execLog, out string disqualText, CacheInvariantPolicyResults cache, bool ignoreDtiRules, bool allowDisqualified)
        {
            PmiResult winningResult = null;
            List<PmiResult> tieBreakers = new List<PmiResult>();
            disqualText = "PMI NOT ELIGIBLE.";
            List<PmiDenial> denials = new List<PmiDenial>();
            StringBuilder log = new StringBuilder();

            // TEMP.  While we tune this feature.
            System.Diagnostics.Stopwatch debugTimer = new System.Diagnostics.Stopwatch();

            HashSet<Guid> totalLoadedPolicies = new HashSet<Guid>();

            Dictionary<int, List<E_PmiCompanyT>> rankList = m_loanFileData.BrokerDB.PmiRankList; // Cached in broker
            foreach (var rank in rankList)
            {

                if (winningResult != null && rank.Key > winningResult.Rank)
                    continue;  // Winner at a lower rank.

                foreach (var company in rank.Value)
                {
                    debugTimer.Reset();
                    debugTimer.Start();

                    var pmiProgram = m_pmiManager.GetPmiProgram(company, m_loanFileData.sProdConvMIOptionT);
                    if (pmiProgram == null)
                    {
                        log.AppendFormat("<br />* Rank{0}: No PMI program for company {1} with type {2}. <br />"
                            , rank.Key
                            , company
                            , m_loanFileData.sProdConvMIOptionT
                            );
                        if (IsLogDebugInfo)
                        {
                            LogDebug(string.Format("Rank{0}: No PMI program for company {1} with type {2}.",
                                rank.Key, company, m_loanFileData.sProdConvMIOptionT));
                        }
                        continue;
                    }
                    if (IsLogDebugInfo)
                    {
                        LogDebug("Rank" + rank.Key + ". PMI Company=" + pmiProgram.Company + ". PmiProgramId:" + pmiProgram.PmiProgramId);
                    }

                    // Increment the counter per MI company.
                    if (m_loanFileData.BrokerDB.UseRateOptionVariantMI)
                    {
                        var counts = cache.pmiCompanyUseCount;
                        if (!counts.ContainsKey(company))
                        {
                            counts[company] = 1;
                        }
                        else
                        {
                            counts[company]++;
                        }
                    }

                    PriceAdjustRecord thisPricing = new PriceAdjustRecord(false);

                    int policyCount = 0;
                    int skippedCount = 0;

                    HashSet<Guid> skipSet = new HashSet<Guid>();

                    EvalStack stack = new EvalStack(EvalStackSize);
                    XmlDocument xmlDocEval = new XmlDocument();

                    // OPM 150035. 01/30/14 mf. PMI evaluation is BEFORE Q-Rules are run.  Any rule that references
                    // a Q value keyword here is an SAE mistake and makes execution for this PMI program invalid.  PDE advised
                    // these ones should be pulled out of the winning PMI progams pool and email SAE.  
                    // (Pricing still goes on if there is a different valid winner).
                    Func<AbstractRtPricePolicy, bool> PolicyUsesQValues = (policy) => { return policy.FirstIdxOfQValue != policy.LastIdxOfQValue; };
                    string invalidQAccessPolicy = string.Empty;

                    foreach (Guid policyId in pmiProgram.Policies)
                    {
                        if (skipSet.Contains(policyId))
                        {
                            if (!ConstSite.PricingCompactLogEnabled)
                            {
                                LogInfo("    Skip PMI PolicyId:" + policyId);
                            }
                            continue; // This policy was already skipped
                        }

                        totalLoadedPolicies.Add(policyId);

                        MyPricePolicy pricePolicy = LoanProgramSet.GetPmiPricePolicy(policyId);

                        pricePolicy.LoadRules();

                        if (PolicyUsesQValues(pricePolicy))
                        {
                            invalidQAccessPolicy = pricePolicy.PricePolicyDescription + "(policyid:" + pricePolicy.PricePolicyId + ")";
                            break; // We are done.  This result is invalid.
                        }

                        MyRuleList rules = pricePolicy.Rules;

                        foreach (MyRule rule in rules)
                        {
                            MyConsequence consequence = rule.Consequence;
                            if (consequence.Skip)
                            {
                                StringBuilder subDebugInfo = new StringBuilder();
                                bool result = rule.Condition.Evaluate(xmlDocEval, m_loanFileData.sSymbolTableForPriceRule, stack, subDebugInfo, IsLogDebugInfo);

                                if (IsLogDebugInfo && (result || !ConstSite.PricingCompactLogEnabled))
                                {
                                    LogDebug("    Eval: PMI PolicyId:" + policyId + ", RuleId:" + rule.RuleId + ", Rule Desc:[" + rule.Description + "]. Result: " + result);
                                    LogDebug(subDebugInfo.ToString());
                                }
                                if (result == true)
                                {
                                    // Skip this policy and all its children
                                    m_pmiManager.PolicyRelationCenter.AddDescendantsToSet(policyId, skipSet);
                                    skipSet.Add(policyId);
                                    break;
                                }
                            }
                        }
                    }

                    if (invalidQAccessPolicy.Length == 0)
                    {
                        foreach (Guid policyId in pmiProgram.Policies)
                        {
                            if (skipSet.Contains(policyId))
                            {
                                if (!ConstSite.PricingCompactLogEnabled)
                                {
                                    LogInfo("    Skip PMI PolicyId:" + policyId);
                                }
                                skippedCount++;
                                continue;
                            }

                            MyPricePolicy pricePolicy = LoanProgramSet.GetPmiPricePolicy(policyId);

                            if (PolicyUsesQValues(pricePolicy))
                            {
                                invalidQAccessPolicy = pricePolicy.PricePolicyDescription + "(policyid:" + pricePolicy.PricePolicyId + ")";
                                break; // We are done.  This result is invalid.
                            }

                            if (!ignoreDtiRules)
                            {
                                    PriceAdjustRecord.Compute(pricePolicy, ref priceAdjustRecordArgs, thisPricing, true /*DTI ONLY */, m_applications, m_debugInfo, m_loanFileData.sPricingModeT, IsLogDebugInfo, isGetAllReasons: true);
                            }

                                PriceAdjustRecord.Compute(pricePolicy, ref priceAdjustRecordArgs, thisPricing, false /*DTI ONLY */, m_applications, m_debugInfo, m_loanFileData.sPricingModeT, IsLogDebugInfo, isGetAllReasons: true);
                            policyCount++;
                        }
                    }

                    if (allowDisqualified || thisPricing.Disqualify == false)
                    {

                        // Potential Winner here, but might need to be Best-X'd with others at this rank.
                        PmiResult result = new PmiResult(rank.Key, pmiProgram, thisPricing, m_loanFileData);

                        StringBuilder policyDebug = new StringBuilder();
                        foreach (LendersOfficeApp.los.RatePrice.PriceAdjustRecord.AdjustItem adjDesc in thisPricing.m_adjDescsDisclosed)
                        {
                            CAdjustItem adjust = adjDesc.ToCAdjustItem(m_losConvert, lHasQRateInRateOptions, IsOptionArm);
                            policyDebug.Append(" |_" + adjust.Description + " Rate: " + adjust.Rate + ",  Fee: " + adjust.Fee + ". " + adjust.DebugString + "<br />");
                        }
                        foreach (LendersOfficeApp.los.RatePrice.PriceAdjustRecord.AdjustItem adjDesc in thisPricing.m_adjDescsHidden)
                        {
                            CAdjustItem adjust = adjDesc.ToCAdjustItem(m_losConvert, lHasQRateInRateOptions, IsOptionArm);
                            policyDebug.Append(" |_" + adjust.Description + " Rate: " + adjust.Rate + ",  Fee: " + adjust.Fee + ". " + adjust.DebugString + "<br />");
                        }

                        log.AppendFormat("{3}* Rank:{0} Company: {1}, Type: {2} {6} {3} SinglePmt: {4} {3} MonthlyPmt: {5} {3} {7}"
                            , result.Rank
                            , result.PmiCompany
                            , result.PmiType
                            , "<br />"
                            , result.SinglePmt
                            , result.MonthlyPmt
                            , " ( " + policyCount + " policies executed,  " + skippedCount + " skipped )"
                            , policyDebug.ToString()
                            );

                        if (this.IsLogDebugInfo)
                        {
                            LogDebug($"    => Rank{result.Rank} Company:{result.PmiCompany}, Type:{result.PmiType} ({policyCount} policies executed, {skippedCount} skipped)\r\n"
                                     + (result.SinglePmt != 0 ? $"    SinglePmt: {result.SinglePmt}\r\n" : string.Empty)
                                     + (result.MonthlyPmt != 0 ? $"    MonthlyPmt: {result.MonthlyPmt}\r\n" : string.Empty)
                                     );
                        }

                        // 108602.  If any eligible MI program evaluates to a 0 premium, it is an SAE rule error.
                        // We should email.  To avoid spamming external OPM and creating tons of cases,
                        // we add one email per pricing run (instead of every program), and use the email-dedupe mechanism.

                        bool isValidPmiResult = true;
                        if (((result.PmiType == E_sProdConvMIOptionT.BorrPaidMonPrem || result.PmiType == E_sProdConvMIOptionT.BorrPaidSplitPrem) && result.MonthlyPct == 0)
                            || result.PmiType == E_sProdConvMIOptionT.LendPaidSinglePrem && result.SinglePct == 0)
                        {
                            isValidPmiResult = false;

                            if (!m_loanFileData.BrokerDB.UseRateOptionVariantMI // OPM 475358. In rate variant MI, $0 premium can happen in iterations.
                                && result.PmiType != E_sProdConvMIOptionT.BorrPaidSplitPrem) // Case 183756.  0$ monthly can happen with Split in expected result.  Only remove as eligible--do not email eOPM.
                            {
                                string Subject = "MI Provider " + result.PmiCompany + " evaluated to $0 premium for " + PmiResult.PmiTypeDescription(result.PmiType);

                                // Dedupe by subject here--this could repeat per program.
                                if (cache.SaeErrors.ContainsKey(Subject) == false)
                                {
                                    string Body = string.Format("Loan: {0}, Program: {1}, Lender: {2}.",
                                        m_loanFileData.sLNm + "::" + m_loanFileData.sLId
                                        , lLpTemplateNm + "::" + lLpTemplateId
                                        , m_loanFileData.BrokerDB.Name + "::" + m_loanFileData.BrokerDB.BrokerID
                                            );

                                    cache.SaeErrors.Add(Subject, Body);
                                }
                            }
                        }

                        // 150035.  Another reason to notify SAEs.  Error when QValues accessed in PMI phase.
                        if (invalidQAccessPolicy.Length != 0)
                        {
                            isValidPmiResult = false;
                            string Subject = "MI Provider execution for " + result.PmiCompany + " accessed QValues for " + PmiResult.PmiTypeDescription(result.PmiType);

                            // Dedupe by subject here--this could repeat per program.
                            if (cache.SaeErrors.ContainsKey(Subject) == false)
                            {
                                string Body = string.Format("Loan: {0}, Program: {1}, Lender: {2}. " + invalidQAccessPolicy,
                                    m_loanFileData.sLNm + "::" + m_loanFileData.sLId
                                    , lLpTemplateNm + "::" + lLpTemplateId
                                    , m_loanFileData.BrokerDB.Name + "::" + m_loanFileData.BrokerDB.BrokerID
                                        );

                                cache.SaeErrors.Add(Subject, Body);
                            }

                        }

                        if (thisPricing.Disqualify)
                        {
                            result.DenialReasons.Add(new PmiDenial() { NumOfDenials = thisPricing.m_reasonArray.Count, Rank = rank.Key, Company = company.ToString(), DenialText = thisPricing.m_disqualifiedRules });
                        }

                        // OPM 135689.
                        int direction = m_loanFileData.BrokerDB.IsHighestPmiProviderWins ? -1 : 1;

                        if (isValidPmiResult)
                        {
                            var comparison = result.CompareTo(winningResult);
                            var isCurrentMiProvider = m_loanFileData.sMiCompanyNmT == m_loanFileData.MapMiProviderToCompanyNmT(result.PmiCompany.ToString());

                            if (winningResult == null || direction * comparison < 0)
                            {
                                winningResult = result;
                                tieBreakers = isCurrentMiProvider ? null : new List<PmiResult>() { result };
                            }
                            else if (comparison == 0 && isCurrentMiProvider)
                            {
                                winningResult = result;
                                tieBreakers = null;
                            }
                            else if (comparison == 0 && tieBreakers != null)
                            {
                                tieBreakers.Add(result);
                            }
                        }
                    }
                    else
                    {
                        StringBuilder policyDebug = new StringBuilder();
                        foreach (CStipulation disqualReason in thisPricing.m_reasonArray)
                        {
                            policyDebug.Append(" |_" + disqualReason.Description + " " + disqualReason.DebugString + "<br />");
                        }

                        log.AppendFormat("<br />* Rank:{0}: Company {1} with type {2} was disqualified. {3} <br /> {4}"
                            , rank.Key
                            , company
                            , m_loanFileData.sProdConvMIOptionT
                            , " ( " + policyCount + " policies,  " + skippedCount + " skipped )"
                            , policyDebug.ToString()
                            );

                        denials.Add(new PmiDenial() { NumOfDenials = thisPricing.m_reasonArray.Count, Rank = rank.Key, Company = company.ToString(), DenialText = thisPricing.m_disqualifiedRules });

                        if (this.IsLogDebugInfo)
                        {
                            LogDebug($"    ===> Rank{rank.Key}: Company {company} with type {m_loanFileData.sProdConvMIOptionT} was disqualified. ({policyCount} policies,  {skippedCount} skipped).");
                        }
                    }
                    debugTimer.Stop();
                    log.Append("( " + company + " total execution time:" + debugTimer.Elapsed.TotalMilliseconds.ToString() + "ms )  <br /> ");
                }
            }

            log.AppendLine($"<br />* Total touched xmlPolicy: {totalLoadedPolicies.Count}");

            execLog = log.ToString();
            if (tieBreakers != null && tieBreakers.Count > 1)
            {
                Random randomGenerator = new Random(m_loanFileData.sLId.GetHashCode());
                winningResult = tieBreakers[randomGenerator.Next(tieBreakers.Count)];
            }

            if (winningResult != null && !winningResult.IsDenied)
            {
                disqualText = "";
            }
            else
            {
                if ( winningResult != null )
                {
                    denials = winningResult.DenialReasons;
                }

                if (denials.Count > 0)
                {
                    denials.Sort();
                    disqualText += " <br />" + denials[0].DenialText;
                }
            }

            return winningResult;
        }

        private void AddLendingLicenseStips()
        {
            if (m_loanFileData.BrokerDB.DisableLendingLicenseStips)
            {
                // OPM 179515
                return;
            }

            CAgentFields agent = m_loanFileData.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            if (string.IsNullOrEmpty(agent.LicenseNumOfAgent))
            {
                m_stipCollection.Add("WARNING", new CStipulation(MissingLoanOfficerLicenseStip, MissingLoanOfficerLicenseStip));
            }
            if (string.IsNullOrEmpty(agent.LicenseNumOfCompany))
            {
                m_stipCollection.Add("WARNING", new CStipulation(MissingOriginationLicenseStip, MissingOriginationLicenseStip));
            }
        }

        // 04/15/09 mf - OPM 25872 - Now that we can allow mutiple applications,
        // we must use the borrwer names so underwriter can easily identify whose
        // credit we are describing in these stips.
        private void AddCreditStips()
        {
            if (m_loanFileData.BrokerDB.DisableCreditStips)
            {
                // OPM 179515
                return;
            }

            for (int appIndex = 0; appIndex < m_loanFileData.nApps; appIndex++)
            {
                // In legacy mode, we only look at app 0.  We ignore any other apps
                // that may have been added in LO.
                if (m_runOptions.UseQbc == false && appIndex != 0) continue;

                CAppData currentDataApp = m_loanFileData.GetAppData(appIndex);
                ILiaCollection liaColl = currentDataApp.aLiaCollection;
                int countLia = liaColl.CountRegular;
                for (int i = 0; i < countLia; ++i)
                {
                    ILiabilityRegular lia = liaColl.GetRegularRecordAt(i);
                    if (lia.WillBePdOff)
                    {
                        string ownerName = m_runOptions.UseQbc ?
                            m_applications.GetOwnerName(lia.OwnerT, appIndex).ToUpper() + ": "
                            : string.Empty;

                        m_stipCollection.Add("CREDIT",
                            new CStipulation(string.Format("{4}ACCOUNT {0} {1}, FROM CREDITOR {2}, WITH BALANCE {3} HAS BEEN MARKED AS TO BE PAID OR PAID OFF. BALANCE UPDATE MUST BE PROVIDED OR PAYMENT INSTRUCTION MUST BE PROVIDED TO ESCROW."
                                , lia.AccNm
                                // TODO: Do we want to start masking this?
                                , lia.AccNum.Value
                                , lia.ComNm
                                , lia.Bal_rep
                                , ownerName
                                )
                                , ""));
                    }
                    else if (lia.NotUsedInRatio && !lia.DebtT.EqualsOneOf(E_DebtRegularT.Mortgage, E_DebtRegularT.Heloc))
                    {
                        string ownerName = m_runOptions.UseQbc ?
                            m_applications.GetOwnerName(lia.OwnerT, appIndex).ToUpper() + ": "
                            : string.Empty;

                        // 8/10/2007 dd - OPM 10199
                        m_stipCollection.Add("CREDIT",
                            new CStipulation(string.Format("{5}ACCOUNT {0} {1}, FROM CREDITOR {2}, WITH PAYMENT {4} AND BALANCE {3} HAS BEEN EXCLUDED FROM DEBT RATIO. EVIDENCE OR EXPLANATION MUST BE PROVIDED."
                                , lia.AccNm
                                // TODO: Do we want to start masking this?
                                , lia.AccNum.Value
                                , lia.ComNm
                                , lia.Bal_rep
                                , lia.Pmt_rep
                                , ownerName
                                )
                                , ""));
                    }
                }

                CreditReportProxy acreditReport = currentDataApp.CreditReportData.Value as CreditReportProxy;
                if (null != acreditReport)
                {
                    if (!acreditReport.IsOptimisticDefault)
                    {
                        foreach (AbstractCreditLiability liability in acreditReport.LiabilitiesExcludeFromUnderwriting)
                        {
                            string ownerName = m_runOptions.UseQbc ?
                                m_applications.GetOwnerName(liability.LiaOwnerT, appIndex).ToUpper() + ": "
                                : string.Empty;

                            m_stipCollection.Add("WARNING",
                                new CStipulation(string.Format("{2}{1} ACCOUNT {0} HAS BEEN MARKED AS EXCLUDE FROM UNDERWRITING."
                                    , liability.AccountIdentifier
                                    , liability.CreditorName
                                    , ownerName
                                    ), ""));
                        }
                        foreach (AbstractCreditLiability liability in acreditReport.AllLiabilities)
                        {
                            if (liability is CreditLiabilityOptimistic)
                            {

                                string ownerName = m_runOptions.UseQbc ?
                                    m_applications.GetOwnerName(liability.LiaOwnerT, appIndex).ToUpper() + ": "
                                    : string.Empty;

                                m_stipCollection.Add("WARNING",
                                    new CStipulation(string.Format("{2}{1} ACCOUNT {0} DOES NOT EXIST IN CREDIT REPORT."
                                        , liability.AccountIdentifier
                                        , liability.CreditorName
                                        , ownerName
                                        ), ""));

                            }
                            else if (liability.IsModified)
                            {
                                string ownerName = m_runOptions.UseQbc ?
                                    m_applications.GetOwnerName(liability.LiaOwnerT, appIndex).ToUpper() + ": "
                                    : string.Empty;

                                m_stipCollection.Add("WARNING",
                                    new CStipulation(string.Format("{3}{1} ACCOUNT {0} WAS MODIFIED. {2}"
                                        , liability.AccountIdentifier
                                        , liability.CreditorName
                                        , liability.ModifiedString
                                        , ownerName
                                        )
                                        , ""));

                            }
                        }
                    }
                }
                if (currentDataApp.aIsPublicRecordAlter)
                {
                    string ownerName = m_applications.GetOwnerName(E_LiaOwnerT.Joint, appIndex).ToUpper();
                    if (ownerName != string.Empty) ownerName += ": ";

                    m_stipCollection.Add("WARNING", new CStipulation(ownerName + ErrorMessages.StipulationWarning_PublicRecordAlter, ""));
                }
                if (currentDataApp.aIsCreditScoresDifferentWithCreditReport)
                {
                    bool useGenericName = !m_runOptions.UseQbc;

                    ICreditReport creditReport = currentDataApp.CreditReportData.Value;

                    if (creditReport.BorrowerScore.Equifax != currentDataApp.aBEquifaxScore)
                    {
                        AddCreditScoreWarning(
                            useGenericName ? "BORROWER" : m_applications.Applications[appIndex].Borrower.FullName.ToUpper()
                            , "EQUIFAX", currentDataApp.aBEquifaxScore, creditReport.BorrowerScore.Equifax);
                    }
                    if (creditReport.BorrowerScore.Experian != currentDataApp.aBExperianScore)
                    {
                        AddCreditScoreWarning(useGenericName ? "BORROWER" : m_applications.Applications[appIndex].Borrower.FullName.ToUpper()
                            , "EXPERIAN", currentDataApp.aBExperianScore, creditReport.BorrowerScore.Experian);

                    }
                    if (creditReport.BorrowerScore.TransUnion != currentDataApp.aBTransUnionScore)
                    {
                        AddCreditScoreWarning(useGenericName ? "BORROWER" : m_applications.Applications[appIndex].Borrower.FullName.ToUpper()
                            , "TRANSUNION", currentDataApp.aBTransUnionScore, creditReport.BorrowerScore.TransUnion);

                    }
                    // OPM 34103 - Only verify coborrower scores if there is a coborrower.
                    // There may be a coborrower on the credit report, but not on the file.
                    if (m_applications.Applications[appIndex].Coborrower != null)
                    {
                        if (creditReport.CoborrowerScore.Equifax != currentDataApp.aCEquifaxScore)
                        {
                            AddCreditScoreWarning(useGenericName ? "CO-BORROWER" : m_applications.Applications[appIndex].Coborrower.FullName.ToUpper()
                                , "EQUIFAX", currentDataApp.aCEquifaxScore, creditReport.CoborrowerScore.Equifax);
                        }
                        if (creditReport.CoborrowerScore.Experian != currentDataApp.aCExperianScore)
                        {
                            AddCreditScoreWarning(useGenericName ? "CO-BORROWER" : m_applications.Applications[appIndex].Coborrower.FullName.ToUpper()
                                , "EXPERIAN", currentDataApp.aCExperianScore, creditReport.CoborrowerScore.Experian);
                        }
                        if (creditReport.CoborrowerScore.TransUnion != currentDataApp.aCTransUnionScore)
                        {
                            AddCreditScoreWarning(useGenericName ? "CO-BORROWER" : m_applications.Applications[appIndex].Coborrower.FullName.ToUpper()
                                , "TRANSUNION", currentDataApp.aCTransUnionScore, creditReport.CoborrowerScore.TransUnion);
                        }
                    }

                }
            }

        }

        private void AddPersistAdjustments()
        {
            // OPM 365561
            foreach (var persistingAdjustment in m_loanFileData.sBrokerLockAdjustments.Where(adj => adj.IsPersist))
            {
                if (m_loanFileData.sPricingModeT == E_sPricingModeT.InternalInvestorPricing
                    && persistingAdjustment.IsLenderAdjustment)
                {
                    // Lender adjustments do not belong in investor results.
                    continue;
                }

                var adjustment = new CAdjustItem(
                    persistingAdjustment.Margin,
                    persistingAdjustment.Rate,
                    persistingAdjustment.Fee,
                    persistingAdjustment.QualifyingRate,
                    persistingAdjustment.TeaserRate,
                    persistingAdjustment.Description,
                    string.Empty, // Debug
                    persistingAdjustment.IsLenderAdjustment,
                    true // IsPersist
                    );

                if (persistingAdjustment.IsHidden)
                {
                    m_rateDeltaHidden += m_loanFileData.m_convertLos.ToDecimal(persistingAdjustment.Rate);
                    m_feeDeltaHidden += m_loanFileData.m_convertLos.ToDecimal(persistingAdjustment.Fee);
                    m_marginDeltaHidden += m_loanFileData.m_convertLos.ToDecimal(persistingAdjustment.Margin);
                    m_qualRDeltaHidden += m_loanFileData.m_convertLos.ToDecimal(persistingAdjustment.QualifyingRate);
                    m_teaserRDeltaHidden += m_loanFileData.m_convertLos.ToDecimal(persistingAdjustment.TeaserRate);
                    m_adjDescsHidden.Add(adjustment);
                }
                else
                {
                    m_rateDeltaDisclosed += m_loanFileData.m_convertLos.ToDecimal(persistingAdjustment.Rate);
                    m_feeDeltaDisclosed += m_loanFileData.m_convertLos.ToDecimal(persistingAdjustment.Fee);
                    m_marginDeltaDisclosed += m_loanFileData.m_convertLos.ToDecimal(persistingAdjustment.Margin);
                    m_qualRDeltaDisclosed += m_loanFileData.m_convertLos.ToDecimal(persistingAdjustment.QualifyingRate);
                    m_teaserRDeltaDisclosed += m_loanFileData.m_convertLos.ToDecimal(persistingAdjustment.TeaserRate);
                    m_adjDescsDisclosed.Add(adjustment);
                }
            }
        }

        // 04/15/09 mf - OPM 25872
        // Evaluate a single condition for Phase 1 execution mutual exclusion policies.
        private static bool EvalWithQBC(AbstractRtRule rule, XmlDocument xmlDocEval, CPageData loanData, EvalStack stack, LoanApplicationList applications, StringBuilder debugInfo, bool isLogDetailInfo)
        {
            RtCondition condition = rule.Condition;

            // Cycle through the borrowers, if any condition evaluates to true, condition hits
            foreach (LoanApplicationList.BorrowerSpec borrower in applications.GetBorrowers(rule.QBC))
            {
                applications.SetPricingContext(borrower);

                if (condition.Evaluate(xmlDocEval, loanData.sSymbolTableForPriceRule, stack, debugInfo, isLogDetailInfo))
                    return true;
            }

            return false; // No borrower in the borrower set specified by the QBC hit this condition
        }

        [Obsolete("use the version for LockPolicies")]
        private static bool IsRateSheetExpirationFeatureEnable(BrokerDB brokerDB, E_LpeRsExpirationByPassPermissionT lpeRsExpirationByPassPermission,
            string lpeAcceptableRsFileId, long lpeAcceptableRsFileVersionNumber)
        {
            bool isEnabled = true;
            if (!brokerDB.UseRateSheetExpirationFeature)
            {
                isEnabled = false;
            }
            else if (lpeRsExpirationByPassPermission == E_LpeRsExpirationByPassPermissionT.ByPassAll)
            {
                // We want to do this early to avoid perf hit for by-passing people.
                isEnabled = false;
            }
            else if (!brokerDB.HasLpeLockDeskWorkHourEndTime)
            {
                // 12/11/2007 dd - OPM 19374 - If Lender doesn't enter end of lock desk business hours then skip this feature.
                isEnabled = false;
            }

            else if (lpeAcceptableRsFileId == "" || lpeAcceptableRsFileId == null || lpeAcceptableRsFileVersionNumber == -1)
            {
                // Default values for rate options. If we encounter this then we assume old behavior of AlwaysUnblock.
                // 12/19/2007 dd - Base on offline discussion with Thinh and Brian, if the loan program is not participate in rate expiration (i.e Subprime) then we will allow 
                // originator to submit a lock even it pass lock desk normal business hour. We determine if program participation base on LpeAcceptableRsFileId or LpeAcceptableRsFileVersionNumber.
                isEnabled = false;
            }

            return isEnabled;

        }

        private static bool IsRateSheetExpirationFeatureEnable(LockPolicy lockPolicy, E_LpeRsExpirationByPassPermissionT lpeRsExpirationByPassPermission,
            string lpeAcceptableRsFileId, long lpeAcceptableRsFileVersionNumber)
        {
            bool isEnabled = true;
            if (!lockPolicy.IsUsingRateSheetExpirationFeature)
            {
                isEnabled = false;
            }
            else if (lpeRsExpirationByPassPermission == E_LpeRsExpirationByPassPermissionT.ByPassAll)
            {
                // We want to do this early to avoid perf hit for by-passing people.
                isEnabled = false;
            }

            else if (!lockPolicy.LpeIsEnforceLockDeskHourForNormalUser && (lpeAcceptableRsFileId == "" || lpeAcceptableRsFileId == null || lpeAcceptableRsFileVersionNumber == -1))
            {
                // Default values for rate options. If we encounter this then we assume old behavior of AlwaysUnblock.
                // 12/19/2007 dd - Base on offline discussion with Thinh and Brian, if the loan program is not participate in rate expiration (i.e Subprime) then we will allow 
                // originator to submit a lock even it pass lock desk normal business hour. We determine if program participation base on LpeAcceptableRsFileId or LpeAcceptableRsFileVersionNumber.
                isEnabled = false;
            }

            return isEnabled;

        }

        //1/29/2008 dd - OPM 19887 - This method is public and static because Price Group editor will need to know the ratesheet expiration status for each product.
        public static RateOptionExpirationResult DetermineRateOptionExpiration(LockPolicy lockPolicy, E_LpeRsExpirationByPassPermissionT lpeRsExpirationByPassPermission,
                    string lpeAcceptableRsFileId, long lpeAcceptableRsFileVersionNumber, string lLpInvestorNm, string productCode, bool byPassManualDisabled, RateOptionExpirationDataLoader dataLoader)
        {
            RateOptionExpirationResult result = null;
            try
            {
                result = DetermineRateOptionExpirationImpl(lockPolicy, lpeRsExpirationByPassPermission, lpeAcceptableRsFileId, lpeAcceptableRsFileVersionNumber, lLpInvestorNm, productCode, byPassManualDisabled, dataLoader);
                return result;
            }
            finally
            {
                if (result != null)
                {
                    if (result.IsBlockedRateLockSubmission && ConstStage.DoesBrokerHaveExpirationDebugEnabled(lockPolicy.BrokerId))
                    {
                        Tools.LogInfo("ExpirationDebug: " + result.GetLog());
                    }
                }
            }
        }
        private static RateOptionExpirationResult DetermineRateOptionExpirationImpl(LockPolicy lockPolicy, E_LpeRsExpirationByPassPermissionT lpeRsExpirationByPassPermission,
            string lpeAcceptableRsFileId, long lpeAcceptableRsFileVersionNumber, string lLpInvestorNm, string productCode, bool byPassManualDisabled, RateOptionExpirationDataLoader dataLoader)
        {

            RateOptionExpirationResult result = new RateOptionExpirationResult();

            //if (result != null) return result; // TEMP DEBUG

            result.AppendLineToLog("DetermineRateOptionExpiration=lockPolicyId:{0},lpeRsExpirationByPassPermission:{1},lpeAcceptableRsFileId:{2},lpeAcceptableRsFileVersionNumber:{3},lLpInvestorNm:{4},productCode:{5},byPassManualDisabled:{6},brokerId:{7}",
                lockPolicy.LockPolicyId, lpeRsExpirationByPassPermission, lpeAcceptableRsFileId, lpeAcceptableRsFileVersionNumber, lLpInvestorNm, productCode, byPassManualDisabled, lockPolicy.BrokerId);


            using (PerformanceStopwatch.Start("Determine RSE - IsRateSheetExpirationFeatureEnable"))
            {
                if (!IsRateSheetExpirationFeatureEnable(lockPolicy, lpeRsExpirationByPassPermission, lpeAcceptableRsFileId, lpeAcceptableRsFileVersionNumber))
                {
                    result.AppendLineToLog("!IsRateSheetExpirationFeatureEnable");
                    return result;
                }
            }

            DateTime now = DateTime.Now;

            bool isRateSheetExpirationFeatureDeployed = false;
            using (PerformanceStopwatch.Start("Determine RSE - GetRateSheetExpirationFeatureByProductCodeInvestorName"))
            {
                RateSheetExpirationFeatureInfo rseFeatureInfo = dataLoader.GetRateSheetExpirationFeatureInfo(productCode, lLpInvestorNm, shouldLog: true);
                if (rseFeatureInfo != null)
                {
                    isRateSheetExpirationFeatureDeployed = rseFeatureInfo.IsRateSheetExpirationFeatureDeployed;
                    result.AppendLineToLog("isRateSheetExpirationFeatureDeployed (sql) = {0};", isRateSheetExpirationFeatureDeployed);
                }
            }

            if (!isRateSheetExpirationFeatureDeployed)
            {
                // 12/21/2007 dd - OPM 19558 - If the ratesheet feature is not turn on for this investor and product code then bypass ratesheet expiration feature.
                return result;
            }

            if (!byPassManualDisabled)
            {
                using (PerformanceStopwatch.Start("Determine RSE - !byPassManualDisabled"))
                {
                    // OPM 22789: Allow the client to manually expire a product.
                    InvestorProductItem lenderManualExpirationItem = InvestorProductManager.RetrieveManualInvestorProduct(lockPolicy.BrokerId, lockPolicy.LockPolicyId, lLpInvestorNm, productCode, dataLoader);
                    if (null != lenderManualExpirationItem)
                    {
                        result.AppendLineToLog("lenderManualExpirationItem = DisableStatus: {0} IsHiddenFromResult:{1} DisableDateTime:{2}",
                            lenderManualExpirationItem.DisableStatus,
                            lenderManualExpirationItem.IsHiddenFromResult,
                            lenderManualExpirationItem.DisableDateTime);

                        switch (lenderManualExpirationItem.DisableStatus)
                        {
                            case E_InvestorProductDisabledStatus.Disabled:
                                result.IsHiddenFromResult = lenderManualExpirationItem.IsHiddenFromResult;
                                result.IsBlockedRateLockSubmission = true;
                                result.RateLockSubmissionUserWarningMessage = ConstAppDavid.BlockedRateLockSubmission_LenderManual + lenderManualExpirationItem.MessagesToSubmittingUsers;
                                result.RateLockSubmissionDevWarningMessage = "Block manually by lender.";
                                return result;
                            case E_InvestorProductDisabledStatus.DisabledTillNextLpeUpdate:
                                DateTime xlsFileVersionDateTime = DateTime.MinValue;
                                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.RateSheet, "GetInvestorFileVersionDateTimeByAcceptableRsFileId",
                                    new SqlParameter("@LpeAcceptableRsFileId", lpeAcceptableRsFileId)))
                                {
                                    if (reader.Read())
                                    {
                                        xlsFileVersionDateTime = (DateTime)reader["FileVersionDateTime"];

                                    }
                                    result.AppendLineToLog("GetInvestorFileVersionDateTimeByAcceptableRsFileId {0}", xlsFileVersionDateTime);
                                }
                                if (xlsFileVersionDateTime < lenderManualExpirationItem.DisableDateTime)
                                {
                                    result.IsHiddenFromResult = lenderManualExpirationItem.IsHiddenFromResult;
                                    result.IsBlockedRateLockSubmission = true;
                                    result.RateLockSubmissionUserWarningMessage = ConstAppDavid.BlockedRateLockSubmission_LenderManual + lenderManualExpirationItem.MessagesToSubmittingUsers;
                                    result.RateLockSubmissionDevWarningMessage = "Block manually by lender.";
                                    return result;
                                }
                                else
                                {
                                    InvestorProductManager.Remove(lockPolicy.BrokerId, lockPolicy.LockPolicyId, lLpInvestorNm, productCode); // Remove the disabled rule.
                                    result.AppendLineToLog("InvestorProductManager.Remove({0},{1},{2},{3})", lockPolicy.BrokerId, lockPolicy.LockPolicyId, lLpInvestorNm, productCode);
                                }
                                break;
                            default:
                                throw new UnhandledEnumException(lenderManualExpirationItem.DisableStatus);
                        }

                    }
                    else
                    {
                        result.AppendLineToLog("lenderManualExpirationItem is null");
                    }
                }
            }
            HashSet<string> investorExceptions = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
            {
                "nflp",
                "fidelity bank"
            };
            using (PerformanceStopwatch.Start("Determine RSE - InvestorExpiration"))
            {
                result.AppendLineToLog("Determine RSE - InvestorExpiration");
                InvestorExpiration investorExpiration = null;
                using (PerformanceStopwatch.Start("Determine RSE - InvestorExpiration - 0"))
                {
                    investorExpiration = InvestorExpirationFactory.GetInvestorExpirationObj(lLpInvestorNm, productCode, lockPolicy, lpeAcceptableRsFileId, dataLoader.InvestorExpirationDataLoader);
                    result.AppendLineToLog("InvestorExpirationFactory.GetInvestorExpirationObj({0},{1},{2},{3})", lLpInvestorNm, productCode, lockPolicy.LockPolicyId, lpeAcceptableRsFileId);
                }
                using (PerformanceStopwatch.Start("Determine RSE - InvestorExpiration - 1"))
                {

                    if (lpeRsExpirationByPassPermission != E_LpeRsExpirationByPassPermissionT.ByPassAll)
                    {
                        // Lock desk
                        if (lpeRsExpirationByPassPermission == E_LpeRsExpirationByPassPermissionT.NoByPass)
                        {
                            //173059 av nflp products dont follow lockpolicy rules
                            if (lockPolicy.LpeIsEnforceLockDeskHourForNormalUser &&
                                lockPolicy.IsOneOfLockDeskHolidayClosures(now) && !investorExceptions.Contains(lLpInvestorNm))
                            {
                                result.AppendLineToLog("LpeIsEnforceLockDeskHourForNormalUser : true, IsOneOfLockDeskHolidayClosures (now), true)");
                                // 12/14/2007 dd - Lock Desk is close on holiday.
                                // 12/14/2007 dd - We want to block normal user from locking rate on weekend.
                                result.IsBlockedRateLockSubmission = true;
                                result.IsExpiredDueToOutsideLockDeskHour = true;
                                result.RateLockSubmissionUserWarningMessage = ConstAppDavid.BlockedRateLockSubmission_OutsideClosureDayHour;
                                result.RateLockSubmissionDevWarningMessage = "Blocked because today is included in LockDesk Closure Days.";
                                return result;
                            }

                            // This case to avoid users submit loans too early before lockdesk opens. Note, we could just check for the starttime here instead of the 
                            // full range because the end of lockdesk hour is handled by the rest of this method (below).
                            // 1/22/2008 dd - OPM 19831 - We no longer factor the LpeMinutesNeededToLockLoan buffer to the Lock Desk close time.
                            //173059 av nflp products dont follow lockpolicy rules
                            if (lockPolicy.LpeIsEnforceLockDeskHourForNormalUser &&
                                !IsWithinWorkingHour(now, lockPolicy) && !investorExceptions.Contains(lLpInvestorNm))
                            {
                                result.AppendLineToLog("LpeIsEnforceLockDeskHourForNormalUser : true, !IsWithinWorkingHour(now): true,  LpeLockDeskWorkHourStartTimeFromPacificStandpoint : {0}, LpeLockDeskWorkHourEndTimeFromPacificStandpoint: {1}, Timezone: {2} )",
                                    lockPolicy.GetLpeLockDeskWorkHourInPstStart(DateTime.Today.DayOfWeek), lockPolicy.GetLpeLockDeskWorkHourInPstEnd(DateTime.Today.DayOfWeek), lockPolicy.TimezoneForRsExpiration);
                                result.AppendLineToLog("InvestorCutoffType {0}", investorExpiration.InvestorCutoffType);
                                result.IsBlockedRateLockSubmission = true;
                                result.IsExpiredDueToOutsideLockDeskHour = true;
                                if (investorExpiration.InvestorCutoffType == E_InvestorCutoffType.IsOutsideInvestorCutoff)
                                {
                                    // 2/13/2008 dd - OPM 19858 - If current time is also pass the investor cut off time then display the message to user.
                                    result.IsExpiredDueToOutsideInvestorCutoffHour = true;
                                    result.RateLockSubmissionUserWarningMessage = ConstAppDavid.BlockedRateLockSubmission_OutsideNormalHourAndPassInvestorCutoff + FormatWithTimezone(lockPolicy.GetLpeLockDeskWorkHoursInLenderTimezoneStart(DateTime.Now.DayOfWeek), lockPolicy) + " - " + FormatWithTimezone(lockPolicy.GetLpeLockDeskWorkHoursInLenderTimezoneEnd(DateTime.Now.DayOfWeek), lockPolicy) + "#" + Tools.GetTimeDescription(investorExpiration.InvestorCutoffTime);
                                    result.RateLockSubmissionDevWarningMessage = "Normal User cannot submit outside of Lock Desk Hour and pass investor cut off time. Lock Desk Hour Start =" + lockPolicy.GetLpeLockDeskWorkHourInPstStart(DateTime.Now.DayOfWeek) + ", Lock Desk End Hour=" + lockPolicy.GetLpeLockDeskWorkHourInPstEnd(DateTime.Now.DayOfWeek) + ", Buffer Time=" + lockPolicy.LpeMinutesNeededToLockLoan + ", Investor Cutoff=" + investorExpiration.InvestorCutoffTime;
                                }
                                else
                                {
                                    result.RateLockSubmissionUserWarningMessage = ConstAppDavid.BlockedRateLockSubmission_OutsideNormalHour + FormatWithTimezone(lockPolicy.GetLpeLockDeskWorkHoursInLenderTimezoneStart(DateTime.Now.DayOfWeek), lockPolicy) + " - " + FormatWithTimezone(lockPolicy.GetLpeLockDeskWorkHoursInLenderTimezoneEnd(DateTime.Now.DayOfWeek), lockPolicy);
                                    result.RateLockSubmissionDevWarningMessage = "Normal User cannot submit outside of Lock Desk Hour. Lock Desk Hour Start =" + lockPolicy.GetLpeLockDeskWorkHourInPstStart(DateTime.Now.DayOfWeek) + ", Lock Desk End Hour=" + lockPolicy.GetLpeLockDeskWorkHourInPstEnd(DateTime.Now.DayOfWeek) + ", Buffer Time=" + lockPolicy.LpeMinutesNeededToLockLoan;
                                }

                                return result; // 12/14/2007 dd - No need to check further.    
                            }

                        }
                    } // if( currentPrincipal.LpeRsExpirationByPassPermission != E_LpeRsExpirationByPassPermissionT.ByPassAll )
                }
                string deploymentType = "";
                bool isBothRsMapAndBotWorking = false;

                using (PerformanceStopwatch.Start("Determine RSE - InvestorExpiration - 2"))
                {
                    result.AppendLineToLog("Determine RSE - InvestorExpiration - 2");
                    var rsFileInfo = dataLoader.GetAcceptableRsFileInfo(lpeAcceptableRsFileId, shouldLog: true);
                    if (rsFileInfo != null)
                    {
                        deploymentType = rsFileInfo.DeploymentType;
                        isBothRsMapAndBotWorking = rsFileInfo.IsBothRsMapAndBotWorking;
                        result.AppendLineToLog("deploymentType: {0} isBothRsMapAndBotWorking: {1}", deploymentType, isBothRsMapAndBotWorking);
                    }
                }
                using (PerformanceStopwatch.Start("Determine RSE - InvestorExpiration - 3"))
                {
                    result.AppendLineToLog("Determine RSE - InvestorExpiration - 3 - deploymentType=[" + deploymentType + "]");

                    switch (deploymentType)
                    {
                        case "":
                        case "AlwaysUnblocked":
                            return result;
                        case "AlwaysBlocked":
                        case "UseVersionAfterGeneratingAnOutputFileOk":
                            result.IsBlockedRateLockSubmission = true;
                            result.IsBlockedManuallyByLender = true;
                            result.RateLockSubmissionUserWarningMessage = ConstAppDavid.BlockedRateLockSubmission_ExpiredMessage;
                            result.RateLockSubmissionDevWarningMessage = "Block because of deployment type. DeploymentType=" + deploymentType;
                            break;
                        case "UseVersion":
                            if (!isBothRsMapAndBotWorking)
                            {
                                result.IsBlockedRateLockSubmission = true;
                                result.IsBlockedRateLockSubmission = true;
                                result.RateLockSubmissionUserWarningMessage = ConstAppDavid.BlockedRateLockSubmission_ExpiredMessage;
                                result.RateLockSubmissionDevWarningMessage = "Block because of IsBothRsMapAndBotWorking=false";
                                break;
                            }

                            if (!investorExpiration.HasInvestorCutoffTime)
                            {
                                result.AppendLineToLog("!investorExpiration.HasInvestorCutoffTime");
                                // 12/13/2007 dd - Skip the checking if investor does not have cut off hour.
                                result.IsBlockedRateLockSubmission = false;
                                result.RateLockSubmissionDevWarningMessage = "";
                                result.RateLockSubmissionUserWarningMessage = "";
                                return result;
                            }

                            //the investor expiration object looks at lockpolicy to determine what timezone to use so we have to use the lender timezone below.

                            DateTime lenderLockPolicyEndDForToday = lockPolicy.GetLpeLockDeskWorkHoursInLenderTimezoneEnd(DateTime.Now.DayOfWeek);

                            DateTime rawLockDeskWorkHour = new DateTime(now.Year, now.Month, now.Day, lenderLockPolicyEndDForToday.Hour, lenderLockPolicyEndDForToday.Minute, lenderLockPolicyEndDForToday.Second);
                            // 1/22/2008 dd - OPM 19831 - We no longer factor the LpeMinutesNeededToLockLoan buffer to the Lock Desk close time.
                            DateTime lockDeskCutOffTime = rawLockDeskWorkHour;
                            //DateTime lockDeskCutOffTime = rawLockDeskWorkHour.AddMinutes(-1 * brokerDB.LpeMinutesNeededToLockLoan);

                            result.AppendLineToLog("rawLockDeskWorkHour: {0}, lockDeskCutOffTime {1}", rawLockDeskWorkHour, lockDeskCutOffTime);
                            if (!lockPolicy.LpeIsEnforceLockDeskHourForNormalUser)
                            {
                                lockDeskCutOffTime = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59); // Set to max time for the day.
                                result.AppendLineToLog("lockDeskCutOffTime = {0}; ", lockDeskCutOffTime);
                            }

                            // 7/8/2008 dd - Use Diana's object
                            DateTime investorCutOffTime = investorExpiration.InvestorCutoffTime;

                            // Taking the earliest cutoff hour between lockdesk and investor
                            // minCutOffTime already factored in the buffer.
                            DateTime minCutOffTime = investorCutOffTime.CompareTo(lockDeskCutOffTime) < 0 ? investorCutOffTime : lockDeskCutOffTime;

                            result.AppendLineToLog("investorCutOffTime: {0}, minCutOffTime {1}", investorCutOffTime, minCutOffTime);

                            if (now.CompareTo(minCutOffTime) >= 0 && lpeRsExpirationByPassPermission == E_LpeRsExpirationByPassPermissionT.NoByPass
                                && !investorExceptions.Contains(lLpInvestorNm))
                            {
                                result.AppendLineToLog("IF now.CompareTo(minCutOffTime) >= 0 && lpeRsExpirationByPassPermission == E_LpeRsExpirationByPassPermissionT.NoByPass");
                                result.IsBlockedRateLockSubmission = true;

                                result.RateLockSubmissionUserWarningMessage = ConstAppDavid.BlockedRateLockSubmission_CutOffMessage + Tools.GetTimeDescription(minCutOffTime);
                                result.RateLockSubmissionDevWarningMessage = "Normal User passed cutoff time. Now=" + now + ", effective cut-off hour=" + minCutOffTime + ", LockDeskWorkHourEnds=" + lockDeskCutOffTime + ", InvestorCutOffHour=" + investorCutOffTime + ", LpeMinutesNeededToLockLoan=" + lockPolicy.LpeMinutesNeededToLockLoan;

                            }
                            else if (now.CompareTo(investorCutOffTime) >= 0
                                && !investorExceptions.Contains(lLpInvestorNm))
                            {
                                result.AppendLineToLog("ELSE IF now.CompareTo(investorCutOffTime) >= 0");

                                result.IsBlockedRateLockSubmission = true;
                                result.IsExpiredDueToOutsideInvestorCutoffHour = true;
                                result.RateLockSubmissionUserWarningMessage = ConstAppDavid.BlockedRateLockSubmission_CutOffMessage + Tools.GetTimeDescription(investorCutOffTime);
                                result.RateLockSubmissionDevWarningMessage = "Pass investor cutoff time. Now=" + now + ", investorCutOffTime=" + investorCutOffTime + ", LpeMinutesNeededToLockLoan=" + lockPolicy.LpeMinutesNeededToLockLoan;
                            }
                            else
                            {
                                result.AppendLineToLog("ELSE");
                                long latestVersionNumber = investorExpiration.LatestVersionNumber;
                                DateTime latestEffectiveDateTime = investorExpiration.LatestEffectiveDateTime;

                                if (investorExpiration.InvestorCutoffType == E_InvestorCutoffType.RatesheetIsNotCurrent) // Investor has sent a reprice notification
                                {
                                    result.AppendLineToLog("IF investorExpiration.InvestorCutoffType == E_InvestorCutoffType.RatesheetIsNotCurrent");
                                    result.IsBlockedRateLockSubmission = true;
                                    result.IsExpiredDueToRatesheetNotCurrent = true;
                                    result.RateLockSubmissionUserWarningMessage = ConstAppDavid.BlockedRateLockSubmission_ExpiredMessage;
                                    result.RateLockSubmissionDevWarningMessage = "Ratesheet latest effective date is not current. Latest Effective Date=" + latestEffectiveDateTime;
                                }
                                // OPM 29001 db - We only want to check version numbers if we are expiring pricing upon a new ratesheet download.
                                // Some investors (just fannie mae, as of 4/20/09) have live pricing, and we don't want to expire pricing every
                                // time we download a new ratesheet because then pricing would be expired too often.  Our clients will be responsible
                                // for handling the data fluctuation since their products won't be expired unless there is an actual map/schema break,
                                // download error, it's outside of the lender or investor cutoff time, or any other expiration feature we add.
                                else if (latestVersionNumber != lpeAcceptableRsFileVersionNumber && investorExpiration.ExpireUponNewRatesheetDownload)
                                {
                                    result.AppendLineToLog("elsee if latestVersionNumber != lpeAcceptableRsFileVersionNumber && investorExpiration.ExpireUponNewRatesheetDownload latestVersionNumber:{0}, lpeAcceptableRsFileVersionNumber:{1}, ExpireUponNewRatesheetDownload:{2}",
                                        latestVersionNumber, lpeAcceptableRsFileVersionNumber, investorExpiration.ExpireUponNewRatesheetDownload);

                                    result.IsBlockedRateLockSubmission = true;
                                    result.IsExpiredDueToRatesheetNotCurrent = true;
                                    result.RateLockSubmissionUserWarningMessage = ConstAppDavid.BlockedRateLockSubmission_ExpiredMessage;
                                    result.RateLockSubmissionDevWarningMessage = "Ratesheet version does not match. Version=" + lpeAcceptableRsFileVersionNumber + ", latest version=" + latestVersionNumber;
                                }
                                else if (latestEffectiveDateTime.Day != now.Day || latestEffectiveDateTime.Month != now.Month || latestEffectiveDateTime.Year != now.Year)
                                {
                                    if (investorExceptions.Contains(lLpInvestorNm))
                                    {

                                    }
                                    else
                                    {
                                        result.AppendLineToLog("latestEffectiveDateTime.Day != now.Day || latestEffectiveDateTime.Month != now.Month || latestEffectiveDateTime.Year != now.Year");
                                        result.IsBlockedRateLockSubmission = true;
                                        result.IsExpiredDueToRatesheetNotCurrent = true;
                                        result.RateLockSubmissionUserWarningMessage = ConstAppDavid.BlockedRateLockSubmission_ExpiredMessage;
                                        result.RateLockSubmissionDevWarningMessage = "Ratesheet latest effective date is not current. Latest Effective Date=" + latestEffectiveDateTime;
                                    }
                                }

                            }
                            break;

                        default:
                            string errMsg = string.Format("Unhandled DeploymentType = {0} for LpeAcceptableRsFileId = {1}.  Investor name = {2}, ProductCode = {3}.  Defaulting to AlwaysUnblocked.", deploymentType, lpeAcceptableRsFileId, lLpInvestorNm, productCode);
                            Tools.LogBug(errMsg);
                            result.IsBlockedRateLockSubmission = false;
                            break;
                    }
                }

                if (lpeRsExpirationByPassPermission == E_LpeRsExpirationByPassPermissionT.ByPassAllExceptInvestorCutOff)
                {
                    if (result.IsBlockedRateLockSubmission && now.CompareTo(investorExpiration.InvestorCutoffTime) < 0)
                    {
                        // 12/12/2007 dd - We allow this type of user to bypass as long as current time is less than investor cutoff time.
                        result.IsBlockedRateLockSubmission = false;
                        result.AppendLineToLog("lpeRsExpirationByPassPermission == E_LpeRsExpirationByPassPermissionT.ByPassAllExceptInvestorCutOff");
                    }

                }
            }

            return result;
        }

        [Obsolete]
        //OPM 25799 jk - Formats the date time into a string with the lender's timezone attached
        private static string FormatWithTimezone(DateTime dt, BrokerDB broker)
        {
            string timezone = broker.TimezoneForRsExpiration;

            if (timezone.ToUpper() == "MST" && broker.Address.State.ToUpper() == "AZ")
                return dt.ToString("hh:mm:tt") + " MST";
            if (timezone.ToUpper() == "PST" || timezone.ToUpper() == "PDT")
                return dt.ToString("hh:mm tt") + (TimeZone.CurrentTimeZone.IsDaylightSavingTime(dt) ? " PDT" : " PST");
            if (timezone.ToUpper() == "EST" || timezone.ToUpper() == "EDT")
                return dt.ToString("hh:mm tt") + (TimeZone.CurrentTimeZone.IsDaylightSavingTime(dt) ? " EDT" : " EST");
            if (timezone.ToUpper() == "CST" || timezone.ToUpper() == "CDT")
                return dt.ToString("hh:mm tt") + (TimeZone.CurrentTimeZone.IsDaylightSavingTime(dt) ? " CDT" : " CST");
            if (timezone.ToUpper() == "MST" || timezone.ToUpper() == "MDT")
                return dt.ToString("hh:mm tt") + (TimeZone.CurrentTimeZone.IsDaylightSavingTime(dt) ? " MDT" : " MST");

            //If not one of the four given timezones, ignore any type of daylight savings naming, and add the given timezone
            return dt.ToString("hh:mm tt") + " " + timezone;
        }

        private static string FormatWithTimezone(DateTime dt, LockPolicy lockPolicy)
        {
            string timezone = lockPolicy.TimezoneForRsExpiration;

            if (timezone.ToUpper() == "MST" && BrokerDB.RetrieveById(lockPolicy.BrokerId).Address.State.ToUpper() == "AZ")
                return dt.ToString("hh:mm:tt") + " MST";
            if (timezone.ToUpper() == "PST" || timezone.ToUpper() == "PDT")
                return dt.ToString("hh:mm tt") + (TimeZone.CurrentTimeZone.IsDaylightSavingTime(dt) ? " PDT" : " PST");
            if (timezone.ToUpper() == "EST" || timezone.ToUpper() == "EDT")
                return dt.ToString("hh:mm tt") + (TimeZone.CurrentTimeZone.IsDaylightSavingTime(dt) ? " EDT" : " EST");
            if (timezone.ToUpper() == "CST" || timezone.ToUpper() == "CDT")
                return dt.ToString("hh:mm tt") + (TimeZone.CurrentTimeZone.IsDaylightSavingTime(dt) ? " CDT" : " CST");
            if (timezone.ToUpper() == "MST" || timezone.ToUpper() == "MDT")
                return dt.ToString("hh:mm tt") + (TimeZone.CurrentTimeZone.IsDaylightSavingTime(dt) ? " MDT" : " MST");

            //If not one of the four given timezones, ignore any type of daylight savings naming, and add the given timezone
            return dt.ToString("hh:mm tt") + " " + timezone;
        }

        private static bool IsWithinWorkingHour(DateTime now, LockPolicy policy)
        {
            if (policy.IsLockDeskOpened(now.DayOfWeek))
            {
                return now.TimeOfDay >= policy.GetLpeLockDeskWorkHourInPstStart(now.DayOfWeek) && now.TimeOfDay < policy.GetLpeLockDeskWorkHourInPstEnd(now.DayOfWeek);
            }
            return false;
        }

        private void AddCreditScoreWarning(string borrower, string bureau, int scoreInFile, int scoreInCredit)
        {
            string str = string.Format("{0} {1} SCORE IS DIFFERENT FROM CREDIT REPORT. ", borrower, bureau);
            if (scoreInFile == 0)
            {
                str += "NO SCORE ENTERED, ";
            }
            else
            {
                str += string.Format("MODIFIED SCORE IS {0}, ", scoreInFile);
            }
            if (scoreInCredit == 0)
            {
                str += "NO SCORE IN CREDIT REPORT.";
            }
            else
            {
                str += string.Format("SCORE IN CREDIT REPORT IS {0}.", scoreInCredit);
            }
            m_stipCollection.Add("WARNING", new CStipulation(str, ""));
        }

        /// <summary>
        /// Snapping it up toward positive infinity to the next 1/8 (0.125)
        /// Example: -2.128, change it to -2.125, 1.123, change it to 1.125. Opm#1726.
        /// </summary>
        /// <param name="orig"></param>
        /// <returns></returns>
        private static decimal SnapupOneEighth(decimal orig)
        {
            //return ( 125.0M * Decimal.Floor( ( ( orig + 0.124M ) * 1000M ) / 125.0M ) ) / 1000M;
            return Snapup(orig, 0.125M);
        }



        private static decimal CalculatePriceWithoutComp(decimal unsnappedPrice, decimal compPoints, decimal snapInterval)
        {
            // Per OPM 67572: When calculating the price without LO Comp, apply the rounding delta to it.

            // EXAMPLE: Price without LO Comp = -3.500, LO Comp points = 2.940, Fee Rounding = 0.125
            // In this situation, the price with LO Comp would start off as -0.560, which would then be rounded up to -0.500 
            // (rounding delta = 0.060). As a result, the price without LO Comp should be quoted as -3.500 + 0.060 = -3.440.

            // The goal is that the final price is rounded, and that the difference between the final price and the price
            // w/o LO Comp is exactly equal to the LO Comp points.

            decimal priceWithComp = unsnappedPrice + Math.Round(compPoints, RateDecimalDigits);
            decimal finalPrice = Snapup(priceWithComp, snapInterval);
            decimal roundingDelta = finalPrice - priceWithComp;

            return unsnappedPrice + roundingDelta;
        }

        private ILoanProgramTemplate m_loanProgramTemplate;

        protected AbstractLoanProgramSet LoanProgramSet { get; private set; }

        private bool m_hasOtherDisqualification = false;
        public bool DisqualifiedBecauseMinMaxFeeViolation
        {
            // 7/13/2007 dd - OPM 1202 - We only true for this bit if bMinMaxFeeViolation is the only disqualify reason.
            get { return m_bMinMaxFeeViolation && !m_hasOtherDisqualification; }
        }

        public bool IsCurrentPricingSnapshot
        {
            get
            {
                return LoanProgramSet.IsCurrentSnapshot;
            }
        }
        public DateTime DataVersionD
        {
            get { return LoanProgramSet.VersionD; }
        }

        public SHA256Checksum FileKey
        {
            get { return LoanProgramSet.FileKey; }
        }

        public int FeeServiceRevisionId { get; private set; }

        public IEnumerable<CStipulation> DenialReasons
        {
            get { return m_reasonArray; }
        }

        public CSortedListOfGroups Stips
        {
            get { return m_stipCollection; }
        }

        public CSortedListOfGroups HiddenStips
        {
            get { return m_hiddenStipCollection; }
        }

        public CApplicantRateOption[] ApplicantRateOptions
        {
            get
            {
                return m_applicantRateOptions;
            }
        }

        public CApplicantRateOption[] RepresentativeRateOption
        {
            get
            {
                if (null == m_repRateOption)
                    return new CApplicantRateOption[] { };

                return new CApplicantRateOption[] { m_repRateOption };
            }
        }

        /// <summary>
        /// True if the loan file is qualified for this product.
        /// </summary>
        public E_EvalStatus Status
        {
            get { return m_evalStatus; }
        }

        //opm 83663
        public bool IsDuRefiFilterEnabled
        {
            get;
            private set;
        }

        public string DisqualifiedRules
        {
            get { return HttpUtility.HtmlEncode(m_disqualifiedRules); }
        }

        public List<CAdjustItem> AdjustDescs
        {
            get { return m_adjDescsDisclosed; }
        }

        public List<CAdjustItem> HiddenAdjustDescs
        {
            get { return this.m_adjDescsHidden; }
        }

        public CAdjustItem TotalAdjustDesc
        {
            get
            {
                if (m_loanFileData.BrokerDB.IsUsePerRatePricing && m_runOptions.SelectedRate != 0)
                {
                    return TotalAdjustDescPerRate;
                }

                return new CAdjustItem(MarginDelta_rep, RateDelta_rep, FeeDelta_rep, QualRDelta_rep, TeaserRDelta_rep, "Total of Adjustments", "");
            }
        }
        private CAdjustItem TotalAdjustDescPerRate
        {
            // OPM 91371.  If submitting for chosen rate and using per-rate,
            // the point total should only be for the selected rate.
            // Calculate it here by summing the applicable per-rate adjustments.

            get
            {
                decimal perRateRateAdj = 0;
                decimal perRateFeeAdj = 0;

                foreach (var option in ApplicantRateOptions)
                {
                    if (m_runOptions.SelectedRate == option.Rate)
                    {
                        string[] optionCodes = option.PerOptionAdjStr.Split(',');
                        foreach (CAdjustItem item in m_adjDescsDisclosed)
                        {
                            if (item.IsPerRateAdjForAllRateOptions || optionCodes.Contains(item.OptionCode))
                            {
                                perRateFeeAdj += m_losConvert.ToRate(item.Fee);
                                perRateRateAdj += m_losConvert.ToRate(item.Rate);
                            }
                        }

                        break;
                    }
                }

                return new CAdjustItem(
                MarginDelta_rep
                , m_losConvert.ToRateString(RateDelta + perRateRateAdj)
                , m_losConvert.ToRateString(FeeDelta + perRateFeeAdj)
                , QualRDelta_rep
                , TeaserRDelta_rep
                , "Total of Adjustments", "");
            }
        }
        public string Errors
        {
            get
            {
                StringBuilder errMsg = new StringBuilder(100);
                foreach (DictionaryEntry entry in m_errors)
                {
                    errMsg.Append(" * Evaluation error: " + CApplicantPrice.GetUserMsg(entry.Key.ToString()) + Environment.NewLine);
                }
                return errMsg.ToString();
            }
        }

        public List<DenialException> DenialExceptions
        {
            get
            {
                return m_runOptions.DenialExceptions;
            }
        }

        public Guid PriceGroupId
        {
            get { return m_runOptions.PriceGroupId; }
        }

        public SHA256Checksum PriceGroupContentKey
        {
            get
            {
                var v = SHA256Checksum.Create(m_runOptions.PriceGroupContentKey);

                if (v == null)
                {
                    return SHA256Checksum.Invalid;
                }
                else
                {
                    return v.Value;
                }
            }
        }
        public string MortgageInsuranceProvider
        {
            get
            {
                return m_mortgageInsuranceProvider;
            }
        }

        public string MortgageInsuranceDesc
        {
            get
            {
                return m_mortgageInsuranceDesc;
            }
        }

        public string MortgageInsuranceSinglePmt
        {
            get
            {
                return m_mortgageInsuranceSinglePmt;
            }
        }

        public string MortgageInsuranceMonthlyPmt
        {
            get
            {
                return m_mortgageInsuranceMonthlyPmt;
            }
        }

        public string MortgageInsuranceSingleRate
        {
            get
            {
                return m_mortgageInsuranceSingleRate;
            }
        }

        public string MortgageInsuranceMonthlyRate
        {
            get
            {
                return m_mortgageInsuranceMonthlyRate;
            }
        }


        public ArrayList DeveloperErrors
        {
            get
            {
                ArrayList list = new ArrayList();
                foreach (DictionaryEntry entry in m_developerErrors)
                {
                    list.Add(entry.Key.ToString());
                }
                return list;
            }
        }

        public decimal MarginDelta
        {
            get { return m_marginDeltaDisclosed; }
        }
        public string MarginDelta_rep
        {
            get
            {
                return m_losConvert.ToRateString(MarginDelta);
            }
        }

        public decimal FeeDelta
        {
            get { return m_feeDeltaDisclosed; }
        }
        public string FeeDelta_rep
        {
            get
            {
                return m_losConvert.ToRateString(FeeDelta);
            }
        }

        public decimal TotalFeeDelta
        {
            get { return m_feeDeltaDisclosed + m_feeDeltaHidden; }
        }
        public string TotalFeeDelta_rep
        {
            get
            {
                return m_losConvert.ToRateString(TotalFeeDelta);
            }
        }
        public decimal RateDelta
        {
            get { return m_rateDeltaDisclosed; }
        }

        public string RateDelta_rep
        {
            get
            {
                return m_losConvert.ToRateString(RateDelta);
            }
        }
        public decimal QualRDelta
        {
            get { return m_qualRDeltaDisclosed; }
        }
        public string QualRDelta_rep
        {
            get
            {
                return m_losConvert.ToRateString(QualRDelta);
            }
        }

        public decimal TeaserRDelta
        {
            get { return m_teaserRDeltaDisclosed; }

        }

        public string TeaserRDelta_rep
        {
            get
            {
                return m_losConvert.ToRateString(TeaserRDelta);
            }
        }



        public decimal MaxDti
        {
            get { return m_maxDti; }
        }
        public string MaxDti_rep
        {
            get
            {
                if (m_maxDti == 0)
                    return "";
                else
                {
                    return m_losConvert.ToRateString(m_maxDti);
                }
            }
        }

        public string QScore_rep
        {
            get
            {
                if (m_qScore == 0)
                {
                    return "";
                }
                else
                {
                    return m_losConvert.ToCountString(m_qScore);
                }
            }
        }

        public decimal RateRounding
        {
            get
            {
                return m_rateRounding;
            }
        }
        public decimal FeeRounding
        {
            get
            {
                return m_feeRounding;
            }
        }

        public decimal FrontEndSnapDown
        {
            get
            {
                return m_frontEndSnapDown;
            }
        }
        public decimal BackEndSnapDown
        {
            get
            {
                return m_backEndSnapDown;
            }
        }

        public bool IsBlockedRateLockSubmission
        {
            // 12/7/2007 dd - OPM 18661 - Ratesheet expiration feature.
            get { return m_isBlockedRateLockSubmission; }
        }
        public bool AreRatesExpired
        {
            // 11/16/10 mf. OPM 20882 - So we can tell if rates are expired even if a lock bypass occured.
            get { return m_areRatesExpired; }
        }

        public string RateLockSubmissionUserWarningMessage
        {
            get { return m_rateLockSubmissionUserWarningMessage; }
        }
        public string RateLockSubmissionDevWarningMessage
        {
            get { return m_rateLockSubmissionDevWarningMessage; }
        }

        public decimal PhaseZeroLockDaysAdj
        {
            get { return m_PhaseZeroLockDaysAdj; }

        }

        internal bool VerifyBasePhaseZeroValues(ref PhaseZeroBaseScores scores, string ruleInfo)
        {
            if (-1000 == scores.qcltv)
            {
                string errStr = "CLTV is not valid for this loan file, please check the appraisal value and the sale price of the property.";
                if (!m_errors.Contains(errStr))
                {
                    Tools.LogRegTest(string.Format("Error encountered in phase 0 (Q values), CLTV for the loan file is undefined, executing rule with {0}. Error msg={1}",
                                                     ruleInfo, errStr));
                    m_errors.Add(errStr, null);
                    m_developerErrors.Add(string.Format("{0}. {1}", errStr, ruleInfo), null);
                }
                m_evalStatus = E_EvalStatus.Eval_InsufficientInfo;
                return false;
            }

            if (-1000 == scores.qhcltv)
            {
                string errStr = "HCLTV is not valid for this loan file, please check the appraisal value and the sale price of the property.";
                if (!m_errors.Contains(errStr))
                {
                    Tools.LogRegTest(string.Format("Error encountered in phase 0 (Q values), HCLTV for the loan file is undefined, executing rule with {0}. Error msg={1}",
                                                     ruleInfo, errStr));
                    m_errors.Add(errStr, null);
                    m_developerErrors.Add(string.Format("{0}. {1}", errStr, ruleInfo), null);
                }
                m_evalStatus = E_EvalStatus.Eval_InsufficientInfo;
                return false;
            }

            if (-1000 == scores.qltv)
            {
                string errStr = "LTV is not valid for this loan file, please check the appraisal value and the sale price of the property.";
                if (!m_errors.Contains(errStr))
                {
                    Tools.LogRegTest(string.Format("Error encountered in phase 0 (Q values), LTV for the loan file is undefined, executing rule with {0}. Error msg={1}",
                                                    ruleInfo, errStr));
                    m_errors.Add(errStr, null);
                    m_developerErrors.Add(string.Format("{0} {1}", errStr, ruleInfo), null);
                }
                m_evalStatus = E_EvalStatus.Eval_InsufficientInfo;
                return false;
            }

            if (-1000 == scores.qlamt)
            {
                string errStr = "Loan amount is not valid for this loan file.";
                if (!m_errors.Contains(errStr))
                {
                    Tools.LogRegTest(string.Format("Error encountered in phase 0 (Q values), Loan amount for the loan file is undefined, executing rule with {0}. Error msg={1}",
                                                    ruleInfo, errStr));
                    m_errors.Add(errStr, null);
                    m_developerErrors.Add(string.Format("{0}. {1}", errStr, ruleInfo), null);
                }
                m_evalStatus = E_EvalStatus.Eval_InsufficientInfo;
                return false;
            }

            if (-1000 == scores.qscore)
            {
                string errStr = "Score type 1 is not valid for this loan file, please check the raw scores.";
                if (!m_errors.Contains(errStr))
                {
                    Tools.LogRegTest(string.Format("Error encountered in phase 0 (Q values), score type 1 for the loan file is undefined, executing rule with {0}. Error msg={1}",
                                                    ruleInfo, errStr));
                    m_errors.Add(errStr, null);
                    m_developerErrors.Add(string.Format("{0}. {1}", errStr, ruleInfo), null);
                }
                m_evalStatus = E_EvalStatus.Eval_InsufficientInfo;
                return false;
            }
            return true;
        }

        static void AddAndClear(ref decimal accumulate, ref decimal delta)
        {
            accumulate += delta;
            delta = 0;
        }


        void ReceiveAndClearAccumulateData(ref decimal maxYsp, PriceAdjustRecord record, bool receiveAll)
        {
            Tools.Assert(record.FinishCalc == false, "ReceiveAndClearAccumulateData : expect FinishCalc == false");

            if (record.m_maxDti != decimal.MaxValue && (m_maxDti == 0 || record.m_maxDti < m_maxDti))
                m_maxDti = record.m_maxDti;

            if (record.m_maxFrontEndYsp != decimal.MinValue && (m_maxFrontEndYsp.HasValue == false || record.m_maxFrontEndYsp > m_maxFrontEndYsp))
            {
                m_maxFrontEndYsp = record.m_maxFrontEndYsp;
            }

            AddAndClear(ref m_marginDeltaHidden, ref record.m_marginDeltaHidden);
            AddAndClear(ref m_marginDeltaDisclosed, ref record.m_marginDeltaDisclosed);

            AddAndClear(ref m_feeDeltaHidden, ref record.m_feeDeltaHidden);
            AddAndClear(ref m_feeDeltaDisclosed, ref record.m_feeDeltaDisclosed);

            AddAndClear(ref m_rateDeltaHidden, ref record.m_rateDeltaHidden);
            AddAndClear(ref m_rateDeltaDisclosed, ref record.m_rateDeltaDisclosed);

            if (IsOptionArm)
            {
                Tools.Assert(record.m_optionArmException == null, "ReceiveAndClearAccumulateData : expect m_optionArmException == null");
                AddAndClear(ref m_teaserRDeltaHidden, ref record.m_teaserRDeltaHidden);
                AddAndClear(ref m_teaserRDeltaDisclosed, ref record.m_teaserRDeltaDisclosed);
            }

            if (lHasQRateInRateOptions)
            {
                Tools.Assert(record.m_qualRateException == null, "ReceiveAndClearAccumulateData : expect m_qualRateException == null");
                AddAndClear(ref m_qualRDeltaHidden, ref record.m_qualRDeltaHidden);
                AddAndClear(ref m_qualRDeltaDisclosed, ref record.m_qualRDeltaDisclosed);
            }

            bool bUseMaxYspAdjRules = lLpeFeeMinAddRuleAdjBit;
            if (bUseMaxYspAdjRules)
            {
                Tools.Assert(record.m_adjMaxYspException == null, "ReceiveAndClearAccumulateData : expect m_adjMaxYspException == null");
                AddAndClear(ref maxYsp, ref record.m_adjMaxYsp);

            }

            if (record.m_marginBaseAllROptions != decimal.MaxValue)
            {
                m_marginBaseAllROptions = record.m_marginBaseAllROptions;
                m_isMarginBaseAllRateOptions = true;
            }

            if (record.m_disallowLock)
            {
                m_lockDisabledReason = record.m_disallowLockReason;
            }

            if (receiveAll == false)
                return;

            if (record.Disqualify)
            {
                if (record.m_qbcDisqualRule.Length != 0)
                    m_qbcDisqualRule = record.m_qbcDisqualRule;

                if (record.m_disqualifiedRules.Length > 0)
                {
                    m_hasOtherDisqualification = true;
                    if (m_disqualifiedRules.Length > 0)
                        m_disqualifiedRules += "<br>";
                    m_disqualifiedRules += " * " + record.m_disqualifiedRules;
                    record.m_disqualifiedRules = "";
                }
            }

            m_reasonArray.AddRange(record.m_reasonArray);
            record.m_reasonArray.Clear();

            m_stipCollection.Append(record.m_stipCollection);
            m_hiddenStipCollection.Append(record.m_hiddenStipCollection);

            // OPM 42423.
            CCategoryVisibilityStip maxDtiStip = record.m_MaxDtiStip;
            if (maxDtiStip != null)
            {
                if (maxDtiStip.IsHidden == false)
                    m_stipCollection.Add(maxDtiStip.Category, maxDtiStip.Stip);
                else
                    m_hiddenStipCollection.Add(maxDtiStip.Category, maxDtiStip.Stip);
            }


            foreach (PriceAdjustRecord.AdjustItem item in record.m_adjDescsHidden)
            {
                CAdjustItem adjustItem = item.ToCAdjustItem(m_losConvert, lHasQRateInRateOptions, IsOptionArm);
                if (adjustItem != null)
                    m_adjDescsHidden.Add(adjustItem);
            }

            foreach (PriceAdjustRecord.AdjustItem item in record.m_adjDescsDisclosed)
            {
                CAdjustItem adjustItem = item.ToCAdjustItem(m_losConvert, lHasQRateInRateOptions, IsOptionArm);
                if (adjustItem != null)
                    m_adjDescsDisclosed.Add(adjustItem);
            }


        }

        public string GetRateLockSubmissionUserWarningMessageDisplay(Guid lockPolicyID, E_LpeRsExpirationByPassPermissionT lpeRsExpirationByPassPermission)
        {
            return RateOptionExpirationResult.GetFriendlyRateLockSubmissionErrorMessage(lockPolicyID, lpeRsExpirationByPassPermission, RateLockSubmissionUserWarningMessage, this.BrokerId);
        }
        public string UnencodedUniqueChecksum
        {
            get
            {

                // OPM 179060. mf. Users who have the ability to submit through ineligible program
                // can now possibly have the eligibility status be different from result screen
                // to submission if the loan program is eligible, but they choose an unavailable rate.
                var _status = Status;
                if (PrincipalFactory.CurrentPrincipal.HasPermission(Permission.CanApplyForIneligibleLoanPrograms))
                {
                    _status = E_EvalStatus.Eval_Eligible;
                }

                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("{0}!{1}!", lLpTemplateId, _status);

                if (null != this.ApplicantRateOptions)
                {
                    foreach (var rateOption in this.ApplicantRateOptions)
                    {
                        sb.AppendFormat("#{0}:{1}:{2}:{3}#", rateOption.Rate, rateOption.Point_, rateOption.Margin, rateOption.QRate);
                    }
                }

                if (null != this.AdjustDescs)
                {
                    // 3/6/2012 dd - OPM 79286 - In order to avoid adjustment item show up in different
                    // order, I need to perform a sort on adjustment item to have consistent list.
                    List<CAdjustItem> adjustList = new List<CAdjustItem>();
                    foreach (CAdjustItem adjustment in this.AdjustDescs)
                    {
                        adjustList.Add(adjustment);
                    }
                    adjustList.Sort(new CAdjustItemComparer());

                    foreach (CAdjustItem adjustment in adjustList)
                    {
                        if (adjustment != null)
                        {
                            sb.AppendFormat("${0}:{1}:{2}:{3}$", adjustment.Rate, adjustment.Fee, adjustment.Margin, adjustment.QRate);
                        }
                    }
                }

                return sb.ToString();
            }
        }
        public string UniqueChecksum
        {
            get
            {
                // 1/20/2011 dd - Convert key information relate to Loan Program result such as rate price, eligibility and adjustment
                // to a unique checksum that we can use to compare where the program had change.

                string input = UnencodedUniqueChecksum;
                string str = EncryptionHelper.ComputeSHA256Hash(input);
                // 1/21/2011 dd - The string is Base64 encoded string, and it is include "+", "/", "=" characters. Since we will
                // pass this value around in query string, in order to behave correctly we need to do proper urlencode/urldecode.
                // To avoid this hassle I am going to replace these 3 characters with something more safe in url.
                string checksum = str.Replace('+', '_').Replace('/', '-').Replace('=', '.');

                // 3/6/2012 dd - Add for debugging purpose.
                // 5/1/2012 dd - Disable because it polute the log.
                // 8/10/15 gf - We're still running into issues with unexpected
                // checksum mismatches. Re-enabling the logging behind a stage
                // constant so we can toggle it as needed.
                if (ConstStage.LogPricingChecksum)
                {
                    Tools.LogInfo("CApplicantPrice::UniqueCheckSum  [" + checksum + "] input=[" + input + "]");
                }

                return checksum;
            }
        }
        private class CAdjustItemComparer : IComparer<CAdjustItem>
        {

            #region IComparer<CAdjustItem> Members

            public int Compare(CAdjustItem x, CAdjustItem y)
            {
                int ret = x.Rate.CompareTo(y.Rate);
                if (ret == 0)
                {
                    ret = x.Fee.CompareTo(y.Fee);
                    if (ret == 0)
                    {
                        ret = x.Margin.CompareTo(y.Margin);
                        if (ret == 0)
                        {
                            ret = x.QRate.CompareTo(y.QRate);
                        }
                    }
                }
                return ret;
            }

            #endregion
        }
        #region PerRateOption

        // Determines if two DTI result sets are identical
        private bool IsSameDtiResult(Dictionary<Guid, Tuple<bool, string>> previousResult, Dictionary<Guid, Tuple<bool, string>> currentResult, List<string> differentHits)
        {

            if (previousResult == null)
            {
                // This is the first iteration:
                // If there are no results, there are no applicable DTI rules.
                // In this situation, we consider the previous result to all FALSE. (If anything hit, then they are different.)

                if (currentResult.Count != 0)
                {
                    foreach (Guid key in currentResult.Keys)
                    {
                        Tuple<bool, string> result = currentResult[key];
                        if (result.Item1 == true)
                            differentHits.Add(result.Item2);
                    }

                    return differentHits.Count == 0;
                }

                return true;
            }

            // If any rule hit differently, result is not the same
            // PERF: might be faster alternatives.

            foreach (Guid key in currentResult.Keys)
            {
                Tuple<bool, string> lastResult = previousResult[key];
                Tuple<bool, string> thisResult = currentResult[key];


                if (lastResult.Item1 != thisResult.Item1)
                {
                    // This result is different.       
                    differentHits.Add(thisResult.Item2);

                }
            }

            return differentHits.Count == 0;
        }

        // Called per-rate when pricing is done in Per-rate pricing.
        // Set up the adjustments and stips based on this result.
        private void ProcessFinalOption(
            CApplicantRateOption newOption
            , PriceAdjustRecord thisRateAdjust
            , PerOptionAdjustRecord optionAdjustRecord
            , int iteration
            , int iterationLimit
            , string rateDebug
            )
        {
            if (thisRateAdjust != null)
            {
                // DTI Pricing resulted in a disqual: means this rate is not permitted.
                if (thisRateAdjust.Disqualify)
                {
                    string disqualReason = "";
                    string disqualDebug = "";
                    foreach (var disqual in thisRateAdjust.m_reasonArray)
                    {
                        CStipulation disqualStip = disqual as CStipulation;
                        if (disqualStip != null)
                        {
                            disqualReason += (disqualReason.Length == 0 ? "" : "<br />") + "* " + disqualStip.Description;
                            disqualDebug += (disqualDebug.Length == 0 ? "" : "<br /> ") + "* " + disqualStip.Description + disqualStip.DebugString;
                        }
                    }

                    newOption.DisqualReason = disqualReason;
                    newOption.DisqualReasonDebug = disqualDebug;

                    if ((m_runOptions.SelectedRate != 0 && newOption.RateOptionId == m_runOptions.SelectedRateOptionId)
                        && m_evalStatus == E_EvalStatus.Eval_Eligible
                        && m_runOptions.LpeTaskT != LendersOffice.DistributeUnderwriting.E_LpeTaskT.RunEngine)
                    {
                        // OPM 179060. Chosen rate is denied.  Per PDE this needs
                        // to be treated as a program denial.

                        if (!m_loanFileData.BrokerDB.UseRateOptionVariantMI) // VarianPMI code will process this later--this is not a full iteration denial.
                        {
                            m_evalStatus = E_EvalStatus.Eval_Ineligible;
                            m_reasonArray.Add(new CStipulation(disqualReason, disqualDebug));
                            if (m_disqualifiedRules.Length > 0)
                                m_disqualifiedRules += "<br>";
                            m_disqualifiedRules += " * " + disqualReason;
                        }
                    }
                }

                // OPM 130497
                if (thisRateAdjust.m_disallowLock)
                {
                    // Rate-level lock block
                    newOption.DisableLockReason = thisRateAdjust.m_disallowLockReason;
                }
                else if (m_lockDisabledReason != string.Empty)
                {
                    // Program-level lock block
                    newOption.DisableLockReason = m_lockDisabledReason;
                }

                // Add per-options adjustments (if we do not already have them).
                if (thisRateAdjust.m_adjDescsDisclosed.Count != 0)
                    foreach (LendersOfficeApp.los.RatePrice.PriceAdjustRecord.AdjustItem adjust in thisRateAdjust.m_adjDescsDisclosed)
                        optionAdjustRecord.AddAdjustment(adjust.ToCAdjustItem(m_losConvert, lHasQRateInRateOptions, IsOptionArm), newOption, true, m_adjDescsDisclosed, adjust.RuleId);

                if (thisRateAdjust.m_adjDescsHidden.Count != 0)
                    foreach (LendersOfficeApp.los.RatePrice.PriceAdjustRecord.AdjustItem adjust in thisRateAdjust.m_adjDescsHidden)
                        optionAdjustRecord.AddAdjustment(adjust.ToCAdjustItem(m_losConvert, lHasQRateInRateOptions, IsOptionArm), newOption, false, m_adjDescsHidden, adjust.RuleId);

                // If we maxed out iterations, warn that pricing is off
                if (iteration == iterationLimit)
                    optionAdjustRecord.AddIterationStip(m_stipCollection);
            }

            optionAdjustRecord.AddOption(newOption, rateDebug, iteration);
        }


        // Apply adjustments to loan file.
        private void ApplyAdjustmentsAndRound_DynamicFee(
            IRateItem baseOption
            , decimal rateDelta
            , decimal feeDelta
            , decimal marginDelta
            , decimal teaserRDelta
            , decimal qrateDelta
            , bool bSnapUpRate
            , bool bSnapupFee
            , decimal snapupFeeInterval
            , decimal backEndMaxYsp
            , decimal? frontEndMaxYsp
            , out bool maxYspSnappedDown
            , bool bApplyCompensationBeforeRounding
            , decimal appliedOriginatorCompPoint
            )
        {

            // Per Spec in caser 126752:
            // If IsApplyPricingEngineRoundingAfterLoComp=bool_yes, then the steps are...
            // 1. B.E. Max YSP Snapdown
            // 2. LO Comp
            // 3. F.E. (dynamic margin) Max YSP Snapdown
            // 4. Rounding Delta

            // If IsApplyPricingEngineRoundingAfterLoComp=bool_no, then the steps are...
            // 1. B.E. Max YSP Snapdown
            // 2. Rounding Delta
            // 3. LO Comp
            // 4. F.E. (dynamic margin) Max YSP Snapdown

            decimal ysp = baseOption.Point + feeDelta;
            decimal frontEndSnapDown = 0;
            decimal backEndSnapDown = 0;
            decimal feeRounding = 0;

            maxYspSnappedDown = false;

            // OPM 87644.
            m_loanFileData.sNoteRateBeforeLOCompAndRounding = baseOption.Rate + rateDelta;

            bool backEndSnapDownOccured = false;
            bool frontEndSnapDownOccured = false;

            // See OPM 126752.
            if (bApplyCompensationBeforeRounding)
            {
                // 1. B.E. Max YSP Snapdown
                decimal preSnap = ysp;
                ysp = SnapdownYsp(ysp, backEndMaxYsp, out backEndSnapDownOccured);
                if (backEndSnapDownOccured)
                {
                    backEndSnapDown = ysp - preSnap;
                }

                m_loanFileData.sPointsBeforeLOCompAndRounding = ysp;

                // 2. LO Comp
                ysp += appliedOriginatorCompPoint;

                m_loanFileData.sRateOptionPointsPreFEMaxYSP = ysp;

                // 3. F.E. (dynamic margin) Max YSP Snapdown
                //maxYspSnappedDown = frontEndYspSnapDown() || maxYspSnappedDown;
                preSnap = ysp;
                ysp = SnapdownYsp(ysp, frontEndMaxYsp, out frontEndSnapDownOccured);
                if (frontEndSnapDownOccured)
                {
                    frontEndSnapDown = ysp - preSnap;
                }

                // 4. Rounding Delta
                if (bSnapupFee)
                {
                    preSnap = ysp;
                    ysp = Snapup(ysp, snapupFeeInterval);
                    feeRounding = ysp - preSnap;
                }
            }
            else
            { // ( IsApplyPricingEngineRoundingAfterLoComp = FALSE)

                // This is the unusual branch.  It is strange that a lender
                // would want to apply rounding before comp.

                // 1. B.E. Max YSP Snapdown
                decimal preSnap = ysp;
                ysp = SnapdownYsp(ysp, backEndMaxYsp, out backEndSnapDownOccured);
                if (backEndSnapDownOccured)
                {
                    backEndSnapDown = ysp - preSnap;
                }

                m_loanFileData.sPointsBeforeLOCompAndRounding = ysp;

                // 2. Rounding Delta
                if (bSnapupFee)
                {
                    preSnap = ysp;
                    ysp = Snapup(ysp, snapupFeeInterval);
                    feeRounding = ysp - preSnap;
                }

                // 3. LO Comp
                ysp += appliedOriginatorCompPoint;

                m_loanFileData.sRateOptionPointsPreFEMaxYSP = ysp;

                // 4. F.E. (dynamic margin) Max YSP Snapdown
                preSnap = ysp;
                ysp = SnapdownYsp(ysp, frontEndMaxYsp, out frontEndSnapDownOccured);
                if (frontEndSnapDownOccured)
                {
                    frontEndSnapDown = ysp - preSnap;
                }
            }

            // Basically, above we calulated the fee value including the comp (ysp),
            // so set the value in the loan file as exclusive of comp.
            m_loanFileData.sLOrigFPc = ysp - appliedOriginatorCompPoint;

            m_loanFileData.sPointsAfterLOCompAndRounding = ysp; // OBSOLETE.  Removed when SAEs have finished migration away.

            m_loanFileData.sRateOptionFinalPrice = ysp;

            // Consider a "rate option max ysp" to have been triggered if there was EITHER 
            // a back-end OR a front-end max ysp snapdown for that rate option.
            // (The per-rate maxYsp is the to be the HIGHER of the "back-end max ysp" and 
            // "front-end max ysp" settings for a given rate, and the per-program maxYSP
            // is the lowest one that trigged a snapdown.)
            maxYspSnappedDown = backEndSnapDownOccured || frontEndSnapDownOccured;


            decimal r = baseOption.Rate + rateDelta;
            if (bSnapUpRate)
                r = SnapupOneEighth(r);

            // OPM 48007
            if (m_runOptions.SelectedRate != 0 && r == m_runOptions.SelectedRate)
            {
                m_rateRounding = r - (baseOption.Rate + rateDelta);
                m_feeRounding = feeRounding;
                m_frontEndSnapDown = frontEndSnapDown;
                m_backEndSnapDown = backEndSnapDown;
            }

            m_loanFileData.sNoteIR = r;

            // OPM 87644.
            m_loanFileData.sNoteRateAfterLOCompAndRounding = r;

            if (!m_isMarginBaseAllRateOptions)
                m_loanFileData.sRAdjMarginR = baseOption.Margin + marginDelta;

            if (IsOptionArm)
                m_loanFileData.sOptionArmTeaserR = baseOption.TeaserRate + teaserRDelta;
            else
                m_loanFileData.sOptionArmTeaserR = 0;

            if (!ConstStage.EnableLegacyQualRateCalculation &&
                (this.lQualRateCalculationT != QualRateCalculationT.FlatValue ||
                this.lQualRateCalculationFieldT1 != QualRateCalculationFieldT.LpeUploadValue))
            {
                var calculationOptions = new QualRateCalculationOptions(this, this.m_loanFileData.sNoteIR, baseOption.Margin + marginDelta);
                this.m_loanFileData.sQualIR = Tools.CalculateQualRate(calculationOptions);
                this.m_loanFileData.sQualIRLckd = false;
            }
            else if (lHasQRateInRateOptions)
            {
                this.m_loanFileData.sQualIR = baseOption.QRateBase + qrateDelta;
                this.m_loanFileData.sQualIRLckd = true;
            }
            else
            {
                this.m_loanFileData.sQualIR = 0;
                this.m_loanFileData.sQualIRLckd = true;
            }

            this.m_loanFileData.sUseQualRate = this.lHasQRateInRateOptions;
            this.m_loanFileData.sQualRateCalculationT = this.lQualRateCalculationT;
            this.m_loanFileData.sQualRateCalculationFieldT1 = this.lQualRateCalculationFieldT1;
            this.m_loanFileData.sQualRateCalculationFieldT2 = this.lQualRateCalculationFieldT2;
            this.m_loanFileData.sQualRateCalculationAdjustment1 = this.lQualRateCalculationAdjustment1;
            this.m_loanFileData.sQualRateCalculationAdjustment2 = this.lQualRateCalculationAdjustment2;

            m_loanFileData.InvalidateCache();
        }

        private decimal SnapdownYsp(decimal ysp, decimal? maxYsp, out bool snappedDown)
        {
            if (maxYsp.HasValue == false)
            {
                snappedDown = false;
                return ysp;
            }

            return SnapdownYsp(ysp, maxYsp.Value, out snappedDown);
        }

        private decimal SnapdownYsp(decimal ysp, decimal maxYsp, out bool snappedDown)
        {
            if (ysp < maxYsp)
            {
                // Oh, snap!
                ysp = maxYsp;
                snappedDown = true;
            }
            else
            {
                snappedDown = false;
            }

            return ysp;
        }

        #endregion

        public ILoanProgramTemplate LoanProgramTemplate { get { return m_loanProgramTemplate; } }
        #region ILoanProgramTemplate Members

        public Guid lLpTemplateId
        {
            get { return m_loanProgramTemplate.lLpTemplateId; }
        }

        public Guid BrokerId
        {
            get { return m_loanProgramTemplate.BrokerId; }
        }

        public string lLpTemplateNm
        {
            get { return m_loanProgramTemplate.lLpTemplateNm; }
        }

        public string lLendNm
        {
            get { return m_loanProgramTemplate.lLendNm; }
        }

        public E_sLT lLT
        {
            get { return m_loanProgramTemplate.lLT; }
        }

        public E_sLienPosT lLienPosT
        {
            get { return m_loanProgramTemplate.lLienPosT; }
        }

        public decimal lQualR
        {
            get { return m_loanProgramTemplate.lQualR; }
        }

        public int lTerm
        {
            get { return m_loanProgramTemplate.lTerm; }
        }

        public int lDue
        {
            get { return m_loanProgramTemplate.lDue; }
        }

        public int lLockedDays
        {
            get { return m_loanProgramTemplate.lLockedDays; }
        }

        public decimal lReqTopR
        {
            get { return m_loanProgramTemplate.lReqTopR; }
        }

        public decimal lReqBotR
        {
            get { return m_loanProgramTemplate.lReqBotR; }
        }

        public decimal lRadj1stCapR
        {
            get { return m_loanProgramTemplate.lRadj1stCapR; }
        }

        public int lRadj1stCapMon
        {
            get { return m_loanProgramTemplate.lRadj1stCapMon; }
        }

        public decimal lRAdjCapR
        {
            get { return m_loanProgramTemplate.lRAdjCapR; }
        }

        public int lRAdjCapMon
        {
            get { return m_loanProgramTemplate.lRAdjCapMon; }
        }

        public decimal lRAdjLifeCapR
        {
            get { return m_loanProgramTemplate.lRAdjLifeCapR; }
        }

        public decimal lRAdjMarginR
        {
            get { return m_loanProgramTemplate.lRAdjMarginR; }
            set { m_loanProgramTemplate.lRAdjMarginR = value; }
        }

        public decimal lRAdjIndexR
        {
            get
            {
                if (this.CurrentProgramForHistoricalPricing != null)
                {
                    return this.CurrentProgramForHistoricalPricing.lRAdjIndexR;
                }

                return m_loanProgramTemplate.lRAdjIndexR;
            }
        }

        public decimal lRAdjFloorR
        {
            get { return m_loanProgramTemplate.lRAdjFloorR; }
        }

        public E_sRAdjRoundT lRAdjRoundT
        {
            get { return m_loanProgramTemplate.lRAdjRoundT; }
        }

        public decimal lPmtAdjCapR
        {
            get { return m_loanProgramTemplate.lPmtAdjCapR; }
        }

        public int lPmtAdjCapMon
        {
            get { return m_loanProgramTemplate.lPmtAdjCapMon; }
        }

        public int lPmtAdjRecastPeriodMon
        {
            get { return m_loanProgramTemplate.lPmtAdjRecastPeriodMon; }
        }

        public int lPmtAdjRecastStop
        {
            get { return m_loanProgramTemplate.lPmtAdjRecastStop; }
        }

        public decimal lBuydwnR1
        {
            get { return m_loanProgramTemplate.lBuydwnR1; }
        }

        public decimal lBuydwnR2
        {
            get { return m_loanProgramTemplate.lBuydwnR2; }
        }

        public decimal lBuydwnR3
        {
            get { return m_loanProgramTemplate.lBuydwnR3; }
        }

        public decimal lBuydwnR4
        {
            get { return m_loanProgramTemplate.lBuydwnR4; }
        }

        public decimal lBuydwnR5
        {
            get { return m_loanProgramTemplate.lBuydwnR5; }
        }

        public int lBuydwnMon1
        {
            get { return m_loanProgramTemplate.lBuydwnMon1; }
        }

        public int lBuydwnMon2
        {
            get { return m_loanProgramTemplate.lBuydwnMon2; }
        }

        public int lBuydwnMon3
        {
            get { return m_loanProgramTemplate.lBuydwnMon3; }
        }

        public int lBuydwnMon4
        {
            get { return m_loanProgramTemplate.lBuydwnMon4; }
        }

        public int lBuydwnMon5
        {
            get { return m_loanProgramTemplate.lBuydwnMon5; }
        }

        public int lGradPmtYrs
        {
            get { return m_loanProgramTemplate.lGradPmtYrs; }
        }

        public decimal lGradPmtR
        {
            get { return m_loanProgramTemplate.lGradPmtR; }
        }

        public int lIOnlyMon
        {
            get { return m_loanProgramTemplate.lIOnlyMon; }
        }

        public bool lHasVarRFeature
        {
            get { return m_loanProgramTemplate.lHasVarRFeature; }
        }

        public string lVarRNotes
        {
            get { return m_loanProgramTemplate.lVarRNotes; }
        }

        public bool lAprIncludesReqDeposit
        {
            get { return m_loanProgramTemplate.lAprIncludesReqDeposit; }
        }

        public bool lHasDemandFeature
        {
            get { return m_loanProgramTemplate.lHasDemandFeature; }
        }

        public string lLateDays
        {
            get { return m_loanProgramTemplate.lLateDays; }
        }

        public string lLateChargePc
        {
            get { return m_loanProgramTemplate.lLateChargePc; }
        }

        public E_sPrepmtPenaltyT lPrepmtPenaltyT
        {
            get { return m_loanProgramTemplate.lPrepmtPenaltyT; }
        }

        public E_sAssumeLT lAssumeLT
        {
            get { return m_loanProgramTemplate.lAssumeLT; }
        }

        public Guid lCcTemplateId
        {
            get { return m_loanProgramTemplate.lCcTemplateId; }
        }

        public string lFilingF
        {
            get { return m_loanProgramTemplate.lFilingF; }
        }

        public string lLateChargeBaseDesc
        {
            get { return m_loanProgramTemplate.lLateChargeBaseDesc; }
        }

        public decimal lPmtAdjMaxBalPc
        {
            get { return m_loanProgramTemplate.lPmtAdjMaxBalPc; }
        }

        public E_sFinMethT lFinMethT
        {
            get { return m_loanProgramTemplate.lFinMethT; }
        }

        public string lFinMethDesc
        {
            get { return m_loanProgramTemplate.lFinMethDesc; }
        }

        public E_sPrepmtRefundT lPrepmtRefundT
        {
            get { return m_loanProgramTemplate.lPrepmtRefundT; }
        }

        public Guid FolderId
        {
            get { return m_loanProgramTemplate.FolderId; }
        }

        public CDateTime lRateSheetEffectiveD
        {
            get { return m_loanProgramTemplate.lRateSheetEffectiveD; }
        }

        public CDateTime lRateSheetExpirationD
        {
            get { return m_loanProgramTemplate.lRateSheetExpirationD; }
        }

        public bool IsMaster
        {
            get { return m_loanProgramTemplate.IsMaster; }
        }

        public decimal lLpeFeeMin
        {
            get { return m_loanProgramTemplate.lLpeFeeMin; }
        }

        public decimal lLpeFeeMax
        {
            get { return m_loanProgramTemplate.lLpeFeeMax; }
        }

        public string PairingProductIds
        {
            get { return m_loanProgramTemplate.PairingProductIds; }
        }

        public Guid lBaseLpId
        {
            get { return m_loanProgramTemplate.lBaseLpId; }
        }

        public bool IsEnabled
        {
            get { return m_loanProgramTemplate.IsEnabled; }
        }

        public Guid lArmIndexGuid
        {
            get { return m_loanProgramTemplate.lArmIndexGuid; }
        }

        public string lArmIndexBasedOnVstr
        {
            get { return m_loanProgramTemplate.lArmIndexBasedOnVstr; }
        }

        public string lArmIndexCanBeFoundVstr
        {
            get { return m_loanProgramTemplate.lArmIndexCanBeFoundVstr; }
        }

        public bool lArmIndexAffectInitIRBit
        {
            get { return m_loanProgramTemplate.lArmIndexAffectInitIRBit; }
        }

        public decimal lRAdjRoundToR
        {
            get { return m_loanProgramTemplate.lRAdjRoundToR; }
        }

        public CDateTime lArmIndexEffectiveD
        {
            get { return m_loanProgramTemplate.lArmIndexEffectiveD; }
        }

        public E_sFreddieArmIndexT lFreddieArmIndexT
        {
            get { return m_loanProgramTemplate.lFreddieArmIndexT; }
        }

        public string lArmIndexNameVstr
        {
            get { return m_loanProgramTemplate.lArmIndexNameVstr; }
        }

        public E_sArmIndexT lArmIndexT
        {
            get { return m_loanProgramTemplate.lArmIndexT; }
        }

        public string lArmIndexNotifyAtLeastDaysVstr
        {
            get { return m_loanProgramTemplate.lArmIndexNotifyAtLeastDaysVstr; }
        }

        public string lArmIndexNotifyNotBeforeDaysVstr
        {
            get { return m_loanProgramTemplate.lArmIndexNotifyNotBeforeDaysVstr; }
        }

        public string lLpTemplateNmInherit
        {
            get { return m_loanProgramTemplate.lLpTemplateNmInherit; }
        }

        public bool lLpTemplateNmOverrideBit
        {
            get { return m_loanProgramTemplate.lLpTemplateNmOverrideBit; }
        }

        public Guid lCcTemplateIdInherit
        {
            get { return m_loanProgramTemplate.lCcTemplateIdInherit; }
        }

        public bool lCcTemplateIdOverrideBit
        {
            get { return m_loanProgramTemplate.lCcTemplateIdOverrideBit; }
        }

        public int lLockedDaysInherit
        {
            get { return m_loanProgramTemplate.lLockedDaysInherit; }
        }

        public bool lLockedDaysOverrideBit
        {
            get { return m_loanProgramTemplate.lLockedDaysOverrideBit; }
        }

        public decimal lLpeFeeMinInherit
        {
            get { return m_loanProgramTemplate.lLpeFeeMinInherit; }
        }

        public bool lLpeFeeMinOverrideBit
        {
            get { return m_loanProgramTemplate.lLpeFeeMinOverrideBit; }
        }

        public decimal lLpeFeeMaxInherit
        {
            get { return m_loanProgramTemplate.lLpeFeeMaxInherit; }
        }

        public bool lLpeFeeMaxOverrideBit
        {
            get { return m_loanProgramTemplate.lLpeFeeMaxOverrideBit; }
        }

        public bool IsEnabledInherit
        {
            get { return m_loanProgramTemplate.IsEnabledInherit; }
        }

        public bool IsEnabledOverrideBit
        {
            get { return m_loanProgramTemplate.IsEnabledOverrideBit; }
        }

        public int lPpmtPenaltyMon
        {
            get { return m_loanProgramTemplate.lPpmtPenaltyMon; }
        }

        public int lPpmtPenaltyMonInherit
        {
            get { return m_loanProgramTemplate.lPpmtPenaltyMonInherit; }
        }

        public bool lPpmtPenaltyMonOverrideBit
        {
            get { return m_loanProgramTemplate.lPpmtPenaltyMonOverrideBit; }
        }

        public decimal lRateDelta
        {
            get { return m_loanProgramTemplate.lRateDelta; }
        }

        public decimal lFeeDelta
        {
            get { return m_loanProgramTemplate.lFeeDelta; }
        }

        public int lPpmtPenaltyMonLowerSearch
        {
            get { return m_loanProgramTemplate.lPpmtPenaltyMonLowerSearch; }
        }

        public int lLockedDaysLowerSearch
        {
            get { return m_loanProgramTemplate.lLockedDaysLowerSearch; }
        }

        public int lLockedDaysLowerSearchInherit
        {
            get { return m_loanProgramTemplate.lLockedDaysLowerSearchInherit; }
        }

        public int lPpmtPenaltyMonLowerSearchInherit
        {
            get { return m_loanProgramTemplate.lPpmtPenaltyMonLowerSearchInherit; }
        }

        public string PairingProductIdsInherit
        {
            get { return m_loanProgramTemplate.PairingProductIdsInherit; }
        }

        public bool PairingProductIdsOverrideBit
        {
            get { return m_loanProgramTemplate.PairingProductIdsOverrideBit; }
        }

        public string lLpInvestorNm
        {
            get { return m_loanProgramTemplate.lLpInvestorNm; }
        }

        public string lLendNmInherit
        {
            get { return m_loanProgramTemplate.lLendNmInherit; }
        }

        public bool lLendNmOverrideBit
        {
            get { return m_loanProgramTemplate.lLendNmOverrideBit; }
        }

        public string lRateOptionBaseId
        {
            get { return m_loanProgramTemplate.lRateOptionBaseId; }
        }

        public Guid PairIdFor1stLienProdGuid
        {
            get { return m_loanProgramTemplate.PairIdFor1stLienProdGuid; }
        }

        public Guid PairIdFor1stLienProdGuidInherit
        {
            get { return m_loanProgramTemplate.PairIdFor1stLienProdGuidInherit; }
        }

        public decimal lLpeFeeMinParam
        {
            get { return m_loanProgramTemplate.lLpeFeeMinParam; }
        }

        public bool lLpeFeeMinAddRuleAdjInheritBit
        {
            get { return m_loanProgramTemplate.lLpeFeeMinAddRuleAdjInheritBit; }
        }

        public bool IsLpe
        {
            get { return m_loanProgramTemplate.IsLpe; }
        }

        public bool lIsArmMarginDisplayed
        {
            get { return m_loanProgramTemplate.lIsArmMarginDisplayed; }
        }

        public string ProductCode
        {
            get { return m_loanProgramTemplate.ProductCode; }
        }

        public string ProductIdentifier
        {
            get
            {
                return m_loanProgramTemplate.ProductIdentifier;
            }
        }

        public bool IsOptionArm
        {
            get { return m_loanProgramTemplate.IsOptionArm; }
        }

        public decimal lRateDeltaInherit
        {
            get { return m_loanProgramTemplate.lRateDeltaInherit; }
        }

        public decimal lFeeDeltaInherit
        {
            get { return m_loanProgramTemplate.lFeeDeltaInherit; }
        }

        public Guid SrcRateOptionsProgIdInherit
        {
            get { return m_loanProgramTemplate.SrcRateOptionsProgIdInherit; }
        }

        public Guid SrcRateOptionsProgId
        {
            get { return m_loanProgramTemplate.SrcRateOptionsProgId; }
        }

        public E_sLpDPmtT lLpDPmtT
        {
            get { return m_loanProgramTemplate.lLpDPmtT; }
        }

        public E_sLpQPmtT lLpQPmtT
        {
            get { return m_loanProgramTemplate.lLpQPmtT; }
        }

        public bool lHasQRateInRateOptions
        {
            get { return m_loanProgramTemplate.lHasQRateInRateOptions; }
            set { m_loanProgramTemplate.lHasQRateInRateOptions = value; }
        }

        public QualRateCalculationT lQualRateCalculationT
        {
            get { return this.m_loanProgramTemplate.lQualRateCalculationT; }
        }

        public QualRateCalculationFieldT lQualRateCalculationFieldT1
        {
            get { return this.m_loanProgramTemplate.lQualRateCalculationFieldT1; }
        }

        public QualRateCalculationFieldT lQualRateCalculationFieldT2
        {
            get { return this.m_loanProgramTemplate.lQualRateCalculationFieldT2; }
        }

        public decimal lQualRateCalculationAdjustment1
        {
            get { return this.m_loanProgramTemplate.lQualRateCalculationAdjustment1; }
        }

        public decimal lQualRateCalculationAdjustment2
        {
            get { return this.m_loanProgramTemplate.lQualRateCalculationAdjustment2; }
        }

        public int lQualTerm
        {
            get { return this.m_loanProgramTemplate.lQualTerm; }
        }

        public QualTermCalculationType lQualTermCalculationType
        {
            get { return this.m_loanProgramTemplate.lQualTermCalculationType; }
        }

        public bool IsLpeDummyProgram
        {
            get { return m_loanProgramTemplate.IsLpeDummyProgram; }
        }

        public bool IsPairedOnlyWithSameInvestor
        {
            get { return m_loanProgramTemplate.IsPairedOnlyWithSameInvestor; }
        }

        public int lIOnlyMonLowerSearch
        {
            get { return m_loanProgramTemplate.lIOnlyMonLowerSearch; }
        }

        public int lIOnlyMonLowerSearchInherit
        {
            get { return m_loanProgramTemplate.lIOnlyMonLowerSearchInherit; }
        }

        public bool lIOnlyMonLowerSearchOverrideBit
        {
            get { return m_loanProgramTemplate.lIOnlyMonLowerSearchOverrideBit; }
        }

        public int lIOnlyMonUpperSearch
        {
            get { return m_loanProgramTemplate.lIOnlyMonUpperSearch; }
        }

        public int lIOnlyMonUpperSearchInherit
        {
            get { return m_loanProgramTemplate.lIOnlyMonUpperSearchInherit; }
        }

        public bool lIOnlyMonUpperSearchOverrideBit
        {
            get { return m_loanProgramTemplate.lIOnlyMonUpperSearchOverrideBit; }
        }

        public bool CanBeStandAlone2nd
        {
            get { return m_loanProgramTemplate.CanBeStandAlone2nd; }
        }

        public bool lDtiUsingMaxBalPc
        {
            get { return m_loanProgramTemplate.lDtiUsingMaxBalPc; }
        }

        public bool IsMasterPriceGroup
        {
            get { return m_loanProgramTemplate.IsMasterPriceGroup; }
        }

        public string lLpProductType
        {
            get { return m_loanProgramTemplate.lLpProductType; }
        }

        public E_sRAdjFloorBaseT lRAdjFloorBaseT
        {
            get { return m_loanProgramTemplate.lRAdjFloorBaseT; }
        }

        public bool IsConvertibleMortgage
        {
            get { return m_loanProgramTemplate.IsConvertibleMortgage; }
        }

        public bool lLpmiSupportedOutsidePmi
        {
            get { return m_loanProgramTemplate.lLpmiSupportedOutsidePmi; }
        }

        public bool lWholesaleChannelProgram
        {
            get { return m_loanProgramTemplate.lWholesaleChannelProgram; }
        }

        public int lHardPrepmtPeriodMonths
        {
            get { return m_loanProgramTemplate.lHardPrepmtPeriodMonths; }
        }

        public int lSoftPrepmtPeriodMonths
        {
            get { return m_loanProgramTemplate.lSoftPrepmtPeriodMonths; }
        }

        public E_sHelocPmtBaseT lHelocQualPmtBaseT
        {
            get { return m_loanProgramTemplate.lHelocQualPmtBaseT; }
        }
        public E_sHelocPmtFormulaT lHelocQualPmtFormulaT
        {
            get { return m_loanProgramTemplate.lHelocQualPmtFormulaT; }
        }

        public E_sHelocPmtFormulaRateT lHelocQualPmtFormulaRateT
        {
            get { return m_loanProgramTemplate.lHelocQualPmtFormulaRateT; }
        }

        public E_sHelocPmtAmortTermT lHelocQualPmtAmortTermT
        {
            get { return m_loanProgramTemplate.lHelocQualPmtAmortTermT; }
        }

        public E_sHelocPmtPcBaseT lHelocQualPmtPcBaseT
        {
            get { return m_loanProgramTemplate.lHelocQualPmtPcBaseT; }
        }

        public decimal lHelocQualPmtMb
        {
            get { return m_loanProgramTemplate.lHelocQualPmtMb; }
        }

        public E_sHelocPmtBaseT lHelocPmtBaseT
        {
            get { return m_loanProgramTemplate.lHelocPmtBaseT; }
        }

        public E_sHelocPmtFormulaT lHelocPmtFormulaT
        {
            get { return m_loanProgramTemplate.lHelocPmtFormulaT; }
        }

        public E_sHelocPmtFormulaRateT lHelocPmtFormulaRateT
        {
            get { return m_loanProgramTemplate.lHelocPmtFormulaRateT; }
        }

        public E_sHelocPmtAmortTermT lHelocPmtAmortTermT
        {
            get { return m_loanProgramTemplate.lHelocPmtAmortTermT; }
        }

        public E_sHelocPmtPcBaseT lHelocPmtPcBaseT
        {
            get { return m_loanProgramTemplate.lHelocPmtPcBaseT; }
        }

        public decimal lHelocPmtMb
        {
            get { return m_loanProgramTemplate.lHelocPmtMb; }
        }

        public int lHelocDraw
        {
            get { return m_loanProgramTemplate.lHelocDraw; }
        }

        public bool lHelocCalculatePrepaidInterest
        {
            get { return m_loanProgramTemplate.lHelocCalculatePrepaidInterest; }
        }

        public string lTerm_rep
        {
            get { return m_loanProgramTemplate.lTerm_rep; }
        }

        public string lDue_rep
        {
            get { return m_loanProgramTemplate.lDue_rep; }
        }

        public string lHardPrepmtPeriodMonths_rep
        {
            get { return m_loanProgramTemplate.lHardPrepmtPeriodMonths_rep; }
        }

        public string lSoftPrepmtPeriodMonths_rep
        {
            get { return m_loanProgramTemplate.lSoftPrepmtPeriodMonths_rep; }
        }

        public string lHelocQualPmtMb_rep
        {
            get { return m_loanProgramTemplate.lHelocQualPmtMb_rep; }
        }

        public string lHelocPmtMb_rep
        {
            get { return m_loanProgramTemplate.lHelocPmtMb_rep; }
        }
        #endregion

        #region ILoanProgramTemplate Members


        public string lLockedDays_rep
        {
            get { return m_loanProgramTemplate.lLockedDays_rep; }
        }

        public string lReqTopR_rep
        {
            get { return m_loanProgramTemplate.lReqTopR_rep; }
        }

        public string lReqBotR_rep
        {
            get { return m_loanProgramTemplate.lReqBotR_rep; }
        }

        public string lRadj1stCapR_rep
        {
            get { return m_loanProgramTemplate.lRadj1stCapR_rep; }
        }

        public string lRadj1stCapMon_rep
        {
            get { return m_loanProgramTemplate.lRadj1stCapMon_rep; }
        }

        public string lRAdjCapR_rep
        {
            get { return m_loanProgramTemplate.lRAdjCapR_rep; }
        }

        public string lRAdjCapMon_rep
        {
            get { return m_loanProgramTemplate.lRAdjCapMon_rep; }
        }

        public string lRAdjLifeCapR_rep
        {
            get { return m_loanProgramTemplate.lRAdjLifeCapR_rep; }
        }

        public string lRAdjMarginR_rep
        {
            get { return m_loanProgramTemplate.lRAdjMarginR_rep; }
        }

        public string lRAdjIndexR_rep
        {
            get { return m_loanProgramTemplate.lRAdjIndexR_rep; }
        }

        public string lRAdjFloorR_rep
        {
            get { return m_loanProgramTemplate.lRAdjFloorR_rep; }
        }

        public string lPmtAdjCapR_rep
        {
            get { return m_loanProgramTemplate.lPmtAdjCapR_rep; }
        }

        public string lPmtAdjCapMon_rep
        {
            get { return m_loanProgramTemplate.lPmtAdjCapMon_rep; }
        }

        public string lPmtAdjRecastPeriodMon_rep
        {
            get { return m_loanProgramTemplate.lPmtAdjRecastPeriodMon_rep; }
        }

        public string lPmtAdjRecastStop_rep
        {
            get { return m_loanProgramTemplate.lPmtAdjRecastStop_rep; }
        }

        public string lBuydwnR1_rep
        {
            get { return m_loanProgramTemplate.lBuydwnR1_rep; }
        }

        public string lBuydwnR2_rep
        {
            get { return m_loanProgramTemplate.lBuydwnR2_rep; }
        }

        public string lBuydwnR3_rep
        {
            get { return m_loanProgramTemplate.lBuydwnR3_rep; }
        }

        public string lBuydwnR4_rep
        {
            get { return m_loanProgramTemplate.lBuydwnR4_rep; }
        }

        public string lBuydwnR5_rep
        {
            get { return m_loanProgramTemplate.lBuydwnR5_rep; }
        }

        public string lBuydwnMon1_rep
        {
            get { return m_loanProgramTemplate.lBuydwnMon1_rep; }
        }

        public string lBuydwnMon2_rep
        {
            get { return m_loanProgramTemplate.lBuydwnMon2_rep; }
        }

        public string lBuydwnMon3_rep
        {
            get { return m_loanProgramTemplate.lBuydwnMon3_rep; }
        }

        public string lBuydwnMon4_rep
        {
            get { return m_loanProgramTemplate.lBuydwnMon4_rep; }
        }

        public string lBuydwnMon5_rep
        {
            get { return m_loanProgramTemplate.lBuydwnMon5_rep; }
        }

        public string lGradPmtYrs_rep
        {
            get { return m_loanProgramTemplate.lGradPmtYrs_rep; }
        }

        public string lGradPmtR_rep
        {
            get { return m_loanProgramTemplate.lGradPmtR_rep; }
        }

        public string lHelocDraw_rep
        {
            get { return m_loanProgramTemplate.lHelocDraw_rep; }
        }

        public bool IsHeloc
        {
            get { return m_loanProgramTemplate.IsHeloc; }
        }

        #endregion

        #region ILoanProgramTemplate Members


        public string lIOnlyMon_rep
        {
            get { return m_loanProgramTemplate.lIOnlyMon_rep; }
        }

        public string lPmtAdjMaxBalPc_rep
        {
            get { return m_loanProgramTemplate.lPmtAdjMaxBalPc_rep; }
        }

        public string lLpeFeeMin_rep
        {
            get { return m_loanProgramTemplate.lLpeFeeMin_rep; }
        }

        public string lLpeFeeMax_rep
        {
            get { return m_loanProgramTemplate.lLpeFeeMax_rep; }
        }

        public string lRateDelta_rep
        {
            get { return m_loanProgramTemplate.lRateDelta_rep; }
        }

        public string lFeeDelta_rep
        {
            get { return m_loanProgramTemplate.lFeeDelta_rep; }
        }

        #endregion

        #region ILoanProgramTemplate Members


        public string lPrepmtPeriod_rep
        {
            get { return m_loanProgramTemplate.lPrepmtPeriod_rep; }
        }

        public decimal lNoteR
        {
            get { return m_loanProgramTemplate.lNoteR; }
        }

        public decimal lLOrigFPc
        {
            get { return m_loanProgramTemplate.lLOrigFPc; }
        }

        public string lHelocRepay_rep
        {
            get { return m_loanProgramTemplate.lHelocRepay_rep; }
        }

        public DateTime lRateSheetDownloadEndD
        {
            get { return m_loanProgramTemplate.lRateSheetDownloadEndD; }
        }

        public DateTime lRateSheetDownloadStartD
        {
            get { return m_loanProgramTemplate.lRateSheetDownloadStartD; }
        }

        #endregion

        #region ILoanProgramTemplate Members


        public string lArmIndexEffectiveD_rep
        {
            get { return m_loanProgramTemplate.lArmIndexEffectiveD_rep; }
        }

        public CCcTemplateData CcTemplate
        {
            get { return m_loanProgramTemplate.CcTemplate; }
        }

        public bool HasCcTemplate
        {
            get { return m_loanProgramTemplate.HasCcTemplate; }
        }

        public string FullName
        {
            get { return m_loanProgramTemplate.FullName; }
        }

        public long LpeAcceptableRsFileVersionNumber
        {
            get { return m_loanProgramTemplate.LpeAcceptableRsFileVersionNumber; }
        }
        public string LpeAcceptableRsFileId
        {
            get { return m_loanProgramTemplate.LpeAcceptableRsFileId; }
        }

        public string lLpCustomCode1
        {
            get { return m_loanProgramTemplate.lLpCustomCode1; }
        }

        public string lLpCustomCode2
        {
            get { return m_loanProgramTemplate.lLpCustomCode2; }
        }

        public string lLpCustomCode3
        {
            get { return m_loanProgramTemplate.lLpCustomCode3; }
        }

        public string lLpCustomCode4
        {
            get { return m_loanProgramTemplate.lLpCustomCode4; }
        }

        public string lLpCustomCode5
        {
            get { return m_loanProgramTemplate.lLpCustomCode5; }
        }

        public string lLpInvestorCode1
        {
            get { return m_loanProgramTemplate.lLpInvestorCode1; }
        }

        public string lLpInvestorCode2
        {
            get { return m_loanProgramTemplate.lLpInvestorCode2; }
        }

        public string lLpInvestorCode3
        {
            get { return m_loanProgramTemplate.lLpInvestorCode3; }
        }

        public string lLpInvestorCode4
        {
            get { return m_loanProgramTemplate.lLpInvestorCode4; }
        }

        public string lLpInvestorCode5
        {
            get { return m_loanProgramTemplate.lLpInvestorCode5; }
        }
        #endregion

        #region ILoanProgramTemplate Members


        public IRateItem[] GetRawRateSheetWithDeltas()
        {

            var items = m_loanProgramTemplate.GetRawRateSheetWithDeltas();

            var sOriginatingCompanyIndexNumber = x_loanFileData.sOriginatingCompanyIndexNumber;
            if (sOriginatingCompanyIndexNumber.HasValue)
            {
                IEnumerable<RtRateMarginItem> rateMarginItems = LoanProgramSet.GetTpoRateMargin(x_loanFileData.sBrokerId, sOriginatingCompanyIndexNumber.Value, m_loanProgramTemplate.lLpTemplateId);
                if (rateMarginItems != null)
                {
                    Dictionary<decimal, decimal> rateMarginDict = rateMarginItems.ToDictionary(ele => ele.NoteRate, ele => ele.Margin);
                    for (int i = 0; i < items.Length; i++)
                    {
                        var item = items[i];
                        decimal rate = item.Rate;

                        if (!item.IsSpecialKeyword && rateMarginDict.ContainsKey(rate))
                        {
                            FileBasedRateItem clone = new FileBasedRateItem();
                            clone.__Rate = item.Rate.ToString();
                            clone.__Point = (item.Point + rateMarginDict[rate]).ToString();
                            clone.__Margin = item.Margin.ToString();
                            clone.__QRateBase = item.QRateBase.ToString();
                            clone.__TeaserRate = item.TeaserRate.ToString();
                            items[i] = clone;
        }
                    }
                }
            }

            return items;
        }



        public IRateItem GetRateOptionKey(int sProdRLckdDays, int sProdPpmtPenaltyMon, int sIOnlyMon)
        {
            if (null == m_rateSheetGroupList)
                ConstructRateSheetGroupList();

            foreach (CRateSheetGroup rateSheetGroup in m_rateSheetGroupList)
            {
                if (rateSheetGroup.IsInRange(sProdRLckdDays, sProdPpmtPenaltyMon, sIOnlyMon))
                {
                    IRateItem[] fields = rateSheetGroup.RateSheetFields;

                    IRateItem keyOption = null;

                    for (int i = 0; i < fields.Length; i++)
                    {
                        if (null == keyOption)
                            keyOption = fields[i];

                        if (fields[i].Point > -1)
                            break;

                        // Assuming that rateoptions are always from most negative fee to least negative fee.
                        keyOption = fields[i];
                    }
                    return keyOption;
                }
            }

            // If Non-LPE broker get here return null if no valid rate option found
            return null;

        }

        public bool VerifyRateOptionPricingSegment(int sProdRLckdDays, int sProdPpmtPenaltyMon, int sIOnlyMon, ref string DisqualMessage)
        {

            if (null == m_rateSheetGroupList)
                ConstructRateSheetGroupList();

            bool ret = CRateSheetGroup.VerifyRateOptionPricingSegment(m_rateSheetGroupList, sProdRLckdDays, sProdPpmtPenaltyMon, sIOnlyMon, ref DisqualMessage);
            LogInfo("VerifyRateOptionPricingSegment sProdRLckdDays=" + sProdRLckdDays + ", sProdPpmtPenaltyMon=" + sProdPpmtPenaltyMon + ", sIOnlyMon=" + sIOnlyMon + ". Ret=" + ret);
            return ret;
        }

        public bool lLpeFeeMinAddRuleAdjBit
        {
            get { return m_loanProgramTemplate.lLpeFeeMinAddRuleAdjBit; }
        }
        public IRateItem GetParRateOption_YspConsidered(int sProdRLckdDays, int sProdPpmtPenaltyMon, int sIOnlyMon, decimal adjustPoint, decimal minYsp, decimal maxYsp, CLpRunOptions options)
        {
            if (null == m_rateSheetGroupList)
                ConstructRateSheetGroupList();

            bool bSnapup = options.RoundUpLpeFee;
            decimal snapUpInterval = options.RoundUpLpeFeeInterval;

            if (bSnapup)
            {

                maxYsp = Snapup(maxYsp, snapUpInterval);
                minYsp = Snapup(minYsp, snapUpInterval);
            }


            foreach (CRateSheetGroup rateSheetGroup in m_rateSheetGroupList)
            {
                if (rateSheetGroup.IsInRange(sProdRLckdDays, sProdPpmtPenaltyMon, sIOnlyMon))
                {
                    IRateItem[] fields = rateSheetGroup.RateSheetFields;

                    // 2 situations to deal with:
                    // 1. Ratesheet, or subset of it has valid rates. => Use best par rate found to be within YSP.
                    // 2. Ratesheet is completely invalid. => Use the best par rate.
                    IRateItem parOptionForYspViolation = null;
                    IRateItem parOption = null;
                    bool bIsYspViolation = false;

                    // Considering min/maxYSP and SnapUp,
                    // find the "par" rate.
                    for (int i = fields.Length - 1; i >= 0; i--)
                    {
                        IRateItem currentOption = fields[i];

                        decimal ysp = currentOption.Point + adjustPoint;
                        bool bThisRateViolatesMaxYsp = false;

                        if (bSnapup)
                        {
                            ysp = Snapup(ysp, snapUpInterval);
                        }

                        if (ysp > minYsp || ysp < maxYsp)
                            bIsYspViolation = bThisRateViolatesMaxYsp = true;

                        // Setting the best par rate we see.
                        if ((null == parOptionForYspViolation)
                            || (parOptionForYspViolation.Point + adjustPoint > 0 &&
                                  (ysp <= 0 || ysp < parOptionForYspViolation.Point + adjustPoint)))
                        {
                            parOptionForYspViolation = currentOption;
                        }

                        if (bThisRateViolatesMaxYsp) continue;

                        // Setting the best par rate for non-violations.
                        if ((null == parOption)
                            || (parOption.Point + adjustPoint > 0 &&
                                  (ysp <= 0 || ysp < parOption.Point + adjustPoint)))
                        {
                            parOption = currentOption;
                        }
                    }


                    if (parOption != null)
                    {
                        // This means there was at least one valid rate,
                        // so return the closest to par that does not violate
                        return parOption;
                    }
                    else
                    {
                        // There exists a rate.
                        return parOptionForYspViolation;
                    }
                    //return keyOption;
                }
            }

            // If Non-LPE broker get here return null if no valid rate option found. Else throw exception
            if (!this.IsLpe)
                return null;
            else
                throw new CRateOptionNotFoundException(lLpTemplateId, lLpTemplateNm, sProdRLckdDays, sProdPpmtPenaltyMon, sIOnlyMon);


        }

        // This debug code will remove in near future.
        // Please synchronize this function with CPageBase.sRespaFeeItemListImpl
        private string GetJsonFeesLog()
        {
            using (PerformanceStopwatch.Start("GetJsonFeesLog"))
            {
                if (m_loanFileData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy)
                {
                    HashSet<E_LegacyGfeFieldT> skipList = new HashSet<E_LegacyGfeFieldT>() {
                        E_LegacyGfeFieldT.sLOrigF,
                        E_LegacyGfeFieldT.sGfeOriginatorCompF,
                        E_LegacyGfeFieldT.sGfeOriginatorComp_BorrowerPaid,
                        E_LegacyGfeFieldT.sGfeOriginatorCompF_Manual,
                        E_LegacyGfeFieldT.sLDiscnt,
                        E_LegacyGfeFieldT.sIPia,
                        E_LegacyGfeFieldT.sMipPia,
                        E_LegacyGfeFieldT.sUpfrontMipPia,
                        E_LegacyGfeFieldT.sVaFf,
                        E_LegacyGfeFieldT.sMInsRsrv,
                    };

                    List<RespaFeeItem> items = new List<RespaFeeItem>();
                    Func<BaseClosingCostFee, bool> filterAllFees = bfee => ClosingCostSetUtils.ValidDiscSecDescriptionFilter((BorrowerClosingCostFee)bfee) && !skipList.Contains(bfee.LegacyGfeFieldT);

                    List<BorrowerClosingCostFee> fees = new List<BorrowerClosingCostFee>();

                    foreach (BorrowerClosingCostFee fee in this.m_loanFileData.sClosingCostSet.GetFees(filterAllFees))
                    {
                        if (fee.TotalAmount != 0 || fee.BorrowerAmount != 0)
                            fees.Add(fee);
                    }

                    return ObsoleteSerializationHelper.JsonSerializeAndSanitize(fees);
                }

                return string.Empty;
            }
        }
        #endregion

        private List<CRateSheetGroup> m_rateSheetGroupList = null;
        private void ConstructRateSheetGroupList()
        {
            using (PerformanceStopwatch.Start("CLoanProductBase.ConstructRateSheetGroupList"))
            {
                m_rateSheetGroupList = new List<CRateSheetGroup>();



                CRateSheetGroup rateSheetGroup = null;
                int minProdRLckdDays = int.MinValue;
                int maxProdRLckdDays = int.MaxValue;
                int minProdPpmtPenaltyMon = int.MinValue;
                int maxProdPpmtPenaltyMon = int.MaxValue;
                int minIOnlyMon = int.MinValue;
                int maxIOnlyMon = int.MaxValue;
                bool bCreateNewGroup = true;

                foreach (var field in GetRawRateSheetWithDeltas())
                {
                    if (field.IsSpecialKeyword)
                    {
                        bCreateNewGroup = true;
                        if (field.RateSheetKeyword == E_RateSheetKeywordT.Lock)
                        {
                            minProdRLckdDays = field.LowerLimit;
                            maxProdRLckdDays = field.UpperLimit;
                        }
                        else if (field.RateSheetKeyword == E_RateSheetKeywordT.Prepay)
                        {
                            minProdPpmtPenaltyMon = field.LowerLimit;
                            maxProdPpmtPenaltyMon = field.UpperLimit;
                        }
                        else if (field.RateSheetKeyword == E_RateSheetKeywordT.IO)
                        {
                            minIOnlyMon = field.LowerLimit;
                            maxIOnlyMon = field.UpperLimit;
                        }

                        continue;
                    }

                    if (bCreateNewGroup)
                    {
                        rateSheetGroup = new CRateSheetGroup(minProdRLckdDays, maxProdRLckdDays, minProdPpmtPenaltyMon, maxProdPpmtPenaltyMon, minIOnlyMon, maxIOnlyMon);
                        m_rateSheetGroupList.Add(rateSheetGroup);
                        bCreateNewGroup = false;
                    }
                            rateSheetGroup.AddRate(field);

                }
            }


        }

        public IRateItem[] GetRateSheetWithDeltas(int sProdRLckdDays, int sProdPpmtPenaltyMon, int sIOnlyMon)
        {
            if (null == m_rateSheetGroupList)
                ConstructRateSheetGroupList();
            foreach (CRateSheetGroup o in m_rateSheetGroupList)
            {
                if (o.IsInRange(sProdRLckdDays, sProdPpmtPenaltyMon, sIOnlyMon))
                {
                    return o.RateSheetFields.ToArray<IRateItem>();
                }
            }

            throw new CRateOptionNotFoundException(lLpTemplateId, lLpTemplateNm, sProdRLckdDays, sProdPpmtPenaltyMon, sIOnlyMon);
        }
        /// <summary>
        /// Use internal admin developer page to test the return value of this method.
        /// </summary>
        /// <param name="orig"></param>
        /// <param name="interval">0.050 or 0.125 are typical interval</param>
        /// <returns></returns>
        public static decimal Snapup(decimal orig, decimal interval)
        {
            if (interval <= 0 || interval > 1)
                throw new CBaseException(ErrorMessages.Generic, "Snapup interval cannot be <=0 or > 1");

            decimal roundVal = decimal.Round(orig, RateDecimalDigits);
            if (interval == 1)
            {
                // OPM 130001. 07/22/2013 mf. We allow snapping up by 1.
                // Smallest integer greater than or equal to the fee.
                return Decimal.Ceiling(roundVal);
            }

            decimal intervalByThousand = 1000 * interval;

            return (intervalByThousand * Decimal.Floor(((roundVal + (interval - 0.001M)) * 1000M) / intervalByThousand)) / 1000M;
        }

        public int ExecutedSequence // this property is used for pricing debug log only. Potential remove it in near future.
        {
            get; set;
        }
    }

    public class CApplicantPriceOLD : CApplicantPrice
    {

        static public CApplicantPrice Create(CPageData loanFileData, ILoanProgramTemplate loanProgramTemplate,
            AbstractLoanProgramSet loanProgramSet,
            CLpRunOptions options,
            CacheInvariantPolicyResults cachResult)
        {
            return new CApplicantPriceOLD(loanFileData, loanProgramTemplate, loanProgramSet,
               options, cachResult);
        }

        private CLoanProductData m_loanProductData = null;
        /// <summary>
        /// Use the static method GetApplicantPrices instead
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="loanFileId"></param>
        /// <param name="productId"></param>
        /// <param name="bIsPriceEngineSubscribed"></param>
        /// <param name="parentHashtable"></param>
        private CApplicantPriceOLD(CPageData loanFileData, ILoanProgramTemplate loanProgramTemplate,
            AbstractLoanProgramSet loanProgramSet,
            CLpRunOptions options,
            CacheInvariantPolicyResults cachResult)
            : base(loanFileData, loanProgramTemplate, loanProgramSet, options)
        {
            m_loanProductData = (CLoanProductData)this.LoanProgramTemplate;

            #region Bypass Security For Pricing Objects
            // OPM 108042.  No need check field security for pricing run.
            // Bypass Loan File Field Security
            bool ByPassFieldSecurityCheckTmpLoanFile = m_loanFileData.ByPassFieldSecurityCheck;
            m_loanFileData.ByPassFieldSecurityCheck = true;
            m_loanProductData.ByPassFieldSecurityCheck = true;
            // Bypass each app's security
            List<bool> ByPassFieldSecurityCheckTmpApps = new List<bool>(m_loanFileData.nApps);
            for (int appIndex = 0; appIndex < m_loanFileData.nApps; appIndex++)
            {
                CAppData dataApp = m_loanFileData.GetAppData(appIndex);
                ByPassFieldSecurityCheckTmpApps.Insert(appIndex, dataApp.ByPassFieldSecurityCheck);
                dataApp.ByPassFieldSecurityCheck = true;
            }
            // Bypass Loan Program Field Security
            bool ByPassFieldSecurityCheckTmpProgram = m_loanProductData.ByPassFieldSecurityCheck;
            m_loanProductData.ByPassFieldSecurityCheck = true;
            // Bypass CC Template security
            bool ByPassFieldSecurityCheckTmpCC = false;
            if (m_loanFileData.LoanProductMatchingTemplate != null)
            {
                ByPassFieldSecurityCheckTmpCC = m_loanFileData.LoanProductMatchingTemplate.ByPassFieldSecurityCheck;
                m_loanFileData.LoanProductMatchingTemplate.ByPassFieldSecurityCheck = true;
            }
            #endregion

            try
            {
                ComputePrices(cachResult);
            }
            catch (InvalidCalculationException e)
            {
                Tools.LogError(e); // Doing as below loses the original exception's call stack.
                throw new InvalidCalculationException(e.UserMessage, loanProgramTemplate.lLpTemplateNm + " (" + loanProgramTemplate.lLpTemplateId + ") - " + e.ToString());
            }
            #region Restore Security for Pricing Objects
            m_loanFileData.ByPassFieldSecurityCheck = ByPassFieldSecurityCheckTmpLoanFile;
            for (int appIndex = 0; appIndex < m_loanFileData.nApps; appIndex++)
            {
                CAppData dataApp = m_loanFileData.GetAppData(appIndex);
                dataApp.ByPassFieldSecurityCheck = ByPassFieldSecurityCheckTmpApps[appIndex];
            }
            m_loanProductData.ByPassFieldSecurityCheck = ByPassFieldSecurityCheckTmpProgram;
            // Bypass CC Template security
            if (m_loanFileData.LoanProductMatchingTemplate != null)
            {
                m_loanFileData.LoanProductMatchingTemplate.ByPassFieldSecurityCheck = ByPassFieldSecurityCheckTmpCC;
            }
            #endregion
        }
    }
}