﻿namespace LendersOfficeApp.los.RatePrice
{
    using DataAccess;

    public sealed class RateMergeGroupKeyParameters
    {
        public string lTerm_rep { get; set; }
        public string lDue_rep { get; set; }
        public E_sFinMethT lFinMethT { get; set; }
        public string lBuydwnR1_rep { get; set; }
        public string lBuydwnR2_rep { get; set; }
        public string lBuydwnR3_rep { get; set; }
        public string lBuydwnR4_rep { get; set; }
        public string lBuydwnR5_rep { get; set; }
        public string lArmIndexNameVstr { get; set; } // 3/26/2010 dd - OPM 44173
        public string lLpProductType { get; set; }

        public string lRAdj1stCapMon_rep { get; set; }
        public string lRAdj1stCapR_rep { get; set; }  // 3/26/2010 dd - OPM 44173
        public string lRAdjCapR_rep { get; set; } // 3/26/2010 dd - OPM 44173
        public string lRAdjLifeCapR_rep { get; set; }// 3/26/2010 dd - OPM 44173
        public string lRAdjCapMon_rep { get; set; }
        public string lPrepmtPeriod_rep { get; set; }
        public string lHelocDraw_rep { get; set; } // 10/25/2013 dd - OPM 7776
        public string lHelocRepay_rep { get; set; } // 10/25/2013 dd - OPM 7776

        //Any time you add more you need to update PricingState class or you will break loan comparison.
    }
}
