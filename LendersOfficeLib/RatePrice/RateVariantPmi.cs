﻿namespace LendersOffice.RatePrice
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using Constants;
    using LendersOfficeApp.los.RatePrice;
    using Toolbox;

    /// <summary>
    /// This is a utility class for handling the operations related to rate-
    /// variant MI and the flow chart in case 471515.
    /// </summary>
    internal class RateVariantPmi
    {
        /// <summary>
        /// The count of the iteration loop that we are in.  In the flowchart,
        /// this represents the 'k' value.
        /// </summary>
        private int currentLoopIteration = 0;

        /// <summary>
        /// The run options from the associated run.
        /// </summary>
        private CLpRunOptions runOptions;

        /// <summary>
        /// True if an MI mismatch (delta) occured.
        /// </summary>
        private bool foundMismatch = false;

        /// <summary>
        /// The cached reserves value.
        /// </summary>
        private Dictionary<decimal, decimal?> reserveMonthsCache = new Dictionary<decimal, decimal?>();

        /// <summary>
        /// The collection of iteration states encountered.
        /// </summary>
        private List<RateVariantPricingState> iterationStates = new List<RateVariantPricingState>();

        /// <summary>
        /// Prevents a default instance of the <see cref="RateVariantPmi"/> class from being created.
        /// </summary>
        private RateVariantPmi()
        {
        }

        /// <summary>
        /// Gets the baseline MI program that should be initally applied in pricing 
        /// based on the current iteration state.
        /// </summary>
        public PmiResult ForcedMIProgram
        {
            get
            {
                if (this.IsFirstIteration)
                {
                    // We want a "natural" MI winner for the first iteration.
                    return null;
                }
                else
                {
                    // We want the MI program that caused the delta from
                    // the previous iteration.
                    return this.PreviousState.PmiDeltaResult;
                }
            }
        }

        /// <summary>
        /// Gets the per-rate adjust record that spans iterations.
        /// </summary>
        public PerOptionAdjustRecord PerOptionAdjust { get; } = new PerOptionAdjustRecord();

        /// <summary>
        /// Gets a value indicating whether we have reached the outer iteration limit.
        /// </summary>
        public bool IsMaxedOutIterations { get; private set; }

        /// <summary>
        /// Gets a value indicting whether this is the first iteration of the loop.
        /// </summary>
        /// <remarks>
        /// Purpose of this property is to make the logic more readable.
        /// </remarks>
        private bool IsFirstIteration => this.currentLoopIteration == 0;

        /// <summary>
        /// Gets a value indicting whether this is the last iteration of the loop.
        /// </summary>
        /// <remarks>
        /// Purpose of this property is to make the logic more readable.
        /// </remarks>
        private bool IsLastIteration => this.currentLoopIteration >= ConstStage.PerRatePricingIterationLimit;

        /// <summary>
        /// The previous iteration's pricing state.
        /// </summary>
        private RateVariantPricingState PreviousState
            => this.iterationStates[this.currentLoopIteration - 1];

        /// <summary>
        /// The current iteration's pricing state.
        /// </summary>
        private RateVariantPricingState CurrentState
            => this.iterationStates[this.currentLoopIteration];

        /// <summary>
        /// Gets the subcollection of iteration states that contributed rates to 
        /// the result.
        /// </summary>
        private IEnumerable<RateVariantPricingState> ContributingIterationStates
        {
            get
            {
                foreach (var state in this.iterationStates)
                {
                    if (state.RateOptions.Count != 0)
                    {
                        yield return state;
                    }
                }
            }
        }

        /// <summary>
        /// Create a new RateVariantPmi instance.
        /// </summary>
        /// <param name="runOptions">The run options for the pricing request.</param>
        /// <returns>The new RateVariantPmi object.</returns>
        public static RateVariantPmi Create(CLpRunOptions runOptions)
        {
            var rateVariantPmi = new RateVariantPmi();
            rateVariantPmi.runOptions = runOptions;
            return rateVariantPmi;
        }

        /// <summary>
        /// This is called upon completion of a delta-checking MI run.
        /// Determines if there was a difference in the MI result and if 
        /// there was, records the result data and returns true.
        /// </summary>
        /// <param name="rate">The current rate being checked.</param>
        /// <param name="currentResult">The current PMI result.</param>
        /// <param name="rateList">The current rate options.</param>
        /// <returns>True if the result changed, otherwise false.</returns>
        public bool ProcessMiEvaluation(decimal rate, PmiResult currentResult, ArrayList rateList)
        {
            // Check if MI result drifted from baseline.
            if (AreResultsDifferent(currentResult, this.CurrentState.PmiBaselineResult))
            {
                // MI result drifted from the baseline at this rate.
                this.CurrentState.ProcessMiChange(rate, currentResult, this.TrimPastRate(rateList, rate));
                this.foundMismatch = true;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Called after baseline PMI result to lock in the new state.
        /// </summary>
        /// <param name="result">The pmi result.</param>
        public void SetBaselineMiResult(PmiResult result)
        {
            // Iteration has started.  Store it.
            var thisIterationState = RateVariantPricingState.Create(this.currentLoopIteration, result);
            this.iterationStates.Add(thisIterationState);
        }

        /// <summary>
        /// Call after per-rate pricing is complete in preperation of
        /// running per-rate MI. Updates the current state and returns
        /// the applicable rates.
        /// </summary>
        /// <param name="rateOptions">The current rate options.</param>
        /// <param name="adjDescsDisclosed">The disclosed adjustments.</param>
        /// <param name="adjDescsHidden">The hidden adjustments.</param>
        /// <param name="denialReasons">The denial reasons.</param>
        /// <returns>The applicable rate options.</returns>
        public ArrayList GetRatesToProcess(
            ArrayList rateOptions,
            List<CAdjustItem> adjDescsDisclosed,
            List<CAdjustItem> adjDescsHidden,
            List<CStipulation> denialReasons)
        {
            this.CurrentState.SetProgramResultData(adjDescsDisclosed, adjDescsHidden, denialReasons);

            if (this.IsFirstIteration)
            {
                // First iteration is all rates.
                this.CurrentState.RateOptions = rateOptions;
                return rateOptions;
            }

            // On a subsequent iteration, we only care about the rate options
            // greater than or equal to the previous triggering rate.
            var lastTriggeringRate = this.PreviousState.TriggeringRate;

            // Building a list starting with the last triggering rate ascending.
            ArrayList newList = new ArrayList();

            for (int i = rateOptions.Count - 1; i >= 0; i--)
            {
                var option = (CApplicantRateOption)rateOptions[i];
                if (option.Rate >= lastTriggeringRate)
                {
                    newList.Add(option);
                }
                else
                {
                    // Anything that applied to this options
                    // should not be kept.
                    this.PerOptionAdjust.RemoveOption(option, maxYspFiltered: false);
                }
            }

            // Return back to descending order.
            newList.Reverse();
            this.CurrentState.RateOptions = newList;
            return newList;
        }

        /// <summary>
        /// Called when the iteration is complete. Set up the main pricing
        /// result parts, considering all the iterations.
        /// </summary>
        /// <param name="rateOptions">The current rate options.</param>
        /// <param name="reasonArray">The current denial reasons.</param>
        /// <param name="adjDescsDisclosed">The current adjustment list.</param>
        /// <param name="adjDescsHidden">The hidden adjustment list.</param>
        /// <param name="evalStatus">The current eval status.</param>
        /// <returns>The final rate options.</returns>
        public ArrayList ProcessEndOfIteration(
            ArrayList rateOptions,
            ref List<CStipulation> reasonArray,
            ref List<CAdjustItem> adjDescsDisclosed,
            ref List<CAdjustItem> adjDescsHidden,
            ref E_EvalStatus evalStatus)
        {
            if (!this.foundMismatch)
            {
                if (this.IsFirstIteration)
                {
                    // No-op.  There will be no iterations.
                    return rateOptions;
                }
                else
                {
                    // The result has converged.
                    return this.ProcessFinalResult(
                        rateOptions,
                        ref reasonArray,
                        ref adjDescsDisclosed,
                        ref adjDescsHidden,
                        ref evalStatus);
                }
            }
            else
            {
                // There is a difference found.  
                // Either we done, or going another round.
                if (this.IsLastIteration)
                {
                    // We will terminate due to lack of convergence.
                    // Build a reasonable result and add stip.
                    return this.ProcessFinalResult(
                        rateOptions,
                        ref reasonArray,
                        ref adjDescsDisclosed,
                        ref adjDescsHidden,
                        ref evalStatus);
                }
                else
                {
                    // We will be going for another round.
                    return rateOptions;
                }
            }
        }

        /// <summary>
        /// Build the final result using all the iterations.
        /// </summary>
        /// <param name="rateOptions">The current rate options.</param>
        /// <param name="reasonArray">The current denial reasons.</param>
        /// <param name="adjDescsDisclosed">The current adjustment list.</param>
        /// <param name="adjDescsHidden">The hidden adjustment list.</param>
        /// <param name="evalStatus">The current eval status.</param>
        /// <returns>The final rate options.</returns>
        public ArrayList ProcessFinalResult(ArrayList rateOptions, ref List<CStipulation> reasonArray, ref List<CAdjustItem> adjDescsDisclosed, ref List<CAdjustItem> adjDescsHidden, ref E_EvalStatus evalStatus)
        {
            // At this point we have a list of iterations with different results in them,
            // Merge the adjustments, rates, and disquals into a final result.
            this.MergeAdjustments(ref adjDescsDisclosed, ref adjDescsHidden);

            bool isDisqual;
            this.MergeDisquals(ref reasonArray, out isDisqual);
            evalStatus = isDisqual ? E_EvalStatus.Eval_Ineligible : E_EvalStatus.Eval_Eligible;

            var finalOptions = this.BuildResultRateOptions();
            this.ProcessSelectedRate(finalOptions, reasonArray, ref evalStatus);

            this.PerOptionAdjust.FinalizeAdjustmentList(adjDescsDisclosed, adjDescsHidden, finalOptions);

            return finalOptions;
        }

        /// <summary>
        /// Get the PMI result for the chosen rate.
        /// </summary>
        /// <param name="rate">The rate whose value is needed.</param>
        /// <returns>The pmi result.  Null if there is no result.</returns>
        public PmiResult GetPmiOfRate(decimal rate)
        {
            // Get the Pmi result for a specific rate.
            foreach (var iteration in this.ContributingIterationStates)
            {
                foreach (CApplicantRateOption option in iteration.RateOptions)
                {
                    if (option.Rate == rate)
                    {
                        return iteration.PmiBaselineResult;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Store reserves for this rate to be used later.
        /// </summary>
        /// <param name="rate">The rate whose value is needed.</param>
        /// <param name="reservesMonths">The reserves months.</param>
        public void SetReserves(decimal rate, decimal? reservesMonths)
        {
            this.reserveMonthsCache[rate] = reservesMonths;
        }

        /// <summary>
        /// Get the reserves months for the rate.
        /// </summary>
        /// <param name="rate">The rate whose value is needed.</param>
        /// <returns>The appropriate reserves qual.</returns>
        public decimal? GetReserves(decimal rate)
        {
            return this.reserveMonthsCache[rate];
        }

        /// <summary>
        /// Deterimine if we should keep going based on the current iterations
        /// and if pricing hass converged or not.
        /// </summary>
        /// <returns>True if we should continue, otherwise false.</returns>
        public bool ShouldContinue()
        {
            var outerIterationMax = ConstStage.PerRatePricingIterationLimit; 
            this.IsMaxedOutIterations = ++this.currentLoopIteration > outerIterationMax;

            if (this.foundMismatch && !this.IsMaxedOutIterations)
            {
                this.foundMismatch = false;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Determine if the mi results are different.
        /// </summary>
        /// <param name="firstResult">The first result.</param>
        /// <param name="secondResult">The second result.</param>
        /// <returns>True if the result is different, otherwise false.</returns>
        private static bool AreResultsDifferent(PmiResult firstResult, PmiResult secondResult)
        {
            // If we went from no MI to MI or vice-versa
            // it is a change in the MI.
            var isPreviousWinner = firstResult != null && !firstResult.IsDenied;
            var isCurrentWinner = secondResult != null && !secondResult.IsDenied;
            if (!isPreviousWinner || !isCurrentWinner)
            {
                // Null -> Null is no change.
                // Null -> Result is a change
                // Result -> Null is a change.
                return isPreviousWinner != isCurrentWinner;
            }

            // This is different than the IComparable implementation of PmiResult.
            // That one only cares about the premium values for rank comparison, not the provider.
            // Provider has meaning here.
            return firstResult.PmiCompany != secondResult.PmiCompany
                || firstResult.CompareTo(secondResult) != 0;
        }

        /// <summary>
        /// Check if adjustments match.
        /// </summary>
        /// <param name="firstAdjustment">The first adjustment.</param>
        /// <param name="secondAdjustment">The second adjustment.</param>
        /// <returns>True if the adjustments match, otherwise false.</returns>
        private static bool DoAdjustmentsMatch(CAdjustItem firstAdjustment, CAdjustItem secondAdjustment)
        {
            return firstAdjustment.Description == secondAdjustment.Description
                && firstAdjustment.Fee == secondAdjustment.Fee
                && firstAdjustment.DebugString == secondAdjustment.DebugString;
        }

        /// <summary>
        /// Check if the stipulations match.
        /// </summary>
        /// <param name="firstStipulation">The first stipulation.</param>
        /// <param name="secondStipulation">The second stipulation.</param>
        /// <returns>True if the stipulations match, otherwise false.</returns>
        private static bool DoDenialsMatch(CStipulation firstStipulation, CStipulation secondStipulation)
        {
            return firstStipulation.Description == secondStipulation.Description;
        }

        /// <summary>
        /// Add an adjustment to the list.
        /// </summary>
        /// <param name="adjustmentList">The adjustment list.</param>
        /// <param name="newAdjustment">The new adjustment.</param>
        private static void AddAdjustment(List<CAdjustItem> adjustmentList, CAdjustItem newAdjustment)
        {
            if (!adjustmentList.Any(adj => DoAdjustmentsMatch(newAdjustment, adj)))
            {
                adjustmentList.Add(newAdjustment);
            }
        }

        /// <summary>
        /// Add a denial to the list.
        /// </summary>
        /// <param name="denialList">The list to add to.</param>
        /// <param name="newDenial">The new denial.</param>
        private static void AddDenial(List<CStipulation> denialList, CStipulation newDenial)
        {
            if (!denialList.Any(denial => DoDenialsMatch(newDenial, denial)))
            {
                denialList.Add(newDenial);
            }
        }

        /// <summary>
        /// Build an rate option list that consists only of the applicable
        /// rates beyond the rate.
        /// </summary>
        /// <param name="rateOptions">Rate options based on.</param>
        /// <param name="rate">The rate rate to ascend from.</param>
        /// <returns>The final options list.</returns>
        private ArrayList TrimPastRate(ArrayList rateOptions, decimal rate)
        {
            // Building a list starting with the last triggering rate ascending.
            ArrayList newList = new ArrayList();

            for (int i = rateOptions.Count - 1; i >= 0; i--)
            {
                var option = (CApplicantRateOption)rateOptions[i];
                if (option.Rate < rate)
                {
                    newList.Add(option);
                }
                else
                {
                    // Since this option is not carrying over to final result,
                    // per-rate manager needs to not track it.
                    this.PerOptionAdjust.RemoveOption(option, maxYspFiltered: false);
                }
            }

            // Return back to descending order.
            newList.Reverse();
            return newList;
        }

        /// <summary>
        /// Get the pricing state corresponding to the iteration.
        /// </summary>
        /// <param name="iteration">The iteration number.</param>
        /// <returns>The pricing state.</returns>
        private RateVariantPricingState GetStateByIterationNumber(int iteration)
        {
            return this.iterationStates.FirstOrDefault(i => i.IterationNumber == iteration);
        }

        /// <summary>
        /// Build the result rate options from the final list.
        /// </summary>
        /// <returns>The resulting rate options.</returns>
        private ArrayList BuildResultRateOptions()
        {
            // Build a list of rate options from each iteration.
            ArrayList newResult = new ArrayList();

            // Rates need to descend, so we start at the end.
            foreach (var iteration in this.ContributingIterationStates.Reverse())
            {
                foreach (var option in iteration.RateOptions)
                {
                    newResult.Add(option);
                }
            }

            return newResult;
        }

        /// <summary>
        /// Merge the disquals across all iterations to correctly show in the final result.
        /// </summary>
        /// <param name="reasonArray">Current disqual reason list.</param>
        /// <param name="isProgramDisqual">True if there is a program-level disqual.</param>
        private void MergeDisquals(ref List<CStipulation> reasonArray, out bool isProgramDisqual)
        {
            var programDenials = new List<CStipulation>();
            isProgramDisqual = true;

            foreach (var iteration in this.ContributingIterationStates)
            {
                if (iteration.DenialReasons.Count() == 0)
                {
                    // There is an iteration that is eligible.
                    // We do not care about its per-rate disquals.
                    // That is handled at higher level.
                    isProgramDisqual = false;
                }
            }

            // If an iteration's program-level disqual rule only affected a subset of
            // iterations, make the disqual per-rate for the options of those iterations.
            HashSet<CStipulation> downGradedDenials = new HashSet<CStipulation>();
            foreach (var iteration in this.ContributingIterationStates)
            {
                foreach (var denial in iteration.DenialReasons)
                {
                    List<int> iterationsUsingThisDenial = new List<int>();
                    iterationsUsingThisDenial.Add(iteration.IterationNumber);

                    foreach (var otherIteration in this.ContributingIterationStates.Where(i => i.IterationNumber != iteration.IterationNumber))
                    {
                        if (otherIteration.DenialReasons.Exists(p => DoDenialsMatch(p, denial)))
                        {
                            // This additional iteration uses this same denial reason.
                            iterationsUsingThisDenial.Add(otherIteration.IterationNumber);
                        }
                    }

                    if (this.ContributingIterationStates.Count() == iterationsUsingThisDenial.Count)
                    {
                        // This denial occurs in ALL iterations.  Leave it as a denial for all.
                        AddDenial(programDenials, denial);
                    }
                    else
                    {
                        if (!downGradedDenials.Any(d => DoDenialsMatch(d, denial)))
                        {
                            // This denial occurs in a subset of iterations.  
                            // Each should take it as per-rate for its rates.
                            foreach (var interationIndex in iterationsUsingThisDenial)
                            {
                                var currentIteration = this.GetStateByIterationNumber(interationIndex);

                                foreach (CApplicantRateOption option in currentIteration.RateOptions)
                                {
                                    option.DisqualReason += (option.DisqualReason.Length == 0 ? string.Empty : "<br /> ") + "* " + denial.Description;
                                    option.DisqualReasonDebug += (option.DisqualReasonDebug.Length == 0 ? string.Empty : "<br /> ") + "* " + denial.Description + denial.DebugString;
                                }
                            }

                            downGradedDenials.Add(denial);
                        }
                    }
                }
            }

            reasonArray = programDenials;
        }

        /// <summary>
        /// Process encountering the selected rate.
        /// </summary>
        /// <param name="finalOptions">The rate options.</param>
        /// <param name="reasonArray">The disqual reasons.</param>
        /// <param name="evalStatus">The eval status.</param>
        private void ProcessSelectedRate(ArrayList finalOptions, List<CStipulation> reasonArray, ref E_EvalStatus evalStatus)
        {
            var selectedRate = this.runOptions.SelectedRate;
            var selectedRateDisqualReason = string.Empty;
            if (selectedRate > 0)
            {
                // Per rate denial on a selected rate needs to be upgraded
                // to a program-level denial even if there are other valid rates.
                foreach (CApplicantRateOption option in finalOptions)
                {
                    if (option.Rate == selectedRate && option.IsDisqualified)
                    {
                        if (this.runOptions.LpeTaskT != DistributeUnderwriting.E_LpeTaskT.RunEngine)
                        {
                            evalStatus = E_EvalStatus.Eval_Ineligible;
                            selectedRateDisqualReason = option.DisqualReason;
                        }
                    }
                }

                // This is not an ideal way of doing this, but because rate options currently
                // store denials as strings, we have to manually break them out here.
                if (!string.IsNullOrEmpty(selectedRateDisqualReason))
                {
                    var disquals = selectedRateDisqualReason.Split('*');

                    var prepend = string.Empty;
                    for (int i = 0; i < disquals.Count(); i++)
                    {
                        var disqual = disquals[i];

                        if (string.IsNullOrEmpty(disqual))
                        {
                            continue;
                        }

                        if (disqual.Trim().StartsWith("PMI NOT ELIGIBLE."))
                        {
                            prepend = disqual;
                            continue;
                        }

                        reasonArray.Add(new CStipulation(prepend + disqual, string.Empty));
                        prepend = string.Empty;
                    }
                }
            }
        }

        /// <summary>
        /// Merge the adjustments across all iterations to correctly show in the final result.
        /// </summary>
        /// <param name="adjDescsDisclosed">Visible adjustments.</param>
        /// <param name="adjDescsHidden">Hidden adjustments.</param>
        private void MergeAdjustments(ref List<CAdjustItem> adjDescsDisclosed, ref List<CAdjustItem> adjDescsHidden)
        {
            this.MergeAdjustments(ref adjDescsDisclosed, true);
            this.MergeAdjustments(ref adjDescsHidden, false);
        }

        /// <summary>
        /// Merge the adjustments across all iterations to correctly show in the final result.
        /// </summary>
        /// <param name="adjustmentList">The adjustments.</param>
        /// <param name="isForVisible">Whether they are hidden or visible.</param>
        private void MergeAdjustments(ref List<CAdjustItem> adjustmentList, bool isForVisible)
        {
            var newAdjustmentList = new List<CAdjustItem>();

            // Adjustments that point to a valid per-rate need to appear.
            foreach (var iteration in this.iterationStates)
            {
                foreach (var adjustment in isForVisible ? iteration.VisibleAdjustments : iteration.HiddenAdjustments)
                {
                    if (this.PerOptionAdjust.IsAdjustmentReferencedByOption(adjustment))
                    {
                        AddAdjustment(newAdjustmentList, adjustment);
                    }
                }
            }

            // Any adjustment that applies to a subset of iterations needs to 
            // be a per-rate for all of them that it applies to.
            // Otherwise, let it through as a standard adjustment.
            foreach (var iteration in this.ContributingIterationStates)
            {
                foreach (var adjustment in isForVisible ? iteration.VisibleAdjustments : iteration.HiddenAdjustments)
                {
                    if (!isForVisible && adjustment.Description == string.Empty && adjustment.Fee == string.Empty && adjustment.Rate == string.Empty && adjustment.DebugString != string.Empty)
                    {
                        // Special debug hidden adjustment. Should not become per-rate.
                        AddAdjustment(newAdjustmentList, adjustment);
                        continue;
                    }

                    if (this.PerOptionAdjust.IsAdjustmentReferencedByOption(adjustment))
                    {
                        // This one is per rate already.  Not per-program.
                        AddAdjustment(newAdjustmentList, adjustment);
                        continue;
                    }

                    List<int> iterationsUsingThisAdjustment = new List<int>();
                    iterationsUsingThisAdjustment.Add(iteration.IterationNumber);

                    foreach (var otherIteration in this.ContributingIterationStates.Where(i => i.IterationNumber != iteration.IterationNumber))
                    {
                        if ((isForVisible ? otherIteration.VisibleAdjustments : otherIteration.HiddenAdjustments).Exists(p => DoAdjustmentsMatch(p, adjustment)))
                        {
                            // This additional iteration uses this same adjustment.
                            iterationsUsingThisAdjustment.Add(otherIteration.IterationNumber);
                        }
                    }

                    AddAdjustment(newAdjustmentList, adjustment);
                    if (this.ContributingIterationStates.Count() == iterationsUsingThisAdjustment.Count)
                    {
                        // This adjustment occurs in ALL iterations.  Leave it as a global adjustment.
                    }
                    else
                    {
                        // This adjustment only occurs in a subset of iterations.
                        // It will need to be a per-rate adjustment for the rates
                        // of the iterations it applies to.
                        var adjustId = Guid.NewGuid();
                        foreach (var interationIndex in iterationsUsingThisAdjustment)
                        {
                            var currentIteration = this.GetStateByIterationNumber(interationIndex);
                            foreach (CApplicantRateOption option in currentIteration.RateOptions)
                            {
                                this.PerOptionAdjust.AddAdjustment(adjustment, option, true, new List<CAdjustItem>(), adjustId);
                            }
                        }
                    }
                }
            }

            adjustmentList = newAdjustmentList;
        }

        /// <summary>
        /// Return a simple debug string that will indicate the important data about each iteration.
        /// </summary>
        /// <remarks>
        /// This should only be used for debuging purposes.
        /// </remarks>
        /// <returns>String indicating the results of each iteration. It is not user friendly data.</returns>
        private string GetDebugStatus()
        {
            var result = new System.Text.StringBuilder();

            foreach (var pricingState in this.iterationStates)
            {
                result.AppendLine($"Iteration: {pricingState.IterationNumber}");
                result.AppendLine($"Delta Rate: {pricingState.TriggeringRate}");

                var pmiStr = string.Empty + Environment.NewLine;
                if (pricingState.PmiBaselineResult != null)
                {
                    pmiStr += "BaseLine: " + pricingState.PmiBaselineResult.PmiCompany.ToString() + Environment.NewLine;
                    pmiStr += "Monthly: " + pricingState.PmiBaselineResult.MonthlyPct.ToString() + Environment.NewLine;
                    pmiStr += "Single: " + pricingState.PmiBaselineResult.SinglePct.ToString() + Environment.NewLine;
                    pmiStr += "Denied: " + pricingState.PmiBaselineResult.IsDenied.ToString() + Environment.NewLine;
                    pmiStr += Environment.NewLine;
                }
                else
                {
                    pmiStr += "null" + Environment.NewLine;
                }

                if (pricingState.PmiDeltaResult != null)
                {
                    pmiStr += "Delta: " + pricingState.PmiDeltaResult.PmiCompany.ToString() + Environment.NewLine;
                    pmiStr += "Monthly: " + pricingState.PmiDeltaResult.MonthlyPct.ToString() + Environment.NewLine;
                    pmiStr += "Single: " + pricingState.PmiDeltaResult.SinglePct.ToString() + Environment.NewLine;
                    pmiStr += "Denied: " + pricingState.PmiDeltaResult.IsDenied.ToString() + Environment.NewLine;
                }
                else
                {
                    pmiStr += "null" + Environment.NewLine;
                }

                result.AppendLine($"PMI: {pmiStr}");
                result.AppendLine("Options:");
                foreach (CApplicantRateOption rate in pricingState.RateOptions)
                {
                    result.AppendLine($"Rate: {rate.Rate_rep}, {rate.Point_rep}, DTI:{rate.BottomRatio_rep}, RESERVES:{rate.Reserves}, DQ:{rate.DisqualReason}, {rate.PerOptionAdjStr}.");
                }

                result.AppendLine("Visible Adjustments:");
                foreach (var adj in pricingState.VisibleAdjustments)
                {
                    result.AppendLine($"Adj: {adj.Description}, {adj.Fee}, {adj.OptionCode}. DEBUG: {adj.DebugString}");
                }

                result.AppendLine("Hidden Adjustments:");
                foreach (var adj in pricingState.HiddenAdjustments)
                {
                    result.AppendLine($"Adj: {adj.Description}, {adj.Fee}, {adj.OptionCode}. DEBUG: {adj.DebugString}");
                }

                result.AppendLine("Disqual:");
                foreach (var denial in pricingState.DenialReasons)
                {
                    result.AppendLine($"Denial: {denial.Description}, {denial.DebugString}.");
                }

                result.AppendLine();
                result.AppendLine();
            }

            return result.ToString();
        }

        /// <summary>
        /// POD class containg a pricing state representing one iteration of the engine.
        /// </summary>
        private class RateVariantPricingState
        {
            /// <summary>
            /// Prevents a default instance of the <see cref="RateVariantPricingState"/> class from being created.
            /// </summary>
            private RateVariantPricingState()
            {
            }

            /// <summary>
            /// Gets the iteration number represented by this iteration.
            /// </summary>
            public int IterationNumber { get; private set; }

            /// <summary>
            /// Gets or sets the rate options associated with this iteration.
            /// </summary>
            public ArrayList RateOptions { get; set; }

            /// <summary>
            /// Gets or sets the triggering rate at which the pricing changed.
            /// </summary>
            public decimal TriggeringRate { get; set; } = -1;

            /// <summary>
            /// Gets the baseline pmi result from the inital run.
            /// </summary>
            public PmiResult PmiBaselineResult { get; private set; }

            /// <summary>
            /// Gets the Pmi result that produced the change.
            /// </summary>
            public PmiResult PmiDeltaResult { get; private set; }

            /// <summary>
            /// Gets the visible adjustments in this result.
            /// </summary>
            public List<CAdjustItem> VisibleAdjustments { get; private set; }

            /// <summary>
            /// Gets the hidden adjustments in this result.
            /// </summary>
            public List<CAdjustItem> HiddenAdjustments { get; private set; }

            /// <summary>
            /// Gets the denial reasons for this iteration.
            /// </summary>
            public List<CStipulation> DenialReasons { get; private set; }

            /// <summary>
            /// Creates a pricing state.
            /// </summary>
            /// <param name="iterationNumber">The iteration number.</param>
            /// <param name="pmiBaselineResult">The baseline PMI result that started this iteration.</param>
            /// <returns>The new pricing state.</returns>
            public static RateVariantPricingState Create(
                int iterationNumber,
                PmiResult pmiBaselineResult)
            {
                var newState = new RateVariantPricingState();
                newState.IterationNumber = iterationNumber;
                newState.PmiBaselineResult = pmiBaselineResult;
                return newState;
            }

            /// <summary>
            /// Accept the values for changed MI result.
            /// </summary>
            /// <param name="rate">Rate of the change.</param>
            /// <param name="currentResult">The Pmi result that caused the change.</param>
            /// <param name="rateOptions">The rate options.</param>
            public void ProcessMiChange(decimal rate, PmiResult currentResult, ArrayList rateOptions)
            {
                this.PmiDeltaResult = currentResult;
                this.TriggeringRate = rate;
                this.RateOptions = rateOptions;
            }

            /// <summary>
            /// Set the program result data from pricing for this iteration.
            /// </summary>
            /// <param name="adjDescsDisclosed">Visible Adjustments.</param>
            /// <param name="adjDescsHidden">Hidden Adjustments.</param>
            /// <param name="denialReasons">Denial reasons.</param>
            public void SetProgramResultData(List<CAdjustItem> adjDescsDisclosed, List<CAdjustItem> adjDescsHidden, List<CStipulation> denialReasons)
            {
                this.VisibleAdjustments = adjDescsDisclosed;
                this.HiddenAdjustments = adjDescsHidden;
                this.DenialReasons = denialReasons;
            }
        }
    }
}