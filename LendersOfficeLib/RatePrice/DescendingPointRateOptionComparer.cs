﻿namespace LendersOfficeApp.los.RatePrice
{
    using System.Collections.Generic;

    public class DescendingPointRateOptionComparer : IComparer<CApplicantRateOption>
    {
        public int Compare(CApplicantRateOption x, CApplicantRateOption y)
        {
            if (null == x || null == y)
                throw new DataAccess.CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Unable to compare two CApplicantRateOptions");

            return y.Point.CompareTo(x.Point);
        }
    }
}
