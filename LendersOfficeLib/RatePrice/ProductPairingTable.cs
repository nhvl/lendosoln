/// Author: David Dao
namespace LendersOfficeApp.los.RatePrice
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.RatePrice.FileBasedPricing;

    public class ProductPairingTable
	{
        private class ProductPairingItem
        {
            private string m_investorName = "";
            private string m_productCode = "";

            public ProductPairingItem(string investorName, string productCode)
            {
                if (null != investorName)
                    m_investorName = investorName.ToUpper();

                if (null != productCode)
                    m_productCode = productCode.ToUpper();
            }
            public override bool Equals(object o)
            {
                ProductPairingItem _obj = o as ProductPairingItem;

                if (_obj != null)
                {
                    return _obj.m_investorName == this.m_investorName && _obj.m_productCode == this.m_productCode;
                }
                return false;
            }
            public override int GetHashCode()
            {
                return m_investorName.GetHashCode() ^ m_productCode.GetHashCode();
            }

        }

        private class ProductPairingItemList
        {
            private bool m_isIncludeList = false;
            private List<ProductPairingItem> m_list = new List<ProductPairingItem>();

            public ProductPairingItemList(bool isIncludeList)
            {
                m_isIncludeList = isIncludeList;
            }

            public void Add(ProductPairingItem item)
            {
                m_list.Add(item);
            }

            public bool IsAllow(ProductPairingItem item)
            {
                if (m_isIncludeList)
                {
                    // 7/19/2007 dd - Only return true if item is explicitly define in the list. Otherwise return false
                    foreach (ProductPairingItem o in m_list)
                    {
                        if (o.Equals(item))
                        {
                            return true;
                        }
                    }
                    return false;
                }
                else
                {
                    // 7/19/2007 dd - Return false if the item define in the list. Otherwise return true.
                    foreach (ProductPairingItem o in m_list)
                    {
                        if (o.Equals(item))
                        {
                            return false;
                        }

                    }
                    return true;
                }
            }
        }
        private Hashtable m_hashtable = null;

        private ProductPairingTable(Hashtable hashtable) 
        {
            m_hashtable = hashtable;
        }
        public bool IsAllowPairing(string investorName1stLien, string productCode1stLien, string investorName2ndLien, string productCode2ndLien) 
        {
            if (null != m_hashtable) 
            {
                ProductPairingItem firstLienItem = new ProductPairingItem(investorName1stLien, productCode1stLien);
                ProductPairingItem secondLienItem = new ProductPairingItem(investorName2ndLien, productCode2ndLien);

                if (m_hashtable.Contains(secondLienItem)) 
                {
                    ProductPairingItemList list = (ProductPairingItemList) m_hashtable[secondLienItem];
                    return list.IsAllow(firstLienItem);
                }
                return true; // There are no rule for this 2nd item then return true.
            }
            return true; // Default to allow

        }


        private static object s_lock = new object();
        private static DateTime s_dataLastModifiedD = DateTime.MinValue;
        private static ProductPairingTable s_cacheHashtable = null;

        private static IEnumerable<Tuple<ProductPairingItem, ProductPairingItem, string>> RetrieveFromDatabase(DataSrc dataSrc)
        {
            List<Tuple<ProductPairingItem, ProductPairingItem, string>> list = new List<Tuple<ProductPairingItem, ProductPairingItem, string>>();

            using (var reader = StoredProcedureHelper.ExecuteReader(dataSrc, "LoadProductPairingTable"))
            {
                while (reader.Read())
                {
                    ProductPairingItem firstLienItem = new ProductPairingItem((string)reader["InvestorName1stLien"], (string)reader["ProductCode1stLien"]);
                    ProductPairingItem secondLienItem = new ProductPairingItem((string)reader["InvestorName2ndLien"], (string)reader["ProductCode2ndLien"]);
                    string pairingModeFor2ndLienProduct = (string)reader["PairingModeFor2ndLienProduct"];

                    list.Add(new Tuple<ProductPairingItem, ProductPairingItem, string>(firstLienItem, secondLienItem, pairingModeFor2ndLienProduct));
                }
            }

            return list;
        }

        private static IEnumerable<Tuple<ProductPairingItem, ProductPairingItem, string>> RetrieveFromSnapshot(FileBasedSnapshot snapshot)
        {
            // Simulate the similar logic of the stored procedure: LoadProductPairingTable
            //    SELECT InvestorName1stLien, ProductCode1stLien,InvestorName2ndLien, ProductCode2ndLien, PairingModeFor2ndLienProduct
            //    FROM Product_Pairing pp JOIN Product p ON pp.InvestorName2ndLien = p.InvestorName AND pp.ProductCode2ndLien = p.ProductCode 
            //                                              AND p.PairingModeFor2ndLienProduct != ''

            Dictionary<ProductPairingItem, string> pairingModeDictionary = new Dictionary<ProductPairingItem, string>();

            foreach (var product in snapshot.GetProductList())
            {
                if (!string.IsNullOrEmpty(product.PairingModeFor2ndLienProduct))
                {
                    ProductPairingItem item = new ProductPairingItem(product.InvestorName, product.ProductCode);
                    pairingModeDictionary.Add(item, product.PairingModeFor2ndLienProduct);
                }
            }

            List<Tuple<ProductPairingItem, ProductPairingItem, string>> list = new List<Tuple<ProductPairingItem, ProductPairingItem, string>>();

            foreach (var item in snapshot.GetProductPairingList())
            {
                ProductPairingItem firstLienItem = new ProductPairingItem(item.InvestorName1stLien, item.ProductCode1stLien);
                ProductPairingItem secondLienItem = new ProductPairingItem(item.InvestorName2ndLien, item.ProductCode2ndLien);

                string pairingMode = null;

                if (pairingModeDictionary.TryGetValue(secondLienItem, out pairingMode))
                {
                    list.Add(new Tuple<ProductPairingItem, ProductPairingItem, string>(firstLienItem, secondLienItem, pairingMode));
                }
            }

            return list;
        }

        private static ProductPairingTable RetrieveProductPairingTableImpl(IEnumerable<Tuple<ProductPairingItem, ProductPairingItem, string>> productPairingTable)
        {
            Hashtable hashtable = new Hashtable();

            foreach (var o in productPairingTable)
            {
                ProductPairingItem firstLienItem = o.Item1;
                ProductPairingItem secondLienItem = o.Item2;
                string pairingModeFor2ndLienProduct = o.Item3;
                pairingModeFor2ndLienProduct = pairingModeFor2ndLienProduct.ToLower();

                ProductPairingItemList list = null;

                if (hashtable.Contains(secondLienItem))
                {
                    list = (ProductPairingItemList)hashtable[secondLienItem];
                }
                else
                {
                    bool isInclude = true;
                    if (pairingModeFor2ndLienProduct == "exclude")
                        isInclude = false;
                    else if (pairingModeFor2ndLienProduct != "include")
                    {
                        Tools.LogBug("Undefined PairingModeFor2ndLienProduct=" + pairingModeFor2ndLienProduct + ". Default to 'Include'.");
                    }
                    list = new ProductPairingItemList(isInclude);
                    hashtable.Add(secondLienItem, list);
                }
                list.Add(firstLienItem);

            }

            return new ProductPairingTable(hashtable);
        }

        public static ProductPairingTable RetrieveProductPairingTableFromSnapshot(FileBasedSnapshot snapshot)
        {
            var productPairingTable = RetrieveFromSnapshot(snapshot);
            return RetrieveProductPairingTableImpl(productPairingTable);
        }

        public static ProductPairingTable RetrieveProductPairingTable() 
        {
            lock (s_lock) 
            {
                CLpeReleaseInfo releaseInfo = CLpeReleaseInfo.GetLatestReleaseInfo();

                // 7/19/2007 dd - Cache the hashtable until we switch snapshot.

                if (s_dataLastModifiedD != releaseInfo.DataLastModifiedD || null == s_cacheHashtable) 
                {
                    s_dataLastModifiedD = releaseInfo.DataLastModifiedD;
                    var productPairingTable = RetrieveFromDatabase(releaseInfo.DataSrcForSnapshot);

                    s_cacheHashtable = RetrieveProductPairingTableImpl(productPairingTable);
                }

                return s_cacheHashtable;
            }
        }
	}
}