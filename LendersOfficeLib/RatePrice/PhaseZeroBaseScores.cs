﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace LendersOfficeApp.los.RatePrice
{
    public struct PhaseZeroBaseScores
    {
        public decimal qcltv, qltv, qlamt, qhcltv;
        public decimal lockDaysAdj;
        public int qscore;

        public void ReadFrom(CPageData loanFileData)
        {
            try
            {
                qcltv = loanFileData.sCltvR;
            }
            catch { qcltv = -1000; }

            try
            {
                this.qhcltv = loanFileData.sHCLTVRPe;
            }
            catch { this.qhcltv = -1000; }

            try { qltv = loanFileData.sLtvR; }
            catch { qltv = -1000; }

            try { qlamt = loanFileData.sLAmtCalc; }
            catch { qlamt = -1000; }

            try
            {
                qscore = loanFileData.sCreditScoreType2; // opm 21721 : Change QSCORE default from SCORE1 to SCORE2
            }
            catch { qscore = -1000; }
        }

        public void WriteTo(CPageData loanFileData)
        {
            if (-1000 != qcltv || -1000 != qhcltv || -1000 != qltv || -1000 != qscore || -1000 != qlamt || 0 != lockDaysAdj)
            {
                if (-1000 != qcltv)
                    loanFileData.sCltvLpeQual = qcltv;

                if (-1000 != qhcltv)
                    loanFileData.sHcltvLpeQual = qhcltv;

                if (-1000 != qltv)
                    loanFileData.sLtvLpeQual = qltv;

                if (-1000 != qlamt)
                    loanFileData.sLAmtCalcLpeQual = qlamt; // using the pe field here to make sure that the peVal is also set so that sLAmtCalc used by symbol table can return correct value.

                if (-1000 != qscore)
                    loanFileData.sCreditScoreLpeQual = qscore;

                if (0 != lockDaysAdj)
                    loanFileData.sRLckdDaysFromInvestor += (int)lockDaysAdj;

                //                loanFileData.sSymbolTableForPriceRule.ReloadValuesForQEntries();
            }
        }
    }
}
