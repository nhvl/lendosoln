﻿namespace LendersOffice.RatePrice
{
    using System;

    /// <summary>
    /// Represents data in INVESTOR_NAME table.
    /// </summary>
    public interface IInvestorName
    {
        /// <summary>
        /// Gets the investor name.
        /// </summary>
        /// <value>The investor name.</value>
        string InvestorName { get; }

        /// <summary>
        /// Gets a value indicating whether item is valid.
        /// </summary>
        /// <value>Item is valid.</value>
        bool IsValid { get; }

        /// <summary>
        /// Gets rate lock cut off time.
        /// </summary>
        /// <value>Rate lock cut off time.</value>
        DateTime? RateLockCutOffTime { get; }

        /// <summary>
        /// Gets a value indicating whether use lender time zone for ratesheet expiration.
        /// </summary>
        /// <value>Use lender time zone for ratesheet expiration.</value>
        bool UseLenderTimezoneForRsExpiration { get; }

        /// <summary>
        /// Gets investor id.
        /// </summary>
        /// <value>Investor id.</value>
        int InvestorId { get; }
    }
}
