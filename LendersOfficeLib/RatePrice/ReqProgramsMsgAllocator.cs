﻿using System;
using System.Collections;

namespace LendersOfficeApp.los.RatePrice
{
    internal class ReqProgramsMsgAllocator
    {
        Stack m_cachedObjects = new Stack();
        int m_maxCached;

        public ReqProgramsMsgAllocator(int maxCached)
        {
            if (maxCached <= 0)
            {
                maxCached = 10;
            }
            m_maxCached = maxCached;
        }
        public ReqProgramsMsg Alloc(PriceGroupMemberArrayList programs)
        {
            lock (m_cachedObjects)
            {
                if (m_cachedObjects.Count > 0)
                {
                    ReqProgramsMsg obj = (ReqProgramsMsg)m_cachedObjects.Pop();

                    obj.Programs = programs;
                    obj.AnswerEvent.Reset();

                    return obj;
                }
            }
            return new ReqProgramsMsg(programs);
        }

        public void Free(ref ReqProgramsMsg obj)
        {
            if (obj != null)
            {
                lock (m_cachedObjects)
                {
                    obj.Programs = null;
                    if (m_cachedObjects.Count < m_maxCached)
                    {
                        m_cachedObjects.Push(obj);
                    }
                }
                obj = null;
            }
        }
    }
}
