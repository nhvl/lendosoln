﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace LendersOfficeApp.los.RatePrice
{
    public interface IRateItem
    {
        bool IsSpecialKeyword { get; }
        E_RateSheetKeywordT RateSheetKeyword { get; }
        int LowerLimit { get; }
        int UpperLimit { get; }
        decimal Rate { get; }
        string Rate_rep { get; }
        decimal Point { get; }
        string Point_rep { get; }

        decimal Margin { get; }
        string Margin_rep { get; }

        decimal QRateBase { get; }
        string QRateBase_rep { get; }

        decimal TeaserRate { get; }
        string TeaserRate_rep { get; }
    }
}
