﻿using System;
using System.Xml;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice
{
    public abstract class AbstractRtRule
    {
        public RtCondition Condition { get; protected set; }

        public RtConsequence Consequence { get; protected set; }

        public string Description { get; protected set; }

        public Guid RuleId { get; protected set; }

        public E_sRuleQBCType QBC { get; protected set; }

        public bool IsLenderRule { get; protected set; }

        public int EstimateSize()
        {
            int size = 40;

            if (Description != null)
                size += 20 + 2 * Description.Length;
            size += Condition.EstimateSize();
            size += Consequence.EstimateSize();
            return size;
        }

        public bool DtiDepend
        {
            get { return Category == LpeKeywordCategory.Dti; }
        }

        public LpeKeywordCategory Category
        {
            get
            {
                return Condition.Category >= Consequence.Category ? Condition.Category
                                                                       : Consequence.Category;
            }
        }

    }
    public class RtRule : AbstractRtRule
    {
        public RtRule(XmlElement ruleElement)
        {

            XmlElement conditionElement = (XmlElement)ruleElement.SelectNodes(".//Condition")[0];
            XmlElement consequenceElement = (XmlElement)ruleElement.SelectNodes(".//Consequence")[0];

            Condition = new RtCondition(conditionElement.InnerText);

            Consequence = new RtConsequence(consequenceElement);
            RuleId = new Guid(ruleElement.Attributes["RuleId"].InnerText);
            Description = ruleElement.Attributes["Description"].Value.TrimWhitespaceAndBOM();
            QBC = GetQbc(ruleElement.Attributes["QBC"]);
            IsLenderRule = GetIsLenderRule(ruleElement.Attributes["LenderRule"]);
        }

        public RtRule(LendersOffice.RatePrice.Model.PricePolicyRule ruleModel)
        {
            Condition = new RtCondition(ruleModel.Condition);
            Consequence = new RtConsequence(ruleModel);
            RuleId = ruleModel.Id;
            Description = ruleModel.Description.TrimWhitespaceAndBOM();
            QBC = GetQbc(ruleModel.QBC);
            IsLenderRule = ruleModel.IsLenderRule;
        }

        public RtRule(LendersOffice.RatePrice.Model.PricePolicyRuleV2 ruleModel)
        {
            Condition = new RtCondition(ruleModel.Condition);
            Consequence = new RtConsequence(ruleModel);
            RuleId = ruleModel.Id;
            Description = ruleModel.Description.TrimWhitespaceAndBOM();
            QBC = GetQbc(ruleModel.QBC);
            IsLenderRule = ruleModel.IsLenderRule;
        }

        private static bool GetIsLenderRule(XmlAttribute lenderRule)
        {
            if (lenderRule == null) return false;

            return lenderRule.Value.ToUpper() == "T";
        }

        private static E_sRuleQBCType GetQbc(XmlAttribute Qbc)
        {
            if (Qbc == null)
            {
                return E_sRuleQBCType.Legacy;
            }

            return GetQbcImpl(Qbc.Value);
        }

        private static E_sRuleQBCType GetQbc(string value)
        {
            if (value == null)
            {
                return E_sRuleQBCType.Legacy;
            }

            return GetQbcImpl(value);
        }

        private static E_sRuleQBCType GetQbcImpl(string value)
        {
            switch (value)
            {
                case "P": return E_sRuleQBCType.PrimaryBorrower;
                case "A": return E_sRuleQBCType.AllBorrowers;
                case "C": return E_sRuleQBCType.OnlyCoborrowers;
                default:
                    Tools.LogBug("Unknown QBC Value: " + value);
                    return E_sRuleQBCType.Legacy;
            }
        }

    }

}
