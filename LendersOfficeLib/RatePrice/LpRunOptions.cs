using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Xml;
using System.Xml.Serialization;
using DataAccess;
using LendersOffice.DistributeUnderwriting;
using DataAccess.LoanComparison;
using LendersOffice.RatePrice;
using LqbGrammar.DataTypes;
using LendersOffice.ObjLib.PriceGroups.Model;

[XmlRoot("CPriceGroupDetail")]
public class CPriceGroupDetails
{
    public CPriceGroupDetails() {}

    public CPriceGroupDetails( Guid productId, decimal priceGroupProfitMargin, decimal priceGroupMaxYspAdj, decimal priceGroupRateMargin, decimal priceGroupMarginMargin )
    {
        ProductId = productId;
        PriceGroupProfitMargin = priceGroupProfitMargin;
        PriceGroupMaxYspAdj = priceGroupMaxYspAdj;
        PriceGroupRateMargin = priceGroupRateMargin;
        PriceGroupMarginMargin = priceGroupMarginMargin;
    }

    [XmlAttribute]
    public Guid ProductId { get; set;}

    [XmlAttribute]
    public decimal PriceGroupProfitMargin { get; set;}

    [XmlAttribute]
    public decimal PriceGroupRateMargin { get; set;}

    [XmlAttribute]
    public decimal PriceGroupMarginMargin { get; set;}

    [XmlAttribute]
    public decimal PriceGroupMaxYspAdj { get; set;}


    public override string ToString() 
    {
        return string.Format("[CPriceGroupDetails: ProductId={0}, ProfitMargin={1}, MaxYspAdj={2}, RateMargin={3}, MarginMargin={4}]", ProductId, PriceGroupProfitMargin, PriceGroupMaxYspAdj, PriceGroupRateMargin, PriceGroupMarginMargin);
    }
}
public class CLpRunOptions
{
    string m_version = "";

    private List<CPriceGroupDetails>  x_priceGroupDetailsList = null;

    private HybridLoanProgramSetOption m_historicalPricingOption = null;

    private List<CPriceGroupDetails> m_priceGroupDetailsList 
    {
        get 
        {
            var result = x_priceGroupDetailsList;
            if (result == null)
            {
                result = GetHistoricalPriceGroupDetailsList();

                if (result == null)
                {
                    result = RetrievePriceGroupDetailsList(this.BrokerId, this.PriceGroupId);
                }

                x_priceGroupDetailsList = result;
            }

            return result;
        }
    }
    private List<CPriceGroupDetails> RetrievePriceGroupDetailsList(Guid brokerId, Guid priceGroupId) 
    {
        List<CPriceGroupDetails> list = new List<CPriceGroupDetails>();

        if (priceGroupId != Guid.Empty) 
        {
            SqlParameter[] parameters = { 
                                            new SqlParameter("@LpePriceGroupId", priceGroupId)
                                        };
            // TODO: Not efficient need to optimize.

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListPriceGroupDetails", parameters)) 
            {
                while (reader.Read()) 
                {
                    Guid lLpTemplateId = (Guid) reader["lLpTemplateId"];
                    decimal lenderProfitMargin = (decimal) reader["LenderProfitMargin"];
                    decimal lenderMaxYspAdjust = (decimal) reader["LenderMaxYspAdjust"];
                    decimal lenderRateMargin = (decimal) reader["LenderRateMargin"];
                    decimal lenderMarginMargin = (decimal) reader["LenderMarginMargin"];

                    list.Add(new CPriceGroupDetails(lLpTemplateId, lenderProfitMargin, lenderMaxYspAdjust, lenderRateMargin, lenderMarginMargin));

                }
            }
        }

        return list;
    }

    private List<CPriceGroupDetails> GetHistoricalPriceGroupDetailsList()
    {
        if (HistoricalPricingOption != null && HistoricalPricingOption.PriceGroupOptions == E_LoanProgramSetPricingT.Historical)
        {
            var priceGroupRevision = SHA256Checksum.Create(HistoricalPricingOption.PriceGroupContentKey);
            if (priceGroupRevision == null)
            {
                return null;
            }

            List<CPriceGroupDetails> list = new List<CPriceGroupDetails>();

            LpePriceGroup priceGroup = LendersOfficeApp.los.RatePrice.Utils.RetrievePriceGroup(this.BrokerId, priceGroupRevision.Value);

            foreach (var item in priceGroup.ProductList)
            {
                if (item.IsValid)
                {
                    list.Add(new CPriceGroupDetails(item.LoanProgramTemplateId, item.LenderProfitMargin, item.LenderMaxYspAdjust, item.LenderRateMargin, item.LenderMarginMargin));

                }
            }

            return list;

        }

        return null;
    }

    public CLpRunOptions()
    {
        this.PrepareLoanOnly = false;
    }


    private volatile bool prepareLoanOnly; 

    /// <summary>
    /// Instructs Applicant Prices  to prepare a loan file and exit instead of 
    /// running full pricing. 
    /// </summary>
    public bool PrepareLoanOnly
    {
        get { return this.prepareLoanOnly;  }
        set { this.prepareLoanOnly = value; }
    }

    public E_sLienPosT GetMode()
    {
        return FirstLienLpProgramId == Guid.Empty ? E_sLienPosT.First : E_sLienPosT.Second;
    }

    [XmlAttribute]
    public Guid FirstLienLpProgramId { get; set; }

    [XmlAttribute]
    public decimal FirstLienNoteRate { get; set; }

    [XmlAttribute]
    public decimal FirstLienPoint { get; set; }

    [XmlAttribute]
    public decimal FirstLienMPmt { get; set; }

    [XmlAttribute]
    public bool IsSetRateLockPricingOption { get; set; }

    [XmlAttribute]
    public bool IsAddConditionOption { get; set; }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="brokerId"></param>
    /// <param name="bGetAllReasons"></param>
    /// <param name="bSnapupRate1Eighth"></param>
    /// <param name="nLockPeriodAdj"></param>
    /// <param name="priceGroupNm"></param>
    /// <param name="priceGroupId"></param>
    /// <param name="priceGroupDetailsList">key=productid(guid), value:CPriceGroupDetails object</param>
    /// <param name="selectedRate">pass 0 if there is no rate option specifically selected</param>
    /// <param name="selectedFee">pass 0 if there is no rate option specifically selected</param>
    public CLpRunOptions( Guid brokerId, bool bGetAllReasons, bool bSnapupRate1Eighth, int nLockPeriodAdj,
        string priceGroupNm, Guid priceGroupId, SHA256Checksum lpePriceGroupContentKey, decimal selectedRate, decimal selectedFee, string selectedRateOptionId,
        bool roundUpLpeFee, decimal roundUpLpeFeeInterval, bool useQbc, bool isRateOptionsWorseThanLowerRateShown, E_LpeTaskT lpeTaskT, E_sPricingModeT pricingModeT,
        Guid firstLienLpProgramId, decimal firstLienNoteRate, decimal firstLienPoint, decimal firstLienMPmt, bool isSetRateLockPricingOption, bool isAddConditionOption, string ratemerge,
        HybridLoanProgramSetOption historicalOption) 
    {
        this.BrokerId = brokerId;
        IsGetAllReasons         = bGetAllReasons;
        IsSnapupRate1Eighth     = bSnapupRate1Eighth;
        LockPeriodAdj         = nLockPeriodAdj;
        
        PriceGroupNm           = priceGroupNm;
        PriceGroupId           = priceGroupId;
        PriceGroupContentKey = lpePriceGroupContentKey.Value;

        SelectedRate = selectedRate;
        SelectedFee = selectedFee;

        SelectedRateOptionId = selectedRateOptionId;

        RoundUpLpeFee = roundUpLpeFee;
        RoundUpLpeFeeInterval = roundUpLpeFeeInterval;
        UseQbc = useQbc;

        IsRateOptionsWorseThanLowerRateShown = isRateOptionsWorseThanLowerRateShown;
        LpeTaskT = lpeTaskT;

        PricingModeT = pricingModeT;

        FirstLienLpProgramId = firstLienLpProgramId;
        FirstLienNoteRate = firstLienNoteRate;
        FirstLienPoint = firstLienPoint;
        FirstLienMPmt = firstLienMPmt;
        IsSetRateLockPricingOption = isSetRateLockPricingOption;
        IsAddConditionOption = isAddConditionOption;
        RateMergeParRate = ratemerge;

        this.HistoricalPricingOption = historicalOption;
    }

    [XmlAttribute]
    public decimal OriginalNoteRate { get; set; }

    [XmlAttribute]
    public string SelectedRateOptionId { get; set;}


    [XmlAttribute]
    public decimal SelectedRate { get; set;}


    [XmlAttribute]
    public decimal SelectedFee { get; set;}


    public string SelectedRateFormat 
    {
        get { return SelectedRate.ToString("0.000") + ":" + SelectedFee.ToString("0.000") + ":" + SelectedRateOptionId; }
    }

    [XmlAttribute]
    public string PriceGroupNm { get; set; }


    [XmlAttribute]
    public Guid PriceGroupId { get; set; }

    /// <summary>
    /// 12/28/2016 - dd - Due to XmlSerialization I am using a string type instead of SHA256Checksum.
    /// </summary>
    [XmlAttribute]
    public string PriceGroupContentKey { get; set; }

    [XmlAttribute]
    public bool RoundUpLpeFee { get; set; }

    [XmlAttribute]
    public decimal RoundUpLpeFeeInterval { get; set;}

    [XmlAttribute]
    public Guid BrokerId { get; set; }

    public CPriceGroupDetails GetPriceGroupDetailsFor( Guid productId )
    {
        // If broker doesn't have pricing group define then return all 0 for PriceGroupDetails.
        if (PriceGroupId == Guid.Empty) 
        {
            return new CPriceGroupDetails(productId, 0.0M, 0.0M, 0.0M, 0.0M);
        }

        CPriceGroupDetails ret = null;
        if (null != m_priceGroupDetailsList) 
        {


            foreach (CPriceGroupDetails o in m_priceGroupDetailsList) 
            {
                if (o.ProductId == productId) 
                {
                    ret = o;
                    break;
                }

            }
        }
        return ret;
    }

    [XmlAttribute]
    public bool IsGetAllReasons { get; set;}

    /// <summary>
    /// Opm # 2006
    /// </summary>
    [XmlAttribute]     
    public bool IsSnapupRate1Eighth { get; set;}

    /// <summary>
    /// Number of days that the lender needs to take out from the investor lock period. Opm # 1472
    /// </summary>
    [XmlAttribute]    
    public int LockPeriodAdj { get; set;}

    [XmlAttribute]
    public string Version 
    {
        get { return m_version; }
        set 
        {
            // 10/12/2006 dd - Version could never be null.
            if (value == null)
                m_version = "";
            else
                m_version = value; 
        }
    }
    [XmlAttribute]
    public string UniqueChecksum { get; set; }

    [XmlAttribute]
    public string SecondLienUniqueChecksum { get; set; }

    /// <summary>
    ///  7/27/2006 dd - For debugging purpose only. Record the time result first display to user.
    /// </summary>
    [XmlAttribute]
    public long DebugResultStartTicks { get; set;}

    [XmlAttribute]
    public bool UseQbc { get; set; }

    [XmlAttribute]
    public bool IsRateOptionsWorseThanLowerRateShown
    {
        get;
        set;
    }
    [XmlAttribute]
    public E_sPricingModeT PricingModeT { get; set; }

    [XmlAttribute]
    public E_LpeTaskT LpeTaskT { get; set; }

    [XmlAttribute]
    public E_RunPmlRequestSourceT RunPmlRequestSourceT { get; set; }

    [XmlAttribute]
    public decimal SecondLienMPmt { get; set; }

    [XmlElement]
    public PricingState PricingState { get; set; }

    [XmlElement]
    public int? RateLockDays { get; set; }

    [XmlElement]
    public string RateMergeParRate { get; set; }

    [XmlElement]
    public bool IsUsePricingSummaryDebug { get; set; }

    [XmlElement]
    public bool IsAutoProcess{ get; set; }


    [XmlElement]
    public System.Collections.Generic.HashSet<string> DisplayRates { get; set; }

    [XmlArray("DenialExceptionList"), XmlArrayItem(typeof(DenialException), ElementName = "DenialException")]
    public List<DenialException> DenialExceptions { get; set; }

    public override string ToString() 
    {

        string str = string.Format("LpRunOptions:[bGetAllReasons={0}, bSnapupRate1Eighth={1}, nLockPeriodAdj={2}, priceGroupNm={3}, priceGroupId={4}, selectedRate={5}, selectedFee={6}, version={7}, RoundUpLpe={8}, RoundUpLpeInterval={9}",
            IsGetAllReasons, IsSnapupRate1Eighth, LockPeriodAdj, PriceGroupNm, PriceGroupId, SelectedRate, SelectedFee, m_version, RoundUpLpeFee, RoundUpLpeFeeInterval);
        if (null != m_priceGroupDetailsList) 
        {

            foreach (CPriceGroupDetails o in m_priceGroupDetailsList) 
            {
                str += Environment.NewLine + "   " + o.ToString();
            }
        }
        return str;

    }

    [XmlElement]
    public HybridLoanProgramSetOption HistoricalPricingOption
    {
        get
        {
            return m_historicalPricingOption;
        }

        set
        {
            m_historicalPricingOption = value;
            x_priceGroupDetailsList = null;
        }
    }
}