using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice
{

    public class TimeStampUtility
    {
        static public bool Equals(byte[] x, byte[] y)
        {
            if (object.ReferenceEquals(x, y))
                return true;

            if (x == null || y == null)
                return false;

            if (x.Length != y.Length)
                return false;

            for (int i = 0; i < x.Length; i++)
                if (x[i] != y[i])
                    return false;
            return true;
        }
    }

    /// <summary>
    /// Manage list of price policy and create them on demand, this class must be thread safe
    /// </summary>
    public class CPricePolicyManager
    {
        private class PolicyWrapper // POD : Plain Old Data 
        {

            public string mutualExclusiveExecSortedId;
            public RtPricePolicy rtPricePolicy;
            public int counter;
            public bool isHome;
            public DateTime expireTime = DateTime.MaxValue;
            public byte[] versionTimestamp;

            public PolicyWrapper(string mutualExclusiveExecSortedId, Byte[] versionTimestamp)
            {
                this.mutualExclusiveExecSortedId = mutualExclusiveExecSortedId;
                this.versionTimestamp = versionTimestamp;
            }

            public int EstimateSize(out int idlePolicySize)
            {
                idlePolicySize = 0;

                const int ObjectFixSize = 12 + (4 * 4) + 8  /* DateTime*/ + 20 /* versionTimestamp */ ;

                int extra = 0;
                if (mutualExclusiveExecSortedId != null)
                    extra = 2 * mutualExclusiveExecSortedId.Length;
                lock (this)
                {
                    if (rtPricePolicy != null)
                    {
                        int policySizeOnly = rtPricePolicy.EstimateSize();
                        extra += policySizeOnly;
                        if (counter == 0)
                            idlePolicySize = policySizeOnly;
                    }
                }
                return ObjectFixSize + extra;
            }

            // restricted use
            public bool StealRtPricePolicy(PolicyWrapper cache)
            {
                if (rtPricePolicy != null || cache == null || versionTimestamp == null ||
                    TimeStampUtility.Equals(versionTimestamp, cache.versionTimestamp) == false)
                    return false;

                rtPricePolicy = cache.rtPricePolicy;
                if (rtPricePolicy == null)
                    return false;

                expireTime = DateTime.Now;
                return true;
            }


        }

        readonly private Dictionary<Guid, PolicyWrapper> m_policies;
        readonly private DataSrc m_dataSrc;
        readonly private DateTime m_versionD;
        readonly private int m_timeoutInMinutes4FrozenPolicy;

        public CPricePolicyManager(DataSrc dataSrc, DateTime versionD, DataRowCollection rowsPolicies,
                                    int timeoutInMinutes4FrozenPolicy)
        {
            m_dataSrc = dataSrc;
            m_versionD = versionD;
            m_policies = new Dictionary<Guid, PolicyWrapper>(rowsPolicies.Count);

            m_timeoutInMinutes4FrozenPolicy = timeoutInMinutes4FrozenPolicy;

            foreach (DataRow row in rowsPolicies)
            {
                Guid policyId = (Guid)row["PricePolicyId"];
                string mutexExecSortedId = (string)row["MutualExclusiveExecSortedId"];
                m_policies[policyId] = new PolicyWrapper(mutexExecSortedId, (byte[])row["VersionTimestamp"]);
            }

            Tools.LogRegTest("<LPE> TimeoutInMinutes4FrozenPolicy = " + m_timeoutInMinutes4FrozenPolicy);
        }


        public void StealData(CPricePolicyManager cacheManager)
        {
            foreach (var policyId in m_policies.Keys)
            {
                PolicyWrapper cur = m_policies[policyId];
                if (cur.StealRtPricePolicy((PolicyWrapper)cacheManager.m_policies[policyId]))
                    StealPolicyCounter++;
            }
        }

        public int StealPolicyCounter { get; private set; }


        public string GetMutualExclusiveExecSortedId(Guid policyId)
        {
            PolicyWrapper policy = m_policies[policyId];
            return policy.mutualExclusiveExecSortedId;
        }


        public RtPricePolicy GetPricePolicy(Guid policyId)
        {

            PolicyWrapper policyWrapper = m_policies[policyId];
            lock (policyWrapper)
            {
                if (policyWrapper.rtPricePolicy == null)
                    Tools.LogBug("Can not get rtPricePolicy for policy = " + policyId + " *** counter = " + policyWrapper.counter);
                return policyWrapper.rtPricePolicy;
            }
        }

        // Frozen policy is one that is loaded in memory but no loan progran references to it.
        public int UnloadSomeFrozenPolicies()
        {
            DateTime now = DateTime.Now;

            int numRemovePolicies = 0;
            foreach (PolicyWrapper policyWrapper in m_policies.Values)
            {
                lock (policyWrapper)
                {
                    if (policyWrapper.rtPricePolicy != null && policyWrapper.counter == 0 && now >= policyWrapper.expireTime)
                    {
                        policyWrapper.rtPricePolicy = null;
                        policyWrapper.expireTime = DateTime.MaxValue;
                        numRemovePolicies++;
                    }
                }
            }
            return numRemovePolicies;
        }


        public int UnloadPolicies(CountTable policyIds)
        {
            DateTime expireTime = m_timeoutInMinutes4FrozenPolicy < 0
                                  ? DateTime.MaxValue
                                  : DateTime.Now.AddMinutes(m_timeoutInMinutes4FrozenPolicy);


            int numRemovePolicies = 0;
            foreach (Guid policyId in policyIds.IdList)
            {
                Integer repeat = policyIds[policyId];
                PolicyWrapper policyWrapper = m_policies[policyId];
                lock (policyWrapper)
                {
                    if (policyWrapper.rtPricePolicy != null)
                    {
                        policyWrapper.counter -= repeat.num;
                        if (policyWrapper.counter < 0)
                            Tools.LogBug("Warning : policyWrapper.counter < 0 in UnloadPolicies()");
                        if (policyWrapper.counter <= 0)
                        {
                            policyWrapper.counter = 0;
                            if (policyWrapper.isHome == false || m_timeoutInMinutes4FrozenPolicy == 0)
                            {
                                policyWrapper.rtPricePolicy = null;

                                numRemovePolicies++;
                                policyWrapper.expireTime = DateTime.MaxValue;
                            }
                            else
                                policyWrapper.expireTime = expireTime;
                        }
                    }
                    else
                    {
                        Tools.LogBug("Warning : policyWrapper.rtPricePolicy = null in UnloadPolicies()");
                    }
                }
            }
            return numRemovePolicies;
        }



        private CountTable GetMissingPolicies(CountTable policyIds)
        {
            CountTable missingIds = null;

            foreach (Guid policyId in policyIds.IdList)
            {
                Integer repeat = policyIds[policyId];
                PolicyWrapper policyWrapper = m_policies[policyId];
                lock (policyWrapper)
                {
                    if (policyWrapper.rtPricePolicy != null)
                    {
                        policyWrapper.counter += repeat.num;

                    }
                    else
                    {
                        if (missingIds == null)
                        {
                            missingIds = new CountTable(policyIds.Count);
                        }
                        missingIds[policyId] = repeat;
                    }
                }
            }
            return missingIds;
        }

        public void SetHomePolicies(Hashtable policyIds, bool isHome)
        {
            foreach (Guid policyId in policyIds.Keys)
            {
                PolicyWrapper policyWrapper = m_policies[policyId];
                lock (policyWrapper)
                {
                    policyWrapper.isHome = isHome;
                }
            }

        }

        public int LoadPolicies(CountTable policyIds)
        {
            CountTable missingIds = GetMissingPolicies(policyIds);

            if (missingIds == null)
                return 0;


            // make sure only one thread load policy.
            LoadMissingPolicies(missingIds);

            List<Guid> errIds = null;

            foreach (Guid policyId in missingIds.IdList)
            {
                PolicyWrapper policyWrapper = m_policies[policyId];
                if (policyWrapper.rtPricePolicy == null)
                {
                    if (errIds == null)
                        errIds = new List<Guid>();
                    errIds.Add(policyId);
                }
            }

            if (errIds == null)
            {
                return missingIds.Count;
            }

            StringBuilder sb = new StringBuilder("Can not load the policies : ");
            foreach (Guid policyId in errIds)
                sb.Append(" ").Append(policyId);

            Tools.LogError(sb.ToString());
            throw new CBaseException(ErrorMessages.FailedToLoadPolicies, sb.ToString());
        }


        static void ContinuouslyLoadPoliciesFromDb(DataSrc dataSrc, List<Guid> policyIds, Dictionary<Guid, RtPricePolicy> appendResult)
        {
            Tools.LogInfo("<LPE_POLICIES> ContinuouslyLoadPoliciesFromDb - dataSrc=" + dataSrc + " - Policy Count=" + policyIds.Count);
 
            List<SqlParameter> paraList = new List<SqlParameter>();

            string sqlSelect = string.Empty;

            sqlSelect = "select PricePolicyId, PricePolicyXmlText, MutualExclusiveExecSortedId, ParentPolicyId, PricePolicyDescription from price_policy where PricePolicyId " +
                DbTools.CreateParameterized4InClauseSql("Po", policyIds, paraList);

            CDataSet ds = new CDataSet();
            ds.LoadWithSqlQueryDangerously(dataSrc, sqlSelect, paraList);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Guid policyId = (Guid)row["PricePolicyId"];
                appendResult[policyId] = new RtPricePolicy(policyId, row);
            }
        }

        private void LoadMissingPolicies(CountTable policyIds)
        {
            const int BlockSize = 400;

            Dictionary<Guid, RtPricePolicy> appendResult = new Dictionary<Guid, RtPricePolicy>(policyIds.Count);
            List<Guid> blockIds = new List<Guid>();

            foreach (Guid id in policyIds.Keys)
            {
                blockIds.Add(id);
                if (blockIds.Count >= BlockSize)
                {
                    ContinuouslyLoadPoliciesFromDb(m_dataSrc, blockIds, appendResult);
                    blockIds.Clear();
                }

            }
            if (blockIds.Count > 0)
                ContinuouslyLoadPoliciesFromDb(m_dataSrc, blockIds, appendResult);

            foreach (Guid policyId in appendResult.Keys)
            {
                RtPricePolicy rtPricePolicy = appendResult[policyId];
                Integer repeat = (Integer)policyIds[policyId];
                PolicyWrapper policyWrapper = (PolicyWrapper)m_policies[policyId];

                lock (policyWrapper)
                {
                    if (policyWrapper.rtPricePolicy != null)
                        Tools.LogBug("Internal error : don't expect rtPricePolicy != null");

                    if (policyWrapper.counter != 0)
                        Tools.LogBug("Internal error : policyWrapper.counter != 0");


                    policyWrapper.rtPricePolicy = rtPricePolicy;
                    policyWrapper.counter = repeat.num;
                    policyWrapper.expireTime = DateTime.MaxValue;
                }

            }

        }

        // Add for hotfix, will remove later
        public int GetNumCachedPolicies(out int numCachedHomePolicies)
        {
            RuleCategoryCounter ruleCategoryCounter = new RuleCategoryCounter();
            return GetNumCachedPolicies(out numCachedHomePolicies, ref ruleCategoryCounter);
        }


        public int GetNumCachedPolicies(out int numCachedHomePolicies, ref RuleCategoryCounter ruleCategoryCounter)
        {
            numCachedHomePolicies = 0;
            int cachedPolicies = 0;
            foreach (PolicyWrapper policyWrapper in m_policies.Values)
            {
                lock (policyWrapper)
                {
                    RtPricePolicy rtPricePolicy = policyWrapper.rtPricePolicy;
                    if (rtPricePolicy != null)
                    {
                        cachedPolicies++;
                        if (policyWrapper.isHome)
                            numCachedHomePolicies++;
                        rtPricePolicy.AccumulateCategoryCounter(ref ruleCategoryCounter);
                    }
                }
            }
            return cachedPolicies;
        }

        public int EstimateSize(out int frozenPoliciesSize)
        {
            int idlePoliciesSize = 0;
            int size = 52 + (12 + 16 + 4) * m_policies.Count;
            int idleSize;
            foreach (PolicyWrapper policyWrapper in m_policies.Values)
            {
                size += policyWrapper.EstimateSize(out idleSize);
                idlePoliciesSize += idleSize;
            }

            frozenPoliciesSize = idlePoliciesSize;
            return size;

        }


        public CountTable GetPolicyCounters()
        {
            CountTable countTable = new CountTable();
            foreach (Guid policyId in m_policies.Keys)
            {
                PolicyWrapper cur = m_policies[policyId];
                if (cur.counter > 0)
                    countTable[policyId] = new Integer(cur.counter);
            }
            return countTable;
        }

        // opm 22473 : Policy counters are not correct if exception happens during loading policy process.
        public void AdjustPolicyCounters(CountTable expectCounters)
        {
            DateTime now = DateTime.Now.AddMinutes(2);
            StringBuilder sbInvalid = null;
            StringBuilder sb = null;

            int numAdjust = 0;
            try
            {
                foreach (Guid policyId in expectCounters.IdList)
                {
                    PolicyWrapper cur = null;

                    if (m_policies.TryGetValue(policyId, out cur) == false)
                    {
                        if (sbInvalid == null)
                            sbInvalid = new StringBuilder();
                        sbInvalid.Append(sbInvalid).Append(" ");
                        continue;

                    }
                    Integer expectCounter = (Integer)expectCounters[policyId];

                    lock (cur)
                    {
                        if (expectCounter.num == cur.counter)
                            continue;

                        numAdjust++;

                        if (expectCounter.num > cur.counter)
                        {
                            if (sb == null)
                                sb = new StringBuilder();
                            sb.AppendFormat("Policy {0} 's counter : {1} vs {2}\r\n", policyId, expectCounter.num, cur.counter);
                            if (cur.rtPricePolicy != null)
                                cur.counter = expectCounter.num;

                        }
                        else // expectCounter.num < cur.counter
                        {
                            cur.counter = expectCounter.num;
                            if (cur.counter == 0 && cur.rtPricePolicy != null)
                                cur.expireTime = now;
                        }
                    }
                }

                if (sbInvalid != null)
                    Tools.LogBug("CPricePolicyManager.AdjustPolicyCounters( expectCounters ) error. Invalid policies : "
                                  + sbInvalid.ToString());

                if (sb != null)
                    Tools.LogBug("CPricePolicyManager.AdjustPolicyCounters( expectCounters ) error : [LpGlobal vs PolicyManager].\r\n "
                                 + sbInvalid.ToString());

                if (numAdjust > 0)
                    Tools.LogRegTest("CPricePolicyManager.AdjustPolicyCounters( expectCounters ) : corrected " + numAdjust.ToString() + " policies.");

            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking("CPricePolicyManager.AdjustPolicyCounters( expectCounters ) error", exc);
            }


        }
    }
}