using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using CommonProjectLib.Common.Lib;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.RatePrice.FileBasedPricing;
using LqbGrammar.DataTypes;
using Toolbox;
using Adapter;
using LendersOffice.RatePrice.CustomRatesheet;

namespace LendersOfficeApp.los.RatePrice
{

    public class LpGlobalPriceGroupAssocItem
    {
        public Guid LpePriceGroupId { get; set; }
        public Guid lLpTemplateId { get; set; }
        public Guid PricePolicyId { get; set; }
        public Guid ProductId { get; set; }
        public byte AssocOverrideTri { get; set; }
    }

    /// <summary>
    /// This class is closely related to pricing policies which is available global.
    ///  1/1/12007 : CLpGlobal is designed as active object. Only "m_permissionWriteThread" 
    ///  thread can edit this object. 
    /// </summary>
    public class CLpGlobal
    {
        // Note that at some moment, there might be some dangling instance out there that 
        // a user is still running on even though it has been invalidated. That way
        // invalidating doesn't interrupt an lpe session that is already running.
        #region Variables/Getters
        static private CLpGlobal s_global = null;
        static private readonly object s_lockPad = new object();
        private CPricePolicyManager m_policyManager;
        private DateTime m_versionD;
        private string m_friendlyId = "";
        private PolicyRelationCenter m_policyRelationDb; /*IPolicyRelationDb*/
        private readonly Hashtable m_rateSheetTable = new Hashtable();
        private readonly Hashtable m_rateSheetItemPoolTable = new Hashtable();
        private DataSrc m_dataSrc;
        private Goodbye m_goodbye;
        private CRowHashtable m_rowsLoanProductFolders;
        private Hashtable m_loanProgramRawItems = new Hashtable();
        private Hashtable m_pgMemberApplicablePolicySet = new Hashtable();
        private Hashtable m_pgMemberApplicableRateSheet = new Hashtable();
        private Hashtable m_pgMemberApplicableRateSheetDownloadTime = new Hashtable();
        private Hashtable m_specialPriceGroupIds = null;
        private DataTable m_loanProgramSchema = null;
        private int m_srcRateSheetFldIdx = -1;
        private int m_policiesFldIdx = -1;
        private int m_programNameFldIdx = -1;
        private int m_lenderNameFldIdx = -1;
        private int m_investorNameFldIdx = -1;
        private int m_programIdFldIdx = -1;
        private int m_productIdFldIdx = -1;
        private int m_productCodeFldIdx = -1;
        private int m_brokerIdFldIdx = -1;
        private int m_directPoliciesIdFldIdx = -1;
        private int m_rateSheetProxyFldIdx = -1;
        private int m_rateSheetDownloadTimeStartDFldIdx = -1;
        private int m_rateSheetDownloadTimeEndDFldIdx = -1;
        private Hashtable m_invalidLoanPrograms = new Hashtable();
        private readonly Hashtable m_homePrograms = new Hashtable();
        private readonly Hashtable m_homePolicies = new Hashtable();
        private string m_snapshotStr = "Not snapshot info";
        private int m_timeoutInMinutes4FrozenPolicy = ConstStage.TimeoutInMinutes4FrozenPolicy;
        private AccessTime m_productAccessTime = new AccessTime();
        private Thread m_permissionWriteThread = null; // only m_permissionWriteThread can edit this object.
        int m_stealRateSheetCounter = 0;
        static ProductIdProvider s_productIdProvider = new ProductIdProvider();
        static ArrayList s_arrayListEmpty = new ArrayList(0);
        static Object s_PriorityOne = 1;
        static Object s_PrioritySix = 6;
        bool m_clearCaptureDataEnable = true;
        static bool s_displaySqlOfGetLoanProgramsFromDb = true;
        bool m_hasOwner = false;
        DateTime m_reduceLpeTime = DateTime.Now;
        int m_reduceLpeCounter = 0;
        bool m_debugCheckWritePermission = true;
        static Hashtable s_empty = new Hashtable();
        private Hashtable m_specialGlobalAssocPriceGroupIds = null; // OPM 21028
        private PmiManager m_PmiManager; // OPM 26013
        private List<Guid> m_RateSheetOverridePriceGroups = new List<Guid>(); // OPM 108981

        internal bool HasOwner
        {
            get { return m_hasOwner; }
            set { m_hasOwner = value; }
        }

        internal int GetNumProgramsInMemory()
        {
            lock (this)
            {
                return m_loanProgramRawItems.Count;
            }
        }

        /// <summary>
        /// Only CLpPerBroker should be calling this directly
        /// </summary>
        static internal CLpGlobal UpdatedInstance
        {
            get
            {
                lock (s_lockPad)
                {
                    if (null == s_global || !s_global.IsUptodate)
                    {
                        s_global = new CLpGlobal(); // some broker instances out there might still use the old one, that's fine, let them finish running.
                    }
                    return s_global;
                }
            }
        }

        public string Id
        {
            get { return m_friendlyId; }
        }

        internal bool IsUptodate
        {
            get { return SnapshotControler.IsUptodate(m_versionD); }
        }

        internal CPricePolicyManager PolicyManager
        {
            get { return m_policyManager; }
        }

        internal PmiManager PmiManager
        {
            get { return m_PmiManager; }
        }

        internal IPolicyRelationDb ParentHashtable
        {
            get { return m_policyRelationDb; }
        }

        internal DateTime VersionD
        {
            get { return m_versionD; }
        }

        internal CRowHashtable ProductFolderRows
        {
            get { return m_rowsLoanProductFolders; }
        }

        int EstimateMemory()
        {
            lock (this)
            {
                return (LpeDataProvider.MinLpeMemory == 0 || LpeDataProvider.MaxLpeMemory == 0) ? -1 : EstimateLpeSize();
            }
        }

        public String SnapshotStr()
        {
            return m_snapshotStr;
        }

        public SHA256Checksum FileKey { get; private set; }
        #endregion

        private CLpGlobal()
        {
            CPmlFIdGenerator idGen = new CPmlFIdGenerator();
            m_friendlyId = idGen.GenerateNewFriendlyId();

            Tools.LogRegTest(string.Format("CLpGlobal: Constructing new object with id = {0}", Id));
            Init();
        }

        private void Init()
        {
            const bool usingTransaction = true;

            DateTime startTime = DateTime.Now;

            DbConnection conn = null;

            DbProviderFactory factory = DbAccessUtils.GetDbProvider();
            DbTransaction tx = null;
            DataSet ds = new DataSet();
            DbDataAdapter da = factory.CreateDataAdapter();
            da.SelectCommand = factory.CreateCommand();

            // NOTE: Watch out for this command type, injected sql statement is possible
            // for this command type.  This code looks secure as we don't use anything 
            // that user can edit but never know what we would do in the future. Can just 
            // use commandtype.StoreProc instead.
            da.SelectCommand.CommandType = CommandType.Text;  // tn: reviewed on 11/16/07: This is safe from sql injection possibility because it doesn't build the sql statement using users' input data.

            try
            {
                m_dataSrc = SnapshotControler.ExpectDataSrc;

                var latestRelease = CLpeReleaseInfo.GetLatestReleaseInfo();
                this.FileKey = latestRelease.FileKey;

                conn = DbAccessUtils.GetConnection(m_dataSrc);
                conn.OpenWithRetry();

                da.SelectCommand.Connection = conn;

                if (usingTransaction)
                {
                    tx = conn.BeginTransaction(IsolationLevel.RepeatableRead);
                    da.SelectCommand.Transaction = tx;
                }

                StringBuilder strBuilder = new StringBuilder(200);

                // ds.Tables[0].Rows
                strBuilder.Append("exec GetPriceEngineVersionDate");

                // ds.Tables[1].Rows
                strBuilder.Append("; select PricePolicyId, ParentPolicyId, MutualExclusiveExecSortedId,  VersionTimestamp from price_policy ;");

                // ds.Tables[2].Rows
                strBuilder.Append("; SELECT f.FolderId , f.FolderName , f.ParentFolderId FROM Loan_Product_Folder AS f ;");

                // ds.Tables[3].Rows
                strBuilder.Append("; SELECT LpePriceGroupId  FROM PRICEGROUP_PROG_POLICY_ASSOC  group by LpePriceGroupId ;");

                // ds.Tables[4].Rows
                strBuilder.Append("; SELECT PricePolicyId, PmiProductId from PMI_POLICY_ASSOC WHERE AssocOverrideTri = 1 OR ( AssocOverrideTri = 0 AND InheritVal = 1 ) ;");

                // ds.Tables[5].Rows
                strBuilder.Append("; SELECT ProductId, MICompany, MIType, UfmipIsRefundableOnProRataBasis FROM PMI_PRODUCT WHERE IsEnabled = 1 AND MIType <> 0; ");

                // ds.Tables[6].Rows
                strBuilder.Append("; SELECT DISTINCT LpePriceGroupId FROM RATE_OPTION_PRICEGROUP_PROGRAM; ");

                // ds.Tables[7].Rows
                // It should normally be sufficient to just use the 'WHERE LpePriceGroupId = lLpTemplateId' clause,
                // but we will also verify that the selected program is actually the price group master (using the 
                // columns IsMaster and IsMasterPriceGroup) just in case the program's ID happens to end up matching
                // a price gropu ID unintentionally.
                // 4/7/2015 dd - ds.Tables[7].Rows will now only query from LOAN_PROGRAM_TEMPLATE instead of joining with VIEW_LPE_PRICEGROUP.
                strBuilder.Append("; SELECT lLpTemplateId AS LpePriceGroupId FROM LOAN_PROGRAM_TEMPLATE WHERE IsMasterPriceGroup=1 AND IsMaster=1;");

                da.SelectCommand.CommandText = strBuilder.ToString();
                da.Fill(ds);
                if (tx != null)
                {
                    tx.Commit();
                    tx = null;
                }
            }
            finally
            {
                if (null != tx)
                {
                    try
                    {
                        tx.Rollback(); tx = null;
                    }
                    catch { }
                }

                if (null != conn)
                {
                    conn.Close();
                }
            }

            m_versionD = Convert.ToDateTime(ds.Tables[0].Rows[0]["LastUpdatedD"]);
            m_snapshotStr = string.Format("{0}/{1} {2} {3}", m_versionD.Month, m_versionD.Day, m_versionD.ToLongTimeString(), m_dataSrc);

            m_policyManager = new CPricePolicyManager(m_dataSrc, m_versionD, ds.Tables[1].Rows, m_timeoutInMinutes4FrozenPolicy);

            CRowHashtable rowsPolicyRelation = new CRowHashtable(ds.Tables[1].Rows, "PricePolicyId");
            m_policyRelationDb = new PolicyRelationCenter(rowsPolicyRelation);

            m_rowsLoanProductFolders = new CRowHashtable(ds.Tables[2].Rows, "FolderId");

            var pmiPrograms = FileBasedPmiProduct.CreateFrom(ds.Tables[5].Rows);
            var pmiPolicyAssociations = FileBasedPmiPolicyAssociation.CreateFrom(ds.Tables[4].Rows);
            m_PmiManager = new PmiManager(pmiPolicyAssociations, pmiPrograms, m_policyRelationDb); // OPM 91382

            int numPolicies, totalParents, depth;
            m_policyRelationDb.GetStatisticInfo(out numPolicies, out totalParents, out depth);
            double avgParents = (numPolicies == 0) ? 0.0 : (100 * totalParents) / numPolicies / 100.0;

            LoadHomePolicies();

            try
            {
                // Special price group IDs refer to price groups that contain local policy associations (meaning, programs in
                // the price group have policy associations that are only local to that program in that particular price grup).
                m_specialPriceGroupIds = new Hashtable();
                foreach (DataRow row in ds.Tables[3].Rows)
                {
                    Guid priceGroupId = (Guid)row["LpePriceGroupId"];
                    m_specialPriceGroupIds[priceGroupId] = null;
                }
            }
            catch (Exception exc)
            {
                Tools.LogError("Can not load managed price group Ids", exc);
                m_specialPriceGroupIds = null;
            }

            // OPM 108981
            foreach (DataRow row in ds.Tables[6].Rows)
            {
                m_RateSheetOverridePriceGroups.Add((Guid)row["LpePriceGroupId"]);
            }

            // OPM 21028
            try
            {
                // Special Global Assoc Price Group IDs refer to price groups that have policies attached directly
                // to them via a Price Group Master program (the policies are attached to the program which is associated
                // with the price group).  Price Group Master Program IDs match the Price Group ID of the price group
                // they're attached to, and they also have IsMasterPriceGroup set to true.
                m_specialGlobalAssocPriceGroupIds = new Hashtable();
                foreach (DataRow row in ds.Tables[7].Rows)
                {
                    Guid priceGroupId = (Guid)row["LpePriceGroupId"];
                    m_specialGlobalAssocPriceGroupIds[priceGroupId] = null;
                }
            }
            catch (Exception exc)
            {
                Tools.LogError("Can not load managed price group Ids for global price group associations: ", exc);
                m_specialGlobalAssocPriceGroupIds = null;
            }

            Tools.LogRegTest(string.Format("CLpGlobal : {0}  #policies = {1}   #ancestors = {2}   #avg ancestors  {3}  #depth = {4}   #policyRelations = {5} *** {6}.",
                                    SnapshotStr(), numPolicies, totalParents, avgParents.ToString(), depth, rowsPolicyRelation.Count,
                                    CommonLib.TimeTool.GetDurationStringInMs(startTime))

                            );


            rowsPolicyRelation = null;
            DbTools.RemoveTables(ds);

            m_goodbye = new Goodbye(string.Format("<LPE> Destroy CLpGlobal, version = {0}", SnapshotStr()));
            m_reduceLpeTime = DateTime.Now;
        }

        public void SendReportToWebservice()
        {
            if (m_hasOwner == false)
            {
                Tools.LogBug("Call CLpGlobal.SendReportToWebservice() with no owner");
            }
            else
            {
                CheckWritePermission();
            }
        }

        /// <summary>
        /// Given an input set of ratesheets whose data needs to be loaded, determine the number of ratesheets from that set
        /// that are currently not loaded.
        /// 
        /// Note: if m_permissionWriteThread != null, only "m_permissionWriteThread" thread can [ directly or ] 
        /// indirectly call this function. 
        /// </summary>
        /// <param name="rateSheetIds">Hashtable containing the ratesheet IDs that need to be loaded (some may already be).</param>
        /// <returns>The number of missing rate sheets whose data needs to be loaded.</returns>
        private int GetRateSheets(Hashtable rateSheetIds)
        {
            CheckWritePermission();

            ArrayList ids = new ArrayList(rateSheetIds.Keys);
            List<Guid> missingIds = null;

            // lock( this ) : don't need to lock because we use Active Object model.
            // only "m_permissionWriteThread" thread can edit this object.
            {
                foreach (Guid rateSheetId in ids)
                {
                    if (m_rateSheetTable.Contains(rateSheetId))
                    {
                        rateSheetIds[rateSheetId] = m_rateSheetTable[rateSheetId];
                    }
                    else
                    {
                        rateSheetIds[rateSheetId] = null;
                        if (missingIds == null)
                        {
                            missingIds = new List<Guid>();
                        }
                        missingIds.Add(rateSheetId);
                    }
                }
            }

            if (missingIds == null)
            {
                return 0;
            }

            List<SqlParameter> parameters = new List<SqlParameter>(missingIds.Count);

            // 11/21/2007 ThienNguyen - Reviewed and safe
            string selectSql = "select * from RATE_OPTIONS where SrcProgId "
                               + DbTools.CreateParameterized4InClauseSql("S", missingIds, parameters);
            DataAccess.CDataSet ds = new CDataSet();
            ds.LoadWithSqlQueryDangerously(m_dataSrc, selectSql, parameters);

            lock (this)
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    Object SrcProgId = row["SrcProgId"];
                    String lRateSheetXmlContent = row["lRateSheetXmlContent"] as String;
                    string acceptableRsFileId = row["LpeAcceptableRsFileId"] as String;
                    long versionNumber = (long)row["LpeAcceptableRsFileVersionNumber"];

                    m_rateSheetTable.Add(
                        SrcProgId,
                        AbstractRateSheetProxy.Create(lRateSheetXmlContent, (byte[])row["VersionTimestamp"],
                                                       acceptableRsFileId, versionNumber,
                                                       m_rateSheetItemPoolTable)
                   );

                    rateSheetIds[SrcProgId] = m_rateSheetTable[SrcProgId];
                }

            }

            // for safety, we don't do if( ds.Tables[0].Rows.Count == missingCount ) return missingCount;
            StringBuilder sb = null;
            foreach (Object id in rateSheetIds.Keys)
            {
                if (rateSheetIds[id] == null)
                {
                    if (sb == null)
                    {
                        sb = new StringBuilder("Can not GetRateSheet for rateSheetIds  = ");
                    }
                    else
                    {
                        sb.Append(", ");
                    }
                    sb.Append(id);
                }
            }

            if (sb == null)
            {
                return missingIds.Count; // 99.9% same as ds.Tables[0].Rows.Count;
            }

            throw new CBaseException(ErrorMessages.Generic, sb.ToString());
        }

        internal int ClearRateSheetsNotInList(Hashtable keepList)
        {
            ArrayList removeIds = new ArrayList(100);

            lock (this)
            {
                foreach (Object rateSheetId in m_rateSheetTable.Keys)
                {
                    if (keepList.Contains(rateSheetId) == false)
                    {
                        removeIds.Add(rateSheetId);
                    }
                }

                foreach (Object rateSheetId in removeIds)
                {
                    m_rateSheetTable.Remove(rateSheetId);
                }
            }
            return removeIds.Count;
        }

        #region Stealing Ratesheets and Policies for Re-Use

        ArrayList GetCachedRateSheetIds()
        {
            lock (this)
            {
                return new ArrayList(m_rateSheetTable.Keys);
            }
        }

        ArrayList GetCachedPriceGroupRateSheetMembers()
        {
            lock (this)
            {
                return new ArrayList(m_pgMemberApplicableRateSheet.Keys);
            }
        }

        void StealRateSheets(CLpGlobal other)
        {
            if (other == null)
            {
                return;
            }

            ArrayList rateSheetIds = other.GetCachedRateSheetIds();
            if (rateSheetIds.Count == 0)
            {
                return;
            }

            const int BlockSize = 200;
            ArrayList parameters = new ArrayList(BlockSize);
            ArrayList blockIs = new ArrayList(BlockSize);

            for (int i = 0; i < rateSheetIds.Count; i++)
            {
                if (m_rateSheetTable.Contains(rateSheetIds[i]) == false)
                {
                    blockIs.Add(rateSheetIds[i]);
                }

                if (blockIs.Count == BlockSize || i == (rateSheetIds.Count - 1))
                {
                    if (blockIs.Count == 0)
                    {
                        continue;
                    }

                    // 11/21/2007 ThienNguyen - Reviewed and safe
                    string selectSql = "select SrcProgId, VersionTimestamp from RATE_OPTIONS where SrcProgId " + DbTools.CreateParameterized4InClauseSql("S", blockIs, parameters);
                    CDataSet ds = new CDataSet();
                    ds.LoadWithSqlQueryDangerously(m_dataSrc, selectSql, parameters);

                    lock (other)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            Guid rateSheetId = (Guid)row["SrcProgId"];
                            byte[] timeStamp = (byte[])row["VersionTimestamp"];

                            AbstractRateSheetProxy proxy = (AbstractRateSheetProxy)other.m_rateSheetTable[rateSheetId];
                            if (proxy == null)
                            {
                                continue;
                            }
                            if (proxy.SameTimeStamp(timeStamp))
                            {
                                m_rateSheetTable[rateSheetId] = proxy.CloneIfNoChoice(m_rateSheetItemPoolTable);
                                m_stealRateSheetCounter++;
                            }
                        }
                    }

                    blockIs.Clear();
                }
            }
        }

        internal void StealSomeRateSheetsAndPolicies(CLpGlobal other)
        {
            if (m_hasOwner == true)
            {
                Tools.LogBug("Cannot call CLpGlobal.StealSomeRateSheetsAndPolicies(...) if m_hasOwner != null");
                return;
            }

            DateTime startTime = DateTime.Now;
            int numCachedRateSheetsOfPrevSnapshot = 0;
            int numCachedPolicisOfPrevSnapshot = 0;
            if (other != null)
            {
                try
                {
                    StealRateSheets(other);
                    numCachedRateSheetsOfPrevSnapshot = other.GetCachedRateSheetIds().Count + other.GetCachedPriceGroupRateSheetMembers().Count;
                }
                catch (Exception exc)
                {
                    Tools.LogError("<LPE_error> can not run CLpGlobal.StealRateSheets(...)", exc);
                }

                try
                {
                    PolicyManager.StealData(other.PolicyManager);
                    int dummy;
                    numCachedPolicisOfPrevSnapshot = other.PolicyManager.GetNumCachedPolicies(out dummy);
                }
                catch (Exception exc)
                {
                    Tools.LogError("<LPE_error> can not run PolicyManager.StealData( ... )", exc);
                }

            }
            Tools.LogRegTest(string.Format("<LPE_Reuse> {0} : reused {1}/{2} ratesheets, {3}/{4}  policies from previous snapshot in {5}",
                                LpeTools.GetCurThreadName(""), m_stealRateSheetCounter, numCachedRateSheetsOfPrevSnapshot,
                                PolicyManager.StealPolicyCounter, numCachedPolicisOfPrevSnapshot,
                                CommonLib.TimeTool.GetDurationStringInMs(startTime)));

        }
        #endregion

        // if m_permissionWriteThread != null, only "m_permissionWriteThread" thread can [ directly or ] indirectly call this function.        
        MissingCounters LoadMissingPriceGroupMembers(PriceGroupMemberHashtable pgMembers)
        {
            pgMembers.Verify();
            MissingCounters missingCounters = new MissingCounters();

            string threadName = LpeTools.GetCurThreadName("");

            if (ConstSite.VerboseLogEnable4Pricing)
            {
                Tools.LogRegTest(string.Format("<LPE_load> {0} : try to load {1} missing price group members, ver = {2}.",
                                threadName, pgMembers.Count, SnapshotStr()));
            }

            const int MaxBlockSize = 300;

            if (pgMembers.Count <= MaxBlockSize)
            {
                return PrivateLoadMissingPriceGroupMembers(pgMembers);
            }

            MissingCounters tmpMissingCounters;
            DateTime startTime = DateTime.Now;

            // At first time, we load a small block of programs 
            // because first block often has more missing ratesheets and missing policies than next blocks.
            int blockSize = 200;

            int counter = 0;

            PriceGroupMemberHashtable blockMembers = new PriceGroupMemberHashtable(MaxBlockSize);
            foreach (PriceGroupMember member in pgMembers.Keys)
            {
                counter++;
                blockMembers.Add(member, null);
                if (blockMembers.Count == blockSize || counter == pgMembers.Count)
                {
                    tmpMissingCounters = PrivateLoadMissingPriceGroupMembers(blockMembers);
                    missingCounters.Add(tmpMissingCounters);

                    blockMembers.Clear();
                    blockSize = MaxBlockSize;
                }
            }

            if (blockMembers.Count > 0)
            {
                tmpMissingCounters = PrivateLoadMissingPriceGroupMembers(blockMembers);
                missingCounters.Add(tmpMissingCounters);

                Tools.LogBug("CLpGlobal.LoadMissingPriceGroupMembers.Counter bug");
            }

            if (ConstSite.VerboseLogEnable4Pricing || (DateTime.Now - startTime).TotalSeconds >= 10)
            {
                Tools.LogRegTest(string.Format("<LPE_load> {0} : in summary, finish to load {5} pgMembers, {1} missing programs, {2} missing ratesheets, {3} missing policies in {4}",
                                            threadName, missingCounters.NumLoanPrograms, missingCounters.NumRateSheets, missingCounters.NumPolicies,
                                            CommonLib.TimeTool.GetDurationStringInMs(startTime), missingCounters.NumPriceGroupMembers));
            }

            return missingCounters;
        }

        /// <summary>
        /// Merges all 3 kinds of associations: 
        /// 1) Global associations between programs and policies
        /// 2) Associations between policies and entire Price Groups
        /// 3) Associations between policies and programs within a specific Price Group
        /// </summary>
        /// <param name="associateForGlobal">Policies that are associated to programs globally (weakest association)</param>
        /// <param name="assocForLocal">Policies that are associated locally to programs within a specific price group (strongest association)</param>
        /// <param name="assocForEntirePriceGroup">Policies that are associated to all programs of a given price group (stronger than global association and weaker than local association)</param>
        /// <returns>The merged group of policies</returns>
        CSortedListOfGroups MergeThreeKindsOfAssoc(ArrayList associateForGlobal, Hashtable assocForLocal, Hashtable assocForEntirePriceGroup)
        {
            associateForGlobal = associateForGlobal ?? new ArrayList();
            assocForLocal = assocForLocal ?? new Hashtable();
            assocForEntirePriceGroup = assocForEntirePriceGroup ?? new Hashtable();

            ArrayList assocResult;
            if (assocForLocal.Count != 0 || assocForEntirePriceGroup.Count != 0)
            {
                Hashtable result = new Hashtable();

                // Cycle through each policy in the list of global associations (those in the Loan Programs ->  Rules page)
                // and if there are no local or price group-wide policies to override it, add it to the result list
                foreach (Guid id in associateForGlobal)
                {
                    if (assocForLocal.Contains(id) == false && assocForEntirePriceGroup.Contains(id) == false)
                    {
                        result[id] = null;
                    }
                }

                // OPM 21028 db - Now cycle though each policy in the list of price group-wide associations, 
                // and if there are no local policies to override it and it is set to Yes (associate)
                // , add it to the result list
                foreach (Guid id in assocForEntirePriceGroup.Keys)
                {
                    if (assocForLocal.Contains(id) == false)
                    {
                        E_TriState assocType = (E_TriState)(Byte)assocForEntirePriceGroup[id];
                        if (assocType == E_TriState.Yes)
                        {
                            result[id] = null;
                        }
                    }
                }

                // Now cycle through each policy in the list of local associations (associations between a policy and a program located
                // inside a price group) and if it is set to Yes (associate), add it to the result list
                foreach (Guid id in assocForLocal.Keys)
                {
                    E_TriState assocType = (E_TriState)(Byte)assocForLocal[id];

                    if (assocType == E_TriState.Yes)
                    {
                        result[id] = null;
                    }
                }

                assocResult = new ArrayList(result.Keys);
            }
            else
            {
                assocResult = associateForGlobal;
            }

            return m_policyRelationDb.GetApplicablePolicies(PolicyManager, assocResult);
        }

        /// <summary>Special price group IDs refer to price groups that contain local policy associations (meaning, programs in
        /// the price group have policy associations that are only local to that program in that particular price grup).
        /// This function might be better named "DoesPriceGroupHaveLocalPolicies" because it does not indicate a permission.</summary>
        /// <returns>True if the input price group has any local policy associations within it, False if not.</returns> 
        bool CanPriceGroupHaveLocalPolicies(Guid priceGroupId)
        {
            if (priceGroupId == Guid.Empty)
            {
                return false;
            }

            if (m_specialPriceGroupIds != null && m_specialPriceGroupIds.Contains(priceGroupId) == false)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Obtains the list of policy IDs that are directly associated with a program within a price group.
        /// </summary>
        /// <param name="priceGroupMembers">The list of programs that are located in price groups which have 
        /// policy associations within them.  This does not necessarily mean that each program has a policy 
        /// association, just that the price group it is contained within has some program-policy associations 
        /// in it.</param>
        /// <returns>A data table containing the Price Group ID, Program ID, Policy ID, and Association Type
        /// of all policies that are directly associated to any of the input programs within a price group.</returns>
        DataTable GetAssocLocalPoliciesFromDb(ICollection priceGroupMembers)
        {
            // Generate a Hashtable with entries representing price groups (key is price group ID).
            // Each value in the hashtable is an ArrayList of programs located within that price group
            Hashtable partiton = new Hashtable(); // partiton By PriceGroupId
            foreach (PriceGroupMember pgMember in priceGroupMembers)
            {
                if (CanPriceGroupHaveLocalPolicies(pgMember.PriceGroupId) == false) // Only load data for price groups that have local associated policies
                {
                    continue;
                }

                ArrayList arr = (ArrayList)partiton[pgMember.PriceGroupId];
                if (arr == null)
                {
                    partiton[pgMember.PriceGroupId] = arr = new ArrayList();
                }
                arr.Add(pgMember.PrgId);
            }

            if (partiton.Count == 0)
            {
                return new DataTable();
            }

            ArrayList allParameters = new ArrayList();
            ArrayList parameters = new ArrayList();
            StringBuilder sb = new StringBuilder();
            int idx = 0;
            foreach (Guid groupId in partiton.Keys)
            {
                idx++;
                string groupVar = "Pr" + idx.ToString();
                string prgPrefix = groupVar + "_";
                ArrayList prgIds = (ArrayList)partiton[groupId];

                if (sb.Length > 1)
                {
                    sb.Append(" or ");
                }

                sb.AppendFormat("\r\n ( LpePriceGroupId = @{0} and ProgId {1} ) ", groupVar,
                    DbTools.CreateParameterized4InClauseSql(prgPrefix, prgIds, parameters));
                allParameters.Add(new SqlParameter("@" + groupVar, groupId));
                allParameters.AddRange(parameters);
            }

            if (sb.Length == 0)
            {
                // do for safety. It never happens.
                Tools.LogBug("CLpGlobal.GetAssocLocalPoliciesFromDb bug : please review.");
                return new DataTable();
            }

            sb.Insert(0, "select * from PRICEGROUP_PROG_POLICY_ASSOC where AssociateType != 0 AND ( ");
            sb.Append(" )");
            string sql = sb.ToString();

            CDataSet ds = new CDataSet();
            ds.LoadWithSqlQueryDangerously(DataSrc.LpeSrc, sql, allParameters);
            return ds.Tables[0];
        }

        /// <summary>
        /// Obtains the list of policy IDs that are associated with an entire price group.
        /// </summary>
        /// <param name="priceGroupMembers">The list of programs that are located in price groups which have 
        /// policies associated with them via a Price Group Master program.</param>
        /// <returns>A data table containing the Price Group ID, Program ID, Policy ID, and Association Type
        /// of all policies that are associated to any of the input programs because of a price group-wide
        /// association.</returns>
        private IEnumerable<LpGlobalPriceGroupAssocItem> GetAssocPriceGroupWidePoliciesFromDb(ICollection priceGroupMembers)
        {
            Dictionary<Guid, List<Guid>> partition = new Dictionary<Guid, List<Guid>>();

            foreach (PriceGroupMember pgMember in priceGroupMembers)
            {
                if (DoesPriceGroupHaveGlobalPolicies(pgMember.PriceGroupId) == false)
                {
                    continue;
                }

                List<Guid> list = null;

                if (partition.TryGetValue(pgMember.PriceGroupId, out list) == false)
                {
                    list = new List<Guid>();
                    partition.Add(pgMember.PriceGroupId, list);
                }

                list.Add(pgMember.PrgId);
            }

            if (partition.Count == 0)
            {
                return new List<LpGlobalPriceGroupAssocItem>();
            }

            List<SqlParameter> firstSqlParameters = new List<SqlParameter>();
            List<SqlParameter> secondSqlParameters = new List<SqlParameter>();

            // We now have a hashtable containing all requested price groups that have a price 
            // group master program associated with it and all programs in that price group that 
            // we need to load data for.
            // Key = PriceGroupId, Value = ArrayList of programs within that price group

            List<SqlParameter> parameters = new List<SqlParameter>();
            StringBuilder firstWhereClause = new StringBuilder();
            StringBuilder secondWhereClause = new StringBuilder();
            int idx = 0;

            foreach (Guid groupId in partition.Keys)
            {
                idx++;
                string groupVar = "Pr" + idx.ToString();
                string prgPrefix = groupVar + "_";
                List<Guid> prgIds = partition[groupId];

                if (firstWhereClause.Length > 1)
                {
                    firstWhereClause.Append(" OR ");
                }

                if (secondWhereClause.Length > 1)
                {
                    secondWhereClause.Append(" OR ");
                }

                // Per OPM 21028, the ProductId will be the same as the PriceGroupId, hence the use of 'groupVar' for both where clauses
                firstWhereClause.AppendFormat("\r\n ProductId = @{0}", groupVar);
                firstSqlParameters.Add(new SqlParameter("@" + groupVar, groupId));

                secondWhereClause.AppendFormat("\r\n LpePriceGroupId = @{0} AND llptemplateid {1}  ", groupVar,
                    DbTools.CreateParameterized4InClauseSql(prgPrefix, prgIds, parameters));
                secondSqlParameters.Add(new SqlParameter("@" + groupVar, groupId));
                secondSqlParameters.AddRange(parameters);
            }

            if (firstWhereClause.Length == 0 || secondWhereClause.Length == 0)
            {
                // do for safety. It never happens.
                Tools.LogBug("CLpGlobal.GetAssocPriceGroupWidePoliciesFromDb bug : please review.");
                return new List<LpGlobalPriceGroupAssocItem>();
            }

            #region The resulting SQL statement should look a little somethin' like this
            //          SELECT LpePriceGroupId, lLpTemplateId, PricePolicyId, AssocOverrideTri 
            //              FROM 
            //                  (
            //                      SELECT PricePolicyId, ProductId, AssocOverrideTri FROM Product_Price_Association 
            //                          WHERE ProductId = '2FA25EE9-8505-47DF-8B10-15C68E31CAE8'
            //                          OR ProductId = '8EA6AAFD-0512-46A4-B2CE-8C83795ED96F'
            //                  ) AS a
            //              JOIN
            //                  ( 
            //                      SELECT LpePriceGroupId, lLpTemplateId FROM View_Lpe_Price_Group_Product 
            //                          WHERE LpePriceGroupId = '2FA25EE9-8505-47DF-8B10-15C68E31CAE8' 
            //                              AND lLpTemplateId IN ('A93311C7-F82C-4392-8420-0018EAD63BB0', 'FFE3837F-C332-41EA-BC16-04830B6F3C4B')
            //                          OR LpePriceGroupId = '8EA6AAFD-0512-46A4-B2CE-8C83795ED96F' 
            //                              AND lLpTemplateId IN ('A1E5F6A7-A3BC-427F-8561-8AD0C07480F7', 'E515DFFA-8BD9-4AA4-AE4E-95B94674B95E', '6C8210E2-09A7-4703-89B7-9CEED4D538CB')
            //                  ) AS b
            //              ON a.ProductId = b.LpePriceGroupId             
            //          WHERE AssocOverrideTri != '0'

            // 4/7/2015 dd - Due to the Database Split feature, we are no longer have the VIEW_LPE_PRICE_GROUP_PRODUCT 
            //               in Lpe database. Therefore we have to run two separate queries and join them manually.
            //              
            //               First Query
            //                      SELECT PricePolicyId, ProductId, AssocOverrideTri FROM Product_Price_Association 
            //                          WHERE AssocOverrideTri != 0 AND (ProductId = '2FA25EE9-8505-47DF-8B10-15C68E31CAE8'
            //                          OR ProductId = '8EA6AAFD-0512-46A4-B2CE-8C83795ED96F')
            //
            //               Second Query
            //                      SELECT LpePriceGroupId, lLpTemplateId FROM Lpe_Price_Group_Product 
            //                          WHERE LpePriceGroupId = '2FA25EE9-8505-47DF-8B10-15C68E31CAE8' 
            //                              AND lLpTemplateId IN ('A93311C7-F82C-4392-8420-0018EAD63BB0', 'FFE3837F-C332-41EA-BC16-04830B6F3C4B')
            //                          OR LpePriceGroupId = '8EA6AAFD-0512-46A4-B2CE-8C83795ED96F' 
            //                              AND lLpTemplateId IN ('A1E5F6A7-A3BC-427F-8561-8AD0C07480F7', 'E515DFFA-8BD9-4AA4-AE4E-95B94674B95E', '6C8210E2-09A7-4703-89B7-9CEED4D538CB') 
            // 
            //               We join based on FirstQuery.ProductId = SecondQuery.LpePriceGroupId
            #endregion

            List<LpGlobalPriceGroupAssocItem> firstResult = new List<LpGlobalPriceGroupAssocItem>();

            RetrieveGlobalPriceGroupAssociations(firstSqlParameters, firstWhereClause, firstResult);

            List<LpGlobalPriceGroupAssocItem> secondResult = new List<LpGlobalPriceGroupAssocItem>();

            RetrieveLpePriceGroupAssociations(secondSqlParameters, secondWhereClause, secondResult);

            // JOIN first result and second result ON first.ProductId = second.LpePriceGroupId
            List<LpGlobalPriceGroupAssocItem> finalResult = new List<LpGlobalPriceGroupAssocItem>();
            foreach (var first in firstResult)
            {
                foreach (var second in secondResult)
                {
                    if (first.ProductId == second.LpePriceGroupId)
                    {
                        LpGlobalPriceGroupAssocItem item = new LpGlobalPriceGroupAssocItem();
                        item.LpePriceGroupId = second.LpePriceGroupId;
                        item.lLpTemplateId = second.lLpTemplateId;
                        item.PricePolicyId = first.PricePolicyId;
                        item.AssocOverrideTri = first.AssocOverrideTri;
                        finalResult.Add(item);
                    }
                }
            }

            return finalResult;
        }

        public static void RetrieveLpePriceGroupAssociations(List<SqlParameter> parameters, StringBuilder whereClause, List<LpGlobalPriceGroupAssocItem> result)
        {
            string secondSql = "SELECT LpePriceGroupId, lLpTemplateId FROM Lpe_Price_Group_Product WHERE " + whereClause.ToString();

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    LpGlobalPriceGroupAssocItem item = new LpGlobalPriceGroupAssocItem();
                    item.LpePriceGroupId = (Guid)reader["LpePriceGroupId"];
                    item.lLpTemplateId = (Guid)reader["lLpTemplateId"];

                    result.Add(item);
                }
            };

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                // Need new parameter instance for each statement to avoid error due to parameter reuse.
                List<SqlParameter> secondSqlParametersCopy = new List<SqlParameter>();
                parameters.ForEach(p => secondSqlParametersCopy.Add(new SqlParameter(p.ParameterName, p.Value)));

                // 4/6/2015 dd - Have to run the query against ALL main databases.
                DBSelectUtility.ProcessDBData(connInfo, secondSql, null, secondSqlParametersCopy, readHandler);
            }
        }

        public static void RetrieveGlobalPriceGroupAssociations(List<SqlParameter> parameters, StringBuilder whereClause, List<LpGlobalPriceGroupAssocItem> result)
        {
            string firstSql = "SELECT PricePolicyId, ProductId, AssocOverrideTri FROM Product_Price_Association WHERE AssocOverrideTri != 0 AND (" + whereClause.ToString() + ")";

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    LpGlobalPriceGroupAssocItem item = new LpGlobalPriceGroupAssocItem();
                    item.PricePolicyId = (Guid)reader["PricePolicyId"];
                    item.ProductId = (Guid)reader["ProductId"];
                    item.AssocOverrideTri = (byte)reader["AssocOverrideTri"];

                    result.Add(item);
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.LpeSrc, firstSql, null, parameters, readHandler);
        }

        // OPM 21028
        /// <summary>
        /// Checks to see if there is a price group master program associated with the input price group (See OPM 21028).
        /// </summary>
        /// <param name="priceGroupId">The ID of the price group that needs to be checked to see if it has any policies associated with it
        /// via a price group master program.</param>
        /// <returns>True if there is a price group master program associated with the input price group, False if not.</returns>
        bool DoesPriceGroupHaveGlobalPolicies(Guid priceGroupId)
        {
            if (priceGroupId == Guid.Empty)
            {
                return false;
            }

            if (m_specialGlobalAssocPriceGroupIds != null && m_specialGlobalAssocPriceGroupIds.Contains(priceGroupId) == false)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Takes a list of programs and gathers policies that are associated with those programs directly inside of a Price Group.
        /// </summary>
        /// <param name="priceGroupMembers">The list of programs that are located in price groups which have policy associations within 
        /// them.  This does not necessarily mean that each program has a policy association, just that the price group it is contained
        /// within has some program-policy associations in it.</param>
        /// <returns>The policies that are directly associated with any of the input programs inside a Price Group.
        /// Each key is the Price Group ID/ Program ID pair, and the value is another hashtable containing
        /// all policies associated with the program (of the key pair) in the particular price group (of the key pair).</returns>
        Hashtable GetAssocLocalPolicies(ICollection priceGroupMembers)
        {
            Hashtable result = new Hashtable(priceGroupMembers.Count);
            DataTable dt = GetAssocLocalPoliciesFromDb(priceGroupMembers);

            foreach (DataRow row in dt.Rows)
            {
                PriceGroupMember member = new PriceGroupMember((Guid)row["ProgId"], (Guid)row["LpePriceGroupId"]);

                Hashtable policies = (Hashtable)result[member];

                if (policies == null)
                {
                    result[member] = policies = new Hashtable();
                }

                policies.Add((Guid)row["PricePolicyId"], row["AssociateType"]);
            }

            foreach (PriceGroupMember member in priceGroupMembers)
            {
                if (result.Contains(member) == false)
                {
                    result.Add(member, s_empty);
                }
            }

            return result;
        }

        // OPM 21028
        private Hashtable GetAssocPriceGroupWidePolicies(ICollection priceGroupMembers)
        {
            Hashtable result = new Hashtable(priceGroupMembers.Count);
            var assocPriceList = GetAssocPriceGroupWidePoliciesFromDb(priceGroupMembers);

            foreach (var row in assocPriceList)
            {
                PriceGroupMember member = new PriceGroupMember(row.lLpTemplateId, row.LpePriceGroupId);
                Hashtable policies = (Hashtable)result[member];

                if (policies == null)
                {
                    result[member] = policies = new Hashtable();
                }

                //policies.Add(row.PricePolicyId, row.AssocOverrideTri);
                policies[row.PricePolicyId] = row.AssocOverrideTri;
            }

            foreach (PriceGroupMember member in priceGroupMembers)
            {
                if (result.Contains(member) == false)
                {
                    result.Add(member, s_empty);
                }
            }

            return result;
        }

        bool CanPriceGroupHaveRateSheets(Guid priceGroupId)
        {
            if (priceGroupId == Guid.Empty)
            {
                return false;
            }

            return m_RateSheetOverridePriceGroups.Contains(priceGroupId);
        }


        private Dictionary<PriceGroupMember, AbstractRateSheetProxy> GetPriceGroupRateSheets(ICollection priceGroupMembers, out Dictionary<PriceGroupMember, Tuple<DateTime, DateTime>> downloadTimes)
        {
            Dictionary<PriceGroupMember, AbstractRateSheetProxy> result = GetPriceGroupRateSheetsFromDb(priceGroupMembers, out downloadTimes);

            foreach (PriceGroupMember member in priceGroupMembers)
            {
                if (result.ContainsKey(member) == false)
                {
                    result.Add(member, null); // Representing No Override.
                }
            }

            return result;
        }

        private Dictionary<PriceGroupMember, AbstractRateSheetProxy> GetPriceGroupRateSheetsFromDb(ICollection priceGroupMembers, out Dictionary<PriceGroupMember, Tuple<DateTime, DateTime>> downloadTimes)
        {
            Dictionary<PriceGroupMember, AbstractRateSheetProxy> result = new Dictionary<PriceGroupMember, AbstractRateSheetProxy>(priceGroupMembers.Count);
            downloadTimes = new Dictionary<PriceGroupMember, Tuple<DateTime, DateTime>>();

            if (priceGroupMembers.Count == 0) return result;

            string selectSql = "";

            List<Guid> overridePriceGroups = new List<Guid>();
            foreach (PriceGroupMember member in priceGroupMembers)
            {
                if (overridePriceGroups.Contains(member.PriceGroupId) == false)
                    overridePriceGroups.Add(member.PriceGroupId);
            }

            // The list of download times of all members we need.
            StringBuilder whereClause = new StringBuilder();
            foreach (Guid priceGroupId in overridePriceGroups)
            {
                ArrayList programIds = new ArrayList();
                foreach (PriceGroupMember member in priceGroupMembers)
                    if (member.PriceGroupId == priceGroupId)
                        programIds.Add(member.PrgId);

                whereClause.AppendFormat("{0} ( LpePriceGroupId = {1} AND ( lLpTemplateId {2} ) )"
                , whereClause.Length == 0 ? "" : " OR "
                , DbAccessUtils.SQLString(priceGroupId.ToString())
                , DbTools.InClauseForSql(programIds)
                );
            }

            if (whereClause.Length == 0)
            {
                // Should not be possible, but should not cause rach.
                Tools.LogBug("<lpe> Expect pricegroup member dates needed.");
                whereClause.Append(" WHERE lLpTemplateId = '0D4BADBE-62B9-4001-82F6-4E08AF1DD190' "); // Dummy.
            }

            // These are not recycled with the RateSheets because they can change when the RS does not.
            selectSql += "; select lLpTemplateId, LpePriceGroupId, lRateSheetDownloadStartD, lRateSheetDownloadEndD from RATE_OPTION_PRICEGROUP_PROGRAM_DOWNLOAD_TIME WHERE " + whereClause.ToString();

            HashSet<PriceGroupMember> currentRateSheetMembers = new HashSet<PriceGroupMember>();
            lock (this)
            {
                foreach (PriceGroupMember member in m_pgMemberApplicableRateSheet.Keys)
                    currentRateSheetMembers.Add(member);
            }

            // The list of all rs members we need (might be 0).
            whereClause = new StringBuilder();
            foreach (Guid priceGroupId in overridePriceGroups)
            {
                ArrayList programIds = new ArrayList();
                foreach (PriceGroupMember member in priceGroupMembers)
                {
                    if (currentRateSheetMembers.Contains(member))
                        continue; // Already loaded this RS.

                    if (member.PriceGroupId == priceGroupId)
                        programIds.Add(member.PrgId);
                }

                if (programIds.Count == 0) continue;

                whereClause.AppendFormat("{0} ( LpePriceGroupId = {1} AND ( lLpTemplateId {2} ) )"
                , whereClause.Length == 0 ? "" : " OR "
                , DbAccessUtils.SQLString(priceGroupId.ToString())
                , DbTools.InClauseForSql(programIds)
                );
            }

            if (whereClause.Length > 0)
                selectSql += "; select lLpTemplateId, LpePriceGroupId, lRateSheetXmlContent, LpeAcceptableRsFileId, LpeAcceptableRsFileVersionNumber, VersionTimestamp " +
                              " from RATE_OPTION_PRICEGROUP_PROGRAM WHERE " + whereClause.ToString();

            if (selectSql.Length > 0)
            {
                CDataSet ds = new CDataSet();
                ds.LoadWithSqlQueryDangerously(m_dataSrc, selectSql);


                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count != 0)
                {
                    HashSet<Tuple<DateTime, DateTime>> timeCache = new HashSet<Tuple<DateTime, DateTime>>(); // There will be repeating.
                    lock (this)
                    {
                        foreach (Tuple<DateTime, DateTime> timePair in m_pgMemberApplicableRateSheetDownloadTime.Values)
                            timeCache.Add(timePair);
                    }

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Guid lLpTemplateId = (Guid)row["lLpTemplateId"];
                        Guid LpePriceGroupId = (Guid)row["LpePriceGroupId"];
                        DateTime lRateSheetDownloadStartD = (DateTime)row["lRateSheetDownloadStartD"];
                        DateTime lRateSheetDownloadEndD = (DateTime)row["lRateSheetDownloadEndD"];

						foreach (PriceGroupMember member in priceGroupMembers)
						{
							if (member.PrgId == lLpTemplateId && member.PriceGroupId == LpePriceGroupId)
							{
								foreach (var timePair in timeCache)
								{
									if (timePair.Item1 == lRateSheetDownloadStartD && timePair.Item2 == lRateSheetDownloadEndD)
									{
										downloadTimes.Add(member, timePair);
										break;
									}
								}

								if (downloadTimes.ContainsKey(member) == false)
								{
									// Did not find in cache.
									var newEntry = Tuple.Create(lRateSheetDownloadStartD, lRateSheetDownloadEndD);
									timeCache.Add(newEntry);
									downloadTimes.Add(member, newEntry);
								}
							}
						}
					}
				}

				if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count != 0)
				{
					foreach (DataRow row in ds.Tables[1].Rows)
					{
						Guid lLpTemplateId = (Guid)row["lLpTemplateId"];
						Guid LpePriceGroupId = (Guid)row["LpePriceGroupId"];
						String lRateSheetXmlContent = row["lRateSheetXmlContent"] as String;
						string acceptableRsFileId = row["LpeAcceptableRsFileId"] as String;
						long versionNumber = (long)row["LpeAcceptableRsFileVersionNumber"];

						foreach (PriceGroupMember member in priceGroupMembers)
						{
							if (member.PrgId == lLpTemplateId && member.PriceGroupId == LpePriceGroupId)
							{
								result.Add(
								member,
								AbstractRateSheetProxy.Create(lRateSheetXmlContent, (byte[])row["VersionTimestamp"],
															   acceptableRsFileId, versionNumber,
															   m_rateSheetItemPoolTable)
								);
							}
						}
					}
				}
			}

			return result;
		}

        MissingCounters PrivateLoadMissingPriceGroupMembers(PriceGroupMemberHashtable missingMembers)
        {
            missingMembers.Verify();
            string threadName = LpeTools.GetCurThreadName("");

            CheckWritePermission();
            DateTime startTime = DateTime.Now;

            Hashtable resultPrograms = new Hashtable();
            MissingCounters missingCounters = LoadMissingPrograms(missingMembers, resultPrograms);

            // At this point, we've loaded all directly associated policies (set from the Loan Programs -> Rules page)
            // Per OPM 21028, we need to now grab policy associations specified per price group.  The first set of price group policy 
            // associations that is available (as of sometime before 5/15/09) is a direct association between policies and programs
            // in the price group.  The second set of associations that is being done for case 21028 is an association between an entire
            // Price Group and policies.  Policies associated with the entire price group will then be inherited by all programs inside
            // that price group.

            // Refine members are the set of programs that belong to a price group which contains policy associations 
            // between policies and programs inside that price group.  It doesn't necessarily mean that each particular program
            // in the refineMembers set has a direct price group level association to a policy.
            Hashtable refineMembers = new Hashtable(missingMembers.Count);
            Hashtable refineMembersForGlobalPGAssoc = new Hashtable(missingMembers.Count);
            Hashtable refineMembersForPGRateSheet = new Hashtable(missingMembers.Count);

            foreach (PriceGroupMember member in missingMembers.Keys)
            {
                if (m_loanProgramRawItems[member.PrgId] != null && CanPriceGroupHaveLocalPolicies(member.PriceGroupId) == true)
                {
                    refineMembers[member] = null;
                }

                // OPM 21028 db - Add Price Group Master programs
                if (m_loanProgramRawItems[member.PrgId] != null && DoesPriceGroupHaveGlobalPolicies(member.PriceGroupId) == true)
                {
                    refineMembersForGlobalPGAssoc[member] = null;
                }

                // OPM 108981 mf. RateSheet Overrides
                if (m_loanProgramRawItems[member.PrgId] != null && CanPriceGroupHaveRateSheets(member.PriceGroupId) == true)
                {
                    refineMembersForPGRateSheet[member] = null;
                }

            }

            Hashtable assocForPriceGroupMemberSet = GetAssocLocalPolicies(refineMembers.Keys);

            Hashtable assocForPriceGroupGlobalSet = new Hashtable();

            assocForPriceGroupGlobalSet = GetAssocPriceGroupWidePolicies(refineMembersForGlobalPGAssoc.Keys);
            
            Hashtable applicablePolicySets = new Hashtable();

            foreach (PriceGroupMember key in assocForPriceGroupMemberSet.Keys)
            {
                Object[] items = (Object[])m_loanProgramRawItems[key.PrgId];
                ArrayList directlyAssociatePolicies = (ArrayList)items[m_directPoliciesIdFldIdx];
                Hashtable assocForPriceGroupMember = (Hashtable)assocForPriceGroupMemberSet[key];
                Hashtable assocForPriceGroupGlobal = (Hashtable)assocForPriceGroupGlobalSet[key]; // OPM 21028
                CSortedListOfGroups applicablePolicies = MergeThreeKindsOfAssoc(directlyAssociatePolicies, assocForPriceGroupMember, assocForPriceGroupGlobal);

                ArrayList orderPolicyIds = applicablePolicies.MergedArrayBySortedOrder;
                orderPolicyIds.TrimToSize();
                applicablePolicySets.Add(key, orderPolicyIds);
            }

            // OPM 21028
            foreach (PriceGroupMember key in assocForPriceGroupGlobalSet.Keys)
            {
                if (!applicablePolicySets.ContainsKey(key))
                {
                    Object[] items = (Object[])m_loanProgramRawItems[key.PrgId];
                    ArrayList directlyAssociatePolicies = (ArrayList)items[m_directPoliciesIdFldIdx];
                    Hashtable assocForPriceGroupMember = (Hashtable)assocForPriceGroupMemberSet[key];
                    Hashtable assocForPriceGroupGlobal = (Hashtable)assocForPriceGroupGlobalSet[key]; 
                    CSortedListOfGroups applicablePolicies = MergeThreeKindsOfAssoc(directlyAssociatePolicies, assocForPriceGroupMember, assocForPriceGroupGlobal);

                    ArrayList orderPolicyIds = applicablePolicies.MergedArrayBySortedOrder;
                    orderPolicyIds.TrimToSize();
                    applicablePolicySets.Add(key, orderPolicyIds);
                }
            }

            missingCounters.NumPriceGroupMembers = applicablePolicySets.Count;

            // load policies
            missingCounters.NumPolicies += LoadApplicablePolicies(applicablePolicySets);

            // load ratesheet overrides
            Dictionary<PriceGroupMember, Tuple<DateTime,DateTime>> downloadTimes;
            var rateOptionOverrides = GetPriceGroupRateSheets(refineMembersForPGRateSheet.Keys, out downloadTimes);

            Hashtable newHomePolicies = new Hashtable();
            lock (this)
            {
                foreach (PriceGroupMember key in applicablePolicySets.Keys)
                {
                    ArrayList applicablePolicies = (ArrayList)applicablePolicySets[key];
                    applicablePolicies = ArrayList.ReadOnly(m_policyRelationDb.StandardizePolicies(applicablePolicies));

                    m_pgMemberApplicablePolicySet[key] = applicablePolicies;

                    Object[] items = (Object[])m_loanProgramRawItems[key.PrgId];
                    string investorName = (string)items[m_investorNameFldIdx];

                    foreach (Object id in applicablePolicies)
                    {
                        newHomePolicies[id] = null;
                        m_homePolicies[id] = null;
                    }
                }

                foreach (var overrideRateSheet in rateOptionOverrides)
                {
                    m_pgMemberApplicableRateSheet[overrideRateSheet.Key] = overrideRateSheet.Value;
                }

                foreach (PriceGroupMember member in downloadTimes.Keys)
                {
                    m_pgMemberApplicableRateSheetDownloadTime[member] = downloadTimes[member];
                }

            }

            PolicyManager.SetHomePolicies(newHomePolicies, true);

            if (ConstSite.VerboseLogEnable4Pricing || (DateTime.Now - startTime).TotalSeconds >= 10)
            {
                Tools.LogRegTest(string.Format("<LPE_load> {0} : finish to load {5} pgMembers, {1} missing programs, {2} missing ratesheets, {3} missing policies in {4}",
                    threadName, missingCounters.NumLoanPrograms, missingCounters.NumRateSheets, missingCounters.NumPolicies,
                    CommonLib.TimeTool.GetDurationStringInMs(startTime), missingCounters.NumPriceGroupMembers));
            }

            return missingCounters;
        }

        int LoadApplicablePolicies(Hashtable applicablePolicySets)
        {
            CheckWritePermission();
            CountTable countTable = new CountTable();

            foreach (ArrayList arrayPolicyIds in applicablePolicySets.Values)
            {
                countTable.AccumulateCount(arrayPolicyIds);
            }

            // OPM 26013. PMI Policies have to be loaded.  They are not related to programs, but are always applicable.
            // They are global and independent.  SAEs indicate we expect this to be a small set.
            countTable.AccumulateCount( new ArrayList(m_PmiManager.PmiPolicyIds.ToArray()));

            return PolicyManager.LoadPolicies(countTable /*distinctPolicyIds*/ );
        }

        // load missing loan record, ratesheet and global policies.
        MissingCounters LoadMissingPrograms(PriceGroupMemberHashtable missingMembers, Hashtable resultPrograms)
        {
            /*  Missing PriceGroupMember  when
             *  - program PriceGroupMember.PrgId not in memory ( => associate policies of program in price group not in memory too)
             *  - program PriceGroupMember in memory but associate policies of program in price group not in memory
             * 
             * This method is used to load program only. Local policies will be loaded by other method
             */
            resultPrograms.Clear();
            CheckWritePermission();

            Hashtable programIds = new Hashtable(missingMembers.Count);
            foreach (PriceGroupMember member in missingMembers.Keys)
            {
                if (m_loanProgramRawItems[member.PrgId] == null)
                {
                    programIds[member.PrgId] = null;
                }
            }

            if (programIds.Count == 0)
            {
                // The required loan programs in memory already, they're just missing local policies, which will be loaded in a separate function
                return new MissingCounters();
            }

            string threadName = LpeTools.GetCurThreadName("");

            DateTime startTime = DateTime.Now;
            DataSet ds = GetLoanProgramsFromDb(programIds, m_dataSrc, false /* getPolicyOnly = false*/);
            DateTime version = Convert.ToDateTime(ds.Tables[0].Rows[0]["LastUpdatedD"]);

            // If the Last Updated date of the data doesn't match the currently used snapshot date, throw an error
            if (0 != m_versionD.CompareTo(version))
            {
                throw new CBaseException(ErrorMessages.Generic, string.Format("Mismatch versions in LoadMissingPrograms(Hashtable programIds). Expect ver = {0} {1} from {2} but get ver = {3} {4}",
                    m_versionD.ToLongTimeString(), m_versionD.ToShortDateString(),
                    m_dataSrc, version.ToLongTimeString(), version.ToShortDateString()));
            }

            const int LoanProgramTableIdx = 1;
            const int DirectPoliciesTableIdx = 2;

            DataRowCollection rowsLoanPrograms = ds.Tables[LoanProgramTableIdx].Rows;
            int numPrograms = rowsLoanPrograms.Count;

            // get directly associated policies 
            Hashtable directlyAssociatePolicies = new Hashtable(rowsLoanPrograms.Count);
            foreach (DataRow row in rowsLoanPrograms)
            {
                directlyAssociatePolicies.Add((Guid)row["lLpTemplateId"], new ArrayList());
            }

            foreach (DataRow row in ds.Tables[DirectPoliciesTableIdx].Rows)
            {
                Guid progId = (Guid)row["ProductId"];
                Guid policyId = (Guid)row["PricePolicyID"];
                ((ArrayList)directlyAssociatePolicies[progId]).Add(policyId);
            }

            // calculate applicable policies
            // Given the set of policies associated with each requested program, determine all the other policies that are associated
            // For example, parent policies of the directly associated policies will be added to the list.
            Hashtable applicablePolicySets = new Hashtable(rowsLoanPrograms.Count);
            foreach (DataRow row in rowsLoanPrograms)
            {
                object progId = row["lLpTemplateId"];

                ArrayList directPolicies = (ArrayList)directlyAssociatePolicies[progId];

                CSortedListOfGroups appicablePolicies = m_policyRelationDb.GetApplicablePolicies(PolicyManager, directPolicies);
                ArrayList orderPolicyIds = appicablePolicies.MergedArrayBySortedOrder;
                orderPolicyIds.TrimToSize();
                applicablePolicySets.Add(progId, orderPolicyIds);
            }

            // Load Ratesheet Data from Rate_Options table (rates, prices, margins) for each program
            DataTable productTable = ds.Tables[LoanProgramTableIdx];
            Hashtable rateSheetIds = new Hashtable(productTable.Rows.Count);
            foreach (DataRow row in productTable.Rows)
            {
                rateSheetIds[row["SrcRateoptionsProgId"]] = null;
            }

            int missingRateSheetCount = GetRateSheets(rateSheetIds);

            // load policies.
            int missingPolicyCount = LoadApplicablePolicies(applicablePolicySets);

            // Add table's columns
            productTable.Columns.Add("RateSheetProxy", typeof(AbstractRateSheetProxy));
            productTable.Columns.Add("ProductId", typeof(Guid));
            productTable.Columns.Add("DirectPolicies", typeof(ArrayList));
            productTable.Columns.Add("Policies", typeof(ArrayList));

            foreach (DataRow row in productTable.Rows)
            {
                row["RateSheetProxy"] = rateSheetIds[row["SrcRateoptionsProgId"]];
                row["DirectPolicies"] = m_policyRelationDb.StandardizePolicies((ArrayList)directlyAssociatePolicies[row["lLpTemplateId"]]);
                row["Policies"] = ArrayList.ReadOnly(m_policyRelationDb.StandardizePolicies((ArrayList)applicablePolicySets[row["lLpTemplateId"]]));

                string investorName = ((string)row["lLpInvestorNm"]).TrimWhitespaceAndBOM();  //.ToUpper();
                row["lLpInvestorNm"] = investorName;

                row["ProductId"] = s_productIdProvider.GetProductId(investorName,
                                                                        (string)row["ProductCode"],
                                                                        (Guid)row["BrokerId"]);
                row.AcceptChanges();
            }

            productTable.AcceptChanges();
            DbTools.RemoveTables(ds);
            if (m_investorNameFldIdx == -1)
            {
                m_investorNameFldIdx = productTable.Columns.IndexOf("lLpInvestorNm");
                m_productIdFldIdx = productTable.Columns.IndexOf("ProductId");
                m_productCodeFldIdx = productTable.Columns.IndexOf("ProductCode");
                m_brokerIdFldIdx = productTable.Columns.IndexOf("BrokerId");
            }

            Hashtable newHomePolicies = new Hashtable();
            lock (this)
            {
                foreach (DataRow row in productTable.Rows)
                {
                    Object prgId = row["lLpTemplateId"];

                    Object[] items = row.ItemArray;
                    resultPrograms.Add(prgId, items);
                    m_loanProgramRawItems.Add(prgId, items);
                    string investorName = (string)items[m_investorNameFldIdx];

                    m_productAccessTime.EntryEnters(items[m_productIdFldIdx], startTime);

                    ArrayList applicablePolicies = (ArrayList)applicablePolicySets[prgId];
                    if (m_homePrograms.Contains(prgId) == false) // home policy
                    {
                        m_homePrograms[prgId] = null;
                        foreach (Object id in applicablePolicies)
                        {
                            newHomePolicies[id] = null;
                            m_homePolicies[id] = null;
                        }
                    }
                }
                PolicyManager.SetHomePolicies(newHomePolicies, true);

                if (m_loanProgramSchema == null)
                {
                    DbTools.RemoveTables(ds);
                    productTable.Clear();
                    productTable.AcceptChanges();

                    DataTable schema = productTable.Clone();

                    m_productIdFldIdx = schema.Columns.IndexOf("ProductId");
                    m_productCodeFldIdx = schema.Columns.IndexOf("ProductCode");
                    m_brokerIdFldIdx = schema.Columns.IndexOf("BrokerId");

                    m_srcRateSheetFldIdx = schema.Columns.IndexOf("SrcRateoptionsProgId"); ;
                    m_policiesFldIdx = schema.Columns.IndexOf("Policies");
                    m_programNameFldIdx = schema.Columns.IndexOf("lLpTemplateNm");
                    m_lenderNameFldIdx = schema.Columns.IndexOf("lLendNm");
                    m_investorNameFldIdx = schema.Columns.IndexOf("lLpInvestorNm");
                    m_programIdFldIdx = schema.Columns.IndexOf("lLpTemplateId");
                    m_directPoliciesIdFldIdx = schema.Columns.IndexOf("DirectPolicies");
                    m_rateSheetProxyFldIdx = schema.Columns.IndexOf("RateSheetProxy");
                    m_rateSheetDownloadTimeStartDFldIdx = schema.Columns.IndexOf("lRateSheetDownloadStartD");
                    m_rateSheetDownloadTimeEndDFldIdx = schema.Columns.IndexOf("lRateSheetDownloadEndD");

                    if (schema.DataSet != null)
                    {
                        Tools.LogBug("Internal error : m_loanProgramSchema.DataSet != null");
                        throw new CBaseException(ErrorMessages.Generic, "Internal error : m_loanProgramSchema.DataSet != null");
                    }
                    m_loanProgramSchema = schema;
                }
            }

            foreach (Guid prgId in programIds.Keys)
            {
                if (m_loanProgramRawItems.Contains(prgId) == false)
                {
                    m_invalidLoanPrograms[prgId] = null;
                    Tools.LogRegTest(string.Format("<LPE_error> ignore invalid loanProgramId {0} *** snapshot ver = {1}", prgId, SnapshotStr()));
                }
            }

            MissingCounters missingCounters = new MissingCounters();
            missingCounters.NumLoanPrograms = programIds.Count;
            missingCounters.NumRateSheets = missingRateSheetCount;
            missingCounters.NumPolicies = missingPolicyCount;

            return missingCounters;
        }

        /// <summary>
        /// Retrieves policy (and loan program if specified) information from the database.  Policies are  
        /// loaded for requested programs that:
        /// 1) Are not Masters
        /// 2) Are enabled
        /// 3) Are Lpe = true
        /// AND the policy must be at least one of the following:
        /// 1) Directly associated
        /// 2) Inherited from base
        /// 3) Otherwise inherited
        ///.
        /// Program data is loaded if getPolicyOnly == false and the requested program is:
        /// 1) Not a Master
        /// 2) Enabled
        /// 3) Is Lpe = true
        /// </summary>
        /// <param name="prgIds">List of programs to load from the database</param>
        /// <param name="dataSrc">The database DataSrc</param>
        /// <param name="getPolicyOnly">If set to false, this function will load all loan program and
        /// policy data from the Loan_Program_Templatetable.  If set to true, only data related
        /// to the policies associated with the programs specified by the prgIds parameter will be loaded.</param>
        /// <returns>The data set containing all associated policy data, and program data if getPolicyOnly == false</returns>
        static private DataSet GetLoanProgramsFromDb(Hashtable prgIds, DataSrc dataSrc, bool getPolicyOnly)
        {
            var prgIdsNEW = new HashSet<Guid>();

            if (prgIds != null)
            {
                foreach (var item in prgIds)
                {
                    if (item is DictionaryEntry)
                    {
                        var entry = (DictionaryEntry)item;
                        prgIdsNEW.Add((Guid)entry.Key);
                    }
                }
            }

            return GetLoanProgramsFromDb(prgIdsNEW, dataSrc, getPolicyOnly);
        }

        static private DataSet GetLoanProgramsFromDb(HashSet<Guid> prgIds, DataSrc dataSrc, bool getPolicyOnly)
        {
            if (prgIds.Count <= 0)
            {
                Tools.LogBug("Internal error : don't expect GetLoanProgramsFromDb(...) with prgIds.Count = 0");
                return null;
            }

			var parameters = new List<SqlParameter>(prgIds.Count);

			string inClause = DbTools.CreateParameterized4InClauseSql("Po", prgIds, parameters);

            StringBuilder selectSql = new StringBuilder(" exec GetPriceEngineVersionDate ");

            if (getPolicyOnly == false)
            {
                //RetrieveAllLoanProductsByBrokerID_WithoutRateSheetXml{1} @BrokerId={0}, @IsEnabled=1, @IsLpe=1
                selectSql.Append("; SELECT Loan_Program_Template.*, lRateSheetDownloadStartD, lRateSheetDownloadEndD\n" +
                    "FROM Loan_Program_Template\n" +
                    "  join RATE_OPTIONS_DOWNLOAD_TIME  on LOAN_PROGRAM_TEMPLATE.SrcRateoptionsProgId = RATE_OPTIONS_DOWNLOAD_TIME.SrcProgId\n" +
                    "  WHERE IsMaster = 0 and IsEnabled = 1 and IsLpe = 1 and lLpTemplateId ");
                selectSql.Append(inClause); // in (  '3cfec542-ea05-4bb8-a711-cfe72b59288b',  '8ad7f6d4-6642-4696-9bb6-aa71a6a9ef10' )
            }
            else
            {
                selectSql.Append("; SELECT 1 as dummy");
            }

            // RetrievePricePoliciesDirectlyAssociatedToLoanProductsOfBrokerId{1} @BrokerId={0}, @IsEnabledProducts=1
            // ";\nSELECT pa.ProductId, p.PricePolicyId, p.PricePolicyDescription, p.MutualExclusiveExecSortedId, lLpTemplateNm " +
            selectSql.Append(
                ";\nSELECT pa.ProductId, p.PricePolicyId " +
                "FROM ( Product_Price_Association pa JOIN Price_Policy p ON pa.PricePolicyID = p.PricePolicyId )              " +
                "   join loan_program_template product on pa.productId = product.lLpTemplateId                                " +
                "WHERE                               \n" +
                "           product.IsMaster = 0     \n" +
                "   and     product.IsEnabled = 1    \n" +
                "   and     product.IsLpe = 1        \n" +
                "   and     pa.AssocOverrideTri != 2 \n" +
                "   and ( pa.InheritVal = 1 or pa.AssocOverrideTri = 1 or pa.InheritValFromBaseProd = 1 ) \n" +
                "   and product.lLpTemplateId ");
            selectSql.Append(inClause); // in (  '3cfec542-ea05-4bb8-a711-cfe72b59288b',  '8ad7f6d4-6642-4696-9bb6-aa71a6a9ef10' )

            if (s_displaySqlOfGetLoanProgramsFromDb == true && getPolicyOnly == false)
            {
                Tools.LogRegTest("<lpe> GetLoanProgramsFromDb :\n" + selectSql.ToString());
                s_displaySqlOfGetLoanProgramsFromDb = false;
            }

			DataSet ds = new DataSet();
			DBSelectUtility.FillDataSet(dataSrc, ds, selectSql.ToString(), TimeoutInSeconds.Sixty, parameters);

			return ds;
		}

		internal MissingCounters LoadPriceGroupMembers(IList pgMembers, Hashtable resultMemberPriorities)
        {
            CheckWritePermission();

            if (resultMemberPriorities != null)
            {
                resultMemberPriorities.Clear();
            }

            // Using hashtable, because want to remove duplicate ids in productIds
            PriceGroupMemberHashtable missingMembers = null;

            lock (this)
            {
                foreach (PriceGroupMember member in pgMembers)
                {
                    if (GetApplicablePoliciesEx(member) == null || GetRateSheetEx(member) == null)
                    {
                        if (missingMembers == null)
                        {
                            missingMembers = new PriceGroupMemberHashtable();
                        }

                        missingMembers.Add(member, null);
                    }
                }
            }

            MissingCounters missingCounters = new MissingCounters();
            if (missingMembers != null)
            {
                missingCounters = LoadMissingPriceGroupMembers(missingMembers);
            }

            if (resultMemberPriorities != null)
            {
                GetPriceGroupMemberPriorities(pgMembers, resultMemberPriorities);
            }

            return missingCounters;
        }

        internal void GetCurProgramsPriorities(Hashtable programPriorities)
        {
            programPriorities.Clear();
            lock (this)
            {
                foreach (Guid prgId in m_loanProgramRawItems.Keys)
                {
                    Object[] arrItems = (Object[])m_loanProgramRawItems[prgId];
                    if (arrItems == null)
                    {
                        continue;
                    }

                    programPriorities[prgId] = 1; // 2016-07-27 - dd - Investor priority will always be 1.
                }
            }
        }

        internal void GetCurProgramIds(ArrayList programIds)
        {
            programIds.Clear();
            lock (this)
            {
                programIds.AddRange(m_loanProgramRawItems.Keys);
            }
        }

        private void GetPriceGroupMemberPriorities(ICollection memberss, Hashtable memberPriorities)
        {
            memberPriorities.Clear();
            lock (this)
            {
                foreach (PriceGroupMember member in memberss)
                {
                    Object[] arrItems = (Object[])m_loanProgramRawItems[member.PrgId];
                    if (arrItems == null)
                    {
                        continue;
                    }

                    memberPriorities[member] = 1; // 2016-07-27 - dd - Priority will always be 1.
                }
            }
        }

        internal void GetMissingPriceGroupMembers(PriceGroupMemberArrayList members, PriceGroupMemberArrayList missingMembers, ArrayList invalidProgramIds)
        {
            missingMembers.Clear();
            invalidProgramIds.Clear();
            lock (this)
            {
                foreach (PriceGroupMember member in members)
                {
                    if (m_invalidLoanPrograms.Contains(member.PrgId) == true)
                    {
                        invalidProgramIds.Add(member.PrgId);
                    }
                    else if (GetApplicablePoliciesEx(member) == null || GetRateSheetEx(member) == null)
                    {
                        missingMembers.Add(member);
                    }
                }
            }
        }

        internal void SetAccessTimeForPrograms(Hashtable programAccessTime)
        {
            Hashtable processed = new Hashtable();
            try
            {
                lock (this)
                {
                    foreach (Guid prgId in programAccessTime.Keys)
                    {
                        Object[] arrItems = (Object[])m_loanProgramRawItems[prgId];
                        if (arrItems == null)
                        {
                            continue;
                        }

                        DateTime now = (DateTime)programAccessTime[prgId];
                        Guid productId = (Guid)arrItems[m_productIdFldIdx];
                        if (processed.Contains(productId) == true)
                        {
                            DateTime prev = (DateTime)processed[productId];
                            if (prev >= now)
                            {
                                continue;
                            }
                        }
                        m_productAccessTime.EntryEnters(productId, now);
                        processed[productId] = now;
                    }
                }
            }
            catch (Exception exc)
            {
                Tools.LogError("SetAccessTimeForPrograms error", exc);
            }
        }

        // caller needs "using lock(this)"
        ArrayList GetApplicablePoliciesEx(PriceGroupMember member)
        {
            // If we already loaded them, return associated policies of this member
            ArrayList result = (ArrayList)m_pgMemberApplicablePolicySet[member];
            if (result != null)
            {
                return result;
            }

            // If this member is not in a pricegroup that can have policy associations, 
            // we can use the direct program associations if they are loaded.
            if (CanPriceGroupHavePolicyAssociation(member.PriceGroupId) == false)
            {
                object[] arrItems = (object[])m_loanProgramRawItems[member.PrgId];
                if (arrItems != null)
                {
                    return arrItems[m_policiesFldIdx] as ArrayList;
                }
            }

            return null; // Missing member.  Needs to be loaded.
        }

        private bool CanPriceGroupHavePolicyAssociation(Guid priceGroupId)
        {
            if (CanPriceGroupHaveLocalPolicies(priceGroupId))
            {
                return true; // This Price Group participates in at least one Program-In-PriceGroup to Policy association
            }

            if (DoesPriceGroupHaveGlobalPolicies(priceGroupId))
            {
                return true;  // This Price Group participates in at least one PriceGroup to Policy association
            }

            return false; // This pricegroup is clean of any sort of policy associations.
        }

        AbstractRateSheetProxy GetRateSheetEx(PriceGroupMember member)
        {
            // If we already loaded it, return ratesheet of this member
            AbstractRateSheetProxy result = (AbstractRateSheetProxy)m_pgMemberApplicableRateSheet[member];
            if (result != null)
            {
                return result;
            }

            // If this member is not in a pricegroup that can have rate sheet overrides,
            // or we already found it is not overridden, use the program's src rate otption
            if (CanPriceGroupHaveRateSheets(member.PriceGroupId) == false
                || m_pgMemberApplicableRateSheet.ContainsKey(member))
            {
                object[] arrItems = (object[])m_loanProgramRawItems[member.PrgId];
                if (arrItems != null)
                {
                    return arrItems[m_rateSheetProxyFldIdx] as AbstractRateSheetProxy;
                }
            }

            return null; // Missing member.  Needs to be loaded.
        }


        internal LoanProgramSet CreateLoanProgramSet(PriceGroupMemberArrayList members, PriceGroupMemberArrayList missingMembers)
        {
            DateTime now = DateTime.Now;

            missingMembers.Clear();
            DataTable table;
            lock (this)
            {
                foreach (PriceGroupMember member in members)
                {
                    if (GetApplicablePoliciesEx(member) == null || GetRateSheetEx(member) == null || m_loanProgramSchema == null)
                    {
                        missingMembers.Add(member);
                    }
                    else
                    {
                        object[] arrItems = (object[])m_loanProgramRawItems[member.PrgId];
                        if (arrItems == null)
                        {
                            Tools.LogBug("<LpGlobalError> m_loanProgramRawItems[ prgId ] == null with prgId = " + member.PrgId.ToString());
                            missingMembers.Add(member);
                        }
                        else
                        {
                            m_productAccessTime.EntryEnters(arrItems[m_productIdFldIdx], now);
                        }
                    }
                }

                if (missingMembers.Count != 0)
                {
                    return null;
                }

                table = m_loanProgramSchema.Copy();

                Object[] items = null; ;
                foreach (PriceGroupMember member in members)
                {
                    Object[] arrItems = (Object[])m_loanProgramRawItems[member.PrgId];
                    if (items == null)
                    {
                        items = new Object[arrItems.Length];
                    }

                    for (int i = 0; i < arrItems.Length; i++)
                    {
                        items[i] = arrItems[i];
                    }

                    items[m_policiesFldIdx] = GetApplicablePoliciesEx(member);
                    items[m_rateSheetProxyFldIdx] = GetRateSheetEx(member);

                    if (m_RateSheetOverridePriceGroups.Contains(member.PriceGroupId) && m_pgMemberApplicableRateSheetDownloadTime.ContainsKey(member))
                    {
                        Tuple<DateTime, DateTime> dates = (Tuple<DateTime, DateTime>)m_pgMemberApplicableRateSheetDownloadTime[member];
                        items[m_rateSheetDownloadTimeStartDFldIdx] = dates.Item1;
                        items[m_rateSheetDownloadTimeEndDFldIdx] = dates.Item2;
                    }

                    table.LoadDataRow(items, true);
                }
            }

            return new LoanProgramSet(this, table);
        }

        internal void UnloadProgramsNotInThisList(ICollection programIds, IGetIdleTimeInfo idleTimeProvider)
        {
            CheckWritePermission();

            Hashtable allow = new Hashtable(programIds.Count);
            foreach (Object programId in programIds)
            {
                allow[programId] = null;
            }

            Hashtable remove = new Hashtable();

            lock (this)
            {
                ArrayList currentPrgIds = new ArrayList(m_loanProgramRawItems.Keys);
                foreach (Object programId in currentPrgIds)
                {
                    if (allow.ContainsKey(programId) == false)
                    {
                        remove[programId] = null;
                    }
                }
                UnloadPrograms(remove.Keys, idleTimeProvider);
            }
        }

        void ClearCaptureData()
        {
            if (m_clearCaptureDataEnable == false)
            {
                return;
            }
            m_clearCaptureDataEnable = false;

            CheckWritePermission();

            Tools.LogRegTest("<LPE_CACHING> " + ReportCaching());

            int unloadPolicyCounter = m_policyManager.UnloadSomeFrozenPolicies();

            Hashtable rateSheetIds = new Hashtable();
            foreach (Object[] arrItems in m_loanProgramRawItems.Values)
            {
                rateSheetIds[(Guid)arrItems[m_srcRateSheetFldIdx]] = null;
            }

            int numRemovedRateSheet = ClearRateSheetsNotInList(rateSheetIds);

            if (unloadPolicyCounter != 0 || numRemovedRateSheet != 0)
            {

                Tools.LogRegTest(string.Format("<LPE_unload*> {0} : unload {1} frozen policies, {2} rate sheets",
                    Thread.CurrentThread.Name, unloadPolicyCounter, numRemovedRateSheet));

                Tools.LogRegTest("<LPE_CACHING> " + ReportCaching());
            }
        }

        internal void UnloadPrograms(ICollection programIds, IGetIdleTimeInfo idleTimeProvider)
        {
            CheckWritePermission();

            ArrayList remove = new ArrayList();
            int numRemovedRateSheet, numRemovePolicies, numRemoveMembers, numRemoveMemberRateSheets;

            lock (this)
            {
                foreach (Object programId in programIds)
                {
                    if (m_loanProgramRawItems.ContainsKey(programId) == true)
                    {
                        Object[] items = (Object[])m_loanProgramRawItems[programId];

                        remove.Add(m_loanProgramRawItems[programId]);
                        m_loanProgramRawItems.Remove(programId);
                    }
                }

                if (remove.Count == 0)
                {
                    return;
                }

                Hashtable rateSheetIds = new Hashtable();
                foreach (Object[] arrItems in m_loanProgramRawItems.Values)
                {
                    rateSheetIds[(Guid)arrItems[m_srcRateSheetFldIdx]] = null;
                }
                numRemovedRateSheet = ClearRateSheetsNotInList(rateSheetIds);

                // PriceGroup Ratesheets
                numRemoveMemberRateSheets = 0;
                ArrayList pgMembersRateSheet = new ArrayList(m_pgMemberApplicableRateSheet.Keys);
                foreach (PriceGroupMember member in m_pgMemberApplicableRateSheet.Keys)
                {
                    if (m_loanProgramRawItems.Contains(member.PrgId))
                    {
                        continue;
                    }
                    numRemoveMemberRateSheets++;
                    m_pgMemberApplicableRateSheet.Remove(member);
                    m_pgMemberApplicableRateSheetDownloadTime.Remove(member);
                }

                CountTable countTable = new CountTable();

                numRemoveMembers = 0;

                // local and price group-wide policies
                ArrayList pgMembers = new ArrayList(m_pgMemberApplicablePolicySet.Keys);
                foreach (PriceGroupMember member in pgMembers)
                {
                    if (m_loanProgramRawItems.Contains(member.PrgId))
                    {
                        continue;
                    }

                    ArrayList applicablePolicies = (ArrayList)m_pgMemberApplicablePolicySet[member];
                    m_pgMemberApplicablePolicySet.Remove(member);
                    numRemoveMembers++;

                    countTable.AccumulateCount(applicablePolicies);
                }

                // global policies
                foreach (Object[] arrItems in remove)
                {
                    ArrayList arrayPolicyIds = (ArrayList)arrItems[m_policiesFldIdx];
                    countTable.AccumulateCount(arrayPolicyIds);
                }

                numRemovePolicies = PolicyManager.UnloadPolicies(countTable /*distinctPolicyIds*/ );
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<LPE_unload> {3} : Unload {4} pgMembers, {5} pgMemberRateSheets {0} programs, {1} rateSheets, {2} policies\n" +
                "idx. InvestorName : priority *** productName *** prgName *** prgId *** product's idle time in seconds"
                , remove.Count, numRemovedRateSheet, numRemovePolicies, Thread.CurrentThread.Name, numRemoveMembers, numRemoveMemberRateSheets);

            DateTime now = DateTime.Now;

            int index = 0;
            foreach (Object[] arrItems in remove)
            {
                index++;
                if (ConstSite.LpeDisplayDetail == false && index != 1 && index != remove.Count)
                {
                    continue;
                }

                Object prgId = arrItems[m_programIdFldIdx];
                string prgName = (String)arrItems[m_programNameFldIdx];
                string investorName = (String)arrItems[m_investorNameFldIdx];
                string product = (String)arrItems[m_productCodeFldIdx];

                sb.AppendFormat("\n{0, 3}. {1, -16} : {2} *** {3} *** {4} *** {5}",
                    index, investorName, product, prgName, prgId,
                    m_productAccessTime.GetAbsIdleTimeInfo(arrItems[m_productIdFldIdx]));

            }
            Tools.LogRegTest(sb.ToString());
        }

        void CheckWritePermission()
        {
            if (m_permissionWriteThread != null && Thread.CurrentThread != m_permissionWriteThread)
            {
                string threadName = LpeTools.GetCurThreadName("");
                string errMsg = string.Format("Thread '{0}' doesn't have permission to edit LpGlobal object ver = {1}.",
                                                threadName, SnapshotStr());
                Tools.LogBug(string.Format("<LPE> {0} : {1}", ConstAppDavid.ServerName, errMsg));
                throw new CBaseException(ErrorMessages.Generic, errMsg);
            }

            if (m_debugCheckWritePermission && m_permissionWriteThread != null)
            {
                Tools.LogRegTest(string.Format("<LPE_WRITE> {0} : only '{1}' has permission to edit lpGlobal version = {2}.",
                                        ConstAppDavid.ServerName,
                                        m_permissionWriteThread.Name != null ? m_permissionWriteThread.Name : "",
                                        SnapshotStr()));
                m_debugCheckWritePermission = false;
            }
        }

        internal void SetOnlyEditableFrom(Thread thread)
        {
            m_permissionWriteThread = thread;
        }

        void LoadHomePolicies()
        {
            CheckWritePermission();
        }

        internal string ReportCaching()
        {
            string productReport = "";
            return ReportCaching(out productReport);
        }

        internal string ReportCaching(out string productReport)
        {
            CheckWritePermission();
            string str;
            lock (this)
            {
                productReport = m_productAccessTime.Report();

                int numPrgs = m_loanProgramRawItems.Count;
                int numRateSheet = m_rateSheetTable.Count;
                int numPriceGroupMembers = m_pgMemberApplicablePolicySet.Count;
                int numPriceGroupRateSheetMembers = m_pgMemberApplicableRateSheet.Count;

                RuleCategoryCounter ruleCategoryCounter = new RuleCategoryCounter();
                int numCachedHomePolicies = 0;
                int cachedPolicies = PolicyManager.GetNumCachedPolicies(out numCachedHomePolicies, ref ruleCategoryCounter);

                str = string.Format("#pgMembers = {5}, #pgMembersRateSheet #programs = {0}, #rateSheet = {1}, #policies =  {2}, #homePolicies = {3} in memory, ver = {4}",
                                    numPrgs, numRateSheet, cachedPolicies, numCachedHomePolicies, SnapshotStr(), numPriceGroupMembers, numPriceGroupRateSheetMembers);

                try
                {
                    CLpGlobalSizeInfo sizeInfo = GetSizeInfo();
                    str = str + ". LPE ~ " + sizeInfo.TotalSizeInKB
                               + " [ policiesSize ~ " + sizeInfo.PoliciesSizeInKB
                               + ", programsSize ~ " + sizeInfo.ProgramsSizeInKB
                               + ", including frozen policies = " + sizeInfo.FrozenPoliciesSizeInKB
                               + ",  frozen rate-sheets = " + sizeInfo.FrozenRateSheetsSizeInKB
                               + " ]"
                               + ". #LoadedRule = " + ruleCategoryCounter.LoadedRuleCounter.ToString()
                               + ", #invariantRule = " + ruleCategoryCounter.InvariantRuleCounter.ToString()
                               + ", #qvalueRule = " + ruleCategoryCounter.QValueRuleCounter.ToString()
                               + ", #variantRule = " + ruleCategoryCounter.VariantRuleCounter.ToString()
                               + ", #dtiRuleCounter = " + ruleCategoryCounter.DtiRuleCounter.ToString()
                               + ", " + ruleCategoryCounter.PolicyNotCallLoadRules_Counter + " policies don't call LoadRules().";
                }
                catch (Exception exc)
                {
                    Tools.LogError("<LPE_error> can not run estimateSize() ", exc);
                }

            }
            return str;
        }

        internal int EstimateLpeSize()
        {
            try
            {
                CLpGlobalSizeInfo sizeInfo = GetSizeInfo();
                return sizeInfo.TotalSize;
            }
            catch (Exception exc)
            {
                Tools.LogError("<LPE_error> can not run estimateSize() ", exc);
                return -1;
            }
        }

        internal CLpGlobalSizeInfo GetSizeInfo()
        {
            CheckWritePermission();

            CLpGlobalSizeInfo sizeInfo = new CLpGlobalSizeInfo();
            if (m_loanProgramSchema == null)
            {
                return sizeInfo;
            }

            sizeInfo.PoliciesSize = PolicyManager.EstimateSize(out sizeInfo.FrozenPoliciesSize);

            // relationship size for price group members
            sizeInfo.PrgAndPolicyRelationshipSize = 0;
            int wasteSize, totalWasteSize = 0;
            foreach (ArrayList applicablePolicies in m_pgMemberApplicablePolicySet.Values)
            {
                sizeInfo.PrgAndPolicyRelationshipSize += m_policyRelationDb.EstimateSizeOfPolicyIds(applicablePolicies, out wasteSize);
                totalWasteSize += wasteSize;
            }

            int sizeOfLocalPolicyIds = sizeInfo.PrgAndPolicyRelationshipSize;

            // loan program size
            Hashtable rateSheets = new Hashtable();
            sizeInfo.ProgramsSize = 0;
            foreach (Object[] arrItems in m_loanProgramRawItems.Values)
            {
                if (arrItems == null)
                {
                    continue;
                }

                sizeInfo.ProgramsSize += 12 + 4 * arrItems.Length + 956 + (arrItems.Length - 2);

                ArrayList directPolicyIds = (ArrayList)arrItems[m_directPoliciesIdFldIdx];
                if (directPolicyIds != null)
                {
                    //sizeInfo.ProgramsSize += 28 /* size of ArrayList */ + (16 /* guid size */ + 12 /* box size */ ) * directPolicyIds.Count;
                    sizeInfo.ProgramsSize += m_policyRelationDb.EstimateSizeOfPolicyIds(directPolicyIds, out wasteSize);
                    totalWasteSize += wasteSize;
                }

                ArrayList globalPolicyIds = (ArrayList)arrItems[m_policiesFldIdx];
                if (globalPolicyIds != null)
                {
                    sizeInfo.ProgramsSize += m_policyRelationDb.EstimateSizeOfPolicyIds(globalPolicyIds, out wasteSize);
                    totalWasteSize += wasteSize;
                }

                AbstractRateSheetProxy proxy = (AbstractRateSheetProxy)arrItems[m_rateSheetProxyFldIdx];
                if (rateSheets.Contains(proxy) == false)
                {
                    rateSheets[proxy] = null;
                    sizeInfo.ProgramsSize += proxy.EstimateSize();
                }
            }

            sizeInfo.FrozenRateSheetsSize = 0;
            foreach (AbstractRateSheetProxy proxyObject in this.m_rateSheetTable.Values)
            {
                if (rateSheets.Contains(proxyObject) == false)
                {
                    rateSheets[proxyObject] = null;

                    int size = proxyObject.EstimateSize();
                    sizeInfo.FrozenRateSheetsSize += size;
                    sizeInfo.ProgramsSize += size;
                }
            }

            return sizeInfo;
        }

        internal class CompareByProduct : IComparer
        {
            Hashtable m_orderProducts;
            int m_productIdFldIdx;

            // condition : productIds is sorted by idle time, from largest to smallest.
            internal CompareByProduct(ArrayList productIds, int productIdFldIdx)
            {
                m_productIdFldIdx = productIdFldIdx;
                m_orderProducts = new Hashtable(productIds.Count);
                for (int i = 0; i < productIds.Count; i++)
                {
                    m_orderProducts[productIds[i]] = i;
                }
            }

            int IComparer.Compare(Object x, Object y)
            {
                Object[] xItem = (Object[])x;
                Object[] yItem = (Object[])y;

                int xOrder = (int)m_orderProducts[xItem[m_productIdFldIdx]];
                int yOrder = (int)m_orderProducts[yItem[m_productIdFldIdx]];

                return xOrder - yOrder;
            }

        }

        internal void ReduceMemoryIfNeed(IGetIdleTimeInfo idleTimeProvider)
        {
            const int NumPrgsKeepInMemory = CompilerConst.IsDebugMode ? 800 : 1600;
            lock (this)
            {
                int memSize = EstimateMemory();
                int numCachedPrgs = GetNumProgramsInMemory();

                if (memSize < 0)
                {
                    if (numCachedPrgs <= LpeDataProvider.MaxLpePrograms)
                    {
                        ClearCaptureData();
                        return;
                    }
                }
                else if (memSize <= LpeDataProvider.MaxLpeMemory)
                {
                    return;
                }
                else if (numCachedPrgs <= NumPrgsKeepInMemory)
                {
                    ClearCaptureData();
                    return;
                }

                if (ConstStage.AdjustPolicyCounterEnable)
                    AdjustPolicyCounters();
                VerifyPolicyCounters();

                if (m_clearCaptureDataEnable)
                {
                    ClearCaptureData();

                    memSize = EstimateMemory();
                    if (memSize >= 0 && memSize <= LpeDataProvider.MaxLpeMemory)
                    {
                        return;
                    }
                }
                else
                {
                    Tools.LogRegTest("<LPE_CACHING> " + ReportCaching());
                }

                int unloadHomePolicyCount = m_policyManager.UnloadSomeFrozenPolicies();
                if (unloadHomePolicyCount > 0)
                {
                    Tools.LogRegTest(string.Format("<LPE_unload> {0} : unload {1} frozen policies",
                                       Thread.CurrentThread.Name, unloadHomePolicyCount));
                    memSize = EstimateMemory();
                    if (memSize >= 0 && memSize <= LpeDataProvider.MaxLpeMemory)
                    {
                        Tools.LogRegTest("<LPE_CACHING> " + ReportCaching());
                        return;
                    }
                }

                const int OneMB = 1000 * 1000;

                ++m_reduceLpeCounter;
                StringBuilder sb = new StringBuilder(300);
                sb = sb.AppendFormat("<LPE> {0} {5}. Estimate Lpe = {1} KB > MaxLpeMemory = {2} MB. Will reduce such that Lpe size < MinLpeMemory = {3} MB or #loan programs < {4}.",
                                Thread.CurrentThread.Name, (memSize / 1000).ToString("N0"),
                                LpeDataProvider.MaxLpeMemory / OneMB, LpeDataProvider.MinLpeMemory / OneMB,
                                NumPrgsKeepInMemory, m_reduceLpeCounter);
                sb.AppendFormat(" [prev at {0} ago]", CommonLib.TimeTool.GetDurationString(m_reduceLpeTime));
                m_reduceLpeTime = DateTime.Now;

                Tools.LogRegTest(sb.ToString());

                // sort loanProgramRawItems by product's idle time.
                ArrayList productIds = new ArrayList(m_loanProgramRawItems.Count / 4);
                m_productAccessTime.GetRecentEntries(productIds, m_loanProgramRawItems.Count);
                CompareByProduct cmpObj = new CompareByProduct(productIds, m_productIdFldIdx);
                ArrayList prgs = new ArrayList(m_loanProgramRawItems.Values);
                prgs.Sort(cmpObj);

                ArrayList candidateProgramIds = new ArrayList(prgs.Count);
                foreach (Object[] arrItem in prgs)
                {
                    candidateProgramIds.Add(arrItem[m_programIdFldIdx]);
                }

                prgs = null;

                if (memSize < 0)
                {
                    if (candidateProgramIds.Count > LpeDataProvider.MaxLpePrograms)
                    {
                        candidateProgramIds.RemoveRange(LpeDataProvider.MinLpePrograms, candidateProgramIds.Count - LpeDataProvider.MinLpePrograms);
                        UnloadProgramsNotInThisList(candidateProgramIds, idleTimeProvider);
                        Tools.LogRegTest("<LPE_CACHING> " + ReportCaching());
                        SendReportToWebservice();
                    }
                    return;
                }

                ArrayList remove = new ArrayList(100);
                while (memSize > 0 && memSize > LpeDataProvider.MinLpeMemory)
                {
                    numCachedPrgs = GetNumProgramsInMemory();
                    if (numCachedPrgs <= NumPrgsKeepInMemory || candidateProgramIds.Count <= NumPrgsKeepInMemory)
                    {
                        break;
                    }

                    // remove last 100 programs.
                    //candidateProgramIds.RemoveRange( candidateProgramIds.Count - 100, 100 );
                    remove.Clear();
                    for (int i = 0; i < 100; i++)
                    {
                        remove.Add(candidateProgramIds[candidateProgramIds.Count - 1]);
                        candidateProgramIds.RemoveAt(candidateProgramIds.Count - 1);
                    }

                    UnloadPrograms(remove, idleTimeProvider);
                    memSize = EstimateMemory();
                }
                Tools.LogRegTest("<LPE_CACHING> " + ReportCaching());
            }
            VerifyPolicyCounters();
        }

        public CountTable GetPolicyCounters()
        {
            lock (this)
            {
                CountTable countTable = new CountTable();

                foreach (ArrayList arrayPolicyIds in m_pgMemberApplicablePolicySet.Values)
                {
                    countTable.AccumulateCount(arrayPolicyIds);
                }

                foreach (Object[] arrItems in m_loanProgramRawItems.Values)
                {
                    ArrayList arrayPolicyIds = (ArrayList)arrItems[m_policiesFldIdx];
                    countTable.AccumulateCount(arrayPolicyIds);
                }

                return countTable;
            }
        }

        void VerifyPolicyCounters()
        {
            if (CompilerConst.IsReleaseModeVar)
            {
                return;
            }

            CheckWritePermission();

            try
            {
                CountTable actualCounters = PolicyManager.GetPolicyCounters();
                CountTable expectCounters = GetPolicyCounters();

                StringBuilder sb = new StringBuilder();
                if (expectCounters.Count != actualCounters.Count)
                {
                    sb.AppendFormat("Count : {0} vs {1}\r\n", expectCounters.Count, actualCounters.Count);
                }
                foreach (Guid policyId in expectCounters.Keys)
                {
                    Integer g = expectCounters[policyId];
                    Integer p = actualCounters[policyId];
                    if (p == null)
                    {
                        sb.AppendFormat("Policy {0} vs empty : PolicyManager doesn't load this policy\r\n", policyId);
                    }
                    else if (g.num != p.num)
                    {
                        sb.AppendFormat("Policy {0} 's counter : {1} vs {2}\r\n", policyId, g.num, p.num);
                    }
                }

                if (sb.Length > 0)
                {
                    sb.Insert(0, "[LpGlobal vs PolicyManager]\r\n");
                    Tools.LogBug("VerifyPolicyCounters() error : " + sb.ToString());
                }
            }
            finally
            {
            }
        }

        // opm 22473 : Policy counters are not correct if exception happens during loading policy process.
        void AdjustPolicyCounters()
        {
            CheckWritePermission();
            try
            {
                CountTable expectCounters = GetPolicyCounters();
                PolicyManager.AdjustPolicyCounters(expectCounters);
            }
            catch (Exception exc)
            {
                Tools.LogError("CLpGlobal.AdjustPolicyCounters() error", exc);
            }
        }

        public Dictionary<Guid, List<RtRateMarginItem>> GetTpoRateMargin(Guid brokerId, int pmlBrokerIdx)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@PmlBrokerIndexNumber", pmlBrokerIdx)
            };

            byte[] ratesMarginProtobufContent = null;
            using (var reader = StoredProcedureHelper.ExecuteReader(m_dataSrc, "RATE_OPTIONS_MARGIN_PML_BROKER_GetByBrokerAndPmIdx", parameters))
            {
                // don't throw exception if cannot find record
                if (reader.Read())
                {
                    ratesMarginProtobufContent = reader["RatesMarginProtobufContent"] == DBNull.Value ? null : (byte[])reader["RatesMarginProtobufContent"];
                }

                reader.NextResult();
                reader.Read();

                DateTime version = Convert.ToDateTime(reader["LastUpdatedD"]);

                // If the Last Updated date of the data doesn't match the currently used snapshot date, throw an error
                if (0 != m_versionD.CompareTo(version))
                {
                    throw new CBaseException(ErrorMessages.Generic, string.Format("Mismatch versions in GetTpoRateMargin(...). Expect ver = {0} {1} from {2} but get ver = {3} {4}",
                        m_versionD.ToLongTimeString(), m_versionD.ToShortDateString(),
                        m_dataSrc, version.ToLongTimeString(), version.ToShortDateString()));
                }
            }

            var dict = new Dictionary<Guid, List<RtRateMarginItem>>(); // productId => RtRateMarignItem list
            if (ratesMarginProtobufContent != null)
            {
                List<RatesMargin> ratesMargins = SerializationHelper.ProtobufDeserialize<List<RatesMargin>>((byte[])ratesMarginProtobufContent);
                foreach( var ratesMargin in ratesMargins)
                {
                    var rtItems = new List<RtRateMarginItem>();
                    foreach(var item in ratesMargin.Items)
                    {
                        decimal noteRate, margin;
                        if( decimal.TryParse(item.NoteRate, out noteRate) &&  decimal.TryParse(item.Margin, out margin) ) 
                        {
                            rtItems.Add(new RtRateMarginItem()
                                        {
                                            NoteRate = noteRate,
                                            Margin = margin
                                        }
                            );
                        }
                    }

                    dict.Add(ratesMargin.lLpTemplateId, rtItems);
                }
            }

            return dict;
        }
    }
}