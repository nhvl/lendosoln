﻿namespace LendersOffice.RatePrice
{
    /// <summary>
    /// List of possible type for pricing engine storage.
    /// </summary>
    public enum PriceEngineStorageType
    {
        /// <summary>
        /// The price policy.
        /// </summary>
        PricePolicy = 0,

        /// <summary>
        /// The rate options.
        /// </summary>
        RateOptions = 1,

        /// <summary>
        /// The pricing snap shot.
        /// </summary>
        PricingSnapshot = 2,

        /// <summary>
        /// The lender snap shot.
        /// </summary>
        PerLenderSnapshot = 3,

        /// <summary>
        /// All Price Policies of a snapshot.
        /// </summary>
        PricePolicySnapshot = 4,
    }
}
