using System;
using System.Text;
using System.Collections;
using System.Data;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice
{
    public class LpeDataVerify
    {

        static DataRow CaclSrcPtr(Hashtable hashTable, Guid idRow)
        {
            DataRow row = (DataRow)hashTable[idRow];
            if (row["computedSrc"] != DBNull.Value)
                return row;

            if (row["parentId"] != DBNull.Value)
            {
                DataRow parentRow = CaclSrcPtr(hashTable, (Guid)row["parentId"]);
                row["computedSrcInherit"] = parentRow["computedSrc"];
            }

            if ((bool)row["ovr"] == true)
                row["computedSrc"] = idRow;
            else
                row["computedSrc"] = row["computedSrcInherit"];

            if (row["computedSrc"] == DBNull.Value)
                throw new CBaseException(ErrorMessages.Generic, "row['computedSrc'] == null with productId = " + idRow);

            return row;
        }

        static public void PrintInfo(Hashtable hashTable, Guid idRow, StringBuilder sb)
        {
            string ident = "    ";
            sb.AppendFormat("\n{0}  lLpTemplateId, parentId, OverrideBit, SrcId, Inherit", ident);
            while (true)
            {
                DataRow row = (DataRow)hashTable[idRow];
                sb.AppendFormat("\n{0}  id = {1}, parent = {2}, ovr = {3}, SrcId = {4}, inherit = {5}",
                    ident, row["Id"], row["parentId"], row["ovr"], row["Src"], row["SrcInherit"]);
                if (row["parentId"] == DBNull.Value)
                    return;

                idRow = (Guid)row["parentId"];
            }
        }

        static public string SrcPtrVerify()
        {
            DataSet ds = RetrieveLoanProgramTemplates();

            //Assert.AreNotEqual(0, ds.Tables[0].Rows.Count);

            Hashtable hashTable = new Hashtable(ds.Tables[0].Rows.Count);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                row["computedSrc"] = DBNull.Value;
                row["computedSrcInherit"] = DBNull.Value;
                hashTable[(Guid)row["Id"]] = row;
            }

            foreach (Guid id in hashTable.Keys)
            {
                CaclSrcPtr(hashTable, id);
            }

            StringBuilder sb = new StringBuilder();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                bool err = false;
                if ((Guid)row["computedSrc"] != (Guid)row["Src"])
                {
                    sb.Append("\nSrcRateOptionsProgId error with id = " + row["Id"]);
                    err = true;
                }
                if (row["computedSrcInherit"].Equals(row["SrcInherit"]) == false)
                {
                    sb.Append("\nSrcRateOptionsProgIdInherit error with id = " + row["Id"]);
                    err = true;

                }
                if (err)
                    PrintInfo(hashTable, (Guid)row["Id"], sb);
                //Assert.AreEqual( row["computedSrcInherit"], row["SrcInherit"], "SrcInherit error with id = " + row["Id"]);
            }
            if (sb.Length == 0)
                sb.Append("Success to verify two fields SrcRateOptionsProgId and SrcRateOptionsProgIdInherit in LOAN_PROGRAM_TEMPLATE table");
            return sb.ToString();
        }

        public static DataSet RetrieveLoanProgramTemplates()
        {
            string sql = "SELECT lLpTemplateId as Id, lBaseLpId as parentId, lRateSheetXmlContentOverrideBit as ovr, " +
                "SrcRateOptionsProgId Src, SrcRateOptionsProgIdInherit SrcInherit, " +
                "SrcRateOptionsProgId as computedSrc, SrcRateOptionsProgIdInherit computedSrcInherit " +
                "from Loan_Program_Template;";

            DataSet ds = new DataSet();
            DBSelectUtility.FillDataSet(DataSrc.LpeSrc, ds, sql, null, null);
            return ds;
        }
    }

}
