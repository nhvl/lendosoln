﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace LendersOfficeApp.los.RatePrice
{
    internal class ReqProgramsMsg // POD : Plain Old Data 
    {
        internal ReqProgramsMsg(PriceGroupMemberArrayList programs)
        {
            Programs = programs;
        }
        internal PriceGroupMemberArrayList Programs;
        internal readonly ManualResetEvent AnswerEvent = new ManualResetEvent(false);
    }
}
