﻿namespace LendersOffice.RatePrice
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Net;
    using DataAccess;
    using LendersOffice.Security;
    using LendersOfficeApp.los.RatePrice;

    /// <summary>
    /// Determines what type of programs are to be shown in the UI.
    /// </summary>
    public enum AssociationTypeForUi
    {
        Program = 0,
        PriceGroup = 1,
        ProgramInPriceGroup = 2,
        ProgramInPriceGroupBatch = 3,
        PmiProduct = 4
    }

    /// <summary>
    /// This class contains method for generating the "state" object used in PricePolicyListByProduct.aspx.
    /// </summary>
    public class LoanProgramTreeGenerator
    {
        /// <summary>
        /// Generates the "state" object for mortgage insurance programs.
        /// </summary>
        /// <param name="pmiProductId">The mortgage insurance product id.</param>
        /// <param name="collapseToAssociated">Determine whether the tree should be collapsed.</param>
        /// <returns>Returns an object representing the tree state.</returns>
        public static object DisplayPmiProgramAssocTree(Guid pmiProductId, bool collapseToAssociated)
        {
            var ds = new DataSet();
            DataSetHelper.Fill(ds, DataSrc.LpeSrc, "ListPricePolicyByPmiProductId", new[] { new SqlParameter("@PmiProductID", pmiProductId) });

            var policies = new List<object>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                var PricePolicyID = (Guid)row["PricePolicyID"];
                var ParentPolicyId = row["ParentPolicyId"] == DBNull.Value ? null : (Guid?)row["ParentPolicyId"];
                var PricePolicyDescription = WebUtility.HtmlEncode((string)row["PricePolicyDescription"]);
                var IsPublished = (bool)row["IsPublished"];
                var AssocOverrideTri = row["AssocOverrideTri"] is DBNull ? E_TriState.Blank : (E_TriState)(byte)row["AssocOverrideTri"];
                var InheritVal = !(row["InheritVal"] is DBNull) && (bool)row["InheritVal"];
                var IsExpandedNode = (bool)row["IsExpandedNode"];

                var hasAssociations = AssocOverrideTri == E_TriState.Yes || AssocOverrideTri == E_TriState.No || InheritVal;
                var parentExpanded = collapseToAssociated && hasAssociations;
                var childExpanded = !collapseToAssociated && (bool)row["IsExpandedNode"];
                var ySel = IsPublished && AssocOverrideTri == E_TriState.Yes;
                var nSel = IsPublished && AssocOverrideTri == E_TriState.No;

                policies.Add(new
                {
                    PricePolicyID,
                    ParentPolicyId,
                    PricePolicyDescription,
                    IsPublished,
                    AssocOverrideTri,
                    InheritVal,
                    IsExpandedNode,

                    hasAssociations,
                    parentExpanded,
                    childExpanded,

                    ySel,
                    nSel,
                });
            }

            return new
            {
                mode = AssociationTypeForUi.PmiProduct,
                policies,
            };
        }

        /// <summary>
        /// Generates the "state" object for loan programs.
        /// </summary>
        /// <param name="loanProductId">The loan product id.</param>
        /// <param name="collapseToAssociated">Determine whether the tree should be collapsed.</param>
        /// <returns>Returns an object representing the tree state.</returns>
        public static object DisplayProgramAssocTree(Guid loanProductId, bool collapseToAssociated)
        {
            var isMaster = CheckIsMaster(loanProductId);
            var currentUser = PrincipalFactory.CurrentPrincipal;

            var fnm = new ForceNoManager(currentUser.BrokerId);

            var ds = new DataSet();
            DataSetHelper.Fill(ds, DataSrc.LpeSrc, "ListPricePolicyByLoanProductId", new[] { new SqlParameter("@LoanProductID", loanProductId) });

            var policies = new List<object>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                var PricePolicyID = (Guid)row["PricePolicyID"];
                var ParentPolicyId = row["ParentPolicyId"] == DBNull.Value ? null : (Guid?)row["ParentPolicyId"];
                var PricePolicyDescription = WebUtility.HtmlEncode((string)row["PricePolicyDescription"]);
                var IsPublished = (bool)row["IsPublished"];
                var AssocOverrideTri = row["AssocOverrideTri"] is DBNull ? E_TriState.Blank : (E_TriState)(byte)row["AssocOverrideTri"];
                var InheritVal = !(row["InheritVal"] is DBNull) && (bool)row["InheritVal"];
                var InheritValFromBaseProd = !(row["InheritValFromBaseProd"] is DBNull) && (bool)row["InheritValFromBaseProd"];
                var IsForcedNo = !(row["IsForcedNo"] is DBNull) && (bool)row["IsForcedNo"];
                var IsExpandedNode = (bool)row["IsExpandedNode"];

                var FNProgramId = IsPublished ? fnm.IsFnAssoc(PricePolicyID, loanProductId) : Guid.Empty;

                var hasAssociations =
                    (AssocOverrideTri == E_TriState.Yes || AssocOverrideTri == E_TriState.No) ||
                    InheritValFromBaseProd ||
                    InheritVal ||
                    (IsPublished && isMaster && IsForcedNo);

                var parentExpanded = collapseToAssociated && hasAssociations;
                var childExpanded = !collapseToAssociated && IsExpandedNode;

                var forceNoText = (FNProgramId != Guid.Empty) ? " ( Disassociated via master )" : string.Empty;

                var ySel = IsPublished && (AssocOverrideTri == E_TriState.Yes) && !(isMaster && FNProgramId != Guid.Empty);
                var yDis = IsPublished && (FNProgramId != Guid.Empty);

                var nSel = IsPublished && (AssocOverrideTri == E_TriState.No) && !(isMaster && FNProgramId != Guid.Empty);
                var nDis = IsPublished && (FNProgramId != Guid.Empty);
                var bSel = IsPublished && InheritValFromBaseProd;
                var mSel = IsPublished && InheritVal;
                var forceNo = IsPublished && (isMaster && (IsForcedNo || (FNProgramId != Guid.Empty)));
                var forceNoDisable = IsPublished && (isMaster && FNProgramId != Guid.Empty);

                policies.Add(new
                {
                    PricePolicyID,
                    ParentPolicyId,
                    PricePolicyDescription,
                    IsPublished,
                    AssocOverrideTri,
                    InheritVal,
                    InheritValFromBaseProd,
                    IsForcedNo,
                    IsExpandedNode,

                    FNProgramId,

                    hasAssociations,
                    parentExpanded,
                    childExpanded,

                    ySel,
                    yDis,
                    nSel,
                    nDis,
                    bSel,
                    mSel,
                    forceNo,
                    forceNoDisable,
                });
            }

            return new
            {
                mode = AssociationTypeForUi.Program,
                isMaster,

                policies,
            };
        }

        /// <summary>
        /// Generates the "state" object for loan programs.
        /// </summary>
        /// <param name="isBatchMode">Determines whether the tree is displayed for a batch operation.</param>
        /// <param name="loanProductId">The loan product id.</param>
        /// <param name="priceGroupId">The price group product id.</param>
        /// <param name="collapseToAssociated">Determine whether the tree should be collapsed.</param>
        /// <returns>Returns an object representing the tree state.</returns>
        public static object DisplayProgramInPriceGroupAssocTree(bool isBatchMode, Guid loanProductId, Guid priceGroupId, bool collapseToAssociated)
        {
            var ds = new DataSet();
            if (!isBatchMode)
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@LoanProductId", loanProductId),
                    new SqlParameter("@PriceGroupId", priceGroupId)
                };
                DataSetHelper.Fill(ds, DataSrc.LpeSrc, "ListPricePolicyByLoanProductIdPriceGroupId", parameters);
            }
            else
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@PriceGroupId", priceGroupId)
                };

                DataSetHelper.Fill(ds, DataSrc.LpeSrc, "ListPricePolicyByPriceGroupId" /* Batch mode can still use the PG associations */, parameters);
            }

            var policies = new List<object>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                var PricePolicyID = (Guid)row["PricePolicyID"];
                var ParentPolicyId = row["ParentPolicyId"] == DBNull.Value ? null : (Guid?)row["ParentPolicyId"];
                var PricePolicyDescription = WebUtility.HtmlEncode((string)row["PricePolicyDescription"]);

                var IsPublished = (bool)row["IsPublished"];
                var AssocOverrideTri = (isBatchMode || row["AssocOverrideTri"] is DBNull) ? E_TriState.Blank : (E_TriState)(byte)row["AssocOverrideTri"];
                var PriceGroupAssociateType = row["PriceGroupAssociateType"] is DBNull ? E_TriState.Blank : (E_TriState)(byte)row["PriceGroupAssociateType"];
                var PriceGroupProgramAssociateType = (isBatchMode || row["PriceGroupProgramAssociateType"] is DBNull) ? E_TriState.Blank : (E_TriState)(byte)row["PriceGroupProgramAssociateType"];
                var InheritVal = !isBatchMode && !(row["InheritVal"] is DBNull) && (bool)row["InheritVal"];
                var InheritValFromBaseProd = !isBatchMode && !(row["InheritValFromBaseProd"] is DBNull) && (bool)row["InheritValFromBaseProd"];
                var IsExpandedNode = (bool)row["IsExpandedNode"];

                var hasAssociations = IsPublished && (
                    (PriceGroupAssociateType == E_TriState.Yes || PriceGroupAssociateType == E_TriState.No) ||
                    (!isBatchMode && (
                        (PriceGroupProgramAssociateType == E_TriState.Yes || PriceGroupProgramAssociateType == E_TriState.No) ||
                        ((AssocOverrideTri == E_TriState.Yes) || (AssocOverrideTri == E_TriState.Blank && (InheritValFromBaseProd || InheritVal))))));

                var parentExpanded = collapseToAssociated && 
                    (hasAssociations ||
                        (!IsPublished && !isBatchMode && PriceGroupProgramAssociateType != E_TriState.Blank));

                var childExpaned = !collapseToAssociated && IsExpandedNode;

                var ySel = IsPublished && !isBatchMode && PriceGroupProgramAssociateType == E_TriState.Yes;
                var nSel = IsPublished && !isBatchMode && PriceGroupProgramAssociateType == E_TriState.No;
                var lSel = IsPublished && !isBatchMode && ((AssocOverrideTri == E_TriState.Yes) || (AssocOverrideTri == E_TriState.Blank && (InheritValFromBaseProd || InheritVal)));

                var pGySel = IsPublished && PriceGroupAssociateType == E_TriState.Yes;
                var pGnSel = IsPublished && PriceGroupAssociateType == E_TriState.No;

                policies.Add(new
                {
                    PricePolicyID,
                    ParentPolicyId,
                    PricePolicyDescription,
                    IsPublished,
                    IsExpandedNode,
                    AssocOverrideTri,
                    PriceGroupAssociateType,
                    PriceGroupProgramAssociateType,
                    InheritVal,
                    InheritValFromBaseProd,

                    hasAssociations,
                    parentExpanded,
                    childExpaned,

                    ySel,
                    nSel,
                    lSel,
                    pGySel,
                    pGnSel,
                });
            }

            return new
            {
                mode = isBatchMode ? AssociationTypeForUi.ProgramInPriceGroupBatch : AssociationTypeForUi.ProgramInPriceGroup,
                policies,
            };
        }

        /// <summary>
        /// Generates the "state" object for loan programs.
        /// </summary>
        /// <param name="priceGroupId">The price group product id.</param>
        /// <param name="collapseToAssociated">Determine whether the tree should be collapsed.</param>
        /// <returns>Returns an object representing the tree state.</returns>
        public static object DisplayPriceGroupAssocTree(Guid priceGroupId, bool collapseToAssociated)
        {
            var ds = new DataSet();
            DataSetHelper.Fill(ds, DataSrc.LpeSrc, "ListPricePolicyByPriceGroupId", new[] { new SqlParameter("@PriceGroupId", priceGroupId) });

            var policies = new List<object>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                var PricePolicyID = (Guid)row["PricePolicyID"];
                var ParentPolicyId = row["ParentPolicyId"] == DBNull.Value ? null : row["ParentPolicyId"];
                var IsPublished = (bool)row["IsPublished"];
                var PriceGroupAssociateType = row["PriceGroupAssociateType"] is DBNull ? E_TriState.Blank : (E_TriState)(byte)row["PriceGroupAssociateType"];
                var PricePolicyDescription = WebUtility.HtmlEncode((string)row["PricePolicyDescription"]);
                var IsExpandedNode = (bool)row["IsExpandedNode"];

                var parentExpanded = (PriceGroupAssociateType != E_TriState.Blank) && (!IsPublished || collapseToAssociated);
                var childExpaned = !collapseToAssociated && IsExpandedNode;
                var ySel = IsPublished && PriceGroupAssociateType == E_TriState.Yes;
                var nSel = IsPublished && PriceGroupAssociateType == E_TriState.No;

                policies.Add(new
                {
                    PricePolicyID,
                    ParentPolicyId,
                    PricePolicyDescription,

                    IsPublished,
                    PriceGroupAssociateType,
                    IsExpandedNode,

                    childExpaned,
                    parentExpanded,
                    ySel,
                    nSel,
                });
            }

            return new
            {
                mode = AssociationTypeForUi.PriceGroup,
                policies,
            };
        }

        /// <summary>
        /// Checks if the loan program is a master program.
        /// </summary>
        /// <param name="fileId">The file id.</param>
        /// <returns>Returns a value determining whether the program is a master program.</returns>
        public static bool CheckIsMaster(Guid fileId)
        {
            bool isMaster = false;
            DBSelectUtility.ProcessDBData(
                DataSrc.LpeSrc,
                "SELECT IsMaster FROM Loan_Program_Template WHERE lLpTemplateId = @p AND IsMaster = 1 AND IsMasterPriceGroup = 0",
                null,
                new[] { new SqlParameter("@p", fileId) }, 
                reader => isMaster = reader.Read());

            return isMaster;
        }
    }
}