﻿namespace LendersOffice.RatePrice
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.ManualInvestorProductExpiration;
    using LendersOffice.ObjLib.PriceGroups;
    using LendersOffice.RatePrice.FileBasedPricing;
    using LendersOfficeApp.los.RatePrice;

    /// <summary>
    /// Get a list of loan program from file snapshot.
    /// </summary>
    internal class ListLoanProgramsFromFile
    {
        /// <summary>
        /// Get a list of loan program from file snapshot to that satisfy the filter parameter.
        /// </summary>
        /// <param name="snapshot">The file based snapshot.</param>
        /// <param name="priceGroup">The price group to retrieve loan program.</param>
        /// <param name="firstLienLpId">The id of first lien loan program.</param>
        /// <param name="parameters">Search criteria.</param>
        /// <param name="preserveProductCode">Whether to preserve the product code.</param>
        /// <returns>The set of loan programs inside price group that match filter.</returns>
        public static LoanProgramByInvestorSet ListUsingSnapshot(FileBasedSnapshot snapshot, PriceGroup priceGroup, Guid firstLienLpId, LoanProgramFilterParameters parameters, bool preserveProductCode = false)
        {
            LoanProgramByInvestorSet lpSet = new LoanProgramByInvestorSet();

            HashSet<Guid> includeSet = new HashSet<Guid>();

            if (priceGroup.ExternalPriceGroupEnabled == false)
            {
                return lpSet;
            }

            Guid brokerId = priceGroup.BrokerID;
            Guid priceGroupId = priceGroup.ID;

            foreach (var o in PriceGroupProduct.ListByPriceGroupId(brokerId, priceGroupId))
            {
                if (o.IsValid)
                {
                    includeSet.Add(o.LpTemplateId);
                }
            }

            /*order of filter precedence:
             * 0)manualExcludeProductList 
             * 1)onlyIncludeLpProductType (only programs of this 1 product type will be included)
             * 2)
             *  -onlyIncludeProductTypes (only programs of these product types will be included)
             *  -ignoreProductTypes (programs of these types will not be included)
             * 3)includeProductTypes
             * 4)ignoreLoanTypes (unless includeProductTypes (but not onlyIncludeProductTypes) includes it.)
            */

            ProductPairingTable productPairingTable = null;

            if (Guid.Empty != firstLienLpId)
            {
                // Need to perform product pairing
                productPairingTable = ProductPairingTable.RetrieveProductPairingTableFromSnapshot(snapshot);
            }

            List<InvestorProductItem> manualExcludeProductList = CLoanProgramsFromDb.GetManualExcludeProductList(brokerId, priceGroup.LockPolicyID.Value);

            foreach (var id in includeSet)
            {
                FileBasedLoanProgramTemplate loanProgram = null;
                if (!snapshot.TryGetLoanProgramTemplate(id, out loanProgram))
                {
                    continue; // Could not find the loan program template in snapshot.
                }

                // Perform filter check on loan program.
                if (loanProgram.IsEnabled == false || loanProgram.IsMaster == true || loanProgram.IsLpe == false)
                {
                    continue;
                }

                if (parameters.LienOption == CLoanProgramsFromDb.E_Option.Only1stLien && loanProgram.lLienPosT != E_sLienPosT.First)
                {
                    continue;
                }

                if (parameters.LienOption == CLoanProgramsFromDb.E_Option.Only2ndLien && loanProgram.lLienPosT != E_sLienPosT.Second)
                {
                    continue;
                }

                if (parameters.CanOnlyBeStandalone2nd && loanProgram.CanBeStandAlone2nd == false)
                {
                    continue;
                }

                if (loanProgram.IsLpeDummyProgram == false)
                {
                    if (string.IsNullOrEmpty(parameters.MatchingProductType) == false && loanProgram.lLpProductType.Equals(parameters.MatchingProductType, StringComparison.OrdinalIgnoreCase) == false)
                    {
                        continue;
                    }

                    if (string.IsNullOrEmpty(parameters.MatchingInvestorName) == false && loanProgram.lLpInvestorNm.Equals(parameters.MatchingInvestorName, StringComparison.OrdinalIgnoreCase) == false)
                    {
                        continue;
                    }

                    if (IsMatchFilterDue(loanProgram, parameters) == false)
                    {
                        continue;
                    }

                    if (IsMatchFinMeth(loanProgram, parameters) == false)
                    {
                        continue;
                    }

                    if (IsMatchPaymentType(loanProgram, parameters) == false)
                    {
                        continue;
                    }
                }

                // 5/26/2009 dd - Check to see if investor name and product code contains in user manual exclude list.
                if (CLoanProgramsFromDb.IsContains(manualExcludeProductList, loanProgram.lLpInvestorNm, loanProgram.ProductCode))
                {
                    continue; // skip this product.
                }

                if (parameters.ProductTypesToConsider.Length > 0)
                {
                    if (!parameters.ProductTypesToConsider.Contains(loanProgram.lLpProductType, StringComparer.OrdinalIgnoreCase))
                    {
                        continue; // skip all products that aren't in the "only include product type filter", if it exists.
                    }
                }

                if (string.IsNullOrEmpty(parameters.MatchingProductType))
                {
                    if (parameters.ProductTypesToIgnore != null && parameters.ProductTypesToIgnore.Contains(loanProgram.lLpProductType, StringComparer.OrdinalIgnoreCase))
                    {
                        continue; // skip this product.
                    }

                    if (parameters.ProductTypesToInclude != null && parameters.ProductTypesToInclude.Contains(loanProgram.lLpProductType, StringComparer.OrdinalIgnoreCase))
                    {
                        // don't skip this product.
                    }
                    else
                    {
                        if (parameters.LoanTypesToIgnore != null && parameters.LoanTypesToIgnore.Contains(loanProgram.lLT))
                        {
                            continue; // skip this product
                        }
                    }
                }
                else
                {
                    // include all results when of sql query when it has a nonempty onlyIncludeLpProductType
                }

                // Don't filter by product code if there are no required product codes set.
                if (parameters.RequiredProductCodes != null &&
                    parameters.RequiredProductCodes.Count > 0 &&
                    !parameters.RequiredProductCodes.Contains(loanProgram.ProductCode, StringComparer.OrdinalIgnoreCase))
                {
                    // Don't include this loan program if it does not contain the correct product code.
                    continue;
                }

                if (productPairingTable != null)
                {
                    FileBasedLoanProgramTemplate firstLienProgram = null;
                    if (snapshot.TryGetLoanProgramTemplate(firstLienLpId, out firstLienProgram))
                    {
                        int pairingProductIdsLength = loanProgram.PairingProductIds == null ? 0 : loanProgram.PairingProductIds.Length;

                        if (pairingProductIdsLength == 0)
                        {
                            // 7/19/2007 dd - Only Proceed with pairing at product level IF AND ONLY IF there is no explicit pairing at LP level.
                            if (!productPairingTable.IsAllowPairing(firstLienProgram.lLpInvestorNm, firstLienProgram.ProductCode, loanProgram.lLpInvestorNm, loanProgram.ProductCode))
                            {
                                continue; // Skip this product.
                            }
                        }
                    }
                }

                lpSet.Add(loanProgram.lLpTemplateId, loanProgram.lLpInvestorNm, loanProgram.ProductCode, preserveProductCode, loanProgram.lLT);
            }

            return lpSet;
        }

        /// <summary>
        /// Check to see if the loan program match criteria.
        /// </summary>
        /// <param name="loanProgram">The loan program to check.</param>
        /// <param name="parameters">Criteria parameters.</param>
        /// <returns>Return true if loan program match.</returns>
        private static bool IsMatchFilterDue(ILoanProgramTemplate loanProgram, LoanProgramFilterParameters parameters)
        {
            if (parameters.Due10Years && loanProgram.lTerm == 120)
            {
                return true;
            }
            else if (parameters.Due15Years && loanProgram.lTerm == 180)
            {
                return true;
            }
            else if (parameters.Due20Years && loanProgram.lTerm == 240)
            {
                return true;
            }
            else if (parameters.Due25Years && loanProgram.lTerm == 300)
            {
                return true;
            }
            else if (parameters.Due30Years && loanProgram.lTerm == 360)
            {
                return true;
            }
            else if (parameters.DueOther && parameters.Include10And25YearTermsInOtherDue &&
                (loanProgram.lTerm != 120 && loanProgram.lTerm != 180 && loanProgram.lTerm != 240 && loanProgram.lTerm != 300 && loanProgram.lTerm != 360))
            {
                return true;
            }
            else if (parameters.DueOther && !parameters.Include10And25YearTermsInOtherDue &&
                (loanProgram.lTerm != 180 && loanProgram.lTerm != 240 && loanProgram.lTerm != 360))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Check to see if loan program match criteria.
        /// </summary>
        /// <param name="loanProgram">The loan program to check.</param>
        /// <param name="parameters">Criteria parameters.</param>
        /// <returns>Return true if loan program match.</returns>
        private static bool IsMatchFinMeth(ILoanProgramTemplate loanProgram, LoanProgramFilterParameters parameters)
        {
            bool isPaymentAdjZero = loanProgram.IsOptionArm == false && loanProgram.lPmtAdjCapR == 0 && loanProgram.lPmtAdjCapMon == 0
                && loanProgram.lPmtAdjRecastPeriodMon == 0 && loanProgram.lPmtAdjRecastStop == 0 && loanProgram.lPmtAdjMaxBalPc == 0;

            if (parameters.FinMethFixed && loanProgram.lFinMethT == E_sFinMethT.Fixed && isPaymentAdjZero)
            {
                return true;
            }
            else if (parameters.FinMeth3YearsArm && loanProgram.lFinMethT == E_sFinMethT.ARM && loanProgram.lRadj1stCapMon == 36 && isPaymentAdjZero)
            {
                return true;
            }
            else if (parameters.FinMeth5YearsArm && loanProgram.lFinMethT == E_sFinMethT.ARM && loanProgram.lRadj1stCapMon == 60 && isPaymentAdjZero)
            {
                return true;
            }
            else if (parameters.FinMeth7YearsArm && loanProgram.lFinMethT == E_sFinMethT.ARM && loanProgram.lRadj1stCapMon == 84 && isPaymentAdjZero)
            {
                return true;
            }
            else if (parameters.FinMeth10YearsArm && loanProgram.lFinMethT == E_sFinMethT.ARM && loanProgram.lRadj1stCapMon == 120 && isPaymentAdjZero)
            {
                return true;
            }
            else if (parameters.FinMethOther && loanProgram.lFinMethT == E_sFinMethT.ARM && loanProgram.lRadj1stCapMon != 36 &&
                loanProgram.lRadj1stCapMon != 60 && loanProgram.lRadj1stCapMon != 84 && loanProgram.lRadj1stCapMon != 120)
            {
                return true;
            }
            else if (parameters.FinMethOther && loanProgram.lFinMethT == E_sFinMethT.Graduated)
            {
                return true;
            }
            else if (parameters.FinMethOther && isPaymentAdjZero == false)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Check to see if the loan program match criteria.
        /// </summary>
        /// <param name="loanProgram">The loan program to check.</param>
        /// <param name="parameters">Criteria parameters.</param>
        /// <returns>Return true if loan program match.</returns>
        private static bool IsMatchPaymentType(ILoanProgramTemplate loanProgram, LoanProgramFilterParameters parameters)
        {
            if (parameters.PaymentTypePI && loanProgram.lIOnlyMon <= 0)
            {
                return true;
            }

            if (parameters.PaymentTypeIOnly && loanProgram.lIOnlyMon > 0)
            {
                return true;
            }
  
            return false;
        }
    }
}