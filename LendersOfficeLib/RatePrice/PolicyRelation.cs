﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using DataAccess;
using Toolbox;

namespace LendersOfficeApp.los.RatePrice
{
    public interface IPolicyRelationDb
    {

        /// <summary>
        /// The descendants will be appended to "descendants" argument.
        /// Warning : this method will not call descendants.clear();
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="descendants"></param>

        void AddDescendantsToSet(Guid policyId, ICollection<Guid> descendants);

        ArrayList GetAncestors(Guid policyId);

    }

    public class PolicyRelationCenter : IPolicyRelationDb
    {
        class Relationship // POD
        {
            public Guid m_policyId;
            public ArrayList m_children;
            public ArrayList m_ancestors;
        }

        private Dictionary<Guid, Relationship> m_relationships;


        public PolicyRelationCenter(CRowHashtable rowsPolicyRelation)
            : this(CreateParentTable(rowsPolicyRelation), CreateChildrenTable(rowsPolicyRelation))
        {
        }

        public PolicyRelationCenter(Hashtable ancestorsHashtable, Hashtable childrenTable)
        {
            Dictionary<Guid, Relationship> result = new Dictionary<Guid, Relationship>(ancestorsHashtable.Count);

            // initialize the member data Relationship.m_policyId to create a guid pool ( look like .net intern string)
            foreach (Guid policyId in ancestorsHashtable.Keys)
            {
                Relationship relationship = new Relationship();
                relationship.m_policyId = policyId;
                result[policyId] = relationship;
            }

            bool isSubset = true;
            foreach (Guid policyId in childrenTable.Keys)
            {
                if (result.ContainsKey(policyId))
                {
                    continue;
                }

                if (isSubset)
                {
                    isSubset = false;
                    Tools.LogError("PolicyRelationCenter error : expect childrenTable.Keys is a subset ancestorsHashtable.Keys ");
                }

                Relationship relationship = new Relationship();
                relationship.m_policyId = policyId;
                result[policyId] = relationship;
            }


            m_relationships = result;

            foreach (Guid policyId in result.Keys)
            {
                Relationship relationship = (Relationship)result[policyId];
                relationship.m_ancestors = StandardizePolicies((ArrayList)ancestorsHashtable[policyId]);
                if (childrenTable[policyId] != null)
                {
                    relationship.m_children = StandardizePolicies((ArrayList)childrenTable[policyId]);
                }
                else
                {
                    relationship.m_children = null;
                }
            }
        }

        public ArrayList StandardizePolicies(ArrayList policies)
        {
            if (policies == null)
            {
                return new ArrayList(0);
            }

            ArrayList result = new ArrayList(policies.Count);
            foreach (Guid policyId in policies)
            {

                Guid alias = ((Relationship)m_relationships[policyId]).m_policyId;
                if (alias == Guid.Empty)
                {
                    alias = policyId;
                }

                result.Add(alias);
            }
            result.TrimToSize();
            return result;
        }

        public void AddDescendantsToSet(Guid policyId, ICollection<Guid> descendants)
        {
            Relationship relationship = (Relationship)m_relationships[policyId];

            // First check valid policy
            if (relationship == null)
            {
                return;
            }

            descendants.Add( (Guid) relationship.m_policyId ) ;

            AddDescendantsToSet(relationship, descendants);
        }


        private void AddDescendantsToSet(Relationship relationship, ICollection<Guid> descendants)
        {
            if (relationship == null || relationship.m_children == null)
            {
                return;
            }

            foreach (Guid id in relationship.m_children)
            {
                descendants.Add((Guid) id) ;
                AddDescendantsToSet((Relationship)m_relationships[id], descendants); // recursive 
            }
        }


        public ArrayList GetAncestors(Guid policyId)
        {
            return ((Relationship)m_relationships[policyId]).m_ancestors;
        }

        public bool Exist(Guid policyId)
        {
            return m_relationships.ContainsKey(policyId);
        }

        public int Count
        {
            get { return (m_relationships != null) ? m_relationships.Count : 0; }
        }

        /// <summary>
        /// This method retrieves parent policies and appends them to the "applicablePolicies" argument.
        /// Note : this method doesn't call applicablePolicies.clear();
        /// </summary>
        /// <param name="policyId"></param>
        /// <param name="applicablePolicies"></param>
        private void GetApplicablePolicies(CPricePolicyManager policyManager, Guid policyId, CSortedListOfGroups applicablePolicies)
        {
            ArrayList parents = GetAncestors(policyId);
            //(ArrayList)( m_lpGlobal.ParentHashtable[threadPolicyId] );

            if (null != parents)
            {
                //Merging parent lists
                for (int iParent = 0; iParent < parents.Count; ++iParent)
                {
                    object parentPolicyId = parents[iParent];

                    if (applicablePolicies.HasSingleItem(parentPolicyId))
                    {
                        break;
                    }

                    string parentSortedId = policyManager.GetMutualExclusiveExecSortedId((Guid)parentPolicyId);
                    applicablePolicies.Add(parentSortedId, parentPolicyId);
                }
            }
        }

        /// <summary>
        /// Retrieves ancestor policies of input policy ID list and adds them to the policy list.
        /// </summary>
        /// <param name="policyManager"></param>
        /// <param name="policyIds">The list of policies to get the parents of.</param>
        /// <returns>Complete list of all input policies and their ancestors.</returns>
        public CSortedListOfGroups GetApplicablePolicies(CPricePolicyManager policyManager, ArrayList policyIds)
        {
            CSortedListOfGroups applicablePolicies = new CSortedListOfGroups(5);
            foreach (Guid policyId in policyIds)
            {
                GetApplicablePolicies(policyManager, policyId, applicablePolicies);
            }
            return applicablePolicies;
        }

        public void GetStatisticInfo(out int policyCount, out int totalParents, out int depth)
        {
            policyCount = m_relationships.Count;
            totalParents = 0;
            depth = 0;
            foreach (Guid guid in m_relationships.Keys)
            {
                ArrayList parents = ((Relationship)m_relationships[guid]).m_ancestors;
                if (parents != null)
                {
                    //parents.TrimToSize();
                    totalParents += parents.Count;
                    if (depth < parents.Count)
                        depth = parents.Count;
                }
            }

        }

        public int EstimateSizeOfPolicyIds(ArrayList policyIds, out int wasteSize)
        {
            wasteSize = 0;
            if (policyIds == null)
                return 0;

            int size = 0;
            foreach (Guid policyId in policyIds)
            {
                Relationship relationship = m_relationships[policyId] as Relationship;
                if (relationship == null || object.ReferenceEquals(relationship.m_policyId, policyId) == false)
                    size += 16 /* guid size */  + 12 /* box size */;

            }
            wasteSize = size + 4 * (policyIds.Capacity - policyIds.Count);
            size += 28 + 4 * policyIds.Capacity;
            return size;
        }

        private static Hashtable CreateParentTable(CRowHashtable rowsPolicyRelation)
        {
            // compute parent hashtable.
            int rowCount = rowsPolicyRelation.Count;

            // the key is price policy id; each m_parentHashtable entry contains the ancestors of a given price policy
            Hashtable parentHashtable = new Hashtable(rowCount);


            ArrayList parents = new ArrayList(rowsPolicyRelation.Count);
            for (int iKeyRow = 0; iKeyRow < rowCount; ++iKeyRow)
            {
                DataRow keyRow = rowsPolicyRelation.GetRowByIndex(iKeyRow);

                parents.Clear();

                Object policyId = (Guid)keyRow["PricePolicyId"];

                while (true)
                {
                    Object keyId = (Guid)keyRow["PricePolicyId"];
                    if (parents.Contains(keyId))
                    {
                        Tools.LogError("Parent-child relationship can not circular link. Need to review the policy : " + policyId);
                        break; // never happend
                    }
                    parents.Add(keyId);

                    if (DBNull.Value == keyRow["ParentPolicyId"])
                        break; // done with the particular tree

                    keyRow = rowsPolicyRelation.GetRowByKey(keyRow["ParentPolicyId"]);
                }

                ArrayList tmp = new ArrayList(parents.Count);
                foreach (Object obj in parents)
                    tmp.Add(obj);

                parentHashtable.Add(policyId, tmp);
            }

            return parentHashtable;
        }

        private static Hashtable CreateChildrenTable(CRowHashtable rowsPolicyRelation)
        {
            // compute parent hashtable.
            int rowCount = rowsPolicyRelation.Count;

            // the key is price policy id; each m_parentHashtable entry contains the ancestors of a given price policy
            Hashtable childrenTable = new Hashtable(rowCount);


            for (int iKeyRow = 0; iKeyRow < rowCount; ++iKeyRow)
            {
                DataRow keyRow = rowsPolicyRelation.GetRowByIndex(iKeyRow);
                if (DBNull.Value == keyRow["ParentPolicyId"])
                    continue;
                Object parentPolicyId = keyRow["ParentPolicyId"];
                ArrayList children = (ArrayList)childrenTable[parentPolicyId];
                if (children == null)
                    childrenTable[parentPolicyId] = children = new ArrayList();

                Object policyId = (Guid)keyRow["PricePolicyId"];
                children.Add(policyId);
            }

            foreach (ArrayList children in childrenTable.Values)
                children.TrimToSize();

            return childrenTable;
        }

    }

    public class Goodbye
    {
        protected string m_msg;
        DateTime m_startTime;

        public Goodbye(string msg, bool logDateTime=true)
        {
            m_msg = msg;
            m_startTime = logDateTime ? DateTime.Now : DateTime.MinValue;
        }

        ~Goodbye()
        {
            try
            {
                string durationStr = string.Empty;
                if(m_startTime != DateTime.MinValue)
                {
                    var duration = DateTime.Now - m_startTime;
                    durationStr = $". Estimate duration {duration.Hours:00}:{duration.Minutes:00}:{duration.Seconds:00}, start time: {m_startTime.ToShortDateString()} {m_startTime.ToLongTimeString()}.";

                }

                Tools.LogRegTest(m_msg + durationStr);
            }
            catch
            {
                // It often happens when application is ternminating.
                // At that time, required component for LogRegTest was gone.
            }
        }

    }
}
