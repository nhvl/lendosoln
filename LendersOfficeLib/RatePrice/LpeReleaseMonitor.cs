// Author : Thien Nguyen

using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Constants;


namespace LendersOfficeApp.los.RatePrice
{
    public class LpeReleaseMonitor
    {
        // when we detector the lpeData's  out of date, we start to investigate it
        private DateTime m_inquiryTime = DateTime.MinValue;      // local time
        private DateTime m_lpeVersionD;                          // db time
        private CLpeReleaseInfo m_lpeReleaseInfo;
        private bool m_isReport = false;
        private bool m_allowToSendEmail;

        private DateTime m_reportErrorTime;

        public LpeReleaseMonitor(bool allowToSendEmail)
        {
            m_allowToSendEmail = allowToSendEmail;
        }

        private DateTime m_reportNoOutOfDateSchedule;
        private void ReportNoOutOfDate(bool forceReport)
        {
            m_inquiryTime = DateTime.MinValue; // for safety
            m_isReport = false;
            m_outOfDateStr = "No out of date";

            if ((forceReport || m_reportNoOutOfDateSchedule <= DateTime.Now) && m_lpeReleaseInfo != null)
            {
                Tools.LogRegTest(string.Format("<LpeRelease_Monitor> no out of date with the lpe release ver = {0} {1}. Estimate #FilebasedSnapshot: {2}",
                                        CommonLib.TimeTool.DateTime2String(m_lpeReleaseInfo.DataLastModifiedD),
                                        m_lpeReleaseInfo.DataSrcForSnapshot, LendersOffice.RatePrice.FileBasedPricing.FileBasedSnapshot.GetCounter()));
                m_reportNoOutOfDateSchedule = DateTime.Now.AddMinutes(10);
            }
        }


        public void Run()
        {
            DateTime now = DateTime.Now;

            CLpeReleaseInfo lpeReleaseInfo = CLpeReleaseInfo.GetLatestReleaseInfo(true);
            DateTime dataVersionD = LpeTools.GetLpeDataLastModified(DataSrc.LpeSrc);
            if (lpeReleaseInfo == null)
            {
                //ThienTodo : need rewrite at here
                return;
            }

            if (m_lpeReleaseInfo != lpeReleaseInfo)
            {
                // at here : lpeReleaseInfo.DataLastModifiedD != dataVersionD 
                m_lpeReleaseInfo = lpeReleaseInfo;
                m_lpeVersionD = dataVersionD;

                if (m_lpeReleaseInfo.DataLastModifiedD == m_lpeVersionD)
                    ReportNoOutOfDate(true /*forceReport*/);
                else
                {
                    m_inquiryTime = now;
                    m_isReport = false;
                    SetOutOfDateStr();
                    Tools.LogRegTest("<LpeRelease_Monitor> " + m_outOfDateStr);
                }
                return;
            }

            if (m_lpeReleaseInfo.DataLastModifiedD == dataVersionD)
            {
                ReportNoOutOfDate(false /*forceReport*/);
                return;
            }

            // at here, lpe data is out of date


            if (m_isReport)
            {
                if (m_reportErrorTime > now)
                    return;
            }


            if (m_inquiryTime == DateTime.MinValue)
            {
                m_inquiryTime = now;
                m_lpeVersionD = dataVersionD;

                SetOutOfDateStr();
                Tools.LogRegTest("<LpeRelease_Monitor> " + m_outOfDateStr);

            }


            TimeSpan monitorDuration = now - m_inquiryTime;


            const int LpeRelease_ProcessTimeInMinute = CompilerConst.IsDebugMode ? 20 : 30;
            const int LpeRelease_MaxMinutesForMonitorOutOfDate = CompilerConst.IsDebugMode ? 30 : 120;
            const int LpeRelease_AliveIntervalInMinute = 20;


            if (monitorDuration.TotalMinutes < LpeRelease_ProcessTimeInMinute)
                return;

            DateTime aliveTime = GetLogTimeOfLpeRelease();
            TimeSpan howlongDontSeeLogMsg = DateTime.Now - aliveTime;
            if (howlongDontSeeLogMsg.TotalMinutes >= LpeRelease_AliveIntervalInMinute)
            {
                string errMsg = string.Format("<LpeRelease_Monitor_Error> Maybe LpeRelease doesn't run at this time. Its last log timestamp is {0} minutes ago [ at {1} ]. " +
                    "The out of date of current snapshot {2} {3} was monitored at {4} minutes ago [ at {5} ]",
                    howlongDontSeeLogMsg.TotalMinutes.ToString("N0"),
                    aliveTime,
                    CommonLib.TimeTool.DateTime2String(m_lpeReleaseInfo.DataLastModifiedD),
                    m_lpeReleaseInfo.DataSrcForSnapshot,
                    monitorDuration.TotalMinutes.ToString("N0"),
                    m_inquiryTime);

                SendErrorReport(errMsg);
                return;
            }

            if (monitorDuration.TotalMinutes >= LpeRelease_MaxMinutesForMonitorOutOfDate)
            {
                string errMsg = string.Format("<LpeRelease_Monitor_Error> The current snapshot {0} {1} is too old. Its out of date was monitored at {2} ie {3} minutes ago.",
                    CommonLib.TimeTool.DateTime2String(m_lpeReleaseInfo.DataLastModifiedD), m_lpeReleaseInfo.DataSrcForSnapshot,
                    m_inquiryTime, monitorDuration.TotalMinutes.ToString("N0"));

                SendErrorReport(errMsg);
                return;
            }

        }

        private string m_outOfDateStr = "";
        void SetOutOfDateStr()
        {
            m_outOfDateStr = string.Format("detected out of date at {0} : dbVersion = {1} vs lpeRelease = {2} {3}",
                m_inquiryTime.ToLongTimeString(),
                CommonLib.TimeTool.DateTime2String(m_lpeVersionD),
                CommonLib.TimeTool.DateTime2String(m_lpeReleaseInfo.DataLastModifiedD),
                m_lpeReleaseInfo.DataSrcForSnapshot);

        }

        private DateTime m_logTime = DateTime.MinValue;
        private DateTime GetLogTimeOfLpeRelease()
        {
            try
            {
                DateTime pulled = PullMaxLogTime();
                if (pulled != DateTime.MinValue)
                {
                    this.m_logTime = pulled;
                }
            }
            finally
            {
                if (m_logTime == DateTime.MinValue)
                    m_logTime = DateTime.Now;

            }
            return m_logTime;
        }

        public static DateTime PullMaxLogTime()
        {
            string sql = "SELECT max(LogTime) as LogTime FROM LPE_RELEASE_LOG";

            DateTime maxDT = DateTime.MinValue;
            Action<IDataReader> readHandler = delegate (IDataReader r)
            {
                if (r.Read())
                {
                    maxDT = (DateTime)r["LogTime"];
#if DEBUG
                    //Tools.LogRegTest( "<LpeRelease_Monitor> m_logTime " + m_logTime );
#endif
                }
            };

            DBSelectUtility.ProcessDBData(DataSrc.LOTransient, sql, null, null, readHandler);
            return maxDT;
        }

        private void SendErrorReport(string errMsg)
        {
            if (m_isReport == false && m_allowToSendEmail)
            {
                Tools.LogErrorWithCriticalTracking(errMsg);
            }
            else
                Tools.LogError(errMsg);

            m_isReport = true;
            m_reportErrorTime = DateTime.Now.AddMinutes(5);

        }

    }

}
