﻿using System;
using System.Text;
using System.Collections;
using System.Data;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.RatePrice
{
    public class CLPERunModeException : CBaseException
    {
        public CLPERunModeException()
            : base("Pricing cannot be run for this loan file.", "Cannot run pricing when lpeRunModeT=NotAllow")
        {

        }
    }
}
