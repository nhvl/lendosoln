using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice
{
    public enum E_UiPolicyProgramAssociation
    {
        Blank = 0,
        Yes = 1,
        No = 2,
        ForceNo = 3
    }

    #region HELPER CLASSES
    internal class CAssocCalc : CDataRow
    {
        internal CAssocCalc() : base() { }
        internal CAssocCalc(DataRow row) : base(row) { }

        static internal bool ComputeResult(bool inherit, E_UiPolicyProgramAssociation assocTri)
        {
            switch (assocTri)
            {
                case E_UiPolicyProgramAssociation.Blank: return inherit;
                case E_UiPolicyProgramAssociation.Yes: return true;
                case E_UiPolicyProgramAssociation.No: return false;
                case E_UiPolicyProgramAssociation.ForceNo: return false;
                default: throw new CBaseException(ErrorMessages.Generic, "Programming error in CAssocCalc.IsAssociatedWith()");
            }
        }



        internal bool InheritValFromMaster
        {
            get { return GetBoolField("InheritVal"); }
            set { SetBoolField("InheritVal", value); }
        }

        internal bool IsForcedNo
        {
            get { return GetBoolField("IsForcedNo"); }
            set { SetBoolField("IsForcedNo", value); }
        }


        internal E_TriState AssocOverrideTri
        {
            get { return GetTriStateField("AssocOverrideTri"); }
            set
            {
                SetTriStateField("AssocOverrideTri", value);
            }
        }

        // 06/28/07 OPM 4873
        internal E_UiPolicyProgramAssociation AssocForce
        {
            get
            {
                if (IsForcedNo)
                    return E_UiPolicyProgramAssociation.ForceNo;

                return (E_UiPolicyProgramAssociation)GetTriStateField("AssocOverrideTri");
            }
            set
            {
                if (value != E_UiPolicyProgramAssociation.ForceNo)
                {
                    SetBoolField("IsForcedNo", false);
                    SetTriStateField("AssocOverrideTri", (E_TriState)value);
                }
                else
                {
                    SetBoolField("IsForcedNo", true);
                    SetTriStateField("AssocOverrideTri", E_TriState.Blank);
                }
            }
        }

        internal bool InheritValFromBaseProd
        {
            get { return GetBoolField("InheritValFromBaseProd"); }
            set { SetBoolField("InheritValFromBaseProd", value); }
        }

        internal Guid PricePolicyId
        {
            get { return GetGuidField("PricePolicyId"); }
        }

        internal Guid ProductId
        {
            get { return GetGuidField("ProductId"); }
        }

        /// <summary>
        /// Merging result between master inheritance and base product inheritance
        /// </summary>
        internal bool IsAssocAfterMerge
        {
            get
            {
                switch (AssocOverrideTri)
                {
                    case E_TriState.Blank: // inheriting
                        return InheritValFromMaster || InheritValFromBaseProd;
                    case E_TriState.No:
                        return false;
                    case E_TriState.Yes: // force association
                        return true;
                    default:
                        throw new CBaseException(ErrorMessages.PricePolicyAssocError,
                            "CAssocCalc::IsAssocAfterMerge fails. Unexpected E_TriState enum value");
                }
            }
        }
    }

    internal class CProductFolderData : CDataRow
    {
        internal CProductFolderData() : base() { }
        internal CProductFolderData(DataRow row) : base(row) { }
        /*
		[FolderId] [uniqueidentifier] NOT NULL CONSTRAINT [DF__LOAN_PROD__Folde__2DBD803D] DEFAULT (newid()),
		[FolderName] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
		[BrokerId] [uniqueidentifier] NOT NULL ,
		[ParentFolderId] [uniqueidentifier] NULL ,
		*/
        internal Guid FolderId
        {
            get { return GetGuidField("FolderId"); }
        }
        internal Guid ParentFolderId
        {
            get { return GetGuidField("ParentFolderId"); }
        }
    }

    public class CFolderTree
    {
        private DataRowCollection m_coll;
        private Hashtable m_hashtable;   //key: parent folder id, value: hashtable of its child folders

        internal CFolderTree(DataRowCollection coll, string parentColNm, string childColNm)
        {
            m_coll = coll;

            int count = coll.Count;
            m_hashtable = new Hashtable(count); //count is the worst case, the whole thing is flat, not related
            CProductFolderData folderData = new CProductFolderData();

            foreach (DataRow row in coll)
            {
                folderData.RecycleFor(row);

                Guid parentId;
                try { parentId = folderData.ParentFolderId; }
                catch { parentId = Guid.Empty; }  // Guid.Empty represent the root folder.
                Hashtable childrenIds;
                if (!m_hashtable.Contains(parentId))
                {
                    childrenIds = new Hashtable(10);
                    m_hashtable[parentId] = childrenIds;
                }
                else
                    childrenIds = (Hashtable)m_hashtable[parentId];

                childrenIds[folderData.FolderId] = null; // adding it.
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="folderId"></param>
        /// <returns>returns null if the folder doesn't have children that this tree would know of</returns>
        internal Hashtable GetChildrenOf(Guid folderId)
        {
            if (m_hashtable.ContainsKey(folderId))
                return (Hashtable)m_hashtable[folderId];
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="folderId"></param>
        /// <returns>returns null if folderId is the root folder in this broker</returns>
        internal Guid GetParentFolderOf(Guid folderId)
        {
            foreach (Guid parentId in m_hashtable.Keys)
            {
                Hashtable children = (Hashtable)m_hashtable[parentId];
                if (children.ContainsKey(folderId))
                    return parentId;
            }
            return Guid.Empty; // folderId doesn't have parent folder, it must be the root in this broker
        }

        internal void AccumulateDescendantsOf(Guid folderId, Hashtable descendants)
        {
            foreach (Guid childId in GetChildrenOf(folderId))
            {
                descendants[childId] = null;
                AccumulateDescendantsOf(childId, descendants);
            }
        }
    }
    #endregion // HELPER CLASSES

    public class CAssocInheritUpdate
    {
        #region PRIVATE STATIC METHODS
        /// <summary>
        /// 
        /// </summary>
        /// <param name="prodId"></param>
        /// <returns>hashtable:policyid->policy association tristate for the given prod</returns>
        static private Hashtable StaticPrivate_GetAssocTrisForThisProd(Guid prodId)
        {
            CDataSet ds = new CDataSet();
            SqlParameter[] sqlParameters = {
                                               new SqlParameter( "@ProductId", prodId )
                                           };

            // 11/21/2007 ThienNguyen - Reviewed and safe
            ds.LoadWithSqlQueryDangerously(DataSrc.LpeSrc, "select * from product_price_association where productid = @ProductId", sqlParameters);

            DataRowCollection rows = ds.Tables[0].Rows;
            Hashtable tris = new Hashtable(rows.Count);

            CAssocCalc assoc = new CAssocCalc();
            foreach (DataRow row in rows)
            {
                assoc.RecycleFor(row);
                tris[assoc.PricePolicyId] = assoc.AssocForce;
            }

            return tris;
        }

        static private DataRow StaticPrivate_InsertNewAssocRow(DataSet ds, Guid priceId, Guid prodId, E_UiPolicyProgramAssociation forceTri, bool inheritVal)
        {
            DataRow newRow = ds.Tables[0].NewRow();
            newRow["PricePolicyId"] = priceId;
            newRow["ProductId"] = prodId;

            // Need to accept ForceNo setting.
            if (forceTri == E_UiPolicyProgramAssociation.ForceNo)
            {
                newRow["AssocOverrideTri"] = E_TriState.Blank;
                newRow["IsForcedNo"] = true;
            }
            else
            {
                newRow["AssocOverrideTri"] = forceTri;
            }

            newRow["InheritVal"] = inheritVal;

            // OPM 62218. Debug info
            //Tools.LogInfo( string.Format( "Pricing Association Update: Inserting PriceId = {0}, ProdId = {1}, ForceTri={2}, Inherit= {3} "
            //	, priceId.ToString(), prodId.ToString(), forceTri.ToString(), inheritVal.ToString() ));
            ds.Tables[0].Rows.Add(newRow);
            return newRow;
        }


        #endregion // PRIVATE STATIC METHODS

        #region PUBLIC STATIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="prodId">either a master or a non-master prod id</param>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        static public CAssocInheritUpdate GetUpdatorForNewProd(Guid prodId, Guid brokerId)
        {
            return new CAssocInheritUpdate(prodId, brokerId, true);
        }

        static public CAssocInheritUpdate GetUpdatorForExistingMasterNoUiAction(Guid masterId, Guid brokerId)
        {
            return new CAssocInheritUpdate(masterId, brokerId, false);
        }

        static public CAssocInheritUpdate GetUpdatorForExistingMasterAfterChangeInUi(Guid masterId, Guid brokerId, Hashtable newTris)
        {
            return new CAssocInheritUpdate(masterId, brokerId, newTris);
        }

        static public void AttachPolicyToNoneMasterProducts(Guid policyId, List<Guid> productIds)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            string sSql = "select * from product_price_association where PricePolicyId = @PricePolicyId AND ProductId "
                          + DbTools.CreateParameterized4InClauseSql("ProductId", productIds, parameters);
            parameters.Add(new SqlParameter("@PricePolicyId", policyId));

            using (CDataSet dsAssoc = new CDataSet())
            {
                dsAssoc.LoadWithSqlQueryDangerously(DataSrc.LpeSrc, sSql, parameters);

                CAssocCalc calc = new CAssocCalc();
                CRowHashtable assocRows = new CRowHashtable(dsAssoc.Tables[0].Rows, "ProductId");

                foreach (var productId in productIds)
                {
                    if (!assocRows.HasRowOfKey(productId))
                    {
                        StaticPrivate_InsertNewAssocRow(dsAssoc, policyId, productId, E_UiPolicyProgramAssociation.Yes, false);
                        continue;
                    }

                    calc.RecycleFor(assocRows.GetRowByKey(productId));

                    if (calc.AssocForce != E_UiPolicyProgramAssociation.Yes)
                    {
                        calc.AssocForce = E_UiPolicyProgramAssociation.Yes;
                    }
                }

                dsAssoc.Save();
            }
        }

        #endregion // PUBLIC STATIC METHODS

        #region  PRIVATE MEMBERS
        private Guid m_productId;
        private Guid m_userBrokerId;
        private CDataSet m_dsAssoc;
        private Hashtable m_prodToIsMasterTable; // key: productId, value: Private_IsMaster bit

        /*
        1.	Search for folder/parent/grand parent master products (hint: check the folder).
            a.	Figure out the path of the product
            b.	Find out the masters associated of each parent folder in the path
            c.	Each master must be able to tell the association state (TriState: force yes, force no, inherit) of each policy.
        2.	Create a hash table of the length of the number attachable policies.  Starting out empty, basically each policy has a �non-associated� as default. If shows up in the hash table, it means the result is associated.
        3.	Starting with the master of the current folder in the path and walk up the folder hierarchy. (The loop starts here). This is to figure out what it inherits. Question, can we figure out the result in one shot? Probably, looks like we only care about the first force only.
            a.	If the first one is yes, it will be associated, 
            b.	If the first one is no, then it�s non-associated,
            c.	If no force is found (doesn�t exist in the assocation table), then it�s non-associated
        */
        private CFolderTree m_folderTree;

        private Hashtable m_prodToFolderTable; // key: productId, value: folderId
        private Hashtable m_folderToMasterTable; //key: folderId, value: master product id, empty folder is Guid.Empty
        private Hashtable m_assocTable; //key=productId string + policy string, value=datarow of assoc table
        private Hashtable m_folderToProdsTable; // key: folderId, value: ArrayList of product ids located in the folder. empty folder is Guid.Empty
        private bool m_bPickupFromAboveMasterOnly;
        private Hashtable m_newTrisFromUI = null;

        private ForceNoManager m_ForceNoManager; // OPM 4873

        private Hashtable m_loadedProducts = new Hashtable();
        #endregion // PRIVATE MEMBERS

        #region CONSTRUCTORS
        /// <summary>
        /// This constructor would set up so that the Update() call will spread association tri array when the product association is changed when users are not 
        /// making changes to the association list via the UI. The assoc tri array will be loaded up from db.
        /// Use the other constructor if the user just modifies the association list of a master or non-master product.
        /// </summary>
        /// <param name="prodId">either master or non-master prod id</param>
        /// <param name="userBrokerId"></param>
        /// <param name="bPickupFromAboveMasterFist">true: typical scenario, this is the new master or non-master product got added</param>
        private CAssocInheritUpdate(Guid prodId, Guid userBrokerId, bool bPickupFromAboveMasterOnly)
        {
            m_productId = prodId;
            m_userBrokerId = userBrokerId;
            // It's true, this is the case for new master or product just gor created, it needs to get inheritance from its above master then spread
            m_bPickupFromAboveMasterOnly = bPickupFromAboveMasterOnly;
        }

        /// <summary>
        /// This is used when the product's association list just got modified in the UI, pass in the new association tri array to newTris
        /// </summary>
        /// <param name="prodId"></param>
        /// <param name="userBrokerId"></param>
        /// <param name="newTris">pass in the new assoc tri array in the UI</param>
        private CAssocInheritUpdate(Guid prodId, Guid userBrokerId, Hashtable newTris)
        {
            m_productId = prodId;
            m_newTrisFromUI = newTris;
            m_userBrokerId = userBrokerId;
        }

        #endregion // CONSTRUCTORS

        #region PUBLIC METHODS

        /// <summary>
        /// 
        /// </summary>
        /// <param name="newTris">hashtable:policyid->tristate, pass null if it needs to be looked up from db</param>
        /// <param name="assocOverrideTris">Hashtable of PolicyId as key and the E_TriState as value</param>
        /// <returns>Hashtable of the non-master products got modified by this call, might be null </returns>
        public Hashtable UpdateAndSpread(bool bTurnOffOptimizer)
        {
            Hashtable modifiedProds = new Hashtable(200);

            // TODO: We really need to make this more readable/maintainable and OO.  
            // This is very C-like procedural code. eg. All these hashtables should be custom objects.

            Hashtable affectedResults = null; // Key:pricepolicy id; value: bool
            if (m_bPickupFromAboveMasterOnly)
            {
                Tools.Assert(null == m_newTrisFromUI, "Encounter bug. If there is newTris from the UI, I do not expect to encounter m_bPickupFromAboveMasterOnly being true.");
                // Note that, for this scenario, since more information from product hierarchy is needed, we'll handle it later in this method
            }
            else if (null == m_newTrisFromUI)
            {
                // This should be a master product, non-master product should fall into the previous case where m_bPickupFromAboveMasterOnly = true
                Hashtable tris = StaticPrivate_GetAssocTrisForThisProd(m_productId);
                // with the optimzation bit off = true, we'll get back the list of bits that need to spread to its children
                affectedResults = StaticPrivate_UpdateDbForProd(m_productId, tris, true);
                //Now we need to convert tris to affected list
            }
            else
            {
                // Must update the assoc entry for the editing product here regardless if it would affect
                // anybody else
                //Hashtable tris = ( null == m_newTrisFromUI ) ? : m_newTrisFromUI;

                affectedResults = StaticPrivate_UpdateDbForProd(m_productId, m_newTrisFromUI, bTurnOffOptimizer);
                if (0 == affectedResults.Count)
                    return null;
                modifiedProds[m_productId] = null;
            }

            // Figure out the hierarchy of the product for this particular brokers

            CDataRow row = new CDataRow();

            // OPM 10296
            LoadAssocForThisProduct(m_productId);

            // II. If it's product, easy because no one can inherit from it, just update the association rows for this product directly.
            // Need to update for this row anyway regardless if it's a master or not	

            // III. If it's a master, complicated
            //	1) Need to pull up all connected children
            //		a) Pull up the list of folders of the broker (this might change in the future)
            #region LoadLender

            DataSet dsFolders = new DataSet();
            DataSetHelper.Fill(dsFolders, DataSrc.LpeSrc, "ListLoanProductFolderByBrokerID", new SqlParameter[]{ new SqlParameter( "@BrokerId", m_userBrokerId ),
                                                                                                    new SqlParameter( "@IsLpe", true ) });
            /*
            [FolderId] [uniqueidentifier] NOT NULL CONSTRAINT [DF__LOAN_PROD__Folde__2DBD803D] DEFAULT (newid()),
            [FolderName] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
            [BrokerId] [uniqueidentifier] NOT NULL ,
            [ParentFolderId] [uniqueidentifier] NULL ,
            */
            //		b) Compute the folder subtree of the product
            //		c) Throw out the non-related folders or parent folders.
            m_folderTree = new CFolderTree(dsFolders.Tables[0].Rows, "ParentFolderId", "FolderId");

            //		d) Pull up the list of products in those folders that have an assoc entry in the assoc table. 
            CDataSet dsProductInfo = new CDataSet();

            // 11/21/2007 ThienNguyen - Reviewed and safe
            dsProductInfo.LoadWithSqlQueryDangerously(DataSrc.LpeSrc,
                string.Format("select lLpTemplateId as ProductId, IsMaster, FolderId from loan_program_template where brokerid = '{0}' and IsMasterPriceGroup = 0",
                m_userBrokerId.ToString()));

            DataRowCollection rows = dsProductInfo.Tables[0].Rows;
            m_prodToFolderTable = new Hashtable(rows.Count);
            m_prodToIsMasterTable = new Hashtable(rows.Count);


            foreach (DataRow rawRow in rows)
            {
                row.RecycleFor(rawRow);
                Guid productId = row.GetGuidField("ProductId");

                if (!m_prodToFolderTable.ContainsKey(productId))
                {
                    Guid folderId;
                    try { folderId = row.GetGuidField("FolderId"); }
                    catch { folderId = Guid.Empty; }
                    m_prodToFolderTable.Add(productId, folderId);
                }

                m_prodToIsMasterTable.Add(productId, row.GetBoolField("IsMaster"));
            }

            CDataSet dsFolderContentInfo = new CDataSet();

            // 11/21/2007 ThienNguyen - Reviewed and safe
            dsFolderContentInfo.LoadWithSqlQueryDangerously(DataSrc.LpeSrc,
                string.Format("select FolderId, lLpTemplateId as productId, IsMaster from loan_program_template where brokerid = '{0}'and IsMasterPriceGroup = 0",
                m_userBrokerId.ToString()));

            rows = dsFolderContentInfo.Tables[0].Rows;
            m_folderToMasterTable = new Hashtable(rows.Count);
            m_folderToProdsTable = new Hashtable(rows.Count);

            foreach (DataRow rawRow in rows)
            {
                row.RecycleFor(rawRow);
                Guid folderId;
                try { folderId = row.GetGuidField("FolderId"); }
                catch { folderId = Guid.Empty; }

                Guid productId = row.GetGuidField("ProductId");

                //m_folderToMasterTable
                if (row.GetBoolField("IsMaster"))
                {
                    Tools.Assert(!m_folderToMasterTable.ContainsKey(folderId), string.Format("Product folder with id = {0} is having more than 1 master product."
                        , folderId.ToString()));
                    m_folderToMasterTable[folderId] = productId;
                }

                //m_folderToProdsTable
                ArrayList array;
                if (!m_folderToProdsTable.ContainsKey(folderId))
                {
                    array = new ArrayList(10);
                    m_folderToProdsTable.Add(folderId, array);
                }
                else
                    array = (ArrayList)m_folderToProdsTable[folderId];
                array.Add(productId);
            } // foreach( DataRow rawRow in rows )

            m_ForceNoManager = new ForceNoManager(
                m_userBrokerId
                , m_prodToIsMasterTable
                , m_prodToFolderTable
                , m_folderToMasterTable
                , m_folderTree
                );

            #endregion

            // TODO: Must reinforce that no master product can be in the root of a broker 
            // product tree (that is, they must have a FolderIs not null ), the reason 
            // is that each broker has a null (root) folder, not unique 

            //	We might have to pull the ones that have product id in assoc table with the affected policies
            // before we can narrow down the set in memory

            //		e) Pull up the association rows of these potentially affected products and the affected policies in the delta
            //	2) Set all I of connected children to new result, create if doesn't exist, stop when encounter Force != 0
            // FIGURE OUT WHEN TO STOP IS TRICKY BECAUSE EACH PRODUCT IS RELATED/INHERIT TO ANOTHER IN A SET NUMBER
            // POLICIES, SOME TREE BRANCH MIGHT BE TURNED OFF FOR SPECIFIC POLICIES BUT NOT OTHERS
            //Hashtable newAffectedPrice = GetNewAffectedPolicies( affectedPrice, m_productId );
            //CRowHashtable prodsWithAssocEntries = new CRowHashtable( dsProdsWithAssocEntries.Tables[0].Rows, "ProductId" );
            //ApplyInheritance( m_productId, folderTree, dsAssocProd, affectedPrice, prodsWithAssocEntries );

            //		Note that we can optimize it when the new result is No but that would be in the future.
            //  3) Flush the dataset changes in m_dsAssoc to the database.


            if (m_bPickupFromAboveMasterOnly)
            {
                // GET THE ABOVE MASTER
                /*
                    private CFolderTree m_folderTree;
                    private Hashtable m_prodToFolderTable; // key: productId, value: folderId
                    private Hashtable m_folderToMasterTable; //key: folderId, value: master product id, empty folder is Guid.Empty
                    private Hashtable m_assocTable; //key=productId string + policy string, value=datarow of assoc table
                    private Hashtable m_folderToProdsTable; // key: folderId, value: ArrayList of product ids located in the folder. empty folder is Guid.Empty
                */
                Guid folderId = (Guid)m_prodToFolderTable[m_productId]; // recycle

                // If the master is the product we need to spread to, we also need to skip it to get
                // its parent master
                while (Guid.Empty != folderId &&
                    (!m_folderToMasterTable.ContainsKey(folderId) || m_productId == (Guid)m_folderToMasterTable[folderId]))
                {
                    folderId = m_folderTree.GetParentFolderOf(folderId);
                } // while

                if (m_folderToMasterTable.ContainsKey(folderId))
                {
                    // Found a master above, now we need to pick assoc tris from that one

                    Guid aboveMasterId = (Guid)m_folderToMasterTable[folderId];
                    Hashtable tris = StaticPrivate_GetAssocTrisForThisProd(aboveMasterId);
                    // Do this just to convert tris to affectedResults
                    affectedResults = StaticPrivate_UpdateDbForProd(aboveMasterId, tris, true);

                    if (0 == affectedResults.Count)
                        return null;

                    Private_ApplyToProduct(m_productId, affectedResults, bTurnOffOptimizer); // this would apply to the masterProductId as well.	

                    modifiedProds[m_productId] = null;
                    m_dsAssoc.Save();

                    return modifiedProds;
                } // if( m_folderToMasterTable.ContainsKey( folderId ) )

                return null; // master above, this one would just contain no association -> no-op.
            } // if( m_bPickupFromAboveMasterOnly )



            // Non-master product doesn't affect any body else in the scope of this class. It would affect the derived classes but we are not handling it here.
            // The reason is, it's more efficient to spread only from the affected products that have derived products
            // The caller would go through the set of prods have been modified and spread it via the derivation hierarchy there.
            if (!Private_IsMaster(m_productId))
                return modifiedProds;

            if (null == affectedResults)
                throw new CBaseException(ErrorMessages.Generic, "Bug, I don't expect affectedResults is not assigned to an real object at this point");

            Private_SpreadAssocsForMaster(m_productId, affectedResults, bTurnOffOptimizer, modifiedProds);
            m_dsAssoc.Save();

            return modifiedProds;
        } // End of function Update( ... )

        #endregion // PUBLIC METHODS

        #region INTERNAL METHODS

        /// <summary>
        /// Update the override associations for the program.
        /// </summary>
        /// <param name="newTris">hashtable:policyid->force tristate</param>
        /// <returns>Returns the hashtable of ids of the affected price policies, policyid->result association</returns>
        static private Hashtable StaticPrivate_UpdateDbForProd(Guid prodId, Hashtable newTris, bool bTurnOffOptimizer)
        {
            // I. Evaluate the delta of the results 
            //		a) Load the association rows of this product  (this would include what are currently inherited)
            Hashtable affectedResults = new Hashtable(newTris.Count);

            CDataSet dsAssoc = new CDataSet();
            SqlParameter[] sqlParameters = {
                                               new SqlParameter( "@ProductId", prodId )
                                           };

            // 11/21/2007 ThienNguyen - Reviewed and safe
            dsAssoc.LoadWithSqlQueryDangerously(DataSrc.LpeSrc, "select * from product_price_association where ProductId = @ProductId",
                sqlParameters);// Future: Should we skip the ones with no force?

            //		b) Calculate the new results based on the parameters and what are currently inherited
            CAssocCalc calc = new CAssocCalc();
            CRowHashtable assocRows = new CRowHashtable(dsAssoc.Tables[0].Rows, "PricePolicyId");

            foreach (Guid priceId in newTris.Keys)
            {
                E_UiPolicyProgramAssociation newTri = (E_UiPolicyProgramAssociation)newTris[priceId];

                if (!assocRows.HasRowOfKey(priceId))
                {   // meaning: no force, and Inheritance was a No
                    switch (newTri)
                    {
                        case E_UiPolicyProgramAssociation.Blank:// no op, no change in I, don't need db assoc row for this case.
                            if (bTurnOffOptimizer)
                                affectedResults[priceId] = false;
                            continue;
                        case E_UiPolicyProgramAssociation.No:// must add to db, but no change in I (still a no)
                            StaticPrivate_InsertNewAssocRow(dsAssoc, priceId, prodId, E_UiPolicyProgramAssociation.No, false);
                            if (bTurnOffOptimizer)
                                affectedResults[priceId] = false;
                            continue;
                        case E_UiPolicyProgramAssociation.Yes:// must save, and there is change in I, from no to yes
                            StaticPrivate_InsertNewAssocRow(dsAssoc, priceId, prodId, E_UiPolicyProgramAssociation.Yes, false);
                            affectedResults[priceId] = true;
                            continue;
                        case E_UiPolicyProgramAssociation.ForceNo: // need to add to db for this master.
                            StaticPrivate_InsertNewAssocRow(dsAssoc, priceId, prodId, E_UiPolicyProgramAssociation.ForceNo, false);
                            if (bTurnOffOptimizer)
                                affectedResults[priceId] = false; // This is a master, so we want to set forceNo,
                            continue;
                        default:
                            Tools.LogBug("Programming error in CAssocInheritUpdate:StaticPrivate_UpdateDbForProd(). Unhandled enum of E_TriState");
                            continue;
                    }
                }
                calc.RecycleFor(assocRows.GetRowByKey(priceId));

                if (bTurnOffOptimizer)
                {
                    bool res = false;
                    switch (newTri)
                    {
                        case E_UiPolicyProgramAssociation.Yes: res = true; break;
                        case E_UiPolicyProgramAssociation.No: res = false; break;
                        case E_UiPolicyProgramAssociation.Blank: res = calc.InheritValFromMaster; break;
                        case E_UiPolicyProgramAssociation.ForceNo: res = false; break;
                    }
                    affectedResults[priceId] = res;
                }
                // If the new value for master inherit == current force, we do not go further
                // Remember that this is direct application, not master inherit
                if (newTri == calc.AssocForce) // This used to be calc.AssocOverTri...
                    continue;

                bool newResult = CAssocCalc.ComputeResult(calc.InheritValFromMaster, newTri);

                // If what will now result changes the assoc.
                if (newResult != CAssocCalc.ComputeResult(calc.InheritValFromMaster, calc.AssocForce))
                    affectedResults[priceId] = newResult; // just to add it to the affected list				

                // Set the Manual association
                calc.AssocForce = newTri;

            }

            dsAssoc.Save();

            return affectedResults;
        }


        #endregion // INTERNAL METHODS



        #region PRIVATE METHODS

        private Guid Private_GetFolderIdOf(Guid prodId)
        {
            return (Guid)m_prodToFolderTable[prodId];
        }


        private Guid Private_GetMasterProdOf(Guid folderId)
        {
            return (Guid)m_folderToMasterTable[folderId];
        }


        private ArrayList Private_GetProdIdsLocatedIn(Guid folderId)
        {
            return (ArrayList)m_folderToProdsTable[folderId];
        }

        private void Private_GetDirectlyAffectedProds(Guid folderId, ArrayList affectedProds)
        {
            ArrayList prods = Private_GetProdIdsLocatedIn(folderId);
            if (null != prods)
                affectedProds.AddRange(prods);

            Hashtable childFolders = m_folderTree.GetChildrenOf(folderId);
            if (null == childFolders)
                return;
            foreach (Guid childFolderId in childFolders.Keys)
            {
                // folderId
                Guid masterProdId;
                try
                {
                    masterProdId = Private_GetMasterProdOf(childFolderId);
                    affectedProds.Add(masterProdId);
                }
                catch
                {
                    // this folder doesn't have master
                    Private_GetDirectlyAffectedProds(childFolderId, affectedProds);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="affectedPolicies"></param>
        /// <param name="?"></param>
        /// <returns>New affectedPolicies table (key:policyid,value:(bool)new result) if it needs to apply down any further for master product</returns>
        private Hashtable Private_ApplyToProduct(Guid productId, Hashtable newInherits, bool bTurnOffOptimizer)
        {
            LoadAssocForThisProduct(productId); // don't need but call for safety

            Hashtable newResults = new Hashtable(100);

            CAssocCalc assoc = new CAssocCalc();

            foreach (Guid policyId in newInherits.Keys)
            {
                // newInherit must be new result because it's inheritance (no force)
                bool newInherit = (bool)newInherits[policyId];

                // 06/02/07 mf. OPM 4873.  When applying inherits to a program, we also now
                // need to know more: if there is an above master that has the FN set for
                // this policy.

                Guid FNProgram = m_ForceNoManager.IsFnAssoc(policyId, productId);
                bool isForcedNoByMaster = (FNProgram != Guid.Empty);

                string assocKey = productId.ToString() + policyId.ToString();
                if (!m_assocTable.ContainsKey(assocKey)) // This association is new
                {
                    if (newInherit == false)
                    {
                        if (isForcedNoByMaster == false)
                        {
                            // Inherit false and this assoc not FN'd -> Nothing we need to do.
                            if (bTurnOffOptimizer)
                                newResults[policyId] = false;
                            continue;
                        }
                        else
                        {
                            // Inherit false, but this association is FN'd. ->
                            // We will need to set our override, but only need a new row here.  Force, Inherit
                            StaticPrivate_InsertNewAssocRow(m_dsAssoc, policyId, productId, E_UiPolicyProgramAssociation.No, false);
                        }
                    }
                    else
                    {
                        if (isForcedNoByMaster == false)
                        {
                            // We have a master inhert, and no FN ->
                            // Create the M association row.
                            StaticPrivate_InsertNewAssocRow(m_dsAssoc, policyId, productId, E_UiPolicyProgramAssociation.Blank, true);
                        }
                        else
                        {
                            // We have both a master inherit and a FN setting for this assoc. ->
                            // Need a row that has master inherit and FN.
                            // Should this be possible?
                            StaticPrivate_InsertNewAssocRow(m_dsAssoc, policyId, productId, E_UiPolicyProgramAssociation.No, true);
                        }
                    }

                    // For Masters:
                    // newResults[] is basically newInherits + any changes as a result of manual
                    // overriding at this master level.
                    // ie. This master inherits assoc from above master, but sets N manually.
                    // Then newInherit will reflect.
                    //
                    // For everyone else: NOTHING

                    newResults[policyId] = (isForcedNoByMaster) ? false : newInherit;
                    continue;
                }

                DataRow assocRow = (DataRow)m_assocTable[assocKey];
                assoc.RecycleFor(assocRow);

                if (bTurnOffOptimizer)
                {
                    if (newInherit != assoc.InheritValFromMaster)
                    {
                        //Tools.LogRegTest( string.Format( "Hm,found one rule association inheritance out of sync, fixed now. Policy id = {0}, Product id = {1}, old inherit = {2}, new inherit = {3}"
                        //	, policyId.ToString(), productId.ToString(), assoc.InheritValFromMaster.ToString(), newInherit.ToString() ) );
                        assoc.InheritValFromMaster = newInherit;    // found one that is out of sync.
                    }

                    switch (assoc.AssocOverrideTri)
                    {
                        case E_TriState.No:
                            newResults[policyId] = false;
                            break;
                        case E_TriState.Yes:
                            newResults[policyId] = true;
                            break; // if it gets forced, don't need to go any further
                        case E_TriState.Blank:
                            newResults[policyId] = (isForcedNoByMaster) ? false : newInherit;
                            break;
                    }
                }

                // If what we are now setting is same as existing, we are done.
                if (newInherit == assoc.InheritValFromMaster)
                {
                    if (!isForcedNoByMaster)
                        continue;
                }
                else
                    assoc.InheritValFromMaster = newInherit;

                // Master FN is inherited as N manual association for the pricing engine.
                if (isForcedNoByMaster && assoc.AssocOverrideTri != E_TriState.No)
                    assoc.AssocOverrideTri = E_TriState.No;

                switch (assoc.AssocOverrideTri)
                {
                    case E_TriState.No:
                    case E_TriState.Yes:
                        continue; // if it gets forced, don't need to go any further
                    case E_TriState.Blank:
                        // This should always be the case because otherwise the recursion logics
                        // would have stopped at the master level. In another word, the inherit
                        // value and the result of this row is always the same as the master
                        // Since the master result changes, it must inherit the change as well.

                        newResults[policyId] = newInherit;

                        break;
                }
            }
            return newResults;
        }


        /// <summary>
        /// Spread associations from the given master to its regular products and then calling to spreadt the masters located in subfolders recursively.
        /// Note that the products that derive from the affected base products won't be taken care here.  
        /// </summary>
        /// <param name="srcMasterProdId">start spreading from this master</param>
        /// <param name="srcMasterProdIdOnlyBranch">If this it not Guid.Empty, then it's the only child branch srcMasterProdId should spread to. Typically this is the new master gets added and the srcMasterProdId would be the master above it</param>
        /// <param name="newInherits"></param>
        /// <param name="modifiedProds">Hashtable to store the modified non-master prods got affected</param>
        private void Private_SpreadAssocsForMaster(Guid srcMasterProdId, Hashtable newInherits, bool bTurnOffOptimizer, Hashtable modifiedProds)
        {
            if (newInherits.Count == 0)
                return;

            if (!Private_IsMaster(srcMasterProdId))
            {
                Tools.LogWarning("CAssocInheritUpdate.SpreadInheritanceForMaster shouldn't be called for this non-master product with id =" + srcMasterProdId.ToString());
                return;
            }

            ArrayList directAffectedProdIds = new ArrayList(100);
            Private_GetDirectlyAffectedProds(Private_GetFolderIdOf(srcMasterProdId), directAffectedProdIds);

            LoadAssocForProducts(directAffectedProdIds);
            foreach (Guid prodId in directAffectedProdIds)
            {
                if (prodId == srcMasterProdId)
                    continue;   // this is the case for the master product is listed as part of the folder but shouldn't 
                                // be processed here.

                if (!Private_IsMaster(prodId))
                    modifiedProds[prodId] = null; // record that it's being modified

                Hashtable newResults = Private_ApplyToProduct(prodId, newInherits, bTurnOffOptimizer); // this would apply to the masterProductId as well.		

                if (newResults.Count > 0 && Private_IsMaster(prodId))
                {
                    Private_SpreadAssocsForMaster(prodId, newResults, bTurnOffOptimizer, modifiedProds); // recursive
                }
            }
        }


        private bool Private_IsMaster(Guid productId)
        {
            try
            {
                return (bool)m_prodToIsMasterTable[productId]; // key: productId, value: Private_IsMaster bit
            }
            catch { return false; }
        }


        // OPM 10296
        void LoadAssocForProducts(ArrayList productIds)
        {
            List<Guid> missingIds = null;
            foreach (Guid productId in productIds)
                if (m_loadedProducts.Contains(productId) == false)
                {
                    if (missingIds == null)
                        missingIds = new List<Guid>();
                    missingIds.Add(productId);
                }

            if (missingIds == null)
                return;

            List<SqlParameter> parameters = new List<SqlParameter>(missingIds.Count);

            CDataSet ds = new CDataSet();

            // 11/21/2007 ThienNguyen - Reviewed and safe

            // Case 12107:   "Use SQL Parameters to overcome Ad Hoc SQL's Performance Issues" in Lpe Loading Data 
            string selectSql = "select * from product_price_association where productId " +
                              DbTools.CreateParameterized4InClauseSql("Pr", missingIds, parameters);

#if DEBUG
            Tools.LogRegTest("Sql for LoadAssocForProducts = " + selectSql);
#endif

            ds.LoadWithSqlQueryDangerously(DataSrc.LpeSrc, selectSql, parameters);

            CDataRow row = new CDataRow();
            if (m_dsAssoc == null)
            {
                // first time : load
                m_dsAssoc = ds;

                m_assocTable = new Hashtable(ds.Tables[0].Rows.Count);
                foreach (DataRow rawRow in ds.Tables[0].Rows)
                {
                    row.RecycleFor(rawRow);
                    string key = row.GetGuidField("ProductId").ToString() + row.GetGuidField("PricePolicyId").ToString();
                    m_assocTable[key] = rawRow;
                }
            }
            else
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    DataRow rawRow = m_dsAssoc.Tables[0].LoadDataRow(r.ItemArray, true);
                    row.RecycleFor(rawRow);
                    string key = row.GetGuidField("ProductId").ToString() + row.GetGuidField("PricePolicyId").ToString();
                    m_assocTable[key] = rawRow;
                }
            }

            foreach (Guid productId in missingIds)
                m_loadedProducts[productId] = null;
        }


        void LoadAssocForThisProduct(Guid prodId)
        {
            if (m_loadedProducts.Contains(prodId))
                return;

            ArrayList productIds = new ArrayList(1);
            productIds.Add(prodId);

            LoadAssocForProducts(productIds);
        }

        #endregion // PRIVATE METHODS
    }

    // 06/27/07 mf. OPM 4873. For the new ForceNo setting, we use this class
    // to report existing FN settings that may occur at a higher level than
    // the current program.  We need to keep an eye on the performance of this
    // implementation.
    public class ForceNoManager
    {
        #region Private Members

        private Hashtable m_forceNoAssocs; // key:programId, value: ArrayList of policyIds FN'd
        private Hashtable m_fnPolicyCache; // key:policyId.
        private Guid m_BrokerId;

        private Hashtable m_prodToIsMasterTable; // key: productId, value: IsMaster bool
        private CFolderTree m_folderTree;
        private Hashtable m_prodToFolderTable; // key: productId, value: folderId
        private Hashtable m_folderToMasterTable; //key: folderId, value: master product id, empty folder is Guid.Empty

        #endregion

        #region Constructors

        // For use in UI reporting in rule association
        public ForceNoManager(Guid BrokerId)
        {
            m_BrokerId = BrokerId;
            LoadLender();
            LoadAllForceNoAssocs();
        }

        // Use in UpdateAndSpread, so we do not double load these tables
        public ForceNoManager(Guid BrokerId, Hashtable ProdToIsMasterTable, Hashtable ProdToFolderTable, Hashtable FolderToMasterTable, CFolderTree FolderTree)
        {
            m_BrokerId = BrokerId;
            m_prodToIsMasterTable = ProdToIsMasterTable;
            m_prodToFolderTable = ProdToFolderTable;
            m_folderToMasterTable = FolderToMasterTable;
            m_folderTree = FolderTree;
            LoadAllForceNoAssocs();
        }

        #endregion

        public Guid IsFnAssoc(Guid policyId, Guid productId)
        {
            // Given this association, report the id of the first
            // FN association found up the program path.
            if (m_fnPolicyCache.Contains(policyId))
            {
                Guid potentialProduct = GetAboveMaster(productId);

                while (potentialProduct != Guid.Empty)
                {
                    if (m_forceNoAssocs.Contains(potentialProduct))
                    {
                        foreach (Guid FNPolicy in (ArrayList)m_forceNoAssocs[potentialProduct])
                        {
                            if (FNPolicy == policyId)
                            {
                                // 06/28/07 mf. It seems that SAE's call all master programs MASTER,
                                // so reporting disassociated Program name mapped from this ID in the UI
                                // is not useful.  We should consider reporting this prog's full path
                                // in rule association UI if it is needed.

                                return potentialProduct;
                            }
                        }
                    }

                    potentialProduct = GetAboveMaster(potentialProduct);
                }
            }
            return Guid.Empty;
        }

        #region Private Methods

        // Load all FN associations of this lender.
        private void LoadAllForceNoAssocs()
        {
            // 11/21/2007 ThienNguyen - Reviewed and safe
            string sql = @"SELECT PricePolicyId, ProductId 
				FROM Product_Price_Association JOIN Loan_Program_Template ON ProductId = lLpTemplateId
				WHERE IsMaster = 1 AND IsForcedNo = 1 AND IsMasterPriceGroup = 0 AND BrokerId = '" + m_BrokerId + "'";

            m_forceNoAssocs = new Hashtable();
            m_fnPolicyCache = new Hashtable();

            CDataSet FnAssocs = new CDataSet();
            FnAssocs.LoadWithSqlQueryDangerously(DataSrc.LpeSrc, sql);


            foreach (DataRow row in FnAssocs.Tables[0].Rows)
            {
                if (!m_fnPolicyCache.Contains(row["PricePolicyId"])) m_fnPolicyCache[row["PricePolicyId"]] = null;

                if (m_forceNoAssocs.Contains(row["ProductId"]))
                {
                    ((ArrayList)m_forceNoAssocs[row["ProductId"]]).Add(row["PricePolicyId"]);
                }
                else
                {
                    ArrayList aL = new ArrayList();
                    aL.Add(row["PricePolicyId"]);
                    m_forceNoAssocs.Add(row["ProductId"], aL);
                }
            }
        }

        // Load up the lender folder structure.
        private void LoadLender()
        {
            // We need to know the lender's folder tree and program mapping to report FN status.
            // We may want to eventually use a static method for this, as we load similarly in
            // CAssocInheritUpdate.UpdateAndSpread()
            CDataRow row = new CDataRow();

            // Load this lender's program folder tree
            DataSet dsFolders = new DataSet();
            DataSetHelper.Fill(dsFolders, DataSrc.LpeSrc, "ListLoanProductFolderByBrokerID", new SqlParameter[]{ new SqlParameter( "@BrokerId", m_BrokerId ),
                                                                                                                    new SqlParameter( "@IsLpe", true ) });
            m_folderTree = new CFolderTree(dsFolders.Tables[0].Rows, "ParentFolderId", "FolderId");


            // Load mapping program->folder, program->IsMaster
            CDataSet dsProductInfo = new CDataSet();

            // 11/21/2007 ThienNguyen - Reviewed and safe
            dsProductInfo.LoadWithSqlQueryDangerously(DataSrc.LpeSrc,
                string.Format("select lLpTemplateId as ProductId, IsMaster, FolderId from loan_program_template where brokerid = '{0}' and IsMasterPriceGroup = 0",
                m_BrokerId.ToString()));

            DataRowCollection rows = dsProductInfo.Tables[0].Rows;
            m_prodToFolderTable = new Hashtable(rows.Count);
            m_prodToIsMasterTable = new Hashtable(rows.Count);

            foreach (DataRow rawRow in rows)
            {
                row.RecycleFor(rawRow);
                Guid productId = row.GetGuidField("ProductId");

                if (!m_prodToFolderTable.ContainsKey(productId))
                {
                    Guid folderId;
                    try { folderId = row.GetGuidField("FolderId"); }
                    catch { folderId = Guid.Empty; }
                    m_prodToFolderTable.Add(productId, folderId);
                }

                m_prodToIsMasterTable.Add(productId, row.GetBoolField("IsMaster"));
            }

            // Load Folder->Master mapping
            CDataSet dsFolderContentInfo = new CDataSet();

            // 11/21/2007 ThienNguyen - Reviewed and safe
            dsFolderContentInfo.LoadWithSqlQueryDangerously(DataSrc.LpeSrc,
                string.Format("select FolderId, lLpTemplateId as productId, IsMaster from loan_program_template where brokerid = '{0}' and IsMasterPriceGroup = 0",
                m_BrokerId.ToString()));

            rows = dsFolderContentInfo.Tables[0].Rows;
            m_folderToMasterTable = new Hashtable(rows.Count);

            foreach (DataRow rawRow in rows)
            {
                row.RecycleFor(rawRow);
                Guid folderId;
                try { folderId = row.GetGuidField("FolderId"); }
                catch { folderId = Guid.Empty; }

                Guid productId = row.GetGuidField("ProductId");

                if (row.GetBoolField("IsMaster"))
                {
                    Tools.Assert(!m_folderToMasterTable.ContainsKey(folderId), string.Format("Product folder with id = {0} is having more than 1 master product."
                        , folderId.ToString()));
                    m_folderToMasterTable[folderId] = productId;
                }
            }
        }

        // If ProgramId is regular program, return the governing master (if any)
        // If ProgramId is a master itself, return the above governing master from it (if any)
        // If none found, return Guid.Empty.
        private Guid GetAboveMaster(Guid ProgramId)
        {
            Guid folderId = (Guid)m_prodToFolderTable[ProgramId];

            if ((bool)m_prodToIsMasterTable[ProgramId])
            {
                // This is a master. We consider the above master its master.
                // If this is non-root, start the search on the parent folder, if it exists.
                if (folderId != Guid.Empty)
                {
                    // 09/05/07 mf. OPM 17867.  The folder to search next can
                    // be Guid.Empty here.  It means we are one level below root,
                    // so we should return the root master if it exists,
                    // or Guid.Empty if it does not.
                    folderId = m_folderTree.GetParentFolderOf(folderId);
                }
                else
                {
                    // 08/08/07 mf OPM 17537.
                    // This is the root master.  Nothing can be above.
                    return Guid.Empty;
                }
            }

            // Go up folder heiarchy looking for a master.
            while (Guid.Empty != folderId && !m_folderToMasterTable.ContainsKey(folderId))
            {
                folderId = m_folderTree.GetParentFolderOf(folderId);
            }

            if (m_folderToMasterTable.ContainsKey(folderId))
                return (Guid)m_folderToMasterTable[folderId];

            return Guid.Empty;
        }
        #endregion
    }


}