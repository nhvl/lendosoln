﻿using System;
using DataAccess;
using LendersOffice.Common;

namespace LendersOfficeApp.los.RatePrice
{
    public class PricingCalcException : CBaseException
    {
        string m_regTestMsg;
        string m_error, m_developerError;

        public PricingCalcException(Exception exc, string regTestMsg, Guid ruleId, Guid policyId)
            : base(ErrorMessages.Generic, string.Format("RuleId={0}, PricePolicyId={1}", ruleId, policyId))
        {
            CBaseException baseExc = exc as CBaseException;

            string userMsg = "";
            string devMsg = "";

            if (null != baseExc)
            {
                userMsg = baseExc.UserMessage;
                devMsg = baseExc.DeveloperMessage;
            }
            else
            {
                userMsg = exc.Message;
                devMsg = exc.Message;
            }

            m_error = userMsg;
            m_developerError = string.Format("{0}. RuleId={1}, PricePolicyId={2}", devMsg, ruleId, policyId);
            m_regTestMsg = regTestMsg;
        }

        public string LogMsg
        {
            get { return m_regTestMsg; }
        }

        public string ErrorMsg
        {
            get { return m_error; }
        }

        public string ErrorMsg4Developer
        {
            get { return m_developerError; }
        }

    }


}
