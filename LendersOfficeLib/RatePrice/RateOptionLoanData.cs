﻿// <copyright file="RateOptionLoanData.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Matthew Flynn
//  Date:   3/25/2016
// </summary>

namespace LendersOffice.RatePrice
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// <para>
    /// This is a simple POD class to be serialized per rate,
    /// for passing list of loan file fields in pricing result.
    /// </para><para>
    /// In PML result display, users need to see what each rate
    /// rate option would do to the loan data if that rate were 
    /// submitted.
    /// </para><para>
    /// Initally created for the batch of fields for case 233679,
    /// we bring in the others or extend it out as we need more
    /// fields rather than add each to the rate option one-by-one.
    /// </para>
    /// </summary>
    public sealed class RateOptionLoanData
    {
        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sPurchPrice { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sAltCost { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sLandCost { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sRefPdOffAmt1003 { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotEstPp1003 { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotEstCcNoDiscnt1003 { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sFfUfmip1003 { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sLDiscnt1003 { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotTransC { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sONewFinBal { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotCcPbs { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sOCredit1Desc { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sOCredit1Amt { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sOCredit2Desc { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sOCredit2Amt { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sOCredit3Desc { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sOCredit3Amt { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sOCredit4Desc { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sOCredit4Amt { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sOCredit5Amt { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.  This does not actually map to the loan's sONewFinCc value.  It maps to sONewFinCcAsOCreditAmt_rep.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sONewFinCc { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.  This actually maps to sONewFinCc for the loan.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sONewFinCcInverted { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotLiquidAssets { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sLAmtCalc { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sFfUfmipFinanced { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sFinalLAmt { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTransNetCash { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTransNetCashNonNegative { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of this loan field for this rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sMonthlyPmt { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the loan is configured to pull the prorations into the 1003 details.
        /// </summary>
        /// <value>A value indicating whether the loan is configured to pull the prorations into the 1003 details.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public bool sIsIncludeProrationsIn1003Details { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the prorations are included in the total prepaids for the loan.
        /// </summary>
        /// <value>A value indicating whether the prorations are included in the total prepaids for the loan.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public bool sIsIncludeProrationsInTotPp { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the total estimated prepaids.
        /// </summary>
        /// <value>The value of the field from the loan for the total estimated prepaids.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotEstPp { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the total borrower paid prorations.
        /// </summary>
        /// <value>The value of the field from the loan for the total borrower paid prorations.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotalBorrowerPaidProrations { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the other financing closing costs are included in the estimated closing costs for the loan.
        /// </summary>
        /// <value>A value indicating whether the other financing closing costs are included in the estimated closing costs for the loan.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public bool sIsIncludeONewFinCcInTotEstCc { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the total estimated closing costs without the discount.
        /// </summary>
        /// <value>The value of the field from the loan for the total estimated closing costs without the discount.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotEstCcNoDiscnt { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it's corresponding field is locked.
        /// </summary>
        /// <value>The value of the field from the loan for the rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public bool sAltCostLckd { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of the field from the loan for the rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sLandIfAcquiredSeparately1003 { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it's corresponding field is locked.
        /// </summary>
        /// <value>The value of the field from the loan for the rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public bool sLDiscnt1003Lckd { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of the field from the loan for the rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sPurchasePrice1003 { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of the field from the loan for the rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sRefNotTransMortgageBalancePayoffAmt { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of the field from the loan for the rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sRefTransMortgageBalancePayoffAmt { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of the field from the loan for the rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sSellerCreditsUlad { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it's corresponding field is locked.
        /// </summary>
        /// <value>The value of the field from the loan for the rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public bool sTotCcPbsLocked { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of the field from the loan for the rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotCreditsUlad { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of the field from the loan for the rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotEstBorrCostUlad { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it's corresponding field is locked.
        /// </summary>
        /// <value>The value of the field from the loan for the rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public bool sTotEstCc1003Lckd { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it's corresponding field is locked.
        /// </summary>
        /// <value>The value of the field from the loan for the rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public bool sTotEstPp1003Lckd { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of the field from the loan for the rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotMortLAmtUlad { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of the field from the loan for the rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotMortLTotCreditUlad { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of the field from the loan for the rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotOtherCreditsUlad { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of the field from the loan for the rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTotTransCUlad { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of the field from the loan for the rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTransNetCashUlad { get; set; }

        /// <summary>
        /// Gets or sets the value of the field from the loan for the rate.
        /// </summary>
        /// <value>The value of the field from the loan for the rate.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public string sTRIDSellerCredits { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the loan is targeting Ulad 2019.
        /// </summary>
        /// <value>The value sIsTargetingUlad2019.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "To match with current naming scheme.")]
        public bool sIsTargetingUlad2019 { get; set; }
    }
}
