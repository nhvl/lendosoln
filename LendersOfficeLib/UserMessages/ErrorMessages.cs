using System;
using System.Collections;
using DataAccess;
using LendersOffice.Common.SerializationTypes;

namespace LendersOffice.Common
{
	public class ErrorMessages
	{
        public static class PDD
        {
            public const string FieldTooLong = "Field \"{0}\" is too long for PDD export. Maximum length: {1} Current length: {2}.";
            public const string EmptyNoteDate = "The Note Date is currently blank. A valid Note Date is required for PDD export.";
            public const string EmptyLenderID = "The Lender ID is currently blank. A valid Lender ID is required for PDD export.";
            public const string Empty1PaymentDueDate = "The 1st Payment Due Date is currently blank. A valid 1st Payment Due Date is required for PDD export.";
            public const string EmptyHouseValue = "The House Value is currently $0. Please set Purchase Price or Appraised Value.";
            public const string EmptyPoolSettlementDate = "The Mortgage Pool Settlement Date is currently empty. A Settlement Date is required for PDD export.";
            public const string EmptyInitialPaymentDate = "The Mortgage Pool Certificate Initial Payment Date is currently empty. An Initial Payment Date is required for PDD export.";
            public const string EmptyIssueDate = "The Mortgage Pool Issue Date is currently empty. An Issue date is required for PDD export.";
            public const string EmptyMaturityDate = "The Mortgage Pool Maturity Date is currently empty. A Maturity date is required for PDD export.";
            public const string EmptyIssueTypeCode = "The Mortgage Pool Issue Type Code is currently empty. A valid Type Code is required for PDD export.";
            public const string WrongIssueTypeCode = "The Mortgage Pool Issue Type Code is not set to a recognized value for PDD export. Recognized values are: ";
            public const string EmptyPoolTypeCode = "The Mortgage Pool Pool Type Code is currently empty. A valid Type Code is required for PDD export.";
            public const string WrongPoolTypeCode = "The Mortgage Pool Pool Type Code is not set to a recognized value for PDD export. Recognized values are: ";
            public const string ZeroTerm = "The Mortgage Pool Term is currently 0. A nonzero Term is required for PDD export.";
            public const string ZeroLoanCount = "The Loan Count is currently 0. Please add loans to the pool for PDD export.";
            public const string PIAccountNumber = "Principal and Interest Account Number is empty. Please enter an Account Number for PDD export.";
            public const string PIRoutingNumber = "Principal and Interest Routing Number is invalid. Please enter a 9-digit Routing Number for PDD export.";
            public const string TIAccountNumber = "Taxes and Insurance Account Number is empty. Please enter an Account Number for PDD export.";
            public const string TIRoutingNumber = "Taxes and Insurance Routing Number is invalid. Please enter a 9-digit Routing Number for PDD export.";
            public const string SettlementAccountNumber = "Settlement Account Number is empty. Please enter an Account Number for PDD export.";
            public const string SettlementRoutingNumber = "Settlement Routing Number is invalid. Please enter a 9-digit Routing Number for PDD export.";
        }

        public static class ULDD
        {
            public const string EmptyNoteDate = "The Note Date is currently blank. A valid Note Date is required for ULDD export.";
            public const string EmptyLenderID = "The Lender ID is currently blank. A valid Lender ID is required for ULDD export.";
            public const string Empty1PaymentDueDate = "The 1st Payment Due Date is currently blank. A valid 1st Payment Due Date is required for ULDD export.";
            public const string EmptyHouseValue ="The House Value is currently $0. Please set Purchase Price or Appraised Value.";
            public const string GseTargetOtherExportNotAvailable = "ULDD export is not available when the target GSE is Other/None.";
            public const string GSeTargetGinnieMaeExportNotAvailable = "ULDD export is not available when the target GSE is Ginnie Mae";
            public const string FieldReviewAppraiserLicenseNumberRequired = "Appraiser license number must be present when valuation method = \"Field Review\".";
            public const string GfeLine1008Error = "Cannot determine the type of GFE line 1008";
            public const string GfeLine1009Error = "Cannot determine the type of GFE line 1009";
            public const string AltLenderRequired = "A Lender official contact record or an Alternate Lender is required for ULDD export.";
        }

        public static class EDocs
        {
            public const string InvalidVersion = "The document version has changed recently.";
            public const string InsufficientPdfPermission = "Cannot edit this document. The document is protected with either owner password or user password.";
            public const string InsufficientCreatePermission = "User does not have permission to create EDocs.";
            public const string InsufficientReadPermission = "User does not have permission to view EDocs.";
            public const string DocTypeNameInUse = "A doc type with the same name already exists. Please select another name.";
            public const string FolderNameInUse = "A folder with the same name already exists. Please select another name.";
            public const string ShippingTemplateNameInUse = "A shipping template with the same name already exists. Please select another name.";
            public const string ShippingTemplateNotFound = "No shipping template with the provided name was found.";
            public const string DocumentNotFoundException = "Document Not Found.";
            public const string InvalidPDF = "Invalid PDF";
            public const string DuplicateFolderName = "A folder with the given name already exists. Please use a different name.";
            public const string InvalidUploadFilename = "Filenames cannot contain \"&#\".  Please rename the file and try uploading again.";
            public const string InvalidEDocsServiceDomain = "This web service method is not allowed at this URL. Please use edocs.lendingqb.com.";
            public const string EmptyShippingTemplateOrEDocList = "The loan file has no Edocs matching the shipping template.";
            public const string InvalidXML = "XML file is not compatible with edocs.";
            public const string InvalidUcd = "The provided UCD content is invalid.";
            public const string InvalidAppraisalXml = "The provided content is not valid LQB Appraisal XML.";
            public const string DocIsNotPrepared = "The document is not prepared.";
        }

        /// <summary>
        /// OPM 119251, 1/8/2016, ML
        /// </summary>
        public static class EdocsImporter
        {
            public const string DuplicateFoldersFoundMessageFormat = "Duplicate folder '{0}' found in the import file.";
            public const string DuplicateDocTypeFoundMessageFormat = "Duplicate doc type '{0}' found in import file.";
            public const string DuplicateDocTypeMappingFoundMessageFormat = "Multiple mapping rows for same doc type '{0}' found in import file.";
            public const string InvalidRolePresentMessageFormat = "Role '{0}' specified in the import file is not currently supported by the importer.";
            public const string UnknownFolderSpecifiedMessageFormat = "Folder '{0}' does not exist, please add it to this broker before adding document types stored in that folder.";
            public const string UnknownDocTypeSpecifiedMessageFormat = "Doc type '{0}' does not exist for this broker, please add it to this broker before adding mappings for that doc type.";
            public const string InvalidDocVendorDocTypeMessageFormat = "{0} document type '{1}' does not exist.";
            public const string InvalidImportFileLineMessageFormat = "Parsing error on line {0}: {1}\n";
            public const string FolderNameExceedsMaxLengthMessageFormat = "Folder '{0}' in import file exceeds max folder name length {1}.";
            public const string DocTypeNameExceedsMaxLengthMessageFormat = "Doc type '{0}' in folder '{1}' in import file exceeds max doc type name length {2}.";
            public const string NoFoldersSpecifiedMessage = "No folders were specified in the input file.";
            public const string NoDocTypesSpecifiedMessage = "No document types were specified in the input file.";
            public const string NoMappingsSpecifiedMessage = "No vendor mappings were specified in the input file.";
            public const string UnknownImportTypeEncounteredMessage = "An unknown import type was encountered. Please try again.";
        }

        public static class ConfigSystem
        {
            public const string ValidationErrors = "There were validation errors in the system configuration";
        }

        public static class CustomPDF
        {
            public const string InvalidField_a1003InterviewD = "Error:\nInvalid field: a1003InterviewD\n Please use: GetPreparerOfForm(App1003Interviewer).PrepareDate instead.";
        }

        public class ConsumerPortal
        {
            public const string NameOnFileDoesNotMatchRequest = "The rows in red indicate that the borrower name(s) on the application no longer matches the borrower name(s) on the requested documents."; //vs 6/11/10
        }

        public static class PML
        {
            public const string AppErrorFailedToClose = "Due to browser security settings, we were unable to close your window for you. In order to minimize further errors, we recommend that you close this browser window and log in again.";
            public const string InsufficientPermissionToSubmitWithoutCredit = "User does not have permission to register/lock a loan without first ordering a credit report.";
        }

        public static class Buydown
        {
            public const string PrematureEmptyTerm = "A buydown term cannot be 0 if a later term has a value.";
            public const string NonMatchingRateChange = "The change in rate between buydown terms must be constant.";
            public const string PrematureEmptyRateChange = "Buydown rate reductions must decrease every term.";
            public const string MismatchedRate = "A buydown rate reduction cannot be zero if its corresponding term has a value";
            public const string MismatchedTerm = "A buydown term cannot be zero if its corresponding rate reduction has a value";
        }

        public static class Flood
        {
            public const string BadFormat = "The flood response was not formatted properly. Please contact your LendingQB system administrator.";
            public const string EdocSetupIncomplete = "Failed to upload certificate. Edocs must be enabled and a flood doctype must be specified. Please contact your LendingQB system administrator to complete the flood integration setup.";
            public const string InsufficientEdocPermission = "You do not have permission to upload the flood certificate to EDocs. Please contact your administrator.";
            public const string NoResponse = "FLOOD ORDER FAILED: No response was received.";
            public const string Timeout = "The flood provider is not responding. Please wait a few minutes and then retry with the Query for Manual Report order type. Contact your LendingQB system administrator if the order fails again.";
            public const string UnexpectedResponse = "An unexpected response type was returned. Please retry in a few minutes. Contact your LendingQB system administrator if the order fails again.";
            public const string UnknownError = "The flood order could not be completed. Please retry in a few minutes. Contact your LendingQB system administrator if the order fails again.";
            public const string UnrecognizedResponse = "The flood response could not be read.";
            
            public const string Manual = "Additional information is required to complete the flood order. Please contact your flood provider. Afterwards the Query for Manual Report order type can be used to pull the certificate.";
        }

        public static class GeoCode
        {
            public const string EmptyAddress = "Unable to import geocodes. Please fill out the full subject property address (street address, city, state, zip, and county) and try again.";
            public const string LibraryReturnedError_DEV = "GeoCode library returned an error.";
            public const string EmptyAddress_DEV = "GeoCode import was aborted due to lack of subject property address.";
        }

        public static class Title
        {
            public const string AppRelationshipMismatch = "Order failed. Each borrower on title should have a Relationship Title Type, but the number of relationships does not match the number of borrowers. Please contact your LendingQB system administrator.";
            public const string AppRelationshipMismatchDevMsg = "The number of Relationship Title Types does not match the number of applications.";
            public const string InsufficientTitlePermission = "You do not have permission to order title services.";
            public const string RelationshipTitleTypesAreRequired = "Order failed. Please fill out the Relationship Title Type for each borrower on title and then retry.";
            public const string TitlePolicyAlreadyOrdered = "Order aborted due to existing Preliminary Report Ordered Date. If there is a Policy Information section at the top of this page with a Policy Id then there is an existing order. Otherwise please clear the Preliminary Report Ordered Date (General Status page) and retry.";
        }

        public static class SubmitToThirdParty
        {
            public const string BadURL = "Submission failed. The system could not determine the target URL.";
            public const string ConnectionFailure = "Submission failed. The system could not connect to the third party.";
            public const string GenericFailure = "Submission failed. Please wait a few minutes and try again. Contact your LendingQB system administrator if submission fails again.";
            public const string PageDisabled = "The Submit to 3rd Party page is disabled.";
            public const string PagePermissionRequired = "You lack permission to access the Submit to 3rd Party page.";
            public const string SubmitPermissionRequired = "You do not have permission to submit to this third party.";
        }

        public static class BatchExport
        {
            public const string BatchExportFailure = "The requested batch export could not be processed. Please contact your LendingQB system administrator for more information.";
            public const string BEFieldPermissionRequired = "The requested batch export includes restricted fields that you do not have permission to access.";
            public const string BEFormatNotFound = "The requested Batch Export Format Name was not found. Please verify that the batch export format is currently available within LendingQB.";
            public const string BEFrameworkPermissionRequired = "You do not have permission to access batch exports.";
            public const string BERetrievalFailure = "The requested batch export file could not be retrieved. Please contact your LendingQB system administrator for more information.";
            public const string NullResult = "Batch Export request failed. Please contact your LendingQB system administrator for assistance.";
        }

        public static class CustomReport
        {
            public const string CustomReportFeaturePermissionRequired = "You do not have permission to access custom reports.";
        }

        public static class WebServices
        {
            public const string ActiveDirectoryNotEnabled = "Active Directory authentication is not enabled. Please contact your account administrator to enable AD authentication.";
            public const string EmptyLoanBatch = "The input LOXML file does not contain any loan numbers";
            public const string InvalidAuthTicket = "The provided authorization ticket is invalid.";
            public const string InvalidCredentials = "Please verify the username, password and customer code.";
            public const string LoanBatchReadAccessDenied = "You do not have permission to access one or more loans in the list provided.";
            public const string MustLoginAsADUser = "This user authenticates via Active Directory. Please use the GetActiveDirectoryUserAuthTicket method to retrieve a ticket for this user.";
            public const string InvalidAppCode = "Please verify the appcode.";
            public const string InvalidLoanNumber = "The provided loan number is invalid.";
            public const string DoDuSessionInvalidExpired = "Your session has expired or is invalid.";
            public const string QuickPricerTimeout = "Please try running quick pricer with fewer loan programs.";
            public const string TemplateRequiredToCreateLoan = "A loan template is required. Please pass in a templateName";
            public const string CannotRunServiceForQuickPricerLoan = "This web service cannot operate on a QuickPricer loan file.";
            public const string QuickPricer2NotEnabled = "Quick Pricer V2.0 is not enabled. Please contact an administrator or use the RunQuickPricerV1 method.";
            public const string InvalidQuickPricer2LoanIdFormat = "The provided quickPricerLoanId has an invalid format.";
            public const string InvalidInputXml = "The input XML was not valid.";
            public const string InvalidInputLOXml = "The input LOXML must not be blank.";

            public static string TemplatePurposeDoesNotMatchQpLoan(string templateName)
            {
                return $"Loan template {templateName} is not intended for use with loans of the selected transaction type. If your quick pricer loan is a purchase then please use a purchase template. If it's a refinance then use a refinance template.";
            }

            public static string InvalidQuickPricerLoan(string quickPricerLoanId)
            {
                return $"There is no quick pricer loan corresponding to quickPricerLoanId {quickPricerLoanId}";
            }

            public static string CannotCreateFiles(bool isCreatingLead = false)
            {
                return $"You do not have permission to create {(isCreatingLead ? "leads" : "loans")}. Please contact an administrator.";
            }

            public static string CannotFindLoanTemplate(string templateName)
            {
                return $"Loan template {templateName} could not be found. Please verify that this loan template exists.";
            }

            public static string QuickPricer2FileNotFound(string quickPricerLoanId)
            {
                return $"There is no quick pricer loan corresponding to quickPricerLoanId {quickPricerLoanId}."
                    + " Please retry and leave quickPricerLoanId blank to generate a new quick pricer loan.";
            }
        }

        public static class DocumentFramework
        {
            public const string AsyncSuccess = "Your message was processed successfully.";
            public const string AsyncInvalidFormat = "The message format was invalid.";
            public const string AsyncRoutingFailure = "The message could not be routed.";
            public const string AsyncProcessingFailure = "The message could not be processed.";
        }

        public static class GenericFramework
        {
            public const string UsernameRequired = "This service requires your username with the service provider to be entered into your LendingQB user account. Please contact your user admin.";
            public static string ReceivedInvalidUrl(string vendorName)
            {
                return ConcatenateVendorNameWithDefault(vendorName, " returned an invalid url.");
            }
            public static string ReceivedNoData(string vendorName)
            {
                return ConcatenateVendorNameWithDefault(vendorName, " did not respond with any data.");
            }
            public static string ReceivedInvalidXml(string vendorName)
            {
                return ConcatenateVendorNameWithDefault(vendorName, " responded with invalid data.");
            }
            private static string ConcatenateVendorNameWithDefault(string vendorName, string message)
            {
                vendorName = (vendorName ?? string.Empty).TrimWhitespaceAndBOM();
                return (string.IsNullOrEmpty(vendorName) ? "Vendor" : vendorName) + message;
            }
        }

        public static class ComplianceEagle
        {
            public const string AppIDLookUpFailed = "The primary application ID lookup using the loanID failed to return a valid app ID.";
            public const string AsyncAccountMismatch = "[CEAGLEQUEUE] The ComplianceEagle username or customer ID included in the async request does not match the lender's account settings.";
            public const string BrokerIDLookUpFailed = "The lender lookup using the loanID failed to return a valid BrokerID.";
            public const string CustomerIDLookUpFailed = "The ComplianceEagle customer ID lookup using the lender's BrokerID failed to return a valid customer ID.";
            public const string DocumentAutoSaveOptionMissing = "The EDoc configuration does not have a Document Autosave Option for the Compliance Report. Please contact your LendingQB system administrator for assistance.";
            public const string IgnoreAsyncRequestDueToFeatureDisable = "[CEAGLEQUEUE] Ignoring ComplianceEagle async request for lender.";
            public const string InvalidFieldID = "The ComplianceEagle exporter attempted to load an invalid field.";
            public const string ParseError = "An unexpected response was returned. Please wait a few minutes and try again. Contact your LendingQB system administrator if submission fails again.";

            public static string InvalidDateValue(string value)
            {
                return $"ComplianceEagle returned an invalid date value: {value}";
            }
        }

        public static class MIFramework
        {
            public const string EDocAutoSaveConfigError = "Failed to save document to EDocs. No Document Autosave Option for mortgage insurance documents was found within the EDoc system config. Please contact your administrator.";
            public const string EDocAutoSaveConfigErrorDevMessage = "MI Framework EDoc upload failure due to lack of document autosave config.";
            public const string UnexpectedResponse = "An unexpected response was returned. Please wait a few minutes and try again. Contact your LendingQB system administrator if submission fails again.";

            private const string ContactLQB = "Please contact your LendingQB system administrator to troubleshoot.";
            private const string MissingLoanID = "Please verify that the response includes the LendingQBLoanID key.";
            private const string MissingTransaction = "Please verify that the response includes the OrderNumber key and the MITransactionIdentifier attribute.";

            public static string DeserializationFailure()
            {
                return "An error occurred on attempt to deserialize the MI response. " + ContactLQB;
            }

            public static string LoanOrAppRetrievalFailure()
            {
                return "Failed to locate the loan or application associated with the MI response. " + MissingLoanID + " " + ContactLQB;
            }

            public static string LenderRetrievalFailure()
            {
                return "Failed to locate the lender associated with the MI response. " + MissingLoanID + " " + ContactLQB;
            }

            public static string OrderRetrievalFailure()
            {
                return "Failed to locate the MI order associated with the response. " + MissingTransaction + " " + ContactLQB;
            }

            public static string GenericProcessingFailure()
            {
                return "An error occurred when attempting to process the MI response. " + ContactLQB;
            }

            public static string GenericBadQuote()
            {
                return "The MI vendor returned an unknown error. " + ContactLQB;
            }

            public static string NoCredentials()
            {
                return "The credentials for this MI vendor could not be retrieved. " + ContactLQB;
            }

            public static string QuoteRetrievalFailure()
            {
                return "Failed to load the MI quote. " + ContactLQB;
            }
        }

        public static class TitleFrameworkErrors
        {
            public const string MissingAddress = "The subject property is missing the street address.";
            public const string MissingCity = "The subject property is missing the city.";
            public const string MissingState = "The subject property is missing the state.";
            public const string MissingZip = "The subject property is missing the zip code.";
            public const string MissingCounty = "The subject property is missing the county.";
            public const string MissingYearBuilt = "The subject property is missing the year built.";
            public const string MissingLoanAmount = "The subject property is missing the loan amount.";
            public const string MissingAppraisedValue = "The subject property is missing the appraised value.";
            public const string MissingLienAmount = "The subject property is missing the lien amount";
        }

        public static class VOXErrors
        {
            public const string GenericError = "Something went wrong, please contact your LQB admin.";
            public const string UnexpectedResponse = "An unexpected response was returned. Please wait a few minutes and try again. Contact your LendingQB system administrator if submission fails again.";

            public static class Asset
            {
                public const string MissingAccountNumber = "Selected asset is missing account number.";
                public const string MissingAccountName = "Selected asset is missing account holder name.";
                public const string MissingCashValue = "Selected asset is missing cash value.";
                public const string MissingDescription = "Selected asset is missing cash description.";
                public const string MissingAccountType = "Selected asset is missing account type.";
                public const string MissingFirstName = "Selected asset is missing account owner First Name.";
                public const string MissingLastName = "Selected asset is missing account owner Last Name.";
            }

            public static class Employer
            {
                public const string MissingName = "Selected employer is missing name.";
                public const string MissingAddress = "Selected employer is missing address.";
                public const string MissingCity = "Selected employer is missing city.";
                public const string MissingState = "Selected employer is missing state.";
                public const string MissingZip = "Selected employer is missing ZIP code.";
                public const string MissingEmploymentStatus = "Selected employer is missing employment status";
            }

            public static class Loan
            {
                public const string MissingLenderCaseNumber = "Lender case number cannot be blank.";
            }

            public static class Party
            {
                public const string MissingFirstName = "Borrower's First Name is missing.";
                public const string MissingLastName = "Borrower's Last Name is missing.";
                public const string MissingEmail = "Borrower's Email is missing.";
                public const string MissingSsn = "Borrower's SSN is missing.";
                public const string MissingDateOfBirth = "Borrower's date of birth is missing.";
                public const string AssetHolderMissingAddress = "Asset holder address is missing.";
                public const string AssetHolderMissingCity = "Asset holder city is missing.";
                public const string AssetHolderMissingState = "Asset holder state is missing.";
                public const string AssetHolderMissingZip = "Asset holder zip code is missing.";
            }

            public static class Misc
            {
                public const string MissingUserDisplayName = "User's Display Name is missing.";
            }
        }

        public static class ArchiveError
        {
            public const string CannotGenerateLEAfterIncludingLEInCD =
                "There is a Loan Estimate Archive marked as \"Included in Closing Disclosure\", " +
                "you may not generate another Loan Estimate.";

            public const string ShouldPendingLoanEstimateArchiveBeIncludedInClosingDisclosure =
                "There is a Loan Estimate Archive in the \"Pending Document Generation\" status. " +
                "Do you want to include this data in the Closing Disclosure?";

            public const string CannotManuallyArchiveLEAfterLEIncludedInCD =
                "There is a Loan Estimate Archive marked as \"Included in Closing Disclosure\", " +
                "you may not manually archive the Loan Estimate.";

            public const string InitialLoanEstimateAssociatedWithInvalidArchiveDocs =
                "There is a Loan Estimate marked as Initial which is associated with an Invalid Archive. " +
                "You must change the status of the archive or select another Initial Loan Estimate before proceeding.";

            public const string InitialLoanEstimateAssociatedWithInvalidArchiveCompliance = 
                "There is a Loan Estimate marked as Initial which is associated with an Invalid Archive. " +
                "You must change the status of the archive or select another Initial Loan Estimate before checking compliance status.";

            public const string InitialLoanEstimateAssociatedWithInvalidArchiveAsyncCompliance = 
                "There is a Loan Estimate marked as Initial which is associated with an Invalid Archive. " +
                "Compliance checks will not run until you change the status of the associated archive or select another Initial Loan Estimate.";

            public const string UnableToCoCBecauseNoLastDisclosedArchive =
                "The Last Disclosed Loan Estimate Archive is currently set to \"None\". " +
                "Please create a new archive or set the status of an existing archive " +
                "to \"Disclosed\" in order to apply a Change of Circumstance.";

            public const string ManuallyArchivingSetPendingManualArchiveToInvalid = 
                "This file was previously manually archived and the archive status was set to \"Invalid\".";

            private static string GetBaseMessageForLoanEstimateInUnknownStatus(string date, ClosingCostArchive.E_ClosingCostArchiveType type)
            {
                string archiveTypeDescription;

                switch (type)
                {
                    case ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate:
                        archiveTypeDescription = "Loan Estimate";
                        break;
                    case ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure:
                        archiveTypeDescription = "Closing Disclosure";
                        break;
                    default:
                        throw new UnhandledEnumException(type);
                }

                return "There is a " + archiveTypeDescription + " Archive in an Unknown status for this file from " +
                    date + ". This means documents were generated but LendingQB does not know if they " +
                    "were provided to the borrower or not.";
            }

            public static string UnableToCoCBecauseOfUnknownArchive(string date, ClosingCostArchive.E_ClosingCostArchiveType type, bool userCanDetermineStatus)
            {
                return GetBaseMessageForLoanEstimateInUnknownStatus(date, type) +
                    " In order to proceed with a CoC, " +
                    (userCanDetermineStatus ? 
                        "please select one of the options below:"
                        : "please set the status of this archive.");
            }

            public static string UnableToCoCBecauseOfPendingArchive(ClosingCostArchive.E_ClosingCostArchiveSource source, string date)
            {
                switch (source)
                {
                    case ClosingCostArchive.E_ClosingCostArchiveSource.ChangeOfCircumstance:
                        return "There is currently an archive in the \"Pending Document Generation\" status " +
                        "which was created by a CoC from " + date + ". Is this still a valid CoC?";
                    case ClosingCostArchive.E_ClosingCostArchiveSource.Manual:
                        return "There is currently an archive in the \"Pending Document Generation\" status from " +
                            date + " which was created by Manually Archiving the Loan Estimate.  Please indicate " +
                            "what was done with this data.";
                    default:
                        throw new DataAccess.UnhandledEnumException(source);
                }
            }

            public static string UnableToGenerateDocumentsBecuaseOfUnknownArchive(
                string date,
                ClosingCostArchive.E_ClosingCostArchiveType type,
                bool userCanDetermineStatus)
            {
                return GetBaseMessageForLoanEstimateInUnknownStatus(date, type) +
                    " In order to proceed with document generation, " +
                    (userCanDetermineStatus ?
                            "please select one of the options below:"
                            : "you must specify the status of this archive.");
            }

            public static string UnableToGenerateDocumentsBecauseOfPendingArchive(
                string date,
                ClosingCostArchive.E_ClosingCostArchiveStatus status,
                bool userCanDetermineStatus)
            {
                string friendlyStatus = null;

                switch (status)
                {
                    case ClosingCostArchive.E_ClosingCostArchiveStatus.PendingDocumentGeneration:
                        friendlyStatus = "Pending Document Generation";
                        break;
                    case ClosingCostArchive.E_ClosingCostArchiveStatus.SupersededCoC:
                        friendlyStatus = "Superseded CoC";
                        break;
                    default:
                        // The rest of the statuses are invalid for this error 
                        // and should not be handled.
                        throw new UnhandledEnumException(status);
                }

                return "There is a Loan Estimate Archive in the " + friendlyStatus + " status for this file from " +
                    date + ". This means documents were generated but LendingQB does not know if they " +
                    "were provided to the borrower or not. In order to proceed with document generation, " +
                    (userCanDetermineStatus ? 
                        "please select one of the options below:"
                        : "you must specify the status of this archive.");
            }

            public static string GeneratingClosingPackageWithPendingArchiveWarning(string date)
            {
                return "Warning: There is a Loan Estimate Archive from " + date + " in the Pending Document " +
                    "Generation status.  Please verify this file is ready for closing documents " +
                    "before proceeding.";
            }

            public static string PendingArchiveWithoutLastDisclosedArchive(string date)
            {
                return "There is no last disclosed archive but there is an archive in the " +
                    "\"Pending Document Generation\" status from " + date + ". Please let us know " +
                    "what you would like to do:";
            }

            public static string ManuallyArchivingSetUnknownArchiveToInvalid(string date)
            {
                return "There was a loan estimate archive in the unknown status from " + date +
                    ". Manually archiving has updated its status to invalid.";
            }

            public static string ManuallyArchivingWithPendingCoCArchive(string date)
            {
                return "There is an archive on file in the \"Pending Document Generation\" status from a CoC generated " + 
                    date + ". Proceeding will set this archive to \"Invalid\".";
            }

            public static string ConfirmUnknownArchiveInvalidation(string date)
            {
                return "WARNING: This will mark the archive from " + date + " as Invalid. " +
                    "This will also invalidate any associated CoC. You may need to re-perform any associated CoCs.";
            }
        }

        public class FeeService
        {
            public const string GenericError = "Accurate fees could not be determined for pricing. Please contact your system administrator.";

            public static string MissingSettlementServiceProvider(string systemId)
            {
                return $"Could not retrieve information for contact with system ID {systemId}. This contact may have been disabled or deleted.";
            }
        }

        public static class TpoDocumentOrder
        {
            public const string FailedToRetrievePlanCode = "Registered loan program could not be mapped with document vendor program. Please submit request to lender to disclose on your behalf.";

            public static class InitialDisclosure
            {
                public const string InitialDisclosureAlreadyExists = "An initial disclosure order already exists on this file.";

                public static string FailedToRetrieveAntiSteeringDisclosure()
                {
                    return GetErrorMessage("anti-steering disclosure");
                }

                public static string FailedToGenerateRequestReview()
                {
                    return GetErrorMessage("request review");
                }

                public static string FailedToRetrieveDocumentAudit()
                {
                    return GetErrorMessage("audit");
                }

                public static string FailedToPlaceDocumentOrder(bool isPreview)
                {
                    return isPreview ? GetErrorMessage("preview") : GetErrorMessage("order");
                }

                private static string GetErrorMessage(string step)
                {
                    return $"There was an error generating the document {step}. Please retry or request for the lender to complete the order.";
                }
            }
        }

        public static class TpoColorTheme
        {
            public const string InvalidColorValue = "The following values are invalid for a color choice:";
        }

        public const string QualRateFieldsMustBeMutuallyExclusive = "Qualifying Rate calculation fields must be mutually exclusive.";
        public const string SmsAuthenticationNotEnabled = "SMS authentication not enabled.";
        public const string FailedToDelete = "Failed to delete data.";
        public const string FannieMae30ImportError = "Fannie Mae 3.0 format files are no longer supported. Fannie Mae files must be version 3.2 or higher.";
        public const string NewTestLeadsUnsupported = "Creation of new Test Lead files is not currently supported.";
        public const string AnotherUserMadeChangeToLoan = "Another user has made changes to this loan file. Your changes have not been saved.";
        public const string PmlUserMustHaveAtLeastOneRole = "PML users must have at least one role: Loan Officer, Processor, Secondary, or Post-Closer.";
        public const string InvalidBrokerProcessorRelationship = "This user's Processor (External) does not belong to <Originating Company>.  Please select another Processor (External) from the Relationships tab.";
        public const string InvalidMiniCorrExternalPostCloserRelationship = "This user's Post-Closer does not belong to <Originating Company>. Please select another Post-Closer from the Mini-Correspondent Relationships tab.";
        public const string InvalidCorrExternalPostCloserRelationship = "This user's Post-Closer does not belong to <Originating Company>.  Please select another Post-Closer from the Correspondent Relationships tab.";
        public const string PMLMissingRolesError = "PML users must have at least one role: Loan Officer, Processor, Secondary, Post-Closer, or Supervisor.";
        public const string PMLUserBatchEditMissingRolesError = "PML users must have at least one role: Loan Officer, Processor, Secondary, Post-Closer, or Supervisor.\n\rThe following users could not be updated:";
        public const string Generic = "System error.  Please contact us if this happens again.";  // dl - reviewed 10/11/05
        public const string LoadLoanFailed = "Unable to load data.";  // dl - reviewed 10/11/05
        public const string SaveLoanFailed = "Unable to save data.";  // dl - reviewed 10/11/05
        public const string PostDataFailed = "Unable to post data.";  // dl - reviewed 10/11/05
        public const string LoanNotFound = "Could not find loan application.";  // dl - reviewed 10/11/05
        public const string GenericAccessDenied = "Access denied.";  // dl - reviewed 10/11/05
        public const string IPRestrictionAccessDenied = "Access failed due to ip restriction";
        public const string InsufficientPermissionToEditLoan = "Insufficient permission to edit loan.";  // dl - reviewed 10/11/05
		public const string SiteUnavailable = "The website is currently unavailable.";  // dl - reviewed 10/11/05
		public const string ICreditReportRequestIsNull = "There was a problem with the credit report request (ICreditReportRequest is null).";  // dl - reviewed 10/11/05
		public const string InvalidMismoFormat = "The credit provider is returning data in an invalid MISMO format.";  // dl - reviewed 10/11/05
		public const string CannotProcessMclCreditReport = "Cannot process the credit report on file.  Tradeline status code {0} is not documented.";  // dl - reviewed 10/11/05
		public const string MclEnumValueNotHandled = "System error: Could not process part of the credit data.";  // dl - reviewed 10/11/05
		public const string DataAccessError = "System Error.  This is a temporary problem -- please try your operation again in a few minutes.";  // dl - reviewed 10/11/05
		public const string RateOptionUpdateError = "The rate option update component encountered an error.";  // dl - reviewed 10/11/05
		public const string InvalidSubjPropState = "Invalid subject property state.  Please make sure that it is filled out.";  // dl - reviewed 10/11/05
		public const string CannotChangeNoteRateForLockedRate = "Note rate cannot be changed when the rate is locked.";  // dl - reviewed 10/11/05
		public const string CannotAccessOldLinkedLoan = "Cannot access the linked loan.  It's either no longer linked or has been deleted.";  // dl - reviewed 10/11/05
		public const string InsufficientPermissionToDeleteLinkedLoan = "Insufficient permission to delete linked loan.";  // dl - reviewed 10/11/05
		public const string PricePolicyAssocError = "Encountered an error during price policy association.";  // dl - reviewed 10/11/05
		public const string SpreadRateOptionFailed = "Failed to spread rate option to inherited loan programs after rate options were batch-updated.";  // dl - reviewed 10/11/05
		public const string UpdatePriceEngineVersionDateFailed = "System failed to update pricing engine version number.";  // dl - reviewed 10/11/05
		public const string InsufficientPermissionToModifyLoanPrograms = "Access denied. You do not have permission to modify loan programs.";  // dl - reviewed 01/18/07
		public const string UnableToDeletePricePolicyGroups = "Unable to delete all selected price policy groups.  Check for product associations in the remaining items.";  // dl - reviewed 10/11/05
		public const string MoveDenied = "Moving a parent under its child will create a cycle.  Move denied.";  // dl - reviewed 10/11/05
		public const string EnumValueNotHandled = "System error (unhandled enum value).";  // dl - reviewed 10/11/05
		public const string LoanCopyFailed = "Unable to copy the loan file.";  // dl - reviewed 10/11/05
		public const string CreateTemplateFailed = "Unable to create template from the loan file.";  // dl - reviewed 10/11/05
		public const string GenericNotified = "System error.  System engineers have been notified.";  // dl - reviewed 10/11/05
		public const string PricingSystemBeingUpdated = "The pricing system is currently being updated.";  // dl - reviewed 10/11/05
        public const string UnableToRetrievePublishedReport = "Unable to retrieve published report.";   // dl - reviewed 10/21/05
        public const string UnableToFindRateOption = "Could not find selected rate.  Please try again.";  // dl - reviewed 10/21/05
        public const string Create2nd_LoanIsTemplate = "Unable to create second loan:  You cannot create a second loan from a template file.";  // dl - reviewed 10/28/05
        public const string Create2nd_HasLoanLinked = "Unable to create second loan:  This loan already has a linked second loan.";  // dl - reviewed 10/28/05
        public const string Create2nd_LacksWriteLoan = "Cannot create subfinancing loan file.\r\n\r\nWrite access to this loan is required to create a subfinancing loan file.";
        public const string Create2nd_NotFirst = "Unable to create second loan:  You cannot create a second loan from within another second loan.";  // dl - reviewed 10/28/05
        public const string CreditReport_GenericError = "If your credit request cannot be completed after several tries, please contact your credit reporting agency for assistance.";  // dl - reviewed 12/13/05
        public const string AggrEscrow_FirstPaymentDateRequired = "First payment date is required.";  // dl - reviewed 01/13/06
        public const string UnableToConvertLead = "Unable to convert lead to loan.  Please try again.";  // dl - reviewed 01/26/06
		public const string RebateCannotBeChangedDuringRateLock = "The rebate percentage value cannot be changed when there is a rate lock.";  // dl - reviewed 02/16/06
		public const string InvalidIntegrationFormat = "Invalid format value.";  // dl - reviewed 03/03/06
        public const string CannotFindLoanTemplate = "Unable to find loan template.";  // dl - reviewed 03/03/06
		public const string DuplicateLoanNumber = "Loan number already exists.";  // dl - reviewed 03/22/06
		public const string CannotExtendLockRateIfRateIsNotLocked = "Lock period extension failed because the rate is not currently locked.";  // dl - reviewed 04/14/06
        public const string CannotFloatDownRateIfRateIsNotLocked = "Float down failed because the rate is not currently locked.";  // dl - reviewed 04/14/06
        public const string CannotRelockRateIfRateIsNotLocked = "Relock failed because the rate is not currently locked.";  // dl - reviewed 04/14/06
        public const string InvalidRateOptionVersion = "Pricing has been updated.  Please re-run this scenario.";  // dl - reviewed 05/12/06
        public const string InvalidEmailAddress = "Please enter a valid e-mail address";  // dl - reviewed 09/15/06
        public const string InvalidCompanyID = "Please enter a valid Company ID";
		public const string InvalidUrl = "Please enter a valid website URL (http://...)";  // dl - reviewed 09/15/06
        public const string InvalidRateLockDate = "Please enter a valid rate lock date."; // OPM 232740, 1/14/2016, ML
        public const string InvalidInvestorRateLockDate = "Please enter a valid investor rate lock date."; // OPM 232740, 1/14/2016, ML
        public const string CannotDisableBUserPriceGroup = "A default price group at the employee level cannot be disabled.";
        public const string CannotDisablePUserPriceGroup = "A default price group at the PriceMyLoan User level cannot be disabled.";
        public const string CannotDisableBranchDefaultPriceGroup = "A default price group at the branch level cannot be disabled."; // dl - reviewed 12/07/06
        public const string CannotDisableOriginatingCompanyPriceGroup = "A default price group at the Originating Company level cannot be disabled.";
        public const string CannotDisableCorporateDefaultPriceGroup = "A default price group at the corporate level cannot be disabled."; // dl - reviewed 12/07/06
        public const string CannotDisableTpoPortalDefaultPriceGroup = "A default price group at the Originator Portal Default level cannot be disabled.";
        public const string CannotChangeAmortTypeForLockedRate = "Amortization type cannot be changed when the rate is locked."; // dl - reviewed 12/07/06
        public const string CannotChangeQualRateForLockedRate = "Qualifying rate cannot be changed when the rate is locked."; // dl - reviewed 12/08/06
        public const string CannotChangeTeaserRateForLockedRate = "Teaser rate cannot be changed when the rate is locked."; // dl - reviewed 12/08/06
        public const string LiablityListModifiedError = "Unable to complete operation: the liability list was modified by another user."; // dl - reviewed 01/05/07
		public const string RateLockExpirationCannotBeChangedDuringRateLock = "The rate lock expiration date cannot be changed when the rate is locked."; // dl - reviewed 01/05/07
		public const string RateLockDateCannotBeChangedDuringRateLock = "The rate lock date cannot be changed when the rate is locked."; // dl - reviewed 01/05/07
		public const string CannotBreakRateLockWithoutRateLock = "Cannot break the rate lock when the rate is not locked."; // dl - reviewed 01/05/07
        public const string CannotSuspendLockWithoutRateLock = "Cannot suspend the rate lock when the rate is not locked.";
        public const string CannotResumeLockWhenRateNotSuspended = "Cannot resume the lock when the rate is not suspended.";
		public const string CannotChangeRateLockPeriodDuringRateLock = "Cannot change the lock period when the rate is locked."; // dl - reviewed 01/05/07
		public const string CannotChangeTermDuringRateLock = "Cannot change the term when the rate is locked."; // dl - reviewed 01/05/07
		public const string CannotChangeDuePeriodDuringRateLock = "Cannot change the due period when the rate is locked."; // dl - reviewed 01/05/07
		public const string CannotChangeMarginDuringRateLock = "Cannot change the margin when the rate is locked."; // dl - reviewed 01/05/07
        public const string CannotChangeNameDuringInvestorRateLock = "Cannot change the investor name when the investor rate is locked.";
        public const string CannotChangeProgramDuringInvestorRateLock = "Cannot change the investor program when the investor rate is locked.";
        public const string CannotChangeLockStatusDuringInvestorRateLock = "Cannot change the investor lock status when the investor rate is locked.";
        public const string CannotChangeLockPeriodDuringInvestorRateLock = "Cannot change the investor lock period when the investor rate is locked.";
        public const string CannotChangeLockDateDuringInvestorRateLock = "Cannot change the investor lock date when the investor rate is locked.";
        public const string CannotChangeLockExpirationDuringInvestorRateLock = "Cannot change the investor lock expiration when the investor rate is locked.";
        public const string CannotChangeLockDeliveryExpirationDuringInvestorRateLock = "Cannot change the investor lock delivery expiration when the investor rate is locked.";
        public const string CannotChangeLoanNumberDuringInvestorRateLock = "Cannot change the investor LoanNumber when the investor rate is locked.";
        public const string CannotChangeProgramIdDuringInvestorRateLock = "Cannot change the investor loan program identifier when the investor rate is locked.";
        public const string CannotChangeLockConfirmationNumberDuringInvestorRateLock = "Cannot change the investor lock confirmation number when the investor rate is locked.";
        public const string CannotChangeRateSheetIdDuringInvestorRateLock = "Cannot change the investor rate sheet Id when the investor rate is locked.";
        public const string CannotChangeRateSheetEffectiveTimeDuringInvestorRateLock = "Cannot change the investor rate sheet effective time when the investor rate is locked.";
        public const string CannotChangeLockFeeDuringInvestorRateLock = "Cannot change the investor lock fee when the investor rate is locked.";
        public const string CannotChangeCommitmentTypeDuringInvestorRateLock = "Cannot change the investor commitment type when the investor rate is locked.";
        public const string CannotChangeRateDuringInvestorRateLock = "Cannot change the investor rate when the investor rate is locked.";
        public const string CannotChangeFeeDuringInvestorRateLock = "Cannot change the investor fee when the investor rate is locked.";
        public const string CannotChangeMarginDuringInvestorRateLock = "Cannot change the investor margin when the investor rate is locked.";
        public const string CannotChangeQualifyRateDuringInvestorRateLock = "Cannot change the investor qualify rate when the investor rate is locked.";
        public const string CannotChangeTeaserRateDuringInvestorRateLock = "Cannot change the investor teaser rate when the investor rate is locked.";
        public const string FailedCreatingDefaultRolePermissions = "Failed to create default role permissions."; // dl - reviewed 01/18/07
        public const string FailedCreatingDefaultNonBoolRolePermissions = "Failed to create default non-bool role permissions.";
		public const string InsufficientPermissionToEditPrintGroups = "Access denied. You lack permission to edit print groups."; // dl - reviewed 01/18/07
		public const string AsyncReportTakingTooLongToProcess = "The credit report request is taking a long time to process. Please check your file again later."; // dl - reviewed 01/18/07
        public const string OriginatorCompensationMinAmountRequired = "Minimum originator compensation amount required (value needs to be greater than $0)";
        public const string OriginatorCompensationMaxAmountRequired = "Maximum originator compensation amount required (value needs to be greater than $0)";
        public const string OriginatorCompensationMaxAmountLessThanMinAmount = "Maximum originator compensation amount needs to be higher than minimum originator compensation amount";
        public const string LoginNameAlreadyInUse = "Login name already exists (may be in use by another company).  Please try another login name."; // la - reviewed 09/22/08
		public const string SpecifyLoginName = "You must specify a login name."; // dl - reviewed 01/30/08
		public const string PwCannotContainLoginOrName = "Password may not contain login name, first name or last name."; // dl - reviewed 03/01/07
		public const string PasswordMustBeAlphaNumeric = "Password must consist of at least 1 number and 1 letter."; // dl - reviewed 03/01/07
		public const string PasswordTooShort = "Password must be at least 6 characters in length."; // dl - reviewed 03/01/07
		public const string PasswordsShouldNotBeRecycled = "We do not recommend recycling old passwords. Please specify a new password."; // dl - reviewed 03/01/07
		public const string PasswordContainsConsecIdenticalChars = "Password may not contain 3 consecutive identical characters (ex: AAAsample1)."; // dl - reviewed 9/12/08
		public const string PasswordContainsConsecCharsInAlphaOrNumericOrder = "Password may not contain 3 letters or numbers in order (ex: ABC_sample1)."; // dl - reviewed 9/12/08
        public const string PasswordHasUnverifiedCharacters = "Please remove any < > \" ' % ; ) ( & + - = from password.";
		public const string SpecifyPassword = "You must specify a password."; // dl - reviewed 03/01/07
		public const string SpecifyValidPasswordExpirationDate = "Specify a valid password expiration date."; // dl - reviewed 03/01/07
		public const string PasswordsDontMatch = "Passwords do not match."; // dl - reviewed 01/30/08
		public const string SpecifyUserType = "No user type specified.  Please choose one."; // dl - reviewed 03/01/07
		public const string FailedToRetrieveBroker = "Failed to retrieve broker.";
		public const string FailedToLoadPage = "Failed to load page.";
		public const string FailedToLoadViewState = "Failed to load viewstate.";
		public const string FailedToRenderPage = "Failed to render page.";
		public const string FailedToRemoveItem = "Failed to remove item.";
		public const string FailedToDeleteRateLockPeriod = "Failed to delete rate lock period.";
		public const string FailedToSaveRateLockPeriods = "Failed to save rate lock periods.";
		public const string FailedToSave = "Failed to save.";
		public const string FailedToAddNewItem = "Failed to add new item.";
        public const string LP_DisabledBySAEMessage = "This loan program is currently being updated by the technical staff. Please call or submit the loan for manual underwriting and pricing."; // dl - reviewed 06/21/07
        public const string StipulationWarning_PublicRecordAlter = "PUBLIC RECORD DATA HAS BEEN ALTERED."; // dl - reviewed 01/30/08
        public const string FNMA_InvalidDUAuthentication = "The Fannie Mae DO / DU login information is incorrect.  Please confirm your Fannie Mae DO / DU User ID and Password.  To verify your account, contact Fannie Mae Customer Care at (877) 722-6757."; // dl - reviewed 04/10/08

        public const string PmlBrokerNotFound = "Failed to retrieve pml broker.";
        public const string PmlCompanyMissingParameters = "PmlCompany is missing some required information.";
        public const string InvalidCreditCardType = "Invalid credit card type.";
        public const string LicenseExpired = "The license being used has expired";
        public const string LicenseNotFound = "Unable to find license information.";
        public const string LicensePaymentNotFound = "Unable to find payment information for this license.";
        public const string InvalidTaskSubmitted = "The task being submitted is invalid.";
        public const string GenericFailureToExportDataToCSV = "A problem occurred when exporting the requested data.";
        public const string InsufficientPermissionToAdministratePMLUsers = "Insufficient permission to administrate PML users.";
        public const string InsufficientPermissionToAdministrateExternalUsers = "Insufficient permission to administrate PML originating companies.";
        public const string FailedToSaveBroker = "Failed to save broker.";
        public const string FailedToSaveBrokerDefaults = "Created broker but failed to set default broker settings.";
        public const string FailedToSaveTask = "Failed to save task.";
        public const string FailedToJoinTask = "Failed to join task.";
        public const string UnableToRetrieveTask = "Unable to retrieve specified task.";
        public const string ProblemWithReportRun = "Problem occurred when report was being run.";
        public const string InvalidLoanProgramFolder = "Unable to find loan program folder.";
		public const string SchemaFileMissing = "Schema file is missing.";
        public const string DataFileMissing = "Data file is missing.";
        public const string FailedToFindFile = "Cannot find file.";
        public const string QueueNotSelected = "The queue has not been selected!";
		public const string ExpirationDateOutOfRange = "Expiration date is not in range.";
        public const string FailedToLoad = "Failed to load required data.";
        public const string FailedToClearMessages = "Unable to clear messages.";
        public const string FailedToRemoveMessage = "Unable to remove message.";
        public const string FailedToReceiveMessage = "Unable to receive message.";
        public const string FailedToLoadUser = "Failed to load specified user.";
        public const string FailedToLoadPolicies = "Failed to load the policies.";
        public const string FailedToCreateTask = "Failed to create new task";
        public const string FailedToReplicateTaskNotifs = "Failed to replicate task notifications";
        public const string FailedToImportData = "Failed to import data.";
        public const string InvalidAuthorization = "Invalid authorization.";
        public const string InvalidAuthentication = "Invalid authentication.";
        public const string FailedToDeleteFolder = "Unable to delete folder.";
        public const string FailedToDeleteFolderNotEmpty = "Folder is not empty and can't be deleted.";
        public const string RecordDoesntExist = "This record no longer exists. It may have been deleted by another user.";
        public const string InvalidLicenseAssignment = "Invalid license assignment detected.";
        public const string FailedToFindLicensePlanInfo = "License Plan info not found";
        public const string FailedToLoadBrokerAccessInfo = "Unable to load broker access control info.";
        public const string TaskOperationFailed = "Task operation failed";
        public const string PasswordExpired = "Your password has expired";
        public const string InvalidLoanID = "The loan id is invalid.";
        public const string CannotFindCustomReportQuery = "Unable to find query.";  // OPM 41733
        public const string InputCausedHttpRequestValidationException = "Remove any invalid characters (e.g. < >) in textboxes and try again";
        public const string UnableToLoadReport = "Unable to load your report. Reports expire after 30 minutes. Please re-run your report.";
        public const string InvalidBrokerSuiteType = "Please select a suite type for the broker.";

        public const string BorrowerSSNDoesntMatchOnLoanFiles = "Borrower SSN does not match on source loan file and destination loan file.";
        public const string NumberOfCoborrowersDoesntMatchOnLoanFiles = "Number of coborrowers do not match between source loan file and destination loan file.";
        public const string FeatureNotAvailableForFirstLienPosition = "This feature is not available for populating to a first lien position, please check the lien position of the other loan.";
        public const string FeatureNotAvailableForSecondLienPosition = "This feature is not available for populating from a second lien position to another loan, please check lien position of this loan.";
        public const string PrincipalCurtailmentPartiesCannotBeModified = "Paid From and Paid To values cannot be modified for principal curtailment adjustments.";
        public const string LinkedServicingPaymentAmountsAreReadonly = "Amounts for servicing payments linked to an adjustment can be changed by updating the adjustment.";
        public const string CannotDeleteVerbalAuthorizationCustomPdf = "This form is selected as the verbal credit authorization form and cannot be deleted.";
        public const string CustomWordFormsMustBeDocX = "Custom word forms must be in .docx format.";
        public const string LqbMobileAppNotEnabled = "Your financial institution does not currently allow for use of this mobile application. Please contact your administrator if you have any questions.";
        public const string SmallMigrationFrameworkPermissionErrorMsg = "This migration cannot be run automatically. A user with the appropriate permissions will need to review and update the required data before this migration can be run.";

        public static string SelectAtLeastOnePortalModeForOriginatorPortalPipelineSetting(string reportName)
        {
            return $"Please select at least one portal mode for the availability of pipeline report '{reportName}'.";
        }

        #region Login Failure Messages
        public const string AccountDisabled = "Your account is disabled.";
		public const string AccountLocked = "Your account is locked.";
		public const string InvalidLoginPassword = "Please ensure the login and password are correct.";
		public const string PleaseContactYourAdministrator = "Please contact your account administrator for more information.";
        public const string GenericLoginFailed = "Login failed";
        public const string NeedsToWaitForLogin = "Please wait before attempting to log in again.";
        
        public static class ActiveDirectoryAuthFailure
        {
            public const string ADNotEnabled = "Active Directory authentication is not enabled. Please select Normal login type or contact your account administrator to enable AD authentication.";
            public const string EmptyResponse = "The Active Directory authentication webservice failed to return a response.";
            public const string InvalidCredentials = "Please verify your login, password and customer code.";
            public const string MustLoginAsADUser = "Please select Active Directory login type.";
        }
        #endregion

        #region ARM index message
        public const string FailedToCheckARMIndexUsage = "Failed to check if ARM index is in use.";
        public const string FailedToLoadARMIndexDescriptors = "Failed to load arm index descriptors.";
        public const string FailedToSaveARMIndexDescriptors = "Failed to save arm index descriptors.";
        public const string FailedToPushARMIndexDescriptors = "Failed to push new arm index descriptor.";
        #endregion

        #region Blocked Rate Lock Submission Messages
        public const string BlockedRateLockSubmission_NormalUserExpiredMessage = "Pricing for this program is not accessible at this time, and the rates shown may have expired. Please contact your AE to price.";
        public const string BlockedRateLockSubmission_LockDeskExpiredMessage = "System may not have the latest rate info at this time."; // dl - reviewed 01/17/08
        public const string BlockedRateLockSubmission_NormalUserCutOffMessageFormat = "We are not accepting lock requests for this program at this time.  The cut-off time for this program is {0}."; // dl - reviewed 01/17/08
        public const string BlockedRateLockSubmission_LockDeskCutOffMessageFormat = "The rates shown for this program have expired.  The investor cut-off time is {0}."; // dl - reviewed 01/17/08
        public const string BlockedRateLockSubmission_OutsideNormalHourFormat = "We are not accepting lock requests for this program at this time.  Our regular lock desk hours are from {0}."; // dl - reviewed 01/17/08
        public const string BlockedRateLockSubmission_OutsideClosureDayHourFormat = "We are not accepting lock requests for this program at this time.  Our lock desk is either closed for the entire day or has short business hours today and we are currently closed."; // dl - reviewed 01/17/08
        public const string BlockedRateLockSubmission_OutsideNormalHourAndPassInvestorCutoffFormat = "We are not accepting lock requests for this program at this time. Our regular lock desk hours are from {0}. Also, the cut-off time for this program is {1}."; // dl - reviewed 03/13/08
		#endregion

        #region DataTrac error messages
        public const string DataTrac_GenericFieldErrorMessage = "Unable to load DataTrac loan field data - please contact us if this happens again.";
        public const string DataTrac_GenericExportErrorMessage = "Unable to export to DataTrac - please contact us if this happens again.";
        #endregion

        #region Encompass error messages
        public const string Encompass_BKorFCTypeorStatusUnexpected = "The bankruptcy or foreclosure data included in the loan file could not be imported. Please verify the type and status of the BK and/or FC on file and contact us if this happens again.";
        public const string Encompass_BorrowerIDUnexpected = "The credit score data could not be imported. The BorrowerID was unexpected.";
        public const string Encompass_GenericExportErrorMessage = "Unable to export to Encompass. Please contact us if this happens again.";
        public const string Encompass_NotSignedUp = "The integration to Encompass is disabled. Please contact us to sign up.";
        public const string Encompass_MissingSSN = "A social security number must be provided for each borrower.  Please enter an SSN for each borrower and try again.";
        #endregion

        #region Total Scorecard Warning Message.
        public const string TotalScorecardWarning_MissingSsn = "SSN is required for each borrower.";
        public const string TotalScorecardWarning_MissingCitizenshipStatus = "Citizenship status is required for each borrower.";
        public const string TotalScorecardWarning_MissingIntentToOccupy = "Intent to occupy must be declared for all applicants.";
        public const string TotalScorecardWarning_IncomeGreaterThanZero = "Income must be greater than 0.";
        public const string TotalScorecardWarning_PurchasePrice = "Purchase price must be greater than 0 for purchase or construction loans.";
        public const string TotalScorecardWarning_AppraisedValue = "Appraised value must be greater than 0 for refinance loans.";
        public const string TotalScorecardWarning_UpfrontMip = "Upfront MIP cannot be negative.";
        public const string TotalScorecardWarning_ExceedBorrowerCount = "TOTAL submission are limited to a maximum of 5 borrowers.";
        public const string TotalScorecardWarning_NoBorrowerCount = "TOTAL submission requires at least 1 valid borrower.";
        public const string TotalScorecardWarning_MissingJobTitle = "Employment information is required for borrowers with employment income.";
        public const string TotalScorecardWarning_LoanAmountNegative = "Loan amount must be greater than 0.";
        public const string TotalScorecardWarning_FhaStreamline = "Per Mortgagee Letter 2009-32, TOTAL should not be used for streamline refinance transactions. If you wish to use TOTAL, please resubmit with a Loan Purpose of Refi Rate/Term.";
        public const string TotalScorecardWarning_VaStreamline = "VA IRRRL are not supported.";
        public const string TotalScorecardWarning_InvalidEmploymentDate = "Employment dates are not valid";
        public const string AudWarning_PropertyAddessEmpty = "Subject property address cannot be empty.";
        public const string AudWarning_PropertyStateInvalid = "Subject property state is invalid."; // OPM 48194
        public const string TotalScorecardWarning_NonPurchasingPrimaryBorrower = "Primary borrower must be individual/co-signer type."; // OPM 184017
        public const string TotalScorecardWarning_NonPurchasingOtherAppBorrower = "Borrower must be individual/co-signer type, please swap the borrower and co-borrower."; // OPM 184017
        public const string H4HWarning_MustOwnResidence = "Occupant borrowers must own their present residence."; // OPM 53539
        #endregion

        #region PML Import Warning Message
        public const string PmlImportWarning_NoBorrowerCount = "File contains no valid borrowers.";
        public const string PmlImportWarning_MissingSsn = "SSN is missing.";
        public const string PmlImportWarning_MissingCitizenshipStatus = "Citizenship status is missing.";
        public const string PmlImportWarning_MissingIntentToOccupy = "Intent to occupy has not been filled out for this borrower.";
        public const string PmlImportWarning_IncomeGreaterThanZero = "Income information appears to be missing for all borrowers.";
        public const string PmlImportWarning_PurchasePrice = "Loan is a purchase but the purchase price is $0.";
        public const string PmlImportWarning_AppraisedValue = "Loan is a refinance, but the appraised value is $0.";
        public const string PmlImportWarning_UpfrontMip = "Upfront MIP should not be negative.";
        public const string PmlImportWarning_MissingJobTitle = "Borrower has employment income, but employer information is not filled out.";
        public const string PmlImportWarning_LoanAmountNegative = "Loan amount is $0.";

        #endregion
        public const string H4HImportWarning_LPurposeT = "Only rate and term refinance transaction are allowed for this program.";
        public const string H4HImportWarning_sOccT = "Only primary residences are allowed for this program.";
        public const string H4HImportWarning_sFinMethT = "Only fixed rate loans may be offered under this program.";
        public const string H4HImportWarning_sTerm = "Only 30 year terms may be offered under this program.";
        public const string H4HImportWarning_sSpLien = "Mortgages and liens on the subject property REO record must match Amount Existing Liens on page 1 of URLA.";
        public const string H4HImportWarning_sReCollectionCount = "Refinance loans require exactly one REO property be selected as the subject property.";

        public const string BorrowerInfoMismatchBetweenLoanAndCredit = "Borrower information on credit report does not match with loan file. Credit order may have been processed and charged - please log in directly to your Credit Agency portal before submitting another order.";
        public static string H4HBorrowerAddressMismatchWarning(string address)
        {
            return string.Format("The address of the subject property is <b>{0}</b>. This does not match the present address of the following borrowers:", address);
        }
        
        public static string WaitTimeDueToMaxFailedLogins(double minutesToWait)
        {
            string MessagePartOne = "Login failed.  Due to the number of failed login attempts, you must now wait ";
            string MessagePartTwo = " minute(s) before attempting to log in again.";

            return MessagePartOne + minutesToWait + MessagePartTwo;
            //if(string.IsNullOrEmpty(customerCode))
            //    return MessagePartOne + PrincipalFactory.GetMinutesToWait(userName, "B") + MessagePartTwo;
            //else
            //    return MessagePartOne + PrincipalFactory.GetMinutesToWait(userName, "P", customerCode) + MessagePartTwo;
        }

        public static string FailedToLoadTemplateConditions(string sTemplateName) 
		{
			return String.Format("Failed to load the conditions belonging to template {0}.", sTemplateName);
		}

		public static string GetFannieMaeWarnings(ArrayList skippedSections)			// LA - reviewed 10/2/08
		{
			if ( skippedSections.Count == 0 ) return string.Empty; 
			string warning = "Unsupported version number"+ (skippedSections.Count != 1 ? "s" : string.Empty )+" found. The following sections were not imported: ";  
			for(int x = 0; x < skippedSections.Count; x++ )  
			{
				if ( x != 0 ) 
				{
					if ( x+1 == skippedSections.Count  ) warning += " and "; 
					else warning += ", "; 
				}
				warning+= skippedSections[x]; 
			}
			warning += ".";
			return warning; 
		}

        public const string SecondLienNotSupportedPML = "Running pricing directly on a second lien is not supported. Please upload first lien files only.";

        public const string CreditReportMissingXml = "Your credit agency returned an incomplete response -- some credit data may be missing.  Please contact your credit reporting agency for assistance."; //la - reviewed 11/19/09
        public const string CreditReportViewableDataMissing = "The credit report is not available for viewing because the credit reporting agency (CRA) did not provide any viewable credit data.  Please contact your CRA about sending over the viewable credit data."; //la - reviewed 11/19/09
        public const string CreditReportNotOrderedMessage = "There is no credit report on this application.";  //la - reviewed 11/19/09
        public const string LqiCreditReportNotOrderedMessage = "There is no LQI credit report on this application.";

        public static string LoginNameAlreadyInUseFunction(string name)
        {
            return "Review the \"Credentials\" tab. \"" + name + "\" is unavailable.  Please try another login name.";
		}

        public static string LoginNameAlreadyInUseFunctionAlternate(string name)
        {
            return "\"" + name + "\" is unavailable.  Please try another login name.";
		}

        public const string PdfPrintAccessDenied = "Access Denied";
        public static string CantUploadInvalidPDF (string name)
        {
            return "Unable to upload " + name + " because it is not a valid pdf.";
        }

        public static string CantUploadProtectedPDF(string name)
        {
            return name + " cannot be used as a custom form due to modification restrictions.";
        }

        public static string CantUploadUnexpectedReason(string name)
        {
            return "Could not read " +  name;
        }

        public const string LendingStaffNotesAccessDenied = "Access denied. You do not have permission to access lending staff notes.";

        #region Consumer Portal related error messages
        public const string DuplicateEmailInLoanFileWithOpenESign = "Duplicate borrower emails found. A loan file with open e-Sign requests cannot have two borrowers with the same email.";
        public const string MissingNecessaryInformationToCreateRequest = "Enter an e-mail address and valid SSN for borrowers before attempting to send a request";
        public const string DuplicateSsnInLoanFile = "Borrower SSNs must be unique before submitting a request";
        #endregion

        public const string DeleteFailForEmployeeGroupHavingActiveMembers = "Employee group cannot be deleted.  There are member employees.";
        public const string GroupExistsInConfig = "Cannot delete the group. The group is being used in one or more Conditions, Constraints or Custom Variables";
        public const string DeleteFailForBranchGroupHavingActiveMembers = "Branch group cannot be deleted.  There are member branches.";
        public const string DeleteFailForPmlBrokerGroupHavingActiveMembers = "Originating company group cannot be deleted.  There are member originating companies.";
        public const string OlderWorkflowConfigNotFound = "An older version of the workflow configuration was not found.";
        
        public static string ErrorDuringReportIdRetrival(string role)
        {
            return string.Format("Error occurred when trying to get the default report id for role = {0}", role);
        }

        public static string InvalidValueForField(object value, string fieldName)
        {
            return string.Format("{0} is not a valid value for the field: {1}", value, fieldName);
        }

        public const string InvalidEmailAddresses = "E-mails can only be sent to valid e-mail addresses.  Please correct the address(es) listed in the To and CC fields and try again.";
        public static string InvalidMBSDescriptionId(Guid id)
        {
            return "Unable to find a description with the id: " + id;
        }
        public static string InvalidMBSDescriptionName(string name)
        {
            return "Unable to find a description with the name: " + name;
        }

        public const string DocMagicInvalidLogin = "An error occurred while attempting to login to DocMagic.  Please verify your login credentials or ask your system administrator for assistance and try again.";
        public const string DocMagicGenericError = "An unexpected issue has occurred while processing this request.  Please try again.";

        public const string SocketExceptionWhenUsingFileDB3_ConfigurationFix = @"Consider setting c:\FileDB3\FileDB3.exe.config > USE_CONNECTIONPOOL to T (and restart FileDB3 service in services.msc)";

        public const string InvalidLegalEntityIdentifier = "A Legal Entity Identifier must be exactly 20 characters and must consist of letters and numbers only.";

        public const string InvalidCertificatePassword = "The password is incorrect for the specified certificate.";

        public const string RetailPortalNotEnabled = "This portal is not currently enabled for your company. " +
            "If you believe you are receiving this message in error, please contact an administrator.";
    }
}
