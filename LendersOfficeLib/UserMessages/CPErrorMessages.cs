﻿

namespace ConsumerPortal.Common
{

    public class ErrorMessages
    {
        #region Login Failure Messages
        public const string AccountLocked                   = "Your account is locked.";
        public const string InvalidLoginPassword            = "Please ensure the login and password are correct.";
        public const string PleaseContactYourAdministrator  = @"Please click the ""Forgot password?"" link to reset your password and unlock your account.";
        public const string SessionExpired                  = "Your session has expired.";
        public const string TemporaryPasswordExpired        = "Your temporary password has expired.";
        #endregion

        public const string EmailAddressNotFound            = "Email address not found.";

        public const string NoLoansAssociatedWithAccount    = "There is no loan file associated with this account.";

        //Password Messages
        public static readonly string INVALID_CURRENT_PASSWORD      = "The entered password is invalid.";
        public static readonly string NEW_PASSWORD_RULES            = "The new password must be at least 6 characters long and contain 1 letter and 1 number.";
        public static readonly string CURRENT_PASSWORD_RULES        = "The current password must be at least 6 characters long and contain 1 letter and 1 number.";
        public static readonly string PASSWORD_CONFIRMATION_MISSMATCH = "Passwords do not match.  Please try again.";
        public static readonly string CANNOT_REPEAT_PASSWORD        = "The new password must be different from the old password.";
        public static readonly string UNABLE_UPDATE_PASSWORD        = "We were unable to update your password.";
        public static readonly string INVALID_PASSWORD_RESET_CODE   = "Invalid password reset code.";

        //Popup blocker
        public static readonly string PopupBlocker = "Unable to open new window.  This may be caused by a pop-up blocker program on your computer.";

        //SSN Validation
        public static readonly string SsnNotFound = "Social security number not found.";
        public static readonly string SsnTimeoutAttempt = "Due to multiple failed attempts, please wait {0} before attempting to validate your social security number again."; //{0} can be: 'a few minutes', 'a few hours', 'until tomorrow'
        public static readonly string SsnTimeoutMinutes = "a few minutes";
        public static readonly string SsnTimeoutHours = "a few hours";
        public static readonly string SsnTimeoutTomorrow = "until tomorrow";
        public static readonly string SsnInvalidFormat = "Invalid social security format. Please try again.";
    }

    public class UserMessages
    {
        public const string PasswordSuccessfullyUpdated = "Password updated. Please login with your new credentials.";
    }
}