using System;

namespace LendersOffice.Common
{
    public class JsMessages 
    {
        // dd - WARNING: All javascript message need to escape single quote properly.
        // INVALID - public const string Foo = "My name is O'Neil";
        // VALID - public const string Foo = @"My name is O\'Neil";

        public const string PMLTimeoutCheckFilter = "Pricing results have timed out. Please consider updating the Results Filter and re-running pricing."; // ejm opm 230961
        public const string LpeResultsTimeout = "The pricing system is currently being updated.  Please try again in 1 minute."; // dl - reviewed 10/28/05
        public const string LpeUnableToSubmitLoanProgram = "Unable to submit because second loan exists.  Please contact your Account Executive to remove the second loan before continuing."; // dl - reviewed 02/17/06
        public const string LpeUnableApplyRateOption = "Unable to apply for the selected rate option."; // dl - reviewed 10/11/05
        public const string LpeUnableSubmitManually = "Unable to submit manually."; // dl - reviewed 10/28/05
        public const string LpeSelectAtLeastOneProgram = "Need to select at least one loan program to search."; // dl - reviewed 10/28/05
        public const string LpeSelectMaxPrograms = "Please select no more than 3 loan programs."; // dl - reviewed 10/11/05
        public const string LpeSelectAtLeastOneRateToCompare = "Need to select at least one rate option to compare."; // dl - reviewed 10/28/05

        public const string NoExportPermission = "You do not have permission to export loans.  Please contact your administrator."; // dl - reviewed 10/11/05
        public const string NoLoanSelected = "No loan is selected."; // dl - reviewed 10/28/05
        public const string TooManyLoansSelected = "You cannot assign or change status for more than 100 loans.";
        public const string ConfirmLoanDelete = "Are you sure you want to delete these loans?"; // dl - reviewed 10/28/05
        public const string FailedToCompleteOperation = "Failed to complete operation.  Please try again."; // dl - reviewed 10/28/05
        public const string CannotInsertTaskToMoreThanOneLoan = "Cannot insert task.  Multiple loan files have been selected.  A new task can be inserted into only one loan at a time."; // dl - reviewed 10/11/05
        public const string CannotDuplicateMoreThanOneLoan = "Only one loan can be duplicated at a time."; // dl - reviewed 10/28/05
        public const string UnableDuplicateLoanFile = "Unable to duplicate loan file."; // dl - reviewed 10/28/05
        public const string CannotOpenSelectedFolder = "Cannot open selected folder."; // dl - reviewed 10/28/05
        public const string InvalidFolder = "Invalid folder."; // dl - reviewed 10/28/05
        public const string PointExport_ExportCompleted = "Export completed."; // dl - reviewed 10/28/05
        public const string PricingGroupEdit_ConfirmDisable = "Are you sure you want to disable this price group?"; // dl - reviewed 10/28/05
        public const string PricingGroupEdit_ConfirmEnable = "Are you sure you want to enable this price group?"; // dl - reviewed 10/28/05
        public const string PricingGroupEdit_UnableToSave = "Unable to save price group.  Please try again."; // dl - reviewed 10/28/05
        
        public const string RateLock_2ndLienNotSupport = "Running pricing engine on second lien is not supported."; // dl - reviewed 10/28/05
        public const string RateLock_ConfirmLockRate = "Locking approved rate will change loan status to APPROVED.  Would you like to continue?"; // dl - reviewed 10/11/05
        public const string RateLock_ConfirmBreakRateLock = "Are you sure you want to break the existing rate lock?"; // dl - reviewed 10/28/05
        public const string RateLock_ConfirmSuspendLock = "Are you sure you want to suspend the existing rate lock?";
        public const string LoanInfo_DuplicateLoanNumber = "Loan number in use.  Please specify a new loan number."; // dl - reviewed 10/28/05
        public const string LoanInfo_DuplicateLoanReferenceNumber = "Loan Reference Number in use."; // dl - reviewed 10/28/05
        public const string LoanInfo_DuplicateUniversalLoanIdentifier = "Universal Loan Identifier in use.";

        public const string LoanInfo_FailToCheckLoanNumber = "Failed to check if loan number already exists.  Please try again."; // dl - reviewed 10/28/05
        public const string EnterCredit_ConfirmDeleteExistingLiabilities = "There are existing liabilities.  Do you want to delete these and import new liabilities from the credit report?"; // dl - reviewed 10/21/05
        public const string EnterCredit_MissingInformation = "If you are ordering a new report, please select at least one bureau.  Or, if you are re-issuing an existing report, please specify the Report ID."; // dl - reviewed 02/17/06

        public const string PmlLoanSummaryView_NoRecipient = "Please specify a recipient.";		// dl - reviewed 12/08/05
        public const string PmlLoanSummaryView_NotValidEmail = "Invalid email address format."; // dl - reviewed 12/08/05
        public const string PmlLoanSummaryView_SendComplete = "PML Summary sent.";				// dl - reviewed 12/08/05
        public const string PmlLoanSummaryView_ApprovalSendComplete = "Approval Certificate sent.";
        public const string PmlLoanSummaryView_RatelockSendComplete = "Rate Lock Confirmation sent.";
        public const string PmlLoanSummaryView_SuspenseNoticeSendComplete = "Suspense Notice sent.";


        public const string LoanOpenedRequired = "Loan opened date is required.";				// dl - reviewed 12/08/05

        public const string LeadConvertSuccessful = "Loan conversion successful.";				// dl - reviewed 01/26/06
        public const string PickLoanTemplate = "Please choose a loan template.";				// dl - reviewed 01/26/06

        public const string PrintGroup_SaveSuccessful = "Save successful.";						// dl - reviewed 01/27/06
        public const string PrintGroup_DeleteGroupFailed = "Delete failed.";					// dl - reviewed 01/27/06
        public const string PrintGroup_ConfirmDeleteGroup = "Do you want to delete this group?";// dl - reviewed 01/27/06
		public const string PrintGroup_Empty = "Print Group is empty.  Please add at least one form.";
		public const string PrintGroup_BlankName = "Group name cannot be blank.";

        public const string LPE_CashoutCannotBeNegative = "Cashout amount cannot be negative."; // dl - reviewed 02/15/06

        public const string LPE_CannotSubmitWithoutCredit = "You do not have permission to submit loans without a credit report."; // dl - reviewed 02/17/06

        public const string LPE_CannotSubmitInQP2Mode = "In order to submit, please first create a lead/loan file from this scenario."; // sk 08/05/2014 opm 185732
        public const string LPE_CannotSubmitInQP2ModeInPML = "In order to submit, please first create a loan file from this scenario."; // sk 08/26/2014 opm 189756
        public const string LPE_CannotSubmitInQP2ModeInPMLAndCantCreateLoan = "In order to submit, please import a file into your pipeline."; // sk 08/26/2014 opm 189756

        public const string RateLock_RateExtendReasonRequired = "Rate lock extension reason is required."; // dl - reviewed 04/14/06
        public const string RateLock_RateExtendExpireRequired = "Number of additional days is required."; // dl - reviewed 04/14/06

        public const string RateLock_ConfirmClearRequestedRate = "Are you sure you want to clear this lock request?"; // fs - reviewed 06/13/08 opm 19109

		public const string CannotDisableLastActivePriceGroup = "Price group cannot be disabled - you must maintain at least one active price group."; // dl - reviewed 12/07/06

        public const string NumFinancedPropertiesMustBePositive = "The number of financed properties cannot be less than one.";
		public const string UserAlreadyHasRole = "Setting does not apply - the user already has this role."; // dl - reviewed 12/07/06
		public const string OnlyDisplayingUsersFromSameBranch = "Note: Only users who are members of the same branch as this user are displayed.  To see all users, enable the \"Allow assigning loans to agents of any branch\" permission."; // LA-Reviewed 12/2/08 Case 19827; dl - reviewed 12/07/06
        public const string RelationshipSettingsHaveBeenCleared = "The relationship settings that do not apply have been cleared because this user does not have the \"Allow assigning loans to agents of any branch\" permission."; // LA-Reviewed 12/2/08 Case 19827; dl - reviewed 12/07/06
		public const string SpecifyFirstName = "You must specify a first name."; // dl - reviewed 12/07/06
		public const string SpecifyLastName = "You must specify a last name."; // dl - reviewed 12/07/06
		public const string SpecifyValidEmail = "You must specify a valid email address."; // dl - reviewed 12/07/06
        public const string SpecifyValidSupportEmail = "You must specify a valid support email address."; // ML - OPM 220848
        public const string SpecifyUniqueSupportEmail = "The support email address you specified is in use. Please specify a different email address. Your changes were not saved."; // ML - OPM 220848
		public const string SpecifyValidContactPhoneNumber = "You must specify a valid contact phone number."; // dl - reviewed 12/07/06
        public const string SpecifyValidCellPhoneNumber = "SMS authentication is enabled. You must specify a valid cell phone number."; // dl - reviewed 12/07/06
		public const string SpecifyValidPwExpirationDate = "You must specify a valid password expiration date."; // dl - reviewed 12/07/06
		public const string PasswordsDontMatch = "Passwords do not match.  Please make sure you type exactly the same password to confirm."; // dl - reviewed 12/07/06
		public const string SpecifyLoginName = "You must specify a login name."; // dl - reviewed 12/07/06
		public const string EmployeeCanLoginButNoLoginSpecified = "Employee is allowed to login, but no login is specified."; // dl - reviewed 12/07/06
		public const string SpecifyRole = "No role specified.  Please choose at least one role for this employee."; // dl - reviewed 12/07/06
		public const string CannotPopulateDefaultRolePermissions = "Unable to populate default role permissions."; // dl - reviewed 03/01/07
		public const string PermissionSetsDontMatch = "Unable to set permissions - default permission set does not match employee permission set."; // dl - reviewed 03/01/07

		public const string CreditAgencyAlreadyInList = "Selected credit agency is already in the list."; // dl - reviewed 12/07/06

		public const string CannotDeleteMaster_OtherProgramsInFolder = "Cannot delete the master product when there are other programs in the folder"; // dl - reviewed 01/05/07
		public const string CannotDeleteMaster_HasSubfolders = "Cannot delete the master product when there are subfolders"; // dl - reviewed 01/05/07
		public const string CannotDuplicateMaster = "Cannot duplicate the master product"; // dl - reviewed 01/05/07
		public const string CannotDuplicateMultiple = "Only 1 product can be duplicated at a time"; // dl - reviewed 01/05/07
		public const string FolderAlreadyContainsMaster = "Folder already contains a Master"; // dl - reviewed 01/05/07
		
		public const string WarnUserRebateCannotBeChangedDuringRateLock = "The rebate percentage value cannot be changed when there is a rate lock."; // dl - reviewed 01/05/07

		public static string ClearPreviousRoleSetting(string role)
		{ 
			return string.Format("Note - The previous {0} relationship setting has been cleared.", role); // dl - reviewed 12/07/06
		}

        public static string LpeClose2ndLoan(string sLNm) 
        {
            return string.Format("Please close 2nd loan window (loan #: {0}) before continuing.", sLNm); // dl - reviewed 10/28/05
        }

		public const string PipelineRefreshNeededForDelete = "Cannot delete in current view.  The listing below may not reflect the latest changes.  \nClick the \"Refresh\" button to update and then retry the delete operation."; // dl - reviewed 08/02/07

		public const string RateLock_2ndLienNotSupportWithLinkedLoan = "Running PriceMyLoan on second lien with a linked first lien is not supported.  Go to first loan and then re-run PriceMyLoan."; // dl - reviewed 08/02/07

        public const string SelectAtLeastOneOptionFnmaDownload = "Need to choose at least one section to update."; // dl - reviewed 10/17/07
        public const string WarnUserBeforeClearDuCaseId = "Clearing case file ID will result in new transaction when importing or submitting to FannieMae.  Do you want to continue?"; // dl - reviewed 12/11/07

		public const string InvalidIpFormat = "IP address must follow this format: X.X.X.X where X is a number less than 256 (e.g. 127.0.0.1).";  // LA - reviewed 10/20/08 - OPM 24860
		public const string DuplicateIp = "IP already exists in list.";																			// LA - reviewed 10/20/08 - OPM 24860
		public const string MaxIpCount = "IP Address Restriction list is full. To add a new IP address, remove an address from the list first.";  // LA - reviewed 10/20/08 - OPM 24860; 12 is the max if the user maxes out each IP address to 15 characters.  It's possible to squeeze in a couple more IPs in the list.
		public const string BreakRateLock = "Break current rate lock.";

        public const string SpecifyDOPassword = "You must specify a DO password.";
        public const string SpecifyDUPassword = "You must specify a DU password.";

		public const string AccessDenied = "Access Denied."; 
		public const string GenericError = "There was an error with your request.";

        public const string LeadHasArchives = "There are closing cost archives on this file.  Converting to a loan with a template will cause the archives to be lost. "
            + "You may keep the closing cost archives by converting without a template. Please contact your administrators if you are unsure of what to do.\n\n"
            + "Press OK to convert this lead to a loan."; // je - 01/04/16

        public const string LeadHasArchives_ForceTemplate = "There are closing cost archives on this file.  Converting to a loan will cause the archives to be lost. "
            + "Please contact your administrators if you are unsure of what to do.\n\n"
            + "Press OK to convert this lead to a loan."; // je - 01/04/16

        public static class PmlUserEditor
        {
            public const string BPRelationshipCleared = "Note - The previous Processor (External) relationship was cleared.";
        }
	}
}
