﻿// <copyright file="ISleeper.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: Scott K
// Date: 11/22/2016
// </summary>
namespace LendersOffice.Sleeping
{
    using System;

    /// <summary>
    /// Instances derived from this should cause the thread to sleep on sleep.
    /// </summary>
    public interface ISleeper
    {
        /// <summary>
        /// Sleeps the specified milliseconds.
        /// </summary>
        /// <param name="milliseconds">The number of milliseconds to sleep.</param>
        void Sleep(int milliseconds);

        /// <summary>
        /// Sleeps the specified duration.
        /// </summary>
        /// <param name="timeout">The duration to sleep.</param>
        void Sleep(TimeSpan timeout);
    }
}
