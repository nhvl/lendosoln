﻿// <copyright file="SystemSleeper.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: Scott K
// Date: 11/22/2016
// </summary>
namespace LendersOffice.Sleeping
{
    using System;
    using System.Threading;

    /// <summary>
    /// When this sleeps, it just uses Thread.Sleep. <para/>
    /// Slight shift from Thread.Sleep (static method) to Instance.Sleep (singleton instance . instance method).
    /// </summary>
    internal class SystemSleeper : ISleeper
    {
        /// <summary>
        /// Initializes static members of the <see cref="SystemSleeper"/> class.<para/>
        /// Initializes the only instance of the SystemSleeper.
        /// </summary>
        static SystemSleeper()
        {
            SystemSleeper.Instance = new SystemSleeper();
        }

        /// <summary>
        /// Gets the singleton instance.
        /// </summary>
        /// <value>The one instance of SystemSleeper.</value>
        internal static SystemSleeper Instance { get; }

        /// <summary>
        /// Sleeps the specified milliseconds.
        /// </summary>
        /// <param name="milliseconds">The number of milliseconds to sleep.</param>
        public void Sleep(int milliseconds)
        {
            Thread.Sleep(milliseconds);
        }

        /// <summary>
        /// Sleeps the specified duration.
        /// </summary>
        /// <param name="timeout">The duration to sleep.</param>
        public void Sleep(TimeSpan timeout)
        {
            Thread.Sleep(timeout);
        }
    }
}
