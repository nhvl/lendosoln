﻿// <copyright file="PerformanceLoggingSleeper.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: Scott K
// Date: 11/22/2016
// </summary>
namespace LendersOffice.Sleeping
{
    using System;
    using HttpModule;

    /// <summary>
    /// When this sleeps, it calls Thread.Sleep and logs the time spent sleeping to our performance logs. <para/>
    /// Slight shift from Thread.Sleep (static method) to Instance.Sleep (singleton instance . instance method). <para/>
    /// An example of the decorator pattern, decorates ISleeper with performance logging.
    /// </summary>
    public class PerformanceLoggingSleeper : ISleeper
    {
        /// <summary>
        /// The sleeper we are wrapping with performance logging.
        /// </summary>
        private readonly ISleeper sleeper;

        /// <summary>
        /// Initializes static members of the <see cref="PerformanceLoggingSleeper"/> class.<para/>
        /// Initializes the only instance with a SystemSleeper.
        /// </summary>
        static PerformanceLoggingSleeper()
        {
            PerformanceLoggingSleeper.Instance = new PerformanceLoggingSleeper(SystemSleeper.Instance);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PerformanceLoggingSleeper"/> class. <para/>
        /// Wraps the specified sleeper.
        /// </summary>
        /// <param name="sleeper">The internal sleeper to call sleep on.</param>
        private PerformanceLoggingSleeper(ISleeper sleeper)
        {
            if (sleeper == null)
            {
                throw new ArgumentNullException(nameof(sleeper));
            }

            this.sleeper = sleeper;
        }

        /// <summary>
        /// Gets the singleton instance.
        /// </summary>
        /// <value>The one instance of PerformanceLoggingSleeper.</value>
        public static PerformanceLoggingSleeper Instance { get; }

        /// <summary>
        /// Sleeps the specified duration.
        /// </summary>
        /// <param name="timeout">The duration to sleep.</param>
        public void Sleep(TimeSpan timeout)
        {
            using (PerformanceMonitorItem.Time("Sleeping."))
            {
                this.sleeper.Sleep(timeout);
            }
        }

        /// <summary>
        /// Sleeps the specified milliseconds.
        /// </summary>
        /// <param name="milliseconds">The number of milliseconds to sleep.</param>
        public void Sleep(int milliseconds)
        {
            using (PerformanceMonitorItem.Time("Sleeping."))
            {
                this.sleeper.Sleep(milliseconds);
            }
        }
    }
}
