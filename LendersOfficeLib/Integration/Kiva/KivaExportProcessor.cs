﻿namespace LendersOffice.Integration.Kiva
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Common;
    using LendersOffice.Conversions.Mismo3.Version3;

    /// <summary>
    /// A processor for exporting data to KIVA group's core system exporter.
    /// </summary>
    public class KivaExportProcessor
    {
        /// <summary>
        /// Gets the URL for the KIVA instance that will forward the data to the lenders' core systems.
        /// </summary>
        private LqbGrammar.DataTypes.LqbAbsoluteUri? ExportUrl { get; } = LqbGrammar.DataTypes.LqbAbsoluteUri.Create(Constants.ConstStage.KivaCoreExportUrl);

        /// <summary>
        /// Checks if the export via KIVA is enabled for a specified lender.
        /// </summary>
        /// <param name="lenderSettings">The lender settings to check.</param>
        /// <param name="instance">An optional instance to use to export, or null.</param>
        /// <returns>A value indicating whether the export is enabled.</returns>
        public static bool IsExportEnabled(LendersOffice.Admin.BrokerDB lenderSettings, KivaExportProcessor instance = null)
        {
            instance = instance ?? new KivaExportProcessor();
            return instance.ExportUrl.HasValue && lenderSettings.IsKivaIntegrationEnabled;
        }

        /// <summary>
        /// Exports a loan to KIVA.
        /// </summary>
        /// <param name="principal">The user who is exporting the loan.</param>
        /// <param name="loanId">The loan to export.</param>
        /// <returns>An export result, whether a success or failure.</returns>
        public KivaExportResultData ExportLoan(Security.AbstractUserPrincipal principal, Guid loanId)
        {
            if (!IsExportEnabled(principal.BrokerDB, this))
            {
                return KivaExportResultData.CreateFailure("Please contact your account manager to enable this integration.");
            }

            Tuple<bool, string> workflowResult = DataAccess.Tools.IsWorkflowOperationAuthorized(principal, loanId, ConfigSystem.Operations.WorkflowOperations.ExportToCoreSystem);
            if (!workflowResult.Item1)
            {
                return KivaExportResultData.CreateFailure(workflowResult.Item2);
            }

            var mismo33String = Mismo33RequestProvider.SerializeMessage(loanId, null, null, null, includeIntegratedDisclosureExtension: true, principal: principal);
            string response = SendMessage(this.ExportUrl.Value, mismo33String);
            var result = ParseResult(response);
            if (result.Successful)
            {
                var loan = DataAccess.CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(KivaExportProcessor));
                loan.InitSave(Constants.ConstAppDavid.SkipVersionCheck);
                loan.sCoreLoanId = result.AccountNumber;
                loan.RecordAuditOnSave(Audit.NoDetailsAuditItem.CreateKivaExportAuditItem(principal));
                loan.Save();
            }

            return result;
        }

        /// <summary>
        /// Parses the actual result from KIVA into a result data object.
        /// </summary>
        /// <param name="response">The response as a string.</param>
        /// <returns>The result of the export.</returns>
        private static KivaExportResultData ParseResult(string response)
        {
            if (string.IsNullOrEmpty(response))
            {
                return KivaExportResultData.CreateFailure(ErrorMessages.Generic);
            }

            var parseResult = DataAccess.Tools.TryParseXml(() => System.Xml.Linq.XDocument.Parse(response));
            if (parseResult.HasError)
            {
                DataAccess.Tools.LogError("Unable to parse result from Kiva", parseResult.Error);
                return KivaExportResultData.CreateFailure(ErrorMessages.Generic);
            }

            System.Xml.Linq.XDocument document = parseResult.Value;
            var outputElement = document.Element("OUTPUT");
            var resultElement = outputElement?.Element("RESULT");

            string accountNumber = resultElement?.Element("Accounts")?.Elements("Account").FirstOrDefault(e => !string.IsNullOrEmpty(e.Value))?.Value;
            if (resultElement?.Attribute("status")?.Value == "COMPLETE" && !string.IsNullOrEmpty(accountNumber))
            {
                return KivaExportResultData.CreateSuccess(accountNumber);
            }
            else
            {
                var errors = outputElement?.Element("ERRORS")?.Elements("ERROR")
                    ?.Where(e => !string.IsNullOrEmpty(e.Value))
                    ?.Select(e => e.Value)
                    ?.ToList();

                if (errors.Any())
                {
                    return KivaExportResultData.CreateFailure(errors);
                }

                return KivaExportResultData.CreateFailure(ErrorMessages.Generic);
            }
        }

        /// <summary>
        /// Sends a message for KIVA, also handling logging of the request and response.
        /// </summary>
        /// <param name="url">The URL to send the request to.</param>
        /// <param name="payload">The message to send to KIVA.</param>
        /// <returns>The response received from KIVA.</returns>
        private static string SendMessage(LqbGrammar.DataTypes.LqbAbsoluteUri url, string payload)
        {
            var options = new LqbGrammar.Drivers.HttpRequest.HttpRequestOptions()
            {
                Method = LqbGrammar.DataTypes.HttpMethod.Post,
                PostData = new LqbGrammar.DataTypes.StringContent(payload),
            };

            string responseString;
            try
            {
                DataAccess.Tools.LogInfo("KivaExportRequest", payload);
                Drivers.HttpRequest.WebRequestHelper.ExecuteCommunication(url, options);
                responseString = options.ResponseBody;
            }
            catch (LqbGrammar.Exceptions.ServerException exc) when (exc.InnerException is System.Net.WebException)
            {
                DataAccess.Tools.LogWarning("Exception exporting to Kiva", exc);

                var webException = exc.InnerException as System.Net.WebException;
                using (var response = webException.Response)
                using (var responseStream = response?.GetResponseStream() ?? new System.IO.MemoryStream())
                using (var responseReader = new System.IO.StreamReader(responseStream))
                {
                    responseString = responseReader.ReadToEnd();
                }
            }

            DataAccess.Tools.LogInfo("KivaExportResponse", responseString);
            return responseString;
        }
    }
}
