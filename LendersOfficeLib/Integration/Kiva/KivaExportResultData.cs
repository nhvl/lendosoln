﻿namespace LendersOffice.Integration.Kiva
{
    using System.Collections.Generic;

    /// <summary>
    /// Represents the result of an attempt to export data via KIVA.
    /// </summary>
    public class KivaExportResultData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KivaExportResultData"/> class.
        /// </summary>
        /// <param name="successful">A value indicating whether the export succeeded.</param>
        /// <param name="userErrorMessages">A collection of errors for the user.</param>
        /// <param name="accountNumber">The identifier for the loan.</param>
        private KivaExportResultData(bool successful, List<string> userErrorMessages, string accountNumber)
        {
            this.Successful = successful;
            this.UserErrorMessages = userErrorMessages;
            this.AccountNumber = accountNumber;
        }

        /// <summary>
        /// Gets a value indicating whether the export via KIVA succeeded.
        /// </summary>
        public bool Successful { get; }

        /// <summary>
        /// Gets a collection of errors to display to the user if <see cref="Successful"/> is <c>false</c>.
        /// </summary>
        public List<string> UserErrorMessages { get; }

        /// <summary>
        /// Gets the identifier for the loan in KIVA's system, provided <see cref="Successful"/> is <c>true</c>.
        /// </summary>
        public string AccountNumber { get; }

        /// <summary>
        /// Create a new instance representing a successful export response.
        /// </summary>
        /// <param name="accountNumber">The identifier for the loan.</param>
        /// <returns>A new instance of this type.</returns>
        public static KivaExportResultData CreateSuccess(string accountNumber)
        {
            return new KivaExportResultData(true, null, accountNumber);
        }

        /// <summary>
        /// Create a new instance representing a failed export response.
        /// </summary>
        /// <param name="userErrorMessages">A collection of errors to display to the user.</param>
        /// <returns>A new instance of this type.</returns>
        public static KivaExportResultData CreateFailure(List<string> userErrorMessages)
        {
            return new KivaExportResultData(
                successful: false,
                userErrorMessages: userErrorMessages,
                accountNumber: null);
        }

        /// <summary>
        /// Create a new instance representing a failed export response.
        /// </summary>
        /// <param name="userErrorMessage">A single error to display to the user.</param>
        /// <returns>A new instance of this type.</returns>
        public static KivaExportResultData CreateFailure(string userErrorMessage)
        {
            return new KivaExportResultData(
                successful: false,
                userErrorMessages: new List<string>() { userErrorMessage },
                accountNumber: null);
        }
    }
}
