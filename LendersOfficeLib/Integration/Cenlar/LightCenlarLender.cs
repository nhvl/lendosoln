﻿namespace LendersOffice.Integration.Cenlar
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Linq;
    using DataAccess;

    /// <summary>
    /// A lightweight class representing lenders using the Cenlar integration.
    /// </summary>
    public class LightCenlarLender
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="LightCenlarLender"/> class from being created.
        /// </summary>
        private LightCenlarLender()
        {
            // no-op
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LightCenlarLender"/> class.
        /// </summary>
        /// <param name="reader">A data reader.</param>
        private LightCenlarLender(IDataReader reader)
        {
            this.BrokerId = (Guid)reader["BrokerId"];
            this.CustomerCode = (string)reader["CustomerCode"];
            this.CompanyName = (string)reader["BrokerNm"];
            this.IsCenlarEnabled = (bool)reader["IsCenlarEnabled"];
            this.EnableAutomaticTransmissions = (bool)reader["EnableAutomaticTransmissions"];
        }

        /// <summary>
        /// Gets the lender's unique identifier.
        /// </summary>
        public Guid BrokerId { get; }

        /// <summary>
        /// Gets the lender's customer code.
        /// </summary>
        public string CustomerCode { get; }

        /// <summary>
        /// Gets the lender's company name.
        /// </summary>
        public string CompanyName { get; }

        /// <summary>
        /// Gets a value indicating whether the lender has the Cenlar integration enabled.
        /// </summary>
        public bool IsCenlarEnabled { get; }

        /// <summary>
        /// Gets a value indicating whether the lender has automatic Cenlar transmissions enabled.
        /// </summary>
        public bool EnableAutomaticTransmissions { get; }

        /// <summary>
        /// Loads lenders who have the Cenlar integration enabled.
        /// </summary>
        /// <param name="filterForAutomatedTransmissions">
        /// If true, only lenders configured for automated transmissions will be returned.
        /// </param>
        /// <returns>A collection of lenders using the Cenlar integration.</returns>
        public static List<LightCenlarLender> LoadAll(bool filterForAutomatedTransmissions = false)
        {
            var lenders = new List<LightCenlarLender>();
            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "GetLendersUsingCenlarIntegration", parameters: null))
                {
                    while (reader.Read())
                    {
                        var lender = new LightCenlarLender(reader);
                        if (filterForAutomatedTransmissions && !lender.EnableAutomaticTransmissions)
                        {
                            continue;
                        }

                        lenders.Add(lender);
                    }
                }
            }

            return lenders;
        }
    }
}
