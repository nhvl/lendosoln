﻿namespace LendersOffice.Integration.Cenlar
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// The results of an individual loan after a transmission to Cenlar.
    /// </summary>
    public class CenlarLoanResult : CenlarLoan
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CenlarLoanResult"/> class.
        /// </summary>
        /// <param name="transmittedLoan">Contains the identifiers for the loan.</param>
        /// <param name="status">The boarding status for this loan.</param>
        public CenlarLoanResult(CenlarLoan transmittedLoan, CenlarLoanStatusT status)
            : base(transmittedLoan.LoanId, transmittedLoan.LoanNumber, transmittedLoan.CenlarIdentifier)
        {
            this.Status = status;
            this.Warnings = new List<string>();
            this.Errors = new List<string>();
        }

        /// <summary>
        /// Gets or sets the loan's boarding status.
        /// </summary>
        public CenlarLoanStatusT Status { get; set; }

        /// <summary>
        /// Indicates whether the loan result contains any warning messages.
        /// </summary>
        public bool HasWarnings => this.Warnings.Any();

        /// <summary>
        /// Indicates whether the loan result contains any error messages.
        /// </summary>
        public bool HasErrors => this.Errors.Any();

        /// <summary>
        /// Gets a list of warnings returned by Cenlar.
        /// </summary>
        public List<string> Warnings { get; }

        /// <summary>
        /// Gets a list of errors returned by Cenlar.
        /// </summary>
        public List<string> Errors { get; }
    }
}
