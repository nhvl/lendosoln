﻿namespace LendersOffice.Integration.Cenlar
{
    /// <summary>
    /// Indicates the boarding status of a loan transmitted to Cenlar.
    /// </summary>
    public enum CenlarLoanStatusT
    {
        /// <summary>
        /// Indicates that the loan was accepted into Cenlar's system.
        /// </summary>
        Accepted = 0,

        /// <summary>
        /// Indicates that the loan was rejected by Cenlar's system.
        /// </summary>
        Rejected = 1
    }
}
