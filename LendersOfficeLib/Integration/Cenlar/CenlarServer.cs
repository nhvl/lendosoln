﻿namespace LendersOffice.Integration.Cenlar
{
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Text;
    using LendersOffice.Integration.Templates;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Manages the process of making a web request to Cenlar and receiving a response.
    /// </summary>
    public class CenlarServer : AbstractIntegrationServer
    {
        /// <summary>
        /// The URL for Cenlar's production environment.
        /// </summary>
        private const string ProductionCenlarUrl = "https://newloans.cennet.com";

        /// <summary>
        /// The URL for Cenlar's beta environment.
        /// </summary>
        private const string BetaCenlarUrl = "https://beta.newloans.cennet.com";

        /// <summary>
        /// The request data for the current transaction.
        /// </summary>
        private CenlarRequestData requestData;

        /// <summary>
        /// A list of errors occurring during the transmission.
        /// </summary>
        private List<string> errors;

        /// <summary>
        /// Initializes a new instance of the <see cref="CenlarServer"/> class.
        /// </summary>
        /// <param name="requestData">The request data for the current transaction.</param>
        public CenlarServer(CenlarRequestData requestData)
        {
            this.requestData = requestData;
            this.errors = new List<string>();
        }

        /// <summary>
        /// Authentication is a special method in the Cenlar API that is only called
        /// immediately prior to sending loans, never by itself.
        /// </summary>
        /// <returns>The authentication response.</returns>
        public string Authenticate()
        {
            string url = this.GenerateUrlForRequest(overrideAction: CenlarActionT.RetrieveFileId);
            return this.Transmit(url, "POST", payload: null);
        }

        /// <summary>
        /// Sends a <see cref="WebRequest" /> to Cenlar based on the given <see cref="CenlarActionT" />.
        /// </summary>
        /// <param name="payload">The payload to transmit.</param>
        /// <returns>An <see cref="XDocument" /> containing the response from Cenlar.</returns>
        public override string PlaceRequest(string payload)
        {
            byte[] payloadBytes = new byte[0];
            if (this.requestData.Action == CenlarActionT.SendLoanFiles)
            {
                if (string.IsNullOrEmpty(payload))
                {
                    // Must have a payload when sending loan files
                    throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"No payload was generated for a loan file transmission for {this.requestData.CustomerCode}."));
                }
                else
                {
                    payloadBytes = Encoding.UTF8.GetBytes(payload);
                }
            }

            string url = this.GenerateUrlForRequest();
            string httpMethod = this.requestData.Action == CenlarActionT.RetrieveResults ? "GET" : "POST";

            var response = this.Transmit(url, httpMethod, payloadBytes);
            return response;
        }

        /// <summary>
        /// Sents a payload to Cenlar and retrieves the response.
        /// </summary>
        /// <param name="url">The URL to transmit to.</param>
        /// <param name="httpMethod">The HTTP method to use.</param>
        /// <param name="payload">The payload to send.</param>
        /// <returns>The response from Cenlar.</returns>
        private string Transmit(string url, string httpMethod, byte[] payload)
        {
            var request = WebRequest.Create(url);
            request.Method = httpMethod;
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = 0;

            if (payload?.Length > 0)
            {
                request.ContentLength = payload.Length;
                request.Timeout = 5 /* minutes */ * 60 /* seconds */ * 1000 /* milliseconds */;

                Stream dataStream = request.GetRequestStream();
                dataStream.Write(payload, 0, payload.Length);
                dataStream.Close();
            }

            string responseString = string.Empty;
            using (WebResponse response = request.GetResponse())
            using (var reader = new StreamReader(response.GetResponseStream()))
            {
                responseString = reader.ReadToEnd();
            }

            if (string.IsNullOrEmpty(responseString))
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext("Cenlar's response did not contain a body."));
            }

            return responseString;
        }

        /// <summary>
        /// Builds the Cenlar URL to send a <see cref="WebRequest" /> to. This is dependent on
        /// which <see cref="CenlarActionT" /> is being taken.
        /// </summary>
        /// <param name="overrideAction">If included, overrides the action specified in the <see cref="CenlarRequestData"/>.</param>
        /// <returns>A constructed URL based on the <see cref="CenlarActionT" /> requested.</returns>
        /// <exception cref="DeveloperException">If an unhandled <see cref="CenlarActionT" /> value is passed in.</exception>
        private string GenerateUrlForRequest(CenlarActionT? overrideAction = null)
        {
            var domain = this.requestData.DestinationEnvironmentT == CenlarEnvironmentT.Beta ? BetaCenlarUrl : ProductionCenlarUrl;
            var action = overrideAction.HasValue ? overrideAction.Value : this.requestData.Action;

            switch (action)
            {
                case CenlarActionT.RetrieveFileId:
                    return $"{domain}/CenBaseAPI.dll?TranKey?{this.requestData.Credentials.UserId}&{this.requestData.Credentials.ApiKey}";
                case CenlarActionT.SendLoanFiles:
                    return $"{domain}/CenBaseAPI.dll?CenPost";
                case CenlarActionT.RetrieveResults:
                    return $"{domain}/CenBaseAPI.dll?TranReturn?{this.requestData.Credentials.UserId}&{this.requestData.Credentials.ApiKey}&{this.requestData.FileId}";
                case CenlarActionT.UpdateApiKey:
                    return $"{domain}/CenBaseAPI.dll?GetKey?{this.requestData.Credentials.UserId}&{this.requestData.Credentials.ApiKey}";
                default:
                    throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"'{action}' is not a valid Cenlar action."));
            }
        }
    }
}
