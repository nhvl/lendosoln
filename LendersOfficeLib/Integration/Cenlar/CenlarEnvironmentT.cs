﻿namespace LendersOffice.Integration.Cenlar
{
    /// <summary>
    /// Specifies one of Cenlar's environments.
    /// </summary>
    public enum CenlarEnvironmentT
    {
        /// <summary>
        /// No environment selected.
        /// </summary>
        LeaveBlank = 0,

        /// <summary>
        /// Cenlar's Beta environment.
        /// </summary>
        Beta = 1,

        /// <summary>
        /// Cenlar's Production environment.
        /// </summary>
        Production = 2
    }
}
