﻿namespace LendersOffice.Integration.Cenlar
{
    using System;
    using System.Linq;
    using System.Text;
    using Constants;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Email;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Constructs an HTML summary after each transaction and emails it
    /// to people specified by the client, as well as internal users.
    /// </summary>
    /// <remarks>
    /// This class can be retired if/when we implement a pipeline dashboard
    /// for Cenlar to show queued loans and look up recent transmissions.
    /// </remarks>
    public static class CenlarMailHelper
    {
        /// <summary>
        /// A "do not reply" email address to send messages on behalf of LendingQB.
        /// </summary>
        private const string DoNotReplyAddress = "do-not-reply@lendingqb.com";

        /// <summary>
        /// Sends a summary of the current data transmission to the lender.
        /// </summary>
        /// <param name="requestData">The transaction request data.</param>
        /// <param name="responseData">The Cenlar response data.</param>
        public static void SendTransmissionSummary(CenlarRequestData requestData, CenlarResponseData responseData)
        {
            var mail = new CBaseEmail(requestData.BrokerId);
            var messageBody = WriteTransmissionSummary(requestData, responseData);

            mail.To = string.Join(",", requestData.NotificationEmails);
            mail.From = DoNotReplyAddress;

            mail.Subject = $"Cenlar Transmission Summary: {requestData.CustomerCode}";
            mail.Message = messageBody;

            mail.AddAttachment($"CenlarResults_{requestData.FileId}.xml", Encoding.UTF8.GetBytes(responseData.RawResponse));
            mail.IsHtmlEmail = true;

            Tools.LogInfo($"Sending {requestData.CustomerCode} summary email to the following recipients: {mail.To}{Environment.NewLine}{Environment.NewLine}{messageBody}");

            EmailUtilities.SendEmail(mail);
        }

        /// <summary>
        /// Sends an exception to the ISE pipeline.
        /// </summary>
        /// <param name="requestData">The transaction request data.</param>
        /// <param name="exc">An exception to send to the ISE pipeline.</param>
        public static void SendErrorToIseTeam(CenlarRequestData requestData, Exception exc)
        {
            if (ConstAppDavid.CurrentServerLocation != ServerLocation.Production || !requestData.IsAutomatedTransmission)
            {
                // We only care about pinging the ISE pipeline for automated production transmissions. In any other case,
                // simply logging the exception is sufficient.
                return;
            }

            var newLine = Environment.NewLine;
            string exceptionBody = string.Empty;
            if (exc is CBaseException)
            {
                var cbaseExc = (CBaseException)exc;
                exceptionBody = $"User message:{newLine}{cbaseExc.UserMessage}{newLine}{newLine}Developer message:{cbaseExc.DeveloperMessage}";
            }
            else if (exc is DeveloperException)
            {
                var devExc = (DeveloperException)exc;
                exceptionBody = $"Message:{newLine}{devExc.Message}{newLine}{newLine}Context:{newLine}{devExc.Context}";
            }
            else
            {
                exceptionBody = exc.Message;
            }

            var mail = new CBaseEmail(requestData.BrokerId);

            mail.From = DoNotReplyAddress;
            mail.To = Tools.JoinEmailList(ConstStage.CenlarIseNotificationEmails);

            mail.Subject = $"{requestData.CustomerCode} | Cenlar Transmission Service Error";
            mail.Message = $"Error running the Cenlar Transmission Service:{newLine}{newLine}{exceptionBody}";

            EmailUtilities.SendEmail(mail);
        }

        /// <summary>
        /// Writes a summary of the transmission instance for emailing to a lender.
        /// </summary>
        /// <param name="requestData">The transaction request data.</param>
        /// <param name="responseData">The Cenlar response data.</param>
        /// <returns>A string summary of the transmission with HTML formatting.</returns>
        private static string WriteTransmissionSummary(CenlarRequestData requestData, CenlarResponseData responseData)
        {
            if (requestData.Configuration.ShouldSuppressAcceptedLoanNotifications && !responseData.LoanResults.Where(result => result.Status == CenlarLoanStatusT.Rejected).Any())
            {
                return string.Empty;
            }

            var messageBody = new StringBuilder();
            messageBody.Append($"The following loan data was transmitted to Cenlar's {requestData.DestinationEnvironmentT.ToString()} environment on {DateTime.Now.ToShortDateString()}.");
            messageBody.Append("<br /><br/><hr /><br />");

            foreach (var rejectedLoan in responseData.LoanResults)
            {
                WriteLoanResult(rejectedLoan, messageBody, requestData.Configuration.ShouldSuppressAcceptedLoanNotifications);
            }

            messageBody.Append("<b>Aggregate Accepted Loan Data:</b>");
            WriteAggregateSummary(responseData.AcceptedLoansSummary, messageBody);

            messageBody.Append("<br /><br />");

            messageBody.Append("<b>Aggregate Rejected Loan Data:</b>");
            WriteAggregateSummary(responseData.RejectedLoansSummary, messageBody);

            return messageBody.ToString();
        }

        /// <summary>
        /// Writes a set of aggregate loan boarding data.
        /// </summary>
        /// <param name="summary">An aggregate boarding summary.</param>
        /// <param name="messageBody">The message body being constructed.</param>
        private static void WriteAggregateSummary(CenlarBoardingSummary summary, StringBuilder messageBody)
        {
            messageBody.Append($"<br />Loan Count: {summary.LoanCount}");
            messageBody.Append($"<br />Principal: {summary.Principal}");
            messageBody.Append($"<br />Escrow Balance: {summary.EscrowBalance}");
            messageBody.Append($"<br />Interim Interest Cash: {summary.InterimInterestCash}");
            messageBody.Append($"<br />Principal And Interest: {summary.PrincipalAndInterest}");
            messageBody.Append($"<br />Tax And Insurance: {summary.TaxAndInsurance}");
            messageBody.Append($"<br />Restricted Escrow: {summary.RestrictedEscrow}");
            messageBody.Append($"<br />Buydown Balance: {summary.BuydownBalance}");
            messageBody.Append($"<br />Total Funds: {summary.TotalFunds}");
        }

        /// <summary>
        /// Writes the results for a particular loan to a string for emailing to the lender.
        /// </summary>
        /// <param name="loan">A set of loan results.</param>
        /// <param name="messageBody">The message body being constructed.</param>
        /// <param name="suppressAcceptedLoans">Indicates whether accepted loans should be excluded from the summary.</param>
        private static void WriteLoanResult(CenlarLoanResult loan, StringBuilder messageBody, bool suppressAcceptedLoans)
        {
            if (suppressAcceptedLoans && loan.Status == CenlarLoanStatusT.Accepted)
            {
                return;
            }

            messageBody.Append($"<b>Cenlar Loan Number:</b> {loan.CenlarIdentifier}<br />");
            messageBody.Append($"<b>LendingQB Loan Number:</b> {loan.LoanNumber}<br />");

            string color = loan.Status.Equals(CenlarLoanStatusT.Accepted) ? "green" : "red";
            messageBody.Append($"<b>Status:</b> <font color=\"{color}\">{loan.Status.ToString()}</font><br />");

            if (loan.HasErrors)
            {
                messageBody.Append("<b>Errors:</b>");
                messageBody.Append("<ul>");

                foreach (var error in loan.Errors)
                {
                    messageBody.Append($"<li>{error}</li>");
                }

                messageBody.Append("</ul>");
            }

            if (loan.HasWarnings)
            {
                messageBody.Append("<br /><b>Warnings:</b>");
                messageBody.Append("<ul>");

                foreach (var warning in loan.Warnings)
                {
                    messageBody.Append($"<li>{warning}</li>");
                }

                messageBody.Append("</ul>");
            }

            messageBody.Append("<hr /><br />");
        }
    }
}