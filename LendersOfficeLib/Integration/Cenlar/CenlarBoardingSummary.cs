﻿namespace LendersOffice.Integration.Cenlar
{
    /// <summary>
    /// Represents aggregate information about a transmission.
    /// </summary>
    public struct CenlarBoardingSummary
    {
        /// <summary>
        /// Gets or sets the number of loans captured in this summary.
        /// </summary>
        public int LoanCount { get; set; }

        /// <summary>
        /// Gets or sets the aggregate principal.
        /// </summary>
        public decimal Principal { get; set; }

        /// <summary>
        /// Gets or sets the aggregate escrow balance.
        /// </summary>
        public decimal EscrowBalance { get; set; }

        /// <summary>
        /// Gets or sets the aggregate interim interest cash amount.
        /// </summary>
        public decimal InterimInterestCash { get; set; }

        /// <summary>
        /// Gets or sets the aggregate principal and interest.
        /// </summary>
        public decimal PrincipalAndInterest { get; set; }

        /// <summary>
        /// Gets or sets the aggregate tax and insurance amount.
        /// </summary>
        public decimal TaxAndInsurance { get; set; }

        /// <summary>
        /// Gets or sets the aggregate restricted escrow amount.
        /// </summary>
        public decimal RestrictedEscrow { get; set; }

        /// <summary>
        /// Gets or sets the aggregate buydown balance.
        /// </summary>
        public decimal BuydownBalance { get; set; }

        /// <summary>
        /// Gets or sets the total aggregate funds.
        /// </summary>
        public decimal TotalFunds { get; set; }
    }
}
