﻿namespace LendersOffice.Integration.Cenlar
{
    /// <summary>
    /// Indicates the action that should be taken in the <see cref="WebRequest" />.
    /// This determines the CenBaseAPI function that we will call in the URI.
    /// </summary>
    public enum CenlarActionT
    {
        /// <summary>
        /// Retrieve a FileID for authentication.
        /// </summary>
        RetrieveFileId = 0,

        /// <summary>
        /// Transmit loan data to Cenlar.
        /// </summary>
        SendLoanFiles = 1,

        /// <summary>
        /// Retrieve transmission results asynchronously.
        /// </summary>
        RetrieveResults = 2,

        /// <summary>
        /// Request a new secret key for the current lender.
        /// </summary>
        UpdateApiKey = 3
    }
}