﻿namespace LendersOffice.Integration.Cenlar
{
    using System;
    using System.Data.SqlClient;
    using DataAccess;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// A set of Cenlar credentials associated with a lender.
    /// </summary>
    public class CenlarCredentialSet
    {
        /// <summary>
        /// Gets or sets the broker ID associated with this credential set.
        /// </summary>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets the Cenlar environment for this credential set.
        /// </summary>
        public CenlarEnvironmentT EnvironmentT { get; set; }

        /// <summary>
        /// Gets or sets the Cenlar user ID.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets the secret key assigned by Cenlar. A new key must be generated
        /// every 30 days.
        /// </summary>
        public string ApiKey { get; set; }

        /// <summary>
        /// Gets or sets the timestamp when the API key was last regenerated.
        /// </summary>
        public DateTime LastApiKeyReset { get; set; }

        /// <summary>
        /// Gets or sets an ID that allows Cenlar to uniquely identify the lender.
        /// </summary>
        public string CustomerId { get; set; }

        /// <summary>
        /// The template name to use when calling into Cenlar's API.
        /// </summary>
        /// <remarks>
        /// This is the template for boarding "new loans" to Cenlar. We don't
        /// expect that any clients will have to use any other template in
        /// the foreseeable future, but if that occurs, we should convert this
        /// to be stored as an enum in the database.
        /// </remarks>
        public string TemplateName => "NEWLOAN";

        /// <summary>
        /// Gets a value indicating whether this credential object is valid.
        /// </summary>
        public bool IsValid
        {
            get
            {
                return this.BrokerId != Guid.Empty
                    && this.EnvironmentT != CenlarEnvironmentT.LeaveBlank
                    && !string.IsNullOrEmpty(this.UserId)
                    && !string.IsNullOrEmpty(this.ApiKey)
                    && this.LastApiKeyReset != default(DateTime)
                    && !string.IsNullOrEmpty(this.CustomerId);
            }
        }

        /// <summary>
        /// Retrieves a Cenlar credential set from the database.
        /// </summary>
        /// <param name="brokerId">The broker to retrieve.</param>
        /// <param name="environment">The environment to retrieve.</param>
        /// <returns>A set of Cenlar credentials.</returns>
        public static CenlarCredentialSet Retrieve(Guid brokerId, CenlarEnvironmentT environment)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@EnvironmentT", environment),
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "CENLAR_LENDER_CREDENTIAL_Retrieve", parameters))
            {
                CenlarCredentialSet credentials = null;

                if (reader.Read())
                {
                    credentials = new CenlarCredentialSet();
                    credentials.BrokerId = (Guid)reader[nameof(credentials.BrokerId)];
                    credentials.EnvironmentT = (CenlarEnvironmentT)reader[nameof(credentials.EnvironmentT)];
                    credentials.UserId = (string)reader[nameof(credentials.UserId)];
                    credentials.ApiKey = (string)reader[nameof(credentials.ApiKey)];
                    credentials.LastApiKeyReset = (DateTime)reader[nameof(credentials.LastApiKeyReset)];
                    credentials.CustomerId = (string)reader[nameof(credentials.CustomerId)];
                }

                return credentials;
            }
        }

        /// <summary>
        /// Saves a credential set to the database.
        /// </summary>
        /// <param name="credentials">The credential set to save.</param>
        public static void Save(CenlarCredentialSet credentials)
        {
            if (!credentials.IsValid)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext("Cannot save an invalid Cenlar credential set."));
            }

            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", credentials.BrokerId),
                new SqlParameter("@EnvironmentT", credentials.EnvironmentT),
                new SqlParameter("@UserId", credentials.UserId),
                new SqlParameter("@ApiKey", credentials.ApiKey),
                new SqlParameter("@LastApiKeyReset", credentials.LastApiKeyReset),
                new SqlParameter("@CustomerId", credentials.CustomerId)
            };

            StoredProcedureHelper.ExecuteNonQuery(credentials.BrokerId, "CENLAR_LENDER_CREDENTIAL_Save", 5, parameters);
        }
    }
}
