﻿namespace LendersOffice.Integration.Cenlar
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using DataAccess;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Holds configuration data for a lender using the Cenlar integration.
    /// </summary>
    public class CenlarLenderConfiguration
    {
        /// <summary>
        /// A joined string of notification emails.
        /// </summary>
        private string notificationEmailsRaw;

        /// <summary>
        /// Gets or sets the broker ID associated with this configuration.
        /// </summary>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the lender is configured for
        /// automatic transmissions.
        /// </summary>
        public bool EnableAutomaticTransmissions { get; set; }

        /// <summary>
        /// Gets or sets the ID of the lender's Cenlar dummy user.
        /// </summary>
        public Guid DummyUserId { get; set; }

        /// <summary>
        /// Gets or sets the name of the lender's Cenlar custom report.
        /// </summary>
        public string CustomReportName { get; set; }

        /// <summary>
        /// Gets or sets the name of the lender's Cenlar batch export.
        /// </summary>
        public string BatchExportName { get; set; }

        /// <summary>
        /// Gets or sets a collection of notification emails.
        /// </summary>
        public IEnumerable<string> NotificationEmails
        {
            get { return Tools.SplitEmailList(this.notificationEmailsRaw); }
            set { this.notificationEmailsRaw = Tools.JoinEmailList(value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether notifications about accepted loans should 
        /// be included in the notification email.
        /// </summary>
        /// <remarks>
        /// Some clients prefer to only see notifications about loans that failed to board.
        /// </remarks>
        public bool ShouldSuppressAcceptedLoanNotifications { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether problematic characters should be stripped
        /// from Cenlar payloads for this lender.
        /// </summary>
        /// <remarks>
        /// Some characters like the apostrophe or grave accent can cause problems on Cenlar's
        /// side. This toggle allows those characters to be auto-removed from payloads to
        /// prevent these issues.
        /// </remarks>
        public bool RemoveProblematicPayloadCharacters { get; set; }

        /// <summary>
        /// Gets a value indicating whether this credential object is valid.
        /// </summary>
        public bool IsValid
        {
            get
            {
                return this.BrokerId != Guid.Empty
                    && this.DummyUserId != Guid.Empty
                    && !string.IsNullOrEmpty(this.CustomReportName)
                    && !string.IsNullOrEmpty(this.BatchExportName)
                    && !string.IsNullOrEmpty(this.notificationEmailsRaw);
            }
        }

        /// <summary>
        /// Retrieves a Cenlar configuration from the database.
        /// </summary>
        /// <param name="brokerId">The broker ID to retrieve.</param>
        /// <returns>A Cenlar configuration.</returns>
        public static CenlarLenderConfiguration Retrieve(Guid brokerId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "CENLAR_LENDER_CONFIGURATION_Retrieve", parameters))
            {
                CenlarLenderConfiguration config = null;

                if (reader.Read())
                {
                    config = new CenlarLenderConfiguration();
                    config.EnableAutomaticTransmissions = (bool)reader[nameof(config.EnableAutomaticTransmissions)];
                    config.DummyUserId = (Guid)reader[nameof(config.DummyUserId)];
                    config.CustomReportName = (string)reader[nameof(config.CustomReportName)];
                    config.BatchExportName = (string)reader[nameof(config.BatchExportName)];
                    config.notificationEmailsRaw = (string)reader[nameof(config.NotificationEmails)];
                    config.ShouldSuppressAcceptedLoanNotifications = (bool)reader[nameof(config.ShouldSuppressAcceptedLoanNotifications)];

                    if (reader[nameof(config.RemoveProblematicPayloadCharacters)] != DBNull.Value)
                    {
                        config.RemoveProblematicPayloadCharacters = (bool)reader[nameof(config.RemoveProblematicPayloadCharacters)];
                    }
                }

                return config;
            }
        }

        /// <summary>
        /// Saves a lender configuration to the database.
        /// </summary>
        /// <param name="config">The configuration to save.</param>
        public static void Save(CenlarLenderConfiguration config)
        {
            if (!config.IsValid)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext("Cannot save an invalid Cenlar configuration."));
            }

            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", config.BrokerId),
                new SqlParameter("@EnableAutomaticTransmissions", config.EnableAutomaticTransmissions),
                new SqlParameter("@DummyUserId", config.DummyUserId),
                new SqlParameter("@CustomReportName", config.CustomReportName),
                new SqlParameter("@BatchExportName", config.BatchExportName),
                new SqlParameter("@NotificationEmails", config.notificationEmailsRaw),
                new SqlParameter("@ShouldSuppressAcceptedLoanNotifications", config.ShouldSuppressAcceptedLoanNotifications),
                new SqlParameter("@RemoveProblematicPayloadCharacters", config.RemoveProblematicPayloadCharacters)
            };

            StoredProcedureHelper.ExecuteNonQuery(config.BrokerId, "CENLAR_LENDER_CONFIGURATION_Save", 5, parameters);
        }
    }
}
