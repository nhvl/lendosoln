﻿namespace LendersOffice.Integration.Cenlar.LoanCleanup
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Constants;

    /// <summary>
    /// Specifies the loan cleanup process for PML0299.
    /// </summary>
    public class CenlarLoanCleanup_PML0299 : CenlarLoanCleanup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CenlarLoanCleanup_PML0299"/> class.
        /// </summary>
        /// <param name="loanIds">The set of loans to clean up.</param>
        public CenlarLoanCleanup_PML0299(IEnumerable<Guid> loanIds)
            : base(loanIds)
        {
            // no-op
        }

        /// <summary>
        /// Removes a loan from the queue for PML0299.
        /// </summary>
        /// <param name="loanId">The loan to dequeue.</param>
        protected override void DequeueLoan(Guid loanId)
        {
            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(CenlarLoanCleanup_PML0299));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // To be picked up in PML0299's custom report for Cenlar boarding, the loan:
            //  * No explicit criteria, the report is maintained by the client's servicing team.
            // To dequeue: Set the current date in the Subservicing Transfer Effective Date.
            dataLoan.sGLServTransEffD = CDateTime.Create(DateTime.Now);

            dataLoan.Save();
        }
    }
}
