﻿namespace LendersOffice.Integration.Cenlar.LoanCleanup
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Constants;

    /// <summary>
    /// Specifies the loan cleanup process for PML0158.
    /// </summary>
    public class CenlarLoanCleanup_PML0158 : CenlarLoanCleanup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CenlarLoanCleanup_PML0158"/> class.
        /// </summary>
        /// <param name="loanIds">The set of loans to clean up.</param>
        public CenlarLoanCleanup_PML0158(IEnumerable<Guid> loanIds)
            : base(loanIds)
        {
            // no-op
        }

        /// <summary>
        /// Removes a loan from the queue for PML0158.
        /// </summary>
        /// <param name="loanId">The loan to dequeue.</param>
        protected override void DequeueLoan(Guid loanId)
        {
            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(CenlarLoanCleanup_PML0158));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // To be picked up in PML0158's custom report for Cenlar boarding, the loan:
            //  * Must have the custom field 21 checkbox ticked.
            // To dequeue: untick the checkbox.
            dataLoan.sCustomField21Bit = false;

            // Record the date the loan was boarded to Cenlar.
            dataLoan.sCustomField21D = CDateTime.Create(DateTime.Now);

            dataLoan.Save();
        }
    }
}
