﻿namespace LendersOffice.Integration.Cenlar.LoanCleanup
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Constants;

    /// <summary>
    /// Specified the loan cleanup process for PML0263.
    /// </summary>
    public class CenlarLoanCleanup_PML0263 : CenlarLoanCleanup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CenlarLoanCleanup_PML0263"/> class.
        /// </summary>
        /// <param name="loanIds">The set of loans to clean up.</param>
        public CenlarLoanCleanup_PML0263(IEnumerable<Guid> loanIds)
            : base(loanIds)
        {
            // no-op
        }

        /// <summary>
        /// Removes a loan from the queue for PML0263.
        /// </summary>
        /// <param name="loanId">The loan to dequeue.</param>
        protected override void DequeueLoan(Guid loanId)
        {
            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(CenlarLoanCleanup_PML0263));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // To be picked up in PML0263's custom report for Cenlar boarding, the loan:
            //  * Must have a subservicer loan number (sSubservicerLoanNm)
            //  * Must have a Loan Package received date (sLoanPackageReceivedD)
            //  * Must NOT have custom field 40 ticked
            // To dequeue: tick the checkbox.
            dataLoan.sCustomField40Bit = true;

            // Record the date the loan was boarded to Cenlar.
            var boardingDate = CDateTime.Create(DateTime.Now);
            dataLoan.sCustomField40D = boardingDate;
            dataLoan.sShippedToInvestorD = boardingDate;

            dataLoan.Save();
        }
    }
}
