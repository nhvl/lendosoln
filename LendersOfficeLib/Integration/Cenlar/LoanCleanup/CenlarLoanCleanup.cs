﻿namespace LendersOffice.Integration.Cenlar.LoanCleanup
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Each lender using Cenlar "queues" loans using a custom report, so the criteria
    /// are lender-specific. After a transmission, loans accepted by Cenlar need to be
    /// "dequeued", which consequently requires lender specific logic. This class is
    /// responsible for cleaning up the loan files after transmission.
    /// </summary>
    /// <remarks>
    /// The need for this class stems from the initial Cenlar implementation, which was an
    /// external tool and sent back LOXML snippets through web services to do this cleanup.
    /// Now that the tool exists in the main project, we should at some point consider
    /// normalizing the queueing logic so we can handle each lender the same way and remove
    /// the dependency on processing loan objects.
    /// </remarks>
    public abstract class CenlarLoanCleanup
    {
        /// <summary>
        /// The set of loan IDs to process.
        /// </summary>
        private IEnumerable<Guid> loanIds;

        /// <summary>
        /// Initializes a new instance of the <see cref="CenlarLoanCleanup"/> class.
        /// </summary>
        /// <param name="loanIds">The collection of loan IDs to process.</param>
        protected CenlarLoanCleanup(IEnumerable<Guid> loanIds)
        {
            this.loanIds = loanIds;
        }

        /// <summary>
        /// Generates the correct loan cleanup object based on the lender.
        /// </summary>
        /// <param name="loanIds">The collection of loan IDs to process.</param>
        /// <param name="customerCode">The lender's customer code.</param>
        /// <returns>A loan cleanup object generated based on the lender.</returns>
        public static CenlarLoanCleanup Generate(IEnumerable<Guid> loanIds, string customerCode)
        {
            switch (customerCode)
            {
                case "THINHTEST2":
                    return new CenlarLoanCleanup_THINHTEST2(loanIds);
                case "PML0158":
                    return new CenlarLoanCleanup_PML0158(loanIds);
                case "PML0263":
                    return new CenlarLoanCleanup_PML0263(loanIds);
                case "PML0270":
                    return new CenlarLoanCleanup_PML0270(loanIds);
                case "PML0299":
                    return new CenlarLoanCleanup_PML0299(loanIds);
                default:
                    throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Lender {customerCode} does not have a Cenlar loan cleanup file."));
            }
        }

        /// <summary>
        /// Goes through the list of IDs and post-processes each loan.
        /// </summary>
        public void Clean()
        {
            foreach (var loanId in this.loanIds)
            {
                this.DequeueLoan(loanId);
            }
        }

        /// <summary>
        /// Performs lender-specific logic to dequeue and clean up a single loan.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        protected abstract void DequeueLoan(Guid loanId);
    }
}
