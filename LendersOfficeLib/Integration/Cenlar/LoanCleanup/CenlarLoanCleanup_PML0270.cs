﻿namespace LendersOffice.Integration.Cenlar.LoanCleanup
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Constants;

    /// <summary>
    /// Specifies the loan cleanup process for PML0270.
    /// </summary>
    public class CenlarLoanCleanup_PML0270 : CenlarLoanCleanup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CenlarLoanCleanup_PML0270"/> class.
        /// </summary>
        /// <param name="loanIds">The set of loans to clean up.</param>
        public CenlarLoanCleanup_PML0270(IEnumerable<Guid> loanIds)
            : base(loanIds)
        {
            // no-op
        }

        /// <summary>
        /// Removes a loan from the queue for PML0270.
        /// </summary>
        /// <param name="loanId">The loan to dequeue.</param>
        protected override void DequeueLoan(Guid loanId)
        {
            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(CenlarLoanCleanup_PML0270));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // To be picked up in PML0270's custom report for Cenlar boarding, the loan:
            //  * Must have a subservicer loan number (sSubservicerLoanNm)
            //  * Must have a Loan Sold date (sLPurchaseD)
            //  * Investor name in back-end rate lock must be set to a Pingora entity.
            //  * The custom field 54 checkbox is not ticked.
            // To dequeue: Tick the checkbox.
            dataLoan.sCustomField54Bit = true;

            // Record the date the loan was boarded to Cenlar.
            dataLoan.sCustomField54D = CDateTime.Create(DateTime.Now);

            dataLoan.Save();
        }
    }
}
