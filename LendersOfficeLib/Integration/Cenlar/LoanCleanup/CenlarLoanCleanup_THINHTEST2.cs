﻿namespace LendersOffice.Integration.Cenlar.LoanCleanup
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Constants;

    /// <summary>
    /// Specifies the loan cleanup process for THINHTEST2.
    /// </summary>
    public class CenlarLoanCleanup_THINHTEST2 : CenlarLoanCleanup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CenlarLoanCleanup_THINHTEST2"/> class.
        /// </summary>
        /// <param name="loanIds">The set of loans to clean up.</param>
        public CenlarLoanCleanup_THINHTEST2(IEnumerable<Guid> loanIds)
            : base(loanIds)
        {
            // no-op
        }

        /// <summary>
        /// Removes a loan from the queue for THINHTEST2.
        /// </summary>
        /// <param name="loanId">The loan to dequeue.</param>
        protected override void DequeueLoan(Guid loanId)
        {
            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(CenlarLoanCleanup_THINHTEST2));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // To be picked up in THINHTEST2's custom report for Cenlar boarding, the loan:
            //  * Must have the custom field 21 checkbox ticked.
            // To dequeue: untick the checkbox.
            dataLoan.sCustomField21Bit = false;

            // Record the date the loan was boarded to Cenlar.
            dataLoan.sGLServTransEffD = CDateTime.Create(DateTime.Now);

            dataLoan.Save();
        }
    }
}
