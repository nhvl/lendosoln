﻿namespace LendersOffice.Integration.Cenlar
{
    using System;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Conversions.Cenlar;
    using LendersOffice.Integration.Cenlar.LoanCleanup;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using Security;

    /// <summary>
    /// Oversees a Cenlar transaction, generating the request and processing the response.
    /// </summary>
    public partial class CenlarRequestHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CenlarRequestHandler"/> class.
        /// </summary>
        /// <param name="requestData">The request data for this transaction.</param>
        public CenlarRequestHandler(CenlarRequestData requestData)
        {
            this.RequestData = requestData;
            this.Server = new CenlarServer(requestData);

            if (requestData.Action == CenlarActionT.SendLoanFiles)
            {
                this.RequestProvider = new CenlarRequestProvider(requestData);
            }
        }

        /// <summary>
        /// Gets the request data for this transaction.
        /// </summary>
        protected CenlarRequestData RequestData { get; }

        /// <summary>
        /// Gets or sets the request provider responsible for generating the payload.
        /// </summary>
        protected CenlarRequestProvider RequestProvider { get; set; }

        /// <summary>
        /// Gets or sets the server responsible for transmission.
        /// </summary>
        protected CenlarServer Server { get; set; }

        /// <summary>
        /// Gets the response provider responsible for processing the data received from Cenlar.
        /// </summary>
        protected CenlarResponseProvider ResponseProvider { get; private set; }

        /// <summary>
        /// Submits a request to Cenlar.
        /// </summary>
        public void SubmitRequest()
        {
            try
            {
                switch (this.RequestData.Action)
                {
                    case CenlarActionT.RetrieveFileId:
                        throw new DeveloperException(
                            ErrorMessage.SystemError,
                            new SimpleContext("This Cenlar action should not be triggered manually. It happens automatically as part of a loan transmission."));
                    case CenlarActionT.RetrieveResults:
                        throw new DeveloperException(
                            ErrorMessage.SystemError,
                            new SimpleContext("We have not implemented asynchronous file retrieval from Cenlar."));
                    case CenlarActionT.SendLoanFiles:
                        this.TransmitLoans();
                        return;
                    case CenlarActionT.UpdateApiKey:
                        this.RegenerateApiKey();
                        return;
                    default:
                        throw new DeveloperException(
                            ErrorMessage.SystemError,
                            new SimpleContext($"The requested action \"{this.RequestData.Action}\" is not handled"));
                }
            }
            catch (Exception exc) when (exc is DeveloperException || exc is CBaseException || exc is ArgumentException)
            {
                CenlarMailHelper.SendErrorToIseTeam(this.RequestData, exc);
                Tools.LogError(exc);
                throw;
            }
        }

        /// <summary>
        /// Transmits queued loans to Cenlar.
        /// </summary>
        /// <exception cref="DeveloperException">Throws if there was an error with the transmission.</exception>
        private void TransmitLoans()
        {
            if (this.RequestData.LoansForTransmission.Count() == 0)
            {
                Tools.LogInfo($"The Cenlar Transmission Service queue for {this.RequestData.CustomerCode} is empty.");
                return;
            }

            this.AuthenticateToCenlar();

            string payload = this.RequestProvider.SerializeRequest();
            string response = this.Server.PlaceRequest(payload);

            this.ResponseProvider = new CenlarResponseProvider(response, this.RequestData);
            this.ResponseProvider.ParseResponse();

            if (this.ResponseProvider.HasErrors)
            {
                string errorMessage = $"Cenlar returned the following errors for lender {this.RequestData.CustomerCode}: {Environment.NewLine}";
                foreach (string error in this.ResponseProvider.Errors)
                {
                    errorMessage += $"* {error}{Environment.NewLine}";
                }

                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext(errorMessage));
            }

            var responseData = this.ResponseProvider.ResponseData as CenlarResponseData;
            if (responseData.LoanResults.Any())
            {
                CenlarMailHelper.SendTransmissionSummary(this.RequestData, responseData);

                var acceptedLoans = responseData.LoanResults
                    .Where(loan => loan.Status == CenlarLoanStatusT.Accepted)
                    .Select(loan => loan.LoanId);

                if (acceptedLoans.Any())
                {
                    var cleanupObject = CenlarLoanCleanup.Generate(acceptedLoans, this.RequestData.CustomerCode);
                    cleanupObject.Clean();
                }
            }
        }

        /// <summary>
        /// Requests a replacement API key from Cenlar.
        /// </summary>
        /// <exception cref="DeveloperException">Throws if no API Key is contained in the response.</exception>
        private void RegenerateApiKey()
        {
            string response = this.Server.PlaceRequest(payload: null);
            string apiKey = CenlarResponseProvider.ScrapeApiKey(response);

            if (string.IsNullOrEmpty(apiKey))
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Error renewing Cenlar API Key for client {this.RequestData.CustomerCode} ({this.RequestData.BrokerId}). The current key may no longer work. Please investigate."));
            }

            var credentials = this.RequestData.Credentials;
            credentials.ApiKey = apiKey;
            credentials.LastApiKeyReset = DateTime.Now;
            CenlarCredentialSet.Save(credentials);
        }

        /// <summary>
        /// Attempts to authenticate with Cenlar and retrieve a FileID, which
        /// will allow data transfer via the CenBaseAPI for two minutes.
        /// </summary>
        /// <remarks>
        /// This should only ever be called during a loan transmission. It is not used for any
        /// other transaction type.
        /// </remarks>
        /// <exception cref="DeveloperException">Throws if no File ID is contained in the response.</exception>
        private void AuthenticateToCenlar()
        {
            string response = this.Server.Authenticate();
            var fileId = CenlarResponseProvider.ScrapeFileId(response);

            if (string.IsNullOrEmpty(fileId))
            {
                Tools.LogWarning("Cenlar returned this error: \n" + response);
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Error generating a Cenlar File ID for {this.RequestData.CustomerCode} ({this.RequestData.BrokerId}). Unable to continue the transmission."));
            }

            this.RequestData.FileId = fileId;
        }
    }
}
