﻿namespace LendersOffice.Integration.Cenlar
{
    using System;
    using System.Data.SqlClient;
    using DataAccess;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Represents a loan requested for transmission.
    /// </summary>
    public class CenlarLoan
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CenlarLoan"/> class.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="brokerId">The broker ID.</param>
        public CenlarLoan(Guid loanId, Guid brokerId)
        {
            this.LoanId = loanId;
            this.RetrieveLoanIdentifiers(brokerId);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CenlarLoan"/> class.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="loanNumber">The loan number.</param>
        /// <param name="cenlarIdentifier">The Cenlar loan identifier.</param>
        public CenlarLoan(Guid loanId, string loanNumber, string cenlarIdentifier)
        {
            this.LoanId = loanId;
            this.LoanNumber = loanNumber;
            this.CenlarIdentifier = cenlarIdentifier;
        }

        /// <summary>
        /// Gets the loan ID.
        /// </summary>
        public Guid LoanId { get; }

        /// <summary>
        /// Gets the loan number.
        /// </summary>
        public string LoanNumber { get; private set; }

        /// <summary>
        /// Gets the Cenlar loan identifier.
        /// </summary>
        public string CenlarIdentifier { get; private set; }

        /// <summary>
        /// Retrieves the loan number and Cenlar loan identifier using the loan ID.
        /// </summary>
        /// <param name="brokerId">The broker ID.</param>
        private void RetrieveLoanIdentifiers(Guid brokerId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", this.LoanId)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetLoanIdentifiersForSubservicerTransmission", parameters))
            {
                if (reader.Read())
                {
                    this.LoanNumber = (string)reader["sLNm"];
                    this.CenlarIdentifier = (string)reader["sSubservicerLoanNm"];
                }
                else
                {
                    throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Loan {this.LoanId} is missing an identifier."));
                }
            }
        }
    }
}
