﻿namespace LendersOffice.Integration.Cenlar
{
    using System.Collections.Generic;
    using LendersOffice.Integration.Templates;

    /// <summary>
    /// Encapsulates the data received in Cenlar's response.
    /// </summary>
    public class CenlarResponseData : IResponseData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CenlarResponseData"/> class.
        /// </summary>
        /// <param name="response">The raw string response.</param>
        public CenlarResponseData(string response)
        {
            this.RawResponse = response;
            this.LoanResults = new List<CenlarLoanResult>();
        }

        /// <summary>
        /// Gets a collection of summaries for each individual loan transmitted to Cenlar.
        /// </summary>
        public List<CenlarLoanResult> LoanResults { get; }

        /// <summary>
        /// Gets or sets a set of aggregated data for the accepted loans.
        /// </summary>
        public CenlarBoardingSummary AcceptedLoansSummary { get; set; }

        /// <summary>
        /// Gets or sets a set of aggregated data for the rejected loans.
        /// </summary>
        public CenlarBoardingSummary RejectedLoansSummary { get; set; }

        /// <summary>
        /// Gets the raw string response received from Cenlar.
        /// </summary>
        public string RawResponse { get; }
    }
}
