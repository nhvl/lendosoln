﻿using System;
using System.Linq;
using System.Text;
using CommonProjectLib.Common.Lib;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Integration.ActiveDirectory
{
    public static class ActiveDirectoryServiceProvider
    {
        public static bool Authenticate(string sUsername, string sPassword, string sWebServiceURL)
        {
            ActiveDirectoryAuthService.ADAuthService service = new LendersOffice.ActiveDirectoryAuthService.ADAuthService();
            service.Url = sWebServiceURL;
            string sResponse = string.Empty;

            try
            {
                sResponse = service.Authenticate(sUsername, sPassword);
            }
            catch (System.Web.Services.Protocols.SoapException soapExc)
            {
                Tools.LogError(BuildExceptionErrorLog(sWebServiceURL, sUsername), soapExc);
                return false;
            }
            catch (System.Security.Authentication.AuthenticationException authExc)
            {
                Tools.LogError(BuildExceptionErrorLog(sWebServiceURL, sUsername), authExc);
                return false;
            }
            catch (System.Net.WebException webExc)
            {
                Tools.LogError(BuildExceptionErrorLog(sWebServiceURL, sUsername), webExc);
                return false;
            }

            Tuple<bool, string> isAuthenticated_ResultStr = IsAuthenticated(sResponse);
            if(!isAuthenticated_ResultStr.Item1 && !String.IsNullOrEmpty(isAuthenticated_ResultStr.Item2))
            {
                Tools.LogError(BuildErrorLog(false, sWebServiceURL, sUsername, isAuthenticated_ResultStr.Item2));
            }

            return isAuthenticated_ResultStr.Item1;
        }

        private static Tuple<bool, string> IsAuthenticated(string sServiceResponse)
        {
            if (string.IsNullOrEmpty(sServiceResponse))
            {
                return new Tuple<bool, string>(false, ErrorMessages.ActiveDirectoryAuthFailure.EmptyResponse);
            }

            sServiceResponse = sServiceResponse.TrimWhitespaceAndBOM();
            if (sServiceResponse.Equals("true", StringComparison.OrdinalIgnoreCase))
            {
                return new Tuple<bool, string>(true, string.Empty);
            }
            else if (sServiceResponse.Equals("false", StringComparison.OrdinalIgnoreCase))
            {
                return new Tuple<bool, string>(false, string.Empty); // AD was successfully hit and rejected the user
            }
            else
            {
                return new Tuple<bool, string>(false, sServiceResponse); // Some service error occurred
            }
        }

        private static string BuildExceptionErrorLog(string sWebServiceURL, string sUserName)
        {
            return BuildErrorLog(true, sWebServiceURL, sUserName, String.Empty);
        }
        private static string BuildErrorLog(bool bException, string sWebServiceURL, string sUserName, string sErrorMessage)
        {
            StringBuilder sLog = new StringBuilder("=== Active Directory Webservice Error ===");
            sLog.AppendLine();
            sLog.Append("Attempt to authenticate user resulted in ");
            
            string sErrDescriptor = (bException) ? "exception." : "error.";
            sLog.Append(sErrDescriptor);
            sLog.AppendLine();

            sLog.AppendLine("Webservice URL:");
            sLog.AppendLine(sWebServiceURL);

            sLog.AppendLine("LQB User:");
            sLog.AppendLine(sUserName);

            if (!String.IsNullOrEmpty(sErrorMessage))
            {
                sLog.AppendLine("Error Message:");
                sLog.AppendLine(sErrorMessage);
            }

            return sLog.ToString();
        }
    }
}
