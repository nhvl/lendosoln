﻿using System;
using System.Linq;
using System.Text;
using LendersOffice.Admin;

namespace LendersOffice.Integration.ActiveDirectory
{
    public class ActiveDirectoryHelper
    {
        #region Variables and Accessors
        private Guid m_BrokerId = Guid.Empty;
        private BrokerDB m_BrokerDb = null;

        public string ADWebServiceURL
        {
            get { return BrokerInfo.ActiveDirectoryURL; }
        }

        private BrokerDB BrokerInfo
        {
            get
            {
                if (m_BrokerDb == null)
                {
                    m_BrokerDb = BrokerDB.RetrieveById(m_BrokerId);
                }
                return m_BrokerDb;
            }
        }
        #endregion

        public ActiveDirectoryHelper(Guid BrokerId)
        {
            m_BrokerId = BrokerId;
        }

        public bool IsLenderADAccountSetupComplete()
        {
            return BrokerInfo.IsActiveDirectoryAuthenticationEnabled && !String.IsNullOrEmpty(BrokerInfo.ActiveDirectoryURL);
        }

        public bool Authenticate(string sUsername, string sPassword)
        {
            return ActiveDirectoryServiceProvider.Authenticate(sUsername, sPassword, ADWebServiceURL);
        }

        public bool IsLQBUserAnActiveDirectoryUser(Guid userID)
        {
            EmployeeDB emp = EmployeeDB.RetrieveByUserId(m_BrokerId, userID);
            return emp.IsActiveDirectoryUser;
        }
    }
}
