﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using Common;
    using Serialization;

    /// <summary>
    /// Class holding envelope update information.
    /// </summary>
    public class EnvelopeUpdateSummary
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnvelopeUpdateSummary"/> class.
        /// </summary>
        /// <param name="status">The status string.</param>
        /// <param name="createdDateTime">The created date time.</param>
        /// <param name="deletedDateTime">The deleted date time.</param>
        /// <param name="sentDateTime">The sent date time.</param>
        /// <param name="completedDateTime">The completed date time.</param>
        /// <param name="declinedDateTime">The declined date time.</param>
        /// <param name="deliveredDateTime">The delivered date time.</param>
        /// <param name="voidedDateTime">The voided date time.</param>
        /// <param name="voidedReason">The voided reason.</param>
        /// <param name="recipients">The recipients of the envelope.</param>
        /// <param name="documents">The documents returned.</param>
        public EnvelopeUpdateSummary(
            DocuSignEnvelopeStatus status,
            DateTime? createdDateTime,
            DateTime? deletedDateTime,
            DateTime? sentDateTime,
            DateTime? completedDateTime,
            DateTime? declinedDateTime,
            DateTime? deliveredDateTime,
            DateTime? voidedDateTime,
            string voidedReason,
            EnvelopeRecipientsUpdateSummary recipients,
            EnvelopeDocumentListUpdateSummary documents)
        {
            this.Status = status;
            this.CreatedDateTime = createdDateTime;
            this.DeletedDateTime = deletedDateTime;
            this.SentDateTime = sentDateTime;
            this.CompletedDateTime = completedDateTime;
            this.DeclinedDateTime = declinedDateTime;
            this.DeliveredDateTime = deliveredDateTime;
            this.VoidedDateTime = voidedDateTime;
            this.VoidedReason = voidedReason;
            this.Recipients = recipients;
            this.Documents = documents;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnvelopeUpdateSummary"/> class.
        /// </summary>
        /// <param name="docusignEnvelope">The deserialized envelope get response.</param>
        public EnvelopeUpdateSummary(EnvelopeGet docusignEnvelope)
        {
            this.Status = DocuSignEnvelope.ParseStatus(docusignEnvelope.status);
            this.CreatedDateTime = docusignEnvelope.createdDateTime?.ToNullable<DateTime>(DateTime.TryParse);
            this.DeclinedDateTime = docusignEnvelope.declinedDateTime?.ToNullable<DateTime>(DateTime.TryParse);
            this.DeletedDateTime = docusignEnvelope.deletedDateTime?.ToNullable<DateTime>(DateTime.TryParse);
            this.DeliveredDateTime = docusignEnvelope.deliveredDateTime?.ToNullable<DateTime>(DateTime.TryParse); 
            this.SentDateTime = docusignEnvelope.sentDateTime?.ToNullable<DateTime>(DateTime.TryParse);
            this.VoidedDateTime = docusignEnvelope.voidedDateTime?.ToNullable<DateTime>(DateTime.TryParse);
            this.VoidedReason = docusignEnvelope.voidedReason;
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        public DocuSignEnvelopeStatus Status { get; private set; }

        /// <summary>
        /// Gets the created date time.
        /// </summary>
        public DateTime? CreatedDateTime { get; private set; }

        /// <summary>
        /// Gets the deleted date time.
        /// </summary>
        public DateTime? DeletedDateTime { get; private set; }

        /// <summary>
        /// Gets the sent date time.
        /// </summary>
        public DateTime? SentDateTime { get; private set; }

        /// <summary>
        /// Gets the completed date time.
        /// </summary>
        public DateTime? CompletedDateTime { get; private set; }

        /// <summary>
        /// Gets the declined date time.
        /// </summary>
        public DateTime? DeclinedDateTime { get; private set; }

        /// <summary>
        /// Gets the delivered date time.
        /// </summary>
        public DateTime? DeliveredDateTime { get; private set; }

        /// <summary>
        /// Gets the voided date time.
        /// </summary>
        public DateTime? VoidedDateTime { get; private set; }

        /// <summary>
        /// Gets the voided reason.
        /// </summary>
        public string VoidedReason { get; private set; }

        /// <summary>
        /// Gets or sets the recipients of the envelope.
        /// </summary>
        /// <value>The recipients of the envelope.</value>
        public EnvelopeRecipientsUpdateSummary Recipients { get; set; }

        /// <summary>
        /// Gets or sets the documents returned.
        /// </summary>
        public EnvelopeDocumentListUpdateSummary Documents { get; set; }
    }
}
