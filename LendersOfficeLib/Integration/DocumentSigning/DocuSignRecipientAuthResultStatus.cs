﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System.ComponentModel;
    
    /// <summary>
    /// Status after the user attempted to authenticate to sign a document.
    /// </summary>
    public enum DocuSignRecipientAuthResultStatus
    {
        /// <summary>
        /// Passed authentication.
        /// </summary>
        [Description("Passed")]
        Passed = 0,

        /// <summary>
        /// Failed authentication.
        /// </summary>
        [Description("Failed")]
        Failed = 1,

        /// <summary>
        /// Some other status that we don't know about.
        /// </summary>
        [Description("Other")]
        Other = 2,

        /// <summary>
        /// No status.
        /// </summary>
        [Description("N/A")]
        NA = 3
    }
}
