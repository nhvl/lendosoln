﻿namespace LendersOffice.Integration.DocumentSigning
{
    // See note in C:\LendOSoln\LendersOfficeLib\ObjLib\Security\ClientCertificateUtilities.cs
    // Essentially, iTextSharp contains a separate version of BouncyCastle, and this is the only way to resolve the reference to the correct DLL.
    extern alias BouncyCastle_v1_8;

    using System;
    using System.Collections.Generic;
    using System.IdentityModel.Tokens;
    using System.Linq;
    using System.Security.Claims;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using BouncyCastle = BouncyCastle_v1_8::Org.BouncyCastle;

    /// <summary>
    /// Provides access to the OAuth2 authentication flow for DocuSign.
    /// </summary>
    internal class DocuSignOAuthServiceAdapter
    {
        /// <summary>
        /// Generates the URL to the consent page in DocuSign's system.
        /// </summary>
        /// <param name="useSandbox">A boolean value indicating whether to use DocuSign's sandbox environment.</param>
        /// <param name="integratorKey">The integrator key to receive consent.</param>
        /// <param name="stateParameter">The state parameter to pass to DocuSign.</param>
        /// <param name="redirectPath">The URL to receive DocuSign's redirect of the user's browser after the consent event is completed.</param>
        /// <returns>A URL to the consent page.</returns>
        public static LqbAbsoluteUri CreateConsentAuthenticationUrl(bool useSandbox, string integratorKey, string stateParameter, LqbAbsoluteUri redirectPath)
        {
            string fullUrl = GetBaseUrl(useSandbox) + "/oauth/auth?response_type=code&scope=signature%20impersonation"
                + "&client_id=" + DocuSignAdapter.EscapeQueryStringValue(integratorKey)
                + "&state=" + DocuSignAdapter.EscapeQueryStringValue(stateParameter)
                + "&redirect_uri=" + DocuSignAdapter.EscapeQueryStringValue(redirectPath.ToString());
            return LqbAbsoluteUri.Create(fullUrl).ForceValue();
        }

        /// <summary>
        /// Gets the user info from the consent code.  This is the second stage of the OAuth consent process.
        /// </summary>
        /// <param name="useSandbox">A boolean value indicating whether to use DocuSign's sandbox environment.</param>
        /// <param name="code">The consent code from DocuSign.</param>
        /// <param name="integratorKey">The integrator key that has received consent.</param>
        /// <param name="secretKey">The secret key for this integrator key, which is used only to load the user data in this interaction.</param>
        /// <returns>User information about the consenting user, or an error result.</returns>
        public Result<DocuSignUserInfo> GetUserInfoFromCode(bool useSandbox, string code, string integratorKey, string secretKey)
        {
            Result<KeyValuePair<string, string>> authHeaderResult = this.GetHeaderTokenFromCode(useSandbox, code, integratorKey, secretKey);
            if (authHeaderResult.HasError)
            {
                return Result<DocuSignUserInfo>.Failure(authHeaderResult.Error);
            }

            return this.GetUserInfo(useSandbox, authHeaderResult.Value);
        }

        /// <summary>
        /// Gets the authentication header to use for subsequent web requests as well as user and account information.
        /// </summary>
        /// <param name="loginData">The authentication data to use.</param>
        /// <returns>The authentication header and user info, or an error result.</returns>
        public Result<Tuple<KeyValuePair<string, string>, DocuSignUserInfo>> GetAuthHeaderAndUserInfo(DocuSignAuthenticationData loginData)
        {
            Result<KeyValuePair<string, string>> authHeaderResult = this.GetHeaderToken(loginData);
            if (authHeaderResult.HasError)
            {
                return Result<Tuple<KeyValuePair<string, string>, DocuSignUserInfo>>.Failure(authHeaderResult.Error);
            }

            Result<DocuSignUserInfo> userInfoResult = this.GetUserInfo(loginData.UseSandbox, authHeaderResult.Value);
            if (userInfoResult.HasError)
            {
                return Result<Tuple<KeyValuePair<string, string>, DocuSignUserInfo>>.Failure(userInfoResult.Error);
            }

            return Result<Tuple<KeyValuePair<string, string>, DocuSignUserInfo>>.Success(Tuple.Create(
                authHeaderResult.Value,
                userInfoResult.Value));
        }

        /// <summary>
        /// Creates the string representation of a JSON Web Token for the authentication data.
        /// </summary>
        /// <remarks>
        /// Adapted from the C# SDK <a href="https://github.com/docusign/docusign-csharp-client/blob/ed571c897136a00426faeacf6127b0d19b9c7088/sdk/src/DocuSign.eSign/Client/ApiClient.cs#L557">implementation</a>.
        /// </remarks>
        /// <param name="loginData">The data to use for authentication with DocuSign.</param>
        /// <returns>A completed JWT representing <paramref name="loginData"/>.</returns>
        private static string CreateJsonWebToken(DocuSignAuthenticationData loginData)
        {
            // Per DocuSign, this value is clipped at 1 hour. Shorter times are honored, but expiration times longer than 1 hour are clipped to 1 hour.
            // https://docs.docusign.com/esign/guide/authentication/jwt_format.html#jwt-body
            int expiresInMinutes = 30;
            DateTime issuedTime = DateTime.UtcNow;
            var descriptor = new SecurityTokenDescriptor()
            {
                Lifetime = new System.IdentityModel.Protocols.WSTrust.Lifetime(issuedTime, issuedTime.AddMinutes(expiresInMinutes)),
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim("iss", loginData.IntegratorKey), // Issuer
                    new Claim("sub", loginData.UserId), // Subject
                    new Claim("aud", GetHostName(loginData.UseSandbox)), // Audience
                    new Claim("scope", "signature impersonation"),
                }),
                SigningCredentials = new SigningCredentials(
                    new RsaSecurityKey(CreateRsaKeyFromPem(loginData.RsaPrivateKey)),
                    SecurityAlgorithms.RsaSha256Signature,
                    SecurityAlgorithms.HmacSha256Signature),
            };

            var handler = new JwtSecurityTokenHandler();
            return handler.WriteToken(handler.CreateToken(descriptor));
        }

        /// <summary>
        /// Creates an RSA key from the text content of a PEM file.
        /// </summary>
        /// <remarks>
        /// Copied from the C# SDK <a href="https://github.com/docusign/docusign-csharp-client/blob/ed571c897136a00426faeacf6127b0d19b9c7088/sdk/src/DocuSign.eSign/Client/ApiClient.cs#L622">implementation</a>.
        /// </remarks>
        /// <param name="privateKey">The text content of a PEM file, which contains the base64-encoded data of the private key.</param>
        /// <returns>The RSA key instance for the private key.</returns>
        private static System.Security.Cryptography.RSA CreateRsaKeyFromPem(string privateKey)
        {
            var pemReader = new BouncyCastle.OpenSsl.PemReader(new System.IO.StringReader(privateKey));

            object result = pemReader.ReadObject();
            if (result is BouncyCastle.Crypto.AsymmetricCipherKeyPair)
            {
                var keyPair = (BouncyCastle.Crypto.AsymmetricCipherKeyPair)result;
                return BouncyCastle.Security.DotNetUtilities.ToRSA((BouncyCastle.Crypto.Parameters.RsaPrivateCrtKeyParameters)keyPair.Private);
            }
            else if (result is BouncyCastle.Crypto.Parameters.RsaKeyParameters)
            {
                var keyParameters = (BouncyCastle.Crypto.Parameters.RsaKeyParameters)result;
                return BouncyCastle.Security.DotNetUtilities.ToRSA(keyParameters);
            }

            throw new LqbGrammar.Exceptions.DeveloperException(ErrorMessage.SystemError, new LqbGrammar.Exceptions.SimpleContext("Unexpected PEM type"));
        }

        /// <summary>
        /// Gets the base URL to use to authenticate with DocuSign via the <c>/oauth/*</c> endpoints.
        /// </summary>
        /// <param name="useSandbox">A boolean value indicating whether to use DocuSign's sandbox environment.</param>
        /// <returns>The scheme and host of the URL.</returns>
        private static string GetBaseUrl(bool useSandbox)
        {
            return "https://" + GetHostName(useSandbox);
        }

        /// <summary>
        /// Gets the host name to use to authenticate with DocuSign via the <c>/oauth/*</c> endpoints.
        /// </summary>
        /// <param name="useSandbox">A boolean value indicating whether to use DocuSign's sandbox environment.</param>
        /// <returns>The host of the URL.</returns>
        private static string GetHostName(bool useSandbox)
        {
            return useSandbox ? "account-d.docusign.com" : "account.docusign.com";
        }

        /// <summary>
        /// Retrieves the user information from DocuSign's <c>/oauth/userinfo</c> endpoint.
        /// </summary>
        /// <param name="useSandbox">A boolean value indicating whether to use DocuSign's sandbox environment.</param>
        /// <param name="authenticationHeader">The authentication header to use for this interaction.</param>
        /// <returns>The user data.</returns>
        private Result<DocuSignUserInfo> GetUserInfo(bool useSandbox, KeyValuePair<string, string> authenticationHeader)
        {
            LqbAbsoluteUri url = LqbAbsoluteUri.Create(GetBaseUrl(useSandbox) + "/oauth/userinfo").ForceValue();

            var options = new LqbGrammar.Drivers.HttpRequest.HttpRequestOptions();
            options.Method = HttpMethod.Get;
            options.AddHeader(authenticationHeader.Key, authenticationHeader.Value);

            Result<string> userInfoResult = DocuSignAdapter.CommunicateForString(url, options, DocuSignLoggingOptions.NoLogging);
            if (userInfoResult.HasError)
            {
                DocuSignAdapter.Log(options.Method, url, "[Redacted for security reasons.]", "Please see error response in other logs", isResponseError: true);
                return Result<DocuSignUserInfo>.Failure(userInfoResult.Error);
            }

            DocuSignAdapter.Log(options.Method, url, "[Redacted for security reasons.]", userInfoResult.Value, isResponseError: false);

            UserInfoResult userInfoData = SerializationHelper.JsonNetDeserialize<UserInfoResult>(userInfoResult.Value);
            UserInfoResultAccount defaultAccountData = userInfoData.accounts.FirstOrDefault(a => a.is_default);
            if (defaultAccountData == null)
            {
                return Result<DocuSignUserInfo>.Failure(DataAccess.CBaseException.GenericException("Unable to find default account for user"));
            }
            else if (defaultAccountData.account_id != DocuSignAdapter.EscapeQueryStringValue(defaultAccountData.account_id))
            {
                return Result<DocuSignUserInfo>.Failure(DataAccess.CBaseException.GenericException("Invalid or unexpected account id")); // expecting int-ish or guid
            }

            return Result<DocuSignUserInfo>.Success(new DocuSignUserInfo(
                userId: userInfoData.sub,
                defaultAccountId: defaultAccountData.account_id,
                baseUrl: LqbAbsoluteUri.Create(defaultAccountData.base_uri + "/restapi").ForceValue(),
                name: userInfoData.name,
                email: userInfoData.email));
        }

        /// <summary>
        /// Gets the authentication header by creating a token with DocuSign's <c>/oauth/token</c> endpoint.
        /// </summary>
        /// <param name="loginData">The login information to use to create a JSON Web Token.</param>
        /// <returns>An authentication header, or an error result.</returns>
        private Result<KeyValuePair<string, string>> GetHeaderToken(DocuSignAuthenticationData loginData)
        {
            LqbAbsoluteUri url = LqbAbsoluteUri.Create(GetBaseUrl(loginData.UseSandbox) + "/oauth/token").ForceValue();

            var options = new LqbGrammar.Drivers.HttpRequest.HttpRequestOptions();
            options.Method = HttpMethod.Post;
            options.MimeType = MimeType.Application.UrlEncoded;
            string jwt = CreateJsonWebToken(loginData);
            options.PostData = new StringContent("grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer&assertion=" + DocuSignAdapter.EscapeQueryStringValue(jwt));

            Result<string> tokenResult = DocuSignAdapter.CommunicateForString(url, options, DocuSignLoggingOptions.OnlyLogUrls);
            if (tokenResult.HasError)
            {
                return Result<KeyValuePair<string, string>>.Failure(tokenResult.Error);
            }

            Dictionary<string, string> data = SerializationHelper.JsonNetDeserialize<Dictionary<string, string>>(tokenResult.Value);
            return Result<KeyValuePair<string, string>>.Success(new KeyValuePair<string, string>("Authorization", data["token_type"] + " " + data["access_token"]));
        }

        /// <summary>
        /// Gets the authentication header by creating a token with DocuSign's <c>/oauth/token</c> endpoint.
        /// </summary>
        /// <param name="useSandbox">A boolean value indicating whether to use DocuSign's sandbox environment.</param>
        /// <param name="code">The consent code from DocuSign.</param>
        /// <param name="integratorKey">The integrator key that has received consent.</param>
        /// <param name="secretKey">The secret key for this integrator key, which is used only to load the user data in this interaction.</param>
        /// <returns>An authentication header, or an error result.</returns>
        private Result<KeyValuePair<string, string>> GetHeaderTokenFromCode(bool useSandbox, string code, string integratorKey, string secretKey)
        {
            LqbAbsoluteUri url = LqbAbsoluteUri.Create(GetBaseUrl(useSandbox) + "/oauth/token").ForceValue();
            var options = new LqbGrammar.Drivers.HttpRequest.HttpRequestOptions();
            options.Method = HttpMethod.Post;
            options.MimeType = MimeType.Application.UrlEncoded;
            options.AddHeader("Authorization", "Basic " + Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(integratorKey + ':' + secretKey)));
            options.PostData = new StringContent("grant_type=authorization_code&code=" + DocuSignAdapter.EscapeQueryStringValue(code));

            Result<string> tokenResult = DocuSignAdapter.CommunicateForString(url, options, DocuSignLoggingOptions.OnlyLogUrls);
            if (tokenResult.HasError)
            {
                return Result<KeyValuePair<string, string>>.Failure(tokenResult.Error);
            }

            Dictionary<string, string> tokenData = SerializationHelper.JsonNetDeserialize<Dictionary<string, string>>(tokenResult.Value);
            return Result<KeyValuePair<string, string>>.Success(new KeyValuePair<string, string>("Authorization", tokenData["token_type"] + " " + tokenData["access_token"]));
        }

        /// <summary>
        /// The JSON response format of GET requests to DocuSign's authentication service <c>/oauth/userinfo</c> endpoint.
        /// </summary>
        /// <remarks>
        /// Unlike many of their other web services, they have not provided explicit documentation for the returned data.
        /// The best I've been able to find is "A typical response looks like this:" <a href="https://docs.docusign.com/esign/guide/authentication/userinfo.html">/oauth/userinfo docs</a>.
        /// I've also found "the response will look something like this:" <a href="https://docs.docusign.com/esign/guide/authentication/oa2_jwt.html#obtaining-the-base-uri">JWT docs</a>.
        /// </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "*", Justification = "DocuSign chose not to document the return values of this webservice; it was built from a sample JSON.")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "*", Justification = "DocuSign JSON response format, strictly used for deserialization.")]
        private class UserInfoResult
        {
            public UserInfoResultAccount[] accounts { get; set; }

            public string name { get; set; }

            public string email { get; set; }

            public string sub { get; set; }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "*", Justification = "DocuSign chose not to document the return values of this webservice; it was built from a sample JSON.")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "*", Justification = "DocuSign JSON response format, strictly used for deserialization.")]
        private class UserInfoResultAccount
        {
            public string account_id { get; set; }

            public bool is_default { get; set; }

            public string account_name { get; set; }

            public string base_uri { get; set; }
        }
    }
}
