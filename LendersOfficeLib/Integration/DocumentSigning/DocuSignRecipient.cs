﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using Admin.DocuSign;
    using Common;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Contains information about the recipients associated with a DocuSign envelope.
    /// </summary>
    public class DocuSignRecipient
    {
        /// <summary>
        /// Stored procedure for creating a recipient.
        /// </summary>
        private static readonly StoredProcedureName DocuSignRecipientsCreate = StoredProcedureName.Create("DOCUSIGN_RECIPIENTS_Create").Value;

        /// <summary>
        /// Stored procedure for updating a recipient.
        /// </summary>
        private static readonly StoredProcedureName DocuSignRecipientsUpdate = StoredProcedureName.Create("DOCUSIGN_RECIPIENTS_Update").Value;

        /// <summary>
        /// Stored procedure for deleting a recipient.
        /// </summary>
        private static readonly StoredProcedureName DocuSignRecipientsDelete = StoredProcedureName.Create("DOCUSIGN_RECIPIENTS_Delete").Value;

        /// <summary>
        /// Stored procedure for deleting the auth results associated with a recipient.
        /// </summary>
        private static readonly StoredProcedureName DocuSignRecipientsAuthResultsDeleteByRecipient = StoredProcedureName.Create("DOCUSIGN_RECIPIENTS_AUTHRESULTS_DeleteByRecipient").Value;

        /// <summary>
        /// The statuses that have associated tracking dates.
        /// </summary>
        private static readonly IReadOnlyDictionary<DocuSignRecipientStatus, string> DbTrackedStatuses = new Dictionary<DocuSignRecipientStatus, string>()
        {
            { DocuSignRecipientStatus.Declined, "DeclinedDate" },
            { DocuSignRecipientStatus.Delivered, "DeliveredDate" },
            { DocuSignRecipientStatus.Sent, "SentDate" },
            { DocuSignRecipientStatus.Signed, "SignedDate" }
        };

        /// <summary>
        /// Maps the status as a string to our enum.
        /// </summary>
        /// <remarks>
        /// From https://www.docusign.com/p/RESTAPIGuide/Content/Introduction+Changes/Envelope%20and%20Recipient%20Status%20Codes.htm.
        /// </remarks>
        private static readonly IReadOnlyDictionary<string, DocuSignRecipientStatus> StatusStringToEnum = new Dictionary<string, DocuSignRecipientStatus>(StringComparer.OrdinalIgnoreCase)
        {
            { "created", DocuSignRecipientStatus.Created },
            { "sent", DocuSignRecipientStatus.Sent },
            { "delivered", DocuSignRecipientStatus.Delivered },
            { "signed", DocuSignRecipientStatus.Signed },
            { "declined", DocuSignRecipientStatus.Declined },
            { "completed", DocuSignRecipientStatus.Completed },
            { "faxpending", DocuSignRecipientStatus.FaxPending },
            { "autoresponded", DocuSignRecipientStatus.AutoResponded }
        };

        /// <summary>
        /// Whether this record has not yet been saved to the DB.
        /// </summary>
        private bool isNew;

        /// <summary>
        /// The statuses mapped to the dates that tstatus was reached.
        /// </summary>
        private Dictionary<DocuSignRecipientStatus, DateTime?> statuses = null;

        /// <summary>
        /// The results after a recipient has attempted to authenticate to get into a document.
        /// </summary>
        private Dictionary<DocuSignRecipientAuthResultType, DocuSignRecipientAuthResult> authResults = null;

        /// <summary>
        /// Prevents a default instance of the <see cref="DocuSignRecipient"/> class from being created.
        /// </summary>
        private DocuSignRecipient()
        {
            this.isNew = true;
            this.Id = -1;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DocuSignRecipient"/> class.
        /// </summary>
        /// <param name="record">The record to load from.</param>
        private DocuSignRecipient(IDataRecord record)
        {
            this.Id = (int)record["Id"];
            this.EnvelopeReferenceId = (int)record["EnvelopeReferenceId"];
            this.SigningOrder = (int)record["SigningOrder"];
            this.Type = (RecipientType)record["Type"];
            this.Name = (string)record["Name"];
            this.Email = record.AsNullableString("Email");
            this.MfaOption = (MfaOptions)record["MfaOption"];
            this.CurrentStatus = record.GetNullableIntEnum<DocuSignRecipientStatus>("CurrentStatus");
            this.DeclinedReason = record.AsNullableString("DeclinedReason");
            this.AutoRespondedReason = record.AsNullableString("AutoRespondedReason");
            this.RecipientIdGuid = record.AsNullableString("RecipientIdGuid");
            this.CustomRecipientId = (int)record["CustomRecipientId"];
            this.AccessCode = record.AsNullableString("AccessCode");
            this.ProvidedPhoneNumber = record.AsNullableString("ProvidedPhoneNumber");
            this.ClientUserId = record.AsNullableGuid("ClientUserId")?.ToString();

            this.statuses = new Dictionary<DocuSignRecipientStatus, DateTime?>();
            foreach (var status in DbTrackedStatuses)
            {
                if (record[status.Value] != DBNull.Value)
                {
                    this.statuses[status.Key] = (DateTime)record[status.Value];
                }
            }
        }

        /// <summary>
        /// Gets the DB id of the recipient.
        /// </summary>
        /// <value>The DB id of the recipient.</value>
        public int Id
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the id of the associated envelope.
        /// </summary>
        /// <value>The id of the associated envelope.</value>
        public int EnvelopeReferenceId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the signing order of the recipient.
        /// </summary>
        /// <value>The signing order of the recipient.</value>
        public int SigningOrder
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the recipient type.
        /// </summary>
        /// <value>The recipient type.</value>
        public RecipientType Type
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the name of the recipient.
        /// </summary>
        /// <value>The name of the recipient.</value>
        public string Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the email of the recipient.
        /// </summary>
        /// <value>The email of the recipient.</value>
        public string Email
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the MFA option chosen for the recipient.
        /// </summary>
        /// <value>The MFA option chosen for the recipient.</value>
        public MfaOptions MfaOption
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the current status of the recipient.
        /// </summary>
        public DocuSignRecipientStatus? CurrentStatus
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a dictionary of recipient statuses mapping to the date that the recipient reached that status.
        /// Note that not all statuses will have a corresponding date.
        /// </summary>
        /// <value>Dictionary of statuses to status dates.</value>
        public IReadOnlyDictionary<DocuSignRecipientStatus, DateTime?> StatusDates
        {
            get
            {
                return this.statuses;
            }
        }

        /// <summary>
        /// Gets the reason why a recipient has declined signing a document.
        /// </summary>
        /// <value>The reason why a recipient has declined signing a document.</value>
        public string DeclinedReason
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the auto response sent back by a recipients email client.
        /// </summary>
        /// <value>The auto response sent back by a recipients email client.</value>
        public string AutoRespondedReason
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the recipient id as determined by DocuSign.
        /// </summary>
        /// <value>The recipient id as determined by DocuSign.</value>
        /// <remarks>This is determined by DocuSign.</remarks>
        public string RecipientIdGuid
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the client user id that we sent to DocuSign.
        /// </summary>
        /// <value>The recipient id sent to DocuSign.</value>
        public string ClientUserId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the integer recipient id that we sent over to DocuSign.
        /// </summary>
        /// <remarks>DocuSign echoes this back to us.</remarks>
        public int CustomRecipientId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the access code for the recipient, if specified.
        /// </summary>
        /// <value>The access code for the recipient, if specified.</value>
        public string AccessCode
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the phone number provided for either Phone or SMS authentication.
        /// </summary>
        /// <value>The phone number provided for either Phone or SMS authentication.</value>
        public string ProvidedPhoneNumber
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the results after a user has attempted to authenticate to get into a document.
        /// </summary>
        /// <value>The results after a user has attempted to authenticate to get into a document.</value>
        public IReadOnlyDictionary<DocuSignRecipientAuthResultType, DocuSignRecipientAuthResult> AuthResults
        {
            get
            {
                return this.authResults;
            }
        }

        /// <summary>
        /// Parses the status string to a DocuSignRecipientStatus.
        /// </summary>
        /// <param name="statusString">The string to parse.</param>
        /// <returns>The status. Will be <see cref="DocuSignRecipientStatus.Other"/> if not recognized.</returns>
        public static DocuSignRecipientStatus ParseStatus(string statusString)
        {
            if (string.IsNullOrEmpty(statusString))
            {
                return DocuSignRecipientStatus.NA;
            }

            DocuSignRecipientStatus status;
            return statusString.TryParseDefine<DocuSignRecipientStatus>(out status, ignoreCase: true) || StatusStringToEnum.TryGetValue(statusString, out status) ? status : DocuSignRecipientStatus.Other;
        }

        /// <summary>
        /// Loads the recipients and buckets them using the envelope id.
        /// </summary>
        /// <param name="reader">The reader to load from.</param>
        /// <param name="recipientIdToResults">The auth results obtained.</param>
        /// <returns>Dictionary mapping envelope reference id to recipients.</returns>
        internal static Dictionary<int, List<DocuSignRecipient>> Load(IDataReader reader, Dictionary<int, Dictionary<DocuSignRecipientAuthResultType, DocuSignRecipientAuthResult>> recipientIdToResults)
        {
            Dictionary<int, List<DocuSignRecipient>> recipients = new Dictionary<int, List<DocuSignRecipient>>();
            while (reader.Read())
            {
                var recipient = new DocuSignRecipient(reader);
                recipient.authResults = null;
                if (recipientIdToResults.ContainsKey(recipient.Id))
                {
                    recipient.authResults = recipientIdToResults[recipient.Id];
                }

                if (!recipients.ContainsKey(recipient.EnvelopeReferenceId))
                {
                    recipients[recipient.EnvelopeReferenceId] = new List<DocuSignRecipient>();
                }

                recipients[recipient.EnvelopeReferenceId].Add(recipient);
            }

            return recipients;
        }

        /// <summary>
        /// Creates the initial recipient records for an envelope.
        /// </summary>
        /// <param name="request">The envelope request containing the recipients.</param>
        /// <param name="summary">The creation summary.</param>
        /// <returns>The list of recipient records.</returns>
        internal static List<DocuSignRecipient> Create(EnvelopeRequest request, EnvelopeCreateSummary summary)
        {
            List<DocuSignRecipient> recipientRecords = new List<DocuSignRecipient>();
            var recipients = request.Recipients;                
            foreach (var recipient in recipients)
            {
                DocuSignRecipient record = new DocuSignRecipient();
                record.Email = recipient.Email;
                record.Name = recipient.Name;
                record.SigningOrder = recipient.SigningPosition;
                record.Type = recipient.Type;
                record.CustomRecipientId = summary.CustomRecipientIdsUsed[recipient.Identifier];
                record.AccessCode = recipient.Authentication?.AccessCode ?? "12345";
                record.ProvidedPhoneNumber = recipient.Authentication?.PhoneNumber ?? "1234567890";
                record.MfaOption = recipient.Authentication?.InitialMfaOption ?? MfaOptions.AccessCode;
                record.ClientUserId = recipient.Identifier.ToString();

                recipientRecords.Add(record);
            }

            return recipientRecords;
        }

        /// <summary>
        /// Creates a new recipient from the GET response.
        /// </summary>
        /// <param name="summary">The GET response.</param>
        /// <returns>The new recipient.</returns>
        internal static DocuSignRecipient Create(EnvelopeRecipientUpdateSummary summary)
        {
            var recipient = new DocuSignRecipient();
            recipient.Email = summary.Email;
            recipient.Name = summary.Name;
            recipient.Type = summary.Type;
            recipient.AccessCode = summary.AccessCode ?? recipient.AccessCode;
            recipient.CustomRecipientId = summary.CustomRecipientId;

            // This is an LQB data point so we don't really have a way of filling it out if a
            // recipient is created like this.
            recipient.MfaOption = MfaOptions.NoSelection;

            recipient.Update(summary);

            return recipient;
        }

        /// <summary>
        /// Updates a recipient using the GET response.
        /// </summary>
        /// <param name="summary">The GET response.</param>
        internal void Update(EnvelopeRecipientUpdateSummary summary)
        {
            this.CurrentStatus = summary.Status;
            this.SigningOrder = summary.RoutingOrder;
            this.RecipientIdGuid = summary.RecipientIdGuid;
            this.DeclinedReason = summary.DeclinedReason;
            this.AccessCode = summary.AccessCode ?? this.AccessCode;

            if (this.statuses == null)
            {
                this.statuses = new Dictionary<DocuSignRecipientStatus, DateTime?>();
            }

            this.statuses[DocuSignRecipientStatus.Sent] = summary.SentDateTime ?? this.StatusDates.GetValueOrNull(DocuSignRecipientStatus.Sent);
            this.statuses[DocuSignRecipientStatus.Delivered] = summary.DeliveredDateTime ?? this.StatusDates.GetValueOrNull(DocuSignRecipientStatus.Delivered);
            this.statuses[DocuSignRecipientStatus.Signed] = summary.SignedDateTime ?? this.StatusDates.GetValueOrNull(DocuSignRecipientStatus.Signed);
            this.statuses[DocuSignRecipientStatus.Declined] = summary.DeclinedDateTime ?? this.StatusDates.GetValueOrNull(DocuSignRecipientStatus.Declined);

            this.UpdateAuthResults(summary.RecipientAuthenticationStatus);
        }

        /// <summary>
        /// Saves the recipient and its child objects to the DB.
        /// </summary>
        /// <param name="envelopeReferenceId">The parent envelope id.</param>
        /// <param name="connection">The connection.</param>
        /// <param name="transaction">The transaction.</param>
        internal void Save(int envelopeReferenceId, DbConnection connection, DbTransaction transaction)
        {
            this.SaveRecipient(envelopeReferenceId, connection, transaction);
            if (this.authResults != null)
            {
                foreach (var authResult in this.authResults)
                {
                    authResult.Value.Save(this.Id, this.EnvelopeReferenceId, connection, transaction);
                }
            }
        }

        /// <summary>
        /// Deletes the recipient.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="transaction">The transaction.</param>
        internal void Delete(DbConnection connection, DbTransaction transaction)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@EnvelopeReferenceId", this.EnvelopeReferenceId),
                new SqlParameter("@RecipientReferenceId", this.Id)
            };

            // Delete the auth results first.
            StoredProcedureDriverHelper.ExecuteNonQuery(connection, transaction, DocuSignRecipientsAuthResultsDeleteByRecipient, parameters, TimeoutInSeconds.Thirty);

            // Then the recipient itself.
            parameters = new SqlParameter[]
            {
                new SqlParameter("@EnvelopeReferenceId", this.EnvelopeReferenceId),
                new SqlParameter("@Id", this.Id)
            };

            StoredProcedureDriverHelper.ExecuteNonQuery(connection, transaction, DocuSignRecipientsDelete, parameters, TimeoutInSeconds.Thirty);
        }

        /// <summary>
        /// Updates all the auth results for the recipient using the GET response.
        /// </summary>
        /// <param name="summary">The GET response.</param>
        private void UpdateAuthResults(RecipientAuthStatusUpdateSummary summary)
        {
            if (summary == null)
            {
                return;
            }

            if (this.authResults == null)
            {
                this.authResults = new Dictionary<DocuSignRecipientAuthResultType, DocuSignRecipientAuthResult>();
            }

            foreach (var authEvent in summary.AuthEvents)
            {
                this.UpdateAuthResult(authEvent);
            }
        }

        /// <summary>
        /// Update's the recipient's auth result record of the given type from a GET response.
        /// </summary>
        /// <param name="summary">The GET response.</param>
        private void UpdateAuthResult(AuthEventUpdateSummary summary)
        {
            if (summary == null)
            {
                return;
            }

            DocuSignRecipientAuthResult result;
            if (this.authResults.TryGetValue(summary.Type, out result) && result != null)
            {
                result.Update(summary);
            }
            else
            {
                this.authResults[summary.Type] = DocuSignRecipientAuthResult.Create(summary);
            }
        }

        /// <summary>
        /// Saves just the recipient to the DB.
        /// </summary>
        /// <param name="envelopeReferenceId">The parent evenlope id.</param>
        /// <param name="connection">The connection.</param>
        /// <param name="transaction">The transaction.</param>
        private void SaveRecipient(int envelopeReferenceId, DbConnection connection, DbTransaction transaction)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            // These aren't available when an envelope is initially made BUT they can be present
            // if we're doing a GET request and it returns new recipients not already tracked. 
            // Thus we have to account for these being available when making a new recipient as well.
            parameters.Add(new SqlParameter("@CurrentStatus", this.CurrentStatus));
            parameters.Add(new SqlParameter("@DeclinedReason", this.DeclinedReason));
            parameters.Add(new SqlParameter("@AutoRespondedReason", this.AutoRespondedReason));
            parameters.Add(new SqlParameter("@RecipientIdGuid", this.RecipientIdGuid));
            parameters.Add(new SqlParameter("@SigningOrder", this.SigningOrder));
            if (this.statuses != null)
            {
                foreach (var status in DbTrackedStatuses)
                {
                    DateTime? date = null;
                    if (this.statuses.TryGetValue(status.Key, out date))
                    {
                        parameters.Add(new SqlParameter($"@{status.Value}", date));
                    }
                }
            }

            StoredProcedureName sp;
            SqlParameter idParameter = null;
            if (this.isNew)
            {
                this.EnvelopeReferenceId = envelopeReferenceId;

                parameters.Add(new SqlParameter("@EnvelopeReferenceId", this.EnvelopeReferenceId));
                parameters.Add(new SqlParameter("@Type", this.Type));
                parameters.Add(new SqlParameter("@Name", this.Name));
                parameters.Add(new SqlParameter("@Email", this.Email));
                parameters.Add(new SqlParameter("@AccessCode", this.AccessCode));
                parameters.Add(new SqlParameter("@ProvidedPhoneNumber", this.ProvidedPhoneNumber));
                parameters.Add(new SqlParameter("@MfaOption", this.MfaOption));
                parameters.Add(new SqlParameter("@CustomRecipientId", this.CustomRecipientId));
                parameters.Add(new SqlParameter("@ClientUserId", this.ClientUserId));

                idParameter = new SqlParameter("@Id", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                parameters.Add(idParameter);

                sp = DocuSignRecipientsCreate;
            }
            else
            {
                parameters.Add(new SqlParameter("@Id", this.Id));
                
                sp = DocuSignRecipientsUpdate;
            }

            StoredProcedureDriverHelper.ExecuteNonQuery(connection, transaction, sp, parameters, TimeoutInSeconds.Thirty);

            if (this.isNew)
            {
                this.Id = (int)idParameter.Value;
                this.isNew = false;
            }
        }
    }
}
