﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The envelope summary data returned by a creation request.
    /// </summary>
    public class EnvelopeCreateSummary
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnvelopeCreateSummary"/> class.
        /// </summary>
        /// <param name="identifier">The identifier for the envelope.</param>
        /// <param name="statusDateTime">The date and time of the current status of the envelope.</param>
        /// <param name="status">The current status of the envelope.</param>
        /// <param name="documentIdsUsed">The document ids of the document ids sent to DocuSign.</param>
        /// <param name="customRecipientIdsUsed">The custom recipient ids sent to DocuSign.</param>
        public EnvelopeCreateSummary(string identifier, DateTime? statusDateTime, string status, IReadOnlyDictionary<Guid, int> documentIdsUsed, IReadOnlyDictionary<Guid, int> customRecipientIdsUsed)
        {
            this.Identifier = identifier;
            this.StatusDateTime = statusDateTime;
            this.Status = status;
            this.DocumentIdsUsed = documentIdsUsed;
            this.CustomRecipientIdsUsed = customRecipientIdsUsed;
        }

        /// <summary>
        /// Gets a mapping of GUID ids to integer ids of the recipients we sent to DocuSign.
        /// </summary>
        public IReadOnlyDictionary<Guid, int> CustomRecipientIdsUsed { get; }

        /// <summary>
        /// Gets a mapping of GUID ids to positive integer ids of the documents sent to DocuSign.
        /// </summary>
        public IReadOnlyDictionary<Guid, int> DocumentIdsUsed { get; }

        /// <summary>
        /// Gets the identifier for the envelope.
        /// </summary>
        public string Identifier { get; }

        /// <summary>
        /// Gets the date and time of the current status of the envelope.
        /// </summary>
        public DateTime? StatusDateTime { get; }

        /// <summary>
        /// Gets the current status of the envelope.
        /// </summary>
        public string Status { get; }
    }
}
