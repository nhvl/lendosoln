﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Admin.DocuSign;
    using Common;
    using DataAccess;

    /// <summary>
    /// The recipient view model.
    /// </summary>
    public class DocuSignRecipientViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocuSignRecipientViewModel"/> class.
        /// </summary>
        /// <param name="recipientRecord">The recipient record to pull values from.</param>
        public DocuSignRecipientViewModel(DocuSignRecipient recipientRecord)
        {
            this.Name = recipientRecord.Name;
            this.Type = recipientRecord.Type.GetDescription();
            this.Status = recipientRecord.CurrentStatus?.GetDescription() ?? string.Empty;
            this.SigningOrder = recipientRecord.SigningOrder;
            this.Email = recipientRecord.Email;
            this.InitialMfaOption = recipientRecord.MfaOption;
            this.AccessCode = recipientRecord.AccessCode;
            this.ProvidedPhoneNumber = recipientRecord.ProvidedPhoneNumber;
            this.AuthResults = recipientRecord.AuthResults
                ?.OrderByDescending(result => result.Value.EventTime)
                ?.Select(authResult => new DocuSignAuthResultDetailsViewModel(authResult.Key, authResult.Value.Status, authResult.Value.EventTime.Value, authResult.Value.FailureDescription))?.ToList();

            this.StatusDates = new List<DocuSignStatusDatesViewModel>();
            foreach (var statusDate in (recipientRecord.StatusDates?.OrderByDescending((date) => date.Value)).CoalesceWithEmpty())
            {
                if (!statusDate.Value.HasValue)
                {
                    continue;
                }

                string note = null;
                if (statusDate.Key == DocuSignRecipientStatus.Declined)
                {
                    note = recipientRecord.DeclinedReason;
                }

                this.StatusDates.Add(new DocuSignStatusDatesViewModel(statusDate.Key.GetDescription(), statusDate.Value) { Note = note });
            }
            
            if (!string.IsNullOrEmpty(recipientRecord.AutoRespondedReason))
            {
                this.StatusDates.Add(new DocuSignStatusDatesViewModel(DocuSignRecipientStatus.AutoResponded.GetDescription(), null) { Note = recipientRecord.AutoRespondedReason });
            }
        }

        /// <summary>
        /// Gets or sets the name of the recipient.
        /// </summary>
        /// <value>The name of the recipient.</value>
        public string Name
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the type of recipient.
        /// </summary>
        /// <value>The type of recipient.</value>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the status of the recipient.
        /// </summary>
        /// <value>The status of the recipient.</value>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the signing order of this recipient.
        /// </summary>
        /// <value>The signing order of this recipient.</value>
        public int SigningOrder { get; set; }

        /// <summary>
        /// Gets or sets the email of this recipient.
        /// </summary>
        /// <value>The email of this recipient.</value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the status dates for this recipient.
        /// </summary>
        /// <value>The status dates for this recipient.</value>
        public List<DocuSignStatusDatesViewModel> StatusDates { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the status dates have notes.
        /// </summary>
        /// <value>Whether the status dates have notes.</value>
        public bool StatusDatesHasNotes
        {
            get
            {
                return this.StatusDates?.Any(date => !string.IsNullOrEmpty(date.Note)) ?? false;
            }

            set
            {
            }
        }

        /// <summary>
        /// Gets or sets the set of authentication results for the recipient.
        /// </summary>
        /// <value>The authentication results for the recipient.</value>
        public List<DocuSignAuthResultDetailsViewModel> AuthResults { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the status dates have notes.
        /// </summary>
        /// <value>Whether the status dates have notes.</value>
        public bool HasFailureNotes
        {
            get
            {
                return this.AuthResults?.Any(result => !string.IsNullOrEmpty(result.FailureDescription)) ?? false;
            }

            set
            {
            }
        }

        /// <summary>
        /// Gets or sets the MFA option assigned to this recipient on envelope creation.
        /// </summary>
        public string InitialMfaOption_rep
        {
            get
            {
                return this.InitialMfaOption.GetDescription();
            }

            set
            {
            }
        }

        /// <summary>
        /// Gets or sets the initial mfa option.
        /// </summary>
        public MfaOptions InitialMfaOption { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the initial mfa option has an associated phone number.
        /// </summary>
        public bool IsInitialMfaOptionPhone
        {
            get
            {
                return this.InitialMfaOption == MfaOptions.Phone || this.InitialMfaOption == MfaOptions.Sms;
            }

            set
            {
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the initial mfa option has an associated access code.
        /// </summary>
        public bool IsInitialMfaOptionAccessCode
        {
            get
            {
                return this.InitialMfaOption == MfaOptions.AccessCode || this.InitialMfaOption == MfaOptions.AccessCodeAndIdCheck;
            }

            set
            {
            }
        }

        /// <summary>
        /// Gets or sets the access code assigned to this recipient on envelope creation.
        /// </summary>
        /// <value>The access code assigned to this recipient on envelope creation.</value>
        public string AccessCode { get; set; }

        /// <summary>
        /// Gets or sets the phone number for phone/SMS authentication on envelope creation.
        /// </summary>
        /// <value>The phone number for phone/SMS authentication on envelope creation.</value>
        public string ProvidedPhoneNumber { get; set; }
    }
}
