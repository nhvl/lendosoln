﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;

    /// <summary>
    /// Represents a recipient of a document signing envelope.
    /// </summary>
    public class EnvelopeRecipient
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnvelopeRecipient"/> class.
        /// </summary>
        /// <param name="identifier">The unique identifier for the recipient.</param>
        /// <param name="name">The name of the recipient.</param>
        /// <param name="email">The email of the recipient.</param>
        /// <param name="type">The type of the recipient.</param>
        /// <param name="note">The note or private message to the recipient.</param>
        /// <param name="signingPosition">The one-indexed position of the recipient in the signing order.</param>
        /// <param name="authentication">The multi-factor authentication requirements for the recipient.</param>
        public EnvelopeRecipient(Guid identifier, string name, string email, RecipientType type, string note, int signingPosition, RecipientMultifactorAuthentication authentication)
        {
            this.Identifier = identifier;
            this.Name = name;
            this.Email = email;
            this.Type = type;
            this.Note = note;
            this.SigningPosition = signingPosition;
            this.Authentication = authentication;
        }

        /// <summary>
        /// Gets the LQB unique identifier (clientUserId) for the recipient.
        /// </summary>
        public Guid Identifier { get; }

        /// <summary>
        /// Gets the name of the recipient.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets the email of the recipient.
        /// </summary>
        public string Email { get; }

        /// <summary>
        /// Gets the type of the recipient.
        /// </summary>
        public RecipientType Type { get; }

        /// <summary>
        /// Gets the note or private message to the recipient.
        /// </summary>
        public string Note { get; }

        /// <summary>
        /// Gets the one-indexed position of the recipient in the signing order.
        /// </summary>
        public int SigningPosition { get; }

        /// <summary>
        /// Gets the multi-factor authentication requirements for the recipient.
        /// </summary>
        public RecipientMultifactorAuthentication Authentication { get; }
    }
}
