﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System.ComponentModel;

    /// <summary>
    /// The status of the recipient.
    /// </summary>
    public enum DocuSignRecipientStatus
    {
        /// <summary>
        /// The recipient is in a draft state. This is only associated with draft envelopes (envelopes with a Created status).
        /// </summary>
        [Description("Created")]
        Created = 0,

        /// <summary>
        /// The recipient has been sent an email notification that it is their turn to sign and envelope.
        /// </summary>
        [Description("Sent")]
        Sent = 1,

        /// <summary>
        /// The recipient has viewed the document(s) in an envelope through the DocuSign signing web site. This is not an email delivery of the documents in an envelope.
        /// </summary>
        [Description("Delivered")]
        Delivered = 2,

        /// <summary>
        /// The recipient has completed (signed) all required tags in an envelope. This is a temporary state during processing, after which the recipient is automatically moved to Completed.
        /// </summary>
        [Description("Signed")]
        Signed = 3,

        /// <summary>
        /// The recipient declined to sign the document(s) in the envelope.
        /// </summary>
        [Description("Declined")]
        Declined = 4,

        /// <summary>
        /// The recipient has completed their actions (signing or other required actions if not a signer) for an envelope.
        /// </summary>
        [Description("Completed")]
        Completed = 5,

        /// <summary>
        /// The recipient has finished signing and the system is waiting a fax attachment by the recipient before completing their signing step.
        /// </summary>
        [Description("Fax Pending")]
        FaxPending = 6,

        /// <summary>
        /// The recipient’s email system auto-responded (bounced-back) to the email from DocuSign. 
        /// This status is used in the web console to inform senders about the bounced-back email. This is only used if “Send-on-behalf-of” is turned off for the account.
        /// </summary>
        [Description("Auto Responded")]
        AutoResponded = 7,

        /// <summary>
        /// For statuses that we don't recognize.
        /// </summary>
        [Description("Other")]
        Other = 8,

        /// <summary>
        /// No status was given back.
        /// </summary>
        [Description("N/A")]
        NA = 9
    }
}
