﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Web;
    using DataAccess;
    using LendersOffice.Admin.DocuSign;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// A class for handling DocuSign recipient view (<see href="https://docs.docusign.com/esign/restapi/Envelopes/EnvelopeViews/createRecipient/"/>) requests.
    /// </summary>
    public static class DocuSignRecipientView
    {
        /// <summary>
        /// Returned signing event categories from DocuSign.
        /// </summary>
        private enum SigningEvent
        {
            /// <summary>
            /// Default value - there was a signing event, but it's not recognized.
            /// </summary>
            NotRecognized = default(int),

            /// <summary>
            /// Recipient used incorrect access code.
            /// </summary>
            [Description("Recipient used incorrect access code.")]
            access_code_failed,

            /// <summary>
            /// Recipient canceled the signing operation, possibly by using the Finish Later option.
            /// </summary>
            [Description("Recipient canceled the signing operation, possibly by using the Finish Later option.")]
            cancel,

            /// <summary>
            /// Recipient declined to sign.
            /// </summary>
            [Description("Recipient declined to sign.")]
            decline,

            /// <summary>
            /// A system error occurred during signing.
            /// </summary>
            [Description("A system error occurred during signing.")]
            exception,

            /// <summary>
            /// Recipient has a fax pending.
            /// </summary>
            [Description("Recipient has a fax pending.")]
            fax_pending,

            /// <summary>
            /// Recipient failed an ID check.
            /// </summary>
            [Description("Recipient failed an ID check.")]
            id_check_failed,

            /// <summary>
            /// The session timed out. An account can control this timeout using the Signer Session Timeout option.
            /// </summary>
            [Description("The session timed out. An account can control this timeout using the Signer Session Timeout option.")]
            session_timeout,

            /// <summary>
            /// Recipient completed the signing ceremony.
            /// </summary>
            [Description("Recipient completed the signing ceremony.")]
            signing_complete,

            /// <summary>
            /// The Time To Live token for the envelope has expired. After being successfully invoked, these tokens expire after 5 minutes or if the envelope is voided.
            /// </summary>
            [Description("The Time To Live token for the envelope has expired. After being successfully invoked, these tokens expire after 5 minutes or if the envelope is voided.")]
            ttl_expired,

            /// <summary>
            /// The recipient completed viewing an envelope that is in a read-only/terminal state such as completed, declined, or voided.
            /// </summary>
            [Description("The recipient completed viewing an envelope that is in a read-only/terminal state such as completed, declined, or voided.")]
            viewing_complete,
        }
        
        /// <summary>
        /// Enumerates statuses to send to the UI.
        /// </summary>
        private enum LiveSigningStatus
        {
            /// <summary>
            /// Status has not been initialized.
            /// </summary>
            unknown = 0,

            /// <summary>
            /// An error status.
            /// </summary>
            error,

            /// <summary>
            /// The singing session is completed.
            /// </summary>
            complete,

            /// <summary>
            /// Signing returned from DocuSign successfully and there are more signers.
            /// </summary>
            success
        }

        /// <summary>
        /// Enumerates error types to send to the UI.
        /// </summary>
        private enum LiveSigningErrorType
        {
            /// <summary>
            /// Error type has not been initialized.
            /// </summary>
            unknown = 0,

            /// <summary>
            /// The CSRF state token was not present or did not match.
            /// </summary>
            /// <remarks>
            /// This is a "soft" error type, where we allow the user to get a new "state" value and reissue their request.
            /// </remarks>
            state,

            /// <summary>
            /// The signing event returned from DocuSign was not a successful one. We should display the error type to the user and ask if they want to try again.
            /// </summary>
            signingEvent,

            /// <summary>
            /// There was a system error in the LendingQB side. (e.g. we threw an exception).
            /// </summary>
            system,
        }

        /// <summary>
        /// Get a view model for the live signing callback page to redirect to the next signer or display an error. 
        /// </summary>
        /// <param name="user">The user in our system.</param>
        /// <param name="loanId">The loan ID of the envelope.</param>
        /// <param name="envelopeId">The envelope ID.</param>
        /// <param name="signingOrder">The signing order.</param>
        /// <param name="eventString">The event parameter string passed from DocuSign.</param>
        /// <param name="state">A state parameter to prevent CSRF.</param>
        /// <returns>The live signing page view model. Note that this includes a URL from DocuSign with a time-to-live of 5 minutes.</returns>
        public static string GetLiveSigningViewModel(AbstractUserPrincipal user, Guid loanId, int envelopeId, int signingOrder, string eventString, string state)
        {
            SigningEvent? docuSignEvent;
            if (eventString != null)
            {
                docuSignEvent = eventString.ToNullableEnum<SigningEvent>() ?? null;
            }
            else
            {
                docuSignEvent = null;
            }

            var envelope = DocuSignEnvelope.Load(user.BrokerId, loanId, envelopeId);
            var lastRecipient = envelope.Recipients.FirstOrDefault(recipient => recipient.SigningOrder == signingOrder - 1);
            var nextRecipient = envelope.Recipients.FirstOrDefault(recipient => recipient.Type == RecipientType.InPersonSigner && recipient.SigningOrder >= signingOrder);

            var lastRecipientSerializable = new LiveSigningViewModel.Recipient(lastRecipient?.Name, lastRecipient?.SigningOrder);
            var nextRecipientSerializable = new LiveSigningViewModel.Recipient(nextRecipient?.Name, nextRecipient?.SigningOrder);

            bool statesMatch = state == AutoExpiredTextCache.GetFromCacheByUser(user, TokenKey(loanId));
            string newState = GenerateStateToken(user, loanId);
            if (!statesMatch)
            {
                return new LiveSigningViewModel(
                    status: LiveSigningStatus.error,
                    errorType: LiveSigningErrorType.state,
                    state: newState,
                    lastRecipient: lastRecipientSerializable,
                    nextRecipient: nextRecipientSerializable).Json();
            }

            if (docuSignEvent != null && (docuSignEvent.Value != SigningEvent.signing_complete && docuSignEvent.Value != SigningEvent.viewing_complete))
            {
                return new LiveSigningViewModel(
                    status: LiveSigningStatus.error,
                    errorType: LiveSigningErrorType.signingEvent,
                    state: newState,
                    eventCode: docuSignEvent,
                    errorMessage: docuSignEvent?.GetDescription(),
                    lastRecipient: lastRecipientSerializable).Json();
            }

            if (nextRecipient == null)
            {
                return new LiveSigningViewModel(
                    status: LiveSigningStatus.complete,
                    lastRecipient: lastRecipientSerializable).Json();
            }

            LenderDocuSignSettings settings = LenderDocuSignSettings.Retrieve(user.BrokerId);
            string hostEmail = settings.DocuSignEmail;
            string hostName = settings.DocuSignUserName;

            RecipientViewRequest request;
            int nextSigningOrder = nextRecipient.SigningOrder + 1;
            string clientUserId = nextRecipient.ClientUserId;

            var callbackUri = GetRecipientViewCallbackUri(
                    urlBase: $"{HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)}{Tools.VRoot}",
                    loanId: loanId,
                    envelopeId: envelopeId,
                    nextSigningOrder: nextSigningOrder,
                    state: newState);
            request = new RecipientViewRequest(envelope.EnvelopeId, "Password", clientUserId, hostEmail, hostName, callbackUri);

            // Use generic locator or inject when that becomes available.
            var recipientViewResult = new DocuSignDriver().GetRecipientViewUrl(settings.ToAuthenticationData(), request);
            if (recipientViewResult.HasError)
            {
                return new LiveSigningViewModel(
                    status: LiveSigningStatus.error,
                    errorType: LiveSigningErrorType.system,
                    errorMessage: recipientViewResult.Error.Message,
                    state: newState,
                    lastRecipient: lastRecipientSerializable,
                    nextRecipient: nextRecipientSerializable).Json();
            }

            string recipientViewUrl = recipientViewResult.Value.ToString();
            return new LiveSigningViewModel(
                status: LiveSigningStatus.success,
                lastRecipient: lastRecipientSerializable,
                nextRecipient: nextRecipientSerializable,
                recipientViewUrl: recipientViewUrl).Json();
        }

        /// <summary>
        /// Gets the callback URI for starting or continuing an in-person signing session.
        /// </summary>
        /// <param name="urlBase">The base of the URL, containing the entire URI authority plus the VRoot (try <code>HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + Tools.VRoot</code> from the UI).</param>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="envelopeId">The envelope ID.</param>
        /// <param name="nextSigningOrder">The next recipient to execute embedded signing for.</param>
        /// <param name="state">A state parameter to make sure the process follows expected flow and isn't vulnerable to CSRF.</param>
        /// <returns>A URI on our side for DocuSign to redirect back to (or to start an in-person signing session ourselves).</returns>
        public static LqbAbsoluteUri GetRecipientViewCallbackUri(string urlBase, Guid loanId, int envelopeId, int nextSigningOrder, string state)
        {
            return LqbAbsoluteUri.Create($"{urlBase}/newlos/Services/DocuSign/LiveSigningCallback.aspx?loanid={loanId}&envelopeId={envelopeId}&signingOrder={nextSigningOrder}&state={state}").ForceValue();
        }

        /// <summary>
        /// Generates and returns a CSRF token linked to the given user and loan.
        /// </summary>
        /// <param name="user">The user associated with the token.</param>
        /// <param name="loanId">The loan ID of a loan associated with the token.</param>
        /// <returns>The new state token.</returns>
        public static string GenerateStateToken(AbstractUserPrincipal user, Guid loanId)
        {
            string newState = Guid.NewGuid().ToString("N");
            AutoExpiredTextCache.RemoveUserEntry(user, TokenKey(loanId));
            AutoExpiredTextCache.AddUserString(user, newState, TimeSpan.FromHours(8), TokenKey(loanId));
            return newState;
        }

        /// <summary>
        /// Gets the token key to pass into <see cref="AutoExpiredTextCache"/> user entry methods. This does not contain the user ID, which those methods will add.
        /// </summary>
        /// <param name="loanId">Loan ID of the associated loan.</param>
        /// <returns>AutoExpiredTextCache partial key.</returns>
        private static string TokenKey(Guid loanId)
        {
            return $"LiveSigningCSRFToken_{loanId:N}";
        }

        /// <summary>
        /// A model to send to the UI for displaying data to the user.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Private class used only for serialization to JSON for the UI.")]
        private class LiveSigningViewModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="LiveSigningViewModel"/> class. Don't use this, it's required (but not really) to serialize using SerializationHelper.
            /// </summary>
            public LiveSigningViewModel()
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="LiveSigningViewModel"/> class.
            /// </summary>
            /// <param name="status">Live signing status.</param>
            /// <param name="errorType">Error type.</param>
            /// <param name="eventCode">Event code from DocuSign.</param>
            /// <param name="errorMessage">Error message.</param>
            /// <param name="state">State token.</param>
            /// <param name="lastRecipient">Previous recipient.</param>
            /// <param name="nextRecipient">Next recipient.</param>
            /// <param name="recipientViewUrl">The view URL to start signing in DocuSign.</param>
            public LiveSigningViewModel(
                LiveSigningStatus status,
                LiveSigningErrorType? errorType = null,
                SigningEvent? eventCode = null,
                string errorMessage = null,
                string state = null,
                Recipient lastRecipient = null,
                Recipient nextRecipient = null,
                string recipientViewUrl = null)
            {
                this.status = status;
                this.errorType = errorType;
                this.eventCode = eventCode;
                this.errorMessage = errorMessage;
                this.state = state;
                this.lastRecipient = lastRecipient;
                this.nextRecipient = nextRecipient;
                this.recipientViewUrl = recipientViewUrl;
            }

            /// <summary>
            /// Gets live signing status.
            /// </summary>
            public LiveSigningStatus status { get; }

            /// <summary>
            /// Gets error type for error status.
            /// </summary>
            public LiveSigningErrorType? errorType { get; }

            /// <summary>
            /// Gets signing event code.
            /// </summary>
            public SigningEvent? eventCode { get; }

            /// <summary>
            /// Gets error message.
            /// </summary>
            public string errorMessage { get; }

            /// <summary>
            /// Gets new state token.
            /// </summary>
            public string state { get; }

            /// <summary>
            /// Gets the previous recipient.
            /// </summary>
            public Recipient lastRecipient { get; }

            /// <summary>
            /// Gets next recipient.
            /// </summary>
            public Recipient nextRecipient { get; }

            /// <summary>
            /// Gets recipient view Url for signing in DocuSign.
            /// </summary>
            public string recipientViewUrl { get; }

            /// <summary>
            /// Serializes this object to JSON.
            /// </summary>
            /// <returns>This object as JSON.</returns>
            public string Json()
            {
                return SerializationHelper.JsonNetSerialize(this, new Newtonsoft.Json.JsonSerializerSettings { Converters = new Newtonsoft.Json.JsonConverter[] { new Newtonsoft.Json.Converters.StringEnumConverter() } });
            }

            /// <summary>
            /// Represents a recipient.
            /// </summary>
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Private class used only for serialization to JSON for the UI.")]
            public class Recipient
            {
                /// <summary>
                /// Initializes a new instance of the <see cref="Recipient"/> class.
                /// </summary>
                /// <param name="name">Recipient name.</param>
                /// <param name="signingOrder">Recipient signing order.</param>
                public Recipient(string name, int? signingOrder)
                {
                    this.name = name;
                    this.signingOrder = signingOrder;
                }

                /// <summary>
                /// Gets recipient name.
                /// </summary>
                public string name { get; }

                /// <summary>
                /// Gets signing order.
                /// </summary>
                public int? signingOrder { get; }
            }
        }
    }
}
