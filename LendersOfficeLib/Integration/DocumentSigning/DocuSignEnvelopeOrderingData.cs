﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Admin.DocuSign;
    using Common;
    using DataAccess;
    using EDocs;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using Rolodex;
    using Security;

    /// <summary>
    /// Provides common methods for loading DocuSign envelope ordering data.
    /// </summary>
    public static class DocuSignEnvelopeOrderingData
    {
        /// <summary>
        /// In order to line up the DocuSign signature and initial tabs with the line they're placed on,
        /// we take the bottom of the field in our system and add this value to determine the top of the
        /// DocuSign tag we should send.
        /// This value is not based on any guidance from DocuSign.
        /// </summary>
        private const double SignatureYPositionUpFromBottom = 26d;

        /// <summary>
        /// Deserializes the recipients from the UI into recipients to send as part of an envelope request.
        /// </summary>
        /// <param name="user">The user making the request.</param>
        /// <param name="jsonRecipients">The JSON string representing a list of recipients from the UI.</param>
        /// <returns>A dictionary mapping role strings to recipients.</returns>
        public static Dictionary<string, EnvelopeRecipient> RecipientDictionary(AbstractUserPrincipal user, string jsonRecipients)
        {
            var docuSignSettings = LenderDocuSignSettings.Retrieve(user.BrokerId);
            return SerializationHelper.JsonNetDeserialize<List<RequestRecipient>>(jsonRecipients)
                .SelectMany(recipient => 
                {
                    var envelopeRecipient = ValidateRecipient(docuSignSettings, recipient).ToEnvelopeRecipient(docuSignSettings);
                    return recipient.roles.Select(role =>
                    {
                        string roleName = role;
                        if (role.StartsWith("Borrower"))
                        {
                            roleName = $"Borrower {recipient.appId}";
                        }

                        if (role.StartsWith("Coborrower"))
                        {
                            roleName = $"Coborrower {recipient.appId}";;
                        }

                        return new { role = roleName, recipient = envelopeRecipient };
                    });
                })
                .ToDictionary(item => item.role, item => item.recipient);
        }

        /// <summary>
        /// Gets document and document tab information to send to DocuSign.
        /// </summary>
        /// <param name="user">The user making the request.</param>
        /// <param name="documentIds">The identifiers for documents to include.</param>
        /// <param name="recipientsByRole">A dictionary that looks up the recipient to use by their role.</param>
        /// <returns>Document and document tab information to include in a DocuSign envelope.</returns>
        public static Result<Tuple<IEnumerable<LqbDocument>, IEnumerable<DocumentTab>>> DocumentInfoForDocuSign(AbstractUserPrincipal user, IEnumerable<Guid> documentIds, IDictionary<string, EnvelopeRecipient> recipientsByRole)
        {
            EDocumentRepository repository = EDocumentRepository.GetSystemRepositoryForUser(user.BrokerId, user.UserId);
            List<LqbDocument> docusignDocuments = new List<LqbDocument>();
            List<DocumentTab> docusignTabs = new List<DocumentTab>();
            int docIndex = 0;
            foreach (Guid docId in documentIds)
            {
                EDocument doc = repository.GetDocumentById(docId);
                if (doc.ImageStatus != E_ImageStatus.HasCachedImages)
                {
                    // This means the document is not prepped for editing, so we can't get page info for tab positions.
                    Tools.LogError($"DocuSign Envelope Ordering: An unprocessed document was chosen to be included. DocumentId = {doc.DocumentId}. This should not be allowed from the UI.");
                    return Result<Tuple<IEnumerable<LqbDocument>, IEnumerable<DocumentTab>>>.Failure(new ServerException(ErrorMessage.SystemError));
                }

                var pages = doc.GetPdfPageInfoList().Select(pageItem => new LqbDocumentPage(pageItem.PageHeight, pageItem.PageWidth));
                Guid docusignDocumentId = Guid.NewGuid();
                var lqbDocument = new LqbDocument(
                        docuSignId: docusignDocumentId,
                        lqbDocId: doc.DocumentId,
                        name: doc.FolderAndDocTypeName,
                        sequenceNumber: ++docIndex,
                        file: LocalFilePath.Create(doc.GetPDFTempFile_Current()).ForceValue(),
                        pages: pages.ToList());
                docusignDocuments.Add(lqbDocument);

                foreach (var tag in doc.ESignTags.AnnotationList)
                {
                    string roleString = null;
                    if (tag.AssociatedBorrower != null)
                    {
                        roleString = tag.AssociatedBorrower.ToString() + " " + doc.AppId;
                    }
                    else if (tag.AssociatedRole != null)
                    {
                        roleString = RolodexDB.GetTypeDescription(tag.AssociatedRole.Value);
                    }

                    EnvelopeRecipient tagRecipient = null;
                    if (recipientsByRole.TryGetValue(roleString, out tagRecipient))
                    {
                        DocumentTab tab = new DocumentTab(
                            documentIdentifier: docusignDocumentId,
                            recipientIdentifier: tagRecipient.Identifier,
                            type: ConvertToTabType(tag.ItemType),
                            pageNumber: tag.PageNumber,
                            xposition: (int)Math.Round(tag.Rectangle.Left), // Upper-left corner
                            yposition: CalculateTagYPositionForDocuSign(tag, lqbDocument));
                        docusignTabs.Add(tab);
                    }
                }
            }

            return Result<Tuple<IEnumerable<LqbDocument>, IEnumerable<DocumentTab>>>.Success(
                new Tuple<IEnumerable<LqbDocument>, IEnumerable<DocumentTab>>(docusignDocuments, docusignTabs));
        }

        /// <summary>
        /// Builds a list of borrower IDs (AppID + borrower position pairs) to be included as ESign recipients for the given list of documents.
        /// </summary>
        /// <param name="documents">The documents to determine borrowers for.</param>
        /// <returns>A set of borrower ID objects (AppID + borrower position pairs).</returns>
        public static HashSet<Tuple<Guid, E_BorrowerModeT>> GetBorrowerIdsForDocs(IEnumerable<EDocument> documents)
        {
            HashSet<Tuple<Guid, E_BorrowerModeT>> borrowerIds = new HashSet<Tuple<Guid, E_BorrowerModeT>>();
            foreach (var doc in documents.Where(document => document.AppId != null))
            {
                if (doc.ESignTags.AnnotationList.Any(annotation => annotation.AssociatedBorrower == E_BorrowerModeT.Borrower))
                {
                    borrowerIds.Add(new Tuple<Guid, E_BorrowerModeT>(doc.AppId.Value, E_BorrowerModeT.Borrower));
                }

                if (doc.ESignTags.AnnotationList.Any(annotation => annotation.AssociatedBorrower == E_BorrowerModeT.Coborrower))
                {
                    borrowerIds.Add(new Tuple<Guid, E_BorrowerModeT>(doc.AppId.Value, E_BorrowerModeT.Coborrower));
                }
            }

            return borrowerIds;
        }

        /// <summary>
        /// Gets document and recipient data in JSON form for the UI.
        /// </summary>
        /// <param name="documents">A list of documents to serialize for the envelope UI.</param>
        /// <param name="loan">The loan data object for the current loan.</param>
        /// <param name="rolesToExclude">A list of roles already present in the UI (as their <see cref="RolodexDB.GetTypeDescription(E_AgentRoleT)"/> value).</param>
        /// <returns>A list of documents and roles to populate back to the UI.</returns>
        public static string GetDocumentAndRecipientJsonString(IEnumerable<EDocument> documents, CPageData loan, IEnumerable<string> rolesToExclude)
        {
            HashSet<string> rolesToExcludeSet = new HashSet<string>(rolesToExclude);
            var allDocRoles = documents.SelectMany(doc => doc.ESignTags.AnnotationList.Select(annotation => annotation.AssociatedRole)).Distinct().WhereNotNull();
            var rolesToSend = allDocRoles.Where(role => !rolesToExcludeSet.Contains(RolodexDB.GetTypeDescription(role)));
            var docList = DocuSignDocumentData.GetDocumentObjects(documents, loan);

            var recipientList = GetRecipientObjects(loan, rolesToSend, GetBorrowerIdsForDocs(documents));

            return SerializationHelper.JsonNetAnonymousSerialize(new
            {
                docInfo = docList,
                recipientList = recipientList
            });
        }

        /// <summary>
        /// Gets a list of recipient objects to populate the recipients in the UI.
        /// </summary>
        /// <param name="loanData">The loan to get recipient data from.</param>
        /// <param name="roles">The roles to get from the loan file as UI objects.</param>
        /// <param name="borrowerIds">An enumerable list of application IDs for which to include the borrower (and coborrower, if applicable).</param>
        /// <returns>An object list intended to be immediately serialized and sent to the UI. 
        /// This should be returned as a string, but there's currently no version of RegisterJsObject() (and maybe there shouldn't be) that takes the object as a serialized JSON string.</returns>
        public static List<object> GetRecipientObjects(CPageData loanData, IEnumerable<E_AgentRoleT> roles, IEnumerable<Tuple<Guid, E_BorrowerModeT>> borrowerIds)
        {
            List<RecipientViewModel> recipientList = new List<RecipientViewModel>();
            var docuSignSettings = LenderDocuSignSettings.Retrieve(loanData.sBrokerId);

            IEnumerable<CAgentFields> agents = roles.Select(role => loanData.GetAgentOfRole(role, E_ReturnOptionIfNotExist.ReturnEmptyObject)).Where(agent => agent.IsValid);
            recipientList = agents.GroupBy(agent => agent.EmailAddr).Where(group => !string.IsNullOrEmpty(group.Key)).Select((grouping, index) => new RecipientViewModel
            {
                id = index,
                name = grouping.FirstOrDefault(agent => !string.IsNullOrEmpty(agent.AgentName))?.AgentName ?? string.Empty,
                email = grouping.Key,
                roles = grouping.Select(agent => agent.AgentRoleDescription).ToList(),
                mfaChosen = docuSignSettings.DefaultMfaOption,
                signingInPerson = docuSignSettings.DefaultMfaOption == MfaOptions.NoMfa,
                signingOrder = index + 1,
                isAgent = true,
                phone = new RecipientViewModel.PhoneViewModel
                {
                    agentPhone = grouping.FirstOrDefault()?.Phone,
                    companyPhone = grouping.FirstOrDefault()?.PhoneOfCompany,
                    cellPhone = grouping.FirstOrDefault()?.CellPhone,
                }
            }).ToList();

            // Agents without an email may still have information that would be useful to pre-populate.
            int noEmailId = recipientList.Count;
            recipientList.AddRange(agents.Where(agent => string.IsNullOrEmpty(agent.EmailAddr)).Select((agent, index) => new RecipientViewModel
            {
                id = noEmailId + index,
                name = string.IsNullOrEmpty(agent.AgentName) ? agent.CompanyName : agent.AgentName,
                email = agent.EmailAddr,
                roles = new List<string> { agent.AgentRoleDescription },
                mfaChosen = docuSignSettings.DefaultMfaOption,
                signingInPerson = docuSignSettings.DefaultMfaOption == MfaOptions.NoMfa,
                signingOrder = noEmailId + index + 1,
                isAgent = true,
                phone = new RecipientViewModel.PhoneViewModel
                {
                    agentPhone = agent.Phone,
                    companyPhone = agent.PhoneOfCompany,
                    cellPhone = agent.CellPhone
                }
            }));

            List<Application> apps = loanData.Apps.Select(loanApp => 
                new Application(
                    appData: loanApp,
                    includeBorrower: borrowerIds.Contains(new Tuple<Guid, E_BorrowerModeT>(loanApp.aAppId, E_BorrowerModeT.Borrower)),
                    includeCoborrower: loanApp.aHasSpouse && borrowerIds.Contains(new Tuple<Guid, E_BorrowerModeT>(loanApp.aAppId, E_BorrowerModeT.Coborrower)))).ToList();
            AddAppsToRecipients(apps, recipientList, docuSignSettings);

            return recipientList.Cast<object>().ToList();
        }

        /// <summary>
        /// Gets recipient objects for each of the borrowers on a loan.
        /// </summary>
        /// <param name="loanData">The loan object to get the borrowers for.</param>
        /// <returns>A list of RecipientViewModel objects to serialize.</returns>
        public static string GetBorrowerRecipients(CPageData loanData)
        {
            List<RecipientViewModel> recipients = new List<RecipientViewModel>();
            AddAppsToRecipients(loanData.Apps.Select(app => new Application(app, true, true)), recipients, LenderDocuSignSettings.Retrieve(loanData.sBrokerId));
            return SerializationHelper.JsonNetSerialize(recipients);
        }

        /// <summary>
        /// Adds recipients for each <see cref="CAppData"/> in the given enumerable to the given list.
        /// </summary>
        /// <param name="apps">An enumerable of application objects to add to the recipient list.</param>
        /// <param name="recipientList">The recipient list to add to.</param>
        /// <param name="docuSignSettings">The lender DocuSign settings object to use for creating recipients.</param>
        private static void AddAppsToRecipients(IEnumerable<Application> apps, List<RecipientViewModel> recipientList, LenderDocuSignSettings docuSignSettings)
        {
            int appCounter = 1;
            int numApps = apps.Count(app => app.IncludeBorrower || app.IncludeCoborrower);
            foreach (Application app in apps)
            {
                var appSuffix = numApps > 1 ? (" " + appCounter) : string.Empty;

                if (app.IncludeBorrower)
                {
                    recipientList.Add(RecipientViewModel.CreateFromApp(docuSignSettings, app.AppData, E_BorrowerModeT.Borrower, recipientList.Count, appSuffix));
                }

                if (app.AppData.aHasSpouse && app.IncludeCoborrower)
                {
                    recipientList.Add(RecipientViewModel.CreateFromApp(docuSignSettings, app.AppData, E_BorrowerModeT.Coborrower, recipientList.Count, appSuffix));
                }

                appCounter++;
            }
        }

        /// <summary>
        /// Converts the y-coordinate of a tag in our system to the y-coordinate we send to DocuSign.
        /// </summary>
        /// <param name="tag">The tag to calculate the y-position of.</param>
        /// <param name="document">The document containing the tag (used for page height info).</param>
        /// <returns>The y-position value to send to DocuSign.</returns>
        private static int CalculateTagYPositionForDocuSign(EDocumentAnnotationItem tag, LqbDocument document)
        {
            DocumentTabType tabType = ConvertToTabType(tag.ItemType);
            int yposition;
            if (tabType == DocumentTabType.InitialHere || tabType == DocumentTabType.SignHere)
            {
                // Attempt to line up the bottom of signatures
                yposition = (int)Math.Round(tag.Rectangle.Bottom + SignatureYPositionUpFromBottom);

                // DocuSign quirk: "the tab appears 21 points lower than the value you provide for the y-coordinate."
                // https://docs.docusign.com/esign/restapi/Envelopes/EnvelopeRecipientTabs/#sign-here-tab-alignment
                yposition += 21;
            }
            else
            {
                yposition = (int)Math.Round(tag.Rectangle.Top);
            }

            // DocuSign measures their coordinates from the top-left corner instead of bottom-left (fortunately, still at 72 DPI)
            int pageHeight = document.Pages[tag.PageNumber - 1].Height;
            yposition = pageHeight - yposition;

            // DocuSign doesn't allow coordinates less than 0 (off the top of the page)
            return Math.Max(yposition, 0);
        }

        /// <summary>
        /// Converts an annotation item type into a document tab type for sending to DocuSign.
        /// </summary>
        /// <param name="annotationType">The annotation type to convert. If not an ESign annotation type, the method will throw.</param>
        /// <returns>The document tab type.</returns>
        private static DocumentTabType ConvertToTabType(E_EDocumentAnnotationItemType annotationType)
        {
            switch (annotationType)
            {
                case E_EDocumentAnnotationItemType.Initial:
                    return DocumentTabType.InitialHere;
                case E_EDocumentAnnotationItemType.Signature:
                    return DocumentTabType.SignHere;
                case E_EDocumentAnnotationItemType.SignedDate:
                    return DocumentTabType.DateSigned;
                default:
                    throw new UnhandledEnumException(annotationType);
            }
        }

        /// <summary>
        /// Validates recipients from the UI.
        /// </summary>
        /// <param name="settings">The Lender's DocuSign settings object.</param>
        /// <param name="recipient">The recipient to validate.</param>
        /// <exception cref="ServerException">Thrown when validation fails and we should not continue.</exception>
        /// <returns>The validated recipient.</returns>
        private static RequestRecipient ValidateRecipient(LenderDocuSignSettings settings, RequestRecipient recipient)
        {
            // Indicates that either an out-of-bounds or multi-selection option was sent.
            if (!Enum.IsDefined(typeof(MfaOptions), recipient.mfaChosen) || !settings.EnabledMfaOptions.HasFlag(recipient.mfaChosen))
            {
                throw new ServerException(ErrorMessage.ValidationError, new ValidationErrorContext(new[] { ValidationErrorMessage.Create($"DocuSign MFA from UI:{recipient.mfaChosen:G}, Enabled MFA:{settings.EnabledMfaOptions:G}").ForceValue() }));
            }

            if (recipient.mfaChosen == MfaOptions.NoSelection)
            {
                throw new ServerException(ErrorMessage.ValidationError);
            }

            if ((recipient.mfaChosen == MfaOptions.AccessCode || recipient.mfaChosen == MfaOptions.AccessCodeAndIdCheck) && string.IsNullOrWhiteSpace(recipient.accessCode))
            {
                throw new ServerException(ErrorMessage.ValidationError);
            }

            if (recipient.mfaChosen == MfaOptions.Phone || recipient.mfaChosen == MfaOptions.Sms)
            {
                if (recipient.mfaChosen == MfaOptions.Sms && recipient.phone.choice != RecipientViewModel.PhoneViewModel.PhoneChoice.cell && recipient.phone.choice != RecipientViewModel.PhoneViewModel.PhoneChoice.chosen)
                {
                    throw new ServerException(ErrorMessage.ValidationError);
                }

                string submittedPhone = recipient.GetSubmittedPhone();
                if (string.IsNullOrEmpty(submittedPhone))
                {
                    throw new ServerException(ErrorMessage.ValidationError);
                }
            }

            return recipient;
        }

        /// <summary>
        /// Gets a list of the distinct roles assigned to sign a document.
        /// </summary>
        /// <param name="document">The document to determine signing roles for.</param>
        /// <returns>A list of roles in string form.</returns>
        private static List<string> GetDocumentSigningRoles(EDocument document)
        {
            var tags = document.ESignTags.AnnotationList;
            return tags.Select(annotation => annotation.RecipientDescription).Distinct().ToList();
        }

        /// <summary>
        /// Represents a loan application and whether the borrower and/or coborrower should be recipients on an envelope.
        /// </summary>
        private class Application
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Application"/> class.
            /// </summary>
            /// <param name="appData">The application on which the borrower is located.</param>
            /// <param name="includeBorrower">Whether to include the borrower as an envelope recipient.</param>
            /// <param name="includeCoborrower">Whether to include the co-borrower as an envelope recipient.</param>
            public Application(CAppData appData, bool includeBorrower, bool includeCoborrower)
            {
                this.AppData = appData;
                this.IncludeBorrower = includeBorrower;
                this.IncludeCoborrower = includeCoborrower;
            }

            /// <summary>
            /// Gets or sets the application on which the borrower is located.
            /// </summary>
            public CAppData AppData { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether to include the borrower as an envelope recipient.
            /// </summary>
            public bool IncludeBorrower { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether to include the co-borrower as an envelope recipient.
            /// </summary>
            public bool IncludeCoborrower { get; set; }
        }

        /// <summary>
        /// Private class to help deserializing the data sent from the envelope request UI.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Used only for serialization from JSON from the UI.")]
        private class RequestRecipient
        {
            /// <summary>
            /// Gets or sets the recipient's name.
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// Gets or sets the recipient's email.
            /// </summary>
            public string email { get; set; }

            /// <summary>
            /// Gets or sets a list of roles to associate the recipient with.
            /// </summary>
            public List<string> roles { get; set; }

            /// <summary>
            /// Gets or sets the MFA option that was chosen.
            /// </summary>
            public MfaOptions mfaChosen { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the recipient is signing in person.
            /// </summary>
            public bool signingInPerson { get; set; }

            /// <summary>
            /// Gets or sets the signing order of the recipient.
            /// </summary>
            public int signingOrder { get; set; }

            /// <summary>
            /// Gets or sets the access code the recipient will use for multi-factor authentication.
            /// </summary>
            public string accessCode { get; set; }

            /// <summary>
            /// Gets or sets the application ID of a borrower recipient.
            /// </summary>
            public Guid? appId { get; set; } = null;

            /// <summary>
            /// Gets or sets phone information about the recipient.
            /// </summary>
            public RecipientViewModel.PhoneViewModel phone { get; set; }

            /// <summary>
            /// Converts this object to an envelope recipient for a DocuSign Envelope.
            /// </summary>
            /// <param name="settings">The lender's DocuSign configuration settings.</param>
            /// <returns>An envelope recipient for a DocuSign Envelope.</returns>
            internal EnvelopeRecipient ToEnvelopeRecipient(LenderDocuSignSettings settings)
            {
                RecipientMultifactorAuthentication authentication = new RecipientMultifactorAuthentication(this.mfaChosen, this.accessCode, this.GetSubmittedPhone(), settings.IdCheckConfigurationName);

                EnvelopeRecipient recipient = new EnvelopeRecipient(
                    identifier: Guid.NewGuid(), 
                    name: this.name, 
                    email: this.email,
                    type: this.signingInPerson ? RecipientType.InPersonSigner : RecipientType.Signer,
                    note: string.Empty,
                    signingPosition: this.signingOrder,
                    authentication: authentication);

                return recipient;
            }

            /// <summary>
            /// Gets the phone number submitted for this recipient.
            /// </summary>
            /// <returns>The submitted phone number as a string.</returns>
            internal string GetSubmittedPhone()
            {
                switch (this.phone.choice)
                {
                    case RecipientViewModel.PhoneViewModel.PhoneChoice.agent:
                        return this.phone.agentPhone;
                    case RecipientViewModel.PhoneViewModel.PhoneChoice.cell:
                        return this.phone.cellPhone;
                    case RecipientViewModel.PhoneViewModel.PhoneChoice.chosen:
                        return this.phone.chosenPhone;
                    case RecipientViewModel.PhoneViewModel.PhoneChoice.company:
                        return this.phone.companyPhone;
                    case RecipientViewModel.PhoneViewModel.PhoneChoice.home:
                        return this.phone.homePhone;
                    case RecipientViewModel.PhoneViewModel.PhoneChoice.work:
                        return this.phone.workPhone;
                    default:
                        throw new UnhandledEnumException(this.phone.choice);
                }
            }
        }

        /// <summary>
        /// Defines the view model sent to the envelope UI to represent recipients.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Used only for serialization to JSON for use in the UI.")]
        private class RecipientViewModel
        {
            /// <summary>
            /// Gets or sets recipient ID.
            /// </summary>
            public int id { get; set; }

            /// <summary>
            /// Gets or sets recipient name.
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// Gets or sets recipient email.
            /// </summary>
            public string email { get; set; }

            /// <summary>
            /// Gets or sets recipient roles list.
            /// </summary>
            public IEnumerable<string> roles { get; set; }

            /// <summary>
            /// Gets or sets recipient MFA option.
            /// </summary>
            public MfaOptions? mfaChosen { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the recipient is signing in person.
            /// </summary>
            public bool signingInPerson { get; set; }

            /// <summary>
            /// Gets or sets the recipient signing order.
            /// </summary>
            public int signingOrder { get; set; }

            /// <summary>
            /// Gets or sets a container for the recipient's phone number(s).
            /// </summary>
            public PhoneViewModel phone { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this recipient is an agent from the "Agents" rolodex list.
            /// </summary>
            public bool isAgent { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether this recipient is a borrower (true), coborrower (false), or neither (null).
            /// </summary>
            public string borrowerMode { get; set; }

            /// <summary>
            /// Gets or sets the application ID of a borrower recipient.
            /// </summary>
            public Guid? appId { get; set; } = null;

            /// <summary>
            /// Creates a recipient corresponding to a borrower or coborrower.
            /// </summary>
            /// <param name="settings">The lender settings object to use for MFA options.</param>
            /// <param name="app">The loan application to get name and contact info from.</param>
            /// <param name="borrowerMode">The borrower mode (borrower or coborrower).</param>
            /// <param name="recipientIndex">The index of the created object among all recipients in the envelope.</param>
            /// <param name="appCounterSuffix">The suffix to add to the borrower roles to identify among multiple applications.</param>
            /// <returns>A new <see cref="RecipientViewModel"/> object.</returns>
            public static RecipientViewModel CreateFromApp(LenderDocuSignSettings settings, CAppData app, E_BorrowerModeT borrowerMode, int recipientIndex, string appCounterSuffix)
            {
                app.BorrowerModeT = borrowerMode;
                return new RecipientViewModel
                {
                    id = recipientIndex,
                    name = app.aNm,
                    email = app.aEmail,
                    roles = new string[] { (borrowerMode == E_BorrowerModeT.Borrower ? "Borrower" : "Coborrower") + appCounterSuffix },
                    mfaChosen = settings.DefaultMfaOption,
                    signingInPerson = settings.DefaultMfaOption == MfaOptions.NoMfa,
                    signingOrder = recipientIndex + 1,
                    isAgent = false,
                    borrowerMode = borrowerMode.ToString("G"),
                    appId = app.aAppId,
                    phone = new PhoneViewModel
                    {
                        homePhone = app.aHPhone,
                        workPhone = app.aBusPhone,
                        cellPhone = app.aCellPhone
                    }
                };
            }

            /// <summary>
            /// Defines a structure to hold recipient phone numbers.
            /// </summary>
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Used only for serialization to JSON for use in the UI")]
            public class PhoneViewModel
            {
                /// <summary>
                /// Specifies which phone number was chosen for authentication.
                /// </summary>
                public enum PhoneChoice
                {
                    home, work, agent, company, cell, chosen
                }

                /// <summary>
                /// Gets or sets the home phone number.
                /// </summary>
                public string homePhone { get; set; }

                /// <summary>
                /// Gets or sets the home phone number.
                /// </summary>
                public string agentPhone { get; set; }

                /// <summary>
                /// Gets or sets the home phone number.
                /// </summary>
                public string companyPhone { get; set; }

                /// <summary>
                /// Gets or sets the work phone number.
                /// </summary>
                public string workPhone { get; set; }

                /// <summary>
                /// Gets or sets the cell phone number.
                /// </summary>
                public string cellPhone { get; set; }

                /// <summary>
                /// Gets or sets a custom-entered phone number not on file for the recipient.
                /// </summary>
                public string chosenPhone { get; set; }

                /// <summary>
                /// Gets or sets which phone number is chosen to use for MFA.
                /// </summary>
                public PhoneChoice choice { get; set; }
            }
        }
    }
}
