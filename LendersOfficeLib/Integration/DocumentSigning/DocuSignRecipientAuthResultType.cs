﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System.ComponentModel;
    
    /// <summary>
    /// The different auth result types that a DocuSign recipient can have.
    /// </summary>
    public enum DocuSignRecipientAuthResultType
    {
        /// <summary>
        /// Access code authentication.
        /// </summary>
        [Description("Access Code")]
        AccessCode = 0,

        /// <summary>
        /// ID question authentication.
        /// </summary>
        [Description("ID Question")]
        IDQuestion = 1,

        /// <summary>
        /// ID lookup authentication.
        /// </summary>
        [Description("ID Lookup")]
        IDLookup = 2,

        /// <summary>
        /// Age verification authentication.
        /// </summary>
        [Description("Age")]
        AgeVerify = 3,

        /// <summary>
        /// Student Authentication Network authentication.
        /// </summary>
        [Description("STAN Pin")]
        STANPin = 4,

        /// <summary>
        /// Office of Foreign Asset Control authentication.
        /// </summary>
        [Description("OFAC")]
        OFAC = 5,

        /// <summary>
        /// Phone authentication.
        /// </summary>
        [Description("Phone")]
        PhoneAuth = 6,

        /// <summary>
        /// Live ID authentication.
        /// </summary>
        [Description("Live ID.")]
        LiveID = 7,

        /// <summary>
        /// Facebook authentication.
        /// </summary>
        [Description("Facebook")]
        Facebook = 8,

        /// <summary>
        /// Google authentication.
        /// </summary>
        [Description("Google")]
        Google = 9,

        /// <summary>
        /// Linkedin authentication.
        /// </summary>
        [Description("LinkedIn")]
        Linkedin = 10,

        /// <summary>
        /// Salesforce authentication.
        /// </summary>
        [Description("Salesforce")]
        Salesforce = 11,

        /// <summary>
        /// Twitter authentication.
        /// </summary>
        [Description("Twitter")]
        Twitter = 12,

        /// <summary>
        /// OpenID authentication.
        /// </summary>
        [Description("OpenID")]
        OpenID = 13,

        /// <summary>
        /// Any other social ID authentication.
        /// </summary>
        [Description("Other Social ID")]
        AnySocialID = 14,

        /// <summary>
        /// Yahoo authentication.
        /// </summary>
        [Description("Yahoo")]
        Yahoo = 15,

        /// <summary>
        /// Signature provider authentication.
        /// </summary>
        [Description("Signature Provider")]
        SignatureProvider = 16,

        /// <summary>
        /// SMS authentication.
        /// </summary>
        [Description("SMS")]
        SMS = 17
    }
}
