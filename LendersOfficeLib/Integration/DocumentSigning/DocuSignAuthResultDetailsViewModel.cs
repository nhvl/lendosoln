﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using DataAccess;

    /// <summary>
    /// Viewmodel for recipient authentication results.
    /// </summary>
    public class DocuSignAuthResultDetailsViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocuSignAuthResultDetailsViewModel"/> class.
        /// </summary>
        /// <param name="type">The authentication type.</param>
        /// <param name="status">The authentication status.</param>
        /// <param name="eventTime">The time the status was recieved.</param>
        /// <param name="failureDescription">The failure description if not successful.</param>
        public DocuSignAuthResultDetailsViewModel(DocuSignRecipientAuthResultType type, DocuSignRecipientAuthResultStatus status, DateTime eventTime, string failureDescription)
        {
            this.Type = type.GetDescription();
            this.Status = status.GetDescription();
            this.EventTime = eventTime.ToString();
            this.FailureDescription = failureDescription;
        }

        /// <summary>
        /// Gets or sets the type of authetication.
        /// </summary>
        /// <value>The type of authentication.</value>
        public string Type
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the status indicating whether it has passed or failed.
        /// </summary>
        /// <value>The status indicating whether it has passed or failed.</value>
        public string Status
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the time the authentication event happened.
        /// </summary>
        /// <value>The time the authentication event happened.</value>
        public string EventTime
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the failure description if authentication was not successful.
        /// </summary>
        /// <value>Failure description if authentication was not successful.</value>
        public string FailureDescription
        {
            get; set;
        }
    }
}
