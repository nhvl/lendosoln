﻿namespace LendersOffice.Integration.DocumentSigning
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Contains the user info data we need from DocuSign in order to issue further web requests, specifically <see cref="BaseUrl"/>.
    /// </summary>
    public class DocuSignUserInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocuSignUserInfo"/> class.
        /// </summary>
        /// <param name="userId">The user id for the account, required to be specified as the "sub" (subject) claim in the JSON Web Token.</param>
        /// <param name="defaultAccountId">The account where <c>is_default</c> is set to true.</param>
        /// <param name="baseUrl">The base URI to use when accessing <see cref="DefaultAccountId"/>.</param>
        /// <param name="name">The name of the user.</param>
        /// <param name="email">The email of the user.</param>
        public DocuSignUserInfo(string userId, string defaultAccountId, LqbAbsoluteUri baseUrl, string name, string email)
        {
            this.UserId = userId;
            this.DefaultAccountId = defaultAccountId;
            this.BaseUrl = baseUrl;
            this.Name = name;
            this.Email = email;
        }

        /// <summary>
        /// Gets the user id for the account, required to be specified as the "sub" (subject) claim in the JSON Web Token.
        /// </summary>
        public string UserId { get; }

        /// <summary>
        /// Gets the account where <c>is_default</c> is set to true.
        /// </summary>
        public string DefaultAccountId { get; }

        /// <summary>
        /// Gets the base URI to use when accessing <see cref="DefaultAccountId"/>.
        /// </summary>
        public LqbAbsoluteUri BaseUrl { get; }

        /// <summary>
        /// Gets the name of the user.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets the email of the user.
        /// </summary>
        public string Email { get; }
    }
}
