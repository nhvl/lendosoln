﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using DataAccess;

    /// <summary>
    /// Viewmodel for status dates.
    /// </summary>
    public class DocuSignStatusDatesViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocuSignStatusDatesViewModel"/> class.
        /// </summary>
        /// <param name="status">The status of the record as a string.</param>
        /// <param name="date">The date of the record.</param>
        public DocuSignStatusDatesViewModel(string status, DateTime? date)
        {
            this.Status = status;
            this.Date = date?.ToString() ?? string.Empty;
        }

        /// <summary>
        /// Gets or sets the status as a string.
        /// </summary>
        /// <value>The status as a string.</value>
        public string Status
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the date as a string.
        /// </summary>
        /// <value>The date as a string.</value>
        public string Date
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets an accompanying note for the status.
        /// </summary>
        /// <value>An accompanying note for the status.</value>
        public string Note
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the user who triggered this status.
        /// </summary>
        /// <value>The user who triggered this status.</value>
        public string DoneBy
        {
            get;
            set;
        }
    }
}
