﻿namespace LendersOffice.Integration.DocumentSigning
{
    /// <summary>
    /// The type of tab in the document.
    /// </summary>
    public enum DocumentTabType
    {
        /// <summary>
        /// A location for a signer's signature.
        /// </summary>
        SignHere = 0,

        /// <summary>
        /// A location for a signer's initials.
        /// </summary>
        InitialHere = 1,

        /// <summary>
        /// A location for the date a signer completed the signing ceremony.
        /// </summary>
        DateSigned = 2,
    }
}
