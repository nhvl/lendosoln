﻿namespace LendersOffice.Integration.DocumentSigning
{
    /// <summary>
    /// Represents the data necessary to authenticate to DocuSign.
    /// </summary>
    public class DocuSignAuthenticationData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocuSignAuthenticationData"/> class.
        /// </summary>
        /// <param name="useJsonWebToken">A value indicating whether the JSON Web Token (JWT) service authentication should be used.</param>
        /// <param name="useSandbox">A value indicating whether the DocuSign sandbox environment (demo.docusign.net) should be used.</param>
        /// <param name="integratorKey">The integrator key to use to access DocuSign.</param>
        /// <param name="userId">The user id to use to access DocuSign.</param>
        /// <param name="password">The password to use to access DocuSign.  This value is only used for DocuSign's legacy authentication model when <see cref="UseJsonWebToken"/> is false.</param>
        /// <param name="rsaPrivateKey">The private key to use when accessing DocuSign as the text content of a PEM file.  This value is only used for DocuSign's OAuth authentication model when <see cref="UseJsonWebToken"/> is true.</param>
        public DocuSignAuthenticationData(bool useJsonWebToken, bool useSandbox, string integratorKey, string userId, string password, string rsaPrivateKey)
        {
            this.UseJsonWebToken = useJsonWebToken;
            this.UseSandbox = useSandbox;
            this.IntegratorKey = integratorKey;
            this.UserId = userId;
            this.Password = password;
            this.RsaPrivateKey = rsaPrivateKey;
        }

        /// <summary>
        /// Gets a value indicating whether the JSON Web Token (JWT) service authentication should be used.
        /// </summary>
        public bool UseJsonWebToken { get; }

        /// <summary>
        /// Gets a value indicating whether the DocuSign sandbox environment (demo.docusign.net) should be used.
        /// </summary>
        public bool UseSandbox { get; }

        /// <summary>
        /// Gets the integrator key to use to access DocuSign.
        /// </summary>
        public string IntegratorKey { get; }

        /// <summary>
        /// Gets the user id to use to access DocuSign.
        /// </summary>
        public string UserId { get; }

        /// <summary>
        /// Gets the password to use to access DocuSign.  This value is only used for DocuSign's legacy authentication model when <see cref="UseJsonWebToken"/> is false.
        /// </summary>
        public string Password { get; }

        /// <summary>
        /// Gets the private key to use when accessing DocuSign as the text content of a PEM file.  This value is only used for DocuSign's OAuth authentication model when <see cref="UseJsonWebToken"/> is true.
        /// </summary>
        public string RsaPrivateKey { get; }
    }
}
