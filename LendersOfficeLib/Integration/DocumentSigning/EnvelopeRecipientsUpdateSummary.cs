﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System.Collections.Generic;
    using System.Linq;
    using Common;
    using Serialization;

    /// <summary>
    /// Class holding envelope recipients update information.
    /// </summary>
    public class EnvelopeRecipientsUpdateSummary
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnvelopeRecipientsUpdateSummary"/> class.
        /// </summary>
        /// <param name="recipients">The recipients for the envelope.</param>
        public EnvelopeRecipientsUpdateSummary(IEnumerable<EnvelopeRecipientUpdateSummary> recipients)
        {
            this.Recipients = recipients;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnvelopeRecipientsUpdateSummary"/> class.
        /// </summary>
        /// <param name="envelopeRecipients">The deserialized envelope recipients list response.</param>
        public EnvelopeRecipientsUpdateSummary(EnvelopeRecipientsList envelopeRecipients)
        {
            var recipients = new List<EnvelopeRecipientUpdateSummary>();
            recipients.AddRangeIfNotNull(envelopeRecipients.agents?.Select(record => new EnvelopeRecipientUpdateSummary(record, RecipientType.Agent)));
            recipients.AddRangeIfNotNull(envelopeRecipients.carbonCopies?.Select(record => new EnvelopeRecipientUpdateSummary(record, RecipientType.CarbonCopy)));
            recipients.AddRangeIfNotNull(envelopeRecipients.certifiedDeliveries?.Select(record => new EnvelopeRecipientUpdateSummary(record, RecipientType.CertifiedDelivery)));
            recipients.AddRangeIfNotNull(envelopeRecipients.editors?.Select(record => new EnvelopeRecipientUpdateSummary(record, RecipientType.Editor)));
            recipients.AddRangeIfNotNull(envelopeRecipients.inPersonSigners?.Select(record => new EnvelopeRecipientUpdateSummary(record, RecipientType.InPersonSigner)));
            recipients.AddRangeIfNotNull(envelopeRecipients.intermediaries?.Select(record => new EnvelopeRecipientUpdateSummary(record, RecipientType.Intermediary)));
            recipients.AddRangeIfNotNull(envelopeRecipients.seals?.Select(record => new EnvelopeRecipientUpdateSummary(record, RecipientType.Seal)));
            recipients.AddRangeIfNotNull(envelopeRecipients.signers?.Select(record => new EnvelopeRecipientUpdateSummary(record, RecipientType.Signer)));

            this.Recipients = recipients;
        }

        /// <summary>
        /// Gets the recipients for the envelope.
        /// </summary>
        public IEnumerable<EnvelopeRecipientUpdateSummary> Recipients { get; private set; }
    }
}