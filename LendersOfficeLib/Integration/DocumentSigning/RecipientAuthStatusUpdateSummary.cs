﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System.Collections.Generic;
    using Common;
    using Serialization;

    /// <summary>
    /// Class holding update information about the authentication status of a recipient.
    /// </summary>
    public class RecipientAuthStatusUpdateSummary
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RecipientAuthStatusUpdateSummary"/> class.
        /// </summary>
        /// <param name="authEvents">A collection of auth statuses.</param>
        public RecipientAuthStatusUpdateSummary(IEnumerable<AuthEventUpdateSummary> authEvents)
        {
            this.AuthEvents = authEvents;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RecipientAuthStatusUpdateSummary"/> class.
        /// </summary>
        /// <param name="authStatus">The deserialized authentication status from a deserialized envelope recipient.</param>
        public RecipientAuthStatusUpdateSummary(AuthenticationStatus authStatus)
        {
            List<AuthEventUpdateSummary> authEvents = new List<AuthEventUpdateSummary>();
            authEvents.Add(this.CreateAuthEventUpdateSummary(authStatus.accessCodeResult, DocuSignRecipientAuthResultType.AccessCode));
            authEvents.Add(this.CreateAuthEventUpdateSummary(authStatus.ageVerifyResult, DocuSignRecipientAuthResultType.AgeVerify));
            authEvents.Add(this.CreateAuthEventUpdateSummary(authStatus.anySocialIDResult, DocuSignRecipientAuthResultType.AnySocialID));
            authEvents.Add(this.CreateAuthEventUpdateSummary(authStatus.facebookResult, DocuSignRecipientAuthResultType.Facebook));
            authEvents.Add(this.CreateAuthEventUpdateSummary(authStatus.googleResult, DocuSignRecipientAuthResultType.Google));
            authEvents.Add(this.CreateAuthEventUpdateSummary(authStatus.idLookupResult, DocuSignRecipientAuthResultType.IDLookup));
            authEvents.Add(this.CreateAuthEventUpdateSummary(authStatus.idQuestionsResult, DocuSignRecipientAuthResultType.IDQuestion));
            authEvents.Add(this.CreateAuthEventUpdateSummary(authStatus.linkedinResult, DocuSignRecipientAuthResultType.Linkedin));
            authEvents.Add(this.CreateAuthEventUpdateSummary(authStatus.liveIDResult, DocuSignRecipientAuthResultType.LiveID));
            authEvents.Add(this.CreateAuthEventUpdateSummary(authStatus.ofacResult, DocuSignRecipientAuthResultType.OFAC));
            authEvents.Add(this.CreateAuthEventUpdateSummary(authStatus.openIDResult, DocuSignRecipientAuthResultType.OpenID));
            authEvents.Add(this.CreateAuthEventUpdateSummary(authStatus.phoneAuthResult, DocuSignRecipientAuthResultType.PhoneAuth));
            authEvents.Add(this.CreateAuthEventUpdateSummary(authStatus.salesforceResult, DocuSignRecipientAuthResultType.Salesforce));
            authEvents.Add(this.CreateAuthEventUpdateSummary(authStatus.signatureProviderResult, DocuSignRecipientAuthResultType.SignatureProvider));
            authEvents.Add(this.CreateAuthEventUpdateSummary(authStatus.smsAuthResult, DocuSignRecipientAuthResultType.SMS));
            authEvents.Add(this.CreateAuthEventUpdateSummary(authStatus.sTANPinResult, DocuSignRecipientAuthResultType.STANPin));
            authEvents.Add(this.CreateAuthEventUpdateSummary(authStatus.twitterResult, DocuSignRecipientAuthResultType.Twitter));
            authEvents.Add(this.CreateAuthEventUpdateSummary(authStatus.yahooResult, DocuSignRecipientAuthResultType.Yahoo));
            this.AuthEvents = authEvents;
        }

        /// <summary>
        /// Gets a collection of auth statuses.
        /// </summary>
        public IEnumerable<AuthEventUpdateSummary> AuthEvents { get; private set; }

        /// <summary>
        /// Creates an auth event update summary.
        /// </summary>
        /// <param name="eventResult">The event result to pull from.</param>
        /// <param name="type">The type of the event result.</param>
        /// <returns>The event update summary or null if the event result is null.</returns>
        private AuthEventUpdateSummary CreateAuthEventUpdateSummary(EventResult eventResult, DocuSignRecipientAuthResultType type)
        {
            return eventResult == null ? null : new AuthEventUpdateSummary(eventResult, type);
        }
    }
}
