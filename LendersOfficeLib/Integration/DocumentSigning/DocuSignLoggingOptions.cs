﻿namespace LendersOffice.Integration.DocumentSigning
{
    /// <summary>
    /// Logging options for docusign requests.
    /// </summary>
    public enum DocuSignLoggingOptions
    {
        /// <summary>
        /// Absolutely no logging done when sending request. Caller is responsible for doing logs themselves.
        /// This will still log errors, however.
        /// </summary>
        NoLogging,
        
        /// <summary>
        /// Only log the endpoint being targetted. Request/Response bodies will not be logged.
        /// This will still log errors, however.
        /// </summary>
        OnlyLogUrls,

        /// <summary>
        /// Logs everything.
        /// </summary>
        LogAll
    }
}
