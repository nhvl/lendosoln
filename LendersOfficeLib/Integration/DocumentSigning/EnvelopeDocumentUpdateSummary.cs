﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using Common;
    using LqbGrammar.DataTypes;
    using Serialization;

    /// <summary>
    /// Class holding information about a returned document.
    /// </summary>
    public class EnvelopeDocumentUpdateSummary
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnvelopeDocumentUpdateSummary"/> class.
        /// </summary>
        /// <param name="documentId">The positive integer document id. If null, this is possibly the certificate document.  Use <see cref="IsCertificate"/> to confirm that.</param>
        /// <param name="isCertificate">A value indicating whether or not this document is the certificate.</param>
        /// <param name="name">The name of the document.</param>
        /// <param name="sequenceNumber">The sequence number of the document.</param>
        /// <param name="tempFilePath">The temp file path that the document is saved to.</param>
        public EnvelopeDocumentUpdateSummary(
            int? documentId,
            bool isCertificate,
            string name,
            int? sequenceNumber,
            LocalFilePath tempFilePath)
        {
            this.DocumentId = documentId;
            this.IsCertificate = isCertificate;
            this.Name = name;
            this.SequenceNumber = sequenceNumber;
            this.TempFilePath = tempFilePath;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnvelopeDocumentUpdateSummary"/> class.
        /// </summary>
        /// <param name="document">The deserialized document.</param>
        /// <param name="tempFilePath">The temporary file path containing the document contents.</param>
        public EnvelopeDocumentUpdateSummary(EnvelopeDocument document, LocalFilePath tempFilePath)
        {
            if (document.documentId.Equals("certificate", StringComparison.OrdinalIgnoreCase))
            {
                this.DocumentId = null;
                this.IsCertificate = true;
            }
            else
            {
                this.DocumentId = document.documentId?.ToNullable<int>(int.TryParse);
            }

            this.Name = document.name;
            this.SequenceNumber = document.order?.ToNullable<int>(int.TryParse);
            this.TempFilePath = tempFilePath;
        }

        /// <summary>
        /// Gets the positive integer document id. If null, this is possibly the certificate document.
        /// Refer to <see cref="IsCertificate"/> to confirm that.
        /// </summary>
        public int? DocumentId { get; private set; }

        /// <summary>
        /// Gets a value indicating whether or not this document is the certificate.
        /// </summary>
        public bool IsCertificate { get; private set; }

        /// <summary>
        /// Gets the name of the document.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the sequence number of the document.
        /// </summary>
        /// <remarks>
        /// Reason why this is a nullable is because the response makes this look like an integer BUT
        /// the documentation states that its a string, either asc or desc, that indicates the sort order.
        /// So err on the side of caution.
        /// </remarks>
        public int? SequenceNumber { get; private set; }

        /// <summary>
        /// Gets the temp file path that the document is saved to.
        /// </summary>
        public LocalFilePath TempFilePath { get; private set; }
    }
}
