﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System.ComponentModel;

    /// <summary>
    /// Status of the envelope.
    /// </summary>
    public enum DocuSignEnvelopeStatus
    {
        /// <summary>
        /// Any status. No documentation for this one.
        /// </summary>
        [Description("Any")]
        Any = 0,

        /// <summary>
        /// The envelope has been voided by the sender.
        /// </summary>
        [Description("Voided")]
        Voided = 1,

        /// <summary>
        /// The envelope is in a draft state and has not been sent out for signing.
        /// </summary>
        [Description("Created")]
        Created = 2,

        /// <summary>
        /// This is a legacy status and is no longer used.
        /// </summary>
        [Description("Deleted")]
        Deleted = 3,

        /// <summary>
        /// An email notification with a link to the envelope has been sent to at least one recipient. The envelope remains in this state until all recipients have viewed it at a minimum.
        /// </summary>
        [Description("Sent")]
        Sent = 4,

        /// <summary>
        /// All recipients have viewed the document(s) in an envelope through the DocuSign signing web site. This is not an email delivery of the documents in an envelope.
        /// </summary>
        [Description("Delivered")]
        Delivered = 5,

        /// <summary>
        /// The envelope has been signed by all the recipients. This is a temporary state during processing, after which the envelope is automatically moved to Completed status.
        /// </summary>
        [Description("Signed")]
        Signed = 6,

        /// <summary>
        /// The envelope has been completed by all the recipients.
        /// </summary>
        [Description("Completed")]
        Completed = 7,

        /// <summary>
        /// The envelope has been declined for signing by one of the recipients.
        /// </summary>
        [Description("Declined")]
        Declined = 8,

        /// <summary>
        /// This is a legacy status and is no longer used.
        /// </summary>
        [Description("Timed Out")]
        TimedOut = 9,

        /// <summary>
        /// The envelope is a Template.
        /// </summary>
        [Description("Template")]
        Template = 10,

        /// <summary>
        /// Processing status? No documentation for this one.
        /// </summary>
        [Description("Processing")]
        Processing = 11,

        /// <summary>
        /// Some other status that we don't know about.
        /// </summary>
        [Description("Other")]
        Other = 12,

        /// <summary>
        /// No status was given back.
        /// </summary>
        [Description("N/A")]
        NA = 13
    }
}
