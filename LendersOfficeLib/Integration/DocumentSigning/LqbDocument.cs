﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Represents a document in LQB, i.e. an Electronic Document or EDoc.
    /// </summary>
    public class LqbDocument
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LqbDocument"/> class.
        /// </summary>
        /// <param name="docuSignId">The unique identifier sent to DocuSign for the document.</param>
        /// <param name="lqbDocId">The LendingQB document identifier.</param>
        /// <param name="name">The name of the document.</param>
        /// <param name="sequenceNumber">The sequence number of the document in the envelope.</param>
        /// <param name="file">The path to a local copy of the document.</param>
        /// <param name="pages">The pages of the document.</param>
        public LqbDocument(Guid docuSignId, Guid lqbDocId, string name, int sequenceNumber, LocalFilePath file, IReadOnlyList<LqbDocumentPage> pages)
        {
            this.DocuSignId = docuSignId;
            this.LqbDocId = lqbDocId;
            this.Name = name;
            this.File = file;
            this.Pages = pages;
            this.SequenceNumber = sequenceNumber;
        }

        /// <summary>
        /// Gets the unique identifier for the document sent to DocuSign.
        /// </summary>
        public Guid DocuSignId { get; }

        /// <summary>
        /// Gets the internal LendingQB doc ID we use.
        /// </summary>
        public Guid LqbDocId { get; }

        /// <summary>
        /// Gets the name of the document.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets the sequence number of the document.
        /// </summary>
        public int SequenceNumber { get; }

        /// <summary>
        /// Gets the path to a local copy of the document.
        /// </summary>
        public LocalFilePath File { get; }

        /// <summary>
        /// Gets the pages of the document.
        /// </summary>
        public IReadOnlyList<LqbDocumentPage> Pages { get; }
    }
}
