﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using Drivers.Environment;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.HttpRequest;
    using Serialization;

    /// <summary>
    /// This is where the magic happens.
    /// </summary>
    public class DocuSignAdapter
    {
        /// <summary>
        /// The header for docusign requests.
        /// </summary>
        private const string DocuSignRequestHeader = "==== DOCUSIGN REQUEST ====";

        /// <summary>
        /// The header for docusign responses.
        /// </summary>
        private const string DocuSignResponseHeader = "==== DOCUSIGN RESPONSE ====";

        /// <summary>
        /// The header for a docusign error response.
        /// </summary>
        private const string DocuSignErrorResponseHeader = "==== DOCUSIGN ERROR RESPONSE ====";

        /// <summary>
        /// Initializes a new instance of the <see cref="DocuSignAdapter"/> class.
        /// </summary>
        /// <param name="baseUrl">The base URI to use to access DocuSign's web services on this account.</param>
        /// <param name="accountId">The account id for this account.</param>
        /// <param name="authenticationHeader">The authentication header to send in any web service requests.</param>
        private DocuSignAdapter(LqbAbsoluteUri baseUrl, string accountId, KeyValuePair<string, string> authenticationHeader)
        {
            this.BaseUrl = baseUrl;
            this.AccountId = accountId;
            this.AuthenticationHeader = authenticationHeader;
        }

        /// <summary>
        /// Gets the base URI to use to access DocuSign's web services on this account.
        /// </summary>
        public LqbAbsoluteUri BaseUrl { get; }

        /// <summary>
        /// Gets the account id for this account.
        /// </summary>
        public string AccountId { get; }

        /// <summary>
        /// Gets the authentication header to send in any web service requests.
        /// </summary>
        public KeyValuePair<string, string> AuthenticationHeader { get; }

        /// <summary>
        /// Creates an authenticated instance of <see cref="DocuSignAdapter"/> or an error result if it cannot authenticate.
        /// </summary>
        /// <param name="loginData">The authentication data to use.</param>
        /// <returns>An authenticated instance of <see cref="DocuSignAdapter"/> or an error result if it cannot authenticate.</returns>
        public static Result<DocuSignAdapter> CreateAndLogin(DocuSignAuthenticationData loginData)
        {
            Result<Tuple<KeyValuePair<string, string>, DocuSignUserInfo>> loginResult = Login(loginData);
            if (loginResult.HasError)
            {
                return Result<DocuSignAdapter>.Failure(loginResult.Error);
            }

            KeyValuePair<string, string> authenticationHeader = loginResult.Value.Item1;
            DocuSignUserInfo userInfo = loginResult.Value.Item2;
            return Result<DocuSignAdapter>.Success(new DocuSignAdapter(userInfo.BaseUrl, userInfo.DefaultAccountId, authenticationHeader));
        }

        /// <summary>
        /// Creates an envelope for signing the documents or returns an error result.
        /// </summary>
        /// <param name="envelope">The data necessary to create the envelope.</param>
        /// <returns>An envelope for signing the documents or returns an error result.</returns>
        public Result<EnvelopeCreateSummary> CreateEnvelope(EnvelopeRequest envelope)
        {
            LqbAbsoluteUri url = LqbAbsoluteUri.Create(this.BaseUrl.ToString() + $"/v2/accounts/{this.AccountId}/envelopes").ForceValue();

            var options = new LqbGrammar.Drivers.HttpRequest.HttpRequestOptions();
            options.Method = HttpMethod.Post;
            options.MimeType = MimeType.Application.Json;
            options.AddHeader(this.AuthenticationHeader.Key, this.AuthenticationHeader.Value);

            Dictionary<Guid, int> documentIdsUsed = null;
            Dictionary<Guid, int> customRecipientIdsUsed = null;
            object envelopeData = CreateEnvelopeSerializableData(envelope, out documentIdsUsed, out customRecipientIdsUsed);
            options.PostData = new StringContent(SerializationHelper.JsonNetAnonymousSerialize(envelopeData));

            // Envelope logging is not straight forward so we're doing it manually.
            Dictionary<Tuple<Type, string>, string> mask = new Dictionary<Tuple<Type, string>, string>()
            {
                { new Tuple<Type, string>(null, "documentBase64"), "[Document redacted]" },
            };
            Dictionary<string, string> dictionaryMask = new Dictionary<string, string>()
            {
                { "accessCode", "MASKED" }
            };

            var maskedRequest = SerializationHelper.JsonNetSerializeWithMask(envelopeData, mask, dictionaryMask);

            Result<string> responseResult = CommunicateForString(url, options, DocuSignLoggingOptions.NoLogging);
            if (responseResult.HasError)
            {
                Log(options.Method, url, maskedRequest, "Please see error response in other logs.", isResponseError: true);
                return Result<EnvelopeCreateSummary>.Failure(responseResult.Error);
            }

            Log(options.Method, url, maskedRequest, options.ResponseBody, isResponseError: false);

            Dictionary<string, string> data = SerializationHelper.JsonNetDeserialize<Dictionary<string, string>>(options.ResponseBody);
            return Result<EnvelopeCreateSummary>.Success(new EnvelopeCreateSummary(
                identifier: data.GetValueOrNull("envelopeId"),
                statusDateTime: data.GetValueOrNull("statusDateTime").ToNullable<DateTime>(DateTime.TryParse),
                status: data.GetValueOrNull("status"),
                documentIdsUsed: documentIdsUsed,
                customRecipientIdsUsed: customRecipientIdsUsed));
        }

        /// <summary>
        /// Gets a recipient embedded signing URL from DocuSign's EnvelopeView API.
        /// </summary>
        /// <param name="request">The recipient view request object holding data to send to DocuSign.</param>
        /// <returns>The recipient's view URL or returns an error result.</returns>
        public Result<LqbAbsoluteUri> GetRecipientViewUrl(RecipientViewRequest request)
        {
            var envelopeId = this.ValidateEnvelopeId(request.EnvelopeId);
            if (envelopeId.HasError)
            {
                return Result<LqbAbsoluteUri>.Failure(envelopeId.Error);
            }

            LqbAbsoluteUri url = LqbAbsoluteUri.Create(this.BaseUrl.ToString() + $"/v2/accounts/{this.AccountId}/envelopes/{envelopeId.Value}/views/recipient").ForceValue();

            var options = new HttpRequestOptions();
            options.Method = HttpMethod.Post;
            options.MimeType = MimeType.Application.Json;
            options.AddHeader(this.AuthenticationHeader.Key, this.AuthenticationHeader.Value);

            string postBody = SerializationHelper.JsonNetAnonymousSerialize(request.ToSerializableData());
            options.PostData = new StringContent(postBody);
            Result<string> responseResult = CommunicateForString(url, options, DocuSignLoggingOptions.LogAll, postBody);
            if (responseResult.HasError)
            {
                return Result<LqbAbsoluteUri>.Failure(responseResult.Error);
            }

            Dictionary<string, string> data = SerializationHelper.JsonNetDeserialize<Dictionary<string, string>>(options.ResponseBody);
            LqbAbsoluteUri? uri = LqbAbsoluteUri.Create(data["url"]);
            if (uri.HasValue)
            {
                return Result<LqbAbsoluteUri>.Success(uri.Value);
            }
            else
            {
                return Result<LqbAbsoluteUri>.Failure(new Exception($"Could not parse {data["url"]} as a URI."));
            }
        }

        /// <summary>
        /// Voids an existing envelope in DocuSign's system.
        /// </summary>
        /// <param name="envelopeId">The envelope's GUID.</param>
        /// <param name="voidedReason">The reason the envelope was voided.</param>
        /// <returns>A success or failure result.</returns>
        public Result<DBNull> VoidEnvelope(string envelopeId, string voidedReason)
        {
            string validatedEnvelopeId = envelopeId?.ToNullable<Guid>(Guid.TryParse).ToString(); // per docs, this should be a Guid, and we cut the time for semantic types
            if (string.IsNullOrEmpty(validatedEnvelopeId))
            {
                return Result<DBNull>.Failure(DataAccess.CBaseException.GenericException("Invalid Envelope id [" + envelopeId + "]"));
            }

            LqbAbsoluteUri url = LqbAbsoluteUri.Create(this.BaseUrl.ToString() + $"/v2/accounts/{this.AccountId}/envelopes/{validatedEnvelopeId}").ForceValue();

            var options = new LqbGrammar.Drivers.HttpRequest.HttpRequestOptions();
            options.Method = HttpMethod.Put;
            options.MimeType = MimeType.Application.Json;
            options.AddHeader(this.AuthenticationHeader.Key, this.AuthenticationHeader.Value);

            string postBody = SerializationHelper.JsonNetAnonymousSerialize(new { status = "voided", voidedReason });
            options.PostData = new StringContent(postBody);
            Result<string> responseResult = CommunicateForString(url, options, DocuSignLoggingOptions.LogAll, postBody);
            if (responseResult.HasError)
            {
                return Result<DBNull>.Failure(responseResult.Error);
            }

            return Result<DBNull>.Success(DBNull.Value);
        }

        /// <summary>
        /// Gets an envelope from DocuSign's system.
        /// </summary>
        /// <param name="envelopeId">The envelope id.</param>
        /// <returns>The envelope returned in the response or error result.</returns>
        public Result<EnvelopeUpdateSummary> GetEnvelope(string envelopeId)
        {
            var validatedEnvelopeId = this.ValidateEnvelopeId(envelopeId);
            if (validatedEnvelopeId.HasError)
            {
                return Result<EnvelopeUpdateSummary>.Failure(validatedEnvelopeId.Error);
            }

            var envelopeEndpoint = $"/v2/accounts/{this.AccountId}/envelopes/{validatedEnvelopeId.Value}";
            LqbAbsoluteUri envelopeUrl = LqbAbsoluteUri.Create(this.BaseUrl.ToString() + envelopeEndpoint).ForceValue();
            var options = this.CreateGetHttpRequestOptions();

            Result<string> envelopeGetResponse = CommunicateForString(envelopeUrl, options, DocuSignLoggingOptions.LogAll);
            if (envelopeGetResponse.HasError)
            {
                return Result<EnvelopeUpdateSummary>.Failure(envelopeGetResponse.Error);
            }

            EnvelopeGet deserializedEnvelope = SerializationHelper.JsonNetDeserialize<EnvelopeGet>(envelopeGetResponse.Value);
            EnvelopeUpdateSummary envelopeSummary = new EnvelopeUpdateSummary(deserializedEnvelope);

            var recipientsSummary = this.GetRecipients(validatedEnvelopeId.Value);
            if (recipientsSummary.HasError)
            {
                return Result<EnvelopeUpdateSummary>.Failure(recipientsSummary.Error);
            }

            envelopeSummary.Recipients = recipientsSummary.Value;

            EnvelopeDocumentListUpdateSummary documentsSummary = null;
            if (envelopeSummary.Status == DocuSignEnvelopeStatus.Completed)
            {
                var documentGetResult = this.GetDocuments(envelopeId);
                if (documentGetResult.HasError)
                {
                    return Result<EnvelopeUpdateSummary>.Failure(documentGetResult.Error);
                }

                documentsSummary = documentGetResult.Value;
            }

            envelopeSummary.Documents = documentsSummary;
            return Result<EnvelopeUpdateSummary>.Success(envelopeSummary);
        }

        /// <summary>
        /// Gets the recipients of the envelope.
        /// </summary>
        /// <param name="envelopeId">The envelope.</param>
        /// <returns>The recipients of the envelope or error results.</returns>
        public Result<EnvelopeRecipientsUpdateSummary> GetRecipients(string envelopeId)
        {
            var validatedEnvelopeId = this.ValidateEnvelopeId(envelopeId);
            if (validatedEnvelopeId.HasError)
            {
                return Result<EnvelopeRecipientsUpdateSummary>.Failure(validatedEnvelopeId.Error);
            }

            LqbAbsoluteUri recipientUrl = LqbAbsoluteUri.Create($"{this.BaseUrl.ToString()}/v2/accounts/{this.AccountId}/envelopes/{validatedEnvelopeId.Value}/recipients").ForceValue();
            var options = this.CreateGetHttpRequestOptions();

            // Recipient results have some sensitive information, so we'll log ourselves
            Result<string> recipientsGetResponse = CommunicateForString(recipientUrl, options, DocuSignLoggingOptions.NoLogging);
            if (recipientsGetResponse.HasError)
            {
                Log(options.Method, recipientUrl, "No POST body", "Please see error response in other logs", isResponseError: true);
                return Result<EnvelopeRecipientsUpdateSummary>.Failure(recipientsGetResponse.Error);
            }

            EnvelopeRecipientsList deserializedRecipients = SerializationHelper.JsonNetDeserialize<EnvelopeRecipientsList>(recipientsGetResponse.Value);
            
            Dictionary<Tuple<Type, string>, string> mask = new Dictionary<Tuple<Type, string>, string>()
            {
                { new Tuple<Type, string>(typeof(Serialization.EnvelopeRecipient), "accessCode"), "MASKED" }
            };

            Log(options.Method, recipientUrl, "No POST body", SerializationHelper.JsonNetSerializeWithMask(deserializedRecipients, mask, null), isResponseError: false);
            return Result<EnvelopeRecipientsUpdateSummary>.Success(new EnvelopeRecipientsUpdateSummary(deserializedRecipients));
        }

        /// <summary>
        /// Gets the documents of an envelope.
        /// </summary>
        /// <param name="envelopeId">The envelope id.</param>
        /// <returns>The documents or an error result.</returns>
        public Result<EnvelopeDocumentListUpdateSummary> GetDocuments(string envelopeId)
        {
            var validatedEnvelopeId = this.ValidateEnvelopeId(envelopeId);
            if (validatedEnvelopeId.HasError)
            {
                return Result<EnvelopeDocumentListUpdateSummary>.Failure(validatedEnvelopeId.Error);
            }

            LqbAbsoluteUri documentListUrl = LqbAbsoluteUri.Create($"{this.BaseUrl.ToString()}/v2/accounts/{this.AccountId}/envelopes/{validatedEnvelopeId.Value}/documents").ForceValue();
            var options = this.CreateGetHttpRequestOptions();

            Result<string> documentListGetResponse = CommunicateForString(documentListUrl, options, DocuSignLoggingOptions.LogAll);
            if (documentListGetResponse.HasError)
            {
                return Result<EnvelopeDocumentListUpdateSummary>.Failure(documentListGetResponse.Error);
            }

            var deserializedDocumentList = SerializationHelper.JsonNetDeserialize<EnvelopeDocumentsList>(documentListGetResponse.Value);

            List<EnvelopeDocumentUpdateSummary> documentList = new List<EnvelopeDocumentUpdateSummary>();
            foreach (var deserializedDoc in deserializedDocumentList.envelopeDocuments)
            {
                var documentUrl = LqbAbsoluteUri.Create($"{this.BaseUrl.ToString()}/v2/accounts/{this.AccountId}{deserializedDoc.uri}").ForceValue();
                var tempFilePath = LqbGrammar.Utils.TempFileUtils.NewTempFilePath();
                options = this.CreateGetHttpRequestOptions(); // Safer to make a new one since we do store things back in this object from the response.
                options.ResponseFileName = tempFilePath;

                var results = CommunicateForString(documentUrl, options, DocuSignLoggingOptions.LogAll);
                if (results.HasError)
                {
                    return Result<EnvelopeDocumentListUpdateSummary>.Failure(results.Error);
                }

                documentList.Add(new EnvelopeDocumentUpdateSummary(deserializedDoc, tempFilePath));
            }

            return Result<EnvelopeDocumentListUpdateSummary>.Success(new EnvelopeDocumentListUpdateSummary(documentList));
        }

        /// <summary>
        /// Escapes a value in a query string.
        /// </summary>
        /// <param name="value">The value to escape.</param>
        /// <returns>The escaped string.</returns>
        internal static string EscapeQueryStringValue(string value)
        {
            return Uri.EscapeDataString(value); // a simple alias, but there's a lot of competing implementations in .NET, so this will standardize it here.
        }

        /// <summary>
        /// Wraps the driver for web requests to handle error responses by catching the exception.
        /// </summary>
        /// <param name="url">The target URL.</param>
        /// <param name="options">The options that control the request and response parsing.</param>
        /// <param name="loggingOption">The logging options for this request.</param>
        /// <param name="postData">The data that is being posted, if any.</param>
        /// <returns>A successful response string, or an error result.</returns>
        /// <remarks>
        /// This is internal specifically to share with the other DocuSign adapters for authentication.
        /// </remarks>
        internal static Result<string> CommunicateForString(LqbAbsoluteUri url, HttpRequestOptions options, DocuSignLoggingOptions loggingOption, string postData = null)
        {
            string requestBody = string.Empty;
            string responseBody = string.Empty;
            try
            {
                if (loggingOption == DocuSignLoggingOptions.OnlyLogUrls)
                {
                    requestBody = "[RequestBody redacted for security reasons.]";
                }
                else if (loggingOption == DocuSignLoggingOptions.LogAll)
                {
                    requestBody = string.IsNullOrEmpty(postData) ? "No POST data" : postData;
                }

                bool successful = Drivers.HttpRequest.WebRequestHelper.ExecuteCommunication(url, options);
                if (successful)
                {
                    if (loggingOption == DocuSignLoggingOptions.OnlyLogUrls)
                    {
                        responseBody = "[ResponseBody redacted for security reasons.]";
                    }
                    else if (loggingOption == DocuSignLoggingOptions.LogAll)
                    {
                        responseBody = string.IsNullOrEmpty(options.ResponseBody) ? "No response body" : options.ResponseBody;
                    }

                    if (loggingOption != DocuSignLoggingOptions.NoLogging)
                    {
                        Log(options.Method, url, requestBody, responseBody, isResponseError: false);
                    }

                    return Result<string>.Success(options.ResponseBody);
                }

                Log(options.Method, url, requestBody, "Unable to read response.", isResponseError: true);
                return Result<string>.Failure(DataAccess.CBaseException.GenericException("Unable to read response from " + url));
            }
            catch (LqbGrammar.Exceptions.LqbException exception) when (exception.InnerException is System.Net.WebException)
            {
                var webException = (System.Net.WebException)exception.InnerException;
                System.Net.HttpWebResponse response = webException.Response as System.Net.HttpWebResponse;
                if (response == null)
                {
                    return Result<string>.Failure(exception);
                }

                string responseString = null;
                using (System.IO.Stream responseStream = webException.Response.GetResponseStream())
                using (var streamReader = new System.IO.StreamReader(responseStream))
                {
                    responseString = streamReader.ReadToEnd();
                }

                Log(options.Method, url, requestBody, responseString, isResponseError: true);

                string debugString = response.StatusCode.ToString("D") + " " + response.StatusCode + Drivers.Environment.EnvironmentHelper.NewLine + responseString;
                return Result<string>.Failure(DataAccess.CBaseException.GenericException(debugString));
            }
        }

        /// <summary>
        /// Logs the request.
        /// </summary>
        /// <param name="method">The method type.</param>
        /// <param name="url">The url we're targetting.</param>
        /// <param name="requestBody">The request body.</param>
        /// <param name="responseBody">The response body.</param>
        /// <param name="isResponseError">Whether the response is an error or not.</param>
        internal static void Log(HttpMethod method, LqbAbsoluteUri url, string requestBody, string responseBody, bool isResponseError)
        {
            System.Text.StringBuilder logger = new System.Text.StringBuilder();
            string endpoint = $"{method.ToString()} {url.ToString()}";
            logger.AppendLine(endpoint);

            logger.AppendLine(DocuSignRequestHeader);
            logger.AppendLine(requestBody);

            logger.AppendLine(isResponseError ? DocuSignErrorResponseHeader : DocuSignResponseHeader);
            logger.AppendLine(responseBody);

            Tools.LogInfo(logger.ToString());
        }

        /// <summary>
        /// Logs in to the specified account via DocuSign's web services.
        /// </summary>
        /// <param name="loginData">The data to use for authentication with DocuSign.</param>
        /// <returns>The authentication header to send in any web service requests, or an error result.</returns>
        private static Result<Tuple<KeyValuePair<string, string>, DocuSignUserInfo>> Login(DocuSignAuthenticationData loginData)
        {
            if (loginData.UseJsonWebToken)
            {
                var oauthAdapter = new DocuSignOAuthServiceAdapter();
                return oauthAdapter.GetAuthHeaderAndUserInfo(loginData);
            }

            var legacyAdapter = new DocuSignLegacyAuthServiceAdapter();
            return legacyAdapter.GetAuthHeaderAndUserInfo(loginData);
        }

        /// <summary>
        /// Creates the serializable object containing an envelope.
        /// </summary>
        /// <param name="envelope">The envelope data to send.</param>
        /// <param name="documentIdsUsed">The document ids that are going to be sent to DocuSign.</param>
        /// <param name="customRecipientIdsUsed">The custom recipient ids used.</param>
        /// <returns>An object supporting basic serialization to JSON.</returns>
        private static object CreateEnvelopeSerializableData(EnvelopeRequest envelope, out Dictionary<Guid, int> documentIdsUsed, out Dictionary<Guid, int> customRecipientIdsUsed)
        {
            int documentIdCounter = 0;
            var documentIdLookup = new Dictionary<Guid, int>();
            foreach (var documentToSend in envelope.Documents)
            {
                documentIdLookup.Add(documentToSend.DocuSignId, ++documentIdCounter);
            }

            documentIdsUsed = documentIdLookup;

            int customRecipientIdCounter = 0;
            List<object> signers = new List<object>();
            List<object> inPersonSigners = new List<object>();
            List<object> carbonCopies = new List<object>();
            customRecipientIdsUsed = new Dictionary<Guid, int>();
            foreach (EnvelopeRecipient recipientToSend in envelope.Recipients)
            {
                int customRecipientId = ++customRecipientIdCounter;
                customRecipientIdsUsed.Add(recipientToSend.Identifier, customRecipientId);

                IEnumerable<DocumentTab> tabsForRecipientToSend = envelope.Tabs.Where(t => t.RecipientIdentifier == recipientToSend.Identifier);
                object tabsForRecipient = CreateRecipientTabsSerializableData(
                    tabsForRecipientToSend,
                    t => CreateTabSerializableData(t, customRecipientId, documentIdLookup[t.DocumentIdentifier]));
                object recipient = CreateRecipientSerializableData(recipientToSend, customRecipientId, tabsForRecipient, envelope.DocuSignUserName, envelope.DocuSignUserEmail);
                switch (recipientToSend.Type)
                {
                    case RecipientType.Signer:
                        signers.Add(recipient);
                        break;
                    case RecipientType.InPersonSigner:
                        inPersonSigners.Add(recipient);
                        break;
                    case RecipientType.CarbonCopy:
                        carbonCopies.Add(recipient);
                        break;
                    default:
                        throw new DataAccess.UnhandledEnumException(recipientToSend.Type);
                }
            }

            return new
            {
                brandId = envelope.DocuSignBrandId?.ToString(),
                emailSubject = envelope.EmailSubject,
                emailBlurb = envelope.EmailBody,
                eventNotification = CreateEventNotificationSerializableData(envelope.WebhookUrl),
                recipients = new { carbonCopies, inPersonSigners, signers },
                documents = envelope.Documents.Select(d => CreateDocumentSerializableData(d, documentIdLookup[d.DocuSignId])),
                status = "sent" // either "sent" (telling DocuSign to actually send) or "created" (telling DocuSign this is a draft)
            };
        }

        /// <summary>
        /// Creates the serializable object containing a document.
        /// </summary>
        /// <param name="documentToSend">The document to send.</param>
        /// <param name="documentId">The id of the document.</param>
        /// <returns>An object supporting basic serialization to JSON.</returns>
        private static object CreateDocumentSerializableData(LqbDocument documentToSend, int documentId)
        {
            return new
            {
                documentId,
                name = documentToSend.Name,
                documentBase64 = Convert.ToBase64String(Drivers.FileSystem.BinaryFileHelper.ReadAllBytes(documentToSend.File)),
                order = documentToSend.SequenceNumber
            };
        }

        /// <summary>
        /// Creates the serializable object containing the event notification configuration to receive updates.
        /// </summary>
        /// <param name="lqbWebhookPath">The web hook URL to receive asynchronous updates about the created envelope.</param>
        /// <returns>An object supporting basic serialization to JSON.</returns>
        private static object CreateEventNotificationSerializableData(LqbAbsoluteUri lqbWebhookPath)
        {
            var eventNotification = new Dictionary<string, object>
            {
                ["includeCertificateOfCompletion"] = "true",
                ["includeDocuments"] = "true",
                ["includeEnvelopeVoidReason"] = "true",
                ["includeTimeZone"] = "true",
                ["loggingEnabled"] = "true",
                ["requireAcknowledgment"] = "true",
                ["signMessageWithX509Cert"] = "true",
                ["url"] = lqbWebhookPath.ToString(),
                ["envelopeEvents"] = new[]
                {
                    new { envelopeEventStatusCode = "Sent" },
                    new { envelopeEventStatusCode = "Delivered" },
                    new { envelopeEventStatusCode = "Completed" },
                    new { envelopeEventStatusCode = "Declined" },
                    new { envelopeEventStatusCode = "Voided" },
                },
                ["recipientEvents"] = new[]
                {
                    new { recipientEventStatusCode = "Sent" },
                    new { recipientEventStatusCode = "Delivered" },
                    new { recipientEventStatusCode = "Completed" },
                    new { recipientEventStatusCode = "Declined" },
                    new { recipientEventStatusCode = "AuthenticationFailed" },
                    new { recipientEventStatusCode = "AutoResponded" },
                },
            };

            return eventNotification;
        }

        /// <summary>
        /// Creates the serializable object containing a recipient.
        /// </summary>
        /// <param name="recipientToSend">The recipient to send.</param>
        /// <param name="customRecipientId">The id of the recipient.</param>
        /// <param name="tabsForRecipientToSend">The serializable data representing the tabs for the recipient.</param>
        /// <param name="docuSignUserName">The DocuSign user name to send for an in-person signer.</param>
        /// <param name="docuSignUserEmail">The DocuSign user email to send for an in-person signer.</param>
        /// <returns>An object supporting basic serialization to JSON.</returns>
        private static object CreateRecipientSerializableData(EnvelopeRecipient recipientToSend, int customRecipientId, object tabsForRecipientToSend, string docuSignUserName, string docuSignUserEmail)
        {
            var recipientData = new Dictionary<string, object>
            {
                ["recipientId"] = customRecipientId.ToString(),
                ["customFields"] = new string[] { $"r_{customRecipientId.ToString()}" },
                ["routingOrder"] = recipientToSend.SigningPosition.ToString(),
                ["note"] = recipientToSend.Note,
            };

            if ((recipientToSend.Authentication?.InitialMfaOption == Admin.DocuSign.MfaOptions.AccessCode || recipientToSend.Authentication?.InitialMfaOption == Admin.DocuSign.MfaOptions.AccessCodeAndIdCheck)
                && !string.IsNullOrEmpty(recipientToSend.Authentication?.AccessCode))
            {
                recipientData["accessCode"] = recipientToSend.Authentication.AccessCode;
            }

            if ((recipientToSend.Authentication?.InitialMfaOption == Admin.DocuSign.MfaOptions.IdCheck || recipientToSend.Authentication?.InitialMfaOption == Admin.DocuSign.MfaOptions.AccessCodeAndIdCheck)
                && !string.IsNullOrEmpty(recipientToSend.Authentication?.IdCheckConfiguration))
            {
                recipientData["idCheckConfigurationName"] = "ID Check $";
                recipientData["requireIdLookup"] = true;
            }

            if ((recipientToSend.Authentication?.InitialMfaOption == Admin.DocuSign.MfaOptions.Phone || recipientToSend.Authentication?.InitialMfaOption == Admin.DocuSign.MfaOptions.Sms) && !string.IsNullOrEmpty(recipientToSend.Authentication?.PhoneNumber))
            {
                recipientData[recipientToSend.Authentication?.InitialMfaOption == Admin.DocuSign.MfaOptions.Phone ? "phoneAuthentication" : "smsAuthentication"] = new Dictionary<string, object>
                {
                    ["senderProvidedNumbers"] = new string[] { recipientToSend.Authentication.PhoneNumber }
                };
                recipientData["idCheckConfigurationName"] = recipientToSend.Authentication?.InitialMfaOption == Admin.DocuSign.MfaOptions.Phone ? "Phone Auth $" : "SMS Auth $";
                recipientData["requireIdLookup"] = true;
            }

            if (tabsForRecipientToSend != null)
            {
                recipientData["tabs"] = tabsForRecipientToSend;
            }

            if (recipientToSend.Type == RecipientType.InPersonSigner)
            {
                recipientData["inPersonSigningType"] = "inPersonSigner";
                recipientData["hostName"] = docuSignUserName;
                recipientData["hostEmail"] = docuSignUserEmail;
                recipientData["signerEmail"] = recipientToSend.Email;
                recipientData["signerName"] = recipientToSend.Name;
                recipientData["clientUserId"] = recipientToSend.Identifier;
            }
            else
            {
                recipientData["email"] = recipientToSend.Email;
                recipientData["name"] = recipientToSend.Name;
            }

            return recipientData;
        }

        /// <summary>
        /// Creates the serializable object containing a set of tabs.
        /// </summary>
        /// <param name="tabsToSend">The tabs to send.</param>
        /// <param name="createTabSerializableData">The creation operation for tabs.</param>
        /// <returns>An object supporting basic serialization to JSON.</returns>
        private static object CreateRecipientTabsSerializableData(IEnumerable<DocumentTab> tabsToSend, Func<DocumentTab, object> createTabSerializableData)
        {
            List<object> dateSignedTabs = new List<object>();
            List<object> initialHereTabs = new List<object>();
            List<object> signHereTabs = new List<object>();
            bool any = false;
            foreach (DocumentTab tabToSend in tabsToSend)
            {
                any = true;
                object tabSerializableData = createTabSerializableData(tabToSend);
                switch (tabToSend.Type)
                {
                    case DocumentTabType.DateSigned:
                        dateSignedTabs.Add(tabSerializableData);
                        break;
                    case DocumentTabType.InitialHere:
                        initialHereTabs.Add(tabSerializableData);
                        break;
                    case DocumentTabType.SignHere:
                        signHereTabs.Add(tabSerializableData);
                        break;
                    default:
                        throw new DataAccess.UnhandledEnumException(tabToSend.Type);
                }
            }

            return any ? new { dateSignedTabs, initialHereTabs, signHereTabs } : null;
        }

        /// <summary>
        /// Creates the serializable object containing a tab.
        /// </summary>
        /// <param name="tabToSend">The tab data to send.</param>
        /// <param name="customRecipientId">The id of the recipient for the tab.</param>
        /// <param name="documentId">The id of the document for the tab.</param>
        /// <returns>An object supporting basic serialization to JSON.</returns>
        private static object CreateTabSerializableData(DocumentTab tabToSend, int customRecipientId, int documentId)
        {
            var tabData = new Dictionary<string, object>
            {
                ["documentId"] = documentId.ToString(),
                ["pageNumber"] = tabToSend.PageNumber.ToString(),
                ["recipientId"] = customRecipientId.ToString(),
                ["xPosition"] = tabToSend.XPosition.ToString(),
                ["yPosition"] = tabToSend.YPosition.ToString(),
            };

            if (tabToSend.Type == DocumentTabType.SignHere || tabToSend.Type == DocumentTabType.InitialHere)
            {
                tabData["scaleValue"] = tabToSend.Scale.ToString();
                tabData["optional"] = "false";
            }

            return tabData;
        }

        /// <summary>
        /// Validates the envelope id.
        /// </summary>
        /// <param name="envelopeId">The envelope id to parse.</param>
        /// <returns>Validated envelope id or error result.</returns>
        private Result<string> ValidateEnvelopeId(string envelopeId)
        {
            string validatedEnvelopeId = envelopeId?.ToNullable<Guid>(Guid.TryParse).ToString();
            if (string.IsNullOrEmpty(validatedEnvelopeId))
            {
                return Result<string>.Failure(DataAccess.CBaseException.GenericException("Invalid Envelope id [" + envelopeId + "]"));
            }

            return Result<string>.Success(validatedEnvelopeId);
        }

        /// <summary>
        /// Creates an HttpRequestOptions object for a GET request.
        /// </summary>
        /// <returns>The request options with basic settings for a GET request.</returns>
        private HttpRequestOptions CreateGetHttpRequestOptions()
        {
            var options = new HttpRequestOptions();
            options.Method = HttpMethod.Get;
            options.AddHeader(this.AuthenticationHeader.Key, this.AuthenticationHeader.Value);

            return options;
        }
    }
}
