﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using EDocs;

    /// <summary>
    /// Gets document data for the DocuSign integration.
    /// </summary>
    public static class DocuSignDocumentData
    {
        /// <summary>
        /// Gets an object containing the necessary UI information for the given list of documents.
        /// </summary>
        /// <param name="documents">The documents to load data for.</param>
        /// <param name="loan">The loan the documents are from.</param>
        /// <returns>An object list to be serialized and sent to the UI.</returns>
        public static List<Dictionary<string, object>> GetDocumentObjects(IEnumerable<EDocument> documents, CPageData loan)
        {
            int docsWithBorrowers = documents.Count(doc => doc.HasESignTags && doc.ESignTags.AnnotationList.Any(annotation => annotation.AssociatedBorrower != null));
            if (docsWithBorrowers > 1)
            {
                var groups = documents.Where(doc => doc.HasESignTags)
                    .GroupBy(doc => doc.AppId ?? loan.GetAppData(0).aAppId);

                // Order the groups the way they appear on the loan
                var groupsInOrder = loan.Apps.Select(app => groups.FirstOrDefault(group => group.Key == app.aAppId)).Where(app => app != null);
                return groupsInOrder.Select((grouping, index) => CreateDocObjectsForApp(loan, grouping, appIndex: groups.Count() > 1 ? index : (int?)null))
                    .SelectMany(i => i)
                    .ToList();
            }
            else
            {
                return CreateDocObjectsForApp(loan, documents, null).ToList();
            }
        }

        /// <summary>
        /// Generates the EDocument viewmodel objects for a single loan application.
        /// </summary>
        /// <param name="loan">The loan data object to load application info from.</param>
        /// <param name="docs">An enumerable of docs belonging to the app.</param>
        /// <param name="appIndex">The index of the application among applications to be included.</param>
        /// <returns>An enumerable list of document viewmodel objects.</returns>
        private static IEnumerable<Dictionary<string, object>> CreateDocObjectsForApp(CPageData loan, IEnumerable<EDocument> docs, int? appIndex)
        {
            foreach (EDocument doc in docs)
            {
                yield return CreateDocDictionary(loan, doc, appIndex);
            }
        }

        /// <summary>
        /// Generates a single EDocument viewmodel object.
        /// </summary>
        /// <param name="loan">The loan data object to load application info from.</param>
        /// <param name="doc">A document.</param>
        /// <param name="appIndex">The index of the application that the doc belongs to.</param>
        /// <returns>A document viewmodel object.</returns>
        private static Dictionary<string, object> CreateDocDictionary(CPageData loan, EDocument doc, int? appIndex)
        {
            return new Dictionary<string, object>
            {
                ["docId"] = doc.DocumentId,
                ["status"] = EDocument.StatusText(doc.DocStatus),
                ["folder"] = doc.Folder.FolderNm,
                ["docType"] = doc.DocType.DocTypeName,
                ["borrowers"] = loan.GetAppData(doc.AppId ?? Guid.Empty)?.aBNm_aCNm,
                ["signingRoles"] = GetDocumentSigningRoles(doc, loan, appIndex),
                ["description"] = doc.PublicDescription,
                ["comments"] = doc.InternalDescription,
                ["lastModified"] = doc.LastModifiedDate,
                ["isPreppedForEditing"] = doc.CanRead() && doc.ImageStatus == E_ImageStatus.HasCachedImages
            };
        }

        /// <summary>
        /// Gets a list of the distinct roles assigned to sign a document.
        /// </summary>
        /// <param name="document">The document to determine signing roles for.</param>
        /// <param name="loan">The loan associated with the document.</param>
        /// <param name="appIndex">The index of the application to display for borrower / coborrower roles.</param>
        /// <returns>A list of roles in string form.</returns>
        private static List<string> GetDocumentSigningRoles(EDocument document, CPageData loan, int? appIndex)
        {
            var tags = document.ESignTags.AnnotationList.Select(annotation =>
                annotation.AssociatedBorrower != null
                    ? $"{annotation.RecipientDescription}{(appIndex == null ? string.Empty : " " + (appIndex + 1))}"
                    : annotation.RecipientDescription).Distinct();
            CAppData associatedApp = document.AppId != null ? loan.GetAppData(document.AppId.Value) : loan.GetAppData(0);
            if (!associatedApp.aHasSpouse)
            {
                tags = tags.Where(role => !role.StartsWith("Coborrower", StringComparison.OrdinalIgnoreCase));
            }

            return tags.ToList();
        }
    }
}
