﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;
    using DataAccess.Utilities;
    using Drivers.SqlServerDB;
    using EDocs;
    using LqbGrammar.DataTypes;
    using ObjLib.Edocs.AutoSaveDocType;

    /// <summary>
    /// Contains information about the EDocs associated with a DocuSign envelope.
    /// </summary>
    public class DocuSignEdoc
    {
        /// <summary>
        /// The stored procedure to create a new docusign edoc record.
        /// </summary>
        private static readonly StoredProcedureName DocuSignEdocsCreate = StoredProcedureName.Create("DOCUSIGN_EDOCS_Create").Value;

        /// <summary>
        /// The stored procedure to update a DocuSign edoc record.
        /// </summary>
        private static readonly StoredProcedureName DocuSignEdocsUpdate = StoredProcedureName.Create("DOCUSIGN_EDOCS_Update").Value;

        /// <summary>
        /// Whether this record has not been saved to the DB yet.
        /// </summary>
        private bool isNew;

        /// <summary>
        /// Prevents a default instance of the <see cref="DocuSignEdoc"/> class from being created.
        /// </summary>
        private DocuSignEdoc()
        {
            this.isNew = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DocuSignEdoc"/> class.
        /// </summary>
        /// <param name="record">The record to load from.</param>
        private DocuSignEdoc(IDataRecord record)
        {
            this.Id = (int)record["Id"];
            this.EnvelopeReferenceId = (int)record["EnvelopeReferenceId"];
            this.SequenceNumber = (int)record["SequenceNumber"];
            this.FromDocuSign = (bool)record["FromDocuSign"];
            this.EdocId = record.AsNullableGuid("EdocId");
            this.Name = (string)record["Name"];
            this.DocumentId = record.AsNullableInt("DocumentId");
            this.IsCertificate = (bool)record["IsCertificate"];
        }

        /// <summary>
        /// Gets the id of the envelope that this edoc is associated with.
        /// </summary>
        /// <value>The id of the envelope that this edoc is associated with.</value>
        public int EnvelopeReferenceId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the id of the record.
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Gets the sequence number of the EDoc in the envelope.
        /// </summary>
        /// <value>The sequence number of the Edoc.</value>
        public int SequenceNumber
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the positive integer document id as passed to DocuSign (or received from DocuSign if it was added post-creation).
        /// If null, this document is a bit broken and has no document id.
        /// </summary>
        public int? DocumentId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether this document is the certificate document.
        /// </summary>
        public bool IsCertificate
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Gets a value indicating whether the EDoc has been signed and returned from DocuSign.
        /// True == Signed and returned from DocuSign.
        /// False == Document was sent to DocuSign for e-signing.
        /// </summary>
        /// <value>Whether the Edoc has been signed and returned from DocuSign.</value>
        public bool FromDocuSign
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Gets the Edoc id.
        /// </summary>
        /// <value>The Edoc id.</value>
        public Guid? EdocId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the name of the EDoc.
        /// </summary>
        /// <value>The name of the EDoc.</value>
        public string Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Creates records from the documents sent over to DocuSign.
        /// </summary>
        /// <param name="request">The envelope request containing the sent documents.</param>
        /// <param name="creationSummary">The tracked data used in envelope creation.</param>
        /// <returns>The sent documents.</returns>
        internal static List<DocuSignEdoc> CreateSentDocuments(EnvelopeRequest request, EnvelopeCreateSummary creationSummary)
        {
            List<DocuSignEdoc> documentRecords = new List<DocuSignEdoc>();
            var sentDocuments = request.Documents;
            foreach (var document in sentDocuments)
            {
                DocuSignEdoc docRecord = new DocuSignEdoc();
                docRecord.EdocId = document.LqbDocId;
                docRecord.Name = document.Name;
                docRecord.FromDocuSign = false;
                docRecord.SequenceNumber = document.SequenceNumber;
                docRecord.DocumentId = creationSummary.DocumentIdsUsed[document.DocuSignId];
                docRecord.IsCertificate = false;

                documentRecords.Add(docRecord);
            }

            return documentRecords;
        }

        /// <summary>
        /// Creates a received document record from the document summary.
        /// </summary>
        /// <param name="documentSummary">The document get summary.</param>
        /// <returns>The document record.</returns>
        internal static DocuSignEdoc CreateReceivedDocument(EnvelopeDocumentUpdateSummary documentSummary)
        {
            DocuSignEdoc docRecord = new DocuSignEdoc();
            docRecord.DocumentId = documentSummary.IsCertificate ? (int?)null : documentSummary.DocumentId;
            docRecord.IsCertificate = documentSummary.IsCertificate;
            docRecord.FromDocuSign = true;
            docRecord.Name = documentSummary.Name;

            docRecord.SequenceNumber = documentSummary.SequenceNumber ?? -1;
            docRecord.EdocId = null;

            return docRecord;
        }

        /// <summary>
        /// Loads all edoc records in the reader and buckets them via envelope reference id.
        /// </summary>
        /// <param name="reader">The reader to load from.</param>
        /// <returns>A dictionary mapping envelope id to the associated edoc record.</returns>
        internal static Dictionary<int, List<DocuSignEdoc>> Load(IDataReader reader)
        {
            Dictionary<int, List<DocuSignEdoc>> envelopeIdToEdocs = new Dictionary<int, List<DocuSignEdoc>>();
            while (reader.Read())
            {
                var edoc = new DocuSignEdoc(reader);
                if (!envelopeIdToEdocs.ContainsKey(edoc.EnvelopeReferenceId))
                {
                    envelopeIdToEdocs.Add(edoc.EnvelopeReferenceId, new List<DocuSignEdoc>());
                }

                envelopeIdToEdocs[edoc.EnvelopeReferenceId].Add(edoc);
            }

            return envelopeIdToEdocs;
        }

        /// <summary>
        /// Saves this document to EDocs if it has not yet been saved.
        /// </summary>
        /// <param name="sourceDocType">The source document type of the pre-signing document, for determining target doc type from.</param>
        /// <param name="tempFilePath">The temporary file path where the document is being held.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="appId">The app to save to.</param>
        /// <param name="docTypeId">The document type to save to.</param>
        internal void SaveToEDocs(DocType sourceDocType, LocalFilePath tempFilePath, Guid brokerId, Guid loanId, Guid appId, int? docTypeId = null)
        {
            if (this.EdocId.HasValue)
            {
                return;
            }

            var repository = EDocumentRepository.GetSystemRepository(brokerId);

            Guid? edocId;
            var autoSavePage = this.IsCertificate ? DataAccess.E_AutoSavePage.ESignedCertificate : DataAccess.E_AutoSavePage.ESignedDocument;
            if (this.IsCertificate)
            {
                edocId = AutoSaveDocTypeFactory.SavePdfDocAsSystem(autoSavePage, tempFilePath.Value, brokerId, loanId, appId, this.Name);
            }
            else
            {
                edocId = AutoSaveDocTypeFactory.SavePdfDocAsSystemForESignedDocument(sourceDocType, tempFilePath.Value, brokerId, loanId, appId, string.Empty);
            }

            this.EdocId = edocId == Guid.Empty ? (Guid?)null : edocId;
        }

        /// <summary>
        /// Updates the doc record with the document summary.
        /// </summary>
        /// <param name="documentSummary">The document summary.</param>
        internal void UpdateDoc(EnvelopeDocumentUpdateSummary documentSummary)
        {
            this.Name = documentSummary.Name;
            this.SequenceNumber = documentSummary.SequenceNumber ?? -1;
        }

        /// <summary>
        /// Saves the DocuSign EDoc record to the DB.
        /// </summary>
        /// <param name="envelopeReferenceId">The id of the associated envelope.</param>
        /// <param name="connection">The connection.</param>
        /// <param name="transaction">The transaction.</param>
        internal void Save(int envelopeReferenceId, DbConnection connection, DbTransaction transaction)
        {
            List<SqlParameter> parameters = null;
            StoredProcedureName sproc;
            SqlParameter idParameter = null;
            if (!this.isNew)
            {
                sproc = DocuSignEdocsUpdate;
                parameters = new List<SqlParameter>()
                {
                    new SqlParameter("@EnvelopeReferenceId", this.EnvelopeReferenceId),
                    new SqlParameter("@Id", this.Id),
                    new SqlParameter("@EdocId", this.EdocId),
                    new SqlParameter("@Name", this.Name),
                    new SqlParameter("@SequenceNumber", this.SequenceNumber)
                };
            }
            else
            {
                sproc = DocuSignEdocsCreate;
                this.EnvelopeReferenceId = envelopeReferenceId;
                idParameter = new SqlParameter("@Id", SqlDbType.Int) { Direction = ParameterDirection.Output };
                parameters = new List<SqlParameter>()
                {
                    new SqlParameter("@EnvelopeReferenceId", this.EnvelopeReferenceId),
                    new SqlParameter("@SequenceNumber", this.SequenceNumber),
                    new SqlParameter("@FromDocuSign", this.FromDocuSign),
                    new SqlParameter("@EdocId", this.EdocId),
                    new SqlParameter("@Name", this.Name),
                    new SqlParameter("@DocumentId", this.DocumentId),
                    new SqlParameter("@IsCertificate", this.IsCertificate),
                    idParameter
                };
            }

            StoredProcedureDriverHelper.ExecuteNonQuery(connection, transaction, sproc, parameters, TimeoutInSeconds.Thirty);

            if (idParameter != null)
            {
                this.Id = (int)idParameter.Value;
            }

            this.isNew = false;
        }
    }
}
