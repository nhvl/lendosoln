﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using Common;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Class representing the auth result after a user has attempted to authenticate to get into a document.
    /// </summary>
    public class DocuSignRecipientAuthResult
    {
        /// <summary>
        /// Stored procedure to create recipient auth results.
        /// </summary>
        private static readonly StoredProcedureName DocuSignRecipientAuthResultsCreate = StoredProcedureName.Create("DOCUSIGN_RECIPIENTS_AUTHRESULTS_Create").Value;

        /// <summary>
        /// Stored procedure to update recipient auth results.
        /// </summary>
        private static readonly StoredProcedureName DocuSignRecipientAuthResultsUpdate = StoredProcedureName.Create("DOCUSIGN_RECIPIENTS_AUTHRESULTS_Update").Value;

        /// <summary>
        /// Maps the status as a string to our enum.
        /// </summary>
        private static readonly IReadOnlyDictionary<string, DocuSignRecipientAuthResultStatus> StatusStringToEnum = new Dictionary<string, DocuSignRecipientAuthResultStatus>(StringComparer.OrdinalIgnoreCase)
        {
            { "passed", DocuSignRecipientAuthResultStatus.Passed },
            { "failed", DocuSignRecipientAuthResultStatus.Failed }
        };

        /// <summary>
        /// Whether this is a new object or not.
        /// </summary>
        private bool isNew;

        /// <summary>
        /// Prevents a default instance of the <see cref="DocuSignRecipientAuthResult"/> class from being created.
        /// </summary>
        private DocuSignRecipientAuthResult()
        {
            this.isNew = true;
            this.Id = -1;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DocuSignRecipientAuthResult"/> class.
        /// </summary>
        /// <param name="record">The reader to load from.</param>
        private DocuSignRecipientAuthResult(IDataRecord record)
        {
            this.Id = (int)record["Id"];
            this.RecipientReferenceId = (int)record["RecipientReferenceId"];
            this.EnvelopeReferenceId = (int)record["EnvelopeReferenceId"];
            this.Type = (DocuSignRecipientAuthResultType)record["Type"];
            this.Status = (DocuSignRecipientAuthResultStatus)record["Status"];
            this.EventTime = (DateTime)record["EventTime"];
            this.FailureDescription = record.AsNullableString("FailureDescription");
        }

        /// <summary>
        /// Gets the id.
        /// </summary>
        /// <value>The id of the auth result.</value>
        public int Id
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the id of the recipient that this auth result belongs to.
        /// </summary>
        /// <value>The id of the recipient that this auth result belongs to.</value>
        public int RecipientReferenceId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the id of the envelope that the associated recipient belongs to.
        /// </summary>
        /// <value>The id of the envelope that the associated recipient belongs to.</value>
        public int EnvelopeReferenceId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the type of the auth result.
        /// </summary>
        /// <value>The type of the auth result.</value>
        public DocuSignRecipientAuthResultType Type
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the status of the auth result.
        /// </summary>
        /// <value>The status of the auth result.</value>
        public DocuSignRecipientAuthResultStatus Status
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the date that this authentication event happend.
        /// </summary>
        /// <value>The date that this authentication event happened.</value>
        public DateTime? EventTime
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the description if authentication failed.
        /// </summary>
        /// <value>The description if authentication failed.</value>
        public string FailureDescription
        {
            get;
            private set;
        }

        /// <summary>
        /// Parses a string to an auth result status.
        /// </summary>
        /// <param name="statusString">The string to parse.</param>
        /// <returns>The result status. Will be <see cref="DocuSignRecipientAuthResultStatus.Other"/> if unrecognized.</returns>
        public static DocuSignRecipientAuthResultStatus ParseStatus(string statusString)
        {
            if (string.IsNullOrEmpty(statusString))
            {
                return DocuSignRecipientAuthResultStatus.NA;
            }

            DocuSignRecipientAuthResultStatus status;
            return statusString.TryParseDefine<DocuSignRecipientAuthResultStatus>(out status, ignoreCase: true) || StatusStringToEnum.TryGetValue(statusString, out status) ? status : DocuSignRecipientAuthResultStatus.Other;
        }

        /// <summary>
        /// Loads the recipient auth results and buckets them using the recipient reference id.
        /// </summary>
        /// <param name="reader">The reader to load from.</param>
        /// <returns>The auth results bucketed using recipient reference id.</returns>
        internal static Dictionary<int, Dictionary<DocuSignRecipientAuthResultType, DocuSignRecipientAuthResult>> Load(IDataReader reader)
        {
            Dictionary<int, Dictionary<DocuSignRecipientAuthResultType, DocuSignRecipientAuthResult>> recipientIdsToResults = new Dictionary<int, Dictionary<DocuSignRecipientAuthResultType, DocuSignRecipientAuthResult>>();
            while (reader.Read())
            {
                var result = new DocuSignRecipientAuthResult(reader);
                if (!recipientIdsToResults.ContainsKey(result.RecipientReferenceId))
                {
                    recipientIdsToResults.Add(result.RecipientReferenceId, new Dictionary<DocuSignRecipientAuthResultType, DocuSignRecipientAuthResult>());
                }

                recipientIdsToResults[result.RecipientReferenceId].Add(result.Type, result);
            }

            return recipientIdsToResults;
        }

        /// <summary>
        /// Creates a new auth result from a GET response.
        /// </summary>
        /// <param name="summary">The GET response.</param>
        /// <returns>The new auth result.</returns>
        internal static DocuSignRecipientAuthResult Create(AuthEventUpdateSummary summary)
        {
            var authResult = new DocuSignRecipientAuthResult();
            authResult.Type = summary.Type;
            authResult.Update(summary);
            return authResult;
        }

        /// <summary>
        /// Updates an auth result using a GET response.
        /// </summary>
        /// <param name="summary">The get response.</param>
        internal void Update(AuthEventUpdateSummary summary)
        {
            this.Status = summary.Status;
            this.EventTime = summary.EventTimestamp;
            this.FailureDescription = summary.FailureDescription;
        }

        /// <summary>
        /// Saves the object.
        /// </summary>
        /// <param name="recipientReferenceId">The recipient id.</param>
        /// <param name="envelopeReferenceId">Envelope reference id.</param>
        /// <param name="connection">The connection.</param>
        /// <param name="transaction">The transaction.</param>
        internal void Save(int recipientReferenceId, int envelopeReferenceId, DbConnection connection, DbTransaction transaction)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@Status", this.Status),
                new SqlParameter("@EventTime", this.EventTime),
                new SqlParameter("@FailureDescription", this.FailureDescription)
            };

            StoredProcedureName sp;
            SqlParameter idParameter = null;
            if (this.isNew)
            {
                this.RecipientReferenceId = recipientReferenceId;
                this.EnvelopeReferenceId = envelopeReferenceId;

                parameters.Add(new SqlParameter("@RecipientReferenceId", this.RecipientReferenceId));
                parameters.Add(new SqlParameter("@EnvelopeReferenceId", this.EnvelopeReferenceId));
                parameters.Add(new SqlParameter("@Type", this.Type));

                idParameter = new SqlParameter("@Id", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output
                };

                parameters.Add(idParameter);

                sp = DocuSignRecipientAuthResultsCreate;
            }
            else
            {
                parameters.Add(new SqlParameter("@Id", this.Id));

                sp = DocuSignRecipientAuthResultsUpdate;
            }

            StoredProcedureDriverHelper.ExecuteNonQuery(connection, transaction, sp, parameters, TimeoutInSeconds.Thirty);

            if (this.isNew)
            {
                this.Id = (int)idParameter.Value;
                this.isNew = false;
            }
        }
    }
}
