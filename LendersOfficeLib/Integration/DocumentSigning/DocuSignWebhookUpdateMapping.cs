namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Common;
    using static DocuSignEnvelopeEventNotificationParser;

    /// <summary>
    /// Defines the mapping from DocuSign's web hook format, in <see cref="DocuSignEnvelopeEventNotificationParser"/>
    /// to LendingQB's envelope update format, with root <see cref="EnvelopeUpdateSummary"/>.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("LendingQBStyleCop.LendingQBCustomRules", "LB1001:DefaultSwitchCaseMustThrowException", Justification = "Web hook behavior stipulates that we want to attempt update and continue processing even if we don't recognize the enum value.")]
    public class DocuSignWebHookUpdateMapping
    {
        /// <summary>
        /// The time zone off set which must be applied to date times without a zone.
        /// </summary>
        private TimeSpan? timeZoneOffset;

        /// <summary>
        /// The data to convert into an envelope update.
        /// </summary>
        private DocuSignEnvelopeInformation data;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocuSignWebHookUpdateMapping"/> class.
        /// </summary>
        /// <param name="data">The data to map into LendingQB.</param>
        public DocuSignWebHookUpdateMapping(DocuSignEnvelopeInformation data)
        {
            int? timeZoneOffsetHours = data?.TimeZoneOffset;
            this.timeZoneOffset = timeZoneOffsetHours.HasValue ? TimeSpan.FromHours(timeZoneOffsetHours.Value) : default(TimeSpan?);
            this.data = data;
        }

        /// <summary>
        /// Creates an envelope update from the specified envelope information data.
        /// </summary>
        /// <returns>The envelope update.</returns>
        public EnvelopeUpdateSummary CreateEnvelopeUpdate()
        {
            var recipientUpdates = new List<EnvelopeRecipientUpdateSummary>();
            foreach (var recipientStatus in (this.data?.EnvelopeStatus?.RecipientStatuses).CoalesceWithEmpty().WhereNotNull())
            {
                List<AuthEventUpdateSummary> authStatuses = this.MapAuthEventResultList(recipientStatus.RecipientAuthenticationStatus)
                    .WhereNotNull().ToList();

                // 2018-03 tj 465468 - Mappings for RoutingOrder/CustomRecipientId to -1 are to handle missing values, but realistically
                // DocuSign should always send these.  For the time being, we'll fall back to -1 and handle it later if there are issues.
                recipientUpdates.Add(new EnvelopeRecipientUpdateSummary(
                    routingOrder: recipientStatus.RoutingOrder ?? -1,
                    sentDateTime: this.FixupDateTime(recipientStatus.Sent),
                    deliveredDateTime: this.FixupDateTime(recipientStatus.Delivered),
                    signedDateTime: this.FixupDateTime(recipientStatus.Signed),
                    declinedDateTime: this.FixupDateTime(recipientStatus.Declined),
                    declinedReason: recipientStatus.DeclineReason,
                    status: MapEnum(recipientStatus.Status),
                    recipientIdGuid: recipientStatus.RecipientId,
                    customRecipientId: ParseCustomRecipientId(recipientStatus.CustomFields) ?? -1,
                    email: recipientStatus.Email,
                    name: recipientStatus.UserName,
                    accessCode: null, // web hook does not include the access code sent to the user
                    recipientAuthenticationStatus: authStatuses.Count == 0 ? null : new RecipientAuthStatusUpdateSummary(authStatuses),
                    type: MapEnum(recipientStatus.Type)));
            }

            var documentUpdates = new List<EnvelopeDocumentUpdateSummary>();
            foreach (var documentUpdate in (this.data?.DocumentPDFs).CoalesceWithEmpty().WhereNotNull())
            {
                if (documentUpdate.PDFBytes.HasValue)
                {
                    DocumentStatus documentStatus = this.data?.EnvelopeStatus?.DocumentStatuses?.FirstOrDefault(d => d.ID == documentUpdate.DocumentID);
                    documentUpdates.Add(new EnvelopeDocumentUpdateSummary(
                        documentId: documentUpdate.DocumentID,
                        isCertificate: documentUpdate.DocumentType == DocumentType.SUMMARY,
                        name: documentUpdate.Name,
                        sequenceNumber: documentStatus?.Sequence,
                        tempFilePath: documentUpdate.PDFBytes.Value));
                }
            }

            return new EnvelopeUpdateSummary(
                status: MapEnum(this.data?.EnvelopeStatus?.Status),
                createdDateTime: this.FixupDateTime(this.data?.EnvelopeStatus?.Created),
                deletedDateTime: this.FixupDateTime(this.data?.EnvelopeStatus?.Deleted),
                sentDateTime: this.FixupDateTime(this.data?.EnvelopeStatus?.Delivered),
                completedDateTime: this.FixupDateTime(this.data?.EnvelopeStatus?.Completed),
                declinedDateTime: this.FixupDateTime(this.data?.EnvelopeStatus?.Declined),
                deliveredDateTime: this.FixupDateTime(this.data?.EnvelopeStatus?.Delivered),
                voidedDateTime: null,
                voidedReason: this.data?.EnvelopeStatus?.VoidReason,
                recipients: new EnvelopeRecipientsUpdateSummary(recipientUpdates),
                documents: new EnvelopeDocumentListUpdateSummary(documentUpdates));
        }

        /// <summary>
        /// Parse the recipient identifier we pass in the custom field list.  We use the custom field list
        /// since DocuSign does not echo back recipient ids immediately during order creation.
        /// </summary>
        /// <param name="customFields">The custom fields received.</param>
        /// <returns>The parsed recipient identifier or null if none was found.</returns>
        private static int? ParseCustomRecipientId(IReadOnlyList<string> customFields)
        {
            if (customFields.Count != 1 || !customFields[0].StartsWith("r_"))
            {
                return null;
            }

            return customFields[0].Substring(2).ToNullable<int>(int.TryParse);
        }

        /// <summary>
        /// Maps the envelope status enum from DocuSign's web hook value to LendingQB's tracked value.
        /// </summary>
        /// <param name="envelopeStatusCode">The envelope status received via the web hook.</param>
        /// <returns>The corresponding envelope status tracked in LendingQB.</returns>
        private static DocuSignEnvelopeStatus MapEnum(EnvelopeStatusCode? envelopeStatusCode)
        {
            switch (envelopeStatusCode)
            {
                case EnvelopeStatusCode.Any: return DocuSignEnvelopeStatus.Any;
                case EnvelopeStatusCode.Voided: return DocuSignEnvelopeStatus.Voided;
                case EnvelopeStatusCode.Created: return DocuSignEnvelopeStatus.Created;
                case EnvelopeStatusCode.Deleted: return DocuSignEnvelopeStatus.Deleted;
                case EnvelopeStatusCode.Sent: return DocuSignEnvelopeStatus.Sent;
                case EnvelopeStatusCode.Delivered: return DocuSignEnvelopeStatus.Delivered;
                case EnvelopeStatusCode.Signed: return DocuSignEnvelopeStatus.Signed;
                case EnvelopeStatusCode.Completed: return DocuSignEnvelopeStatus.Completed;
                case EnvelopeStatusCode.Declined: return DocuSignEnvelopeStatus.Declined;
                case EnvelopeStatusCode.TimedOut: return DocuSignEnvelopeStatus.TimedOut;
                case EnvelopeStatusCode.Template: return DocuSignEnvelopeStatus.Template;
                case EnvelopeStatusCode.Processing: return DocuSignEnvelopeStatus.Processing;
                case EnvelopeStatusCode.Undefined:
                case null:
                default:
                    return DocuSignEnvelopeStatus.Other;
            }
        }

        /// <summary>
        /// Maps the recipient status enum from DocuSign's web hook value to LendingQB's tracked value.
        /// </summary>
        /// <param name="recipientStatusCode">The recipient status received via the web hook.</param>
        /// <returns>The corresponding recipient status tracked in LendingQB.</returns>
        private static DocuSignRecipientStatus MapEnum(RecipientStatusCode recipientStatusCode)
        {
            switch (recipientStatusCode)
            {
                case RecipientStatusCode.Created: return DocuSignRecipientStatus.Created;
                case RecipientStatusCode.Sent: return DocuSignRecipientStatus.Sent;
                case RecipientStatusCode.Delivered: return DocuSignRecipientStatus.Delivered;
                case RecipientStatusCode.Signed: return DocuSignRecipientStatus.Signed;
                case RecipientStatusCode.Declined: return DocuSignRecipientStatus.Declined;
                case RecipientStatusCode.Completed: return DocuSignRecipientStatus.Completed;
                case RecipientStatusCode.FaxPending: return DocuSignRecipientStatus.FaxPending;
                case RecipientStatusCode.AutoResponded: return DocuSignRecipientStatus.AutoResponded;
                case RecipientStatusCode.Undefined:
                default:
                    return DocuSignRecipientStatus.Other;
            }
        }

        /// <summary>
        /// Maps the recipient authentication event status enum from DocuSign's web hook value to LendingQB's tracked value.
        /// </summary>
        /// <param name="eventStatusCode">The event status received via the web hook.</param>
        /// <returns>The corresponding authentication result status tracked in LendingQB.</returns>
        private static DocuSignRecipientAuthResultStatus MapEnum(EventStatusCode eventStatusCode)
        {
            switch (eventStatusCode)
            {
                case EventStatusCode.Passed: return DocuSignRecipientAuthResultStatus.Passed;
                case EventStatusCode.Failed: return DocuSignRecipientAuthResultStatus.Failed;
                case EventStatusCode.Undefined:
                default:
                    return DocuSignRecipientAuthResultStatus.Other;
            }
        }

        /// <summary>
        /// Maps the recipient type enum from DocuSign's web hook value to LendingQB's tracked value.
        /// </summary>
        /// <param name="recipientType">The recipient type received via the web hook.</param>
        /// <returns>The corresponding recipient type tracked in LendingQB.</returns>
        private static RecipientType MapEnum(RecipientTypeCode recipientType)
        {
            switch (recipientType)
            {
                case RecipientTypeCode.Agent: return RecipientType.Agent;
                case RecipientTypeCode.CarbonCopy: return RecipientType.CarbonCopy;
                case RecipientTypeCode.CertifiedDelivery: return RecipientType.CertifiedDelivery;
                case RecipientTypeCode.Editor: return RecipientType.Editor;
                case RecipientTypeCode.InPersonSigner: return RecipientType.InPersonSigner;
                case RecipientTypeCode.Intermediary: return RecipientType.Intermediary;
                case RecipientTypeCode.Signer: return RecipientType.Signer;
                case RecipientTypeCode.SigningHost: // We're not sure what they're going to do here; SigningHost is not documented, but it is part of the schema.  For now, we're mapping it to Other.
                case RecipientTypeCode.Undefined:
                default:
                    return RecipientType.Other;
            }
        }

        /// <summary>
        /// Maps the auth results for many types into a list, with null entries for missing items.
        /// </summary>
        /// <param name="authenticationStatus">The authentication status container.</param>
        /// <returns>A sequence of the authentication status data.</returns>
        private IEnumerable<AuthEventUpdateSummary> MapAuthEventResultList(AuthenticationStatus authenticationStatus)
        {
            yield return this.MapAuthEventResult(authenticationStatus?.AccessCodeResult, DocuSignRecipientAuthResultType.AccessCode);
            yield return this.MapAuthEventResult(authenticationStatus?.IDQuestionsResult, DocuSignRecipientAuthResultType.IDQuestion);
            yield return this.MapAuthEventResult(authenticationStatus?.IDLookupResult, DocuSignRecipientAuthResultType.IDLookup);
            yield return this.MapAuthEventResult(authenticationStatus?.AgeVerifyResult, DocuSignRecipientAuthResultType.AgeVerify);
            yield return this.MapAuthEventResult(authenticationStatus?.STANPinResult, DocuSignRecipientAuthResultType.STANPin);
            yield return this.MapAuthEventResult(authenticationStatus?.OFACResult, DocuSignRecipientAuthResultType.OFAC);
            yield return this.MapAuthEventResult(authenticationStatus?.PhoneAuthResult, DocuSignRecipientAuthResultType.PhoneAuth);
            yield return this.MapAuthEventResult(authenticationStatus?.LiveIDResult, DocuSignRecipientAuthResultType.LiveID);
            yield return this.MapAuthEventResult(authenticationStatus?.FacebookResult, DocuSignRecipientAuthResultType.Facebook);
            yield return this.MapAuthEventResult(authenticationStatus?.GoogleResult, DocuSignRecipientAuthResultType.Google);
            yield return this.MapAuthEventResult(authenticationStatus?.LinkedinResult, DocuSignRecipientAuthResultType.Linkedin);
            yield return this.MapAuthEventResult(authenticationStatus?.SalesforceResult, DocuSignRecipientAuthResultType.Salesforce);
            yield return this.MapAuthEventResult(authenticationStatus?.TwitterResult, DocuSignRecipientAuthResultType.Twitter);
            yield return this.MapAuthEventResult(authenticationStatus?.OpenIDResult, DocuSignRecipientAuthResultType.OpenID);
            yield return this.MapAuthEventResult(authenticationStatus?.AnySocialIDResult, DocuSignRecipientAuthResultType.AnySocialID);
            yield return this.MapAuthEventResult(authenticationStatus?.YahooResult, DocuSignRecipientAuthResultType.Yahoo);
        }

        /// <summary>
        /// Maps the result of an authentication event to an update item.
        /// </summary>
        /// <param name="eventResult">The event result received via the web hook.</param>
        /// <param name="type">The type in LendingQB's tracking of the event.</param>
        /// <returns>An event update summary or null.</returns>
        private AuthEventUpdateSummary MapAuthEventResult(EventResult eventResult, DocuSignRecipientAuthResultType type)
        {
            return eventResult == null
                ? null
                : new AuthEventUpdateSummary(
                    eventTimestamp: this.FixupDateTime(eventResult.EventTimestamp),
                    failureDescription: eventResult.FailureDescription,
                    status: MapEnum(eventResult.Status),
                    type: type);
        }

        /// <summary>
        /// Fixes up any unspecified date time from DocuSign's time zone into LendingQB's local time zone.
        /// </summary>
        /// <param name="originalDate">The original date to update.</param>
        /// <returns><paramref name="originalDate"/> converted to a local time instance.</returns>
        private DateTime? FixupDateTime(DateTime? originalDate)
        {
            if (!originalDate.HasValue)
            {
                return default(DateTime?);
            }

            DateTimeOffset dtwithzone = originalDate.Value.Kind == DateTimeKind.Unspecified && this.timeZoneOffset.HasValue ? new DateTimeOffset(originalDate.Value, this.timeZoneOffset.Value) : new DateTimeOffset(originalDate.Value);
            return dtwithzone.LocalDateTime;
        }
    }
}
