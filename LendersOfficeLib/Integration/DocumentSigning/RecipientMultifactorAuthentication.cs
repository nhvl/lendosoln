﻿namespace LendersOffice.Integration.DocumentSigning
{
    using LendersOffice.Admin.DocuSign;

    /// <summary>
    /// Represents the multi-factor authentication data to use to validate a recipient.
    /// </summary>
    public class RecipientMultifactorAuthentication
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RecipientMultifactorAuthentication"/> class.
        /// </summary>
        /// <param name="initialMfaOption">The MFA option chosen on envelope creation.</param>
        /// <param name="accessCode">The access code to input.</param>
        /// <param name="phoneNumber">The phone number for phone/SMS authentication.</param>
        /// <param name="idCheckConfiguration">The ID-Check configuration name to use.</param>
        public RecipientMultifactorAuthentication(MfaOptions initialMfaOption, string accessCode, string phoneNumber, string idCheckConfiguration)
        {
            this.AccessCode = accessCode;
            this.InitialMfaOption = initialMfaOption;
            this.PhoneNumber = phoneNumber;
            this.IdCheckConfiguration = idCheckConfiguration;
        }

        /// <summary>
        /// Gets the MFA option chosen on envelope creation.
        /// </summary>
        public MfaOptions InitialMfaOption { get; }

        /// <summary>
        /// Gets the access code to input.
        /// </summary>
        public string AccessCode { get; }

        /// <summary>
        /// Gets the phone number used for phone/SMS authentication.
        /// </summary>
        public string PhoneNumber { get; }

        /// <summary>
        /// Gets the ID-Check configuration name for authentication.
        /// </summary>
        public string IdCheckConfiguration { get; }
    }
}
