<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  xmlns:custom="urn:custom-xslt"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt">
  <xsl:output method="text" indent="no" />
  <xsl:variable name="TargetNamespace" select="/xsd:schema/@targetNamespace"/>
  <msxsl:script language="C#" implements-prefix="custom"><![CDATA[
    public static string ReadFunctionSuffix(string typeName, bool isNillable)
    {
        int colonIndex = typeName.IndexOf(':');
        string ns = colonIndex > 0 ? typeName.Remove(colonIndex) : string.Empty;
        string localTypeName = colonIndex > 0 ? typeName.Substring(colonIndex + 1) : typeName;
        string nillable = isNillable ? "Nillable" : string.Empty;
        switch (localTypeName)
        {
            case "string": return nillable + "String";
            case "int": return nillable + "Int";
            case "dateTime": return nillable + "DateTime";
            case "boolean": return nillable + "Bool";
            case "positiveInteger": return nillable + "Int";
            case "base64Binary": return nillable + "Base64";
            case "unsignedShort": return nillable + "Int";
            case "double": return nillable + "Double";
            case "nonNegativeInteger": return nillable + "Int";
            case "decimal": return nillable + "Decimal";
            default:
              return nillable + localTypeName;
        }
    }
    public static string FixTypeName(string typeName, bool isNullable)
    {
        isNullable = true; // Per conversation with Brian, we're going to attempt to handle any partial results, even if the value is missing.
        int colonIndex = typeName.IndexOf(':');
        string ns = colonIndex > 0 ? typeName.Remove(colonIndex) : string.Empty;
        string localTypeName = colonIndex > 0 ? typeName.Substring(colonIndex + 1) : typeName;
        if (localTypeName.StartsWith("ArrayOf"))
        {
          string arrayType = localTypeName.Substring(7);
          if (arrayType == "String1")
          {
              arrayType = "string";
          }
          
          return "IReadOnlyList<" + FixTypeName(arrayType, false) + ">";
        }
        
        string nullable = isNullable ? "?" : string.Empty;
        switch (localTypeName)
        {
            case "string": return "string";
            case "int": return "int" + nullable;
            case "dateTime": return "DateTime" + nullable;
            case "boolean": return "bool" + nullable;
            case "positiveInteger": return "int" + nullable;
            case "base64Binary": return "LocalFilePath" + nullable;
            case "unsignedShort": return "int" + nullable;
            case "double": return "double" + nullable;
            case "nonNegativeInteger": return "int" + nullable;
            case "decimal": return "decimal" + nullable;
            default:
              return localTypeName;
        }
    }
    public static string LocalCsName(string name)
    {
        return char.ToLower(name[0]) + name.Substring(1);
    }
  ]]></msxsl:script>
  
  <xsl:template match="xsd:schema">
    <xsl:text>// &lt;auto-generated/&gt;
// &lt;remarks&gt;
// To regenerate this file, run ./DocuSignEnvelopeEventNotificationParserCodeGen/RunTransform.ps1
// &lt;/remarks&gt;
namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using LqbGrammar.DataTypes;

    public partial class DocuSignEnvelopeEventNotificationParser
    {
        private const string TargetNamespace = </xsl:text>
    <xsl:value-of select="concat('&quot;',@targetNamespace,'&quot;;')"/>
    <xsl:text>
</xsl:text>
    <xsl:apply-templates select="xsd:complexType" mode="ParseData"/>
    <xsl:apply-templates select="xsd:simpleType" mode="ParseData"/>
    <xsl:call-template name="ReadDefaultTypes"/>
    <xsl:text>    }

    public partial class DocuSignEnvelopeEventNotificationParser
    {</xsl:text>
    <xsl:apply-templates select="xsd:simpleType" mode="TypeGen"/>
    <xsl:apply-templates select="xsd:complexType" mode="TypeGen"/>
    <xsl:text>    }
}
</xsl:text>
  </xsl:template>
  
  <xsl:template match="xsd:complexType[starts-with(@name,'ArrayOf')]" mode="ParseData">
    <xsl:variable name="ComplexTypeName" select="@name"/>
    <xsl:variable name="ItemTypeName" select="custom:FixTypeName(xsd:sequence[1]/xsd:element[1]/@type,false)"/>
    <xsl:variable name="ItemElementName" select="xsd:sequence[1]/xsd:element[1]/@name"/>
    <xsl:text>
        private IReadOnlyList&lt;</xsl:text>
    <xsl:value-of select="concat($ItemTypeName,'&gt; Read',custom:ReadFunctionSuffix(@name,false),'(XmlReader reader, XmlWriter log)')"/>
    <xsl:text>
        {
            string startElementName = reader.Name;
            this.LogStartElement(reader, log);
            var list = new List&lt;</xsl:text>
    <xsl:value-of select="$ItemTypeName"/>
    <xsl:text>&gt;();
            if (reader.IsEmptyElement)
            {
                reader.Read();
                return list;
            }

            reader.Read();
            while (!(reader.MoveToContent() == XmlNodeType.EndElement &amp;&amp; reader.Name == startElementName))
            {
                if (reader.IsStartElement(&quot;</xsl:text>
    <xsl:value-of select="$ItemElementName"/>
    <xsl:text>&quot;, TargetNamespace))
                {
                    list.Add(this.Read</xsl:text>
    <xsl:value-of select="custom:ReadFunctionSuffix(xsd:sequence[1]/xsd:element[1]/@type,@nillable='true')"/>
    <xsl:text>(reader, log));
                }
                else if (reader.IsStartElement())
                {
                    this.ReadUnknownElement(reader, log);
                }
                else
                {
                    this.ReadUnknownData(reader, log);
                }
            }

            this.LogFullEndElement(reader, log);
            reader.Read();
            return list;
        }
</xsl:text>
  </xsl:template>
  <xsl:template match="xsd:complexType" mode="ParseData">
    <xsl:variable name="ComplexTypeName" select="@name"/>
    <xsl:text>
        </xsl:text>
    <xsl:value-of select="concat('private ',$ComplexTypeName,' Read',custom:ReadFunctionSuffix(@name,false),'(XmlReader reader, XmlWriter log)')"/>
    <xsl:text>
        {
            string startElementName = reader.Name;
            this.LogStartElement(reader, log);
            if (reader.IsEmptyElement)
            {
                reader.Read();
                return null;
            }

            reader.Read();</xsl:text>
    <xsl:for-each select="xsd:sequence[1]/xsd:element">
      <xsl:text>
            </xsl:text>
      <xsl:value-of select="concat(custom:FixTypeName(@type,@minOccurs=0),' ',custom:LocalCsName(@name),' = default(',custom:FixTypeName(@type,@minOccurs=0),');')"/>
    </xsl:for-each>
    <xsl:text>
            while (!(reader.MoveToContent() == XmlNodeType.EndElement &amp;&amp; reader.Name == startElementName))
            {
                </xsl:text>
    <xsl:for-each select="xsd:sequence[1]/xsd:element">
      <xsl:text>if (reader.IsStartElement(&quot;</xsl:text>
      <xsl:value-of select="@name"/>
      <xsl:text>&quot;, TargetNamespace))
                {
                    </xsl:text>
      <xsl:value-of select="concat(custom:LocalCsName(@name),' = this.Read',custom:ReadFunctionSuffix(@type,@nillable='true'))"/>
      <xsl:text>(reader, log);
                }
                else </xsl:text>
      <xsl:choose>
        <xsl:when test="@minOccurs = 0"></xsl:when>
        <xsl:when test="@nillable = 'true'"></xsl:when>
        <xsl:when test="@minOccurs = 1"><!-- 
        Per discussion with Brian, we're no longer going to validate this, and instead try to process even
        incomplete XML. --></xsl:when>
        <xsl:otherwise>
          <xsl:message terminate="yes"><xsl:value-of select="concat('Unexpected minOccurs (',@minOccurs,') for ',@name,' on ',$ComplexTypeName,'.')"/></xsl:message>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
    <xsl:text>if (reader.IsStartElement())
                {
                    this.ReadUnknownElement(reader, log);
                }
                else
                {
                    this.ReadUnknownData(reader, log);
                }
            }

            this.LogFullEndElement(reader, log);
            reader.Read();
            return new </xsl:text>
    <xsl:value-of select="concat($ComplexTypeName,'(')"/>
    <xsl:for-each select="xsd:sequence[1]/xsd:element">
      <xsl:if test="count(preceding-sibling::*) &gt; 0">
        <xsl:text>,</xsl:text>
      </xsl:if>
      <xsl:text>
                </xsl:text>
      <xsl:value-of select="custom:LocalCsName(@name)"/>
    </xsl:for-each>
    <xsl:text>);
        }
</xsl:text>
  </xsl:template>
  
  <xsl:template match="xsd:simpleType" mode="ParseData">
    <xsl:text>
        </xsl:text>
    <xsl:value-of select="concat('private ',@name,' Read',@name,'(XmlReader reader, XmlWriter log)')"/>
    <xsl:text>
        {
            string input = this.ReadString(reader, log);
            </xsl:text>
    <xsl:value-of select="concat(@name,' parsed;')"/>
    <xsl:text>
            </xsl:text>
    <xsl:value-of select="concat('return Enum.TryParse(input, out parsed) ? parsed : ',@name,'.Undefined;')" />
    <xsl:text>
        }
</xsl:text>
  </xsl:template>
  
  <xsl:template match="xsd:simpleType" mode="TypeGen">
    <xsl:if test="count(*) != 1 or count(xsd:restriction) != 1">
      <xsl:message terminate="yes">Expected only one child of <xsl:value-of select="@name"/></xsl:message>
    </xsl:if>
    <xsl:text>
        public enum </xsl:text>
    <xsl:value-of select="@name"/>
    <xsl:text>
        {
            Undefined = 0,</xsl:text>
    <xsl:for-each select="xsd:restriction/xsd:enumeration">
      <xsl:text>
            </xsl:text>
      <xsl:value-of select="concat(@value, ',')"/>
    </xsl:for-each>
    <xsl:text>
        }
</xsl:text>
  </xsl:template>
  
  <xsl:template match="xsd:complexType[starts-with(@name,'ArrayOf')]" mode="TypeGen">
    <xsl:if test="count(*) != 1 or count(xsd:sequence/xsd:element) != 1">
      <xsl:message terminate="yes">Expected only one child of <xsl:value-of select="@name"/></xsl:message>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="xsd:complexType" mode="TypeGen">
    <xsl:text>
        public class </xsl:text><xsl:value-of select="@name"/><xsl:text>
        {
            public </xsl:text><xsl:value-of select="@name"/><xsl:text>(</xsl:text>
    <xsl:for-each select="xsd:sequence[1]/xsd:element">
      <xsl:if test="position()!=1">,</xsl:if>
      <xsl:text>
                </xsl:text>
      <xsl:value-of select="concat(custom:FixTypeName(@type,@minOccurs=0),' ',custom:LocalCsName(@name))"/>
    </xsl:for-each>
    <xsl:text>)
            {</xsl:text>
    <xsl:for-each select="xsd:sequence[1]/xsd:element">
      <xsl:text>
                </xsl:text>
      <xsl:value-of select="concat('this.',@name,' = ',custom:LocalCsName(@name),';')"/>
    </xsl:for-each>
    <xsl:text>
            }</xsl:text>
    <xsl:for-each select="xsd:sequence[1]/xsd:element">
      <xsl:text>

            </xsl:text>
      <xsl:value-of select="concat('public ',custom:FixTypeName(@type,@minOccurs=0),' ',@name,' { get; set; }')"/>
    </xsl:for-each>
    <xsl:text>
        }
</xsl:text>
  </xsl:template>
  
  <xsl:template match="@*|node()">
    <xsl:message terminate="no">Unexpected element [<xsl:value-of select="name(.)"/>]</xsl:message>
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" />
    </xsl:copy>
  </xsl:template>
  
  <xsl:template name="ReadDefaultTypes">
    <xsl:text>
        private string ReadString(XmlReader reader, XmlWriter log)
        {
            bool isEmptyElement = reader.IsEmptyElement;
            this.LogStartElement(reader, log);
            string input = reader.ReadElementContentAsString();
            if (!isEmptyElement)
            {
                this.LogString(input, log);
                this.LogFullEndElement(reader, log);
            }

            return input;
        }

        private int? ReadInt(XmlReader reader, XmlWriter log)
        {
            string input = this.ReadString(reader, log);
            int parsed;
            return int.TryParse(input, out parsed) ? parsed : default(int?);
        }

        private DateTime? ReadDateTime(XmlReader reader, XmlWriter log)
        {
            string input = this.ReadString(reader, log);
            DateTime parsed;
            return DateTime.TryParse(input, out parsed) ? parsed : default(DateTime?);
        }

        private bool? ReadBool(XmlReader reader, XmlWriter log)
        {
            string input = this.ReadString(reader, log);
            bool parsed;
            return bool.TryParse(input, out parsed) ? parsed : default(bool?);
        }

        private double? ReadDouble(XmlReader reader, XmlWriter log)
        {
            string input = this.ReadString(reader, log);
            double parsed;
            return double.TryParse(input, out parsed) ? parsed : default(double?);
        }

        private decimal? ReadDecimal(XmlReader reader, XmlWriter log)
        {
            string input = this.ReadString(reader, log);
            decimal parsed;
            return decimal.TryParse(input, out parsed) ? parsed : default(decimal?);
        }
</xsl:text>
  </xsl:template>
</xsl:stylesheet>