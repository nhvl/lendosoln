$ScriptPath = split-path -parent $MyInvocation.MyCommand.Definition

$XslPath = 'TransformEnvelopeInfoSchemaToPartialClass.xsl'
$InputXml = 'DocuSignEnvelopeInformation.xsd'
$OutputFile = "..\DocuSignEnvelopeEventNotificationParser.generated.cs"

pushd $ScriptPath
C:\LendingQB\XslExport\Tools\ExcelToXml.exe "$InputXml" "$XslPath"
if ($LastExitCode -eq 0)
{
  Move-Item "$InputXml.xml" "$OutputFile" -Force
}
popd
