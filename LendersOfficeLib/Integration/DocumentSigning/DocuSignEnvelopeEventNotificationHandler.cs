﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System.Xml.Linq;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Defines a handling method for DocuSign's Web Hook/Connect update calls.
    /// </summary>
    public class DocuSignEnvelopeEventNotificationHandler : System.Web.IHttpHandler
    {
        /// <summary>
        /// The category to log messages under.
        /// </summary>
        private const string LogCategory = nameof(DocuSignEnvelopeEventNotificationHandler);

        /// <summary>
        /// Gets a value indicating whether this handler instance can be re-used, i.e., whether it eschews local state.
        /// </summary>
        public bool IsReusable => true;

        /// <summary>
        /// Processes the request specified by <paramref name="context"/>.
        /// </summary>
        /// <param name="context">The request context.</param>
        public void ProcessRequest(System.Web.HttpContext context)
        {
            if (context.Request.HttpMethod != HttpMethod.Post.ToString())
            {
                context.Response.StatusCode = 405; // Method Not Allowed
                return;
            }

            bool successflag = false;
            string responseMessage = null;
            try
            {
                var sb = new System.Text.StringBuilder();
                DocuSignEnvelopeEventNotificationParser.DocuSignEnvelopeInformation data;
                var parser = new DocuSignEnvelopeEventNotificationParser();
                using (var reader = System.Xml.XmlReader.Create(context.Request.InputStream))
                using (var log = System.Xml.XmlWriter.Create(sb))
                {
                    var result = parser.Read(reader, log);
                    if (result.HasError)
                    {
                        DataAccess.Tools.LogWarning((result.Error as LqbGrammar.Exceptions.LqbException)?.Context?.Serialize(), result.Error);
                    }

                    data = result.Value;
                }

                string loggedXml = sb.ToString();
                if (!string.IsNullOrEmpty(loggedXml))
                {
                    DataAccess.Tools.LogInfo(LogCategory, loggedXml);
                }

                DocuSignEnvelope envelope = DocuSignEnvelope.LoadByOnlyEnvelopeId(data?.EnvelopeStatus?.EnvelopeID);
                if (envelope != null)
                {
                    var mapper = new DocuSignWebHookUpdateMapping(data);
                    EnvelopeUpdateSummary envelopeUpdate = mapper.CreateEnvelopeUpdate();
                    envelope.UpdateAndSave(envelopeUpdate);
                    successflag = true;

                    DocuSignEnvelopeEventNotificationParser.DeleteTempFiles(data);
                }
                else if (!string.IsNullOrWhiteSpace(data?.EnvelopeStatus?.EnvelopeID))
                {
                    DataAccess.Tools.LogInfo(LogCategory, "Unable to resolve envelope with id [" + data.EnvelopeStatus.EnvelopeID + "]; update message will be discarded.");
                    responseMessage = "Matching Envelope id not found";
                }
                else
                {
                    responseMessage = "Envelope id not found";
                }
            }
            finally
            {
                // DocuSign ignores our response, but this does get logged on their side, so a nice message might help debug.
                context.Response.ContentEncoding = new System.Text.UTF8Encoding(false);
                context.Response.ContentType = MimeType.Text.Xml.ToString();
                var responseXml = new XElement(
                    "EnvelopeUpdateResponse",
                    new XElement("success", successflag),
                    new XElement("message", responseMessage));
                context.Response.BinaryWrite(context.Response.ContentEncoding.GetBytes(responseXml.ToString(SaveOptions.DisableFormatting)));
                context.ApplicationInstance.CompleteRequest();
            }
        }
    }
}
