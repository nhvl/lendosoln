﻿namespace LendersOffice.Integration.DocumentSigning.Serialization
{
    using System.Collections.Generic;

    /// <summary>
    /// Class used to deserialize the response of an EnvelopeDocument list request.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Class used for deserialization. Properties must match those in the JSON.")]
    public class EnvelopeDocumentsList
    {
        /// <summary>
        /// Gets or sets the documents in the envelope.
        /// </summary>
        public IEnumerable<EnvelopeDocument> envelopeDocuments { get; set; }
    }
}
