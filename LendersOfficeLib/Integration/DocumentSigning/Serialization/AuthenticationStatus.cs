﻿namespace LendersOffice.Integration.DocumentSigning.Serialization
{
    /// <summary>
    /// Class used to deserialize the auth statuses for an envelope recipient.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Class used for deserialization. Properties must match those in the JSON.")]
    public class AuthenticationStatus
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="AuthenticationStatus" /> class from being created.
        /// </summary>
        private AuthenticationStatus()
        {
        }

        /// <summary>
        /// Gets or sets the results for access code authentication.
        /// </summary>
        public EventResult accessCodeResult { get; set; }

        /// <summary>
        /// Gets or sets the results for age verification authentication.
        /// </summary>
        public EventResult ageVerifyResult { get; set; }

        /// <summary>
        /// Gets or sets the results for any social id authentication.
        /// </summary>
        public EventResult anySocialIDResult { get; set; }

        /// <summary>
        /// Gets or sets the results for facebook authentication.
        /// </summary>
        public EventResult facebookResult { get; set; }

        /// <summary>
        /// Gets or sets the results for google authentication.
        /// </summary>
        public EventResult googleResult { get; set; }

        /// <summary>
        /// Gets or sets the results for id lookup authentication.
        /// </summary>
        public EventResult idLookupResult { get; set; }

        /// <summary>
        /// Gets or sets the results for id question authentication.
        /// </summary>
        public EventResult idQuestionsResult { get; set; }

        /// <summary>
        /// Gets or sets the results for linkedin authentication.
        /// </summary>
        public EventResult linkedinResult { get; set; }

        /// <summary>
        /// Gets or sets the results for live id authentication.
        /// </summary>
        public EventResult liveIDResult { get; set; }

        /// <summary>
        /// Gets or sets the results for OFAC authentication.
        /// </summary>
        public EventResult ofacResult { get; set; }

        /// <summary>
        /// Gets or sets the results for open id authentication.
        /// </summary>
        public EventResult openIDResult { get; set; }

        /// <summary>
        /// Gets or sets the results for phone authentication.
        /// </summary>
        public EventResult phoneAuthResult { get; set; }

        /// <summary>
        /// Gets or sets the results for sales force authentication.
        /// </summary>
        public EventResult salesforceResult { get; set; }

        /// <summary>
        /// Gets or sets the results for signature provider authentication.
        /// </summary>
        public EventResult signatureProviderResult { get; set; }

        /// <summary>
        /// Gets or sets the results for SMS authentication.
        /// </summary>
        public EventResult smsAuthResult { get; set; }

        /// <summary>
        /// Gets or sets the results for STANPin authentication.
        /// </summary>
        public EventResult sTANPinResult { get; set; }

        /// <summary>
        /// Gets or sets the results for twitter authentication.
        /// </summary>
        public EventResult twitterResult { get; set; }

        /// <summary>
        /// Gets or sets the results for yahoo authentication.
        /// </summary>
        public EventResult yahooResult { get; set; }
    }
}
