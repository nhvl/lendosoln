﻿namespace LendersOffice.Integration.DocumentSigning.Serialization
{
    /// <summary>
    /// Class used to deserialize the document info in the response of an EnvelopeDocument list request.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Class used for deserialization. Properties must match those in the JSON.")]
    public class EnvelopeDocument
    {
        /// <summary>
        /// Gets or sets the positive integer document id. If null, this is possibly the certificate document.
        /// </summary>
        public string documentId { get; set; }

        /// <summary>
        /// Gets or sets the name of the document.
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Gets or sets the order number of the document.
        /// </summary>
        public string order { get; set; }

        /// <summary>
        /// Gets or sets the uri we need to use to retrieve the document contents.
        /// In the format '/envelopes/{envelopeid}/documents/{docId}'.
        /// </summary>
        public string uri { get; set; }
    }
}
