﻿namespace LendersOffice.Integration.DocumentSigning.Serialization
{
    /// <summary>
    /// Class used to deserialize the response of an Envelope get request.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Class used for deserialization. Properties must match those in the JSON.")]
    public class EnvelopeGet
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="EnvelopeGet" /> class from being created.
        /// </summary>
        private EnvelopeGet()
        {
        }

        /// <summary>
        /// Gets or sets the status string.
        /// </summary>
        /// <value>The status string.</value>
        public string status { get; set; }

        /// <summary>
        /// Gets or sets the created date time.
        /// </summary>
        /// <value>The created date time.</value>
        public string createdDateTime { get; set; }

        /// <summary>
        /// Gets or sets the deleted date time.
        /// </summary>
        /// <value>The deleted date time.</value>
        public string deletedDateTime { get; set; }

        /// <summary>
        /// Gets or sets the sent date time.
        /// </summary>
        /// <value>The sent date time.</value>
        public string sentDateTime { get; set; }

        /// <summary>
        /// Gets or sets the completed date time.
        /// </summary>
        /// <value>The completed date time.</value>
        public string completedDateTime { get; set; }

        /// <summary>
        /// Gets or sets the declined date time.
        /// </summary>
        /// <value>The declined date time.</value>
        public string declinedDateTime { get; set; }

        /// <summary>
        /// Gets or sets the delivered date time.
        /// </summary>
        /// <value>The delivered date time.</value>
        public string deliveredDateTime { get; set; }

        /// <summary>
        /// Gets or sets the voided date time.
        /// </summary>
        /// <value>The voided date time.</value>
        public string voidedDateTime { get; set; }

        /// <summary>
        /// Gets or sets the voided reason.
        /// </summary>
        /// <value>The voided reason.</value>
        public string voidedReason { get; set; }
    }
}
