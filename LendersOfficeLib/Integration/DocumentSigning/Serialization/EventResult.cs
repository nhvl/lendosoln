﻿namespace LendersOffice.Integration.DocumentSigning.Serialization
{
    /// <summary>
    /// Class used to deserialize the auth event result of an envelope recipient.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Class used for deserialization. Properties must match those in the JSON.")]
    public class EventResult
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="EventResult" /> class from being created.
        /// </summary>
        private EventResult()
        {
        }

        /// <summary>
        /// Gets or sets the event time stamp.
        /// </summary>
        public string eventTimestamp { get; set; }

        /// <summary>
        /// Gets or sets the failure description.
        /// </summary>
        public string failureDescription { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public string status { get; set; }
    }
}
