﻿namespace LendersOffice.Integration.DocumentSigning.Serialization
{
    using System.Collections.Generic;

    /// <summary>
    /// Class used to deserialize the recipients in the response of an EnvelopeRecipients list request.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Class used for deserialization. Properties must match those in the JSON.")]
    public class EnvelopeRecipient
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="EnvelopeRecipient" /> class from being created.
        /// </summary>
        private EnvelopeRecipient()
        {
        }

        /// <summary>
        /// Gets or sets the routing order.
        /// </summary>
        public string routingOrder { get; set; }

        /// <summary>
        /// Gets or sets the sent date time.
        /// </summary>
        public string sentDateTime { get; set; }

        /// <summary>
        /// Gets or sets the delivered date time.
        /// </summary>
        public string deliveredDateTime { get; set; }

        /// <summary>
        /// Gets or sets the signed date time.
        /// </summary>
        public string signedDateTime { get; set; }

        /// <summary>
        /// Gets or sets the declined date time.
        /// </summary>
        public string declinedDateTime { get; set; }

        /// <summary>
        /// Gets or sets the declined reason.
        /// </summary>
        public string declinedReason { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public string status { get; set; }

        /// <summary>
        /// Gets or sets the recipient id guid assigned by DocuSign.
        /// </summary>
        public string recipientIdGuid { get; set; }

        /// <summary>
        /// Gets or sets the recipient id sent to DocuSign.
        /// </summary>
        public string recipientId { get; set; }

        /// <summary>
        /// Gets or sets the email of the recipient.
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// Gets or sets the name of the recipient.
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Gets or sets the access code.
        /// </summary>
        public string accessCode { get; set; }

        /// <summary>
        /// Gets or sets the signer name.
        /// </summary>
        public string signerName { get; set; }

        /// <summary>
        /// Gets or sets the signer email.
        /// </summary>
        public string signerEmail { get; set; }

        /// <summary>
        /// Gets or sets the signer host name.
        /// </summary>
        public string hostName { get; set; }

        /// <summary>
        /// Gets or sets the signer host email.
        /// </summary>
        public string hostEmail { get; set; }

        /// <summary>
        /// Gets or sets the custom fields that we send over.
        /// </summary>
        public List<string> customFields { get; set; }

        /// <summary>
        /// Gets or sets the authentication attempt statuses for this user.
        /// </summary>
        public AuthenticationStatus recipientAuthenticationStatus { get; set; }
    }
}
