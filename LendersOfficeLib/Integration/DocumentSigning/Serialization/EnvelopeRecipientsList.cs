﻿namespace LendersOffice.Integration.DocumentSigning.Serialization
{
    using System.Collections.Generic;

    /// <summary>
    /// Class used to deserialize the response of an EnvelopeRecipients list request.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Class used for deserialization. Properties must match those in the JSON.")]
    public class EnvelopeRecipientsList
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="EnvelopeRecipientsList" /> class from being created.
        /// </summary>
        private EnvelopeRecipientsList()
        {
        }

        /// <summary>
        /// Gets or sets the signer recipients.
        /// </summary>
        public List<EnvelopeRecipient> signers { get; set; }

        /// <summary>
        /// Gets or sets the agent recipients.
        /// </summary>
        public List<EnvelopeRecipient> agents { get; set; }

        /// <summary>
        /// Gets or sets the editor recipients.
        /// </summary>
        public List<EnvelopeRecipient> editors { get; set; }

        /// <summary>
        /// Gets or sets the intermediary recipients.
        /// </summary>
        public List<EnvelopeRecipient> intermediaries { get; set; }

        /// <summary>
        /// Gets or sets the carbon copy recipients.
        /// </summary>
        public List<EnvelopeRecipient> carbonCopies { get; set; }

        /// <summary>
        /// Gets or sets the certified delivery recipients.
        /// </summary>
        public List<EnvelopeRecipient> certifiedDeliveries { get; set; }

        /// <summary>
        /// Gets or sets the in person signer recipients.
        /// </summary>
        public List<EnvelopeRecipient> inPersonSigners { get; set; }

        /// <summary>
        /// Gets or sets the seal signer recipients.
        /// </summary>
        public List<EnvelopeRecipient> seals { get; set; }
    }
}
