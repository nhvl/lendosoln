﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The document view model.
    /// </summary>
    public class DocuSignDocumentViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocuSignDocumentViewModel"/> class.
        /// </summary>
        /// <param name="documentRecord">The document record to pull from.</param>
        /// <param name="availableEdocs">The edocs that are viewable.</param>
        public DocuSignDocumentViewModel(DocuSignEdoc documentRecord, Dictionary<Guid, EDocs.EDocument> availableEdocs)
        {
            this.EdocId = documentRecord.EdocId.HasValue && availableEdocs.ContainsKey(documentRecord.EdocId.Value) ? documentRecord.EdocId.Value.ToString() : string.Empty;
            EDocs.EDocument document;
            if (!documentRecord.IsCertificate && documentRecord.FromDocuSign && documentRecord.EdocId.HasValue && availableEdocs.TryGetValue(documentRecord.EdocId.Value, out document))
            {
                this.Name = document.FolderAndDocTypeName;
            }
            else
            {
                this.Name = documentRecord.Name;
            }
        }

        /// <summary>
        /// Gets or sets the edoc id.
        /// </summary>
        /// <value>The edoc id.</value>
        public string EdocId
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the name of the document.
        /// </summary>
        /// <value>The name of the document.</value>
        public string Name
        {
            get; set;
        }
    }
}
