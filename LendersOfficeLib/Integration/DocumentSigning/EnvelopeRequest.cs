﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Represents the data necessary to create a signing envelope; i.e. to initiate the signing process.
    /// </summary>
    public class EnvelopeRequest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnvelopeRequest"/> class.
        /// </summary>
        /// <param name="emailSubject">The email subject to send to the recipients for the created envelope.</param>
        /// <param name="emailBody">The email body to send to recipients of the created envelope.</param>
        /// <param name="webhookUrl">The web hook URL to receive asynchronous updates about the created envelope.</param>
        /// <param name="recipients">The list of recipients to receive the created envelope.</param>
        /// <param name="documents">The list of documents to include in the created envelope.</param>
        /// <param name="tabs">The list of tabs to include in the created envelope.</param>
        /// <param name="docuSignUserName">The username of the DocuSign account to be used for envelope contents (in-person signers).</param>
        /// <param name="docuSignEmail">The email of the DocuSign account to be used for envelope contents (in-person signers).</param>
        /// <param name="docuSignBrandId">The brand ID in DocuSign to use for the created envelope.</param>
        public EnvelopeRequest(
            string emailSubject,
            string emailBody,
            LqbAbsoluteUri webhookUrl,
            IEnumerable<EnvelopeRecipient> recipients,
            IEnumerable<LqbDocument> documents,
            IEnumerable<DocumentTab> tabs,
            string docuSignUserName,
            string docuSignEmail,
            Guid? docuSignBrandId)
        {
            this.EmailSubject = emailSubject;
            this.EmailBody = emailBody;
            this.Recipients = recipients;
            this.Documents = documents;
            this.Tabs = tabs;
            this.WebhookUrl = webhookUrl;
            this.DocuSignUserName = docuSignUserName;
            this.DocuSignUserEmail = docuSignEmail;
            this.DocuSignBrandId = docuSignBrandId;
        }

        /// <summary>
        /// Gets the email subject to send to the recipients for this envelope.
        /// </summary>
        public string EmailSubject { get; }

        /// <summary>
        /// Gets the list of recipients to receive the created envelope.
        /// </summary>
        public IEnumerable<EnvelopeRecipient> Recipients { get; }

        /// <summary>
        /// Gets the list of documents to include in the created envelope.
        /// </summary>
        public IEnumerable<LqbDocument> Documents { get; }

        /// <summary>
        /// Gets the list of tabs to include in the created envelope.
        /// </summary>
        public IEnumerable<DocumentTab> Tabs { get; }

        /// <summary>
        /// Gets the web hook URL to receive asynchronous updates about the created envelope.
        /// </summary>
        public LqbAbsoluteUri WebhookUrl { get; }

        /// <summary>
        /// Gets the Email body to send in the emails to recipients.
        /// </summary>
        public object EmailBody { get; }

        /// <summary>
        /// Gets the DocuSign user name associated with the request.
        /// </summary>
        public string DocuSignUserName { get; }

        /// <summary>
        /// Gets the DocuSign user email associated with the request.
        /// </summary>
        public string DocuSignUserEmail { get; }

        /// <summary>
        /// Gets the brand ID in DocuSign to use for the created envelope.
        /// </summary>
        public Guid? DocuSignBrandId { get; }
    }
}
