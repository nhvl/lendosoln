﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using Common;
    using Serialization;

    /// <summary>
    /// Class holding update information about an authentication event.
    /// </summary>
    public class AuthEventUpdateSummary
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthEventUpdateSummary"/> class.
        /// </summary>
        /// <param name="eventTimestamp">The event time stamp.</param>
        /// <param name="failureDescription">The failure description.</param>
        /// <param name="status">The status.</param>
        /// <param name="type">The type of this auth event.</param>
        public AuthEventUpdateSummary(
            DateTime? eventTimestamp,
            string failureDescription,
            DocuSignRecipientAuthResultStatus status,
            DocuSignRecipientAuthResultType type)
        {
            this.EventTimestamp = eventTimestamp;
            this.FailureDescription = failureDescription;
            this.Status = status;
            this.Type = type;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthEventUpdateSummary"/> class.
        /// </summary>
        /// <param name="eventResult">The deserialized event result from a deserialized authentication status.</param>
        /// <param name="type">The type of auth event.</param>
        public AuthEventUpdateSummary(EventResult eventResult, DocuSignRecipientAuthResultType type)
        {
            this.EventTimestamp = eventResult.eventTimestamp?.ToNullable<DateTime>(DateTime.TryParse);
            this.FailureDescription = eventResult.failureDescription;
            this.Status = DocuSignRecipientAuthResult.ParseStatus(eventResult.status);
            this.Type = type;
        }

        /// <summary>
        /// Gets the event time stamp.
        /// </summary>
        public DateTime? EventTimestamp { get; private set; }

        /// <summary>
        /// Gets the failure description.
        /// </summary>
        public string FailureDescription { get; private set; }

        /// <summary>
        /// Gets the status.
        /// </summary>
        public DocuSignRecipientAuthResultStatus Status { get; private set; }

        /// <summary>
        /// Gets the type of this auth event.
        /// </summary>
        public DocuSignRecipientAuthResultType Type { get; private set; }
    }
}
