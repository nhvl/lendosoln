﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using DataAccess;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides a simple interface into the DocuSign integration.
    /// </summary>
    public class DocuSignDriver
    {
        /// <summary>
        /// The types of request that the driver supports.
        /// </summary>
        private enum RequestType
        {
            /// <summary>
            /// Gets the envelope + recipients + documents.
            /// </summary>
            FullEnvelope
        }

        /// <summary>
        /// Creates an envelope or returns an error result.
        /// </summary>
        /// <param name="loginData">The authentication data to use.</param>
        /// <param name="envelope">The envelope data to attempt to create.</param>
        /// <returns>An envelope summary or an error result.</returns>
        public Result<EnvelopeCreateSummary> CreateEnvelope(DocuSignAuthenticationData loginData, EnvelopeRequest envelope)
        {
            Result<DocuSignAdapter> adapterResult = DocuSignAdapter.CreateAndLogin(loginData);
            if (adapterResult.HasError)
            {
                return Result<EnvelopeCreateSummary>.Failure(adapterResult.Error);
            }

            return adapterResult.Value.CreateEnvelope(envelope);
        }

        /// <summary>
        /// Voids an envelope or returns an error result.
        /// </summary>
        /// <param name="loginData">The authentication data to use.</param>
        /// <param name="envelopeId">The envelope's GUID.</param>
        /// <param name="voidedReason">The reason the envelope was voided.</param>
        /// <returns>An envelope summary or an error result.</returns>
        public Result<DBNull> VoidEnvelope(DocuSignAuthenticationData loginData, string envelopeId, string voidedReason)
        {
            Result<DocuSignAdapter> adapterResult = DocuSignAdapter.CreateAndLogin(loginData);
            if (adapterResult.HasError)
            {
                return Result<DBNull>.Failure(adapterResult.Error);
            }

            return adapterResult.Value.VoidEnvelope(envelopeId, voidedReason);
        }

        /// <summary>
        /// Gets and updates an envelope.
        /// </summary>
        /// <param name="loginData">The login data.</param>
        /// <param name="envelopeToUpdate">The envelope to update.</param>
        /// <returns>A result with the updated envelope. Failure result if an error occured.</returns>
        public Result<DocuSignEnvelope> GetAndUpdateEnvelope(DocuSignAuthenticationData loginData, DocuSignEnvelope envelopeToUpdate)
        {
            Result<DocuSignAdapter> adapterResult = DocuSignAdapter.CreateAndLogin(loginData);
            if (adapterResult.HasError)
            {
                return Result<DocuSignEnvelope>.Failure(adapterResult.Error);
            }

            var limitCheckResult = this.CheckGetLimitation(envelopeToUpdate.EnvelopeId, adapterResult.Value.AccountId, RequestType.FullEnvelope); 
            if (limitCheckResult.HasError)
            {
                return Result<DocuSignEnvelope>.Failure(limitCheckResult.Error);
            }

            var getResult = adapterResult.Value.GetEnvelope(envelopeToUpdate.EnvelopeId);
            if (getResult.HasError)
            {
                return Result<DocuSignEnvelope>.Failure(getResult.Error);
            }

            envelopeToUpdate.UpdateAndSave(getResult.Value);
            if (getResult.Value.Documents?.Documents != null)
            {
                // We've updated. We can now delete the temporary files used for saving docs.
                foreach (var doc in getResult.Value.Documents?.Documents)
                {
                    Drivers.Gateways.FileOperationHelper.Delete(doc.TempFilePath.Value);
                }
            }

            return Result<DocuSignEnvelope>.Success(envelopeToUpdate);
        }

        /// <summary>
        /// Gets the URL to an OAuth consent page to allow a user in browser to give consent.  Consent via this URL is the first stage of the OAuth2 authentication process.
        /// </summary>
        /// <param name="principal">The user who will give consent.</param>
        /// <param name="useSandbox">A value indicating whether to use the DocuSign's sandbox environment.</param>
        /// <param name="integratorKey">The integrator key to use for authentication.</param>
        /// <param name="redirectPath">The URL for DocuSign to redirect the user to after consent has been received.</param>
        /// <returns>The URL to the OAuth consent page.</returns>
        public LqbAbsoluteUri GetOAuthConsentUrl(AbstractUserPrincipal principal, bool useSandbox, string integratorKey, LqbAbsoluteUri redirectPath)
        {
            // https://docs.docusign.com/esign/guide/authentication/auth_server.html#using-the-state-parameter
            // "You should set the state parameter to an unguessable string known only to your application, and
            // check to make sure that the value has not changed between requests and responses."
            string stateParameter = Guid.NewGuid().ToString("N");
            DataAccess.AutoExpiredTextCache.RemoveUserEntry(principal, "DocuSignOAuthConsentStateKey"); // if they're retrying, don't block them
            DataAccess.AutoExpiredTextCache.InsertUserString(principal, "DocuSignOAuthConsentStateKey", stateParameter, TimeSpan.FromDays(1));

            return DocuSignOAuthServiceAdapter.CreateConsentAuthenticationUrl(useSandbox, integratorKey, stateParameter, redirectPath);
        }

        /// <summary>
        /// Gets the DocuSign user info for a given consent code, which will be used to extract data necessary to authenticate as that user.  This data can then be
        /// saved and used for subsequent access under the OAuth2 authentication process.
        /// </summary>
        /// <param name="principal">The user who has given consent.</param>
        /// <param name="code">The code query string parameter returned by DocuSign.</param>
        /// <param name="stateParameter">The state query string parameter returned by DocuSign.</param>
        /// <param name="useSandbox">A value indicating whether to use the DocuSign's sandbox environment.</param>
        /// <param name="integratorKey">The integrator key to use for authentication.</param>
        /// <param name="secretKey">The secret key to allow access to DocuSign's web services.</param>
        /// <returns>The DocuSign user info for the consenting user, or an error result.</returns>
        public Result<DocuSignUserInfo> GetOAuthUserInfoFromCode(AbstractUserPrincipal principal, string code, string stateParameter, bool useSandbox, string integratorKey, string secretKey)
        {
            string savedStateParameter = DataAccess.AutoExpiredTextCache.GetUserString(principal, "DocuSignOAuthConsentStateKey");
            if (stateParameter != savedStateParameter)
            {
                return Result<DocuSignUserInfo>.Failure(DataAccess.CBaseException.GenericException($"Invalid state; expected [{savedStateParameter}], received [{stateParameter}]"));
            }

            var adapter = new DocuSignOAuthServiceAdapter();
            return adapter.GetUserInfoFromCode(useSandbox, code, integratorKey, secretKey);
        }

        /// <summary>
        /// Gets a recipient's embedded signing URL from DocuSign.
        /// </summary>
        /// <param name="loginData">The authentication data to use.</param>
        /// <param name="request">The recipient view request object holding data to send to DocuSign.</param>
        /// <returns>The recipient's view URL or returns an error result.</returns>
        public Result<LqbAbsoluteUri> GetRecipientViewUrl(DocuSignAuthenticationData loginData, RecipientViewRequest request)
        {
            Result<DocuSignAdapter> adapterResult = DocuSignAdapter.CreateAndLogin(loginData);
            if (adapterResult.HasError)
            {
                return Result<LqbAbsoluteUri>.Failure(adapterResult.Error);
            }

            return adapterResult.Value.GetRecipientViewUrl(request);
        }

        /// <summary>
        /// Checks to make sure we're not violating DocuSign's GET request limitations.
        /// </summary>
        /// <param name="envelopeId">The envelope id.</param>
        /// <param name="accountId">The account id.</param>
        /// <param name="requestType">The request type.</param>
        /// <returns>Success result if can issue the get request. Error result if not.</returns>
        private Result<DBNull> CheckGetLimitation(string envelopeId, string accountId, RequestType requestType)
        {
            /* DocuSign has an API usage limit for GET requests.
             * This particular check will enforce rule #2.
             * "You may not exceed one GET request per unique envelope endpoint per 15 minutes. This limit applies to endpoints such as GET /accounts/{accountId}/envelopes/..."
             */
            string cacheKey = $"{envelopeId}_{accountId}_{requestType.ToString()}_DocuSign";
            var nextAvailableGetTimeString = AutoExpiredTextCache.GetFromCache(cacheKey);
            DateTime nextAvailableGetTime;
            if (string.IsNullOrEmpty(nextAvailableGetTimeString))
            {
                // Either envelope never requested or it has expired and record was wiped.
                AutoExpiredTextCache.AddToCache(DateTime.Now.AddMinutes(15).ToString("o"), TimeSpan.FromMinutes(15), cacheKey);
            }
            else if (DateTime.TryParse(nextAvailableGetTimeString, out nextAvailableGetTime) && DateTime.Now > nextAvailableGetTime)
            {
                // Record wasn't wiped but the time limit has expired.
                AutoExpiredTextCache.RefreshCacheDuration(cacheKey, TimeSpan.FromMinutes(15));
                AutoExpiredTextCache.UpdateCache(DateTime.Now.AddMinutes(15).ToString("o"), cacheKey);
            }
            else
            {
                return Result<DBNull>.Failure(new CBaseException($"Unable to request due to API limitations. Try again at {nextAvailableGetTime.ToString()}.", "DocuSign API limitation reached."));
            }

            return Result<DBNull>.Success(null);
        }
    }
}
