﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System.Collections.Generic;

    /// <summary>
    /// Class representing the returned document list for an envelope.
    /// </summary>
    public class EnvelopeDocumentListUpdateSummary
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnvelopeDocumentListUpdateSummary"/> class.
        /// </summary>
        /// <param name="documentList">List of document update summaries.</param>
        public EnvelopeDocumentListUpdateSummary(IEnumerable<EnvelopeDocumentUpdateSummary> documentList)
        {
            this.Documents = documentList;
        }

        /// <summary>
        /// Gets the documents in the envelope.
        /// </summary>
        public IEnumerable<EnvelopeDocumentUpdateSummary> Documents { get; private set; }
    }
}
