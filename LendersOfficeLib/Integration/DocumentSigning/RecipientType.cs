﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System.ComponentModel;

    /// <summary>
    /// The type of recipient.
    /// </summary>
    public enum RecipientType
    {
        /// <summary>
        /// The recipient will sign the documents.
        /// </summary>
        [Description("Signer")]
        Signer = 0,

        /// <summary>
        /// The recipient will sign the documents via an in-person signing ceremony.
        /// </summary>
        [Description("In-Person Signer")]
        InPersonSigner = 1,

        /// <summary>
        /// The recipient will receive a copy of the documents without signing.
        /// </summary>
        [Description("Carbon Copy")]
        CarbonCopy = 2,

        /// <summary>
        /// Recipient assigned assigned as an agent on the document.
        /// </summary>
        [Description("Agent")]
        Agent = 3,

        /// <summary>
        /// Recipient assigned assigned as an editor on the document.
        /// </summary>
        [Description("Editor")]
        Editor = 4,

        /// <summary>
        /// Identifies a recipient that can, but is not required to, add name and email information for recipients at the same or subsequent level in the routing order.
        /// </summary>
        [Description("Intermediary")]
        Intermediary = 5,

        /// <summary>
        /// A complex type containing information on a recipient the must receive the completed documents for the envelope to be completed, 
        /// but the recipient does not need to sign, initial, date, or add information to any of the documents.
        /// </summary>
        [Description("Certified Delivery")]
        CertifiedDelivery = 6,

        /// <summary>
        /// Recipient is designated as a seal signer.
        /// </summary>
        [Description("Seal")]
        Seal = 7,

        /// <summary>
        /// Other recipient type.
        /// </summary>
        [Description("Other")]
        Other = 8
    }
}
