﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using Common;
    using DataAccess;
    using DataAccess.Utilities;
    using Drivers.SqlServerDB;
    using EDocs;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using ObjLib.Edocs.AutoSaveDocType;
    using Security;

    /// <summary>
    /// Represents a DocuSign Envelope record in LQB.
    /// </summary>
    public class DocuSignEnvelope
    {
        /// <summary>
        /// Stored procedure to create an envelope.
        /// </summary>
        private static readonly StoredProcedureName DocuSignEnvelopesCreate = StoredProcedureName.Create("DOCUSIGN_ENVELOPES_Create").Value;

        /// <summary>
        /// Stored procedure to update an envelope.
        /// </summary>
        private static readonly StoredProcedureName DocuSignEnvelopesUpdate = StoredProcedureName.Create("DOCUSIGN_ENVELOPES_Update").Value;

        /// <summary>
        /// Retrieves all parts needed to construct a set of DocuSignEnvelope records from the DB.
        /// Can retrieve using loan id, envelope id, or id.
        /// </summary>
        private static readonly StoredProcedureName DocuSignEnvelopesRetrieve = StoredProcedureName.Create("DOCUSIGN_ENVELOPES_Retrieve").Value;

        /// <summary>
        /// Retrieves all parts needed to construct a DocuSignEnvelope record from the DB.
        /// Can retrieve using envelope id, or id.
        /// </summary>
        private static readonly StoredProcedureName DocuSignEnvelopesRetrieveSingle = StoredProcedureName.Create("DOCUSIGN_ENVELOPES_RetrieveSingle").Value;

        /// <summary>
        /// Retrieves all parts needed to construct a DocuSignEnvelope record from the DB, but only by the envelope id.
        /// </summary>
        private static readonly StoredProcedureName DocuSignEnvelopeRetrieveByOnlyEnvelopeId = StoredProcedureName.Create("DOCUSIGN_ENVELOPES_RetrieveByOnlyEnvelopeId").Value;

        /// <summary>
        /// The statuses that have tracking dates.
        /// </summary>
        private static readonly IReadOnlyDictionary<DocuSignEnvelopeStatus, string> DbTrackedStatuses = new Dictionary<DocuSignEnvelopeStatus, string>
        {
            { DocuSignEnvelopeStatus.Completed, "CompletedDate" },
            { DocuSignEnvelopeStatus.Created, "CreatedDate" },
            { DocuSignEnvelopeStatus.Declined, "DeclinedDate" },
            { DocuSignEnvelopeStatus.Deleted, "DeletedDate" },
            { DocuSignEnvelopeStatus.Delivered, "DeliveredDate" },
            { DocuSignEnvelopeStatus.Sent, "SentDate" },
            { DocuSignEnvelopeStatus.Signed, "SignedDate" },
            { DocuSignEnvelopeStatus.TimedOut, "TimedOutDate" },
            { DocuSignEnvelopeStatus.Voided, "VoidedDate" }
        };

        /// <summary>
        /// Dictionary mapping the status string to our enums.
        /// </summary>
        /// <remarks>
        /// Pulled from https://www.docusign.com/p/RESTAPIGuide/Content/REST%20API%20References/Get%20Envelope%20Status%20Changes.htm.
        /// </remarks>
        private static readonly IReadOnlyDictionary<string, DocuSignEnvelopeStatus> StatusStringToEnum = new Dictionary<string, DocuSignEnvelopeStatus>(StringComparer.OrdinalIgnoreCase)
        {
            { "voided", DocuSignEnvelopeStatus.Voided },
            { "created", DocuSignEnvelopeStatus.Created },
            { "deleted", DocuSignEnvelopeStatus.Deleted },
            { "sent", DocuSignEnvelopeStatus.Sent },
            { "delivered", DocuSignEnvelopeStatus.Delivered },
            { "signed", DocuSignEnvelopeStatus.Signed },
            { "completed", DocuSignEnvelopeStatus.Completed },
            { "declined", DocuSignEnvelopeStatus.Declined },
            { "timedout", DocuSignEnvelopeStatus.TimedOut },
            { "processing", DocuSignEnvelopeStatus.Processing }
        };

        /// <summary>
        /// Whether this is a new envelope.
        /// </summary>
        private bool isNew;

        /// <summary>
        /// The recipients to delete.
        /// </summary>
        private IEnumerable<DocuSignRecipient> recipientsToDelete = null;

        /// <summary>
        /// The statuses mapped to the date that they were reached.
        /// </summary>
        private Dictionary<DocuSignEnvelopeStatus, DateTime?> statuses = null;

        /// <summary>
        /// The documents sent to DocuSign.
        /// </summary>
        private List<DocuSignEdoc> sentDocuments = null;

        /// <summary>
        /// The documents returned from DocuSign.
        /// </summary>
        private List<DocuSignEdoc> returnedDocuments = null;

        /// <summary>
        /// The recipients to this envelope.
        /// </summary>
        private List<DocuSignRecipient> recipients = null;

        /// <summary>
        /// Prevents a default instance of the <see cref="DocuSignEnvelope"/> class from being created.
        /// </summary>
        private DocuSignEnvelope()
        {
            this.Id = -1;
            this.isNew = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DocuSignEnvelope"/> class.
        /// </summary>
        /// <param name="record">The record to load from.</param>
        /// <param name="documents">The documents associated with the envelope.</param>
        /// <param name="recipients">The recipients of the envelope.</param>
        private DocuSignEnvelope(IDataRecord record, List<DocuSignEdoc> documents, List<DocuSignRecipient> recipients)
        {
            this.Id = (int)record["Id"];
            this.EnvelopeId = (string)record["EnvelopeId"];
            this.Name = (string)record["Name"];
            this.RequestedDate = (DateTime)record["RequestedDate"];
            this.SenderName = (string)record["SenderName"];
            this.SenderEmail = record.AsNullableString("SenderEmail");
            this.SenderUserId = (Guid)record["SenderUserId"];
            this.LoanId = (Guid)record["LoanId"];
            this.BrokerId = (Guid)record["BrokerId"];
            this.CurrentStatus = (DocuSignEnvelopeStatus)record["CurrentStatus"];
            this.VoidReason = record.AsNullableString("VoidReason");
            this.VoidDoneBy = record.AsNullableString("VoidDoneBy");

            this.statuses = new Dictionary<DocuSignEnvelopeStatus, DateTime?>();
            foreach (var status in DbTrackedStatuses)
            {
                if (record[status.Value] != DBNull.Value)
                {
                    this.statuses[status.Key] = (DateTime)record[status.Value];
                }
            }

            this.sentDocuments = new List<DocuSignEdoc>();
            this.returnedDocuments = new List<DocuSignEdoc>();
            foreach (var doc in documents)
            {
                if (doc.FromDocuSign)
                {
                    this.returnedDocuments.Add(doc);
                }
                else
                {
                    this.sentDocuments.Add(doc);
                }
            }

            this.recipients = recipients;
        }

        /// <summary>
        /// Gets the DB id for the envelope.
        /// </summary>
        /// <value>The DB id for the envelope.</value>
        public int Id
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the id assigned to the envelope by DocuSign.
        /// </summary>
        /// <value>The id assigned to the envelope by DocuSign.</value>
        public string EnvelopeId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the name of the envelope.
        /// </summary>
        /// <value>The name of the envelope.</value>
        public string Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the date that this envelope was requested to be sent. 
        /// This should be the date as determined by LQB.
        /// </summary>
        /// <value>The requested date.</value>
        public DateTime RequestedDate
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the sender's name.
        /// </summary>
        /// <value>The sender's name.</value>
        public string SenderName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the sender user id.
        /// </summary>
        /// <value>The sender user id.</value>
        public Guid SenderUserId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the sender's email.
        /// </summary>
        /// <value>The sender's email.</value>
        public string SenderEmail
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the id of the loan that the envelope belongs to.
        /// </summary>
        /// <value>The id of the loan that the envelope belongs to.</value>
        public Guid LoanId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the broker id.
        /// </summary>
        /// <value>The broker id.</value>
        public Guid BrokerId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the current status.
        /// </summary>
        /// <value>The current status.</value>
        public DocuSignEnvelopeStatus CurrentStatus
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a dictionary of statuses to the date that the envelope hit that status.
        /// Note that not all statuses will have a corresponding date.
        /// </summary>
        public IReadOnlyDictionary<DocuSignEnvelopeStatus, DateTime?> StatusDates
        {
            get
            {
                return this.statuses;
            }
        }
        
        /// <summary>
        /// Gets the reason why the envelope was voided.
        /// </summary>
        /// <value>The reason why the envelope was voided.</value>
        public string VoidReason
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the user who requested the envelope be voided.
        /// </summary>
        /// <value>The user who requested the envelope be voided.</value>
        public string VoidDoneBy
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the set of documents that were sent to DocuSign for e-signing.
        /// </summary>
        /// <value>The set of documents that were sent to DocuSign for e-signing.</value>
        public IReadOnlyList<DocuSignEdoc> SentDocuments
        {
            get
            {
                return this.sentDocuments;
            }
        }

        /// <summary>
        /// Gets the set of e-signed documents that were returned from DocuSign.
        /// </summary>
        /// <value>The set of documents that were returned from DocuSign.</value>
        public IReadOnlyList<DocuSignEdoc> ReturnedDocuments
        {
            get
            {
                return this.returnedDocuments;
            }
        }

        /// <summary>
        /// Gets the set of recipients for this envelope.
        /// </summary>
        /// <value>The set of recipients for this envelope.</value>
        public IReadOnlyList<DocuSignRecipient> Recipients
        {
            get
            {
                return this.recipients;
            }
        }

        /// <summary>
        /// Parses the status string into a possible DocuSign envelope status.
        /// </summary>
        /// <param name="statusString">The string to parse.</param>
        /// <returns>The status. <see cref="DocuSignEnvelopeStatus.Other"/> if not recognized.</returns>
        public static DocuSignEnvelopeStatus ParseStatus(string statusString)
        {
            if (string.IsNullOrEmpty(statusString))
            {
                return DocuSignEnvelopeStatus.NA;
            }

            DocuSignEnvelopeStatus status;
            return statusString.TryParseDefine<DocuSignEnvelopeStatus>(out status) || StatusStringToEnum.TryGetValue(statusString, out status) ? status : DocuSignEnvelopeStatus.Other;
        }

        /// <summary>
        /// Loads all envelopes associated with a loan.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="loanId">The loan id.</param>
        /// <returns>List of envelopes associated with the loan.</returns>
        public static List<DocuSignEnvelope> Load(Guid brokerId, Guid loanId)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", loanId)
            };

            return Load(DbAccessUtils.GetConnection(brokerId), parameters, DocuSignEnvelopesRetrieve);
        }

        /// <summary>
        /// Loads an envelope with the given envelope id.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="envelopeId">The envelope id.</param>
        /// <returns>The envelope or null if not found.</returns>
        public static DocuSignEnvelope Load(Guid brokerId, Guid loanId, string envelopeId)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@EnvelopeId", envelopeId)
            };

            return Load(DbAccessUtils.GetConnection(brokerId), parameters, DocuSignEnvelopesRetrieveSingle).FirstOrDefault();
        }

        /// <summary>
        /// Loads the loan using the int id.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="id">The int id.</param>
        /// <returns>The envelope or null if not found.</returns>
        public static DocuSignEnvelope Load(Guid brokerId, Guid loanId, int id)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@Id", id)
            };

            return Load(DbAccessUtils.GetConnection(brokerId), parameters, DocuSignEnvelopesRetrieveSingle).FirstOrDefault();
        }

        /// <summary>
        /// Loads the envelope by only the envelope id, which is hopefully globally unique (if it's not, blame DocuSign).
        /// This will be much slower than most other ways to load an envelope and is really only intended for the handler,
        /// which will not have a principal context to load a particular connection from.
        /// </summary>
        /// <param name="envelopeId">The envelope id.</param>
        /// <returns>The loaded envelope or null, if no such envelope was found.</returns>
        public static DocuSignEnvelope LoadByOnlyEnvelopeId(string envelopeId)
        {
            if (string.IsNullOrWhiteSpace(envelopeId))
            {
                return null;
            }

            List<DocuSignEnvelope> envelopes = new List<DocuSignEnvelope>();
            foreach (DbConnectionInfo connectionInfo in DbConnectionInfo.ListAll())
            {
                var parameters = new SqlParameter[]
                {
                    new SqlParameter("@EnvelopeId", envelopeId),
                };
                envelopes.AddRange(Load(connectionInfo.GetConnection(), parameters, DocuSignEnvelopeRetrieveByOnlyEnvelopeId));
            }

            if (envelopes.Count == 1)
            {
                return envelopes[0];
            }
            else if (envelopes.Count > 1)
            {
                Tools.LogBug("DocuSign Envelopes violated unique value for @EnvelopeId='" + envelopeId.Replace("'", "''") + "' across the DBs.");
            }

            return null;
        }

        /// <summary>
        /// Creates a DocuSignEnvelope record from an initial request and saves it to the DB.
        /// </summary>
        /// <param name="envelopeRequest">The request data used to create the envelope.</param>
        /// <param name="creationSummary">The tracked data used in envelope creation.</param>
        /// <param name="requestedDate">The date that the envelope was requested to be sent.</param>
        /// <param name="sender">The principal of the sender.</param>
        /// <param name="senderName">The name of the sender as sent in the envelope.</param>
        /// <param name="senderEmail">The email of the sender as sent in the envelope.</param>
        /// <param name="loanId">The loan id.</param>
        /// <returns>The saved DocuSignEnvelope record.</returns>
        public static DocuSignEnvelope CreateAndSave(EnvelopeRequest envelopeRequest, EnvelopeCreateSummary creationSummary, DateTime requestedDate, AbstractUserPrincipal sender, string senderName, string senderEmail, Guid loanId)
        {
            DocuSignEnvelope envelope = new DocuSignEnvelope();
            envelope.BrokerId = sender.BrokerId;
            envelope.LoanId = loanId;
            envelope.Name = envelopeRequest.Documents.First().Name;
            envelope.RequestedDate = requestedDate;
            envelope.SenderName = senderName;
            envelope.SenderEmail = senderEmail;
            envelope.SenderUserId = sender.UserId;
            envelope.EnvelopeId = creationSummary.Identifier;

            envelope.recipients = DocuSignRecipient.Create(envelopeRequest, creationSummary);
            envelope.sentDocuments = DocuSignEdoc.CreateSentDocuments(envelopeRequest, creationSummary);

            DocuSignEnvelopeStatus status;
            if (!StatusStringToEnum.TryGetValue(creationSummary.Status, out status))
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Unknown status: {creationSummary.Status}"));
            }

            envelope.CurrentStatus = status;
            envelope.statuses = new Dictionary<DocuSignEnvelopeStatus, DateTime?>();
            envelope.statuses[envelope.CurrentStatus] = creationSummary.StatusDateTime;

            envelope.Save();
            return envelope;
        }

        /// <summary>
        /// Sets the envelope to void status and saves.
        /// </summary>
        /// <param name="voidReason">The void reason.</param>
        /// <param name="voider">The user who requested the void.</param>
        public void VoidAndSave(string voidReason, AbstractUserPrincipal voider)
        {
            this.CurrentStatus = DocuSignEnvelopeStatus.Voided;
            this.VoidReason = voidReason;
            this.VoidDoneBy = voider.DisplayName;
            this.statuses[DocuSignEnvelopeStatus.Voided] = DateTime.Now;

            this.Save();
        }

        /// <summary>
        /// Updates an envelope using the response returned from a GET request.
        /// </summary>
        /// <param name="envelopeSummary">The response from the request.</param>
        public void UpdateAndSave(EnvelopeUpdateSummary envelopeSummary)
        {
            var oldStatus = this.CurrentStatus;
            this.CurrentStatus = envelopeSummary.Status;
            this.VoidReason = envelopeSummary.VoidedReason;

            this.statuses[DocuSignEnvelopeStatus.Created] = envelopeSummary.CreatedDateTime ?? this.StatusDates.GetValueOrNull(DocuSignEnvelopeStatus.Created);
            this.statuses[DocuSignEnvelopeStatus.Deleted] = envelopeSummary.DeletedDateTime ?? this.StatusDates.GetValueOrNull(DocuSignEnvelopeStatus.Deleted);
            this.statuses[DocuSignEnvelopeStatus.Sent] = envelopeSummary.SentDateTime ?? this.StatusDates.GetValueOrNull(DocuSignEnvelopeStatus.Sent);
            this.statuses[DocuSignEnvelopeStatus.Completed] = envelopeSummary.CompletedDateTime ?? this.StatusDates.GetValueOrNull(DocuSignEnvelopeStatus.Completed);
            this.statuses[DocuSignEnvelopeStatus.Declined] = envelopeSummary.DeclinedDateTime ?? this.StatusDates.GetValueOrNull(DocuSignEnvelopeStatus.Declined);
            this.statuses[DocuSignEnvelopeStatus.Delivered] = envelopeSummary.DeliveredDateTime ?? this.StatusDates.GetValueOrNull(DocuSignEnvelopeStatus.Delivered);
            this.statuses[DocuSignEnvelopeStatus.Voided] = envelopeSummary.VoidedDateTime ?? this.StatusDates.GetValueOrNull(DocuSignEnvelopeStatus.Voided);
            
            this.UpdateRecipients(envelopeSummary.Recipients);

            if (this.CurrentStatus == DocuSignEnvelopeStatus.Completed)
            {
                this.UpdateReturnedDocuments(envelopeSummary.Documents);
            }

            this.Save();
        }

        /// <summary>
        /// Determines what app id to use when uploading a document to Edocs.
        /// </summary>
        /// <param name="documentId">The document id the doc to upload.</param>
        /// <param name="sentDocs">Dictionary of sent document ids.</param>
        /// <param name="edocToAppId">The lazy-loaded edocs mapped to app id.</param>
        /// <returns>The app id to use.</returns>
        private static Guid? DetermineAppId(int? documentId, Dictionary<int, DocuSignEdoc> sentDocs, Lazy<Dictionary<Guid, Guid?>> edocToAppId)
        {
            DocuSignEdoc sentDoc = null;
            Guid? sentDocAppId = null;
            if (documentId.HasValue && sentDocs.TryGetValue(documentId.Value, out sentDoc) && sentDoc.EdocId.HasValue &&
                edocToAppId.Value.TryGetValue(sentDoc.EdocId.Value, out sentDocAppId) && sentDocAppId.HasValue)
            {
                return sentDocAppId.Value;
            }

            return null;
        }

        /// <summary>
        /// Creates a list of envelopes from the returned results of the stored procedure.
        /// </summary>
        /// <param name="connection">The connection to use.</param>
        /// <param name="parameters">The parameters to pass into the stored procedure.</param>
        /// <param name="sproc">The stored procedure to run.</param>
        /// <returns>The list of found envelopes.</returns>
        private static List<DocuSignEnvelope> Load(DbConnection connection, IEnumerable<SqlParameter> parameters, StoredProcedureName sproc)
        {
            List<DocuSignEnvelope> envelopes = new List<DocuSignEnvelope>();
            using (var conn = connection)
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, sproc, parameters, TimeoutInSeconds.Thirty))
            {
                // Edocs section first
                var envelopeIdToEdocs = DocuSignEdoc.Load(reader);

                // Recipient auth results section
                reader.NextResult();
                var recipientIdToResults = DocuSignRecipientAuthResult.Load(reader);

                // Recipients section
                reader.NextResult();
                var envelopeIdToRecipients = DocuSignRecipient.Load(reader, recipientIdToResults);

                reader.NextResult();
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    var edocs = envelopeIdToEdocs[id];
                    var recipients = envelopeIdToRecipients[id];

                    envelopes.Add(new DocuSignEnvelope(reader, edocs, recipients));
                }
            }

            return envelopes;
        }

        /// <summary>
        /// Updates the recipients of the envelope after issuing a GET request.
        /// </summary>
        /// <param name="recipientsSummary">The recipient data from the request.</param>
        private void UpdateRecipients(EnvelopeRecipientsUpdateSummary recipientsSummary)
        {
            if (recipientsSummary == null)
            {
                return;
            }

            List<DocuSignRecipient> newRecipients = new List<DocuSignRecipient>();
            HashSet<int> updatedRecipientIds = new HashSet<int>();
            foreach (var recipientSummary in recipientsSummary.Recipients)
            {
                var guidId = recipientSummary.RecipientIdGuid;
                var customRecipientId = recipientSummary.CustomRecipientId;
                var foundRecipient = this.recipients.FirstOrDefault(record => record.RecipientIdGuid == guidId || record.CustomRecipientId == customRecipientId);

                if (foundRecipient != null)
                {
                    foundRecipient.Update(recipientSummary);

                    newRecipients.Add(foundRecipient);
                    updatedRecipientIds.Add(foundRecipient.Id);
                }
                else
                {
                    newRecipients.Add(DocuSignRecipient.Create(recipientSummary));
                }
            }

            this.recipientsToDelete = this.recipients.Where(recipient => !updatedRecipientIds.Contains(recipient.Id)).ToList();
            this.recipients = newRecipients;
        }

        /// <summary>
        /// Handles updating the returned document list of this envelope.
        /// </summary>
        /// <param name="documentListSummary">The document list summary.</param>
        private void UpdateReturnedDocuments(EnvelopeDocumentListUpdateSummary documentListSummary)
        {
            Lazy<Dictionary<Guid, Guid?>> edocToAppId = new Lazy<Dictionary<Guid, Guid?>>(() => EdocUtilities.GetEdocToAppIdMap(this.LoanId, this.BrokerId));
            Lazy<Guid> defaultAppId = new Lazy<Guid>(() =>
            {
                CPageData loanData = new CFullAccessPageData(this.LoanId, new[] { nameof(CAppData.aAppId) }); // we don't want any dependency on the principal here, since this code runs for the handler
                loanData.InitLoad();
                return loanData.GetAppData(0).aAppId;
            });

            this.returnedDocuments = this.returnedDocuments ?? new List<DocuSignEdoc>();
            var existingSentDocs = this.SentDocuments?.Where(doc => doc.DocumentId.HasValue)?.ToDictionary(doc => doc.DocumentId.Value) ?? new Dictionary<int, DocuSignEdoc>();
            var existingReturnedDocs = this.ReturnedDocuments?.Where(doc => doc.DocumentId.HasValue)?.ToDictionary(doc => doc.DocumentId.Value) ?? new Dictionary<int, DocuSignEdoc>();
            EDocumentRepository repo = EDocumentRepository.GetSystemRepository(this.BrokerId);
            foreach (var returnedDoc in documentListSummary.Documents)
            {
                if (!returnedDoc.DocumentId.HasValue && !returnedDoc.IsCertificate)
                {
                    continue;
                }

                DocuSignEdoc document = null;
                DocuSignEdoc matchedDocument = null;
                EDocument oldEdoc = null;

                // We want to check if the document is already being tracked.
                if (returnedDoc.IsCertificate)
                {
                    document = this.ReturnedDocuments?.FirstOrDefault((doc) => doc.IsCertificate);
                }
                else
                {
                    existingReturnedDocs.TryGetValue(returnedDoc.DocumentId.Value, out document);
                    existingSentDocs.TryGetValue(returnedDoc.DocumentId.Value, out matchedDocument);
                    try
                    {
                        oldEdoc = repo.GetDocumentById(matchedDocument?.EdocId ?? Guid.Empty);
                    }
                    catch (NotFoundException)
                    {
                        // No need to return an error or throw exception here.
                        Tools.LogWarning($"No document found to match returned document. Returned doc name: {returnedDoc.Name}.{(matchedDocument?.EdocId == null ? "" : "Matched EDocID: " + matchedDocument?.EdocId?.ToString() + ".")}");
                    }
                }

                if (document == null)
                {
                    document = DocuSignEdoc.CreateReceivedDocument(returnedDoc);
                    this.returnedDocuments.Add(document);
                }
                else
                {
                    document.UpdateDoc(returnedDoc);
                }

                if (!document.EdocId.HasValue)
                {
                    document.SaveToEDocs(oldEdoc?.DocType, returnedDoc.TempFilePath, this.BrokerId, this.LoanId, DetermineAppId(document.DocumentId, existingSentDocs, edocToAppId) ?? defaultAppId.Value);
                }

                if (oldEdoc != null 
                    && document.EdocId.HasValue 
                    && edocToAppId.Value.ContainsKey(matchedDocument.EdocId.Value)
                    && oldEdoc.DocStatus != E_EDocStatus.Obsolete)
                {
                    // There's a matched document with an existing edoc id and this document successfully saved to edocs. We want to obsolete the old one.
                    oldEdoc.DocStatus = E_EDocStatus.Obsolete;
                    oldEdoc.Save(null);
                }
            }
        }

        /// <summary>
        /// Saves the envelope to the DB.
        /// </summary>
        private void Save()
        {
            using (DbConnection connection = DbAccessUtils.GetConnection(this.BrokerId))
            {
                connection.Open();

                using (DbTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        this.SaveEnvelope(connection, transaction);

                        if (this.sentDocuments != null)
                        {
                            foreach (var edoc in this.sentDocuments)
                            {
                                edoc.Save(this.Id, connection, transaction);
                            }
                        }

                        if (this.returnedDocuments != null)
                        {
                            foreach (var edoc in this.returnedDocuments)
                            {
                                edoc.Save(this.Id, connection, transaction);
                            }
                        }

                        if (this.recipientsToDelete != null)
                        {
                            foreach (var recipient in this.recipientsToDelete)
                            {
                                recipient.Delete(connection, transaction);
                            }
                        }

                        if (this.recipients != null)
                        {
                            foreach (var recipient in this.recipients)
                            {
                                recipient.Save(this.Id, connection, transaction);
                            }
                        }

                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        Tools.LogError(e);
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Saves the envelope to its associated DB table.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="transaction">The transaction.</param>
        private void SaveEnvelope(DbConnection connection, DbTransaction transaction)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@CurrentStatus", this.CurrentStatus),
                new SqlParameter("@LoanId", this.LoanId),
                new SqlParameter("@BrokerId", this.BrokerId)
            };

            if (this.statuses != null)
            {
                foreach (var status in DbTrackedStatuses)
                {
                    DateTime? date = null;
                    if (this.statuses.TryGetValue(status.Key, out date))
                    {
                        parameters.Add(new SqlParameter($"@{status.Value}", date));
                    }
                }
            }

            StoredProcedureName sp;
            SqlParameter idParameter = null;
            if (this.isNew)
            {
                parameters.Add(new SqlParameter("@EnvelopeId", this.EnvelopeId));
                parameters.Add(new SqlParameter("@Name", this.Name));
                parameters.Add(new SqlParameter("@RequestedDate", this.RequestedDate));
                parameters.Add(new SqlParameter("@SenderEmail", this.SenderEmail));
                parameters.Add(new SqlParameter("@SenderName", this.SenderName));
                parameters.Add(new SqlParameter("@SenderUserId", this.SenderUserId));

                idParameter = new SqlParameter("@Id", System.Data.SqlDbType.Int)
                {
                    Direction = System.Data.ParameterDirection.Output,
                };
                parameters.Add(idParameter);

                sp = DocuSignEnvelopesCreate;
            }
            else
            {
                parameters.Add(new SqlParameter("@Id", this.Id));
                parameters.Add(new SqlParameter("@VoidReason", this.VoidReason));
                parameters.Add(new SqlParameter("@VoidDoneBy", this.VoidDoneBy));

                sp = DocuSignEnvelopesUpdate;
            }

            StoredProcedureDriverHelper.ExecuteNonQuery(connection, transaction, sp, parameters, TimeoutInSeconds.Thirty);

            if (this.isNew)
            {
                this.Id = (int)idParameter.Value;
            }

            this.isNew = false;
        }
    }
}
