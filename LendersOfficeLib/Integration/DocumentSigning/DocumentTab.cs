﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;

    /// <summary>
    /// Represents a document tab for signing, i.e. a location to either sign, initial, or date.
    /// </summary>
    public class DocumentTab
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentTab"/> class.
        /// </summary>
        /// <param name="documentIdentifier">The unique identifier for the document, i.e. <see cref="LqbDocument.DocuSignId"/>.</param>
        /// <param name="recipientIdentifier">The unique identifier for the recipient, i.e. <see cref="EnvelopeRecipient.Identifier"/>.</param>
        /// <param name="type">The type of the document tab.</param>
        /// <param name="pageNumber">The page number of the tab in the document.</param>
        /// <param name="xposition">The X-coordinate of the tab's position in the document measured from the bottom-left corner in units of 72 DPI.</param>
        /// <param name="yposition">The Y-coordinate of the tab's position in the document measured from the bottom-left corner in units of 72 DPI.</param>
        /// <param name="scale">The scale for resizing the tab smaller than the default.</param>
        public DocumentTab(Guid documentIdentifier, Guid recipientIdentifier, DocumentTabType type, int pageNumber, int xposition, int yposition, double scale = 1.0)
        {
            this.DocumentIdentifier = documentIdentifier;
            this.RecipientIdentifier = recipientIdentifier;
            this.Type = type;
            this.PageNumber = pageNumber;
            this.XPosition = xposition;
            this.YPosition = yposition;
            this.Scale = scale;
        }

        /// <summary>
        /// Gets the unique identifier for the document, i.e. <see cref="LqbDocument.DocuSignId"/>.
        /// </summary>
        public Guid DocumentIdentifier { get; }

        /// <summary>
        /// Gets the unique identifier for the recipient, i.e. <see cref="EnvelopeRecipient.Identifier"/>.
        /// </summary>
        public Guid RecipientIdentifier { get; }

        /// <summary>
        /// Gets the type of the document tab.
        /// </summary>
        public DocumentTabType Type { get; }

        /// <summary>
        /// Gets the page number of the tab in the document.
        /// </summary>
        public int PageNumber { get; }

        /// <summary>
        /// Gets the X-coordinate of the tab's position in the document measured from the bottom-left corner in units of 72 DPI.
        /// </summary>
        public int XPosition { get; }

        /// <summary>
        /// Gets the Y-coordinate of the tab's position in the document measured from the bottom-left corner in units of 72 DPI.
        /// </summary>
        public int YPosition { get; }

        /// <summary>
        /// Gets the scale for resizing the tab smaller than the default.
        /// </summary>
        public double Scale { get; }
    }
}
