﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides access to the legacy header authentication flow for DocuSign.
    /// </summary>
    internal class DocuSignLegacyAuthServiceAdapter
    {
        /// <summary>
        /// Authenticates and retrieves an authentication header and user data for future requests.
        /// </summary>
        /// <param name="loginData">The data to use for authentication with DocuSign.</param>
        /// <returns>The authentication header and user information, or an error result.</returns>
        public Result<Tuple<KeyValuePair<string, string>, DocuSignUserInfo>> GetAuthHeaderAndUserInfo(DocuSignAuthenticationData loginData)
        {
            var legacyHeaderData = new Dictionary<string, string>();
            legacyHeaderData.Add("Username", loginData.UserId);
            legacyHeaderData.Add("Password", loginData.Password);
            legacyHeaderData.Add("IntegratorKey", loginData.IntegratorKey);
            KeyValuePair<string, string> authHeader = new KeyValuePair<string, string>("X-DocuSign-Authentication", SerializationHelper.JsonNetSerialize(legacyHeaderData));

            Result<DocuSignUserInfo> userInfoResult = GetUserInfo(loginData.UseSandbox, authHeader);
            if (userInfoResult.HasError)
            {
                return Result<Tuple<KeyValuePair<string, string>, DocuSignUserInfo>>.Failure(userInfoResult.Error);
            }

            return Result<Tuple<KeyValuePair<string, string>, DocuSignUserInfo>>.Success(Tuple.Create(
                authHeader,
                userInfoResult.Value));
        }

        /// <summary>
        /// Given the basic login data, this method retrieves the user info via /v2/login_information.
        /// </summary>
        /// <remarks>
        /// Per DocuSign: "This method must only be used for the Legacy Header Authentication flow."
        /// <a href="https://docs.docusign.com/esign/restapi/Authentication/Authentication/login/#/definitions/loginAccount">docs</a>.
        /// </remarks>
        /// <param name="useSandbox">A value indicating whether the DocuSign sandbox environment (demo.docusign.net) should be used.</param>
        /// <param name="authenticationHeader">The authentication header to include in the request.</param>
        /// <returns>The user information for the authenticated user, or an error result.</returns>
        private static Result<DocuSignUserInfo> GetUserInfo(bool useSandbox, KeyValuePair<string, string> authenticationHeader)
        {
            string domain = useSandbox ? "https://demo.docusign.net/restapi" : "https://www.docusign.net/restapi";
            LqbAbsoluteUri url = LqbAbsoluteUri.Create(domain + "/v2/login_information").ForceValue();

            var options = new LqbGrammar.Drivers.HttpRequest.HttpRequestOptions();
            options.Method = HttpMethod.Get;
            options.AddHeader(authenticationHeader.Key, authenticationHeader.Value);

            Result<string> result = CommunicateForString(url, options);
            if (result.HasError)
            {
                return Result<DocuSignUserInfo>.Failure(result.Error);
            }

            var loginInfo = SerializationHelper.JsonNetDeserialize<LoginInformation>(result.Value);
            foreach (LoginAccountEntry loginEntry in loginInfo.loginAccounts.CoalesceWithEmpty())
            {
                if (StringComparer.OrdinalIgnoreCase.Equals(loginEntry.isDefault, "true"))
                {
                    int endOfBaseUrl = loginEntry.baseUrl.IndexOf("/v2", StringComparison.OrdinalIgnoreCase);
                    if (endOfBaseUrl < 0)
                    {
                        return Result<DocuSignUserInfo>.Failure(DataAccess.CBaseException.GenericException("Unexpected URL from DocuSign"));
                    }
                    else if (loginEntry.accountId != System.Net.WebUtility.UrlEncode(loginEntry.accountId))
                    {
                        return Result<DocuSignUserInfo>.Failure(DataAccess.CBaseException.GenericException("Invalid or unexpected account id")); // expecting int-ish or guid
                    }

                    LqbAbsoluteUri baseUrl = LqbAbsoluteUri.Create(loginEntry.baseUrl.Remove(endOfBaseUrl)).ForceValue();
                    return Result<DocuSignUserInfo>.Success(new DocuSignUserInfo(
                        userId: loginEntry.userId,
                        defaultAccountId: loginEntry.accountId,
                        baseUrl: baseUrl,
                        name: loginEntry.userName,
                        email: loginEntry.email));
                }
            }

            return Result<DocuSignUserInfo>.Failure(DataAccess.CBaseException.GenericException("Unable to find user"));
        }

        /// <summary>
        /// Wraps the driver for web requests to handle error responses by catching the exception.
        /// </summary>
        /// <param name="url">The target URL.</param>
        /// <param name="options">The options that control the request and response parsing.</param>
        /// <returns>A successful response string, or an error result.</returns>
        private static Result<string> CommunicateForString(LqbAbsoluteUri url, LqbGrammar.Drivers.HttpRequest.HttpRequestOptions options)
        {
            return DocuSignAdapter.CommunicateForString(url, options, DocuSignLoggingOptions.OnlyLogUrls);
        }

        /// <summary>
        /// Serialization POCO for storing the result of a call to /v2/login_information.
        /// </summary>
        /// <remarks>
        /// <a href="https://docs.docusign.com/esign/restapi/Authentication/Authentication/login/#/definitions/Authentication">Documentation as of 2018-01</a>.
        /// </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "*", Justification = "DocuSign JSON response format, strictly used for deserialization.")]
        private class LoginInformation
        {
            /// <summary>
            /// Gets or sets the list of accounts that the authenticating user is a member of.
            /// </summary>
            public LoginAccountEntry[] loginAccounts { get; set; }
        }

        /// <summary>
        /// Serialization POCO for storing the result.
        /// </summary>
        /// <remarks>
        /// <a href="https://docs.docusign.com/esign/restapi/Authentication/Authentication/login/#/definitions/loginAccount">Documentation as of 2018-01</a>.
        /// </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "*", Justification = "DocuSign JSON response format, strictly used for deserialization.")]
        private class LoginAccountEntry
        {
            /// <summary>
            /// Gets or sets the email address for the user.
            /// </summary>
            public string email { get; set; }

            /// <summary>
            /// Gets or sets the account ID associated with the envelope.
            /// </summary>
            public string accountId { get; set; }

            /// <summary>
            /// Gets or sets a string boolean ("true" or "false") indicating if this entry is the default account for the user.
            /// </summary>
            public string isDefault { get; set; }

            /// <summary>
            /// Gets or sets the URL that should be used for successive calls to this account.
            /// </summary>
            public string baseUrl { get; set; }

            /// <summary>
            /// Gets or sets the user ID of the user being accessed.
            /// </summary>
            public string userId { get; set; }

            /// <summary>
            /// Gets or sets the name of this user as defined by the account. 
            /// </summary>
            public string userName { get; set; }
        }
    }
}
