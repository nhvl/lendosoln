﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using EDocs;

    /// <summary>
    /// Viewmodel for envelopes.
    /// </summary>
    public class DocuSignEnvelopeViewModel
    {
        /// <summary>
        /// Edocs that can be viewed.
        /// </summary>
        private HashSet<Guid> availableEdocs = new HashSet<Guid>();

        /// <summary>
        /// Initializes a new instance of the <see cref="DocuSignEnvelopeViewModel"/> class.
        /// </summary>
        /// <param name="envelopeRecord">The envelope record to create the view model from.</param>
        /// <param name="availableEdocs">The edocs that can be viewed.</param>
        public DocuSignEnvelopeViewModel(DocuSignEnvelope envelopeRecord, Dictionary<Guid, EDocument> availableEdocs)
        {
            if (envelopeRecord != null)
            {
                this.Id = envelopeRecord.Id;
                this.Name = envelopeRecord.Name;
                this.Status = envelopeRecord.CurrentStatus;
                this.CreationDate = envelopeRecord.RequestedDate.ToString("O");
                this.SenderName = envelopeRecord.SenderName;
                this.Recipients = envelopeRecord.Recipients.OrderBy(rec => rec.SigningOrder).Select(record => new DocuSignRecipientViewModel(record)).ToList();
                this.SentDocuments = envelopeRecord.SentDocuments.Select(record => new DocuSignDocumentViewModel(record, availableEdocs)).ToList();
                this.ReturnedDocuments = envelopeRecord.ReturnedDocuments?.Select(record => new DocuSignDocumentViewModel(record, availableEdocs))?.ToList();
                this.IncompleteInPerson = envelopeRecord.CurrentStatus != DocuSignEnvelopeStatus.Completed && envelopeRecord.CurrentStatus != DocuSignEnvelopeStatus.Voided && envelopeRecord.Recipients.Any(recipient => recipient.Type == RecipientType.InPersonSigner && recipient.CurrentStatus != DocuSignRecipientStatus.Signed && recipient.CurrentStatus != DocuSignRecipientStatus.Completed);
                this.StatusDates = new List<DocuSignStatusDatesViewModel>();
                foreach (var statusDate in envelopeRecord.StatusDates.OrderByDescending((date) => date.Value))
                {
                    if (!statusDate.Value.HasValue)
                    {
                        // We'll skip null dates.
                        continue;
                    }

                    string note = null;
                    string doneBy = "DocuSign System";
                    if (statusDate.Key == DocuSignEnvelopeStatus.Voided)
                    {
                        note = envelopeRecord.VoidReason;
                        doneBy = envelopeRecord.VoidDoneBy;
                    }
                    else if (statusDate.Key == DocuSignEnvelopeStatus.Sent)
                    {
                        doneBy = envelopeRecord.SenderName;
                    }

                    this.StatusDates.Add(new DocuSignStatusDatesViewModel(statusDate.Key.GetDescription(), statusDate.Value) { Note = note, DoneBy = doneBy });
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the envelope.
        /// </summary>
        /// <value>The id of the envelope.</value>
        public int Id
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the name of the envelope.
        /// </summary>
        /// <value>The name of the envelope.</value>
        public string Name
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the status of the envelope.
        /// </summary>
        /// <value>The status of the envelope.</value>
        public DocuSignEnvelopeStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the status of the envelope as a string.
        /// </summary>
        /// <value>The status of the envelope as a string.</value>
        public string Status_rep
        {
            get
            {
                return this.Status.GetDescription();
            }

            set
            {
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the envelope can be voided.
        /// </summary>
        /// <value>A value indicating whether the envelope can be voided.</value>
        public bool CanVoid
        {
            get
            {
                return !(this.Status == DocuSignEnvelopeStatus.Completed ||
                         this.Status == DocuSignEnvelopeStatus.Deleted ||
                         this.Status == DocuSignEnvelopeStatus.Processing ||
                         this.Status == DocuSignEnvelopeStatus.Template ||
                         this.Status == DocuSignEnvelopeStatus.TimedOut ||
                         this.Status == DocuSignEnvelopeStatus.Voided);
            }

            set
            {
            }
        }

        /// <summary>
        /// Gets or sets the status dates.
        /// </summary>
        /// <value>The status dates.</value>
        public List<DocuSignStatusDatesViewModel> StatusDates
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the status dates have notes.
        /// </summary>
        /// <value>Whether the status dates have notes.</value>
        public bool StatusDatesHasNotes
        {
            get
            {
                return this.StatusDates?.Any(date => !string.IsNullOrEmpty(date.Note)) ?? false;
            }

            set
            {
            }
        }

        /// <summary>
        /// Gets or sets the requested date as a string.
        /// </summary>
        /// <value>The requested date as a string.</value>
        public string CreationDate
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the sender's name.
        /// </summary>
        /// <value>The sender's name.</value>
        public string SenderName
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the list of recipient view models.
        /// </summary>
        /// <value>The recipient view models.</value>
        public List<DocuSignRecipientViewModel> Recipients
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the sent document view models.
        /// </summary>
        /// <value>The sent document view models.</value>
        public List<DocuSignDocumentViewModel> SentDocuments
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the returned document view models.
        /// </summary>
        /// <value>The returned document view models.</value>
        public List<DocuSignDocumentViewModel> ReturnedDocuments
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the envelope has an incomplete in-person signing session.
        /// </summary>
        /// <value>Whether the envelope has incomplete in-person signing.</value>
        public bool IncompleteInPerson
        {
            get; set;
        }
    }
}
