﻿namespace LendersOffice.Integration.DocumentSigning
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Contains information for creating a recipient view for embedded signing.
    /// </summary>
    public class RecipientViewRequest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RecipientViewRequest"/> class.
        /// </summary>
        /// <param name="envelopeId">The identifier for the envelope.</param>
        /// <param name="authenticationMethod">The authentication method.</param>
        /// <param name="clientUserId">The user identifier authenticating an account in LendingQB.</param>
        /// <param name="email">The email of the recipient.</param>
        /// <param name="userName">The name of the recipient.</param>
        /// <param name="returnUrl">The return for DocuSign to direct back to our app.</param>
        public RecipientViewRequest(string envelopeId, string authenticationMethod, string clientUserId, string email, string userName, LqbAbsoluteUri returnUrl)
        {
            this.EnvelopeId = envelopeId;
            this.AuthenticationMethod = authenticationMethod;
            this.ClientUserId = clientUserId;
            this.Email = email;
            this.UserName = userName;
            this.ReturnUrl = returnUrl;
        }

        /// <summary>
        /// Gets the envelope ID to get a view URL for.
        /// </summary>
        public string EnvelopeId { get; }

        /// <summary>
        /// Gets the authentication method to send to DocuSign.
        /// </summary>
        public string AuthenticationMethod { get; }

        /// <summary>
        /// Gets the user ID we use to authenticate the account in LendingQB.
        /// </summary>
        public string ClientUserId { get; }

        /// <summary>
        /// Gets the email of the recipient.
        /// </summary>
        public string Email { get; }

        /// <summary>
        /// Gets the recipient ID recognized by DocuSign.
        /// </summary>
        public string RecipientId { get; }

        /// <summary>
        /// Gets the return URL for DocuSign to redirect back to our application.
        /// </summary>
        public LqbAbsoluteUri ReturnUrl { get; }

        /// <summary>
        /// Gets the user name of the recipient.
        /// </summary>
        public string UserName { get; }

        /// <summary>
        /// Creates the serializable object representing a recipient view request.
        /// </summary>
        /// <returns>An object supporting basic serialization to JSON.</returns>
        public object ToSerializableData()
        {
            return new
            {
                authenticationMethod = this.AuthenticationMethod,
                clientUserId = this.ClientUserId,
                email = this.Email,
                userName = this.UserName,
                recipientId = this.RecipientId,
                returnUrl = this.ReturnUrl.ToString(),
            };
        }
    }
}
