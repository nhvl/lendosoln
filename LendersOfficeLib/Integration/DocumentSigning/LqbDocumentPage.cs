﻿namespace LendersOffice.Integration.DocumentSigning
{
    /// <summary>
    /// Represents a page of a document in LQB via the dimensions of the page.
    /// </summary>
    public class LqbDocumentPage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LqbDocumentPage"/> class.
        /// </summary>
        /// <param name="height">The height of the page in dots measured at 72 DPI (dots per inch).</param>
        /// <param name="width">The width of the page in dots measured at 72 DPI (dots per inch).</param>
        public LqbDocumentPage(int height, int width)
        {
            this.Height = height;
            this.Width = width;
        }

        /// <summary>
        /// Gets the height of the page in dots measured at 72 DPI (dots per inch).
        /// </summary>
        public int Height { get; }

        /// <summary>
        /// Gets the width of the page in dots measured at 72 DPI (dots per inch).
        /// </summary>
        public int Width { get; }
    }
}
