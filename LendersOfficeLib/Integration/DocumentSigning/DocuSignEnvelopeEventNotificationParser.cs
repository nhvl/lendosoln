﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides parsing for DocuSign Envelope Information XML.
    /// </summary>
    public partial class DocuSignEnvelopeEventNotificationParser
    {
        /// <summary>
        /// Cleans up any temp files created when <paramref name="envelopeInformation"/> was parsed.
        /// </summary>
        /// <param name="envelopeInformation">The data to search for temp files.</param>
        public static void DeleteTempFiles(DocuSignEnvelopeInformation envelopeInformation)
        {
            foreach (var documentPdf in (envelopeInformation?.DocumentPDFs).CoalesceWithEmpty())
            {
                DeleteFile(documentPdf?.PDFBytes);
            }

            foreach (var recipientStatus in (envelopeInformation?.EnvelopeStatus?.RecipientStatuses).CoalesceWithEmpty())
            {
                foreach (var attachment in (recipientStatus?.RecipientAttachment).CoalesceWithEmpty())
                {
                    DeleteFile(attachment?.Data);
                }
            }
        }

        /// <summary>
        /// Reads an XML blob into an instance.
        /// </summary>
        /// <param name="reader">The reader to read.</param>
        /// <param name="log">The log of the read XML.</param>
        /// <returns>The envelope data, or an error result.</returns>
        public Result<DocuSignEnvelopeInformation> Read(XmlReader reader, XmlWriter log)
        {
            try
            {
                if (!reader.IsStartElement("DocuSignEnvelopeInformation", TargetNamespace))
                {
                    var context = new LqbGrammar.Exceptions.SimpleContext("Expected Element to DocuSignEnvelopeInformation; instead received " + reader.NodeType + " for " + reader.LocalName + " (namespace: " + reader.NamespaceURI + ").");
                    return Result<DocuSignEnvelopeInformation>.Failure(new LqbGrammar.Exceptions.DeveloperException(ErrorMessage.SystemError, context));
                }

                return Result<DocuSignEnvelopeInformation>.Success(this.ReadDocuSignEnvelopeInformation(reader, log));
            }
            catch (XmlException exception)
            {
                return Result<DocuSignEnvelopeInformation>.Failure(new LqbGrammar.Exceptions.DeveloperException(ErrorMessage.SystemError, exception));
            }
        }

        /// <summary>
        /// Deletes the specified file at the local file path.
        /// </summary>
        /// <param name="filePath">The file path to delete the file from.</param>
        private static void DeleteFile(LocalFilePath? filePath)
        {
            if (filePath.HasValue)
            {
                Drivers.FileSystem.FileOperationHelper.Delete(filePath.Value);
            }
        }

        /// <summary>
        /// Reads an array of <see cref="RecipientStatus"/> element that could have the <c>xsi:nil=&quot;true&quot;</c> attribute.
        /// </summary>
        /// <param name="reader">The reader to read.</param>
        /// <param name="log">The ongoing log of read XML.</param>
        /// <returns>A list of <see cref="RecipientStatus"/>, or an error result.</returns>
        private IReadOnlyList<RecipientStatus> ReadNillableArrayOfRecipientStatus(XmlReader reader, XmlWriter log)
        {
            return this.ReadArrayOfRecipientStatus(reader, log);
        }

        /// <summary>
        /// Reads a string element that could have the <c>xsi:nil=&quot;true&quot;</c> attribute.
        /// </summary>
        /// <param name="reader">The reader to read.</param>
        /// <param name="log">The ongoing log of read XML.</param>
        /// <returns>A string, or an error result.</returns>
        private string ReadNillableString(XmlReader reader, XmlWriter log)
        {
            return this.ReadString(reader, log);
        }

        /// <summary>
        /// Reads Base64 content into a temporary file.
        /// </summary>
        /// <param name="reader">The reader to read.</param>
        /// <param name="log">The ongoing log of read XML.</param>
        /// <returns>A local file path to the temp file containing the file data, or an error result.</returns>
        private LocalFilePath ReadBase64(XmlReader reader, XmlWriter log)
        {
            LocalFilePath tempFile = LqbGrammar.Utils.TempFileUtils.NewTempFilePath();

            const int BufferSize = 3 * 1024; // base64 uses 3 bytes per 4 characters, so this should ensure it can read an even character
            byte[] buffer = new byte[BufferSize];
            int totalBytes = 0;
            Action<LqbGrammar.Drivers.FileSystem.LqbBinaryStream> writer = binaryStream =>
            {
                int readBytes = -1;
                while ((readBytes = reader.ReadElementContentAsBase64(buffer, 0, BufferSize)) > 0)
                {
                    totalBytes += readBytes;
                    binaryStream.Stream.Write(buffer, 0, readBytes);
                }
            };

            string startElementName = reader.Name;
            this.LogStartElement(reader, log);
            bool isEmptyElement = reader.IsEmptyElement;
            Drivers.FileSystem.BinaryFileHelper.OpenNew(tempFile, writer);
            if (!isEmptyElement)
            {
                this.LogString("[Base64 content redacted.  Read " + totalBytes + " bytes into " + tempFile.Value + "]", log);
                this.LogFullEndElement(reader, log);
            }

            return tempFile;
        }

        /// <summary>
        /// Reads an unknown element from <paramref name="reader"/> and records the data to <paramref name="log"/>.
        /// </summary>
        /// <param name="reader">The reader containing the XML.</param>
        /// <param name="log">The log recording processed data.</param>
        private void ReadUnknownElement(XmlReader reader, XmlWriter log)
        {
            string startElementName = reader.Name;
            this.LogStartElement(reader, log);
            if (reader.IsEmptyElement)
            {
                reader.Read();
                return;
            }

            reader.Read();
            while (!(reader.MoveToContent() == XmlNodeType.EndElement && reader.Name == startElementName))
            {
                if (reader.IsStartElement())
                {
                    this.ReadUnknownElement(reader, log);
                }
                else
                {
                    this.ReadUnknownData(reader, log);
                }
            }

            this.LogFullEndElement(reader, log);
            reader.Read();
        }

        /// <summary>
        /// Reads a piece of unknown data from <paramref name="reader"/> and records the data to <paramref name="log"/>.
        /// </summary>
        /// <param name="reader">The reader containing the XML.</param>
        /// <param name="log">The log recording processed data.</param>
        private void ReadUnknownData(XmlReader reader, XmlWriter log)
        {
            if (reader.NodeType == XmlNodeType.Text)
            {
                log.WriteString(reader.Value);
            }
            else if (reader.NodeType == XmlNodeType.CDATA)
            {
                log.WriteCData(reader.Value);
            }
            else if (reader.NodeType == XmlNodeType.EntityReference)
            {
                log.WriteEntityRef(reader.Name);
            }

            reader.Read();
        }

        /// <summary>
        /// Logs an occurrence of a start element.
        /// </summary>
        /// <param name="reader">The reader containing the XML.</param>
        /// <param name="log">The log recording processed data.</param>
        private void LogStartElement(XmlReader reader, XmlWriter log)
        {
            log.WriteStartElement(reader.Prefix, reader.LocalName, reader.NamespaceURI);
            log.WriteAttributes(reader, true);
            if (reader.IsEmptyElement)
            {
                log.WriteEndElement();
            }
        }

        /// <summary>
        /// Logs an end element (not self-closing).
        /// </summary>
        /// <param name="reader">The reader containing the XML.</param>
        /// <param name="log">The log recording processed data.</param>
        private void LogFullEndElement(XmlReader reader, XmlWriter log)
        {
            log.WriteFullEndElement();
        }

        /// <summary>
        /// Logs a string to <paramref name="log"/>.
        /// </summary>
        /// <param name="str">The string to log.</param>
        /// <param name="log">The log recording processed data.</param>
        private void LogString(string str, XmlWriter log)
        {
            log.WriteString(str);
        }
    }
}