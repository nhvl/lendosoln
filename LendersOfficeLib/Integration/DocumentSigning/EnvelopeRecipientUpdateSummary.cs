﻿namespace LendersOffice.Integration.DocumentSigning
{
    using System;
    using Common;

    /// <summary>
    /// Class holding update information about an envelope recipient.
    /// </summary>
    public class EnvelopeRecipientUpdateSummary
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnvelopeRecipientUpdateSummary"/> class.
        /// </summary>
        /// <param name="routingOrder">The routing order.</param>
        /// <param name="sentDateTime">The sent date time.</param>
        /// <param name="deliveredDateTime">The delivered date time.</param>
        /// <param name="signedDateTime">The signed date time.</param>
        /// <param name="declinedDateTime">The declined date time.</param>
        /// <param name="declinedReason">The declined reason.</param>
        /// <param name="status">The status.</param>
        /// <param name="recipientIdGuid">The recipient id guid assigned by DocuSign.</param>
        /// <param name="customRecipientId">The custom recipient id passed to DocuSign.</param>
        /// <param name="email">The email of the recipient.</param>
        /// <param name="name">The name of the recipient.</param>
        /// <param name="accessCode">The access code, if it is used.</param>
        /// <param name="recipientAuthenticationStatus">The authentication attempt statuses for this user.</param>
        /// <param name="type">The type of the recipient.</param>
        public EnvelopeRecipientUpdateSummary(
            int routingOrder,
            DateTime? sentDateTime,
            DateTime? deliveredDateTime,
            DateTime? signedDateTime,
            DateTime? declinedDateTime,
            string declinedReason,
            DocuSignRecipientStatus status,
            string recipientIdGuid,
            int customRecipientId,
            string email,
            string name,
            string accessCode,
            RecipientAuthStatusUpdateSummary recipientAuthenticationStatus,
            RecipientType type)
        {
            this.RoutingOrder = routingOrder;
            this.SentDateTime = sentDateTime;
            this.DeliveredDateTime = deliveredDateTime;
            this.SignedDateTime = signedDateTime;
            this.DeclinedDateTime = declinedDateTime;
            this.DeclinedReason = declinedReason;
            this.Status = status;
            this.RecipientIdGuid = recipientIdGuid;
            this.CustomRecipientId = customRecipientId;
            this.Email = email;
            this.Name = name;
            this.AccessCode = accessCode;
            this.RecipientAuthenticationStatus = recipientAuthenticationStatus;
            this.Type = type;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EnvelopeRecipientUpdateSummary"/> class.
        /// </summary>
        /// <param name="envelopeRecipient">A recipient from a deserialized envelope recipients list response.</param>
        /// <param name="type">The type of this recipient.</param>
        public EnvelopeRecipientUpdateSummary(Serialization.EnvelopeRecipient envelopeRecipient, RecipientType type)
        {
            this.DeclinedDateTime = envelopeRecipient.declinedDateTime?.ToNullable<DateTime>(DateTime.TryParse);
            this.DeliveredDateTime = envelopeRecipient.deliveredDateTime?.ToNullable<DateTime>(DateTime.TryParse);
            this.SentDateTime = envelopeRecipient.sentDateTime?.ToNullable<DateTime>(DateTime.TryParse); 
            this.SignedDateTime = envelopeRecipient.signedDateTime?.ToNullable<DateTime>(DateTime.TryParse);
            this.Status = DocuSignRecipient.ParseStatus(envelopeRecipient.status);
            this.Email = envelopeRecipient.email;
            this.Name = envelopeRecipient.name;
            this.DeclinedReason = envelopeRecipient.declinedReason;
            this.RecipientIdGuid = envelopeRecipient.recipientIdGuid;
            this.CustomRecipientId = int.Parse(envelopeRecipient.recipientId);
            this.RoutingOrder = int.Parse(envelopeRecipient.routingOrder);
            this.Type = type;
            this.AccessCode = envelopeRecipient.accessCode;

            if (type == RecipientType.InPersonSigner)
            {
                this.Name = envelopeRecipient.signerName;
                this.Email = envelopeRecipient.signerEmail;
            }

            this.RecipientAuthenticationStatus = envelopeRecipient.recipientAuthenticationStatus == null ? null : new RecipientAuthStatusUpdateSummary(envelopeRecipient.recipientAuthenticationStatus);
        }

        /// <summary>
        /// Gets the routing order.
        /// </summary>
        public int RoutingOrder { get; private set; }

        /// <summary>
        /// Gets the sent date time.
        /// </summary>
        public DateTime? SentDateTime { get; private set; }

        /// <summary>
        /// Gets the delivered date time.
        /// </summary>
        public DateTime? DeliveredDateTime { get; private set; }

        /// <summary>
        /// Gets the signed date time.
        /// </summary>
        public DateTime? SignedDateTime { get; private set; }

        /// <summary>
        /// Gets the declined date time.
        /// </summary>
        public DateTime? DeclinedDateTime { get; private set; }

        /// <summary>
        /// Gets the declined reason.
        /// </summary>
        public string DeclinedReason { get; private set; }

        /// <summary>
        /// Gets the status.
        /// </summary>
        public DocuSignRecipientStatus Status { get; private set; }

        /// <summary>
        /// Gets the recipient id guid assigned by DocuSign.
        /// </summary>
        public string RecipientIdGuid { get; private set; }

        /// <summary>
        /// Gets the custom recipient id passed to DocuSign.
        /// </summary>
        public int CustomRecipientId { get; private set; }

        /// <summary>
        /// Gets the email of the recipient.
        /// </summary>
        public string Email { get; private set; }

        /// <summary>
        /// Gets the name of the recipient.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the access code of the recipient if applicable.
        /// </summary>
        public string AccessCode { get; private set; }

        /// <summary>
        /// Gets the authentication attempt statuses for this user.
        /// </summary>
        public RecipientAuthStatusUpdateSummary RecipientAuthenticationStatus { get; private set; }

        /// <summary>
        /// Gets the type of the recipient.
        /// </summary>
        public RecipientType Type { get; private set; }
    }
}
