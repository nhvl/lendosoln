﻿namespace LendersOffice.Integration.DocumentCapture
{
    using System;
    using Admin;
    using EDocs;
    using Security;

    /// <summary>
    /// Provides a simple container for KTA document upload parameters.
    /// </summary>
    public class KtaDocumentUploadParameters
    {
        /// <summary>
        /// Gets or sets the user ID of the employee.
        /// </summary>
        /// <value>
        /// The user ID of the employee.
        /// </value>
        public Guid? UserId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the employee.
        /// </summary>
        /// <value>
        /// The ID of the employee.
        /// </value>
        public Guid EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the originating company for the user.
        /// </summary>
        public Guid PmlBrokerId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the branch.
        /// </summary>
        /// <value>
        /// The ID of the branch.
        /// </value>
        public Guid BranchId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the broker.
        /// </summary>
        /// <value>
        /// The ID of the broker.
        /// </value>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the loan.
        /// </summary>
        /// <value>
        /// The ID of the loan.
        /// </value>
        public Guid LoanId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the app.
        /// </summary>
        /// <value>
        /// The ID of the app.
        /// </value>
        public Guid ApplicationId { get; set; }

        /// <summary>
        /// Gets or sets the type of user.
        /// </summary>
        /// <value>
        /// The type of user.
        /// </value>
        /// <remarks>
        /// Unless otherwise specified, document uploads from
        /// other integrations always assume the upload to be
        /// from a B user. This behavior is replicated here.
        /// </remarks>
        public string UserType { get; set; } = "B";

        /// <summary>
        /// Gets or sets the user login name.
        /// </summary>
        /// <value>
        /// The user login name.
        /// </value>
        public string UserLoginName { get; set; }

        /// <summary>
        /// Gets or sets the customer code for the broker.
        /// </summary>
        /// <value>
        /// The customer code for the broker.
        /// </value>
        public string CustomerCode { get; set; }

        /// <summary>
        /// Gets or sets the source of the document upload.
        /// </summary>
        /// <value>
        /// The source of the document upload.
        /// </value>
        public E_EDocumentSource DocumentSource { get; set; }

        /// <summary>
        /// Gets or sets the PDF content bytes.
        /// </summary>
        /// <value>
        /// The PDF content bytes.
        /// </value>
        public byte[] PdfBytes { get; set; }

        /// <summary>
        /// Gets or sets the path to the PDF.
        /// </summary>
        /// <value>
        /// The path to the PDF.
        /// </value>
        public string PdfPath { get; set; }

        /// <summary>
        /// Creates and returns a new instance of the <see cref="KtaDocumentUploadParameters"/>
        /// class using data from the provided employee.
        /// </summary>
        /// <param name="employee">
        /// The employee.
        /// </param>
        /// <returns>
        /// The new instance.
        /// </returns>
        public static KtaDocumentUploadParameters FromEmployee(EmployeeDB employee)
        {
            return new KtaDocumentUploadParameters()
            {
                UserId = employee.UserID,
                EmployeeId = employee.ID,
                PmlBrokerId = employee.PmlBrokerId,
                BranchId = employee.BranchID,
                BrokerId = employee.BrokerID,
                UserType = employee.UserType.ToString(),
                UserLoginName = employee.LoginName,
            };
        }

        /// <summary>
        /// Creates and returns a new instance of the <see cref="KtaDocumentUploadParameters"/>
        /// class using data from the provided user principal.
        /// </summary>
        /// <param name="principal">
        /// The principal for the user.
        /// </param>
        /// <returns>
        /// The new instance.
        /// </returns>
        public static KtaDocumentUploadParameters FromUserPrincipal(AbstractUserPrincipal principal)
        {
            return new KtaDocumentUploadParameters()
            {
                UserId = principal.UserId,
                EmployeeId = principal.EmployeeId,
                PmlBrokerId = principal.PmlBrokerId,
                BranchId = principal.BranchId,
                BrokerId = principal.BrokerId,
                UserType = principal.Type,
                UserLoginName = principal.LoginNm,
                CustomerCode = principal.BrokerDB.CustomerCode
            };
        }
    }
}
