﻿namespace LendersOffice.Integration.DocumentCapture
{
    using System;
    using System.Xml.Linq;
    using Common;
    using Constants;
    using DataAccess;

    /// <summary>
    /// Provides a way to handle authentication for LQB transmission to KTA.
    /// </summary>
    public class KtaAuthenticationHandler
    {
        /// <summary>
        /// The format for request times expected by KTA.
        /// </summary>
        private const string RequestTimeFormat = "yyyMMddHHmmss";

        /// <summary>
        /// The name of the root element in the XML payload expected by KTA.
        /// </summary>
        private const string XmlPayloadRootElementName = "KTAPayload";

        /// <summary>
        /// Initializes a new instance of the <see cref="KtaAuthenticationHandler"/> class.
        /// </summary>
        /// <param name="request">
        /// The authentication request.
        /// </param>
        public KtaAuthenticationHandler(KtaAuthenticationRequest request)
        {
            if (!request.Validate())
            {
                throw new CBaseException(ErrorMessages.GenericAccessDenied, "Invalid request.");
            }

            this.Request = request;
        }

        /// <summary>
        /// Gets the encrypted cookie data.
        /// </summary>
        /// <value>
        /// The encrypted cookie data.
        /// </value>
        public string EncryptedCookieData { get; private set; }

        /// <summary>
        /// Gets the encrypted XML payload for the transmission.
        /// </summary>
        /// <value>
        /// The encrypted XML payload for the transmission.
        /// </value>
        public string EncryptedXmlPayload { get; private set; }

        /// <summary>
        /// Gets or sets the authentication request.
        /// </summary>
        /// <value>
        /// The authentication request.
        /// </value>
        private KtaAuthenticationRequest Request { get; set; }

        /// <summary>
        /// Generates the authentication data using the 
        /// supplied username and password.
        /// </summary>
        public void Generate()
        {
            var transactionId = Guid.NewGuid();
            var requestTime = DateTime.Now;

            this.GenerateCookieData(transactionId, requestTime);
            this.GenerateXmlPayload(transactionId, requestTime);
        }

        /// <summary>
        /// Generates the authentication cookie data.
        /// </summary>
        /// <param name="transactionId">
        /// The ID for the transaction.
        /// </param>
        /// <param name="requestTime">
        /// The time for the transaction.
        /// </param>
        private void GenerateCookieData(Guid transactionId, DateTime requestTime)
        {
            var cookieData = string.Join(
                "|",
                transactionId.ToString("N"), 
                requestTime.ToString(RequestTimeFormat), 
                this.Request.UserLogin);

            this.EncryptedCookieData = EncryptionHelper.EncryptKta(cookieData);
        }

        /// <summary>
        /// Generates the XML payload for the transmission.
        /// </summary>
        /// <param name="transactionId">
        /// The ID for the transaction.
        /// </param>
        /// <param name="requestTime">
        /// The time for the transaction.
        /// </param>
        private void GenerateXmlPayload(Guid transactionId, DateTime requestTime)
        {
            var root = new XElement(XmlPayloadRootElementName);

            root.Add(new XElement("Id", transactionId.ToString("N")));
            root.Add(new XElement("RequestTime", requestTime.ToString(RequestTimeFormat)));
            root.Add(new XElement("UserLogin", this.Request.UserLogin));
            root.Add(new XElement("URL", ConstStage.AmazonEdocsClientUrl));
            root.Add(new XElement("SecretKey", ConstStage.KtaAmazonSecretKey));
            root.Add(new XElement("KtaLogin", this.Request.KtaUsername));
            root.Add(new XElement("KtaPass", this.Request.KtaPassword.Value));
            root.Add(new XElement("OcrId", this.Request.RequestId.Value));
            root.Add(new XElement("CustomerCode", this.Request.CustomerCode));
            root.Add(new XElement("LoanName", this.Request.LoanName));

            var xmlPayload = root.ToString(SaveOptions.DisableFormatting);
            this.EncryptedXmlPayload = EncryptionHelper.EncryptKta(xmlPayload);
        }
    }
}
