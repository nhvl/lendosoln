﻿namespace LendersOffice.Integration.DocumentCapture
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using Common;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Represents the status of an OCR request.
    /// </summary>
    public class OcrStatus
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OcrStatus"/> class.
        /// </summary>
        /// <param name="requestId">
        /// The ID of the request.
        /// </param>
        /// <param name="brokerId">
        /// The ID of the broker.
        /// </param>
        public OcrStatus(OcrRequestId requestId, Guid brokerId)
        {
            this.RequestId = requestId;
            this.BrokerId = brokerId;
        }

        /// <summary>
        /// Gets or sets the request ID of the request.
        /// </summary>
        /// <value>
        /// The request ID of the request.
        /// </value>
        public OcrRequestId RequestId { get; set; }

        /// <summary>
        /// Gets or sets the broker ID of the request.
        /// </summary>
        /// <value>
        /// The ID of the request.
        /// </value>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets the status of the request.
        /// </summary>
        /// <value>
        /// The status of the association.
        /// </value>
        public OcrRequestStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the date of the request status.
        /// </summary>
        /// <value>
        /// The date of the request status.
        /// </value>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets any supplemental data of the request.
        /// </summary>
        /// <value>
        /// The supplemental data of the request.
        /// </value>
        public OcrStatusSupplementalData SupplementalData { get; set; }

        /// <summary>
        /// Retrieves the request by request id.
        /// </summary>
        /// <param name="requestId">
        /// The ID of the request.
        /// </param>
        /// <param name="brokerId">
        /// The ID of the broker.
        /// </param>
        /// <returns>
        /// The request.
        /// </returns>
        public static OcrStatus Retrieve(OcrRequestId requestId, Guid brokerId)
        {
            var procedureName = StoredProcedureName.Create("EDOCS_OCR_STATUS_Retrieve").Value;
            SqlParameter[] parameters =
            {
                new SqlParameter("@OcrRequestId", requestId.Value),
                new SqlParameter("@BrokerId", brokerId),
            };

            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    var status = new OcrStatus(requestId, brokerId);
                    status.Load(reader);
                    return status;
                }
            }

            return null;
        }

        /// <summary>
        /// Saves the status.
        /// </summary>
        public void Save()
        {
            using (var connection = DbConnectionInfo.GetConnection(this.BrokerId))
            {
                connection.Open();

                using (var transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead))
                {
                    try
                    {
                        var procedureName = StoredProcedureName.Create("EDOCS_OCR_STATUS_Save").Value;
                        var parameters = new List<SqlParameter>(5)
                        {
                            new SqlParameter("@OcrRequestId", this.RequestId.Value),
                            new SqlParameter("@BrokerId", this.BrokerId),
                            new SqlParameter("@Status", this.Status),
                            new SqlParameter("@Date", this.Date)
                        };

                        if (this.SupplementalData != null)
                        {
                            var supplementalDataJson = SerializationHelper.JsonNetSerialize(this.SupplementalData);
                            parameters.Add(new SqlParameter("@SupplementalData", supplementalDataJson));
                        }

                        StoredProcedureDriverHelper.ExecuteNonQuery(connection, transaction, procedureName, parameters, TimeoutInSeconds.Thirty);

                        procedureName = StoredProcedureName.Create("EDOCS_OCR_REQUEST_UpdateLatestStatus").Value;
                        parameters = new List<SqlParameter>(4)
                        {
                            new SqlParameter("@OcrRequestId", this.RequestId.Value),
                            new SqlParameter("@BrokerId", this.BrokerId),
                            new SqlParameter("@Status", this.Status),
                            new SqlParameter("@Date", this.Date)
                        };

                        StoredProcedureDriverHelper.ExecuteNonQuery(connection, transaction, procedureName, parameters, TimeoutInSeconds.Thirty);

                        transaction.Commit();
                    }
                    catch
                    {
                        transaction?.Rollback();
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Loads data for the status.
        /// </summary>
        /// <param name="reader">
        /// The reader containing data for the status.
        /// </param>
        private void Load(IDataReader reader)
        {
            this.Status = (OcrRequestStatus)reader.SafeInt("Status");
            this.Date = reader.SafeDateTime("Date");
            
            var supplementalDataJson = reader.AsNullableString("SupplementalData");
            this.SupplementalData = SerializationHelper.JsonNetDeserialize<OcrStatusSupplementalData>(supplementalDataJson);
        }
    }
}
