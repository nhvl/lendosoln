﻿namespace LendersOffice.Integration.DocumentCapture
{
    /// <summary>
    /// Provides a simple container for KTA submission results.
    /// </summary>
    public class KtaSubmissionResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether the submission was successful.
        /// </summary>
        /// <value>
        /// True if the submission was successful, false otherwise.
        /// </value>
        public bool Success { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the result failed due to missing
        /// service credentials.
        /// </summary>
        /// <value>
        /// True if the result failed due to missing credentials, false otherwise.
        /// </value>
        public bool MissingCredentials { get; set; }

        /// <summary>
        /// Gets or sets the error message for the submission, if any.
        /// </summary>
        /// <value>
        /// The error message or null if no error occurred.
        /// </value>
        public string ErrorMessage { get; set; }
    }
}
