﻿namespace LendersOffice.Integration.DocumentCapture
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using Common;
    using Constants;
    using DataAccess;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides a means for transmitting to KTA.
    /// </summary>
    public class KtaServer
    {
        /// <summary>
        /// Submits a request to KTA.
        /// </summary>
        /// <param name="requestId">
        /// The ID of the request.
        /// </param>
        /// <param name="authenticationRequest">
        /// The authentication request.
        /// </param>
        /// <param name="pdfContent">
        /// The PDF content.
        /// </param>
        /// <returns>
        /// The result of the submission.
        /// </returns>
        internal KtaSubmissionResult Submit(OcrRequestId requestId, KtaAuthenticationRequest authenticationRequest, byte[] pdfContent)
        {
            var result = new KtaSubmissionResult();
            try
            {
                var generator = new KtaAuthenticationHandler(authenticationRequest);
                generator.Generate();

                this.SendRequest(requestId, generator, pdfContent);
                result.Success = true;
            }
            catch (CBaseException exc)
            {
                Tools.LogError($"KTA submission failure: Request {requestId}, customer code {authenticationRequest.CustomerCode}, login {authenticationRequest.UserLogin}", exc);
                result.Success = false;
                result.ErrorMessage = exc.UserMessage;
            }

            return result;
        }

        /// <summary>
        /// Sends the request to KTA.
        /// </summary>
        /// <param name="requestId">
        /// The ID of the request.
        /// </param>
        /// <param name="generator">
        /// The generator for KTA authentication.
        /// </param>
        /// <param name="pdfContent">
        /// The PDF content.
        /// </param>
        private void SendRequest(OcrRequestId requestId, KtaAuthenticationHandler generator, byte[] pdfContent)
        {
            try
            {
                var boundary = Guid.NewGuid().ToString("N");

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConstStage.KtaApiUrl);
                request.Method = "POST";
                request.ContentType = $"multipart/form-data; boundary={boundary}";

                var cookieContainer = this.GenerateCookieContainer(generator.EncryptedCookieData);
                request.CookieContainer = cookieContainer;

                this.PopulateMultipartRequestBody(generator.EncryptedXmlPayload, pdfContent, boundary, request);

                var response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception exc) when (!(exc is CBaseException))
            {
                throw new CBaseException(ErrorMessages.Generic, exc);
            }
        }

        /// <summary>
        /// Generates a cookie container holding the given cookie data.
        /// </summary>
        /// <param name="ktaCookie">
        /// The encrypted KTA cookie string.
        /// </param>
        /// <returns>
        /// A cookie container.
        /// </returns>
        private CookieContainer GenerateCookieContainer(string ktaCookie)
        {
            var container = new CookieContainer();
            container.Add(new Uri(ConstStage.KtaApiUrl), new Cookie("KtaAuth", ktaCookie));
            return container;
        }

        /// <summary>
        /// Generates MIME multi-part data containing the given KTA payload and file content.
        /// </summary>
        /// <param name="ktaPayload">
        /// The encrypted KTA payload.
        /// </param>
        /// <param name="pdfContent">
        /// The PDF byte content to upload.
        /// </param>
        /// <param name="boundary">
        /// The MIME multipart boundary.
        /// </param>
        /// <param name="request">
        /// The request to be populated with the message body.
        /// </param>
        private void PopulateMultipartRequestBody(string ktaPayload, byte[] pdfContent, string boundary, HttpWebRequest request)
        {
            var requestBeginning = new StringBuilder();
            requestBeginning.Append($"--{boundary}\r\n");
            requestBeginning.Append($"Content-Disposition: form-data; name=\"KtaPayload\"\r\n\r\n");
            requestBeginning.Append($"{ktaPayload}\r\n");
            requestBeginning.Append($"--{boundary}\r\n");
            requestBeginning.Append($"Content-Disposition: form-data; name=\"FileToUpload\"; filename=\"{Guid.NewGuid().ToString("N")}.pdf\"\r\n\r\n");

            string endBoundary = $"\r\n--{boundary}--\r\n";

            // Document packages can be in the tens of megabytes, so we want to avoid any serialization on the PDF
            // contents for performance reasons. Because of this, we will write the request parts directly to a stream.
            using (Stream requestStream = request.GetRequestStream())
            {
                var encoding = Encoding.UTF8;

                var requestBeginningBytes = encoding.GetBytes(requestBeginning.ToString());
                requestStream.Write(requestBeginningBytes, offset: 0, count: requestBeginningBytes.Length);

                requestStream.Write(pdfContent, offset: 0, count: pdfContent.Length);

                var endBoundaryBytes = encoding.GetBytes(endBoundary);
                requestStream.Write(endBoundaryBytes, offset: 0, count: endBoundaryBytes.Length);

                request.ContentLength = requestStream.Length;
            }
        }
    }
}
