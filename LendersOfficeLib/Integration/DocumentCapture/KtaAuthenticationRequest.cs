﻿namespace LendersOffice.Integration.DocumentCapture
{
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides a simple container for KTA authentication requests.
    /// </summary>
    public class KtaAuthenticationRequest
    {
        /// <summary>
        /// Gets or sets the ID for the request.
        /// </summary>
        /// <value>
        /// The ID for the request.
        /// </value>
        public OcrRequestId RequestId { get; set; }

        /// <summary>
        /// Gets or sets the KTA username.
        /// </summary>
        /// <value>
        /// The KTA username.
        /// </value>
        public string KtaUsername { get; set; }

        /// <summary>
        /// Gets or sets the KTA password.
        /// </summary>
        /// <value>
        /// The KTA password.
        /// </value>
        public Sensitive<string> KtaPassword { get; set; }

        /// <summary>
        /// Gets or sets the customer code for the user's lender.
        /// </summary>
        /// <value>
        /// The customer code for the user's lender.
        /// </value>
        public string CustomerCode { get; set; }

        /// <summary>
        /// Gets or sets the loan name.
        /// </summary>
        /// <value>
        /// The loan name.
        /// </value>
        public string LoanName { get; set; }

        /// <summary>
        /// Gets or sets the LQB login name for the user.
        /// </summary>
        /// <value>
        /// The LQB login name for the user.
        /// </value>
        public string UserLogin { get; set; }

        /// <summary>
        /// Validates the authentication request.
        /// </summary>
        /// <returns>
        /// True if the request is valid, false otherwise.
        /// </returns>
        public bool Validate()
        {
            return this.RequestId != OcrRequestId.BadIdentifier &&
                !string.IsNullOrEmpty(this.KtaUsername) &&
                !default(Sensitive<string>).Equals(this.KtaPassword) &&
                !string.IsNullOrEmpty(this.KtaPassword.Value) &&
                !string.IsNullOrEmpty(this.CustomerCode) &&
                !string.IsNullOrEmpty(this.UserLogin) &&
                !string.IsNullOrEmpty(this.LoanName);
        }
    }
}
