﻿namespace LendersOffice.Integration.DocumentCapture
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;

    /// <summary>
    /// Manages the branch groups that have OCR enabled.
    /// </summary>
    /// <remarks>This is configured in the General Settings document generation options.</remarks>
    public static class OcrEnabledBranchGroupManager
    {
        /// <summary>
        /// The sproc name for retrieving all enabled group IDs associated with a given lender.
        /// </summary>
        private const string RetrieveByBrokerSpName = "EDOCS_OCR_ENABLED_BRANCH_GROUPS_RetrieveByBroker";

        /// <summary>
        /// The sproc name for enabling a branch group.
        /// </summary>
        private const string EnableSpName = "EDOCS_OCR_ENABLED_BRANCH_GROUPS_Create";

        /// <summary>
        /// The sproc name for disabling a branch group.
        /// </summary>
        private const string DisableSpName = "EDOCS_OCR_ENABLED_BRANCH_GROUPS_Delete";

        /// <summary>
        /// The sproc name for disabling all branch groups associated with a given lender.
        /// </summary>
        private const string DisableAllSpName = "EDOCS_OCR_ENABLED_BRANCH_GROUPS_DeleteByBroker";

        /// <summary>
        /// Indicates whether the branch associated with a loan is enabled for OCR.
        /// </summary>
        /// <param name="broker">A broker object.</param>
        /// <param name="loanId">A loan ID.</param>
        /// <returns>A boolean indicating whether the branch associated with a loan is enabled for OCR.</returns>
        public static bool IsLoanBranchGroupEnabledForOcr(BrokerDB broker, Guid loanId)
        {
            if (broker.DocumentFrameworkCaptureEnabledBranchGroupT == BranchGroupSelection.All)
            {
                return true;
            }

            var brokerId = broker.BrokerID;
            var branchId = Tools.GetLoanBranchIdByLoanId(brokerId, loanId);
            var branchGroups = GroupDB.ListInclusiveGroupForBranch(brokerId, branchId);
            var ocrEnabledGroups = RetrieveEnabledBranchGroups(brokerId);

            return branchGroups.Any(g => ocrEnabledGroups.Contains(g.Id));
        }

        /// <summary>
        /// Retrieves the branch groups under the given broker that have OCR enabled.
        /// </summary>
        /// <param name="brokerId">A broker ID.</param>
        /// <returns>A collection of branch group IDs that have OCR enabled.</returns>
        public static HashSet<Guid> RetrieveEnabledBranchGroups(Guid brokerId)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            var enabledBranchGroups = new HashSet<Guid>();
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, RetrieveByBrokerSpName, parameters))
            {
                while (reader.Read())
                {
                    enabledBranchGroups.Add((Guid)reader["GroupId"]);
                }
            }

            return enabledBranchGroups;
        }

        /// <summary>
        /// Sets the OCR enabled branch groups for a given broker.
        /// </summary>
        /// <param name="brokerId">A broker ID.</param>
        /// <param name="newBranchGroups">The set of branch groups to be enabled.</param>
        public static void SetEnabledBranchGroups(Guid brokerId, List<Guid> newBranchGroups)
        {
            using (CStoredProcedureExec exec = new CStoredProcedureExec(brokerId))
            {
                try
                {
                    exec.BeginTransactionForWrite();

                    DisableBranchGroupsByBroker(brokerId, exec);

                    foreach (var groupId in newBranchGroups)
                    {
                        EnableBranchGroup(brokerId, groupId, exec);
                    }

                    exec.CommitTransaction();
                }
                catch (SqlException)
                {
                    exec.RollbackTransaction();
                    throw;
                }
            }
        }

        /// <summary>
        /// Enables OCR for a specified branch group.
        /// </summary>
        /// <param name="brokerId">A broker ID.</param>
        /// <param name="groupId">A branch group ID.</param>
        /// <param name="exec">A SQL transaction.</param>
        private static void EnableBranchGroup(Guid brokerId, Guid groupId, CStoredProcedureExec exec)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@GroupId", groupId)
            };

            exec.ExecuteNonQuery(EnableSpName, 3, parameters);
        }

        /// <summary>
        /// Disables OCR for all branch groups associated with a given lender.
        /// </summary>
        /// <param name="brokerId">A broker ID.</param>
        /// <param name="exec">A SQL transaction.</param>
        private static void DisableBranchGroupsByBroker(Guid brokerId, CStoredProcedureExec exec)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            exec.ExecuteNonQuery(DisableAllSpName, 3, parameters);
        }
    }
}
