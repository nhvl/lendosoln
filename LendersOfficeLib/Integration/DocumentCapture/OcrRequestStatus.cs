﻿namespace LendersOffice.Integration.DocumentCapture
{
    /// <summary>
    /// The status for an OCR request.
    /// </summary>
    public enum OcrRequestStatus
    {
        /// <summary>
        /// The request has been uploaded.
        /// </summary>
        Uploaded = 0,

        /// <summary>
        /// The request has been received.
        /// </summary>
        Received = 1,

        /// <summary>
        /// The request has been queued.
        /// </summary>
        Queued = 2,

        /// <summary>
        /// The request is being scanned.
        /// </summary>
        Scanning = 3,

        /// <summary>
        /// The request has been completed.
        /// </summary>
        ScanComplete = 4,

        /// <summary>
        /// The request has been sent to LQB.
        /// </summary>
        SentToLqb = 5,

        /// <summary>
        /// The request requires review.
        /// </summary>
        NeedsReview = 6
    }
}
