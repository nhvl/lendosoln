namespace LendersOffice.Integration.DocumentCapture
{
    using System;
    using System.Net;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Description;
    using System.ServiceModel.Dispatcher;

    /// <summary>
    /// An endpoint hehavior implementation that allows an HTTP cookie
    /// to be sent and read through a WCF service.
    /// </summary>
    /// <remarks>This class was provided by KMBS for the KTA integration.</remarks>
    public class CookieBehavior : IEndpointBehavior
    {
        /// <summary>
        /// A cookie container.
        /// </summary>
        private CookieContainer cookieContainer;

        /// <summary>
        /// Initializes a new instance of the <see cref="CookieBehavior"/> class.
        /// </summary>
        /// <param name="cookieContainer">The cookie container.</param>
        public CookieBehavior(CookieContainer cookieContainer)
        {
            this.cookieContainer = cookieContainer;
        }

        /// <summary>
        /// Adds a binding parameter.
        /// </summary>
        /// <param name="serviceEndpoint">A service endpoint.</param>
        /// <param name="bindingParameters">The binding parameters.</param>
        public void AddBindingParameters(ServiceEndpoint serviceEndpoint, BindingParameterCollection bindingParameters)
        {
        }

        /// <summary>
        /// Applies a client behavior.
        /// </summary>
        /// <param name="serviceEndpoint">A service endpoint.</param>
        /// <param name="behavior">The behavior to apply.</param>
        public void ApplyClientBehavior(ServiceEndpoint serviceEndpoint, ClientRuntime behavior)
        {
            behavior.MessageInspectors.Add(new CookieMessageInspector(this.cookieContainer));
        }

        /// <summary>
        /// Applies a dispatch behavior.
        /// </summary>
        /// <param name="serviceEndpoint">A service endpoint.</param>
        /// <param name="endpointDispatcher">The endpoint dispatcher.</param>
        public void ApplyDispatchBehavior(ServiceEndpoint serviceEndpoint, EndpointDispatcher endpointDispatcher)
        {
        }

        /// <summary>
        /// Validates a service endpoint.
        /// </summary>
        /// <param name="serviceEndpoint">A service endpoint.</param>
        public void Validate(ServiceEndpoint serviceEndpoint)
        {
        }
    }
}
