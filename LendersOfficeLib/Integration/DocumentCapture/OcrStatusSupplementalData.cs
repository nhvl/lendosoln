﻿namespace LendersOffice.Integration.DocumentCapture
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// Provides a container for the supplemental data in
    /// an OCR status update.
    /// </summary>
    [DataContract]
    public class OcrStatusSupplementalData
    {
        /// <summary>
        /// Gets or sets the message associated with the status update.
        /// </summary>
        /// <value>
        /// The message associated with the status update.
        /// </value>
        [DataMember(Name = "m")]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the page count for a document by the document's
        /// index in the request.
        /// </summary>
        /// <value>
        /// The page count by document index.
        /// </value>
        /// <remarks>
        /// For split documents, the document index will be the order
        /// in which the documents were split from the original document.
        /// For non-split documents, this dictionary will contain the total
        /// number of pages from the original document.
        /// </remarks>
        [DataMember(Name = "pc")]
        public Dictionary<int, int> PageCountByDocumentIndex { get; set; }
    }
}
