﻿namespace LendersOffice.Integration.DocumentCapture
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using DataAccess;
    using EDocs;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.ObjLib.ServiceCredential;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides a set of utilities for KTA.
    /// </summary>
    public static class KtaUtilities
    {
        /// <summary>
        /// The name of the hidden field containing the XML payload expected by KTA.
        /// </summary>
        public const string XmlPayloadFieldName = "KtaPayload";

        /// <summary>
        /// The name of the cookie expected by KTA.
        /// </summary>
        private const string AuthCookieName = "KtaAuth";

        /// <summary>
        /// Loads post data for a service page.
        /// </summary>
        /// <param name="servicePage">
        /// The service page requesting post data.
        /// </param>
        /// <param name="principal">
        /// The user making the request.
        /// </param>
        /// <param name="origin">
        /// The origin of the request.
        /// </param>
        public static void GenerateKtaPostbackData(BaseSimpleServiceXmlPage servicePage, AbstractUserPrincipal principal, E_EDocOrigin origin)
        {
            var loanId = servicePage.GetGuid("LoanId");

            var workflowCheck = Tools.IsWorkflowOperationAuthorized(principal, loanId, WorkflowOperations.UseDocumentCapture);
            if (!workflowCheck.Item1)
            {
                throw new CBaseException(workflowCheck.Item2, $"User {principal.UserId} attempted to circumvent Document Capture workflow checks in the UI. Reason for failure: {workflowCheck.Item2}");
            }

            var requestTime = DateTime.Now;

            string username, password;
            
            var serviceCredential = GetServiceCredential(principal);
            if (serviceCredential != null)
            {
                username = serviceCredential.UserName;
                password = serviceCredential.UserPassword.Value;
            }
            else
            {
                username = servicePage.GetString("Username");
                password = servicePage.GetString("Password");
            }

            var requestCount = servicePage.GetInt("RequestCount");

            var requestIds = new List<OcrRequestId>(requestCount);
            var cookies = new List<string>(requestCount);
            var ktaPayloads = new List<string>(requestCount);

            var loanName = Tools.GetLoanNameByLoanId(principal.BrokerId, loanId);

            for (int i = 0; i < requestCount; ++i)
            {
                var ocrRequestId = OcrRequestId.Generate();
                var authRequest = new KtaAuthenticationRequest()
                {
                    RequestId = ocrRequestId,
                    UserLogin = principal.LoginNm,
                    KtaUsername = username,
                    KtaPassword = password,
                    CustomerCode = principal.BrokerDB.CustomerCode,
                    LoanName = loanName
                };

                var authHandler = new KtaAuthenticationHandler(authRequest);
                authHandler.Generate();

                var ocrRequest = OcrRequest.Create(ocrRequestId, principal.BrokerId, principal.UserId);
                ocrRequest.AppId = servicePage.GetGuid("AppId");
                ocrRequest.LoanId = loanId;
                ocrRequest.Origin = origin;
                ocrRequest.UploadingUserType = principal.Type;
                ocrRequest.DocumentSource = E_EDocumentSource.UserUpload;

                ocrRequest.Save();

                requestIds.Add(ocrRequestId);
                cookies.Add(authHandler.EncryptedCookieData);
                ktaPayloads.Add(authHandler.EncryptedXmlPayload);
            }

            servicePage.SetResult("RequestIds", SerializationHelper.JsonNetAnonymousSerialize(requestIds.Select(id => id.Value)));
            servicePage.SetResult("Cookies", SerializationHelper.JsonNetSerialize(cookies));
            servicePage.SetResult("KtaPayloads", SerializationHelper.JsonNetSerialize(ktaPayloads));
        }

        /// <summary>
        /// Sets the upload cookie for the service page.
        /// </summary>
        /// <param name="servicePage">
        /// The service page requesting the upload cookie.
        /// </param>
        public static void SetUploadCookie(BaseSimpleServiceXmlPage servicePage)
        {
            var cookieData = servicePage.GetString("CookieData");
            RequestHelper.StoreToCookie(AuthCookieName, cookieData, expiration: DateTime.Now.AddMinutes(5), setDomain: true);
        }

        /// <summary>
        /// Polls an OCR request for status.
        /// </summary>
        /// <param name="servicePage">
        /// The page requesting the polling.
        /// </param>
        /// <param name="principal">
        /// The user requesting the polling.
        /// </param>
        public static void PollForOcrStatus(BaseSimpleServiceXmlPage servicePage, AbstractUserPrincipal principal)
        {
            var ready = true;
            
            var requestId = servicePage.GetString("RequestId");
            var ocrRequestId = OcrRequestId.Create(requestId);
            if (!ocrRequestId.HasValue)
            {
                throw new CBaseException(ErrorMessages.Generic, "Invalid OCR request ID" + requestId);
            }

            var ocrStatus = OcrStatus.Retrieve(ocrRequestId.Value, principal.BrokerId);
            ready &= ocrStatus != null && (int)ocrStatus.Status >= (int)OcrRequestStatus.Received;

            servicePage.SetResult("Ready", ready);
        }

        /// <summary>
        /// Creates supplemental data from the specified XML content.
        /// </summary>
        /// <param name="supplementalDataXml">
        /// The XML content containing supplemental data.
        /// </param>
        /// <returns>
        /// The supplemental data.
        /// </returns>
        public static OcrStatusSupplementalData CreateSupplementaryData(string supplementalDataXml)
        {
            var supplementalData = new OcrStatusSupplementalData()
            {
                PageCountByDocumentIndex = new Dictionary<int, int>()
            };

            var xmlDocument = Tools.CreateXmlDoc(supplementalDataXml);

            foreach (XmlNode node in xmlDocument.SelectNodes("//documents/document"))
            {
                var documentIndex = int.Parse(node.Attributes["index"].Value);
                var pageCount = int.Parse(node.Attributes["page_count"].Value);

                supplementalData.PageCountByDocumentIndex.Add(documentIndex, pageCount);
            }

            var messageNode = xmlDocument.SelectSingleNode("//message");
            supplementalData.Message = messageNode?.InnerText;

            return supplementalData;
        }

        /// <summary>
        /// Processes the specified metadata to create LQB e-docs metadata.
        /// </summary>
        /// <param name="brokerId">
        /// The broker for the documents.
        /// </param>
        /// <param name="requestId">
        /// The ID of the OCR request.
        /// </param>
        /// <param name="xmlMetadata">
        /// The xml metadata.
        /// </param>
        public static void ProcessDocumentCaptureMetadata(Guid brokerId, OcrRequestId requestId, string xmlMetadata)
        {
            var ocrRequest = OcrRequest.Retrieve(requestId, brokerId);
            var repository = EDocumentRepository.GetSystemRepositoryForUser(brokerId, ocrRequest.UploadingUserId);
            var xmlDocument = Tools.CreateXmlDoc(xmlMetadata);
            var docTypeDictionary = EDocumentDocType.GetDocTypesByClassificationName(brokerId, E_EnforceFolderPermissions.False);

            foreach (XmlNode processedDocument in xmlDocument.SelectNodes("//documents/document"))
            {
                var document = repository.CreateDocument(ocrRequest.DocumentSource);
                document.ProcessDocumentCaptureMetadata(ocrRequest, processedDocument, docTypeDictionary);
                repository.Save(document);

                EDocument.UpdateImageStatus(brokerId, document.DocumentId, E_ImageStatus.HasCachedImages);
            }
        }

        /// <summary>
        /// Uploads a document to KTA.
        /// </summary>
        /// <param name="parameters">
        /// The user parameters for the upload.
        /// </param>
        /// <param name="captureUsername">
        /// An optional parameter specifying the capture username.
        /// </param>
        /// <param name="capturePassword">
        /// An optional parameter specifying the capture password.
        /// </param>
        /// <returns>
        /// The result of the upload.
        /// </returns>
        public static KtaSubmissionResult UploadDocument(KtaDocumentUploadParameters parameters, string captureUsername = null, string capturePassword = null)
        {
            var username = captureUsername?.Trim();
            var password = capturePassword?.Trim();

            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                var credential = GetServiceCredential(parameters);
                if (credential == null)
                {
                    return new KtaSubmissionResult()
                    {
                        Success = false,
                        MissingCredentials = true,
                        ErrorMessage = ErrorMessages.GenericAccessDenied
                    };
                }
                else
                {
                    username = credential.UserName;
                    password = credential.UserPassword.Value;
                }
            }
            
            var requestId = OcrRequestId.Generate();
            var authenticationRequest = new KtaAuthenticationRequest()
            {
                RequestId = requestId,
                KtaUsername = username,
                KtaPassword = password,
                UserLogin = parameters.UserLoginName,
                CustomerCode = parameters.CustomerCode,
                LoanName = Tools.GetLoanNameByLoanId(parameters.BrokerId, parameters.LoanId)
            };

            var pdfContent = parameters.PdfBytes ?? File.ReadAllBytes(parameters.PdfPath);

            Tools.ScanForVirus(pdfContent);

            var server = new KtaServer();
            var submissionResult = server.Submit(requestId, authenticationRequest, pdfContent);

            if (submissionResult.Success)
            {
                var ocrRequest = OcrRequest.Create(requestId, parameters.BrokerId, parameters.UserId);
                ocrRequest.AppId = parameters.ApplicationId;
                ocrRequest.LoanId = parameters.LoanId;
                ocrRequest.Origin = E_EDocOrigin.LO;
                ocrRequest.UploadingUserType = parameters.UserType;
                ocrRequest.DocumentSource = parameters.DocumentSource;

                ocrRequest.Save();
            }

            return submissionResult;
        }

        /// <summary>
        /// Gets the service credential for the user if one exists.
        /// </summary>
        /// <param name="currentUser">
        /// The user to load service credentials.
        /// </param>
        /// <returns>
        /// The service credential or null if none exists.
        /// </returns>
        public static ServiceCredential GetServiceCredential(AbstractUserPrincipal currentUser)
        {
            return ServiceCredential.ListAvailableServiceCredentials(
                currentUser,
                currentUser.BranchId,
                ServiceCredentialService.DocumentCapture).FirstOrDefault();
        }

        /// <summary>
        /// Gets the service credential for the document upload if one exists.
        /// </summary>
        /// <param name="parameters">
        /// The parameters to load service credentials.
        /// </param>
        /// <returns>
        /// The service credential or null if none exists.
        /// </returns>
        private static ServiceCredential GetServiceCredential(KtaDocumentUploadParameters parameters)
        {
            return ServiceCredential.ListAvailableServiceCredentials(
                parameters.BranchId,
                parameters.EmployeeId,
                parameters.PmlBrokerId,
                parameters.BrokerId,
                parameters.UserType == "P" ? UserType.TPO : UserType.LQB,
                ServiceCredentialService.DocumentCapture).FirstOrDefault();
        }
    }
}
