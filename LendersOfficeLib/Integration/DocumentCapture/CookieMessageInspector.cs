﻿namespace LendersOffice.Integration.DocumentCapture
{
    using System;
    using System.Net;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Dispatcher;

    /// <summary>
    /// A behavior that can be added to a cookie, allowing it to be sent or read
    /// through a WCF service.
    /// </summary>
    /// <remarks>This class was provided by KMBS for the KTA integration.</remarks>
    public class CookieMessageInspector : IClientMessageInspector
    {
        /// <summary>
        /// A cookie container.
        /// </summary>
        private CookieContainer cookieContainer;

        /// <summary>
        /// Initializes a new instance of the <see cref="CookieMessageInspector"/> class.
        /// </summary>
        /// <param name="cookieContainer">A cookie container.</param>
        public CookieMessageInspector(CookieContainer cookieContainer)
        {
            this.cookieContainer = cookieContainer;
        }

        /// <summary>
        /// Sets the cookie container based on the correlation state.
        /// </summary>
        /// <param name="reply">The reply.</param>
        /// <param name="correlationState">The correlation state.</param>
        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            object obj;
            if (reply.Properties.TryGetValue(HttpResponseMessageProperty.Name, out obj))
            {
                HttpResponseMessageProperty httpResponseMsg = obj as HttpResponseMessageProperty;
                if (!string.IsNullOrEmpty(httpResponseMsg.Headers["Set-Cookie"]))
                {
                    this.cookieContainer.SetCookies((Uri)correlationState, httpResponseMsg.Headers["Set-Cookie"]);
                }
            }
        }

        /// <summary>
        /// Sets the request Cookie header prior to transmission.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="channel">The client channel.</param>
        /// <returns>Returns the URL for the remote address.</returns>
        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            object obj;
            if (request.Properties.TryGetValue(HttpRequestMessageProperty.Name, out obj))
            {
                HttpRequestMessageProperty httpRequestMsg = obj as HttpRequestMessageProperty;
                this.SetRequestCookies(channel, httpRequestMsg);
            }
            else
            {
                var httpRequestMsg = new HttpRequestMessageProperty();
                this.SetRequestCookies(channel, httpRequestMsg);
                request.Properties.Add(HttpRequestMessageProperty.Name, httpRequestMsg);
            }

            return channel.RemoteAddress.Uri;
        }

        /// <summary>
        /// Sets the cookie header in an HTTP request based on the cookie header in the container.
        /// </summary>
        /// <param name="channel">The client channel.</param>
        /// <param name="httpRequestMessage">The request message.</param>
        private void SetRequestCookies(IClientChannel channel, HttpRequestMessageProperty httpRequestMessage)
        {
            httpRequestMessage.Headers["Cookie"] = this.cookieContainer.GetCookieHeader(channel.RemoteAddress.Uri);
        }
    }
}
