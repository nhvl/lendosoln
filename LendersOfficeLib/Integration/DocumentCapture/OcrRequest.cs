﻿namespace LendersOffice.Integration.DocumentCapture
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using DataAccess;
    using Drivers.SqlServerDB;
    using EDocs;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Represents a request to an OCR vendor.
    /// </summary>
    public class OcrRequest
    {
        /// <summary>
        /// Indicates whether the OCR request is new.
        /// </summary>
        private bool isNew = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="OcrRequest"/> class.
        /// </summary>
        /// <param name="requestId">
        /// The ID of the request.
        /// </param>
        /// <param name="brokerId">
        /// The ID of the broker.
        /// </param>
        /// <param name="userId">
        /// The ID of the user creating the request.
        /// </param>
        private OcrRequest(OcrRequestId requestId, Guid brokerId, Guid? userId)
            : this(requestId, brokerId)
        {
            this.UploadingUserId = userId;
            this.isNew = true;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OcrRequest"/> class.
        /// </summary>
        /// <param name="requestId">
        /// The ID of the request.
        /// </param>
        /// <param name="brokerId">
        /// The ID of the broker.
        /// </param>
        private OcrRequest(OcrRequestId requestId, Guid brokerId)
        {
            this.RequestId = requestId;
            this.BrokerId = brokerId;
        }

        /// <summary>
        /// Gets or sets the request ID of the request.
        /// </summary>
        /// <value>
        /// The request ID of the request.
        /// </value>
        public OcrRequestId RequestId { get; set; }

        /// <summary>
        /// Gets or sets the broker ID of the request.
        /// </summary>
        /// <value>
        /// The ID of the request.
        /// </value>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets the loan ID of the request.
        /// </summary>
        /// <value>
        /// The loan ID of the request.
        /// </value>
        public Guid LoanId { get; set; }

        /// <summary>
        /// Gets the loan number of the request.
        /// </summary>
        /// <value>
        /// The loan number of the request.
        /// </value>
        public string LoanNumber { get; private set; }

        /// <summary>
        /// Gets or sets the app ID of the request.
        /// </summary>
        /// <value>
        /// The app ID of the request.
        /// </value>
        public Guid AppId { get; set; }

        /// <summary>
        /// Gets or sets the origin of the request.
        /// </summary>
        /// <value>
        /// The origin of the request.
        /// </value>
        public E_EDocOrigin Origin { get; set; }

        /// <summary>
        /// Gets or sets the source of the document upload.
        /// </summary>
        /// <value>
        /// The source of the document upload.
        /// </value>
        public E_EDocumentSource DocumentSource { get; set; }

        /// <summary>
        /// Gets or sets the ID of the user that created the request.
        /// </summary>
        /// <value>
        /// The ID of the user that created the request.
        /// </value>
        public Guid? UploadingUserId { get; set; }

        /// <summary>
        /// Gets or sets the type of the user that created the request.
        /// </summary>
        /// <value>
        /// The type of user that created the request.
        /// </value>
        public string UploadingUserType { get; set; }

        /// <summary>
        /// Gets the latest status of the request.
        /// </summary>
        /// <value>
        /// The latest status of the request.
        /// </value>
        public OcrRequestStatus? LatestStatus { get; private set; }

        /// <summary>
        /// Gets the latest status date of the request.
        /// </summary>
        /// <value>
        /// The latest status date of the request.
        /// </value>
        public DateTime? LatestStatusDate { get; private set; }

        /// <summary>
        /// Retrieves the ID of the broker associated with an OCR request.
        /// </summary>
        /// <param name="requestId">
        /// The ID of the OCR request.
        /// </param>
        /// <returns>
        /// The ID of the broker or null if no request exists.
        /// </returns>
        public static Guid? GetBrokerIdFromRequestId(OcrRequestId requestId)
        {
            var procedureName = StoredProcedureName.Create("EDOCS_OCR_REQUEST_GetBrokerIdFromRequestId").Value;
            foreach (var connectionInfo in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@OcrRequestId", requestId.Value)
                };

                using (var connection = connectionInfo.GetConnection())
                using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty))
                {
                    if (reader.Read())
                    {
                        return (Guid)reader["BrokerId"];
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Creates a new OCR request.
        /// </summary>
        /// <param name="requestId">The request ID.</param>
        /// <param name="brokerId">The broker ID associated with the request.</param>
        /// <param name="userId">The user ID associated with the request.</param>
        /// <returns>A new OCR request.</returns>
        public static OcrRequest Create(OcrRequestId requestId, Guid brokerId, Guid? userId)
        {
            return new OcrRequest(requestId, brokerId, userId);
        }

        /// <summary>
        /// Retrieves the request by request id.
        /// </summary>
        /// <param name="requestId">
        /// The ID of the request.
        /// </param>
        /// <param name="brokerId">
        /// The ID of the broker.
        /// </param>
        /// <returns>
        /// The request.
        /// </returns>
        public static OcrRequest Retrieve(OcrRequestId requestId, Guid brokerId)
        {
            var procedureName = StoredProcedureName.Create("EDOCS_OCR_REQUEST_Retrieve").Value;
            SqlParameter[] parameters =
            {
                new SqlParameter("@OcrRequestId", requestId.Value),
                new SqlParameter("@BrokerId", brokerId),
            };

            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    var request = new OcrRequest(requestId, brokerId);
                    request.Load(reader);
                    return request;
                }
            }

            return null;
        }

        /// <summary>
        /// Lists OCR requests by loan ID.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the loan's broker.
        /// </param>
        /// <param name="loanId">
        /// The ID of the loan.
        /// </param>
        /// <returns>
        /// The list of requests.
        /// </returns>
        public static IEnumerable<OcrRequest> ListByLoanId(Guid brokerId, Guid loanId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", loanId)
            };

            return ListRequests(brokerId, parameters);
        }

        /// <summary>
        /// Lists requests by broker ID.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker.
        /// </param>
        /// <returns>
        /// The list of requests.
        /// </returns>
        public static IEnumerable<OcrRequest> ListByBrokerId(Guid brokerId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            return ListRequests(brokerId, parameters);
        }

        /// <summary>
        /// Saves the request.
        /// </summary>
        public void Save()
        {
            var procedureName = StoredProcedureName.Create("EDOCS_OCR_REQUEST_Save").Value;
            SqlParameter[] parameters = 
            {
                new SqlParameter("@OcrRequestId", this.RequestId.Value),
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@LoanId", this.LoanId),
                new SqlParameter("@AppId", this.AppId),
                new SqlParameter("@Origin", this.Origin),
                new SqlParameter("@DocumentSource", this.DocumentSource),
                new SqlParameter("@UploadingUserId", this.UploadingUserId) { IsNullable = true },
                new SqlParameter("@UploadingUserType", this.UploadingUserType)
            };

            using (var connection = DbConnectionInfo.GetConnection(this.BrokerId))
            {
                StoredProcedureDriverHelper.ExecuteNonQuery(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty);
            }

            if (this.isNew)
            {
                var initialStatus = new OcrStatus(this.RequestId, this.BrokerId);
                initialStatus.Status = OcrRequestStatus.Uploaded;
                initialStatus.Date = DateTime.Now;
                initialStatus.Save();

                this.isNew = false;
            }
        }

        /// <summary>
        /// Lists requests using the specified parameters.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker.
        /// </param>
        /// <param name="parameters">
        /// The parameters to use in the search.
        /// </param>
        /// <returns>
        /// The list of requests.
        /// </returns>
        private static IEnumerable<OcrRequest> ListRequests(Guid brokerId, SqlParameter[] parameters)
        {
            var procedureName = StoredProcedureName.Create("EDOCS_OCR_REQUEST_List").Value;
            var requestList = new LinkedList<OcrRequest>();

            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty))
            {
                while (reader.Read())
                {
                    var requestId = OcrRequestId.Create(reader["OcrRequestId"].ToString()).Value;

                    var request = new OcrRequest(requestId, brokerId);
                    request.Load(reader);

                    requestList.AddLast(request);
                }
            }

            return requestList;
        }

        /// <summary>
        /// Loads data for the request.
        /// </summary>
        /// <param name="reader">
        /// The reader containing data for the request.
        /// </param>
        private void Load(IDataReader reader)
        {
            this.LoanId = reader.SafeGuid("LoanId");
            this.LoanNumber = reader.SafeString("sLNm");
            this.AppId = reader.SafeGuid("AppId");
            this.Origin = (E_EDocOrigin)reader.SafeInt("Origin");
            this.DocumentSource = (E_EDocumentSource)reader.SafeInt("DocumentSource");
            this.UploadingUserId = reader.AsNullableGuid("UploadingUserId");
            this.UploadingUserType = reader.SafeString("UploadingUserType");
            this.LatestStatusDate = reader.AsNullableDateTime("LatestStatusDate");

            var latestStatus = reader.AsNullableStruct<byte>("LatestStatus");
            this.LatestStatus = latestStatus.HasValue ? (OcrRequestStatus)latestStatus : default(OcrRequestStatus?);
        }
    }
}
