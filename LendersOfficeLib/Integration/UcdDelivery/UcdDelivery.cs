﻿namespace LendersOffice.Integration.UcdDelivery
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Used for tracking UCD file delivery data.
    /// </summary>
    public abstract class UcdDelivery
    {
        /// <summary>
        /// Name of stored procedure to create a new UCD delivery.
        /// </summary>
        protected static readonly StoredProcedureName CreateProcedureName = StoredProcedureName.Create("UCD_DELIVERY_create").Value;

        /// <summary>
        /// Name of stored procedure to update a UCD delivery.
        /// </summary>
        protected static readonly StoredProcedureName UpdateProcedureName = StoredProcedureName.Create("UCD_DELIVERY_update").Value;

        /// <summary>
        /// Gets or sets the entity to which the UCD file would be delivered.
        /// </summary>
        private UcdDeliveredToEntity deliveredTo;

        /// <summary>
        /// The Generic EDoc ID for the UCD file.
        /// </summary>
        private Guid? fileDeliveredDocumentId;

        /// <summary>
        /// UCD/DU case file ID.
        /// </summary>
        /// <remarks>Limited to 50 chars.</remarks>
        private string caseFileId;

        /// <summary>
        /// The date the file was delivered.
        /// </summary>
        private CDateTime dateOfDelivery;

        /// <summary>
        /// Various notes about the file delivered.
        /// </summary>
        /// <remarks>Limited to 500 chars.</remarks>
        private string note = string.Empty;

        /// <summary>
        /// A boolean indicating whether the UCD delivery should sent via ULDD.
        /// </summary>
        /// <remarks>This should only be true for one delivery per loan.</remarks>
        private bool includeInUldd;

        /// <summary>
        /// Initializes a new instance of the <see cref="UcdDelivery"/> class.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="brokerId">Broker ID.</param>
        public UcdDelivery(Guid loanId, Guid brokerId)
        {
            this.UcdDeliveryId = -1;
            this.IsDirty = true;   // New object, so it's dirty.

            this.LoanId = loanId;
            this.BrokerId = brokerId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UcdDelivery"/> class from a view model object.
        /// </summary>
        /// <param name="view">UI view model object.</param>
        public UcdDelivery(UcdDeliveryView view)
        {
            if (view.UcdDeliveryType != this.UcdDeliveryType)
            {
                throw new CBaseException(ErrorMessages.Generic, $"Attempted to create a UcdDelivery object from a UcdDeliveryView object with a mismatched type. Desired type=[{this.UcdDeliveryType}]. View type=[{view.UcdDeliveryType}].");
            }

            this.UcdDeliveryId = view.UcdDeliveryId;
            this.LoanId = view.LoanId;
            this.BrokerId = view.BrokerId;
            this.deliveredTo = view.DeliveredTo;
            this.fileDeliveredDocumentId = view.FileDeliveredDocumentId;
            this.caseFileId = view.CaseFileId;
            this.dateOfDelivery = view.DateOfDelivery;
            this.note = view.Note;
            this.includeInUldd = view.IncludeInUldd;
            this.IsDirty = true;    // If we are loading this from view, then it is most likely dirty.
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UcdDelivery"/> class.
        /// </summary>
        /// <param name="reader">Data reader.</param>
        protected UcdDelivery(IDataReader reader)
        {
            this.UcdDeliveryId = (int)reader["UcdDeliveryId"];
            this.LoanId = (Guid)reader["LoanId"];
            this.BrokerId = (Guid)reader["BrokerId"];
            this.deliveredTo = (UcdDeliveredToEntity)reader["DeliveredTo"];

            var item = reader["FileDeliveredDocumentId"];
            this.fileDeliveredDocumentId = item == DBNull.Value ? null : (Guid?)item;

            this.caseFileId = (string)reader["CaseFileId"];
            this.dateOfDelivery = Tools.GetCDateTimeFromReader(reader, "DateOfDelivery");
            this.note = (string)reader["Note"];
            this.includeInUldd = (bool)reader["IncludeInUldd"];
        }

        /// <summary>
        /// Gets the UCD Delivery ID.
        /// </summary>
        /// <value>UCD Delivery ID.</value>
        public int UcdDeliveryId { get; private set; }

        /// <summary>
        /// Gets the loan ID.
        /// </summary>
        /// <value>A loan ID.</value>
        public Guid LoanId { get; }

        /// <summary>
        /// Gets the broker ID.
        /// </summary>
        /// <value>Broker ID.</value>
        public Guid BrokerId { get; }

        /// <summary>
        /// Gets the UCD Delivery Type.
        /// </summary>
        /// <value>UCD Delivery Type.</value>
        public abstract UcdDeliveryType UcdDeliveryType { get; }

        /// <summary>
        /// Gets or sets the entity to which the UCD file would be delivered.
        /// </summary>
        /// <value>The entity to which the UCD file would be delivered.</value>
        public UcdDeliveredToEntity DeliveredTo
        {
            get
            {
                return this.deliveredTo;
            }

            set
            {
                this.deliveredTo = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets the Generic EDoc ID for the UCD file.
        /// </summary>
        /// <value>The Generic EDoc ID for the UCD file that was submitted.</value>
        public Guid? FileDeliveredDocumentId
        {
            get
            {
                return this.fileDeliveredDocumentId;
            }

            set
            {
                this.fileDeliveredDocumentId = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets the UCD/DU case file ID.
        /// </summary>
        /// <remarks>Limited to 50 chars.</remarks>
        /// <value>The UCD/DU case file ID.</value>
        public string CaseFileId
        {
            get
            {
                return this.caseFileId;
            }

            set
            {
                this.caseFileId = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets the date the file was delivered.
        /// </summary>
        /// <value>The date the file was delivered.</value>
        public CDateTime DateOfDelivery
        {
            get
            {
                return this.dateOfDelivery;
            }

            set
            {
                this.dateOfDelivery = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets various notes about the file delivered.
        /// </summary>
        /// <remarks>Limited to 500 chars.</remarks>
        /// <value>Various notes about the file delivered.</value>
        public string Note
        {
            get
            {
                return this.note;
            }

            set
            {
                this.note = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this UCD delivery record should be sent
        /// in ULDD payloads.
        /// </summary>
        /// <value>A boolean indicating whether the UCD delivery should sent via ULDD.</value>
        /// <remarks>This should only be true for one delivery per loan.</remarks>
        public bool IncludeInUldd
        {
            get
            {
                return this.includeInUldd;
            }

            set
            {
                this.includeInUldd = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this UCD delivery item is a system generated one.
        /// </summary>
        /// <value>Whether this UCD delivery item is a system generated one.</value>
        public bool IsSystemGenerated
        {
            get
            {
                return this.UcdDeliveryType != UcdDeliveryType.Manual;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this delivery object is dirty and needs to be saved.
        /// </summary>
        /// <value>Whether this delivery object is dirty and needs to be saved.</value>
        protected internal bool IsDirty
        {
            get;
            protected set;
        }

        /// <summary>
        /// Converts this object to it's view model representation for use with the UI.
        /// </summary>
        /// <returns>View model representation for use with the UI.</returns>
        public virtual UcdDeliveryView ToView()
        {
            return new UcdDeliveryView(this);
        }

        /// <summary>
        /// Save to DB.
        /// </summary>
        /// <param name="conn">Sql connection.</param>
        /// <param name="trans">Sql transaction context.</param>
        protected internal virtual void Save(DbConnection conn, DbTransaction trans)
        {
            bool isNew = this.UcdDeliveryId <= 0;
            StoredProcedureName spName = isNew ? CreateProcedureName : UpdateProcedureName;
            List<SqlParameter> parameters = this.GetBaseParameters(isNew);

            SqlParameter idOut = new SqlParameter("@UcdDeliveryId", SqlDbType.Int);
            idOut.Direction = ParameterDirection.Output;

            if (isNew)
            {
                parameters.Add(idOut);
            }
            else
            {
                parameters.Add(new SqlParameter("@UcdDeliveryId", this.UcdDeliveryId));
            }

            StoredProcedureDriverHelper.ExecuteNonQuery(conn, trans, spName, parameters, TimeoutInSeconds.Default);

            if (isNew)
            {
                this.UcdDeliveryId = (int)idOut.Value;
            }

            this.PerformAdditionalSave(conn, trans, isNew);

            this.IsDirty = false;   // This delivery has been saved, so it's no longer dirty.
        }

        /// <summary>
        /// Performs more saves in a transaction.
        /// </summary>
        /// <param name="conn">Sql connection.</param>
        /// <param name="trans">Sql transaction context.</param>
        /// <param name="isNew">Whether this is a new delivery item or not.</param>
        protected virtual void PerformAdditionalSave(DbConnection conn, DbTransaction trans, bool isNew)
        {
            return;
        }

        /// <summary>
        /// Gets a list of parameters for saving order to DB.
        /// </summary>
        /// <param name="isNew">Whether the delivery item is new.</param>
        /// <returns>List of parameter.</returns>
        protected List<SqlParameter> GetBaseParameters(bool isNew)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@LoanId", this.LoanId));
            parameters.Add(new SqlParameter("@BrokerId", this.BrokerId));
            parameters.Add(new SqlParameter("@Note", Tools.TruncateString(this.Note, 500)));
            parameters.Add(new SqlParameter("@IncludeInUldd", this.IncludeInUldd));

            if (isNew || !this.IsSystemGenerated)
            {
                // System generated items don't get to update the below fields.
                parameters.Add(new SqlParameter("@UcdDeliveryType", this.UcdDeliveryType));
                parameters.Add(new SqlParameter("@DeliveredTo", this.DeliveredTo));
                parameters.Add(new SqlParameter("@FileDeliveredDocumentId", this.FileDeliveredDocumentId));
                parameters.Add(new SqlParameter("@CaseFileId", Tools.TruncateString(this.CaseFileId, 50)));
                parameters.Add(new SqlParameter("@DateOfDelivery", this.DateOfDelivery.DateTimeForDBWriting));
            }

            return parameters;
        }
    }  
}