﻿namespace LendersOffice.Integration.UcdDelivery
{
    /// <summary>
    /// Indicates the intended action for the UCD request to complete.
    /// </summary>
    public enum UcdDeliveryAction
    {
        /// <summary>
        /// Generate a new UCD file.
        /// </summary>
        GenerateOnly = 1,

        /// <summary>
        /// Generate a new UCD file and deliver to an investor.
        /// </summary>
        GenerateAndDeliver = 2,

        /// <summary>
        /// Deliver an existing UCD file to an investor.
        /// </summary>
        DeliverExisting = 3,

        /// <summary>
        /// Get the results from a previous UCD delivery.
        /// </summary>
        GetResults = 4
    }
}
