﻿namespace LendersOffice.Integration.UcdDelivery.FannieMae
{
    using System.Collections.Generic;
    using Conversions.Templates;
    using DataAccess;
    using ObjLib.Conversions.UcdDelivery.FannieMae;

    /// <summary>
    /// Processes UCD requests to Fannie Mae.
    /// </summary>
    public class FannieMaeUcdDeliveryRequestHandler : AbstractUcdDeliveryRequestHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FannieMaeUcdDeliveryRequestHandler"/> class.
        /// </summary>
        /// <param name="requestData">The data for the request.</param>
        public FannieMaeUcdDeliveryRequestHandler(AbstractUcdDeliveryRequestData requestData)
            : base(requestData)
        {
            base.RequestProvider = new FannieMaeUcdDeliveryRequestProvider(this.RequestData);
            base.Server = new FannieMaeUcdDeliveryServer(this.RequestData);
        }

        /// <summary>
        /// Gets the Fannie Mae request data.
        /// </summary>
        /// <value> The Fannie Mae request data.</value>
        protected new FannieMaeUcdDeliveryRequestData RequestData => (FannieMaeUcdDeliveryRequestData)base.RequestData;

        /// <summary>
        /// Gets the Fannie Mae request provider.
        /// </summary>
        /// <value>The Fannie Mae request provider.</value>
        protected new FannieMaeUcdDeliveryRequestProvider RequestProvider => (FannieMaeUcdDeliveryRequestProvider)base.RequestProvider;

        /// <summary>
        /// Gets the Fannie Mae server.
        /// </summary>
        /// <value>The Fannie Mae server.</value>
        protected new FannieMaeUcdDeliveryServer Server => (FannieMaeUcdDeliveryServer)base.Server;

        /// <summary>
        /// Gets the response provider.
        /// </summary>
        /// <value>The response provider.</value>
        protected new FannieMaeUcdDeliveryResponseProvider ResponseProvider => (FannieMaeUcdDeliveryResponseProvider)base.ResponseProvider;

        /// <summary>
        /// Creates a response provider from the Fannie Mae response payload.
        /// </summary>
        /// <param name="responsePayload">The response from Fannie Mae.</param>
        /// <returns>The response provider.</returns>
        protected override AbstractResponseProvider GenerateResponseProvider(string responsePayload)
        {
            return new FannieMaeUcdDeliveryResponseProvider(responsePayload);
        }

        /// <summary>
        /// Processes the Fannie Mae UCD response.
        /// </summary>
        /// <param name="deliveryItem">A delivery item generated from the request.</param>
        /// <param name="errors">Any errors encountered during the process.</param>
        /// <returns>A result status.</returns>
        protected override UcdDeliveryResultStatus ProcessResponse(out UcdDelivery deliveryItem, out List<string> errors)
        {
            FannieMaeUcdDeliveryResponseData responseData = this.ResponseProvider.ResponseData;

            var findingsXmlFileDbKey = this.SaveFindingsXmlToFileDb(this.RequestData.DeliveryTarget.Value, responseData.DeliveryResults.FindingsXml, out errors);
            if (!findingsXmlFileDbKey.HasValue)
            {
                deliveryItem = null;
                return UcdDeliveryResultStatus.Error;
            }

            responseData.DeliveryResults.FindingsXmlFileDbKey = findingsXmlFileDbKey.Value;
            var edocId = this.SaveFindingsHtmlToEdoc(this.RequestData.DeliveryTarget.Value, responseData, out errors);
            if (!edocId.HasValue)
            {
                deliveryItem = null;
                return UcdDeliveryResultStatus.Error;
            }

            responseData.DeliveryResults.FindingsEdocId = edocId.Value;
            if (this.RequestData.Action == UcdDeliveryAction.DeliverExisting)
            {
                return this.ProcessPostRequest(responseData, out deliveryItem, out errors);
            }
            else
            {
                var previousDelivery = this.RequestData.PreviousDeliveryItem;
                if (previousDelivery.UcdDeliveryType == UcdDeliveryType.Manual)
                {
                    return this.ProcessManualGetRequest(responseData, out deliveryItem, out errors);
                }
                else
                {
                    return this.ProcessFannieGetRequest(responseData, out deliveryItem, out errors);
                }
            }
        }

        /// <summary>
        /// Processes a post request.
        /// </summary>
        /// <param name="responseData">The response data from the POST request.</param>
        /// <param name="deliveryItem">The delivery item created.</param>
        /// <param name="errors">Any errors found.</param>
        /// <returns>The result status.</returns>
        private UcdDeliveryResultStatus ProcessPostRequest(FannieMaeUcdDeliveryResponseData responseData, out UcdDelivery deliveryItem, out List<string> errors)
        {
            responseData.UcdEdocId = this.RequestData.UcdEdocId.Value;

            deliveryItem = this.CreateUcdDeliveryItem(responseData, out errors);
            if (deliveryItem == null)
            {
                return UcdDeliveryResultStatus.Error;
            }

            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.RequestData.LoanId, typeof(FannieMaeUcdDeliveryRequestHandler));
            dataLoan.InitSave(Constants.ConstAppDavid.SkipVersionCheck);

            dataLoan.sUcdDeliveryCollection.Add(deliveryItem);
            dataLoan.Save();

            return UcdDeliveryResultStatus.Delivered;
        }

        /// <summary>
        /// Processes a previous Manual delivery item for a GET request.
        /// </summary>
        /// <param name="responseData">The response data.</param>
        /// <param name="deliveryItem">The delivery item created.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>The response status.</returns>
        private UcdDeliveryResultStatus ProcessManualGetRequest(FannieMaeUcdDeliveryResponseData responseData, out UcdDelivery deliveryItem, out List<string> errors)
        {
            deliveryItem = FannieMaeUcdDelivery.CreateFromManual(responseData, this.RequestData.PreviousDeliveryItem as ManualUcdDelivery);
            if (deliveryItem == null)
            {
                errors = new List<string>() { "Unable to create UCD item." };
                return UcdDeliveryResultStatus.Error;
            }

            var dataLoan = CPageData.CreateUsingSmartDependency(this.RequestData.LoanId, typeof(FannieMaeUcdDeliveryRequestHandler));
            dataLoan.InitSave(Constants.ConstAppDavid.SkipVersionCheck);

            dataLoan.sUcdDeliveryCollection.Add(deliveryItem);
            dataLoan.sUcdDeliveryCollection.Remove(this.RequestData.PreviousDeliveryItem.UcdDeliveryId);

            dataLoan.Save();
            errors = null;
            return UcdDeliveryResultStatus.Retrieved;
        }

        /// <summary>
        /// Process a previous Fannie Mae delivery item for a GET request.
        /// </summary>
        /// <param name="responseData">The response data.</param>
        /// <param name="deliveryItem">The delivery item updated.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>The response status.</returns>
        private UcdDeliveryResultStatus ProcessFannieGetRequest(FannieMaeUcdDeliveryResponseData responseData, out UcdDelivery deliveryItem, out List<string> errors)
        {
            var dataLoan = CPageData.CreateUsingSmartDependency(this.RequestData.LoanId, typeof(FannieMaeUcdDeliveryRequestHandler));
            dataLoan.InitSave(Constants.ConstAppDavid.SkipVersionCheck);

            // I want to modify the one in the loan rather than the one in the request data.
            FannieMaeUcdDelivery fannieItem = dataLoan.sUcdDeliveryCollection.Get(this.RequestData.PreviousDeliveryItem.UcdDeliveryId) as FannieMaeUcdDelivery;
            if (fannieItem == null)
            {
                errors = new List<string>() { "Unable to find UCD delivery item." };
                deliveryItem = null;
                return UcdDeliveryResultStatus.Error;
            }

            fannieItem.UpdateDeliveryProperties(responseData);
            dataLoan.Save();

            deliveryItem = fannieItem;
            errors = null;
            return UcdDeliveryResultStatus.Retrieved;
        }
    }
}
