﻿namespace LendersOffice.Integration.UcdDelivery.FannieMae
{
    using System;

    /// <summary>
    /// The Fannie Mae UCD generation response data.
    /// </summary>
    public class FannieMaeUcdDeliveryResponseData : UcdDeliveryResponseData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FannieMaeUcdDeliveryResponseData"/> class.
        /// </summary>
        /// <param name="resultStatus">The generation status.</param>
        /// <param name="status">The status on the delivery xml.</param>
        /// <param name="findingsHtml">The findings html.</param>
        /// <param name="casefileId">The casefile id extracted from the delivery xml.</param>
        /// <param name="generationResults">Results from a generation request.</param>
        /// <param name="deliveryResults">Results from a delivery request.</param>
        public FannieMaeUcdDeliveryResponseData(UcdDeliveryResultStatus resultStatus, FannieMaeUcdDeliveryStatus status, string findingsHtml, string casefileId, UcdGenerationResults generationResults = null, UcdDeliveryResults deliveryResults = null)
            : base(resultStatus, deliveryResults, generationResults)
        {
            this.CasefileStatus = status;
            this.CasefileId = casefileId;
            this.FindingsHtml = findingsHtml;
        }

        /// <summary>
        /// Gets the casefile statuls.
        /// </summary>
        /// <value>The Fannie case file status.</value>
        public FannieMaeUcdDeliveryStatus CasefileStatus
        {
            get;
        }

        /// <summary>
        /// Gets the casefile id.
        /// </summary>
        /// <value>The casefile id.</value>
        public string CasefileId
        {
            get;
        }

        /// <summary>
        /// Gets the findings HTML.
        /// </summary>
        /// <value>The findings HTML.</value>
        public string FindingsHtml
        {
            get;
        }

        /// <summary>
        /// Gets or sets the Edoc Id for the UCD file.
        /// </summary>
        /// <value>The Edoc id for the UCD file.</value>
        /// <remarks>
        /// The GenerationResult property technically contains the UcdEdocId but Fannie Mae responses don't need to Generate in order to contain a UCD file.
        /// Thus we have this property here to hold the Ucd Edoc id rather than adding in some fake GenerationResult object.
        /// </remarks>
        public Guid? UcdEdocId
        {
            get;
            set;
        }
    }
}
