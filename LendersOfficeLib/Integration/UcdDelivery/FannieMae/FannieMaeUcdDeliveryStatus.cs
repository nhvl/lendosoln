﻿namespace LendersOffice.Integration.UcdDelivery.FannieMae
{
    using System.ComponentModel;

    /// <summary>
    /// Delivery status for requests to Fannie Mae.
    /// </summary>
    public enum FannieMaeUcdDeliveryStatus
    {
        /// <summary>
        /// Successful result.
        /// </summary>
        [Description("Successful")]
        Successful = 0,

        /// <summary>
        /// Not successful result.
        /// </summary>
        [Description("Not Successful")]
        NotSuccessful = 1
    }
}
