﻿namespace LendersOffice.Integration.UcdDelivery.FannieMae
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Net;
    using System.Text;
    using Admin;
    using Constants;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using Templates;

    /// <summary>
    /// Server for Fannie Mae UCD integration requests.
    /// </summary>
    public class FannieMaeUcdDeliveryServer : AbstractIntegrationServer
    {
        /// <summary>
        /// The Fannie Mae request data.
        /// </summary>
        private FannieMaeUcdDeliveryRequestData requestData;

        /// <summary>
        /// The errors encountered.
        /// </summary>
        private List<string> errors = new List<string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="FannieMaeUcdDeliveryServer"/> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        public FannieMaeUcdDeliveryServer(FannieMaeUcdDeliveryRequestData requestData)
        {
            this.requestData = requestData;
        }

        /// <summary>
        /// Gets the errors encountered.
        /// </summary>
        /// <returns>The errors encountered.</returns>
        public override List<string> GetErrors()
        {
            return this.errors;
        }

        /// <summary>
        /// Places the request.
        /// </summary>
        /// <param name="orderXml">The payload.</param>
        /// <returns>The response as a string.</returns>
        public override string PlaceRequest(string orderXml)
        {
            bool isTest = BrokerDBExtensions.GetUcdConfig(this.requestData.Principal.BrokerDB).IsFannieMaeTesting;
            string url;
            string method;
            NameValueCollection headers;
            if (this.requestData.Action == UcdDeliveryAction.DeliverExisting)
            {
                method = "POST";
                url = isTest ? ConstStage.FannieMaeUcdTestPostUrl : ConstStage.FannieMaeUcdProductionPostUrl;
                headers = this.CreatePostHeaders();
            }
            else if (this.requestData.Action == UcdDeliveryAction.GetResults)
            {
                method = "GET";
                url = string.Format(isTest ? ConstStage.FannieMaeUcdTestCasefileIdGetUrlFormatString : ConstStage.FannieMaeUcdProductionCasefileIdGetUrlFormatString, this.requestData.FannieCaseFileId);
                headers = this.CreateGetHeaders();
            }
            else
            {
                this.errors.Add($"Invalid action: {this.requestData.Action}");
                return null;
            }

            var webRequest = this.CreateWebRequest(url, method, headers);
            if (!string.IsNullOrEmpty(orderXml))
            {
                byte[] payloadBytes = Encoding.UTF8.GetBytes(orderXml);

                using (Stream stream = webRequest.GetRequestStream())
                {
                    stream.Write(payloadBytes, 0, payloadBytes.Length);
                }
            }

            try
            {
                using (WebResponse response = webRequest.GetResponse())
                {
                    string content;
                    using (Stream stream = response.GetResponseStream())
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        content = reader.ReadToEnd();
                        return content;
                    }
                }
            }
            catch (WebException webExc)
            {
                // Fannie's authorization errors come in as web exceptions.
                this.errors.Add(webExc.Message);
                return null;
            }
            catch (InvalidOperationException exc)
            {
                throw new DeveloperException(ErrorMessage.SystemError, exc);
            }
        }

        /// <summary>
        /// Creates a web request to send out.
        /// </summary>
        /// <param name="url">The url receiving the request.</param>
        /// <param name="method">The method.</param>
        /// <param name="customHeaders">The custom headers based on method.</param>
        /// <returns>A prepared web request.</returns>
        private HttpWebRequest CreateWebRequest(string url, string method, NameValueCollection customHeaders)
        {
            var webRequest = (HttpWebRequest)WebRequest.Create(url.ToString());
            webRequest.KeepAlive = false;
            webRequest.Method = method;
            webRequest.Timeout = 60000;
            webRequest.ContentType = "application/xml";

            var headers = this.CreateCommonHeaders();
            headers.Add(customHeaders);

            webRequest.Headers.Add(headers);

            this.LogHeaders(headers, url);
            return webRequest;
        }

        /// <summary>
        /// Logs the headers.
        /// </summary>
        /// <param name="headers">The headers to log.</param>
        /// <param name="url">The target url.</param>
        private void LogHeaders(NameValueCollection headers, string url)
        {
            var tempHeader = new NameValueCollection(headers);
            if (tempHeader["Authorization"] != null)
            {
                tempHeader["Authorization"] = "*****";
            }

            // Might as well log the url with the headers.
            tempHeader.Add("Url", url);
            UcdDeliveryUtilities.LogHeaders(UcdDeliveryUtilities.FannieMaeServerLogHeader, tempHeader);
        }

        /// <summary>
        /// Gets the common headers between the two requests.
        /// </summary>
        /// <returns>The common headers between the two requests.</returns>
        private NameValueCollection CreateCommonHeaders()
        {
            var basicAuthCredentials = Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{this.requestData.GseUserName}:{this.requestData.GsePassword}"));
            return new NameValueCollection()
            {
                { "EntityName", "UCD" },
                { "producername", "MeridianLink" },
                { "Authorization", $"Basic {basicAuthCredentials}" }
            };
        }

        /// <summary>
        /// Creates the headers for a POST request.
        /// </summary>
        /// <returns>Dictionary containing the headers for a post request.</returns>
        private NameValueCollection CreatePostHeaders()
        {
            return new NameValueCollection()
            {
                { "EventName", "SYNC_SUBMIT" },
                { "file_name", "ucd.xml" },
                { "softwareproviderID",  "VMLILQ01B" },
                { "softwareversion", DateTime.Today.Year.ToString() },
            };
        }

        /// <summary>
        /// Gets the headers for a GET request.
        /// </summary>
        /// <returns>The headers.</returns>
        private NameValueCollection CreateGetHeaders()
        {
            return new NameValueCollection()
            {
                { "EventName", "LENDER_FEEDBACK" },
            };
        }
    }
}
