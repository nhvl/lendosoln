﻿namespace LendersOffice.Integration.UcdDelivery.FannieMae
{
    using System;
    using DataAccess;
    using LendersOffice.Security;

    /// <summary>
    /// Data used to generated a UCD delivery request to Fannie Mae.
    /// </summary>
    public class FannieMaeUcdDeliveryRequestData : AbstractUcdDeliveryRequestData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FannieMaeUcdDeliveryRequestData"/> class. Meant for POST requests.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="appId">The app id.</param>
        /// <param name="closingDisclosureId">The closing disclosure.</param>
        /// <param name="ucdEdocId">The UCD Edoc id.</param>
        /// <param name="principal">The principal.</param>
        private FannieMaeUcdDeliveryRequestData(Guid loanId, Guid appId, Guid closingDisclosureId, Guid ucdEdocId, AbstractUserPrincipal principal)
            : base(loanId, appId, closingDisclosureId, UcdDeliveryAction.DeliverExisting, principal)
        {
            this.UcdEdocId = ucdEdocId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FannieMaeUcdDeliveryRequestData"/> class.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="appId">The app id.</param>
        /// <param name="deliveryItem">The delivery item the GET request was called on.</param>
        /// <param name="principal">The principal of the user.</param>
        private FannieMaeUcdDeliveryRequestData(Guid loanId, Guid appId, UcdDelivery deliveryItem, AbstractUserPrincipal principal)
            : base(loanId, appId, null, UcdDeliveryAction.GetResults, principal)
        {
            this.PreviousDeliveryItem = deliveryItem;
        }

        /// <summary>
        /// Gets the delivery target.
        /// </summary>
        /// <value>The delivery target.</value>
        /// <remarks>The delivery target is always Fannie Mae.</remarks>
        public override UcdDeliveredToEntity? DeliveryTarget => UcdDeliveredToEntity.FannieMae;

        /// <summary>
        /// Gets the UCD Edoc id of the UCD XML to deliver.
        /// </summary>
        /// <value>The UCD Edoc id. Can be null if just a GET.</value>
        /// <seealso cref="FreddieMac.FreddieMacUcdDeliveryRequestData.UcdEdocId"/>
        public Guid? UcdEdocId
        {
            get;
        }

        /// <summary>
        /// Gets the case file id from Fannie Mae.
        /// </summary>
        /// <value>The case file id from Fannie Mae.</value>
        public string FannieCaseFileId
        {
            get
            {
                return this.PreviousDeliveryItem?.CaseFileId;
            }
        }

        /// <summary>
        /// Gets the delivery item that the GET request was called on.
        /// </summary>
        /// <value>The delivery item of the GET request.</value>
        public UcdDelivery PreviousDeliveryItem
        {
            get;
        }

        /// <summary>
        /// Creates a Fannie Mae request data specifically for delivering existing UCD files.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="appId">The app id.</param>
        /// <param name="cdDate">The closing disclosure date of the UCD doc to deliver.</param>
        /// <param name="principal">The principal of the user.</param>
        /// <returns>The created request data.</returns>
        public static FannieMaeUcdDeliveryRequestData CreateFannieMaePostRequestData(Guid loanId, Guid appId, ClosingDisclosureDates cdDate, AbstractUserPrincipal principal)
        {
            return new FannieMaeUcdDeliveryRequestData(loanId, appId, cdDate.UniqueId, cdDate.UcdDocument, principal);
        }

        /// <summary>
        /// Creates a Fannie Mae request data specifically for retrieving findings from a pre existing Fannie Mae delivery item.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="appId">The app id.</param>
        /// <param name="deliveryItem">The delivery item.</param>
        /// <param name="principal">The principal.</param>
        /// <returns>The Fannie Mae request data for GET requests.</returns>
        public static FannieMaeUcdDeliveryRequestData CreateFannieMaeGetRequestData(Guid loanId, Guid appId, UcdDelivery deliveryItem, AbstractUserPrincipal principal)
        {
            return new FannieMaeUcdDeliveryRequestData(loanId, appId, deliveryItem, principal);
        }

        /// <summary>
        /// Validates the request data.
        /// </summary>
        /// <param name="error">Any errors during validation.</param>
        /// <returns>True if valid, false otherwise.</returns>
        public override bool Validate(out string error)
        {
            if (this.Action != UcdDeliveryAction.DeliverExisting && this.Action != UcdDeliveryAction.GetResults)
            {
                error = $"Invalid action {this.Action}";
                return false;
            }

            if (this.Action == UcdDeliveryAction.DeliverExisting &&
                (!this.UcdEdocId.HasValue || this.UcdEdocId.Value == Guid.Empty))
            {
                error = $"Invalid closing disclosure. No associated UCD file.";
                return false;
            }

            if (this.Action == UcdDeliveryAction.GetResults &&
                (this.PreviousDeliveryItem == null ||
                 (this.PreviousDeliveryItem.UcdDeliveryType != UcdDeliveryType.Manual && this.PreviousDeliveryItem.UcdDeliveryType != UcdDeliveryType.FannieMaeUcdDelivery) ||
                 this.PreviousDeliveryItem.DeliveredTo != UcdDeliveredToEntity.FannieMae))
            {
                error = "Please specify a valid delivery item for this GET request.";
                return false;
            }

            return base.Validate(out error);
        }
    }
}
