﻿namespace LendersOffice.Integration.UcdDelivery.FannieMae
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Xml;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Represents UCD delivery data sent to Fannie Mae through LendingQB.
    /// </summary>
    public class FannieMaeUcdDelivery : UcdDelivery
    {
        /// <summary>
        /// Fannie Mae namespace.
        /// </summary>
        public const string FannieMaeNamespace = @"http://www.fanniemae.com/singlefamily/lenderfeedback/v1.x";

        /// <summary>
        /// Stored procedure to create a Fannie Mae delivery item.
        /// </summary>
        private static readonly StoredProcedureName UcdDeliveryFannieMaeCreate = StoredProcedureName.Create("UCD_DELIVERY_FANNIE_MAE_Create").Value;

        /// <summary>
        /// Stored procedure to update a Fannie Mae delivery item.
        /// </summary>
        private static readonly StoredProcedureName UcdDeliveryFannieMaeUpdate = StoredProcedureName.Create("UCD_DELIVERY_FANNIE_MAE_Update").Value;

        /// <summary>
        /// A suffix for the findings xml FileDB key.
        /// </summary>
        private static readonly string FannieMaeFindingsXmlSuffix = "_FannieMaeFindingsXml";

        /// <summary>
        /// The findings xml loaded from file db.
        /// </summary>
        private Lazy<string> findingsXml;

        /// <summary>
        /// The findings HTML.
        /// </summary>
        private Lazy<string> findingsHtml;

        /// <summary>
        /// The findings XML FileDb key.
        /// </summary>
        private Guid? findingsXmlFileDbKey;

        /// <summary>
        /// The batch id.
        /// </summary>
        private string batchId;

        /// <summary>
        /// The casefile status.
        /// </summary>
        private FannieMaeUcdDeliveryStatus casefileStatus;

        /// <summary>
        /// The findings document id.
        /// </summary>
        private Guid? findingsDocumentId;

        /// <summary>
        /// Initializes a new instance of the <see cref="FannieMaeUcdDelivery"/> class.
        /// </summary>
        /// <param name="reader">Data reader.</param>
        protected internal FannieMaeUcdDelivery(IDataReader reader)
            : base(reader)
        {
            this.batchId = (string)reader["BatchId"];
            this.casefileStatus = (FannieMaeUcdDeliveryStatus)reader["CasefileStatus"];
            this.findingsDocumentId = reader["FindingsDocumentId"] == DBNull.Value ? (Guid?)null : (Guid)reader["FindingsDocumentId"];
            this.FindingsXmlFileDbKey = reader["FindingsXmlFileDbKey"] == DBNull.Value ? (Guid?)null : (Guid)reader["FindingsXmlFileDbKey"];

            // The FindingsXmlFileDbKey property will trigger the dirty bit but this is not dirty since it just loaded from a reader.
            this.IsDirty = false;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FannieMaeUcdDelivery"/> class.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="brokerId">The broker id.</param>
        private FannieMaeUcdDelivery(Guid loanId, Guid brokerId)
            : base(loanId, brokerId)
        {
        }

        /// <summary>
        /// Gets the UCD Delivery Type.
        /// </summary>
        /// <value>UCD Delivery Type.</value>
        public override UcdDeliveryType UcdDeliveryType => UcdDeliveryType.FannieMaeUcdDelivery;

        /// <summary>
        /// Gets an ID generated by FannieMae for which batch the UCD was processed in, later used to find and identify the UCD file.
        /// </summary>
        /// <remarks>FannieMae specific field.</remarks>
        /// <value>ID generated by FannieMae for which batch the UCD was processed in.</value>
        public string BatchId
        {
            get
            {
                return this.batchId;
            }

            private set
            {
                this.batchId = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets the status of the UCD file.
        /// </summary>
        /// <remarks>FannieMae specific field.</remarks>
        /// <value>The status of the UCD file.</value>
        public FannieMaeUcdDeliveryStatus CasefileStatus
        {
            get
            {
                return this.casefileStatus;
            }

            private set
            {
                this.casefileStatus = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets the Generic Edoc Id for the HTML findings of the UCD file, much like DU findings.
        /// </summary>
        /// <remarks>FannieMae specific field.</remarks>
        /// <value>Generic Edoc ID for  the HTML findings document.</value>
        public Guid? FindingsDocumentId
        {
            get
            {
                return this.findingsDocumentId;
            }

            private set
            {
                this.findingsDocumentId = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets the FileDB key for the findings xml.
        /// </summary>
        /// <value>The FileDB key for the findings xml.</value>
        public Guid? FindingsXmlFileDbKey
        {
            get
            {
                return this.findingsXmlFileDbKey;
            }

            private set
            {
                this.findingsXmlFileDbKey = value;
                if (!this.findingsXmlFileDbKey.HasValue)
                {
                    this.findingsXml = new Lazy<string>(() => null);
                }
                else
                {
                    this.findingsXml = new Lazy<string>(() => FileDBTools.ReadDataText(E_FileDB.Normal, GetFindingsXmlFileDbKey(this.findingsXmlFileDbKey.Value)));
                }

                this.findingsHtml = null;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets the findings HTML from the findings XML.
        /// </summary>
        /// <value>The findings HTML from the findings XML.</value>
        public string FindingsHtml
        {
            get
            {
                if (this.findingsHtml == null)
                {
                    var xml = this.findingsXml.Value;
                    if (string.IsNullOrEmpty(xml))
                    {
                        this.findingsHtml = new Lazy<string>(() => null);
                    }
                    else
                    {
                        this.findingsHtml = new Lazy<string>(() =>
                        {
                            XmlDocument findingsXml = new XmlDocument();
                            findingsXml.LoadXml(xml);
                            XmlNamespaceManager manager = new XmlNamespaceManager(findingsXml.NameTable);
                            manager.AddNamespace("ns", FannieMaeNamespace);

                            // If you're going to edit this, please edit DocMagicUcdGenerationResponseProvider.ParseDeliveryForFannie as well.
                            var node = findingsXml.SelectSingleNode("//ns:LenderFeedback/ns:Casefiles/ns:Casefile/ns:Datasets/ns:Dataset/ns:CasefileAttachments/ns:CasefileAttachment[ns:CasefileAttachmentType/text()='HTML']/ns:CasefileAttachmentContent", manager);
                            return node?.InnerText;
                        });
                    }
                }

                return this.findingsHtml.Value;
            }
        }

        /// <summary>
        /// Creates the findings xml FileDB key given a guid key.
        /// </summary>
        /// <param name="key">A guid key.</param>
        /// <returns>The full FileDB key.</returns>
        public static string GetFindingsXmlFileDbKey(Guid key)
        {
            return key.ToString("N") + FannieMaeFindingsXmlSuffix;
        }

        /// <summary>
        /// Creates a Fannie delivery item from response and request data.
        /// </summary>
        /// <param name="responseData">The response data.</param>
        /// <param name="requestData">The request data.</param>
        /// <returns>The Fannie delivery item. Null if not successful.</returns>
        public static FannieMaeUcdDelivery Create(FannieMaeUcdDeliveryResponseData responseData, AbstractUcdDeliveryRequestData requestData)
        {
            if (responseData == null || requestData == null)
            {
                return null;
            }

            FannieMaeUcdDelivery deliveryItem = new FannieMaeUcdDelivery(requestData.LoanId, requestData.Principal.BrokerId);
            deliveryItem.BatchId = responseData.DeliveryResults.BatchId;
            deliveryItem.CaseFileId = responseData.CasefileId;
            deliveryItem.CasefileStatus = responseData.CasefileStatus;
            deliveryItem.DateOfDelivery = CDateTime.Create(requestData.RequestDate);
            deliveryItem.DeliveredTo = UcdDeliveredToEntity.FannieMae;
            deliveryItem.FindingsDocumentId = responseData.DeliveryResults.FindingsEdocId;
            deliveryItem.FindingsXmlFileDbKey = responseData.DeliveryResults.FindingsXmlFileDbKey;

            if (responseData?.GenerationResults?.UcdFileEdocId != null &&
                (requestData.Action == UcdDeliveryAction.GenerateAndDeliver || requestData.Action == UcdDeliveryAction.GenerateOnly))
            {
                deliveryItem.FileDeliveredDocumentId = responseData.GenerationResults.UcdFileEdocId;
            }
            else
            {
                deliveryItem.FileDeliveredDocumentId = responseData.UcdEdocId.Value;
            }

            return deliveryItem;
        }

        /// <summary>
        /// Creates a Fannie Mae delivery item from a Manual delivery item.
        /// </summary>
        /// <param name="responseData">The response data.</param>
        /// <param name="previousItem">The previous item.</param>
        /// <returns>The created Fannie Mae delivery item.</returns>
        public static FannieMaeUcdDelivery CreateFromManual(FannieMaeUcdDeliveryResponseData responseData, ManualUcdDelivery previousItem)
        {
            if (responseData == null || previousItem == null)
            {
                return null;
            }

            FannieMaeUcdDelivery deliveryItem = new FannieMaeUcdDelivery(previousItem.LoanId, previousItem.BrokerId);
            deliveryItem.BatchId = responseData.DeliveryResults.BatchId;
            deliveryItem.CaseFileId = previousItem.CaseFileId;
            deliveryItem.CasefileStatus = responseData.CasefileStatus;
            deliveryItem.DateOfDelivery = previousItem.DateOfDelivery;
            deliveryItem.DeliveredTo = UcdDeliveredToEntity.FannieMae;
            deliveryItem.FindingsDocumentId = responseData.DeliveryResults.FindingsEdocId;
            deliveryItem.FindingsXmlFileDbKey = responseData.DeliveryResults.FindingsXmlFileDbKey;
            deliveryItem.FileDeliveredDocumentId = previousItem.FileDeliveredDocumentId;
            deliveryItem.IncludeInUldd = previousItem.IncludeInUldd;
            deliveryItem.Note = previousItem.Note;

            return deliveryItem;
        }

        /// <summary>
        /// Updates the Fannie Mae delivery item after the GET request.
        /// </summary>
        /// <param name="responseData">The response data to update from.</param>
        public void UpdateDeliveryProperties(FannieMaeUcdDeliveryResponseData responseData)
        {
            this.BatchId = responseData.DeliveryResults.BatchId;
            this.CasefileStatus = responseData.CasefileStatus;
            this.FindingsDocumentId = responseData.DeliveryResults.FindingsEdocId;
            this.FindingsXmlFileDbKey = responseData.DeliveryResults.FindingsXmlFileDbKey;
        }

        /// <summary>
        /// Creates a viewmodel out of this object.
        /// </summary>
        /// <returns>The viewmodel of this object.</returns>
        public override UcdDeliveryView ToView()
        {
            var viewModel = base.ToView();
            viewModel.BatchId = this.BatchId;
            viewModel.Status = this.CasefileStatus.GetDescription();

            return viewModel;
        }

        /// <summary>
        /// Saves to the Fannie Mae table.
        /// </summary>
        /// <param name="conn">Sql connection.</param>
        /// <param name="trans">Sql transaction context.</param>
        /// <param name="isNew">Whether this is a new delivery item or not.</param>
        protected override void PerformAdditionalSave(DbConnection conn, DbTransaction trans, bool isNew)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@UcdDeliveryId", this.UcdDeliveryId),
                new SqlParameter("@BatchId", this.BatchId),
                new SqlParameter("@CasefileStatus", this.CasefileStatus),
                new SqlParameter("@FindingsDocumentId", this.FindingsDocumentId),
                new SqlParameter("@FindingsXmlFileDbKey", this.FindingsXmlFileDbKey)
            };

            StoredProcedureName sp;
            if (!isNew)
            {
                sp = UcdDeliveryFannieMaeUpdate;
            }
            else
            {
                sp = UcdDeliveryFannieMaeCreate;
            }

            StoredProcedureDriverHelper.ExecuteNonQuery(conn, trans, sp, parameters, TimeoutInSeconds.Default);
        }
    }
}
