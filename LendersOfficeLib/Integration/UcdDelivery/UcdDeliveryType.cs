﻿namespace LendersOffice.Integration.UcdDelivery
{
    /// <summary>
    /// UCD Delivery Data Type.
    /// </summary>
    public enum UcdDeliveryType
    {
        /// <summary>
        /// UCD delivery data was manually entered.
        /// </summary>
        Manual = 0,

        /// <summary>
        /// UCD delivery data for Fannie Mae.
        /// </summary>
        FannieMaeUcdDelivery = 1,

        /// <summary>
        /// Ucd delivery data for Freddie Mac.
        /// </summary>
        FreddieMacUcdDelivery = 2
    }
}
