﻿namespace LendersOffice.Integration.UcdDelivery
{
    /// <summary>
    /// The result type from a ucd generation request.
    /// </summary>
    public enum UcdDeliveryResultStatus
    {
        /// <summary>
        /// Error encountered. 
        /// </summary>
        Error = 0,

        /// <summary>
        /// UCD was generated. Delivery not requested.
        /// </summary>
        Generated = 1,

        /// <summary>
        /// Ucd was generated but it could not be delivered.
        /// </summary>
        GeneratedButNotDelivered = 2,

        /// <summary>
        /// Ucd was generated and it was successfully delivered.
        /// </summary>
        GeneratedAndDelivered = 3,

        /// <summary>
        /// An existing UCD file was delivered.
        /// </summary>
        Delivered = 4,

        /// <summary>
        /// The findings for an existing delivery item was retrieved.
        /// </summary>
        Retrieved = 5
    }
}
