﻿namespace LendersOffice.Integration.UcdDelivery.DocMagic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Constants;
    using Conversions.Templates;
    using DataAccess;
    using Integration.Templates;
    using LendersOffice.Conversions;
    using LendersOffice.Conversions.UcdDelivery;

    /// <summary>
    /// Handles UCD generation and delivery requests to DocMagic.
    /// </summary>
    public class DocMagicUcdDeliveryRequestHandler : AbstractUcdDeliveryRequestHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocMagicUcdDeliveryRequestHandler"/> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        public DocMagicUcdDeliveryRequestHandler(AbstractUcdDeliveryRequestData requestData)
            : base(requestData)
        {
            base.RequestProvider = new DocMagicUcdDeliveryRequestProvider(this.RequestData);
        }

        /// <summary>
        /// Gets the data for the request.
        /// </summary>
        /// <value>The data for the request.</value>
        protected new DocMagicUcdDeliveryRequestData RequestData => (DocMagicUcdDeliveryRequestData)base.RequestData;

        /// <summary>
        /// Gets the exporter that creates the payload.
        /// </summary>
        /// <value>The exporter that creates the payload.</value>
        protected new DocMagicUcdDeliveryRequestProvider RequestProvider => (DocMagicUcdDeliveryRequestProvider)base.RequestProvider;

        /// <summary>
        /// Gets the importer that processes the response.
        /// </summary>
        /// <value>The importer that processes the response.</value>
        protected new DocMagicUcdDeliveryResponseProvider ResponseProvider => (DocMagicUcdDeliveryResponseProvider)base.ResponseProvider;

        /// <summary>
        /// Gets a server object that transmits the payload.
        /// </summary>
        /// <value>A server object that transmits the payload.</value>
        protected override AbstractIntegrationServer Server
        {
            get
            {
                // We're stuck with the legacy implementation of DocMagicServer2 for sending
                // DsiDocumentServerRequests, so we can't use this property.
                throw new NotImplementedException("Must use DocMagicServer2 directly in this request handler.");
            }
        }

        /// <summary>
        /// Processes the response from DocMagic.
        /// </summary>
        /// <param name="deliveryItem">A delivery item generated from the response.</param>
        /// <param name="errors">A list of errors generated during the request.</param>
        /// <returns>A result status.</returns>
        protected override UcdDeliveryResultStatus ProcessResponse(out UcdDelivery deliveryItem, out List<string> errors)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.RequestData.LoanId, typeof(DocMagicUcdDeliveryRequestHandler));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            var foundCd = dataLoan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.FirstOrDefault(cd => cd.UniqueId == this.RequestData.ClosingDisclosureId);
            if (foundCd == null)
            {
                errors = new List<string>() { "Unable to find closing disclosure." };
                deliveryItem = null;
                return UcdDeliveryResultStatus.Error;
            }

            if (this.ResponseProvider.HasErrors)
            {
                deliveryItem = null;
                errors = new List<string>(this.ResponseProvider.Errors);
                return UcdDeliveryResultStatus.Error;
            }

            var responseData = this.ResponseProvider.ResponseData;
            var ucdEdocId = this.SaveUcdFile(responseData.GenerationResults.UcdFileXml, out errors);
            if (!ucdEdocId.HasValue)
            {
                deliveryItem = null;
                return UcdDeliveryResultStatus.Error;
            }

            responseData.GenerationResults.UcdFileEdocId = ucdEdocId.Value;
            foundCd.UcdDocument = ucdEdocId.Value;
            dataLoan.SaveLEandCDDates();

            if (this.RequestData.Action == UcdDeliveryAction.GenerateOnly)
            {
                deliveryItem = null;
                errors = null;

                dataLoan.Save();
                return UcdDeliveryResultStatus.Generated;
            }
            else
            {
                if (responseData.ResultStatus == UcdDeliveryResultStatus.GeneratedButNotDelivered)
                {
                    // At this point, we have the UCD document uploaded to EDocs but delivery failed. We just need to save now.
                    // The delivery errors are in the responseProvider.
                    deliveryItem = null;
                    if (this.ResponseProvider.HasErrors)
                    {
                        errors = this.ResponseProvider.Errors;
                    }
                    else
                    {
                        errors = new List<string>() { "Delivery unsuccessful." };
                    }

                    dataLoan.Save();
                    return UcdDeliveryResultStatus.GeneratedButNotDelivered;
                }

                // TODO CLEANUP: We may want to pull the Findings saving into the Create methods.
                UcdDeliveryResponseData deliveryData = responseData as UcdDeliveryResponseData;
                var fileDbKey = this.SaveFindingsXmlToFileDb(this.RequestData.DeliveryTarget.Value, deliveryData.DeliveryResults.FindingsXml, out errors);
                if (!fileDbKey.HasValue)
                {
                    deliveryItem = null;
                    return UcdDeliveryResultStatus.Error;
                }

                deliveryData.DeliveryResults.FindingsXmlFileDbKey = fileDbKey.Value;
                var edocId = this.SaveFindingsHtmlToEdoc(this.RequestData.DeliveryTarget.Value, deliveryData, out errors);
                if (!edocId.HasValue)
                {
                    deliveryItem = null;
                    return UcdDeliveryResultStatus.Error;
                }

                deliveryData.DeliveryResults.FindingsEdocId = edocId.Value;
                deliveryItem = this.CreateUcdDeliveryItem(deliveryData, out errors);
                if (deliveryItem == null)
                {
                    return UcdDeliveryResultStatus.Error;
                }

                dataLoan.sUcdDeliveryCollection.Add(deliveryItem);
                dataLoan.Save();

                return UcdDeliveryResultStatus.GeneratedAndDelivered;
            }
        }

        /// <summary>
        /// Since we pass around a <see cref="DsiDocumentServerResponse"/> object instead of a string payload,
        /// this inherited method has no use. The relevant functionality instead occurs in <see cref="TransmitPayload"/>.
        /// </summary>
        /// <param name="responsePayload">The string response payload.</param>
        /// <returns>The response provider.</returns>
        protected override AbstractResponseProvider GenerateResponseProvider(string responsePayload)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// The DocMagic-specific implementation for transmitting the payload. This overrides the abstract version
        /// because we have to directly call the static <see cref="DocMagicServer2"/> class instead of using
        /// the <see cref="AbstractUcdDeliveryRequestHandler.Server"/> property.
        /// </summary>
        /// <param name="errors">Any errors found.</param>
        /// <returns>True if parsed. False otherwise.</returns>
        protected override bool TransmitPayload(out List<string> errors)
        {
            var payload = this.RequestProvider.GenerateRequest();

            // Logging is already done in this method.
            var response = DocMagicServer2.Submit(payload, DocMagicServer2.RequestType.UniformClosingDataset, docMagicVendor: this.RequestData.Vendor);
            base.ResponseProvider = new DocMagicUcdDeliveryResponseProvider(response, this.RequestData);
            this.ResponseProvider.ParseResponse();

            errors = null;
            return true;
        }
    }
}
