﻿namespace LendersOffice.Integration.UcdDelivery.DocMagic
{
    using System;
    using DocumentVendor;
    using LendersOffice.Security;

    /// <summary>
    /// Data required to transmit a UCD generation or delivery request to DocMagic.
    /// </summary>
    public class DocMagicUcdDeliveryRequestData : AbstractUcdDeliveryRequestData
    {
        /// <summary>
        /// The vendor config.
        /// </summary>
        private Lazy<VendorConfig> vendorConfig;

        /// <summary>
        /// The vendor config id.
        /// </summary>
        private Guid vendorConfigId;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocMagicUcdDeliveryRequestData"/> class.
        /// </summary>
        /// <param name="docCode">The DocCode for the CD.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="appId">The app id.</param>
        /// <param name="closingDisclosureId">The closing disclosure.</param>
        /// <param name="vendorId">The vendor id associated with the closing disclosure.</param>
        /// <param name="action">The action to be completed by the request.</param>
        /// <param name="principal">The principal.</param>
        public DocMagicUcdDeliveryRequestData(string docCode, Guid loanId, Guid appId, Guid closingDisclosureId, Guid vendorId, UcdDeliveryAction action, AbstractUserPrincipal principal)
            : base(loanId, appId, closingDisclosureId, action, principal)
        {
            this.DocCode = docCode;
            this.VendorId = vendorId;
        }

        /// <summary>
        /// Gets the UCD DocCode.
        /// </summary>
        /// <value>The UCD DocCode.</value>
        public string DocCode
        {
            get;
        }

        /// <summary>
        /// Gets or sets the DM customer id.
        /// </summary>
        /// <value>The customer id.</value>
        public string DmCustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the DM username.
        /// </summary>
        /// <value>The DM username.</value>
        public string DmUsername
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the DM password.
        /// </summary>
        /// <value>The DM password.</value>
        public string DmPassword
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the vendor id associated with the closing disclosure.
        /// </summary>
        /// <value>The vendor id.</value>
        public Guid VendorId
        {
            get
            {
                return this.vendorConfigId;
            }

            private set
            {
                this.vendorConfigId = value;
                this.vendorConfig = new Lazy<VendorConfig>(() => VendorConfig.Retrieve(this.vendorConfigId));
            }
        }

        /// <summary>
        /// Gets the vendor associated with the closing disclosure.
        /// </summary>
        /// <value>The vendor.</value>
        public VendorConfig Vendor
        {
            get
            {
                return this.vendorConfig.Value;
            }
        }

        /// <summary>
        /// Validates the request data.
        /// </summary>
        /// <param name="error">A validation error, if any exist.</param>
        /// <returns>A boolean indicating whether the request data is valid.</returns>
        public override bool Validate(out string error)
        {
            if (string.IsNullOrEmpty(this.DocCode))
            {
                error = "Invalid DocCode";
                return false;
            }

            if (string.IsNullOrEmpty(this.DmCustomerId) || string.IsNullOrEmpty(this.DmUsername) || string.IsNullOrEmpty(this.DmPassword))
            {
                error = "Invalid Doc Magic credentials.";
                return false;
            }

            if (this.VendorId == Guid.Empty)
            {
                error = "No vendor specified.";
                return false;
            }

            if (!this.ClosingDisclosureId.HasValue || this.ClosingDisclosureId == Guid.Empty)
            {
                error = "Invalid closing disclosure ID";
                return false;
            }

            return base.Validate(out error);
        }
    }
}
