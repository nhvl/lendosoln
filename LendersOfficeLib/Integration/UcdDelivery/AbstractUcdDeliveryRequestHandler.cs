﻿namespace LendersOffice.Integration.UcdDelivery
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using ConfigSystem.Operations;
    using DataAccess;
    using Drivers.Gateways;
    using EDocs;
    using EDocs.Contents;
    using Integration.Templates;
    using LendersOffice.Conversions.Templates;
    using LendersOffice.Integration.UcdDelivery.DocMagic;
    using LendersOffice.Integration.UcdDelivery.FannieMae;
    using LendersOffice.Integration.UcdDelivery.FreddieMac;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using ObjLib.Edocs.AutoSaveDocType;

    /// <summary>
    /// Processes requests to generate UCD files and deliver to investors.
    /// </summary>
    public abstract class AbstractUcdDeliveryRequestHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractUcdDeliveryRequestHandler"/> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        public AbstractUcdDeliveryRequestHandler(AbstractUcdDeliveryRequestData requestData)
        {
            this.RequestData = requestData;
        }

        /// <summary>
        /// Gets the data for the request.
        /// </summary>
        /// <value>The data for the request.</value>
        protected AbstractUcdDeliveryRequestData RequestData { get; }

        /// <summary>
        /// Gets or sets an exporter that creates a payload.
        /// </summary>
        /// <value>An exporter that creates a payload.</value>
        protected IRequestProvider RequestProvider { get; set; }

        /// <summary>
        /// Gets or sets a server object that transmits the payload.
        /// </summary>
        /// <value>A server object that transmits the payload.</value>
        protected virtual AbstractIntegrationServer Server { get; set; }

        /// <summary>
        /// Gets or sets the importer that processes the response payload.
        /// </summary>
        /// <value>The importer that processes the response payload.</value>
        protected AbstractResponseProvider ResponseProvider { get; set; } = null;

        /// <summary>
        /// Creates a UCD Generation request handler based on the request data type.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        /// <returns>A new UCD Generation request handler.</returns>
        public static AbstractUcdDeliveryRequestHandler GenerateRequestHandler(AbstractUcdDeliveryRequestData requestData)
        {
            if (requestData is DocMagicUcdDeliveryRequestData)
            {
                return new DocMagicUcdDeliveryRequestHandler(requestData);
            }
            else if (requestData is FannieMaeUcdDeliveryRequestData)
            {
                return new FannieMaeUcdDeliveryRequestHandler(requestData);
            }
            else if (requestData is FreddieMacUcdDeliveryRequestData)
            {
                return new FreddieMacUcdDeliveryRequestHandler(requestData);
            }
            else
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"The RequestData type {requestData.GetType().ToString()} is not handled."));
            }
        }

        /// <summary>
        /// Submits a UCD delivery request to a vendor.
        /// </summary>
        /// <param name="deliveryItem">The delivery item if this was a successful delivery request. Null otherwise.</param>
        /// <param name="errors">Any errors found.</param>
        /// <returns>The UCD delivery result.</returns>
        public UcdDeliveryResultStatus SubmitRequest(out UcdDelivery deliveryItem, out List<string> errors)
        {
            var workflowResults = Tools.IsWorkflowOperationAuthorized(this.RequestData.Principal, this.RequestData.LoanId, WorkflowOperations.GenerateUCD);
            if (!workflowResults.Item1)
            {
                errors = new List<string>() { workflowResults.Item2 };
                deliveryItem = null;
                return UcdDeliveryResultStatus.Error;
            }

            if (!this.TransmitPayload(out errors))
            {
                deliveryItem = null;
                return UcdDeliveryResultStatus.Error;
            }

            return this.ProcessResponse(out deliveryItem, out errors);
        }

        /// <summary>
        /// Transmits the payload to a third party.
        /// </summary>
        /// <param name="errors">Any errors found when transmitting and parsing the response.</param>
        /// <returns>True if parsed successfully. False otherwise.</returns>
        protected virtual bool TransmitPayload(out List<string> errors)
        {
            var payload = this.RequestProvider.SerializeRequest();
            this.RequestProvider.LogRequest(payload);

            var responsePayload = this.Server.PlaceRequest(payload);
            errors = this.Server.GetErrors();
            if (errors != null && errors.Any())
            {
                return false;
            }

            this.ResponseProvider = this.GenerateResponseProvider(responsePayload);
            this.ResponseProvider.LogResponse();
            this.ResponseProvider.ParseResponse();
            if (this.ResponseProvider.ResponseData == null && this.ResponseProvider.HasErrors)
            {
                errors = this.ResponseProvider.Errors;
                return false;
            }

            errors = null;
            return true;
        }

        /// <summary>
        /// Processes the response from a third party.
        /// </summary>
        /// <param name="deliveryItem">A delivery item generated from the response.</param>
        /// <param name="errors">Any errors encountered during the process.</param>
        /// <returns>A result status.</returns>
        protected abstract UcdDeliveryResultStatus ProcessResponse(out UcdDelivery deliveryItem, out List<string> errors);

        /// <summary>
        /// Generates a response provider from the response payload.
        /// </summary>
        /// <param name="responsePayload">The response from a third party.</param>
        /// <returns>The response provider.</returns>
        protected abstract AbstractResponseProvider GenerateResponseProvider(string responsePayload);

        /// <summary>
        /// Creates a UCD delivery item based on target GSE.
        /// </summary>
        /// <param name="responseData">The response data.</param>
        /// <param name="errors">Any errors during creation.</param>
        /// <returns>The item if successful. Null otherwise.</returns>
        protected UcdDelivery CreateUcdDeliveryItem(UcdDeliveryResponseData responseData, out List<string> errors)
        {
            UcdDelivery deliveryItem = null;
            switch (this.RequestData.DeliveryTarget)
            {
                case UcdDeliveredToEntity.FannieMae:
                    var fannieData = responseData as FannieMaeUcdDeliveryResponseData;
                    deliveryItem = FannieMaeUcdDelivery.Create(fannieData, this.RequestData);
                    if (deliveryItem == null)
                    {
                        errors = new List<string>() { "Unable to create UCD item." };
                        return null;
                    }

                    break;
                case UcdDeliveredToEntity.FreddieMac:
                    var freddieData = responseData as FreddieMacUcdDeliveryResponseData;
                    deliveryItem = FreddieMacUcdDelivery.Create(responseData as FreddieMacUcdDeliveryResponseData, this.RequestData);
                    if (deliveryItem == null)
                    {
                        errors = new List<string>() { "Unable to create UCD item." };
                        return null;
                    }

                    break;
                default:
                    throw new UnhandledEnumException(this.RequestData.DeliveryTarget);
            }

            errors = null;
            return deliveryItem;
        }

        /// <summary>
        /// Saves the findings xml to file db.
        /// </summary>
        /// <param name="deliveredTo">The delivery target.</param>
        /// <param name="findingsXml">The findings xml.</param>
        /// <param name="errors">Any errors found.</param>
        /// <returns>The FileDB key. Null if not successful.</returns>
        protected Guid? SaveFindingsXmlToFileDb(UcdDeliveredToEntity deliveredTo, string findingsXml, out List<string> errors)
        {
            Guid guidKey = Guid.NewGuid();
            string fileDbKey = string.Empty;
            switch (deliveredTo)
            {
                case UcdDeliveredToEntity.FannieMae:
                    fileDbKey = FannieMaeUcdDelivery.GetFindingsXmlFileDbKey(guidKey);
                    break;
                case UcdDeliveredToEntity.FreddieMac:
                    fileDbKey = FreddieMacUcdDelivery.GetFindingsXmlFileDbKey(guidKey);
                    break;
                default:
                    throw new UnhandledEnumException(deliveredTo);
            }

            try
            {
                FileDBTools.WriteData(E_FileDB.Normal, fileDbKey, findingsXml);
            }
            catch (CBaseException exception)
            {
                Tools.LogError(exception);
                errors = new List<string>() { exception.UserMessage };
                return null;
            }

            errors = null;
            return guidKey;
        }

        /// <summary>
        /// Saves the findings html to edoc.
        /// </summary>
        /// <param name="deliveredTo">The delivery target.</param>
        /// <param name="deliveryData">The delivery data.</param>
        /// <param name="errors">Any errors found.</param>
        /// <returns>The guid if saved to Edocs. Null otherwise.</returns>
        protected Guid? SaveFindingsHtmlToEdoc(UcdDeliveredToEntity deliveredTo, UcdDeliveryResponseData deliveryData, out List<string> errors)
        {
            string html = string.Empty;
            var deliveredToDescription = deliveredTo.GetDescription();
            switch (deliveredTo)
            {
                case UcdDeliveredToEntity.FannieMae:
                    var fannieData = deliveryData as FannieMaeUcdDeliveryResponseData;
                    html = fannieData.FindingsHtml;
                    break;
                case UcdDeliveredToEntity.FreddieMac:
                    var freddieData = deliveryData as FreddieMacUcdDeliveryResponseData;
                    html = FreddieMacUcdDelivery.ConvertFindingsXmlToHtml(freddieData.DeliveryResults.FindingsXml);
                    break;
                default:
                    throw new UnhandledEnumException(deliveredTo);
            }

            var description = $"{deliveredToDescription} UCD Findings";
            Guid? edocId = AutoSaveDocTypeFactory.SaveHtmlDoc(E_AutoSavePage.UniformClosingDatasetFindings, () => html, this.RequestData.LoanId, this.RequestData.AppId, description, this.RequestData.Principal);
            if (!edocId.HasValue)
            {
                errors = new List<string>() { "Unable to save Findings document to Edocs." };
                return null;
            }

            errors = null;
            return edocId.Value;
        }

        /// <summary>
        /// Extracts the UCD file from the response and saves it to the system.
        /// </summary>
        /// <param name="ucdFileXml">The UCD file xml extracted from the response.</param>
        /// <param name="errors">Errors found during saving.</param>
        /// <returns>The EDoc id if successful, null otherwise.</returns>
        protected Guid? SaveUcdFile(string ucdFileXml, out List<string> errors)
        {
            Guid brokerId = this.RequestData.Principal.BrokerId;

            if (!UcdGenericEDocument.IsUcdStringValid(ucdFileXml, brokerId))
            {
                errors = new List<string>() { "Invalid UCD file." };
                return null;
            }

            Guid loanId = this.RequestData.LoanId;
            Guid appId = this.RequestData.AppId;
            Guid userId = this.RequestData.Principal.UserId;
            bool isPmlUser = this.RequestData.Principal.Type.Equals("P", StringComparison.OrdinalIgnoreCase);

            var ucdXml = System.Xml.Linq.XDocument.Parse(ucdFileXml);
            var ucdPdfFilePath = EDocsUpload.ParseUcdDocFromXml(ucdXml);

            DefaultDocTypeInfo docTypeInfo = AutoSaveDocTypeFactory.GetDocTypeForPageId(E_AutoSavePage.UniformClosingDataset, this.RequestData.Principal.BrokerId, E_EnforceFolderPermissions.False);
            if (docTypeInfo == null || docTypeInfo.DocType == null)
            {
                docTypeInfo = AutoSaveDocTypeFactory.GetUnclassifiedDocTypeInfo(E_AutoSavePage.UniformClosingDataset, this.RequestData.Principal.BrokerId, "Uniform Closing Dataset");
            }

            int docTypeId = docTypeInfo.DocType.Id;
            string description = $"Autosaved: {DateTime.Now.ToShortTimeString()}";

            var pdfEdoc = EDocument.CreateEdoc(ucdPdfFilePath, docTypeId, brokerId, loanId, appId, userId, isPmlUser, E_EDocumentSource.GeneratedDocs, description);
            var pdfEdocId = pdfEdoc.DocumentId;

            string ucdXmlFilePath = TempFileUtils.NewTempFilePath();
            TextFileHelper.WriteString(ucdXmlFilePath, ucdFileXml, false);

            GenericEDocument.CreateGenericDoc(ucdXmlFilePath, E_FileType.UniformClosingDataset, docTypeId, pdfEdocId, brokerId, loanId, appId, userId, description);

            errors = null;
            return pdfEdocId;
        }
    }
}
