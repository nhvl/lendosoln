﻿namespace LendersOffice.Integration.UcdDelivery
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Text;
    using DataAccess;

    /// <summary>
    /// UCD generation utilities class.
    /// </summary>
    public class UcdDeliveryUtilities
    {
        /// <summary>
        /// The generic header for a request log.
        /// </summary>
        public const string RequestLogHeader = "=== UCD DELIVERY REQUEST ===";

        /// <summary>
        /// The generic header for a response log.
        /// </summary>
        public const string ResponseLogHeader = "=== UCD DELIVERY RESPONSE ===";

        /// <summary>
        /// The header for a DocMagic request log.
        /// </summary>
        public const string DocMagicRequestLogHeader = "=== DOCMAGIC UCD DELIVERY REQUEST ===";

        /// <summary>
        /// The header for a DocMagic response log.
        /// </summary>
        public const string DocMagicResponseLogHeader = "=== DOCMAGIC UCD DELIVERY RESPONSE ===";

        /// <summary>
        /// The header for a Fannie Mae UCD delivery request log.
        /// </summary>
        public const string FannieMaeUcdRequestLogHeader = "=== FANNIEMAE UCD DELIVERY REQUEST ===";

        /// <summary>
        /// The header for a Fannie Mae UCD delivery response log.
        /// </summary>
        public const string FannieMaeUcdResponseLogHeader = "=== FANNIEMAE UCD DELIVERY RESPONSE ===";
        
        /// <summary>
        /// The header for a Fannie Mae request.
        /// </summary>
        public const string FannieMaeRequestLogHeader = "=== FANNIE MAE UCD DELIVERY REQUEST ===";

        /// <summary>
        /// The header for a Fannie Mae response.
        /// </summary>
        public const string FannieMaeResponseLogHeader = "=== FANNIE MAE UCD DELIVERY RESPONSE ===";

        /// <summary>
        /// The header for a request passing through the Fannie Mae UCD server.
        /// </summary>
        public const string FannieMaeServerLogHeader = "=== FANNIE MAE UCD DELIVERY SERVER ===";

        /// <summary>
        /// The header for a Freddie Mac request.
        /// </summary>
        public const string FreddieMacRequestLogHeader = "=== FREDDIE MAC UCD DELIVERY REQUEST ===";

        /// <summary>
        /// The header for a Freddie Mac response.
        /// </summary>
        public const string FreddieMacResponseLogHeader = "=== FREDDIE MAC UCD DELIVERY RESPONSE ===";

        /// <summary>
        /// The header for a request passing through the Freddie Mac UCD server.
        /// </summary>
        public const string FreddieMacServerLogHeader = "=== FREDDIE MAC UCD DELIVERY SERVER ===";

        /// <summary>
        /// Logs the payload.
        /// </summary>
        /// <param name="payload">The payload.</param>
        /// <param name="header">The header to use.</param>
        public static void LogPayload(string payload, string header)
        {
            Tools.LogInfo(header + Environment.NewLine + Environment.NewLine + payload);
        }

        /// <summary>
        /// Logs headers sent in a request.
        /// </summary>
        /// <param name="header">The header of the log.</param>
        /// <param name="requestHeaders">The request headers sent.</param>
        public static void LogHeaders(string header, NameValueCollection requestHeaders)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(header);
            stringBuilder.AppendLine();
            foreach (var headerKey in requestHeaders.AllKeys)
            {
                stringBuilder.AppendLine($" Key: {headerKey}, Value: {requestHeaders[headerKey]}");
            }

            Tools.LogInfo(stringBuilder.ToString());
        }
    }
}