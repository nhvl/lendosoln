﻿namespace LendersOffice.Integration.UcdDelivery
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;

    using DataAccess;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Integration.UcdDelivery.FannieMae;
    using LendersOffice.Integration.UcdDelivery.FreddieMac;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Represents a collection of UCD Deliveries.
    /// </summary>
    public class UcdDeliveryCollection : ICollection<UcdDelivery>
    {
        /// <summary>
        /// Stored procedure to retrieve all manual orders.
        /// </summary>
        private static readonly StoredProcedureName UcdDeliveryRetrieveAll = StoredProcedureName.Create("UCD_DELIVERY_RetrieveAll").Value;

        /// <summary>
        /// The loan this collection belongs to.
        /// </summary>
        private Guid loanId;

        /// <summary>
        /// The broker the loan this collection belongs to belongs to.
        /// </summary>
        private Guid brokerId;

        /// <summary>
        /// Dictionary used to store collection items.
        /// </summary>
        private Dictionary<int, UcdDelivery> deliveries = new Dictionary<int, UcdDelivery>();

        /// <summary>
        /// Set of IDs for deliverys that were removed from this collection.
        /// Used on save to delete entries from DB.
        /// </summary>
        private HashSet<int> removedDeliveryIds = new HashSet<int>();

        /// <summary>
        /// Prevents a default instance of the <see cref="UcdDeliveryCollection"/> class from being created.
        /// </summary>
        private UcdDeliveryCollection()
        {
        }

        /// <summary>
        /// Gets the number of items in this collection.
        /// </summary>
        /// <value>The number of items in this collection.</value>
        public int Count
        {
            get
            {
                return this.deliveries.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the collection is read only.
        /// </summary>
        /// <value>A value indicating whether the collection is read only.</value>
        public bool IsReadOnly
        {
            get
            {
                return ((IDictionary<int, UcdDelivery>)this.deliveries).IsReadOnly;
            }
        }

        /// <summary>
        /// Creates an empty UcdDelivery collection object. Be careful with this.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <returns>The UcdDeliveryCollectin object.</returns>
        public static UcdDeliveryCollection CreateEmpty(Guid loanId, Guid brokerId)
        {
            UcdDeliveryCollection collection = new UcdDeliveryCollection();
            collection.loanId = loanId;
            collection.brokerId = brokerId;

            return collection;
        }

        /// <summary>
        /// Converts this object to it's view model representation for use with the UI.
        /// </summary>
        /// <returns>An array of <see cref="UcdDeliveryView"/> objects.</returns>
        public UcdDeliveryView[] ToView()
        {
            return this.Select(d => d.ToView()).ToArray();
        }

        /// <summary>
        /// Gets the <see cref="UcdDelivery"/> item with the given ID if it is contained in the collection.
        /// </summary>
        /// <param name="ucdDeliveryId">UCD Delivery ID.</param>
        /// <returns><see cref="UcdDelivery"/> object with matching ID, or null if no object in the collection matches the ID.</returns>
        public UcdDelivery Get(int ucdDeliveryId)
        {
            UcdDelivery delivery;
            this.deliveries.TryGetValue(ucdDeliveryId, out delivery); // delivery is default value (null) if not found.
            return delivery;
        }

        /// <summary>
        /// Adds multiple items from an enumerable to this collection.
        /// </summary>
        /// <param name="enumerable"><see cref="IEnumerable"/> containing the items to add.</param>
        public void AddMany(IEnumerable<UcdDelivery> enumerable)
        {
            foreach (UcdDelivery item in enumerable)
            {
                this.Add(item);
            }
        }

        /// <summary>
        /// Add item to collection.
        /// </summary>
        /// <param name="item">Item to add to collection.</param>
        public void Add(UcdDelivery item)
        {
            this.deliveries.Add(item.UcdDeliveryId, item);
        }

        /// <summary>
        /// Clears collection.
        /// </summary>
        public void Clear()
        {
            this.deliveries.Clear();
        }

        /// <summary>
        /// Checks if collection contains the item.
        /// </summary>
        /// <param name="item">Item to look for.</param>
        /// <returns>True, if the collection contains the item. False, otherwise.</returns>
        public bool Contains(UcdDelivery item)
        {
            return this.deliveries.ContainsKey(item.UcdDeliveryId);
        }

        /// <summary>
        /// Copy element to array starting at the passed in index.
        /// </summary>
        /// <param name="array">Array to copy to.</param>
        /// <param name="arrayIndex">Index in output array to start copying data to.</param>
        public void CopyTo(UcdDelivery[] array, int arrayIndex)
        {
            List<UcdDelivery> sorted = this.deliveries.Values.ToList();
            sorted.Sort((lhs, rhs) => lhs.UcdDeliveryId.CompareTo(rhs.UcdDeliveryId));
            sorted.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Returns an Enumerator for this collection.
        /// </summary>
        /// <returns>An Enumerator for this collection.</returns>
        /// <remarks>Enumerator is sorted in order entries were made.</remarks>
        public IEnumerator<UcdDelivery> GetEnumerator()
        {
            List<UcdDelivery> sorted = this.deliveries.Values.ToList();
            sorted.Sort((lhs, rhs) => lhs.UcdDeliveryId.CompareTo(rhs.UcdDeliveryId));
            return sorted.GetEnumerator();
        }

        /// <summary>
        /// Remove item from collection.
        /// </summary>
        /// <param name="item">Item to remove from collection.</param>
        /// <returns>True, if the item was found and removed. False, otherwise.</returns>
        public bool Remove(UcdDelivery item)
        {
            return this.Remove(item.UcdDeliveryId);
        }

        /// <summary>
        /// Remove item from collection.
        /// </summary>
        /// <param name="ucdDeliveryId">ID of item to remove from collection.</param>
        /// <returns>True, if the item was found and removed. False, otherwise.</returns>
        public bool Remove(int ucdDeliveryId)
        {
            this.removedDeliveryIds.Add(ucdDeliveryId);
            return this.deliveries.Remove(ucdDeliveryId);
        }

        /// <summary>
        /// Returns an Enumerator for this collection.
        /// </summary>
        /// <returns>An Enumerator for this collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)this.GetEnumerator();
        }

        /// <summary>
        /// Loads <see cref="UcdDeliveryCollection"/> from DB.
        /// </summary>
        /// <param name="loanId">The loan to load the collection from.</param>
        /// <param name="brokerId">The broker this collection belongs to.</param>
        /// <param name="conn">Sql connection.</param>
        /// <param name="trans">Sql transaction context.</param>
        /// <returns>A <see cref="UcdDeliveryCollection"/> object.</returns>
        /// <remarks>
        /// This collection should only be accessed via the loan.
        /// Only load via loan init.
        /// </remarks>
        internal static UcdDeliveryCollection Load(Guid loanId, Guid brokerId, DbConnection conn, DbTransaction trans)
        {
            UcdDeliveryCollection collection = new UcdDeliveryCollection();
            collection.loanId = loanId;
            collection.brokerId = brokerId;

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@BrokerId", brokerId)
            };

            List<ManualUcdDelivery> deliveries = new List<ManualUcdDelivery>();
            using (DataSet ds = StoredProcedureDriverHelper.ExecuteDataSet(conn, trans, UcdDeliveryRetrieveAll, parameters, TimeoutInSeconds.Default))
            {
                // Manual UCD Deliveries.
                DataTableReader reader = ds.Tables[0].CreateDataReader();
                while (reader.Read())
                {
                    collection.Add(new ManualUcdDelivery(reader));
                }

                // Fannie Mae UDC Deliveries.
                reader = ds.Tables[1].CreateDataReader();
                while (reader.Read())
                {
                    collection.Add(new FannieMaeUcdDelivery(reader));
                }

                // Freddie Mac UCD Deliveries.
                reader = ds.Tables[2].CreateDataReader();
                while (reader.Read())
                {
                    collection.Add(new FreddieMacUcdDelivery(reader));
                }
            }

            return collection;
        }

        /// <summary>
        /// Saved this collection to DB.
        /// </summary>
        /// <param name="conn">Sql connection.</param>
        /// <param name="trans">Sql transaction context.</param>
        /// <remarks>
        /// This collection should only be manipulated via the loan.
        /// Only save via loan save.
        /// </remarks>
        internal void Save(DbConnection conn, DbTransaction trans)
        {
            // Save deliveries still in set.
            foreach (UcdDelivery delivery in this.deliveries.Values)
            {
                if (!delivery.IsDirty)
                {
                    continue;
                }

                delivery.Save(conn, trans);
            }

            // Delete removed deliveries.
            // NOTE: Only Manual Delivery Data will be deleted.
            foreach (int id in this.removedDeliveryIds)
            {
                ManualUcdDelivery.Delete(id, conn, trans);
            }

            this.removedDeliveryIds.Clear();
        }
    }
}
