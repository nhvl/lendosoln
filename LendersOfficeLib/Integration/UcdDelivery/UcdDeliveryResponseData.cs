﻿namespace LendersOffice.Integration.UcdDelivery
{
    using Templates;

    /// <summary>
    /// UCD generation and delivery response data.
    /// </summary>
    public class UcdDeliveryResponseData : IResponseData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UcdDeliveryResponseData"/> class.
        /// </summary>
        /// <param name="status">The result status.</param>
        /// <param name="deliveryResults">The results from a delivery request, if any.</param>
        /// <param name="generationResults">The results from a generation request, if any.</param>
        public UcdDeliveryResponseData(UcdDeliveryResultStatus status, UcdDeliveryResults deliveryResults = null, UcdGenerationResults generationResults = null)
        {
            this.ResultStatus = status;
            this.DeliveryResults = deliveryResults;
            this.GenerationResults = generationResults;
        }

        /// <summary>
        /// Gets the result status.
        /// </summary>
        /// <value>The result status.</value>
        public UcdDeliveryResultStatus ResultStatus
        {
            get;
        }

        /// <summary>
        /// Gets the results of a requested UCD generation.
        /// </summary>
        /// <value>The results of a requested UCD generation.</value>
        public UcdGenerationResults GenerationResults
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the results of a requested UCD delivery.
        /// </summary>
        /// <value>The results of a requested UCD delivery.</value>
        public UcdDeliveryResults DeliveryResults
        {
            get;
        }
    }
}
