﻿namespace LendersOffice.Integration.UcdDelivery
{
    using System;
    using System.Runtime.Serialization;
    using Common;
    using DataAccess;
    using EDocs;
    using LendersOffice.Security;

    /// <summary>
    /// Used to represent UCD delivery data in the UI.
    /// </summary>
    [DataContract]
    public class UcdDeliveryView
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UcdDeliveryView"/> class.
        /// </summary>
        /// <param name="ucdDelivery"><see cref="UcdDelivery"/> object to convert.</param>
        public UcdDeliveryView(UcdDelivery ucdDelivery)
        {
            this.UcdDeliveryId = ucdDelivery.UcdDeliveryId;
            this.LoanId = ucdDelivery.LoanId;
            this.BrokerId = ucdDelivery.BrokerId;
            this.UcdDeliveryType = ucdDelivery.UcdDeliveryType;
            this.DeliveredTo = ucdDelivery.DeliveredTo;
            this.CaseFileId = ucdDelivery.CaseFileId;
            this.DateOfDelivery = ucdDelivery.DateOfDelivery;
            this.Note = ucdDelivery.Note;
            this.IncludeInUldd = ucdDelivery.IncludeInUldd;
            this.FileDeliveredDocumentId = ucdDelivery.FileDeliveredDocumentId;
            EDocument document = this.LoadDocument(ucdDelivery.FileDeliveredDocumentId);
            this.CanViewFileDeliveredDocument = document != null;
            this.FileDeliveredDocumentMetadataJSON = document == null ? null : Tools.CreateDocMetaDataTooltipDataAttributeValue(document);
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="UcdDeliveryView"/> class from being created.
        /// Well not really, but style cop requires me to write that.
        /// This constructor is actually used for serialization.
        /// </summary>
        [Newtonsoft.Json.JsonConstructor]
        private UcdDeliveryView()
        {
        }

        /// <summary>
        /// Gets the UCD Delivery ID.
        /// </summary>
        /// <value>UCD Delivery ID.</value>
        [DataMember(Name = "UcdDeliveryId")]
        public int UcdDeliveryId { get; private set; }

        /// <summary>
        /// Gets the loan ID.
        /// </summary>
        /// <value>A loan ID.</value>
        [DataMember(Name = "LoanId")]
        public Guid LoanId { get; private set; }

        /// <summary>
        /// Gets the broker ID.
        /// </summary>
        /// <value>Broker ID.</value>
        [DataMember(Name = "BrokerId")]
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets the UCD Delivery Type.
        /// </summary>
        /// <value>UCD Delivery Type.</value>
        [DataMember(Name = "UcdDeliveryType")]
        public UcdDeliveryType UcdDeliveryType { get; private set; }

        /// <summary>
        /// Gets or sets the entity to which the UCD file would be delivered.
        /// </summary>
        /// <value>The entity to which the UCD file would be delivered.</value>
        [DataMember(Name = "DeliveredTo")]
        public UcdDeliveredToEntity DeliveredTo { get; set; }

        /// <summary>
        /// Gets the DeliveredTo as a string.
        /// </summary>
        /// <value>The DeliveredTo as a friendly string.</value>
        [DataMember(Name = "DeliveredTo_rep")]
        public string DeliveredTo_rep
        {
            get
            {
                return this.DeliveredTo.GetDescription();
            }
        }

        /// <summary>
        /// Gets or sets the Generic EDoc ID for the UCD file.
        /// </summary>
        /// <value>The Generic EDoc ID for the UCD file that was submitted.</value>
        [DataMember(Name = "FileDeliveredDocumentId")]
        public Guid? FileDeliveredDocumentId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the file delivered document can be viewed.
        /// </summary>
        /// <value>Whether the file delivered document can be viewed.</value>
        [DataMember(Name = "CanViewFileDeliveredDocument")]
        public bool CanViewFileDeliveredDocument { get; set; }

        /// <summary>
        /// Gets or sets the document metadata JSON, for use by the doc metadata tooltip.
        /// </summary>
        /// <value>The document metadata JSON.</value>
        [DataMember(Name = "FileDeliveredDocumentMetadataJSON")]
        public string FileDeliveredDocumentMetadataJSON { get; set; }

        /// <summary>
        /// Gets or sets the UCD/DU case file ID.
        /// </summary>
        /// <remarks>Limited to 50 chars.</remarks>
        /// <value>The UCD/DU case file ID.</value>
        [DataMember(Name = "CaseFileId")]
        public string CaseFileId { get; set; }

        /// <summary>
        /// Gets or sets the date the file was delivered.
        /// </summary>
        /// <value>The date the file was delivered.</value>
        public CDateTime DateOfDelivery { get; set; }

        /// <summary>
        /// Gets or sets the date the file was delivered.
        /// </summary>
        /// <value>The date the file was delivered as string.</value>
        [DataMember(Name = "DateOfDelivery")]
        public virtual string DateOfDelivery_rep
        {
            get
            {
                return this.DateOfDelivery.ToStringWithDefaultFormatting();
            }

            set
            {
                this.DateOfDelivery = CDateTime.Create(value, null);
            }
        }

        /// <summary>
        /// Gets or sets various notes about the file delivered.
        /// </summary>
        /// <remarks>Limited to 500 chars.</remarks>
        /// <value>Various notes about the file delivered.</value>
        [DataMember(Name = "Note")]
        public string Note { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the UCD delivery should be included in the ULDD.
        /// </summary>
        /// <value>A boolean indicating whether the UCD delivery should be included in the ULDD.</value>
        /// <remarks>This should only be true for one delivery per loan at any given time.</remarks>
        [DataMember(Name = "IncludeInUldd")]
        public bool IncludeInUldd { get; set; }

        /// <summary>
        /// Gets or sets an ID generated by FannieMae for which batch the UCD was processed in, later used to find and identify the UCD file.
        /// </summary>
        /// <remarks>FannieMae specific field.</remarks>
        /// <value>ID generated by FannieMae for which batch the UCD was processed in.</value>
        [DataMember(Name = "BatchId")]
        public string BatchId { get; set; }

        /// <summary>
        /// Gets or sets the status of the UCD file.
        /// </summary>
        /// <remarks>FannieMae specific field.</remarks>
        /// <value>The status of the UCD file.</value>
        [DataMember(Name = "Status")]
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this is a new UCD Delivery.
        /// </summary>
        /// <value>A value indicating whether this is a new UCD Delivery.</value>
        [DataMember(Name = "IsNew")]
        internal bool IsNew { get; set; }

        /// <summary>
        /// Loads the specified EDoc, returning null if the user lacks permission to view.
        /// </summary>
        /// <param name="fileId">The Edoc file id.</param>
        /// <returns>The <see cref="EDocument"/> instance if it can be viewed; null otherwise.</returns>
        private EDocument LoadDocument(Guid? fileId)
        {
            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            if (fileId.HasValue)
            {
                try
                {
                    return repo.GetDocumentById(fileId.Value);
                }
                catch (NotFoundException)
                {
                    return null;
                }
                catch (AccessDenied)
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
    }
}
