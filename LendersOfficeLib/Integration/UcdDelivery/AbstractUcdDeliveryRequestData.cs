﻿namespace LendersOffice.Integration.UcdDelivery
{
    using System;
    using Integration.DocumentVendor;
    using Integration.Templates;
    using LendersOffice.Security;

    /// <summary>
    /// Request data class.
    /// </summary>
    /// <remarks>Will be moved to another file later on.</remarks>
    public abstract class AbstractUcdDeliveryRequestData : IRequestData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractUcdDeliveryRequestData"/> class.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="appId">The app id.</param>
        /// <param name="closingDisclosureId">The closing disclosure.</param>
        /// <param name="action">The action to be completed by the request.</param>
        /// <param name="principal">The principal.</param>
        protected AbstractUcdDeliveryRequestData(Guid loanId, Guid appId, Guid? closingDisclosureId, UcdDeliveryAction action, AbstractUserPrincipal principal)
        {
            this.LoanId = loanId;
            this.AppId = appId;
            this.ClosingDisclosureId = closingDisclosureId;
            this.Action = action;
            this.Principal = principal;
            this.TransactionId = Guid.NewGuid();
        }

        /// <summary>
        /// Gets the app id.
        /// </summary>
        /// <value>The app id.</value>
        public Guid AppId
        {
            get;
        }

        /// <summary>
        /// Gets the transaction ID.
        /// </summary>
        /// <value>The transaction ID.</value>
        public Guid TransactionId
        {
            get;
        }

        /// <summary>
        /// Gets the closing disclosure ID.
        /// </summary>
        /// <value>The closing disclosure ID.</value>
        public Guid? ClosingDisclosureId
        {
            get;
        }

        /// <summary>
        /// Gets the loan id.
        /// </summary>
        /// <value>The loan id.</value>
        public Guid LoanId
        {
            get;
        }

        /// <summary>
        /// Gets the principal used.
        /// </summary>
        /// <value>The principal used.</value>
        public AbstractUserPrincipal Principal
        {
            get;
        }

        /// <summary>
        /// Gets the request date.
        /// </summary>
        /// <value>The request date.</value>
        public DateTime RequestDate { get; } = DateTime.Now;

        /// <summary>
        /// Gets the action to be completed by the request.
        /// </summary>
        /// <value>The action to be completed by the request.</value>
        public virtual UcdDeliveryAction Action
        {
            get;
        }

        /// <summary>
        /// Gets or sets the delivery target. Null if not delivery.
        /// </summary>
        /// <value>The delivery target.</value>
        public virtual UcdDeliveredToEntity? DeliveryTarget
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the GSE customer id.
        /// </summary>
        /// <value>The GSE customer id.</value>
        public virtual string GseCustomerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the GSE username.
        /// </summary>
        /// <value>The GSE username.</value>
        public virtual string GseUserName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the GSE password.
        /// </summary>
        /// <value>The GSE password.</value>
        public virtual string GsePassword
        {
            get;
            set;
        }

        /// <summary>
        /// Validates the request data.
        /// </summary>
        /// <param name="error">Any errors found.</param>
        /// <returns>True if validated. False otherwise.</returns>
        public virtual bool Validate(out string error)
        {
            if (this.LoanId == Guid.Empty)
            {
                error = "Invalid loan Id";
                return false;
            }

            if (this.Action == UcdDeliveryAction.GenerateAndDeliver || this.Action == UcdDeliveryAction.DeliverExisting ||
                this.Action == UcdDeliveryAction.GetResults)
            {
                if (!this.DeliveryTarget.HasValue || this.DeliveryTarget == UcdDeliveredToEntity.Blank)
                {
                    error = "No delivery target set for delivery request.";
                    return false;
                }

                if (string.IsNullOrEmpty(this.GseUserName) || string.IsNullOrEmpty(this.GsePassword))
                {
                    error = "Invalid GSE credentials.";
                    return false;
                }
            }

            error = string.Empty;
            return true;
        }
    }
}
