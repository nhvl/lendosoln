﻿namespace LendersOffice.Integration.UcdDelivery
{
    using System.ComponentModel;
    using DataAccess;

    /// <summary>
    /// Enum for entities UCD file can be delivered to.
    /// </summary>
    public enum UcdDeliveredToEntity
    {
        /// <summary>
        /// Blank entry.
        /// </summary>
        [Description("N/A") /* For displaying in a sentence */]
        [OrderedDescription("") /* For Dropdowns */]
        Blank = 0,

        /// <summary>
        /// UCD file delivered to Fannie Mae.
        /// </summary>
        [Description("Fannie Mae")]
        FannieMae = 1,

        /// <summary>
        /// UCD file delivered to Freddie Mac.
        /// </summary>
        [Description("Freddie Mac")]
        FreddieMac = 2,
    }
}
