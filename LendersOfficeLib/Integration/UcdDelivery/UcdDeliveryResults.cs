﻿namespace LendersOffice.Integration.UcdDelivery
{
    using System;

    /// <summary>
    /// The results from a UCD delivery request.
    /// </summary>
    public class UcdDeliveryResults
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UcdDeliveryResults"/> class.
        /// </summary>
        /// <param name="findingsXml">The findings xml.</param>
        /// <param name="batchId">The batch id extracted from the delivery xml.</param>
        public UcdDeliveryResults(string findingsXml, string batchId)
        {
            this.FindingsXml = findingsXml;
            this.BatchId = batchId;
        }

        /// <summary>
        /// Gets the raw findings xml.
        /// </summary>
        /// <value>The raw findings xml.</value>
        public string FindingsXml
        {
            get;
        }

        /// <summary>
        /// Gets or sets the findings Edoc id.
        /// </summary>
        /// <value>The findings Edoc id.</value>
        public Guid FindingsEdocId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the findings xml FileDB key.
        /// </summary>
        /// <value>The findings XML FileDB key.</value>
        public Guid FindingsXmlFileDbKey
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the batch id.
        /// </summary>
        /// <value>The batch id.</value>
        public string BatchId
        {
            get;
        }
    }
}
