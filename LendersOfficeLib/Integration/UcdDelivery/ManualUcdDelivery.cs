﻿namespace LendersOffice.Integration.UcdDelivery
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Represents manually entered UCD delivery data.
    /// </summary>
    public class ManualUcdDelivery : UcdDelivery
    {
        /// <summary>
        /// Stored procedure to delete a manual order.
        /// </summary>
        private static readonly StoredProcedureName UcdDeliveryDeleteManual = StoredProcedureName.Create("UCD_DELIVERY_DeleteManual").Value;

        /// <summary>
        /// Initializes a new instance of the <see cref="ManualUcdDelivery"/> class.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="brokerId">Broker ID.</param>
        public ManualUcdDelivery(Guid loanId, Guid brokerId)
            : base(loanId, brokerId)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ManualUcdDelivery"/> class from a view model object.
        /// </summary>
        /// <param name="view">UI view model object.</param>
        public ManualUcdDelivery(UcdDeliveryView view)
            : base(view)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ManualUcdDelivery"/> class.
        /// </summary>
        /// <param name="reader">Data reader.</param>
        protected internal ManualUcdDelivery(IDataReader reader)
            : base(reader)
        {
        }

        /// <summary>
        /// Gets the UCD Delivery Type.
        /// </summary>
        /// <value>UCD Delivery Type.</value>
        public override UcdDeliveryType UcdDeliveryType => UcdDeliveryType.Manual;

        /// <summary>
        /// Retrieves all manual delivery rows.
        /// </summary>
        /// <param name="ucdDeliveryId">The loan id.</param>.
        /// <param name="conn">Sql connection.</param>
        /// <param name="trans">Sql transaction context.</param>
        internal static void Delete(int ucdDeliveryId, DbConnection conn, DbTransaction trans)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@UcdDeliveryId", ucdDeliveryId),
            };

            StoredProcedureDriverHelper.ExecuteNonQuery(conn, trans, UcdDeliveryDeleteManual, parameters, TimeoutInSeconds.Default);
        }
    }
}
