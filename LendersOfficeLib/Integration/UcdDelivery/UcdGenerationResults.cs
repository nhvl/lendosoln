﻿namespace LendersOffice.Integration.UcdDelivery
{
    using System;

    /// <summary>
    /// The results from a UCD Generation request.
    /// </summary>
    public class UcdGenerationResults
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UcdGenerationResults"/> class.
        /// </summary>
        /// <param name="ucdFileXml">The UCD file XML.</param>
        public UcdGenerationResults(string ucdFileXml)
        {
            this.UcdFileXml = ucdFileXml;
        }

        /// <summary>
        /// Gets the UCD file XML.
        /// </summary>
        /// <value>The UCD file XML.</value>
        public string UcdFileXml
        {
            get;
        }

        /// <summary>
        /// Gets or sets the Edoc ID for the UCD file.
        /// </summary>
        /// <value>The UCD file Edoc ID.</value>
        public Guid UcdFileEdocId
        {
            get;
            set;
        }
    }
}
