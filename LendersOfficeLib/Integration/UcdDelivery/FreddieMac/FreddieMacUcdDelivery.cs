﻿namespace LendersOffice.Integration.UcdDelivery.FreddieMac
{
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Class for all Freddie Mac UCD delivery rows.
    /// </summary>
    public class FreddieMacUcdDelivery : UcdDelivery
    {
        /// <summary>
        /// Freddie Mac 'sch' namespace.
        /// </summary>
        public const string FreddieMacSchNamespace = @"http://www.freddiemac.com/lcla/LCLALoanEvaluationService/schemas";

        /// <summary>
        /// Freddie Mac 'lclaln' namespace.
        /// </summary>
        public const string FreddieMacLclalnNamespace = @"http://lclaloanresults.freddiemac.com/schema/types";

        /// <summary>
        /// Stored procedure to create a Freddie Mac UCD delivery item.
        /// </summary>
        private static readonly StoredProcedureName UcdDeliveryFreddieMacCreate = StoredProcedureName.Create("UCD_DELIVERY_FREDDIE_MAC_Create").Value;

        /// <summary>
        /// Suffix for the findings xml FileDB key.
        /// </summary>
        private static readonly string FreddieMacFindingsFileDBKey = "_FreddieMacFindingsXml";

        /// <summary>
        /// The findings xml loaded from file db.
        /// </summary>
        private Lazy<string> findingsXml;

        /// <summary>
        /// The findings HTML.
        /// </summary>
        private Lazy<string> findingsHtml;

        /// <summary>
        /// The findings XML FileDb key.
        /// </summary>
        private Guid? findingsXmlFileDbKey;

        /// <summary>
        /// The batch id.
        /// </summary>
        private string batchId;

        /// <summary>
        /// The Freddie Mac status.
        /// </summary>
        private FreddieMacUcdDeliveryStatus ucdRequirementStatus;

        /// <summary>
        /// The findings edoc id.
        /// </summary>
        private Guid? findingsDocumentId;

        /// <summary>
        /// Initializes a new instance of the <see cref="FreddieMacUcdDelivery"/> class.
        /// </summary>
        /// <param name="reader">Data reader.</param>
        protected internal FreddieMacUcdDelivery(IDataReader reader)
            : base(reader)
        {
            this.batchId = (string)reader["BatchId"];
            this.ucdRequirementStatus = (FreddieMacUcdDeliveryStatus)reader["UcdRequirementStatus"];
            this.findingsDocumentId = reader["FindingsDocumentId"] == DBNull.Value ? (Guid?)null : (Guid)reader["FindingsDocumentId"];
            this.FindingsXmlFileDbKey = reader["FindingsXmlFileDbKey"] == DBNull.Value ? (Guid?)null : (Guid)reader["FindingsXmlFileDbKey"];

            // The FindingsXmlFileDbKey property will trigger the dirty bit but this is not dirty since it just loaded from a reader.
            this.IsDirty = false;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FreddieMacUcdDelivery"/> class.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="brokerId">Broker ID.</param>
        private FreddieMacUcdDelivery(Guid loanId, Guid brokerId)
            : base(loanId, brokerId)
        {
        }
        
        /// <summary>
        /// Gets the UCD delivery type.
        /// </summary>
        /// <value>The UCD delivery type.</value>
        public override UcdDeliveryType UcdDeliveryType => UcdDeliveryType.FreddieMacUcdDelivery;   

        /// <summary>
        /// Gets the batch id.
        /// </summary>
        /// <value>The batch id.</value>
        public string BatchId
        {
            get
            {
                return this.batchId;
            }

            private set
            {
                this.batchId = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>The status.</value>
        public FreddieMacUcdDeliveryStatus UcdRequirementStatus
        {
            get
            {
                return this.ucdRequirementStatus;
            }

            private set
            {
                this.ucdRequirementStatus = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets the findings edoc id.
        /// </summary>
        /// <value>The findings edoc id.</value>
        public Guid? FindingsDocumentId
        {
            get
            {
                return this.findingsDocumentId;
            }

            private set
            {
                this.findingsDocumentId = value;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets the findings xml fileDB key.
        /// </summary>
        /// <value>The findings FileDB key.</value>
        public Guid? FindingsXmlFileDbKey
        {
            get
            {
                return this.findingsXmlFileDbKey;
            }

            private set
            {
                this.findingsXmlFileDbKey = value;
                if (!this.findingsXmlFileDbKey.HasValue)
                {
                    this.findingsXml = new Lazy<string>(() => null);
                }
                else
                {
                    this.findingsXml = new Lazy<string>(() => FileDBTools.ReadDataText(E_FileDB.Normal, GetFindingsXmlFileDbKey(this.findingsXmlFileDbKey.Value)));
                }

                this.findingsHtml = null;
                this.IsDirty = true;
            }
        }

        /// <summary>
        /// Gets the findings html.
        /// </summary>
        /// <value>The findings html.</value>
        public string FindingsHtml
        {
            get
            {
                if (this.findingsHtml == null)
                {
                    var xml = this.findingsXml.Value;
                    if (string.IsNullOrEmpty(xml))
                    {
                        this.findingsHtml = new Lazy<string>(() => null);
                    }
                    else
                    {
                        this.findingsHtml = new Lazy<string>(() => ConvertFindingsXmlToHtml(xml));
                    }
                }

                return this.findingsHtml.Value;
            }
        }

        /// <summary>
        /// Converts a set of findings XML into the HTML representation used by LendingQB.
        /// </summary>
        /// <param name="findingsXml">The findings XML.</param>
        /// <returns>The HTML for the specified findings.</returns>
        public static string ConvertFindingsXmlToHtml(string findingsXml)
        {
            var xmlReader = System.Xml.XmlReader.Create(new System.IO.StringReader(findingsXml));
            if (xmlReader.ReadToDescendant("EvaluationResults", "http://www.freddiemac.com/lcla/LCLALoanEvaluationService/schemas")
                && xmlReader.GetAttribute("Version") == "1.0")
            {
                return Conversions.FreddieMacXmlTransform.LoanClosingAdvisor.Version1Dot0.FeedbackCertificateDetailsXslt.TransformXmlToHtml(findingsXml);
            }

            return Conversions.FreddieMacXmlTransform.LoanClosingAdvisor.Version2Dot1.FeedbackCertificateDetailsXslt.TransformXmlToHtml(findingsXml);
        }

        /// <summary>
        /// Creates the findings xml FileDB key given a guid key.
        /// </summary>
        /// <param name="key">A guid key.</param>
        /// <returns>The full FileDB key.</returns>
        public static string GetFindingsXmlFileDbKey(Guid key)
        {
            return key.ToString("N") + FreddieMacFindingsFileDBKey;
        }

        /// <summary>
        /// Creates a Freddie UCD delivery item from the response data and request data.
        /// </summary>
        /// <param name="responseData">The response data.</param>
        /// <param name="requestData">The request data.</param>
        /// <returns>The item if successful, false otherwise.</returns>
        public static FreddieMacUcdDelivery Create(FreddieMacUcdDeliveryResponseData responseData, AbstractUcdDeliveryRequestData requestData)
        {
            if (responseData == null || requestData == null)
            {
                return null;
            }

            var deliveryItem = new FreddieMacUcdDelivery(requestData.LoanId, requestData.Principal.BrokerId);
            deliveryItem.BatchId = responseData.DeliveryResults.BatchId;
            deliveryItem.CaseFileId = responseData.TransactionId;
            deliveryItem.UcdRequirementStatus = responseData.Status;
            deliveryItem.DateOfDelivery = CDateTime.Create(requestData.RequestDate);
            deliveryItem.DeliveredTo = UcdDeliveredToEntity.FreddieMac;
            deliveryItem.FileDeliveredDocumentId = responseData.UcdEdocId ?? responseData.GenerationResults?.UcdFileEdocId;
            deliveryItem.FindingsDocumentId = responseData.DeliveryResults.FindingsEdocId;
            deliveryItem.FindingsXmlFileDbKey = responseData.DeliveryResults.FindingsXmlFileDbKey;

            return deliveryItem;
        }

        /// <summary>
        /// Creates a viewmodel out of this object.
        /// </summary>
        /// <returns>The viewmodel of this object.</returns>
        public override UcdDeliveryView ToView()
        {
            var viewModel = base.ToView();
            viewModel.BatchId = this.BatchId;
            viewModel.Status = this.UcdRequirementStatus.GetDescription();

            return viewModel;
        }

        /// <summary>
        /// Performs additional saves in the transaction.
        /// </summary>
        /// <param name="conn">Sql connection.</param>
        /// <param name="trans">Sql transaction context.</param>
        /// <param name="isNew">Whether this is a new delivery item or not.</param>
        protected override void PerformAdditionalSave(DbConnection conn, DbTransaction trans, bool isNew)
        {
            if (!isNew)
            {
                // Can't update this system generated item.
                return;
            }

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@UcdDeliveryId", this.UcdDeliveryId),
                new SqlParameter("BatchId", this.BatchId),
                new SqlParameter("@UcdRequirementStatus", this.UcdRequirementStatus),
                new SqlParameter("@FindingsDocumentId", this.FindingsDocumentId),
                new SqlParameter("@FindingsXmlFileDbKey", this.FindingsXmlFileDbKey)
            };

            StoredProcedureDriverHelper.ExecuteNonQuery(conn, trans, UcdDeliveryFreddieMacCreate, parameters, TimeoutInSeconds.Default);
        }
    }
}
