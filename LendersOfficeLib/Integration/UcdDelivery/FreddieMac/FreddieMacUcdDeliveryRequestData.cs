﻿namespace LendersOffice.Integration.UcdDelivery.FreddieMac
{
    using System;
    using System.Collections.Generic;
    using Admin;
    using DataAccess;
    using EDocs;
    using Security;

    /// <summary>
    /// Contains the data necessary for an "evaluateLoan" request to Loan Closing Advisor.
    /// </summary>
    public class FreddieMacUcdDeliveryRequestData : AbstractUcdDeliveryRequestData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FreddieMacUcdDeliveryRequestData"/> class.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="appId">The app id.</param>
        /// <param name="closingDisclosureId">The closing disclosure.</param>
        /// <param name="ucdEdoc">The ucd document contents as a lazy-loaded string.</param>
        /// <param name="principal">The principal.</param>
        /// <param name="ucdDocumentId">The UCD Document ID for loading from.</param>
        /// <param name="relationshipWithFreddie">The customer's relationship with Freddie Mac.</param>
        /// <param name="correlationId">An optional correlation ID for identifying a communication with Freddie Mac.</param>
        public FreddieMacUcdDeliveryRequestData(Guid loanId, Guid appId, Guid? closingDisclosureId, Lazy<string> ucdEdoc, AbstractUserPrincipal principal, Guid ucdDocumentId, LenderRelationshipWithFreddieMac relationshipWithFreddie, string correlationId = null)
            : base(loanId, appId, closingDisclosureId, UcdDeliveryAction.DeliverExisting, principal)
        {
            this.LoanClosingAdvisorRequest = new LoanClosingAdvisorLoanEvaluationRequest
            {
                RelationshipWithFreddie = relationshipWithFreddie,
                CorrelationId = correlationId,
                UcdFile = ucdEdoc
            };
            this.UcdEdocId = ucdDocumentId;
        }

        /// <summary>
        /// Gets the action associated with the request.
        /// </summary>
        public override UcdDeliveryAction Action => UcdDeliveryAction.DeliverExisting;

        /// <summary>
        /// Gets the delivery target.
        /// </summary>
        /// <value>Always Freddie Mac.</value>
        public override UcdDeliveredToEntity? DeliveryTarget => UcdDeliveredToEntity.FreddieMac;

        /// <summary>
        /// Gets or sets the customer ID, which Freddie Mac calls the seller identifier.
        /// </summary>
        public override string GseCustomerId
        {
            get
            {
                return this.LoanClosingAdvisorRequest.SellerIdentifier;
            }

            set
            {
                this.LoanClosingAdvisorRequest.SellerIdentifier = value;
            }
        }

        /// <summary>
        /// Gets or sets the Freddie Mac system user name.
        /// </summary>
        public override string GseUserName
        {
            get
            {
                return this.LoanClosingAdvisorRequest.FreddieUserName;
            }

            set
            {
                this.LoanClosingAdvisorRequest.FreddieUserName = value;
            }
        }

        /// <summary>
        /// Gets or sets the Freddie Mac system password.
        /// </summary>
        public override string GsePassword
        {
            get
            {
                return this.LoanClosingAdvisorRequest.FreddiePassword;
            }

            set
            {
                this.LoanClosingAdvisorRequest.FreddiePassword = value;
            }
        }

        /// <summary>
        /// Gets or sets the EDocument ID of the UCD Xml document.
        /// </summary>
        public Guid UcdEdocId { get; set; }

        /// <summary>
        /// Gets or sets the actual data that will go into the request transmission.
        /// </summary>
        public LoanClosingAdvisorLoanEvaluationRequest LoanClosingAdvisorRequest { get; set; }

        /// <summary>
        /// Creates a new Request Data object for submitting to Freddie Mac.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="appId">The app id.</param>
        /// <param name="closingDisclosure">The closing disclosure.</param>
        /// <param name="user">The user object.</param>
        /// <returns>A new instance of the <see cref="FreddieMacUcdDeliveryRequestData"/> class ready for placing a request.</returns>
        public static FreddieMacUcdDeliveryRequestData CreateFreddieMacRequestData(Guid loanId, Guid appId, ClosingDisclosureDates closingDisclosure, AbstractUserPrincipal user)
        {
            var loadUcdDocumentLazy = new Lazy<string>(() => System.IO.File.ReadAllText(EDocumentRepository.GetUserRepository(user).GetGenericDocumentById(closingDisclosure.UcdDocument).GetContentPath()));
            return new FreddieMacUcdDeliveryRequestData(loanId, appId, closingDisclosure.UniqueId, loadUcdDocumentLazy, user, closingDisclosure.UcdDocument, user.BrokerDB.RelationshipWithFreddieMac);
        }
    }
}
