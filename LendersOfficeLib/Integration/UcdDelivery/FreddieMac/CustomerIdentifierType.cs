﻿namespace LendersOffice.Integration.UcdDelivery.FreddieMac
{
    /// <summary>
    /// Represents the type of relationship identified between Freddie Mac and their customer.
    /// </summary>
    public enum CustomerIdentifierType
    {
        /// <summary>
        /// Seller relationship.
        /// </summary>
        Seller,

        /// <summary>
        /// Correspondent relationship.
        /// </summary>
        Correspondent
    }
}
