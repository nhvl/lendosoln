﻿namespace LendersOffice.Integration.UcdDelivery.FreddieMac
{
    using System.ComponentModel;

    /// <summary>
    /// Enum representing the status from a Freddie UCD delivery response.
    /// </summary>
    public enum FreddieMacUcdDeliveryStatus
    {
        /// <summary>
        /// Satisfied response.
        /// </summary>
        [Description("Satisfied")]
        Satisfied = 0,
        
        /// <summary>
        /// Not satisfied response.
        /// </summary>
        [Description("Not Satisfied")]
        NotSatisfied = 1,

        /// <summary>
        /// Satisfied without closing disclosure PDF response.
        /// </summary>
        [Description("Satisfied (Without Closing Disclosure)")]
        SatisfiedWithoutClosingDisclosure = 2
    }
}
