﻿namespace LendersOffice.Integration.UcdDelivery.FreddieMac
{
    using System;
    using System.Collections.Generic;
    using Admin;
    using LendersOffice.Integration.Templates;
    using Security;

    /// <summary>
    /// Contains the data necessary for an "evaluateLoan" request to Loan Closing Advisor.
    /// </summary>
    public class LoanClosingAdvisorLoanEvaluationRequest
    {
        /// <summary>
        /// Gets or sets the user's Freddie Mac user name.
        /// </summary>
        public string FreddieUserName { get; set; }

        /// <summary>
        /// Gets or sets the user's Freddie Mac password.
        /// </summary>
        public string FreddiePassword { get; set; }

        /// <summary>
        /// Gets or sets the correlation ID to identify the transaction with Freddie Mac.
        /// </summary>
        public string CorrelationId { get; set; }

        /// <summary>
        /// Gets or sets the lender's relationship with Freddie Mac (Seller or Correspondent).
        /// </summary>
        public LenderRelationshipWithFreddieMac RelationshipWithFreddie { get; set; }

        /// <summary>
        /// Gets or sets the seller identifier for the user.
        /// </summary>
        public string SellerIdentifier { get; set; }

        /// <summary>
        /// Gets or sets the UCD File MISMO XML as a string.
        /// </summary>
        public Lazy<string> UcdFile { get; set; }
    }
}
