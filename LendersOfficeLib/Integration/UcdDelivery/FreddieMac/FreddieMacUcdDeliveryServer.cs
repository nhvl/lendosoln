﻿namespace LendersOffice.Integration.UcdDelivery.FreddieMac
{
    using System;
    using System.Collections.Generic;
    using Drivers.HttpRequest;
    using LendersOffice.Integration.Templates;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.HttpRequest;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Provides methods for placing requests and getting responses from Freddie Mac's Loan Closing Advisor system.
    /// </summary>
    public class FreddieMacUcdDeliveryServer : AbstractIntegrationServer
    {
        /// <summary>
        /// The soap action for Loan Closing Advisor's "evaluateLoan" service.
        /// </summary>
        private const string EvaluateLoanSoapAction = "http://www.freddiemac.com/lcla/LCLALoanEvaluationService/evaluateLoan";

        /// <summary>
        /// The url to post to for requests to the production environment.
        /// </summary>
        private const string LclaProductionUrl = "https://lassvcs.freddiemac.com/eso/EWSG_LCLALoanEvaluationServiceV1.0";

        /// <summary>
        /// The url to post to for requests to the test environment.
        /// </summary>
        private const string LclaTestUrl = "https://las-ewscte.fmrei.com/eso/EWSG_LCLALoanEvaluationServiceV1.0";

        /// <summary>
        /// Initializes a new instance of the <see cref="FreddieMacUcdDeliveryServer"/> class.
        /// </summary>
        /// <param name="useTestEnvironment">Whether the communications should target the LCLA test environment (true) or production (false).</param>
        public FreddieMacUcdDeliveryServer(bool useTestEnvironment = false)
        {
            this.UseTestEnvironment = useTestEnvironment;
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use LCLA's test environment.
        /// </summary>
        public bool UseTestEnvironment { get; set; }

        /// <summary>
        /// Gets or sets a list of error messages.
        /// </summary>
        private List<string> Errors { get; set; } = new List<string>();

        /// <summary>
        /// Places a request to the configured LCLA environment's evaluateLoan operation.
        /// </summary>
        /// <param name="orderXml">The SOAP xml content to POST to Loan Closing Advisor.</param>
        /// <returns>The response body from Freddie Mac.</returns>
        public override string PlaceRequest(string orderXml)
        {
            HttpRequestOptions options = new HttpRequestOptions
            {
                MimeType = MimeType.Text.Xml,
                PostData = new StringContent(orderXml),
                Method = HttpMethod.Post
            };
            options.AddHeader("SOAPAction", EvaluateLoanSoapAction);

            try
            {
                WebRequestHelper.ExecuteCommunication(LqbAbsoluteUri.Create(this.UseTestEnvironment ? LclaTestUrl : LclaProductionUrl).Value, options);
                return options.ResponseBody;
            }
            catch (ServerException se)
            {
                if (se.InnerException is System.Net.WebException)
                {
                    var statusCode = ((se.InnerException as System.Net.WebException).Response as System.Net.HttpWebResponse)?.StatusCode;
                    if (statusCode == System.Net.HttpStatusCode.InternalServerError)
                    {
                        this.Errors.Add("Internal Server Error. Credentials may be invalid.");
                    }
                    else if (statusCode == System.Net.HttpStatusCode.Unauthorized)
                    {
                        this.Errors.Add("Unauthorized. Credentials may be invalid.");
                    }
                    else if (statusCode == System.Net.HttpStatusCode.Forbidden)
                    {
                        this.Errors.Add("Forbidden. Make sure that your credentials are correct and for system-to-system (S2S).");
                    }
                    else
                    {
                        this.Errors.Add(se.InnerException.Message);
                    }
                }
                else
                {
                    this.Errors.Add(se.Message);
                    DataAccess.Tools.LogError(se);
                }

                return null;
            }
        }

        /// <summary>
        /// Gets a list of errors that occurred during communication with the LCLA server.
        /// </summary>
        /// <returns>A list of error messages.</returns>
        public override List<string> GetErrors()
        {
            return this.Errors;
        }
    }
}
