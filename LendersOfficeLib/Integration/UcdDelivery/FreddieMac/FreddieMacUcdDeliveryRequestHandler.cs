﻿namespace LendersOffice.Integration.UcdDelivery.FreddieMac
{
    using System;
    using System.Collections.Generic;
    using Conversions.Templates;
    using ObjLib.Conversions.UcdDelivery.FreddieMac;

    /// <summary>
    /// Handles UCD delivery requests to Freddie Mac's Loan Closing Advisor system.
    /// </summary>
    public class FreddieMacUcdDeliveryRequestHandler : AbstractUcdDeliveryRequestHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FreddieMacUcdDeliveryRequestHandler"/> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        public FreddieMacUcdDeliveryRequestHandler(AbstractUcdDeliveryRequestData requestData)
            : base(requestData)
        {
            this.RequestProvider = new LoanClosingAdvisorRequestProvider(this.RequestData);
            this.Server = new FreddieMacUcdDeliveryServer(useTestEnvironment: UcdLenderService.RetrieveByBrokerId(requestData.Principal.BrokerId).IsFreddieMacTesting);
        }

        /// <summary>
        /// Gets the Freddie Mac request data.
        /// </summary>
        protected new FreddieMacUcdDeliveryRequestData RequestData => (FreddieMacUcdDeliveryRequestData)base.RequestData;

        /// <summary>
        /// Gets the response provider.
        /// </summary>
        protected new FreddieMacUcdDeliveryResponseProvider ResponseProvider => (FreddieMacUcdDeliveryResponseProvider)base.ResponseProvider;

        /// <summary>
        /// Generates a Freddie response provider from the response payload.
        /// </summary>
        /// <param name="responsePayload">The response payload.</param>
        /// <returns>The response provider.</returns>
        protected override AbstractResponseProvider GenerateResponseProvider(string responsePayload)
        {
            return new FreddieMacUcdDeliveryResponseProvider(responsePayload);
        }

        /// <summary>
        /// Processes the response data from Freddie Mac.
        /// </summary>
        /// <param name="deliveryItem">A delivery item generated from the response.</param>
        /// <param name="errors">Any errors occurring during the process.</param>
        /// <returns>A generation result status.</returns>
        protected override UcdDeliveryResultStatus ProcessResponse(out UcdDelivery deliveryItem, out List<string> errors)
        {
            var responseData = (FreddieMacUcdDeliveryResponseData)this.ResponseProvider.ResponseData;
            var findingsXmlFileDbKey = this.SaveFindingsXmlToFileDb(this.RequestData.DeliveryTarget.Value, responseData.DeliveryResults.FindingsXml, out errors);
            if (!findingsXmlFileDbKey.HasValue)
            {
                deliveryItem = null;
                return UcdDeliveryResultStatus.Error;
            }

            responseData.DeliveryResults.FindingsXmlFileDbKey = findingsXmlFileDbKey.Value;
            var edocId = this.SaveFindingsHtmlToEdoc(this.RequestData.DeliveryTarget.Value, responseData, out errors);
            if (!edocId.HasValue)
            {
                deliveryItem = null;
                return UcdDeliveryResultStatus.Error;
            }

            responseData.DeliveryResults.FindingsEdocId = edocId.Value;

            responseData.UcdEdocId = this.RequestData.UcdEdocId;
            deliveryItem = this.CreateUcdDeliveryItem(responseData, out errors);
            if (deliveryItem == null)
            {
                return UcdDeliveryResultStatus.Error;
            }

            var dataLoan = DataAccess.CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.RequestData.LoanId, typeof(FreddieMacUcdDeliveryRequestHandler));
            dataLoan.InitSave(Constants.ConstAppDavid.SkipVersionCheck);
            dataLoan.sUcdDeliveryCollection.Add(deliveryItem);
            dataLoan.Save();
            return UcdDeliveryResultStatus.Delivered;
        }
    }
}
