﻿namespace LendersOffice.Integration.UcdDelivery.FreddieMac
{
    using System;

    /// <summary>
    /// The Freddie Mac UCD generation response data.
    /// </summary>
    public class FreddieMacUcdDeliveryResponseData : UcdDeliveryResponseData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FreddieMacUcdDeliveryResponseData"/> class.
        /// </summary>
        /// <param name="resultStatus">The result status.</param>
        /// <param name="status">The Freddie status.</param>
        /// <param name="transactionId">The transaction id.</param>
        /// <param name="ucdEdocId">The EDoc ID of the delivered UCD file, or null if one wasn't explicitly delivered.</param>
        /// <param name="deliveryResults">Results from a delivery request.</param>
        /// <param name="generationResults">Results from a generation request.</param>
        public FreddieMacUcdDeliveryResponseData(UcdDeliveryResultStatus resultStatus, FreddieMacUcdDeliveryStatus status, string transactionId, Guid? ucdEdocId = null, UcdDeliveryResults deliveryResults = null, UcdGenerationResults generationResults = null)
            : base(resultStatus, deliveryResults, generationResults)
        {
            this.Status = status;
            this.TransactionId = transactionId;
            this.UcdEdocId = ucdEdocId;
        }

        /// <summary>
        /// Gets Freddie status message.
        /// </summary>
        /// <value>The Freddie satus message.</value>
        public FreddieMacUcdDeliveryStatus Status { get; }

        /// <summary>
        /// Gets the transaction id.
        /// </summary>
        /// <value>The transaction id.</value>
        public string TransactionId { get; }

        /// <summary>
        /// Gets or sets the Edoc Id for the UCD file.
        /// </summary>
        /// <remarks>
        /// The GenerationResult property technically contains the UcdEdocId but Freddie Mac responses don't need to Generate in order to contain a UCD file.
        /// Thus we have this property here to hold the Ucd Edoc id rather than adding in some fake GenerationResult object.
        /// </remarks>
        public Guid? UcdEdocId { get; set; }
    }
}
