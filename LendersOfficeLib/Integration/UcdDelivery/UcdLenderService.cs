﻿namespace LendersOffice.Integration.UcdDelivery
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using Admin;
    using Common;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Represents the lender level configuration to do with whether to allow certain paths of UCD delivery.
    /// </summary>
    public class UcdLenderService
    {
        /// <summary>
        /// Lazy-loads and stores the associated BrokerDB.
        /// </summary>
        private Lazy<BrokerDB> broker;

        /// <summary>
        /// Initializes a new instance of the <see cref="UcdLenderService"/> class.
        /// </summary>
        /// <param name="brokerId">The broker ID of the associated broker.</param>
        public UcdLenderService(Guid brokerId)
        {
            this.BrokerId = brokerId;
            this.broker = new Lazy<BrokerDB>(() => BrokerDB.RetrieveById(this.BrokerId));
            this.LoadFromDB();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UcdLenderService"/> class.
        /// </summary>
        /// <param name="broker">The broker whose UCD settings to load.</param>
        public UcdLenderService(BrokerDB broker)
        {
            this.BrokerId = broker.BrokerID;
            this.broker = new Lazy<BrokerDB>(() => broker);
            this.LoadFromDB();
        }

        /// <summary>
        /// Gets the Broker ID corresponding to the lender for which this is the configuration.
        /// </summary>
        /// <value>Broker ID.</value>
        public Guid BrokerId { get; }

        /// <summary>
        /// Gets the broker associated with this lender configuration.
        /// </summary>
        /// <value>The associated broker.</value>
        public BrokerDB Broker
        {
            get
            {
                return this.broker.Value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether UCD delivery should be allowed through the LQB integration to Fannie Mae.
        /// </summary>
        /// <value>Whether to allow UCD delivery directly to Fannie Mae.</value>
        public bool AllowLqbDeliveryToFannieMae { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the direct LQB -> Fannie Mae integration should use Fannie Mae's test environment for this lender.
        /// </summary>
        /// <value>Whether to use the Fannie Mae test environment.</value>
        public bool IsFannieMaeTesting { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether UCD delivery should be allowed through the LQB integration to Freddie Mac.
        /// </summary>
        /// <value>Whether to allow UCD delivery directly to Freddie Mac.</value>
        public bool AllowLqbDeliveryToFreddieMac { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the direct LQB -> Freddie Mac integration should use Freddie Mac's test environment for this lender.
        /// </summary>
        /// <value>Whether to use the Freddie Mac test environment.</value>
        public bool IsFreddieMacTesting { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether UCD delivery should be allowed through DocMagic's integration to Fannie Mae.
        /// </summary>
        /// <value>Whether to allow UCD delivery to Fannie Mae through DocMagic.</value>
        public bool AllowDocMagicDeliveryToFannieMae { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether UCD delivery should be allowed through DocMagic's integration to Freddie Mac.
        /// </summary>
        /// <value>Whether to allow UCD delivery to Freddie Mac through DocMagic.</value>
        public bool AllowDocMagicDeliveryToFreddieMac { get; set; }

        /// <summary>
        /// Gets or sets a value describing the relationship between the lender and Freddie Mac.
        /// </summary>
        /// <value>Lender's relationship to Freddie Mac.</value>
        public LenderRelationshipWithFreddieMac RelationshipWithFreddieMac
        {
            get
            {
                return this.Broker.RelationshipWithFreddieMac;
            }

            set
            {
                this.Broker.RelationshipWithFreddieMac = value;
            }
        }

        /// <summary>
        /// Retrieves the configuration by BrokerId.
        /// </summary>
        /// <param name="brokerId">The broker ID to retrieve by.</param>
        /// <returns>The <see cref="UcdLenderService"/> corresponding to the BrokerId.</returns>
        public static UcdLenderService RetrieveByBrokerId(Guid brokerId)
        {
            try
            {
                return new UcdLenderService(brokerId);
            }
            catch (DeveloperException)
            {
                return null;
            }
            catch (SqlException)
            {
                return null;
            }
        }

        /// <summary>
        /// Save the UCD service configuration to the database.
        /// Also saves the associated <see cref="BrokerDB"/>. 
        /// </summary>
        public void Save()
        {
            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@AllowLqbDeliveryToFannieMae", this.AllowLqbDeliveryToFannieMae),
                new SqlParameter("@IsFannieMaeTesting", this.IsFannieMaeTesting),
                new SqlParameter("@AllowLqbDeliveryToFreddieMac", this.AllowLqbDeliveryToFreddieMac),
                new SqlParameter("@IsFreddieMacTesting", this.IsFreddieMacTesting),
                new SqlParameter("@AllowDocMagicDeliveryToFannieMae", this.AllowDocMagicDeliveryToFannieMae),
                new SqlParameter("@AllowDocMagicDeliveryToFreddieMac", this.AllowDocMagicDeliveryToFreddieMac),
            };

            using (var conn = DbAccessUtils.GetConnection(this.BrokerId))
            {
                var modifiedRows = StoredProcedureDriverHelper.ExecuteNonQuery(
                    conn,
                    transaction: null,
                    procedureName: StoredProcedureName.Create("BROKER_UCD_CONFIG_Save").ForceValue(),
                    parameters: parameters,
                    timeout: TimeoutInSeconds.Default);
                if (modifiedRows.Value > 0)
                {
                    this.Broker.RelationshipWithFreddieMac = this.RelationshipWithFreddieMac;
                    this.Broker.Save();
                    this.broker = new Lazy<BrokerDB>(() => BrokerDB.RetrieveById(this.BrokerId));
                }
            }
        }

        /// <summary>
        /// Load the data for this class from the database.
        /// </summary>
        private void LoadFromDB()
        {
            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@BrokerId", this.BrokerId)
            };
            using (var conn = DbAccessUtils.GetConnection(this.BrokerId))
            {
                IDataReader reader = StoredProcedureDriverHelper.ExecuteReader(
                    conn,
                    transaction: null,
                    procedureName: StoredProcedureName.Create("BROKER_UCD_CONFIG_Load").ForceValue(),
                    parameters: parameters,
                    timeout: TimeoutInSeconds.Default);
                if (reader.Read())
                {
                    this.LoadFromData(reader);
                }
            }
        }

        /// <summary>
        /// Loads the data from an <see cref="IDataRecord"/> into this object.
        /// </summary>
        /// <param name="record">The record to load from.</param>
        private void LoadFromData(IDataRecord record)
        {
            this.AllowLqbDeliveryToFannieMae = (bool)record["AllowLqbDeliveryToFannieMae"];
            this.IsFannieMaeTesting = (bool)record["IsFannieMaeTesting"];
            this.AllowLqbDeliveryToFreddieMac = (bool)record["AllowLqbDeliveryToFreddieMac"];
            this.IsFreddieMacTesting = (bool)record["IsFreddieMacTesting"];
            this.AllowDocMagicDeliveryToFannieMae = (bool)record["AllowDocMagicDeliveryToFannieMae"];
            this.AllowDocMagicDeliveryToFreddieMac = (bool)record["AllowDocMagicDeliveryToFreddieMac"];
            this.RelationshipWithFreddieMac = (LenderRelationshipWithFreddieMac)record["RelationshipWithFreddieMac"];
        }
    }
}
