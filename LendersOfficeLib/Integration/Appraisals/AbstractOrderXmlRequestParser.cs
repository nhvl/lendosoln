﻿namespace LendersOffice.Integration.Appraisals
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using Common;

    /// <summary>
    /// Parses XML contain Appraisal order data.
    /// </summary>
    public abstract class AbstractOrderXmlRequestParser
    {
        /// <summary>
        /// Holds the list of errors encountered parsing the file.
        /// </summary>
        private List<string> errors;

        /// <summary>
        /// Holds the list of fields found on the XML file and their values.
        /// </summary>
        private Dictionary<string, string> fields;

        /// <summary>
        /// Holds the list of documents that are to be attached.
        /// </summary>
        private List<Guid> attachableDocuments;

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractOrderXmlRequestParser" /> class.
        /// </summary>
        /// <param name="xml">The XML document to be parsed.</param>
        public AbstractOrderXmlRequestParser(string xml)
        {
            this.errors = new List<string>();
            this.fields = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            this.attachableDocuments = new List<Guid>();
            this.Parse(xml);
        }

        /// <summary>
        /// Gets a value indicating whether any errors were encountered.
        /// </summary>
        /// <value>True if there are errors in the XML document.</value>
        public bool HasErrors
        {
            get
            {
                return this.errors.Count > 0;
            }
        }

        /// <summary>
        /// Gets the set of errors that were encountered parsing the XML.
        /// </summary>
        /// <returns>A set of errors that were encountered or an empty list.</returns>
        public IEnumerable<string> GetErrors()
        {
            return this.errors;
        }

        /// <summary>
        /// Gets a value indicating whether the credit card and billing data is required.
        /// </summary>
        /// <returns>True if the billing data is required.</returns>
        protected abstract bool RequiresCreditCard();

        /// <summary>
        /// Applies the generic data found in the XML to the abstract order details.
        /// </summary>
        /// <param name="details">The details to fill with the data found in the file.</param>
        protected virtual void ApplyTo(AbstractOrderRequestDetails details)
        {
            details.ContactName = this.GetNonEmptyStringOrLog("contactname");
            details.ContactPhone = this.GetSafeValue("contactphone");
            details.ContactPhoneWork = this.GetSafeValue("contactphonework");
            details.ContactPhoneOther = this.GetSafeValue("contactphoneother");
            details.ContactEmail = this.GetSafeValue("contactemail");
            details.ContactAditionalEmail = this.GetSafeValue("contactemailother");
            details.LoanName = this.GetNonEmptyStringOrLog("loanname");
            details.Notes = this.GetSafeValue("ordernotes");
            details.AppraisalNeededBy = this.GetFutureDate("orderduedate");
            details.DocumentIds = this.attachableDocuments.ToList();

            var creds = new string[]
            {
                this.GetSafeValue("orderaccountid"),
                this.GetSafeValue("orderusername"),
                this.GetSafeValue("orderpassword")
            };

            if (creds.Any(p => !string.IsNullOrWhiteSpace(p)))
            {
                if (creds.All(p => !string.IsNullOrWhiteSpace(p)))
                {
                    Credential credentials = new Credential()
                    {
                        AccountId = creds[0].Trim(),
                        Username = creds[1].Trim(),
                        Password = creds[2].Trim()
                    };

                    details.Credentials = credentials;
                }
                else
                {
                    this.AddError("All 3 are required orderaccountid, orderusername,  orderpassword.");
                }
            }

            if (this.RequiresCreditCard())
            {
                var billing = details.Billing = new BillingDetails();
                billing.Name = this.GetNonEmptyStringOrLog("billingname");
                billing.Street = this.GetNonEmptyStringOrLog("billingnstreet");
                billing.City = this.GetNonEmptyStringOrLog("billingcity");
                billing.Zip = this.GetNonEmptyStringOrLog("billingzip");
                billing.State = this.GetNonEmptyStringOrLog("billingstate");
                billing.CreditCardNumber = this.GetNonEmptyStringOrLog("billingccnumber");
                billing.CreditCardExpirationMonth = this.GetIntAndLogErrorIfNotFound("billingccexpirationmonth");
                billing.CreditCardExpirationYear = this.GetIntAndLogErrorIfNotFound("billingccexpirationyear");
                billing.CreditCardSecurityCode = this.GetNonEmptyStringOrLog("billingccsecuritycode");
                billing.CreditCardType = this.GetNonEmptyStringOrLog("billingcctype");
            }

            details.LinkedReoId = this.GetSafeValue("reoid").ToNullable<Guid>(Guid.TryParse);
        }

        /// <summary>
        /// Gets the int associated with the given id. If the int is not found or is not there
        /// an error is logged.
        /// </summary>
        /// <param name="id">The identifier for the field element containing the id.</param>
        /// <returns>The integer value if found and parsed otherwise -1. </returns>
        protected int GetIntAndLogErrorIfNotFound(string id)
        {
            string value = this.GetNonEmptyStringOrLog(id);

            int v;

            if (!int.TryParse(value, out v))
            {
                v = -1;
            }

            return v;
        }

        /// <summary>
        /// Gets a string value at the given id. If it is not found empty string is returned 
        /// and no error is logged.
        /// </summary>
        /// <param name="id">The identifier of the element containing the value.</param>
        /// <returns>The string or empty string if not found.</returns>
        protected string GetSafeValue(string id)
        {
            return this.GetValue(id) ?? string.Empty;
        }

        /// <summary>
        /// Gets the string at the given id. If it is not found null is returned.
        /// </summary>
        /// <param name="id">The identifier of the element containing the value.</param>
        /// <returns>The string or a null value.</returns>
        protected string GetValue(string id)
        {
            string value;
            this.fields.TryGetValue(id, out value);
            return value;
        }

        /// <summary>
        /// Gets a date from the given value. If the date is not in the future an error is logged and
        /// minimum date is returned.
        /// </summary>
        /// <param name="id">The identifier containing the wanted date.</param>
        /// <returns>The date in the future or the date minimum.</returns>
        protected DateTime GetFutureDate(string id)
        {
            string value = this.GetNonEmptyStringOrLog(id);

            DateTime dt;

            if (!DateTime.TryParse(value, out dt) || dt < DateTime.Today)
            {
                this.errors.Add($"{id} has to be date in the future");
                dt = DateTime.MinValue;
            }

            return dt;
        }

        /// <summary>
        /// Gets the string at the given id or logs an error.
        /// </summary>
        /// <param name="id">The id that should contain the string.</param>
        /// <returns>Empty string or the string if it exist.</returns>
        protected string GetNonEmptyStringOrLog(string id)
        {
            string value;

            if (!this.fields.TryGetValue(id, out value))
            {
                this.errors.Add($"{id} is required.");
            }

            if (string.IsNullOrWhiteSpace(value))
            {
                this.errors.Add($"{id} cannot be empty.");
            }

            return value ?? string.Empty;
        }

        /// <summary>
        /// Adds the given message as an error to the set of errors.
        /// </summary>
        /// <param name="msg">The message to add to the error log.</param>
        protected void AddError(string msg)
        {
            this.errors.Add(msg);
        }

        /// <summary>
        /// Parses the given XML file.
        /// </summary>
        /// <param name="xml">The XML file to be parsed.</param>
        private void Parse(string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            foreach (XmlNode node in doc.SelectNodes("//appraisal/field"))
            {
                XmlAttribute idAttr = node.Attributes["id"];

                if (idAttr == null)
                {
                    continue;
                }

                string value = node.InnerText;

                if (idAttr.Value.StartsWith("document"))
                {
                    Guid g;

                    if (!Guid.TryParse(value, out g))
                    {
                        this.errors.Add($"{idAttr.Value} has incorrect value.");
                    }

                    this.attachableDocuments.Add(g);
                }
                else
                {
                    this.fields.Add(idAttr.Value, value);
                }
            }
        }
    }
}
