﻿namespace LendersOffice.Integration.Appraisals
{
    /// <summary>
    /// The main class for vendor setting responses.
    /// </summary>
    public abstract class AbstractVendorSettingsResponse
    {
        /// <summary>
        /// Gets the type of response this instance represents.
        /// </summary>
        /// <value>The type of response.</value>
        public abstract ResponseType ResponseType { get; }
    }
}
