﻿namespace LendersOffice.Integration.Appraisals
{
    using System;
    using System.Collections.Generic;
    using Common;

    /// <summary>
    /// Parses a xml document as a LQB order request details.
    /// </summary>
    public class LqbOrderXmlRequestParser : AbstractOrderXmlRequestParser
    {
        /// <summary>
        /// The details to be populated.
        /// </summary>
        private LqbOrderRequestDetails details;

        /// <summary>
        /// Initializes a new instance of the <see cref="LqbOrderXmlRequestParser" /> class.
        /// </summary>
        /// <param name="xml">The xml document to parse.</param>
        public LqbOrderXmlRequestParser(string xml) : base(xml)
        {
        }

        /// <summary>
        /// Creates the request details and generates a list of errors.
        /// </summary>
        public void Generate()
        {
            this.details = new LqbOrderRequestDetails();
            this.details.Products = new List<string>();

            this.details.IsRushOrder = this.GetSafeBool("orderisrush");
            this.details.OrderNumber = this.GetSafeValue("ordernumber");
            this.details.BillingMethod = this.GetNonEmptyStringOrLog("billingmethod");

            string creditCardIndicatorAttr = this.GetValue("creditcardindicator");
            if (creditCardIndicatorAttr == null)
            {
                this.details.CreditCardIndicator = DataAccess.E_TriState.Blank;
            }
            else
            {
                DataAccess.E_TriState creditCardIndicator;
                if (creditCardIndicatorAttr.TryParseDefine(out creditCardIndicator, ignoreCase: true))
                {
                    this.details.CreditCardIndicator = creditCardIndicator;
                }
                else
                {
                    this.details.CreditCardIndicator = DataAccess.E_TriState.Blank;
                    this.AddError($"Invalid value for attribute creditcardindicator: {creditCardIndicatorAttr}.");
                }
            }

            string amcIdentifier = this.GetValue("amcidentifier");
            
            if (!string.IsNullOrWhiteSpace(amcIdentifier))
            {
                this.details.AddCustomItem("AMCIdentifier", amcIdentifier);
            }

            for (int i = 1; i <= 5; i++)
            {
                string value = this.GetSafeValue($"product{i}");
                if (!string.IsNullOrWhiteSpace(value))
                {
                    this.details.Products.Add(value);
                }
            }

            if (this.details.Products.Count == 0)
            {
                this.AddError("At least one product is required.");
            }

            this.ApplyTo(this.details);
        }

        /// <summary>
        /// Gets the generated request details.
        /// </summary>
        /// <returns>The request details for the order.</returns>
        public LqbOrderRequestDetails GetDetails()
        {
            if (this.HasErrors)
            {
                return null;
            }

            return this.details;
        }

        /// <summary>
        /// Gets a value indicating whether the billing data is required.
        /// </summary>
        /// <returns>True if the billing data is required.</returns>
        protected override bool RequiresCreditCard()
        {
            return
                this.details.CreditCardIndicator == DataAccess.E_TriState.Yes ||
                (this.details.CreditCardIndicator == DataAccess.E_TriState.Blank && this.details.BillingMethod.ToLower().Contains("credit card"));
        }

        /// <summary>
        /// Gets the boolean with the given id. Will return false if the element is missing.
        /// No error is logged.
        /// </summary>
        /// <param name="id">The id of the element to look for.</param>
        /// <returns>True if the element exist and we can parse it as true.</returns>
        private bool GetSafeBool(string id)
        {
            string v = GetSafeValue(id);
            bool b;

            bool.TryParse(v, out b);
            return b;
        }
    }
}
