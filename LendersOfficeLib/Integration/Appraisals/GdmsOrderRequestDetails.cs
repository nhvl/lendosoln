﻿namespace LendersOffice.Integration.Appraisals
{
    /// <summary>
    /// A class holding data required for GDMS orders.
    /// </summary>
    public class GdmsOrderRequestDetails : AbstractOrderRequestDetails
    {
        /// <summary>
        /// Gets or sets the billing method id.
        /// </summary>
        /// <value>The billing method id.</value>
        public int BillingMethodId { get; set; }

        /// <summary>
        /// Gets or sets the case number.
        /// </summary>
        /// <value>The case number.</value>
        public string CaseNumber { get; set; }

        /// <summary>
        /// Gets or sets the billing method description.
        /// </summary>
        /// <value>The billing method description.</value>
        public string BillingMethodDescription { get; set; }

        /// <summary>
        /// Gets or sets the lender name.
        /// </summary>
        /// <value>THe lender name.</value>
        public string LenderName { get; set; }

        /// <summary>
        /// Gets or sets the property type id.
        /// </summary>
        /// <value>The property type id.</value>
        public int PropertyTypeId { get; set; }

        /// <summary>
        /// Gets or sets the property type name.
        /// </summary>
        public string PropertyTypeName { get; set; }

        /// <summary>
        /// Gets or sets the report type name.
        /// </summary>
        public string ReportTypeName { get; set; }

        /// <summary>
        /// Gets or sets the first report type id.
        /// </summary>
        /// <value>The first report type id.</value>
        public int ReportType1Id { get; set; }

        /// <summary>
        /// Gets or sets the second report type id.
        /// </summary>
        /// <value>The second report type id.</value>
        public int ReportType2Id { get; set; }

        /// <summary>
        /// Gets or sets the third report type id.
        /// </summary>
        /// <value>The third report type id.</value>        
        public int ReportType3Id { get; set; }

        /// <summary>
        /// Gets or sets the fourth report type id.
        /// </summary>
        /// <value>The fourth report type id.</value>
        public int ReportType4Id { get; set; }

        /// <summary>
        /// Gets or sets the fifth report type id.
        /// </summary>
        /// <value>The fifth report type id.</value>
        public int ReportType5Id { get; set; }

        /// <summary>
        /// Gets or sets the intended use id.
        /// </summary>
        /// <value>The intended use id.</value>
        public int IntendedUseId { get; set; }

        /// <summary>
        /// Gets or sets the intended use name.
        /// </summary>
        public string IntendedUseName { get; set; }

        /// <summary>
        /// Gets or sets the loan type name.
        /// </summary>
        public string LoanTypeName { get; set; }

        /// <summary>
        /// Gets or sets the loan type id.
        /// </summary>
        /// <value>The loan type id.</value>
        public int LoanTypeId { get; set; }

        /// <summary>
        /// Gets or sets the occupancy type name.
        /// </summary>
        public string OccupancyTypeName { get; set; }

        /// <summary>
        /// Gets or sets the occupancy type id.
        /// </summary>
        /// <value>The occupancy type id.</value>
        public int OccupancyTypeId { get; set; }

        /// <summary>
        /// Gets or sets the client2 id.
        /// </summary>
        /// <value>The client2 id.</value>
        public int Client2Id { get; set; }

        /// <summary>
        /// Gets or sets the processor id.
        /// </summary>
        /// <value>The process id.</value>
        public int ProcessorId { get; set; }

        /// <summary>
        /// Gets or sets the second processor id.
        /// </summary>
        /// <value>The second process id.</value>
        public int Processor2Id { get; set; }

        /// <summary>
        /// Gets or sets the attachment type id.
        /// </summary>
        /// <value>The attachment type id.</value>
        public int OrderFileUploadTypeId { get; set; }
    }
}
