﻿namespace LendersOffice.Integration.Appraisals
{
    using LendersOffice.ObjLib.Webservices;

    /// <summary>
    /// The base response for abstract vendor settings.
    /// </summary>
    public abstract class AbstractVendorSettingsResult : ServiceResult
    {
    }
}
