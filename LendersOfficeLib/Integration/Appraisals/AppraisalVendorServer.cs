﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using LQBAppraisal.LQBAppraisalRequest;
using LQBAppraisal.LQBAppraisalResponse;
using DataAccess;

namespace LendersOffice.Integration.Appraisals
{
    public static class AppraisalVendorServer
    {
        public static LQBAppraisalResponse Submit(LQBAppraisalRequest request, string url)
        {
            LQBAppraisalResponse response = null;
            StringBuilder debugStringBuilder = new StringBuilder(10000);
            try
            {
                debugStringBuilder.AppendLine("Url=" + url);
                byte[] bytes = null;
                using (MemoryStream stream = new MemoryStream(5000))
                {
                    using (XmlWriter writer = XmlWriter.Create(stream))
                    {
                        request.WriteXml(writer);
                    }

                    bytes = stream.ToArray();
                }

                // Mask out the password in PaulBunyan log
                string bytesString = System.Text.Encoding.UTF8.GetString(bytes);
                bytesString = Regex.Replace(bytesString, "Password=\"[^\"]+?\"", "Password=\"******\""); // use nongreedy matching so we don't stomp over values
                bytesString = Regex.Replace(bytesString, "<EmbeddedDoc( [^>]*)*>[^<]+</EmbeddedDoc>", "<EmbeddedDoc$1> [base64 Encoded file data] </EmbeddedDoc>"); //remove file data to prevent log memory over-run.

                debugStringBuilder.AppendLine("Request:");
                debugStringBuilder.AppendLine(bytesString);

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.KeepAlive = false; // 11/4/2004 dd - DO NOT REMOVE THIS LINE. Else you will get "Cannot access a disposed object named "System.Net.TlsStream"" error randomly.
                webRequest.Method = "POST";
                webRequest.Timeout = 300000; //5/17/2016 BS - Case 239334. Extend timeout to 5 mins
                webRequest.ContentType = "text/xml";
                webRequest.ContentLength = bytes.Length;

                // Send out the data
                using (Stream stream = webRequest.GetRequestStream())
                {
                    stream.Write(bytes, 0, bytes.Length);
                }

                StringBuilder sb = new StringBuilder();

                using (WebResponse webResponse = webRequest.GetResponse())
                using (Stream stream = webResponse.GetResponseStream())
                {
                    byte[] buffer = new byte[60000];
                    int size = stream.Read(buffer, 0, buffer.Length);

                    while (size > 0)
                    {
                        string chunk = System.Text.Encoding.UTF8.GetString(buffer, 0, size);
                        sb.Append(chunk);
                        size = stream.Read(buffer, 0, buffer.Length);
                    }
                }

                XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
                xmlReaderSettings.XmlResolver = null;
#if LQB_NET45
                xmlReaderSettings.DtdProcessing = DtdProcessing.Parse;
#else
                xmlReaderSettings.ProhibitDtd = false;
#endif

                if (sb.Length <= 20000)
                {
                    debugStringBuilder.AppendLine(Environment.NewLine + "Response:" + Environment.NewLine + sb.ToString());
                }
                else
                {
                    debugStringBuilder.AppendLine("Response was too large, parts redacted. Original length was: " + sb.Length);

                    XmlDocument xdoc = new XmlDocument();

                    using (XmlReader reader = XmlReader.Create(new StringReader(sb.ToString()), xmlReaderSettings))
                    {
                        xdoc.Load(reader);
                    }

                    string[] redactedTags = {"ProofSheetHTML", "AuditHTML", 
                                          "APRPaymentCalculationHTML", 
                                          "Section32CalculationHTML", 
                                          "GFEComparisonHTML",
                                          "ImpoundAnalysisHTML", 
                                          "DocHTML", "CheckHTML",
                                          "EmbeddedContent"};

                    foreach (var tagName in redactedTags)
                    {
                        foreach (XmlNode node in xdoc.GetElementsByTagName(tagName))
                        {
                            node.InnerText = "Redacted for space, length was " + (node.InnerText ?? "").Length;
                        }
                    }

                    if (xdoc.InnerXml.Length <= 60000)
                    {
                        debugStringBuilder.AppendLine("Redacted response:" + Environment.NewLine + xdoc.InnerXml);
                    }
                    else
                    {
                        var responseStatus = "Unknown, couldn't find it";
                        var statusNodes = xdoc.GetElementsByTagName("LQBAppraisalResponse");
                        if (statusNodes.Count > 0)
                        {
                            var statusAttr = statusNodes[0].Attributes["status"];
                            if (statusAttr != default(XmlAttribute)) responseStatus = statusAttr.Value;
                        }

                        debugStringBuilder.AppendLine("Redacted response was still to large (final size: " + xdoc.InnerXml.Length + "), not logging it. Response status was: " + responseStatus);
                    }

                }

                response = new LQBAppraisalResponse();
                using (XmlReader reader = XmlReader.Create(new StringReader(sb.ToString()), xmlReaderSettings))
                {
                    response.ReadXml(reader);
                }
            }
            catch (XmlException)
            {
                response = new LQBAppraisalResponse();
                response.ServerResponse.RequestStatus = E_ServerResponseRequestStatus.Failure;
                response.ServerResponse.ServerMessageList.Add(new ServerMessage()
                {
                    Description = "Error submitting request to vendor.",
                    Type = E_ServerMessageType.ServerError
                });
            }
            finally
            {
                Tools.LogInfo(debugStringBuilder.ToString());
            }

            return response;
        }
    }
}
