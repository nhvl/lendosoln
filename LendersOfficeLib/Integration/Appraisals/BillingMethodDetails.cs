﻿namespace LendersOffice.Integration.Appraisals
{
    using DataAccess;

    /// <summary>
    /// Billing method details.
    /// </summary>
    public class BillingMethodDetails
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BillingMethodDetails"/> class. For LQBAppraisal responses.
        /// </summary>
        /// <param name="billingMethod">The billing method.</param>
        public BillingMethodDetails(LQBAppraisal.LQBAppraisalResponse.BillingMethod billingMethod)
        {
            this.Name = billingMethod.Name;
            this.Identifier = billingMethod.Name;
            if (billingMethod.CreditCardIndicator == LQBAppraisal.E_YNIndicator.Y)
            {
                this.CreditCardIndicator = E_TriState.Yes;
            }
            else if (billingMethod.CreditCardIndicator == LQBAppraisal.E_YNIndicator.N)
            {
                this.CreditCardIndicator = E_TriState.No;
            }
            else
            {
                this.CreditCardIndicator = E_TriState.Blank;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BillingMethodDetails"/> class. For GDMS responses.
        /// </summary>
        /// <param name="billingMethod">The billing method.</param>
        public BillingMethodDetails(GDMS.LookupMethods.BillingMethod billingMethod)
        {
            this.Name = billingMethod.BillingMethodName;
            this.Identifier = billingMethod.BillingMethodId.ToString();
            this.CreditCardIndicator = E_TriState.Blank;
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the credit card indicator.
        /// </summary>
        public E_TriState CreditCardIndicator { get; set; }
    }
}
