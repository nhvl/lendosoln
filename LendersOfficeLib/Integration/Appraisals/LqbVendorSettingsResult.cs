﻿namespace LendersOffice.Integration.Appraisals
{
    using System.Xml;

    /// <summary>
    /// A vendor result generator for LQB vendor setting calls.
    /// </summary>
    public class LqbVendorSettingsResult : AbstractVendorSettingsResult
    {
        /// <summary>
        /// The response to serialize.
        /// </summary>
        private LqbVendorSettingsResponse response;

        /// <summary>
        /// Initializes a new instance of the <see cref="LqbVendorSettingsResult" /> class.
        /// </summary>
        /// <param name="response">The vendor setting response being serialized.</param>
        public LqbVendorSettingsResult(LqbVendorSettingsResponse response)
        {
            this.response = response;
        }

        /// <summary>
        /// Given an Xml writer turns the current response object into xml.
        /// </summary>
        /// <param name="writer">The Xml writer to write to.</param>
        protected override void WriteResponseResult(XmlWriter writer)
        {
            writer.WriteStartElement("appraisalordersettings");
            writer.WriteAttributeString("usesgdms", bool.FalseString);
            writer.WriteAttributeString("vendorid", this.response.Id.ToString());
            writer.WriteStartElement("setting");
            writer.WriteAttributeString("name", "BillingMethod");
            writer.WriteAttributeString("min", "1");
            writer.WriteAttributeString("max", "1");

            foreach (var method in this.response.BillingMethods)
            {
                writer.WriteStartElement("option");
                writer.WriteAttributeString("name", method.Name);
                writer.WriteAttributeString("value", method.Name);
                if (method.CreditCardIndicator != DataAccess.E_TriState.Blank)
                {
                    writer.WriteAttributeString("creditcardindicator", method.CreditCardIndicator.ToString());
                }

                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            writer.WriteStartElement("setting");

            writer.WriteAttributeString("name", "Product");
            writer.WriteAttributeString("min", "1");
            writer.WriteAttributeString("max", "5");

            foreach (var product in this.response.ProductList)
            {
                writer.WriteStartElement("option");
                writer.WriteAttributeString("name", product.name);
                writer.WriteAttributeString("value", product.name);
                if (!string.IsNullOrEmpty(product.requires))
                {
                    writer.WriteAttributeString("requiresproduct", product.requires);
                }

                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            writer.WriteEndElement();
        }
    }
}