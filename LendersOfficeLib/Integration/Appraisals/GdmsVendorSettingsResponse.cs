﻿namespace LendersOffice.Integration.Appraisals
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The successful vendor response for Gdms.
    /// </summary>
    public class GdmsVendorSettingsResponse : AbstractVendorSettingsResponse
    {
        /// <summary>
        /// Gets the Gdms response type.
        /// </summary>
        /// <value>A hardcoded Gdms value.</value>
        public override ResponseType ResponseType
        {
            get
            {
                return ResponseType.GDMS;
            }
        }

        /// <summary>
        /// Gets or sets the property types available.
        /// </summary>
        /// <value>A set of property types.</value>
        public List<Option> PropertyTypes { get; set; }

        /// <summary>
        /// Gets or sets the report types available.
        /// </summary>
        /// <value>A set of report types.</value>
        public List<Option> ReportTypes { get; set; }

        /// <summary>
        /// Gets or sets the intended uses available.
        /// </summary>
        /// <value>A set of intended uses.</value>
        public List<Option> IntendedUses { get; set; }

        /// <summary>
        /// Gets or sets the loan types available.
        /// </summary>
        /// <value>A set of loan types.</value>
        public List<Option> LoanTypes { get; set; }

        /// <summary>
        /// Gets or sets the occupancy types available.
        /// </summary>
        /// <value>A set of occupancy types.</value>
        public List<Option> OccupancyTypes { get; set; }

        /// <summary>
        /// Gets or sets the billing methods available.
        /// </summary>
        /// <value>A set of billing methods.</value>
        public List<Option> BillingMethods { get; set; }

        /// <summary>
        /// Gets or sets the client available.
        /// </summary>
        /// <value>A set of clients available.</value>
        public List<Option> Client2 { get; set; }

        /// <summary>
        /// Gets or sets the processors available.
        /// </summary>
        /// <value>A set of processors.</value>
        public List<Option> Processors { get; set; }

        /// <summary>
        /// Gets or sets the processors available.
        /// </summary>
        /// <value>A set of processors.</value>
        public List<Option> Processors2 { get; set; }

        /// <summary>
        /// Gets or sets the file upload types available.
        /// </summary>
        /// <value>A set of file upload types available.</value>
        public List<Option> OrderFileUploadTypes { get; set; }

        /// <summary>
        /// Gets or sets the vendor id.
        /// </summary>
        /// <value>The vendor id.</value>
        public Guid Id { get; set; }
    }
}
