﻿// <copyright file="AppraisalVendorConfig.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Paolo Arrastia
//  Date:   4/15/2013 1:41:43 PM
// </summary>
namespace LendersOffice.Integration.Appraisals
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Conversions.GlobalDMS;

    /// <summary>
    /// Represents the configuration settings of an appraisal vendor.
    /// </summary>
    public class AppraisalVendorConfig
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppraisalVendorConfig"/> class.
        /// Use this constructor when you are creating a new vendor configuration.
        /// </summary>
        public AppraisalVendorConfig()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppraisalVendorConfig"/> class.
        /// </summary>
        /// <param name="reader">Containing the vendor information.</param>
        private AppraisalVendorConfig(DbDataReader reader)
        {
            this.VendorId = (Guid)reader["VendorId"];
            this.VendorName = (string)reader["VendorName"];
            this.UsesAccountId = (bool)reader["UsesAccountId"];
            this.UsesGlobalDMS = (bool)reader["IsUseGlobalDMSIntegration"];
            this.AppraisalNeededDateOption = (E_AppraisalNeededDateOptions)reader["AppraisalNeededDateOption"];
            this.HideNotes = (bool)reader["HideNotes"];
            this.PrimaryExportPath = reader["ExportPath"].ToString();
            this.ExportToStagePath = (bool)reader["ExportToStagePath"];
            this.AllowOnlyOnePrimaryProduct = (bool)reader["AllowOnlyOnePrimaryProduct"];
            this.SendAuthTicketWithOrders = (bool)reader["SendAuthTicketWithOrders"];
            this.ShowAdditionalEmails = (bool)reader["ShowAdditionalEmails"];
            this.EnableConnectionTest = (bool)reader["EnableConnectionTest"];
            this.EnableBiDirectionalCommunication = (bool)reader["EnableBiDirectionalCommunication"];
        }

        /// <summary>
        /// Gets or sets the unique identifier of the vendor.
        /// </summary>
        /// <value>The unique identifier of the vendor.</value>
        public Guid VendorId { get; set; }

        /// <summary>
        /// Gets or sets the name of the vendor.
        /// </summary>
        /// <value>The name of the vendor.</value>
        public string VendorName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the vendor uses an account identifier in credentials.
        /// </summary>
        /// <value>A value indicating whether the vendor uses an account identifier in credentials.</value>
        public bool UsesAccountId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the vendor uses GlobalDMS.
        /// </summary>
        /// <value>A value indicating whether the vendor uses GlobalDMS.</value>
        public bool UsesGlobalDMS { get; set; }

        /// <summary>
        /// Gets or sets a value determining the state of the "Appraisal Needed" date. Possible values include "Enabled, Not Required", "Enabled and Required", and "Hidden".
        /// </summary>
        /// <value>A value indicating whether the "Appraisal Needed" date should be required, hidden, or neither on appraisal orders.</value>
        public E_AppraisalNeededDateOptions AppraisalNeededDateOption { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the "Notes" field should be visible on appraisal orders.
        /// </summary>
        /// <value>A value indicating whether the "Notes" field should be visible on appraisal orders.</value>
        public bool HideNotes { get; set; }

        /// <summary>
        /// Gets or sets the primary export path of the vendor. Not valid for vendors where <see cref="UsesGlobalDMS"/> is true.
        /// </summary>
        /// <value>The primary export path of the vendor.</value>
        public string PrimaryExportPath { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether export will occur to the stage path. Only valid for vendors where <see cref="UsesGlobalDMS"/> is true.
        /// </summary>
        /// <value>A value indicating whether export will occur to the stage path.</value>
        public bool ExportToStagePath { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether an appraisal order may have more than one primary product selected.
        /// </summary>
        /// <value>A value indicating whether an appraisal order may have more than one primary product selected.</value>
        public bool AllowOnlyOnePrimaryProduct { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether an appraisal order will contain a user authentication ticket.
        /// </summary>
        /// <value>A value indicating whether an appraisal order will contain a user authentication ticket.</value>
        public bool SendAuthTicketWithOrders { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the "Additional Emails" field on the appraisal order page should appear.
        /// </summary>
        /// <value>A value indicating whether the "Additional Emails" field on the appraisal order page should appear.</value>
        public bool ShowAdditionalEmails { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether connection tests accessing the appraisal vendor should be run.
        /// </summary>
        /// <value>Whether connection tests accessing the appraisal vendor should be run.</value>
        public bool EnableConnectionTest { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the vendor supports the communication framework.
        /// </summary>
        /// <value>A value indicating whether the vendor supports the communication framework.</value>
        public bool EnableBiDirectionalCommunication { get; set; }

        /// <summary>
        /// Gets an array of SqlParameters needed to save the vendor in its present state.
        /// </summary>
        /// <value>An array of SqlParameters needed to save the vendor in its present state.</value>
        private SqlParameter[] SqlParameters
        {
            get
            {
                return new[]
                {
                    new SqlParameter("@VendorId", this.VendorId),
                    new SqlParameter("@VendorName", this.VendorName),
                    new SqlParameter("@UsesAccountId", this.UsesAccountId),
                    new SqlParameter("@ExportPath", this.PrimaryExportPath),
                    new SqlParameter("@IsUseGlobalDMSIntegration", this.UsesGlobalDMS),
                    new SqlParameter("@AppraisalNeededDateOption", this.AppraisalNeededDateOption),
                    new SqlParameter("@HideNotes", this.HideNotes),
                    new SqlParameter("@ExportToStagePath", this.ExportToStagePath),
                    new SqlParameter("@AllowOnlyOnePrimaryProduct", this.AllowOnlyOnePrimaryProduct),
                    new SqlParameter("@SendAuthTicketWithOrders", this.SendAuthTicketWithOrders),
                    new SqlParameter("@ShowAdditionalEmails", this.ShowAdditionalEmails),
                    new SqlParameter("@EnableConnectionTest", this.EnableConnectionTest),
                    new SqlParameter("@EnableBiDirectionalCommunication", this.EnableBiDirectionalCommunication)
                };
            }
        }

        /// <summary>
        /// Return a list of available appraiser vendors configuration.
        /// </summary>
        /// <returns>The list of available appraiser vendors configuration.</returns>
        public static IEnumerable<AppraisalVendorConfig> ListAll()
        {
            List<AppraisalVendorConfig> list = new List<AppraisalVendorConfig>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "APPRAISAL_VENDOR_AvailableVendors"))
            {
                while (reader.Read())
                {
                    list.Add(new AppraisalVendorConfig(reader));
                }
            }

            return list;
        }

        /// <summary>
        /// Retrieves the specified vendor configuration from the database.
        /// </summary>
        /// <param name="vendorId">The <see cref="VendorId"/> the vendor being retrieved.</param>
        /// <returns>The appraisal vendor configuration for the specified <paramref name="vendorId"/>.</returns>
        /// <exception cref="CBaseException">The appraisal vendor was not found.</exception>
        public static AppraisalVendorConfig Retrieve(Guid vendorId)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@VendorId", vendorId)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "APPRAISAL_VENDOR_GetConfig", parameters))
            {
                if (reader.Read())
                {
                    return new AppraisalVendorConfig(reader);
                }
            }

            throw new NotFoundException("Configured appraisal vendor with id " + vendorId + " was requested, but not found in the database.");
        }

        /// <summary>
        /// Saves the specified appraisal vendor configuration to the database.
        /// </summary>
        /// <param name="configToSave">The appraisal vendor configuration to save.</param>
        public static void Save(AppraisalVendorConfig configToSave)
        {
            if (configToSave.VendorId == Guid.Empty)
            {
                configToSave.VendorId = Guid.NewGuid();
            }

            var result = StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "APPRAISAL_VENDOR_Save", 3, configToSave.SqlParameters);
        }

        /// <summary>
        /// Enables the specified vendors for a broker.
        /// </summary>
        /// <param name="vendors">The list of <see cref="VendorId"/> values for each vendor to be enabled.</param>
        /// <param name="brokerId">The broker for which the vendors will be enabled.</param>
        public static void EnableVendorsForBroker(List<Guid> vendors, Guid brokerId)
        {
            foreach (Guid vendorId in vendors)
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@VendorId", vendorId),
                    new SqlParameter("@BrokerId", brokerId)
                };
                StoredProcedureHelper.ExecuteNonQuery(brokerId, "APPRAISAL_VENDOR_EnableForBroker", 3, parameters);
            }
        }

        /// <summary>
        /// Disables the specified vendors for a broker.
        /// </summary>
        /// <param name="vendors">The list of <see cref="VendorId"/> values for each vendor to be disabled.</param>
        /// <param name="brokerId">The broker for which the vendors will be disabled.</param>
        public static void DisableVendorsForBroker(List<Guid> vendors, Guid brokerId)
        {
            foreach (Guid vendorId in vendors)
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@VendorId", vendorId),
                    new SqlParameter("@BrokerId", brokerId)
                };
                StoredProcedureHelper.ExecuteNonQuery(brokerId, "APPRAISAL_VENDOR_DisableForBroker", 3, parameters);
            }
        }

        /// <summary>
        /// Deletes the specified vendor from the database, including broker and employee level credentials.
        /// </summary>
        /// <param name="vendorId">The <see cref="VendorId"/> to delete.</param>
        public static void DeleteVendor(Guid vendorId)
        {
            CheckForOrders(vendorId);
            DeleteDependents(vendorId);

            SqlParameter[] parameters =
            {
                new SqlParameter("@VendorId", vendorId)
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "APPRAISAL_VENDOR_Delete", 3, parameters);
        }

        /// <summary>
        /// Retrieves the Global DMS credentials of the specified user for this vendor.
        /// </summary>
        /// <param name="brokerId">The broker identifier of the user's broker.</param>
        /// <param name="employeeId">The employee identifier of the user.</param>
        /// <param name="errorMessage">The error message of what caused the credentials to fail to load.</param>
        /// <returns>The credentials of the user for this vendor if <see cref="UsesGlobalDMS"/> is true and <see cref="GDMSCredentials.CompanyId"/> is valid; null otherwise.</returns>
        public GDMSCredentials GetGDMSCredentials(Guid brokerId, Guid employeeId, out string errorMessage)
        {
            if (!this.UsesGlobalDMS)
            {
                errorMessage = this.LogGDMSCredentialError("Vendor does not use GlobalDMS.", null);
                return null;
            }

            GDMSCredentials credentials = new GDMSCredentials();
            int companyId;
            AppraisalVendorCredentials cred = new AppraisalVendorCredentials(brokerId, employeeId, this.VendorId);
            if (!int.TryParse(cred.AccountId, out companyId))
            {
                errorMessage = this.LogGDMSCredentialCompanyIdError("AppraisalVendorCredentials", cred.AccountId, cred.UserName);
                return null;
            }

            credentials.CompanyId = companyId;
            credentials.UserName = cred.UserName;
            credentials.Password = cred.Password;

            errorMessage = null;
            credentials.ExportToStage = this.ExportToStagePath;
            credentials.UserType = 2; // 1 - Vendor/Appraiser, 2 - Client, 3 - Staff
            return credentials;
        }

        /// <summary>
        /// Checks if there are any existing appraisal orders tied to this vendor. Throws if any are found.
        /// </summary>
        /// <param name="vendorId">Appraisal vendor configuration ID.</param>
        private static void CheckForOrders(Guid vendorId)
        {
            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@VendorId", vendorId)
                };

                using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "GetAppraisalOrdersByVendorId", parameters))
                {
                    if (reader.Read())
                    {
                        throw new CBaseException(ErrorMessages.Generic, "Cannot delete appraisal vendor with VendorId=[" + vendorId + "], as there are appraisal orders linked to this vendor.");
                    }
                }
            }
        }

        /// <summary>
        /// Delete database entries in tables dependent on APPRAISAL_VENDOR_CONFIGURATION.
        /// </summary>
        /// <param name="vendorId">Appraisal vendor configuration ID.</param>
        private static void DeleteDependents(Guid vendorId)
        {
            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@VendorId", vendorId)
                };

                StoredProcedureHelper.ExecuteNonQuery(connInfo, "APPRAISAL_VENDOR_DeleteDependents", 3, parameters);
            }
        }

        /// <summary>
        /// Logs debug information about an error loading <see cref="GDMSCredentials.CompanyId"/> in <see cref="GetGDMSCredentials"/>.
        /// </summary>
        /// <param name="location">The source information from where the value is loading.</param>
        /// <param name="companyID">The <see cref="GDMSCredentials.CompanyId"/> value that failed.</param>
        /// <param name="userName">The GlobalDMS username of the user for which this occurred.</param>
        /// <returns>The error message to report.</returns>
        private string LogGDMSCredentialCompanyIdError(string location, string companyID, string userName)
        {
            string debugInfo = "Error occurred while retrieving from " + location + "." + Environment.NewLine + Environment.NewLine
                + "CompanyID: '" + companyID + "'" + Environment.NewLine
                + "UserName:  '" + userName + "'";
            return this.LogGDMSCredentialError("Invalid Company ID.", debugInfo);
        }

        /// <summary>
        /// Logs debug information about an error loading <see cref="GDMSCredentials"/> in <see cref="GetGDMSCredentials"/>.
        /// </summary>
        /// <param name="errorMessage">The error message corresponding to the error that occurred.</param>
        /// <param name="debugInformation">The debug information, if any.</param>
        /// <returns>The error message to report.</returns>
        private string LogGDMSCredentialError(string errorMessage, string debugInformation)
        {
            string log = "GDMS Credential Error: \"" + errorMessage + "\"" + Environment.NewLine + Environment.NewLine
                + (!string.IsNullOrEmpty(debugInformation) ? debugInformation + Environment.NewLine : null)
                + "Vendor Information" + Environment.NewLine
                + "VendorName: '" + this.VendorName + "'" + Environment.NewLine
                + "VendorId: " + this.VendorId.ToString();
            Tools.LogWarning(log);
            return errorMessage;
        }
    }
}
