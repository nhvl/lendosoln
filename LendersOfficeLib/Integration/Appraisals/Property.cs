﻿namespace LendersOffice.Integration.Appraisals
{
    /// <summary>
    /// Holds property info for appraisal request.
    /// </summary>
    public class Property
    {
        /// <summary>
        /// Gets or sets the property address.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the zip.
        /// </summary>
        public string Zip { get; set; }
    }
}
