﻿namespace LendersOffice.Integration.Appraisals
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Common;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    public class AppraisalVendorCredentials
    {
        private EncryptionKeyIdentifier? passwordEncryptionKeyId;
        private Lazy<string> lazyPassword;
        private Guid EmployeeId;
        private Guid VendorId;

        public Guid BrokerId { get; private set; }

        public string AccountId { get; set; }

        public string UserName { get; set; }
        
        public string Password
        {
            get
            {
                return this.lazyPassword?.Value ?? string.Empty;
            }

            set
            {
                if (value == Constants.ConstAppDavid.FakePasswordDisplay)
                {
                    throw new ArgumentException("can't assign a password of " + Constants.ConstAppDavid.FakePasswordDisplay);
                }

                this.lazyPassword = new Lazy<string>(() => value);
            }
        }

        public AppraisalVendorCredentials(Guid brokerId, Guid VendorId, string AccountId, string UserName, string Password)
        {
            this.BrokerId = brokerId;
            this.VendorId = VendorId;
            this.AccountId = AccountId;
            this.UserName = UserName;
            this.Password = Password;
        }

        public AppraisalVendorCredentials(Guid brokerId, Guid EmployeeId, Guid VendorId)
        {
            this.BrokerId = brokerId;
            this.EmployeeId = EmployeeId;
            this.VendorId = VendorId;

            SqlParameter[] parameters = {
                                            new SqlParameter("@EmployeeId", EmployeeId), 
                                            new SqlParameter("@VendorId", VendorId)
                                        };

            IReadOnlyDictionary<string, object> readFields = null;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, "APPRAISAL_VENDOR_RetrieveLogin", parameters))
            {
                if (reader.Read())
                {
                    readFields = reader.ToDictionary();
                }
            }

            if (readFields == null)
            {
                AccountId = string.Empty;
                UserName = string.Empty;
                Password = string.Empty;
            }
            else
            {
                AccountId = readFields["AccountId"].ToString();
                UserName = readFields["Username"].ToString();

                this.passwordEncryptionKeyId = EncryptionKeyIdentifier.Create((Guid)readFields["EncryptionKeyId"]);
                var passwordBytes = readFields.GetValueOrNull("EncryptedPassword") as byte[];
                this.lazyPassword = new Lazy<string>(() => Drivers.Encryption.EncryptionHelper.DecryptString(this.passwordEncryptionKeyId.Value, passwordBytes));
            }
        }

        public void SaveCredentials()
        {
            if (!this.passwordEncryptionKeyId.HasValue)
            {
                this.passwordEncryptionKeyId = GenericLocator<IEncryptionKeyDriverFactory>.Factory.Create().GenerateKey().Key;
            }

            var encryptedPassword = Drivers.Encryption.EncryptionHelper.EncryptString(this.passwordEncryptionKeyId.Value, this.Password);
            SqlParameter[] parameters = {
                                            new SqlParameter("@AccountId", AccountId),
                                            new SqlParameter("@Username", UserName),
                                            new SqlParameter("@EmployeeId", EmployeeId),
                                            new SqlParameter("@VendorId", VendorId),
                                            new SqlParameter("@EncryptedPassword", encryptedPassword),
                                            new SqlParameter("@EncryptionKeyId", this.passwordEncryptionKeyId.Value.Value)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "APPRAISAL_VENDOR_SaveLogin", 3, parameters);
        }
    }
}
