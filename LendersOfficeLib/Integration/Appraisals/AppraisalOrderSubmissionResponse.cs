﻿namespace LendersOffice.Integration.Appraisals
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Xml;
    using LQBAppraisal.LQBAppraisalResponse;
    using ObjLib.Conversions.GlobalDMS;

    /// <summary>
    /// Represents a appraisal order submission response.
    /// </summary>
    public class AppraisalOrderSubmissionResponse : ObjLib.Webservices.ServiceResult
    {
        /// <summary>
        /// The order id.
        /// </summary>
        private string orderId;

        /// <summary>
        /// The order number from appraisal vendor.
        /// </summary>
        private string orderNumber;

        /// <summary>
        /// Prevents a default instance of the <see cref="AppraisalOrderSubmissionResponse" /> class from being created.
        /// </summary>
        private AppraisalOrderSubmissionResponse()
        {
        }

        /// <summary>
        /// Creates a error response with the given string.
        /// </summary>
        /// <param name="e">The error string.</param>
        /// <returns>A error appraisal order submission response.</returns>
        public static AppraisalOrderSubmissionResponse CreateError(string e)
        {
            AppraisalOrderSubmissionResponse r = new AppraisalOrderSubmissionResponse();
            r.Status = ObjLib.Webservices.ServiceResultStatus.Error;
            r.AppendError(e);
            return r;
        }

        /// <summary>
        /// Creates an error for Lqb Responses.
        /// </summary>
        /// <param name="errors">The errors.</param>
        /// <returns>An error response.</returns>
        public static AppraisalOrderSubmissionResponse CreateError(List<ServerMessage> errors)
        {
            var r = new AppraisalOrderSubmissionResponse();
            r.Status = ObjLib.Webservices.ServiceResultStatus.Error;

            foreach (var item in errors)
            {
                string errorType = "UnhandledException";
                if (Enum.IsDefined(typeof(E_ServerMessageType), item.Type))
                {
                    switch (item.Type)
                    {
                        case E_ServerMessageType.DataError:
                            errorType = "Data Error";
                            break;
                        case E_ServerMessageType.LoginError:
                            errorType = "Login Error";
                            break;
                        case E_ServerMessageType.ServerError:
                            errorType = "Server Error";
                            break;
                        case E_ServerMessageType.Other:
                        case E_ServerMessageType.Undefined:
                            errorType = "Warning";
                            break;
                        default:
                            throw new DataAccess.UnhandledEnumException(item.Type);
                    }
                }

                r.AppendError($"({errorType}) {item.Description}");
            }

            return r;
        }

        /// <summary>
        /// Creates a successful response.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <param name="orderNumber">The appraisal vendor order number.</param>
        /// <returns>A response representing success.</returns>
        public static AppraisalOrderSubmissionResponse Success(string orderId, string orderNumber)
        {
            AppraisalOrderSubmissionResponse r = new AppraisalOrderSubmissionResponse();
            r.Status = ObjLib.Webservices.ServiceResultStatus.OK;
            r.orderId = orderId;
            r.orderNumber = orderNumber;
            return r;
        }

        /// <summary>
        /// Creates a error response for a Gdms failure.
        /// </summary>
        /// <param name="e">The gdms exception.</param>
        /// <returns>A error response with the gdms failure.</returns>
        public static AppraisalOrderSubmissionResponse CreateError(GDMSErrorResponseException e)
        {
            AppraisalOrderSubmissionResponse r = new AppraisalOrderSubmissionResponse();
            r.Status = ObjLib.Webservices.ServiceResultStatus.Error;

            StringBuilder errorMsg = new StringBuilder();
            errorMsg.AppendLine("There was an error processing your request.");
            errorMsg.AppendFormat(" {0}.", e.ErrorMessage);

            foreach (string error in e.Errors)
            {
                errorMsg.AppendFormat(" {0}.", error);
            }

            r.AppendError(errorMsg.ToString());
            return r;
        }

        /// <summary>
        /// Gets the error list.
        /// </summary>
        /// <returns>The error list.</returns>
        public IReadOnlyCollection<string> GetErrorList()
        {
            return this.ErrorList;
        }

        /// <summary>
        /// Writes the order number to the writer.
        /// </summary>
        /// <param name="writer">The xml writer.</param>
        protected override void WriteResponseResult(XmlWriter writer)
        {
            if (this.Status == ObjLib.Webservices.ServiceResultStatus.Error)
            {
                return;
            }

            writer.WriteStartElement("appraisalsubmitorder");
            writer.WriteStartElement("field");
            writer.WriteAttributeString("id", "appraisalorderid");
            writer.WriteValue(this.orderId);
            writer.WriteEndElement();
            writer.WriteStartElement("field");
            writer.WriteAttributeString("id", "ordernumber");
            writer.WriteValue(this.orderNumber);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
    }
}
