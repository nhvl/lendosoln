﻿namespace LendersOffice.Integration.Appraisals
{
    /// <summary>
    /// Represents the type of response.
    /// </summary>
    public enum ResponseType
    {
        /// <summary>
        /// Marks the response as an error response.
        /// </summary>
        Error = 0,

        /// <summary>
        /// Marks the response as a Lqb response.
        /// </summary>
        LQB = 1,

        /// <summary>
        /// Mars the response as Gdms.
        /// </summary>
        GDMS = 2
    }
}
