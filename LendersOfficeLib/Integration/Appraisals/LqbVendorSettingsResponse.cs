﻿namespace LendersOffice.Integration.Appraisals
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Presents a Lqb vendor settings response.
    /// </summary>
    public class LqbVendorSettingsResponse : AbstractVendorSettingsResponse
    {
        /// <summary>
        /// Gets or sets the list of products.
        /// </summary>
        /// <value>The product list.</value>
        public IEnumerable<AppraisalOrderProduct> ProductList { get; set; }

        /// <summary>
        /// Gets or sets the response type.
        /// </summary>
        /// <value>A hardcoded lqb response.</value>
        public override ResponseType ResponseType
        {
            get
            {
                return ResponseType.LQB;
            }
        }

        /// <summary>
        /// Gets or sets the list of billing methods available.
        /// </summary>
        /// <value>The billing methods available.</value>
        public List<BillingMethodDetails> BillingMethods { get; set; }

        /// <summary>
        /// Gets or sets the id of the vendor.
        /// </summary>
        /// <value>The vendor id.</value>
        public Guid Id { get; set; }
    }
}
