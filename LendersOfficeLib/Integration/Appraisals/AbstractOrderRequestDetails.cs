﻿namespace LendersOffice.Integration.Appraisals
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Holds the data common to all order appraisal request.
    /// </summary>
    public abstract class AbstractOrderRequestDetails
    {
        /// <summary>
        /// Gets or sets the name of the contact for the appraisal order.
        /// </summary>
        /// <value>The contact name.</value>
        public string ContactName { get; set; }

        /// <summary>
        /// Gets or sets the phone of the contact for the appraisal order.
        /// </summary>
        /// <value>The contact phone.</value>
        public string ContactPhone { get; set; }

        /// <summary>
        /// Gets or sets the work phone of the contact for the appraisal order.
        /// </summary>
        /// <value>The contact work phone number.</value>
        public string ContactPhoneWork { get; set; }

        /// <summary>
        /// Gets or sets the other phone of the contact for the appraisal order.
        /// </summary>
        /// <value>The contact other phone number.</value>
        public string ContactPhoneOther { get; set; }

        /// <summary>
        /// Gets or sets the email of the contact for the appraisal order.
        /// </summary>
        /// <value>The contact's email.</value>
        public string ContactEmail { get; set; }

        /// <summary>
        /// Gets or sets the additional email of the contact for the appraisal order.
        /// </summary>
        /// <value>The contact's additional email.</value>
        public string ContactAditionalEmail { get; set; }

        /// <summary>
        /// Gets or sets the date the appraisal is needed by.
        /// </summary>
        /// <value>The date the appraisal is needed by.</value>
        public DateTime? AppraisalNeededBy { get; set; }

        /// <summary>
        /// Gets or sets the billing data associated with the request.
        /// </summary>
        /// <value>A plain object holding billing details.</value>
        public BillingDetails Billing { get; set; }

        /// <summary>
        /// Gets or sets the notes associated with the request.
        /// </summary>
        /// <value>The order notes.</value>
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets a list of documents that are to be attached to the request.
        /// </summary>
        /// <value>A list of document ids that are to be attached ot the order.</value>
        public List<Guid> DocumentIds { get; set; }

        /// <summary>
        /// Gets or sets the loan name for the order.
        /// </summary>
        /// <value>The loan name.</value>
        public string LoanName { get; set; }

        /// <summary>
        /// Gets or sets the custom order credentials. If this is not set 
        /// the saved user credentials will be used.
        /// </summary>
        /// <value>The credentials.</value>
        public Credential Credentials { get; set;  }

        /// <summary>
        /// Gets or sets the app id associated with the chosen property.
        /// This should be the primary app if no REO is chosen.
        /// It will be the REO's app if an REO is chosen.
        /// </summary>
        public Guid AppIdAssociatedWithProperty { get; set; }

        /// <summary>
        /// Gets or sets the REO id that is to be linked with this appraisal order.
        /// </summary>
        public Guid? LinkedReoId { get; set; }

        /// <summary>
        /// Gets or sets the property info.
        /// </summary>
        public Property Property { get; set; }
    }
}
