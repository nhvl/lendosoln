﻿namespace LendersOffice.Integration.Appraisals
{
    using System.Xml;

    /// <summary>
    /// Responsbile for serializing Gdms setting responses.
    /// </summary>
    public class GdmsVendorSettingsResult : AbstractVendorSettingsResult
    {
        /// <summary>
        /// The Gdms response to be serialized.
        /// </summary>
        private GdmsVendorSettingsResponse response;

        /// <summary>
        /// Initializes a new instance of the <see cref="GdmsVendorSettingsResult" /> class.
        /// </summary>
        /// <param name="response">The response to be serialized.</param>
        public GdmsVendorSettingsResult(GdmsVendorSettingsResponse response)
        {
            this.response = response;
        }

        /// <summary>
        /// Writes the gdms settings response to the Xml writer.
        /// </summary>
        /// <param name="writer">The xml writer in which the Xml should be written to.</param>
        protected override void WriteResponseResult(XmlWriter writer)
        {
            var o = new[]
            {
                new { name = "PropertyTypes", options = this.response.PropertyTypes },
                new { name = "ReportTypes", options = this.response.ReportTypes },
                new { name = "IntendedUses", options = this.response.IntendedUses },
                new { name = "LoanTypes", options = this.response.LoanTypes },
                new { name = "OccupancyTypes", options = this.response.OccupancyTypes },
                new { name = "BillingMethods", options = this.response.BillingMethods },
                new { name = "Client2", options = this.response.Client2 },
                new { name = "Processors", options = this.response.Processors },
                new { name = "Processors2", options = this.response.Processors2 },
                new { name = "OrderFileUploadTypes", options = this.response.OrderFileUploadTypes },
            };

            writer.WriteStartElement("appraisalordersettings");
            writer.WriteAttributeString("isgdms", bool.TrueString);
            writer.WriteAttributeString("vendorid", this.response.Id.ToString());

            foreach (var setting in o)
            {
                writer.WriteStartElement("setting");
                writer.WriteAttributeString("name", setting.name);

                foreach (var option in setting.options)
                {
                    writer.WriteStartElement("option");
                    writer.WriteAttributeString("name", option.Description);
                    writer.WriteAttributeString("value", option.Id.ToString());
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }
    }
}
