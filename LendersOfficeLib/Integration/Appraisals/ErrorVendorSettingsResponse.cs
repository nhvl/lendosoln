﻿namespace LendersOffice.Integration.Appraisals
{
    using System.Collections.Generic;
    using System.Text;
    using DataAccess;
    using LendersOffice.ObjLib.Conversions.GlobalDMS;
    using LQBAppraisal.LQBAppraisalResponse;

    /// <summary>
    /// Represents a Error Vendor Setting Response. 
    /// </summary>
    public class ErrorVendorSettingsResponse : AbstractVendorSettingsResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorVendorSettingsResponse" /> class.
        /// </summary>
        /// <param name="response">The server response to turn into a response.</param>
        public ErrorVendorSettingsResponse(ServerResponse response)
        {
            this.Message = this.GetMessage(response);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorVendorSettingsResponse" /> class.
        /// </summary>
        /// <param name="e">The error to convert.</param>
        public ErrorVendorSettingsResponse(GDMSErrorResponseException e)
        {
            StringBuilder errorMsg = new StringBuilder();
            errorMsg.AppendLine("There was an error processing your request.");
            errorMsg.AppendFormat(" {0}.", e.ErrorMessage);
            foreach (string error in e.Errors)
            {
                errorMsg.AppendFormat(" {0}.", error);
            }

            this.Message = errorMsg.ToString();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ErrorVendorSettingsResponse" /> class.
        /// </summary>
        /// <param name="error">The error message to turn into a response.</param>
        public ErrorVendorSettingsResponse(string error)
        {
            this.Message = error;
        }

        /// <summary>
        /// Gets the response type for this setting response.
        /// </summary>
        /// <value>The response type for this response.</value>
        public override ResponseType ResponseType
        {
            get
            {
                return ResponseType.Error;
            }
        }

        /// <summary>
        /// Gets the message for this response type.
        /// </summary>
        /// <value>The error message.</value>
        public string Message { get; private set; }

        /// <summary>
        /// Gets the message given the Lqb appraisal error response.
        /// </summary>
        /// <param name="exc">The exception to convert to error.</param>
        /// <returns>The error response based on the exception.</returns>
        private string GetMessage(ServerResponse exc)
        {
            if (exc.ServerMessageList.Count == 0)
            {
                return string.Empty;
            }

            StringBuilder errorMsg = new StringBuilder();

            if (exc.RequestStatus != E_ServerResponseRequestStatus.Success)
            {
                errorMsg.AppendLine("There was an error processing your request.\n");
            }

            var errors = exc.ServerMessageList;
            errors.Sort(ErrorMessageSortComparer.Instance);

            foreach (ServerMessage error in errors)
            {
                string errorType;
                switch (error.Type)
                {
                    case E_ServerMessageType.DataError:
                        errorType = "Data Error";
                        break;
                    case E_ServerMessageType.LoginError:
                        errorType = "Login Error";
                        break;
                    case E_ServerMessageType.ServerError:
                        errorType = "Server Error";
                        break;
                    case E_ServerMessageType.Other:
                    case E_ServerMessageType.Undefined:
                        errorType = "Warning";
                        break;
                    default:
                        throw new UnhandledEnumException(error.Type);
                }

                errorMsg.AppendFormat("({0}) {1}\n", errorType, error.Description);
            }

            return errorMsg.ToString();
        }

        /// <summary>
        /// Sorts ServeMessages.
        /// </summary>
        private class ErrorMessageSortComparer : IComparer<ServerMessage>
        {
            /// <summary>
            /// Gets a singleton instance of the comparer.
            /// </summary>
            public static readonly ErrorMessageSortComparer Instance = new ErrorMessageSortComparer();

            /// <summary>
            /// Gets a number indicating the difference between x and y.
            /// </summary>
            /// <param name="x">The first object to compare.</param>
            /// <param name="y">The second object to compare.</param>
            /// <returns>Zero if equal, one if y is greater and -1 if otherwise.</returns>
            public int Compare(ServerMessage x, ServerMessage y)
            {
                var sortOrder = new List<E_ServerMessageType> { E_ServerMessageType.ServerError, E_ServerMessageType.LoginError, E_ServerMessageType.DataError, E_ServerMessageType.Other, E_ServerMessageType.Undefined };

                int xi = sortOrder.IndexOf(x.Type);

                int yi = sortOrder.IndexOf(y.Type);

                if (xi == -1)
                {
                    xi = sortOrder.Count;
                }

                if (yi == -1)
                {
                    yi = sortOrder.Count;
                }

                return xi.CompareTo(yi);
            }
        }
    }
}
