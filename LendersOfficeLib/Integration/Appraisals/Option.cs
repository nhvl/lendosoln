﻿namespace LendersOffice.Integration.Appraisals
{
    /// <summary>
    /// Represents a Gdms option for fields that are required 
    /// to order Gdms appraisals.
    /// </summary>
    public class Option
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Option" /> class.
        /// </summary>
        /// <param name="id">The id of the option.</param>
        /// <param name="description">The friendly user description.</param>
        public Option(int id, string description)
        {
            this.Id = id;
            this.Description = description;
        }

        /// <summary>
        /// Gets or sets the id of the option.
        /// </summary>
        /// <value>The id of the option.</value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the user description of the option.
        /// </summary>
        /// <value>The friendly user description.</value>
        public string Description { get; set; }
    }
}
