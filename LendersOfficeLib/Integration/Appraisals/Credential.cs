﻿namespace LendersOffice.Integration.Appraisals
{
    /// <summary>
    /// Represents account credentials for appraisal orders.
    /// </summary>
    public class Credential
    {
        /// <summary>
        /// Gets or sets the account identifier.
        /// </summary>
        /// <value>The account identifier.</value>
        public string AccountId { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        /// <value>The user name.</value>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the password for the order.
        /// </summary>
        /// <value>The account password.</value>
        public string Password { get; set; }
    }
}
