﻿namespace LendersOffice.Integration.Appraisals
{
    /// <summary>
    /// Container for appraisal billing order details.
    /// </summary>
    public class BillingDetails
    {
        /// <summary>
        /// Gets or sets the billing name.
        /// </summary>
        /// <value>The billing name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the billing address.
        /// </summary>
        /// <value>The billing street.</value>
        public string Street { get; set; }

        /// <summary>
        /// Gets or sets the billing city.
        /// </summary>
        /// <value>The billing city.</value>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the billing state.
        /// </summary>
        /// <value>The billing state.</value>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the zip code.
        /// </summary>
        /// <value>The billing zipcode.</value>
        public string Zip { get; set; }

        /// <summary>
        /// Gets or sets the billing credit card type.
        /// </summary>
        /// <value>The billing credit card type.</value>
        public string CreditCardType { get; set; }

        /// <summary>
        /// Gets or sets the credit card number.
        /// </summary>
        /// <value>The billing credit card number.</value>
        public string CreditCardNumber { get; set; }

        /// <summary>
        /// Gets or sets the credit card expiration month.
        /// </summary>
        /// <value>The expiration month.</value>
        public int CreditCardExpirationMonth { get; set; }

        /// <summary>
        /// Gets or sets the credit card expiration year.
        /// </summary>
        /// <value>The credit card expiration year.</value>
        public int CreditCardExpirationYear { get; set; }

        /// <summary>
        /// Gets or sets the security code.
        /// </summary>
        /// <value>The security code.</value>
        public string CreditCardSecurityCode { get; set; }
    }
}
