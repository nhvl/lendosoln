namespace LendersOffice.Integration.Appraisals
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    using AuthTicket;
    using ConfigSystem;
    using ConfigSystem.Operations;
    using DataAccess;
    using EDocs;
    using GDMS.Orders;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Conversions;
    using LendersOffice.ObjLib.Conversions.GlobalDMS;
    using LendersOffice.Security;
    using LQBAppraisal.LQBAppraisalRequest;
    using LQBAppraisal.LQBAppraisalResponse;
    using ObjLib.Conversions.LQBAppraisal;

    public class AppraisalVendorFactory
    {
        public static IEnumerable<AppraisalVendorConfig> GetAvailableVendorsForBroker(Guid brokerId)
        {
            var allVendors = AppraisalVendorConfig.ListAll();
            var vendorsForBrokerById = new List<Guid>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "APPRAISAL_VENDOR_VendorsForBroker", parameters))
            {
                while (reader.Read())
                {
                    vendorsForBrokerById.Add((Guid)reader["VendorId"]);
                }
            }

            return allVendors.Where(v => vendorsForBrokerById.Contains(v.VendorId));
        }

        public static AbstractVendorSettingsResponse GetVendorSettings(AbstractUserPrincipal principal, Guid sLId, Guid vendorId, Credential creds)
        {

            if (!principal.HasPermission(Permission.AllowOrderingGlobalDMSAppraisals))
            {
                return new ErrorVendorSettingsResponse("User does not have permission to order appraisals.");
            }

            var items = AppraisalVendorFactory.GetAvailableVendorsForBroker(principal.BrokerId);
            var vendor = items.FirstOrDefault(p => p.VendorId == vendorId);

            if (vendor == null)
            {
                return new ErrorVendorSettingsResponse("Vendor not found.");
            }

            if (vendor.UsesGlobalDMS)
            {
                return GetGdmsVendorSettings(principal, sLId, vendor);
            }

            AppraisalVendorCredentials lqbCreds = null;

            if (creds != null)
            {
                lqbCreds = new AppraisalVendorCredentials(principal.BrokerId, vendorId, creds.AccountId, creds.Username, creds.Password);
            }

            return GetLqbVendorSettings(principal, sLId, vendor, lqbCreds);
        }

        private static AbstractVendorSettingsResponse GetGdmsVendorSettings(AbstractUserPrincipal principal, Guid sLId, AppraisalVendorConfig vendor)
        {
            string errorMessage;
            GDMSCredentials credentials = vendor.GetGDMSCredentials(principal.BrokerId, principal.EmployeeId, out errorMessage);

            if (credentials == null)
            {
                return new ErrorVendorSettingsResponse(errorMessage);
            }

            GdmsVendorSettingsResponse response = new GdmsVendorSettingsResponse();
            response.Id = vendor.VendorId;

            using (var lookupMethodsClient = new LookupMethodsClient(credentials))
            {
                try
                {
                    var propertyTypes = lookupMethodsClient.GetPropertyTypes();
                    var products = lookupMethodsClient.GetProducts();
                    var intendedUses = lookupMethodsClient.GetIntendedUses();
                    var loanTypes = lookupMethodsClient.GetLoanTypes();
                    var occupancyTypes = lookupMethodsClient.GetOccupancyTypes();

                    var client2Users = lookupMethodsClient.GetClient2Users();
                    var clientUsers = lookupMethodsClient.GetClientUsers(0);

                    var billingMethods = lookupMethodsClient.GetBillingMethods();

                    var orderFileUploadFileTypes = lookupMethodsClient.GetOrderFileUploadFileTypes();
                    orderFileUploadFileTypes = Array.FindAll(orderFileUploadFileTypes, o => o.ViewableByClient);

                    response.PropertyTypes = propertyTypes.Select(o => new Option(o.PropertyTypeId, o.PropertyTypeName)).ToList();
                    response.ReportTypes = products.Select(o => new Option(o.ProductID, o.ProductFriendlyName)).ToList();
                    response.IntendedUses = intendedUses.Select(o => new Option(o.LoanTypeId, o.LoanTypeName)).ToList();
                    response.LoanTypes = loanTypes.Select(o => new Option(o.LoanTypeId, o.LoanTypeName)).ToList();
                    response.OccupancyTypes = occupancyTypes.Select(o => new Option(o.OccupancyID, o.OccupancyName)).ToList();
                    response.BillingMethods = billingMethods.Select(o => new Option(o.BillingMethodId, o.BillingMethodName)).ToList();
                    response.Client2 = client2Users.Select(o => new Option(o.ClientID, o.ClientName)).ToList();
                    response.Processors = clientUsers.Select(o => new Option(o.ClientUserID, o.UserFullName)).ToList();
                    response.Processors2 = clientUsers.Select(o => new Option(o.ClientUserID, o.UserFullName)).ToList();
                    response.OrderFileUploadTypes = orderFileUploadFileTypes.Select(o => new Option(o.FileTypesId, o.FileTypeDescription)).ToList();
                }

                catch (GDMSErrorResponseException e)
                {
                    Tools.LogError(e);
                    return new ErrorVendorSettingsResponse(e);
                }
            }

            return response;
        }

        private static AbstractVendorSettingsResponse GetLqbVendorSettings(AbstractUserPrincipal principal, Guid sLId, AppraisalVendorConfig vendor, AppraisalVendorCredentials customCreds)
        {
            AppraisalVendorCredentials cred = customCreds ?? new AppraisalVendorCredentials(principal.BrokerId, principal.EmployeeId, vendor.VendorId);

            if (cred.AccountId.Equals(string.Empty) && cred.UserName.Equals(string.Empty) && cred.Password.Equals(string.Empty))
            {
                return new ErrorVendorSettingsResponse("Credentials not found.");
            }

            LQBAppraisalRequest request = LQBAppraisalExporter.CreateAccountInfoRequest(cred, sLId, vendor.VendorName);
            LQBAppraisalResponse response = AppraisalVendorServer.Submit(request, vendor.PrimaryExportPath);

            if (response.ServerResponse.RequestStatus != E_ServerResponseRequestStatus.Success)
            {
                return new ErrorVendorSettingsResponse(response.ServerResponse);
            }

            LqbVendorSettingsResponse settings = new LqbVendorSettingsResponse();
            List<AppraisalOrderProduct> products = new List<AppraisalOrderProduct>();

            foreach (AvailableProduct prod in response.AccountInfoResponse.AvailableProductList)
            {
                products.Add(new AppraisalOrderProduct(prod.ProductName, ""));
                foreach (AvailableSecondaryProduct prod2 in prod.AvailableSecondaryProductList)
                {
                    products.Add(new AppraisalOrderProduct(prod2.ProductName, prod.ProductName));
                }
            }

            var billingMethods = response.AccountInfoResponse.BillingMethodList.Select(method => new BillingMethodDetails(method)).ToList();
            settings.BillingMethods = billingMethods;
            settings.ProductList = products;
            settings.Id = vendor.VendorId;

            return settings;
        }

        private static bool UserHasOrderAppraisalPrivilege(AbstractUserPrincipal principal, Guid LoanId, out string errorMessage)
        {
            errorMessage = string.Empty;

            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(PrincipalFactory.CurrentPrincipal.ConnectionInfo, PrincipalFactory.CurrentPrincipal.BrokerId, LoanId, WorkflowOperations.OrderAppraisal);
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(PrincipalFactory.CurrentPrincipal));

            bool bHasPrivilege = LendingQBExecutingEngine.CanPerform(WorkflowOperations.OrderAppraisal, valueEvaluator);

            if (!bHasPrivilege)
            {
                errorMessage = LendingQBExecutingEngine.GetUserFriendlyMessage(WorkflowOperations.OrderAppraisal, valueEvaluator);
            }

            return bHasPrivilege;
        }

        public static AppraisalOrderSubmissionResponse SubmitOrder(AbstractUserPrincipal principal, Guid sLId, AbstractOrderRequestDetails details, AppraisalVendorConfig vendor)
        {
            if (!principal.HasPermission(Permission.AllowOrderingGlobalDMSAppraisals))
            {
                return AppraisalOrderSubmissionResponse.CreateError("User does not have permission to order appraisals.");
            }

            string workflowError;
            if (!UserHasOrderAppraisalPrivilege(principal, sLId, out workflowError))
            {
                return AppraisalOrderSubmissionResponse.CreateError(workflowError);
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(AppraisalVendorFactory));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            AppraisalOrderSubmissionResponse response;

            CAppData associatedApp = null;
            E_BorrowerModeT borrowerForProperty = E_BorrowerModeT.Borrower;
            if (details.LinkedReoId.HasValue)
            {
                for (int i = 0; i < dataLoan.nApps; i++)
                {
                    var app = dataLoan.GetAppData(i);
                    IRealEstateOwned reo = app.aReCollection.GetRecordOf(details.LinkedReoId.Value) as IRealEstateOwned;

                    if (reo != null)
                    {
                        borrowerForProperty = reo.ReOwnerT == E_ReOwnerT.CoBorrower ? E_BorrowerModeT.Coborrower : E_BorrowerModeT.Borrower;
                        associatedApp = app;
                        break;
                    }
                }

                if (associatedApp == null)
                {
                    return AppraisalOrderSubmissionResponse.CreateError($"Unable to populate property address from REO with id {details.LinkedReoId.Value.ToString()}.");
                }
            }
            else
            {
                associatedApp = dataLoan.GetAppData(0);
            }

            details.AppIdAssociatedWithProperty = associatedApp.aAppId;
            FixupPropertyAddress(dataLoan, associatedApp, details);

            if (vendor.AppraisalNeededDateOption == E_AppraisalNeededDateOptions.Required && (!details.AppraisalNeededBy.HasValue || details.AppraisalNeededBy.Value == default(DateTime)))
            {
                return AppraisalOrderSubmissionResponse.CreateError("Appraisal needed date required.");
            }

            if (vendor.UsesGlobalDMS)
            {
                var gdmsSettings = details as GdmsOrderRequestDetails;
                response = SubmitGdmsOrder(principal, dataLoan, associatedApp, borrowerForProperty, vendor, gdmsSettings);
            }
            else
            {
                var lqbSettings = details as LqbOrderRequestDetails;
                response = SubmitLqbOrder(principal, dataLoan, associatedApp, vendor, lqbSettings);
            }

            if (response.Status != ObjLib.Webservices.ServiceResultStatus.OK)
            {
                return response;
            }

            if (details.AppraisalNeededBy.HasValue)
            {
                dataLoan.sApprRprtDueD_rep = details.AppraisalNeededBy.Value.ToShortDateString();
            }

            // If this is the first appraisal order, set sApprRprtOd to today
            if (string.IsNullOrEmpty(dataLoan.sApprRprtOd_rep))
            {
                dataLoan.sApprRprtOd_rep = DateTime.Now.ToShortDateString();
            }

            dataLoan.Save();

            return response;
        }

        private static AppraisalOrderSubmissionResponse SubmitGdmsOrder(AbstractUserPrincipal principal, CPageData dataLoan, CAppData associatedApp, E_BorrowerModeT borrowerForProperty, AppraisalVendorConfig vendor, GdmsOrderRequestDetails details)
        {
            var order = GetClientOrder(dataLoan, associatedApp, borrowerForProperty, details);

            string errorMessage;
            GDMSCredentials credentials = vendor.GetGDMSCredentials(principal.BrokerId, principal.EmployeeId, out errorMessage);

            if (credentials == null)
            {
                return AppraisalOrderSubmissionResponse.CreateError(errorMessage);
            }

            try
            {
                PopulateClientInformation(order, credentials);
                var d = Common.SerializationHelper.JsonNetAnonymousSerialize(order);
                int fileNumber;

                using (var ordersClient = new LendersOffice.ObjLib.Conversions.GlobalDMS.OrdersClient(credentials))
                {
                    string response = ordersClient.AddClientOrder(order);
                    if (!int.TryParse(response, out fileNumber))
                    {
                        Tools.LogError($"Could not parse file number. Got {response}");
                        return AppraisalOrderSubmissionResponse.CreateError("Error submitting order.");
                    }
                }

                // Create a new order appraisal item and save it to the DB
                var orderInfo = new GDMSAppraisalOrderInfo(fileNumber, dataLoan.sLId, principal.BrokerId, principal.UserId, principal.EmployeeId, vendor.VendorId, details);
                orderInfo.ProductName = order.ProductName;
                orderInfo.OrderedDate = CDateTime.Create(DateTime.Now);
                orderInfo.NeededDate_rep = order.DateNeeded_String.Split(' ')[0];
                orderInfo.Save();

                string error;
                AttachGDMSDocumentsToOrder(credentials, fileNumber, details.DocumentIds, details.OrderFileUploadTypeId, out error);

                return AppraisalOrderSubmissionResponse.Success(orderInfo.AppraisalOrderId.ToString(), fileNumber.ToString());
            }
            catch (GDMSErrorResponseException e)
            {
                Tools.LogError(e);
                return AppraisalOrderSubmissionResponse.CreateError(e);
            }
        }

        private static bool AttachGDMSDocumentsToOrder(GDMSCredentials credentials, int fileNumber, List<Guid> docIds, int attachmentType, out string errors)
        {
            errors = string.Empty;
            if (docIds == null || docIds.Count == 0)
            {
                return true;
            }

            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            StringBuilder errorList = new StringBuilder();
            List<string> filePathsToClean = new List<string>();
            foreach (Guid docId in docIds)
            {
                EDocument edoc = repo.GetDocumentById(docId);
                string fileName = edoc.FolderAndDocTypeName + ".pdf";
                //need to make sure fileName has no invalid characters for a file name; otherwise GDMS rejects the file
                fileName = fileName.Replace('/', '-').Replace('\\', '-').Replace('?', '-').Replace('%', '-').Replace('*', '-')
                        .Replace(':', '-').Replace('|', '-').Replace('"', '-').Replace('<', '-').Replace('>', '-');
                string filePath = edoc.GetPDFTempFile_Current();
                filePathsToClean.Add(filePath);

                using (Stream stream = File.OpenRead(filePath))
                using (var fileUploadClient = new FileUploadClient(credentials, fileName, fileNumber, attachmentType, stream))
                {
                    try
                    {
                        var uploadResults = fileUploadClient.SubmitReport();
                        if (!uploadResults.success)
                        {
                            if (errorList.Length != 0) errorList.AppendLine();
                            {
                                errorList.AppendFormat("{0}:\n\t{1} did not upload properly.", uploadResults.message, fileName);
                            }
                        }
                    }
                    catch (Exception exc)
                    {
                        Tools.LogError(string.Format("Unable to upload file: {0} to ETrac.\n", fileName), exc);
                        if (errorList.Length != 0)
                        {
                            errorList.AppendLine();
                        }
                        errorList.AppendFormat("Unable to upload file {0}. Please contact support if this happens again.", fileName);
                    }
                }
            }
            errors = errorList.ToString();
            return true;
        }

        private static void PopulateClientInformation(ClientOrder order, GDMSCredentials credentials)
        {
            using (var lookupClient = new LookupMethodsClient(credentials))
            {
                var clientInfo = lookupClient.GetClientInfo(0);
                order.ClientAddress1 = clientInfo.ClientAddress1;
                order.ClientCSZ = string.Format("{0}, {1} {2}", clientInfo.ClientCity, clientInfo.ClientState, clientInfo.ClientZip);
                order.ClientCity = clientInfo.ClientCity;
                order.ClientFax = clientInfo.ClientFax;
                order.ClientID = clientInfo.ClientID;
                order.ClientName = clientInfo.ClientName;
                order.ClientPhone1 = clientInfo.ClientPhone1;
                order.ClientState = clientInfo.ClientState;
                order.ClientZip = clientInfo.ClientZip;

                order.OrderedBy = clientInfo.ClientName;
                order.OrderedByEmailAddress = clientInfo.ClientEmail;

                // 10/15/2013 gf - opm 141181 use ClientUserId instead of ClientId
                var clientUser = lookupClient.GetClientUsers(0)
                    .Where(user => user.UserName.Trim().Equals(credentials.UserName.Trim(), StringComparison.CurrentCultureIgnoreCase))
                    .FirstOrDefault();
                order.OrderedByID = clientUser.ClientUserID;
            }
        }

        private static void SetIfValid(int optionId, params Action<int>[] items)
        {
            if (optionId != -1)
            {
                foreach (var item in items)
                {
                    item(optionId);
                }
            }
        }

        private static void FixupPropertyAddress(CPageData dataLoan, CAppData associatedApp, AbstractOrderRequestDetails details)
        {
            if (details.Property == null)
            {
                // Property address not specified. Probably from webservices. We'll pull from either sSpAddr and friends or the REO if specified.
                details.Property = new Property();
                if (details.LinkedReoId.HasValue)
                {
                    IRealEstateOwned reo = associatedApp.aReCollection.GetRecordOf(details.LinkedReoId.Value) as IRealEstateOwned;
                    details.Property.Address = reo.Addr;
                    details.Property.City = reo.City;
                    details.Property.State = reo.State;
                    details.Property.Zip = reo.Zip;
                }
                else
                {
                    details.Property.Address = dataLoan.sSpAddr;
                    details.Property.City = dataLoan.sSpCity;
                    details.Property.State = dataLoan.sSpState;
                    details.Property.Zip = dataLoan.sSpZip;
                }
            }
        }

        private static ClientOrder GetClientOrder(CPageData dataLoan, CAppData associatedApp, E_BorrowerModeT borrowerForProperty, GdmsOrderRequestDetails details)
        {
            var order = new LendersOffice.GDMS.Orders.ClientOrder();

            order.LoanNumber = dataLoan.sLNm;
            order.CaseNumber = details.CaseNumber;

            order.Address1 = details.Property.Address;
            order.City = details.Property.City;
            order.State = details.Property.State;
            order.Zip = details.Property.Zip;

            order.BorrowerName = borrowerForProperty == E_BorrowerModeT.Borrower ? associatedApp.aBNm : associatedApp.aCNm;
            order.BorrowerEmail = borrowerForProperty == E_BorrowerModeT.Borrower ? associatedApp.aBEmail : associatedApp.aCEmail;
            order.CoBorrowerName = borrowerForProperty == E_BorrowerModeT.Borrower ? associatedApp.aCNm : associatedApp.aBNm;
            order.County = dataLoan.sSpCounty;
            order.FHAFlag = (dataLoan.sLT == E_sLT.FHA);
            order.LegalDescription = dataLoan.sSpLegalDesc;
            order.ContactName = details.ContactName;
            order.ContactNumber = details.ContactPhone;
            order.ContactWorkNumber = details.ContactPhoneWork;
            order.ContactOtherNumber = details.ContactPhoneOther;
            order.SalePrice = dataLoan.sPurchPrice;
            order.SettlementDate_String = dataLoan.sConsummationD_rep;
            order.LoanAmount = dataLoan.sLAmtCalc;

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.Til, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            order.TILDate_String = preparer.PrepareDate_rep;

            SetIfValid(details.PropertyTypeId, x => order.PropertyTypeID = x, x => order.PropertyTypeName = details.PropertyTypeName);

            order.ProductName = details.ReportTypeName;
            SetIfValid(details.ReportType1Id, o => order.ProductID = o);
            SetIfValid(details.ReportType2Id, o => order.ProductID2 = o);
            SetIfValid(details.ReportType3Id, o => order.ProductID3 = o);
            SetIfValid(details.ReportType4Id, o => order.ProductID4 = o);
            SetIfValid(details.ReportType5Id, o => order.ProductID5 = o);

            order.DateNeeded_String = details.AppraisalNeededBy?.ToShortDateString() ?? string.Empty;

            SetIfValid(details.Client2Id, o => order.ClientID2 = o);
            SetIfValid(details.ProcessorId, o => order.ProcessorID = o);
            SetIfValid(details.Processor2Id, o => order.ProcessorID2 = o);

            order.LoanTypeName = details.IntendedUseName;
            order.LoanTypeID = details.IntendedUseId;

            order.LoanType2Name = details.LoanTypeName;
            order.LoanTypeID2 = details.LoanTypeId;

            order.OccupancyName = details.OccupancyTypeName;
            order.OccupancyID = details.OccupancyTypeId;

            order.LenderName = details.LenderName;
            order.Notes = details.Notes;

            
            SetIfValid(details.BillingMethodId, o => order.BillingMethodID = o, x => order.BillingMethodName = details.BillingMethodDescription);
            if (details.BillingMethodDescription.ToLower().Contains("credit card"))
            {
                order.CCBillName = details.Billing.Name;
                order.CCAddress1 = details.Billing.Street;
                order.CCCity = details.Billing.City;
                order.CCState = details.Billing.State;
                order.CCZip = details.Billing.Zip;
                order.CCType = GetGdmsCcType(details.Billing.CreditCardType); // opm 126671
                order.CCNumber = details.Billing.CreditCardNumber;
                order.CCExpMonth = details.Billing.CreditCardExpirationMonth;
                order.CCExpYear = details.Billing.CreditCardExpirationYear;
                order.CCCode = details.Billing.CreditCardSecurityCode;
            }

            return order;
        }

        public static string GetGdmsCcType(string cctypeEnumValue) 
        {
            CommonLib.CreditCardType val;
            try
            {
                val = (CommonLib.CreditCardType)Enum.Parse(typeof(CommonLib.CreditCardType), cctypeEnumValue);
            }
            catch (Exception e)
            {
                Tools.LogError("unhandle cctype " + cctypeEnumValue, e);
                return cctypeEnumValue;
            }
            switch (val)
            {
                case CommonLib.CreditCardType.AmericanExpress:
                    return "Amex";
                case CommonLib.CreditCardType.Discover:
                    return "Discover";
                case CommonLib.CreditCardType.MasterCard:
                    return "Mastercard";
                case CommonLib.CreditCardType.Visa:
                    return "Visa";
                default:
                    Tools.LogError("unhandle cctype " + cctypeEnumValue);
                    return cctypeEnumValue;
            }

        }

        private static AppraisalOrderSubmissionResponse SubmitLqbOrder(AbstractUserPrincipal principal, CPageData dataLoan, CAppData associatedApp, AppraisalVendorConfig vendor, LqbOrderRequestDetails requestDetails)
        {
            AppraisalOrder order = new AppraisalOrder();

            order.LoanInfo = GetLoanInfo(dataLoan, vendor);
            PopulateAuthTicket(vendor, order.LoanInfo, dataLoan, principal);
            order.BorrowerInfoList.AddRange(GetBorrowers(dataLoan));
            if (order.BorrowerInfoList.Count == 0)
            {
                return AppraisalOrderSubmissionResponse.CreateError("No valid borrower found");
            }

            order.PropertyInfo = GetPropertyInfo(dataLoan, associatedApp, requestDetails);
            order.ContactInfo = GetContactInfo(requestDetails);
            order.OrderInfo = GetOrderInfo(requestDetails);
            order.Billing = GetBilling(requestDetails);
            
            foreach(var item in requestDetails.CustomDataPoints)
            {
                order.CustomDataList.Add(new CustomData(item.Key, item.Value));
            }

            try
            {
                order.EmbeddedDocList.AddRange(GetDocuments(principal, dataLoan.sLId, requestDetails.DocumentIds));
            }
            catch (LendersOffice.Common.NotFoundException)
            {
                return AppraisalOrderSubmissionResponse.CreateError("Documents not found.");
            }

            if (string.IsNullOrEmpty(requestDetails.OrderNumber))
            {
                order.OrderType = E_AppraisalOrderOrderType.NewOrder;
            }
            else
            {
                order.OrderType = E_AppraisalOrderOrderType.UpdateOrder;
                order.OrderNumber = requestDetails.OrderNumber;
            }

            var cred =  requestDetails.GetCredentials(principal.BrokerId, vendor.VendorId) ??  new AppraisalVendorCredentials(principal.BrokerId, principal.EmployeeId, vendor.VendorId);
            if (cred == null)
            {
                // This is the best we can do for credential checks since some vendors don't have any credentials at all (ex: Valutrac)
                return AppraisalOrderSubmissionResponse.CreateError("Credentials not found.");
            }

            var request = LQBAppraisalExporter.CreateAppraisalOrderRequest(cred, order);
            var response = AppraisalVendorServer.Submit(request, vendor.PrimaryExportPath);

            if (response.ServerResponse.RequestStatus == E_ServerResponseRequestStatus.Failure)
            {
                return AppraisalOrderSubmissionResponse.CreateError(response.ServerResponse.ServerMessageList);
            }
            
            LQBAppraisalOrder lqbSubmittedOrder = null;
            if (string.IsNullOrEmpty(requestDetails.OrderNumber) || (lqbSubmittedOrder = LQBAppraisalOrder.Load(dataLoan.sLId.ToString(), requestDetails.OrderNumber)) == null)
            {
                lqbSubmittedOrder = new LQBAppraisalOrder(dataLoan.sLId, principal.BrokerId, vendor.VendorId, response.OrderConfirmation.OrderNumber);
            }

            lqbSubmittedOrder.AppraisalOrder = order;
            lqbSubmittedOrder.EmbeddedDocumentIds = requestDetails.DocumentIds;
            lqbSubmittedOrder.OrderedDate = CDateTime.Create(DateTime.Now);
            lqbSubmittedOrder.SetPropertyInfo(requestDetails);
            lqbSubmittedOrder.Save();

            return AppraisalOrderSubmissionResponse.Success(lqbSubmittedOrder.AppraisalOrderId.ToString(), lqbSubmittedOrder.OrderNumber);
        }

        private static List<EmbeddedDoc> GetDocuments(AbstractUserPrincipal principal, Guid sLId, IEnumerable<Guid> documentIds)
        {
            if (documentIds == null || documentIds.Count() <= 0)
            {
                return new List<EmbeddedDoc>();
            }

            List<EmbeddedDoc> docs = new List<EmbeddedDoc>();

            EDocumentRepository repo = EDocumentRepository.GetUserRepository();

            foreach (Guid docId in documentIds)
            {
                EDocument edoc = repo.GetDocumentById(docId);
                string path = edoc.GetPDFTempFile_Current();
                byte[] docBytes = File.ReadAllBytes(path);
                EmbeddedDoc embeddedDoc = new EmbeddedDoc();
                embeddedDoc.Content = Convert.ToBase64String(docBytes);
                embeddedDoc.DocumentFormat = E_EmbeddedDocDocumentFormat.PDF;
                embeddedDoc.DocumentName = $"{edoc.Folder.FolderNm} : {edoc.DocType.DocTypeName}";
                docs.Add(embeddedDoc);
            }

            return docs;
        }

        private static bool IsCreditCard(LqbOrderRequestDetails details)
        {
            return details.CreditCardIndicator == E_TriState.Yes ||
                (details.CreditCardIndicator == E_TriState.Blank && details.BillingMethod != null && details.BillingMethod.ToLower().Contains("credit card"));
        }

        private static Billing GetBilling(LqbOrderRequestDetails details)
        {
            var billing = new Billing();
            billing.BillingMethodName = details.BillingMethod;
            if (!IsCreditCard(details))
            {
                return billing;
            }

            var billInfo = new BillingInfo();
            billInfo.BillingName = details.Billing.Name;
            billInfo.BillingStreet = details.Billing.Street;
            billInfo.BillingCity = details.Billing.City;
            billInfo.BillingState = details.Billing.State;
            billInfo.BillingZIP = details.Billing.Zip;

            var cc = new CCInfo();
            cc.CCType = details.Billing.CreditCardType;
            cc.CCNumber = details.Billing.CreditCardNumber;
            cc.CCExpMonth = details.Billing.CreditCardExpirationMonth.ToString();
            cc.CCExpYear = details.Billing.CreditCardExpirationYear.ToString();
            cc.CCSecurityCode = details.Billing.CreditCardSecurityCode;

            billing.BillingInfo = billInfo;
            billing.CCInfo = cc;
            return billing;
        }

        private static OrderInfo GetOrderInfo(LqbOrderRequestDetails details)
        {
            var ordInfo = new OrderInfo();

            ordInfo.AppraisalNeededDate = details.AppraisalNeededBy?.ToString("dd-MM-yyyy") ?? string.Empty;
            ordInfo.RushOrderIndicator = details.IsRushOrder ? LQBAppraisal.E_YNIndicator.Y : LQBAppraisal.E_YNIndicator.N;
            ordInfo.OrderNotes = details.Notes;
            ordInfo.TransactionID = Guid.NewGuid().ToString();

            int maxProductCount = Math.Min(details.Products.Count, 5);
            for (int i = 0; i < maxProductCount; i++)
            {
                var ordP = new OrderedProduct();
                ordP.ProductName = details.Products[i];
                ordInfo.OrderedProductList.Add(ordP);
            }

            return ordInfo;
        }

        private static ContactInfo GetContactInfo(LqbOrderRequestDetails request)
        {
            var contactInfo = new ContactInfo();
            contactInfo.ContactName = request.ContactName;
            contactInfo.ContactPhone = request.ContactPhone;
            contactInfo.ContactWork = request.ContactPhoneWork;
            contactInfo.ContactOther = request.ContactPhoneOther;
            contactInfo.ContactEmail = request.ContactEmail;
            contactInfo.AdditionalEmail = request.ContactAditionalEmail;
            return contactInfo;
        }

        private static void PopulateAuthTicket(AppraisalVendorConfig vendor, LoanInfo loanInfo, CPageData dataLoan, AbstractUserPrincipal principal)
        {
            if (!vendor.SendAuthTicketWithOrders)
            {
                return;
            }

            XElement genericFrameworkUserTicket = AuthServiceHelper.GetGenericFrameworkUserAuthTicket(principal, dataLoan.sLRefNm, null);
            string authTicket = genericFrameworkUserTicket.Attribute("EncryptedTicket").Value;

            if (!string.IsNullOrEmpty(authTicket))
            {
                string siteCode = genericFrameworkUserTicket.Attribute("Site").Value;
                loanInfo.AuthenticationTicket = new GenericFrameworkUserTicket(authTicket, siteCode);
                loanInfo.WebServiceDomain = LendersOffice.Constants.ConstStage.GenericFrameworkWebServiceDomain;
            }
        }

        private static PropertyInfo GetPropertyInfo(CPageData dataLoan, CAppData associatedApp, LqbOrderRequestDetails details)
        {
            var propInfo = new PropertyInfo();
            propInfo.PropertyType = ToLQBAppraisal(dataLoan.sGseSpT);

            propInfo.PropertyAddress = details.Property.Address;
            propInfo.PropertyCity = details.Property.City;
            propInfo.PropertyState = details.Property.State;
            propInfo.PropertyZIP = details.Property.Zip;

            propInfo.PropertyCounty = dataLoan.sSpCounty;
            propInfo.PropertyLegalDescription = dataLoan.sSpLegalDesc;
            propInfo.OccupancyType = ToLQBAppraisal(associatedApp.aOccT);

            propInfo.MhAdvantage = dataLoan.sHomeIsMhAdvantageTri.ToNullableBool();

            return propInfo;
        }

        private static LoanInfo GetLoanInfo(CPageData dataLoan, AppraisalVendorConfig vendor)
        {
            var loanInfo = new LQBAppraisal.LQBAppraisalRequest.LoanInfo();
            loanInfo.LoanNumber = dataLoan.sLNm;
            loanInfo.LoanID = dataLoan.sLId.ToString();
            loanInfo.PostbackUrl = LendersOffice.Constants.ConstStage.AppraisalFrameworkPostBackUrl;
            if (vendor.EnableBiDirectionalCommunication)
            {
                loanInfo.BDCPostbackUrl = ConstStage.CommunicationFrameworkPostBackUrl;
            }

            loanInfo.CaseNumber = dataLoan.sAgencyCaseNum;
            loanInfo.IntendedUse = ToLQBAppraisal(dataLoan.sLPurposeT);
            loanInfo.LoanType = ToLQBAppraisal(dataLoan.sLT);
            loanInfo.PurchasePrice = dataLoan.sPurchPrice_rep;

            var loanOfficerAgent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            loanInfo.LoanOfficerName = loanOfficerAgent.AgentName;
            loanInfo.LoanOfficerEmail = loanOfficerAgent.EmailAddr;

            var loanOfficerAssistant = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficerAssistant, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            loanInfo.LoanOfficerAssistantName = loanOfficerAssistant.AgentName;
            loanInfo.LoanOfficerAssistantEmail = loanOfficerAssistant.EmailAddr;

            var processor = dataLoan.GetAgentOfRole(E_AgentRoleT.Processor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            loanInfo.ProcessorName = processor.AgentName;
            loanInfo.ProcessorEmail = processor.EmailAddr;

            var juniorProcessor = dataLoan.GetAgentOfRole(E_AgentRoleT.JuniorProcessor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            loanInfo.JuniorProcessorName = juniorProcessor.AgentName;
            loanInfo.JuniorProcessorEmail = juniorProcessor.EmailAddr;

            DateTime settleD;
            if (DateTime.TryParse(dataLoan.sConsummationD_rep, out settleD))
            {
                loanInfo.SettlementDate = settleD.ToString("dd-MM-yyyy");
            }

            loanInfo.LoanAmount = dataLoan.sLAmtCalc_rep;
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.Til, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            loanInfo.TILPreparedDate = preparer.PrepareDate.ToString("dd-MM-yyyy");

            return loanInfo;
        }

        private static List<BorrowerInfo> GetBorrowers(CPageData dataLoan)
        {
            List<BorrowerInfo> borrowers = new List<BorrowerInfo>();

            // 1/28/2016 BS - 228886. Include all non-title borrowers in appraisal order
            foreach (var app in dataLoan.Apps)
            {
                if (app.aBIsValidApplicant && app.aBTypeT != E_aTypeT.TitleOnly)
                {
                    var borrInfo = new LQBAppraisal.LQBAppraisalRequest.BorrowerInfo();
                    borrInfo.BorrowerName = app.aBNm;
                    borrInfo.BorrowerFirstName = app.aBFirstNm;
                    borrInfo.BorrowerMiddleName = app.aBMidNm;
                    borrInfo.BorrowerLastName = app.aBLastNm;
                    borrInfo.BorrowerEmail = app.aBEmail;
                    borrInfo.BorrowerHomePhone = app.aBHPhone;
                    borrInfo.BorrowerCellPhone = app.aBCellPhone;
                    borrInfo.BorrowerWorkPhone = app.aBBusPhone;
                    if (app.aCIsValidApplicant && app.aCTypeT != E_aTypeT.TitleOnly)
                    {
                        borrInfo.CoborrowerName = app.aCNm;
                        borrInfo.CoborrowerFirstName = app.aCFirstNm;
                        borrInfo.CoborrowerMiddleName = app.aCMidNm;
                        borrInfo.CoborrowerLastName = app.aCLastNm;
                        borrInfo.CoborrowerEmail = app.aCEmail;
                        borrInfo.CoborrowerHomePhone = app.aCHPhone;
                        borrInfo.CoborrowerCellPhone = app.aCCellPhone;
                        borrInfo.CoborrowerWorkPhone = app.aCBusPhone;
                    }

                    borrowers.Add(borrInfo);
                }
                else if (app.aCIsValidApplicant && app.aCTypeT != E_aTypeT.TitleOnly)
                {
                    var borrInfo = new LQBAppraisal.LQBAppraisalRequest.BorrowerInfo();
                    borrInfo.BorrowerName = app.aCNm;
                    borrInfo.BorrowerFirstName = app.aCFirstNm;
                    borrInfo.BorrowerMiddleName = app.aCMidNm;
                    borrInfo.BorrowerLastName = app.aCLastNm;
                    borrInfo.BorrowerEmail = app.aCEmail;
                    borrInfo.BorrowerHomePhone = app.aCHPhone;
                    borrInfo.BorrowerCellPhone = app.aCCellPhone;
                    borrInfo.BorrowerWorkPhone = app.aCBusPhone;
                    borrowers.Add(borrInfo);
                }
            }
            return borrowers;
        }

        private static E_LoanInfoIntendedUse ToLQBAppraisal(E_sLPurposeT type)
        {
            switch (type)
            {
                case E_sLPurposeT.Construct: return E_LoanInfoIntendedUse.Construction;
                case E_sLPurposeT.ConstructPerm: return E_LoanInfoIntendedUse.ConstructionPerm;
                case E_sLPurposeT.FhaStreamlinedRefinance: return E_LoanInfoIntendedUse.FHAStreamlineRefi;
                case E_sLPurposeT.Other: return E_LoanInfoIntendedUse.Other;
                case E_sLPurposeT.Purchase: return E_LoanInfoIntendedUse.Purchase;
                case E_sLPurposeT.Refin: return E_LoanInfoIntendedUse.RefiRateTerm;
                case E_sLPurposeT.RefinCashout: return E_LoanInfoIntendedUse.RefinanceCashout;
                case E_sLPurposeT.VaIrrrl: return E_LoanInfoIntendedUse.VAIRRRL;
            }
            return E_LoanInfoIntendedUse.Undefined;
        }

        private static E_LoanInfoLoanType ToLQBAppraisal(E_sLT type)
        {
            switch (type)
            {
                case E_sLT.Conventional: return E_LoanInfoLoanType.Conventional;
                case E_sLT.FHA: return E_LoanInfoLoanType.FHA;
                case E_sLT.Other: return E_LoanInfoLoanType.Other;
                case E_sLT.UsdaRural: return E_LoanInfoLoanType.USDARuralHousing;
                case E_sLT.VA: return E_LoanInfoLoanType.VA;
            }
            return E_LoanInfoLoanType.Undefined;
        }

        private static E_PropertyInfoOccupancyType ToLQBAppraisal(E_aOccT type)
        {
            switch (type)
            {
                case E_aOccT.Investment: return E_PropertyInfoOccupancyType.Investment;
                case E_aOccT.PrimaryResidence: return E_PropertyInfoOccupancyType.PrimaryResidence;
                case E_aOccT.SecondaryResidence: return E_PropertyInfoOccupancyType.SecondaryResidence;
            }
            return E_PropertyInfoOccupancyType.Undefined;
        }

        private static E_PropertyInfoPropertyType ToLQBAppraisal(E_sGseSpT type)
        {
            switch (type)
            {
                case E_sGseSpT.Attached: return E_PropertyInfoPropertyType.Attached;
                case E_sGseSpT.Condominium: return E_PropertyInfoPropertyType.Condominium;
                case E_sGseSpT.Cooperative: return E_PropertyInfoPropertyType.Cooperative;
                case E_sGseSpT.Detached: return E_PropertyInfoPropertyType.Detached;
                case E_sGseSpT.DetachedCondominium: return E_PropertyInfoPropertyType.DetachedCondominium;
                case E_sGseSpT.HighRiseCondominium: return E_PropertyInfoPropertyType.HighRiseCondominium;
                case E_sGseSpT.ManufacturedHomeCondominium: return E_PropertyInfoPropertyType.ManufacturedHomeCondominium;
                case E_sGseSpT.ManufacturedHomeMultiwide: return E_PropertyInfoPropertyType.ManufacturedHomeMultiwide;
                case E_sGseSpT.ManufacturedHousing: return E_PropertyInfoPropertyType.ManufacturedHousing;
                case E_sGseSpT.ManufacturedHousingSingleWide: return E_PropertyInfoPropertyType.ManufacturedHousingSingleWide;
                case E_sGseSpT.Modular: return E_PropertyInfoPropertyType.Modular;
                case E_sGseSpT.PUD: return E_PropertyInfoPropertyType.PUD;
            }
            return E_PropertyInfoPropertyType.Undefined;
        }
    }
}
