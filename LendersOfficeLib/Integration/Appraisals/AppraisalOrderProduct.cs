﻿namespace LendersOffice.Integration.Appraisals
{
    using System.Runtime.Serialization;

    [DataContract]
    public class AppraisalOrderProduct
    {
        //used for Javascript serialization for product dependencies - OPM 125002
        public AppraisalOrderProduct(string n, string r)
        {
            name = n;
            requires = r;
        }
        [DataMember]
        public string name { get; set; }
        [DataMember]
        public string requires { get; set; }
    }

}
