﻿namespace LendersOffice.Integration.Appraisals
{
    using System.Collections.Generic;
    using System.Xml;
    using LendersOffice.ObjLib.Webservices;

    /// <summary>
    /// Generates a Xml response for a list of appraisal vendors.
    /// </summary>
    public class VendorListingServiceResult : ServiceResult
    {
        /// <summary>
        /// The vendors to export.
        /// </summary>
        private IEnumerable<AppraisalVendorConfig> vendors;

        /// <summary>
        /// Initializes a new instance of the <see cref="VendorListingServiceResult" /> class.
        /// </summary>
        /// <param name="vendors">The set of vendors to serialized.</param>
        public VendorListingServiceResult(IEnumerable<AppraisalVendorConfig> vendors)
        {
            this.vendors = vendors;
        }

        /// <summary>
        /// Outputs the vendor Xml to the writer.
        /// </summary>
        /// <param name="writer">The ml writer to write to.</param>
        protected override void WriteResponseResult(XmlWriter writer)
        {
            writer.WriteStartElement("appraisalvendors");

            foreach (AppraisalVendorConfig vendor in this.vendors)
            {
                writer.WriteStartElement("vendor");
                writer.WriteAttributeString("id", vendor.VendorId.ToString());
                writer.WriteAttributeString("name", vendor.VendorName);
                writer.WriteAttributeString("usesgdms", vendor.UsesGlobalDMS.ToString());
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }
    }
}
