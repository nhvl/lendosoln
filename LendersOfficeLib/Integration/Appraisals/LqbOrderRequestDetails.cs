﻿namespace LendersOffice.Integration.Appraisals
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// The LQB specific request details.
    /// </summary>
    public class LqbOrderRequestDetails : AbstractOrderRequestDetails
    {
        /// <summary>
        /// Holds custom data points that should be sent in with the order.
        /// </summary>
        private Dictionary<string, string> customDataPoints = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// Gets or sets a list of products to be requested.
        /// </summary>
        /// <value>A list of products to be requested.</value>
        public List<string> Products { get; set; }

        /// <summary>
        /// Gets or sets the order number.
        /// </summary>
        /// <value>The order number.</value>
        public string OrderNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the order is a rush order.
        /// </summary>
        /// <value>Whether the order is rushed.</value>
        public bool IsRushOrder { get; set; }

        /// <summary>
        /// Gets or sets the billing method of the order.
        /// </summary>
        /// <value>The billing method of the order.</value>
        public string BillingMethod { get; set; }

        /// <summary>
        /// Gets or sets whether the billing method is a credit card.
        /// </summary>
        public E_TriState CreditCardIndicator { get; set; }

        /// <summary>
        /// Gets the custom data points for the order.
        /// </summary>
        /// <value>Custom data points to send with the appraisal order.</value>
        public IEnumerable<KeyValuePair<string, string>> CustomDataPoints
        {
            get
            {
                return this.customDataPoints;
            }
        }

        /// <summary>
        /// Gets the Lqb vendor credentials.
        /// </summary>
        /// <param name="brokerId">The broker identifier.</param>
        /// <param name="vendorId">The vendor identifier.</param>
        /// <returns>The vendor credentials.</returns>
        public AppraisalVendorCredentials GetCredentials(Guid brokerId, Guid vendorId)
        {
            if (this.Credentials == null)
            {
                return null;
            }

            return new AppraisalVendorCredentials(brokerId, vendorId, this.Credentials.AccountId, this.Credentials.Username, this.Credentials.Password);
        }

        /// <summary>
        /// Adds a custom data a point to the appraisal order.
        /// </summary>
        /// <param name="fieldId">The field identifier.</param>
        /// <param name="value">The value for the given data point.</param>
        public void AddCustomItem(string fieldId, string value)
        {
            this.customDataPoints[fieldId] = value;
        }
    }
}
