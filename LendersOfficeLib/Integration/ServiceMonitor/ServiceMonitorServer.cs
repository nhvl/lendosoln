﻿namespace LendersOffice.Integration.ServiceMonitor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.HttpRequest;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.HttpRequest;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Status types for Service Monitor Framework API.
    /// </summary>
    public enum StatusType
    {
        OK,
        FAILED,
        STARTED
    }

    /// <summary>
    /// Provides an interface with LQB Service Monitor Framework API.
    /// </summary>
    public class ServiceMonitorServer
    {
        /// <summary>
        /// Name of the service as it should appear on Service Monitor page.
        /// </summary>
        private string serviceName;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceMonitorServer"/> class.
        /// </summary>
        /// <param name="serviceName">The name of the service as it should appear on Service Monitor page.</param>
        public ServiceMonitorServer(string serviceName)
        {
            this.serviceName = serviceName;
        }

        /// <summary>
        /// Records the log to Service Monitor, falling back on PB if unable to post.
        /// </summary>
        /// <param name="status">The status event to be logged to Service Monitor.</param>
        /// <param name="logData">Addtional message data to be logged to Service Monitor.</param>
        /// <returns>A boolean indicating whether response from Service Monitor API is expected 204 status code.</returns>
        public bool Record(StatusType status, string logData)
        {
            LqbAbsoluteUri? targetUri = LqbAbsoluteUri.Create(ConstStage.ServiceMonitorFrameworkApiUrl);
            if (targetUri == null)
            {
                return false;
            }

            var message = new
            {
                StatusType = status,
                ServiceName = this.serviceName,
                LogData = logData
            };

            string messageJson = SerializationHelper.JsonNetAnonymousSerialize(message);

            try
            {
                HttpRequestOptions options = new HttpRequestOptions
                {
                    Method = HttpMethod.Post,
                    PostData = new StringContent(messageJson),
                    MimeType = MimeType.Application.Json,
                    Timeout = TimeoutInSeconds.Thirty,
                    KeepAlive = false
                };

                WebRequestHelper.ExecuteCommunication(targetUri.Value, options);
                return options.ResponseStatusCode == HttpStatusCode.NoContent;
            }
            catch (LqbException exc)
            {
                ////If exception is ServerException or innerException is WebException, log that to PB.
                Exception exceptionToLog = (exc is ServerException || exc.InnerException is WebException) ? exc.InnerException : exc;
                Tools.LogError("LQB to ServiceMonitor communication Fail: " + messageJson, exceptionToLog);
                return false;
            }
        }
    }
}
