﻿namespace LendersOffice.Integration.VOXFramework
{
    /// <summary>
    /// The transmission model type.
    /// </summary>
    public enum TransmissionModelT
    {
        /// <summary>
        /// MCL polling model.
        /// </summary>
        MCLPolling = 0,

        /// <summary>
        /// Equifax Verification Services OFX Polling Model.
        /// </summary>
        EquifaxVerificationServicesOfxPollingModel = 1,
    }
}
