﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class holding basic info for a vendor service.
    /// </summary>
    public class LightVOXVendorService
    {
        /// <summary>
        /// Gets or sets the id for this vendor level service.
        /// </summary>
        /// <value>The id for this vendor level service.</value>
        public int VendorServiceId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the type of service this is.
        /// </summary>
        /// <value>The type of service this is.</value>
        public VOXServiceT ServiceType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the vendor this service belongs to.
        /// </summary>
        /// <value>The vendor this service belongs to.</value>
        public int VendorInfoId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this service is enabled.
        /// </summary>
        /// <value>Whether this service is enabled.</value>
        public bool IsEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is a direct vendor.
        /// </summary>
        /// <value>Whether this is a direct vendor.</value>
        public bool IsADirectVendor
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this sells via resellers.
        /// </summary>
        /// <value>Whether this vendor sells via resellers.</value>
        public bool SellsViaResellers
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is a reseller.
        /// </summary>
        /// <value>Whether this is a reseller.</value>
        public bool IsAReseller
        {
            get;
            set;
        }

        /// <summary>
        /// Creates a LightVendorService from the reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns>The created light vendor service.</returns>
        internal static LightVOXVendorService CreateFromReader(IDataReader reader)
        {
            LightVOXVendorService vendorService = new LightVOXVendorService();
            vendorService.IsADirectVendor = (bool)reader["IsADirectVendor"];
            vendorService.IsAReseller = (bool)reader["IsAReseller"];
            vendorService.IsEnabled = (bool)reader["IsEnabled"];
            vendorService.SellsViaResellers = (bool)reader["SellsViaResellers"];
            vendorService.ServiceType = (VOXServiceT)reader["ServiceT"];
            vendorService.VendorInfoId = (int)reader["VendorId"];
            vendorService.VendorServiceId = (int)reader["VendorServiceId"];

            return vendorService;
        }
    }
}
