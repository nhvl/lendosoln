﻿namespace LendersOffice.Integration.VOXFramework
{
    using DataAccess;

    /// <summary>
    /// View model for SSA89 orders.
    /// </summary>
    public class SSA89OrderViewModel : AbstractVOXOrderViewModel
    {
        /// <summary>
        /// Gets the service type.
        /// </summary>
        /// <value>The service type.</value>
        public override VOXServiceT ServiceType => VOXServiceT.SSA_89;

        /// <summary>
        /// Gets or sets the borrower name.
        /// </summary>
        /// <value>The borrower name.</value>
        public string BorrowerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the SSN of the borrower.
        /// </summary>
        /// <value>The SSN of the borrower.</value>
        public string Ssn
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the date of birth as a string.
        /// </summary>
        /// <value>The date of birth as a string.</value>
        public string DateOfBirth
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets status of the DM subproduct.
        /// </summary>
        /// <value>The status of the DM subproduct.</value>
        public SSA89SubproductStatus DmStatus
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets status of the SSA subproduct.
        /// </summary>
        /// <value>The status of the SSA subproduct.</value>
        public SSA89SubproductStatus SsaStatus
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets status of the OFAC subproduct.
        /// </summary>
        /// <value>The status of the OFAC subproduct.</value>
        public SSA89SubproductStatus OfacStatus
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets status of the CH subproduct.
        /// </summary>
        /// <value>The status of the CH subproduct.</value>
        public SSA89SubproductStatus ChStatus
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the status of the DM subproduct as a string.
        /// </summary>
        /// <value>The status of the DM subproduct as a string.</value>
        public string DmStatusString
        {
            get
            {
                return this.StringifySubproductStatus(this.DmStatus);
            }
        }

        /// <summary>
        /// Gets the status of the SSA subproduct as a string.
        /// </summary>
        /// <value>The status of the SSA subproduct as a string.</value>
        public string SsaStatusString
        {
            get
            {
                return this.StringifySubproductStatus(this.SsaStatus);
            }
        }

        /// <summary>
        /// Gets the status of the OFAC subproduct as a string.
        /// </summary>
        /// <value>The status of the OFAC subproduct as a string.</value>
        public string OfacStatusString
        {
            get
            {
                return this.StringifySubproductStatus(this.OfacStatus);
            }
        }

        /// <summary>
        /// Gets the status of the CH subproduct as a string.
        /// </summary>
        /// <value>The status of the CH subproduct as a string.</value>
        public string ChStatusString
        {
            get
            {
                return this.StringifySubproductStatus(this.ChStatus);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the order has an edoc already.
        /// </summary>
        /// <value>Whether the order has an edoc already.</value>
        public bool HasEdoc
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the SSA form was authorized and accepted.
        /// </summary>
        /// <value>Whether the SSA form was authorized and accepted.</value>
        public bool? SsaFormAccepted
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the rejection reason if the SSA form was not accepted.
        /// </summary>
        /// <value>The rejection reason if the SSA form was not accepted.</value>
        public string SsaFormRejectionReason
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the doc name prefix for the UI.
        /// </summary>
        /// <returns>Doc name prefix.</returns>
        public override string GetDocNamePrefix()
        {
            return "SSA-89";
        }

        /// <summary>
        /// Creates a string out of the ssa89 subproduct status.
        /// </summary>
        /// <param name="status">The status to stringify.</param>
        /// <returns>The string.</returns>
        private string StringifySubproductStatus(SSA89SubproductStatus status)
        {
            switch (status)
            {
                case SSA89SubproductStatus.Match:
                    return "Match";
                case SSA89SubproductStatus.NoMatch:
                    return "No Match";
                case SSA89SubproductStatus.NotOrdered:
                    return "Not Ordered";
                case SSA89SubproductStatus.NotPresent:
                    return "Not Present";
                case SSA89SubproductStatus.Pending:
                    return "Pending";
                case SSA89SubproductStatus.Present:
                    return "Present";
                case SSA89SubproductStatus.Returned:
                    return "Returned";
                default:
                    throw new UnhandledEnumException(status);
            }
        }
    }
}
