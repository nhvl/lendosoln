﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using Common;
    using DataAccess;

    /// <summary>
    /// Class representing a VOA order.
    /// </summary>
    public class VOAOrder : AbstractVOXOrder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOAOrder"/> class.
        /// </summary>
        /// <param name="record">The database record of an order.</param>
        /// <param name="assetRecords">The database records of the assets on the order.</param>
        /// <param name="verifiedAssetRecords">The database records of the verified/returned assets on the order.</param>
        /// <param name="documentIds">The document ids for this order.</param>
        /// <param name="vendorReferenceIds">The vendor reference ids.</param>
        /// <param name="statuses">The collection of order statuses on the order.</param>
        private VOAOrder(IReadOnlyDictionary<string, object> record, IReadOnlyList<IReadOnlyDictionary<string, object>> assetRecords, IReadOnlyList<IReadOnlyDictionary<string, object>> verifiedAssetRecords, List<Guid> documentIds, HashSet<string> vendorReferenceIds, List<VOXStatus> statuses)
            : base(record, documentIds, vendorReferenceIds)
        {
            this.VerificationType = (VOAVerificationT)record["VerificationType"];
            this.RefreshPeriod = (int)record["RefreshPeriod"];
            this.AssetRecords = assetRecords.Select(
                assetRecord => new VOAOrderAssetRecord(
                    (Guid)assetRecord["Id"],
                    (string)assetRecord["Institution"],
                    (string)assetRecord["AccountNumber"])).ToList();
            this.VerifiedAssetRecords = verifiedAssetRecords.Select(
                assetRecord => VerifiedAsset.FromDatabase(assetRecord)).ToList();
            this.IsForCoborrower = (bool)record["IsForCoborrower"];
            this.Statuses = statuses;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VOAOrder"/> class.
        /// </summary>
        /// <param name="requestData">The source data of the UI's request.</param>
        /// <param name="orderRequest">The request of the order metadata that was sent.</param>
        /// <param name="serviceResponse">The response to the order.</param>
        /// <returns>An order object with a corresponding database entry.</returns>
        private VOAOrder(VOARequestData requestData, VOXRequestOrder orderRequest, VOXServiceResponse serviceResponse)
            : base(requestData, orderRequest, serviceResponse)
        {
            var existingOrder = (VOAOrder)requestData.PreviousOrder;
            this.VerificationType = existingOrder?.VerificationType ?? requestData.VerificationType;
            this.RefreshPeriod = existingOrder?.RefreshPeriod ?? requestData.RefreshPeriodOption;
            this.AssetRecords = existingOrder?.AssetRecords ?? orderRequest.LinkedRecords.Select(id => requestData.AssetsExported.Single(a => a.Id == id)).ToList();

            // The orderRequest object should have IsForCoborrower populated for all Initial requests.
            this.IsForCoborrower = existingOrder?.IsForCoborrower ?? orderRequest.IsForCoborrower.Value;

            this.VerifiedAssetRecords = serviceResponse.ReturnedAssets;
            this.Statuses = serviceResponse.Statuses;
        }

        /// <summary>
        /// Gets the type of verification (Assets/Deposits).
        /// </summary>
        /// <value>The type of verification.</value>
        public VOAVerificationT VerificationType { get; }

        /// <summary>
        /// Gets a value indicating whether this order can be refreshed.
        /// Based on status and refresh option.
        /// </summary>
        /// <value>Whether the order can be refreshed.</value>
        public override bool CanBeRefreshed
        {
            get
            {
                if (this.VerificationType == VOAVerificationT.Assets)
                {
                    return this.CurrentStatus == VOXOrderStatusT.Complete;
                }
                else
                {
                return !this.HasBeenRefreshed  &&
                        this.CurrentStatus == VOXOrderStatusT.Complete &&
                        (DateTime.Today < this.DateOrdered.Date.AddDays(this.RefreshPeriod));
                }
            }
        }

        /// <summary>
        /// Gets the refresh period for the order.
        /// </summary>
        /// <value>The refresh period for the order.</value>
        public int RefreshPeriod { get; }

        /// <summary>
        /// Gets the asset records chosen for this order.
        /// </summary>
        /// <value>The asset records chosen for this order.</value>
        public IReadOnlyList<VOAOrderAssetRecord> AssetRecords { get; }

        /// <summary>
        /// Gets the verified asset records returned for this order.
        /// </summary>
        /// <value>The verified asset records returned for this order.</value>
        public IReadOnlyList<VerifiedAsset> VerifiedAssetRecords { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the borrower associated with this order is the coborrower of the associated app.
        /// </summary>
        /// <value>Whether the associated borrower is the coborrower of the app.</value>
        public bool IsForCoborrower { get; }

        /// <summary>
        /// Retrieves all verification of assets orders for this loan.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="loanId">The loan id.</param>
        /// <returns>The list of verification of assets orders.</returns>
        public static IEnumerable<VOAOrder> GetOrdersForLoan(Guid brokerId, Guid loanId)
        {
            return GetOrdersForLoan(brokerId, loanId, null);
        }

        /// <summary>
        /// Retrieves an order.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="orderId">The order id.</param>
        /// <returns>The order if found; null otherwise.</returns>
        public static VOAOrder GetOrderForLoan(Guid brokerId, Guid loanId, int orderId)
        {
            return GetOrdersForLoan(brokerId, loanId, orderId).SingleOrDefault();
        }

        /// <summary>
        /// Creates a new record of an order in the database from a placed order.
        /// </summary>
        /// <param name="requestData">The source data of the UI's request.</param>
        /// <param name="orderRequest">The request of the order metadata that was sent.</param>
        /// <param name="serviceResponse">The response to the order.</param>
        /// <returns>The order object representing the database's entry.</returns>
        public static VOAOrder CreateOrder(VOARequestData requestData, VOXRequestOrder orderRequest, VOXServiceResponse serviceResponse)
        {
            VOAOrder order = new VOAOrder(requestData, orderRequest, serviceResponse);
            order.SaveNewOrderInDatabase(requestData.PreviousOrder);
            return order;
        }

        /// <summary>
        /// Creates a view model out of this loan.
        /// </summary>
        /// <param name="extraInfo">Any extra info needed to add to the view model.</param>
        /// <returns>The view model.</returns>
        public new VOAOrderViewModel CreateViewModel(ExtraOrderViewModelInfo extraInfo)
        {
            VOAOrderViewModel model = new VOAOrderViewModel()
            {
                AccountNumbers = this.AssetRecords.Select(asset => asset.AccountNumber.Value).ToList(), // Not ideal to expose the sensitive string, but the view model is used directly for serialization
                CanBeRefreshed = this.CanBeRefreshed,
                DateCompleted = this.DateCompleted?.ToString("G") ?? string.Empty,
                DateOrdered = this.DateOrdered.ToString("G"),
                Error = this.ErrorMessage,
                HasBeenRefreshed = this.HasBeenRefreshed,
                Institutions = this.AssetRecords.Select(asset => asset.Institution).ToList(),
                OrderedBy = this.OrderedBy,
                OrderId = this.OrderId,
                OrderNumber = this.OrderNumber,
                RefreshPeriod = this.RefreshPeriod,
                LenderServiceName = this.LenderServiceName,
                Status = this.CurrentStatus,
                StatusDescription = this.CurrentStatusDescription,
                VerificationType = this.VerificationType,
                VendorReferenceIds = this.VendorReferenceIds.ToList(),
                HasStatusList = this.Statuses?.Count > 1 || (this.Statuses.Count == 0 && this.Statuses.FirstOrDefault()?.StatusDateTime != null)
            };

            model.SetAssociatedEdocs(this.AssociatedEdocs, extraInfo?.AvailableEdocs);
            if (this.VerificationType == VOAVerificationT.Assets && this.CurrentStatus == VOXOrderStatusT.Complete)
            {
                // We need to tweak the view model to show an Active status in some cases to mimic the MCL UI.
                // In the backend, the order is still Complete.
                if (DateTime.Today < this.DateOrdered.Date.AddDays(this.RefreshPeriod))
                {
                    model.Status = VOXOrderStatusT.Active;
                }
            }

            if (extraInfo != null)
            {
                model.RequiresAccountId = extraInfo.RequiresAccountId;
                model.UsesAccountId = extraInfo.UsesAccountId;
                model.ServiceCredentialHasAccountId = extraInfo.ServiceCredentialHasAccountId;
                model.ServiceCredentialId = extraInfo.ServiceCredentialId;
            }

            return model;
        }

        /// <summary>
        /// Updates an order based on the response from a "Get" request.
        /// </summary>
        /// <param name="requestData">The source data of the UI's request.</param>
        /// <param name="orderRequest">The request of the order metadata that was sent.</param>
        /// <param name="serviceResponse">The response to the order.</param>
        public override void UpdateForGetRequest(AbstractVOXRequestData requestData, VOXRequestOrder orderRequest, VOXServiceResponse serviceResponse)
        {
            this.VerifiedAssetRecords = serviceResponse.ReturnedAssets;
            base.UpdateForGetRequest(requestData, orderRequest, serviceResponse);
        }

        /// <summary>
        /// Creates a view model out of this loan.
        /// </summary>
        /// <param name="extraInfo">Extra info outside of the order.</param>
        /// <returns>The view model.</returns>
        protected override AbstractVOXOrderViewModel CreateAbstractViewModel(ExtraOrderViewModelInfo extraInfo)
        {
            return this.CreateViewModel(extraInfo);
        }

        /// <summary>
        /// Handles order creation in the database, for the current instance's data.
        /// </summary>
        /// <param name="storedProcedureExecutionContext">The context of the stored procedure, i.e. connection and transaction.</param>
        protected override void CreateOrderInDatabase(DataAccess.CStoredProcedureExec storedProcedureExecutionContext)
        {
            base.CreateOrderInDatabase(storedProcedureExecutionContext); // The base implementation will determine the OrderId
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@OrderId", this.OrderId),
                new SqlParameter("@VerificationType", this.VerificationType),
                new SqlParameter("@RefreshPeriod", this.RefreshPeriod),
                new SqlParameter("@IsForCoborrower", this.IsForCoborrower)
            };

            storedProcedureExecutionContext.ExecuteNonQuery("VOA_ORDER_Create", parameters);

            foreach (var asset in this.AssetRecords)
            {
                var assetParameters = new SqlParameter[]
                {
                    new SqlParameter("@OrderId", this.OrderId),
                    new SqlParameter("@Id", asset.Id),
                    new SqlParameter("@Institution", asset.Institution),
                    new SqlParameter("@AccountNumber", asset.AccountNumber.Value)
                };

                storedProcedureExecutionContext.ExecuteNonQuery("VOA_ORDER_ASSET_Create", assetParameters);
            }

            this.SaveVerifiedAssets(storedProcedureExecutionContext);
        }

        /// <summary>
        /// Updates an existing order in the database with data from this object.
        /// </summary>
        /// <param name="storedProcedureExecutionContext">The context of the stored procedure, i.e. connection and transaction.</param>
        protected override void UpdateOrderInDatabase(CStoredProcedureExec storedProcedureExecutionContext)
        {
            base.UpdateOrderInDatabase(storedProcedureExecutionContext);
            this.ClearVerifiedAssets(storedProcedureExecutionContext);
            this.SaveVerifiedAssets(storedProcedureExecutionContext);
        }

        /// <summary>
        /// Retrieves the verification of assets orders for the specified loan, optionally restricted to a single order.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="orderId">The order id.</param>
        /// <returns>The list of verification of assets orders.</returns>
        private static IReadOnlyList<VOAOrder> GetOrdersForLoan(Guid brokerId, Guid loanId, int? orderId)
        {
            string storedProcedureName = "VOA_ORDER_Retrieve";
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@OrderId", orderId)
            };

            IReadOnlyList<IReadOnlyList<IReadOnlyDictionary<string, object>>> results = DataAccess.StoredProcedureHelper.ExecuteReaderIntoResultListDictionaries(brokerId, storedProcedureName, parameters);
            IReadOnlyList<IReadOnlyDictionary<string, object>> orderRecords = results[0];
            IReadOnlyList<IReadOnlyDictionary<string, object>> assetRecords = results[1];
            Dictionary<int, List<Guid>> orderToDocIds = CreateOrderToDocsMap(results[2]);
            Dictionary<int, HashSet<string>> orderToVendorReferenceIds = CreateOrderToVendorReferenceIdsMap(results[3]);
            IReadOnlyList<IReadOnlyDictionary<string, object>> verifiedAssetRecords = results[4];
            IReadOnlyList<IReadOnlyDictionary<string, object>> statusList = results[5];

            var orders = new List<VOAOrder>(orderRecords.Count);
            foreach (var orderRecord in orderRecords)
            {
                int orderRecordId = (int)orderRecord["OrderId"];
                var assetRecordsOnOrder = new List<IReadOnlyDictionary<string, object>>(assetRecords.Count);
                var verifiedAssetRecordsOnOrder = new List<IReadOnlyDictionary<string, object>>(verifiedAssetRecords.Count);
                foreach (var assetRecord in assetRecords)
                {
                    if (orderRecordId == (int)assetRecord["OrderId"])
                    {
                        assetRecordsOnOrder.Add(assetRecord);
                    }
                }

                var statusesOnOrder = new List<VOXStatus>();
                foreach (var status in statusList)
                {
                    if (orderRecordId == (int)status["OrderId"])
                    {
                        statusesOnOrder.Add(new VOXStatus((VOXOrderStatusT)status["StatusCode"], (string)status["StatusDescription"], (DateTime)status["StatusTime"]));
                    }
                }

                foreach (var verifiedAssetRecord in verifiedAssetRecords)
                {
                    if (orderRecordId == (int)verifiedAssetRecord["OrderId"])
                    {
                        verifiedAssetRecordsOnOrder.Add(verifiedAssetRecord);
                    }
                }

                List<Guid> documentIds;
                if (!orderToDocIds.TryGetValue(orderRecordId, out documentIds))
                {
                    documentIds = new List<Guid>();
                }

                orders.Add(new VOAOrder(orderRecord, assetRecordsOnOrder, verifiedAssetRecordsOnOrder, documentIds, orderToVendorReferenceIds.GetValueOrNull(orderRecordId), statusesOnOrder));
            }

            return orders;
        }

        /// <summary>
        /// Deletes all of the verified assets from the order so that the entire collection can be updated from a response.
        /// </summary>
        /// <param name="storedProcedureExecutionContext">The context of the stored procedure, i.e. connection and transaction.</param>
        private void ClearVerifiedAssets(CStoredProcedureExec storedProcedureExecutionContext)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@OrderId", this.OrderId),
                new SqlParameter("@LoanId", this.LoanId),
                new SqlParameter("@BrokerId", this.BrokerId)
            };
            storedProcedureExecutionContext.ExecuteNonQuery("VOA_ORDER_ASSET_VERIFIED_ClearForOrder", parameters);
        }

        /// <summary>
        /// Saves the returned/verified assets on this order to the database.
        /// </summary>
        /// <param name="storedProcedureExecutionContext">The database execution context to use.</param>
        private void SaveVerifiedAssets(CStoredProcedureExec storedProcedureExecutionContext)
        {
            foreach (var verifiedAsset in this.VerifiedAssetRecords.CoalesceWithEmpty())
            {
                var encryptionKeyId = Drivers.Encryption.EncryptionHelper.GenerateNewKey();
                var verifiedAssetParameters = new SqlParameter[]
                {
                    new SqlParameter("@OrderId", this.OrderId),
                    new SqlParameter("@AccountBalance", verifiedAsset.AccountBalance),
                    new SqlParameter("@FinancialInstitutionName", verifiedAsset.FinancialInstitutionName),
                    new SqlParameter("@AssetType", verifiedAsset.AssetType),
                    new SqlParameter("@EncryptionKeyId", encryptionKeyId.Value),
                    new SqlParameter("@EncryptedAccountNumber", Drivers.Encryption.EncryptionHelper.EncryptString(encryptionKeyId, verifiedAsset.AccountNumber))
                };

                storedProcedureExecutionContext.ExecuteNonQuery("VOA_ORDER_ASSET_VERIFIED_Create", verifiedAssetParameters);
            }
        }
    }
}
