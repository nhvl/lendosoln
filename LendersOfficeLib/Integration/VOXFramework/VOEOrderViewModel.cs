﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// View model for VOE orders.
    /// </summary>
    public class VOEOrderViewModel : AbstractVOXOrderViewModel
    {
        /// <summary>
        /// Gets the service type.
        /// </summary>
        /// <value>The service type.</value>
        public override VOXServiceT ServiceType
        {
            get
            {
                return VOXServiceT.VOE;
            }
        }

        /// <summary>
        /// Gets or sets the vendor ID for the parent order, if applicable.
        /// </summary>
        /// <value>The vendor ID for the parent order, if applicable.</value>
        public string ParentOrderNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the borrower associated with this order.
        /// </summary>
        /// <value>The name of the borrower associated with this order.</value>
        public string BorrowerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a list of employers associated with this order.
        /// </summary>
        /// <value>List of employers associated with this order.</value>
        public List<string> Employers
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this order is for self-employment.
        /// </summary>
        /// <value>Whether this order is for self-employment.</value>
        public bool IsSelfEmployment
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the VOE verification type.
        /// </summary>
        /// <value>The VOE verification type.</value>
        public VOEVerificationT VerificationType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the VOE verification type as a string.
        /// </summary>
        /// <value>The VOE verification type.</value>
        public string VerificationTypeString
        {
            get
            {
                if (this.VerificationType == VOEVerificationT.Employment)
                {
                    return "VOE";
                }
                else
                {
                    return "VOI";
                }
            }
        }

        /// <summary>
        /// Gets the doc name prefix.
        /// </summary>
        /// <returns>The doc name prefix.</returns>
        public override string GetDocNamePrefix()
        {
            if (this.VerificationType == VOEVerificationT.Employment)
            {
                return "VOE";
            }
            else
            {
                return "VOI";
            }
        }
    }
}
