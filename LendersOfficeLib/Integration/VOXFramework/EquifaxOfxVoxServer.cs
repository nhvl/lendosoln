﻿namespace LendersOffice.Integration.VOXFramework
{
    /// <summary>
    /// Defines an Equifax OFX-specific implementation of the VOX server.
    /// </summary>
    public class EquifaxOfxVoxServer : VOXServer
    {
        /// <summary>
        /// The divider used to separate the multi-part request.
        /// </summary>
        private readonly string boundaryDivider;

        /// <summary>
        /// Initializes a new instance of the <see cref="EquifaxOfxVoxServer"/> class.
        /// </summary>
        /// <param name="boundaryDivider">The divider used to separate the multi-part request.</param>
        public EquifaxOfxVoxServer(string boundaryDivider)
        {
            this.boundaryDivider = boundaryDivider;
        }

        /// <summary>
        /// Gets the content type header to send.
        /// </summary>
        /// <value>The content type header to send.</value>
        protected override string ContentType => $"Multipart/Related; boundary=\"{this.boundaryDivider}\"; type=application/x-ofx";
    }
}
