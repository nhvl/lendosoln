﻿namespace LendersOffice.Integration.VOXFramework
{
    /// <summary>
    /// How LQB authenticates with a service provider.
    /// </summary>
    public enum TransmissionAuthenticationT
    {
        /// <summary>
        /// Through basic authentication.
        /// </summary>
        BasicAuthentication = 0,
        
        /// <summary>
        /// Through a digital certificate.
        /// </summary>
        DigitalCert = 1,

        /// <summary>
        /// Through IP address validation.
        /// </summary>
        IpAddressValidation = 2
    }
}
