﻿namespace LendersOffice.Integration.VOXFramework
{
    /// <summary>
    /// What VOE verification type a vendor is configured for.
    /// </summary>
    public enum VOEVerificationT
    {
        /// <summary>
        /// Employment only.
        /// </summary>
        Employment = 0,

        /// <summary>
        /// Employment and income.
        /// </summary>
        EmploymentIncome = 1
    }
}
