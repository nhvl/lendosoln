﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common;
    using Security;

    /// <summary>
    /// The request data for a VOA service request.
    /// </summary>
    public class VOARequestData : AbstractVOXRequestData
    {
        /// <summary>
        /// The assets on the order.
        /// </summary>
        private readonly List<VOAOrderAssetRecord> assetsExported;

        /// <summary>
        /// Initializes a new instance of the <see cref="VOARequestData"/> class.
        /// </summary>
        /// <param name="principal">The user placing the request.</param>
        /// <param name="lenderServiceId">The lender service id.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="requestType">The request type.</param>
        public VOARequestData(AbstractUserPrincipal principal, int lenderServiceId, Guid loanId, VOXRequestT requestType)
            : base(principal, lenderServiceId, loanId, requestType)
        {
            this.assetsExported = new List<VOAOrderAssetRecord>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VOARequestData"/> class.
        /// This is to be used for follow up requests with existing orders.
        /// </summary>
        /// <param name="principal">The user placing the request.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="requestType">The request type.</param>
        /// <param name="voaOrder">The existing order to follow up.</param>
        public VOARequestData(AbstractUserPrincipal principal, Guid loanId, VOXRequestT requestType, VOAOrder voaOrder)
            : base(principal, loanId, requestType, voaOrder)
        {
            this.RefreshPeriodOption = voaOrder.RefreshPeriod;
            this.assetsExported = new List<VOAOrderAssetRecord>(voaOrder.AssetRecords);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VOARequestData"/> class by
        /// copying an existing instance.
        /// </summary>
        /// <param name="requestData">The instance to copy.</param>
        /// <param name="overrideRequestType">Can override the request type.</param>
        /// <param name="isSystemGenerated">Can override the system generated indicator.</param>
        public VOARequestData(VOARequestData requestData, VOXRequestT? overrideRequestType = null, bool? isSystemGenerated = null)
            : base(requestData, overrideRequestType, isSystemGenerated)
        {
            this.assetsExported = requestData.assetsExported;
            this.AccountHistoryOption = requestData.AccountHistoryOption;
            this.RefreshPeriodOption = requestData.RefreshPeriodOption;
            this.AssetsToInclude = requestData.AssetsToInclude;
        }

        /// <summary>
        /// Gets the borrowers for this request data.
        /// Pulls a single borrower from the previous order for subsequent requests. For Initial requests, this will pull from the assets in the request data.
        /// </summary>
        /// <value>The borrowers for this request data.</value>
        public override IEnumerable<KeyValuePair<Guid, bool>> BorrowersInRequest
        {
            get
            {
                if (this.PreviousOrder != null)
                {
                    return new List<KeyValuePair<Guid, bool>>()
                    {
                        new KeyValuePair<Guid, bool>(this.PreviousOrder.ApplicationId, ((VOAOrder)this.PreviousOrder).IsForCoborrower)
                    };
                }
                else if (!this.VerifiesBorrower && this.AssetsToInclude != null)
                {
                    List<KeyValuePair<Guid, bool>> borrowers = new List<KeyValuePair<Guid, bool>>();
                    foreach (var asset in this.AssetsToInclude)
                    {
                        var borrowerId = new KeyValuePair<Guid, bool>(asset.AppId, asset.IsForCoborrower);
                        if (!borrowers.Contains(borrowerId))
                        {
                            borrowers.Add(borrowerId);
                        }
                    }

                    return borrowers;
                }
                else if (this.VerifiesBorrower && this.BorrowerAppId.HasValue && this.BorrowerType.HasValue)
                {
                    return new List<KeyValuePair<Guid, bool>>()
                    {
                        new KeyValuePair<Guid, bool>(this.BorrowerAppId.Value, this.BorrowerType.Value == DataAccess.E_BorrowerModeT.Coborrower)
                    };
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the service type.
        /// </summary>
        /// <value>The service type.</value>
        public override VOXServiceT ServiceType
        {
            get
            {
                return VOXServiceT.VOA_VOD;
            }
        }

        /// <summary>
        /// Gets or sets the account history options.
        /// </summary>
        /// <value>The account history options.</value>
        public int AccountHistoryOption
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the refresh period options.
        /// </summary>
        /// <value>The refresh period options.</value>
        public int RefreshPeriodOption
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this request is for TPO.
        /// </summary>
        /// <value>Whether this request is for TPO.</value>
        public bool IsForTpo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the assets to include.
        /// </summary>
        /// <value>The assets to include.</value>
        public IEnumerable<VOAServiceAssetData> AssetsToInclude
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the verification type for this VOA/VOD request.
        /// </summary>
        /// <value>The verification type for this VOA/VOD request.</value>
        public VOAVerificationT VerificationType
        {
            get
            {
                VOAVendorService service = this.LenderService.VendorForVendorAndServiceData.Services[VOXServiceT.VOA_VOD] as VOAVendorService;
                return service.VoaVerificationT;
            }
        }

        /// <summary>
        /// Gets the asset records exported to the vendor in the current request.  May not contain any values until the exporter has processed the request.
        /// </summary>
        /// <value>The asset records exported to the vendor in the current request.</value>
        public IEnumerable<VOAOrderAssetRecord> AssetsExported
        {
            get { return this.assetsExported; }
        }

        /// <summary>
        /// Validates the request data.
        /// </summary>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if valid, false otherwise.</returns>
        public override bool ValidateRequestData(out string errors)
        {
            if (!base.ValidateRequestData(out errors))
            {
                return false;
            }

            if (this.RequestType == VOXRequestT.Initial)
            {
                if (this.LenderService.VoaEnforceAllowRequestWithoutAssets && this.LenderService.VoaAllowRequestWithoutAssets && !this.VerifiesBorrower)
                {
                    errors = "This service provider may only verify borrowers.";
                    return false;
                }

                if (this.LenderService.VoaEnforceAllowRequestWithoutAssets && !this.LenderService.VoaAllowRequestWithoutAssets && this.VerifiesBorrower)
                {
                    errors = "This service provider may only verify assets.";
                    return false;
                }

                if (!this.VerifiesBorrower && !this.AssetsToInclude.CoalesceWithEmpty().Any())
                {
                    errors = "No assets selected.";
                    return false;
                }

                if (this.VerifiesBorrower && (!this.BorrowerAppId.HasValue || !this.BorrowerType.HasValue))
                {
                    errors = "No borrower selected.";
                    return false;
                }
            }

            AbstractVOXVendorService voaService;
            if (this.LenderService.VendorForVendorAndServiceData.Services.TryGetValue(VOXServiceT.VOA_VOD, out voaService) && ((VOAVendorService)voaService).AskForNotificationEmail &&
                (string.IsNullOrEmpty(this.NotificationEmail) || !LqbGrammar.DataTypes.EmailAddress.Create(this.NotificationEmail).HasValue))
            {
                errors = "A valid email address is required.";
                return false;
            }

            errors = string.Empty;
            return true;
        }

        /// <summary>
        /// Records an asset record from the exporter into the request's list of assets.
        /// </summary>
        /// <param name="assetRecord">The asset record to record.</param>
        public void RecordAssetExport(VOAOrderAssetRecord assetRecord)
        {
            this.assetsExported.Add(assetRecord);
        }
    }
}
