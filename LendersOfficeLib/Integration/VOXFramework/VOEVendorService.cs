﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using DataAccess;

    /// <summary>
    /// VOE service at the vendor level.
    /// </summary>
    public class VOEVendorService : AbstractVOXVendorService
    {
        /// <summary>
        /// Stored procedure for creating a VOE service.
        /// </summary>
        private const string CreateVOEService = "CreateVOEService";

        /// <summary>
        /// Stored procedure for updting a VOE service.
        /// </summary>
        private const string UpdateVOEService = "UpdateVOEService";

        /// <summary>
        /// Gets the service type.
        /// </summary>
        /// <value>The service type.</value>
        public override VOXServiceT ServiceType
        {
            get
            {
                return VOXServiceT.VOE;
            }
        }

        /// <summary>
        /// Gets or sets the verification type for the VOE service.
        /// </summary>
        /// <value>The verification type for the VOE service.</value>
        public VOEVerificationT VerificationType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether a salary key should be provided.
        /// </summary>
        /// <value>Whether a salary key should be provided.</value>
        public bool AskForSalaryKey
        {
            get;
            set;
        }

        /// <summary>
        /// Creates a new service from the model. This assumes that this instance is 100% new.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if made, false otherwise.</returns>
        internal override bool CreateFromViewModel(AbstractVOXVendorServiceViewModel viewModel, out string errors)
        {
            if (viewModel == null)
            {
                errors = "Null model";
                return false;
            }

            VOEVendorServiceViewModel trueModel = viewModel as VOEVendorServiceViewModel;
            if (trueModel == null)
            {
                errors = "Invalid model passed in.";
                return false;
            }

            this.IsADirectVendor = trueModel.IsADirectVendor;
            this.IsAReseller = trueModel.IsAReseller;
            this.IsEnabled = trueModel.IsEnabled;
            this.SellsViaResellers = trueModel.SellsViaResellers;
            this.VendorInfoId = -1;
            this.VendorServiceId = -1;
            this.AskForSalaryKey = trueModel.AskForSalaryKey;
            this.VerificationType = trueModel.VerificationType;

            errors = string.Empty;
            return true;
        }

        /// <summary>
        /// Populates this VOE service from the reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        internal override void PopulateFromReader(IDataReader reader)
        {
            this.VendorServiceId = (int)reader["VendorServiceId"];
            this.IsADirectVendor = (bool)reader["IsADirectVendor"];
            this.SellsViaResellers = (bool)reader["SellsViaResellers"];
            this.IsAReseller = (bool)reader["IsAReseller"];
            this.AskForSalaryKey = (bool)reader["AskForSalaryKey"];
            this.VerificationType = (VOEVerificationT)reader["VoeVerificationT"];
            this.VendorInfoId = (int)reader["VendorId"];
            this.IsEnabled = (bool)reader["IsEnabled"];
        }

        /// <summary>
        /// Populates the service from the view model. For editting.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if populated. False otherwise.</returns>
        internal override bool PopulateFromViewModel(VOXVendorViewModel viewModel, out string errors)
        {
            if (viewModel == null)
            {
                errors = "Null model passed in.";
                return false;
            }

            VOEVendorServiceViewModel trueModel = viewModel.VOEService;

            this.IsADirectVendor = trueModel.IsADirectVendor;
            this.IsAReseller = trueModel.IsAReseller;
            this.IsEnabled = trueModel.IsEnabled;
            this.SellsViaResellers = trueModel.SellsViaResellers;
            this.AskForSalaryKey = trueModel.AskForSalaryKey;
            this.VerificationType = trueModel.VerificationType;

            errors = string.Empty;
            return true;
        }

        /// <summary>
        /// Saves this service to the DB.
        /// </summary>
        /// <param name="vendorId">The vendor id.</param>
        /// <param name="exec">The transaction.</param>
        internal override void SaveToDb(int vendorId, CStoredProcedureExec exec)
        {
            this.VendorInfoId = vendorId;

            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@ServiceT", this.ServiceType),
                new SqlParameter("@IsADirectVendor", this.IsADirectVendor),
                new SqlParameter("@SellsViaResellers ", this.SellsViaResellers),
                new SqlParameter("@IsAReseller", this.IsAReseller),
                new SqlParameter("@VoeVerificationT", this.VerificationType),
                new SqlParameter("@VendorId", this.VendorInfoId),
                new SqlParameter("@IsEnabled", this.IsEnabled),
                new SqlParameter("@AskForSalaryKey", this.AskForSalaryKey)
            };

            SqlParameter vendorServiceIdOutput = null;
            string sp = null;
            var vendorServiceIsNew = this.VendorServiceId < 0;

            if (vendorServiceIsNew)
            {
                vendorServiceIdOutput = new SqlParameter("@VendorServiceId", System.Data.SqlDbType.Int);
                vendorServiceIdOutput.Direction = System.Data.ParameterDirection.Output;
                parameters.Add(vendorServiceIdOutput);

                sp = CreateVOEService;
            }
            else
            {
                parameters.Add(new SqlParameter("@VendorServiceId", this.VendorServiceId));

                sp = UpdateVOEService;
            }

            exec.ExecuteNonQuery(sp, 2, parameters.ToArray());

            if (vendorServiceIsNew)
            {
                this.VendorServiceId = (int)vendorServiceIdOutput.Value;
            }
        }

        /// <summary>
        /// Creates a view model out of this service.
        /// </summary>
        /// <returns>The view model.</returns>
        internal override AbstractVOXVendorServiceViewModel ToViewModel()
        {
            VOEVendorServiceViewModel viewModel = new VOEVendorServiceViewModel();
            viewModel.IsADirectVendor = this.IsADirectVendor;
            viewModel.IsAReseller = this.IsAReseller;
            viewModel.SellsViaResellers = this.SellsViaResellers;
            viewModel.VendorInfoId = this.VendorInfoId;
            viewModel.VendorServiceId = this.VendorServiceId;
            viewModel.IsEnabled = this.IsEnabled;
            viewModel.VerificationType = this.VerificationType;
            viewModel.AskForSalaryKey = this.AskForSalaryKey;

            return viewModel;
        }
    }
}
