﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using ObjLib.ServiceCredential;
    using Security;

    /// <summary>
    /// The VOE component loader.
    /// </summary>
    public class VOELoader : VOXLoader
    {
        /// <summary>
        /// Lazy instance for all the VOE orders for this loan.
        /// </summary>
        private Lazy<IEnumerable<VOEOrder>> orders;

        /// <summary>
        /// Initializes a new instance of the <see cref="VOELoader"/> class.
        /// </summary>
        /// <param name="voxVendorIdToCredential">Mapping of VOX vendor id to service credentials.</param>
        /// <param name="availableEdocs">The available EDocs.</param>
        /// <param name="lenderServices">The lender services available.</param>
        /// <param name="groups">The groups for checking VOX services.</param>
        /// <param name="loanData">The loan data.</param>
        /// <param name="principal">The principal of the user loading the service.</param>
        internal VOELoader(
            Lazy<Dictionary<int, ServiceCredential>> voxVendorIdToCredential,
            Lazy<HashSet<Guid>> availableEdocs,
            Lazy<Dictionary<int, VOXLenderService>> lenderServices,
            Lazy<Dictionary<Guid, Group>> groups,
            CPageData loanData,
            AbstractUserPrincipal principal) 
            : base(voxVendorIdToCredential, availableEdocs, lenderServices, groups, loanData, principal)
        {
            this.orders = new Lazy<IEnumerable<VOEOrder>>(() => VOEOrder.GetOrdersForLoan(this.BrokerId, this.LoanId));
        }

        /// <summary>
        /// Gets the type of VOX service.
        /// </summary>
        protected override VOXServiceT ServiceType
        {
            get { return VOXServiceT.VOE; }
        }

        /// <summary>
        /// Gets a bindable list of borrowers to provide for choosing borrowers to verify.
        /// </summary>
        /// <returns>A list of verifiable borrowers.</returns>
        public Dictionary<string, string> GetBorrowers()
        {
            var borrowerDictionary = new Dictionary<string, string>();
            foreach (CAppData app in this.LoanData.Apps)
            {
                if ((app.aBTypeT == E_aTypeT.Individual || app.aBTypeT == E_aTypeT.CoSigner) && !string.IsNullOrEmpty(app.aBSsn))
                {
                    borrowerDictionary.Add($"{app.aAppId:N}_{E_BorrowerModeT.Borrower:D}", app.aBNm);
                }

                if ((app.aCTypeT == E_aTypeT.Individual || app.aCTypeT == E_aTypeT.CoSigner) && !string.IsNullOrEmpty(app.aCSsn))
                {
                    borrowerDictionary.Add($"{app.aAppId:N}_{E_BorrowerModeT.Coborrower:D}", app.aCNm);
                }
            }

            return borrowerDictionary;
        }

        /// <summary>
        /// Gets the employment records that can be used for ordering VOE orders.
        /// </summary>
        /// <returns>The employment records that can be used for ordering VOE orders.</returns>
        public IEnumerable<EmploymentToInclude> GetEmploymentRecords()
        {
            var employmentRecordOrderStatus = this.MapEmploymentToOrderStatus();
            List<EmploymentToInclude> records = new List<EmploymentToInclude>();
            for (int i = 0; i < this.LoanData.nApps; i++)
            {
                CAppData app = this.LoanData.GetAppData(i);

                bool borrowerValid = app.aBTypeT == E_aTypeT.Individual || app.aBTypeT == E_aTypeT.CoSigner;
                bool coborrowerValid = app.aBHasSpouse && (app.aCTypeT == E_aTypeT.Individual || app.aCTypeT == E_aTypeT.CoSigner);
                if (!borrowerValid && !coborrowerValid)
                {
                    continue;
                }

                if (borrowerValid)
                {
                    foreach (IEmploymentRecord empRec in app.aBEmpCollection.GetSubcollection(true, E_EmpGroupT.All))
                    {
                        var employment = this.CreateEmploymentToInclude(empRec, E_BorrowerModeT.Borrower, employmentRecordOrderStatus, app);
                        if (employment == null)
                        {
                            continue;
                        }

                        records.Add(employment);
                    }
                }

                if (coborrowerValid)
                {
                    foreach (IEmploymentRecord empRec in app.aCEmpCollection.GetSubcollection(true, E_EmpGroupT.All))
                    {
                        var employment = this.CreateEmploymentToInclude(empRec, E_BorrowerModeT.Coborrower, employmentRecordOrderStatus, app);
                        if (employment == null)
                        {
                            continue;
                        }

                        records.Add(employment);
                    }
                }
            }

            return records;
        }

        /// <summary>
        /// Gets all the VOE orders for this loan.
        /// </summary>
        /// <returns>The VOE orders for this loan.</returns>
        protected override IEnumerable<AbstractVOXOrder> GetOrders()
        {
            return this.orders.Value.Cast<AbstractVOXOrder>();
        }

        /// <summary>
        /// Maps employment records to most recent order's status.
        /// </summary>
        /// <returns>Employment record to the order status of the most recent VOE order with that record.</returns>
        private Dictionary<Guid, Tuple<VOXOrderStatusT, VOEVerificationT>> MapEmploymentToOrderStatus()
        {
            Dictionary<Guid, Tuple<VOXOrderStatusT, VOEVerificationT>> employmentRecordOrderStatus = new Dictionary<Guid, Tuple<VOXOrderStatusT, VOEVerificationT>>();
            foreach (var order in this.orders.Value.OrderBy((order) => order.DateOrdered))
            {
                foreach (var record in order.EmploymentRecords)
                {
                    if (!employmentRecordOrderStatus.ContainsKey(record.Id))
                    {
                        employmentRecordOrderStatus.Add(record.Id, new Tuple<VOXOrderStatusT, VOEVerificationT>(order.CurrentStatus, order.VerificationType));
                    }
                }
            }

            return employmentRecordOrderStatus;
        }

        /// <summary>
        /// Creates an <see cref="EmploymentToInclude"/> from the employment record.
        /// </summary>
        /// <param name="employmentRecord">The employment record.</param>
        /// <param name="borrowerType">The borrower type.</param>
        /// <param name="employmentRecordOrderStatus">The employment record order statuses.</param>
        /// <param name="app">The application.</param>
        /// <returns>The employment record. Null if not a valid employment record.</returns>
        private EmploymentToInclude CreateEmploymentToInclude(IEmploymentRecord employmentRecord, E_BorrowerModeT borrowerType, Dictionary<Guid, Tuple<VOXOrderStatusT, VOEVerificationT>> employmentRecordOrderStatus, CAppData app)
        {
            if (string.IsNullOrEmpty(employmentRecord.EmplrNm))
            {
                return null;
            }

            var endDate = string.Empty;
            if (employmentRecord is IRegularEmploymentRecord)
            {
                endDate = employmentRecord.IsCurrent ? string.Empty : ((IRegularEmploymentRecord)employmentRecord).EmplmtEndD_rep;
            }

            Tuple<VOXOrderStatusT, VOEVerificationT> order = null;
            employmentRecordOrderStatus.TryGetValue(employmentRecord.RecordId, out order);
            EmploymentToInclude model = new EmploymentToInclude()
            {
                AppId = app.aAppId,
                Borrower = borrowerType == E_BorrowerModeT.Borrower ? app.aBNm : app.aCNm,
                BorrowerType = borrowerType.ToString("D"),
                EmploymentRecordId = employmentRecord.RecordId,
                Employer = employmentRecord.EmplrNm,
                IsSelfEmployment = employmentRecord.IsSelfEmplmt ? "Yes" : "No",
                IsCurrent = employmentRecord.IsCurrent ? "Yes" : "No",
                EmploymentEndDate = employmentRecord.IsCurrent ? string.Empty : endDate,
                YrsOnJob = (employmentRecord.EmplmtLenInYrs + Math.Round(employmentRecord.EmplmtLenInMonths / 12.0, 2)).ToString(),
                PositionTitle = employmentRecord.JobTitle,
                OrderStatus = order?.Item1.ToString() ?? "None",
                Type = order == null ? string.Empty : order.Item2 == VOEVerificationT.Employment ? "VOE" : "VOI"
            };

            return model;
        }

        /// <summary>
        /// Employment records that can be used in VOE orders.
        /// </summary>
        public class EmploymentToInclude
        {
            /// <summary>
            /// Gets the key for the employment record.
            /// </summary>
            /// <value>The key for the employment record.</value>
            public string Key
            {
                get
                {
                    return this.AppId.ToString() + this.BorrowerType;
                }
            }

            /// <summary>
            /// Gets or sets the app id.
            /// </summary>
            /// <value>The app id.</value>
            public Guid AppId
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets which borrower this record belongs to.
            /// </summary>
            /// <value>Which borrower this record belongs to.</value>
            public string BorrowerType
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the record id.
            /// </summary>
            /// <value>The record id.</value>
            public Guid EmploymentRecordId
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the borrower name.
            /// </summary>
            /// <value>The borrower name.</value>
            public string Borrower
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the employer name.
            /// </summary>
            /// <value>The employer name.</value>
            public string Employer
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets if this is a self employment record.
            /// </summary>
            /// <value>If this is a self employment record.</value>
            public string IsSelfEmployment
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets if this is current employment.
            /// </summary>
            /// <value>If this is current employment.</value>
            public string IsCurrent
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the employment end date.
            /// </summary>
            /// <value>The employment end date.</value>
            public string EmploymentEndDate
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the years on the job.
            /// </summary>
            /// <value>The years on the job.</value>
            public string YrsOnJob
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the position or title.
            /// </summary>
            /// <value>The position or title.</value>
            public string PositionTitle
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the order status.
            /// </summary>
            /// <value>The order status.</value>
            public string OrderStatus
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the verification type.
            /// </summary>
            /// <value>The verification type.</value>
            public string Type
            {
                get; set;
            }
        }
    }
}
