﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using Common;
    using DataAccess;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Abstract class representing a VOX order.
    /// </summary>
    public abstract class AbstractVOXOrder
    {
        /// <summary>
        /// Stored procedure to get order and lender service counts.
        /// </summary>
        private const string GetVOXLenderServiceAndOrderCounts = "GetVOXLenderServiceAndOrderCounts";

        /// <summary>
        /// List of new vendor reference ids to save.
        /// </summary>
        private List<string> newVendorReferenceIdsToSave = new List<string>();

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractVOXOrder"/> class.
        /// </summary>
        /// <param name="record">The database record of an order.</param>
        /// <param name="documentIds">The associated document ids.</param>
        /// <param name="vendorReferenceIds">The vendor reference ids.</param>
        protected AbstractVOXOrder(IReadOnlyDictionary<string, object> record, List<Guid> documentIds, HashSet<string> vendorReferenceIds)
        {
            this.BrokerId = (Guid)record["BrokerId"];
            this.LoanId = (Guid)record["LoanId"];
            this.ApplicationId = (Guid)record["ApplicationId"];
            this.OrderId = (int)record["OrderId"];

            this.TransactionId = (Guid)record["TransactionId"];
            this.VendorId = (int)record["VendorId"];
            this.ProviderId = (int)record["ProviderId"];
            this.HasBeenRefreshed = (bool)record["HasBeenRefreshed"];
            this.RefreshedFromOrderId = record["RefreshedFromOrderId"] as int?;
            this.IsTestOrder = (bool)record["IsTestOrder"];
            this.LenderServiceId = (int)record["LenderServiceId"];
            this.LenderServiceName = (string)record["LenderServiceName"];
            this.OrderNumber = (string)record["OrderNumber"];
            this.CurrentStatus = (VOXOrderStatusT)record["Status"];
            this.CurrentStatusDescription = record["StatusDescription"] as string;
            this.OrderedBy = (string)record["OrderedBy"];
            this.DateOrdered = (DateTime)record["DateOrdered"];
            this.DateCompleted = record["DateCompleted"] as DateTime?;
            this.ErrorMessage = record["ErrorMessage"] as string;
            this.NotificationEmail = record["NotificationEmail"] as string;
            this.AssociatedEdocs = documentIds;
            this.VendorReferenceIds = vendorReferenceIds ?? new HashSet<string>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractVOXOrder"/> class.
        /// </summary>
        /// <param name="requestData">The source data of the UI's request.</param>
        /// <param name="orderRequest">The request of the order metadata that was sent.</param>
        /// <param name="serviceResponse">The response to the order.</param>
        /// <returns>An order object with a corresponding database entry.</returns>
        protected AbstractVOXOrder(AbstractVOXRequestData requestData, VOXRequestOrder orderRequest, VOXServiceResponse serviceResponse)
        {
            this.BrokerId = requestData.BrokerId;
            this.LoanId = requestData.LoanId;
            this.ApplicationId = orderRequest.ApplicationId;
            this.OrderId = -1;

            this.TransactionId = orderRequest.TransactionId;
            this.VendorId = requestData.LenderService.AssociatedVendorId;
            this.ProviderId = requestData.LenderService.AssociatedResellerId;
            this.HasBeenRefreshed = false;
            if (requestData.PreviousOrder != null)
            {
                requestData.PreviousOrder.HasBeenRefreshed = true;
                this.RefreshedFromOrderId = requestData.PreviousOrder.OrderId;
            }

            this.IsTestOrder = requestData.LenderService.VendorForProtocolAndTransmissionData.IsTestVendor;
            this.LenderServiceId = requestData.LenderServiceId;
            this.LenderServiceName = requestData.LenderService.DisplayName;
            this.OrderNumber = serviceResponse.VendorOrderId;
            this.CurrentStatus = serviceResponse.CurrentStatus;
            this.CurrentStatusDescription = serviceResponse.CurrentStatusDescription;
            this.OrderedBy = requestData.Principal.DisplayName;
            this.DateOrdered = requestData.RequestDate;
            this.DateCompleted = serviceResponse.DateCompleted;
            this.AssociatedEdocs = serviceResponse.DocumentIds;
            this.ErrorMessage = serviceResponse.ErrorMessage;
            this.NotificationEmail = requestData.NotificationEmail;
            this.VendorReferenceIds = serviceResponse.VendorReferenceIds ?? new HashSet<string>();
            this.newVendorReferenceIdsToSave.AddRange(this.VendorReferenceIds);
        }

        /// <summary>
        /// Gets the broker id for this order.
        /// </summary>
        /// <value>The broker id.</value>
        public Guid BrokerId { get; }

        /// <summary>
        /// Gets the loan id for this order.
        /// </summary>
        /// <value>The loan id.</value>
        public Guid LoanId { get; }

        /// <summary>
        /// Gets the application id for this order.
        /// </summary>
        /// <value>The application id.</value>
        /// <remarks>
        /// The primary requirement here is to determine where future documents should be saved.
        /// </remarks>
        public Guid ApplicationId { get; }

        /// <summary>
        /// Gets the order id.
        /// </summary>
        /// <value>The order id.</value>
        public int OrderId { get; private set; }

        /// <summary>
        /// Gets the unique identifier for each transaction, used primarily for resolving asynchronous updates to a particular order.
        /// </summary>
        /// <value>The unique identifier for each transaction.</value>
        public Guid TransactionId { get; }

        /// <summary>
        /// Gets the id of the vendor of the chosen lender service for this order.
        /// Should not change from the initial order.
        /// </summary>
        /// <value>The id of the vendor of the lender service for this order.</value>
        public int VendorId { get; }

        /// <summary>
        /// Gets the id of the reseller of the chosen lender service for this order.
        /// </summary>
        /// <value>The id of the reseller of the lender service for this order.</value>
        public int ProviderId { get; }

        /// <summary>
        /// Gets a value indicating whether this order has been refreshed once.
        /// </summary>
        /// <value>Whether this order has been refreshed once.</value>
        public bool HasBeenRefreshed { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this order can be refreshed.
        /// </summary>
        /// <value>Whether the order can be refreshed.</value>
        public virtual bool CanBeRefreshed { get; }

        /// <summary>
        /// Gets the <see cref="OrderId"/> of the order that this order was refreshed from.
        /// </summary>
        /// <value>The <see cref="OrderId"/> of the order that this order was refreshed from.</value>
        public int? RefreshedFromOrderId { get; }

        /// <summary>
        /// Gets a value indicating whether the order was placed as a test order.
        /// </summary>
        /// <value>A value indicating whether the order was placed as a test order.</value>
        public bool IsTestOrder { get; }

        /// <summary>
        /// Gets the identifier of the lender's service provider.
        /// </summary>
        /// <value>The service provider id.</value>
        public int LenderServiceId { get; private set; }

        /// <summary>
        /// Gets the name of the lender's service provider.
        /// </summary>
        /// <value>The service provider name.</value>
        public string LenderServiceName { get; private set; }

        /// <summary>
        /// Gets the order number.
        /// </summary>
        /// <value>The order number.</value>
        public string OrderNumber { get; }

        /// <summary>
        /// Gets the status of the order.
        /// </summary>
        /// <value>The status of the order.</value>
        public VOXOrderStatusT CurrentStatus { get; private set; }

        /// <summary>
        /// Gets or sets the list of past statuses of the order.
        /// </summary>
        public List<VOXStatus> Statuses { get; protected set; }

        /// <summary>
        /// Gets the description of the order's status.
        /// </summary>
        /// <value>The description of the order's status.</value>
        public string CurrentStatusDescription { get; private set; }

        /// <summary>
        /// Gets the name of who requested this order.
        /// </summary>
        /// <value>Who requested this order.</value>
        public string OrderedBy { get; }

        /// <summary>
        /// Gets when the order was requested.
        /// </summary>
        /// <value>When the order was requested.</value>
        public DateTime DateOrdered { get; }

        /// <summary>
        /// Gets when this order was completed.
        /// </summary>
        /// <value>When this order was completed.</value>
        public DateTime? DateCompleted { get; private set; }

        /// <summary>
        /// Gets or sets the associated edocs for this order.
        /// </summary>
        /// <value>The associated edocs.</value>
        public IEnumerable<Guid> AssociatedEdocs { get; set; }

        /// <summary>
        /// Gets the error returned by the vendor if this order is in the error status.
        /// </summary>
        /// <value>The message from the vendor if order is in error status.</value>
        public string ErrorMessage { get; private set; }

        /// <summary>
        /// Gets the notification email for this order.
        /// </summary>
        /// <value>The notification email for this order.</value>
        public string NotificationEmail { get; }

        /// <summary>
        /// Gets the vendor reference ids.
        /// </summary>
        public HashSet<string> VendorReferenceIds { get; private set; }

        /// <summary>
        /// Checks if the loan has any orders or has any lender services available for use.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <returns>True if either of the two counts are non-zero. False otherwise.</returns>
        public static bool LoanHasOrdersOrLenderServices(Guid loanId, Guid brokerId)
        {
            StoredProcedureName? procedureName = StoredProcedureName.Create(GetVOXLenderServiceAndOrderCounts);
            if (!procedureName.HasValue)
            {
                throw new DeveloperException(LqbGrammar.DataTypes.ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {GetVOXLenderServiceAndOrderCounts}."));
            }

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@BrokerId", brokerId)
            };

            int lenderServiceCount = 0;
            int orderCount = 0;
            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);
            using (var connection = DbAccessUtils.GetConnection(brokerId))
            using (var reader = driver.ExecuteReader(connection, null, procedureName.Value, parameters))
            {
                if (reader.Read())
                {
                    lenderServiceCount = (int)reader["LenderServiceCount"];
                    orderCount = (int)reader["OrderCount"];
                }
            }

            return lenderServiceCount != 0 || orderCount != 0;
        }

        /// <summary>
        /// Updates the lender service used for this order in case it has been deleted.
        /// </summary>
        /// <returns>True if updated, false otherwise.</returns>
        public bool UpdateForLenderService()
        {
            if (this.OrderId == -1)
            {
                return false;
            }

            VOXLenderService service = VOXLenderService.GetLenderServiceById(this.BrokerId, this.LenderServiceId);
            if (service == null)
            {
                var newLenderService = VOXLenderService.FindReplacementLenderService(this.BrokerId, this.VendorId, this.ProviderId);
                if (newLenderService == null)
                {
                    this.CurrentStatus = VOXOrderStatusT.Error;
                    this.ErrorMessage = "Service provider has been removed. Please see your administrator.";
                }
                else
                {
                    this.LenderServiceId = newLenderService.Value.Key;
                    this.LenderServiceName = newLenderService.Value.Value;
                }

                this.SaveUpdatedOrderInDatabase();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Updates an order based on the response from a "Get" request.
        /// </summary>
        /// <param name="requestData">The source data of the UI's request.</param>
        /// <param name="orderRequest">The request of the order metadata that was sent.</param>
        /// <param name="serviceResponse">The response to the order.</param>
        public virtual void UpdateForGetRequest(AbstractVOXRequestData requestData, VOXRequestOrder orderRequest, VOXServiceResponse serviceResponse)
        {
            this.Statuses = serviceResponse.Statuses;
            this.CurrentStatus = serviceResponse.CurrentStatus;
            this.CurrentStatusDescription = serviceResponse.CurrentStatusDescription;
            this.DateCompleted = serviceResponse.DateCompleted;
            this.ErrorMessage = serviceResponse.ErrorMessage;
            this.AssociatedEdocs = serviceResponse.DocumentIds;
            this.VendorReferenceIds = this.VendorReferenceIds ?? new HashSet<string>();
            foreach (var vendorReferenceId in serviceResponse.VendorReferenceIds.CoalesceWithEmpty())
            {
                if (this.VendorReferenceIds.Add(vendorReferenceId))
                {
                    this.newVendorReferenceIdsToSave.Add(vendorReferenceId);
                }
            }

            this.SaveUpdatedOrderInDatabase();
        }

        /// <summary>
        /// Creates a view model out of this order.
        /// </summary>
        /// <param name="extraInfo">Extra info outside of the order.</param>
        /// <returns>The view model.</returns>
        /// <remarks>
        /// This is using a standard pattern for return type covariance as described
        /// by <c>Eric Lippert</c> here: <c>http://stackoverflow.com/a/5709191/2946652</c>.
        /// </remarks>
        public AbstractVOXOrderViewModel CreateViewModel(ExtraOrderViewModelInfo extraInfo)
        {
            return this.CreateAbstractViewModel(extraInfo);
        }

        /// <summary>
        /// Creates a dictionary mapping orders to documents.
        /// </summary>
        /// <param name="documents">The documents for all pulled orders.</param>
        /// <returns>Dictionary mapping order to doc ids.</returns>
        protected static Dictionary<int, List<Guid>> CreateOrderToDocsMap(IReadOnlyList<IReadOnlyDictionary<string, object>> documents)
        {
            Dictionary<int, List<Guid>> orderToDocuments = new Dictionary<int, List<Guid>>();
            foreach (var document in documents)
            {
                int assocOrderId = (int)document["OrderId"];
                Guid docId = (Guid)document["DocumentId"];
                List<Guid> docIdsForOrder;
                if (!orderToDocuments.TryGetValue(assocOrderId, out docIdsForOrder))
                {
                    docIdsForOrder = new List<Guid>();
                    orderToDocuments.Add(assocOrderId, docIdsForOrder);
                }

                docIdsForOrder.Add(docId);
            }

            return orderToDocuments;
        }

        /// <summary>
        /// Pairs each reference number to the associated order's id.
        /// </summary>
        /// <param name="vendorReferenceIdRows">The returned vendor reference id rows.</param>
        /// <returns>The dictionary mapping order ids to the set of vendor reference ids.</returns>
        protected static Dictionary<int, HashSet<string>> CreateOrderToVendorReferenceIdsMap(IReadOnlyList<IReadOnlyDictionary<string, object>> vendorReferenceIdRows)
        {
            Dictionary<int, HashSet<string>> orderToVendorReferenceIds = new Dictionary<int, HashSet<string>>();
            foreach (var referenceIdRow in vendorReferenceIdRows.CoalesceWithEmpty())
            {
                int orderId = (int)referenceIdRow["OrderId"];
                string referenceId = (string)referenceIdRow["VendorReferenceId"];

                HashSet<string> referenceIdsForOrder;
                if (!orderToVendorReferenceIds.TryGetValue(orderId, out referenceIdsForOrder))
                {
                    referenceIdsForOrder = new HashSet<string>();
                    orderToVendorReferenceIds.Add(orderId, referenceIdsForOrder);
                }

                referenceIdsForOrder.Add(referenceId);
            }

            return orderToVendorReferenceIds;
        }

        /// <summary>
        /// Creates a view model out of this order.
        /// </summary>
        /// <param name="extraInfo">Extra info outside of the model.</param>
        /// <returns>The view model.</returns>
        protected abstract AbstractVOXOrderViewModel CreateAbstractViewModel(ExtraOrderViewModelInfo extraInfo);

        /// <summary>
        /// Saves a newly created order to the database.
        /// </summary>
        /// <param name="parentOrderForUpdate">The order's parent, if it needs an update (as is the case for a refresh).</param>
        protected void SaveNewOrderInDatabase(AbstractVOXOrder parentOrderForUpdate)
        {
            if (parentOrderForUpdate == null)
            {
                this.SaveChangesToDatabase(this.CreateOrderInDatabase);
            }
            else
            {
                this.SaveChangesToDatabase(
                    storedProcedureExecutionContext =>
                    {
                        this.CreateOrderInDatabase(storedProcedureExecutionContext);
                        parentOrderForUpdate.UpdateOrderInDatabase(storedProcedureExecutionContext);
                    });
            }
        }

        /// <summary>
        /// Saves a set of updates to an order to the database.
        /// </summary>
        protected void SaveUpdatedOrderInDatabase()
        {
            this.SaveChangesToDatabase(this.UpdateOrderInDatabase);
        }

        /// <summary>
        /// Handles order creation in the database, for the current instance's data.
        /// </summary>
        /// <param name="storedProcedureExecutionContext">The context of the stored procedure, i.e. connection and transaction.</param>
        protected virtual void CreateOrderInDatabase(DataAccess.CStoredProcedureExec storedProcedureExecutionContext)
        {
            var orderIdParameter = new SqlParameter("@OrderId", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@LoanId", this.LoanId),
                new SqlParameter("@ApplicationId", this.ApplicationId),
                new SqlParameter("@TransactionId", this.TransactionId),
                new SqlParameter("@VendorId", this.VendorId),
                new SqlParameter("@ProviderId", this.ProviderId),
                new SqlParameter("@HasBeenRefreshed", this.HasBeenRefreshed),
                new SqlParameter("@RefreshedFromOrderId", NullableDbParameterValue(this.RefreshedFromOrderId)),
                new SqlParameter("@IsTestOrder", this.IsTestOrder),
                new SqlParameter("@LenderServiceId", this.LenderServiceId),
                new SqlParameter("@LenderServiceName", this.LenderServiceName),
                new SqlParameter("@OrderNumber", this.OrderNumber),
                new SqlParameter("@Status", this.CurrentStatus),
                new SqlParameter("@StatusDescription", NullableDbParameterValue(this.CurrentStatusDescription)),
                new SqlParameter("@OrderedBy", this.OrderedBy),
                new SqlParameter("@DateOrdered", this.DateOrdered),
                new SqlParameter("@DateCompleted", NullableDbParameterValue(this.DateCompleted)),
                new SqlParameter("@ErrorMessage", NullableDbParameterValue(this.ErrorMessage)),
                new SqlParameter("@NotificationEmail", NullableDbParameterValue(this.NotificationEmail)),
                orderIdParameter
            };

            storedProcedureExecutionContext.ExecuteNonQuery("VOX_ORDER_Create", parameters);
            this.OrderId = (int)orderIdParameter.Value;

            foreach (var doc in this.AssociatedEdocs)
            {
                var docParams = new SqlParameter[]
                {
                    new SqlParameter("@OrderId", this.OrderId),
                    new SqlParameter("@DocumentId", doc)
                };

                storedProcedureExecutionContext.ExecuteNonQuery("VOX_DOCUMENT_Insert", docParams);
            }

            this.SaveVendorReferenceIds(storedProcedureExecutionContext);
            this.SaveStatuses(storedProcedureExecutionContext);
        }

        /// <summary>
        /// Handles updates to orders already present in the database, for the current instance's data.
        /// </summary>
        /// <param name="storedProcedureExecutionContext">The context of the stored procedure, i.e. connection and transaction.</param>
        protected virtual void UpdateOrderInDatabase(DataAccess.CStoredProcedureExec storedProcedureExecutionContext)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@OrderId", this.OrderId),
                new SqlParameter("@HasBeenRefreshed", this.HasBeenRefreshed),
                new SqlParameter("@LenderServiceId", this.LenderServiceId),
                new SqlParameter("@LenderServiceName", this.LenderServiceName),
                new SqlParameter("@Status", this.CurrentStatus),
                new SqlParameter("@StatusDescription", NullableDbParameterValue(this.CurrentStatusDescription)),
                new SqlParameter("@DateCompleted", NullableDbParameterValue(this.DateCompleted)),
                new SqlParameter("@ErrorMessage", NullableDbParameterValue(this.ErrorMessage)),
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@LoanId", this.LoanId),
            };

            storedProcedureExecutionContext.ExecuteNonQuery("VOX_ORDER_Update", parameters);

            var removeParams = new SqlParameter[]
            {
                new SqlParameter("@OrderId", this.OrderId)
            };

            storedProcedureExecutionContext.ExecuteNonQuery("VOX_DOCUMENT_RemoveByOrderId", removeParams);

            // I don't expect that many documents being associated in a single order so this should be fine.
            // If not, we can look into doing a bulk insert.
            foreach (var docs in this.AssociatedEdocs)
            {
                var docParams = new SqlParameter[]
                {
                    new SqlParameter("@OrderId", this.OrderId),
                    new SqlParameter("@DocumentId", docs)
                };

                storedProcedureExecutionContext.ExecuteNonQuery("VOX_DOCUMENT_Insert", docParams);
            }

            this.SaveVendorReferenceIds(storedProcedureExecutionContext);
            this.SaveStatuses(storedProcedureExecutionContext);
        }

        /// <summary>
        /// Coalesces the value with <see cref="DBNull.Value"/>.
        /// </summary>
        /// <param name="value">The value to coalesce.</param>
        /// <returns>A value that can be transmitted to SQL Server.</returns>
        private static object NullableDbParameterValue(object value)
        {
            return value ?? DBNull.Value;
        }

        /// <summary>
        /// Coerces an empty Guid to <see cref="DBNull.Value"/> to avoid violating a foreign key constraint.
        /// </summary>
        /// <param name="id">The GUID value.</param>
        /// <returns>A value that can be transmitted to SQL Server.</returns>
        private static object NullableForeignKeyGuid(Guid? id)
        {
            if (!id.HasValue || id.Value == Guid.Empty)
            {
                return DBNull.Value;
            }

            return id.Value;
        }

        /// <summary>
        /// Applies a save action in a transaction.
        /// </summary>
        /// <param name="saveAction">The save action to apply.</param>
        private void SaveChangesToDatabase(Action<DataAccess.CStoredProcedureExec> saveAction)
        {
            using (var storedProcedureExecutionContext = new DataAccess.CStoredProcedureExec(this.BrokerId))
            {
                try
                {
                    storedProcedureExecutionContext.BeginTransactionForWrite();
                    saveAction(storedProcedureExecutionContext);
                    storedProcedureExecutionContext.CommitTransaction();
                }
                catch
                {
                    storedProcedureExecutionContext.RollbackTransaction();
                    throw;
                }
            }
        }

        /// <summary>
        /// Saves the VOX status list to the database.
        /// </summary>
        /// <param name="storedProcedureExecutionContext">The execution context.</param>
        private void SaveStatuses(CStoredProcedureExec storedProcedureExecutionContext)
        {
            if (this.Statuses == null || !this.Statuses.Any())
            {
                return;
            }

            storedProcedureExecutionContext.ExecuteNonQuery("VOX_ORDER_STATUSES_DeleteByOrderId", new[] { new SqlParameter("@OrderId", this.OrderId) });

            foreach (var status in this.Statuses)
            {
                var referenceNumberParams = new SqlParameter[]
                {
                    new SqlParameter("@OrderId", this.OrderId),
                    new SqlParameter("@StatusCode", status.StatusCode),
                    new SqlParameter("@StatusDescription", status.StatusDescription),
                    new SqlParameter("@StatusTime", status.StatusDateTime)
                };

                storedProcedureExecutionContext.ExecuteNonQuery("VOX_ORDER_STATUSES_Insert", referenceNumberParams);
            }
        }

        /// <summary>
        /// Saves the new vendor reference numbers to the database.
        /// </summary>
        /// <param name="storedProcedureExecutionContext">The execution context.</param>
        private void SaveVendorReferenceIds(CStoredProcedureExec storedProcedureExecutionContext)
        {
            foreach (var referenceNumberToSave in this.newVendorReferenceIdsToSave)
            {
                var referenceNumberParams = new SqlParameter[]
                {
                    new SqlParameter("@OrderId", this.OrderId),
                    new SqlParameter("@VendorReferenceId", referenceNumberToSave)
                };

                storedProcedureExecutionContext.ExecuteNonQuery("VOX_ORDER_VENDOR_REFERENCE_ID_Insert", referenceNumberParams);
            }
        }
    }
}
