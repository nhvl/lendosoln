﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using DataAccess;
    using EDocs;

    /// <summary>
    /// Abstract view model for VOX orders.
    /// </summary>
    public abstract class AbstractVOXOrderViewModel
    {
        /// <summary>
        /// Gets the service type.
        /// </summary>
        /// <value>The service type.</value>
        public abstract VOXServiceT ServiceType { get; }

        /// <summary>
        /// Gets or sets the order id.
        /// </summary>
        /// <value>The order id.</value>
        public int OrderId { get; set; }

        /// <summary>
        /// Gets or sets the service provider name.
        /// </summary>
        /// <value>The service provider name.</value>
        public string LenderServiceName { get; set; }

        /// <summary>
        /// Gets or sets the order number.
        /// </summary>
        /// <value>The order number.</value>
        public string OrderNumber { get; set; }

        /// <summary>
        /// Gets the order status as a string.
        /// </summary>
        /// <value>The order status as a string.</value>
        public string StatusString
        {
            get
            {
                return GetOrderStatusString(this.Status);
            }
        }

        /// <summary>
        /// Gets or sets the order status.
        /// </summary>
        /// <value>The order status.</value>
        public VOXOrderStatusT Status { get; set; }

        /// <summary>
        /// Gets or sets the description of the order's status.
        /// </summary>
        /// <value>The description of the order's status.</value>
        public string StatusDescription { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this order has a list of statuses in addition to the most recent status.
        /// </summary>
        /// <value>Whether there is a list of statuses.</value>
        public bool HasStatusList { get; set; } = false;

        /// <summary>
        /// Gets or sets a value indicating whether this order can be refreshed.
        /// </summary>
        /// <value>Whether this order can be refreshed.</value>
        public bool CanBeRefreshed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this order has been refreshed.
        /// </summary>
        /// <value>Whether this order has been refreshed.</value>
        public bool HasBeenRefreshed { get; set; }

        /// <summary>
        /// Gets or sets the ordered by.
        /// </summary>
        /// <value>The ordered by.</value>
        public string OrderedBy { get; set; }

        /// <summary>
        /// Gets or sets the date the order was made.
        /// </summary>
        /// <value>The date the order was made.</value>
        public string DateOrdered { get; set; }

        /// <summary>
        /// Gets or sets the date the order was completed.
        /// </summary>
        /// <value>The date the order was completed.</value>
        public string DateCompleted { get; set; }

        /// <summary>
        /// Gets the EDocs associated with the order.
        /// </summary>
        /// <value>The Edocs associated with the order.</value>
        public List<EDocView> AssociatedEdocs { get; private set; } = new List<EDocView>();

        /// <summary>
        /// Gets or sets the error message associated with an errored order.
        /// </summary>
        /// <value>The error message associated with an errored order.</value>
        public string Error { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this order needs an account id for later requests.
        /// </summary>
        /// <value>Whether this order needs an account id for later requests.</value>
        public bool RequiresAccountId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the lender service uses an account id.
        /// </summary>
        /// <value>Whether the lender service uses an account id.</value>
        public bool UsesAccountId { get; set; }

        /// <summary>
        /// Gets or sets the service credential id.
        /// </summary>
        /// <value>The service credential id.</value>
        public int ServiceCredentialId { get; set; } = -1;

        /// <summary>
        /// Gets or sets a value indicating whether the service credential has an account id.
        /// </summary>
        /// <value>Whether the service credential has an account id.</value>
        public bool ServiceCredentialHasAccountId { get; set; }

        /// <summary>
        /// Gets or sets the vendor reference ids.
        /// </summary>
        public List<string> VendorReferenceIds { get; set; }

        /// <summary>
        /// Creates a string out of the order status.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <returns>The status as a string.</returns>
        public static string GetOrderStatusString(VOXOrderStatusT? status)
        {
            if (!status.HasValue)
            {
                return "N/A";
            }

            switch (status.Value)
            {
                case VOXOrderStatusT.Canceled:
                    return "Canceled";
                case VOXOrderStatusT.Complete:
                    return "Complete";
                case VOXOrderStatusT.Error:
                    return "Error";
                case VOXOrderStatusT.NoHit:
                    return "No Hit";
                case VOXOrderStatusT.Pending:
                    return "Pending";
                case VOXOrderStatusT.Active:
                    return "Active";
                default:
                    throw new UnhandledEnumException(status);
            }
        }

        /// <summary>
        /// Gets the doc name prefix.
        /// </summary>
        /// <returns>The name prefix.</returns>
        public abstract string GetDocNamePrefix();

        /// <summary>
        /// Sets the associated edocs.
        /// </summary>
        /// <param name="docIds">The doc ids.</param>
        /// <param name="availableEdocIds">The available edocs ids.</param>
        public void SetAssociatedEdocs(IEnumerable<Guid> docIds, HashSet<Guid> availableEdocIds)
        {
            string docDescriptionPrefix = this.GetDocNamePrefix();
            int count = 1;
            List<EDocView> docs = new List<EDocView>();
            Dictionary<Guid, string> docsWithNames = new Dictionary<Guid, string>();
            foreach (var id in docIds)
            {
                var doc = new EDocView();
                doc.EDocId = id;
                doc.IsAvailable = availableEdocIds == null ? false : availableEdocIds.Contains(id);

                var deletedPart = doc.IsAvailable ? string.Empty : "(deleted)";
                doc.DocumentName = $"{docDescriptionPrefix} Order {this.OrderNumber}, Doc {count++}{deletedPart}";
                docs.Add(doc);
            }

            this.AssociatedEdocs = docs;
        }
    }
}
