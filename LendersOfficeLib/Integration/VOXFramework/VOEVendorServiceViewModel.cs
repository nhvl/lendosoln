﻿namespace LendersOffice.Integration.VOXFramework
{
    /// <summary>
    /// View model for VOE services.
    /// </summary>
    public class VOEVendorServiceViewModel : AbstractVOXVendorServiceViewModel
    {
        /// <summary>
        /// Gets or sets a value indicating whether a salary key should be provided.
        /// </summary>
        /// <value>Whether a salary key should be provided.</value>
        public bool AskForSalaryKey
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the service type.
        /// </summary>
        /// <value>The service type.</value>
        public override VOXServiceT ServiceType
        {
            get
            {
                return VOXServiceT.VOE;
            }
        }

        /// <summary>
        /// Gets or sets the VOE verification type.
        /// </summary>
        /// <value>The VOE verification type.</value>
        public VOEVerificationT VerificationType
        {
            get;
            set;
        }
    }
}
