﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Data class to hold some data needed in an order view model that doesn't exist inside the order itself.
    /// </summary>
    public class ExtraOrderViewModelInfo
    {
        /// <summary>
        /// Gets or sets a value indicating whether an account id is used.
        /// </summary>
        /// <value>Whether an account is used.</value>
        public bool UsesAccountId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether an account id is required.
        /// </summary>
        /// <value>Whether an account id is required.</value>
        public bool RequiresAccountId { get; set; }

        /// <summary>
        /// Gets or sets the service credential id.
        /// </summary>
        /// <value>The service credential id.</value>
        public int ServiceCredentialId { get; set; } = -1;

        /// <summary>
        /// Gets or sets a value indicating whether the service credential has an account id.
        /// </summary>
        /// <value>Whether the service credential has an account id.</value>
        public bool ServiceCredentialHasAccountId { get; set; }

        /// <summary>
        /// Gets or sets the available edoc ids.
        /// </summary>
        /// <value>The edoc ids.</value>
        public HashSet<Guid> AvailableEdocs { get; set; }
    }
}
