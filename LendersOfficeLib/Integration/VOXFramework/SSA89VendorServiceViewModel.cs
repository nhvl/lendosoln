namespace LendersOffice.Integration.VOXFramework
{
    /// <summary>
    /// View model for SSA-89 services.
    /// </summary>
    public class SSA89VendorServiceViewModel : AbstractVOXVendorServiceViewModel
    {
        /// <summary>
        /// Gets the service type.
        /// </summary>
        /// <value>The service type.</value>
        public override VOXServiceT ServiceType => VOXServiceT.SSA_89;
    }
}
