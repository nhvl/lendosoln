﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using Common;
    using DataAccess;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Class for SSA-89 Order.
    /// </summary>
    public class SSA89Order : AbstractVOXOrder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SSA89Order"/> class.
        /// </summary>
        /// <param name="record">The database record of an order.</param>
        /// <param name="documentIds">The document ids for this order.</param>
        private SSA89Order(IReadOnlyDictionary<string, object> record, List<Guid> documentIds)
            : base(record, documentIds, null)
        {
            this.IsForCoborrower = (bool)record["IsForCoborrower"];
            this.BorrowerName = (string)record["BorrowerName"];
            this.LastFourSSN = (string)record["LastFourSsn"];
            this.DateOfBirth = record["DateOfBirth"] as DateTime?;
            this.DeathMasterStatus = (SSA89SubproductStatus)record["DeathMasterStatus"];
            this.SocialSecurityAdministrationStatus = (SSA89SubproductStatus)record["SocialSecurityAdministrationStatus"];
            this.OfficeOfForeignAssetsControlStatus = (SSA89SubproductStatus)record["OfficeOfForeignAssetsControlStatus"];
            this.CreditHeaderStatus = (SSA89SubproductStatus)record["CreditHeaderStatus"];
            this.RequestFormAccepted = record["RequestFormAccepted"] as bool?;
            this.RequestFormRejectionReason = record["RequestFormRejectionReason"] as string;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SSA89Order"/> class.
        /// </summary>
        /// <param name="requestData">The source data of the UI's request.</param>
        /// <param name="orderRequest">The request of the order metadata that was sent.</param>
        /// <param name="serviceResponse">The response to the order.</param>
        /// <returns>An order object with a corresponding database entry.</returns>
        private SSA89Order(SSA89RequestData requestData, VOXRequestOrder orderRequest, SSA89ServiceResponse serviceResponse)
            : base(requestData, orderRequest, serviceResponse)
        {
            var existingOrder = (SSA89Order)requestData.PreviousOrder;
            this.IsForCoborrower = existingOrder?.IsForCoborrower ?? orderRequest.IsForCoborrower.Value;
            this.DeathMasterStatus = serviceResponse.DmStatus;
            this.SocialSecurityAdministrationStatus = serviceResponse.SsaStatus;
            this.OfficeOfForeignAssetsControlStatus = serviceResponse.OfacStatus;
            this.CreditHeaderStatus = serviceResponse.ChStatus;
            this.RequestFormAccepted = serviceResponse.RequestFormAccepted;
            this.RequestFormRejectionReason = serviceResponse.RequestFormRejectionReason;

            if (existingOrder != null)
            {
                this.BorrowerName = existingOrder.BorrowerName;
                this.DateOfBirth = existingOrder.DateOfBirth;
                this.LastFourSSN = existingOrder.LastFourSSN;
            }
            else
            {
                this.SetFieldsFromApp(this.LoanId, this.ApplicationId, this.IsForCoborrower ? E_BorrowerModeT.Coborrower : E_BorrowerModeT.Borrower);
            }
        }

        /// <summary>
        /// Gets a value indicating whether this order was for the coborrower for the associated app.
        /// </summary>
        /// <value>Whether the order was for the coborrower of the associated app.</value>
        public bool IsForCoborrower { get; }

        /// <summary>
        /// Gets the name of the borrower.
        /// </summary>
        /// <value>The name of the borrower.</value>
        public string BorrowerName { get; private set; }

        /// <summary>
        /// Gets the LastFourSSN of the borrower. This only displays the last 4 digits of the LastFourSSN.
        /// </summary>
        /// <value>The LastFourSSN of the borrower.</value>
        public string LastFourSSN { get; private set; }

        /// <summary>
        /// Gets or the borrower's date of birth.
        /// </summary>
        /// <value>The borrower's date of birth.</value>
        public DateTime? DateOfBirth { get; private set; }

        /// <summary>
        /// Gets the status of the CH subproduct.
        /// </summary>
        /// <value>The status of the CH subproduct.</value>
        public SSA89SubproductStatus CreditHeaderStatus { get; private set; }

        /// <summary>
        /// Gets the status of the DM subproduct.
        /// </summary>
        /// <value>The status of the DM subproduct.</value>
        public SSA89SubproductStatus DeathMasterStatus { get; private set; }

        /// <summary>
        /// Gets the status of the SSA subproduct.
        /// </summary>
        /// <value>The status of the SSA subproduct.</value>
        public SSA89SubproductStatus SocialSecurityAdministrationStatus { get; private set; }

        /// <summary>
        /// Gets the status of the OFAC subproduct.
        /// </summary>
        /// <value>The status of the OFAC subproduct.</value>
        public SSA89SubproductStatus OfficeOfForeignAssetsControlStatus { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the request form was accepted.
        /// </summary>
        /// <value>Whether the request form was accepted.</value>
        public bool? RequestFormAccepted { get; private set; }

        /// <summary>
        /// Gets the rejection reason if the request form was not accepted.
        /// </summary>
        /// <value>The rejection reason.</value>
        public string RequestFormRejectionReason { get; private set; }

        /// <summary>
        /// Retrieves all verification of assets orders for this loan.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="loanId">The loan id.</param>
        /// <returns>The list of verification of assets orders.</returns>
        public static IEnumerable<SSA89Order> GetOrdersForLoan(Guid brokerId, Guid loanId)
        {
            return GetOrdersForLoan(brokerId, loanId, null);
        }

        /// <summary>
        /// Retrieves an order.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="orderId">The order id.</param>
        /// <returns>The order if found; null otherwise.</returns>
        public static SSA89Order GetOrderForLoan(Guid brokerId, Guid loanId, int orderId)
        {
            return GetOrdersForLoan(brokerId, loanId, orderId).SingleOrDefault();
        }

        /// <summary>
        /// Creates a new record of an order in the database from a placed order.
        /// </summary>
        /// <param name="requestData">The source data of the UI's request.</param>
        /// <param name="orderRequest">The request of the order metadata that was sent.</param>
        /// <param name="serviceResponse">The response to the order.</param>
        /// <returns>The order object representing the database's entry.</returns>
        public static SSA89Order CreateOrder(SSA89RequestData requestData, VOXRequestOrder orderRequest, SSA89ServiceResponse serviceResponse)
        {
            SSA89Order order = new SSA89Order(requestData, orderRequest, serviceResponse);
            order.SaveNewOrderInDatabase(requestData.PreviousOrder);
            return order;
        }

        /// <summary>
        /// Updates an order based on the response from a "Get" request.
        /// </summary>
        /// <param name="requestData">The source data of the UI's request.</param>
        /// <param name="orderRequest">The request of the order metadata that was sent.</param>
        /// <param name="serviceResponse">The response to the order.</param>
        public override void UpdateForGetRequest(AbstractVOXRequestData requestData, VOXRequestOrder orderRequest, VOXServiceResponse serviceResponse)
        {
            var ssa89ServiceResponse = serviceResponse as SSA89ServiceResponse;
            this.DeathMasterStatus = ssa89ServiceResponse.DmStatus;
            this.SocialSecurityAdministrationStatus = ssa89ServiceResponse.SsaStatus;
            this.OfficeOfForeignAssetsControlStatus = ssa89ServiceResponse.OfacStatus;
            this.CreditHeaderStatus = ssa89ServiceResponse.ChStatus;
            this.RequestFormAccepted = ssa89ServiceResponse.RequestFormAccepted;
            this.RequestFormRejectionReason = ssa89ServiceResponse.RequestFormRejectionReason;
            base.UpdateForGetRequest(requestData, orderRequest, serviceResponse); 
        }

        /// <summary>
        /// Creates a view model out of this loan.
        /// </summary>
        /// <param name="extraInfo">Any extra info needed to add to the view model.</param>
        /// <returns>The view model.</returns>
        public new SSA89OrderViewModel CreateViewModel(ExtraOrderViewModelInfo extraInfo)
        {
            SSA89OrderViewModel viewModel = new SSA89OrderViewModel()
            {
                BorrowerName = this.BorrowerName,
                ChStatus = this.CreditHeaderStatus,
                DateCompleted = this.DateCompleted?.ToString("G") ?? string.Empty,
                DateOfBirth = this.DateOfBirth?.ToString("d") ?? string.Empty,
                DateOrdered = this.DateOrdered.ToString("G"),
                DmStatus = this.DeathMasterStatus,
                Error = this.ErrorMessage,
                CanBeRefreshed = false,
                HasBeenRefreshed = false,
                LenderServiceName = this.LenderServiceName,
                OfacStatus = this.OfficeOfForeignAssetsControlStatus,
                OrderedBy = this.OrderedBy,
                OrderId = this.OrderId,
                OrderNumber = this.OrderNumber,
                SsaFormAccepted = this.RequestFormAccepted,
                SsaFormRejectionReason = this.RequestFormRejectionReason,
                SsaStatus = this.SocialSecurityAdministrationStatus,
                Ssn = "***-**-" + this.LastFourSSN,
                Status = this.CurrentStatus,
                StatusDescription = this.CurrentStatusDescription,
                HasEdoc = this.AssociatedEdocs.Any(),
            };

            viewModel.SetAssociatedEdocs(this.AssociatedEdocs, extraInfo?.AvailableEdocs);
            if (extraInfo != null)
            {
                viewModel.RequiresAccountId = extraInfo.RequiresAccountId;
                viewModel.UsesAccountId = extraInfo.UsesAccountId;
                viewModel.ServiceCredentialId = extraInfo.ServiceCredentialId;
                viewModel.ServiceCredentialHasAccountId = extraInfo.ServiceCredentialHasAccountId;
            }

            return viewModel;
        }

        /// <summary>
        /// Creates a view model out of this loan.
        /// </summary>
        /// <param name="extraInfo">Extra info outside of the order.</param>
        /// <returns>The view model.</returns>
        protected override AbstractVOXOrderViewModel CreateAbstractViewModel(ExtraOrderViewModelInfo extraInfo)
        {
            return this.CreateViewModel(extraInfo);
        }

        /// <summary>
        /// Handles order creation in the database, for the current instance's data.
        /// </summary>
        /// <param name="storedProcedureExecutionContext">The context of the stored procedure, i.e. connection and transaction.</param>
        protected override void CreateOrderInDatabase(DataAccess.CStoredProcedureExec storedProcedureExecutionContext)
        {
            base.CreateOrderInDatabase(storedProcedureExecutionContext); // The base implementation will determine the OrderId
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@OrderId", this.OrderId),
                new SqlParameter("@IsForCoborrower", this.IsForCoborrower),
                new SqlParameter("@BorrowerName", this.BorrowerName),
                new SqlParameter("@LastFourSsn", this.LastFourSSN),
                new SqlParameter("@DateOfBirth", this.DateOfBirth),
                new SqlParameter("@DeathMasterStatus", this.DeathMasterStatus),
                new SqlParameter("@SocialSecurityAdministrationStatus", this.SocialSecurityAdministrationStatus),
                new SqlParameter("@OfficeOfForeignAssetsControlStatus", this.OfficeOfForeignAssetsControlStatus),
                new SqlParameter("@CreditHeaderStatus", this.CreditHeaderStatus),
                new SqlParameter("@RequestFormAccepted", this.RequestFormAccepted),
                new SqlParameter("@RequestFormRejectionReason", this.RequestFormRejectionReason),
            };

            storedProcedureExecutionContext.ExecuteNonQuery("SSA89_ORDER_Create", parameters);
        }

        /// <summary>
        /// Handles updates to orders already present in the database, for the current instance's data.
        /// </summary>
        /// <param name="storedProcedureExecutionContext">The context of the stored procedure, i.e. connection and transaction.</param>
        protected override void UpdateOrderInDatabase(DataAccess.CStoredProcedureExec storedProcedureExecutionContext)
        {
            base.UpdateOrderInDatabase(storedProcedureExecutionContext);

            var parameters = new SqlParameter[]
            {
                new SqlParameter("@OrderId", this.OrderId),
                new SqlParameter("@DeathMasterStatus", (object)this.DeathMasterStatus ?? DBNull.Value),
                new SqlParameter("@SocialSecurityAdministrationStatus", (object)this.SocialSecurityAdministrationStatus ?? DBNull.Value),
                new SqlParameter("@OfficeOfForeignAssetsControlStatus", (object)this.OfficeOfForeignAssetsControlStatus ?? DBNull.Value),
                new SqlParameter("@CreditHeaderStatus", (object)this.CreditHeaderStatus ?? DBNull.Value),
                new SqlParameter("@RequestFormAccepted", (object)this.RequestFormAccepted ?? DBNull.Value),
                new SqlParameter("@RequestFormRejectionReason", (object)this.RequestFormRejectionReason ?? DBNull.Value),
            };

            storedProcedureExecutionContext.ExecuteNonQuery("SSA89_ORDER_Update", parameters);
        }

        /// <summary>
        /// Retrieves the verification of social security number orders for the specified loan, optionally restricted to a single order.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="orderId">The order id.</param>
        /// <returns>The list of verification of social security number orders.</returns>
        private static IReadOnlyList<SSA89Order> GetOrdersForLoan(Guid brokerId, Guid loanId, int? orderId)
        {
            string storedProcedureName = "SSA89_ORDER_Retrieve";
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@OrderId", orderId)
            };

            IReadOnlyList<IReadOnlyList<IReadOnlyDictionary<string, object>>> results = DataAccess.StoredProcedureHelper.ExecuteReaderIntoResultListDictionaries(brokerId, storedProcedureName, parameters);
            IReadOnlyList<IReadOnlyDictionary<string, object>> orderRecords = results[0];
            Dictionary<int, List<Guid>> orderToDocIds = CreateOrderToDocsMap(results[1]);

            if (results.Count > 2)
            {
                throw new LqbGrammar.Exceptions.DeveloperException(LqbGrammar.DataTypes.ErrorMessage.SystemError);
            }

            var orders = new List<SSA89Order>(orderRecords.Count);
            foreach (var orderRecord in orderRecords)
            {
                int orderRecordId = (int)orderRecord["OrderId"];
                orders.Add(new SSA89Order(orderRecord, orderToDocIds.GetValueOrNull(orderRecordId) ?? new List<Guid>()));
            }

            return orders;
        }

        /// <summary>
        /// Pulls the necessary fields from the app.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="appId">The app id.</param>
        /// <param name="borrowerMode">The borrower mode.</param>
        private void SetFieldsFromApp(Guid loanId, Guid appId, E_BorrowerModeT borrowerMode)
        {
            var loanData = CPageData.CreateUsingSmartDependency(loanId, typeof(SSA89Order));
            loanData.InitLoad();

            var appData = loanData.GetAppData(appId);
            if (appData == null)
            {
                throw new DeveloperException(LqbGrammar.DataTypes.ErrorMessage.SystemError, new SimpleContext($"Unable to load app. LoanId: {loanId} AppId: {appId}"));
            }

            appData.BorrowerModeT = borrowerMode;
            this.BorrowerName = appData.aNm;
            this.DateOfBirth = appData.aDob.DateTimeForComputation;
            this.LastFourSSN = borrowerMode == E_BorrowerModeT.Borrower ? appData.aBSsnLastFour : appData.aCSsnLastFour;
        }
    }
}
