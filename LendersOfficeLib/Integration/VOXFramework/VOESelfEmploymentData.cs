﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;

    /// <summary>
    /// Self employment data for VOE requests.
    /// </summary>
    public class VOESelfEmploymentData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOESelfEmploymentData"/> class.
        /// </summary>
        /// <param name="appId">The app id.</param>
        /// <param name="isCoborrower">Whether this borrowr is the coborrower of the app.</param>
        public VOESelfEmploymentData(Guid appId, bool isCoborrower)
        {
            this.AppId = appId;
            this.IsCoborrower = isCoborrower;
        }

        /// <summary>
        /// Gets or sets the app id.
        /// </summary>
        /// <value>The app id.</value>
        public Guid AppId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is the coborrower of the app.
        /// </summary>
        /// <value>Whether the borrower is the coborrower of the app.</value>
        public bool IsCoborrower { get; set; }

        /// <summary>
        /// Gets or sets the notes.
        /// </summary>
        /// <value>The notes.</value>
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the taxes are self prepred.
        /// </summary>
        /// <value>Whether the taxes are self prepared.</value>
        public bool TaxesSelfPrepared { get; set; }

        /// <summary>
        /// Gets or sets the comapny name of the tax preparer.
        /// </summary>
        /// <value>The company name of the tax preparer.</value>
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or sets the street address of the tax preparer.
        /// </summary>
        /// <value>The street address of the tax preparer.</value>
        public string TaxStreetAddress { get; set; }

        /// <summary>
        /// Gets or sets the city of the tax preparer.
        /// </summary>
        /// <value>The city of the tax preparer.</value>
        public string TaxCity { get; set; }

        /// <summary>
        /// Gets or sets the state of the tax preparer.
        /// </summary>
        /// <value>The state of the tax preparer.</value>
        public string TaxState { get; set; }

        /// <summary>
        /// Gets or sets the zipcode of the tax preparer.
        /// </summary>
        /// <value>The zipcode of the tax preparer.</value>
        public string TaxZipcode { get; set; }

        /// <summary>
        /// Gets or sets the phone number of the tax preparer.
        /// </summary>
        /// <value>The phone number of the tax preparer.</value>
        public string TaxPhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets the fax number of the tax preparer.
        /// </summary>
        /// <value>The fax number of the tax preparer.</value>
        public string TaxFaxNumber { get; set; }
    }
}
