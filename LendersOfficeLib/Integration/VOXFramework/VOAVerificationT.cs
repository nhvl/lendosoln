﻿namespace LendersOffice.Integration.VOXFramework
{
    /// <summary>
    /// What kind of verification service the VOA vendor provides.
    /// </summary>
    public enum VOAVerificationT
    {
        /// <summary>
        /// Verification of assets.
        /// </summary>
        Assets = 0,

        /// <summary>
        /// Verification of deposits.
        /// </summary>
        Deposits = 1
    }
}
