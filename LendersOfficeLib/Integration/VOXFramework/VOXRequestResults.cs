﻿namespace LendersOffice.Integration.VOXFramework
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Class for holding VOX request results.
    /// </summary>
    public class VOXRequestResults
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOXRequestResults"/> class.
        /// </summary>
        /// <param name="orders">The resulting orders.</param>
        /// <param name="errors">The errors if any.</param>
        /// <param name="subsequentRequestData">The request data that must be resubmitted to fully complete the request.</param>
        public VOXRequestResults(IEnumerable<AbstractVOXOrder> orders, IEnumerable<string> errors, AbstractVOXRequestData subsequentRequestData)
        {
            this.Orders = orders;
            this.Errors = errors;
            this.SubsequentRequestData = subsequentRequestData;
        }

        /// <summary>
        /// Gets a value indicating whether the result indicates an error in the request.
        /// </summary>
        public bool IsError
        {
            get
            {
                return this.Errors != null && this.Errors.Any();
            }
        }

        /// <summary>
        /// Gets the resulting orders.
        /// </summary>
        public IEnumerable<AbstractVOXOrder> Orders
        {
            get;
        }

        /// <summary>
        /// Gets the errors.
        /// </summary>
        public IEnumerable<string> Errors
        {
            get;
        }

        /// <summary>
        /// Gets the request data that must be resubmitted to fully complete the request.
        /// </summary>
        public AbstractVOXRequestData SubsequentRequestData
        {
            get;
        }
    }
}
