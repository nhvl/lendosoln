﻿namespace LendersOffice.Integration.VOXFramework
{
    /// <summary>
    /// Indicates what object a transmission is associated with.
    /// </summary>
    public enum TransmissionAssociationT
    {
        /// <summary>
        /// Transmission details are associated with a platform.
        /// </summary>
        Platform = 0,

        /// <summary>
        /// Transmission details are associated with a vendor.
        /// </summary>
        Vendor = 1
    }
}
