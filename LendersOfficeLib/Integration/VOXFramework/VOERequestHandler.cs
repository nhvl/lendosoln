﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using ConfigSystem.Operations;
    using Conversions.VOXFramework;
    using Conversions.VOXFramework.Mismo;
    using DataAccess;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Takes in request data and processes the request and response.
    /// </summary>
    public class VOERequestHandler : AbstractVOXRequestHandler<VOEOrder, VOERequestData>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOERequestHandler"/> class.
        /// </summary>
        /// <param name="requestData">The data for the request.</param>
        public VOERequestHandler(VOERequestData requestData)
            : base(requestData)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VOERequestHandler"/> class.
        /// </summary>
        /// <param name="requestData">The data for the request.</param>
        /// <param name="predefinedOrder">A predefined VOX order.</param>
        /// <remarks>
        /// Special constructor that takes in a VOXRequestOrder instead of generating orders. Currently this
        /// is only used for some system-generated requests.
        /// </remarks>
        public VOERequestHandler(VOERequestData requestData, VOXRequestOrder predefinedOrder)
            : base(requestData, predefinedOrder)
        {
        }

        /// <summary>
        /// Gets the workflow operation required to submit a request.
        /// </summary>
        /// <value>The workflow operation required to submit a request.</value>
        protected override WorkflowOperation RequiredOperation => WorkflowOperations.OrderVoe;

        /// <summary>
        /// Gets the document type to save the document as.
        /// </summary>
        /// <value>The document type to save the document as.</value>
        protected override E_AutoSavePage AutoSaveDocType => E_AutoSavePage.VoeVoiDocuments;

        /// <summary>
        /// Creates an instance of the specified order in the database and in memory.
        /// </summary>
        /// <param name="orderRequest">The request of the order metadata that was sent.</param>
        /// <param name="serviceResponse">The response to the order.</param>
        /// <returns>An order object with a corresponding database entry.</returns>
        protected override VOEOrder CreateOrder(VOXRequestOrder orderRequest, VOXServiceResponse serviceResponse)
        {
            return VOEOrder.CreateOrder(this.RequestData, orderRequest, serviceResponse);
        }

        /// <summary>
        /// Creates an instance of the MCL request provider for this instance.
        /// </summary>
        /// <returns>The MCL request provider for this instance.</returns>
        protected override AbstractMismoVOXRequestProvider CreateMismoRequestProvider()
        {
            return new MismoVOERequestProvider(this.RequestData, this.IndividualServiceOrdersToRequest);
        }

        /// <summary>
        /// Generates a response provider from the vendor's response string.
        /// </summary>
        /// <param name="responseData">The response from the vendor.</param>
        /// <returns>The response provider of the request.</returns>
        protected override AbstractVOXResponseProvider GenerateResponseProvider(VOXServerResponseData responseData)
        {
            var format = this.RequestData.PayloadFormatT;
            switch (format)
            {
                case PayloadFormatT.LQBMismo:
                    return new MismoVOXResponseProvider(responseData.Content, this.RequestData);
                case PayloadFormatT.MCLSmartApi:
                    var response = new MismoVOXResponseProvider(responseData.Content, this.RequestData);
                    return this.GetMCLOrders(response);

                default:
                    throw new UnhandledEnumException(format);
            }
        }

        /// <summary>
        /// Overrides an order with Error status to instead save it to the DB for MCL REQUESTS ONLY. This is preferred for
        /// MCL's VOE as many services can be returned for a single order, so letting the user look at them individually
        /// instead of dumping all errors in a single popup can make it clearer where the problem occurred.
        /// </summary>
        /// <param name="orders">A list of processed orders.</param>
        /// <param name="errors">A list of errors.</param>
        /// <param name="orderRequest">An order request.</param>
        /// <param name="serviceResponse">A service response from the vendor.</param>
        protected override void ProcessError(List<VOEOrder> orders, List<string> errors, VOXRequestOrder orderRequest, VOXServiceResponse serviceResponse)
        {
            if (this.RequestData.PayloadFormatT == PayloadFormatT.MCLSmartApi && !string.IsNullOrEmpty(serviceResponse.VendorOrderId))
            {
                this.ProcessNew(orders, orderRequest, serviceResponse);
                return;
            }
            else if (!string.IsNullOrEmpty(serviceResponse.VendorOrderId))
            {
                base.ProcessError(orders, errors, orderRequest, serviceResponse);
            }
        }

        /// <summary>
        /// Retrieves the order corresponding to the given service response.
        /// </summary>
        /// <param name="serviceResponse">A service response.</param>
        /// <returns>The corresponding order.</returns>
        protected override VOXRequestOrder GetOrderForServiceResponse(VOXServiceResponse serviceResponse)
        {
            var order = base.GetOrderForServiceResponse(serviceResponse);

            if (this.RequestData.PayloadFormatT == PayloadFormatT.MCLSmartApi)
            {
                // While we send per-employment services to MCL, they send per-borrower services back.
                // This means that if we sent multiple employment orders for a borrower, the response
                // will only link back to one. We must include the employment information from other
                // orders for the same borrower to prevent that information from being lost.
                var allVerifiedEmploymentsAssociatedWithBorrower = this.IndividualServiceOrdersToRequest
                        .Where(e => e.ApplicationId == order.ApplicationId && e.IsForCoborrower == order.IsForCoborrower)
                        .SelectMany(e => e.LinkedRecords)
                        .Distinct()
                        .ToList()
                        .AsReadOnly();

                order = new VOXRequestOrder(
                    order.TransactionId,
                    order.ApplicationId,
                    allVerifiedEmploymentsAssociatedWithBorrower,
                    order.IsForCoborrower);
            }

            return order;
        }

        /// <summary>
        /// Performs a Get on each returned MCL that is not an error status and includes the returned
        /// services in the response provider before handing it back.
        /// </summary>
        /// <param name="abstractResponse">The abstract response provider.</param>
        /// <returns>A response provider populated with all relevant services.</returns>
        protected AbstractVOXResponseProvider GetMCLOrders(AbstractVOXResponseProvider abstractResponse)
        {
            if (this.RequestData.RequestType == VOXRequestT.Get)
            {
                // This automation should not run for Get orders.
                return abstractResponse;
            }

            // Give MCL time to hit TWN before we start sending Gets.
            Thread.Sleep(10 * 1000); // ms

            var responseProvider = (MismoVOXResponseProvider)abstractResponse;
            var responsesToReturn = new List<VOXServiceResponse>();

            var transactionIds = new HashSet<Guid>();
            foreach (var response in responseProvider.ServiceResponses)
            {
                if (response.CurrentStatus == VOXOrderStatusT.Error)
                {
                    responsesToReturn.Add(response);
                    continue;
                }

                List<VOXServiceResponse> automaticGetResponses = this.TriggerAutomaticGet(response);
                foreach (var getResponse in automaticGetResponses)
                {
                    transactionIds.Add(getResponse.TransactionId);
                    getResponse.AllTransactionIds = transactionIds;
                }

                responsesToReturn.AddRange(automaticGetResponses);
            }

            responseProvider.ServiceResponses = responsesToReturn;
            return responseProvider;
        }

        /// <summary>
        /// Sends a system-generated Get request for a given service response to an Initial request.
        /// </summary>
        /// <param name="serviceResponse">A service response from MCL.</param>
        /// <returns>A list of service responses to the automated Get request.</returns>
        protected List<VOXServiceResponse> TriggerAutomaticGet(VOXServiceResponse serviceResponse)
        {
            var getRequestData = new VOERequestData(this.RequestData, VOXRequestT.Get, isSystemGenerated: true);
            getRequestData.GeneratedOrderNumber = serviceResponse.VendorOrderId;

            var requestOrder = this.IndividualServiceOrdersToRequest.Single(o => o.TransactionId == serviceResponse.TransactionId);
            var getRequestHandler = new VOERequestHandler(getRequestData, requestOrder);
            getRequestHandler.SubmitRequest(doAudit: false);

            string parentOrderNumber = serviceResponse.VendorOrderId;
            foreach (var response in getRequestHandler.ResponseData.ServiceResponses)
            {
                response.ParentOrderId = parentOrderNumber == response.VendorOrderId ? string.Empty : parentOrderNumber;
            }

            return getRequestHandler.ResponseData.ServiceResponses;
        }

        /// <summary>
        /// Splits the request data into the orders that will be sent to the vendor.
        /// </summary>
        /// <returns>The orders that will be sent to the vendor.</returns>
        protected override IReadOnlyCollection<VOXRequestOrder> CreateServiceOrdersFromRequestData()
        {
            var previousOrder = (VOEOrder)this.RequestData.PreviousOrder;
            if (this.RequestData.RequestType == VOXRequestT.Get)
            {
                return new[]
                {
                    VOXRequestOrder.ForGetRequest(previousOrder, previousOrder.EmploymentRecords.Select(r => r.Id).ToList())
                };
            }
            else if (this.RequestData.RequestType == VOXRequestT.Refresh)
            {
                return new[]
                {
                    VOXRequestOrder.ForRefreshRequest(previousOrder, previousOrder.EmploymentRecords.Select(r => r.Id).ToList())
                };
            }
            else if (this.RequestData.RequestType == VOXRequestT.Initial && !this.RequestData.VerifiesBorrower)
            {
                var orders = new List<VOXRequestOrder>();
                foreach (var employmentRecord in this.RequestData.EmploymentData)
                {
                    orders.Add(VOXRequestOrder.ForInitialRequest(
                        employmentRecord.AppId,
                        employmentRecord.IsForCoborrower,
                        new[] { employmentRecord.EmploymentRecordId }));
                }

                return orders;
            }
            else if (this.RequestData.RequestType == VOXRequestT.Initial 
                && this.RequestData.VerifiesBorrower 
                && this.RequestData.BorrowerAppId.HasValue
                && this.RequestData.BorrowerType.HasValue)
            {
                return new[]
                {
                    VOXRequestOrder.ForInitialRequest(
                        this.RequestData.BorrowerAppId.Value,
                        this.RequestData.BorrowerType == E_BorrowerModeT.Coborrower,
                        new Guid[] { })
                };
            }
            else
            {
                throw new UnhandledEnumException(this.RequestData.RequestType);
            }
        }

        /// <summary>
        /// Creates an Equifax OFX request provider.
        /// </summary>
        /// <returns>The Equifax OFX request provider.</returns>
        protected override IVOXRequestProvider CreateEquifaxOfxRequestProvider()
        {
            // Not supported.
            throw new DeveloperException(ErrorMessage.BadConfiguration);
        }
    }
}
