﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using EDocs;
    using LqbGrammar.DataTypes;
    using Security;
    using static Audit.AssetImportAuditItem;

    /// <summary>
    /// Contains static methods for saving imported verification framework assets.
    /// </summary>
    public static class AssetImport
    {
        /// <summary>
        /// Gets the totals of assets for the order borrower and the whole loan file, if the given order were to be applied to the loan.
        /// </summary>
        /// <param name="assetsToIgnore">A list of Asset record IDs to ignore in the sum. These assets would be removed when applying the order from the UI.</param>
        /// <param name="loan">The loan to compute loan totals for.</param>
        /// <param name="app">The application to compute borrower totals for.</param>
        /// <param name="order">The order containing new assets to import and info about which borrower to calculate for.</param>
        /// <returns>A view model dictionary of loan totals to send to the UI.</returns>
        public static Dictionary<string, decimal> GetAssetTotals(ISet<Guid> assetsToIgnore, CPageData loan, CAppData app, VOAOrder order)
        {
            HashSet<string> verifiedAssetAccountNumbers = new HashSet<string>(order.VerifiedAssetRecords.Select(asset => asset.AccountNumber));
            var borrowerOnFileAssets = app.aAssetCollection.RegularAssets.Where(asset => AssetOwnerMatchesOrderOwner(asset, order)).ToList();
            var loanOnFileAssets = loan.Apps.SelectMany(loanApp => loanApp.aAssetCollection.RegularAssets).ToList();

            var verifiedAssetSum = order.VerifiedAssetRecords.Sum(verifiedAsset => verifiedAsset.AccountBalance);
            var borrowerUpdatedTotal = borrowerOnFileAssets.Where(asset => !assetsToIgnore.Contains(asset.RecordId) && !verifiedAssetAccountNumbers.Contains(asset.AccNum.Value)).Sum(asset => asset.Val)
                + verifiedAssetSum;
            var loanUpdatedTotal = loanOnFileAssets.Where(asset => !assetsToIgnore.Contains(asset.RecordId) && !verifiedAssetAccountNumbers.Contains(asset.AccNum.Value)).Sum(asset => asset.Val)
                + verifiedAssetSum;

            return new Dictionary<string, decimal>
                {
                    { "borrowerOnFileTotal", borrowerOnFileAssets.Sum(asset => asset.Val) },
                    { "loanOnFileTotal", loanOnFileAssets.Sum(asset => asset.Val) },
                    { "borrowerTotal", borrowerUpdatedTotal },
                    { "loanTotal", loanUpdatedTotal }
                };
        }

        /// <summary>
        /// Updates the assets on a loan file according to the request from the UI.
        /// </summary>
        /// <param name="user">The user making the request.</param>
        /// <param name="loan">The loan file to update assets of.</param>
        /// <param name="assets">The collection of assets to update.</param>
        /// <param name="accountUpdates">The list of updates from the UI to execute.</param>
        /// <param name="order">The verifications order record to update from.</param>
        /// <returns>A result of whether the request succeeded.</returns>
        public static Result<List<AssetUpdate>> UpdateAssets(AbstractUserPrincipal user, CPageData loan, IAssetCollection assets, List<AccountUpdateFromClient> accountUpdates, VOAOrder order)
        {
            var returnedAccounts = order.VerifiedAssetRecords.DistinctBy(asset => asset.AccountNumber).ToDictionary(asset => asset.AccountNumber);
            List<AssetUpdate> auditChanges = new List<AssetUpdate>();
            foreach (var accountUpdate in accountUpdates)
            {
                var auditChange = new AssetUpdate();
                VerifiedAsset returnedAccount = null;
                if (!returnedAccounts.TryGetValue(accountUpdate.AccountNumber, out returnedAccount) && !accountUpdate.RemoveAccount)
                {
                    // If there's nothing to update or remove, just skip
                    continue;
                }

                IAssetRegular assetToUpdate;
                if (accountUpdate.AssetId.HasValue)
                {
                    try
                    {
                        assetToUpdate = assets.GetRegRecordOf(accountUpdate.AssetId.Value);
                    }
                    catch (CBaseException ce)
                    {
                        return Result<List<AssetUpdate>>.Failure(ce);
                    }

                    auditChange.AccountNumber = assetToUpdate.AccNum.Value;
                    auditChange.Institution = assetToUpdate.ComNm;
                    auditChange.Balance = assetToUpdate.Val_rep;

                    if (accountUpdate.RemoveAccount)
                    {
                        auditChange.UpdateT = UpdateType.Remove;
                        auditChange.AssetType = assetToUpdate.AssetT_rep;
                        assetToUpdate.IsOnDeathRow = true;
                        auditChanges.Add(auditChange);
                        continue;
                    }
                    else
                    {
                        auditChange.UpdateT = UpdateType.Modify;
                    }
                }
                else
                {
                    assetToUpdate = assets.AddRegularRecord();
                    assetToUpdate.AccNum = returnedAccount.AccountNumber;
                    auditChange.AccountNumber = returnedAccount.AccountNumber;
                    auditChange.UpdateT = UpdateType.New;
                    auditChange.AssetType = assetToUpdate.AssetT_rep;
                }

                assetToUpdate.AssetT = (E_AssetRegularT)returnedAccount.AssetType;
                assetToUpdate.ComNm = returnedAccount.FinancialInstitutionName;
                assetToUpdate.Val = returnedAccount.AccountBalance;

                if (auditChange.Institution != assetToUpdate.ComNm || auditChange.Balance != assetToUpdate.Val_rep)
                {
                    auditChange.NewInstitution = assetToUpdate.ComNm;
                    auditChange.NewBalance = assetToUpdate.Val_rep;
                    auditChange.AssetType = assetToUpdate.AssetT_rep;
                    auditChanges.Add(auditChange);
                }
            }

            loan.RecordAuditOnSave(new Audit.AssetImportAuditItem(user, auditChanges));
            return Result<List<AssetUpdate>>.Success(auditChanges);
        }

        /// <summary>
        /// Determines whether the asset owner of the given asset matches the order type of the given order. Assumes that both have the same appID.
        /// </summary>
        /// <param name="asset">The asset to check.</param>
        /// <param name="order">The order to check.</param>
        /// <returns>True if both belong to the borrower or both belong to the coborrower. Otherwise, false.</returns>
        public static bool AssetOwnerMatchesOrderOwner(IAssetRegular asset, VOAOrder order)
        {
            if (asset == null || order == null)
            {
                return false;
            }

            switch (asset.OwnerT)
            {
                case E_AssetOwnerT.Joint:
                    return true;
                case E_AssetOwnerT.Borrower:
                    return !order.IsForCoborrower;
                case E_AssetOwnerT.CoBorrower:
                    return order.IsForCoborrower;
                default:
                    throw new UnhandledEnumException(asset.OwnerT);
            }
        }

        /// <summary>
        /// Information about an update to an account. Deserialized from JSON.
        /// </summary>
        public class AccountUpdateFromClient
        {
            /// <summary>
            /// Gets or sets the asset record identifier for the account to update.
            /// </summary>
            public Guid? AssetId { get; set; }

            /// <summary>
            /// Gets or sets the account number for the account to update.
            /// </summary>
            public string AccountNumber { get; set; }

            /// <summary>
            /// Gets or sets a value indicating whether the account should be removed from the list of assets.
            /// </summary>
            public bool RemoveAccount { get; set; }
        }
    }
}
