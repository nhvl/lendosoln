﻿namespace LendersOffice.Integration.VOXFramework
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using FrameworkCommon;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Platform data. Holds basic information like transmission model and payload type.
    /// </summary>
    public class VOXPlatform : IntegrationPlatform<VOXTransmissionInfo>
    {
        /// <summary>
        /// The stored procedure name to get all platforms.
        /// </summary>
        private const string ListAllPlatforms = "VERIFICATION_PLATFORM_ListAllPlatforms";

        /// <summary>
        /// The stored procedure name to get a platform with a platform id.
        /// </summary>
        private const string GetPlatformWithPlatformId = "VERIFICATION_PLATFORM_GetPlatformWithPlatformId";

        /// <summary>
        /// The stored procedure name to delete a platform.
        /// </summary>
        private const string DeletePlatformWithPlaformId = "VERIFICATION_PLATFORM_DeletePlatformWithPlaformId";
        
        /// <summary>
        /// Prevents a default instance of the <see cref="VOXPlatform"/> class from being created.
        /// </summary>
        private VOXPlatform()
        {
        }

        /// <summary>
        /// Gets or sets the transmission model for this platform.
        /// </summary>
        /// <value>The transmission model for this platform.</value>
        public TransmissionModelT TransmissionModelT { get; set; }

        /// <summary>
        /// Gets or sets the payload format for this platform.
        /// </summary>
        /// <value>The payload format for this platform.</value>
        public PayloadFormatT PayloadFormatT { get; set; }

        /// <summary>
        /// Gets the stored procedure used to create a new platform.
        /// </summary>
        /// <value>The stored procedure used to create a new platform.</value>
        protected override string CreatePlatformSP => "VERIFICATION_PLATFORM_CreatePlatform";

        /// <summary>
        /// Gets the stored procedure used to update a platform.
        /// </summary>
        /// <value>The stored procedure used to update a platform.</value>
        protected override string UpdatePlatformSP => "VERIFICATION_PLATFORM_UpdatePlatform";

        /// <summary>
        /// Loads a platform with a platform id.
        /// </summary>
        /// <param name="platformId">The platform id.</param>
        /// <returns>Returns the platform if found. Null otherwise.</returns>
        public static VOXPlatform LoadPlatform(int platformId)
        {
            StoredProcedureName procedureName = StoredProcedureName.Create(GetPlatformWithPlatformId).ForceValue();
            return LoadPlatform(platformId, procedureName, () => new VOXPlatform()) as VOXPlatform;
        }

        /// <summary>
        /// Loads all Platforms.
        /// </summary>
        /// <returns>Enumeration of platforms.</returns>
        public static IEnumerable<VOXPlatform> LoadPlatforms()
        {
            StoredProcedureName procedureName = StoredProcedureName.Create(ListAllPlatforms).ForceValue();
            return LoadPlatforms(procedureName, () => new VOXPlatform()).Cast<VOXPlatform>();
        }

        /// <summary>
        /// Deletes the platform and the associated transmission.
        /// </summary>
        /// <param name="platformId">The platform id.</param>
        /// <param name="errors">Any error encountered.</param>
        /// <returns>True if deleted false otherwise.</returns>
        public static bool DeletePlatform(int platformId, out string errors)
        {
            StoredProcedureName procedureName = StoredProcedureName.Create(DeletePlatformWithPlaformId).ForceValue();
            return DeletePlatform(platformId, procedureName, out errors);
        }

        /// <summary>
        /// Creates a platform from the viewmodel.
        /// </summary>
        /// <param name="viewModel">The view model of the platform.</param>
        /// <param name="errors">Any errors found during creation.</param>
        /// <returns>The created platform if successful. Null otherwise.</returns>
        public static VOXPlatform CreateFromViewModel(VOXPlatformViewModel viewModel, out string errors)
        {
            bool isPlatformNew = !viewModel.PlatformId.HasValue || viewModel.PlatformId.Value < 1;

            if (!viewModel.Verify(out errors))
            {
                return null;
            }

            VOXPlatform loadedPlatform = null;
            if (!isPlatformNew)
            {
                loadedPlatform = LoadPlatform(viewModel.PlatformId.Value);
                if (!loadedPlatform.TransmissionInfo.PopulateFromViewModel(viewModel.TransmissionInfo, out errors))
                {
                    return null;
                }
            }
            else
            {
                VOXTransmissionInfo createdTransmission = VOXTransmissionInfo.CreatePlatformTransmissionFromViewModel(viewModel.TransmissionInfo, out errors);
                if (createdTransmission == null)
                {
                    return null;
                }

                loadedPlatform = new VOXPlatform();
                loadedPlatform.PlatformId = -1;
                loadedPlatform.LinkedTransmissionId = -1;
                loadedPlatform.TransmissionInfo = createdTransmission;
            }

            loadedPlatform.PlatformName = viewModel.PlatformName;
            loadedPlatform.TransmissionModelT = viewModel.TransmissionModelT;
            loadedPlatform.PayloadFormatT = viewModel.PayloadFormatT;
            loadedPlatform.UsesAccountId = viewModel.UsesAccountId;
            loadedPlatform.RequiresAccountId = viewModel.RequiresAccountId;
            loadedPlatform.IsNew = isPlatformNew;

            return loadedPlatform;
        }

        /// <summary>
        /// Creates a viewmodel out of this platform.
        /// </summary>
        /// <returns>The viewmodel for this platform.</returns>
        public VOXPlatformViewModel ToViewModel()
        {
            VOXPlatformViewModel viewModel = new VOXPlatformViewModel()
            {
                PlatformId = this.PlatformId,
                PlatformName = this.PlatformName,
                TransmissionModelT = this.TransmissionModelT,
                PayloadFormatT = this.PayloadFormatT,
                UsesAccountId = this.UsesAccountId,
                RequiresAccountId = this.RequiresAccountId,
                TransmissionInfo = this.TransmissionInfo.ToViewModel()
            };

            return viewModel;
        }

        /// <summary>
        /// Creates additional parameters needed to save the platform.
        /// </summary>
        /// <returns>Enumerable containing the additional parameters.</returns>
        protected override IEnumerable<SqlParameter> GetAdditionalPlatformSaveParameters()
        {
            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@TransmissionModelT", this.TransmissionModelT),
                new SqlParameter("@PayloadFormatT", this.PayloadFormatT),
            };

            parameters.AddRange(base.GetAdditionalPlatformSaveParameters());
            return parameters;
        }

        /// <summary>
        /// Loads from the reader.
        /// </summary>
        /// <param name="reader">The reader to load from.</param>
        protected override void LoadFromReader(IDataReader reader)
        {
            base.LoadFromReader(reader);
            VOXTransmissionInfo transmissionInfo = VOXTransmissionInfo.LoadTransmissionFromReader(reader);
            this.TransmissionModelT = (TransmissionModelT)reader["TransmissionModelT"];
            this.PayloadFormatT = (PayloadFormatT)reader["PayloadFormatT"];
            this.TransmissionInfo = transmissionInfo;
        }
    }
}
