﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// SSA89 service response.
    /// </summary>
    public class SSA89ServiceResponse : VOXServiceResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SSA89ServiceResponse"/> class. For Initial requests.
        /// </summary>
        /// <param name="vendorOrderId">The vendor's order ID.</param>
        /// <param name="transactionId">The unique transaction ID.</param>
        /// <param name="status">The order status.</param>
        /// <param name="statusDescription">The status description.</param>
        /// <param name="documents">The documents, in bytes, from the response.</param>
        /// <param name="validationReferenceNumbers">The DU validation reference numbers received in the response.</param>
        public SSA89ServiceResponse(string vendorOrderId, Guid transactionId, VOXOrderStatusT status, string statusDescription, List<byte[]> documents, List<DuValidationReferenceNumberVoxData> validationReferenceNumbers)
            : base(vendorOrderId, transactionId, new List<VOXStatus> { new VOXStatus(status, statusDescription, DateTime.Now) }, documents, validationReferenceNumbers, null)
        {
            this.DmStatus = SSA89SubproductStatus.Pending;
            this.SsaStatus = SSA89SubproductStatus.Pending;
            this.OfacStatus = SSA89SubproductStatus.Pending;
            this.ChStatus = SSA89SubproductStatus.Pending;
            this.RequestFormAccepted = null;
            this.RequestFormRejectionReason = null;
        }

        /// <summary>
        /// Gets or sets the status of the DM subproduct.
        /// </summary>
        /// <value>The status of the DM subproduct.</value>
        public SSA89SubproductStatus DmStatus { get; set; }

        /// <summary>
        /// Gets or sets the status of the SSA subproduct.
        /// </summary>
        /// <value>The status of the SSA subproduct.</value>
        public SSA89SubproductStatus SsaStatus { get; set; }

        /// <summary>
        /// Gets or sets the status of the OFAC subproduct.
        /// </summary>
        /// <value>The status of the OFAC subproduct.</value>
        public SSA89SubproductStatus OfacStatus { get; set; }

        /// <summary>
        /// Gets or sets the status of the CH subproduct.
        /// </summary>
        /// <value>The status of the CH subproduct.</value>
        public SSA89SubproductStatus ChStatus { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the request form was accepted.
        /// </summary>
        /// <value>Whether the request form was accepted.</value>
        public bool? RequestFormAccepted { get; set; }
        
        /// <summary>
        /// Gets or sets the rejection reason if the request form was not accepted.
        /// </summary>
        /// <value>Reason for rejecting the request form.</value>
        public string RequestFormRejectionReason { get; set; }
    }
}
