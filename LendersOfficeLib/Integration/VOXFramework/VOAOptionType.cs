﻿namespace LendersOffice.Integration.VOXFramework
{
    /// <summary>
    /// Indicates a VOA option type.
    /// </summary>
    public enum VOAOptionType
    {
        /// <summary>
        /// The requested length of account history analysis.
        /// </summary>
        AccountHistory = 0,

        /// <summary>
        /// The requested refresh period.
        /// </summary>
        RefreshPeriod = 1
    }
}
