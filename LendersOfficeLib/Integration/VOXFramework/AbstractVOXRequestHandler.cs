﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ConfigSystem.Operations;
    using DataAccess;
    using LendersOffice.Conversions.VOXFramework;
    using LendersOffice.Conversions.VOXFramework.Mismo;
    using ObjLib.Edocs.AutoSaveDocType;
    using Templates;

    /// <summary>
    /// Takes in request data and processes the request and response.
    /// </summary>
    /// <typeparam name="TVOXOrder">The verification order type that will be returned by this instance.</typeparam>
    /// <typeparam name="TVOXRequestData">The request data type that will be used to place or fetch orders.</typeparam>
    /// <remarks>
    /// While generics and abstraction can easily become a slippery slope, they are particularly useful here for input and output type security.
    /// </remarks>
    public abstract class AbstractVOXRequestHandler<TVOXOrder, TVOXRequestData> where TVOXOrder : AbstractVOXOrder where TVOXRequestData : AbstractVOXRequestData
    {
        /// <summary>
        /// The exporter for the request.
        /// </summary>
        private IVOXRequestProvider exporter;

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractVOXRequestHandler{TVOXOrder, TVOXRequestData}"/> class.
        /// </summary>
        /// <param name="requestData">The data for the request.</param>
        public AbstractVOXRequestHandler(TVOXRequestData requestData)
        {
            this.RequestData = requestData;
            this.IndividualServiceOrdersToRequest = this.CreateServiceOrdersFromRequestData();
            this.exporter = this.CreateRequestProvider();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractVOXRequestHandler{TVOXOrder, TVOXRequestData}"/> class.
        /// </summary>
        /// <param name="requestData">The data for the request.</param>
        /// <param name="predefinedOrder">A predefined VOX order.</param>
        /// <remarks>
        /// Special constructor that takes in a VOXRequestOrder instead of generating orders. Currently this
        /// is only used for some system-generated requests.
        /// </remarks>
        public AbstractVOXRequestHandler(TVOXRequestData requestData, VOXRequestOrder predefinedOrder)
        {
            this.RequestData = requestData;
            this.IndividualServiceOrdersToRequest = new[] { predefinedOrder };
            this.exporter = this.CreateRequestProvider();
        }

        /// <summary>
        /// Gets the request configuration.
        /// </summary>
        /// <value>The request configuration.</value>
        public TVOXRequestData RequestData { get; }

        /// <summary>
        /// Gets or sets the response data received from the vendor.
        /// </summary>
        /// <value>The response data received from the order.</value>
        public AbstractVOXResponseProvider ResponseData { get; protected set; } = null;

        /// <summary>
        /// Gets a value indicating whether a response has been received.
        /// </summary>
        /// <value>A boolean indicating whether a response has been received.</value>
        public bool HasResponse
        {
            get
            {
                return this.ResponseData != null;
            }
        }

        /// <summary>
        /// Gets the workflow operation required to submit a request.
        /// </summary>
        /// <value>The workflow operation required to submit a request.</value>
        protected abstract WorkflowOperation RequiredOperation { get; }

        /// <summary>
        /// Gets the document type to save the document as.
        /// </summary>
        /// <value>The document type to save the document as.</value>
        protected abstract E_AutoSavePage AutoSaveDocType { get; }

        /// <summary>
        /// Gets the actual orders from the request data that will be sent to the vendor.
        /// </summary>
        /// <value>The orders that will be sent to the vendor.</value>
        protected IReadOnlyCollection<VOXRequestOrder> IndividualServiceOrdersToRequest { get; }

        /// <summary>
        /// Run an audit of the request.
        /// </summary>
        /// <returns>An audit result.</returns>
        public IntegrationAuditResult AuditRequest()
        {
            return this.exporter.AuditRequest();
        }

        /// <summary>
        /// Submits a verification request to the vendor.
        /// </summary>
        /// <param name="doAudit">Indicates whether an audit should be performed prior to submission.</param>
        /// <returns>A list of orders resulting from the submission.</returns>
        public VOXRequestResults SubmitRequest(bool doAudit = true)
        {
            var errors = new List<string>();
            var workflowResults = Tools.IsWorkflowOperationAuthorized(this.RequestData.Principal, this.RequestData.LoanId, this.RequiredOperation);
            if (!workflowResults.Item1)
            {
                errors.Add(workflowResults.Item2);
                return new VOXRequestResults(orders: null, errors: errors, subsequentRequestData: null);
            }

            var payload = this.exporter.SerializeRequest();
            this.exporter.LogRequest(payload);

            // Need to audit the request here in case the audit function doesn't get called before submitting the request. 
            // It can be bypassed if someone does call the audit function before hand.
            if (doAudit)
            {
                var auditErrors = this.AuditRequest();
                if (auditErrors.HasErrors)
                {
                    errors.AddRange(auditErrors.GetAllErrors().Select(auditItem => auditItem.ErrorMessage).ToList());
                    return new VOXRequestResults(orders: null, errors: errors, subsequentRequestData: null);
                }
            }

            var responseData = this.exporter.Server.SubmitRequest(this.RequestData, payload);
            this.ResponseData = this.GenerateResponseProvider(responseData);

            if (this.ResponseData.HasErrors)
            { 
                errors.AddRange(this.ResponseData.Errors);
            }

            if (!this.RequestData.IsSystemGeneratedRequest)
            {
                var orders = new List<TVOXOrder>(this.ResponseData.ServiceResponses.Count);

                var subsequentRequestData = this.GetSubsequentRequestData(this.ResponseData);
                if (subsequentRequestData != null)
                {
                    return new VOXRequestResults(orders: null, errors: null, subsequentRequestData: subsequentRequestData);
                }

                // Updating the loan from an order should ignore workflow as the user action of placing the order has already cleared workflow
                var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.RequestData.LoanId, typeof(AbstractVOXRequestHandler<TVOXOrder, TVOXRequestData>));
                foreach (VOXServiceResponse serviceResponse in this.ResponseData.ServiceResponses)
                {
                    VOXRequestOrder orderRequest = this.GetOrderForServiceResponse(serviceResponse);
                    RecordDataToLoan(loan, orderRequest, serviceResponse);
                    this.SaveDocumentsToEDocs(orderRequest, serviceResponse);

                    if (this.ShouldUpdateOrder())
                    {
                        this.ProcessUpdate(orders, orderRequest, serviceResponse);
                    }
                    else if (serviceResponse.CurrentStatus == VOXOrderStatusT.Error)
                    {
                        this.ProcessError(orders, errors, orderRequest, serviceResponse);
                    }
                    else
                    {
                        this.ProcessNew(orders, orderRequest, serviceResponse);
                    }
                }

                if (loan.DataState == E_DataState.InitSave)
                {
                    loan.Save();
                }

                return new VOXRequestResults(orders: orders, errors: errors, subsequentRequestData: null);
            }

            return null;
        }

        /// <summary>
        /// Gets the request data that must be submitted to fully complete this request.
        /// </summary>
        /// <param name="responseProvider">The response provider.</param>
        /// <returns>The subsequent request data.</returns>
        protected virtual AbstractVOXRequestData GetSubsequentRequestData(AbstractVOXResponseProvider responseProvider)
        {
            return null;
        }

        /// <summary>
        /// Processes an update to an existing order.
        /// </summary>
        /// <param name="orders">A list of processed orders.</param>
        /// <param name="orderRequest">An order request.</param>
        /// <param name="serviceResponse">A service response from the vendor.</param>
        protected virtual void ProcessUpdate(List<TVOXOrder> orders, VOXRequestOrder orderRequest, VOXServiceResponse serviceResponse)
        {
            TVOXOrder existingOrder = (TVOXOrder)this.RequestData.PreviousOrder;
            existingOrder.UpdateForGetRequest(this.RequestData, orderRequest, serviceResponse);
            orders.Add(existingOrder);
        }

        /// <summary>
        /// Processes an order with an error result, sending the error back to be displayed to the user.
        /// </summary>
        /// <param name="orders">A list of processed orders. This is only used in overridden behavior.</param>
        /// <param name="errors">A list of submission errors.</param>
        /// <param name="orderRequest">An order request.</param>
        /// <param name="serviceResponse">A service response.</param>
        protected virtual void ProcessError(List<TVOXOrder> orders, List<string> errors, VOXRequestOrder orderRequest, VOXServiceResponse serviceResponse)
        {
            errors.Add(serviceResponse.CurrentStatusDescription);
        }

        /// <summary>
        /// Processes and saves a new order.
        /// </summary>
        /// <param name="orders">A list of processes orders.</param>
        /// <param name="orderRequest">An order request.</param>
        /// <param name="serviceResponse">A service response.</param>
        protected virtual void ProcessNew(List<TVOXOrder> orders, VOXRequestOrder orderRequest, VOXServiceResponse serviceResponse)
        {
            orders.Add(this.CreateOrder(orderRequest, serviceResponse));
        }

        /// <summary>
        /// Retrieves the order corresponding to the given service response.
        /// </summary>
        /// <param name="serviceResponse">A service response.</param>
        /// <returns>The corresponding order.</returns>
        protected virtual VOXRequestOrder GetOrderForServiceResponse(VOXServiceResponse serviceResponse)
        {
            return this.IndividualServiceOrdersToRequest.Single(o => o.TransactionId == serviceResponse.TransactionId);
        }

        /// <summary>
        /// Used to determine if an order should be updated when processing the Service Responses we obtain.
        /// </summary>
        /// <returns>True if the order should be updated. False otherwise.</returns>
        protected virtual bool ShouldUpdateOrder()
        {
            return this.RequestData.RequestType == VOXRequestT.Get;
        }

        /// <summary>
        /// Creates an instance of the specified order in the database and in memory.
        /// </summary>
        /// <param name="orderRequest">The request of the order metadata that was sent.</param>
        /// <param name="serviceResponse">The response to the order.</param>
        /// <returns>An order object.</returns>
        protected abstract TVOXOrder CreateOrder(VOXRequestOrder orderRequest, VOXServiceResponse serviceResponse);

        /// <summary>
        /// Creates an instance of the MCL request provider for this instance.
        /// </summary>
        /// <returns>The MCL request provider for this instance.</returns>
        protected abstract AbstractMismoVOXRequestProvider CreateMismoRequestProvider();

        /// <summary>
        /// Creates an Equifax OFX request provider for this VOX service handler.
        /// </summary>
        /// <returns>The OFX request provider.</returns>
        protected abstract IVOXRequestProvider CreateEquifaxOfxRequestProvider();

        /// <summary>
        /// Splits the request data into the orders that will be sent to the vendor.
        /// </summary>
        /// <returns>The orders that will be sent to the vendor.</returns>
        /// <remarks>
        /// As this involves generating unique values, it should be called once and saved per export.
        /// </remarks>
        protected abstract IReadOnlyCollection<VOXRequestOrder> CreateServiceOrdersFromRequestData();

        /// <summary>
        /// Generates a response provider from the vendor's response string.
        /// </summary>
        /// <param name="responseData">The response data from the vendor.</param>
        /// <returns>The response provider of the request.</returns>
        protected abstract AbstractVOXResponseProvider GenerateResponseProvider(VOXServerResponseData responseData);

        /// <summary>
        /// Saves received documents to EDocs.
        /// </summary>
        /// <param name="orderRequest">The order request metadata that was sent to the vendor.</param>
        /// <param name="serviceResponse">The service response received from the vendor.</param>
        protected virtual void SaveDocumentsToEDocs(VOXRequestOrder orderRequest, VOXServiceResponse serviceResponse)
        {
            List<Guid> docIds = new List<Guid>(serviceResponse.Documents.Count);
            foreach (var document in serviceResponse.Documents)
            {
                Guid? docId = AutoSaveDocTypeFactory.SavePdfDocAsSystem(
                    this.AutoSaveDocType,
                    document,
                    this.RequestData.BrokerId,
                    this.RequestData.LoanId,
                    orderRequest.ApplicationId,
                    string.Empty,
                    E_EnforceFolderPermissions.False);

                if (docId.HasValue)
                {
                    docIds.Add(docId.Value);
                }
            }

            serviceResponse.DocumentIds = docIds;
        }

        /// <summary>
        /// Records any necessary updates to the loan file.
        /// </summary>
        /// <param name="loan">The loan to apply the data to.</param>
        /// <param name="orderRequest">The order request to pull data from.</param>
        /// <param name="serviceResponse">The service response to pull data from.</param>
        private static void RecordDataToLoan(CPageData loan, VOXRequestOrder orderRequest, VOXServiceResponse serviceResponse)
        {
            if (!serviceResponse.ValidationReferenceNumbers.Any())
            {
                return;
            }

            if (loan.DataState == E_DataState.Normal)
            {
                loan.InitSave(Constants.ConstAppDavid.SkipVersionCheck);
            }

            CAppData application = loan.GetAppData(orderRequest.ApplicationId);
            var referenceNumbersToAdd = new List<DataAccess.FannieMae.FannieMaeThirdPartyProvider>(serviceResponse.ValidationReferenceNumbers.Count);
            foreach (DuValidationReferenceNumberVoxData referenceNumberHolder in serviceResponse.ValidationReferenceNumbers)
            {
                bool useCoborrowerSsn = referenceNumberHolder.UseCoborrowerSsn(application.aBFirstNm, application.aBLastNm, application.aCFirstNm, application.aCLastNm);
                application.BorrowerModeT = useCoborrowerSsn ? E_BorrowerModeT.Coborrower : E_BorrowerModeT.Borrower;
                string referenceNumber = referenceNumberHolder.ComputeReferenceNumber(application.aSsn);
                referenceNumbersToAdd.Add(new DataAccess.FannieMae.FannieMaeThirdPartyProvider(referenceNumberHolder.DuValidationProvider, referenceNumber));
            }

            loan.sDuThirdPartyProviders.AddProviders(referenceNumbersToAdd);
        }

        /// <summary>
        /// Initializes a request provider based on the request type.
        /// </summary>
        /// <returns>The request provider.</returns>
        private IVOXRequestProvider CreateRequestProvider()
        {
            var format = this.RequestData.PayloadFormatT;
            if (format == PayloadFormatT.MCLSmartApi || format == PayloadFormatT.LQBMismo)
            {
                return this.CreateMismoRequestProvider();
            }
            else if (format == PayloadFormatT.EquifaxVerificationServicesOfxXml)
            {
                return this.CreateEquifaxOfxRequestProvider();
            }

            throw new ArgumentException($"{format} is not a valid format for {RequestData.ServiceType} requests.");
        }
    }
}
