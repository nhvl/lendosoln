﻿namespace LendersOffice.Integration.VOXFramework
{
    using System.Collections.Generic;
    
    /// <summary>
    /// A light version of the vendor info.
    /// </summary>
    public class LightVOXVendor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LightVOXVendor"/> class.
        /// </summary>
        public LightVOXVendor()
        {
            this.Services = new Dictionary<VOXServiceT, LightVOXVendorService>();
        }

        /// <summary>
        /// Gets or sets the vendor id.
        /// </summary>
        /// <value>The vendor id.</value>
        public int VendorId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the company name.
        /// </summary>
        /// <value>The company name.</value>
        public string CompanyName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this vendor is enabled.
        /// </summary>
        /// <value>Whether this vendor is enabled.</value>
        public bool IsEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this vendor is a test vendor.
        /// </summary>
        /// <value>Whether this is a test vendor.</value>
        public bool IsTestVendor
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this vendor allows payment by credit card.
        /// </summary>
        /// <value>Whether this vendor allows payment by credit card.</value>
        public bool CanPayWithCreditCard
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the associated platform id.
        /// </summary>
        /// <value>The associated platform id.</value>
        public int? AssociatedPlatformId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this vendor has a platform that requires an account id.
        /// </summary>
        /// <value>Whether this vendor has a platform that requires an account id.</value>
        public bool RequiresAccountId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this vendor has a platform that uses an account id.
        /// </summary>
        /// <value>Whether this vendor has a platform that uses an account id.</value>
        public bool UsesAccountId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the services associated with this vendor.
        /// </summary>
        /// <value>The services associated with this vendor.</value>
        public Dictionary<VOXServiceT, LightVOXVendorService> Services
        {
            get;
            set;
        }
    }
}
