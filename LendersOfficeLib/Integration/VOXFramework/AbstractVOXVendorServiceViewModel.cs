﻿namespace LendersOffice.Integration.VOXFramework
{
    /// <summary>
    /// Interface for the viewmodels for the services.
    /// </summary>
    public abstract class AbstractVOXVendorServiceViewModel
    {
        /// <summary>
        /// Gets or sets the vendor service id.
        /// </summary>
        /// <value>The vendor service id.</value>
        public int? VendorServiceId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the service type.
        /// </summary>
        /// <value>The service type.</value>
        public abstract VOXServiceT ServiceType
        {
            get;
        }

        /// <summary>
        /// Gets or sets the vendor info id.
        /// </summary>
        /// <value>The vendor info id.</value>
        public int? VendorInfoId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this service is enabled.
        /// </summary>
        /// <value>Whether this service is enabled.</value>
        public bool IsEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is a direct vendor.
        /// </summary>
        /// <value>Whether this is a direct vendor.</value>
        public bool IsADirectVendor
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this sells via resellers.
        /// </summary>
        /// <value>Whether this vendor sells via resellers.</value>
        public bool SellsViaResellers
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is a reseller.
        /// </summary>
        /// <value>Whether this is a reseller.</value>
        public bool IsAReseller
        {
            get;
            set;
        }
    }
}
