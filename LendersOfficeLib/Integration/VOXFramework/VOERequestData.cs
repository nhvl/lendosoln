﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LqbGrammar.DataTypes;
    using Security;

    /// <summary>
    /// VOE requet data.
    /// </summary>
    public class VOERequestData : AbstractVOXRequestData
    {
        /// <summary>
        /// The employments on the order.
        /// </summary>
        private readonly List<VOEOrderEmploymentRecord> employmentsExported;

        /// <summary>
        /// The borrower data that needs to be recorded and was exported in the request.
        /// </summary>
        private readonly Dictionary<Tuple<Guid, bool>, VOEExportedBorrowerData> borrowerDataExported = new Dictionary<Tuple<Guid, bool>, VOEExportedBorrowerData>();

        /// <summary>
        /// The employment record id/service transaction id relationships exported in the request.
        /// </summary>
        private Dictionary<Guid, Guid> serviceTransactionIdEmploymentRecordIdRelationshipsExported = new Dictionary<Guid, Guid>();

        /// <summary>
        /// Initializes a new instance of the <see cref="VOERequestData"/> class.
        /// </summary>
        /// <param name="principal">The user placing the request.</param>
        /// <param name="lenderServiceId">The chosen lender service id.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="requestType">The request type.</param>
        public VOERequestData(AbstractUserPrincipal principal, int lenderServiceId, Guid loanId, VOXRequestT requestType)
            : base(principal, lenderServiceId, loanId, requestType)
        {
            this.employmentsExported = new List<VOEOrderEmploymentRecord>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VOERequestData"/> class.
        /// </summary>
        /// <param name="principal">The user placing the request.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="requestType">The request type.</param>
        /// <param name="order">The previous order.</param>
        public VOERequestData(AbstractUserPrincipal principal, Guid loanId, VOXRequestT requestType, VOEOrder order)
            : base(principal, loanId, requestType, order)
        {
            this.employmentsExported = new List<VOEOrderEmploymentRecord>(order.EmploymentRecords);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VOERequestData"/> class by
        /// copying an existing instance.
        /// </summary>
        /// <param name="requestData">The instance to copy.</param>
        /// <param name="overrideRequestType">Can override the request type.</param>
        /// <param name="isSystemGenerated">Can override the system generated indicator.</param>
        public VOERequestData(VOERequestData requestData, VOXRequestT? overrideRequestType = null, bool? isSystemGenerated = null)
            : base(requestData, overrideRequestType, isSystemGenerated)
        {
            this.borrowerDataExported = requestData.borrowerDataExported;
            this.employmentsExported = requestData.employmentsExported;
            this.serviceTransactionIdEmploymentRecordIdRelationshipsExported = requestData.serviceTransactionIdEmploymentRecordIdRelationshipsExported;
            this.ShouldVerifyIncome = requestData.ShouldVerifyIncome;
            this.EmploymentData = requestData.EmploymentData;
            this.SelfEmploymentData = requestData.SelfEmploymentData;
            this.SalaryKeyData = requestData.SalaryKeyData;
        }

        /// <summary>
        /// Gets the borrowers in this request.
        /// </summary>
        /// <value>The borrowers in this request.</value>
        public override IEnumerable<KeyValuePair<Guid, bool>> BorrowersInRequest
        {
            get
            {
                if (!this.VerifiesBorrower && this.EmploymentData != null)
                {
                    List<KeyValuePair<Guid, bool>> borrowers = new List<KeyValuePair<Guid, bool>>();
                    foreach (var employmentData in this.EmploymentData)
                    {
                        var borrowerId = new KeyValuePair<Guid, bool>(employmentData.AppId, employmentData.IsForCoborrower);
                        if (!borrowers.Contains(borrowerId))
                        {
                            borrowers.Add(borrowerId);
                        }
                    }

                    return borrowers;
                }
                else if (this.VerifiesBorrower && this.BorrowerAppId.HasValue && this.BorrowerType.HasValue)
                {
                    return new List<KeyValuePair<Guid, bool>>()
                    {
                        new KeyValuePair<Guid, bool>(this.BorrowerAppId.Value, this.BorrowerType.Value == DataAccess.E_BorrowerModeT.Coborrower)
                    };
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the service type.
        /// </summary>
        /// <value>This is a VOE service.</value>
        public override VOXServiceT ServiceType
        {
            get
            {
                return VOXServiceT.VOE;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this request should also verify income.
        /// </summary>
        /// <value>Can only be true if the chosen lender service has VOEVerificationT == EmploymentIncome.</value>
        public bool ShouldVerifyIncome { get; set; }

        /// <summary>
        /// Gets a value indicating whether this request intends to use a specific employer record search.
        /// </summary>
        public bool UseSpecificEmployerRecordSearch
        {
            get { return this.LenderService.VoeEnforceUseSpecificEmployerRecordSearch ? this.LenderService.VoeUseSpecificEmployerRecordSearch : this.UserRequestedUseSpecificEmployerRecordSearch; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user intend this request to use a specific employer record search.
        /// </summary>
        public bool UserRequestedUseSpecificEmployerRecordSearch { get; set; }

        /// <summary>
        /// Gets or sets the employment data.
        /// </summary>
        /// <value>The employment data.</value>
        public IEnumerable<VOEEmploymentData> EmploymentData { get; set; }

        /// <summary>
        /// Gets or sets the self employment data.
        /// </summary>
        /// <value>The self employment data.</value>
        public IEnumerable<VOESelfEmploymentData> SelfEmploymentData { get; set; }

        /// <summary>
        /// Gets or sets the salary key data.
        /// </summary>
        /// <value>The salary key data.</value>
        public IEnumerable<VOESalaryKey> SalaryKeyData { get; set; }

        /// <summary>
        /// Gets the dictionary of borrower data exported in the request.
        /// </summary>
        /// <value>The dictionary of borrower data exported in the request.</value>
        public IReadOnlyDictionary<Tuple<Guid, bool>, VOEExportedBorrowerData> BorrowerDataExported
        {
            get { return this.borrowerDataExported; }
        }

        /// <summary>
        /// Gets the employment records exported to the vendor in the current request.  May not contain any values until the exporter has processed the request.
        /// </summary>
        /// <value>The employment records exported to the vendor in the current request.</value>
        public IEnumerable<VOEOrderEmploymentRecord> EmploymentsExported
        {
            get { return this.employmentsExported; }
        }

        /// <summary>
        /// Gets the employment record id/service transaction id relationships exported in the request.
        /// </summary>
        public IReadOnlyDictionary<Guid, Guid> ServiceTransactionIdEmploymentRecordIdRelationshipsExported => this.serviceTransactionIdEmploymentRecordIdRelationshipsExported;

        /// <summary>
        /// Gets or sets an order number when there's no previous order to reference. Currently this only
        /// happens for VOE Initial requests.
        /// </summary>
        /// <value>An MCL order number.</value>
        public string GeneratedOrderNumber { get; set; }

        /// <summary>
        /// Validates this VOE request data.
        /// </summary>
        /// <param name="errors">Validates the VOE request data.</param>
        /// <returns>True if validated. False otherwise.</returns>
        public override bool ValidateRequestData(out string errors)
        {
            if (!base.ValidateRequestData(out errors))
            {
                return false;
            }

            AbstractVOXVendorService voeService;
            if (!this.LenderService.VendorForVendorAndServiceData.Services.TryGetValue(VOXServiceT.VOE, out voeService) || !voeService.IsEnabled)
            {
                errors = "VOE is not enabled for the chosen service provider.";
                return false;
            }

            if (!this.VerifiesBorrower && this.EmploymentData != null && !this.EmploymentData.Any())
            {
                errors = "No Employment Data selected.";
                return false;
            }

            StringBuilder errorLog = new StringBuilder();
            if (this.EmploymentData != null)
            {
                foreach (var data in this.EmploymentData)
                {
                    if (data.AppId == Guid.Empty || data.EmploymentRecordId == Guid.Empty)
                    {
                        errorLog.AppendLine("Employment Data not set");
                        break;
                    }
                }
            }

            if (this.SalaryKeyData != null)
            {
                foreach (var key in this.SalaryKeyData)
                {
                    if (!string.IsNullOrEmpty(key.SalaryKey) && key.AppId == Guid.Empty)
                    {
                        errorLog.AppendLine($"Invalid salary key data for {key.SalaryKey}");
                    }
                }
            }

            if (this.ShouldVerifyIncome && ((VOEVendorService)voeService).VerificationType != VOEVerificationT.EmploymentIncome)
            {
                errorLog.AppendLine("This service provider is not configured to verify Income.");
                return false;
            }

            if (string.IsNullOrEmpty(this.NotificationEmail) || !EmailAddress.Create(this.NotificationEmail).HasValue)
            {
                errorLog.AppendLine("A valid email address is required.");
            }

            if (this.SelfEmploymentData != null)
            {
                foreach (var data in this.SelfEmploymentData)
                {
                    if (data.AppId == Guid.Empty)
                    {
                        errorLog.AppendLine("Self Employment data not set.");
                    }

                    if (!data.TaxesSelfPrepared)
                    {
                        if (string.IsNullOrEmpty(data.CompanyName))
                        {
                            errorLog.AppendLine("Company not specified.");
                        }

                        if (string.IsNullOrEmpty(data.TaxStreetAddress))
                        {
                            errorLog.AppendLine("Street Address not specified.");
                        }

                        var state = Tools.SafeStateCode(data.TaxState);
                        if (string.IsNullOrEmpty(state))
                        {
                            if (string.IsNullOrEmpty(data.TaxState))
                            {
                                errorLog.AppendLine("State not specified");
                            }
                            else
                            {
                                errorLog.AppendLine($"Invalid state {data.TaxState}.");
                            }
                        }

                        data.TaxState = state;

                        if (Zipcode.CreateWithValidation(data.TaxZipcode) == null)
                        {
                            if (string.IsNullOrEmpty(data.TaxZipcode))
                            {
                                errorLog.AppendLine("Zipcode not specified.");
                            }
                            else
                            {
                                errorLog.AppendLine($"Invalid zipcode {data.TaxZipcode}");
                            }
                        }

                        if (PhoneNumber.CreateWithValidation(data.TaxPhoneNumber) == null)
                        {
                            if (string.IsNullOrEmpty(data.TaxPhoneNumber))
                            {
                                errorLog.AppendLine("Phone number not specified.");
                            }
                            else
                            {
                                errorLog.AppendLine($"Invalid phone number {data.TaxPhoneNumber}");
                            }
                        }

                        if (!string.IsNullOrEmpty(data.TaxFaxNumber))
                        {
                            if (PhoneNumber.CreateWithValidation(data.TaxFaxNumber) == null)
                            {
                                errorLog.AppendLine($"Invalid fax number {data.TaxFaxNumber}");
                            }
                        }
                    }
                }
            }

            errors = errorLog.ToString();
            return errorLog.Length == 0;
        }

        /// <summary>
        /// Records a borrower from the exporter into the request's set of borrowers.
        /// </summary>
        /// <param name="applicationId">The application id of the borrower.</param>
        /// <param name="isCoborrower">A value indicating whether this borrower is the co-borrower on the application.</param>
        /// <param name="name">The borrower name to record.</param>
        /// <param name="last4Ssn">The last 4 ssn digits of the borrower.</param>
        public void RecordBorrowerExport(Guid applicationId, bool isCoborrower, string name, string last4Ssn)
        {
            this.borrowerDataExported.Add(Tuple.Create(applicationId, isCoborrower), new VOEExportedBorrowerData(name, last4Ssn));
        }

        /// <summary>
        /// Records an employment record from the exporter into the request's list of employments.
        /// </summary>
        /// <param name="employmentRecord">The employment record to record.</param>
        public void RecordEmploymentExport(VOEOrderEmploymentRecord employmentRecord)
        {
            this.employmentsExported.Add(employmentRecord);
        }

        /// <summary>
        /// Records a transaction id and employment record id relationship exported in the request.
        /// </summary>
        /// <param name="serviceTransactionId">The transaction id sent to the service.</param>
        /// <param name="recordId">The employment record id.</param>
        public void RecordServiceTransactionIdEmploymentRecordIdRelationshipExport(Guid serviceTransactionId, Guid recordId)
        {
            this.serviceTransactionIdEmploymentRecordIdRelationshipsExported.Add(serviceTransactionId, recordId);
        }
    }
}
