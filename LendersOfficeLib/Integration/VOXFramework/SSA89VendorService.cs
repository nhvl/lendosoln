﻿namespace LendersOffice.Integration.VOXFramework
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;

    /// <summary>
    /// SSA-89 service at the vendor level.
    /// </summary>
    public class SSA89VendorService : AbstractVOXVendorService
    {
        /// <summary>
        /// Gets the service type.
        /// </summary>
        /// <value>The service type.</value>
        public override VOXServiceT ServiceType => VOXServiceT.SSA_89;

        /// <summary>
        /// Creates a new service from the model. This assumes that this instance is 100% new.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if made, false otherwise.</returns>
        internal override bool CreateFromViewModel(AbstractVOXVendorServiceViewModel viewModel, out string errors)
        {
            if (viewModel == null)
            {
                errors = "Null model";
                return false;
            }

            SSA89VendorServiceViewModel trueModel = viewModel as SSA89VendorServiceViewModel;
            if (trueModel == null)
            {
                errors = "Invalid model passed in.";
                return false;
            }

            this.VendorServiceId = -1;
            this.VendorInfoId = -1;
            this.IsEnabled = trueModel.IsEnabled;
            this.IsADirectVendor = trueModel.IsADirectVendor;
            this.SellsViaResellers = trueModel.SellsViaResellers;
            this.IsAReseller = trueModel.IsAReseller;

            errors = string.Empty;
            return true;
        }

        /// <summary>
        /// Populates this SSA-89 service from the reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        internal override void PopulateFromReader(IDataReader reader)
        {
            this.VendorServiceId = (int)reader["VendorServiceId"];
            this.VendorInfoId = (int)reader["VendorId"];
            this.IsEnabled = (bool)reader["IsEnabled"];
            this.IsADirectVendor = (bool)reader["IsADirectVendor"];
            this.SellsViaResellers = (bool)reader["SellsViaResellers"];
            this.IsAReseller = (bool)reader["IsAReseller"];
        }

        /// <summary>
        /// Populates the service from the view model. For editting.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if populated. False otherwise.</returns>
        internal override bool PopulateFromViewModel(VOXVendorViewModel viewModel, out string errors)
        {
            if (viewModel == null)
            {
                errors = "Null model passed in.";
                return false;
            }

            SSA89VendorServiceViewModel trueModel = viewModel.SSA89Service;

            this.IsEnabled = trueModel.IsEnabled;
            this.IsADirectVendor = trueModel.IsADirectVendor;
            this.SellsViaResellers = trueModel.SellsViaResellers;
            this.IsAReseller = trueModel.IsAReseller;

            errors = string.Empty;
            return true;
        }

        /// <summary>
        /// Saves this service to the DB.
        /// </summary>
        /// <param name="vendorId">The vendor id.</param>
        /// <param name="exec">The transaction.</param>
        internal override void SaveToDb(int vendorId, DataAccess.CStoredProcedureExec exec)
        {
            this.VendorInfoId = vendorId;

            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@VendorId", this.VendorInfoId),
                new SqlParameter("@IsEnabled", this.IsEnabled),
                new SqlParameter("@IsADirectVendor", this.IsADirectVendor),
                new SqlParameter("@SellsViaResellers ", this.SellsViaResellers),
                new SqlParameter("@IsAReseller", this.IsAReseller),
            };

            SqlParameter vendorServiceIdOutput = null;
            string sp = null;
            var vendorServiceIsNew = this.VendorServiceId < 0;

            if (vendorServiceIsNew)
            {
                vendorServiceIdOutput = new SqlParameter("@VendorServiceId", SqlDbType.Int);
                vendorServiceIdOutput.Direction = ParameterDirection.Output;
                parameters.Add(vendorServiceIdOutput);

                sp = "SSA89_SERVICE_Create";
            }
            else
            {
                parameters.Add(new SqlParameter("@VendorServiceId", this.VendorServiceId));

                sp = "SSA89_SERVICE_Update";
            }

            exec.ExecuteNonQuery(sp, 2, parameters.ToArray());

            if (vendorServiceIsNew)
            {
                this.VendorServiceId = (int)vendorServiceIdOutput.Value;
            }
        }

        /// <summary>
        /// Creates a view model out of this service.
        /// </summary>
        /// <returns>The view model.</returns>
        internal override AbstractVOXVendorServiceViewModel ToViewModel()
        {
            SSA89VendorServiceViewModel viewModel = new SSA89VendorServiceViewModel();
            viewModel.VendorServiceId = this.VendorServiceId;
            viewModel.VendorInfoId = this.VendorInfoId;
            viewModel.IsEnabled = this.IsEnabled;
            viewModel.IsADirectVendor = this.IsADirectVendor;
            viewModel.SellsViaResellers = this.SellsViaResellers;
            viewModel.IsAReseller = this.IsAReseller;

            return viewModel;
        }
    }
}
