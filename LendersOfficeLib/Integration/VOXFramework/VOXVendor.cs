﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Text;
    using DataAccess;
    using LendersOffice.Common;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// The vendor. The company that provides services.
    /// Most vendors require a parent platform. These vendors can override that platform's TransmissionInfo.
    /// These vendors also hold the services that they provide.
    /// </summary>
    public class VOXVendor
    {
        /// <summary>
        /// Stored procedure to get basic info for vendors.
        /// </summary>
        private const string ListAllVendorsLight = "VERIFICATION_VENDOR_ListAllVendorsLight";

        /// <summary>
        /// Stored procedure to pull full vendor info.
        /// </summary>
        private const string GetVendorWithVendorId = "VERIFICATION_VENDOR_GetVendorWithVendorId";

        /// <summary>
        /// Stored procedure to get the number of lender services using a vendor.
        /// </summary>
        private const string GetLenderServiceCountWithVendorId = "VERIFICATION_LENDER_SERVICE_GetLenderServiceCountWithVendorId";

        /// <summary>
        /// Stored procedure to delete a vendor and its associated services.
        /// </summary>
        private const string DeleteVendorWithVendorId = "VERIFICATION_VENDOR_DeleteVendorWithVendorId";

        /// <summary>
        /// Stored procedure to create a vendor.
        /// </summary>
        private const string CreateVendor = "VERIFICATION_VENDOR_CreateVendor";

        /// <summary>
        /// Stored procedure to update a vendor.
        /// </summary>
        private const string UpdateVendor = "VERIFICATION_VENDOR_UpdateVendor";

        /// <summary>
        /// Stored procedure to get a light instance of the vendors with services.
        /// </summary>
        private const string ListAllVendorsWithServicesLight = "VERIFICATION_VENDOR_ListAllVendorsWithServicesLight";

        /// <summary>
        /// The transmission info if the vendor is overriding the platform.
        /// </summary>
        private VOXTransmissionInfo overridingTransmission = null;

        /// <summary>
        /// The platform associated with this vendor.
        /// </summary>
        private Lazy<VOXPlatform> associatedPlatform = null;

        /// <summary>
        /// The associated platform id.
        /// </summary>
        private int? associatedPlatformId;

        /// <summary>
        /// If the platform is being overridden.
        /// </summary>
        private bool isOverridingPlatform;

        /// <summary>
        /// Prevents a default instance of the <see cref="VOXVendor"/> class from being created.
        /// </summary>
        private VOXVendor()
        {
            this.Services = new Dictionary<VOXServiceT, AbstractVOXVendorService>();
        }

        /// <summary>
        /// Gets a value indicating whether this vendor is new.
        /// </summary>
        /// <value>Indicating whether this vendor is new.</value>
        public bool IsNew
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the id of the vendor level service.
        /// </summary>
        /// <value>The id of the vendor level service.</value>
        public int VendorId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the vendor name.
        /// </summary>
        /// <value>The vendor name.</value>
        public string CompanyName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the vendor is enabled.
        /// </summary>
        /// <value>If the vendor is enabled.</value>
        public bool IsEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the vendor is a test vendor.
        /// </summary>
        /// <value>Whether the vendor is a test vendor.</value>
        public bool IsTestVendor
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether payment with credit card is accepted.
        /// </summary>
        /// <value>If payment with credit card is accepted.</value>
        public bool CanPayWithCreditCard
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the platform associated with this vendor.
        /// </summary>
        /// <value>The platform associated with this vendor.</value>
        public int? AssociatedPlatformId
        {
            get
            {
                if (!(this.IsDirectSeller || this.IsAReseller))
                {
                    return null;
                }

                return this.associatedPlatformId;
            }

            set
            {
                this.associatedPlatformId = value.HasValue && value.Value < 0 ? null : value;
                if (value.HasValue && value.Value > 0)
                {
                    this.associatedPlatform = new Lazy<VOXPlatform>(() => VOXPlatform.LoadPlatform(value.Value));
                }
                else
                {
                    this.associatedPlatform = new Lazy<VOXPlatform>(() => null);
                }
            }
        }

        /// <summary>
        /// Gets the associated platform for this vendor.
        /// </summary>
        /// <value>The associated platform for this vendor.</value>
        public VOXPlatform AssociatedPlatform
        {
            get
            {
                return this.associatedPlatform.Value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the platform's transmission details are overriden.
        /// </summary>
        /// <value>If the transmission details are overriden.</value>
        public bool IsOverridingPlatform
        {
            get
            {
                if (!(this.IsDirectSeller || this.IsAReseller))
                {
                    return false;
                }

                return this.isOverridingPlatform;
            }

            set
            {
                this.isOverridingPlatform = value;
            }
        }

        /// <summary>
        /// Gets the id of the transmission info that is linked to this vendor.
        /// </summary>
        /// <value>Id of the transmission info linked to this vendor.</value>
        public int LinkedTransmissionId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the transmission info to use for this vendor.
        /// </summary>
        /// <value>The transmission info this vendor is using.</value>
        /// <remarks>
        /// Note that this transmission info could be pulling from the platform,
        /// so its id may not match the overriding transmission id for this vendor.
        /// </remarks>
        public VOXTransmissionInfo TransmissionInfo
        {
            get
            {
                if (!(this.IsDirectSeller || this.IsAReseller))
                {
                    return VOXTransmissionInfo.GetEmptyTransmissionInfo();
                }
                else if (this.IsOverridingPlatform)
                {
                    return this.overridingTransmission;
                }
                else
                {
                    return this.associatedPlatform.Value.TransmissionInfo;
                }
            }
        }

        /// <summary>
        /// Gets or sets the id of the DU validation service provider.
        /// </summary>
        /// <value>The id of the DUE validation service provider.</value>
        public int? DuValidationServiceProviderId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the MCL CRA Id.
        /// </summary>
        /// <value>The MCL CRA Id.</value>
        public string MclCraId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the services that this vendor provides.
        /// </summary>
        /// <value>The services that this vendor provides.</value>
        public Dictionary<VOXServiceT, AbstractVOXVendorService> Services
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether this vendor is a direct seller for any service.
        /// </summary>
        /// <value>Whether the vendor is a direct seller for any service.</value>
        public bool IsDirectSeller
        {
            get
            {
                foreach (var service in this.Services)
                {
                    if (service.Value.IsADirectVendor)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this vendor is a seller via reseller for any service.
        /// </summary>
        /// <value>Whether this vendor is a seller via reseller for any service.</value>
        public bool IsSellerViaReseller
        {
            get
            {
                foreach (var service in this.Services)
                {
                    if (service.Value.SellsViaResellers)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this vendor is a reseller for any service.
        /// </summary>
        /// <value>Whether this vendor is a reseller for any service.</value>
        public bool IsAReseller
        {
            get
            {
                foreach (var service in this.Services)
                {
                    if (service.Value.IsAReseller)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// Gets basic info for all verification vendors.
        /// </summary>
        /// <returns>A list of basic info for all verification vendors.</returns>
        public static IEnumerable<LightVOXVendor> LoadLightVendors()
        {
            StoredProcedureName procedureName = StoredProcedureName.Create(ListAllVendorsLight).ForceValue();
            List<LightVOXVendor> list = new List<LightVOXVendor>();

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);
            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShareROnly))
            using (var reader = driver.ExecuteReader(connection, null, procedureName, null))
            {
                while (reader.Read())
                {
                    list.Add(LoadLightVendorFromReader(reader));
                }
            }

            return list;
        }

        /// <summary>
        /// Gets a subset of vendor info along with a subset of their services' info.
        /// </summary>
        /// <returns>The list of light vendors.</returns>
        public static Dictionary<int, LightVOXVendor> LoadLightVendorsWithServices()
        {
            StoredProcedureName procedureName = StoredProcedureName.Create(ListAllVendorsWithServicesLight).ForceValue();
            Dictionary<int, LightVOXVendor> lightVendors = new Dictionary<int, LightVOXVendor>();
            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);
            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShareROnly))
            using (var reader = driver.ExecuteReader(connection, null, procedureName, null))
            {
                while (reader.Read())
                {
                    LightVOXVendor lightVendor = LoadLightVendorFromReader(reader);
                    lightVendors.Add(lightVendor.VendorId, lightVendor);
                }

                while (reader.NextResult())
                {
                    while (reader.Read())
                    {
                        LightVOXVendorService vendorService = LightVOXVendorService.CreateFromReader(reader);
                        LightVOXVendor vendor;
                        if (lightVendors.TryGetValue(vendorService.VendorInfoId, out vendor))
                        {
                            vendor.Services.Add(vendorService.ServiceType, vendorService);
                        }
                    }
                }
            }

            return lightVendors;
        }

        /// <summary>
        /// Loads a vendor with the given vendor id.
        /// </summary>
        /// <param name="vendorId">The id of the vendor to load.</param>
        /// <returns>The vendor if successfully created. Null otherwise.</returns>
        public static VOXVendor LoadVendor(int vendorId)
        {
            if (vendorId < 1)
            {
                return null;
            }

            StoredProcedureName procedureName = StoredProcedureName.Create(GetVendorWithVendorId).ForceValue();

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@VendorId", vendorId)
            };

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);
            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShareROnly))
            using (var reader = driver.ExecuteReader(connection, null, procedureName, parameters))
            {
                VOXVendor vendor = null;

                // The vendor stuff should be loaded first.
                if (reader.Read())
                {
                    vendor = LoadVendorInfoFromReader(reader);
                }

                if (vendor == null)
                {
                    return null;
                }

                Dictionary<VOXServiceT, AbstractVOXVendorService> services = new Dictionary<VOXServiceT, AbstractVOXVendorService>();

                // Below here is the services
                while (reader.NextResult())
                {
                    if (reader.Read())
                    {
                        var service = VOXVendorServiceHandler.LoadServiceFromReader(reader);
                        services.Add(service.ServiceType, service);
                    }
                }

                vendor.Services = services;
                return vendor;
            }
        }

        /// <summary>
        /// Deletes a vendor with a vendor id.
        /// </summary>
        /// <param name="vendorId">The id of the vendor to delete.</param>
        /// <param name="errorMessage">Any error messages encountered.</param>
        /// <returns>True if deleted. False otherwise.</returns>
        public static bool DeleteVendor(int vendorId, out string errorMessage)
        {
            if (vendorId < 1)
            {
                errorMessage = $"Invalid vendor Id: {vendorId}";
                return false;
            }

            StoredProcedureName countServicesSp = StoredProcedureName.Create(GetLenderServiceCountWithVendorId).ForceValue();

            SqlParameter[] countServicesSpParams = new SqlParameter[]
            {
                new SqlParameter("@VendorId", vendorId)
            };

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);

            int serviceCount = 0;
            foreach (var connection in DbConnectionInfo.ListAll())
            {
                using (var sqlConnection = connection.GetReadOnlyConnection())
                using (var reader = driver.ExecuteReader(sqlConnection, null, countServicesSp, countServicesSpParams))
                {
                    if (reader.Read())
                    {
                        serviceCount += (int)reader["ServiceCount"];
                    }
                }
            }

            if (serviceCount > 0)
            {
                errorMessage = "Unable to delete vendor. Lender services are using this vendor.";
                return false;
            }

            StoredProcedureName deleteSp = StoredProcedureName.Create(DeleteVendorWithVendorId).ForceValue();
            SqlParameter[] deleteParams = new SqlParameter[]
            {
                new SqlParameter("@VendorId", vendorId)
            };

            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShareROnly))
            {
                driver.ExecuteNonQuery(connection, null, deleteSp, deleteParams);
            }

            errorMessage = string.Empty;
            return true;
        }

        /// <summary>
        /// Creates a vendor object from the model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>The vendor if successful. Null otherwise.</returns>
        public static VOXVendor CreateFromViewModel(VOXVendorViewModel viewModel, out string errors)
        {
            bool vendorIsNew = !viewModel.VendorId.HasValue || viewModel.VendorId.Value < 1;

            if (!VerifyViewModel(viewModel, out errors))
            {
                return null;
            }

            VOXVendor loadedVendor;
            if (!vendorIsNew)
            {
                loadedVendor = VOXVendor.LoadVendor(viewModel.VendorId.Value);
                foreach (var serviceModel in viewModel.GetAllServices())
                {
                    if (loadedVendor.Services.ContainsKey(serviceModel.ServiceType))
                    {
                        var service = loadedVendor.Services[serviceModel.ServiceType];
                        if (!service.PopulateFromViewModel(viewModel, out errors))
                        {
                            return null;
                        }
                    }
                    else
                    {
                        AbstractVOXVendorService service = VOXVendorServiceHandler.CreateServiceFromViewModel(serviceModel, out errors);
                        if (service == null)
                        {
                            return null;
                        }

                        loadedVendor.Services.Add(service.ServiceType, service);
                    }
                }
            }
            else
            {
                loadedVendor = new VOXVendor();

                foreach (var serviceViewModel in viewModel.GetAllServices())
                {
                    AbstractVOXVendorService service = VOXVendorServiceHandler.CreateServiceFromViewModel(serviceViewModel, out errors);
                    if (service == null)
                    {
                        return null;
                    }

                    loadedVendor.Services.Add(service.ServiceType, service);
                }

                loadedVendor.VendorId = -1;
                loadedVendor.LinkedTransmissionId = -1;
            }

            bool isDifferentPlatform = loadedVendor.AssociatedPlatformId != viewModel.AssociatedPlatformId;
            loadedVendor.AssociatedPlatformId = viewModel.AssociatedPlatformId;
            loadedVendor.IsOverridingPlatform = viewModel.IsOverridingPlatform;
            if (loadedVendor.IsOverridingPlatform)
            {
                if (isDifferentPlatform || loadedVendor.overridingTransmission == null)
                {
                    loadedVendor.overridingTransmission = loadedVendor.AssociatedPlatform.TransmissionInfo.CreateVendorCopy();
                }

                if (!loadedVendor.overridingTransmission.PopulateFromViewModel(viewModel.TransmissionInfo, out errors))
                {
                    return null;
                }
            }

            loadedVendor.CompanyName = viewModel.CompanyName;
            loadedVendor.IsEnabled = viewModel.IsEnabled;
            loadedVendor.IsTestVendor = viewModel.IsTestVendor;
            loadedVendor.CanPayWithCreditCard = viewModel.CanPayWithCreditCard;
            loadedVendor.DuValidationServiceProviderId = viewModel.DuValidationServiceProviderId;
            loadedVendor.MclCraId = viewModel.MclCraId;
            loadedVendor.IsNew = vendorIsNew;

            return loadedVendor;
        }

        /// <summary>
        /// Saves this vendor info and its associated services and transmission info.
        /// </summary>
        public void Save()
        {
            using (var exec = new CStoredProcedureExec(DataSrc.LOShare))
            {
                try
                {
                    exec.BeginTransactionForWrite();
                    this.Save(exec);
                    foreach (var service in this.Services)
                    {
                        service.Value.SaveToDb(this.VendorId, exec);
                    }

                    exec.CommitTransaction();
                }
                catch (SystemException)
                {
                    exec.RollbackTransaction();
                    throw;
                }
            }

            foreach (var service in this.Services)
            {
                service.Value.RunPostSaveActions();
            }

            this.IsNew = false;
        }

        /// <summary>
        /// Creates a view model out of this vendor.
        /// </summary>
        /// <returns>The view model.</returns>
        public VOXVendorViewModel ToViewModel()
        {
            VOXVendorViewModel viewModel = new VOXVendorViewModel();
            viewModel.VendorId = this.VendorId;
            viewModel.CompanyName = this.CompanyName;
            viewModel.IsEnabled = this.IsEnabled;
            viewModel.IsTestVendor = this.IsTestVendor;
            viewModel.CanPayWithCreditCard = this.CanPayWithCreditCard;
            viewModel.DuValidationServiceProviderId = this.DuValidationServiceProviderId;
            viewModel.AssociatedPlatformId = this.AssociatedPlatformId.HasValue ? this.AssociatedPlatformId.Value : 0;
            viewModel.IsOverridingPlatform = this.IsOverridingPlatform;
            viewModel.MclCraId = this.MclCraId;
            viewModel.TransmissionInfo = this.TransmissionInfo.ToViewModel();

            foreach (var service in this.Services)
            {
                switch (service.Key)
                {
                    case VOXServiceT.VOA_VOD:
                        viewModel.VOAService = (VOAVendorServiceViewModel)service.Value.ToViewModel();
                        break;
                    case VOXServiceT.VOE:
                        viewModel.VOEService = (VOEVendorServiceViewModel)service.Value.ToViewModel();
                        break;
                    case VOXServiceT.SSA_89:
                        viewModel.SSA89Service = (SSA89VendorServiceViewModel)service.Value.ToViewModel();
                        break;
                    default:
                        throw new UnhandledEnumException(service.Key);
                }
            }

            return viewModel;
        }

        /// <summary>
        /// Loads a complete VendorInfo object from the data reader.
        /// </summary>
        /// <param name="reader">The reader to load from.</param>
        /// <returns>The constructed vendor.</returns>
        private static VOXVendor LoadVendorInfoFromReader(IDataReader reader)
        {
            VOXVendor vendorInfo = new VOXVendor();
            vendorInfo.VendorId = (int)reader["VendorId"];
            vendorInfo.CompanyName = (string)reader["CompanyName"];
            vendorInfo.IsEnabled = (bool)reader["IsEnabled"];
            vendorInfo.IsTestVendor = (bool)reader["IsTestVendor"];
            vendorInfo.CanPayWithCreditCard = (bool)reader["CanPayWithCreditCard"];
            vendorInfo.IsOverridingPlatform = (bool)reader["IsOverridingPlatform"];
            vendorInfo.LinkedTransmissionId = (int)reader["OverridingTransmissionId"];
            vendorInfo.MclCraId = (string)reader["MclCraId"];

            if (reader["DuValidationServiceProviderId"] == DBNull.Value)
            {
                vendorInfo.DuValidationServiceProviderId = null;
            }
            else
            {
                vendorInfo.DuValidationServiceProviderId = (int)reader["DuValidationServiceProviderId"];
            }

            if (reader["PlatformId"] == DBNull.Value)
            {
                vendorInfo.AssociatedPlatformId = null;
            }
            else
            {
                vendorInfo.AssociatedPlatformId = (int)reader["PlatformId"];
            }

            VOXTransmissionInfo overridingTransmission = VOXTransmissionInfo.LoadTransmissionFromReader(reader);
            vendorInfo.overridingTransmission = overridingTransmission;

            return vendorInfo;
        }

        /// <summary>
        /// Loads light vendor info from a reader.
        /// </summary>
        /// <param name="reader">The reader to load from.</param>
        /// <returns>A light version of the vendor info.</returns>
        private static LightVOXVendor LoadLightVendorFromReader(IDataReader reader)
        {
            int? platformId;
            if (reader["PlatformId"] == DBNull.Value)
            {
                platformId = null;
            }
            else
            {
                platformId = (int)reader["PlatformId"];
            }

            var requiresAccountId = false;
            if (reader["RequiresAccountId"] != DBNull.Value)
            {
                requiresAccountId = (bool)reader["RequiresAccountId"];
            }

            var usesAccountId = false;
            if (reader["UsesAccountId"] != DBNull.Value)
            {
                usesAccountId = (bool)reader["UsesAccountId"];
            }

            LightVOXVendor lightVendor = new LightVOXVendor()
            {
                VendorId = (int)reader["VendorId"],
                CompanyName = (string)reader["CompanyName"],
                IsEnabled = (bool)reader["IsEnabled"],
                IsTestVendor = (bool)reader["IsTestVendor"],
                CanPayWithCreditCard = (bool)reader["CanPayWithCreditCard"],
                AssociatedPlatformId = platformId,
                RequiresAccountId = requiresAccountId,
                UsesAccountId = usesAccountId
            };

            return lightVendor;
        }

        /// <summary>
        /// Checks if the vendor view model is valid.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if valid. False otherwise.</returns>
        private static bool VerifyViewModel(VOXVendorViewModel viewModel, out string errors)
        {
            string invalidString = "Invalid field {0}: {1}";
            StringBuilder errorBuilder = new StringBuilder();
            if (string.IsNullOrEmpty(viewModel.CompanyName))
            {
                errorBuilder.AppendLine(string.Format(invalidString, "Company Name", viewModel.CompanyName));
            }

            bool canHaveNoPlatform = true;
            foreach (var service in viewModel.GetAllServices())
            {
                if (service.IsADirectVendor || service.IsAReseller)
                {
                    canHaveNoPlatform = false;
                    break;
                }
            }

            if (!canHaveNoPlatform && viewModel.AssociatedPlatformId < 1)
            {
                errorBuilder.AppendLine("Please select a platform. Only vendors that aren not Direct Vendors or Resellers can have no platform.");
            }

            errors = errorBuilder.ToString();
            return errorBuilder.Length == 0;
        }

        /// <summary>
        /// Saves the vendor info.
        /// </summary>
        /// <param name="exec">The transaction.</param>
        private void Save(CStoredProcedureExec exec)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@CompanyName", this.CompanyName),
                new SqlParameter("@IsEnabled", this.IsEnabled),
                new SqlParameter("@IsTestVendor", this.IsTestVendor),
                new SqlParameter("@CanPayWithCreditCard", this.CanPayWithCreditCard),
                new SqlParameter("@DuValidationServiceProviderId", this.DuValidationServiceProviderId),
                new SqlParameter("@PlatformId", this.AssociatedPlatformId),
                new SqlParameter("@IsOverridingPlatform", this.IsOverridingPlatform),
                new SqlParameter("@MclCraId", this.MclCraId)
            };

            parameters.AddRange(this.TransmissionInfo.ConstructSqlParameters(this.IsNew));

            SqlParameter vendorIdOutput = null;
            SqlParameter transmissionIdOutput = null;
            string sp = this.IsNew ? CreateVendor : UpdateVendor;

            if (!this.IsNew)
            {
                parameters.Add(new SqlParameter("@VendorId", this.VendorId));
                parameters.Add(new SqlParameter("@TransmissionId", this.LinkedTransmissionId));
            }
            else
            {
                vendorIdOutput = new SqlParameter("@VendorId", SqlDbType.Int);
                vendorIdOutput.Direction = ParameterDirection.Output;
                parameters.Add(vendorIdOutput);

                transmissionIdOutput = new SqlParameter("@TransmissionId", SqlDbType.Int);
                transmissionIdOutput.Direction = ParameterDirection.Output;
                parameters.Add(transmissionIdOutput);
            }

            exec.ExecuteNonQuery(sp, 2, parameters.ToArray());

            if (this.IsNew)
            {
                this.VendorId = (int)vendorIdOutput.Value;
                this.LinkedTransmissionId = (int)transmissionIdOutput.Value;
            }
        }
    }
}
