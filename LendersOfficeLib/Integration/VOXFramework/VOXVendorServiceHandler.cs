﻿namespace LendersOffice.Integration.VOXFramework
{
    using System.Data;
    using DataAccess;

    /// <summary>
    /// Utilities class for some vendor service functions.
    /// </summary>
    public class VOXVendorServiceHandler
    {
        /// <summary>
        /// Loads a service from the reader depending on the value of ServiceT.
        /// </summary>
        /// <param name="reader">The reader to load from.</param>
        /// <returns>The created service.</returns>
        public static AbstractVOXVendorService LoadServiceFromReader(IDataReader reader)
        {
            VOXServiceT serviceType = (VOXServiceT)reader["ServiceT"];
            AbstractVOXVendorService service = GetDefaultServiceInstance(serviceType);
            service.PopulateFromReader(reader);
            return service;
        }

        /// <summary>
        /// Creates a service from the view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>The service is successful. Null otherwise.</returns>
        public static AbstractVOXVendorService CreateServiceFromViewModel(AbstractVOXVendorServiceViewModel viewModel, out string errors)
        {
            AbstractVOXVendorService service = GetDefaultServiceInstance(viewModel.ServiceType);
            return service.CreateFromViewModel(viewModel, out errors) ? service : null;
        }

        /// <summary>
        /// Gets a default instance of the specified service type.
        /// </summary>
        /// <param name="serviceType">The type of service to make an instance of.</param>
        /// <returns>A default instance of the service.</returns>
        private static AbstractVOXVendorService GetDefaultServiceInstance(VOXServiceT serviceType)
        {
            switch (serviceType)
            {
                case VOXServiceT.VOA_VOD: return new VOAVendorService();
                case VOXServiceT.VOE: return new VOEVendorService();
                case VOXServiceT.SSA_89: return new SSA89VendorService();
                default:
                    throw new UnhandledEnumException(serviceType);
            }
        }
    }
}
