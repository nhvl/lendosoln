﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Data;
    using System.Data.SqlClient;

    /// <summary>
    /// Represents a configured VOA option.
    /// </summary>
    public class VOAOption
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOAOption"/> class.
        /// </summary>
        public VOAOption()
        {
            // no-op
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VOAOption"/> class.
        /// </summary>
        /// <param name="reader">A data reader.</param>
        public VOAOption(IDataReader reader)
        {
            this.VendorId = (int)reader["VendorId"];
            this.BrokerId = (Guid)reader["BrokerId"];
            this.LenderServiceId = (int)reader["LenderServiceId"];
            this.OptionType = (VOAOptionType)reader["EnabledOptionType"];
            this.OptionValue = (int)reader["EnabledOptionValue"];
            this.UserType = (string)reader["UserType"];
            this.IsDefault = (bool)reader["IsDefault"];
        }

        /// <summary>
        /// Gets or sets the vendor ID associated with the configured option.
        /// </summary>
        public int VendorId { get; set; }

        /// <summary>
        /// Gets or sets the broker ID associated with the configured option.
        /// </summary>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets the lender service ID associated with the configured option.
        /// </summary>
        public int LenderServiceId { get; set; }

        /// <summary>
        /// Gets or sets the configured option type.
        /// </summary>
        public VOAOptionType OptionType { get; set; }

        /// <summary>
        /// Gets or sets the configured option value.
        /// </summary>
        public int OptionValue { get; set; }

        /// <summary>
        /// Gets or sets the user type able to select this option.
        /// </summary>
        public string UserType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this is the default option that should be selected for VOA orders.
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// Gets a value indicating whether this is a valid option configuration.
        /// </summary>
        public bool IsValid
        {
            get
            {
                return this.VendorId >= 0
                    && this.BrokerId != Guid.Empty
                    && this.LenderServiceId >= 0
                    && this.OptionValue >= 0
                    && (this.UserType == "B" || this.UserType == "P");
            }
        }

        /// <summary>
        /// Gets a collection of <see cref="SqlParameters"/> representing this object.
        /// </summary>
        /// <returns>A collection of SQL parameters.</returns>
        public SqlParameter[] GetSqlParams()
        {
            return new SqlParameter[]
            {
                new SqlParameter("@VendorId", this.VendorId),
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@LenderServiceId", this.LenderServiceId),
                new SqlParameter("@EnabledOptionType", this.OptionType.ToString("D")),
                new SqlParameter("@EnabledOptionValue", this.OptionValue),
                new SqlParameter("@UserType", this.UserType),
                new SqlParameter("@IsDefault", this.IsDefault)
            };
        }
    }
}
