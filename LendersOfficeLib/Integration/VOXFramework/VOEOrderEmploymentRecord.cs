﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;

    /// <summary>
    /// Simple plain old data object for storing the snapshot of an employment record associated with an order.
    /// </summary>
    public class VOEOrderEmploymentRecord
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOEOrderEmploymentRecord"/> class.
        /// </summary>
        /// <param name="id">The id of the employment record on the loan.</param>
        /// <param name="employerName">The name of the employer at the time of the order.</param>
        /// <param name="isSelfEmployed">A value indicating whether the employment record was marked as self-employed at the time of the order.</param>
        public VOEOrderEmploymentRecord(Guid id, string employerName, bool isSelfEmployed)
        {
            this.Id = id;
            this.EmployerName = employerName;
            this.IsSelfEmployed = isSelfEmployed;
        }

        /// <summary>
        /// Gets the id of the asset on the loan.
        /// </summary>
        /// <value>The id of the asset on the loan.</value>
        public Guid Id { get; }

        /// <summary>
        /// Gets the name of the employer at the time of the order.
        /// </summary>
        /// <value>The name of the employer at the time of the order.</value>
        public string EmployerName { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the employment record was marked as self-employed at the time of the order.
        /// </summary>
        /// <value>A value indicating whether the employment record was marked as self-employed at the time of the order.</value>
        public bool IsSelfEmployed { get; private set; }
    }
}
