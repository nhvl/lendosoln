﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Data;
    using FrameworkCommon;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Holds data on how the data is transmitted and what is needed for that transmission.
    /// Initially defined at the Platform level but can be overridden at the Vendor level.
    /// </summary>
    public class VOXTransmissionInfo : IntegrationTransmissionInfo
    {
        /// <summary>
        /// An empty transmission info object.
        /// </summary>
        private static VOXTransmissionInfo emptyTransmissionInfo = null;
        
        /// <summary>
        /// Prevents a default instance of the <see cref="VOXTransmissionInfo"/> class from being created.
        /// </summary>
        private VOXTransmissionInfo()
        {
        }

        /// <summary>
        /// Gets the save SP.
        /// </summary>
        /// <value>Not currently implemented for VOX transmission info.</value>
        protected override string CreateSp
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Gets the update SP.
        /// </summary>
        /// <value>Not currently implemented for VOX transmission info.</value>
        protected override string UpdateSp
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Gets an empty transmission info.
        /// </summary>
        /// <returns>The empty transmission info.</returns>
        public static VOXTransmissionInfo GetEmptyTransmissionInfo()
        {
            if (emptyTransmissionInfo == null)
            {
                emptyTransmissionInfo = new VOXTransmissionInfo();
                emptyTransmissionInfo.RequestCredentialName = string.Empty;
                emptyTransmissionInfo.RequestCredentialPassword = string.Empty;
                emptyTransmissionInfo.ResponseCertificateId = Guid.Empty;
                emptyTransmissionInfo.ResponseCredentialName = string.Empty;
                emptyTransmissionInfo.ResponseCredentialPassword = string.Empty;
                emptyTransmissionInfo.TargetUrl = LqbAbsoluteUri.BadURI;
                emptyTransmissionInfo.TransmissionAssociationT = TransmissionAssociationT.Platform;
                emptyTransmissionInfo.TransmissionAuthenticationT = TransmissionAuthenticationT.IpAddressValidation;
            }

            return emptyTransmissionInfo;
        }

        /// <summary>
        /// Loads the transmission info from a reader.
        /// </summary>
        /// <param name="reader">The reader to load from.</param>
        /// <returns>The transmission info created from reader.</returns>
        internal static VOXTransmissionInfo LoadTransmissionFromReader(IDataReader reader)
        {
            var info = new VOXTransmissionInfo();
            info.LoadFromReader(reader);
            return info;
        }

        /// <summary>
        /// Creates a transmission info from the parameters.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="errors">Any errors during creation.</param>
        /// <returns>The created transmission info if successful. Null otherwise.</returns>
        internal static VOXTransmissionInfo CreatePlatformTransmissionFromViewModel(IntegrationTransmissionInfoViewModel viewModel, out string errors)
        {
            var info = new VOXTransmissionInfo();
            if (!info.PopulateNewPlatformTransmissionFromViewModel(viewModel, out errors))
            {
                return null;
            }

            return info;
        }

        /// <summary>
        /// Creates a copy of the current instance for the vendor instance to override.
        /// </summary>
        /// <returns>A copy of the current instance.</returns>
        internal VOXTransmissionInfo CreateVendorCopy()
        {
            var copy = new VOXTransmissionInfo();
            this.CopyTo(copy, TransmissionAssociationT.Vendor);
            return copy;
        }
    }
}
