﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;

    /// <summary>
    /// Data class holding info about the borrower authorization documents for a VOX service request.
    /// </summary>
    public class VOXBorrowerAuthDoc
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOXBorrowerAuthDoc"/> class.
        /// </summary>
        /// <param name="docId">The doc id.</param>
        /// <param name="appId">The app id.</param>
        /// <param name="isForCoborrower">Whether this doc is for the coborrower or not.</param>
        public VOXBorrowerAuthDoc(Guid docId, Guid appId, bool isForCoborrower)
        {
            this.DocId = docId;
            this.AppId = appId;
            this.IsForCoborrower = isForCoborrower;
        }

        /// <summary>
        /// Gets the document id.
        /// </summary>
        /// <value>The document id.</value>
        public Guid DocId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the application this doc belongs to.
        /// </summary>
        /// <value>The application this doc belongs to.</value>
        public Guid AppId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether this doc belongs to the coborrower on the chosen app.
        /// </summary>
        /// <value>Whether this doc belongs to the coborrower on the app.</value>
        public bool IsForCoborrower
        {
            get;
            private set;
        }
    }
}
