﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using Common;
    using DataAccess;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// A service that can be configured by admins in the General Settings page.
    /// Essentially made up of two different vendors (normal and reseller).
    /// </summary>
    public class VOXLenderService
    {
        /// <summary>
        /// A constant that indicates the special value for the reseller id if None/Direct Vendor is chosen.
        /// </summary>
        public const int NoneDirectVendorId = -1;

        /// <summary>
        /// Special value representing all groups are enabled.
        /// </summary>
        public static readonly Guid AllGroupsId = Guid.Empty;

        /// <summary>
        /// Stored procedure to get all lender services for a broker.
        /// </summary>
        private const string ListAllLenderServices = "VERIFICATION_LENDER_SERVICE_ListAll";

        /// <summary>
        /// Stored procedure to get just enough info for all lender services to display to users.
        /// </summary>
        private const string GetLightVOXLenderServices = "VERIFICATION_LENDER_SERVICE_GetLight";

        /// <summary>
        /// Stored procedure to get a lender service by id.
        /// </summary>
        private const string GetLenderService = "VERIFICATION_LENDER_SERVICE_Get";

        /// <summary>
        /// Stored procedure to find a replacement lender service.
        /// </summary>
        private const string FindReplacementService = "VERIFICATION_LENDER_SERVICE_FindReplacement";

        /// <summary>
        /// Stored procedure for creating a lender service.
        /// </summary>
        private const string CreateLenderService = "VERIFICATION_LENDER_SERVICE_Create";

        /// <summary>
        /// Stored procedure for updating a lender service.
        /// </summary>
        private const string UpdateLenderService = "VERIFICATION_LENDER_SERVICE_Update";

        /// <summary>
        /// Stored procedure for deleting a lender service.
        /// </summary>
        private const string DeleteLenderService = "VERIFICATION_LENDER_SERVICE_Delete";

        /// <summary>
        /// The display name.
        /// </summary>
        private string displayName;

        /// <summary>
        /// The enabled employee group id.
        /// </summary>
        private Guid? enabledEmployeeGroupId;

        /// <summary>
        /// The enabled oc group id.
        /// </summary>
        private Guid? enabledOCGroupId;

        /// <summary>
        /// Whether payment by credit card is allowed.
        /// </summary>
        private bool allowPaymentByCreditCard;

        /// <summary>
        /// The associated vendor.
        /// </summary>
        private Lazy<VOXVendor> associatedVendor;

        /// <summary>
        /// The associated vendor id.
        /// </summary>
        private int associatedVendorId;

        /// <summary>
        /// The associated reseller.
        /// </summary>
        private Lazy<VOXVendor> associatedReseller;

        /// <summary>
        /// The associated reseller id.
        /// </summary>
        private int associatedResellerId;

        /// <summary>
        /// Prevents a default instance of the <see cref="VOXLenderService"/> class from being created.
        /// </summary>
        private VOXLenderService()
        {
        }

        /// <summary>
        /// Gets a value indicating whether this is a new lender service.
        /// </summary>
        /// <value>Whether this is a new lender service.</value>
        public bool IsNew
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the id of the lender level service.
        /// </summary>
        /// <value>The id of the lender level service.</value>
        public int LenderServiceId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the broker id.
        /// </summary>
        /// <value>The broker id.</value>
        public Guid BrokerId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the type of service.
        /// </summary>
        /// <value>The type of service.</value>
        public VOXServiceT ServiceType
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the id of the associated vendor.
        /// </summary>
        /// <value>The id of the associated vendor.</value>
        public int AssociatedVendorId
        {
            get
            {
                return this.associatedVendorId;
            }

            set
            {
                this.associatedVendorId = value;
                this.associatedVendor = new Lazy<VOXVendor>(() => VOXVendor.LoadVendor(value));
            }
        }

        /// <summary>
        /// Gets or sets the id of the associated vendor that is a reseller.
        /// </summary>
        /// <value>The id of the associated vendor that is a resller.</value>
        public int AssociatedResellerId
        {
            get
            {
                return this.associatedResellerId;
            }

            set
            {
                this.associatedResellerId = value;
                this.associatedReseller = new Lazy<VOXVendor>(() =>
                {
                    if (this.associatedResellerId < 1 || this.associatedResellerId == NoneDirectVendorId)
                    {
                        return null;
                    }
                    else
                    {
                        return VOXVendor.LoadVendor(associatedResellerId);
                    }
                });
            }
        }

        /// <summary>
        /// Gets a value indicating whether this lender service is a direct seller.
        /// </summary>
        /// <value>This is a direct seller if no reseller is chosen.</value>
        public bool IsDirectSeller
        {
            get
            {
                return this.AssociatedResellerId == NoneDirectVendorId;
            }
        }

        /// <summary>
        /// Gets or sets the display name of this lender level service. Warning. Can be heavy if not locked.
        /// </summary>
        /// <value>The display name of the lender level service.</value>
        public string DisplayName
        {
            get
            {
                if (!this.DisplayNameLckd)
                {
                    if (this.associatedResellerId >= 1 && this.associatedResellerId != NoneDirectVendorId)
                    {
                        return $"{this.AssociatedVendor.CompanyName} by {this.AssociatedReseller.CompanyName}";
                    }
                    else
                    {
                        return this.AssociatedVendor.CompanyName;
                    }
                }

                return this.displayName;
            }

            set
            {
                this.displayName = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the display name has been locked.
        /// </summary>
        /// <value>A value indicating whether the display name has been locked.</value>
        public bool DisplayNameLckd
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this service has been enabled for LQB users.
        /// </summary>
        /// <value>A value indicating whether this service has been enabled for LQB users.</value>
        public bool EnabledForLqbUsers
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the id of the employee group that this service has been enabled for.
        /// </summary>
        /// <value>Null means no group. Guid.Empty means all groups.</value>
        public Guid? EnabledEmployeeGroupId
        {
            get
            {
                if (!this.EnabledForLqbUsers)
                {
                    return null;
                }

                return this.enabledEmployeeGroupId;
            }

            set
            {
                this.enabledEmployeeGroupId = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this service has been enabled for TPO users.
        /// </summary>
        /// <value>Whether this service has been enabled for TPO users.</value>
        public bool EnabledForTpoUsers
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the id of the OC group that this service has been enabled for.
        /// </summary>
        /// <value>Null means no group. Guid.Empty means all groups.</value>
        public Guid? EnabledOCGroupId
        {
            get
            {
                if (!this.EnabledForTpoUsers)
                {
                    return null;
                }

                return this.enabledOCGroupId;
            }

            set
            {
                this.enabledOCGroupId = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether payment by credit card is allowed.
        /// </summary>
        /// <value>Whether payment by credit card is allowed.</value>
        public bool AllowPaymentByCreditCard
        {
            get
            {
                if (this.IsVendorPayByCreditCardOverridable)
                {
                   return this.allowPaymentByCreditCard;
                }

                return false;
            }

            set
            {
                this.allowPaymentByCreditCard = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether users in TPO portal can create/edit service credentials using this service.
        /// </summary>
        public bool CanBeUsedForCredentialsInTpo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether VOE orders will use the specific employer record search.
        /// </summary>
        public bool VoeUseSpecificEmployerRecordSearch { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether <see cref="VoeUseSpecificEmployerRecordSearch"/> must be obeyed.
        /// If false, the ordering user may override the value.
        /// </summary>
        public bool VoeEnforceUseSpecificEmployerRecordSearch { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether VOA orders can be requested without specifying assets.
        /// </summary>
        public bool VoaAllowRequestWithoutAssets { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether <see cref="VoaAllowRequestWithoutAssets"/> should be enforced.
        /// If false, the user may override the setting.
        /// </summary>
        public bool VoaEnforceAllowRequestWithoutAssets { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether importing assets is allowed after a VOA order.
        /// </summary>
        public bool VoaAllowImportingAssets { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether VOE orders can be requested for a borrower without specifying employment records.
        /// </summary>
        public bool VoeAllowRequestWithoutEmployment { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether <see cref="VoeAllowRequestWithoutEmployment"/> should be enforced.
        /// If false, the user may override the setting.
        /// </summary>
        public bool VoeEnforceRequestsWithoutEmployment { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether importing employment records is allowed after a VOE order.
        /// </summary>
        public bool VoeAllowImportingEmployment { get; set; }

        /// <summary>
        /// Gets the vendor that we should pull platform/transmission data from.
        /// </summary>
        /// <value>The vendor to pull platform/transmission data from.</value>
        public VOXVendor VendorForProtocolAndTransmissionData
        {
            get
            {
                if (this.IsDirectSeller)
                {
                    return this.AssociatedVendor;
                }
                else
                {
                    return this.AssociatedReseller;
                }
            }
        }

        /// <summary>
        /// Gets the vendor that contains the vendor and service data.
        /// </summary>
        /// <value>The vendor that contains vendor and service data.</value>
        public VOXVendor VendorForVendorAndServiceData
        {
            get
            {
                return this.AssociatedVendor;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the pay by credit card value can be overriden.
        /// </summary>
        /// <value>Whether the pay by credit card value can be overriden.</value>
        private bool IsVendorPayByCreditCardOverridable
        {
            get
            {
                return this.VendorForProtocolAndTransmissionData.CanPayWithCreditCard;
            }
        }

        /// <summary>
        /// Gets the associated vendor.
        /// </summary>
        /// <value>The associated vendor.</value>
        private VOXVendor AssociatedVendor
        {
            get
            {
                return this.associatedVendor.Value;
            }
        }

        /// <summary>
        /// Gets the associated reseller.
        /// </summary>
        /// <value>The associated reseller.</value>
        private VOXVendor AssociatedReseller
        {
            get
            {
                return this.associatedReseller.Value;
            }
        }

        /// <summary>
        /// Loads a lender service by id.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="serviceId">The service id.</param>
        /// <returns>The service id if found. Null otherwise.</returns>
        public static VOXLenderService GetLenderServiceById(Guid brokerId, int serviceId)
        {
            StoredProcedureName? procedureName = StoredProcedureName.Create(GetLenderService);
            if (!procedureName.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {GetLenderService}."));
            }

            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@BrokerId", brokerId.ToString()),
                new SqlParameter("@LenderServiceId", serviceId)
            };

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);
            using (var connection = DbAccessUtils.GetConnection(brokerId))
            using (var reader = driver.ExecuteReader(connection, null, procedureName.Value, parameters))
            {
                if (reader.Read())
                {
                    var service = LoadFromReader(reader);
                    service.BrokerId = brokerId;
                    return service;
                }
            }

            return null;
        }

        /// <summary>
        /// Finds a replacement lender service that has the same vendor and reseller id.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="vendorId">The vendor id.</param>
        /// <param name="resellerId">The reseller id.</param>
        /// <returns>The lender service if a replacement is found. Null otherwise.</returns>
        public static KeyValuePair<int, string>? FindReplacementLenderService(Guid brokerId, int vendorId, int resellerId)
        {
            StoredProcedureName? procedureName = StoredProcedureName.Create(FindReplacementService);
            if (!procedureName.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {FindReplacementService}."));
            }

            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@BrokerId", brokerId.ToString()),
                new SqlParameter("@VendorId", vendorId),
                new SqlParameter("@ResellerId", resellerId)
            };

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);
            using (var connection = DbAccessUtils.GetConnection(brokerId))
            using (var reader = driver.ExecuteReader(connection, null, procedureName.Value, parameters))
            {
                if (reader.Read())
                {
                    KeyValuePair<int, string> lenderService = new KeyValuePair<int, string>((int)reader["LenderServiceId"], (string)reader["DisplayName"]);
                    return lenderService;
                }
            }

            return null;
        }

        /// <summary>
        /// Loads all services for a given lender.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <returns>List of services for a broker.</returns>
        public static IEnumerable<VOXLenderService> GetLenderServices(Guid brokerId)
        {
            List<VOXLenderService> lenderServices = new List<VOXLenderService>();

            StoredProcedureName? procedureName = StoredProcedureName.Create(ListAllLenderServices);
            if (!procedureName.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {ListAllLenderServices}."));
            }

            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@BrokerId", brokerId.ToString())
            };

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);
            using (var connection = DbAccessUtils.GetConnection(brokerId))
            using (var reader = driver.ExecuteReader(connection, null, procedureName.Value, parameters))
            {
                while (reader.Read())
                {
                    var service = LoadFromReader(reader);
                    service.BrokerId = brokerId;
                    lenderServices.Add(service);
                }
            }

            return lenderServices;
        }

        /// <summary>
        /// Gets the lender service names and ids only. Use this if you want to get a list of names since using the normal batch get method will calculate the names, and that is a heavy operation.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="serviceType">The type of service to grab. Null for it doesn't matter.</param>
        /// <returns>List of light lender services.</returns>
        public static IEnumerable<LightVOXLenderService> GetLightLenderServices(Guid brokerId, VOXServiceT? serviceType)
        {
            List<LightVOXLenderService> lenderServices = new List<LightVOXLenderService>();

            StoredProcedureName? procedureName = StoredProcedureName.Create(GetLightVOXLenderServices);
            if (!procedureName.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {GetLightVOXLenderServices}."));
            }

            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@BrokerId", brokerId.ToString()),
                new SqlParameter("@ServiceT", serviceType)
            };

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);
            using (var connection = DbAccessUtils.GetConnection(brokerId))
            using (var reader = driver.ExecuteReader(connection, null, procedureName.Value, parameters))
            {
                while (reader.Read())
                {
                    int id = (int)reader["LenderServiceId"];
                    int vendorId = (int)reader["VendorId"];
                    int resellerId = (int)reader["ResellerId"];
                    string name = (string)reader["DisplayName"];
                    bool isEnabledForLqbUsers = (bool)reader["IsEnabledForLqbUsers"];
                    bool isEnabledForTpoUsers = (bool)reader["IsEnabledForTpoUsers"];
                    Guid? enabledEmployeeGroupId = null;
                    Guid? enabledTpoGroupId = null;
                    bool canBeUsedForCredentialsInTpo = (bool)reader["CanBeUsedForCredentialsInTpo"];
                    if (reader["EnabledEmployeeGroupId"] != DBNull.Value)
                    {
                        enabledEmployeeGroupId = (Guid)reader["EnabledEmployeeGroupId"];
                    }
                    else if (isEnabledForLqbUsers)
                    {
                        enabledEmployeeGroupId = AllGroupsId;
                    }

                    if (reader["EnabledOcGroupId"] != DBNull.Value)
                    {
                        enabledTpoGroupId = (Guid)reader["EnabledOcGroupId"];
                    }
                    else if (isEnabledForTpoUsers)
                    {
                        enabledTpoGroupId = AllGroupsId;
                    }

                    LightVOXLenderService lenderService = new LightVOXLenderService()
                    {
                        LenderServiceId = id,
                        DisplayName = name,
                        EnabledEmployeeGroupId = enabledEmployeeGroupId,
                        EnabledOcGroupId = enabledTpoGroupId,
                        IsEnabledforLqbUsers = isEnabledForLqbUsers,
                        IsEnabledForTpoUsers = isEnabledForTpoUsers,
                        CanBeUsedForCredentialsInTpo = canBeUsedForCredentialsInTpo,
                        VendorId = vendorId,
                        ResellerId = resellerId
                    };

                    lenderServices.Add(lenderService);
                }
            }

            return lenderServices;
        }

        /// <summary>
        /// Gets a value indicating whether the specified lender service can be accessed by the TPO portal for the specified originating company group ids.
        /// </summary>
        /// <param name="lenderService">The lender service to check.</param>
        /// <param name="originatingCompanyGroupIds">The cached list of OC group IDs.  If this method gets more usage, we may want to re-evaluate the type.</param>
        /// <returns><c>true</c> if <see cref="lenderService"/> can be accessed in the TPO portal by the specified OC groups; false otherwise.</returns>
        public static bool ShouldAllowTpoAccess(LightVOXLenderService lenderService, Lazy<IEnumerable<Guid>> originatingCompanyGroupIds)
        {
            return lenderService.IsEnabledForTpoUsers
                && lenderService.EnabledOcGroupId.HasValue
                && (lenderService.EnabledOcGroupId == AllGroupsId
                    || originatingCompanyGroupIds.Value.Contains(lenderService.EnabledOcGroupId.Value));
        }

        /// <summary>
        /// Creates the lender service from the view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>The lender service if made. Null otherwise.</returns>
        public static VOXLenderService CreateFromViewModel(VOXLenderServiceViewModel viewModel, Guid brokerId, out string errors)
        {
            if (!VerifyViewModel(viewModel, out errors))
            {
                return null;
            }

            bool serviceNew = !viewModel.LenderServiceId.HasValue || viewModel.LenderServiceId.Value < 1;
            VOXLenderService loadedService;
            if (serviceNew)
            {
                loadedService = new VOXLenderService();
                loadedService.LenderServiceId = -1;
                loadedService.BrokerId = brokerId;
            }
            else
            {
                loadedService = VOXLenderService.GetLenderServiceById(brokerId, viewModel.LenderServiceId.Value);
            }

            loadedService.AllowPaymentByCreditCard = viewModel.AllowPaymentByCreditCard;
            loadedService.AssociatedResellerId = viewModel.AssociatedResellerId;
            loadedService.AssociatedVendorId = viewModel.AssociatedVendorId;
            loadedService.DisplayName = viewModel.DisplayName;
            loadedService.DisplayNameLckd = viewModel.DisplayNameLckd;
            loadedService.EnabledEmployeeGroupId = viewModel.EnabledEmployeeGroupId;
            loadedService.EnabledForLqbUsers = viewModel.EnabledForLqbUsers;
            loadedService.EnabledForTpoUsers = viewModel.EnabledForTpoUsers;
            loadedService.EnabledOCGroupId = viewModel.EnabledOCGroupId;
            loadedService.CanBeUsedForCredentialsInTpo = viewModel.CanBeUsedForCredentialsInTpo;
            loadedService.VoeUseSpecificEmployerRecordSearch = viewModel.VoeUseSpecificEmployerRecordSearch;
            loadedService.VoeEnforceUseSpecificEmployerRecordSearch = viewModel.VoeEnforceUseSpecificEmployerRecordSearch;
            loadedService.ServiceType = viewModel.ServiceType;
            loadedService.VoaAllowImportingAssets = viewModel.VoaAllowImportingAssets;
            loadedService.VoaAllowRequestWithoutAssets = viewModel.VoaAllowRequestWithoutAssets;
            loadedService.VoaEnforceAllowRequestWithoutAssets = viewModel.VoaEnforceAllowRequestWithoutAssets;
            loadedService.VoeAllowRequestWithoutEmployment = viewModel.VoeAllowRequestWithoutEmployment;
            loadedService.VoeEnforceRequestsWithoutEmployment = viewModel.VoeEnforceRequestsWithoutEmployment;
            loadedService.VoeAllowImportingEmployment = viewModel.VoeAllowImportingEmployment;
            loadedService.IsNew = serviceNew;

            return loadedService;
        }

        /// <summary>
        /// Deletes the lender service.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="lenderServiceId">The lender service id.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if deleted. False otherwise.</returns>
        public static bool DeleteService(Guid brokerId, int lenderServiceId, out string errors)
        {
            if (lenderServiceId < 1)
            {
                errors = $"Invalid id {lenderServiceId}";
                return false;
            }

            StoredProcedureName? procedureName = StoredProcedureName.Create(DeleteLenderService);
            if (!procedureName.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {DeleteLenderService}."));
            }

            List<SqlParameter> parameters = new List<SqlParameter>()
                    {
                        new SqlParameter("@LenderServiceId", lenderServiceId),
                        new SqlParameter("@BrokerId", brokerId)
                    };

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);
            using (var connection = DbAccessUtils.GetConnection(brokerId))
            {
                // This sproc also deletes any VOA options configured for the lender service.
                driver.ExecuteNonQuery(connection, null, procedureName.Value, parameters);
            }

            errors = string.Empty;
            return true;
        }

        /// <summary>
        /// Filters out the valid vendors based on service type.
        /// </summary>
        /// <param name="vendorList">The vendor list.</param>
        /// <param name="serviceT">The chosen service type.</param>
        /// <returns>The list of valid vendors.</returns>
        public static IEnumerable<LightVOXVendor> FilterValidVendorsByServiceT(IEnumerable<LightVOXVendor> vendorList, VOXServiceT serviceT)
        {
            IEnumerable<LightVOXVendor> validVendors = vendorList.Where((vendor) =>
            {
                return vendor.IsEnabled &&
                       vendor.Services.Values.Any((service) => service.ServiceType == serviceT &&
                                                               service.IsEnabled &&
                                                               (service.IsADirectVendor || service.SellsViaResellers));
            });

            return validVendors == null ? new List<LightVOXVendor>() : validVendors;
        }

        /// <summary>
        /// Gets a list of service types used by valid vendors from the specified list.
        /// </summary>
        /// <param name="vendorList">The list of vendors containing the services.</param>
        /// <returns>A list of service types.</returns>
        /// <remarks>
        /// This method is somewhat coupled with <see cref="FilterValidVendorsByServiceT(IEnumerable{LightVOXVendor}, VOXServiceT)"/> in
        /// that they both contain very similar logic.
        /// </remarks>
        public static IEnumerable<VOXServiceT> GetValidVendorServiceTypes(IEnumerable<LightVOXVendor> vendorList)
        {
            return vendorList.Where(vendor => vendor.IsEnabled)
                .SelectMany(vendor => vendor.Services.Values)
                .Where(service => service.IsEnabled && (service.IsADirectVendor || service.SellsViaResellers))
                .Select(service => service.ServiceType)
                .Distinct();
        }

        /// <summary>
        /// Calculates valid resellers.
        /// </summary>
        /// <param name="vendorList">The vendor list.</param>
        /// <param name="serviceT">The service type.</param>
        /// <param name="chosenVendor">The chosen vendor.</param>
        /// <param name="isDirectVendor">Whether the vendor is a direct vendor for the service.</param>
        /// <returns>The list of valid resellers.</returns>
        public static IEnumerable<LightVOXVendor> FilterValidResellersByVendorAndServiceT(IEnumerable<LightVOXVendor> vendorList, VOXServiceT serviceT, LightVOXVendor chosenVendor, out bool isDirectVendor)
        {
            List<LightVOXVendor> resellers = new List<LightVOXVendor>();
            isDirectVendor = false;
            LightVOXVendorService chosenService;
            if (chosenVendor.Services.TryGetValue(serviceT, out chosenService))
            {
                if (chosenService.IsADirectVendor)
                {
                    isDirectVendor = true;
                }

                if (chosenService.SellsViaResellers)
                {
                    var chosen = vendorList.Where((vendor) =>
                    {
                        return vendor.IsEnabled &&
                               vendor.Services.Values.Any((service) => service.ServiceType == serviceT &&
                                                                       service.IsEnabled &&
                                                                       service.IsAReseller);
                    });

                    resellers.AddRange(chosen);
                }
            }

            return resellers;
        }

        /// <summary>
        /// Saves the lender service.
        /// </summary>
        /// <param name="errors">Any errors encountered.</param>
        /// <param name="voaOptionConfiguration">A VOA option configuration.</param>
        /// <returns>True if saved. False otherwise.</returns>
        /// <remarks>
        /// VOXLenderService should be a target for refactor to break up the different service types, so we don't have to
        /// deal with a VOA option configuration in the save logic for every type of lender service.
        /// </remarks>
        public bool Save(out string errors, List<VOAOption> voaOptionConfiguration = null)
        {
            string spName = this.IsNew ? CreateLenderService : UpdateLenderService;
            StoredProcedureName? procedureName = StoredProcedureName.Create(spName);
            if (!procedureName.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {spName}."));
            }

            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@ServiceT", this.ServiceType),
                new SqlParameter("@VendorId", this.AssociatedVendorId),
                new SqlParameter("@ResellerId", this.AssociatedResellerId),
                new SqlParameter("@DisplayName", this.displayName),
                new SqlParameter("@DisplayNameLckd", this.DisplayNameLckd),
                new SqlParameter("@IsEnabledForLqbUsers", this.EnabledForLqbUsers),
                new SqlParameter("@EnabledEmployeeGroupId", this.EnabledEmployeeGroupId.HasValue && this.EnabledEmployeeGroupId.Value == AllGroupsId ? null : this.EnabledEmployeeGroupId),
                new SqlParameter("@IsEnabledForTpoUsers", this.EnabledForTpoUsers),
                new SqlParameter("@EnabledOcGroupId", this.EnabledOCGroupId.HasValue && this.EnabledOCGroupId.Value == AllGroupsId ? null : this.EnabledOCGroupId),
                new SqlParameter("@AllowPaymentByCreditCard", this.AllowPaymentByCreditCard),
                new SqlParameter("@CanBeUsedForCredentialsInTpo", this.CanBeUsedForCredentialsInTpo),
                new SqlParameter("@VoeUseSpecificEmployerRecordSearch", this.VoeUseSpecificEmployerRecordSearch),
                new SqlParameter("@VoeEnforceUseSpecificEmployerRecordSearch", this.VoeEnforceUseSpecificEmployerRecordSearch),
                new SqlParameter("@VoaAllowImportingAssets", this.VoaAllowImportingAssets),
                new SqlParameter("@VoaAllowRequestWithoutAssets", this.VoaAllowRequestWithoutAssets),
                new SqlParameter("@VoaEnforceAllowRequestWithoutAssets", this.VoaEnforceAllowRequestWithoutAssets),
                new SqlParameter("@VoeAllowImportingEmployment", this.VoeAllowImportingEmployment),
                new SqlParameter("@VoeAllowRequestWithoutEmployment", this.VoeAllowRequestWithoutEmployment),
                new SqlParameter("@VoeEnforceRequestsWithoutEmployment", this.VoeEnforceRequestsWithoutEmployment)
            };

            SqlParameter lenderServiceIdOutput = null;
            if (this.IsNew)
            {
                lenderServiceIdOutput = new SqlParameter("@LenderServiceId", System.Data.SqlDbType.Int);
                lenderServiceIdOutput.Direction = System.Data.ParameterDirection.Output;
                parameters.Add(lenderServiceIdOutput);
            }
            else
            {
                parameters.Add(new SqlParameter("@LenderServiceId", this.LenderServiceId));
            }

            using (var exec = new CStoredProcedureExec(this.BrokerId))
            {
                try
                {
                    exec.BeginTransactionForWrite();
                    exec.ExecuteNonQuery(procedureName.Value.ToString(), 3, parameters.ToArray());

                    if (this.IsNew)
                    {
                        this.LenderServiceId = (int)lenderServiceIdOutput.Value;
                    }

                    if (this.ServiceType == VOXServiceT.VOA_VOD && voaOptionConfiguration != null)
                    {
                        foreach (var option in voaOptionConfiguration)
                        {
                            // We don't populate these datapoints before they come back from the frontend due to client security risk.
                            option.BrokerId = this.BrokerId;
                            option.LenderServiceId = this.LenderServiceId;
                            option.VendorId = this.AssociatedVendorId;
                        }

                        var configuration = VOAOptionConfiguration.Create(
                            this.BrokerId,
                            this.LenderServiceId,
                            this.AssociatedVendorId,
                            overrideOptions: voaOptionConfiguration);

                        if (!configuration.SaveConfiguration(exec, out errors))
                        {
                            exec.RollbackTransaction();
                            return false;
                        }
                    }

                    exec.CommitTransaction();

                    this.IsNew = false;
                    errors = string.Empty;
                    return true;
                }
                catch (SqlException exc)
                {
                    exec.RollbackTransaction();
                    errors = exc.Number == 2627 ? "Cannot have services with duplicate names and service type." : ErrorMessages.Generic;
                    return false;
                }
                catch (CBaseException exc)
                {
                    exec.RollbackTransaction();
                    errors = exc.UserMessage;
                    return false;
                }
            }
        }

        /// <summary>
        /// Validates the lender service to make sure it is still valid after any changes to the platform/vendor config.
        /// </summary>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if valid, false otherwise.</returns>
        public bool ValidateLenderService(out string errors)
        {
            var allVendors = VOXVendor.LoadLightVendorsWithServices();
            var validVendors = VOXLenderService.FilterValidVendorsByServiceT(allVendors.Values, this.ServiceType);
            var chosenVendor = validVendors.FirstOrDefault((vendor) => vendor.VendorId == this.AssociatedVendorId);
            if (chosenVendor == null) 
            {
                errors = "Chosen Vendor is no longer valid.";
                return false;
            }

            bool isDirectVendor;
            var validResellers = VOXLenderService.FilterValidResellersByVendorAndServiceT(allVendors.Values, this.ServiceType, chosenVendor, out isDirectVendor);
            if (!isDirectVendor && this.AssociatedResellerId == NoneDirectVendorId)
            {
                errors = "Chosen Vendor is not a Direct Vendor but no Reseller is chosen.";
                return false;
            }

            if (this.AssociatedResellerId != NoneDirectVendorId)
            {
                var chosenReseller = validResellers.FirstOrDefault((reseller) => reseller.VendorId == this.AssociatedResellerId);
                if (chosenReseller == null)
                {
                    errors = "Chosen reseller is no longer valid.";
                    return false;
                }
            }

            errors = string.Empty;
            return true;
        }

        /// <summary>
        /// Loads a lender service from a reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns>The created lender service.</returns>
        private static VOXLenderService LoadFromReader(IDataReader reader)
        {
            VOXLenderService newService = new VOXLenderService();
            newService.AssociatedResellerId = (int)reader["ResellerId"];
            newService.AssociatedVendorId = (int)reader["VendorId"];
            newService.DisplayName = (string)reader["DisplayName"];
            newService.DisplayNameLckd = (bool)reader["DisplayNameLckd"];
            newService.EnabledForLqbUsers = (bool)reader["IsEnabledForLqbUsers"];
            newService.EnabledForTpoUsers = (bool)reader["IsEnabledForTpoUsers"];
            newService.LenderServiceId = (int)reader["LenderServiceId"];
            newService.ServiceType = (VOXServiceT)reader["ServiceT"];
            newService.AllowPaymentByCreditCard = (bool)reader["AllowPaymentByCreditCard"];
            newService.CanBeUsedForCredentialsInTpo = (bool)reader["CanBeUsedForCredentialsInTpo"];
            newService.VoeUseSpecificEmployerRecordSearch = (bool)reader["VoeUseSpecificEmployerRecordSearch"];
            newService.VoeEnforceUseSpecificEmployerRecordSearch = (bool)reader["VoeEnforceUseSpecificEmployerRecordSearch"];
            newService.VoaAllowImportingAssets = (bool)reader["VoaAllowImportingAssets"];
            newService.VoaAllowRequestWithoutAssets = (bool)reader["VoaAllowRequestWithoutAssets"];
            newService.VoaEnforceAllowRequestWithoutAssets = (bool)reader["VoaEnforceAllowRequestWithoutAssets"];
            newService.VoeAllowImportingEmployment = (bool)reader["VoeAllowImportingEmployment"];
            newService.VoeAllowRequestWithoutEmployment = (bool)reader["VoeAllowRequestWithoutEmployment"];
            newService.VoeEnforceRequestsWithoutEmployment = (bool)reader["VoeEnforceRequestsWithoutEmployment"];

            if (reader["EnabledOcGroupId"] == DBNull.Value)
            {
                newService.EnabledOCGroupId = null;
                if (newService.EnabledForTpoUsers)
                {
                    newService.EnabledOCGroupId = AllGroupsId;
                }
            }
            else
            {
                newService.EnabledOCGroupId = (Guid)reader["EnabledOcGroupId"];
            }

            if (reader["EnabledEmployeeGroupId"] == DBNull.Value)
            {
                newService.EnabledEmployeeGroupId = null;
                if (newService.EnabledForLqbUsers)
                {
                    newService.EnabledEmployeeGroupId = AllGroupsId;
                }
            }
            else
            {
                newService.EnabledEmployeeGroupId = (Guid)reader["EnabledEmployeeGroupId"];
            }

            return newService;
        }

        /// <summary>
        /// Verifies the view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if verified. False otherwise.</returns>
        private static bool VerifyViewModel(VOXLenderServiceViewModel viewModel, out string errors)
        {
            StringBuilder errorBuilder = new StringBuilder();
            if (viewModel.AssociatedResellerId < 1 && viewModel.AssociatedResellerId != NoneDirectVendorId)
            {
                errorBuilder.AppendLine("Please select a Reseller.");
            }

            if (viewModel.AssociatedVendorId < 1)
            {
                errorBuilder.AppendLine("Please select a Vendor");
            }

            if (string.IsNullOrEmpty(viewModel.DisplayName))
            {
                errorBuilder.AppendLine("Please specify a Display Name");
            }

            errors = errorBuilder.ToString();
            return errorBuilder.Length == 0;
        }
    }
}
