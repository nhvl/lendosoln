﻿namespace LendersOffice.Integration.VOXFramework
{
    using System.Collections.Generic;

    /// <summary>
    /// Class for holding VOA request results.
    /// </summary>
    public class VOARequestResults
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOARequestResults" /> class.
        /// </summary>
        /// <param name="status">The status of the VOA job.</param>
        /// <param name="orders">The VOA orders, if the job is completed.</param>
        /// <param name="errors">The errors, if the job has errored.</param>
        public VOARequestResults(IntegrationJobStatus status, IEnumerable<VOAOrder> orders, IEnumerable<string> errors)
        {
            this.Status = status;
            this.Orders = orders;
            this.Errors = errors;
        }

        /// <summary>
        /// Gets the status of the integration job.
        /// </summary>
        public IntegrationJobStatus Status
        {
            get; private set;
        }

        /// <summary>
        /// Gets the VOA orders if the job is completed.
        /// </summary>
        public IEnumerable<VOAOrder> Orders
        {
            get; private set;
        }

        /// <summary>
        /// Gets the errors if the job has errored.
        /// </summary>
        public IEnumerable<string> Errors
        {
            get; private set;
        }
    }
}
