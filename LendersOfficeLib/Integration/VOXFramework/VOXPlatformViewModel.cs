﻿namespace LendersOffice.Integration.VOXFramework
{
    using LendersOffice.Integration.FrameworkCommon;

    /// <summary>
    /// Viewmodel for a platform.
    /// </summary>
    public class VOXPlatformViewModel : IntegrationPlatformViewModel
    {
        /// <summary>
        /// Gets or sets the transmission model.
        /// </summary>
        /// <value>The transmission model.</value>
        public TransmissionModelT TransmissionModelT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the payload format.
        /// </summary>
        /// <value>The payload format.</value>
        public PayloadFormatT PayloadFormatT
        {
            get;
            set;
        }
    }
}
