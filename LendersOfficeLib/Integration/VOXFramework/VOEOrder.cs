﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using Common;

    /// <summary>
    /// Class representing a VOE order.
    /// </summary>
    public class VOEOrder : AbstractVOXOrder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOEOrder"/> class.
        /// </summary>
        /// <param name="record">The database record of an order.</param>
        /// <param name="employmentRecords">The database records of the employment records on the order.</param>
        /// <param name="documentIds">The document ids for this order.</param>
        /// <param name="vendorReferenceIds">The vendor reference ids.</param>
        /// <param name="statuses">The collection of statuses for this order.</param>
        public VOEOrder(IReadOnlyDictionary<string, object> record, IReadOnlyList<IReadOnlyDictionary<string, object>> employmentRecords, List<Guid> documentIds, HashSet<string> vendorReferenceIds, List<VOXStatus> statuses)
            : base(record, documentIds, vendorReferenceIds)
        {
            this.ParentOrderNumber = record["ParentOrderNumber"] == DBNull.Value ? string.Empty : (string)record["ParentOrderNumber"];
            this.VerificationType = (VOEVerificationT)record["VerificationType"];
            this.BorrowerName = (string)record["BorrowerName"];
            this.Last4Ssn = record["Last4Ssn"] == DBNull.Value ? null : (string)record["Last4Ssn"];
            this.EmploymentRecords = employmentRecords.Select(
                employmentRecord => new VOEOrderEmploymentRecord(
                    (Guid)employmentRecord["Id"],
                    (string)employmentRecord["EmployerName"],
                    (bool)employmentRecord["IsSelfEmployed"])).ToList();
            this.Statuses = statuses;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VOEOrder"/> class.
        /// </summary>
        /// <param name="requestData">The source data of the UI's request.</param>
        /// <param name="orderRequest">The request of the order metadata that was sent.</param>
        /// <param name="serviceResponse">The response to the order.</param>
        /// <returns>An order object with a corresponding database entry.</returns>
        private VOEOrder(VOERequestData requestData, VOXRequestOrder orderRequest, VOXServiceResponse serviceResponse)
            : base(requestData, orderRequest, serviceResponse)
        {
            var existingOrder = (VOEOrder)requestData.PreviousOrder; // null for initial requests
            this.ParentOrderNumber = serviceResponse.ParentOrderId ?? string.Empty;
            this.VerificationType = existingOrder?.VerificationType ?? (requestData.ShouldVerifyIncome ? VOEVerificationT.EmploymentIncome : VOEVerificationT.Employment);

            Lazy<Tuple<Guid, bool>> exportedBorrowerKey = new Lazy<Tuple<Guid, bool>>(() => Tuple.Create(orderRequest.ApplicationId, orderRequest.IsForCoborrower.Value));
            this.BorrowerName = existingOrder?.BorrowerName ?? requestData.BorrowerDataExported[exportedBorrowerKey.Value].BorrowerName;
            this.Last4Ssn = existingOrder?.Last4Ssn ?? requestData.BorrowerDataExported[exportedBorrowerKey.Value].Last4Ssn;
            this.Statuses = serviceResponse.Statuses;

            if (!string.IsNullOrEmpty(this.ParentOrderNumber) || requestData.VerifiesBorrower)
            {
                // Employment records should not populate for a sub-order (where ParentOrderNumber is not empty) or a Borrower verification order.
            }
            else if (existingOrder?.EmploymentRecords != null)
            {
                this.EmploymentRecords = existingOrder?.EmploymentRecords;
            }
            else if (serviceResponse.AllTransactionIds.All(transactionId => requestData.ServiceTransactionIdEmploymentRecordIdRelationshipsExported.ContainsKey(transactionId)))
            {
                Guid employmentRecordId = requestData.ServiceTransactionIdEmploymentRecordIdRelationshipsExported[serviceResponse.TransactionId];
                this.EmploymentRecords = new[] { requestData.EmploymentsExported.Single(e => e.Id == employmentRecordId) };
            }
            else
            {
                this.EmploymentRecords = existingOrder?.EmploymentRecords ?? orderRequest.LinkedRecords.Select(id => requestData.EmploymentsExported.Single(e => e.Id == id)).ToList();
            }
        }

        /// <summary>
        /// Gets the vendor ID for the parent order.
        /// </summary>
        /// <value>The vendor ID for the parent order.</value>
        public string ParentOrderNumber { get; }

        /// <summary>
        /// Gets the VOE verification type.
        /// </summary>
        /// <value>The VOE verification type.</value>
        public VOEVerificationT VerificationType { get; }

        /// <summary>
        /// Gets the name of the borrower associated with this order.
        /// </summary>
        /// <value>The name of the borrower associated with this order.</value>
        public string BorrowerName { get; }

        /// <summary>
        /// Gets the last 4 SSN numbers of the borrower associated with this order.
        /// </summary>
        /// <value>The last 4 SSN numbers of the borrower associated with this order.</value>
        public string Last4Ssn { get; }

        /// <summary>
        /// Gets a value indicating whether this can be refreshed.
        /// </summary>
        /// <value>Can be refreshed if is a complete order.</value>
        public override bool CanBeRefreshed
        {
            get { return this.CurrentStatus == VOXOrderStatusT.Complete; }
        }

        /// <summary>
        /// Gets a list of employment records associated with this order.
        /// </summary>
        /// <value>List of employment records associated with this order.</value>
        public IReadOnlyList<VOEOrderEmploymentRecord> EmploymentRecords { get; } = new List<VOEOrderEmploymentRecord>().AsReadOnly();

        /// <summary>
        /// Gets all Verification of Employment orders for this loan.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="loanId">The loan id.</param>
        /// <returns>The list of verification of employment orders.</returns>
        public static IEnumerable<VOEOrder> GetOrdersForLoan(Guid brokerId, Guid loanId)
        {
            return GetOrdersForLoan(brokerId, loanId, null);
        }

        /// <summary>
        /// Creates a new record of an order in the database from a placed order.
        /// </summary>
        /// <param name="requestData">The source data of the UI's request.</param>
        /// <param name="orderRequest">The request of the order metadata that was sent.</param>
        /// <param name="serviceResponse">The response to the order.</param>
        /// <returns>The order object representing the database's entry.</returns>
        public static VOEOrder CreateOrder(VOERequestData requestData, VOXRequestOrder orderRequest, VOXServiceResponse serviceResponse)
        {
            VOEOrder order = new VOEOrder(requestData, orderRequest, serviceResponse);
            order.SaveNewOrderInDatabase(requestData.PreviousOrder);
            return order;
        }

        /// <summary>
        /// Retrieves an order.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="orderId">The order id.</param>
        /// <returns>The order if found. Null otherwise.</returns>
        public static VOEOrder GetOrderForLoan(Guid brokerId, Guid loanId, int orderId)
        {
            return GetOrdersForLoan(brokerId, loanId, orderId).SingleOrDefault();
        }

        /// <summary>
        /// Creates a view model out of this order.
        /// </summary>
        /// <param name="extraInfo">Extra info outside of the order.</param>
        /// <returns>The view model.</returns>
        public new VOEOrderViewModel CreateViewModel(ExtraOrderViewModelInfo extraInfo)
        {
            VOEOrderViewModel model = new VOEOrderViewModel()
            {
                CanBeRefreshed = this.CanBeRefreshed,
                DateCompleted = this.DateCompleted?.ToString("G") ?? string.Empty,
                DateOrdered = this.DateOrdered.ToString("G"),
                Error = this.ErrorMessage,
                HasBeenRefreshed = this.HasBeenRefreshed,
                OrderedBy = this.OrderedBy,
                OrderId = this.OrderId,
                OrderNumber = this.OrderNumber,
                ParentOrderNumber = this.ParentOrderNumber,
                LenderServiceName = this.LenderServiceName,
                Status = this.CurrentStatus,
                StatusDescription = this.CurrentStatusDescription,
                BorrowerName = this.BorrowerName,
                Employers = this.EmploymentRecords.Select(r => r.EmployerName).ToList(),
                IsSelfEmployment = this.EmploymentRecords.Any(r => r.IsSelfEmployed),
                VerificationType = this.VerificationType,
                VendorReferenceIds = this.VendorReferenceIds.ToList(),
                HasStatusList = this.Statuses?.Count > 1 || (this.Statuses.Count == 0 && this.Statuses.FirstOrDefault()?.StatusDateTime != null)
            };

            model.SetAssociatedEdocs(this.AssociatedEdocs, extraInfo?.AvailableEdocs);
            if (extraInfo != null)
            {
                model.RequiresAccountId = extraInfo.RequiresAccountId;
                model.UsesAccountId = extraInfo.UsesAccountId;
                model.ServiceCredentialId = extraInfo.ServiceCredentialId;
                model.ServiceCredentialHasAccountId = extraInfo.ServiceCredentialHasAccountId;
            }

            return model;
        }

        /// <summary>
        /// Creates a view model out of this loan.
        /// </summary>
        /// <param name="extraInfo">Extra info outside of the model.</param>
        /// <returns>The view model.</returns>
        protected override AbstractVOXOrderViewModel CreateAbstractViewModel(ExtraOrderViewModelInfo extraInfo)
        {
            return this.CreateViewModel(extraInfo);
        }

        /// <summary>
        /// Handles order creation in the database, for the current instance's data.
        /// </summary>
        /// <param name="storedProcedureExecutionContext">The context of the stored procedure, i.e. connection and transaction.</param>
        protected override void CreateOrderInDatabase(DataAccess.CStoredProcedureExec storedProcedureExecutionContext)
        {
            base.CreateOrderInDatabase(storedProcedureExecutionContext); // The base implementation will determine the OrderId
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@OrderId", this.OrderId),
                new SqlParameter("@VerificationType", this.VerificationType),
                new SqlParameter("@BorrowerName", this.BorrowerName),
                new SqlParameter("@ParentOrderNumber", this.ParentOrderNumber),
                new SqlParameter("@Last4Ssn", this.Last4Ssn.ToString()) // I'm calling ToString on the string so that this will throw if this property is null, since we don't actually want it to be null.
            };

            storedProcedureExecutionContext.ExecuteNonQuery("VOE_ORDER_Create", parameters);

            foreach (var employment in this.EmploymentRecords)
            {
                var employmentParameter = new SqlParameter[]
                {
                new SqlParameter("@OrderId", this.OrderId),
                new SqlParameter("@Id", employment.Id),
                new SqlParameter("@EmployerName", employment.EmployerName),
                new SqlParameter("@IsSelfEmployed", employment.IsSelfEmployed)
                };

                storedProcedureExecutionContext.ExecuteNonQuery("VOE_ORDER_EMPLOYMENT_Create", employmentParameter);
            }
        }

        /// <summary>
        /// Retrieves the verification of employment orders for the specified loan, optionally restricted to a single order.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="orderId">The order id.</param>
        /// <returns>The list of verification of employment orders.</returns>
        private static IReadOnlyList<VOEOrder> GetOrdersForLoan(Guid brokerId, Guid loanId, int? orderId)
        {
            string storedProcedureName = "VOE_ORDER_Retrieve";
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@OrderId", orderId)
            };

            IReadOnlyList<IReadOnlyList<IReadOnlyDictionary<string, object>>> results = DataAccess.StoredProcedureHelper.ExecuteReaderIntoResultListDictionaries(brokerId, storedProcedureName, parameters);
            IReadOnlyList<IReadOnlyDictionary<string, object>> orderRecords = results[0];
            IReadOnlyList<IReadOnlyDictionary<string, object>> employmentRecords = results[1];
            Dictionary<int, List<Guid>> orderToDocIds = CreateOrderToDocsMap(results[2]);
            Dictionary<int, HashSet<string>> orderToVendorReferenceIds = CreateOrderToVendorReferenceIdsMap(results[3]);
            IReadOnlyList<IReadOnlyDictionary<string, object>> statusList = results[4];

            var orders = new List<VOEOrder>(orderRecords.Count);
            foreach (var orderRecord in orderRecords)
            {
                int orderRecordId = (int)orderRecord["OrderId"];
                var employmentRecordsOnOrder = new List<IReadOnlyDictionary<string, object>>(employmentRecords.Count);
                foreach (var employmentRecord in employmentRecords)
                {
                    if (orderRecordId == (int)employmentRecord["OrderId"])
                    {
                        employmentRecordsOnOrder.Add(employmentRecord);
                    }
                }

                var statusesOnOrder = new List<VOXStatus>();
                foreach (var status in statusList)
                {
                    if (orderRecordId == (int)status["OrderId"])
                    {
                        statusesOnOrder.Add(new VOXStatus((VOXOrderStatusT)status["StatusCode"], (string)status["StatusDescription"], (DateTime)status["StatusTime"]));
                    }
                }

                List<Guid> documentIds;
                if (!orderToDocIds.TryGetValue(orderRecordId, out documentIds))
                {
                    documentIds = new List<Guid>();
                }

                orders.Add(new VOEOrder(orderRecord, employmentRecordsOnOrder, documentIds, orderToVendorReferenceIds.GetValueOrNull(orderRecordId), statusesOnOrder));
            }

            return orders;
        }
    }
}
