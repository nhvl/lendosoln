﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Manages saving and retrieval of VOA options for a <see cref="VOXLenderService"/>.
    /// </summary>
    public class VOAOptionConfiguration
    {
        /// <summary>
        /// The sproc name for deleting all options by vendor.
        /// </summary>
        private const string DeleteAllOptionsByVendorSprocName = "VOA_LENDER_ENABLED_OPTION_DeleteAllOptionsByVendor";

        /// <summary>
        /// The sproc name for deleting a single option by vendor.
        /// </summary>
        private const string DeleteOptionValueByVendorSprocName = "VOA_LENDER_ENABLED_OPTION_DeleteOptionValueByVendor";

        /// <summary>
        /// The sproc name for retrieving all DISTINCT options by vendor.
        /// </summary>
        private const string RetrieveDistinctOptionValuesForVendorSprocName = "VOA_LENDER_ENABLED_OPTION_RetrieveDistinctOptionValuesForVendor";

        /// <summary>
        /// The sproc name for deleting all options by lender service.
        /// </summary>
        private const string DeleteAllOptionsByLenderServiceSprocName = "VOA_LENDER_ENABLED_OPTION_DeleteAllOptionsByLenderService";

        /// <summary>
        /// The sproc name for retrieving all options for a lender service.
        /// </summary>
        private const string RetrieveOptionsForLenderServiceSprocName = "VOA_LENDER_ENABLED_OPTION_RetrieveOptionsForLenderService";

        /// <summary>
        /// The sproc name for saving an option.
        /// </summary>
        private const string SaveOptionSprocName = "VOA_LENDER_ENABLED_OPTION_SaveOption";

        /// <summary>
        /// The associated broker ID for the configuration.
        /// </summary>
        private Guid brokerId;

        /// <summary>
        /// The associated lender service ID for the configuration.
        /// </summary>
        private int lenderServiceId;

        /// <summary>
        /// The vendor associated with the lender service.
        /// </summary>
        private int vendorId;

        /// <summary>
        /// Initializes a new instance of the <see cref="VOAOptionConfiguration"/> class.
        /// </summary>
        /// <param name="brokerId">The broker ID associated with the configuration.</param>
        /// <param name="lenderServiceId">The lender service ID associated with the configuration.</param>
        /// <param name="vendorId">The vendor associated with the configuration.</param>
        /// <param name="options">The configuration will be initialized with these items.</param>
        private VOAOptionConfiguration(Guid brokerId, int lenderServiceId, int vendorId, IEnumerable<VOAOption> options)
        {
            this.brokerId = brokerId;
            this.lenderServiceId = lenderServiceId;
            this.vendorId = vendorId;

            this.AccountHistoryOptions_LqbUsers = options.Where(o => o.OptionType.Equals(VOAOptionType.AccountHistory) && o.UserType.Equals("B", StringComparison.OrdinalIgnoreCase)).ToList();
            this.AccountHistoryOptions_PmlUsers = options.Where(o => o.OptionType.Equals(VOAOptionType.AccountHistory) && o.UserType.Equals("P", StringComparison.OrdinalIgnoreCase)).ToList();

            this.RefreshPeriodOptions_LqbUsers = options.Where(o => o.OptionType.Equals(VOAOptionType.RefreshPeriod) && o.UserType.Equals("B", StringComparison.OrdinalIgnoreCase)).ToList();
            this.RefreshPeriodOptions_PmlUsers = options.Where(o => o.OptionType.Equals(VOAOptionType.RefreshPeriod) && o.UserType.Equals("P", StringComparison.OrdinalIgnoreCase)).ToList();
        }

        /// <summary>
        /// Gets the list of account history options configured for B-users.
        /// </summary>
        public List<VOAOption> AccountHistoryOptions_LqbUsers { get; }

        /// <summary>
        /// Gets the default account history value configured for B-users.
        /// </summary>
        public int? AccountHistoryOptionsDefaultValue_LqbUsers
        {
            get
            {
                return this.GetDefaultValue(this.AccountHistoryOptions_LqbUsers);
            }
        }

        /// <summary>
        /// Gets the list of account history options configured for P-users.
        /// </summary>
        public List<VOAOption> AccountHistoryOptions_PmlUsers { get; }

        /// <summary>
        /// Gets the default refresh period value configured for B-users.
        /// </summary>
        public int? RefreshPeriodOptionsDefaultValue_LqbUsers
        {
            get
            {
                return this.GetDefaultValue(this.RefreshPeriodOptions_LqbUsers);
            }
        }

        /// <summary>
        /// Gets the list of refresh period options configured for B-users.
        /// </summary>
        public List<VOAOption> RefreshPeriodOptions_LqbUsers { get; }

        /// <summary>
        /// Gets the default account history value configured for P-users.
        /// </summary>
        public int? AccountHistoryOptionsDefaultValue_PmlUsers
        {
            get
            {
                return this.GetDefaultValue(this.AccountHistoryOptions_PmlUsers);
            }
        }

        /// <summary>
        /// Gets the list of refresh period options configured for P-users.
        /// </summary>
        public List<VOAOption> RefreshPeriodOptions_PmlUsers { get; }

        /// <summary>
        /// Gets the default account history value configured for B-users.
        /// </summary>
        public int? RefreshPeriodOptionsDefaultValue_PmlUsers
        {
            get
            {
                return this.GetDefaultValue(this.RefreshPeriodOptions_PmlUsers);
            }
        }

        /// <summary>
        /// Creates a new VOA option configuration.
        /// </summary>
        /// <param name="brokerId">The broker ID associated with the configuration.</param>
        /// <param name="lenderServiceId">The lender service ID associated with the configuration.</param>
        /// <param name="vendorId">The vendor ID associated with the configuration.</param>
        /// <param name="overrideOptions">If provided, these options will populate the new configuration object.</param>
        /// <returns>An empty VOA option configuration.</returns>
        public static VOAOptionConfiguration Create(Guid brokerId, int lenderServiceId, int vendorId, IEnumerable<VOAOption> overrideOptions = null)
        {
            if (overrideOptions == null)
            {
                overrideOptions = Enumerable.Empty<VOAOption>();
            }

            return new VOAOptionConfiguration(brokerId, lenderServiceId, vendorId, overrideOptions);
        }

        /// <summary>
        /// Loads an existing VOA option configuration from the DB.
        /// </summary>
        /// <param name="brokerId">The broker ID associated with the configuration.</param>
        /// <param name="lenderServiceId">The lender service ID associated with the configuration.</param>
        /// <param name="vendorId">The vendor ID associated with the configuration.</param>
        /// <returns>A VOA option configuration.</returns>
        public static VOAOptionConfiguration Load(Guid brokerId, int lenderServiceId, int vendorId)
        {
            var savedOptions = RetrieveOptionsForLenderService(brokerId, lenderServiceId, vendorId);
            return new VOAOptionConfiguration(brokerId, lenderServiceId, vendorId, savedOptions);
        }

        /// <summary>
        /// Deletes all VOA options associated with a given vendor, regardless of lender service association.
        /// </summary>
        /// <param name="vendorId">The vendor ID to delete.</param>
        /// <remarks>This will generally be called when a Vendor VOA service is disabled.</remarks>
        public static void DeleteAllOptionsByVendor(int vendorId)
        {
            StoredProcedureName? procedureName = StoredProcedureName.Create(DeleteAllOptionsByVendorSprocName);
            if (!procedureName.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {DeleteAllOptionsByVendorSprocName}."));
            }

            foreach (DbConnectionInfo conn in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@VendorId", vendorId)
                };

                StoredProcedureHelper.ExecuteNonQuery(conn, procedureName.Value.ToString(), 3, parameters);
            }
        }

        /// <summary>
        /// Deletes a single VOA option value for ALL lender services that have it configured.
        /// </summary>
        /// <param name="vendorId">The vendor ID associated with the given option.</param>
        /// <param name="optionType">The option type to delete.</param>
        /// <param name="optionValue">The option value to delete.</param>
        public static void DeleteOptionValueByVendor(int vendorId, VOAOptionType optionType, int optionValue)
        {
            StoredProcedureName? procedureName = StoredProcedureName.Create(DeleteOptionValueByVendorSprocName);
            if (!procedureName.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {DeleteOptionValueByVendorSprocName}."));
            }

            foreach (DbConnectionInfo conn in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@VendorId", vendorId),
                    new SqlParameter("@EnabledOptionType", optionType.ToString("D")),
                    new SqlParameter("@EnabledOptionValue", optionValue)
                };

                StoredProcedureHelper.ExecuteNonQuery(conn, procedureName.Value.ToString(), 3, parameters);
            }
        }

        /// <summary>
        /// Retrieves all DISTINCT option values associated with a given vendor.
        /// </summary>
        /// <param name="vendorId">The vendor ID.</param>
        /// <param name="optionType">The option type.</param>
        /// <returns>A list of DISTINCT option values.</returns>
        public static IEnumerable<int> RetrieveDistinctOptionValuesForVendor(int vendorId, VOAOptionType optionType)
        {
            StoredProcedureName? procedureName = StoredProcedureName.Create(RetrieveDistinctOptionValuesForVendorSprocName);
            if (!procedureName.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {RetrieveDistinctOptionValuesForVendorSprocName}."));
            }

            var values = new List<int>();
            foreach (DbConnectionInfo conn in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@VendorId", vendorId),
                    new SqlParameter("@EnabledOptionType", optionType)
                };

                using (var reader = StoredProcedureHelper.ExecuteReader(conn, procedureName.Value.ToString(), parameters))
                {
                    while (reader.Read())
                    {
                        values.Add((int)reader["EnabledOptionValue"]);
                    }
                }
            }

            // There may be duplicate values across databases, so we need to make one final call to Distinct().
            return values.Distinct();
        }

        /// <summary>
        /// Deletes all VOA options associated with a lender service.
        /// </summary>
        /// <param name="brokerId">The broker ID associated with the lender service.</param>
        /// <param name="lenderServiceId">The lender service ID.</param>
        /// <param name="exec">The SQL transaction. This call should always be associated with a <see cref="VOXLenderService"/> deletion.</param>
        public static void DeleteAllOptionsByLenderService(Guid brokerId, int lenderServiceId, CStoredProcedureExec exec)
        {
            StoredProcedureName? procedureName = StoredProcedureName.Create(DeleteAllOptionsByLenderServiceSprocName);
            if (!procedureName.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {DeleteAllOptionsByLenderServiceSprocName}."));
            }

            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LenderServiceId", lenderServiceId)
            };

            exec.ExecuteNonQuery(procedureName.Value.ToString(), 3, parameters);
        }

        /// <summary>
        /// Saves a VOA option configuration.
        /// </summary>
        /// <param name="exec">The SQL transaction.</param>
        /// <param name="errorMessage">Any errors occurring during the process.</param>
        /// <param name="overrideDefaultValidation">
        /// Indicates whether to override validation requiring a default option for each category.
        /// Should only be used for system processes.
        /// </param>
        /// <returns>A boolean indicating whether the save was successful.</returns>
        public bool SaveConfiguration(CStoredProcedureExec exec, out string errorMessage, bool overrideDefaultValidation = false)
        {
            List<VOAOption> optionsToSave = new List<VOAOption>();
            errorMessage = string.Empty;

            if (!this.ValidateOptionSet(this.AccountHistoryOptions_LqbUsers, optionsToSave, out errorMessage, overrideDefaultValidation)
                || !this.ValidateOptionSet(this.AccountHistoryOptions_PmlUsers, optionsToSave, out errorMessage, overrideDefaultValidation)
                || !this.ValidateOptionSet(this.RefreshPeriodOptions_LqbUsers, optionsToSave, out errorMessage, overrideDefaultValidation)
                || !this.ValidateOptionSet(this.RefreshPeriodOptions_PmlUsers, optionsToSave, out errorMessage, overrideDefaultValidation))
            {
                // The error message will have been set in the validation method.
                return false;
            }

            // A service can only be configured to one vendor at a time. Delete any existing configured options
            // (including any associations with a different vendor) before saving the current config.
            DeleteAllOptionsByLenderService(this.brokerId, this.lenderServiceId, exec);

            foreach (var option in optionsToSave)
            {
                SaveOption(option, exec);
            }

            return true;
        }

        /// <summary>
        /// Retrieves options for the given lender service.
        /// </summary>
        /// <param name="brokerId">The broker ID associated with the lender service.</param>
        /// <param name="lenderServiceId">The lender service ID.</param>
        /// <param name="vendorId">The vendor associated with the lender service.</param>
        /// <returns>A collection of options saved for the given lender service.</returns>
        /// <remarks>
        /// The vendor ID is included to ensure that if the associated vendor has been changed, no options
        /// will be retrieved that are incompatible with the current vendor.
        /// </remarks>
        private static IEnumerable<VOAOption> RetrieveOptionsForLenderService(Guid brokerId, int lenderServiceId, int vendorId)
        {
            StoredProcedureName? procedureName = StoredProcedureName.Create(RetrieveOptionsForLenderServiceSprocName);
            if (!procedureName.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {RetrieveOptionsForLenderServiceSprocName}."));
            }

            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LenderServiceId", lenderServiceId),
                new SqlParameter("@VendorId", vendorId)
            };

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);
            var options = new List<VOAOption>();

            using (var connection = DbAccessUtils.GetConnection(brokerId))
            using (var reader = driver.ExecuteReader(connection, null, procedureName.Value, parameters))
            {
                while (reader.Read())
                {
                    options.Add(new VOAOption(reader));
                }
            }

            return options;
        }

        /// <summary>
        /// Saves an option for a given lender service.
        /// </summary>
        /// <param name="option">The option to save.</param>
        /// <param name="exec">The SQL transaction.</param>
        private static void SaveOption(VOAOption option, CStoredProcedureExec exec)
        {
            StoredProcedureName? procedureName = StoredProcedureName.Create(SaveOptionSprocName);
            if (!procedureName.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {SaveOptionSprocName}."));
            }

            exec.ExecuteNonQuery(procedureName.Value.ToString(), 3, option.GetSqlParams());
        }

        /// <summary>
        /// Validates an option set to ensure proper configuration before saving.
        /// </summary>
        /// <param name="optionSet">The option set to validate.</param>
        /// <param name="optionsToSave">A collection of validated options for saving.</param>
        /// <param name="errorMessage">Any errors that occur during the validation.</param>
        /// <param name="overrideDefaultValidation">Indicates whether to override default validation.</param>
        /// <returns>A boolean indicating whether the validation was successful.</returns>
        private bool ValidateOptionSet(List<VOAOption> optionSet, List<VOAOption> optionsToSave, out string errorMessage, bool overrideDefaultValidation)
        {
            errorMessage = string.Empty;

            if (optionSet.Any(o => !o.IsValid))
            {
                errorMessage = "The option configuration is invalid.";
                return false;
            }

            if (!overrideDefaultValidation)
            {
                if (optionSet.Any() && optionSet.Count(o => o.IsDefault) != 1)
                {
                    errorMessage = "VOA option categories must have exactly one default option set.";
                }
            }

            if (optionSet.Count() != optionSet.Select(o => o.OptionValue).Distinct().Count())
            {
                errorMessage = "Duplicate options are not permitted.";
                return false;
            }

            optionsToSave.AddRange(optionSet);
            return true;
        }

        /// <summary>
        /// Gets the default value for the given option set.
        /// </summary>
        /// <param name="optionSet">The option set.</param>
        /// <returns>The default value, or null if none exists.</returns>
        private int? GetDefaultValue(List<VOAOption> optionSet)
        {
            var defaultOption = optionSet.FirstOrDefault(o => o.IsDefault);
            return defaultOption?.OptionValue;
        }
    }
}
