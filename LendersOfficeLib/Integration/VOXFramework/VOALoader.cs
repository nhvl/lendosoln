﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common;
    using DataAccess;
    using LendersOffice.Admin;
    using ObjLib.ServiceCredential;
    using Security;

    /// <summary>
    /// The VOA component loader.
    /// </summary>
    public class VOALoader : VOXLoader
    {
        /// <summary>
        /// Lazy instance of the VOA orders for this loan.
        /// </summary>
        private Lazy<IEnumerable<VOAOrder>> orders;

        /// <summary>
        /// Initializes a new instance of the <see cref="VOALoader"/> class.
        /// </summary>
        /// <param name="voxVendorIdToCredential">Mapping of VOX vendor id to service credentials.</param>
        /// <param name="availableEdocs">The available EDocs.</param>
        /// <param name="lenderServices">The lender services available.</param>
        /// <param name="groups">The groups for checking VOX services.</param>
        /// <param name="loanData">The loan data.</param>
        /// <param name="principal">The principal of the user loading the service.</param>
        internal VOALoader(
            Lazy<Dictionary<int, ServiceCredential>> voxVendorIdToCredential,
            Lazy<HashSet<Guid>> availableEdocs,
            Lazy<Dictionary<int, VOXLenderService>> lenderServices,
            Lazy<Dictionary<Guid, Group>> groups,
            CPageData loanData,
            AbstractUserPrincipal principal) 
            : base(voxVendorIdToCredential, availableEdocs, lenderServices, groups, loanData, principal)
        {
            this.orders = new Lazy<IEnumerable<VOAOrder>>(() => VOAOrder.GetOrdersForLoan(this.BrokerId, this.LoanId));
        }

        /// <summary>
        /// Gets the VOX service type.
        /// </summary>
        protected override VOXServiceT ServiceType
        {
            get
            {
                return VOXServiceT.VOA_VOD;
            }
        }

        /// <summary>
        /// Gets the assets available for use in ordering.
        /// </summary>
        /// <returns>The list of assets and associated properties.</returns>
        public IEnumerable<AssetsToInclude> GetAvailableAssets()
        {
            var assetToMostRecentStatus = this.MapAssetToOrderStatus();

            List<AssetsToInclude> assetsToInclude = new List<AssetsToInclude>();
            for (int i = 0; i < this.LoanData.nApps; i++)
            {
                CAppData app = this.LoanData.GetAppData(i);

                bool borrowerValid = this.IsBorrowerValid(app.aBTypeT, app.aBSsn);
                bool coborrowerValid = this.IsBorrowerValid(app.aCTypeT, app.aCSsn);
                if (!borrowerValid && !coborrowerValid)
                {
                    continue;
                }

                foreach (var asset in app.aAssetCollection.AssetsForVerification)
                {
                    if ((asset.OwnerT == E_AssetOwnerT.Borrower && !borrowerValid) || 
                        (asset.OwnerT == E_AssetOwnerT.CoBorrower && !coborrowerValid))
                    {
                        continue;
                    }

                    AssetsToInclude convertedAsset = new AssetsToInclude()
                    {
                        AccountNum = asset.AccNum.Value,
                        AssetId = asset.RecordId,
                        AssetType = asset.AssetT_rep,
                        Description = asset.Desc,
                        Institution = asset.ComNm,
                        OrderStatus = assetToMostRecentStatus.ContainsKey(asset.RecordId) ? assetToMostRecentStatus[asset.RecordId].ToString() : "None",
                        Owner = this.ResolveAssetOwner(app, asset.OwnerT),
                        Value = asset.Val_rep,
                        AppId = app.aAppId,
                        OwnerT = asset.OwnerT == E_AssetOwnerT.CoBorrower ? E_BorrowerModeT.Coborrower.ToString("D") : E_BorrowerModeT.Borrower.ToString("D")
                    };

                    assetsToInclude.Add(convertedAsset);
                }
            }

            return assetsToInclude;
        }

        /// <summary>
        /// Gets the borrowers available for use in ordering.
        /// </summary>
        /// <returns>The list of elligible borrowers.</returns>
        public IEnumerable<BorrowersToInclude> GetAvailableBorrowers()
        {
            List<BorrowersToInclude> borrowersToInclude = new List<BorrowersToInclude>();
            for (int i = 0; i < this.LoanData.nApps; i++)
            {
                CAppData app = this.LoanData.GetAppData(i);

                bool borrowerValid = this.IsBorrowerValid(app.aBTypeT, app.aBSsn);
                bool coborrowerValid = this.IsBorrowerValid(app.aCTypeT, app.aCSsn);
                if (!borrowerValid && !coborrowerValid)
                {
                    continue;
                }

                Guid appId = app.aAppId;
                if (borrowerValid)
                {
                    borrowersToInclude.Add(new BorrowersToInclude()
                    {
                        AppId = appId,
                        BorrowerType = E_BorrowerModeT.Borrower,
                        Name = app.aBNm
                    });
                }

                if (coborrowerValid)
                {
                    borrowersToInclude.Add(new BorrowersToInclude()
                    {
                        AppId = appId,
                        BorrowerType = E_BorrowerModeT.Coborrower,
                        Name = app.aCNm
                    });
                }
            }

            return borrowersToInclude;
        }

        /// <summary>
        /// Loads the VOA orders for this loan.
        /// </summary>
        /// <returns>The VOA orders for this loan.</returns>
        protected override IEnumerable<AbstractVOXOrder> GetOrders()
        {
            return this.orders.Value.Cast<AbstractVOXOrder>();
        }

        /// <summary>
        /// Gets the name of the borrower depending on asset owner type.
        /// </summary>
        /// <param name="app">The app to pull from.</param>
        /// <param name="owner">The owner type.</param>
        /// <returns>A string getting the correct borrower name.</returns>
        private string ResolveAssetOwner(CAppData app, E_AssetOwnerT owner)
        {
            switch (owner)
            {
                case E_AssetOwnerT.Borrower:
                case E_AssetOwnerT.Joint:
                    return app.aBNm;
                case E_AssetOwnerT.CoBorrower:
                    return app.aCNm;
                default:
                    throw new UnhandledEnumException(owner);
            }
        }

        /// <summary>
        /// Checks if the borrower is valid for VOA orders.
        /// </summary>
        /// <param name="typeT">The type of the borrower.</param>
        /// <param name="ssn">The ssn of the borrower.</param>
        /// <returns>True if valid, false otherwise.</returns>
        private bool IsBorrowerValid(E_aTypeT typeT, string ssn)
        {
            return (typeT == E_aTypeT.Individual || typeT == E_aTypeT.CoSigner) && !string.IsNullOrEmpty(ssn);
        }

        /// <summary>
        /// Maps all assets to the most recent VOA order status that contains said asset.
        /// </summary>
        /// <returns>The mapping from asset to order status.</returns>
        private Dictionary<Guid, VOXOrderStatusT> MapAssetToOrderStatus()
        {
            Dictionary<Guid, VOXOrderStatusT> assetToMostRecentStatus = new Dictionary<Guid, VOXOrderStatusT>();

            foreach (VOAOrder order in this.orders.Value.CoalesceWithEmpty().OrderBy((order) => order.DateOrdered))
            {
                foreach (var asset in order.AssetRecords)
                {
                    if (!assetToMostRecentStatus.ContainsKey(asset.Id))
                    {
                        assetToMostRecentStatus.Add(asset.Id, order.CurrentStatus);
                    }
                }
            }

            return assetToMostRecentStatus;
        }

        /// <summary>
        /// An asset that can be used in VOA orders.
        /// </summary>
        public class AssetsToInclude
        {
            /// <summary>
            /// Gets the key for the asset.
            /// </summary>
            /// <value>The key for the asset.</value>
            public string Key
            {
                get
                {
                    return this.AppId.ToString() + this.OwnerT;
                }
            }

            /// <summary>
            /// Gets or sets the app id.
            /// </summary>
            /// <value>The app id.</value>
            public Guid AppId
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the asset id.
            /// </summary>
            /// <value>The asset id.</value>
            public Guid AssetId
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the institution.
            /// </summary>
            /// <value>The institution.</value>
            public string Institution
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the account number.
            /// </summary>
            /// <value>The account number.</value>
            public string AccountNum
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the owner name.
            /// </summary>
            /// <value>The owner's name.</value>
            public string Owner
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the owner type.
            /// </summary>
            /// <value>The owner type.</value>
            public string OwnerT
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the asset type.
            /// </summary>
            /// <value>The type of asset.</value>
            public string AssetType
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the description.
            /// </summary>
            /// <value>The description.</value>
            public string Description
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the dollar value.
            /// </summary>
            /// <value>The dollar value.</value>
            public string Value
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the order status.
            /// </summary>
            /// <value>The order status.</value>
            public string OrderStatus
            {
                get; set;
            }
        }

        /// <summary>
        /// A borrower that can be used for VOA orders.
        /// </summary>
        public class BorrowersToInclude
        {
            /// <summary>
            /// Gets the suggested key for this borrower. AppId_BorrowerType.
            /// </summary>
            public string Key
            {
                get
                {
                    return $"{this.AppId.ToString("N")}_{this.BorrowerType.ToString("D")}";
                }
            }

            /// <summary>
            /// Gets or sets the name of the borrower.
            /// </summary>
            public string Name
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the app id of the borrower.
            /// </summary>
            public Guid AppId
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the borrower type.
            /// </summary>
            public E_BorrowerModeT BorrowerType
            {
                get;
                set;
            }
        }
    }
}
