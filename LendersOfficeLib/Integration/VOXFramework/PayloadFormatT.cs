﻿namespace LendersOffice.Integration.VOXFramework
{
    /// <summary>
    /// The payload format.
    /// </summary>
    public enum PayloadFormatT
    {
        /// <summary>
        /// MISMO 3.4 with MCL Smart API extensions and namespacing.
        /// </summary>
        MCLSmartApi = 0,

        /// <summary>
        /// MISMO 3.4 with proprietary LQB extensions and namespacing.
        /// </summary>
        LQBMismo = 1,

        /// <summary>
        /// Equifax Verification Services OFX XML.
        /// </summary>
        EquifaxVerificationServicesOfxXml = 2,
    }
}
