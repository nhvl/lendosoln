﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using CommonLib;
    using Security;

    /// <summary>
    /// Abstract class for the request data to pass into the VOX framework.
    /// </summary>
    public abstract class AbstractVOXRequestData
    {
        /// <summary>
        /// Lazy lender service object.
        /// </summary>
        private Lazy<VOXLenderService> lenderService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractVOXRequestData"/> class.
        /// </summary>
        /// <param name="principal">The user placing the request.</param>
        /// <param name="lenderServiceId">The lender service id to be used for this request.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="requestType">The request type.</param>
        protected AbstractVOXRequestData(AbstractUserPrincipal principal, int lenderServiceId, Guid loanId, VOXRequestT requestType)
        {
            this.LenderServiceId = lenderServiceId;
            this.Principal = principal;
            this.BrokerId = principal.BrokerId;
            this.LoanId = loanId;
            this.RequestType = requestType;
            this.lenderService = new Lazy<VOXLenderService>(() => VOXLenderService.GetLenderServiceById(this.BrokerId, this.LenderServiceId));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractVOXRequestData"/> class.
        /// This is to be used for follow up requests on existing orders.
        /// </summary>
        /// <param name="principal">The user placing the request.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="requestType">The request type.</param>
        /// <param name="order">The existing order.</param>
        protected AbstractVOXRequestData(AbstractUserPrincipal principal, Guid loanId, VOXRequestT requestType, AbstractVOXOrder order)
            : this(principal, order.LenderServiceId, loanId, requestType)
        {
            this.PreviousOrder = order;
            this.NotificationEmail = order.NotificationEmail;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractVOXRequestData"/> class by
        /// copying an existing instance.
        /// </summary>
        /// <param name="requestData">The instance to copy.</param>
        /// <param name="overrideRequestType">Can override the request type.</param>
        /// <param name="isSystemGenerated">Can override the system generated indicator.</param>
        protected AbstractVOXRequestData(AbstractVOXRequestData requestData, VOXRequestT? overrideRequestType = null, bool? isSystemGenerated = null)
        {
            this.lenderService = requestData.lenderService;
            this.Principal = requestData.Principal;
            this.LoanId = requestData.LoanId;
            this.RequestType = overrideRequestType ?? requestData.RequestType;
            this.LenderServiceId = requestData.LenderServiceId;
            this.BrokerId = requestData.BrokerId;
            this.NotificationEmail = requestData.NotificationEmail;
            this.AccountId = requestData.AccountId;
            this.Username = requestData.Username;
            this.Password = requestData.Password;
            this.PayWithCreditCard = requestData.PayWithCreditCard;
            this.BillingFirstName = requestData.BillingFirstName;
            this.BillingMiddleName = requestData.BillingMiddleName;
            this.BillingLastName = requestData.BillingLastName;
            this.BillingCardNumber = requestData.BillingCardNumber;
            this.BillingCVV = requestData.BillingCVV;
            this.BillingStreetAddress = requestData.BillingStreetAddress;
            this.BillingCity = requestData.BillingCity;
            this.BillingState = requestData.BillingState;
            this.BillingZipcode = requestData.BillingZipcode;
            this.BillingExpirationMonth = requestData.BillingExpirationMonth;
            this.BillingExpirationYear = requestData.BillingExpirationYear;
            this.BorrowerAuthDocs = requestData.BorrowerAuthDocs;
            this.PreviousOrder = requestData.PreviousOrder;
            this.RequestDate = requestData.RequestDate;
            this.IsSystemGeneratedRequest = isSystemGenerated ?? requestData.IsSystemGeneratedRequest;
            this.VerifiesBorrower = requestData.VerifiesBorrower;
            this.BorrowerAppId = requestData.BorrowerAppId;
            this.BorrowerType = requestData.BorrowerType;
        }

        /// <summary>
        /// Gets the user placing the request.
        /// </summary>
        /// <value>The user placing the request.</value>
        public AbstractUserPrincipal Principal { get; }

        /// <summary>
        /// Gets the borrowers for this request.
        /// Key is the app id, value is whether it is the coborrower or not.
        /// </summary>
        /// <value>Borrowers in this request.</value>
        public abstract IEnumerable<KeyValuePair<Guid, bool>> BorrowersInRequest
        {
            get;
        }

        /// <summary>
        /// Gets the loan id.
        /// </summary>
        /// <value>The loan id.</value>
        public Guid LoanId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the vox request type.
        /// </summary>
        /// <value>The vox request type.</value>
        public VOXRequestT RequestType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the type of service for this request.
        /// </summary>
        /// <value>The service type for this request.</value>
        public abstract VOXServiceT ServiceType
        {
            get;
        }

        /// <summary>
        /// Gets the id of the lender service to use.
        /// </summary>
        /// <value>The lender service id.</value>
        public int LenderServiceId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this order is verifying a borrower rather than records.
        /// </summary>
        /// <value>Whether this order is verifying a borrower rather than records.</value>
        public bool VerifiesBorrower
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the app id of the selected borrower.
        /// </summary>
        /// <remarks>May want to move this to the base class once VOE version is implemented.</remarks>
        public Guid? BorrowerAppId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the borrower type.
        /// </summary>
        /// <remarks>May want to move this to the base class once VOE version is implemented.</remarks>
        public DataAccess.E_BorrowerModeT? BorrowerType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the lender service.
        /// </summary>
        /// <value>The lender service.</value>
        public VOXLenderService LenderService
        {
            get
            {
                return this.lenderService.Value;
            }
        }

        /// <summary>
        /// Gets the broker id.
        /// </summary>
        /// <value>The broker id.</value>
        public Guid BrokerId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the notification email for the request.
        /// </summary>
        /// <value>The notification email for the request.</value>
        public string NotificationEmail
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the account id for the request.
        /// </summary>
        /// <value>The account id for the request.</value>
        public string AccountId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the username for the request.
        /// </summary>
        /// <value>The username for the request.</value>
        public string Username
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the password for the request.
        /// </summary>
        /// <value>The password for the request.</value>
        public string Password
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this request is paid for by credit card.
        /// </summary>
        /// <value>Whether paid by credit card.</value>
        public bool PayWithCreditCard
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the first name for credit card billing.
        /// </summary>
        /// <value>The first name for credit card billing.</value>
        public string BillingFirstName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the middle name for credit card billing.
        /// </summary>
        /// <value>Middle name for credit card billing.</value>
        public string BillingMiddleName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the last name for credit card billing.
        /// </summary>
        /// <value>The last name for credit card billing.</value>
        public string BillingLastName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the card number for billing.
        /// </summary>
        /// <value>The card number for billing.</value>
        public string BillingCardNumber
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the CVV for billing.
        /// </summary>
        /// <value>The CVV for billing.</value>
        public string BillingCVV
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the street address for billing.
        /// </summary>
        /// <value>The street address for billing.</value>
        public string BillingStreetAddress
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the city for billing.
        /// </summary>
        /// <value>The city for billing.</value>
        public string BillingCity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the state for billing.
        /// </summary>
        /// <value>The state for billing.</value>
        public string BillingState
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the zipcode for billing.
        /// </summary>
        /// <value>The zipcode for billing.</value>
        public string BillingZipcode
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the expiration month for billing.
        /// </summary>
        /// <value>The expiration month for billing.</value>
        public int BillingExpirationMonth
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the expiration year for billing.
        /// </summary>
        /// <value>The expiration year for billing.</value>
        public int BillingExpirationYear
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the borrower authentication documents.
        /// </summary>
        /// <value>The borrower authentication documents.</value>
        public IReadOnlyCollection<VOXBorrowerAuthDoc> BorrowerAuthDocs
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the current order data.
        /// </summary>
        /// <value>The current order data.</value>
        public AbstractVOXOrder PreviousOrder
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the date and time the request was placed.
        /// </summary>
        /// <value>The date and time the request was placed.</value>
        public DateTime RequestDate { get; } = DateTime.Now;

        /// <summary>
        /// Gets the service product identifier.
        /// This is actually the TransactionId in PreviousOrder.
        /// If there is no previous order, this is null.
        /// </summary>
        /// <value>The service product identifier.</value>
        public Guid? TransactionId
        {
            get
            {
                return this.PreviousOrder?.TransactionId;
            }
        }

        /// <summary>
        /// Gets the payload format associated with the order.
        /// </summary>
        /// <value>The payload format associated with the order.</value>
        public PayloadFormatT PayloadFormatT
        {
            get
            {
                return this.LenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.PayloadFormatT;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the order is generated by the system.
        /// Some integrations require auto-generated requests. For example, getting VOA results from MCL requires
        /// sending a Refresh and Get request in succession. Usually special handling is needed for these.
        /// </summary>
        /// <value>A boolean indicating whether the order is generated by the system.</value>
        public bool IsSystemGeneratedRequest { get; set; } = false;

        /// <summary>
        /// Validates the request data.
        /// </summary>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if validated, false otherwise.</returns>
        public virtual bool ValidateRequestData(out string errors)
        {
            if (this.LenderServiceId < 1)
            {
                errors = "Invalid service provider chosen.";
                return false;
            }

            if (this.LenderService == null)
            {
                errors = "Service provider not found.";
                return false;
            }

            if (this.PreviousOrder != null &&
                (this.LenderService.AssociatedVendorId != this.PreviousOrder.VendorId ||
                 this.LenderService.AssociatedResellerId != this.PreviousOrder.ProviderId))
            {
                errors = "The service provider for this order has been modified since the intial request.";
                return false;
            }

            StringBuilder errorLog = new StringBuilder();
            string validationErrors;
            if (!this.LenderService.ValidateLenderService(out validationErrors))
            {
                errorLog.AppendLine(validationErrors);
            }

            if (this.LenderService.VendorForProtocolAndTransmissionData.AssociatedPlatform.RequiresAccountId && string.IsNullOrEmpty(this.AccountId))
            {
                errorLog.AppendLine("Account Id required.");
            }

            if (string.IsNullOrEmpty(this.Username) || string.IsNullOrEmpty(this.Password))
            {
                errorLog.AppendLine("Authentication information required.");
            }

            if (this.BorrowerAuthDocs != null)
            {
                foreach (var doc in this.BorrowerAuthDocs)
                {
                    if (doc.AppId == Guid.Empty || doc.DocId == Guid.Empty)
                    {
                        errorLog.AppendLine("Borrower authorization documents not set.");
                        break;
                    }
                }
            }

            if (this.PayWithCreditCard && this.PreviousOrder == null)
            {
                if (this.BillingExpirationMonth < 1)
                {
                    errorLog.AppendLine("Invalid expiration month.");
                }

                if (this.BillingExpirationYear < 1)
                {
                    errorLog.AppendLine("Invalid expiration year.");
                }

                if (this.BillingExpirationYear == DateTime.Now.Year && this.BillingExpirationMonth < DateTime.Now.Month)
                {
                    errorLog.AppendLine("Credit card is expired.");
                }

                SimpleAddress address = null;
                try
                {
                    address = new SimpleAddress(this.BillingStreetAddress, this.BillingCity, this.BillingState, this.BillingZipcode);
                }
                catch (ApplicationException exception)
                {
                    errorLog.AppendLine(exception.Message);
                }

                CreditCard cc = null;
                try
                {
                    cc = new CreditCard(this.BillingFirstName, this.BillingLastName, this.BillingCardNumber, this.BillingExpirationMonth, this.BillingExpirationYear, this.BillingCVV, address);
                }
                catch (ApplicationException ex)
                {
                    errorLog.AppendLine(ex.Message);
                }
            }

            errors = errorLog.ToString();
            return errorLog.Length == 0;
        }
    }
}
