﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Data from response.
    /// </summary>
    public class VOXServerResponseData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOXServerResponseData"/> class.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <param name="headers">The headers in the response.</param>
        public VOXServerResponseData(string content, Dictionary<string, List<string>> headers)
        {
            this.Content = content;
            this.Headers = headers;
        }

        /// <summary>
        /// Gets the content as a string.
        /// </summary>
        /// <value>The content as a string.</value>
        public string Content
        {
            get;
        }

        /// <summary>
        /// Gets the headers included in the web response.
        /// </summary>
        /// <value>The headers included in the web response.</value>
        public Dictionary<string, List<string>> Headers
        {
            get;
        }
    }
}
