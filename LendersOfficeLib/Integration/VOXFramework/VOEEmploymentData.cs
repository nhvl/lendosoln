﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;

    /// <summary>
    /// VOE employment data.
    /// </summary>
    public class VOEEmploymentData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOEEmploymentData"/> class.
        /// </summary>
        /// <param name="appId">The app id.</param>
        /// <param name="employmentRecordId">The employment record id.</param>
        /// <param name="isForCoborrower">Indicates whether the employment record is for a coborrower.</param>
        public VOEEmploymentData(Guid appId, Guid employmentRecordId, bool isForCoborrower)
        {
            this.AppId = appId;
            this.EmploymentRecordId = employmentRecordId;
            this.IsForCoborrower = isForCoborrower;
        }

        /// <summary>
        /// Gets the app id.
        /// </summary>
        /// <value>The app id.</value>
        public Guid AppId { get; private set; }

        /// <summary>
        /// Gets the employment record id.
        /// </summary>
        /// <value>The employment record id.</value>
        public Guid EmploymentRecordId { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the employment record is for a coborrower.
        /// </summary>
        /// <value>Indicates whether the employment record is for a coborrower.</value>
        public bool IsForCoborrower { get; private set; }
    }
}
