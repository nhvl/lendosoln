﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using ObjLib.ServiceCredential;
    using Security;

    /// <summary>
    /// Request data for SSA-89 services.
    /// </summary>
    public class SSA89RequestData : AbstractVOXRequestData
    {
        /// <summary>
        /// Whether this is the retrieve portion of a GET request.
        /// </summary>
        private bool isRetrieveGetRequest;

        /// <summary>
        /// Initializes a new instance of the <see cref="SSA89RequestData"/> class.
        /// Meant for initial requests.
        /// </summary>
        /// <param name="principal">The user placing the request.</param>
        /// <param name="lenderServiceId">The lender service id.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="requestType">The request type.</param>
        public SSA89RequestData(AbstractUserPrincipal principal, int lenderServiceId, Guid loanId, VOXRequestT requestType)
            : base(principal, lenderServiceId, loanId, requestType)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SSA89RequestData"/> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        /// <param name="overrideRequestType">The request type if needed to override.</param>
        /// <param name="isSystemGenerated">Whether this new request data is system generated.</param>
        /// <param name="isRetrieveGetRequest">Whether this is for a retrieve request.</param>
        public SSA89RequestData(SSA89RequestData requestData, VOXRequestT? overrideRequestType = null, bool? isSystemGenerated = null, bool? isRetrieveGetRequest = null)
            : base(requestData, overrideRequestType, isSystemGenerated)
        {
            this.AppIdForChosenBorrower = requestData.AppIdForChosenBorrower;
            this.IsChosenBorrowerACoborrower = requestData.IsChosenBorrowerACoborrower;
            this.UsedServiceCredential = requestData.UsedServiceCredential;
            this.IsRetrieveGetRequest = isRetrieveGetRequest ?? requestData.IsRetrieveGetRequest;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SSA89RequestData"/> class.
        /// This is to be used for follow up requests with existing orders.
        /// </summary>
        /// <param name="principal">The user placing the request.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="requestType">The request type.</param>
        /// <param name="voaOrder">The existing order to follow up.</param>
        public SSA89RequestData(AbstractUserPrincipal principal, Guid loanId, VOXRequestT requestType, SSA89Order voaOrder)
            : base(principal, loanId, requestType, voaOrder)
        {
        }

        /// <summary>
        /// Gets the borrowers involved in this request.
        /// </summary>
        /// <value>The borrowers involved in this request.</value>
        public override IEnumerable<KeyValuePair<Guid, bool>> BorrowersInRequest
        {
            get
            {
                return new List<KeyValuePair<Guid, bool>>()
                {
                    new KeyValuePair<Guid, bool>(this.AppIdForChosenBorrower, this.IsChosenBorrowerACoborrower)
                };
            }
        }

        /// <summary>
        /// Gets the service type.
        /// </summary>
        /// <value>The service type.</value>
        public override VOXServiceT ServiceType
        {
            get
            {
                return VOXServiceT.SSA_89;
            }
        }

        /// <summary>
        /// Gets or sets the app id for the chosen borrower.
        /// </summary>
        /// <value>The app id for the chosen borrower.</value>
        public Guid AppIdForChosenBorrower
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the chosen borrower is actually a coborrower.
        /// </summary>
        /// <value>A value indicating whether the chosen borrower is actually a coborrower.</value>
        public bool IsChosenBorrowerACoborrower
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the service credential used. Needed to determine some element in Equifax OFX payload format.
        /// </summary>
        /// <value>The service credential.</value>
        public ServiceCredential UsedServiceCredential
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is the retrieve portion of the GET request.
        /// </summary>
        /// <value>Whether this is the retrieve portion of the GET request.</value>
        public bool IsRetrieveGetRequest
        {
            get
            {
                if (this.RequestType != VOXRequestT.Get)
                {
                    return false;
                }
                else
                {
                    return this.isRetrieveGetRequest;
                }
            }

            set
            {
                this.isRetrieveGetRequest = value;
            }
        }
    }
}
