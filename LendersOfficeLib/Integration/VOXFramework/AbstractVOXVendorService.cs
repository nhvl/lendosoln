﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using DataAccess;

    /// <summary>
    /// Interface for the services that a vendor will provide.
    /// </summary>
    public abstract class AbstractVOXVendorService
    {
        /// <summary>
        /// Gets or sets the id for this vendor level service.
        /// </summary>
        /// <value>The id for this vendor level service.</value>
        public int VendorServiceId
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the type of service this is.
        /// </summary>
        /// <value>The type of service this is.</value>
        public abstract VOXServiceT ServiceType
        {
            get;
        }

        /// <summary>
        /// Gets or sets the vendor this service belongs to.
        /// </summary>
        /// <value>The vendor this service belongs to.</value>
        public int VendorInfoId
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this service is enabled.
        /// </summary>
        /// <value>Whether this service is enabled.</value>
        public bool IsEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is a direct vendor.
        /// </summary>
        /// <value>Whether this is a direct vendor.</value>
        public bool IsADirectVendor
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this sells via resellers.
        /// Note: Reseller and Provider are interchangeable.
        /// </summary>
        /// <value>Whether this vendor sells via resellers.</value>
        public bool SellsViaResellers
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is a reseller.
        /// Note: Reseller and Provider are interchangeable.
        /// </summary>
        /// <value>Whether this is a reseller.</value>
        public bool IsAReseller
        {
            get;
            set;
        }
        
        /// <summary>
        /// Gets the post save actions.
        /// </summary>
        /// <value>Any post save actions to run.</value>
        protected List<Action> PostSaveActions { get; } = new List<Action>();

        /// <summary>
        /// Creates a view model out of this vendor service.
        /// </summary>
        /// <returns>The view model.</returns>
        internal abstract AbstractVOXVendorServiceViewModel ToViewModel();

        /// <summary>
        /// Saves this vendor service to the DB.
        /// </summary>
        /// <param name="vendorId">The id of the associated vendor.</param>
        /// <param name="exec">The transaction.</param>
        internal abstract void SaveToDb(int vendorId, CStoredProcedureExec exec);

        /// <summary>
        /// Loads the service from a data reader.
        /// </summary>
        /// <param name="reader">The reader to load from.</param>
        internal abstract void PopulateFromReader(IDataReader reader);

        /// <summary>
        /// Populates the vendor service from a view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if successfully made. False otherwise.</returns>
        internal abstract bool PopulateFromViewModel(VOXVendorViewModel viewModel, out string errors);

        /// <summary>
        /// Creates the vendor service from the view model. This assumes that this current service is a new one.
        /// </summary>
        /// <param name="viewModel">The service view model.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if made, false otherwise.</returns>
        internal abstract bool CreateFromViewModel(AbstractVOXVendorServiceViewModel viewModel, out string errors);

        /// <summary>
        /// Post save action for VOA service saving.
        /// </summary>
        internal void RunPostSaveActions()
        {
            foreach (var action in this.PostSaveActions)
            {
                action();
            }
        }
    }
}
