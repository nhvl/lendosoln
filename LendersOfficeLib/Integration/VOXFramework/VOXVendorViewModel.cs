﻿namespace LendersOffice.Integration.VOXFramework
{
    using System.Collections.Generic;
    using FrameworkCommon;
    using LendersOffice.Common;

    /// <summary>
    /// Verification vendor view model.
    /// </summary>
    public class VOXVendorViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOXVendorViewModel"/> class.
        /// </summary>
        public VOXVendorViewModel()
        {
            this.TransmissionInfo = new IntegrationTransmissionInfoViewModel();
        }

        /// <summary>
        /// Gets or sets the vendor id.
        /// </summary>
        /// <value>The vendor id.</value>
        public int? VendorId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the company name.
        /// </summary>
        /// <value>The company name.</value>
        public string CompanyName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this vendor is enabled.
        /// </summary>
        /// <value>Whether this vendor is enabled.</value>
        public bool IsEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this vendor is a test vendor.
        /// </summary>
        /// <value>Whether this is a test vendor.</value>
        public bool IsTestVendor
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this vendor uses credit cards.
        /// </summary>
        /// <value>Whether this vendor uses credit cards.</value>
        public bool CanPayWithCreditCard
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the du validation service provider id.
        /// </summary>
        /// <value>Du service provider id.</value>
        public int? DuValidationServiceProviderId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the associated platform id.
        /// </summary>
        /// <value>The associated platform id.</value>
        public int AssociatedPlatformId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the platform info is overriden.
        /// </summary>
        /// <value>Whether the platform info is overriden.</value>
        public bool IsOverridingPlatform
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the MCL CRA id for this vendor.
        /// </summary>
        /// <value>The MCL CRA id for this vendor.</value>
        public string MclCraId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the transmission info for this vendor.
        /// </summary>
        /// <value>The transmission info for this vendor.</value>
        public IntegrationTransmissionInfoViewModel TransmissionInfo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the voa service.
        /// </summary>
        /// <value>The VOA service.</value>
        public VOAVendorServiceViewModel VOAService
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the VOE service.
        /// </summary>
        /// <value>The VOE service.</value>
        public VOEVendorServiceViewModel VOEService
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the SSA-89 service.
        /// </summary>
        /// <value>The SSA-89 service.</value>
        public SSA89VendorServiceViewModel SSA89Service { get; set; }

        /// <summary>
        /// Gets all non null services in this view model.
        /// </summary>
        /// <returns>List of all non null services.</returns>
        public IEnumerable<AbstractVOXVendorServiceViewModel> GetAllServices()
        {
            List<AbstractVOXVendorServiceViewModel> viewModels = new List<AbstractVOXVendorServiceViewModel>();
            viewModels.AddIfNotNull(this.VOAService);
            viewModels.AddIfNotNull(this.VOEService);
            viewModels.AddIfNotNull(this.SSA89Service);
            return viewModels;
        }
    }
}
