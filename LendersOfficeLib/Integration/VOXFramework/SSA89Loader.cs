﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common;
    using DataAccess;
    using LendersOffice.Admin;
    using ObjLib.ServiceCredential;
    using Security;

    /// <summary>
    /// The SSA-89 component loader.
    /// </summary>
    public class SSA89Loader : VOXLoader
    {
        /// <summary>
        /// Lazy instance for loading the SSA-89 orders.
        /// </summary>
        private Lazy<IEnumerable<SSA89Order>> orders;

        /// <summary>
        /// Initializes a new instance of the <see cref="SSA89Loader"/> class.
        /// </summary>
        /// <param name="voxVendorIdToCredential">Mapping of VOX vendor id to service credentials.</param>
        /// <param name="availableEdocs">The available EDocs.</param>
        /// <param name="lenderServices">The lender services available.</param>
        /// <param name="groups">The groups for checking VOX services.</param>
        /// <param name="loanData">The loan data.</param>
        /// <param name="principal">The principal of the user loading the service.</param>
        internal SSA89Loader(
            Lazy<Dictionary<int, ServiceCredential>> voxVendorIdToCredential,
            Lazy<HashSet<Guid>> availableEdocs,
            Lazy<Dictionary<int, VOXLenderService>> lenderServices,
            Lazy<Dictionary<Guid, Group>> groups,
            CPageData loanData,
            AbstractUserPrincipal principal)
            : base(voxVendorIdToCredential, availableEdocs, lenderServices, groups, loanData, principal)
        {
            this.orders = new Lazy<IEnumerable<SSA89Order>>(() => SSA89Order.GetOrdersForLoan(this.BrokerId, this.LoanId));
        }

        /// <summary>
        /// Gets the VOX service type.
        /// </summary>
        protected override VOXServiceT ServiceType
        {
            get { return VOXServiceT.SSA_89; }
        }

        /// <summary>
        /// Gets the borrowers that can be used for SSA-89 ordering.
        /// </summary>
        /// <returns>The borrowers that can be used for SSA-89 ordering.</returns>
        public IEnumerable<BorrowersToInclude> GetBorrowers()
        {
            var borrowerToMostRecentStatus = this.MapBorrowerToMostRecentStatus();
            List<BorrowersToInclude> borrowers = new List<BorrowersToInclude>();
            for (int i = 0; i < this.LoanData.nApps; i++)
            {
                CAppData appData = this.LoanData.GetAppData(i);
                var borrowerKey = $"{appData.aAppId}|{false.ToString()}";
                BorrowersToInclude borrower = new BorrowersToInclude()
                {
                    BorrowerName = appData.aBNm,
                    DateOfBirth = appData.aBDob_rep,
                    OrderStatus = borrowerToMostRecentStatus.ContainsKey(borrowerKey) ? borrowerToMostRecentStatus[borrowerKey] : "None",
                    Ssn = appData.aBSsn,
                    AppId = appData.aAppId,
                    BorrowerType = E_BorrowerModeT.Borrower.ToString("D")
                };

                borrowers.Add(borrower);
                if (appData.aHasSpouse)
                {
                    var coborrowerKey = $"{appData.aAppId}|{true.ToString()}";
                    BorrowersToInclude coborrower = new BorrowersToInclude()
                    {
                        BorrowerName = appData.aCNm,
                        DateOfBirth = appData.aCDob_rep,
                        OrderStatus = borrowerToMostRecentStatus.ContainsKey(coborrowerKey) ? borrowerToMostRecentStatus[coborrowerKey] : "None",
                        Ssn = appData.aCSsn,
                        AppId = appData.aAppId,
                        BorrowerType = E_BorrowerModeT.Coborrower.ToString("D")
                    };

                    borrowers.Add(coborrower);
                }
            }

            return borrowers;
        }

        /// <summary>
        /// Gets the SSA-89 orders for this loan.
        /// </summary>
        /// <returns>The SSA-89 orders for this loan.</returns>
        protected override IEnumerable<AbstractVOXOrder> GetOrders()
        {
            return this.orders.Value.Cast<AbstractVOXOrder>();
        }

        /// <summary>
        /// Maps borrowers to the status of the most recent SSA-89 ordering containing that borrower.
        /// </summary>
        /// <returns>The mapping from borrower to associated order status.</returns>
        private Dictionary<string, string> MapBorrowerToMostRecentStatus()
        {
            Dictionary<string, string> borrowerToMostRecentStatus = new Dictionary<string, string>();
            foreach (var order in this.orders.Value.CoalesceWithEmpty().OrderBy((order) => order.DateOrdered))
            {
                var key = $"{order.ApplicationId}|{order.IsForCoborrower.ToString()}";
                if (!borrowerToMostRecentStatus.ContainsKey(key))
                {
                    borrowerToMostRecentStatus.Add(key, AbstractVOXOrderViewModel.GetOrderStatusString(order.CurrentStatus));
                }
            }

            return borrowerToMostRecentStatus;
        }

        /// <summary>
        /// Class to contain info for borrowers to be selected.
        /// </summary>
        public class BorrowersToInclude
        {
            /// <summary>
            /// Gets or sets the app id.
            /// </summary>
            /// <value>The app id.</value>
            public Guid AppId
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the borrower type. This should be the E_BorrowerModeT enum as an int string.
            /// </summary>
            /// <value>The borrower type.</value>
            public string BorrowerType
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the borrower name.
            /// </summary>
            /// <value>The borrower name.</value>
            public string BorrowerName
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the ssn.
            /// </summary>
            /// <value>The ssn of the borrower.</value>
            public string Ssn
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the date of birth.
            /// </summary>
            /// <value>The date of birth.</value>
            public string DateOfBirth
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the order status.
            /// </summary>
            /// <value>The order status.</value>
            public string OrderStatus
            {
                get; set;
            }
        }
    }
}
