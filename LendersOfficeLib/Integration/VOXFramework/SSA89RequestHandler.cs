﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ConfigSystem.Operations;
    using Conversions.VOXFramework;
    using Conversions.VOXFramework.Mismo;
    using DataAccess;
    using LendersOffice.Conversions.VOXFramework.SSA89;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using ObjLib.Conversions.VOXFramework.SSA89;

    /// <summary>
    /// SSA-89 request handler.
    /// </summary>
    public class SSA89RequestHandler : AbstractVOXRequestHandler<SSA89Order, SSA89RequestData>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SSA89RequestHandler"/> class.
        /// </summary>
        /// <param name="requestData">The data for the request.</param>
        public SSA89RequestHandler(SSA89RequestData requestData)
            : base(requestData)
        {
        }

        /// <summary>
        /// Gets the document type to save the document as.
        /// </summary>
        /// <value>The document type to save the document as.</value>
        protected override DataAccess.E_AutoSavePage AutoSaveDocType => DataAccess.E_AutoSavePage.SSA89Documents;

        /// <summary>
        /// Gets the workflow operation required to submit a request.
        /// </summary>
        /// <value>The workflow operation required to submit a request.</value>
        protected override WorkflowOperation RequiredOperation => WorkflowOperations.OrderSSA89;
       
        /// <summary>
        /// Gets the request provider for the Equifax OFX payload type.
        /// </summary>
        /// <returns>The request provider for Equifax OFX payload type.</returns>
        protected override IVOXRequestProvider CreateEquifaxOfxRequestProvider()
        {
            return new EquifaxOfxSSA89RequestProvider(this.RequestData, this.IndividualServiceOrdersToRequest.FirstOrDefault());
        }

        /// <summary>
        /// Generates a response provider from the vendor's response string.
        /// </summary>
        /// <param name="responseData">The response from the vendor.</param>
        /// <returns>The response provider of the request.</returns>
        protected override AbstractVOXResponseProvider GenerateResponseProvider(VOXServerResponseData responseData)
        {
            var format = this.RequestData.PayloadFormatT;
            switch (format)
            {
                case PayloadFormatT.EquifaxVerificationServicesOfxXml:
                    var responseProvider = new EquifaxOfxSSA89ResponseProvider(responseData, this.RequestData, this.IndividualServiceOrdersToRequest.FirstOrDefault());
                    return this.Equifax_ShouldSendRetrieveRequest(responseProvider) ? this.SendRetrieveRequest(responseProvider) : responseProvider;
                case PayloadFormatT.LQBMismo:
                case PayloadFormatT.MCLSmartApi:
                    throw new UnhandledCaseException(format.ToString(), "SSA89 does not support this payload format.");
                default:
                    throw new UnhandledEnumException(format);
            }
        }

        /// <summary>
        /// Creates an instance of the MCL request provider for this instance.
        /// </summary>
        /// <returns>The MCL request provider for this instance.</returns>
        protected override AbstractMismoVOXRequestProvider CreateMismoRequestProvider()
        {
            // Mismo payload formats are not yet implemented for SSA89.
            throw new DeveloperException(ErrorMessage.BadConfiguration);
        }

        /// <summary>
        /// Creates an instance of the specified order in the database and in memory.
        /// </summary>
        /// <param name="orderRequest">The request of the order metadata that was sent.</param>
        /// <param name="serviceResponse">The response to the order.</param>
        /// <returns>An order object with a corresponding database entry.</returns>
        protected override SSA89Order CreateOrder(VOXRequestOrder orderRequest, VOXServiceResponse serviceResponse)
        {
            return SSA89Order.CreateOrder(this.RequestData, orderRequest, (SSA89ServiceResponse)serviceResponse);
        }

        /// <summary>
        /// Splits the request data into the orders that will be sent to the vendor.
        /// </summary>
        /// <returns>The orders that will be sent to the vendor.</returns>
        protected override IReadOnlyCollection<VOXRequestOrder> CreateServiceOrdersFromRequestData()
        {
            switch (this.RequestData.RequestType)
            {
                case VOXRequestT.Initial:
                    return this.GenerateInitialOrder();
                case VOXRequestT.Get:
                    return this.GenerateGetOrder();
                case VOXRequestT.Refresh:
                    return this.GenerateRefreshOrder();
                default:
                    throw new UnhandledEnumException(this.RequestData.RequestType);
            }
        }

        /// <summary>
        /// Checks if needs to send a RETRIEVE request for Equifax.
        /// </summary>
        /// <param name="responseProvider">The response from the STATUS check request.</param>
        /// <returns>True if needs to run a RETRIEVE. False otherwise.</returns>
        private bool Equifax_ShouldSendRetrieveRequest(EquifaxOfxSSA89ResponseProvider responseProvider)
        {
            if (this.RequestData.RequestType != VOXRequestT.Get || this.RequestData.IsRetrieveGetRequest || responseProvider.HasErrors)
            {
                return false;
            }

            SSA89ServiceResponse serviceResponse = (SSA89ServiceResponse)responseProvider.ServiceResponses.FirstOrDefault();
            if (serviceResponse == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext("No service response."));
            }

            return serviceResponse.CurrentStatus != VOXOrderStatusT.Error &&
                   (this.Equifax_SubproductReady(serviceResponse.DmStatus) ||
                    this.Equifax_SubproductReady(serviceResponse.SsaStatus) ||
                    this.Equifax_SubproductReady(serviceResponse.OfacStatus) ||
                    this.Equifax_SubproductReady(serviceResponse.ChStatus));
        }

        /// <summary>
        /// Checks if the subproduct needs a RETRIEVE request.
        /// </summary>
        /// <param name="status">The status to check.</param>
        /// <returns>True if needs a retrieve request. False otherwise.</returns>
        private bool Equifax_SubproductReady(SSA89SubproductStatus status)
        {
            return status != SSA89SubproductStatus.Error && status != SSA89SubproductStatus.NotOrdered && status != SSA89SubproductStatus.Pending;
        }

        /// <summary>
        /// Send the retrieve request.
        /// </summary>
        /// <param name="getResponse">The response from the STATUS check request.</param>
        /// <returns>The response from the RETRIEVE request.</returns>
        private EquifaxOfxSSA89ResponseProvider SendRetrieveRequest(EquifaxOfxSSA89ResponseProvider getResponse)
        {
            var retrieveRequestData = new SSA89RequestData(this.RequestData, null, true, true);
            var retrieveRequestHandler = new SSA89RequestHandler(retrieveRequestData);
            retrieveRequestHandler.SubmitRequest();

            return retrieveRequestHandler.ResponseData as EquifaxOfxSSA89ResponseProvider;
        }

        /// <summary>
        /// Generates a request order for INITIAL requests.
        /// </summary>
        /// <returns>The order wrapped in a collection.</returns>
        private IReadOnlyCollection<VOXRequestOrder> GenerateInitialOrder()
        {
            var order = VOXRequestOrder.ForInitialRequest(
                                this.RequestData.AppIdForChosenBorrower,
                                this.RequestData.IsChosenBorrowerACoborrower,
                                new List<Guid>());

            return new List<VOXRequestOrder>()
            {
                order
            };
        }

        /// <summary>
        /// Generates a request order for GET requests.
        /// </summary>
        /// <returns>A list of VOX request orders even though there's only one of them.</returns>
        private IReadOnlyCollection<VOXRequestOrder> GenerateGetOrder()
        {
            var previousOrder = (SSA89Order)this.RequestData.PreviousOrder;
            return new List<VOXRequestOrder>()
            {
                VOXRequestOrder.ForGetRequest(previousOrder, new List<Guid>(), previousOrder.IsForCoborrower)
            };
        }

        /// <summary>
        /// Generates a request order for SSA89 Refresh orders.
        /// </summary>
        /// <returns>The collection of VOX request orders. For SSA89, this only returns a single item.</returns>
        private IReadOnlyCollection<VOXRequestOrder> GenerateRefreshOrder()
        {
            var previousOrder = (SSA89Order)this.RequestData.PreviousOrder;
            return new List<VOXRequestOrder>()
            {
                VOXRequestOrder.ForRefreshRequest(previousOrder, new List<Guid>())
            };
        }
    }
}
