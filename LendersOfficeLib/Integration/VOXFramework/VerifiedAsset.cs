﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using Common;
    using DataAccess;
    using LqbGrammar.DataTypes;
    using Mismo3Specification.Version4Schema;

    /// <summary>
    /// Holds information returned from a VOA vendor about an asset.
    /// </summary>
    public class VerifiedAsset
    {
        /// <summary>
        /// The encryption key identifier for decrypting encrypted values of this class.
        /// </summary>
        private EncryptionKeyIdentifier encryptionKeyId;

        /// <summary>
        /// The encrypted bytes of the account number.
        /// </summary>
        private byte[] encryptedAccountNumberBytes;

        /// <summary>
        /// The lazy instance for decrypting the account number on-demand.
        /// </summary>
        private Lazy<string> lazyAccountNumber;

        /// <summary>
        /// Initializes a new instance of the <see cref="VerifiedAsset"/> class from MISMO.
        /// </summary>
        /// <param name="accountNumber">The account number of the asset.</param>
        /// <param name="accountBalance">The account balance of the asset.</param>
        /// <param name="financialInstitution">The financial institution of the asset.</param>
        /// <param name="accountTypeMismo">The account type, from MISMO enum types.</param>
        public VerifiedAsset(string accountNumber, decimal accountBalance, string financialInstitution, AssetBase accountTypeMismo)
        {
            this.AccountNumber = accountNumber;
            this.AccountBalance = accountBalance;
            this.FinancialInstitutionName = financialInstitution;
            this.SetAssetType(accountTypeMismo);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VerifiedAsset"/> class from a database record.
        /// </summary>
        /// <param name="assetRecord">The database record to load from.</param>
        private VerifiedAsset(IReadOnlyDictionary<string, object> assetRecord)
        {
            this.encryptionKeyId = EncryptionKeyIdentifier.Create((Guid)assetRecord["EncryptionKeyId"]).ForceValue();
            this.encryptedAccountNumberBytes = (byte[])assetRecord["EncryptedAccountNumber"];
            this.lazyAccountNumber = new Lazy<string>(() => Drivers.Encryption.EncryptionHelper.DecryptString(this.encryptionKeyId, this.encryptedAccountNumberBytes));
            this.AccountBalance = (decimal)assetRecord["AccountBalance"];
            this.FinancialInstitutionName = (string)assetRecord["FinancialInstitutionName"];
            this.AssetType = (E_AssetT)assetRecord["AssetType"];
        }

        /// <summary>
        /// Gets a map from MISMO asset types to LQB asset types.
        /// </summary>
        public static IReadOnlyDictionary<AssetBase, E_AssetT> AssetTypeMap { get; } = new Dictionary<AssetBase, E_AssetT>
        {
            { AssetBase.Annuity, E_AssetT.OtherLiquidAsset },
            { AssetBase.Automobile, E_AssetT.Auto },
            { AssetBase.Blank, E_AssetT.OtherLiquidAsset },
            { AssetBase.Boat, E_AssetT.OtherIlliquidAsset },
            { AssetBase.Bond, E_AssetT.Bonds },
            { AssetBase.BorrowerEstimatedTotalAssets, E_AssetT.OtherLiquidAsset },
            { AssetBase.BorrowerPrimaryHome, E_AssetT.OtherIlliquidAsset },
            { AssetBase.BridgeLoanNotDeposited, E_AssetT.BridgeLoanNotDeposited },
            { AssetBase.CashOnHand, E_AssetT.OtherLiquidAsset },
            { AssetBase.CertificateOfDepositTimeDeposit, E_AssetT.CertificateOfDeposit },
            { AssetBase.CheckingAccount, E_AssetT.Checking },
            { AssetBase.EarnestMoneyCashDepositTowardPurchase, E_AssetT.OtherLiquidAsset },
            { AssetBase.EmployerAssistance, E_AssetT.OtherLiquidAsset },
            { AssetBase.GiftOfCash, E_AssetT.GiftFunds },
            { AssetBase.GiftOfPropertyEquity, E_AssetT.GiftEquity },
            { AssetBase.GiftsTotal, E_AssetT.GiftFunds },
            { AssetBase.Grant, E_AssetT.OtherLiquidAsset },
            { AssetBase.IndividualDevelopmentAccount, E_AssetT.OtherLiquidAsset },
            { AssetBase.LifeInsurance, E_AssetT.LifeInsurance },
            { AssetBase.MoneyMarketFund, E_AssetT.MoneyMarketFund },
            { AssetBase.MutualFund, E_AssetT.MutualFunds },
            { AssetBase.NetWorthOfBusinessOwned, E_AssetT.Business },
            { AssetBase.Other, E_AssetT.OtherLiquidAsset },
            { AssetBase.PendingNetSaleProceedsFromRealEstateAssets, E_AssetT.PendingNetSaleProceedsFromRealEstateAssets },
            { AssetBase.ProceedsFromSaleOfNonRealEstateAsset, E_AssetT.OtherLiquidAsset },
            { AssetBase.ProceedsFromSecuredLoan, E_AssetT.OtherLiquidAsset },
            { AssetBase.ProceedsFromUnsecuredLoan, E_AssetT.OtherLiquidAsset },
            { AssetBase.RealEstateOwned, E_AssetT.OtherIlliquidAsset },
            { AssetBase.RecreationalVehicle, E_AssetT.OtherIlliquidAsset },
            { AssetBase.RelocationMoney, E_AssetT.OtherLiquidAsset },
            { AssetBase.RetirementFund, E_AssetT.Retirement },
            { AssetBase.SaleOtherAssets, E_AssetT.OtherIlliquidAsset },
            { AssetBase.SavingsAccount, E_AssetT.Savings },
            { AssetBase.SavingsBond, E_AssetT.Bonds },
            { AssetBase.SeverancePackage, E_AssetT.OtherLiquidAsset },
            { AssetBase.Stock, E_AssetT.Stocks },
            { AssetBase.StockOptions, E_AssetT.Stocks },
            { AssetBase.TrustAccount, E_AssetT.TrustFunds }
        };

        /// <summary>
        /// Gets or sets the account number of the asset.
        /// </summary>
        public string AccountNumber
        {
            get
            {
                return this.lazyAccountNumber.Value;
            }

            set
            {
                this.lazyAccountNumber = new Lazy<string>(() => value);
            }
        }

        /// <summary>
        /// Gets or sets the account balance of the asset.
        /// </summary>
        public decimal AccountBalance { get; set; }

        /// <summary>
        /// Gets or sets the financial institution name of the asset, which will be returned as the asset description.
        /// </summary>
        public string FinancialInstitutionName { get; set; }

        /// <summary>
        /// Gets or sets the type of asset.
        /// </summary>
        public E_AssetT? AssetType { get; set; }

        /// <summary>
        /// Creates a new instance of the <see cref="VerifiedAsset"/> class from a database row.
        /// </summary>
        /// <param name="databaseRecord">The database record to initialize from, represented as a dictionary.</param>
        /// <returns>A new verified asset object created from the given database record.</returns>
        public static VerifiedAsset FromDatabase(IReadOnlyDictionary<string, object> databaseRecord)
        {
            return new VerifiedAsset(databaseRecord);
        }

        /// <summary>
        /// Sets the MISMO asset type.
        /// </summary>
        /// <param name="value">The MISMO asset type to set.</param>
        /// <returns>The new LQB asset type that was set.</returns>
        public E_AssetT? SetAssetType(AssetBase value)
        {
            this.AssetType = AssetTypeMap.GetNullableValue(value);
            return this.AssetType;
        }
    }
}
