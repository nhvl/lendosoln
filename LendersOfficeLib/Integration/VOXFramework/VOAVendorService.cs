﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;

    /// <summary>
    /// A VOA vendor service.
    /// </summary>
    public class VOAVendorService : AbstractVOXVendorService
    {
        /// <summary>
        /// The max value for a VOA option.
        /// </summary>
        public const int MaxOption = 999;

        /// <summary>
        /// The min value for a VOA option.
        /// </summary>
        public const int MinOption = 0;

        /// <summary>
        /// Stored procedure for creating a new VOA service.
        /// </summary>
        private const string CreateVOAService = "CreateVOAService";

        /// <summary>
        /// Stored procedure for updating a new VOA service.
        /// </summary>
        private const string UpdateVOAService = "UpdateVOAService";

        /// <summary>
        /// The options for this VOA service.
        /// </summary>
        private Lazy<Options> options = null;

        /// <summary>
        /// The file db key for the JSON file that holds the options for this service.
        /// </summary>
        private Guid optionsFileDbKey;

        /// <summary>
        /// Initializes a new instance of the <see cref="VOAVendorService"/> class.
        /// </summary>
        internal VOAVendorService()
        {
        }

        /// <summary>
        /// Gets the type of service.
        /// </summary>
        /// <value>The type of service.</value>
        public override VOXServiceT ServiceType
        {
            get
            {
                return VOXServiceT.VOA_VOD;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to ask for a notification email.
        /// </summary>
        /// <value>Whether to ask for a notification email.</value>
        public bool AskForNotificationEmail
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets what this service verifies.
        /// </summary>
        /// <value>What this service verifies.</value>
        public VOAVerificationT VoaVerificationT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the refresh period options.
        /// </summary>
        /// <value>The refresh period options.</value>
        public List<int> RefreshPeriodOptions
        {
            get
            {
                return this.options.Value.RefreshPeriodOptions;
            }
        }

        /// <summary>
        /// Gets the account history options.
        /// </summary>
        /// <value>The account history options.</value>
        public List<int> AccountHistoryOptions
        {
            get
            {
                return this.options.Value.AccountHistoryOptions;
            }
        }

        /// <summary>
        /// Saves this service to the DB.
        /// </summary>
        /// <param name="vendorId">The id of the associated vendor.</param>
        /// <param name="exec">The transaction.</param>
        internal override void SaveToDb(int vendorId, CStoredProcedureExec exec)
        {
            this.VendorInfoId = vendorId;
            Guid oldKey = this.optionsFileDbKey;
            this.optionsFileDbKey = Guid.NewGuid();
            FileDBTools.WriteJsonNetObject(E_FileDB.Normal, this.GetOptionsFileDbKey(), this.options.Value);

            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@ServiceT", this.ServiceType),
                new SqlParameter("@IsADirectVendor", this.IsADirectVendor),
                new SqlParameter("@SellsViaResellers ", this.SellsViaResellers),
                new SqlParameter("@IsAReseller", this.IsAReseller),
                new SqlParameter("@AskForNotificationEmail", this.AskForNotificationEmail),
                new SqlParameter("@VoaVerificationT", this.VoaVerificationT),
                new SqlParameter("@VendorId", this.VendorInfoId),
                new SqlParameter("@OptionsFileDbKey", this.optionsFileDbKey),
                new SqlParameter("@IsEnabled", this.IsEnabled)
            };

            SqlParameter vendorServiceIdOutput = null;
            string sp = null;
            var vendorServiceIsNew = this.VendorServiceId < 0;
            if (vendorServiceIsNew)
            {
                vendorServiceIdOutput = new SqlParameter("@VendorServiceId", System.Data.SqlDbType.Int);
                vendorServiceIdOutput.Direction = System.Data.ParameterDirection.Output;
                parameters.Add(vendorServiceIdOutput);

                sp = CreateVOAService;
            }
            else
            {
                parameters.Add(new SqlParameter("@VendorServiceId", this.VendorServiceId));

                sp = UpdateVOAService;
            }

            exec.ExecuteNonQuery(sp, 2, parameters.ToArray());

            if (vendorServiceIsNew)
            {
                this.VendorServiceId = (int)vendorServiceIdOutput.Value;
            }

            Action removeOldFile = () => FileDBTools.Delete(E_FileDB.Normal, this.GetOptionsFileDbKey(oldKey));
            this.PostSaveActions.Add(removeOldFile);
            this.PostSaveActions.Add(this.SyncLenderConfiguredVoaOptions);
        }

        /// <summary>
        /// Creates a view model of this service.
        /// </summary>
        /// <returns>The view model.</returns>
        internal override AbstractVOXVendorServiceViewModel ToViewModel()
        {
            VOAVendorServiceViewModel viewModel = new VOAVendorServiceViewModel();
            viewModel.AccountHistoryOptions = this.AccountHistoryOptions;
            viewModel.AskForNotificationEmail = this.AskForNotificationEmail;
            viewModel.IsADirectVendor = this.IsADirectVendor;
            viewModel.IsAReseller = this.IsAReseller;
            viewModel.RefreshPeriodOptions = this.RefreshPeriodOptions;
            viewModel.SellsViaResellers = this.SellsViaResellers;
            viewModel.VendorInfoId = this.VendorInfoId;
            viewModel.VendorServiceId = this.VendorServiceId;
            viewModel.VoaVerificationT = this.VoaVerificationT;
            viewModel.IsEnabled = this.IsEnabled;

            return viewModel;
        }

        /// <summary>
        /// Loads a VOA Vendor service from a reader.
        /// </summary>
        /// <param name="reader">The reader to load from.</param>
        internal override void PopulateFromReader(IDataReader reader)
        {
            this.VendorServiceId = (int)reader["VendorServiceId"];
            this.IsADirectVendor = (bool)reader["IsADirectVendor"];
            this.SellsViaResellers = (bool)reader["SellsViaResellers"];
            this.IsAReseller = (bool)reader["IsAReseller"];
            this.AskForNotificationEmail = (bool)reader["AskForNotificationEmail"];
            this.VoaVerificationT = (VOAVerificationT)reader["VoaVerificationT"];
            this.optionsFileDbKey = (Guid)reader["OptionsFileDbKey"];
            this.VendorInfoId = (int)reader["VendorId"];
            this.IsEnabled = (bool)reader["IsEnabled"];

            this.options = new Lazy<Options>(() =>
            {
                return FileDBTools.ReadJsonNetObject<Options>(E_FileDB.Normal, this.GetOptionsFileDbKey());
            });
        }

        /// <summary>
        /// Creates a new service from a model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if made. False otherwise.</returns>
        internal override bool CreateFromViewModel(AbstractVOXVendorServiceViewModel viewModel, out string errors)
        {
            if (viewModel == null)
            {
                errors = "Null model";
                return false;
            }

            VOAVendorServiceViewModel trueModel = viewModel as VOAVendorServiceViewModel;
            if (trueModel == null)
            {
                errors = "Invalid model passed in.";
                return false;
            }

            if (trueModel.AccountHistoryOptions.Any((option) => option < MinOption || option > MaxOption) ||
                trueModel.RefreshPeriodOptions.Any((option) => option < MinOption || option > MaxOption))
            {
                errors = "Invalid Options specified.";
                return false;
            }

            this.AskForNotificationEmail = trueModel.AskForNotificationEmail;
            this.IsADirectVendor = trueModel.IsADirectVendor;
            this.IsAReseller = trueModel.IsAReseller;
            this.IsEnabled = trueModel.IsEnabled;
            this.SellsViaResellers = trueModel.SellsViaResellers;
            this.VendorInfoId = -1;
            this.VendorServiceId = -1;
            this.VoaVerificationT = trueModel.VoaVerificationT;
            Options options = new Options();
            options.AccountHistoryOptions = trueModel.AccountHistoryOptions;
            options.RefreshPeriodOptions = trueModel.RefreshPeriodOptions;

            this.options = new Lazy<Options>(() => options);

            errors = string.Empty;
            return true;
        }

        /// <summary>
        /// Populates this vendor service using a view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if successful. False otherwise.</returns>
        internal override bool PopulateFromViewModel(VOXVendorViewModel viewModel, out string errors)
        {
            if (viewModel == null)
            {
                errors = "Null model passed in.";
                return false;
            }

            VOAVendorServiceViewModel trueModel = viewModel.VOAService;

            if (trueModel.AccountHistoryOptions.Any((option) => option < MinOption || option > MaxOption) ||
                trueModel.RefreshPeriodOptions.Any((option) => option < MinOption || option > MaxOption))
            {
                errors = "Invalid Options specified.";
                return false;
            }

            this.AskForNotificationEmail = trueModel.AskForNotificationEmail;
            this.IsADirectVendor = trueModel.IsADirectVendor;
            this.IsAReseller = trueModel.IsAReseller;
            this.IsEnabled = trueModel.IsEnabled;
            this.SellsViaResellers = trueModel.SellsViaResellers;
            this.VoaVerificationT = trueModel.VoaVerificationT;

            Options newOptions = new Options();
            newOptions.AccountHistoryOptions = trueModel.AccountHistoryOptions;
            newOptions.RefreshPeriodOptions = trueModel.RefreshPeriodOptions;
            this.options = new Lazy<Options>(() => newOptions);

            errors = string.Empty;
            return true;
        }

        /// <summary>
        /// Propagates any deleted VOA options to the table of lender-configured option values.
        /// </summary>
        private void SyncLenderConfiguredVoaOptions()
        {
            if (this.IsEnabled)
            {
                // If any options are removed at the vendor level, we want to remove them from all lenders.
                var configuredAccountHistoryOptions = VOAOptionConfiguration.RetrieveDistinctOptionValuesForVendor(this.VendorInfoId, VOAOptionType.AccountHistory);
                var deletedAccountHistoryOptions = configuredAccountHistoryOptions.Where(o => !this.AccountHistoryOptions.Contains(o));
                foreach (var deletedOptionValue in deletedAccountHistoryOptions)
                {
                    VOAOptionConfiguration.DeleteOptionValueByVendor(this.VendorInfoId, VOAOptionType.AccountHistory, deletedOptionValue);
                }

                var configuredRefreshPeriodOptions = VOAOptionConfiguration.RetrieveDistinctOptionValuesForVendor(this.VendorInfoId, VOAOptionType.RefreshPeriod);
                var deletedRefreshPeriodOptions = configuredRefreshPeriodOptions.Where(o => !this.RefreshPeriodOptions.Contains(o));
                foreach (var deletedOptionValue in deletedRefreshPeriodOptions)
                {
                    VOAOptionConfiguration.DeleteOptionValueByVendor(this.VendorInfoId, VOAOptionType.RefreshPeriod, deletedOptionValue);
                }
            }
            else
            {
                VOAOptionConfiguration.DeleteAllOptionsByVendor(this.VendorInfoId);
            }
        }

        /// <summary>
        /// Creates a file db key for the options.
        /// </summary>
        /// <param name="overrideGuid">A guid to use.</param>
        /// <returns>The full file db key.</returns>
        private string GetOptionsFileDbKey(Guid overrideGuid)
        {
            return $"{overrideGuid.ToString("N")}_VoaServiceOptions";
        }

        /// <summary>
        /// Creates the full file db key for the options.
        /// </summary>
        /// <returns>The file db key for the options.</returns>
        private string GetOptionsFileDbKey()
        {
            return this.GetOptionsFileDbKey(this.optionsFileDbKey);
        }

        /// <summary>
        /// Class that holds the options for the VOA service.
        /// </summary>
        private class Options
        {
            /// <summary>
            /// Gets or sets the refresh period options.
            /// </summary>
            /// <value>The refresh period options.</value>
            public List<int> RefreshPeriodOptions
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the account history options.
            /// </summary>
            /// <value>The account history options.</value>
            public List<int> AccountHistoryOptions
            {
                get;
                set;
            }
        }
    }
}
