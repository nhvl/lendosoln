﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// View for EDoc links for VOX orders.
    /// </summary>
    public class EDocView
    {
        /// <summary>
        /// Gets or sets the Edoc id.
        /// </summary>
        /// <value>The Edoc id.</value>
        public Guid EDocId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the document name.
        /// </summary>
        /// <value>The document name.</value>
        public string DocumentName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the document is available.
        /// </summary>
        /// <value>Whether the document is available.</value>
        public bool IsAvailable
        {
            get;
            set;
        }
    }
}
