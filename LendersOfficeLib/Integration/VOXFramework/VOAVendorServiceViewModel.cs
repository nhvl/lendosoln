﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// View model for the VOA vendor service.
    /// </summary>
    public class VOAVendorServiceViewModel : AbstractVOXVendorServiceViewModel
    {
        /// <summary>
        /// Gets the service type.
        /// </summary>
        /// <value>The service type.</value>
        public override VOXServiceT ServiceType
        {
            get
            {
                return VOXServiceT.VOA_VOD;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to ask for a notification email.
        /// </summary>
        /// <value>Whether to ask for a notification email.</value>
        public bool AskForNotificationEmail
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets what this service verifies.
        /// </summary>
        /// <value>What this service verifies.</value>
        public VOAVerificationT VoaVerificationT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the refresh period options.
        /// </summary>
        /// <value>The refresh period options.</value>
        public List<int> RefreshPeriodOptions
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the account history options.
        /// </summary>
        /// <value>The account history options.</value>
        public List<int> AccountHistoryOptions
        {
            get;
            set;
        }
    }
}
