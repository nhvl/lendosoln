﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;

    /// <summary>
    /// Data class for asset id for VOA requests.
    /// </summary>
    public class VOAServiceAssetData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOAServiceAssetData"/> class.
        /// </summary>
        /// <param name="appId">The app id.</param>
        /// <param name="assetId">The asset id.</param>
        /// <param name="isCoborrower">Whether this asset belongs to the coborrower.</param>
        public VOAServiceAssetData(Guid appId, Guid assetId, bool isCoborrower)
        {
            this.AppId = appId;
            this.AssetId = assetId;
            this.IsForCoborrower = isCoborrower;
        }

        /// <summary>
        /// Gets the application id for this asset.
        /// </summary>
        /// <value>The application id for this asset.</value>
        public Guid AppId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the id for this asset.
        /// </summary>
        /// <value>The id for this asset.</value>
        public Guid AssetId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether this asset belongs to the coborrower.
        /// </summary>
        /// <value>Whether this asset data is for the coborrower of the app.</value>
        public bool IsForCoborrower
        {
            get;
            private set;
        }
    }
}
