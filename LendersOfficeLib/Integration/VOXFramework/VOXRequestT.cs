﻿namespace LendersOffice.Integration.VOXFramework
{
    /// <summary>
    /// The type of VOX request.
    /// </summary>
    public enum VOXRequestT
    {
        /// <summary>
        /// An initial request.
        /// </summary>
        Initial = 0,
        
        /// <summary>
        /// A request to retrieve informating for a pending order.
        /// </summary>
        Get = 1,

        /// <summary>
        /// A request to refresh an order.
        /// </summary>
        Refresh = 2
    }
}
