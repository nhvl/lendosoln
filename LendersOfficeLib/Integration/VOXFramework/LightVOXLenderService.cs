﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;

    /// <summary>
    /// Light lender service.
    /// </summary>
    public class LightVOXLenderService
    {
        /// <summary>
        /// Gets or sets the lender service id.
        /// </summary>
        /// <value>The lender service id.</value>
        public int LenderServiceId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the vendor id.
        /// </summary>
        /// <value>The vendor id.</value>
        public int VendorId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the reseller id.
        /// </summary>
        /// <value>The reseller id.</value>
        public int ResellerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the display name.
        /// </summary>
        /// <value>The display name.</value>
        public string DisplayName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is enabled for LQB users.
        /// </summary>
        /// <value>Whether this is enabled for LQB users.</value>
        public bool IsEnabledforLqbUsers
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the enabled employee group.
        /// </summary>
        /// <value>The enabled employee group.</value>
        public Guid? EnabledEmployeeGroupId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this is enabled for TPO users.
        /// </summary>
        /// <value>Whether this is enabled for TPO users.</value>
        public bool IsEnabledForTpoUsers
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the enabled OC group.
        /// </summary>
        /// <value>The enabled OC group.</value>
        public Guid? EnabledOcGroupId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this lender service can be used for credentials in TPO.
        /// </summary>
        public bool CanBeUsedForCredentialsInTpo { get; set; }
    }
}
