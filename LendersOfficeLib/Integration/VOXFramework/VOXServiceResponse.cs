﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Encapsulates data in a VOX service response.
    /// </summary>
    public class VOXServiceResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOXServiceResponse"/> class.
        /// </summary>
        /// <param name="vendorOrderId">The vendor's order ID.</param>
        /// <param name="transactionId">The unique transaction ID.</param>
        /// <param name="statuses">The list of statuses returned from the vendor. Should be in ascending chronological order (current status last).</param>
        /// <param name="documents">The documents, in bytes, from the response.</param>
        /// <param name="validationReferenceNumbers">The DU validation reference numbers received in the response.</param>
        /// <param name="vendorReferenceIds">The vendor reference ids.</param>
        /// <param name="isInProgress">Whether the response indicates that the order is in progress.</param>
        public VOXServiceResponse(
            string vendorOrderId,
            Guid transactionId,
            List<VOXStatus> statuses,
            List<byte[]> documents,
            List<DuValidationReferenceNumberVoxData> validationReferenceNumbers,
            HashSet<string> vendorReferenceIds,
            bool isInProgress = false)
        {
            this.VendorOrderId = vendorOrderId;
            this.TransactionId = transactionId;
            VOXStatus currentVoxStatus = null;
            if (statuses != null && statuses.Any())
            {
                currentVoxStatus = statuses[statuses.Count - 1];
            }

            this.CurrentStatus = currentVoxStatus?.StatusCode ?? VOXOrderStatusT.Error;
            this.CurrentStatusDescription = currentVoxStatus?.StatusDescription ?? "No vendor status messages.";
            this.Statuses = statuses;
            this.ErrorMessage = this.CurrentStatus == VOXOrderStatusT.Error || this.CurrentStatus == VOXOrderStatusT.Canceled ? this.CurrentStatusDescription : null;
            this.DateCompleted = this.CurrentStatus == VOXOrderStatusT.Complete ? (DateTime?)DateTime.Now : null;
            this.Documents = documents ?? new List<byte[]>();
            this.ValidationReferenceNumbers = validationReferenceNumbers ?? new List<DuValidationReferenceNumberVoxData>();
            this.VendorReferenceIds = vendorReferenceIds ?? new HashSet<string>();
            this.OrderIsInProgress = isInProgress;
        }

        /// <summary>
        /// Gets the unique vendor ID for the order.
        /// </summary>
        /// <value>The unique vendor ID for the order.</value>
        public string VendorOrderId { get; private set; }

        /// <summary>
        /// Gets or sets the parent vendor ID for the order.
        /// </summary>
        /// <value>The parent vendor ID for the order.</value>
        public string ParentOrderId { get; set; }

        /// <summary>
        /// Gets the unique transaction ID echoed by the vendor.
        /// </summary>
        /// <value>Our unique transaction ID.</value>
        public Guid TransactionId { get; private set; }

        /// <summary>
        /// Gets the list of statuses returned from the VOX vendor.
        /// </summary>
        public List<VOXStatus> Statuses { get; private set; }

        /// <summary>
        /// Gets the order status.
        /// </summary>
        /// <value>The order status.</value>
        public VOXOrderStatusT CurrentStatus { get; private set; }

        /// <summary>
        /// Gets a description of the order status.
        /// </summary>
        /// <value>A description of the order status.</value>
        public string CurrentStatusDescription { get; private set; }

        /// <summary>
        /// Gets the error message returned by the order.
        /// </summary>
        /// <value>The error message returned by the order.</value>
        public string ErrorMessage { get; }

        /// <summary>
        /// Gets or sets the date of the order completion.
        /// </summary>
        /// <value>The date of the order completion.</value>
        public DateTime? DateCompleted { get; set; }

        /// <summary>
        /// Gets the documents converted to bytes.
        /// </summary>
        /// <value>The document bytes.</value>
        public IReadOnlyCollection<byte[]> Documents { get; private set; }

        /// <summary>
        /// Gets or sets the document ids.
        /// </summary>
        /// <value>The document ids.</value>
        public IEnumerable<Guid> DocumentIds { get; set; }

        /// <summary>
        /// Gets the DU validation reference numbers received in the response.
        /// </summary>
        /// <value>The DU validation reference numbers received in the response.</value>
        public IReadOnlyCollection<DuValidationReferenceNumberVoxData> ValidationReferenceNumbers { get; }

        /// <summary>
        /// Gets the vendor reference numbers.
        /// </summary>
        public HashSet<string> VendorReferenceIds { get; }

        /// <summary>
        /// Gets or sets the transaction ids for all services received in the response.
        /// </summary>
        public IEnumerable<Guid> AllTransactionIds { get; set; } = Enumerable.Empty<Guid>();

        /// <summary>
        /// Gets a list of returned asset information objects, but only if this was a VOA order which returned ASSET containers.
        /// </summary>
        public List<VerifiedAsset> ReturnedAssets { get; internal set; } = null;

        /// <summary>
        /// Gets a value indicating whether this response indicated that the order is in progress.
        /// This is not a final status for an order, thus why it is not part of that enum.
        /// </summary>
        public bool OrderIsInProgress
        {
            get;
        } 
    }
}
