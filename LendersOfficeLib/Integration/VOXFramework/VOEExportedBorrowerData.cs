﻿namespace LendersOffice.Integration.VOXFramework
{
    /// <summary>
    /// Class holding borrower data that was exported for VOE and needs to be recorded.
    /// </summary>
    public class VOEExportedBorrowerData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOEExportedBorrowerData"/> class.
        /// </summary>
        /// <param name="borrowerName">The borrower's name.</param>
        /// <param name="last4Ssn">The last 4 digits of the borrower's SSN.</param>
        public VOEExportedBorrowerData(string borrowerName, string last4Ssn)
        {
            this.BorrowerName = borrowerName;
            this.Last4Ssn = last4Ssn;
        }

        /// <summary>
        /// Gets the borrower's name.
        /// </summary>
        public string BorrowerName { get; }

        /// <summary>
        /// Gets the last 4 digits of the borrwer's SSN.
        /// </summary>
        public string Last4Ssn { get; }
    }
}
