﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Net;
    using System.Text;
    using Common;
    using DataAccess;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using ObjLib.Security.ThirdParty;

    /// <summary>
    /// Transmits a VOX request to a vendor.
    /// </summary>
    public abstract class VOXServer
    {
        /// <summary>
        /// Gets the content type header to send.
        /// </summary>
        /// <value>The content type header to send.</value>
        protected abstract string ContentType { get; }

        /// <summary>
        /// Submits a request to the specified vendor.
        /// </summary>
        /// <param name="requestData">The request and transmission data.</param>
        /// <param name="requestString">The request payload.</param>
        /// <returns>The response string.</returns>
        public VOXServerResponseData SubmitRequest(AbstractVOXRequestData requestData, string requestString)
        {
            var vendor = requestData.LenderService.VendorForProtocolAndTransmissionData;
            HttpWebRequest webRequest = this.CreateWebRequest(vendor.TransmissionInfo.TargetUrl);
            this.CustomizeWebRequest(requestData, vendor, webRequest);

            // IP authentication doesn't need any special handling.
            if (vendor.TransmissionInfo.TransmissionAuthenticationT == TransmissionAuthenticationT.DigitalCert)
            {
                ThirdPartyClientCertificateManager.AppendCertificate(webRequest);
            }
            else if (vendor.TransmissionInfo.TransmissionAuthenticationT == TransmissionAuthenticationT.BasicAuthentication)
            {
                throw new DeveloperException(ErrorMessage.BadConfiguration);
            }

            byte[] bytes = Encoding.UTF8.GetBytes(requestString);

            Stopwatch watch = new Stopwatch();
            watch.Start();

            using (Stream stream = webRequest.GetRequestStream())
            {
                stream.Write(bytes, 0, bytes.Length);
            }

            try
            {
                var responseData = GetWebResponse(webRequest);
                return responseData;
            }
            catch (CBaseException exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                throw exc;
            }
            finally
            {
                watch.Stop();
                Tools.LogInfo($"{VOXUtilities.TimerLogHeader}{Environment.NewLine}GetResponseTook: {watch.ElapsedMilliseconds}ms");
            }
        }

        /// <summary>
        /// Allows subclasses to customize the format of the web request.
        /// </summary>
        /// <param name="requestData">The request and transmission data.</param>
        /// <param name="vendor">The vendor to receive the data.</param>
        /// <param name="webRequest">The request to mutate.</param>
        protected virtual void CustomizeWebRequest(AbstractVOXRequestData requestData, VOXVendor vendor, HttpWebRequest webRequest)
        {
        }

        /// <summary>
        /// Transmits the request and reads the vendor response.
        /// </summary>
        /// <param name="webRequest">The web request to transmit.</param>
        /// <returns>The response string from the vendor.</returns>
        private static VOXServerResponseData GetWebResponse(HttpWebRequest webRequest)
        {
            try
            {
                using (WebResponse response = webRequest.GetResponse())
                {
                    string content;
                    Dictionary<string, List<string>> headersDictionary = new Dictionary<string, List<string>>();
                    using (Stream stream = response.GetResponseStream())
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        content = reader.ReadToEnd();
                    }

                    var headers = response.Headers;
                    for (int i = 0; i < headers.Count; i++)
                    {
                        string header = headers.GetKey(i);
                        List<string> values;
                        if (!headersDictionary.TryGetValue(header, out values))
                        {
                            values = new List<string>();
                            headersDictionary.Add(header, values);
                        }

                        foreach (string value in headers.GetValues(i))
                        {
                            values.Add(value);
                        }
                    }

                    return new VOXServerResponseData(content, headersDictionary);
                }
            }
            catch (InvalidOperationException exc)
            {
                throw new CBaseException(ErrorMessages.VOXErrors.UnexpectedResponse, exc);
            }
        }

        /// <summary>
        /// Creates a web request to send out.
        /// </summary>
        /// <param name="url">The url receiving the request.</param>
        /// <returns>A prepared web request.</returns>
        private HttpWebRequest CreateWebRequest(LqbGrammar.DataTypes.LqbAbsoluteUri url)
        {
            var webRequest = (HttpWebRequest)WebRequest.Create(url.ToString());

            webRequest.KeepAlive = false;
            webRequest.Method = "POST";
            webRequest.Timeout = 60000;
            webRequest.ContentType = this.ContentType;

            return webRequest;
        }
    }
}
