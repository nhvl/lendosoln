﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;

    /// <summary>
    /// Simple plain old data object for storing the snapshot of an asset associated with an order.
    /// </summary>
    public class VOAOrderAssetRecord
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOAOrderAssetRecord"/> class.
        /// </summary>
        /// <param name="id">The id of the asset on the loan.</param>
        /// <param name="institution">The name of the institution at the time of the order.</param>
        /// <param name="accountNumber">The account number at the time of the order.</param>
        public VOAOrderAssetRecord(Guid id, string institution, LqbGrammar.DataTypes.Sensitive<string> accountNumber)
        {
            this.Id = id;
            this.Institution = institution;
            this.AccountNumber = accountNumber;
        }

        /// <summary>
        /// Gets the id of the asset on the loan.
        /// </summary>
        /// <value>The id of the asset on the loan.</value>
        public Guid Id { get; }

        /// <summary>
        /// Gets the name of the institution at the time of the order.
        /// </summary>
        /// <value>The name of the institution at the time of the order.</value>
        public string Institution { get; }

        /// <summary>
        /// Gets the account number at the time of the order.
        /// </summary>
        /// <value>The account number at the time of the order.</value>
        public LqbGrammar.DataTypes.Sensitive<string> AccountNumber { get; }
    }
}
