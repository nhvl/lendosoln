﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;

    /// <summary>
    /// VOE salary key data.
    /// </summary>
    public class VOESalaryKey
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOESalaryKey" /> class.
        /// </summary>
        /// <param name="appId">The app id.</param>
        /// <param name="isCoborrower">Whether the borrower is a coborrower of the app.</param>
        /// <param name="salaryKey">The salary key.</param>
        public VOESalaryKey(Guid appId, bool isCoborrower, string salaryKey)
        {
            this.AppId = appId;
            this.IsCoborrower = isCoborrower;
            this.SalaryKey = salaryKey;
        }

        /// <summary>
        /// Gets the app id for the borrower.
        /// </summary>
        /// <value>The app id for the borrower.</value>
        public Guid AppId { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the borrower is the coborrower of the app.
        /// </summary>
        /// <value>Whether the borrower is the coborrower of the app.</value>
        public bool IsCoborrower { get; private set; }

        /// <summary>
        /// Gets the salary key.
        /// </summary>
        /// <value>The salary key.</value>
        public string SalaryKey { get; private set; }
    }
}
