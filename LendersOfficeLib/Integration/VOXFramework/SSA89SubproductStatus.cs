﻿namespace LendersOffice.Integration.VOXFramework
{
    /// <summary>
    /// Subproduct statuses for SSA-89.
    /// </summary>
    public enum SSA89SubproductStatus
    {
        /// <summary>
        /// Not ordered status.
        /// </summary>
        NotOrdered = 0,

        /// <summary>
        /// Pending status.
        /// </summary>
        Pending = 1,

        /// <summary>
        /// Present status.
        /// </summary>
        Present = 2,

        /// <summary>
        /// Not present status.
        /// </summary>
        NotPresent = 3,

        /// <summary>
        /// Match status.
        /// </summary>
        Match = 4,

        /// <summary>
        /// Not match status.
        /// </summary>
        NoMatch = 5,

        /// <summary>
        /// Returned status.
        /// </summary>
        Returned = 6,

        /// <summary>
        /// Error status.
        /// </summary>
        Error = 7
    }
}
