﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using EDocs;
    using LendersOffice.Admin;
    using ObjLib.ServiceCredential;
    using Security;

    /// <summary>
    /// Factory that will generate the component loaders for all VOX services. All VOX loaders in this instance will share common objects.
    /// </summary>
    public class VOXLoaderFactory
    {
        /// <summary>
        /// The VOA loader.
        /// </summary>
        private Lazy<VOALoader> voaLoader = null;

        /// <summary>
        /// The VOE loader.
        /// </summary>
        private Lazy<VOELoader> voeLoader = null;

        /// <summary>
        /// The SSA-89 loader.
        /// </summary>
        private Lazy<SSA89Loader> ssa89Loader = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="VOXLoaderFactory"/> class.
        /// </summary>
        /// <param name="principal">The principal of the user loading up the service.</param>
        /// <param name="loanId">The loan id.</param>
        public VOXLoaderFactory(AbstractUserPrincipal principal, Guid loanId)
        {
            var brokerId = principal.BrokerId;
            var loanData = CPageData.CreateUsingSmartDependency(loanId, typeof(VOXLoaderFactory));
            loanData.InitLoad();

            var voxVendorIdToCredential = new Lazy<Dictionary<int, ServiceCredential>>(() =>
            {
                var credentials = ServiceCredential.ListAvailableServiceCredentials(principal, loanData.sBranchId, ServiceCredentialService.Verifications);
                Dictionary<int, ServiceCredential> voxVendorToCredentials = new Dictionary<int, ServiceCredential>();
                foreach (var credential in credentials)
                {
                    if (credential.VoxVendorId.HasValue && !voxVendorToCredentials.ContainsKey(credential.VoxVendorId.Value))
                    {
                        voxVendorToCredentials.Add(credential.VoxVendorId.Value, credential);
                    }
                }

                return voxVendorToCredentials;
            });

            var availableEdocs = new Lazy<HashSet<Guid>>(() =>
            {
                var edocRepo = EDocumentRepository.GetUserRepository(principal);
                return edocRepo.GetDocumentIdsByLoanId(loanId);
            });

            var lenderServices = new Lazy<Dictionary<int, VOXLenderService>>(() =>
            {
                return VOXLenderService.GetLenderServices(brokerId).ToDictionary((service) => service.LenderServiceId);
            });

            var groups = new Lazy<Dictionary<Guid, Group>>(() =>
            {
                if (principal.Type.Equals("P", StringComparison.OrdinalIgnoreCase))
                {
                    return GroupDB.ListInclusiveGroupForPmlBroker(brokerId, principal.PmlBrokerId).ToDictionary(group => group.GroupId);
                }
                else
                {
                    return GroupDB.ListInclusiveGroupForEmployee(brokerId, principal.EmployeeId).ToDictionary((group) => group.GroupId);
                }
            });

            this.voaLoader = new Lazy<VOALoader>(() => new VOALoader(voxVendorIdToCredential, availableEdocs, lenderServices, groups, loanData, principal));
            this.voeLoader = new Lazy<VOELoader>(() => new VOELoader(voxVendorIdToCredential, availableEdocs, lenderServices, groups, loanData, principal));
            this.ssa89Loader = new Lazy<SSA89Loader>(() => new SSA89Loader(voxVendorIdToCredential, availableEdocs, lenderServices, groups, loanData, principal));
        }

        /// <summary>
        /// Gets the VOA loader.
        /// </summary>
        public VOALoader VoaLoader
        {
            get { return this.voaLoader.Value; }
        }

        /// <summary>
        /// Gets the VOE loader.
        /// </summary>
        public VOELoader VoeLoader
        {
            get { return this.voeLoader.Value; }
        }

        /// <summary>
        /// Gets the SSA-89 loader.
        /// </summary>
        public SSA89Loader Ssa89Loader
        {
            get { return this.ssa89Loader.Value; }
        }
    }
}
