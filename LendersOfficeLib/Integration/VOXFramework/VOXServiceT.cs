﻿namespace LendersOffice.Integration.VOXFramework
{
    /// <summary>
    /// Indicates what type of service this is.
    /// </summary>
    public enum VOXServiceT
    {
        /// <summary>
        /// VOA/VOD service.
        /// </summary>
        VOA_VOD = 0,

        /// <summary>
        /// VOE service.
        /// </summary>
        VOE = 1,

        /// <summary>
        /// SSA-89, aka SSN verification.
        /// </summary>
        SSA_89 = 2,
    }
}
