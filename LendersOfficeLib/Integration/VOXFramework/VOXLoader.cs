﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common;
    using DataAccess;
    using LendersOffice.Admin;
    using ObjLib.ServiceCredential;
    using Security;

    /// <summary>
    /// Base class for a VOX component loader. A component loader loads all the various fields/collections/whatever necessary for ordering from a VOX service.
    /// </summary>
    public abstract class VOXLoader
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOXLoader"/> class.
        /// </summary>
        /// <param name="voxVendorIdToCredential">Mapping of VOX vendor id to service credentials.</param>
        /// <param name="availableEdocs">The available EDocs.</param>
        /// <param name="lenderServices">The lender services available.</param>
        /// <param name="groups">The groups for checking VOX services.</param>
        /// <param name="loanData">The loan data.</param>
        /// <param name="principal">The principal of the user loading the service.</param>
        protected VOXLoader(
            Lazy<Dictionary<int, ServiceCredential>> voxVendorIdToCredential,
            Lazy<HashSet<Guid>> availableEdocs,
            Lazy<Dictionary<int, VOXLenderService>> lenderServices,
            Lazy<Dictionary<Guid, Group>> groups,
            CPageData loanData,
            AbstractUserPrincipal principal)
        {
            this.VoxVendorIdToCredential = voxVendorIdToCredential;
            this.AvailableEdocs = availableEdocs;
            this.LenderServices = lenderServices;
            this.Groups = groups;
            this.LoanData = loanData;
            this.Principal = principal;
            this.LoanId = this.LoanData.sLId;
            this.BrokerId = principal.BrokerId;
        }

        /// <summary>
        /// Gets the type of service for this VOX loader.
        /// </summary>
        protected abstract VOXServiceT ServiceType { get; }

        /// <summary>
        /// Gets the principal of the user doing the loading.
        /// </summary>
        protected AbstractUserPrincipal Principal { get; }

        /// <summary>
        /// Gets the loan id.
        /// </summary>
        protected Guid LoanId { get; }

        /// <summary>
        /// Gets the broker id.
        /// </summary>
        protected Guid BrokerId { get; }

        /// <summary>
        /// Gets the loaded loan data.
        /// </summary>
        /// <remarks>
        /// May want to lazy load this somehow if we add an operation that doesn't require the loan.
        /// </remarks>
        protected CPageData LoanData { get; }

        /// <summary>
        /// Gets the mapping of all platforms.
        /// </summary>
        protected Lazy<Dictionary<int, VOXPlatform>> Platforms { get; } = new Lazy<Dictionary<int, VOXPlatform>>(() => VOXPlatform.LoadPlatforms().ToDictionary((platform) => platform.PlatformId));

        /// <summary>
        /// Gets the mapping of all vendors.
        /// </summary>
        protected Lazy<Dictionary<int, LightVOXVendor>> Vendors { get; } = new Lazy<Dictionary<int, LightVOXVendor>>(() => VOXVendor.LoadLightVendors().ToDictionary((vendor) => vendor.VendorId));

        /// <summary>
        /// Gets the mapping of vendor id to associated service credential.
        /// </summary>
        protected Lazy<Dictionary<int, ServiceCredential>> VoxVendorIdToCredential { get; }

        /// <summary>
        /// Gets the available edocs.
        /// </summary>
        protected Lazy<HashSet<Guid>> AvailableEdocs { get; }

        /// <summary>
        /// Gets the mapping of all lender services for this broker.
        /// </summary>
        protected Lazy<Dictionary<int, VOXLenderService>> LenderServices { get; }

        /// <summary>
        /// Gets the groups for checking VOX services.
        /// </summary>
        protected Lazy<Dictionary<Guid, Group>> Groups { get; }

        /// <summary>
        /// Gets a list of viewmodels for all the orders of the given VOX service.
        /// </summary>
        /// <returns>The collection of order viewmodels.</returns>
        public IEnumerable<AbstractVOXOrderViewModel> LoadOrderViewModels()
        {
            List<AbstractVOXOrderViewModel> viewModels = new List<AbstractVOXOrderViewModel>();
            foreach (var order in this.GetOrders().CoalesceWithEmpty())
            {
                var vendorInfo = this.GetVendorInfo(order.LenderServiceId);
                vendorInfo.AvailableEdocs = this.AvailableEdocs.Value;
                var model = order.CreateViewModel(vendorInfo);
                viewModels.Add(model);
            }

            return viewModels;
        }

        /// <summary>
        /// Gets a collection of lender services of the given type available for use by the user.
        /// </summary>
        /// <returns>The collection of lender services available.</returns>
        public IEnumerable<VOXLenderService> GetLenderServicesAvailableToUser()
        {
            return this.LenderServices.Value.Values.CoalesceWithEmpty().Where((service) =>
            {
                var userIsBUser = this.Principal.Type.Equals("B", StringComparison.OrdinalIgnoreCase);
                var isServiceEnabled = userIsBUser ? service.EnabledForLqbUsers : service.EnabledForTpoUsers;
                var enabledGroupId = userIsBUser ? service.EnabledEmployeeGroupId : service.EnabledOCGroupId;

                if (service.ServiceType != this.ServiceType || !isServiceEnabled || !enabledGroupId.HasValue)
                {
                    return false;
                }

                return enabledGroupId.Value == VOXLenderService.AllGroupsId || this.Groups.Value.ContainsKey(enabledGroupId.GetValueOrDefault());
            });
        }

        /// <summary>
        /// Gets the orders on this loan for this VOX service.
        /// </summary>
        /// <returns>The orders on this loan for this VOX service.</returns>
        protected abstract IEnumerable<AbstractVOXOrder> GetOrders();

        /// <summary>
        /// Gets addition vendor info for each order's lender service.
        /// </summary>
        /// <param name="serviceId">The service id.</param>
        /// <returns>The vendor info for the order.</returns>
        private ExtraOrderViewModelInfo GetVendorInfo(int serviceId)
        {
            ExtraOrderViewModelInfo vendorInfo = new ExtraOrderViewModelInfo();
            VOXLenderService service;
            if (this.LenderServices.Value.TryGetValue(serviceId, out service))
            {
                LightVOXVendor vendor = null;
                int vendorId = service.IsDirectSeller ? service.AssociatedVendorId : service.AssociatedResellerId;
                this.Vendors.Value.TryGetValue(vendorId, out vendor);
                if (vendor != null && vendor.AssociatedPlatformId.HasValue)
                {
                    VOXPlatform platform;
                    if (this.Platforms.Value.TryGetValue(vendor.AssociatedPlatformId.Value, out platform))
                    {
                        vendorInfo.UsesAccountId = platform.UsesAccountId;
                        vendorInfo.RequiresAccountId = platform.RequiresAccountId;
                    }
                }

                ServiceCredential serviceCredential;
                if (this.VoxVendorIdToCredential.Value.TryGetValue(vendorId, out serviceCredential))
                {
                    vendorInfo.ServiceCredentialId = serviceCredential.Id;
                    vendorInfo.ServiceCredentialHasAccountId = !string.IsNullOrEmpty(serviceCredential.AccountId);
                }
            }

            return vendorInfo;
        }
    }
}
