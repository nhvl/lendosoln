﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Net;
    using System.Text;

    /// <summary>
    /// Defines MCL-specific implementation of the VOX server.
    /// </summary>
    public class MclVoxServer : VOXServer
    {
        /// <summary>
        /// Gets the content type header to send.
        /// </summary>
        /// <value>The content type header to send.</value>
        protected override string ContentType => "application/xml";

        /// <summary>
        /// Adds MCL-specific headers to the web request.
        /// </summary>
        /// <param name="requestData">The request and transmission data.</param>
        /// <param name="vendor">The vendor to receive the data.</param>
        /// <param name="webRequest">The request to mutate.</param>
        protected override void CustomizeWebRequest(AbstractVOXRequestData requestData, VOXVendor vendor, HttpWebRequest webRequest)
        {
            var craId = string.IsNullOrEmpty(vendor.MclCraId) ? string.Empty : $"{vendor.MclCraId}-";
            var username = requestData.Username;
            var password = requestData.Password;
            string basicAuthInfo = $"{craId}{username}:{password}";
            basicAuthInfo = $"Basic {Convert.ToBase64String(Encoding.ASCII.GetBytes(basicAuthInfo))}";

            webRequest.Headers.Add(HttpRequestHeader.Authorization, basicAuthInfo);

            string mclHeader = vendor.IsTestVendor ? "SmartAPITestingIdentifier" : "LENDERS_OFFICE";
            webRequest.Headers.Add("MCL-Interface", mclHeader);
        }
    }
}
