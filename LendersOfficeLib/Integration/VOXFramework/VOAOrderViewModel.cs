﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// VOA order view model.
    /// </summary>
    public class VOAOrderViewModel : AbstractVOXOrderViewModel
    {
        /// <summary>
        /// Gets the service type.
        /// </summary>
        /// <value>The service type.</value>
        public override VOXServiceT ServiceType => VOXServiceT.VOA_VOD;

        /// <summary>
        /// Gets or sets the list of institutions in the request.
        /// </summary>
        /// <value>The list of institutions in the request.</value>
        public IReadOnlyList<string> Institutions { get; set; }

        /// <summary>
        /// Gets or sets the account numbers in the request.
        /// </summary>
        /// <value>The account numbers in the request.</value>
        public IReadOnlyList<string> AccountNumbers { get; set; }

        /// <summary>
        /// Gets or sets the refresh period for the order.
        /// </summary>
        /// <value>The refresh period for the option.</value>
        public int RefreshPeriod { get; set; }

        /// <summary>
        /// Gets or sets the verification type for the order.
        /// </summary>
        /// <value>The verification type for the order.</value>
        public VOAVerificationT VerificationType { get; set; }

        /// <summary>
        /// Gets the doc name prefix.
        /// </summary>
        /// <returns>The doc name prefix.</returns>
        public override string GetDocNamePrefix()
        {
            if (this.VerificationType == VOAVerificationT.Assets)
            {
                return "VOA";
            }
            else
            {
                return "VOD";
            }
        }
    }
}
