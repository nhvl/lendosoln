﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using ConfigSystem.Operations;
    using Conversions.VOXFramework;
    using Conversions.VOXFramework.Mismo;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.BackgroundJobs.VOA;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using Mismo3Specification.Version4Schema;

    /// <summary>
    /// Takes in request data and processes the request and response.
    /// </summary>
    public class VOARequestHandler : AbstractVOXRequestHandler<VOAOrder, VOARequestData>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOARequestHandler"/> class.
        /// </summary>
        /// <param name="requestData">The data for the request.</param>
        public VOARequestHandler(VOARequestData requestData)
            : base(requestData)
        {
        }

        /// <summary>
        /// Gets the workflow operation required to submit a request.
        /// </summary>
        /// <value>The workflow operation required to submit a request.</value>
        protected override WorkflowOperation RequiredOperation => WorkflowOperations.OrderVoa;

        /// <summary>
        /// Gets the document type to save the document as.
        /// </summary>
        /// <value>The document type to save the document as.</value>
        protected override DataAccess.E_AutoSavePage AutoSaveDocType => DataAccess.E_AutoSavePage.VoaVodDocuments;

        /// <summary>
        /// Checks the VOA job and returns appropriate results.
        /// </summary>
        /// <param name="publicJobId">The public job id.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <returns>The request results.</returns>
        public static VOARequestResults CheckRequest(Guid publicJobId, Guid loanId, Guid brokerId)
        {
            var jobresult = VOAJob.CheckJob(publicJobId);
            switch (jobresult.ProcessingStatus)
            {
                case ObjLib.BackgroundJobs.ProcessingStatus.Incomplete:
                case ObjLib.BackgroundJobs.ProcessingStatus.Processing:
                    return new VOARequestResults(IntegrationJobStatus.Processing, null, null);
                case ObjLib.BackgroundJobs.ProcessingStatus.Invalid:
                    return new VOARequestResults(IntegrationJobStatus.Error, null, jobresult.Errors);
                case ObjLib.BackgroundJobs.ProcessingStatus.Completed:
                    List<VOAOrder> orders = new List<VOAOrder>();
                    foreach (var orderId in jobresult.OrderIds.CoalesceWithEmpty())
                    {
                        orders.Add(VOAOrder.GetOrderForLoan(brokerId, loanId, orderId));
                    }

                    return new VOARequestResults(IntegrationJobStatus.Complete, orders, null);
                default:
                    throw new UnhandledEnumException(jobresult.ProcessingStatus);
            }
        }

        /// <summary>
        /// Submits the VOA request to the Background Job Processor.
        /// </summary>
        /// <returns>The public job id.</returns>
        public Guid SubmitToProcessor()
        {
            return VOAJob.AddJob(this.RequestData, ConstStage.VoaBJPPollingIntervalInSeconds, ConstStage.VoaBJPMaxPollingAttempts);
        }

        /// <summary>
        /// Used to determine if an order should be updated when processing the Service Responses we obtain.
        /// </summary>
        /// <returns>True if the order should be updated. False otherwise.</returns>
        /// <remarks>
        /// For VOA and VOA only, Refresh does not create new orders.
        /// </remarks>
        protected override bool ShouldUpdateOrder()
        {
            return base.ShouldUpdateOrder() || 
                (this.RequestData.VerificationType == VOAVerificationT.Assets && this.RequestData.RequestType == VOXRequestT.Refresh);
        }

        /// <summary>
        /// Creates an instance of the specified order in the database and in memory.
        /// </summary>
        /// <param name="orderRequest">The request of the order metadata that was sent.</param>
        /// <param name="serviceResponse">The response to the order.</param>
        /// <returns>An order object with a corresponding database entry.</returns>
        protected override VOAOrder CreateOrder(VOXRequestOrder orderRequest, VOXServiceResponse serviceResponse)
        {
            return VOAOrder.CreateOrder(this.RequestData, orderRequest, serviceResponse);
        }

        /// <summary>
        /// Creates an Equifax OFX request provider.
        /// </summary>
        /// <returns>The Equifax OFX request provider.</returns>
        protected override IVOXRequestProvider CreateEquifaxOfxRequestProvider()
        {
            // Not supported.
            throw new DeveloperException(ErrorMessage.BadConfiguration);
        }

        /// <summary>
        /// Creates an instance of the MCL request provider for this instance.
        /// </summary>
        /// <returns>The MCL request provider for this instance.</returns>
        protected override AbstractMismoVOXRequestProvider CreateMismoRequestProvider()
        {
            return new MismoVOARequestProvider(this.RequestData, this.IndividualServiceOrdersToRequest);
        }

        /// <summary>
        /// Generates a response provider from the vendor's response string.
        /// </summary>
        /// <param name="responseData">The response from the vendor.</param>
        /// <returns>The response provider of the request.</returns>
        protected override AbstractVOXResponseProvider GenerateResponseProvider(VOXServerResponseData responseData)
        {
            var format = this.RequestData.PayloadFormatT;
            switch (format)
            {
                case PayloadFormatT.LQBMismo:
                case PayloadFormatT.MCLSmartApi:
                    return new MismoVOXResponseProvider(responseData.Content, this.RequestData);
                default:
                    throw new UnhandledEnumException(format);
            }
        }

        /// <summary>
        /// Saves documents relating to the VOA order.
        /// </summary>
        /// <param name="orderRequest">The request data object for the order.</param>
        /// <param name="serviceResponse">The parsed service response object.</param>
        protected override void SaveDocumentsToEDocs(VOXRequestOrder orderRequest, VOXServiceResponse serviceResponse)
        {
            base.SaveDocumentsToEDocs(orderRequest, serviceResponse);

            // Save returned account transactions to EDocs
            var mismoAssets = ((this.ResponseData as MismoVOXResponseProvider)?.Response?.DealSets?.DealSetList?.FirstOrDefault()?.Deals?.DealList?.FirstOrDefault()?.Assets?.AssetList).CoalesceWithEmpty();
            if (mismoAssets.Count(asset => (asset?.AssetDetail?.Extension?.Other as MCL_ASSET_DETAIL_EXTENSION)?.Transactions?.TransactionList?.Any() ?? false) > 0)
            {
                MESSAGE mismoMessage = new MESSAGE();
                mismoMessage.DealSets = new DEAL_SETS();
                mismoMessage.DealSets.DealSetList = new List<DEAL_SET>()
                {
                    new DEAL_SET()
                    {
                        Deals = new DEALS()
                        {
                            DealList = new List<DEAL>()
                            {
                                new DEAL()
                                {
                                    Assets = new ASSETS()
                                    {
                                        AssetList = mismoAssets.ToList()
                                    }
                                }
                            }
                        }
                    }
                };

                var transactionsXml = SerializationHelper.XmlSerialize(mismoMessage, true, Mismo3Specification.Mismo3Constants.MismoNamespace);
                var fileDBKey = $"MismoVoaRequest_TransactionXML_{Guid.NewGuid()}";
                FileDBTools.WriteData(E_FileDB.Normal, fileDBKey, new System.Text.UTF8Encoding(false).GetBytes(transactionsXml));
                ObjLib.Edocs.AutoSaveDocType.AutoSaveDocTypeFactory.SaveXmlDocAsSystem(
                    E_AutoSavePage.VoaVodDocuments,
                    fileDBKey,
                    E_FileDB.Normal,
                    RequestData.BrokerId,
                    RequestData.LoanId,
                    RequestData.BorrowerAppId ?? CPageData.CreateUsingSmartDependencyForLoad(RequestData.LoanId, typeof(MismoVOXResponseProvider)).GetAppData(0).aAppId,
                    $"VOA account XML data with transactions from {this.RequestData.LenderService.VendorForVendorAndServiceData.CompanyName}. Vendor order number: {serviceResponse.VendorOrderId}.",
                    EDocs.Contents.E_FileType.UnspecifiedXml);
            }
        }

        /// <summary>
        /// Constructs the request data that will need to be used for running the subsequent GET for REFRESH requests.
        /// </summary>
        /// <param name="responseProvider">The response provider.</param>
        /// <returns>The request data. Null if it does not need subsequent runs.</returns>
        protected override AbstractVOXRequestData GetSubsequentRequestData(AbstractVOXResponseProvider responseProvider)
        {
            var isRefresh = this.RequestData.VerificationType == VOAVerificationT.Assets && this.RequestData.RequestType == VOXRequestT.Refresh;
            var isGet = this.RequestData.VerificationType == VOAVerificationT.Assets && this.RequestData.RequestType == VOXRequestT.Get &&
                        responseProvider?.ServiceResponses != null && responseProvider.ServiceResponses.Count == 1 && responseProvider.ServiceResponses.Single().OrderIsInProgress;

            if (isRefresh || isGet)
            {
                return new VOARequestData(this.RequestData, VOXRequestT.Get);
            }

            return null;
        }

        /// <summary>
        /// Splits the request data into the orders that will be sent to the vendor.
        /// </summary>
        /// <returns>The orders that will be sent to the vendor.</returns>
        protected override IReadOnlyCollection<VOXRequestOrder> CreateServiceOrdersFromRequestData()
        {
            switch (this.RequestData.RequestType)
            {
                case VOXRequestT.Get:
                    return this.GenerateGetOrder();
                case VOXRequestT.Refresh:
                    return this.GenerateRefreshOrder();
                case VOXRequestT.Initial:
                    return this.GenerateInitialOrder();
                default:
                    throw new UnhandledEnumException(this.RequestData.RequestType);
            }
        }

        /// <summary>
        /// Generates an order to Get results for an existing transaction.
        /// </summary>
        /// <returns>A collection containing a single order.</returns>
        private IReadOnlyCollection<VOXRequestOrder> GenerateGetOrder()
        {
            var previousOrder = (VOAOrder)this.RequestData.PreviousOrder;
            bool? isForCoborrower = null;
            if (this.RequestData.PayloadFormatT == PayloadFormatT.MCLSmartApi)
            {
                // MCL requires borrower information to be sent for Get requests.
                isForCoborrower = this.RequestData.BorrowersInRequest.First().Value;
            }

            return new[] { VOXRequestOrder.ForGetRequest(previousOrder, previousOrder.AssetRecords.Select(r => r.Id).ToList(), isForCoborrower) };
        }

        /// <summary>
        /// Generates an order to create a new Refresh order based on an existing transaction.
        /// </summary>
        /// <returns>A collection containing a single order.</returns>
        private IReadOnlyCollection<VOXRequestOrder> GenerateRefreshOrder()
        {
            var previousOrder = (VOAOrder)this.RequestData.PreviousOrder;

            if (this.RequestData.PayloadFormatT == PayloadFormatT.MCLSmartApi)
            {
                // A Refresh order for MCL is effectively a Get order, as it references the existing transaction rather
                // than creating a new one.
                return new[] { VOXRequestOrder.ForGetRequest(previousOrder, previousOrder.AssetRecords.Select(r => r.Id).ToList()) };
            }

            return new[] { VOXRequestOrder.ForRefreshRequest(previousOrder, previousOrder.AssetRecords.Select(r => r.Id).ToList()) };
        }

        /// <summary>
        /// Generates a set of new VOA orders.
        /// </summary>
        /// <returns>A collection containing a set of VOA orders.</returns>
        private IReadOnlyCollection<VOXRequestOrder> GenerateInitialOrder()
        {
            var orders = new List<VOXRequestOrder>();
            foreach (KeyValuePair<Guid, bool> kvp in this.RequestData.BorrowersInRequest)
            {
                Guid appId = kvp.Key;
                bool isForCoborrower = kvp.Value;
                orders.Add(VOXRequestOrder.ForInitialRequest(
                    appId,
                    isForCoborrower,
                    this.RequestData.AssetsToInclude.Where(a => a.AppId == appId && a.IsForCoborrower == isForCoborrower).Select(a => a.AssetId)));
            }

            return orders;
        }
    }
}
