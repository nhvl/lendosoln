﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Represents a single order as a part of a verification request.
    /// </summary>
    /// <remarks>
    /// This class became necessary when there was data determined in the order's initial mapping that
    /// is necessary to be saved and retrieved later.
    /// </remarks>
    public class VOXRequestOrder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOXRequestOrder"/> class.
        /// </summary>
        /// <param name="transactionId">The <seealso cref="AbstractVOXOrder.TransactionId"/> of the order.</param>
        /// <param name="applicationId">The application of the loan for which the order was placed.</param>
        /// <param name="linkedRecords">The identifiers to the records will be verified in this request.</param>
        /// <param name="isForCoborrower">Whether the borrower was the co-borrower on the application specified by <see cref="ApplicationId"/>.  Can be null in every case except an initial order.</param>
        public VOXRequestOrder(Guid transactionId, Guid applicationId, IReadOnlyCollection<Guid> linkedRecords, bool? isForCoborrower)
        {
            this.TransactionId = transactionId;
            this.ApplicationId = applicationId;
            this.LinkedRecords = linkedRecords;
            this.IsForCoborrower = isForCoborrower;
        }

        /// <summary>
        /// Gets the <seealso cref="AbstractVOXOrder.TransactionId"/> of the order.
        /// </summary>
        /// <value>The <seealso cref="AbstractVOXOrder.TransactionId"/> of the order.</value>
        public Guid TransactionId { get; }

        /// <summary>
        /// Gets the application of the loan for which the order was placed.
        /// </summary>
        /// <value>The application of the loan for which the order was placed.</value>
        public Guid ApplicationId { get; }

        /// <summary>
        /// Gets a value indicating whether the borrower was the co-borrower on the application specified by <see cref="ApplicationId"/>.  Can be null in every case except an initial order.
        /// </summary>
        /// <value>A value indicating whether the borrower was the co-borrower on the application specified by <see cref="ApplicationId"/>.</value>
        public bool? IsForCoborrower { get; }

        /// <summary>
        /// Gets the identifiers to the records will be verified in this request.
        /// </summary>
        /// <value>The identifiers to the records will be verified in this request.</value>
        public IReadOnlyCollection<Guid> LinkedRecords { get; }

        /// <summary>
        /// Creates an order for a <seealso cref="VOXRequestT.Get"/> request.
        /// </summary>
        /// <param name="previousOrder">The previous order that this is retrieving.</param>
        /// <param name="linkedRecords">The identifiers to the records will be verified in this request.</param>
        /// <param name="isForCoborrower">A value indicating whether the borrower was the co-borrower on the application specified by <see cref="ApplicationId"/>.
        /// Usually not applicable as the vendor already has this information, but some Get requests (such as VOA) require exporting borrower info again.</param>
        /// <returns>An entry representing the order that will be sent.</returns>
        public static VOXRequestOrder ForGetRequest(AbstractVOXOrder previousOrder, IEnumerable<Guid> linkedRecords, bool? isForCoborrower = null)
        {
            return new VOXRequestOrder(
                previousOrder.TransactionId,
                previousOrder.ApplicationId,
                linkedRecords.ToList(),
                isForCoborrower: isForCoborrower);
        }

        /// <summary>
        /// Creates an order for a <seealso cref="VOXRequestT.Refresh"/> request.
        /// </summary>
        /// <param name="previousOrder">The previous order that this is refreshing.</param>
        /// <param name="linkedRecords">The identifiers to the records which were verified in this request.</param>
        /// <returns>An entry representing the order that will be sent.</returns>
        public static VOXRequestOrder ForRefreshRequest(AbstractVOXOrder previousOrder, IEnumerable<Guid> linkedRecords)
        {
            return new VOXRequestOrder(
                transactionId: Guid.NewGuid(),
                applicationId: previousOrder.ApplicationId,
                linkedRecords: linkedRecords.ToList(),
                isForCoborrower: null);
        }

        /// <summary>
        /// Creates an order for a <seealso cref="VOXRequestT.Initial"/> request.
        /// </summary>
        /// <param name="applicationId">The application of the loan for which the order was placed.</param>
        /// <param name="isForCoborrower">A value indicating whether the borrower was the co-borrower on the application specified by <see cref="ApplicationId"/>.</param>
        /// <param name="linkedRecords">The identifiers to the records which were verified in this request.</param>
        /// <returns>An entry representing the order that will be sent.</returns>
        public static VOXRequestOrder ForInitialRequest(Guid applicationId, bool isForCoborrower, IEnumerable<Guid> linkedRecords)
        {
            return new VOXRequestOrder(
                transactionId: Guid.NewGuid(),
                applicationId: applicationId,
                linkedRecords: linkedRecords.ToList(),
                isForCoborrower: isForCoborrower);
        }
    }
}
