﻿namespace LendersOffice.Integration.VOXFramework
{
    /// <summary>
    /// The order status.
    /// </summary>
    public enum VOXOrderStatusT
    {
        /// <summary>
        /// Order is pending.
        /// </summary>
        Pending = 0,

        /// <summary>
        /// Order is complete.
        /// </summary>
        Complete = 1,

        /// <summary>
        /// Order has encountered a fatal error.
        /// </summary>
        Error = 2,

        /// <summary>
        /// Vendor could not find info necessary for order.
        /// </summary>
        NoHit = 3,

        /// <summary>
        /// The order has been canceled.
        /// </summary>
        Canceled = 4,

        /// <summary>
        /// The order is active.
        /// </summary>
        Active = 5
    }
}
