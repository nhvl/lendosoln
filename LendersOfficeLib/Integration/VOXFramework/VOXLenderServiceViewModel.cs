﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// View model for lender services.
    /// </summary>
    public class VOXLenderServiceViewModel
    {
        /// <summary>
        /// Gets or sets the id of the lender level service.
        /// </summary>
        /// <value>The id of the lender level service.</value>
        public int? LenderServiceId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the type of service.
        /// </summary>
        /// <value>The type of service.</value>
        public VOXServiceT ServiceType
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the service type as a string.
        /// </summary>
        /// <value>The service type as a string.</value>
        public string ServiceTypeName
        {
            get
            {
                return Tools.VendorServiceTToString(this.ServiceType);
            }
        }

        /// <summary>
        /// Gets or sets the id of the associated vendor.
        /// </summary>
        /// <value>The id of the associated vendor.</value>
        public int AssociatedVendorId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the associated vendor's name.
        /// </summary>
        /// <value>The associated vendor's name.</value>
        public string AssociatedVendorName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the id of the associated vendor that is a reseller.
        /// </summary>
        /// <value>The id of the associated vendor that is a resller.</value>
        public int AssociatedResellerId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the associated reseller's name.
        /// </summary>
        /// <value>The associated reseller's name.</value>
        public string AssociatedResellerName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the display name of this lender level service.
        /// </summary>
        /// <value>The display name of the lender level service.</value>
        public string DisplayName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the display name has been locked.
        /// </summary>
        /// <value>A value indicating whether the display name has been locked.</value>
        public bool DisplayNameLckd
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this service has been enabled for LQB users.
        /// </summary>
        /// <value>A value indicating whether this service has been enabled for LQB users.</value>
        public bool EnabledForLqbUsers
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the id of the employee group that this service has been enabled for.
        /// </summary>
        /// <value>The id of the employee group that this service has been enabled for.</value>
        public Guid? EnabledEmployeeGroupId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this service has been enabled for TPO users.
        /// </summary>
        /// <value>Whether this service has been enabled for TPO users.</value>
        public bool EnabledForTpoUsers
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the id of the OC group that this service has been enabled for.
        /// </summary>
        /// <value>The id of the oc group that this service has been enabled for.</value>
        public Guid? EnabledOCGroupId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this payment with credit card is allowed.
        /// </summary>
        /// <value>Whether payment with credit card is allowed.</value>
        public bool AllowPaymentByCreditCard
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether users in TPO portal can create/edit service credentials using this service.
        /// </summary>
        public bool CanBeUsedForCredentialsInTpo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether VOE orders will use the specific employer record search.
        /// </summary>
        public bool VoeUseSpecificEmployerRecordSearch { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether <see cref="VoeUseSpecificEmployerRecordSearch"/> must be obeyed.
        /// If false, the ordering user may override the value.
        /// </summary>
        public bool VoeEnforceUseSpecificEmployerRecordSearch { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether VOA orders can be requested without specifying assets.
        /// </summary>
        public bool VoaAllowRequestWithoutAssets { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether <see cref="VoaAllowRequestWithoutAssets"/> should be enforced.
        /// If false, the user may override the setting.
        /// </summary>
        public bool VoaEnforceAllowRequestWithoutAssets { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether importing assets is allowed after a VOA order.
        /// </summary>
        public bool VoaAllowImportingAssets { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether VOE orders can be requested for a borrower without specifying employment records.
        /// </summary>
        public bool VoeAllowRequestWithoutEmployment { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether <see cref="VoeAllowRequestWithoutEmployment"/> should be enforced.
        /// If false, the user may override the setting.
        /// </summary>
        public bool VoeEnforceRequestsWithoutEmployment { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether importing employment records is allowed after a VOE order.
        /// </summary>
        public bool VoeAllowImportingEmployment { get; set; }

        /// <summary>
        /// Creates a list of view models using a list of services and vendors.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="vendors">The vendors.</param>
        /// <returns>List of view models.</returns>
        public static IEnumerable<VOXLenderServiceViewModel> CreateViewModelFromVendorAndServices(IEnumerable<VOXLenderService> services, Dictionary<int, LightVOXVendor> vendors)
        {
            List<VOXLenderServiceViewModel> viewModels = new List<VOXLenderServiceViewModel>();
            foreach (var service in services)
            {
                LightVOXVendor vendor = null;
                vendors.TryGetValue(service.AssociatedVendorId, out vendor);
                LightVOXVendor reseller = null;
                if (service.AssociatedResellerId != VOXLenderService.NoneDirectVendorId)
                {
                    vendors.TryGetValue(service.AssociatedResellerId, out reseller);
                }

                VOXLenderServiceViewModel viewModel = new VOXLenderServiceViewModel();
                viewModel.ServiceType = service.ServiceType;
                viewModel.DisplayName = service.DisplayName;
                viewModel.LenderServiceId = service.LenderServiceId;
                viewModel.AssociatedVendorName = vendor?.CompanyName ?? string.Empty;

                string resellerName;
                if (service.AssociatedResellerId == VOXLenderService.NoneDirectVendorId)
                {
                    resellerName = "None/Direct Vendor";
                }
                else
                {
                    resellerName = reseller.CompanyName;
                }

                viewModel.AssociatedResellerName = resellerName;
                viewModels.Add(viewModel);
            }

            return viewModels;
        }
    }
}
