﻿namespace LendersOffice.Integration.VOXFramework
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;

    /// <summary>
    /// Represents a single status message returned from a verification service response.
    /// </summary>
    public class VOXStatus
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VOXStatus"/> class.
        /// </summary>
        /// <param name="statusCode">Status code.</param>
        /// <param name="statusDescription">Status description.</param>
        /// <param name="statusDateTime">Status datetime.</param>
        public VOXStatus(VOXOrderStatusT statusCode, string statusDescription, DateTime? statusDateTime)
        {
            this.StatusCode = statusCode;
            this.StatusDescription = statusDescription;
            this.StatusDateTime = statusDateTime;
        }

        /// <summary>
        /// Gets the status code of the status.
        /// </summary>
        public VOXOrderStatusT StatusCode { get; }

        /// <summary>
        /// Gets the description message associated with the status.
        /// </summary>
        public string StatusDescription { get; }

        /// <summary>
        /// Gets the time associated with the status.
        /// </summary>
        public DateTime? StatusDateTime { get; }

        /// <summary>
        /// Gets the list of returned VOX status messages returned for the given VOX order ID.
        /// </summary>
        /// <param name="brokerId">The broker that the vox order belongs to.</param>
        /// <param name="loanId">The loan ID of the loan belonging to the order.</param>
        /// <param name="orderId">The VOX order ID to get statuses for.</param>
        /// <returns>The list of status messages for the order.</returns>
        public static IReadOnlyList<VOXStatus> GetStatusesForOrder(Guid brokerId, Guid loanId, int orderId)
        {
            return DataAccess.StoredProcedureHelper.ExecuteReaderAndGetResults(
                record => new VOXStatus(
                    (VOXOrderStatusT)record["StatusCode"],
                    (string)record["StatusDescription"],
                    (DateTime)record["StatusTime"] <= System.Data.SqlTypes.SqlDateTime.MinValue ? null : (DateTime?)record["StatusTime"]),
                brokerId,
                "VOX_ORDER_STATUSES_LoadFromOrder",
                new List<SqlParameter> { new SqlParameter("@OrderId", orderId), new SqlParameter("@LoanId", loanId) });
        }
    }
}