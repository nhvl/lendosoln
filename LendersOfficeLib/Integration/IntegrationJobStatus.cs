﻿namespace LendersOffice.Integration
{
    /// <summary>
    /// Generic job statuses for integrations that go through a job processor.
    /// </summary>
    public enum IntegrationJobStatus
    {
        /// <summary>
        /// The job is still being processed.
        /// </summary>
        Processing,

        /// <summary>
        /// The job has completed with an error.
        /// </summary>
        Error,

        /// <summary>
        /// The job has completed.
        /// </summary>
        Complete
    }
}
