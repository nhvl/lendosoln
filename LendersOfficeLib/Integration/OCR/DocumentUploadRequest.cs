﻿// <copyright file="DocumentUploadRequest.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.Integration.OCR
{
    using System;
    using System.Xml;
    using LendersOffice.Drivers.Gateways;

    /// <summary>
    /// Generates an upload document request for OCRs.
    /// </summary>
    public sealed class DocumentUploadRequest : LQBOCRRequest
    {
        /// <summary>
        /// The transaction id.
        /// </summary>
        private Guid transactionId;

        /// <summary>
        /// The path of the PDF that will be sent.
        /// </summary>
        private string filePath;

        /// <summary>
        /// The loan reference number for an associated loan.
        /// </summary>
        private string loanReferenceNumber;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentUploadRequest"/> class.
        /// </summary>
        /// <param name="username">The username for the request.</param>
        /// <param name="password">The password for the request.</param>
        /// <param name="transactionId">The transaction id for the request.</param>
        /// <param name="filePath">The file path for the file that will be sent.</param>
        /// <param name="loanRefNum">The LQB stable reference number for the associated loan, if there is one.</param>
        public DocumentUploadRequest(string username, string password, Guid transactionId, string filePath, string loanRefNum = null)
            : base(username, password)
        {
            this.transactionId = transactionId;
            this.filePath = filePath;
            this.loanReferenceNumber = loanRefNum;
        }

        /// <summary>
        /// Writes the payload for the upload document request.
        /// </summary>
        /// <param name="writer">The writer for the XML generator.</param>
        /// <param name="isForLogging">Specifies if the request will be logged.</param>
        protected override void WritePayload(XmlWriter writer, bool isForLogging)
        {
            string base64Data = string.Empty;

            if (!isForLogging)
            {
                byte[] fileData = BinaryFileHelper.ReadAllBytes(this.filePath);
                base64Data = System.Convert.ToBase64String(fileData);
            }

            writer.WriteStartElement("DocumentUploadRequest");

            this.WriteAuthNode(writer, isForLogging);

            writer.WriteElementString("TransactionID", this.transactionId.ToString());

            if (!string.IsNullOrEmpty(this.loanReferenceNumber))
            {
                writer.WriteElementString("TargetLoanRefNumber", this.loanReferenceNumber.ToString());
            }

            writer.WriteStartElement("Document");
            writer.WriteCData(base64Data);
            writer.WriteEndElement();

            writer.WriteEndElement();
        }
    }
}
