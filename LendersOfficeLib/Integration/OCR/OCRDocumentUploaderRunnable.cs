﻿// <copyright file="OCRDocumentUploaderRunnable.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.Integration.OCR
{
    using System;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Handles OCR queued uploads.
    /// </summary>
    public class OCRDocumentUploaderRunnable : CommonProjectLib.Runnable.IRunnable
    {
        /// <summary>
        /// Gets the description for the runnable.
        /// </summary>
        /// <value>The description for the runnable.</value>
        public string Description
        {
            get { return "Uploads documents to OCR Vendors."; }
        }

        /// <summary>
        /// The main entry point for this runnable.
        /// </summary>
        public void Run()
        {
            bool uploaded = true;

            while (uploaded)
            {
                uploaded = OCRService.UploadFile(TimeoutInSeconds.Thirty);
            }
        }
    }
}
