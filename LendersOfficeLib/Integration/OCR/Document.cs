﻿// <copyright file="Document.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.Integration.OCR
{
    using System;

    /// <summary>
    /// Represents a document that will be sent to the OCR. 
    /// </summary>
    public sealed class Document
    {
        /// <summary>
        /// Gets the broker id that owns the document.
        /// </summary>
        /// <value>The owner's broker ID.</value>
        public Guid BrokerID { get; internal set; }

        /// <summary>
        /// Gets the user id of who uploaded it.
        /// </summary>
        /// <value>The user id of who uploaded it.</value>
        public Guid UserID { get; internal set; }

        /// <summary>
        /// Gets the unique identifier for this document.
        /// </summary>
        /// <value>A unique id for this document.</value>
        public Guid TransactionID { get; internal set; }

        /// <summary>
        /// Gets the filename the document was uploaded under.
        /// </summary>
        /// <value>The file name of the PDF.</value>
        public string FileName { get; internal set; }

        /// <summary>
        /// Gets the size in bytes of the document.
        /// </summary>
        /// <value>The size in bytes.</value>
        public int SizeBytes { get; internal set; }

        /// <summary>
        /// Gets the date and time the document was uploaded on.
        /// </summary>
        /// <value>The date and time the doc was uploaded.</value>
        public DateTime UploadedOn { get; internal set; }

        /// <summary>
        /// Gets the document status for the current document.
        /// </summary>
        /// <value>The document status.</value>
        public DocumentStatus Status { get; internal set; }

        /// <summary>
        /// Gets the last time the document status was updated.
        /// </summary>
        /// <value>The date the status field was last updated on.</value>
        public DateTime StatusUpdatedOn { get; internal set; }

        /// <summary>
        /// Gets the vendor id who the document was sent to.
        /// </summary>
        /// <value>The id of the vendor the doc is going to be sent to.</value>
        public Guid VendorID { get; internal set; }

        /// <summary>
        /// Gets the id for the loan the document was uploaded to if applicable.
        /// </summary>
        /// <value>The loan id.</value>
        public Guid? UploadLoanID { get; internal set; }

        /// <summary>
        /// Gets the reference number for the loan the document was uploaded to if applicable.
        /// </summary>
        /// <value>The loan reference number.</value>
        public string UploadLoanRefNum
        {
            get
            {
                return this.UploadLoanID.HasValue ? DataAccess.Tools.GetLoanRefNmByLoanId(this.BrokerID, this.UploadLoanID.Value) : null;
            }
        }

        /// <summary>
        /// Gets the document type id the user chose if the document was uploaded via the TPO portal.
        /// </summary>
        /// <value>The document type id chosen by the user.</value>
        public int? UploadDocTypeID { get; internal set; }

        /// <summary>
        /// Gets the description entered by the user if the document was uploaded via the TPO portal.
        /// </summary>
        /// <value>The description for the document.</value>
        public string UploadInternalDescription { get; internal set; }

        /// <summary>
        /// Gets the source of the document.
        /// </summary>
        /// <value>The location the document was uploaded to.</value>
        public Source Source { get; internal set; }

        /// <summary>
        /// Gets the application selected by the user on upload if applicable.
        /// </summary>
        /// <value>The application id selected by the user.</value>
        public Guid? UploadAppID { get; internal set; }
    }
}
