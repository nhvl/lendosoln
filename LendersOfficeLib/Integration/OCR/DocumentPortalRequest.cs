﻿// <copyright file="DocumentPortalRequest.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.Integration.OCR
{
    using System;
    using System.IO;
    using System.Xml;

    /// <summary>
    /// Generates an upload document request for OCRs.
    /// </summary>
    public sealed class DocumentPortalRequest : LQBOCRRequest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentPortalRequest"/> class.
        /// </summary>
        /// <param name="username">The username for the request.</param>
        /// <param name="password">The password for the request.</param>
        public DocumentPortalRequest(string username, string password)
            : base(username, password)
        {
        }

        /// <summary>
        /// Writes the payload for the upload document request.
        /// </summary>
        /// <param name="writer">The writer for the XML generator.</param>
        /// <param name="isForLogging">Specifies if the request will be logged.</param>
        protected override void WritePayload(XmlWriter writer, bool isForLogging)
        {
            writer.WriteStartElement("DocumentPortalRequest");
            this.WriteAuthNode(writer, isForLogging);
            writer.WriteEndElement();
        }
    }
}
