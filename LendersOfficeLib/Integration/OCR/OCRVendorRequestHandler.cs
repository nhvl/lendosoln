﻿// <copyright file="OCRVendorRequestHandler.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.Integration.OCR
{
    using System;
    using System.IO;
    using System.Web;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;

    /// <summary>
    /// A HTTP handler that handles OCR vendor request.
    /// </summary>
    public sealed class OCRVendorRequestHandler : System.Web.IHttpHandler
    {
        /// <summary>
        /// Gets a value indicating whether the handler is reusable.
        /// </summary>
        /// <value>This is true all the time.</value>
        public bool IsReusable
        {
            get { return true; }
        }

        /// <summary>
        /// Processes the given request.
        /// </summary>
        /// <param name="context">The http context for the request.</param>
        public void ProcessRequest(HttpContext context)
        {
            string request;
            using (StreamReader reader = new StreamReader(context.Request.InputStream))
            {
                request = reader.ReadToEnd();
            }
            
            byte[] data = OCRService.HandleVendorUpdate(request);

            context.Response.Clear();
            context.Response.ContentType = "application/xml";
            context.Response.StatusCode = 200;
            context.Response.BinaryWrite(data);
        }
    }
}
