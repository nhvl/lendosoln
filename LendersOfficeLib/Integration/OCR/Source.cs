﻿// <copyright file="Source.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.Integration.OCR
{
    /// <summary>
    /// Sources for Documents.
    /// </summary>
    public enum Source
    {
        /// <summary>
        /// Uploaded by virtual Printer.
        /// </summary>
        Dropbox = 0,

        /// <summary>
        /// Uploaded by the user.
        /// </summary>
        TPOPortal = 1,

        /// <summary>
        /// Uploaded to barcode scanner by user.
        /// </summary>
        BarcodeScanner = 2,
    }
}
