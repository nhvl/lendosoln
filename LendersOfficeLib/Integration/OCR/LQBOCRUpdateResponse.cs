﻿// <copyright file="LQBOCRUpdateResponse.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.Integration.OCR
{
    using System;
    using System.IO;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;

    /// <summary>
    /// Creates responses for update request.
    /// </summary>
    public class LQBOCRUpdateResponse
    {
        /// <summary>
        /// Creates an error response and returns its bytes in utf8.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        /// <returns>The xml document in a bytes array.</returns>
        public static byte[] CreateError(string errorMessage)
        {
            XDocument doc = new XDocument();
            XElement root = new XElement("LQBOCRUpdateResponse");
            XElement errorMessageElement = new XElement("ErrorMessage", errorMessage);
            XElement resultElement = new XElement("Response", "Error");

            doc.Add(root);
            root.Add(resultElement);
            root.Add(errorMessageElement);

            return LQBOCRUpdateResponse.Convert(doc);
        }

        /// <summary>
        /// Creates a success response and returns its bytes in utf8.
        /// </summary>
        /// <returns>The xml document in a byte array.</returns>
        public static byte[] CreateSuccess()
        {
            XDocument doc = new XDocument();
            XElement root = new XElement("LQBOCRUpdateResponse");
            XElement resultElement = new XElement("Response", "OK");

            doc.Add(root);
            root.Add(resultElement);

            return LQBOCRUpdateResponse.Convert(doc);
        }

        /// <summary>
        /// Serializes the xml document.
        /// </summary>
        /// <param name="doc">The xml document to serialize.</param>
        /// <returns>The byte array.</returns>
        private static byte[] Convert(XDocument doc)
        {
            var settings = new XmlWriterSettings { OmitXmlDeclaration = true, Encoding = Encoding.UTF8 };
            using (var memoryStream = new MemoryStream())
            {
                using (var xmlWriter = XmlWriter.Create(memoryStream, settings))
                {
                    doc.WriteTo(xmlWriter);
                }

                return memoryStream.ToArray();
            }
        }
    }
}
