﻿// <copyright file="LQBOCRUpdateRequest.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.Integration.OCR
{
    using System;
using System.Collections.Generic;
using System.Xml.Linq;
    using DataAccess;

    /// <summary>
    /// Represents a vendor update request.
    /// </summary>
    public sealed class LQBOCRUpdateRequest
    {
        /// <summary>
        /// Gets the transaction id for the status being updated.
        /// </summary>
        /// <value>The transaction id for the document.</value>
        public Guid? TransactionId { get; private set; }

        /// <summary>
        /// Gets a value indicating whether OCR was able to process the document.
        /// </summary>
        /// <value>True if the request was an error.</value>
        public bool IsError { get; private set; }

        /// <summary>
        /// Gets a value indicating whethere there is an issue parsing the request.
        /// </summary>
        /// <value>True if the xml was not in the proper format.</value>
        public bool FormatError { get; private set; }

        /// <summary>
        /// Gets the list of PDFs sent by the vendor that could not be processed.
        /// </summary>
        /// <value>PDFs that could not be processed by the vendor.</value>
        public IEnumerable<byte[]> UnprocessedDocuments { get; private set; }

        /// <summary>
        /// Parses the request.
        /// </summary>
        /// <param name="request">The request in XDocument format.</param>
        public void ReadFrom(XDocument request)
        {
            List<byte[]> pdfs = new List<byte[]>();
            XElement root = request.Root;
            XElement transactionIdElement = root.Element("TransactionID");
            XElement processingStatusElement = root.Element("ProcessingStatus");
            XElement unprocessableDocElement = root.Element("UnprocessableDocuments");
            this.UnprocessedDocuments = pdfs;

            if (transactionIdElement == null || processingStatusElement == null)
            {
                this.FormatError = true;
                return;
            }

            Guid id;

            if (!Guid.TryParse(transactionIdElement.Value, out id))
            {
                this.FormatError = true;
                return;
            }

            if (unprocessableDocElement != null)
            {
                foreach (XElement document in unprocessableDocElement.Elements("Document"))
                {
                    if (string.IsNullOrEmpty(document.Value))
                    {
                        continue;
                    }

                    try
                    {
                        pdfs.Add(System.Convert.FromBase64String(document.Value));
                    }
                    catch (FormatException)
                    {
                        Tools.LogError("PDF is invalid.");
                        this.FormatError = true;
                    }
                }
            }

            this.TransactionId = id;
            this.IsError = !processingStatusElement.Value.Equals("Completed");
        }
    }
}
