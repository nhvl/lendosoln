﻿// <copyright file="OCRDataSource.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.Integration.OCR
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Xml.Linq;
    using DataAccess;

    /// <summary>
    /// Data layer for OCR related data. 
    /// </summary>
    internal sealed class OCRDataSource
    {
        /// <summary>
        /// Gets a list of vendors.
        /// </summary>
        /// <param name="vendorId">The vendor id to limit the results by.</param>
        /// <returns>A list of vendors.</returns>
        public IEnumerable<Vendor> GetVendors(Guid? vendorId = null)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            if (vendorId.HasValue)
            {
                parameters.Add(new SqlParameter("@VendorId", vendorId.Value));
            }

            List<Vendor> vendors = new List<Vendor>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "OCR_VENDOR_List", parameters))
            {
                while (reader.Read())
                {
                    vendors.Add(this.GetVendorFrom(reader));
                }
            }

            return vendors;
        }

        /// <summary>
        /// Saves the given vendor.
        /// </summary>
        /// <param name="vendor">The vendor to save.</param>
        public void Save(Vendor vendor)
        {
            List<SqlParameter> parameters = new List<SqlParameter>() 
            { 
                new SqlParameter("@IsValid", vendor.IsValid),
                new SqlParameter("@DisplayName", vendor.DisplayName), 
                new SqlParameter("@PortalUrl", vendor.PortalURL),
                new SqlParameter("@UploadURL", vendor.UploadURL),
                new SqlParameter("@EnableConnectionTest", vendor.EnableConnectionTest)
            };

            if (vendor.Id != Guid.Empty)
            {
                parameters.Add(new SqlParameter("@VendorId", vendor.Id));
            }

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "OCR_VENDOR_Save", 1, parameters);
        }

        /// <summary>
        /// Deletes the document with the given ID. 
        /// </summary>
        /// <param name="transactionId">The id of the document.</param>
        /// <returns>True if the document was deleted false if it could not be found.</returns>
        public bool DeleteDocument(Guid transactionId)
        {
            foreach (var connection in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters = new SqlParameter[]
                {
                    new SqlParameter("@TransactionId", transactionId),
                };

                int rowsDeleted = (int)StoredProcedureHelper.ExecuteScalar(connection, "OCR_Document_Delete", parameters);
                if (rowsDeleted > 1)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Deletes the vendor with the given id.
        /// </summary>
        /// <param name="vendorId">The id of the vendor to delete.</param>
        public void DeleteVendor(Guid vendorId)
        {
            SqlParameter[] parameters = new SqlParameter[]
           {
                new SqlParameter("@VendorId", vendorId)
           };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "OCR_VENDOR_Delete", 1, parameters);
        }

        /// <summary>
        /// Stores a document in the data store.
        /// </summary>
        /// <param name="doc">The document to store.</param>
        public void AddDocument(Document doc)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", doc.BrokerID),
                new SqlParameter("@FileName", doc.FileName),
                new SqlParameter("@SizeBytes", doc.SizeBytes),
                new SqlParameter("@Status", doc.Status),
                new SqlParameter("@StatusUpdatedOn", doc.StatusUpdatedOn),
                new SqlParameter("@TransactionId", doc.TransactionID),
                new SqlParameter("@UploadedOn", doc.UploadedOn),
                new SqlParameter("@UserId", doc.UserID),
                new SqlParameter("@VendorId", doc.VendorID),

                new SqlParameter("@Source", doc.Source),
                new SqlParameter("@UploadAppID", doc.UploadAppID),
                new SqlParameter("@UploadLoanID", doc.UploadLoanID),
                new SqlParameter("@UploadDocTypeID", doc.UploadDocTypeID),
                new SqlParameter("@UploadInternalDescription", doc.UploadInternalDescription),
            };

            StoredProcedureHelper.ExecuteNonQuery(doc.BrokerID, "OCR_Document_Add", 1, parameters);
        }

        /// <summary>
        /// Updates the given document status.
        /// </summary>
        /// <param name="doc">The document whose status is being updated.</param>
        public void UpdateDocumentStatus(Document doc)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@Status", doc.Status),
                new SqlParameter("@StatusUpdatedOn", doc.StatusUpdatedOn),
                new SqlParameter("@BrokerId", doc.BrokerID),
                new SqlParameter("@TransactionId", doc.TransactionID)
            };

            StoredProcedureHelper.ExecuteNonQuery(doc.BrokerID, "OCR_Document_UpdateStatus", 1, parameters);
        }

        /// <summary>
        /// Gets the document with the given broker and transaction id.
        /// </summary>
        /// <param name="brokerId">The broker id of the document.</param>
        /// <param name="transactionId">The transaction id of the document.</param>
        /// <returns>The document or null if not found.</returns>
        public Document GetDocument(Guid brokerId, Guid transactionId)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@TransactionId", transactionId),
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "OCR_DOCUMENT_List", parameters))
            {
                if (reader.Read())
                {
                    return this.GetDocumentFrom(reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the document with the given transaction id.
        /// </summary>
        /// <param name="transactionId">The transaction id of the document.</param>
        /// <returns>The document or null if not found.</returns>
        public Document GetDocumentById(Guid transactionId)
        {
            foreach (var connection in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters = new SqlParameter[]
                {
                    new SqlParameter("@TransactionId", transactionId),
                };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connection, "OCR_DOCUMENT_List", parameters))
                {
                    if (reader.Read())
                    {
                        return this.GetDocumentFrom(reader);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Gets a vendor from the data row.
        /// </summary>
        /// <param name="reader">SqlData reader position at the next row.</param>
        /// <returns>A vendor object from said row.</returns>
        private Vendor GetVendorFrom(DbDataReader reader)
        {
            Vendor vendor = new Vendor();
            vendor.DisplayName = (string)reader["DisplayName"];
            vendor.Id = (Guid)reader["VendorId"];
            vendor.IsValid = (bool)reader["IsValid"];
            vendor.PortalURL = (string)reader["PortalURL"];
            vendor.UploadURL = (string)reader["UploadURL"];
            vendor.EnableConnectionTest = (bool)reader["EnableConnectionTest"];
            return vendor;
        }

        /// <summary>
        /// Gets a vendor from the data row.
        /// </summary>
        /// <param name="reader">SqlData reader position at the next row.</param>
        /// <returns>A vendor object from said row.</returns>
        private Document GetDocumentFrom(DbDataReader reader)
        {
            Document document = new Document();
            document.BrokerID = (Guid)reader["BrokerId"];
            document.FileName = (string)reader["FileName"];
            document.SizeBytes = (int)reader["SizeBytes"];
            document.Status = (DocumentStatus)reader["Status"];
            document.StatusUpdatedOn = (DateTime)reader["StatusUpdatedOn"];
            document.TransactionID = (Guid)reader["TransactionId"];
            document.UploadedOn = (DateTime)reader["UploadedOn"];
            document.UserID = (Guid)reader["UserId"];
            document.VendorID = (Guid)reader["VendorId"];

            document.Source = (Source)reader["Source"];
            document.UploadLoanID = reader.AsNullableGuid("UploadLoanID");
            document.UploadAppID = reader.AsNullableGuid("UploadAppId");
            document.UploadDocTypeID = reader.AsNullableInt("UploadDocTypeId");
            document.UploadInternalDescription = (string)reader["UploadInternalDescription"];
            
            return document;
        }
    }
}
