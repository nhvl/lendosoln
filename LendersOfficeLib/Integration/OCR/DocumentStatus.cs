﻿// <copyright file="DocumentStatus.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.Integration.OCR
{
    /// <summary>
    /// A enum used to represent the status of the document.
    /// </summary>
    public enum DocumentStatus
    {
        /// <summary>
        /// The document has been uploaded to our system but not queued. 
        /// </summary>
        Uploaded = 0, 

        /// <summary>
        /// The document has been queued. 
        /// </summary>
        Queued = 1, 

        /// <summary>
        /// The document has been pulled from the queue.
        /// </summary>
        SendingToOCR = 2,

        /// <summary>
        /// The document has been succesfully sent to the OCR.
        /// </summary>
        SentToOCR = 3,

        /// <summary>
        /// The document failed for some reasont o send to OCR.
        /// </summary>
        FailedToSend = 4
    }
}
