﻿// <copyright file="Vendor.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.Integration.OCR
{
    using System;

    /// <summary>
    /// OCR Vendor details. 
    /// </summary>
    public class Vendor
    {
        /// <summary>
        /// Gets or sets the id of the vendor.
        /// </summary>
        /// <value>The id for the vendor.</value>
        public Guid Id { get;  set; }

        /// <summary>
        /// Gets or sets a value indicating whether the vendor is active.
        /// </summary>
        /// <value>A value indicating whether the vendor is active.</value>
        public bool IsValid { get;  set; }

        /// <summary>
        /// Gets or sets the display name of the vendor.
        /// </summary>
        /// <value>The name of the vendor.</value>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the portal url of the vendor.
        /// </summary>
        /// <value>The portal url of the vendor.</value>
        public string PortalURL { get; set; }

        /// <summary>
        /// Gets or sets the upload url for new documents.
        /// </summary>
        /// <value>The URL for new documents.</value>
        public string UploadURL { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to run connection tests for this vendor.
        /// </summary>
        /// <value>Whether to run connection tests for this vendor.</value>
        public bool EnableConnectionTest { get; set; }
    }
}
