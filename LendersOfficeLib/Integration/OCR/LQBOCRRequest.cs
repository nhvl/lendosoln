﻿// <copyright file="LQBOCRRequest.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.Integration.OCR
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using LendersOffice.Common;
    using LendersOffice.Constants;

    /// <summary>
    /// Represents the container with authentication for LQB OCR request.
    /// </summary>
    public abstract class LQBOCRRequest
    {
        /// <summary>
        /// The username for the request.
        /// </summary>
        private string username;

        /// <summary>
        /// The password for the request.
        /// </summary>
        private string password;

        /// <summary>
        /// Initializes a new instance of the <see cref="LQBOCRRequest"/> class.
        /// </summary>
        /// <param name="username">The username for the request.</param>
        /// <param name="password">The password for the request.</param>
        public LQBOCRRequest(string username, string password)
        {
            this.username = username;
            this.password = password;
        }

        /// <summary>
        /// Generates a xml request.
        /// </summary>
        /// <param name="forLogging">A value indicating whether pdf/password should be left out.</param>
        /// <returns>The XML request ready to be sent.</returns>
        public byte[] GenerateRequest(bool forLogging = false)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                using (StreamWriter sw = new StreamWriter(ms, Encoding.UTF8))
                using (XmlWriter writer = XmlWriter.Create(sw))
                {
                    writer.WriteStartElement("LQBOCRRequest");

                    this.WritePayload(writer, forLogging);

                    // </LQBOCRRequest> 
                    writer.WriteEndElement();
                }

                return ms.ToArray();
            }
        }

        /// <summary>
        /// Allows children class to write custom payload to the request. The child class should
        /// close its section only.
        /// </summary>
        /// <param name="writer">The XML Writer.</param>
        /// <param name="isForLogging">Whether the request is for logging purposes.</param>
        protected abstract void WritePayload(XmlWriter writer, bool isForLogging);

        /// <summary>
        /// Writes the authentication node for the payload. If isForLogging is true no password 
        /// will be written.
        /// </summary>
        /// <param name="writer">The XML writer.</param>
        /// <param name="isForLogging">Whether the request is for logging.</param>
        protected void WriteAuthNode(XmlWriter writer, bool isForLogging)
        {
            string passwordForReq = this.password;

            if (isForLogging)
            {
                passwordForReq = ConstAppDavid.FakePasswordDisplay;
            }

            writer.WriteStartElement("Authentication");
            writer.WriteElementString("Username", this.username);
            writer.WriteElementString("Password", passwordForReq);
            writer.WriteEndElement();
        }
    }
}
