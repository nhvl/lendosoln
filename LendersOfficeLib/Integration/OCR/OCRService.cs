﻿// <copyright file="OCRService.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.Integration.OCR
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Messaging;
    using System.Net;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;
    using EDocs;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Drivers.NetFramework;
    using LendersOffice.Edocs.DocMagic;
    using LendersOffice.ObjLib.UserDropbox;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provide the OCR services for the application.
    /// </summary>
    public class OCRService
    {
        /// <summary>
        /// The file db database location for OCR documents.
        /// </summary>
        private const E_FileDB DOCSTORAGELOCATION = E_FileDB.EDMS;

        /// <summary>
        /// The data layer for the ocr service.
        /// </summary>
        private OCRDataSource database;

        /// <summary>
        /// The principal of the user.
        /// </summary>
        private AbstractUserPrincipal principal;

        /// <summary>
        /// Initializes a new instance of the <see cref="OCRService"/> class.
        /// </summary>
        /// <param name="principal">The principal of the calling code.</param>
        public OCRService(AbstractUserPrincipal principal)
        {
            this.principal = principal;
            this.database = new OCRDataSource();
        }

        /// <summary>
        /// Adds a file to the OCR queue if OCR is enabled and configured correctly.
        /// </summary>
        /// <param name="principal">The principal of the user adding the file. Will not work for internal staff.</param>
        /// <param name="fileName">The filename of the uploaded file.</param>
        /// <param name="uploadedOn">The date the file was uploaded on.</param>
        /// <param name="fileBytes">The file size in bytes.</param>
        /// <returns>True if the file was handled, false otherwise.</returns>
        public static bool AddFileFromDropbox(AbstractUserPrincipal principal, string fileName, DateTime uploadedOn, byte[] fileBytes)
        {
            if (string.IsNullOrEmpty(ConstStage.MSMQ_OCR_Upload))
            {
                return false;
            }

            if (principal.IsLQBStaff())
            {
                throw CBaseException.GenericException("Internal admins cannot upload file.");
            }

            OCRDataSource data = new OCRDataSource();
            BrokerDB db = principal.BrokerDB;

            if (!db.IsOCREnabled || !db.OCRHasConfiguredAccount || db.DisableVPrinterOCRFrameworkCapture)
            {
                return false;
            }

            Vendor vendor = data.GetVendors(vendorId: db.OCRVendorId.Value).FirstOrDefault();

            if (vendor == null || !vendor.IsValid)
            {
                return false;
            }

            Document doc = new Document();
            doc.BrokerID = db.BrokerID;
            doc.FileName = fileName;
            doc.SizeBytes = fileBytes.Length;
            doc.Status = DocumentStatus.Uploaded;
            doc.StatusUpdatedOn = uploadedOn;
            doc.TransactionID = Guid.NewGuid();
            doc.UploadedOn = uploadedOn;
            doc.UserID = principal.UserId;
            doc.VendorID = db.OCRVendorId.Value;
            doc.Source = Source.Dropbox;
            data.AddDocument(doc);

            string fileDBKey = OCRService.GetDocumentFileDBKey(doc.BrokerID, doc.TransactionID);
            FileDBTools.WriteData(OCRService.DOCSTORAGELOCATION, fileDBKey, fileBytes);

            OCRService.EnqueueDocumentForUpload(doc);

            doc.Status = DocumentStatus.Queued;
            doc.StatusUpdatedOn = DateTime.Now;

            data.UpdateDocumentStatus(doc);

            return true;
        }

        /// <summary>
        /// Adds a file to the OCR queue if OCR is enabled and configured correctly.
        /// </summary>
        /// <param name="principal">The principal of the user adding the file. Will not work for internal staff.</param>
        /// <param name="fileName">The filename of the uplaoded file.</param>
        /// <param name="uploadedOn">The date the file was uploaded on.</param>
        /// <param name="fileBytes">The file size in bytes.</param>
        /// <param name="docType">The document type the user chose.</param>
        /// <param name="internalDescription">The description the user typed.</param>
        /// <param name="loanId">The loan file the user uploaded the file to.</param>
        /// <param name="appId">The app the user chose.</param>
        /// <param name="isScanBarcode">Whether the user wanted to scan for barcodes.</param>
        /// <returns>True if the file was handled, false otherwise.</returns>
        public static bool AddFileFromTPO(AbstractUserPrincipal principal, string fileName, DateTime uploadedOn, byte[] fileBytes, int? docType, string internalDescription, Guid loanId, Guid appId, bool isScanBarcode)
        {
            if (string.IsNullOrEmpty(ConstStage.MSMQ_OCR_Upload))
            {
                return false;
            }

            if (!docType.HasValue && !isScanBarcode)
            {
                throw CBaseException.GenericException("doctype is invalid.");
            }

            if (principal.IsLQBStaff())
            {
                throw CBaseException.GenericException("Internal admins cannot upload file.");
            }

            OCRDataSource data = new OCRDataSource();
            BrokerDB db = principal.BrokerDB;

            if (!db.IsOCREnabled || !db.OCRHasConfiguredAccount)
            {
                return false;
            }

            Vendor vendor = data.GetVendors(vendorId: db.OCRVendorId.Value).FirstOrDefault();

            if (vendor == null || !vendor.IsValid)
            {
                return false;
            }

            Document doc = new Document();
            doc.BrokerID = db.BrokerID;
            doc.FileName = fileName;
            doc.SizeBytes = fileBytes.Length;
            doc.Status = DocumentStatus.Uploaded;
            doc.StatusUpdatedOn = uploadedOn;
            doc.TransactionID = Guid.NewGuid();
            doc.UploadedOn = uploadedOn;
            doc.UserID = principal.UserId;
            doc.VendorID = db.OCRVendorId.Value;
            doc.Source = isScanBarcode ? Source.BarcodeScanner : Source.TPOPortal;
            doc.UploadDocTypeID = docType;
            doc.UploadInternalDescription = internalDescription;
            doc.UploadAppID = appId;
            doc.UploadLoanID = loanId;
            data.AddDocument(doc);

            string fileDBKey = OCRService.GetDocumentFileDBKey(doc.BrokerID, doc.TransactionID);
            FileDBTools.WriteData(OCRService.DOCSTORAGELOCATION, fileDBKey, fileBytes);

            OCRService.EnqueueDocumentForUpload(doc);

            doc.Status = DocumentStatus.Queued;
            doc.StatusUpdatedOn = DateTime.Now;

            data.UpdateDocumentStatus(doc);

            return true;
        }

        /// <summary>
        /// Requeues the given transaction id.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="transactionId">The transaction id.</param>
        public static void RequeueDoc(Guid brokerId, Guid transactionId)
        {
            OCRDataSource data = new OCRDataSource();
            Document doc = data.GetDocument(brokerId, transactionId);
            OCRService.EnqueueDocumentForUpload(doc);
        }

        /// <summary>
        /// Handles a vendor update request.
        /// </summary>
        /// <param name="doc">The data in the request.</param>
        /// <returns>The response bytes.</returns>
        public static byte[] HandleVendorUpdate(string doc)
        {
            LQBOCRUpdateRequest request = new LQBOCRUpdateRequest();

            try
            {
                XDocument xmldoc = XDocument.Parse(doc);
                request.ReadFrom(xmldoc);

                foreach (XElement element in xmldoc.Descendants().Where(e => e.Name == "Username" || e.Name == "Password"))
                {
                    element.Value = ConstAppDavid.FakePasswordDisplay;
                }

                Tools.LogInfo(xmldoc.ToString(SaveOptions.None));
            }
            catch (XmlException)
            {
                string invalidXml = Tools.RedactXmlUsingRegex(doc, "Username");
                invalidXml = Tools.RedactXmlUsingRegex(invalidXml, "Password");
                Tools.LogInfo(invalidXml);

                return LQBOCRUpdateResponse.CreateError("Invalid XML");
            }

            if (request.FormatError)
            {
                return LQBOCRUpdateResponse.CreateError("Invalid Request Format");
            }

            OCRDataSource db = new OCRDataSource();
            Document document = db.GetDocumentById(request.TransactionId.Value);

            if (document == null)
            {
                return LQBOCRUpdateResponse.CreateError("No Matching Transaction Id"); 
            }

            string key = OCRService.GetDocumentFileDBKey(document.BrokerID, document.TransactionID);

            if (request.IsError)
            {
                if (document.Source == Source.Dropbox)
                {
                    foreach (byte[] pdf in request.UnprocessedDocuments)
                    {
                        Dropbox.AddFile(document.UserID, document.BrokerID, document.FileName, document.UploadedOn, pdf);
                    }
                }
                else if (document.Source == Source.BarcodeScanner)
                {
                    DocMagicPDFManager.UploadDocument(document.UserID, document.BrokerID, document.UploadLoanID.Value, document.UploadAppID.Value, document.UploadInternalDescription, string.Empty, document.FileName);
                }
                else if (document.Source == Source.TPOPortal)
                {
                    EmployeeDB employeeDB = EmployeeDB.RetrieveByUserId(document.BrokerID, document.UserID);
                    EDocumentRepository repo = EDocumentRepository.GetSystemRepositoryForUser(document.BrokerID, document.UserID);

                    foreach (byte[] pdf in request.UnprocessedDocuments)
                    {
                        string tempFile = TempFileUtils.NewTempFilePath();
                        BinaryFileHelper.WriteAllBytes(tempFile, pdf);

                        EDocument edocument = repo.CreateDocument(E_EDocumentSource.UserUpload);
                        edocument.LoanId = document.UploadLoanID.Value;
                        edocument.DocumentTypeId = document.UploadDocTypeID.Value;
                        edocument.InternalDescription = document.UploadInternalDescription;
                        edocument.AppId = document.UploadAppID.Value;
                        edocument.IsUploadedByPmlUser = employeeDB.UserType == 'P';
                        edocument.EDocOrigin = edocument.IsUploadedByPmlUser ? E_EDocOrigin.PML : E_EDocOrigin.LO;
                        edocument.UpdatePDFContentOnSave(tempFile);
                        repo.Save(edocument);
                    }
                }            
            }

            db.DeleteDocument(document.TransactionID);
            FileDBTools.Delete(OCRService.DOCSTORAGELOCATION, key);

            return LQBOCRUpdateResponse.CreateSuccess();
        }

        /// <summary>
        /// Dequeues a file from the OCR queue and uploads it. 
        /// TODO will clean this up.
        /// </summary>
        /// <param name="timeout">The amount of time to wait for an entry from the queue.</param>
        /// <returns>Returns false if nothing was done or true if it processed one.</returns>
        public static bool UploadFile(TimeoutInSeconds timeout)
        {
            if (string.IsNullOrEmpty(ConstStage.MSMQ_OCR_Upload))
            {
                return false;
            }

            Document doc = OCRService.DequeueOrWait(timeout);

            if (doc == null)
            {
                return false;
            }

            Tools.LogInfo("Processing " + doc.TransactionID);

            OCRDataSource data = new OCRDataSource();
            BrokerDB db = BrokerDB.RetrieveById(doc.BrokerID);
            string error;
            if (!db.IsOCREnabled || !db.OCRHasConfiguredAccount)
            {
                error = "OCR Document checked but OCR Is not configured correctly. BrokerId : " + doc.BrokerID;
                OCRService.UpdateDocStatus(data, doc, DocumentStatus.FailedToSend, error);
                return true;
            }

            Vendor vendor = data.GetVendors(db.OCRVendorId.Value).FirstOrDefault();

            if (vendor == null || string.IsNullOrEmpty(vendor.UploadURL) || !vendor.IsValid)
            {
                error = "OCR Document checked but OCR vendor is not enabled for. BrokerId : " + doc.BrokerID + " " + doc.VendorID;
                OCRService.UpdateDocStatus(data, doc, DocumentStatus.FailedToSend, error);
                return true;
            }

            OCRService.UpdateDocStatus(data, doc, DocumentStatus.SendingToOCR);

            string key = OCRService.GetDocumentFileDBKey(doc.BrokerID, doc.TransactionID);
            string path = FileDBTools.CreateCopy(OCRService.DOCSTORAGELOCATION, key);

            LQBOCRRequest request = new DocumentUploadRequest(db.OCRUsername, db.OCRPassword.Value, doc.TransactionID, path, doc.UploadLoanRefNum);
            XDocument response;

            try
            {
                response = OCRService.SendRequest(vendor.UploadURL, request);
            }
            catch (Exception e)
            {
                OCRService.UpdateDocStatus(data, doc, DocumentStatus.FailedToSend, e.ToString());
                throw;
            }

            if (OCRService.IsVendorErrorResponse(response, "DocumentUploadResponse", out error))
            {
                OCRService.UpdateDocStatus(data, doc, DocumentStatus.FailedToSend, error);
            }
            else
            {
                OCRService.UpdateDocStatus(data, doc, DocumentStatus.SentToOCR);
            }

            Tools.LogInfo("Finished Processing doc " + doc.TransactionID);

            return true;
        }

        /// <summary>
        /// Gets a document portal url for the current user.
        /// </summary>
        /// <returns>The URL returned.</returns>
        public string GetDocumentReviewPortalUrl()
        {
            BrokerDB db = this.principal.BrokerDB;

            if (!db.IsOCREnabled || !db.OCRHasConfiguredAccount)
            {
                return null;
            }

            Vendor vendor = this.database.GetVendors(db.OCRVendorId.Value).FirstOrDefault();

            if (vendor == null || string.IsNullOrEmpty(vendor.UploadURL) || !vendor.IsValid)
            {
                return null;
            }

            LQBOCRRequest request = new DocumentPortalRequest(db.OCRUsername, db.OCRPassword.Value);
            XDocument response;

            try
            {
                response = OCRService.SendRequest(vendor.UploadURL, request);
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw CBaseException.GenericException(e.ToString());
            }

            string error;

            if (!OCRService.IsVendorErrorResponse(response, "DocumentPortalResponse", out error))
            {
                XElement url = response.Root.Element("DocumentPortalResponse").Element("URL");

                if (url == null)
                {
                    error = "URL not found.";
                }
                else
                {
                    return vendor.PortalURL + url.Value;
                }
            }

            throw CBaseException.GenericException(error);
        }

        /// <summary>
        /// Gets a list of OCR vendors.
        /// </summary>
        /// <returns>A list of OCR vendors.</returns>
        public IEnumerable<Vendor> GetVendors()
        {
            if (!this.principal.IsLQBStaff())
            {
                throw new AccessDenied();
            }

            return this.database.GetVendors();
        }

        /// <summary>
        /// Saves the given vendor.
        /// </summary>
        /// <param name="vendor">The vendor to save.</param>
        public void SaveVendor(Vendor vendor)
        {
            if (!this.principal.IsLQBStaff())
            {
                throw new AccessDenied();
            }

            this.database.Save(vendor);
        }

        /// <summary>
        /// Delete the vendor with the given id.
        /// </summary>
        /// <param name="id">The Id of the vendor to delete.</param>
        public void DeleteVendor(Guid id)
        {
            if (!this.principal.IsLQBStaff())
            {
                throw new AccessDenied();
            }

            this.database.DeleteVendor(id);
        }

        /// <summary>
        /// Sends the given request to the url specified.
        /// </summary>
        /// <param name="url">The URL to send to.</param>
        /// <param name="request">The request to send.</param>
        /// <returns>An XDocument with the response.</returns>
        private static XDocument SendRequest(string url, LQBOCRRequest request)
        {
            byte[] requestXmlBytes = request.GenerateRequest();

            XDocument xmlDoc = null;
            using (var xmlStream = new MemoryStream(requestXmlBytes))
            using (var xmlReader = new XmlTextReader(xmlStream))
            {
                xmlDoc = XDocument.Load(xmlReader);
            }

            foreach (XElement element in xmlDoc.Descendants().Where(e => e.Name == "Username" || e.Name == "Password"))
            {
                element.Value = ConstAppDavid.FakePasswordDisplay;
            }

            Tools.LogInfo(string.Format("Sending Upload Request to upload url {0}", url));
            Tools.LogInfo(xmlDoc.ToString(SaveOptions.None));

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.KeepAlive = false;
            webRequest.Method = "POST";
            webRequest.Timeout = 500000;
            webRequest.ContentType = "application/xml";
            webRequest.ContentLength = requestXmlBytes.Length;

            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();

            using (Stream stream = webRequest.GetRequestStream())
            {
                stream.Write(requestXmlBytes, 0, requestXmlBytes.Length);
            }

            XDocument response = null;
            string responseRaw = null;

            try
            {
                using (WebResponse webResponse = webRequest.GetResponse())
                using (StreamReader sr = new StreamReader(webResponse.GetResponseStream()))
                {
                    responseRaw = sr.ReadToEnd();
                }

                response = XDocument.Parse(responseRaw);
            }
            finally
            {
                if (!string.IsNullOrEmpty(responseRaw))
                {
                    Tools.LogInfo("UploadToOCR Response : " + responseRaw);
                }

                sw.Stop();
                Tools.LogInfo(string.Format("UploadToOCR GetResponseTook: {0}ms", sw.ElapsedMilliseconds));
            }

            return response;
        }

        /// <summary>
        /// Reads through the given response and returns a value indicating whether the response failed either for 
        /// OCR reasons or for formatting reasons. Returns the error message as well.
        /// </summary>
        /// <param name="response">The response.</param>
        /// <param name="responseType">The expected response node.</param>
        /// <param name="errorMessage">The error message if returned or generated if the format does not match.</param>
        /// <returns>A value indicating whether the request was successful.</returns>
        private static bool IsVendorErrorResponse(XDocument response, string responseType, out string errorMessage)
        {
            errorMessage = "Invalid Xml";

            XElement node = response.Element("LQBOCRResponse");
            if (node == null)
            {
                return true;
            }

            node = node.Element(responseType);
            if (node == null)
            {
                return true;
            }

            XElement statusElement = node.Element("Status");
            XElement statusMessageElement = node.Element("StatusMessage");

            if (statusElement == null)
            {
                return true;
            }

            bool isError = !statusElement.Value.Equals("OK", StringComparison.OrdinalIgnoreCase);
            errorMessage = string.Empty;

            if (isError)
            {
                if (statusMessageElement != null)
                {
                    errorMessage = statusElement.Value;
                }
                else
                {
                    errorMessage = "No Status Message received.";
                }
            }

            return isError;
        }

        /// <summary>
        /// Update the given documents status. Log error message if one is provided. 
        /// </summary>
        /// <param name="data">The data source to save.</param>
        /// <param name="doc">The document to update the status on.</param>
        /// <param name="status">The status for the document.</param>
        /// <param name="errorMessage">Error message to log.</param>
        private static void UpdateDocStatus(OCRDataSource data, Document doc, DocumentStatus status, string errorMessage = null)
        {
            doc.Status = status;
            doc.StatusUpdatedOn = DateTime.Now;
            data.UpdateDocumentStatus(doc);

            if (!string.IsNullOrEmpty(errorMessage))
            {
                Tools.LogError(errorMessage);
            }
        }

        /// <summary>
        /// Enqueues the given document for upload. 
        /// </summary>
        /// <param name="doc">The document to enqueue for upload.</param>
        private static void EnqueueDocumentForUpload(Document doc)
        {
            XDocument xdoc = OCRService.GenerateQueueForDoc(doc);
            Drivers.Gateways.MessageQueueHelper.SendXML(ConstStage.MSMQ_OCR_Upload, null, xdoc);
        }

        /// <summary>
        /// Loads a document from the details specified in the XDOcument.
        /// </summary>
        /// <param name="queueDocument">The xml document with the broker id and transaction id.</param>
        /// <returns>The Document or null if it doesnt exist.</returns>
        private static Document GetDocumentFrom(XDocument queueDocument)
        {
            XAttribute transactionIdAttr = queueDocument.Root.Attribute("TransactionID");
            XAttribute brokerIdAttr = queueDocument.Root.Attribute("BrokerID");

            if (transactionIdAttr == null || brokerIdAttr == null)
            {
                return null;
            }

            OCRDataSource data = new OCRDataSource();

            Guid brokerId = new Guid(brokerIdAttr.Value);
            Guid transactionId = new Guid(transactionIdAttr.Value);

            return data.GetDocument(brokerId, transactionId);
        }

        /// <summary>
        /// Removes a message from the queue or returns null if the timeout is expired.
        /// </summary>
        /// <param name="timeout">The timeout to wait for a new message.</param>
        /// <returns>The document in the queue or null.</returns>
        private static Document DequeueOrWait(TimeoutInSeconds timeout)
        {
            using (var messageQueue = Drivers.Gateways.MessageQueueHelper.PrepareForXML(ConstStage.MSMQ_OCR_Upload, false))
            {
                try
                {
                    DateTime arrivalTime;
                    XDocument doc = Drivers.Gateways.MessageQueueHelper.ReceiveXML(messageQueue, timeout.Value, out arrivalTime);
                    if (doc == null)
                    {
                        return null; // same behavior as MessageQueueException.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout
                    }

                    return OCRService.GetDocumentFrom(doc);
                }
                catch (MessageQueueException)
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Generates a XML Message for the queue to upload. 
        /// </summary>
        /// <param name="doc">The document to generate the queue entry for.</param>
        /// <returns>The XDocument with the data needed in the queue.</returns>
        private static XDocument GenerateQueueForDoc(Document doc)
        {
            XDocument xdoc = new XDocument();
            xdoc.Add(new XElement("OCRUpload"));
            xdoc.Root.Add(new XAttribute("TransactionID", doc.TransactionID));
            xdoc.Root.Add(new XAttribute("BrokerID", doc.BrokerID));

            return xdoc;
        }

        /// <summary>
        /// Gets a unique filedb key for the given broker id and transaction id.
        /// </summary>
        /// <param name="brokerId">The broker id of the broker who has the file.</param>
        /// <param name="transactionid">The transaction id of the file.</param>
        /// <returns>A unique file db key for the given broker and transaction ids.</returns>
        private static string GetDocumentFileDBKey(Guid brokerId, Guid transactionid)
        {
            return string.Format("OCR_DOC_{0:N}_{1:N}.pdf", brokerId, transactionid);
        }
    }
}
