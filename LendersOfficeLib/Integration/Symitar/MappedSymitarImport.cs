﻿// <copyright file="MappedSymitarImport.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2014-11-20 14:44:13 -0800
// </summary>
namespace LendersOffice.Integration.Symitar
{
    using System;
    using System.Collections.Generic;
    using Import;
    using Security;

    /// <summary>
    /// Represents a complete, mapped import of Symitar data to specific locations on a loan file.
    /// </summary>
    public class MappedSymitarImport
    {
        /// <summary>
        /// The length of time to save imports before expiring them.
        /// </summary>
        private static readonly TimeSpan SaveTimePeriod = TimeSpan.FromHours(8);

        /// <summary>
        /// Initializes a new instance of the <see cref="MappedSymitarImport"/> class.
        /// </summary>
        /// <param name="mappedValues">The values with their mappings to the loan file.</param>
        public MappedSymitarImport(IList<MappedValueIntoLqb> mappedValues)
        {
            this.MappedValues = mappedValues;
        }

        /// <summary>
        /// Gets the values with their mappings to the loan file.
        /// </summary>
        /// <value>The values with their mappings to the loan file.</value>
        public IList<MappedValueIntoLqb> MappedValues { get; private set; }

        /// <summary>
        /// Constructs an instance of <see cref="MappedSymitarImport"/> from the cached version.
        /// </summary>
        /// <param name="principal">The principal who initially loaded and saved the data.</param>
        /// <param name="key">The key of the data.</param>
        /// <returns>The reconstructed MappedSymitarImport.</returns>
        public static MappedSymitarImport ConstructFromSavedCopy(AbstractUserPrincipal principal, string key)
        {
            string text = DataAccess.AutoExpiredTextCache.GetUserString(principal, key);
            return string.IsNullOrEmpty(text) ? null : LendersOffice.Common.SerializationHelper.JsonNetDeserializeWithTypeNameHandling<MappedSymitarImport>(text);
        }

        /// <summary>
        /// Saves this instance to a cache so it can be reconstructed in a later request.
        /// </summary>
        /// <param name="principal">The principal to save the item under.</param>
        /// <returns>The key, used to retrieve the value later.</returns>
        public string SaveCopy(AbstractUserPrincipal principal)
        {
            var serializedThis = LendersOffice.Common.SerializationHelper.JsonNetSerializeWithTypeNameHandling(this);
            return DataAccess.AutoExpiredTextCache.InsertUserString(principal, serializedThis, SaveTimePeriod);
        }
    }
}
