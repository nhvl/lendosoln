﻿// <copyright file="SymitarServer.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2014-11-20 14:44:13 -0800
// </summary>
namespace LendersOffice.Integration.Symitar
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Text;
    using LendersOffice.Common;

    /// <summary>
    /// Handles sending and receiving messages from Symitar.
    /// </summary>
    internal class SymitarServer : IDisposable
    {
        /// <summary>
        /// The number of milliseconds to define the timeout while reading values from Symitar.
        /// </summary>
        private const int ReadTimeout = 750;

        /// <summary>
        /// The connection object handling the actual messages being sent.
        /// </summary>
        private readonly TcpClient client;

        /// <summary>
        /// The writer for sending messages to Symitar.
        /// </summary>
        private readonly TextWriter writer;

        /// <summary>
        /// The reader for receiving messages from Symitar.
        /// </summary>
        private readonly TextReader reader;

        /// <summary>
        /// The messages that we have already successfully read from Symitar, keyed based on the
        /// message identifier.
        /// </summary>
        private readonly Dictionary<string, SymitarMessage> messagesLoaded = new Dictionary<string, SymitarMessage>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// The current identifier of messages being sent from this instance to Symitar. This allows
        /// us to batch messages together but still ensure uniqueness of the id.
        /// </summary>
        private int messageId = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="SymitarServer" /> class.
        /// </summary>
        /// <param name="client">The connection object, already connected to its endpoint.</param>
        private SymitarServer(TcpClient client)
        {
            this.client = client;
            this.client.ReceiveTimeout = ReadTimeout;
            NetworkStream stream = this.client.GetStream();
            this.writer = new StreamWriter(stream, Encoding.ASCII);
            this.reader = new StreamReader(stream, Encoding.ASCII);
        }

        /// <summary>
        /// Creates a new <see cref="SymitarServer"/> object connected to an available endpoint.
        /// </summary>
        /// <param name="ip">The IP address of the Symitar instance to connect to.</param>
        /// <param name="ports">The ports of the Symitar instance accepting connections.</param>
        /// <returns>An initialized instance of <see cref="SymitarServer"/>.</returns>
        public static SymitarServer Create(IPAddress ip, IEnumerable<int> ports)
        {
            var portsToTry = ports.ToList();
            portsToTry.Shuffle(new Random());
            foreach (int port in portsToTry)
            {
                var endpoint = new IPEndPoint(ip, port);
                TcpClient client = new TcpClient();
                try
                {
                    client.Connect(endpoint);
                    Log("Connected to " + endpoint);
                    return new SymitarServer(client);
                }
                catch (SocketException)
                {
                    // Someone else already has this port
                    Log("Unable to connect to " + endpoint);
                    ((IDisposable)client).Dispose();
                }
            }

            throw new DataAccess.CBaseException("Unable to connect to Symitar.", "None of the configured ports worked.");
        }

        /// <summary>
        /// Disposes the current object.
        /// </summary>
        public void Dispose()
        {
            if (this.writer != null)
            {
                this.writer.Dispose();
            }

            if (this.reader != null)
            {
                this.reader.Dispose();
            }

            if (this.client != null)
            {
                ((IDisposable)this.client).Dispose(); // clients are disposable
            }
        }

        /// <summary>
        /// Writes the specified message to Symitar, assigning it an identifier used on retrieval.
        /// </summary>
        /// <param name="message">The message to send to Symitar.  Will have the <seealso cref="SymitarMessage.MessageID"/> value mutated.</param>
        /// <returns>The <seealso cref="SymitarMessage.MessageID"/> associated with the sent message, used to look up value returned by Symitar.</returns>
        public string Write(SymitarMessage message)
        {
            int currentMessageId = System.Threading.Interlocked.Increment(ref this.messageId); // overkill, but if we ever want thread security, this is the best way.
            string messageId = currentMessageId.ToString().PadLeft(4, '0');
            message.MessageID = messageId;
            LogMessage(message, isSend: true);
            this.writer.Write(message.ToString());
            this.writer.Flush();
            return messageId;
        }

        /// <summary>
        /// Retrieves the specified message.
        /// </summary>
        /// <param name="messageId">The identifier of the message to retrieve.</param>
        /// <returns>The message with the specified identifier.</returns>
        public SymitarMessage Retrieve(string messageId)
        {
            SymitarMessage response;
            if (this.messagesLoaded.TryGetValue(messageId, out response))
            {
                return response;
            }

            foreach (var message in ReadMessages(this.reader, this.messageId - this.messagesLoaded.Count))
            {
                this.messagesLoaded.Add(message.MessageID, message);
            }

            if (this.messagesLoaded.TryGetValue(messageId, out response))
            {
                return response;
            }
            else
            {
                throw new GenericUserErrorMessageException("No message with the ID=[" + messageId + "] was found.");
            }
        }

        /// <summary>
        /// Reads complete messages from Symitar from <paramref name="reader"/>.
        /// </summary>
        /// <param name="reader">The reader to use when reading.</param>
        /// <param name="expectedMessageCount">The number of messages to read.</param>
        /// <returns>A collection of messages.</returns>
        private static ICollection<SymitarMessage> ReadMessages(TextReader reader, int expectedMessageCount)
        {
            List<SymitarMessage> messages = new List<SymitarMessage>(expectedMessageCount);
            StringBuilder partialMessage = new StringBuilder();
            char[] buffer = new char[1024];

            while (messages.Count < expectedMessageCount)
            {
                int charactersRead = reader.Read(buffer, 0, buffer.Length);
                Log("Read (raw): [" + new string(buffer, 0, charactersRead) + "]");
                var messagesRead = SymitarMessage.ParseCompleteMessages(buffer, charactersRead, partialMessage);
                messages.AddRange(messagesRead);
                foreach (var message in messagesRead)
                {
                    LogMessage(message, isSend: false);
                }
            }

            return messages;
        }

        /// <summary>
        /// Logs the sending of a message to Symitar.
        /// </summary>
        /// <param name="message">The message to log.</param>
        /// <param name="isSend">A boolean value indicating whether the message is being sent or received.</param>
        private static void LogMessage(SymitarMessage message, bool isSend)
        {
            Log((isSend ? "Sending:" : "Receiving:") + Environment.NewLine + message.ToString(hidePassword: true));
        }

        /// <summary>
        /// Logs a string as a Symitar log.
        /// </summary>
        /// <param name="message">The string to log.</param>
        private static void Log(string message)
        {
            DataAccess.Tools.LogInfo("Symitar", message);
        }
    }
}
