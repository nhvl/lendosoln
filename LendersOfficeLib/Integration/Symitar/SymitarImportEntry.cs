﻿﻿// <copyright file="SymitarImportEntry.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Michael Leinweaver
//    Date:   12/7/2015
// </summary>
namespace LendersOffice.Integration.Symitar
{
    /// <summary>
    /// Provides a wrapper for the datapoints imported
    /// from Symitar to apply to a LendingQB loan, including
    /// a friendly description of the field to import, the
    /// current LendingQB loan value, the Symitar datapoint
    /// value, and whether the entry should be excluded from
    /// the import.
    /// </summary>
    public class SymitarImportEntry
    {
        /// <summary>
        /// Gets or sets the value for the friendly description
        /// of the entry.
        /// </summary>
        /// <value>The string friendly description of the entry.</value>
        public string FieldDescription { get; set; }

        /// <summary>
        /// Gets or sets the value for the current LendingQB loan value
        /// for the entry.
        /// </summary>
        /// <value>
        /// The string current LendingQB loan value for the entry.
        /// </value>
        public string CurrentValue { get; set; }

        /// <summary>
        /// Gets or sets the value for the Symitar imported datapoint value
        /// for the entry.
        /// </summary>
        /// <value>
        /// The string Symitar imported datapoint value for the entry.
        /// </value>
        public string ImportValue { get; set; }

        /// <summary>
        /// Gets or sets the value indicating whether the entry should be
        /// included in the import.
        /// </summary>
        /// <value>
        /// True if the entry should be included in the import, false otherwise.
        /// </value>
        public bool Include { get; set; }
    }
}
