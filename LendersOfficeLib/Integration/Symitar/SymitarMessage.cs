﻿// <copyright file="SymitarMessage.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   11/20/2014 11:40:19 AM
// </summary>
namespace LendersOffice.Integration.Symitar
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Common;

    /// <summary>
    /// The message type exchanged with Symitar.
    /// </summary>
    public enum SymitarMessageType
    {
        /// <summary>
        /// An inquiry message.
        /// </summary>
        Inquiry,

        /// <summary>
        /// A <c>specfile</c> request message.
        /// </summary>
        SpecfileRequest,

        /// <summary>
        /// A file maintenance request message.
        /// </summary>
        FileMaintenanceRequest,

        /// <summary>
        /// A transaction message.
        /// </summary>
        TransactionRequest,

        /// <summary>
        /// An administrative request.
        /// </summary>
        AdministrativeRequest,

        /// <summary>
        /// A handshake request message.
        /// </summary>
        Handshake,

        /// <summary>
        /// An unknown request message.
        /// </summary>
        Unknown
    }

    /// <summary>
    /// Represents a message being sent to or from the Symitar SymConnect Interface.
    /// </summary>
    internal class SymitarMessage
    {
        /// <summary>
        /// The character separating fields within the message.
        /// </summary>
        internal const char FieldSeparator = '~';

        /// <summary>
        /// The character signifying the end of the message.
        /// </summary>
        internal const char EndOfMessage = '\n';

        /// <summary>
        /// The character key corresponding to the password in the message.
        /// </summary>
        private const char PasswordFieldKey = 'F';

        /// <summary>
        /// The character key corresponding to the parameter fields in the message.
        /// </summary>
        private const char ParameterFieldKey = 'J';

        /// <summary>
        /// The dictionary holding the message fields with their associated values.
        /// </summary>
        private Dictionary<char, Fields.CharacterFieldValue> messageFields = new Dictionary<char, Fields.CharacterFieldValue>();

        /// <summary>
        /// Initializes a new instance of the <see cref="SymitarMessage"/> class.
        /// </summary>
        /// <param name="messageType">The type of message to create.</param>
        public SymitarMessage(SymitarMessageType messageType)
            : this()
        {
            this.MessageTypeCode = new TypeCode(messageType);
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="SymitarMessage"/> class from being created.
        /// </summary>
        private SymitarMessage()
        {
            this.ParameterFields = new List<KeyValuePair<string, string>>();
        }

        /// <summary>
        /// Gets the code of the type of message.
        /// </summary>
        /// <value>The code of the type of message.</value>
        public TypeCode MessageTypeCode { get; private set; }

        /// <summary>
        /// Gets or sets the message identifier.
        /// </summary>
        /// <value>The message identifier.</value>
        public string MessageID { get; set; }

        /// <summary>
        /// Gets or sets the device number.
        /// </summary>
        /// <value>The device number. Valid range is 1 to 32767.</value>
        public short? DeviceNumber
        {
            get { return this.messageFields.ContainsKey('A') ? (short?)short.Parse(this.messageFields['A'].ToString()) : null; }
            set { this.SetField('A', value.HasValue ? value.ToString() : null); }
        }

        /// <summary>
        /// Gets or sets the device type.
        /// </summary>
        /// <value>The device type.</value>
        public string DeviceType
        {
            get { return this.GetField('B'); }
            set { this.SetField('B', value); }
        }

        /// <summary>
        /// Gets or sets the device location.
        /// </summary>
        /// <value>The device location.</value>
        public string DeviceLocation
        {
            get { return this.GetField('C'); }
            set { this.SetField('C', value); }
        }

        /// <summary>
        /// Gets or sets the account number.
        /// </summary>
        /// <value>The account number.</value>
        public string AccountNumber
        {
            get { return this.GetField('D'); }
            set { this.SetField('D', value); }
        }

        /// <summary>
        /// Gets or sets the user code.
        /// </summary>
        /// <value>The user code.</value>
        public string UserCode
        {
            get { return this.GetField('E'); }
            set { this.SetField('E', value); }
        }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password.</value>
        public string Password
        {
            get { return this.GetField(SymitarMessage.PasswordFieldKey); }
            set { this.SetField(SymitarMessage.PasswordFieldKey, value); }
        }

        /// <summary>
        /// Gets or sets the file name.
        /// </summary>
        /// <value>The file name.</value>
        public string FileName
        {
            get { return this.GetField('G'); }
            set { this.SetField('G', value); }
        }

        /// <summary>
        /// Gets or sets the hierarchic record path.
        /// </summary>
        /// <value>The hierarchic record path.</value>
        public string HierarchicRecordPath
        {
            get { return this.GetField('H'); }
            set { this.SetField('H', value); }
        }

        /// <summary>
        /// Gets the parameter fields and their values.
        /// </summary>
        /// <value>The parameter fields and their values.</value>
        /// <remarks>
        /// We initially thought this was going to be a dictionary, but exchanges like the following do not fit:<para/>
        /// <code>IQ~0001~A20978~BLENDINGQB~DCARD~F50500000017339~HACCOUNT#0000017339:NAME~JLAST=ALL</code><para/>
        /// <code>RSIQ~0001~K0~JLAST=TESTCASE~JLAST=TESTCASE~JLAST=TESTER</code>
        /// </remarks>
        public List<KeyValuePair<string, string>> ParameterFields { get; private set; }

        /// <summary>
        /// Gets or sets the response status.
        /// </summary>
        /// <value>The response status.</value>
        public string ResponseStatus
        {
            get { return this.GetField('K'); }
            set { this.SetField('K', value); }
        }

        /// <summary>
        /// Gets or sets miscellaneous data.
        /// </summary>
        /// <value>Miscellaneous data.</value>
        public string MiscellaneousData
        {
            get { return this.GetField('L'); }
            set { this.SetField('L', value); }
        }

        /// <summary>
        /// Gets or sets the administrative request type.<para/>
        /// This specifies the action to be taken in response to the
        /// administrative request message.
        /// </summary>
        /// <value>The administrative request type.</value>
        public string AdmistrativeRequestType
        {
            get { return this.GetField('M'); }
            set { this.SetField('M', value); }
        }

        /// <summary>
        /// Gets or sets the client system user number.
        /// </summary>
        /// <value>The client system user number.</value>
        public string ClientSystemUserNumber
        {
            get { return this.GetField('N'); }
            set { this.SetField('N', value); }
        }

        /// <summary>
        /// Gets or sets the second password.
        /// </summary>
        /// <value>The second password.</value>
        public string SecondPassword
        {
            get { return this.GetField('P'); }
            set { this.SetField('P', value); }
        }

        /// <summary>
        /// Parses a message from Symitar into a corresponding object representation.
        /// </summary>
        /// <param name="message">The string to parse into a <see cref="SymitarMessage"/></param>
        /// <returns>A parsed message from Symitar, or null if <paramref name="message"/> is the empty string.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="message"/> is null.</exception>
        public static SymitarMessage ParseMessage(string message)
        {
            if (message == null)
            {
                throw new ArgumentNullException("message");
            }
            else if (message == string.Empty)
            {
                return null;
            }

            if (message[message.Length - 1] == SymitarMessage.EndOfMessage)
            {
                message = message.Remove(message.Length - 1);
            }

            if (message.Contains(SymitarMessage.EndOfMessage))
            {
                throw new GenericUserErrorMessageException("More than one message found");
            }

            string[] messageParts = message.Split(new char[] { SymitarMessage.FieldSeparator }, StringSplitOptions.RemoveEmptyEntries);

            SymitarMessage parsedMessage = new SymitarMessage();
            parsedMessage.MessageTypeCode = TypeCode.Parse(messageParts[0]);

            if (messageParts.Length > 1)
            {
                parsedMessage.MessageID = messageParts[1];
                var parameterFieldValues = new List<KeyValuePair<string, string>>();
                foreach (var field in messageParts.Skip(2).Select(messageField => new { Key = messageField[0], Value = messageField.Substring(1) }))
                {
                    if (field.Key == ParameterFieldKey)
                    {
                        int equalsSignIndex = field.Value.IndexOf('=');
                        var fieldData = equalsSignIndex <= 0
                            ? new KeyValuePair<string, string>(field.Value, null)
                            : new KeyValuePair<string, string>(field.Value.Remove(equalsSignIndex), field.Value.Substring(equalsSignIndex + 1));

                        parameterFieldValues.Add(fieldData);
                    }
                    else
                    {
                        parsedMessage.SetField(field.Key, field.Value);
                    }
                }

                parsedMessage.ParameterFields.AddRange(parameterFieldValues.Where(field => field.Key != "9999")); // 9999 is used to mark invalid fields
            }

            return parsedMessage;
        }

        /// <summary>
        /// Parses <paramref name="buffer"/> for messages, through the specified number of characters read. A partial message read previously may be specified.
        /// </summary>
        /// <param name="buffer">The character array containing one or more messages.</param>
        /// <param name="charactersToParse">The number of characters in <paramref name="buffer"/> which should be considered for parsing.</param>
        /// <param name="partialMessage">A partial message previously read from Symitar; may be altered to contain a different partial message.</param>
        /// <returns>A collection of Symitar messages.</returns>
        public static ICollection<SymitarMessage> ParseCompleteMessages(char[] buffer, int charactersToParse, StringBuilder partialMessage)
        {
            List<SymitarMessage> completeMessages = new List<SymitarMessage>();
            int messageStartIndex = 0;
            foreach (int endOfMessageIndex in buffer.Take(charactersToParse).SelectIndicesWhere(ch => ch == SymitarMessage.EndOfMessage))
            {
                int messageLength = endOfMessageIndex + 1 - messageStartIndex; // include the EOM
                string completedMessage = GetSubstring(buffer, messageStartIndex, messageLength, partialMessage);
                messageStartIndex += messageLength;
                if (!string.IsNullOrWhiteSpace(completedMessage))
                {
                    completeMessages.Add(SymitarMessage.ParseMessage(completedMessage));
                }
            }

            if (messageStartIndex < charactersToParse)
            {
                partialMessage.Append(buffer, messageStartIndex, charactersToParse - messageStartIndex);
            }

            return completeMessages;
        }

        /// <summary>
        /// Returns a string that represents the current object to be sent to Symitar.
        /// </summary>
        /// <returns>A string that represents the current object to be sent to Symitar.</returns>
        public override string ToString()
        {
            return this.ToString(false);
        }

        /// <summary>
        /// Returns a string that represents the current object to be sent to Symitar, allowing the <see cref="Password"/> to be masked.
        /// </summary>
        /// <param name="hidePassword">A value indicating whether the password should be masked.</param>
        /// <returns>A string that represents the current object to be sent to Symitar.</returns>
        public string ToString(bool hidePassword)
        {
            var data = new List<string>(1 + this.messageFields.Count + this.ParameterFields.Count);
            data.AddIfNotNullOrEmpty(this.MessageTypeCode.ToString());
            data.AddIfNotNullOrEmpty(this.MessageID);
            data.AddRange(SelectMessageFields(this.messageFields.Where(pair => pair.Key < ParameterFieldKey), hidePassword));

            data.AddRange(this.ParameterFields.Select(pair => ParameterFieldKey + pair.Key + (pair.Value != null ? "=" + pair.Value : null)));

            data.AddRange(SelectMessageFields(this.messageFields.Where(pair => pair.Key > ParameterFieldKey), hidePassword));
            data[data.Count - 1] += EndOfMessage; // do this now to save allocating the big string twice
            return string.Join(FieldSeparator.ToString(), data.ToArray());
        }

        /// <summary>
        /// Creates a fake response message for testing or internal consumption.
        /// </summary>
        /// <param name="messageType">The type of message.</param>
        /// <returns>A fake response message.</returns>
        internal static SymitarMessage CreateFakeResponse(SymitarMessageType messageType)
        {
            return new SymitarMessage()
            {
                MessageTypeCode = new TypeCode(messageType, isResponse: true),
                ResponseStatus = "0" // success
            };
        }

        /// <summary>
        /// Selects message fields into an enumeration of the combined values.
        /// </summary>
        /// <param name="messageFields">The message fields to combine.</param>
        /// <param name="hidePassword">A value indicating whether the password should be masked.</param>
        /// <returns>An enumeration of the fields.</returns>
        private static IEnumerable<string> SelectMessageFields(IEnumerable<KeyValuePair<char, Fields.CharacterFieldValue>> messageFields, bool hidePassword)
        {
            return messageFields.Select(
                pair =>
                    pair.Key
                    + (hidePassword && pair.Key == SymitarMessage.PasswordFieldKey
                        ? LendersOffice.Constants.ConstAppDavid.FakePasswordDisplay
                        : pair.Value.ToString()));
        }

        /// <summary>
        /// Reads the specified substring from the buffer.  If there is a start to the string in
        /// <paramref name="prefixString"/>, join these and clear the partial string.
        /// </summary>
        /// <param name="buffer">The character array containing the characters of the string.</param>
        /// <param name="startIndex">The index of <paramref name="buffer"/> where the substring begins.</param>
        /// <param name="count">The number of characters to retrieve from <paramref name="buffer"/> into the substring.</param>
        /// <param name="prefixString">A prefix to the value in the buffer; will be cleared if read.</param>
        /// <returns>The substring of <paramref name="buffer"/>, prefixed by <paramref name="prefixString"/>.</returns>
        private static string GetSubstring(char[] buffer, int startIndex, int count, StringBuilder prefixString)
        {
            if (prefixString != null && prefixString.Length > 0)
            {
                prefixString.Append(buffer, startIndex, count);
                string completeMessage = prefixString.ToString();
                prefixString.Clear();
                return completeMessage;
            }
            else
            {
                return new string(buffer, startIndex, count);
            }
        }

        /// <summary>
        /// Retrieves a field from the internal dictionary.
        /// </summary>
        /// <param name="key">The key to the dictionary value.</param>
        /// <returns>The value at the key, or null if not found.</returns>
        private string GetField(char key)
        {
            Fields.CharacterFieldValue value;
            return this.messageFields.TryGetValue(key, out value) ? value.ToString() : null;
        }

        /// <summary>
        /// Sets a field in the internal dictionary.  If the value is null, removes the key.
        /// </summary>
        /// <param name="key">The key to the dictionary value.</param>
        /// <param name="value">The value to be inserted in the dictionary.</param>
        private void SetField(char key, string value)
        {
            if (key < 'A' || key > 'Z')
            {
                throw new GenericUserErrorMessageException("Invalid Key: [" + key + "]");
            }

            if (value == null)
            {
                this.messageFields.Remove(key);
            }
            else
            {
                this.messageFields[key] = new Fields.CharacterFieldValue(value);
            }
        }

        /// <summary>
        /// Encapsulates a message type and boolean value indicating whether the message is a response to another message.
        /// </summary>
        public class TypeCode
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TypeCode"/> class.
            /// </summary>
            /// <param name="messageType">The type of message.</param>
            /// <param name="isResponse">Indicates whether this message is a response to another message.</param>
            public TypeCode(SymitarMessageType messageType, bool isResponse = false)
            {
                this.MessageType = messageType;
                this.IsResponse = isResponse;
            }

            /// <summary>
            /// Gets a value indicating whether this instance is a response from the server.
            /// </summary>
            /// <value>A value indicating whether this instance is a response from the server.</value>
            public bool IsResponse { get; private set; }

            /// <summary>
            /// Gets the type of message.
            /// </summary>
            /// <value>The type of message.</value>
            public SymitarMessageType MessageType { get; private set; }

            /// <summary>
            /// Parses a Symitar <see cref="TypeCode"/> into the correct message type information.
            /// </summary>
            /// <param name="typeCode">The type code string in Symitar's format.</param>
            /// <returns>The type code detected based on <paramref name="typeCode"/>.</returns>
            public static TypeCode Parse(string typeCode)
            {
                bool isResponseTypeCode = typeCode.StartsWith("RS");
                return new TypeCode(FromSymitar(typeCode.Substring(isResponseTypeCode ? 2 : 0)), isResponseTypeCode);
            }

            /// <summary>
            /// Returns a string that represents the current <see cref="TypeCode"/>.
            /// </summary>
            /// <returns>A string that represents the current <see cref="TypeCode"/>.</returns>
            public override string ToString()
            {
                return (this.IsResponse ? "RS" : string.Empty) + ToSymitar(this.MessageType);
            }

            /// <summary>
            /// Maps the message type to the representation used by Symitar.
            /// </summary>
            /// <param name="messageType">The message type.</param>
            /// <returns>The Symitar representation of the inquiry type.</returns>
            /// <exception cref="DataAccess.UnhandledEnumException"><paramref name="messageType"/> does not correspond to an expected message type to send to Symitar.</exception>
            private static string ToSymitar(SymitarMessageType messageType)
            {
                switch (messageType)
                {
                    case SymitarMessageType.Inquiry: return "IQ";
                    case SymitarMessageType.SpecfileRequest: return "SF";
                    case SymitarMessageType.FileMaintenanceRequest: return "FM";
                    case SymitarMessageType.TransactionRequest: return "TR";
                    case SymitarMessageType.AdministrativeRequest: return "AD";
                    case SymitarMessageType.Handshake: return "HANDSHAKE";
                    default:
                        throw new DataAccess.UnhandledEnumException(messageType);
                }
            }

            /// <summary>
            /// Maps the message types we expect to receive from Symitar to the corresponding message types.
            /// </summary>
            /// <param name="symitarMessageType">The message type we receive from Symitar.</param>
            /// <returns>The message type corresponding to the type.</returns>
            /// <exception cref="LendersOffice.Common.GenericUserErrorMessageException"><paramref name="symitarMessageType"/> does not match a <see cref="SymitarMessageType"/> expected from Symitar.</exception>
            private static SymitarMessageType FromSymitar(string symitarMessageType)
            {
                switch (symitarMessageType)
                {
                    case "IQ": return SymitarMessageType.Inquiry;
                    case "SF": return SymitarMessageType.SpecfileRequest;
                    case "FM": return SymitarMessageType.FileMaintenanceRequest;
                    case "TR": return SymitarMessageType.TransactionRequest;
                    case "AD": return SymitarMessageType.AdministrativeRequest;
                    case "HANDSHAKE": return SymitarMessageType.Handshake;
                    case "WHAT?": return SymitarMessageType.Unknown;
                    default:
                        throw new LendersOffice.Common.GenericUserErrorMessageException("Unhandled Symitar MessageType: [" + symitarMessageType + "]");
                }
            }
        }
    }
}
