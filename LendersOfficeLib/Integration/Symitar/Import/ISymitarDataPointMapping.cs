﻿// <copyright file="ISymitarDataPointMapping.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2014-11-20 14:44:13 -0800
// </summary>
namespace LendersOffice.Integration.Symitar.Import
{
    using System.Collections.Generic;

    /// <summary>
    /// Encapsulates a data point to import from Symitar.
    /// </summary>
    public interface ISymitarDataPointMapping
    {
        /// <summary>
        /// Gets an enumeration over the field names within the data point.
        /// </summary>
        /// <value>An enumeration over the field names within the data point.</value>
        IEnumerable<string> FieldNames { get; }

        /// <summary>
        /// Gets the string representing this data point that will be imported.
        /// </summary>
        /// <param name="rawValues">The raw field/value information from Symitar.</param>
        /// <param name="converter">A converter to determine formatting on the output strings.</param>
        /// <returns>A string representing the value from Symitar, intended for import.</returns>
        string GetString(Dictionary<string, string> rawValues, DataAccess.LosConvert converter);
    }
}
