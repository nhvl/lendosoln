﻿// <copyright file="MappedValueIntoLqb.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2014-11-20 14:44:13 -0800
// </summary>
namespace LendersOffice.Integration.Symitar.Import
{
    /// <summary>
    /// Represents a mapped value of a string of Symitar data to a specific location on a loan file.
    /// </summary>
    public class MappedValueIntoLqb
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MappedValueIntoLqb"/> class.
        /// </summary>
        /// <param name="value">The value to import into LendingQB.</param>
        /// <param name="mapping">The mapping of the value to a specific data point on a loan file.</param>
        public MappedValueIntoLqb(string value, DataPointMapping mapping)
        {
            this.Value = value;
            this.Mapping = mapping;
        }

        /// <summary>
        /// Gets the value to import into LendingQB.
        /// </summary>
        /// <value>The value to import into LendingQB.</value>
        public string Value { get; private set; }

        /// <summary>
        /// Gets the mapping of the value to a specific data point on a loan file.
        /// </summary>
        /// <value>The mapping of the value to a specific data point on a loan file.</value>
        public DataPointMapping Mapping { get; private set; }
    }
}
