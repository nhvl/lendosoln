﻿// <copyright file="SymitarToLqbMapping.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2014-11-20 14:44:13 -0800
// </summary>
namespace LendersOffice.Integration.Symitar.Import
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using Fields;

    /// <summary>
    /// Defines a complete mapping from the data of Symitar into LendingQB.
    /// </summary>
    public class SymitarToLqbMapping
    {
        /// <summary>
        /// The account number of the member to import from.
        /// </summary>
        private readonly string memberAccountNumber;

        /// <summary>
        /// Indicates whether the data is bound to the borrower or co-borrower.
        /// </summary>
        private readonly bool isForBorrower;

        /// <summary>
        /// Initializes a new instance of the <see cref="SymitarToLqbMapping"/> class.
        /// </summary>
        /// <param name="memberAccountNumber">The account number of the member to import from.</param>
        /// <param name="isForBorrower">Indicates whether the data is bound to the borrower or co-borrower.</param>
        public SymitarToLqbMapping(string memberAccountNumber, bool isForBorrower)
        {
            this.memberAccountNumber = memberAccountNumber;
            this.isForBorrower = isForBorrower;
        }

        /// <summary>
        /// Gets the Record mappings.
        /// </summary>
        /// <value>The record mappings.</value>
        public IReadOnlyList<RecordMapping> RecordMappings
        {
            get
            {
                return new RecordMapping[]
                {
                    new RecordMapping(
                        this.memberAccountNumber,
                        "NAME",
                        "TYPE",
                        type => type == "0",
                        CreateNameMappings(this.isForBorrower)),
                };
            }
        }

        /// <summary>
        /// Creates the mappings from Symitar Name records to corresponding LendingQB fields.
        /// </summary>
        /// <param name="isForBorrower">Specifies whether these mappings are for the borrower or co-borrower.</param>
        /// <returns>An enumeration of mappings from data points on the Name record to LendingQB.</returns>
        private static IEnumerable<Tuple<ISymitarDataPointMapping, DataPointMapping>> CreateNameMappings(bool isForBorrower)
        {
            return new Tuple<ISymitarDataPointMapping, DataPointMapping>[]
            {
                CreateTuple(
                    new SymitarFieldMapping("FIRST", FieldMaker.Character),
                    PageDataUtilitiesMapping.BorrowerField("First Name", "aBFirstNm", "aCFirstNm", isForBorrower)),
                CreateTuple(
                    new SymitarFieldMapping("MIDDLE", FieldMaker.Character),
                    PageDataUtilitiesMapping.BorrowerField("Middle Name", "aBMidNm", "aCMidNm", isForBorrower)),
                CreateTuple(
                    new SymitarFieldMapping("LAST", FieldMaker.Character),
                    PageDataUtilitiesMapping.BorrowerField("Last Name", "aBLastNm", "aCLastNm", isForBorrower)),
                CreateTuple(
                    new SymitarFieldMapping("SUFFIX", FieldMaker.Character),
                    PageDataUtilitiesMapping.BorrowerField("Suffix", "aBSuffix", "aCSuffix", isForBorrower)),
                CreateTuple(
                    new SymitarFieldMapping("HOMEPHONE", FieldMaker.Character),
                    PageDataUtilitiesMapping.BorrowerField("Home Phone", "aBHPhone", "aCHPhone", isForBorrower)),
                CreateTuple(
                    new SymitarCharacterFieldConcatenation("WORKPHONE", "WORKPHONEEXTENSION"),
                    PageDataUtilitiesMapping.BorrowerField("Work Phone", "aBBusPhone", "aCBusPhone", isForBorrower)),
                CreateTuple(
                    new SymitarFieldMapping("MOBILEPHONE", FieldMaker.Character),
                    PageDataUtilitiesMapping.BorrowerField("Cell Phone", "aBCellPhone", "aCCellPhone", isForBorrower)),
                CreateTuple(
                    new SymitarFieldMapping("EMAIL", FieldMaker.Character),
                    PageDataUtilitiesMapping.BorrowerField("Email", "aBEmail", "aCEmail", isForBorrower)),
                CreateTuple(
                    new SymitarFieldMapping("BIRTHDATE", FieldMaker.Date),
                    PageDataUtilitiesMapping.BorrowerField("Date of Birth", "aBDob", "aCDob", isForBorrower)),
                CreateTuple(
                    new SymitarFieldMapping("SSN", FieldMaker.Character),
                    PageDataUtilitiesMapping.BorrowerField("SSN", "aBSsn", "aCSsn", isForBorrower)),
                CreateTuple(
                    new SymitarFieldMapping("EMPLOYERNAME", FieldMaker.Character),
                    new CurrentEmploymentMapping.EmployerName(isForBorrower)),
                CreateTuple(
                    new SymitarFieldMapping("OCCUPATION", FieldMaker.Character),
                    new CurrentEmploymentMapping.JobTitle(isForBorrower)),
                CreateTuple(
                    new SymitarFieldMapping("CURRGROSSMONTHPAY", FieldMaker.Money),
                    PageDataUtilitiesMapping.BorrowerField("Base Income", "aBBaseI", "aCBaseI", isForBorrower)),
                CreateTuple(
                    new SymitarFieldMapping("STREET", FieldMaker.Character),
                    PageDataUtilitiesMapping.BorrowerField("Street Address", "aBAddr", "aCAddr", isForBorrower)),
                CreateTuple(
                    new SymitarFieldMapping("CITY", FieldMaker.Character),
                    PageDataUtilitiesMapping.BorrowerField("City", "aBCity", "aCCity", isForBorrower)),
                CreateTuple(
                    new SymitarFieldMapping("STATE", FieldMaker.Character),
                    PageDataUtilitiesMapping.BorrowerField("State", "aBState", "aCState", isForBorrower)),
                CreateTuple(
                    new SymitarFieldMapping("ZIPCODE", FieldMaker.Character),
                    PageDataUtilitiesMapping.BorrowerField("Zip Code", "aBZip", "aCZip", isForBorrower)),
            };
        }

        /// <summary>
        /// Creates a tuple out of the items, using specific types to avoid
        /// issues of covariance.
        /// </summary>
        /// <param name="symitarDataPoint">The data point mapping in Symitar to load from.</param>
        /// <param name="lqbMapping">The mapping into the loan file.</param>
        /// <returns>A tuple containing both items.</returns>
        private static Tuple<ISymitarDataPointMapping, DataPointMapping> CreateTuple(ISymitarDataPointMapping symitarDataPoint, DataPointMapping lqbMapping)
        {
            return Tuple.Create(symitarDataPoint, lqbMapping);
        }
    }
}
