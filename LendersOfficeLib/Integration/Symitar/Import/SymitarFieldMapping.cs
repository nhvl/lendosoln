﻿// <copyright file="SymitarFieldMapping.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2016-04-18 08:43:43 -0700
// </summary>
namespace LendersOffice.Integration.Symitar.Import
{
    using System.Collections.Generic;

    /// <summary>
    /// Represents a field from Symitar to import as a part of a record.
    /// </summary>
    public class SymitarFieldMapping : ISymitarDataPointMapping
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SymitarFieldMapping"/> class.
        /// </summary>
        /// <param name="name">The name of the field.</param>
        /// <param name="maker">The <seealso cref="Fields.FieldMaker"/> corresponding to this field's data type.</param>
        public SymitarFieldMapping(string name, Fields.FieldMaker maker)
        {
            this.Name = name;
            this.Maker = maker;
        }

        /// <summary>
        /// Gets the name of the field in Symitar, documented as the "mnemonic".
        /// </summary>
        /// <value>The name of the field in Symitar.</value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets a <seealso cref="Fields.FieldMaker"/> capable of turning a raw
        /// string value into a <see cref="Fields.ISymitarFieldValue"/>.
        /// </summary>
        /// <value>The generator for fields of this type.</value>
        public Fields.FieldMaker Maker { get; private set; }

        /// <summary>
        /// Gets an enumeration over the field names within the data point.
        /// </summary>
        /// <value>An enumeration over the field names within the data point.</value>
        public IEnumerable<string> FieldNames
        {
            get { yield return this.Name; }
        }

        /// <summary>
        /// Gets the string representing this data point that will be imported.
        /// </summary>
        /// <param name="rawValues">The raw field/value information from Symitar.</param>
        /// <param name="converter">A converter to determine formatting on the output strings.</param>
        /// <returns>A string representing the value from Symitar, intended for import.</returns>
        public string GetString(Dictionary<string, string> rawValues, DataAccess.LosConvert converter)
        {
            var symitarField = this.Maker.FromSymitar(rawValues[this.Name]);
            return symitarField.ToLQBString(converter);
        }
    }
}
