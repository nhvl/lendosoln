﻿// <copyright file="SymitarCharacterFieldConcatenation.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2014-11-20 14:44:13 -0800
// </summary>
namespace LendersOffice.Integration.Symitar.Import
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Defines a mapping from Symitar's system into a value for LendingQB to consume.
    /// </summary>
    public class SymitarCharacterFieldConcatenation : ISymitarDataPointMapping
    {
        /// <summary>
        /// The list of fields concatenated by the current instance.
        /// </summary>
        private readonly IEnumerable<string> fieldNames;

        /// <summary>
        /// Initializes a new instance of the <see cref="SymitarCharacterFieldConcatenation"/> class.
        /// </summary>
        /// <param name="fieldNames">The names of the fields.</param>
        public SymitarCharacterFieldConcatenation(params string[] fieldNames)
        {
            this.fieldNames = fieldNames;
        }

        /// <summary>
        /// Gets an enumeration over the field names within the data point.
        /// </summary>
        /// <value>An enumeration over the field names within the data point.</value>
        public IEnumerable<string> FieldNames
        {
            get { return this.fieldNames; }
        }

        /// <summary>
        /// Gets the string representing this data point that will be imported.
        /// </summary>
        /// <param name="rawValues">The raw field/value information from Symitar.</param>
        /// <param name="converter">A converter to determine formatting on the output strings.</param>
        /// <returns>A string representing the value from Symitar, intended for import.</returns>
        public string GetString(Dictionary<string, string> rawValues, DataAccess.LosConvert converter)
        {
            return string.Concat(this.fieldNames.Select(fieldName => new SymitarFieldMapping(fieldName, Fields.FieldMaker.Character).GetString(rawValues, converter)));
        }
    }
}
