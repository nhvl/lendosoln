﻿// <copyright file="CurrentEmploymentMapping.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2014-11-20 14:44:13 -0800
// </summary>
namespace LendersOffice.Integration.Symitar.Import
{
    using System;
    using DataAccess;

    /// <summary>
    /// Provides a set of mappings to the current employment record, which is represented as <see cref="CEmpPrimaryRec"/>.
    /// </summary>
    public abstract class CurrentEmploymentMapping : DataPointMapping
    {
        /// <summary>
        /// The name of the field containing the borrower's employment collection.
        /// </summary>
        /// <remarks>This field is necessary due to a bug in StyleCop causing it to fail to recognize the nameof operator in the call to the base constructor.</remarks>
        private const string BorrowerEmploymentCollectionFieldName = "aBEmpCollection";

        /// <summary>
        /// The name of the field containing the co-borrower's employment collection.
        /// </summary>
        /// <remarks>This field is necessary due to a bug in StyleCop causing it to fail to recognize the nameof operator in the call to the base constructor.</remarks>
        private const string CoBorrowerEmploymentCollectionFieldName = "aCEmpCollection";

        /// <summary>
        /// Gets the collection from the application.
        /// </summary>
        private readonly Func<CAppData, IEmpCollection> getCollection;

        /// <summary>
        /// Initializes a new instance of the <see cref="CurrentEmploymentMapping"/> class.
        /// </summary>
        /// <param name="neutralDescription">The description of the field, independent of borrower/co-borrower specifics.</param>
        /// <param name="isForBorrower">A boolean value indicating whether to use the borrower employment collection.</param>
        public CurrentEmploymentMapping(string neutralDescription, bool isForBorrower)
            : base(
                  DataPointMapping.GetBorrowerFieldDescription(isForBorrower, neutralDescription),
                  isForBorrower ? BorrowerEmploymentCollectionFieldName : CoBorrowerEmploymentCollectionFieldName)
        {
            this.IsForBorrower = isForBorrower; // Here for serialization
            this.getCollection = isForBorrower ? (Func<CAppData, IEmpCollection>)(app => app.aBEmpCollection) : app => app.aCEmpCollection;
        }

        /// <summary>
        /// Gets a value indicating whether to use the borrower or co-borrower employment collection.
        /// </summary>
        /// <value>A value indicating whether to use the borrower or co-borrower employment collection.</value>
        /// <remarks>This value is necessary so that serialization can reconstruct the object.</remarks>
        public bool IsForBorrower { get; private set; }

        /// <summary>
        /// Gets the value from the loan and application. Should work before and
        /// after calling <seealso cref="SetValue(CPageData, CAppData, string)"/>.
        /// </summary>
        /// <param name="loan">The loan containing the value.</param>
        /// <param name="application">The application from the loan containing the value.</param>
        /// <returns>A string representation of the value.</returns>
        public override string GetValue(CPageData loan, CAppData application)
        {
            var primaryEmployer = this.getCollection(application).GetPrimaryEmp(false);
            return primaryEmployer != null ? this.GetFieldValue(primaryEmployer) : string.Empty;
        }

        /// <summary>
        /// Sets the value on a loan and application.
        /// </summary>
        /// <param name="loan">The loan to set the value on.</param>
        /// <param name="application">The application from the loan to set the value on.</param>
        /// <param name="value">The value to assign.</param>
        public override void SetValue(CPageData loan, CAppData application, string value)
        {
            var primaryEmployer = this.getCollection(application).GetPrimaryEmp(true);
            this.SetFieldValue(primaryEmployer, value);
        }

        /// <summary>
        /// Gets the value of the field from the employment collection.
        /// </summary>
        /// <param name="employmentRecord">The employment record to get the value from.</param>
        /// <returns>The value of the field.</returns>
        protected abstract string GetFieldValue(IPrimaryEmploymentRecord employmentRecord);

        /// <summary>
        /// Sets the value of the field from the employment collection.
        /// </summary>
        /// <param name="employmentRecord">The employment record to set the value to.</param>
        /// <param name="value">The new value of the field.</param>
        protected abstract void SetFieldValue(IPrimaryEmploymentRecord employmentRecord, string value);

        /// <summary>
        /// Represents a mapping to the "Job Title" represented by <see cref="IEmploymentRecord.JobTitle"/>.
        /// </summary>
        public class JobTitle : CurrentEmploymentMapping
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="JobTitle"/> class.
            /// </summary>
            /// <param name="isForBorrower">A boolean value indicating whether to use the borrower employment collection.</param>
            public JobTitle(bool isForBorrower)
                : base("Job Title", isForBorrower)
            {
            }

            /// <summary>
            /// Gets the value of the field from the employment collection.
            /// </summary>
            /// <param name="employmentRecord">The employment record to get the value from.</param>
            /// <returns>The value of the field.</returns>
            protected override string GetFieldValue(IPrimaryEmploymentRecord employmentRecord)
            {
                return employmentRecord.JobTitle;
            }

            /// <summary>
            /// Sets the value of the field from the employment collection.
            /// </summary>
            /// <param name="employmentRecord">The employment record to set the value to.</param>
            /// <param name="value">The new value of the field.</param>
            protected override void SetFieldValue(IPrimaryEmploymentRecord employmentRecord, string value)
            {
                employmentRecord.JobTitle = value;
            }
        }

        /// <summary>
        /// Represents a mapping to the "Employer Name" represented by <see cref="IEmploymentRecord.EmplrNm"/>.
        /// </summary>
        public class EmployerName : CurrentEmploymentMapping
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="EmployerName"/> class.
            /// </summary>
            /// <param name="isForBorrower">A boolean value indicating whether to use the borrower employment collection.</param>
            public EmployerName(bool isForBorrower)
                : base("Employer Name", isForBorrower)
            {
            }

            /// <summary>
            /// Gets the value of the field from the employment collection.
            /// </summary>
            /// <param name="employmentRecord">The employment record to get the value from.</param>
            /// <returns>The value of the field.</returns>
            protected override string GetFieldValue(IPrimaryEmploymentRecord employmentRecord)
            {
                return employmentRecord.EmplrNm;
            }

            /// <summary>
            /// Sets the value of the field from the employment collection.
            /// </summary>
            /// <param name="employmentRecord">The employment record to set the value to.</param>
            /// <param name="value">The new value of the field.</param>
            protected override void SetFieldValue(IPrimaryEmploymentRecord employmentRecord, string value)
            {
                employmentRecord.EmplrNm = value;
            }
        }
    }
}
