﻿// <copyright file="RecordMapping.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2014-11-20 14:44:13 -0800
// </summary>
namespace LendersOffice.Integration.Symitar.Import
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Defines a mapping from Symitar Records to LendingQB data points.
    /// </summary>
    public class RecordMapping
    {
        /// <summary>
        /// A private function for filtering the initial query based on the results.
        /// </summary>
        private Func<string, bool> keepItemDecider;

        /// <summary>
        /// Initializes a new instance of the <see cref="RecordMapping"/> class.
        /// </summary>
        /// <param name="accountNumber">The account number of the member Symitar is running for.</param>
        /// <param name="pathFromAccount">The subpath of the record hierarchy from the main account record.</param>
        /// <param name="initialQueryPoint">The field to query for "ALL" to determine which we import.</param>
        /// <param name="initialItemFilter">The filter by which we determine which records to read.</param>
        /// <param name="mappings">The mappings from Symitar fields within this record to LendingQB data points.</param>
        public RecordMapping(string accountNumber, string pathFromAccount, string initialQueryPoint, Func<string, bool> initialItemFilter, IEnumerable<Tuple<ISymitarDataPointMapping, DataPointMapping>> mappings)
        {
            this.RecordPath = "ACCOUNT#" + accountNumber + (string.IsNullOrEmpty(pathFromAccount) ? null : ":" + pathFromAccount);
            this.InitialQueryPoint = initialQueryPoint;
            this.keepItemDecider = initialItemFilter;
            this.Mappings = new System.Collections.ObjectModel.ReadOnlyCollection<Tuple<ISymitarDataPointMapping, DataPointMapping>>(mappings.ToList());
        }

        /// <summary>
        /// Gets the fully qualified record path of the fields.
        /// </summary>
        /// <value>The fully qualified record path of the fields.</value>
        public string RecordPath { get; private set; }

        /// <summary>
        /// Gets the field name to query for "ALL" to determine how many records we have and which to import.
        /// </summary>
        /// <value>The field name to query for "ALL" to determine how many records we have and which to import.</value>
        public string InitialQueryPoint { get; private set; }

        /// <summary>
        /// Gets an enumeration of all the field names queried from this record.
        /// </summary>
        /// <value>An enumeration of all the field names queried from this record.</value>
        public IEnumerable<string> AllQueryFields
        {
            get { return this.Mappings.SelectMany(mapping => mapping.Item1.FieldNames).Distinct(); }
        }

        /// <summary>
        /// Gets a list of all the mappings from Symitar fields to LendingQB data points.
        /// </summary>
        /// <value>A list of all the mappings from Symitar fields to LendingQB data points.</value>
        public IReadOnlyList<Tuple<ISymitarDataPointMapping, DataPointMapping>> Mappings { get; private set; }

        /// <summary>
        /// Decides whether we should query for the complete record, based on a value returned by the initial query.
        /// </summary>
        /// <param name="value">The value returned by the initial query.</param>
        /// <returns>Whether the value should be queried further.</returns>
        public bool KeepItem(string value)
        {
            return this.keepItemDecider(value);
        }
    }
}
