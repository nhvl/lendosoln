﻿// <copyright file="PageDataUtilitiesMapping.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2014-11-20 14:44:13 -0800
// </summary>
namespace LendersOffice.Integration.Symitar.Import
{
    using DataAccess;

    /// <summary>
    /// Provides a simple way to map data points that <seealso cref="PageDataUtilities"/> knows about.
    /// </summary>
    public class PageDataUtilitiesMapping : DataPointMapping
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PageDataUtilitiesMapping"/> class.
        /// </summary>
        /// <param name="description">The description of the field.</param>
        /// <param name="fieldId">The identifier of the field being mapped.</param>
        public PageDataUtilitiesMapping(string description, string fieldId)
            : base(description, fieldId)
        {
            if (!PageDataUtilities.ContainsField(fieldId))
            {
                throw CBaseException.GenericException("Unable to recognize mapping to field id [" + fieldId + "]");
            }

            this.FieldId = fieldId;
        }

        /// <summary>
        /// Gets the identifier of the field being mapped.
        /// </summary>
        /// <value>The identifier of the field being mapped.</value>
        public string FieldId { get; private set; }

        /// <summary>
        /// Creates a borrower-level field mapping for either the borrower or co-borrower.
        /// </summary>
        /// <param name="neutralDescription">The description of the field, independent of borrower/co-borrower specifics.</param>
        /// <param name="borrowerFieldId">The field identifier for the borrower version of the field.</param>
        /// <param name="coborrowerFieldId">The field identifier for the co-borrower version of the field.</param>
        /// <param name="isForBorrower">A boolean value indicating whether to use the borrower employment collection.</param>
        /// <returns>Returns the mapping to the field.</returns>
        public static DataPointMapping BorrowerField(string neutralDescription, string borrowerFieldId, string coborrowerFieldId, bool isForBorrower)
        {
            return new PageDataUtilitiesMapping(
                DataPointMapping.GetBorrowerFieldDescription(isForBorrower, neutralDescription),
                isForBorrower ? borrowerFieldId : coborrowerFieldId);
        }

        /// <summary>
        /// Gets the value from the loan and application. Should work before and
        /// after calling <seealso cref="SetValue(CPageData, CAppData, string)"/>.
        /// </summary>
        /// <param name="loan">The loan containing the value.</param>
        /// <param name="application">The application from the loan containing the value.</param>
        /// <returns>A string representation of the value.</returns>
        public override string GetValue(CPageData loan, CAppData application)
        {
            return PageDataUtilities.GetValue(loan, application, this.FieldId);
        }

        /// <summary>
        /// Sets the value on a loan and application.
        /// </summary>
        /// <param name="loan">The loan to set the value on.</param>
        /// <param name="application">The application from the loan to set the value on.</param>
        /// <param name="value">The value to assign.</param>
        public override void SetValue(CPageData loan, CAppData application, string value)
        {
            PageDataUtilities.SetValue(loan, application, this.FieldId, value);
        }
    }
}
