﻿// <copyright file="DataPointMapping.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2014-11-20 14:44:13 -0800
// </summary>
namespace LendersOffice.Integration.Symitar.Import
{
    using DataAccess;

    /// <summary>
    /// Represents a mapping to data point on the <seealso cref="CPageData"/> object tree.
    /// </summary>
    public abstract class DataPointMapping
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DataPointMapping"/> class.
        /// </summary>
        /// <param name="description">The description of the data point that this mapping targets.</param>
        /// <param name="dependency">The dependency of this data point.</param>
        public DataPointMapping(string description, string dependency)
        {
            this.Description = description;
            this.Dependency = dependency;
        }

        /// <summary>
        /// Gets the human-readable description of the data point that this maps into.
        /// </summary>
        /// <value>The human-readable description of the data point that this maps into.</value>
        public string Description { get; private set; }

        /// <summary>
        /// Gets the dependency to add for this data point.
        /// </summary>
        /// <value>The dependency to add for this data point.</value>
        /// <remarks>If you're thinking about making this a collection, you're probably mapping more than one data point.</remarks>
        public string Dependency { get; private set; }

        /// <summary>
        /// Gets the value from the loan and application. Should work before and
        /// after calling <seealso cref="SetValue(CPageData, CAppData, string)"/>.
        /// </summary>
        /// <param name="loan">The loan containing the value.</param>
        /// <param name="application">The application from the loan containing the value.</param>
        /// <returns>A string representation of the value.</returns>
        public abstract string GetValue(CPageData loan, CAppData application);

        /// <summary>
        /// Sets the value on a loan and application.
        /// </summary>
        /// <param name="loan">The loan to set the value on.</param>
        /// <param name="application">The application from the loan to set the value on.</param>
        /// <param name="value">The value to assign.</param>
        public abstract void SetValue(CPageData loan, CAppData application, string value);

        /// <summary>
        /// Gets a description of a field specifying whether the field belongs to a borrower or co-borrower.
        /// </summary>
        /// <param name="isForBorrower">A boolean value indicating which borrower the field describes.</param>
        /// <param name="neutralDescription">A borrower-neutral base description of the data point.</param>
        /// <returns>The <paramref name="neutralDescription"/>, prefixed by a borrower or co-borrower descriptor.</returns>
        protected static string GetBorrowerFieldDescription(bool isForBorrower, string neutralDescription)
        {
            return (isForBorrower ? "Borrower " : "Co-Borrower ") + neutralDescription;
        }
    }
}
