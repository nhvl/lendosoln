﻿// <copyright file="ImportFromSymitarViewModel.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Michael Leinweaver
//    Date:   12/7/2015
// </summary>
namespace LendersOffice.Integration.Symitar
{
    using System.Collections.Generic;

    /// <summary>
    /// Provides an implementation for the model used to populate the 
    /// "Import from Symitar" page.
    /// </summary>
    public class ImportFromSymitarViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ImportFromSymitarViewModel"/>
        /// class.
        /// </summary>
        public ImportFromSymitarViewModel()
        {
            this.BorrowerImportTableList = new List<SymitarImportEntry>();
            this.CoborrowerImportTableList = new List<SymitarImportEntry>();
        }

        /// <summary>
        /// Gets or sets the key corresponding to the borrower import mapping used for this mapping.
        /// </summary>
        /// <value>The key corresponding to the borrower import mapping used for this mapping.</value>
        public string BorrowerImportMappingKey { get; set; }

        /// <summary>
        /// Gets or sets the key corresponding to the coborrower import mapping used for this mapping.
        /// </summary>
        /// <value>The key corresponding to the coborrower import mapping used for this mapping.</value>
        public string CoborrowerImportMappingKey { get; set; }

        /// <summary>
        /// Gets or sets the value for the list of Symitar borrower datapoints to
        /// import to a LendingQB loan.
        /// </summary>
        /// <value>
        /// The list of Symitar borrower datapoints to import to a LendingQB loan.
        /// </value>
        public List<SymitarImportEntry> BorrowerImportTableList { get; set; }

        /// <summary>
        /// Gets or sets the value for the list of Symitar coborrower datapoints to
        /// import to a LendingQB loan.
        /// </summary>
        /// <value>
        /// The list of Symitar coborrower datapoints to import to a LendingQB loan.
        /// </value>
        public List<SymitarImportEntry> CoborrowerImportTableList { get; set; }
    }
}
