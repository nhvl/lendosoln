﻿// <copyright file="LenderConfiguration.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2016-04-08 14:07:13 -0700
// </summary>
namespace LendersOffice.Integration.Symitar
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Represents a complete lender-level configuration from Symitar.
    /// </summary>
    public class LenderConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LenderConfiguration"/> class.
        /// </summary>
        /// <param name="deviceNumber">The device number to convey to Symitar.</param>
        /// <param name="deviceType">The device type to convey to Symitar.</param>
        /// <param name="virtualCardPrefix">The virtual card number to prefix the account number with as the password.</param>
        /// <param name="ipAddress">The IP Address for the lender.</param>
        /// <param name="ports">The available ports to connect to Symitar.</param>
        /// <param name="batchExportReportName">The name of the Batch Export Report used to export the data.</param>
        /// <param name="allowImportFromSymitar">A value indicating whether the integration should permit importing from Symitar.</param>
        /// <param name="allowExportToSymitar">A value indicating whether the integration should permit exporting to Symitar.</param>
        public LenderConfiguration(short deviceNumber, string deviceType, string virtualCardPrefix, IPAddress ipAddress, List<int> ports, string batchExportReportName, bool allowImportFromSymitar, bool allowExportToSymitar)
        {
            this.DeviceNumber = deviceNumber;
            this.DeviceType = deviceType;
            this.VirtualCardPrefix = virtualCardPrefix;
            this.IpAddress = ipAddress;
            this.Ports = ports;
            this.BatchExportReportName = batchExportReportName;
            this.AllowImportFromSymitar = allowImportFromSymitar;
            this.AllowExportToSymitar = allowExportToSymitar;
        }

        /// <summary>
        /// Gets a value indicating whether the integration should permit importing from Symitar.
        /// </summary>
        /// <value>A value indicating whether the integration should permit importing from Symitar.</value>
        public bool AllowImportFromSymitar { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the integration should permit exporting to Symitar.
        /// </summary>
        /// <value>A value indicating whether the integration should permit exporting to Symitar.</value>
        public bool AllowExportToSymitar { get; private set; }

        /// <summary>
        /// Gets the device number to convey to Symitar.
        /// </summary>
        /// <value>The device number to convey to Symitar.</value>
        public Sensitive<short> DeviceNumber { get; private set; }

        /// <summary>
        /// Gets the device type to convey to Symitar.
        /// </summary>
        /// <value>The device type to convey to Symitar.</value>
        public Sensitive<string> DeviceType { get; private set; }

        /// <summary>
        /// Gets the virtual card number to prefix the account number with as the password.
        /// </summary>
        /// <value>The virtual card number to prefix the account number with as the password.</value>
        public Sensitive<string> VirtualCardPrefix { get; private set; }

        /// <summary>
        /// Gets the IP Address for the lender.
        /// </summary>
        /// <value>The IP Address for the lender.</value>
        public Sensitive<IPAddress> IpAddress { get; private set; }

        /// <summary>
        /// Gets the available ports to connect to Symitar.
        /// </summary>
        /// <value>The available ports to connect to Symitar.</value>
        public Sensitive<IEnumerable<int>> Ports { get; private set; }

        /// <summary>
        /// Gets the name of the Batch Export Report used to export the data.
        /// </summary>
        /// <value>The name of the Batch Export Report used to export the data.</value>
        public string BatchExportReportName { get; private set; }

        /// <summary>
        /// Tries to create a <see cref="LenderConfiguration"/> from the specified dictionary.
        /// </summary>
        /// <param name="valueDictionary">A dictionary of all the string values needed to represent the data.</param>
        /// <returns>The initialized configuration for lender or null if something was not present correctly in the dictionary.</returns>
        public static LenderConfiguration TryCreate(IReadOnlyDictionary<string, string> valueDictionary)
        {
            bool successful = true;
            string temp;

            short deviceNumber = default(short);
            successful &= valueDictionary.TryGetValue("DeviceNumber", out temp) && short.TryParse(temp, out deviceNumber);

            string deviceType;
            successful &= valueDictionary.TryGetValue("DeviceType", out deviceType);

            string virtualCardPrefix;
            successful &= valueDictionary.TryGetValue("VirtualCardPrefix", out virtualCardPrefix);

            IPAddress ipAddress = default(IPAddress);
            successful &= valueDictionary.TryGetValue("IpAddress", out temp) && IPAddress.TryParse(temp, out ipAddress);

            string portsCsv;
            successful &= valueDictionary.TryGetValue("Ports", out portsCsv) && portsCsv != null;
            List<int> ports = new List<int>();
            if (successful)
            {
                int port;
                foreach (string portString in portsCsv.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    successful &= int.TryParse(portString, out port);
                    ports.Add(port);
                }
            }

            string batchExportReportName;
            successful &= valueDictionary.TryGetValue("BatchExportReportName", out batchExportReportName);

            bool allowOnlyTestFiles = false; // test file restriction is optional, but if included, it should parse
            if (valueDictionary.TryGetValue("AllowOnlyTestFiles", out temp))
            {
                successful &= bool.TryParse(temp, out allowOnlyTestFiles);
            }

            bool allowImportFromSymitar = default(bool);
            successful &= valueDictionary.TryGetValue("AllowImportFromSymitar", out temp) && bool.TryParse(temp, out allowImportFromSymitar);

            bool allowExportToSymitar = default(bool);
            successful &= valueDictionary.TryGetValue("AllowExportToSymitar", out temp) && bool.TryParse(temp, out allowExportToSymitar);

            return successful ? new LenderConfiguration(deviceNumber, deviceType, virtualCardPrefix, ipAddress, ports, batchExportReportName, allowImportFromSymitar, allowExportToSymitar) : null;
        }
    }
}
