﻿// <copyright file="ISymitarFieldValue.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2016-04-18 08:43:43 -0700
// </summary>
namespace LendersOffice.Integration.Symitar.Fields
{
    /// <summary>
    /// Defines a simple interface for representing a field and converting it to Symitar or LendingQB representations.
    /// </summary>
    public interface ISymitarFieldValue
    {
        /// <summary>
        /// Gets a value indicating whether the field's value is valid with respect to the constraints of Symitar.
        /// </summary>
        /// <value>A value indicating whether the field's value is valid with respect to the constraints of Symitar.</value>
        bool IsValid { get; }

        /// <summary>
        /// Converts the field value to a string that LendingQB can parse correctly.
        /// </summary>
        /// <param name="converter">A converter containing the desired format.</param>
        /// <returns>The field value as a string that LendingQB can parse correctly.</returns>
        string ToLQBString(DataAccess.LosConvert converter);

        /// <summary>
        /// Overrides the default string conversion to convert it to a string as Symitar understands the field value.
        /// </summary>
        /// <returns>A string that represents the current field value.</returns>
        string ToString();
    }
}
