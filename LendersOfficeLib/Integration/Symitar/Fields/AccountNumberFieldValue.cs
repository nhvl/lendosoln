﻿// <copyright file="AccountNumberFieldValue.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2016-04-18 08:43:43 -0700
// </summary>
namespace LendersOffice.Integration.Symitar.Fields
{
    using System.Globalization;

    /// <summary>
    /// Represents an account number field value.
    /// </summary>
    /// <remarks>
    /// 1-22 numeric characters representing the member's account number or ATM card number.
    /// Response messages use leading zeros. For example, if the member's account number is
    /// 101, the client can transmit 101, 0101, or 0000000101 in the request, but the server
    /// always returns 0000000101.
    /// </remarks>
    internal class AccountNumberFieldValue : SymitarFieldValue<string>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AccountNumberFieldValue"/> class.
        /// </summary>
        /// <param name="value">The value of the field.</param>
        public AccountNumberFieldValue(string value)
            : base(value)
        {
        }

        /// <summary>
        /// Converts an account number string from LendingQB into an account number field value.
        /// </summary>
        /// <param name="lqbValue">The string from LendingQB.</param>
        /// <returns>The account number field value.</returns>
        public static ISymitarFieldValue FromLendingQB(string lqbValue)
        {
            string value = string.IsNullOrEmpty(lqbValue) ? lqbValue : lqbValue.Trim();
            if (IsValidAccountNumberValue(lqbValue))
            {
                return new AccountNumberFieldValue(value);
            }

            return new SymitarInvalidFieldValue(value);
        }

        /// <summary>
        /// Computes whether the value passed in is a valid value for this field type.
        /// </summary>
        /// <param name="value">The value to test for validity.</param>
        /// <returns><see langword="true"/> if the value is valid; <see langword="false"/> otherwise.</returns>
        protected override bool IsValidValue(string value)
        {
            return IsValidAccountNumberValue(value);
        }

        /// <summary>
        /// Computes whether the value passed in is a valid value for this field type.
        /// </summary>
        /// <param name="value">The value to test for validity.</param>
        /// <returns><see langword="true"/> if the value is valid; <see langword="false"/> otherwise.</returns>
        private static bool IsValidAccountNumberValue(string value)
        {
            return value != null && System.Text.RegularExpressions.Regex.IsMatch(value, @"^\d{1,22}$");
        }
    }
}
