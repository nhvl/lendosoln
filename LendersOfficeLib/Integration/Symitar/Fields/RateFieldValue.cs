﻿// <copyright file="RateFieldValue.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2016-04-18 08:43:43 -0700
// </summary>
namespace LendersOffice.Integration.Symitar.Fields
{
    using System.Globalization;

    /// <summary>
    /// Represents a rate field value.
    /// </summary>
    /// <remarks>
    /// 1-6 numeric characters representing percent times 1000.
    /// The range is 0.000% to 999.999%. For example, 7.9% is
    /// represented as 7900.
    /// </remarks>
    internal class RateFieldValue : SymitarFieldValue<decimal>
    {
        /// <summary>
        /// The multiplication factor necessary to convert a value from a percentage to Symitar's value.
        /// </summary>
        private const int SymitarMultiplicationFactor = 1000;

        /// <summary>
        /// The maximum value for a rate field.
        /// </summary>
        private const decimal MaximumRate = 999.999M;

        /// <summary>
        /// Initializes a new instance of the <see cref="RateFieldValue"/> class.
        /// </summary>
        /// <param name="value">The value of the field.</param>
        public RateFieldValue(decimal value)
            : base(value)
        {
        }

        /// <summary>
        /// Converts a rate string from LendingQB into a rate field value.
        /// </summary>
        /// <param name="lqbValue">The string from LendingQB.</param>
        /// <returns>The rate field value.</returns>
        public static ISymitarFieldValue FromLendingQB(string lqbValue)
        {
            decimal rate;
            if (decimal.TryParse(lqbValue.Replace("%", string.Empty), out rate) && IsValidRateValue(rate))
            {
                return new RateFieldValue(rate);
            }

            return new SymitarInvalidFieldValue(lqbValue);
        }

        /// <summary>
        /// Converts a rate string from Symitar into a rate field value.
        /// </summary>
        /// <param name="symitarValue">The string from Symitar.</param>
        /// <returns>The rate field value.</returns>
        public static ISymitarFieldValue FromSymitar(string symitarValue)
        {
            return new RateFieldValue(decimal.Parse(symitarValue, NumberStyles.Integer) / SymitarMultiplicationFactor);
        }

        /// <summary>
        /// Overrides the default string conversion to convert it to a string as Symitar understands the field value.
        /// </summary>
        /// <returns>A string that represents the current field value.</returns>
        public override string ToString()
        {
            return (this.ActualValue * SymitarMultiplicationFactor).ToString("0");
        }

        /// <summary>
        /// Converts the field value to a string that LendingQB can parse correctly.
        /// </summary>
        /// <param name="converter">A converter containing the desired format.</param>
        /// <returns>The field value as a string that LendingQB can parse correctly.</returns>
        public override string ToLQBString(DataAccess.LosConvert converter)
        {
            return converter.ToRateString(this.ActualValue);
        }

        /// <summary>
        /// Computes whether the value passed in is a valid value for this field type.
        /// </summary>
        /// <param name="value">The value to test for validity.</param>
        /// <returns><see langword="true"/> if the value is valid; <see langword="false"/> otherwise.</returns>
        protected override bool IsValidValue(decimal value)
        {
            return IsValidRateValue(value);
        }

        /// <summary>
        /// Computes whether the value passed in is a valid value for this field type.
        /// </summary>
        /// <param name="value">The value to test for validity.</param>
        /// <returns><see langword="true"/> if the value is valid; <see langword="false"/> otherwise.</returns>
        private static bool IsValidRateValue(decimal value)
        {
            return value >= 0
                && value <= MaximumRate;
        }
    }
}
