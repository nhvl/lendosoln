﻿namespace LendersOffice.Integration.Symitar.Fields
{
    using System;

    /// <summary>
    /// Represents an invalid field to export to Symitar.
    /// </summary>
    internal class SymitarInvalidFieldValue : ISymitarFieldValue
    {
        /// <summary>
        /// The invalid value of the field.
        /// </summary>
        private readonly string invalidValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="SymitarInvalidFieldValue"/> class.
        /// </summary>
        /// <param name="invalidValue">The invalid value of the field.</param>
        public SymitarInvalidFieldValue(string invalidValue)
        {
            this.invalidValue = invalidValue;
        }

        /// <summary>
        /// Gets a value indicating whether the field's value is valid for export.
        /// </summary>
        public bool IsValid
        {
            get { return false; }
        }

        /// <summary>
        /// Converts the field to a representation understood by LendingQB.
        /// </summary>
        /// <param name="converter">The converter to use to decide value formatting.</param>
        /// <returns>A string representing the field's value.</returns>
        public string ToLQBString(DataAccess.LosConvert converter)
        {
            return this.invalidValue;
        }

        /// <summary>
        /// Overrides the default string conversion to convert it to a string as Symitar understands the field value.
        /// </summary>
        /// <returns>A string that represents the current field value.</returns>
        public override string ToString()
        {
            throw new Exception("This invalid field cannot be exported into a valid string value.");
        }
    }
}
