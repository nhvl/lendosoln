﻿// <copyright file="NumberFieldValue.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2016-04-18 08:43:43 -0700
// </summary>
namespace LendersOffice.Integration.Symitar.Fields
{
    /// <summary>
    /// Represents a number field value.
    /// </summary>
    /// <remarks>
    /// 1-11 numeric characters, positive sign (+), and negative sign (-),
    /// representing an integer with a prefix sign (optional for positive integers).
    /// The range is -2,147,483,647 to +2,147,483,647. For example, 23,456 is
    /// represented as 23456.
    /// </remarks>
    internal class NumberFieldValue : SymitarFieldValue<int>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NumberFieldValue"/> class.
        /// </summary>
        /// <param name="value">The value of the field.</param>
        public NumberFieldValue(int value)
            : base(value)
        {
        }

        /// <summary>
        /// Converts a number string from LendingQB into a number field value.
        /// </summary>
        /// <param name="lqbValue">The string from LendingQB.</param>
        /// <returns>The number field value.</returns>
        public static ISymitarFieldValue FromLendingQB(string lqbValue)
        {
            int number;
            if (int.TryParse(lqbValue, out number))
            {
                return new NumberFieldValue(number);
            }

            return new SymitarInvalidFieldValue(lqbValue);
        }

        /// <summary>
        /// Converts a number string from Symitar into a number field value.
        /// </summary>
        /// <param name="symitarValue">The string from Symitar.</param>
        /// <returns>The number field value.</returns>
        public static ISymitarFieldValue FromSymitar(string symitarValue)
        {
            return new NumberFieldValue(int.Parse(symitarValue));
        }
    }
}
