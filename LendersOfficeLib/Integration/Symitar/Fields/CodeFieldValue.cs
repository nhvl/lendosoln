﻿// <copyright file="CodeFieldValue.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2016-04-18 08:43:43 -0700
// </summary>
namespace LendersOffice.Integration.Symitar.Fields
{
    /// <summary>
    /// Represents a code field value.
    /// </summary>
    /// <remarks>
    /// 1-10 numeric characters representing a positive integer.
    /// Range is 0 to the maximum value specified in Records and Fields.
    /// For example, <c>203</c>.
    /// </remarks>
    internal class CodeFieldValue : SymitarFieldValue<string>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CodeFieldValue"/> class.
        /// </summary>
        /// <param name="value">The value of the field.</param>
        public CodeFieldValue(string value)
            : base(value)
        {
        }

        /// <summary>
        /// Converts a code string from LendingQB into a code field value.
        /// </summary>
        /// <param name="lqbValue">The string from LendingQB.</param>
        /// <returns>The code field value.</returns>
        public static ISymitarFieldValue FromLendingQB(string lqbValue)
        {
            string value = string.IsNullOrEmpty(lqbValue) ? lqbValue : lqbValue.Trim();
            if (IsValidCodeValue(lqbValue))
            {
                return new CodeFieldValue(value);
            }

            return new SymitarInvalidFieldValue(value);
        }

        /// <summary>
        /// Computes whether the value passed in is a valid value for this field type.
        /// </summary>
        /// <param name="value">The value to test for validity.</param>
        /// <returns><see langword="true"/> if the value is valid; <see langword="false"/> otherwise.</returns>
        protected override bool IsValidValue(string value)
        {
            return IsValidCodeValue(value);
        }

        /// <summary>
        /// Computes whether the value passed in is a valid value for this field type.
        /// </summary>
        /// <param name="value">The value to test for validity.</param>
        /// <returns><see langword="true"/> if the value is valid; <see langword="false"/> otherwise.</returns>
        private static bool IsValidCodeValue(string value)
        {
            return value != null && System.Text.RegularExpressions.Regex.IsMatch(value, @"^\d{1,10}$");
        }
    }
}
