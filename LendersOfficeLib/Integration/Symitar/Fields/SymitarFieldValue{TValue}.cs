﻿// <copyright file="SymitarFieldValue{TValue}.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2016-04-18 08:43:43 -0700
// </summary>
namespace LendersOffice.Integration.Symitar.Fields
{
    using System;

    /// <summary>
    /// A simple implementation of a Symitar field value.
    /// </summary>
    /// <typeparam name="TValue">The underlying type of the field's value.</typeparam>
    internal abstract class SymitarFieldValue<TValue> : ISymitarFieldValue
    {
        /// <summary>
        /// The underlying value of the field.
        /// </summary>
        public readonly TValue ActualValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="SymitarFieldValue{TValue}"/> class.
        /// </summary>
        /// <param name="actualValue">The value of the field.</param>
        protected SymitarFieldValue(TValue actualValue)
        {
            if (this.IsValidValue(actualValue))
            {
                this.ActualValue = actualValue;
            }
            else
            {
                throw new Exception("Invalid Value \"" + actualValue + "\" for " + this.GetType().Name);
            }
        }

        /// <summary>
        /// Gets a value indicating whether or not <see cref="ActualValue"/> is a valid value for this field.
        /// </summary>
        public bool IsValid
        {
            get { return this.IsValidValue(this.ActualValue); }
        }

        /// <summary>
        /// Converts the field value to a string that LendingQB can parse correctly.
        /// </summary>
        /// <param name="converter">A converter containing the desired format.</param>
        /// <returns>The field value as a string that LendingQB can parse correctly.</returns>
        public virtual string ToLQBString(DataAccess.LosConvert converter)
        {
            return this.ToString();
        }

        /// <summary>
        /// Overrides the default string conversion to convert it to a string as Symitar understands the field value.
        /// </summary>
        /// <returns>A string that represents the current field value.</returns>
        public override string ToString()
        {
            return this.ActualValue.ToString();
        }

        /// <summary>
        /// Computes whether the value passed in is a valid value for this field type.
        /// </summary>
        /// <param name="value">The value to test for validity.</param>
        /// <returns><see langword="true"/> if the value is valid; <see langword="false"/> otherwise.</returns>
        protected virtual bool IsValidValue(TValue value)
        {
            return true;
        }
    }
}