﻿// <copyright file="MoneyFieldValue.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2016-04-18 08:43:43 -0700
// </summary>
namespace LendersOffice.Integration.Symitar.Fields
{
    using System.Globalization;

    /// <summary>
    /// Represents a money field value.
    /// </summary>
    /// <remarks>
    /// 1-17 numeric characters, positive sign (+), and negative sign (-),
    /// representing integer cents (dollars times 100) with a prefix sign
    /// (optional for positive monetary amounts).
    /// Normal range is -999,999,999,999.99 to +999,999,999,999.99.
    /// Several infrequently used fields may range to 16 digits and the prefix sign.
    /// For example, $24,778.27 is represented as 2477827.
    /// </remarks>
    internal class MoneyFieldValue : SymitarFieldValue<decimal>
    {
        /// <summary>
        /// The multiplication factor necessary to convert a value from a dollar amount to Symitar's value.
        /// </summary>
        private const int SymitarMultiplicationFactor = 100;

        /// <summary>
        /// The maximum value for a money field.
        /// </summary>
        private static readonly decimal MaximumMoney = decimal.Parse(new string('9', 17));

        /// <summary>
        /// The minimum value for a money field.
        /// </summary>
        private static readonly decimal MinimumMoney = -MaximumMoney;

        /// <summary>
        /// Initializes a new instance of the <see cref="MoneyFieldValue"/> class.
        /// </summary>
        /// <param name="value">The value of the field.</param>
        public MoneyFieldValue(decimal value)
            : base(value)
        {
        }

        /// <summary>
        /// Converts a money string from LendingQB into a money field value.
        /// </summary>
        /// <param name="lqbValue">The string from LendingQB.</param>
        /// <returns>The money field value.</returns>
        public static ISymitarFieldValue FromLendingQB(string lqbValue)
        {
            decimal money;
            if (decimal.TryParse(lqbValue, NumberStyles.Currency, null, out money) && IsValidMoneyValue(money))
            {
                return new MoneyFieldValue(money);
            }

            return new SymitarInvalidFieldValue(lqbValue);
        }

        /// <summary>
        /// Converts a money string from Symitar into a money field value.
        /// </summary>
        /// <param name="symitarValue">The string from Symitar.</param>
        /// <returns>The money field value.</returns>
        public static ISymitarFieldValue FromSymitar(string symitarValue)
        {
            return new MoneyFieldValue(decimal.Parse(symitarValue, NumberStyles.Integer) / SymitarMultiplicationFactor);
        }

        /// <summary>
        /// Overrides the default string conversion to convert it to a string as Symitar understands the field value.
        /// </summary>
        /// <returns>A string that represents the current field value.</returns>
        public override string ToString()
        {
            return (this.ActualValue * SymitarMultiplicationFactor).ToString("0");
        }

        /// <summary>
        /// Converts the field value to a string that LendingQB can parse correctly.
        /// </summary>
        /// <param name="converter">A converter containing the desired format.</param>
        /// <returns>The field value as a string that LendingQB can parse correctly.</returns>
        public override string ToLQBString(DataAccess.LosConvert converter)
        {
            return converter.ToMoneyString(this.ActualValue, DataAccess.FormatDirection.ToRep);
        }

        /// <summary>
        /// Computes whether the value passed in is a valid value for this field type.
        /// </summary>
        /// <param name="value">The value to test for validity.</param>
        /// <returns><see langword="true"/> if the value is valid; <see langword="false"/> otherwise.</returns>
        protected override bool IsValidValue(decimal value)
        {
            return IsValidMoneyValue(value);
        }

        /// <summary>
        /// Computes whether the value passed in is a valid value for this field type.
        /// </summary>
        /// <param name="value">The value to test for validity.</param>
        /// <returns><see langword="true"/> if the value is valid; <see langword="false"/> otherwise.</returns>
        private static bool IsValidMoneyValue(decimal value)
        {
            return value >= MinimumMoney
                && value <= MaximumMoney;
        }
    }
}
