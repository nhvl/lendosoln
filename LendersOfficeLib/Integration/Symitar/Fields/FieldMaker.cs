﻿// <copyright file="FieldMaker.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2016-04-18 08:43:43 -0700
// </summary>
namespace LendersOffice.Integration.Symitar.Fields
{
    using System;

    /// <summary>
    /// Provides a unified way to instantiate instances of various Symitar field types from a string value.
    /// </summary>
    /// <remarks>This object is and should remain immutable.</remarks>
    public class FieldMaker
    {
        /// <summary>
        /// An instance for building Symitar Account Number fields.
        /// </summary>
        public static readonly FieldMaker AccountNumber = new FieldMaker(value => new AccountNumberFieldValue(value), AccountNumberFieldValue.FromLendingQB);

        /// <summary>
        /// An instance for building Symitar Character fields.
        /// </summary>
        public static readonly FieldMaker Character = new FieldMaker(value => new CharacterFieldValue(value), CharacterFieldValue.FromLendingQB);

        /// <summary>
        /// An instance for building Symitar Code fields.
        /// </summary>
        public static readonly FieldMaker Code = new FieldMaker(value => new CodeFieldValue(value), CodeFieldValue.FromLendingQB);

        /// <summary>
        /// An instance for building Symitar Date fields.
        /// </summary>
        public static readonly FieldMaker Date = new FieldMaker(DateFieldValue.FromSymitar, DateFieldValue.FromLendingQB);

        /// <summary>
        /// An instance for building Symitar Money fields.
        /// </summary>
        public static readonly FieldMaker Money = new FieldMaker(MoneyFieldValue.FromSymitar, MoneyFieldValue.FromLendingQB);

        /// <summary>
        /// An instance for building Symitar Number fields.
        /// </summary>
        public static readonly FieldMaker Number = new FieldMaker(NumberFieldValue.FromSymitar, NumberFieldValue.FromLendingQB);

        /// <summary>
        /// An instance for building Symitar Rate fields.
        /// </summary>
        public static readonly FieldMaker Rate = new FieldMaker(RateFieldValue.FromSymitar, RateFieldValue.FromLendingQB);

        /// <summary>
        /// An instance for building Symitar Time fields.
        /// </summary>
        public static readonly FieldMaker Time = new FieldMaker(TimeFieldValue.FromSymitar, TimeFieldValue.FromLendingQB);

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldMaker" /> class.
        /// </summary>
        /// <param name="fromSymitarOrLQB">A function that converts a string to field value.</param>
        private FieldMaker(Func<string, ISymitarFieldValue> fromSymitarOrLQB)
            : this(fromSymitarOrLQB, fromSymitarOrLQB)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FieldMaker" /> class.
        /// </summary>
        /// <param name="fromSymitar">A function that converts a string from Symitar into a field value.</param>
        /// <param name="fromLendingQB">A function that converts a string from LendingQB into a field value.</param>
        private FieldMaker(Func<string, ISymitarFieldValue> fromSymitar, Func<string, ISymitarFieldValue> fromLendingQB)
        {
            this.FromSymitar = fromSymitar;
            this.FromLendingQB = fromLendingQB;
        }

        /// <summary>
        /// Gets a function that converts a string from Symitar into a field value.
        /// </summary>
        /// <value>A function that converts a string from Symitar into a field value.</value>
        public Func<string, ISymitarFieldValue> FromSymitar { get; private set; }

        /// <summary>
        /// Gets a function that converts a string from LendingQB into a field value.
        /// </summary>
        /// <value>A function that converts a string from LendingQB into a field value.</value>
        public Func<string, ISymitarFieldValue> FromLendingQB { get; private set; }

        /// <summary>
        /// Finds the relevant object to the specified data type.
        /// </summary>
        /// <param name="fieldDataType">The data type of the field.</param>
        /// <exception cref="NotImplementedException">An invalid <paramref name="fieldDataType"/> was specified.</exception>
        /// <returns>A field maker to help with construction of Symitar fields.</returns>
        public static FieldMaker GetFieldMaker(string fieldDataType)
        {
            switch (fieldDataType)
            {
                case nameof(AccountNumber): return AccountNumber;
                case nameof(Character): return Character;
                case nameof(Code): return Code;
                case nameof(Date): return Date;
                case nameof(Money): return Money;
                case nameof(Number): return Number;
                case nameof(Rate): return Rate;
                case nameof(Time): return Time;
                default:
                    throw new NotImplementedException("Unable to match a field maker for \"" + fieldDataType + "\"");
            }
        }
    }
}
