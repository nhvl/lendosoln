﻿// <copyright file="CharacterFieldValue.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2016-04-18 08:43:43 -0700
// </summary>
namespace LendersOffice.Integration.Symitar.Fields
{
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Represents a character field value.
    /// </summary>
    /// <remarks>
    /// 0-132 characters. Actual size, as specified in the Files, Records, &amp; Fields book.
    /// Printable characters (alphanumeric and special characters), specifically excluding
    /// the field separator (~) and end of message (&lt;EOM&gt;) characters. For example,
    /// <c>ON US DRAFT 002437 RECEIVED</c>.
    /// </remarks>
    internal class CharacterFieldValue : SymitarFieldValue<string>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CharacterFieldValue"/> class.
        /// </summary>
        /// <param name="value">The value of the field.</param>
        public CharacterFieldValue(string value)
            : base(value)
        {
        }

        /// <summary>
        /// Converts a character string from LendingQB into a character field value.
        /// </summary>
        /// <param name="lqbValue">The string from LendingQB.</param>
        /// <returns>The character field value.</returns>
        public static ISymitarFieldValue FromLendingQB(string lqbValue)
        {
            string sanitizedString = SanitizeCharacterField(lqbValue);
            if (IsValidCharacterValue(sanitizedString))
            {
                return new CharacterFieldValue(sanitizedString);
            }

            return new SymitarInvalidFieldValue(lqbValue);
        }

        /// <summary>
        /// Removes invalid characters from a character field being sent to Symitar.
        /// </summary>
        /// <param name="input">A string to be sent to in a character field to Symitar.</param>
        /// <returns>A <see name="System.String"/> with only valid characters, or <seealso cref="System.String.Empty"/> if it contains none.</returns>
        public static string SanitizeCharacterField(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }

            StringBuilder sanitizedString = new StringBuilder(input.Length);
            for (int i = 0; i < input.Length; ++i)
            {
                char c = input[i];
                if (IsValidCharacter(c))
                {
                    sanitizedString.Append(c);
                }
                else if (c == '\t')
                {
                    sanitizedString.Append("    ");
                }
                else if (c == '\r' || c == '\n')
                {
                    sanitizedString.Append(' ');
                    if (c == '\r' && i + 1 < input.Length && input[i + 1] == '\n')
                    {
                        i += 1;
                    }
                }
            }

            return sanitizedString.ToString();
        }

        /// <summary>
        /// Returns a value indicating whether <paramref name="c"/> is a permitted character value within a character field.
        /// </summary>
        /// <param name="c">The character to check.</param>
        /// <returns><see langword="true"/> if <paramref name="c"/> is a valid character for a character field; <see langword="false"/> otherwise.</returns>
        public static bool IsValidCharacter(char c)
        {
            // Character fields are limited to printable ASCII characters and must
            // not contain the field separator or the end-of-message character.
            return c >= 32 && c <= 126
                    && c != SymitarMessage.FieldSeparator
                    && c != SymitarMessage.EndOfMessage;
        }

        /// <summary>
        /// Computes whether the value passed in is a valid value for this field type.
        /// </summary>
        /// <param name="value">The value to test for validity.</param>
        /// <returns><see langword="true"/> if the value is valid; <see langword="false"/> otherwise.</returns>
        protected override bool IsValidValue(string value)
        {
            return IsValidCharacterValue(value);
        }

        /// <summary>
        /// Computes whether the value passed in is a valid value for this field type.
        /// </summary>
        /// <param name="value">The value to test for validity.</param>
        /// <returns><see langword="true"/> if the value is valid; <see langword="false"/> otherwise.</returns>
        private static bool IsValidCharacterValue(string value)
        {
            return value != null
                && value.Length <= 132
                && value.All(IsValidCharacter);
        }
    }
}
