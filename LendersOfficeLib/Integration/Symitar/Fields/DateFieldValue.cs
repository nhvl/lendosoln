﻿// <copyright file="DateFieldValue.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2016-04-18 08:43:43 -0700
// </summary>
namespace LendersOffice.Integration.Symitar.Fields
{
    using System;
    using System.Globalization;

    /// <summary>
    /// Represents a date field value.
    /// </summary>
    /// <remarks>
    /// 8 numeric characters in the format <c>yyyymmdd</c>, where <c>yyyy</c> is the
    /// four-digit year, <c>mm</c> is the month, and <c>dd</c> is the day. For example,
    /// November 17, 1996 is represented as <c>19961117</c>. Eight zeros (<c>00000000</c>)
    /// are used to represent unspecified dates.
    /// </remarks>
    internal class DateFieldValue : SymitarFieldValue<DateTime?>
    {
        /// <summary>
        /// The value Symitar uses to represent an unspecified date.
        /// </summary>
        private const string SymitarUnspecifiedDateValue = "00000000";

        /// <summary>
        /// The format string for Symitar's date format.
        /// </summary>
        private const string SymitarDateTimeFormat = "yyyyMMdd";

        /// <summary>
        /// Initializes a new instance of the <see cref="DateFieldValue"/> class.
        /// </summary>
        /// <param name="date">The date to use as the field value.  If not null, it will be truncated to the date component.</param>
        public DateFieldValue(DateTime? date)
            : base(date.HasValue ? (DateTime?)date.Value.Date : null)
        {
        }

        /// <summary>
        /// Converts a date string from LendingQB into a date field value.
        /// </summary>
        /// <param name="lqbValue">The string from LendingQB.</param>
        /// <returns>The date field value.</returns>
        public static ISymitarFieldValue FromLendingQB(string lqbValue)
        {
            if (string.IsNullOrEmpty(lqbValue))
            {
                return new DateFieldValue(null);
            }

            DateTime date;
            if (DateTime.TryParse(lqbValue, out date))
            {
                return new DateFieldValue(date);
            }

            return new SymitarInvalidFieldValue(lqbValue);
        }

        /// <summary>
        /// Converts a date string from Symitar into a date field value.
        /// </summary>
        /// <param name="symitarValue">The string from Symitar.</param>
        /// <returns>The date field value.</returns>
        public static ISymitarFieldValue FromSymitar(string symitarValue)
        {
            return new DateFieldValue(
                symitarValue == SymitarUnspecifiedDateValue
                ? (DateTime?)null
                : DateTime.ParseExact(symitarValue, SymitarDateTimeFormat, CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// Overrides the default string conversion to convert it to a string as Symitar understands the field value.
        /// </summary>
        /// <returns>A string that represents the current field value.</returns>
        public override string ToString()
        {
            return this.ActualValue.HasValue ? this.ActualValue.Value.ToString(SymitarDateTimeFormat) : SymitarUnspecifiedDateValue;
        }

        /// <summary>
        /// Converts the field value to a string that LendingQB can parse correctly.
        /// </summary>
        /// <param name="converter">A converter containing the desired format.</param>
        /// <returns>The field value as a string that LendingQB can parse correctly.</returns>
        public override string ToLQBString(DataAccess.LosConvert converter)
        {
            return this.ActualValue.HasValue ? converter.ToDateTimeString(this.ActualValue.Value) : converter.InvalidDateString;
        }
    }
}
