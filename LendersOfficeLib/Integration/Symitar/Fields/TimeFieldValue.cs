﻿// <copyright file="TimeFieldValue.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2016-04-18 08:43:43 -0700
// </summary>
namespace LendersOffice.Integration.Symitar.Fields
{
    using System;
    using System.Globalization;

    /// <summary>
    /// Represents a time field value.
    /// </summary>
    /// <remarks>
    /// 4 numeric characters in the format hhmm, based on the 24-hour clock,
    /// where hh represents the hour and mm represents the minute
    /// (defined as code to 2359 in Records and Fields). For example,
    /// 1345 represents 1:45 p.m., 0000 represents midnight,
    /// 2359 represents one minute before midnight, and 1200 represents noon.
    /// </remarks>
    internal class TimeFieldValue : SymitarFieldValue<TimeSpan>
    {
        /// <summary>
        /// The format string for Symitar's date format.
        /// </summary>
        private const string SymitarTimeFormat = "hhmm";

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeFieldValue"/> class.
        /// </summary>
        /// <param name="time">The time to use as the field value.</param>
        private TimeFieldValue(TimeSpan time)
            : base(time)
        {
        }

        /// <summary>
        /// Converts a time string from LendingQB into a time field value.
        /// </summary>
        /// <param name="lqbValue">The string from LendingQB.</param>
        /// <returns>The time field value.</returns>
        public static ISymitarFieldValue FromLendingQB(string lqbValue)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Converts a time string from Symitar into a time field value.
        /// </summary>
        /// <param name="symitarValue">The string from Symitar.</param>
        /// <returns>The time field value.</returns>
        public static ISymitarFieldValue FromSymitar(string symitarValue)
        {
            return new TimeFieldValue(TimeSpan.ParseExact(symitarValue, SymitarTimeFormat, CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// Converts the value of the current <see cref="TimeFieldValue"/> object to its equivalent string representation.
        /// </summary>
        /// <returns>A string representation of the value of the current <see cref="TimeFieldValue"/> object.</returns>
        public override string ToString()
        {
            return this.ActualValue.ToString(SymitarTimeFormat);
        }
    }
}
