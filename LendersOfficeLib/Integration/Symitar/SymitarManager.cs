﻿// <copyright file="SymitarManager.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2014-11-20 14:44:13 -0800
// </summary>
namespace LendersOffice.Integration.Symitar
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common;
    using DataAccess;

    /// <summary>
    /// Manages interactions and higher-level logic of the interactions with Symitar.
    /// </summary>
    public class SymitarManager
    {
        /// <summary>
        /// Converts a mapped import from Symitar into a list of corresponding entries available for import.
        /// </summary>
        /// <param name="import">The import values to load.</param>
        /// <param name="loanId">The loan identifier to get current and proposed import values from.</param>
        /// <param name="appId">The application identifier to get the current and proposed import values from.</param>
        /// <returns>A list of entries available for import.</returns>
        public static List<SymitarImportEntry> MapToDisplayLogic(MappedSymitarImport import, Guid loanId, Guid appId)
        {
            var loan = new CPageData(loanId, import.MappedValues.Select(d => d.Mapping.Dependency)); // We enforce workflow for now since there isn't a Symitar permission
            loan.InitLoad();
            var application = loan.GetAppData(appId);
            var importEntryList = new List<SymitarImportEntry>(import.MappedValues.Select(dataPoint => new SymitarImportEntry()
            {
                FieldDescription = dataPoint.Mapping.Description,
                CurrentValue = dataPoint.Mapping.GetValue(loan, application),
                Include = true
            }));

            for (int i = 0; i < importEntryList.Count; ++i)
            {
                var dataPoint = import.MappedValues[i];
                dataPoint.Mapping.SetValue(loan, application, dataPoint.Value);
                importEntryList[i].ImportValue = dataPoint.Mapping.GetValue(loan, application);
            }

            return importEntryList;
        }

        /// <summary>
        /// Creates the string of an inquiry message for the specified lender.
        /// </summary>
        /// <param name="lenderConfig">The lender config to use.</param>
        /// <param name="accountNumber">The accountNumber to send.</param>
        /// <param name="recordPath">The record path to look at.</param>
        /// <param name="fields">The fields to query.</param>
        /// <returns>A string of the message that would be sent.</returns>
        public static string CreateInquiryMessageString(LenderConfiguration lenderConfig, string accountNumber, string recordPath, string[] fields)
        {
            return BuildInquiry(lenderConfig, StandardizeAccountNumber(accountNumber), recordPath, fields).ToString();
        }

        /// <summary>
        /// Saves data from the Symitar import to the specified loan file.
        /// </summary>
        /// <param name="principal">The user who is performing the import.</param>
        /// <param name="loanId">The loan file to save the data to.</param>
        /// <param name="appId">The loan application to save the data to.</param>
        /// <param name="importData">The Symitar import data to save.</param>
        /// <param name="indicesToSave">The indices within the import mapping to save to the loan file.</param>
        public static void SaveToLoan(Security.AbstractUserPrincipal principal, Guid loanId, Guid appId, MappedSymitarImport importData, IEnumerable<int> indicesToSave)
        {
            if (!indicesToSave.Any())
            {
                return;
            }

            var dataPoints = indicesToSave.Select(i => importData.MappedValues[i]).ToList();
            var loan = new CPageData(loanId, importData.MappedValues.Select(d => d.Mapping.Dependency)); // We enforce workflow for now since there isn't a Symitar permission
            loan.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);
            var application = loan.GetAppData(appId);

            var beforeValues = new List<string>(dataPoints.Count);
            beforeValues.AddRange(dataPoints.Select(dataPoint => dataPoint.Mapping.GetValue(loan, application)));
            foreach (var dataPoint in dataPoints)
            {
                dataPoint.Mapping.SetValue(loan, application, dataPoint.Value);
            }

            var auditHistoryValues = new List<Tuple<string, string, string>>(dataPoints.Count);
            for (int i = 0; i < dataPoints.Count; ++i)
            {
                auditHistoryValues.Add(Tuple.Create(dataPoints[i].Mapping.Description, beforeValues[i], dataPoints[i].Mapping.GetValue(loan, application)));
            }

            loan.RecordAuditOnSave(LendersOffice.Audit.SymitarAuditItem.CreateImportAuditItem(principal, auditHistoryValues));
            loan.Save();
        }

        /// <summary>
        /// Sends a set of debugging inquiries off to Symitar for evaluation, matching each message with a response.
        /// </summary>
        /// <param name="lenderConfig">The lender config to use for the connection and authentication settings.</param>
        /// <param name="stringMessages">The messages to send.</param>
        /// <returns>A dictionary of sent messages to received responses, in raw string form.</returns>
        public static Dictionary<string, string> SendDebugInquiries(LenderConfiguration lenderConfig, string stringMessages)
        {
            if (lenderConfig == null)
            {
                throw new ArgumentNullException(nameof(lenderConfig));
            }

            var allMessages = stringMessages.Split(new[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries)
                .Select(stringMessage => SymitarMessage.ParseMessage(stringMessage))
                .ToList();

            var allowedMessages = allMessages.Where(message => message.MessageTypeCode.MessageType == SymitarMessageType.Inquiry);

            if (!allowedMessages.Any())
            {
                return new Dictionary<string, string>(0);
            }

            using (var server = SymitarServer.Create(lenderConfig.IpAddress.Value, lenderConfig.Ports.Value))
            {
                var requests = new Dictionary<string, SymitarMessage>();
                foreach (var message in allowedMessages)
                {
                    string messageId = server.Write(message);
                    requests.Add(messageId, message);
                }

                var requestToResponses = new Dictionary<string, string>(requests.Count);
                foreach (var request in requests)
                {
                    requestToResponses.Add(request.Value.ToString(), server.Retrieve(request.Key).ToString());
                }

                return requestToResponses;
            }
        }

        /// <summary>
        /// Recursively maps the table to the UI representation.
        /// </summary>
        /// <param name="tableToExport">The table that needs to be exported.</param>
        /// <param name="converter">The conversion definition to use for the display format of the value item.</param>
        /// <returns>A model of the data for use in the view.</returns>
        public static Export.SymitarExportDataSection MapToDisplayLogic(Export.NestedTableExport tableToExport, LosConvert converter)
        {
            var rows = new List<Export.SymitarExportDataSectionRow>(tableToExport.Fields.Count);
            foreach (var field in tableToExport.Fields)
            {
                var fieldValue = Fields.FieldMaker.GetFieldMaker(field.DataType).FromLendingQB(field.Value);
                rows.Add(new Export.SymitarExportDataSectionRow
                {
                    Description = field.Description ?? field.Name,
                    Mnemonic = field.Mnemonic,
                    Value = fieldValue.ToLQBString(converter),
                    HasError = !fieldValue.IsValid,
                    ValueDataType = field.DataType
                });
            }

            return new Export.SymitarExportDataSection()
            {
                Name = tableToExport.FullName,
                Rows = rows,
                Subsections = tableToExport.SubRecords.Select(table => MapToDisplayLogic(table, converter)).ToList()
            };
        }

        /// <summary>
        /// Saves the provided export into Symitar.
        /// </summary>
        /// <param name="principal">The principal who is exporting the data.</param>
        /// <param name="export">The export to save.</param>
        /// <param name="lenderConfig">The lender configuration for Symitar.</param>
        /// <param name="memberAccountNumber">The member's account number where we'll export to.</param>
        /// <param name="symitarLoanId">The loan id to use.</param>
        /// <param name="isNewSymitarLoan">A value indicating the user's intent to either create a new loan or revise an existing record.</param>
        /// <returns>A value indicating whether the save was successful.</returns>
        public static bool SaveToSymitar(Security.AbstractUserPrincipal principal, Export.SymitarDataExport export, LenderConfiguration lenderConfig, string memberAccountNumber, string symitarLoanId, bool isNewSymitarLoan)
        {
            if (lenderConfig == null)
            {
                throw new ArgumentNullException(nameof(lenderConfig));
            }
            else if (!lenderConfig.AllowExportToSymitar)
            {
                throw new Security.AccessDenied();
            }

            var dataMap = new Export.HardcodedDataExportResolver(memberAccountNumber, symitarLoanId, isNewSymitarLoan);
            var exportRecord = new Export.SymitarExportRecord(export.TableToExport, dataMap);
            bool exportResult;
            using (var server = SymitarServer.Create(lenderConfig.IpAddress.Value, lenderConfig.Ports.Value))
            {
                string standardizedMemberAccountNumber = StandardizeAccountNumber(memberAccountNumber);

                exportResult = exportRecord.ExportEntireRecord(server, lenderConfig, standardizedMemberAccountNumber);
            }

            if (exportResult)
            {
                Audit.AuditManager.RecordAudit(export.LoanId, Audit.SymitarAuditItem.CreateExportAuditItem(principal));
            }

            return exportResult;
        }

        /// <summary>
        /// Loads a raw export of data for transmission to Symitar.
        /// </summary>
        /// <param name="lenderConfig">The lender configuration for Symitar.</param>
        /// <param name="loanId">The loan to export.</param>
        /// <param name="appId">The app to export.</param>
        /// <param name="principal">The user who is performing the export.</param>
        /// <returns>An export of data.</returns>
        public static Export.SymitarDataExport LoadExportToSymitar(LenderConfiguration lenderConfig, Guid loanId, Guid appId, Security.AbstractUserPrincipal principal)
        {
            if (lenderConfig == null)
            {
                throw new ArgumentNullException(nameof(lenderConfig));
            }
            else if (!lenderConfig.AllowExportToSymitar)
            {
                throw new Security.AccessDenied(); // enforcement is not strictly needed since this doesn't export to Symitar yet, but this does simplify later operations.
            }

            var batchExportRequest = new XsltExportReport.XsltExportRequest(principal.UserId, new Guid[] { loanId }, lenderConfig.BatchExportReportName, null, null);
            var batchExportResult = XsltExportReport.XsltExport.ExecuteSynchronously(batchExportRequest, principal);
            if (batchExportResult.HasError)
            {
                return new Export.SymitarDataExport(loanId, null, true, "Unable to load loan data to export"); // Send Error message like this
            }

            var document = System.Xml.Linq.XDocument.Load(batchExportResult.OutputFileLocation);
            return Export.SymitarDataExport.ParseFromDocument(document, loanId, appId);
        }

        /// <summary>
        /// Retrieves a list of loans in the specified Symitar account.
        /// </summary>
        /// <param name="lenderConfig">The lender configuration to use when loading the loans.</param>
        /// <param name="memberAccountNumber">The account number to load from.</param>
        /// <returns>A list of loans in the account.</returns>
        public static List<Export.SymitarLoan> GetLoans(LenderConfiguration lenderConfig, string memberAccountNumber)
        {
            if (lenderConfig == null)
            {
                throw new ArgumentNullException(nameof(lenderConfig));
            }

            memberAccountNumber = StandardizeAccountNumber(memberAccountNumber);
            var fieldMappings = new[]
            {
                new Tuple<Import.ISymitarDataPointMapping, Import.DataPointMapping>(new Import.SymitarFieldMapping("ID", Fields.FieldMaker.Character), null), // ignore the mappings into LQB
                new Tuple<Import.ISymitarDataPointMapping, Import.DataPointMapping>(new Import.SymitarFieldMapping("DESCRIPTION", Fields.FieldMaker.Character), null),
            };
            var mapping = new Import.RecordMapping(
                memberAccountNumber,
                "LOAN",
                "ID",
                str => true,
                fieldMappings);
            List<Dictionary<string, string>> rawLoanData;
            using (SymitarServer server = SymitarServer.Create(lenderConfig.IpAddress.Value, lenderConfig.Ports.Value))
            {
                rawLoanData = LoadRecordsFromSymitar(server, mapping, lenderConfig, memberAccountNumber);
            }

            List<Export.SymitarLoan> loans = new List<Export.SymitarLoan>(rawLoanData.Count);
            loans.AddRange(rawLoanData.Select(loanDictionary => new Export.SymitarLoan(loanDictionary["ID"], loanDictionary["DESCRIPTION"])));
            return loans;
        }

        /// <summary>
        /// Loads data from Symitar to import into a loan file.
        /// </summary>
        /// <param name="lenderConfig">The lender configuration to use for connection.</param>
        /// <param name="memberAccountNumber">The member's account number to load from.</param>
        /// <param name="isForBorrower">A boolean value indicating whether the import is targeting borrower fields (as opposed to co-borrower).</param>
        /// <returns>A mapped import of the data from Symitar to specific loan fields.</returns>
        public static MappedSymitarImport LoadImportFromSymitar(LenderConfiguration lenderConfig, string memberAccountNumber, bool isForBorrower)
        {
            if (lenderConfig == null)
            {
                throw new ArgumentNullException(nameof(lenderConfig));
            }
            else if (!lenderConfig.AllowImportFromSymitar)
            {
                throw new Security.AccessDenied();
            }

            memberAccountNumber = StandardizeAccountNumber(memberAccountNumber);

            LosConvert converter = new LosConvert(); // Web Form formatting is fine for now.
            var generalMappings = new Import.SymitarToLqbMapping(memberAccountNumber, isForBorrower);

            var mappedValues = new List<Import.MappedValueIntoLqb>();
            using (SymitarServer server = SymitarServer.Create(lenderConfig.IpAddress.Value, lenderConfig.Ports.Value))
            {
                foreach (var recordMapping in generalMappings.RecordMappings)
                {
                    var loadedRecords = LoadRecordsFromSymitar(server, recordMapping, lenderConfig, memberAccountNumber);
                    foreach (var record in loadedRecords)
                    {
                        foreach (var symitarToLqbMapping in recordMapping.Mappings)
                        {
                            string value = symitarToLqbMapping.Item1.GetString(record, converter);

                            // May need a more detailed check if the field is equal to the default, but not yet
                            if (!string.IsNullOrEmpty(value))
                            {
                                mappedValues.Add(new Import.MappedValueIntoLqb(value, symitarToLqbMapping.Item2));
                            }
                        }
                    }
                }
            }

            return new MappedSymitarImport(mappedValues);
        }

        /// <summary>
        /// Builds a basic message to Symitar, initializing fundamental authentication and communication fields.
        /// </summary>
        /// <param name="messageType">The type of message to send to Symitar.</param>
        /// <param name="lenderConfig">The lender configuration to use to determine the authentication values.</param>
        /// <param name="accountNumber">The account number to connect to.</param>
        /// <returns>A Symitar message with only basic header information filled out.</returns>
        internal static SymitarMessage BuildMessage(SymitarMessageType messageType, LenderConfiguration lenderConfig, string accountNumber)
        {
            var message = new SymitarMessage(messageType);
            message.MessageID = "TEMP"; // A placeholder; the server will reassign to use as necessary
            message.DeviceNumber = lenderConfig.DeviceNumber.Value;
            message.DeviceType = lenderConfig.DeviceType.Value;
            message.AccountNumber = "CARD";
            message.Password = lenderConfig.VirtualCardPrefix.Value + accountNumber;
            return message;
        }

        /// <summary>
        /// Validates and cleans a value into a usable Symitar account number.
        /// </summary>
        /// <param name="memberAccountNumber">The Symitar account number to standardize.</param>
        /// <returns>A bona fide account number, left-padded with zeros to have a character count equal to the max length.</returns>
        /// <exception cref="InvalidAccountNumberException"><paramref name="memberAccountNumber"/> is not a valid account number.</exception>
        private static string StandardizeAccountNumber(string memberAccountNumber)
        {
            const int MaxLength = 10;
            if (string.IsNullOrWhiteSpace(memberAccountNumber))
            {
                throw new InvalidAccountNumberException(memberAccountNumber);
            }

            memberAccountNumber = memberAccountNumber.Trim();
            if (memberAccountNumber.Any(ch => !char.IsDigit(ch)))
            {
                throw new InvalidAccountNumberException(memberAccountNumber, "The value should be entirely digit characters.");
            }
            else if (memberAccountNumber.Length > MaxLength)
            {
                throw new InvalidAccountNumberException(memberAccountNumber, "The value has " + memberAccountNumber.Length + " characters, which is longer than the maximum of " + MaxLength + ".");
            }

            return memberAccountNumber.PadLeft(MaxLength, '0'); // the way Symitar wants it.
        }

        /// <summary>
        /// Loads records from Symitar from the specified record mapping.
        /// </summary>
        /// <param name="server">The server connection to load from.</param>
        /// <param name="recordMapping">The record mapping that we are attempting to load.</param>
        /// <param name="lenderConfig">The lender configuration to use for message parameters.</param>
        /// <param name="memberAccountNumber">The account number to access and use for credentials.</param>
        /// <returns>A list of dictionaries of the data for each record specified by the mapping.</returns>
        private static List<Dictionary<string, string>> LoadRecordsFromSymitar(SymitarServer server, Import.RecordMapping recordMapping, LenderConfiguration lenderConfig, string memberAccountNumber)
        {
            SymitarMessage initialMessage = BuildInquiry(lenderConfig, memberAccountNumber, recordMapping.RecordPath);
            initialMessage.ParameterFields.Add(new KeyValuePair<string, string>(recordMapping.InitialQueryPoint, "ALL"));
            string initialMessageId = server.Write(initialMessage);
            var initialResponse = server.Retrieve(initialMessageId);

            var messageIds = initialResponse.ParameterFields.Where(field => field.Key == recordMapping.InitialQueryPoint)
                    .SelectIndicesWhere(field => recordMapping.KeepItem(field.Value))
                    .Select(recordIndex => BuildInquiry(lenderConfig, memberAccountNumber, recordMapping.RecordPath, recordMapping.AllQueryFields, recordIndex))
                    .Select(message => server.Write(message))
                    .ToList();

            var results = new List<Dictionary<string, string>>(messageIds.Count);
            foreach (var msgId in messageIds)
            {
                results.Add(
                    server.Retrieve(msgId)
                    .ParameterFields.ToDictionary(field => field.Key, field => field.Value));
            }

            return results;
        }

        /// <summary>
        /// Builds an inquiry message using the specified parameters.
        /// </summary>
        /// <param name="lenderConfig">The lender configuration to use for the default field settings.</param>
        /// <param name="accountNumber">The account number to use for authentication.</param>
        /// <param name="recordPath">The hierarchic record path to query.</param>
        /// <param name="queryFields">The list of fields to query.</param>
        /// <param name="index">The index of the record to query.</param>
        /// <returns>A query message, ready to be sent to Symitar.</returns>
        private static SymitarMessage BuildInquiry(LenderConfiguration lenderConfig, string accountNumber, string recordPath, IEnumerable<string> queryFields = null, int? index = null)
        {
            var message = BuildMessage(SymitarMessageType.Inquiry, lenderConfig, accountNumber);
            message.HierarchicRecordPath = recordPath + (index.HasValue ? "=" + index : null);
            if (queryFields != null)
            {
                foreach (var fieldName in queryFields)
                {
                    message.ParameterFields.Add(new KeyValuePair<string, string>(fieldName, null));
                }
            }

            return message;
        }

        /// <summary>
        /// An exception to throw for an invalid account number.  The user message will point the user to the action necessary.
        /// </summary>
        private class InvalidAccountNumberException : CBaseException
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="InvalidAccountNumberException"/> class.
            /// </summary>
            /// <param name="accountNumber">The account number that was invalid.</param>
            /// <param name="additionalInstructions">Any additional instructions to pass to the user beyond the invalidity of the account number.</param>
            public InvalidAccountNumberException(string accountNumber, string additionalInstructions = null)
                : base(
                      "Account Number was not valid." + (additionalInstructions != null ? " " + additionalInstructions : null),
                      "User input account number \"" + accountNumber + "\", which was recognized as invalid.")
            {
            }
        }
    }
}
