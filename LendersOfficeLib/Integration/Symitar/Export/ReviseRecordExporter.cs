﻿namespace LendersOffice.Integration.Symitar.Export
{
    /// <summary>
    /// Represents an export operation to update an existing record in Symitar.
    /// </summary>
    public class ReviseRecordExporter : RecordExporter
    {
        /// <summary>
        /// Gets the type of file maintenance being performed.
        /// </summary>
        /// <value>The type of file maintenance being performed.</value>
        protected override string FileMaintenanceType
        {
            get { return "REVISE"; }
        }
    }
}