﻿namespace LendersOffice.Integration.Symitar.Export
{
    using System;
    using System.Linq;

    /// <summary>
    /// Contains the result of a single message transmission to Symitar.
    /// </summary>
    public class SymitarMessageResult
    {
        /// <summary>
        /// The response message we received from Symitar.
        /// </summary>
        private readonly SymitarMessage response;

        /// <summary>
        /// Initializes a new instance of the <see cref="SymitarMessageResult"/> class.
        /// </summary>
        /// <param name="response">The message response we received from Symitar.</param>
        internal SymitarMessageResult(SymitarMessage response)
        {
            this.response = response;
            var status = ParseStatus(response.ResponseStatus);
            this.StatusCode = status.Item1;
            this.StatusMessage = status.Item2;
        }

        /// <summary>
        /// Gets a value indicating whether the message was successful.
        /// </summary>
        /// <value>A value indicating whether the message was successful.</value>
        public bool Success
        {
            get { return this.StatusCode == 0; }
        }

        /// <summary>
        /// Gets a number indicating the status of the message.
        /// </summary>
        /// <value>A number indicating the status of the message.</value>
        public int StatusCode { get; private set; }

        /// <summary>
        /// Gets a message returned by Symitar to indicate what went wrong.
        /// </summary>
        /// <value>A message returned by Symitar to indicate what went wrong.</value>
        public string StatusMessage { get; private set; }

        /// <summary>
        /// Retrieves a record identifier from the response to the message.
        /// </summary>
        /// <param name="identifierType">The type of record identifier to look for in the response.</param>
        /// <returns>A record identifier retrieved from the message, or null if none was found.</returns>
        public RecordIdentifier GetNewRecordIdentifier(IRecordIdentifierType identifierType)
        {
            var identifierValue = this.response.ParameterFields.SingleOrDefault(field => field.Key == identifierType.ColumnName).Value;
            return identifierValue == null ? null : new RecordIdentifier(identifierType, identifierValue);
        }

        /// <summary>
        /// Parses the status of the message into a status code and message.
        /// </summary>
        /// <param name="responseStatus">The string of the response status from the message.</param>
        /// <returns>A tuple of the status code and message.</returns>
        private static Tuple<int, string> ParseStatus(string responseStatus)
        {
            if (responseStatus == null)
            {
                return Tuple.Create(-1, LendersOffice.Common.ErrorMessages.Generic);
            }

            int colonIndex = responseStatus.IndexOf(':');
            string statusCode = colonIndex < 0 ? responseStatus : responseStatus.Remove(colonIndex);
            string statusMessage = colonIndex < 0 ? string.Empty : responseStatus.Substring(colonIndex + 1);
            return Tuple.Create(int.Parse(statusCode), statusMessage);
        }
    }
}
