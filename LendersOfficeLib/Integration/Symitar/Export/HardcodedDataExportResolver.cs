﻿namespace LendersOffice.Integration.Symitar.Export
{
    using System.Collections.Generic;

    /// <summary>
    /// Provides a simple, hardcoded approach to the export resolution.
    /// </summary>
    public class HardcodedDataExportResolver : IDataExportResolver
    {
        /// <summary>
        /// A simple dictionary from table name to the record identifier.
        /// </summary>
        private readonly Dictionary<string, RecordIdentifier> tableToIdLookup = new Dictionary<string, RecordIdentifier>();

        /// <summary>
        /// A simple dictionary from table name to a value indicating if the caller intends to make a new record.
        /// </summary>
        private readonly Dictionary<string, bool> exportingToNewRecordLookup = new Dictionary<string, bool>();

        /// <summary>
        /// Initializes a new instance of the <see cref="HardcodedDataExportResolver"/> class.
        /// </summary>
        /// <param name="accountNumber">The account number to resolve accounts to.</param>
        /// <param name="symitarLoanId">The loan id in Symitar to receive the data of our export.</param>
        /// <param name="isNewSymitarLoan">Indicates whether the caller believes this export will create a new loan in Symitar.</param>
        public HardcodedDataExportResolver(string accountNumber, string symitarLoanId, bool isNewSymitarLoan)
        {
            IdNumberIdentifierType idNumberIdentifierType = new IdNumberIdentifierType();
            this.tableToIdLookup.Add("ACCOUNT", new RecordIdentifier(idNumberIdentifierType, accountNumber));
            this.tableToIdLookup.Add("LOAN", new RecordIdentifier(idNumberIdentifierType, symitarLoanId));

            this.exportingToNewRecordLookup.Add("ACCOUNT", false); // Creating a new account is outside our scope
            this.exportingToNewRecordLookup.Add("LOAN", isNewSymitarLoan);
        }

        /// <summary>
        /// Gets the record identifier for the specified table.
        /// </summary>
        /// <param name="table">The table to look for the identifier.</param>
        /// <returns>The record identifier, or null if not found.</returns>
        public RecordIdentifier GetRecordIdentifier(NestedTableExport table)
        {
            RecordIdentifier foundRecord;
            if (this.tableToIdLookup.TryGetValue(table.SymConnectName, out foundRecord))
            {
                return foundRecord;
            }

            return null;
        }

        /// <summary>
        /// Gets the type of identifier used by the table record.
        /// </summary>
        /// <param name="table">The table record to lookup.</param>
        /// <returns>The record identifier type for the specified table.</returns>
        public IRecordIdentifierType GetRecordIdentifierType(NestedTableExport table)
        {
            if (table.SymConnectName == "LOAN" || table.SymConnectName == "SHARE" || table.SymConnectName == "ACCOUNT")
            {
                return new IdNumberIdentifierType(); // We'll move this logic somewhere else eventually, but this will get us through it.
            }

            return new LocatorNumberIdentifierType();
        }

        /// <summary>
        /// Gets the exporter associated with the particular table export.
        /// </summary>
        /// <param name="table">The table export needing an exporter.</param>
        /// <returns>The record exporter to use.</returns>
        public RecordExporter GetExporter(NestedTableExport table)
        {
            if (table.Fields.Count == 0)
            {
                return new NoOperationExporter(); // No fields means this is a no-op, so we simply use this as a pass through
            }

            bool isNewRecord;
            if (this.exportingToNewRecordLookup.TryGetValue(table.SymConnectName, out isNewRecord))
            {
                return isNewRecord ? (RecordExporter)new CreateRecordExporter() : new ReviseRecordExporter();
            }

            return new CreateRecordExporter(); // For now, we default all records not defined in the dictionary to creating new records
        }
    }
}
