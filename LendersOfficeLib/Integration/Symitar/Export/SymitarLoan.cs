﻿// <copyright file="SymitarLoan.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Michael Leinweaver
//    Date:   5/26/2015
// </summary>
namespace LendersOffice.Integration.Symitar.Export
{
    /// <summary>
    /// Provides a wrapper for Symitar loan information.
    /// </summary>
    public struct SymitarLoan
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SymitarLoan"/>
        /// struct.
        /// </summary>
        /// <param name="id">
        /// The id of the <see cref="SymitarLoan"/>.
        /// </param>
        /// <param name="description">
        /// The description of the <see cref="SymitarLoan"/>.
        /// </param>
        public SymitarLoan(string id, string description) : this()
        {
            this.Id = id;
            this.Description = description;
        }

        /// <summary>
        /// Gets the value for the id of the Symitar loan.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> id of the Symitar loan.
        /// </value>
        public string Id { get; }

        /// <summary>
        /// Gets the value for the description of the Symitar loan.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> description of the Symitar loan.
        /// </value>
        public string Description { get; }
    }
}
