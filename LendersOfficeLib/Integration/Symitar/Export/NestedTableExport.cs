﻿namespace LendersOffice.Integration.Symitar.Export
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    /// Represents a table of data to export to Symitar. This table can be nested.
    /// </summary>
    /// <remarks>
    /// This item is the "raw" output of the batch export, and needs to
    /// be "refined" into a <see cref="SymitarDataExport"/> in order for
    /// the data to be sent to Symitar.
    /// </remarks>
    public class NestedTableExport
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NestedTableExport"/> class.
        /// </summary>
        /// <param name="fullName">The full name of the table.</param>
        /// <param name="name">The name of the table.</param>
        /// <param name="symConnectName">The name of table as Symitar understands.</param>
        /// <param name="fields">The fields to export.</param>
        /// <param name="subRecords">The tables nested within this table.</param>
        [Newtonsoft.Json.JsonConstructor]
        public NestedTableExport(string fullName, string name, string symConnectName, IEnumerable<FieldExport> fields, IEnumerable<NestedTableExport> subRecords)
        {
            this.FullName = fullName;
            this.Name = name;
            this.SymConnectName = symConnectName ?? this.Name;
            this.Fields = new List<FieldExport>(fields);
            this.SubRecords = new List<NestedTableExport>(subRecords);
        }

        /// <summary>
        /// Gets the full name of the table, as put on the Batch Export and presented to the user.
        /// </summary>
        /// <value>The full name of the table.</value>
        public string FullName { get; private set; }

        /// <summary>
        /// Gets the name of the table, as we understand it from the Symitar documentation.
        /// </summary>
        /// <value>The name of the table.</value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the name of the table as we refer to it from the SymConnect interface. May be null if same as <see cref="Name"/>.
        /// </summary>
        /// <value>The name of the table in SymConnect.</value>
        public string SymConnectName { get; private set; }

        /// <summary>
        /// Gets the fields to export to this table.
        /// </summary>
        /// <value>The fields to export to this table.</value>
        public IReadOnlyList<FieldExport> Fields { get; private set; }

        /// <summary>
        /// Gets the sub-records to export under this table.
        /// </summary>
        /// <value>The sub-records to export under this table.</value>
        public IReadOnlyList<NestedTableExport> SubRecords { get; private set; }

        /// <summary>
        /// Parses a table element into the nested structure.
        /// </summary>
        /// <param name="tableElement">The table element to parse.</param>
        /// <returns>The table item.</returns>
        public static NestedTableExport Parse(XElement tableElement)
        {
            if (tableElement == null)
            {
                throw new ArgumentNullException();
            }

            return new NestedTableExport(
                tableElement.Attribute("FullName").Value,
                tableElement.Attribute("Name").Value,
                tableElement.Attribute("SymConnectName")?.Value,
                tableElement.Elements("Field").Select(fieldElement => FieldExport.Parse(fieldElement)),
                tableElement.Elements("Table").Select(subRecord => Parse(subRecord)));
        }
    }
}
