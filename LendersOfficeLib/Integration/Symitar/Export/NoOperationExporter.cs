﻿namespace LendersOffice.Integration.Symitar.Export
{
    /// <summary>
    /// Represents an exporter that performs no export operation.  We use this to export
    /// a child record without needing to export the parent.
    /// </summary>
    public class NoOperationExporter : RecordExporter
    {
        /// <summary>
        /// Gets the type of file maintenance being performed.
        /// </summary>
        /// <value>The type of file maintenance being performed.</value>
        protected override string FileMaintenanceType
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// Exports a record to Symitar, which in this case, does nothing and simply returns a successful result.
        /// </summary>
        /// <param name="record">The record to export.</param>
        /// <param name="server">The server where the record is bound.</param>
        /// <param name="lenderConfig">The lender configuration to use to build messages.</param>
        /// <param name="accountNumber">The account number where the data is bound.</param>
        /// <returns>The result of exporting the record to Symitar.</returns>
        internal override SymitarMessageResult ExportRecord(SymitarExportRecord record, SymitarServer server, LenderConfiguration lenderConfig, string accountNumber)
        {
            // no op for exporting
            return new SymitarMessageResult(SymitarMessage.CreateFakeResponse(SymitarMessageType.FileMaintenanceRequest));
        }
    }
}