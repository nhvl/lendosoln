﻿namespace LendersOffice.Integration.Symitar.Export
{
    using System;
    using System.Linq;
    using System.Xml.Linq;
    using LendersOffice.Security;

    /// <summary>
    /// Represents a set of data to export to Symitar.
    /// </summary>
    /// <remarks>
    /// This item is the "raw" output of the batch export, and, assuming
    /// it is valid, <see cref="TableToExport"/> will need to undergo further
    /// refinement before export.
    /// </remarks>
    public class SymitarDataExport
    {
        /// <summary>
        /// The length of time to save exports before expiring them.
        /// </summary>
        private static readonly TimeSpan SaveTimePeriod = TimeSpan.FromHours(8);

        /// <summary>
        /// Initializes a new instance of the <see cref="SymitarDataExport"/> class.
        /// </summary>
        /// <param name="loanId">The identifier of the loan to export.</param>
        /// <param name="tableToExport">The tables in a nested structure to export to Symitar.</param>
        /// <param name="hasError">A value indicating whether there was an error in the export.</param>
        /// <param name="errorMessage">The error message to display to the user.</param>
        [Newtonsoft.Json.JsonConstructor]
        public SymitarDataExport(Guid loanId, NestedTableExport tableToExport, bool hasError = false, string errorMessage = null)
        {
            this.LoanId = loanId;
            this.TableToExport = tableToExport;
            this.HasError = hasError;
            this.ErrorMessage = errorMessage;
        }

        /// <summary>
        /// Gets the identifier of the loan being exported.
        /// </summary>
        /// <value>The identifier of the loan being exported.</value>
        public Guid LoanId { get; private set; }

        /// <summary>
        /// Gets the tables in a nested structure to export to Symitar.
        /// </summary>
        /// <value>The tables in a nested structure to export to Symitar.</value>
        public NestedTableExport TableToExport { get; private set; }

        /// <summary>
        /// Gets a value indicating whether there was an error in the export.
        /// </summary>
        /// <value>A value indicating whether there was an error in the export.</value>
        public bool HasError { get; private set; }

        /// <summary>
        /// Gets the error message to display to the user.
        /// </summary>
        /// <value>The error message to display to the user.</value>
        public string ErrorMessage { get; private set; }

        /// <summary>
        /// Parse the export for an application from a complete XML document.
        /// </summary>
        /// <param name="document">The document to export.</param>
        /// <param name="loanId">The identifier of the loan to export.</param>
        /// <param name="appId">The identifier of the application to export.</param>
        /// <returns>The export for the specified application.</returns>
        public static SymitarDataExport ParseFromDocument(XDocument document, Guid loanId, Guid appId)
        {
            var applicationElement = document.Descendants("Application").Single(el => new Guid(el.Attribute("Identifier").Value) == appId);
            var tableRootElement = applicationElement.Elements("Table").Select(tableElement => NestedTableExport.Parse(tableElement)).Single();
            return new SymitarDataExport(loanId, tableRootElement);
        }

        /// <summary>
        /// Constructs an instance of <see cref="MappedSymitarImport"/> from the cached version.
        /// </summary>
        /// <param name="principal">The principal who initially loaded and saved the data.</param>
        /// <param name="key">The key of the data.</param>
        /// <returns>The reconstructed MappedSymitarImport.</returns>
        public static SymitarDataExport ConstructFromSavedCopy(AbstractUserPrincipal principal, string key)
        {
            string text = DataAccess.AutoExpiredTextCache.GetUserString(principal, key);
            return string.IsNullOrEmpty(text) ? null : LendersOffice.Common.SerializationHelper.JsonNetDeserializeWithTypeNameHandling<SymitarDataExport>(text);
        }

        /// <summary>
        /// Saves this instance to a cache so it can be reconstructed in a later request.
        /// </summary>
        /// <param name="principal">The principal to save the item under.</param>
        /// <returns>The key, used to retrieve the value later.</returns>
        public string SaveCopy(AbstractUserPrincipal principal)
        {
            var serializedThis = LendersOffice.Common.SerializationHelper.JsonNetSerializeWithTypeNameHandling(this);
            return DataAccess.AutoExpiredTextCache.InsertUserString(principal, serializedThis, SaveTimePeriod);
        }
    }
}