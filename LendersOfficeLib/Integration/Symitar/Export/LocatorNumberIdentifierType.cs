﻿namespace LendersOffice.Integration.Symitar.Export
{
    /// <summary>
    /// Defines a locator based record identifier type.
    /// </summary>
    public class LocatorNumberIdentifierType : IRecordIdentifierType
    {
        /// <summary>
        /// Gets the column name used to identify the record.
        /// </summary>
        /// <value>The column name used to identify the record.</value>
        public string ColumnName
        {
            get { return "LOCATOR"; }
        }

        /// <summary>
        /// Creates an identifier component to the record path from an identifier value.
        /// </summary>
        /// <param name="identifier">The value of the identifier.  This value is usually numeric.</param>
        /// <returns>A record path identifier.</returns>
        public string CreateRecordPathIdentifier(string identifier)
        {
            return "-" + identifier;
        }
    }
}
