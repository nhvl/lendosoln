﻿namespace LendersOffice.Integration.Symitar.Export
{
    using System.Collections.Generic;

    /// <summary>
    /// Represents an abstract exporter of a single record to Symitar.  Subclasses will define whether they are performing an insert or an update.
    /// </summary>
    public abstract class RecordExporter
    {
        /// <summary>
        /// Gets the type of file maintenance being performed.
        /// </summary>
        /// <value>The type of file maintenance being performed.</value>
        protected abstract string FileMaintenanceType { get; }

        /// <summary>
        /// Exports a record along with all the child records.
        /// </summary>
        /// <param name="record">The record to export from.</param>
        /// <param name="server">The server where the record is bound.</param>
        /// <param name="lenderConfig">The lender configuration, used for authentication.</param>
        /// <param name="accountNumber">The account number receiving the record.</param>
        /// <returns>A value indicating whether the export was successful.</returns>
        internal bool ExportRecordAndSubRecords(SymitarExportRecord record, SymitarServer server, LenderConfiguration lenderConfig, string accountNumber)
        {
            var result = this.ExportRecord(record, server, lenderConfig, accountNumber);
            if (!result.Success)
            {
                return false;
            }

            var newRecordPath = record.ComputeRecordPathWithNewIdentifier(result);
            foreach (var subRecord in record.GetSubRecords(newRecordPath))
            {
                if (!subRecord.ExportEntireRecord(server, lenderConfig, accountNumber))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Exports the record into Symitar.
        /// </summary>
        /// <param name="record">The record to export.</param>
        /// <param name="server">The server where the record is bound.</param>
        /// <param name="lenderConfig">The lender configuration to use to build messages.</param>
        /// <param name="accountNumber">The account number where the data is bound.</param>
        /// <returns>The result of exporting the record to Symitar.</returns>
        internal virtual SymitarMessageResult ExportRecord(SymitarExportRecord record, SymitarServer server, LenderConfiguration lenderConfig, string accountNumber)
        {
            var message = SymitarManager.BuildMessage(SymitarMessageType.FileMaintenanceRequest, lenderConfig, accountNumber);
            message.HierarchicRecordPath = record.HierarchicRecordPath.ToString();
            message.ParameterFields.Add(new KeyValuePair<string, string>("FMTYPE", this.FileMaintenanceType));
            foreach (var field in this.AdditionalParameterAdjustments(record.Fields))
            {
                var fieldValue = Fields.FieldMaker.GetFieldMaker(field.DataType).FromLendingQB(field.Value);
                message.ParameterFields.Add(new KeyValuePair<string, string>(field.Mnemonic, fieldValue.ToString()));
            }

            var response = server.Retrieve(server.Write(message));
            return new SymitarMessageResult(response);
        }

        /// <summary>
        /// Adjusts the raw field export into a form necessary for the export.
        /// </summary>
        /// <param name="initialFields">The raw field export.</param>
        /// <returns>A set of fields to add to the message we're exporting.</returns>
        protected virtual IEnumerable<SymitarField> AdditionalParameterAdjustments(IReadOnlyList<SymitarField> initialFields)
        {
            return initialFields;
        }
    }
}
