﻿namespace LendersOffice.Integration.Symitar.Export
{
    using System;
    using System.Xml.Linq;

    /// <summary>
    /// Represents a field of data to export to Symitar.
    /// </summary>
    public class FieldExport
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FieldExport"/> class.
        /// </summary>
        /// <param name="value">The value of the field.</param>
        /// <param name="description">The description of the field.</param>
        /// <param name="dataType">The data type of the field.</param>
        /// <param name="name">The name of the field.</param>
        /// <param name="mnemonic">The mnemonic of the field.</param>
        [Newtonsoft.Json.JsonConstructor]
        public FieldExport(string value, string description, string dataType, string name, string mnemonic)
        {
            this.Value = value;
            this.Description = description;
            this.DataType = dataType;
            this.Name = name;
            this.Mnemonic = mnemonic;
        }

        /// <summary>
        /// Gets the value of the field.
        /// </summary>
        /// <value>The value of the field.</value>
        public string Value { get; private set; }

        /// <summary>
        /// Gets a human-readable description of the field; this is often lender-specific.  If null, use <see cref="Name"/> instead.
        /// </summary>
        /// <example>A field might be named "User Amount 01", but the lender specifies this is "Appraised Value".</example>
        /// <value>A human-readable description of the field.</value>
        public string Description { get; private set; }

        /// <summary>
        /// Gets the data type of the field, to be used for future decisions with validation and formatting.
        /// </summary>
        /// <value>The data type of the field.</value>
        public string DataType { get; private set; }

        /// <summary>
        /// Gets the name of the field, as documented by Symitar.
        /// </summary>
        /// <value>The name of the field.</value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the mnemonic of the field used to refer to it.
        /// </summary>
        /// <value>The mnemonic of the field.</value>
        public string Mnemonic { get; private set; }

        /// <summary>
        /// Parses a field element into the nested structure.
        /// </summary>
        /// <param name="fieldElement">The field element to parse.</param>
        /// <returns>The field.</returns>
        public static FieldExport Parse(XElement fieldElement)
        {
            if (fieldElement == null)
            {
                throw new ArgumentNullException();
            }

            return new FieldExport(
                fieldElement.Element("Value").Value,
                fieldElement.Element("Description")?.Value,
                fieldElement.Element("DataType").Value,
                fieldElement.Element("Name").Value,
                fieldElement.Element("Mnemonic").Value);
        }
    }
}
