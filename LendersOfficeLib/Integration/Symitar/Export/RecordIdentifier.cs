﻿namespace LendersOffice.Integration.Symitar.Export
{
    /// <summary>
    /// Defines an identifier for a record on a table.
    /// </summary>
    public class RecordIdentifier
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RecordIdentifier"/> class.
        /// </summary>
        /// <param name="identifierType">The type of identifier to use.</param>
        /// <param name="identifier">The identifier value on the record.</param>
        public RecordIdentifier(IRecordIdentifierType identifierType, string identifier)
        {
            this.IdentifierType = identifierType;
            this.Identifier = identifier;
        }

        /// <summary>
        /// Gets the type of identifier to use.
        /// </summary>
        /// <value>The type of identifier to use.</value>
        public IRecordIdentifierType IdentifierType { get; private set; }

        /// <summary>
        /// Gets the identifier value on the record.
        /// </summary>
        /// <value>The identifier value on the record.</value>
        public string Identifier { get; private set; }

        /// <summary>
        /// Converts this instance into the string representation, as understood by Symitar.
        /// </summary>
        /// <returns>A string representation of the record identifier in the record path.</returns>
        public override string ToString()
        {
            return this.IdentifierType.CreateRecordPathIdentifier(this.Identifier);
        }
    }
}
