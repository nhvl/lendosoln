﻿namespace LendersOffice.Integration.Symitar.Export
{
    /// <summary>
    /// This interface is a container for dependency injection into the export.
    /// </summary>
    /// <remarks>
    /// For now, the type that implements this is hardcoded, but the idea is that
    /// one day we could actually make this work.
    /// </remarks>
    public interface IDataExportResolver
    {
        /// <summary>
        /// Gets the record identifier for the specified table export.
        /// </summary>
        /// <param name="table">The table export needing an identifier.</param>
        /// <returns>The record identifier, or null if not found.</returns>
        RecordIdentifier GetRecordIdentifier(NestedTableExport table);

        /// <summary>
        /// Gets the type of identifier used by the table record.
        /// </summary>
        /// <param name="table">The table record to lookup.</param>
        /// <returns>The record identifier type for the specified table.</returns>
        IRecordIdentifierType GetRecordIdentifierType(NestedTableExport table);

        /// <summary>
        /// Gets the exporter associated with the particular table export.
        /// </summary>
        /// <param name="table">The table export needing an exporter.</param>
        /// <returns>The record exporter to use.</returns>
        RecordExporter GetExporter(NestedTableExport table);
    }
}
