﻿namespace LendersOffice.Integration.Symitar.Export
{
    /// <summary>
    /// Represents a single entry in a Symitar table.
    /// </summary>
    public class SymitarTableEntry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SymitarTableEntry"/> class.
        /// </summary>
        /// <param name="tableName">The name of the table, as SymConnect understands it.</param>
        /// <param name="identifier">The identifier for this entry of the table.</param>
        public SymitarTableEntry(string tableName, RecordIdentifier identifier)
        {
            this.TableName = tableName;
            this.Identifier = identifier;
        }

        /// <summary>
        /// Gets the name of the table, as SymConnect understands it.
        /// </summary>
        /// <value>The name of the table, as SymConnect understands it.</value>
        public string TableName { get; private set; }

        /// <summary>
        /// Gets the identifier for this entry of the table.
        /// </summary>
        /// <value>The identifier for this entry of the table.</value>
        public RecordIdentifier Identifier { get; private set; }
    }
}
