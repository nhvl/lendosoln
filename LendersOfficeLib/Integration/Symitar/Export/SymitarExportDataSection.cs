﻿// <copyright file="SymitarExportDataSection.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Michael Leinweaver
//    Date:   5/26/2015
// </summary>
namespace LendersOffice.Integration.Symitar.Export
{
    using System.Collections.Generic;

    /// <summary>
    /// Provides a wrapper for an export data section on the 
    /// Symitar export page.
    /// </summary>
    public class SymitarExportDataSection
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SymitarExportDataSection"/>
        /// class.
        /// </summary>
        public SymitarExportDataSection()
        {
            this.Rows = new List<SymitarExportDataSectionRow>();
            this.Subsections = new List<SymitarExportDataSection>();
        }

        /// <summary>
        /// Gets or sets the value for the name of the section.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> name of the section.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the value for the rows within the section.
        /// </summary>
        /// <value>
        /// The list of <see cref="SymitarExportDataSectionRow"/>
        /// instances for the section.
        /// </value>
        public List<SymitarExportDataSectionRow> Rows { get; set; }

        /// <summary>
        /// Gets or sets the value for the subsections of the section.
        /// </summary>
        /// <value>
        /// The list of <see cref="SymitarExportDataSection"/> subsections
        /// for the section.
        /// </value>
        public List<SymitarExportDataSection> Subsections { get; set; }
    }
}
