﻿namespace LendersOffice.Integration.Symitar.Export
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Represents a path to a record within the record hierarchy defined
    /// by Symitar. Instances of this class are immutable.
    /// </summary>
    public class RecordPath
    {
        /// <summary>
        /// The table entries (possibly tied to identifiers) in the current instance.
        /// </summary>
        private IReadOnlyList<SymitarTableEntry> identifiedTableEntries;

        /// <summary>
        /// Initializes a new instance of the <see cref="RecordPath"/> class.
        /// </summary>
        public RecordPath()
            : this(Enumerable.Empty<SymitarTableEntry>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RecordPath"/> class.
        /// </summary>
        /// <param name="identifiedTableEntries">The table entries representing the current instance.</param>
        private RecordPath(IEnumerable<SymitarTableEntry> identifiedTableEntries)
        {
            if (identifiedTableEntries == null || identifiedTableEntries.Any(entry => entry == null))
            {
                throw new ArgumentNullException(nameof(identifiedTableEntries));
            }

            this.identifiedTableEntries = new List<SymitarTableEntry>(identifiedTableEntries);
        }

        /// <summary>
        /// Creates a new <see cref="RecordPath"/> by appending an entry to this instance.
        /// </summary>
        /// <param name="tableEntry">The entry to append.</param>
        /// <returns>The new record path.</returns>
        public RecordPath Add(SymitarTableEntry tableEntry)
        {
            return new RecordPath(this.identifiedTableEntries.Concat(new[] { tableEntry }));
        }

        /// <summary>
        /// Converts this instance into the string representation, as understood by Symitar.
        /// </summary>
        /// <returns>A string representation of the record path.</returns>
        public override string ToString()
        {
            return string.Join(":", this.identifiedTableEntries.Select(tableAndId => tableAndId.TableName + tableAndId.Identifier));
        }
    }
}
