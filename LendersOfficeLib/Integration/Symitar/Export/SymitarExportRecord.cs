﻿namespace LendersOffice.Integration.Symitar.Export
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Represents a record to be exported to Symitar.
    /// </summary>
    public class SymitarExportRecord
    {
        /// <summary>
        /// The export resolver to use to help create instances.
        /// </summary>
        private readonly IDataExportResolver dataMapping;

        /// <summary>
        /// The record path to the parent of the current record.
        /// </summary>
        private readonly RecordPath parentRecordPath;

        /// <summary>
        /// The table entry where the record data is targeted.
        /// </summary>
        private readonly SymitarTableEntry tableEntry;

        /// <summary>
        /// The table to export, along with any nested child tables.
        /// </summary>
        private readonly NestedTableExport table;

        /// <summary>
        /// Initializes a new instance of the <see cref="SymitarExportRecord"/> class.
        /// </summary>
        /// <param name="table">The table to export, along with any nested child tables.</param>
        /// <param name="dataMap">The export resolver used to determine instances.</param>
        /// <param name="parentRecordPath">The record path to the parent record of the record being created.</param>
        public SymitarExportRecord(NestedTableExport table, IDataExportResolver dataMap, RecordPath parentRecordPath = null)
        {
            this.dataMapping = dataMap;
            this.tableEntry = new SymitarTableEntry(table.SymConnectName, dataMap.GetRecordIdentifier(table));
            this.parentRecordPath = parentRecordPath ?? new RecordPath();
            this.HierarchicRecordPath = this.parentRecordPath.Add(this.tableEntry);
            this.Fields = new List<SymitarField>(table.Fields.Select(field => new SymitarField(field.Value, field.Mnemonic, field.DataType)));
            this.table = table;
        }

        /// <summary>
        /// Gets the hierarchic record path to this instance.
        /// </summary>
        /// <value>The hierarchic record path to this instance.</value>
        public RecordPath HierarchicRecordPath { get; private set; }

        /// <summary>
        /// Gets the fields to export.
        /// </summary>
        /// <value>The fields to export.</value>
        public IReadOnlyList<SymitarField> Fields { get; private set; }

        /// <summary>
        /// Initializes the immediate child records to export.
        /// </summary>
        /// <param name="currentRecordPath">The record path to this instance inside Symitar.</param>
        /// <returns>A set of sub records of this instance.</returns>
        /// <remarks>
        /// Though <paramref name="currentRecordPath"/> is often the same as
        /// <seealso cref="HierarchicRecordPath"/>, in some cases exporting a parent record may
        /// yield an identifier to use when exporting to the child. As such, we can only create
        /// child records after the parent export is completed, and must pass in the new record
        /// path.
        /// </remarks>
        public IEnumerable<SymitarExportRecord> GetSubRecords(RecordPath currentRecordPath)
        {
            foreach (var record in this.table.SubRecords)
            {
                yield return new SymitarExportRecord(record, this.dataMapping, currentRecordPath);
            }
        }

        /// <summary>
        /// Computes a new record path from the result of exporting this instance.
        /// </summary>
        /// <param name="result">The result of exporting this instance.</param>
        /// <returns>A record path to this instance inside Symitar.</returns>
        public RecordPath ComputeRecordPathWithNewIdentifier(SymitarMessageResult result)
        {
            if (this.tableEntry.Identifier != null)
            {
                return this.HierarchicRecordPath;
            }

            return this.parentRecordPath.Add(new SymitarTableEntry(
                this.tableEntry.TableName,
                result.GetNewRecordIdentifier(this.dataMapping.GetRecordIdentifierType(this.table))));
        }

        /// <summary>
        /// Exports the entire record to Symitar, including child records.
        /// </summary>
        /// <param name="server">The server to use for export.</param>
        /// <param name="lenderConfig">The configuration to use for items.</param>
        /// <param name="accountNumber">The number of the account in Symitar.</param>
        /// <returns>A value indicating whether the export succeeded.</returns>
        internal bool ExportEntireRecord(SymitarServer server, LenderConfiguration lenderConfig, string accountNumber)
        {
            RecordExporter exporter = this.dataMapping.GetExporter(this.table);
            return exporter.ExportRecordAndSubRecords(this, server, lenderConfig, accountNumber);
        }
    }
}
