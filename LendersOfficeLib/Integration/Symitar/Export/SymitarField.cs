﻿namespace LendersOffice.Integration.Symitar.Export
{
    /// <summary>
    /// Represents a simple field export to Symitar.
    /// </summary>
    /// <remarks>
    /// This class is intended to be the minimal interface necessary
    /// to export data to a field.
    /// </remarks>
    public class SymitarField
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SymitarField"/> class.
        /// </summary>
        /// <param name="value">The value of the field that we're sending to Symitar.</param>
        /// <param name="mnemonic">The mnemonic name by which we refer to the field in SymConnect.</param>
        /// <param name="dataType">The type of the data.</param>
        public SymitarField(string value, string mnemonic, string dataType)
        {
            this.Value = value;
            this.Mnemonic = mnemonic;
            this.DataType = dataType;
        }

        /// <summary>
        /// Gets the type of the data.
        /// </summary>
        /// <value>The type of the data.</value>
        public string DataType { get; private set; }

        /// <summary>
        /// Gets the mnemonic name of the field.
        /// </summary>
        /// <value>The mnemonic name of the field.</value>
        public string Mnemonic { get; private set; }

        /// <summary>
        /// Gets the value of the field.
        /// </summary>
        /// <value>The value of the field.</value>
        public string Value { get; private set; }
    }
}
