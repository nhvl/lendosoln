﻿namespace LendersOffice.Integration.Symitar.Export
{
    /// <summary>
    /// Defines a type of record identifier for a table.
    /// </summary>
    public interface IRecordIdentifierType
    {
        /// <summary>
        /// Gets the column name used to identify the record.
        /// </summary>
        /// <value>The column name used to identify the record.</value>
        string ColumnName { get; }

        /// <summary>
        /// Creates an identifier component to the record path from an identifier value.
        /// </summary>
        /// <param name="identifier">The value of the identifier.  This value is usually numeric.</param>
        /// <returns>A record path identifier.</returns>
        string CreateRecordPathIdentifier(string identifier);
    }
}
