﻿namespace LendersOffice.Integration.Symitar.Export
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Represents an export operation to create a new record in Symitar.
    /// </summary>
    public class CreateRecordExporter : RecordExporter
    {
        /// <summary>
        /// Gets the type of file maintenance being performed.
        /// </summary>
        /// <value>The type of file maintenance being performed.</value>
        protected override string FileMaintenanceType
        {
            get { return "CREATE"; }
        }

        /// <summary>
        /// Adjusts the raw field export into a form necessary for the export.
        /// </summary>
        /// <param name="initialFields">The raw field export.</param>
        /// <returns>A set of fields to add to the message we're exporting.</returns>
        protected override IEnumerable<SymitarField> AdditionalParameterAdjustments(IReadOnlyList<SymitarField> initialFields)
        {
            var typeField = initialFields.FirstOrDefault(field => field.Mnemonic == "TYPE");
            if (typeField != null)
            {
                return new[] { new SymitarField(typeField.Value, "FMRECTYPE", typeField.DataType) }.Concat(initialFields);
            }

            return initialFields;
        }
    }
}