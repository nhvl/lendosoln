﻿// <copyright file="SymitarExportDataSectionRow.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Michael Leinweaver
//    Date:   5/26/2015
// </summary>
namespace LendersOffice.Integration.Symitar.Export
{
    /// <summary>
    /// Provides a wrapper for the values within a row of
    /// an "Export Data" section on the Symitar export
    /// page.
    /// </summary>
    public class SymitarExportDataSectionRow
    {
        /// <summary>
        /// Gets or sets the value for the description
        /// of the data.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> description of the
        /// data.
        /// </value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the value for the Symitar mnemonic
        /// of the data.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> mnemonic.
        /// </value>
        public string Mnemonic { get; set; }

        /// <summary>
        /// Gets or sets the value of the data.
        /// </summary>
        /// <value>
        /// The value of the data.
        /// </value>
        public string Value { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the row has an error.
        /// </summary>
        /// <value>
        /// A value indicating whether the row has an error.
        /// </value>
        public bool HasError { get; set; }

        /// <summary>
        /// Gets or sets the data type of the row's value.
        /// </summary>
        /// <value>
        /// The data type of the row's value.
        /// </value>
        public string ValueDataType { get; set; }
    }
}
