﻿namespace LendersOffice.Integration.VendorCommunication
{
    using System.Text;
    using System.Web;
    using LendersOffice.Common;
    using LqbCommunication;

    /// <summary>
    /// A base handler for the <see cref="LqbCommunicationResponse"/>.
    /// </summary>
    public class CommunicationResponseHandler
    {
        /// <summary>
        /// The HTTP context to populate with a response.
        /// </summary>
        private HttpResponse httpResponse;

        /// <summary>
        /// The status of the response.
        /// </summary>
        private Status status;

        /// <summary>
        /// A supplementary message to send with the response.
        /// </summary>
        private string message;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommunicationResponseHandler"/> class.
        /// </summary>
        /// <param name="httpResponse">The HTTP context to send a response over.</param>
        public CommunicationResponseHandler(HttpResponse httpResponse)
        {
            this.httpResponse = httpResponse;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CommunicationResponseHandler"/> class.
        /// </summary>
        /// <param name="httpResponse">The HTTP context to populate with a response.</param>
        /// <param name="status">The status of the response.</param>
        /// <param name="message">A supplementary message.</param>
        public CommunicationResponseHandler(HttpResponse httpResponse, Status status, string message = null)
        {
            this.httpResponse = httpResponse;
            this.status = status;
            this.message = message;
        }

        /// <summary>
        /// Gets or sets the status of the response.
        /// </summary>
        /// <value>The status of the response.</value>
        public Status Status
        {
            get
            {
                return this.status;
            }

            set 
            {
                this.status = value;
            }
        }

        /// <summary>
        /// Gets or sets a supplementary message to send with the response.
        /// </summary>
        /// <value>A supplementary message to send with the response.</value>
        public string Message
        {
            get
            {
                return this.message;
            }

            set
            {
                this.message = value;
            }
        }

        /// <summary>
        /// Sends the response.
        /// </summary>
        public virtual void SendResponse()
        {
            var response = this.PackageResponse();
            var responseString = SerializationHelper.XmlSerializeStripDefaultNamespace(response, includeXMLDeclaration: true);
            this.LogResponse(responseString);

            this.httpResponse.ContentType = "text/xml";
            this.httpResponse.ContentEncoding = Encoding.UTF8;
            this.httpResponse.Output.Write(responseString);
            this.httpResponse.Flush();
        }

        /// <summary>
        /// Creates an <see cref="LqbCommunicationResponse"/>.
        /// </summary>
        /// <returns>A populated <see cref="LqbCommunicationResponse"/>.</returns>
        protected virtual LqbCommunicationResponse PackageResponse()
        {
            var response = new LqbCommunicationResponse();
            response.Status = this.status;

            if (!string.IsNullOrEmpty(this.message))
            {
                response.Message = this.message;
            }

            return response;
        }

        /// <summary>
        /// Logs the serialized response.
        /// </summary>
        /// <param name="response">The string to log.</param>
        protected virtual void LogResponse(string response)
        {
            CommunicationLogs.LogPostResponse(response);
        }
    }
}
