﻿namespace LendersOffice.Integration.VendorCommunication
{
    using System;
    using System.IO;
    using System.Text;
    using System.Web;
    using Common;
    using DataAccess;
    using LendersOffice.Integration.VendorCommunication.Appraisal;
    using LqbCommunication;

    /// <summary>
    /// Receives <see cref="LqbCommunication"/> transmissions and passes them off to the corresponding
    /// framework for processing.
    /// </summary>
    public class CommunicationPostHandler : IHttpHandler
    {
        /// <summary>
        /// Gets a value indicating whether another request can use the CommunicationPostHandler instance.
        /// </summary>
        /// <value>True, since the handler is reusable.</value>
        public bool IsReusable
        {
            get { return true; }
        }

        /// <summary>
        /// Receives HTTP requests containing vendor communications and sends a synchronous response.
        /// </summary>
        /// <param name="context">The context of the request.</param>
        public void ProcessRequest(HttpContext context)
        {
            HttpRequest request = context.Request;
            var responseHandler = new CommunicationResponseHandler(context.Response);
            var communication = this.ExtractCommunication(request);

            if (communication == null)
            {
                string error = $"Invalid LQBCommunication XML from {request.UserHostName}:{request.UserHostAddress}.";
                CommunicationLogs.LogError(error);

                responseHandler.Status = Status.Error;
                responseHandler.Message = "The XML could not be parsed.";
                responseHandler.SendResponse();
                return;
            }

            try
            {
                this.ImportMessage(communication);
            }
            catch (ArgumentException exc)
            {
                responseHandler.Status = Status.Error;
                responseHandler.Message = exc.Message;
                responseHandler.SendResponse();
                Tools.LogError("Unable to process LQBCommunication request.", exc);
                return;
            }

            responseHandler.Status = Status.Ok;
            responseHandler.SendResponse();
        }

        /// <summary>
        /// Sends a vendor message off to the correct framework for processing.
        /// </summary>
        /// <param name="message">The received message.</param>
        private void ImportMessage(LqbCommunication message)
        {
            CommunicationFrameworkType framework;

            if (!Enum.TryParse(message.TransactionInfo.Framework, out framework)
                || !Enum.IsDefined(typeof(CommunicationFrameworkType), framework))
            {
                throw new ArgumentException($"The Framework attribute {message.TransactionInfo.Framework} is invalid.");
            }
            else if (framework == CommunicationFrameworkType.Appraisal)
            {
                var handler = new AppraisalMessageServer();
                handler.ReceiveMessage(message);
            }
        }

        /// <summary>
        /// Extracts the vendor communication XML from the request.
        /// </summary>
        /// <param name="request">The HTTP request.</param>
        /// <returns>A deserialized vendor communication.</returns>
        private LqbCommunication ExtractCommunication(HttpRequest request)
        {
            string requestXml;
            using (var reader = new StreamReader(request.InputStream, request.ContentEncoding))
            {
                requestXml = reader.ReadToEnd();
            }

            CommunicationLogs.LogPost(requestXml);

            try
            {
                var communication = (LqbCommunication)SerializationHelper.XmlDeserialize(requestXml, typeof(LqbCommunication));
                return communication;
            }
            catch (GenericUserErrorMessageException)
            {
                return null;
            }
        }
    }
}
