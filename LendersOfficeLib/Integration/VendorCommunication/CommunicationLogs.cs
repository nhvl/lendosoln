﻿namespace LendersOffice.Integration.VendorCommunication
{
    using System;
    using DataAccess;

    /// <summary>
    /// Handles logging for the Communication Framework.
    /// </summary>
    public static class CommunicationLogs
    {
        /// <summary>
        /// The log header for a LendingQB communication request.
        /// </summary>
        private const string LogRequestHeader = "=== COMMUNICATION FRAMEWORK REQUEST ===";

        /// <summary>
        /// The log header for a communication response from a vendor.
        /// </summary>
        private const string LogResponseHeader = "=== COMMUNICATION FRAMEWORK RESPONSE ===";

        /// <summary>
        /// The log header for a POST received from a vendor.
        /// </summary>
        private const string LogPostHeader = "=== COMMUNICATION FRAMEWORK POST ===";

        /// <summary>
        /// The log header for our response to a vendor POST.
        /// </summary>
        private const string LogPostResponseHeader = "=== COMMUNICATION FRAMEWORK POST RESPONSE ===";

        /// <summary>
        /// The log header for Communication Framework info.
        /// </summary>
        private const string LogInfoHeader = "=== COMMUNICATION FRAMEWORK INFO ===";

        /// <summary>
        /// The log header for a Communication Framework error.
        /// </summary>
        private const string LogErrorHeader = "=== COMMUNICATION FRAMEWORK ERROR ===";

        /// <summary>
        /// Logs a communication request sent by LendingQB.
        /// </summary>
        /// <param name="requestXml">The request XML.</param>
        /// <param name="url">The destination URL.</param>
        public static void LogRequest(string requestXml, string url)
        {
            LogRequest(requestXml, url, isFrameworkPost: false);
        }

        /// <summary>
        /// Logs a vendor response to a communication request.
        /// </summary>
        /// <param name="responseXml">The response XML.</param>
        public static void LogResponse(string responseXml)
        {
            LogResponse(responseXml, isFrameworkPost: false);
        }

        /// <summary>
        /// Logs a vendor post.
        /// </summary>
        /// <param name="requestXml">The request XML.</param>
        public static void LogPost(string requestXml)
        {
            LogRequest(requestXml, string.Empty, isFrameworkPost: true);
        }

        /// <summary>
        /// Log a response to a vendor post.
        /// </summary>
        /// <param name="responseXml">The response XML.</param>
        /// <param name="overrideHeader">Overrides the communication log header with a custom header.</param>
        public static void LogPostResponse(string responseXml, string overrideHeader = null)
        {
            LogResponse(responseXml, isFrameworkPost: true, overrideHeader: overrideHeader);
        }

        /// <summary>
        /// Log some communication framework info.
        /// </summary>
        /// <param name="infoMessage">The information to log.</param>
        public static void LogInfo(string infoMessage)
        {
            string message = LogInfoHeader + Environment.NewLine + infoMessage;
            Tools.LogInfo(message);
        }
        
        /// <summary>
        /// Logs a communication framework error.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        public static void LogError(string errorMessage)
        {
            string message = LogErrorHeader + Environment.NewLine + errorMessage;
            Tools.LogError(message);
        }

        /// <summary>
        /// Logs a communication request from LendingQB or a vendor.
        /// </summary>
        /// <param name="requestXml">The request XML.</param>
        /// <param name="url">The vendor URL, if sent by LendingQB.</param>
        /// <param name="isFrameworkPost">Indicates whether this represents a POST received from a vendor.</param>
        private static void LogRequest(string requestXml, string url, bool isFrameworkPost)
        {
            string header = isFrameworkPost ? LogPostHeader : LogRequestHeader;

            string urlSegment = string.Empty;
            if (!string.IsNullOrEmpty(url))
            {
                urlSegment = "Url: " + url + Environment.NewLine;
            }

            string message = header + Environment.NewLine
                + urlSegment
                + "Request: " + requestXml;

            Tools.LogInfo(message);
        }

        /// <summary>
        /// Logs a communication response from LendingQB or a vendor.
        /// </summary>
        /// <param name="responseXml">The response XML.</param>
        /// <param name="isFrameworkPost">Indicates whether this represents a response to a POST received from a vendor.</param>
        /// <param name="overrideHeader">Overrides the communication log header with a custom header.</param>
        private static void LogResponse(string responseXml, bool isFrameworkPost, string overrideHeader = null)
        {
            string header = string.Empty;
            if (string.IsNullOrEmpty(overrideHeader))
            {
                header = isFrameworkPost ? LogPostResponseHeader : LogResponseHeader;
            }
            else
            {
                header = overrideHeader;
            }

            string message = header + Environment.NewLine
                + "Response: " + responseXml;

            Tools.LogInfo(message);
        }
    }
}