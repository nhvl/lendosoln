﻿namespace LendersOffice.Integration.VendorCommunication
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LqbCommunication;

    /// <summary>
    /// Sends <see cref="LqbCommunication"/> data out to vendors.
    /// </summary>
    public static class CommunicationServer
    {
        /// <summary>
        /// Transmits a communication to the specified destination.
        /// </summary>
        /// <param name="request">The request data.</param>
        /// <param name="url">The destination URL.</param>
        /// <returns>A response from the vendor, or null if the transmission was unsuccessful.</returns>
        public static LqbCommunicationResponse Submit(LqbCommunication request, string url)
        {
            string requestString = SerializationHelper.XmlSerialize(request);

            CommunicationLogs.LogRequest(requestString, url);

            byte[] requestBytes = Encoding.UTF8.GetBytes(requestString);
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.KeepAlive = false;
            webRequest.Method = "POST";
            webRequest.Timeout = 300000;
            webRequest.ContentType = "text/xml";
            webRequest.ContentLength = requestBytes.Length;

            using (Stream stream = webRequest.GetRequestStream())
            {
                stream.Write(requestBytes, 0, requestBytes.Length);
            }

            string responseXml = string.Empty;
            LqbCommunicationResponse response = null;
            try
            {
                using (var reader = new StreamReader(webRequest.GetResponse().GetResponseStream()))
                {
                    responseXml = reader.ReadToEnd();
                }

                CommunicationLogs.LogResponse(responseXml);
                response = (LqbCommunicationResponse)SerializationHelper.XmlDeserialize(responseXml, typeof(LqbCommunicationResponse));
            }
            catch (GenericUserErrorMessageException exc)
            {
                var errorMessage = DetermineBadResponseErrorMessage(responseXml);
                Tools.LogError(errorMessage, exc);
                throw;
            }

            return response;
        }

        /// <summary>
        /// Determines the error message that should be logged when a vendor response cannot be deserialized.
        /// </summary>
        /// <param name="responseXml">The response XML returned by the vendor.</param>
        /// <returns>The error message to log.</returns>
        private static string DetermineBadResponseErrorMessage(string responseXml)
        {
            try
            {
                var doc = XDocument.Parse(responseXml);
                var rootName = doc.Root.Name.LocalName;
                return $"An LQBCommunicationResponse payload was expected, but the vendor returned a {rootName} payload instead.";
            }
            catch (XmlException)
            {
                return $"The vendor returned a payload that contains invalid XML.";
            }
        }
    }
}
