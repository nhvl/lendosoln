﻿namespace LendersOffice.Integration.VendorCommunication
{
    using System;
    using System.Collections.Generic;
    using LqbCommunication;
    using Security;

    /// <summary>
    /// Provides methods for frontend pages to interact with vendor message.
    /// </summary>
    public abstract class VendorMessageHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VendorMessageHandler"/> class.
        /// </summary>
        /// <param name="loanId">The loan ID associated with this message handler.</param>
        public VendorMessageHandler(Guid loanId)
            : this(loanId, PrincipalFactory.CurrentPrincipal)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VendorMessageHandler"/> class.
        /// </summary>
        /// <param name="loanId">The loan ID associated with this message handler.</param>
        /// <param name="user">The user viewing vendor messages.</param>
        public VendorMessageHandler(Guid loanId, AbstractUserPrincipal user)
        {
            this.LoanId = loanId;
            this.User = user;
        }

        /// <summary>
        /// Gets or sets the ID of the associated loan.
        /// </summary>
        /// <value>The ID of the associated loan.</value>
        protected Guid LoanId { get; set; }

        /// <summary>
        /// Gets or sets the user viewing vendor messages.
        /// </summary>
        /// <value>The user viewing vendor messages.</value>
        protected AbstractUserPrincipal User { get; set; }

        /// <summary>
        /// Retrieves all messages associated with a loan.
        /// </summary>
        /// <returns>A list of messages.</returns>
        public abstract List<VendorMessage> GetAllMessages();

        /// <summary>
        /// Retrieves a list of messages associated with a loan that have
        /// not been read by the current user.
        /// </summary>
        /// <returns>The message IDs associated with any messages unread by the current user.</returns>
        public abstract List<int> GetUnreadMessages();

        /// <summary>
        /// Sets a message as read by the user.
        /// </summary>
        /// <param name="messageId">The message ID.</param>
        public abstract void SetMessageRead(int messageId);

        /// <summary>
        /// Transmits a message to a vendor.
        /// </summary>
        /// <param name="message">The message to send.</param>
        /// <param name="url">The URL to transmit to.</param>
        /// <returns>The response from the vendor.</returns>
        public abstract LqbCommunicationResponse SendMessage(VendorMessage message, string url);
    }
}
