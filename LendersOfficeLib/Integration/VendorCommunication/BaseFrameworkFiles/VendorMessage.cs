﻿namespace LendersOffice.Integration.VendorCommunication
{
    using System;

    /// <summary>
    /// Represents a message to send to or receive from a vendor.
    /// </summary>
    public class VendorMessage
    {
        /// <summary>
        /// Gets or sets a unique ID for the message.
        /// </summary>
        /// <value>A unique ID for the message.</value>
        public int MessageId { get; set; }

        /// <summary>
        /// Gets or sets the vendor who sent or will receive this message.
        /// </summary>
        /// <value>The vendor who sent or will receive this message.</value>
        public Guid VendorId { get; set; }

        /// <summary>
        /// Gets or sets the lender associated with this message.
        /// </summary>
        /// <value>The lender associated with this message.</value>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets the loan ID associated with this message.
        /// </summary>
        /// <value>The loan ID associated with this message.</value>
        public Guid LoanId { get; set; }

        /// <summary>
        /// Gets or sets the service order number associated with this message.
        /// </summary>
        /// <value>The service order number associated with this message.</value>
        public string OrderNumber { get; set; }

        /// <summary>
        /// Gets or sets the sender of this message.
        /// </summary>
        /// <value>The sender of this message.</value>
        public string SendingParty { get; set; }

        /// <summary>
        /// Gets or sets the receiver of this message.
        /// </summary>
        /// <value>The receiver of this message.</value>
        public string ReceivingParty { get; set; }

        /// <summary>
        /// Gets or sets the message subject.
        /// </summary>
        /// <value>The message subject.</value>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the message body.
        /// </summary>
        /// <value>The message body.</value>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the point in time this message was sent to or received from the vendor.
        /// </summary>
        /// <value>The point in time this message was sent to or received from the vendor.</value>
        public DateTime Date { get; set; }
    }
}
