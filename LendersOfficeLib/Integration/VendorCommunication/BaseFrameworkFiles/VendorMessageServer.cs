﻿namespace LendersOffice.Integration.VendorCommunication
{
    using System;
    using System.Text.RegularExpressions;
    using System.Xml;
    using Constants;
    using LqbCommunication;

    /// <summary>
    /// Handles transmission and receipt of vendor messages.
    /// </summary>
    public abstract class VendorMessageServer
    {
        /// <summary>
        /// Transmits a message to the given destination.
        /// </summary>
        /// <param name="message">The message to send.</param>
        /// <param name="url">The destination for the message.</param>
        /// <returns>The response from the vendor.</returns>
        public LqbCommunicationResponse SendMessage(VendorMessage message, string url)
        {
            var response = this.TransmitMessage(message, url);
            this.SaveMessage(message);
            return response;
        }

        /// <summary>
        /// Receives an incoming <see cref="LqbCommunication"/> from the post handler and
        /// converts it to a <see cref="VendorMessage"/> to store in the database.
        /// </summary>
        /// <param name="communication">A communication received from a vendor.</param>
        public void ReceiveMessage(LqbCommunication communication)
        {
            var message = this.ConvertToMessage(communication);
            this.SaveMessage(message);
        }

        /// <summary>
        /// Transmits a communication to a vendor at the given URL.
        /// </summary>
        /// <param name="message">A vendor message.</param>
        /// <param name="url">The destination URL.</param>
        /// <returns>The response from the vendor.</returns>
        protected abstract LqbCommunicationResponse TransmitMessage(VendorMessage message, string url);

        /// <summary>
        /// Saves a message to the appropriate database. This should be called to record both
        /// incoming and outgoing messages.
        /// </summary>
        /// <param name="message">The message to save to the database.</param>
        protected abstract void SaveMessage(VendorMessage message);

        /// <summary>
        /// Converts an <see cref="LqbCommunication"/> to a <see cref="VendorMessage"/>.
        /// </summary>
        /// <param name="communication">The communication to convert.</param>
        /// <returns>A populated <see cref="VendorMessage"/> object.</returns>
        protected abstract VendorMessage ConvertToMessage(LqbCommunication communication);

        /// <summary>
        /// Converts a <see cref="VendorMessage"/> to a serializable <see cref="LqbCommunication"/>.
        /// </summary>
        /// <param name="message">The message to transmit.</param>
        /// <param name="framework">The integration framework associated with this message.</param>
        /// <returns>An <see cref="LqbCommunication"/> for transmission.</returns>
        protected virtual LqbCommunication ConvertToCommunication(VendorMessage message, CommunicationFrameworkType framework)
        {
            var communication = new LqbCommunication();

            communication.TransactionInfo = new TransactionInfo();
            communication.TransactionInfo.Framework = framework.ToString();
            communication.TransactionInfo.LoanId = message.LoanId.ToString();
            communication.TransactionInfo.OrderNumber = message.OrderNumber;

            communication.SendingParty = message.SendingParty;
            communication.ReceivingParty = message.ReceivingParty;
            communication.Subject = message.Subject;
            communication.Message_rep = message.Message;
            communication.PostBackUrl = ConstStage.CommunicationFrameworkPostBackUrl;

            return communication;
        }
    }
}
