﻿namespace LendersOffice.Integration.VendorCommunication
{
    /// <summary>
    /// Designates the recipient of an appraisal vendor communication.
    /// </summary>
    public enum AppraisalMessageRecipient
    {
        /// <summary>
        /// The recipient is the appraiser.
        /// </summary>
        Appraiser,

        /// <summary>
        /// The recipient is the AMC.
        /// </summary>
        AMC
    }
}
