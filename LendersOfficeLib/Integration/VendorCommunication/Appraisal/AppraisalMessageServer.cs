﻿namespace LendersOffice.Integration.VendorCommunication.Appraisal
{
    using System;
    using System.Data.Common;
using System.Data.SqlClient;
    using Common;
    using DataAccess;
    using LqbCommunication;

    /// <summary>
    /// Handles transmission and receipt of Appraisal vendor messages.
    /// </summary>
    public class AppraisalMessageServer : VendorMessageServer
    {
        /// <summary>
        /// Transmits a message to the given destination.
        /// </summary>
        /// <param name="message">The message to send.</param>
        /// <param name="url">The destination URL.</param>
        /// <returns>The response from the vendor.</returns>
        /// <exception cref="GenericUserErrorMessageException">Throws if the message is not successfully sent.</exception>
        protected override LqbCommunicationResponse TransmitMessage(VendorMessage message, string url)
        {
            var communication = this.ConvertToCommunication(message, CommunicationFrameworkType.Appraisal);
            var response = CommunicationServer.Submit(communication, url);

            if (response.Status == Status.Error)
            {
                string errorMessage = string.IsNullOrEmpty(response.Message)
                    ? "The message was not successfully sent."
                    : response.Message;

                throw new GenericUserErrorMessageException(errorMessage);
            }

            return response;
        }

        /// <summary>
        /// Saves a message to the database before sending or after receiving it.
        /// </summary>
        /// <param name="message">The message to save.</param>
        protected override void SaveMessage(VendorMessage message)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@VendorId", message.VendorId),
                new SqlParameter("@BrokerId", message.BrokerId),
                new SqlParameter("@LoanId", message.LoanId),
                new SqlParameter("@OrderNumber", message.OrderNumber),
                new SqlParameter("@SendingParty", message.SendingParty),
                new SqlParameter("@ReceivingParty", message.ReceivingParty),
                new SqlParameter("@Subject", message.Subject),
                new SqlParameter("@Message", message.Message),
                new SqlParameter("@Date", message.Date),
            };

            StoredProcedureHelper.ExecuteNonQuery(message.BrokerId, "APPRAISAL_VENDOR_MESSAGES_SaveMessage", 3, parameters);
        }

        /// <summary>
        /// Converts an <see cref="LqbCommunication"/> to an appraisal framework vendor message.
        /// </summary>
        /// <param name="communication">The <see cref="LqbCommunication"/> XML.</param>
        /// <returns>A <see cref="VendorMessage"/> object.</returns>
        /// <exception cref="ArgumentException">Throws if there is an issue deriving the loan, broker, or vendor ID.</exception>
        protected override VendorMessage ConvertToMessage(LqbCommunication communication)
        {
            var message = new VendorMessage();

            Guid loanId;
            if (!Guid.TryParse(communication.TransactionInfo.LoanId, out loanId))
            {
                throw new ArgumentException($"The loan ID {communication.TransactionInfo.LoanId} is not a valid GUID.");
            }

            Guid brokerId = Guid.Empty;
            try
            {
                brokerId = Tools.GetBrokerIdByLoanID(loanId);
            }
            catch (NotFoundException)
            {
                throw new ArgumentException($"The loan ID {loanId} is not a valid loan, or is not associated with a valid lender.");
            }

            Guid vendorId = this.GetVendorForAppraisalOrder(loanId, brokerId, communication.TransactionInfo.OrderNumber);
            if (vendorId == Guid.Empty)
            {
                throw new ArgumentException($"The order number {communication.TransactionInfo.OrderNumber} does not exist for the loan {loanId}.");
            }

            message.VendorId = vendorId;
            message.BrokerId = brokerId;
            message.LoanId = loanId;
            message.OrderNumber = communication.TransactionInfo.OrderNumber;
            message.SendingParty = communication.SendingParty;
            message.ReceivingParty = communication.ReceivingParty;
            message.Subject = communication.Subject;
            message.Message = communication.Message_rep;
            message.Date = DateTime.Now;

            return message;
        }

        /// <summary>
        /// Gets the vendor ID associated with the given loan and order.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="orderNumber">The appraisal order number.</param>
        /// <returns>The associated vendor ID.</returns>
        private Guid GetVendorForAppraisalOrder(Guid loanId, Guid brokerId, string orderNumber)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@sLId", loanId),
                new SqlParameter("@OrderNumber", orderNumber)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetAppraisalOrderById", parameters))
            {
                if (reader.Read())
                {
                    return (Guid)reader["VendorId"];
                }
            }

            return Guid.Empty;
        }
    }
}
