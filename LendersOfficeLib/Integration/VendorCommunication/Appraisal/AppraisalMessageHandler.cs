﻿namespace LendersOffice.Integration.VendorCommunication.Appraisal
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Integration.Appraisals;
    using LendersOffice.Integration.VendorCommunication;
    using LqbCommunication;
    using Security;

    /// <summary>
    /// Provides methods for the Appraisal frontend to handle messages.
    /// </summary>
    public class AppraisalMessageHandler : VendorMessageHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppraisalMessageHandler"/> class.
        /// </summary>
        /// <param name="loanId">The ID of the associated loan.</param>
        public AppraisalMessageHandler(Guid loanId)
            : base(loanId)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppraisalMessageHandler"/> class.
        /// </summary>
        /// <param name="loanId">The ID of the associated loan.</param>
        /// <param name="user">The user viewing appraisal messages.</param>
        public AppraisalMessageHandler(Guid loanId, AbstractUserPrincipal user)
            : base(loanId, user)
        {
        }

        /// <summary>
        /// Retrieves all messages associated with the loan.
        /// </summary>
        /// <returns>A list of messages.</returns>
        public override List<VendorMessage> GetAllMessages()
        {
            var messages = new List<VendorMessage>();
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", this.User.BrokerId),
                new SqlParameter("@LoanId", this.LoanId)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(this.User.BrokerId, "APPRAISAL_VENDOR_MESSAGES_RetrieveByLoan", parameters))
            {
                while (reader.Read())
                {
                    var message = new VendorMessage();
                    message.MessageId = (int)reader["MessageId"];
                    message.VendorId = (Guid)reader["VendorId"];
                    message.BrokerId = (Guid)reader["BrokerId"];
                    message.LoanId = (Guid)reader["LoanId"];
                    message.OrderNumber = (string)reader["OrderNumber"];
                    message.SendingParty = (string)reader["SendingParty"];
                    message.ReceivingParty = (string)reader["ReceivingParty"];
                    message.Subject = (string)reader["Subject"];
                    message.Message = (string)reader["Message"];
                    message.Date = (DateTime)reader["Date"];

                    messages.Add(message);
                }
            }

            return messages;
        }

        /// <summary>
        /// Retrieves the IDs of all messages associated with the current loan
        /// that have not been read by the current user.
        /// </summary>
        /// <returns>A list of message IDs.</returns>
        public override List<int> GetUnreadMessages()
        {
            var messageIds = new List<int>();
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", this.User.BrokerId),
                new SqlParameter("@LoanId", this.LoanId),
                new SqlParameter("@UserId", this.User.UserId)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(this.User.BrokerId, "APPRAISAL_VENDOR_MESSAGES_GetUnreadMessages", parameters))
            {
                while (reader.Read())
                {
                    var messageId = (int)reader["MessageId"];
                    messageIds.Add(messageId);
                }
            }

            return messageIds;
        }

        /// <summary>
        /// Sets the given message as read by the current user.
        /// </summary>
        /// <param name="messageId">The message to set as read.</param>
        public override void SetMessageRead(int messageId)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@MessageId", messageId),
                new SqlParameter("@UserId", this.User.UserId),
                new SqlParameter("@BrokerId", this.User.BrokerId),
                new SqlParameter("@LoanId", this.LoanId)
            };

            StoredProcedureHelper.ExecuteNonQuery(this.User.BrokerId, "APPRAISAL_VENDOR_MESSAGES_SetMessageRead", 3, parameters);
        }

        /// <summary>
        /// Transmits a message to the given URL.
        /// </summary>
        /// <param name="message">The message to send.</param>
        /// <param name="url">The destination URL.</param>
        /// <returns>The response from the vendor.</returns>
        public override LqbCommunicationResponse SendMessage(VendorMessage message, string url)
        {
            var server = new AppraisalMessageServer();
            return server.SendMessage(message, url);
        }

        /// <summary>
        /// Generates a message and sends it to the specified appraisal vendor.
        /// </summary>
        /// <param name="loanId">The associated loan ID.</param>
        /// <param name="vendor">The recipient appraisal vendor.</param>
        /// <param name="principal">The current user.</param>
        /// <param name="orderNumber">The associated order number.</param>
        /// <param name="receivingParty">The recipient of the message.</param>
        /// <param name="subject">The message subject.</param>
        /// <param name="body">The message body.</param>
        /// <returns>The response from the vendor.</returns>
        public LqbCommunicationResponse GenerateAndSendMessage(Guid loanId, AppraisalVendorConfig vendor, AbstractUserPrincipal principal, string orderNumber, AppraisalMessageRecipient receivingParty, string subject, string body)
        {
            var message = new VendorMessage();
            message.BrokerId = principal.BrokerId;
            message.LoanId = loanId;
            message.VendorId = vendor.VendorId;

            message.OrderNumber = orderNumber;
            message.Subject = subject;
            message.Message = body;
            message.ReceivingParty = receivingParty.ToString();
            message.SendingParty = principal.DisplayName;
            message.Date = DateTime.Now;

            return this.SendMessage(message, vendor.PrimaryExportPath);
        }
    }
}
