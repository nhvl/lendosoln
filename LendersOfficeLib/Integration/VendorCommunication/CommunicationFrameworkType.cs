﻿namespace LendersOffice.Integration.VendorCommunication
{
    /// <summary>
    /// Indicates which integration frameworks currently make use of the communication framework.
    /// </summary>
    public enum CommunicationFrameworkType
    {
        /// <summary>
        /// The appraisal framework.
        /// </summary>
        Appraisal = 0,
    }
}
