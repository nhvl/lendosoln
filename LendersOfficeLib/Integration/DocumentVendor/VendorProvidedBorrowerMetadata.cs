﻿namespace LendersOffice.Integration.DocumentVendor
{
    /// <summary>
    /// Holds parsed vendor-submitted borrower metadata.
    /// </summary>
    public class VendorProvidedBorrowerMetadata
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VendorProvidedBorrowerMetadata"/> class.
        /// </summary>
        public VendorProvidedBorrowerMetadata()
        {
        }

        /// <summary>
        /// Gets or sets the borrower's consumer ID.
        /// </summary>
        public string ConsumerId { get; set; } = null;

        /// <summary>
        /// Gets or sets the borrower's first name.
        /// </summary>
        public string BorrFirstName { get; set; } = null;

        /// <summary>
        /// Gets or sets the borrower's middle name.
        /// </summary>
        public string BorrMiddleName { get; set; } = null;

        /// <summary>
        /// Gets or sets the borrower's last name.
        /// </summary>
        public string BorrLastName { get; set; } = null;

        /// <summary>
        /// Gets or sets the borrower's name suffix.
        /// </summary>
        public string BorrSuffix { get; set; } = null;

        /// <summary>
        /// Gets or sets the borrower's full name.
        /// </summary>
        public string BorrFullName { get; set; } = null;

        /// <summary>
        /// Gets or sets the borrowers email.
        /// </summary>
        public string BorrEmail { get; set; } = null;

        /// <summary>
        /// Gets or sets the delivery method.
        /// </summary>
        public string DeliveryMethod { get; set; } = null;

        /// <summary>
        /// Gets or sets the date the disclosure was issued to the borrower.
        /// </summary>
        public string IssuedDate { get; set; } = null;

        /// <summary>
        /// Gets or sets the date the disclosure was received by the borrower.
        /// </summary>
        public string ReceivedDate { get; set; } = null;

        /// <summary>
        /// Gets or sets the date the disclosure was signed by the borrower.
        /// </summary>
        public string SignedDate { get; set; } = null;
    }
}
