﻿namespace LendersOffice.Integration.DocumentVendor
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using DataAccess;

    /// <summary>
    /// Holds parsed vendor-submitted disclosure metadata.
    /// </summary>
    [Serializable]
    public class VendorProvidedDisclosureMetadata : ISerializable
    {        
        /// <summary>
        /// Initializes a new instance of the <see cref="VendorProvidedDisclosureMetadata"/> class.
        /// Used for deserialization.
        /// </summary>
        /// <param name="info">SerializationInfo object.</param>
        /// <param name="context">StreamingContext object.</param>
        private VendorProvidedDisclosureMetadata(SerializationInfo info, StreamingContext context)
        {
            this.IsClosingDisclosure = info.GetBoolean("IsClosingDisclosure");
            this.LoanId = (Guid)info.GetValue("LoanId", typeof(Guid));
            this.TransactionId = info.GetString("TransactionId");
            this.IssuedDate = info.GetString("IssuedDate");
            this.DeliveryMethod = info.GetString("DeliveryMethod");
            this.ReceivedDate = info.GetString("ReceivedDate");
            this.IsInitial = info.GetString("IsInitial");
            this.IsPreview = info.GetString("IsPreview");
            this.IsFinal = info.GetString("IsFinal");
            this.LastDisclosedTRIDLoanProductDescription = info.GetString("LastDisclosedTRIDLoanProductDescription");
            this.DocVendorApr = info.GetString("DocVendorApr");
            this.SignedDate = info.GetString("SignedDate");
            this.DocCode = info.GetString("DocCode");
            this.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower = info.GetString("IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower");
            this.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller = info.GetString("IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller");
            this.IsDisclosurePostClosingDueToNonNumericalClericalError = info.GetString("IsDisclosurePostClosingDueToNonNumericalClericalError");
            this.IsDisclosurePostClosingDueToCureForToleranceViolation = info.GetString("IsDisclosurePostClosingDueToCureForToleranceViolation");
            this.PostConsummationRedisclosureReasonDate = info.GetString("PostConsummationRedisclosureReasonDate");
            this.PostConsummationKnowledgeOfEventDate = info.GetString("PostConsummationKnowledgeOfEventDate");
            this.BorrowerMetadata = (List<VendorProvidedBorrowerMetadata>)info.GetValue("BorrowerMetadata", typeof(List<VendorProvidedBorrowerMetadata>));
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="VendorProvidedDisclosureMetadata"/> class from being created.
        /// </summary>
        private VendorProvidedDisclosureMetadata()
        {
            this.BorrowerMetadata = new List<VendorProvidedBorrowerMetadata>();
        }

        /// <summary>
        /// Gets a value indicating whether the disclosure is a Closing Disclosure.
        /// </summary>
        public bool IsClosingDisclosure { get; private set; }

        /// <summary>
        /// Gets the associated loan ID.
        /// </summary>
        public Guid LoanId { get; private set; }

        /// <summary>
        /// Gets the transaction ID.
        /// </summary>
        public string TransactionId { get; private set; } = null;

        /// <summary>
        /// Gets the issued date.
        /// </summary>
        public string IssuedDate { get; private set; } = null;

        /// <summary>
        /// Gets the delivery method.
        /// </summary>
        public string DeliveryMethod { get; private set; } = null;

        /// <summary>
        /// Gets the received date.
        /// </summary>
        public string ReceivedDate { get; private set; } = null;

        /// <summary>
        /// Gets a value indicating whether this disclosure is the initial disclosure.
        /// </summary>
        public string IsInitial { get; private set; } = null;

        /// <summary>
        /// Gets a value indicating whether this disclosure is a preview disclosure.
        /// </summary>
        public string IsPreview { get; private set; } = null;

        /// <summary>
        /// Gets a value indicating whether this disclosure is the final disclosure.
        /// </summary>
        public string IsFinal { get; private set; } = null;

        /// <summary>
        /// Gets the last disclosed TRID loan product description.
        /// </summary>
        public string LastDisclosedTRIDLoanProductDescription { get; private set; } = null;

        /// <summary>
        /// Gets the document vendor APR.
        /// </summary>
        public string DocVendorApr { get; private set; } = null;

        /// <summary>
        /// Gets the date the disclosure was signed.
        /// </summary>
        public string SignedDate { get; private set; } = null;

        /// <summary>
        /// Gets the doc code.
        /// </summary>
        public string DocCode { get; private set; } = null;

        /// <summary>
        /// Gets a value indicating whether a post closing disclosure is due to a numerical change
        /// in the amount paid by the borrower.
        /// </summary>
        public string IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower { get; private set; } = null;

        /// <summary>
        /// Gets a value indicating whether a post closing disclosure is due to a numerical change
        /// in the amount paid by the seller.
        /// </summary>
        public string IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller { get; private set; } = null;

        /// <summary>
        /// Gets a value indicating whether a post closing disclosure is due to a non-numerical clerical error.
        /// </summary>
        public string IsDisclosurePostClosingDueToNonNumericalClericalError { get; private set; } = null;

        /// <summary>
        /// Gets a value indicating whether a post closing disclosure is due to a cure for a tolerance violation.
        /// </summary>
        public string IsDisclosurePostClosingDueToCureForToleranceViolation { get; private set; } = null;

        /// <summary>
        /// Gets the post-consummation redisclosure reason date.
        /// </summary>
        public string PostConsummationRedisclosureReasonDate { get; private set; } = null;

        /// <summary>
        /// Gets the post-consummation knowledge of event date.
        /// </summary>
        public string PostConsummationKnowledgeOfEventDate { get; private set; } = null;

        /// <summary>
        /// Gets the borrower metadata associated with this disclosure.
        /// </summary>
        public List<VendorProvidedBorrowerMetadata> BorrowerMetadata { get; private set; }

        /// <summary>
        /// Parses disclosure metadata out of an LQBESign response.
        /// </summary>
        /// <param name="eSignNotification">The LQBESign XML.</param>
        /// <param name="loanId">The associated loan ID.</param>
        /// <returns>An object containing parsed disclosure metadata.</returns>
        public static VendorProvidedDisclosureMetadata ParseLqbESignMetadata(LQBESignUpdate.LQBESignUpdate eSignNotification, Guid loanId)
        {
            if (eSignNotification?.DisclosureMetadata == null)
            {
                return null;
            }

            var metadata = new VendorProvidedDisclosureMetadata();
            metadata.TransactionId = eSignNotification.TransactionID;
            metadata.LoanId = loanId;

            var disclosureData = eSignNotification.DisclosureMetadata.Item;
            if (disclosureData == null)
            {
                Tools.LogError($"A document vendor attempted to update document metadata for loan {metadata.LoanId}, but did not provide any metadata.");
                return null;
            }

            if (disclosureData is LQBESignUpdate.LoanEstimateMetadata)
            {
                var loanEstimate = disclosureData as LQBESignUpdate.LoanEstimateMetadata;
                metadata.IsClosingDisclosure = false;

                metadata.IssuedDate = loanEstimate.IssuedDate;
                metadata.DeliveryMethod = loanEstimate.DeliveryMethod;
                metadata.ReceivedDate = loanEstimate.ReceivedDate;
                metadata.IsInitial = loanEstimate.IsInitial;
                metadata.LastDisclosedTRIDLoanProductDescription = loanEstimate.LastDisclosedTRIDLoanProductDescription;
                metadata.DocVendorApr = loanEstimate.APR;
                metadata.SignedDate = loanEstimate.SignedDate;

                if (loanEstimate.BorrowerDisclosureMetadata != null)
                {
                    var borrowerMetadata = new VendorProvidedBorrowerMetadata();
                    borrowerMetadata.ConsumerId = eSignNotification.ConsumerID;
                    borrowerMetadata.BorrFirstName = eSignNotification.BorrFirstName;
                    borrowerMetadata.BorrMiddleName = eSignNotification.BorrMiddleName;
                    borrowerMetadata.BorrLastName = eSignNotification.BorrLastName;
                    borrowerMetadata.BorrSuffix = eSignNotification.BorrSuffix;
                    borrowerMetadata.BorrFullName = eSignNotification.BorrFullName;
                    borrowerMetadata.BorrEmail = eSignNotification.BorrEmail;
                    borrowerMetadata.DeliveryMethod = loanEstimate.BorrowerDisclosureMetadata.DeliveryMethod;
                    borrowerMetadata.IssuedDate = loanEstimate.BorrowerDisclosureMetadata.IssuedDate;
                    borrowerMetadata.ReceivedDate = loanEstimate.BorrowerDisclosureMetadata.ReceivedDate;
                    borrowerMetadata.SignedDate = loanEstimate.BorrowerDisclosureMetadata.SignedDate;

                    metadata.BorrowerMetadata.Add(borrowerMetadata);
                }
            }
            else if (disclosureData is LQBESignUpdate.ClosingDisclosureMetadata)
            {
                var closingDisclosure = disclosureData as LQBESignUpdate.ClosingDisclosureMetadata;
                metadata.IsClosingDisclosure = true;

                metadata.IssuedDate = closingDisclosure.IssuedDate;
                metadata.DeliveryMethod = closingDisclosure.DeliveryMethod;
                metadata.ReceivedDate = closingDisclosure.ReceivedDate;
                metadata.IsInitial = closingDisclosure.IsInitial;
                metadata.IsPreview = closingDisclosure.IsPreview;
                metadata.IsFinal = closingDisclosure.IsFinal;
                metadata.LastDisclosedTRIDLoanProductDescription = closingDisclosure.LastDisclosedTRIDLoanProductDescription;
                metadata.DocVendorApr = closingDisclosure.APR;
                metadata.SignedDate = closingDisclosure.SignedDate;
                metadata.DocCode = closingDisclosure.DocCode;
                metadata.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower = closingDisclosure.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower;
                metadata.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller = closingDisclosure.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller;
                metadata.IsDisclosurePostClosingDueToNonNumericalClericalError = closingDisclosure.IsDisclosurePostClosingDueToNonNumericalClericalError;
                metadata.IsDisclosurePostClosingDueToCureForToleranceViolation = closingDisclosure.IsDisclosurePostClosingDueToCureForToleranceViolation;
                metadata.PostConsummationRedisclosureReasonDate = closingDisclosure.PostConsummationRedisclosureReasonDate;
                metadata.PostConsummationKnowledgeOfEventDate = closingDisclosure.PostConsummationKnowledgeOfEventDate;

                if (closingDisclosure.BorrowerDisclosureMetadata != null)
                {
                    var borrowerMetadata = new VendorProvidedBorrowerMetadata();
                    borrowerMetadata.ConsumerId = eSignNotification.ConsumerID;
                    borrowerMetadata.BorrFirstName = eSignNotification.BorrFirstName;
                    borrowerMetadata.BorrMiddleName = eSignNotification.BorrMiddleName;
                    borrowerMetadata.BorrLastName = eSignNotification.BorrLastName;
                    borrowerMetadata.BorrSuffix = eSignNotification.BorrSuffix;
                    borrowerMetadata.BorrFullName = eSignNotification.BorrFullName;
                    borrowerMetadata.BorrEmail = eSignNotification.BorrEmail;
                    borrowerMetadata.DeliveryMethod = closingDisclosure.BorrowerDisclosureMetadata.DeliveryMethod;
                    borrowerMetadata.IssuedDate = closingDisclosure.BorrowerDisclosureMetadata.IssuedDate;
                    borrowerMetadata.ReceivedDate = closingDisclosure.BorrowerDisclosureMetadata.ReceivedDate;
                    borrowerMetadata.SignedDate = closingDisclosure.BorrowerDisclosureMetadata.SignedDate;

                    metadata.BorrowerMetadata.Add(borrowerMetadata);
                }
            }
            else
            {
                Tools.LogError($"A document vendor attempted to update disclosure metadata for loan {metadata.LoanId}, but the metadata XML could not be parsed.");
                return null;
            }

            return metadata;
        }

        /// <summary>
        /// Parses disclosure metadata out of an LQBDocumentResponse.
        /// </summary>
        /// <param name="dataValidation">The data validation container from an LQBDocumentResponse.</param>
        /// <param name="transactionId">The associated transaction ID.</param>
        /// <param name="loanId">The associated loan ID.</param>
        /// <returns>An object containing parsed disclosure metadata.</returns>
        public static VendorProvidedDisclosureMetadata ParseLqbDocumentResponseMetadata(LQBDocumentResponse.DataValidation dataValidation, string transactionId, Guid loanId)
        {
            if (dataValidation?.DisclosureMetadata == null)
            {
                return null;
            }

            var metadata = new VendorProvidedDisclosureMetadata();
            metadata.TransactionId = transactionId;
            metadata.LoanId = loanId;

            var disclosureData = dataValidation.DisclosureMetadata.Item;
            if (disclosureData == null)
            {
                Tools.LogError($"A document vendor attempted to update document metadata for loan {metadata.LoanId}, but did not provide any metadata.");
                return null;
            }

            if (disclosureData is LQBDocumentResponse.LoanEstimateMetadata)
            {
                var newData = disclosureData as LQBDocumentResponse.LoanEstimateMetadata;
                metadata.IsClosingDisclosure = false;

                metadata.IssuedDate = newData.IssuedDate;
                metadata.DeliveryMethod = newData.DeliveryMethod;
                metadata.ReceivedDate = newData.ReceivedDate;
                metadata.IsInitial = newData.IsInitial;
                metadata.LastDisclosedTRIDLoanProductDescription = newData.LastDisclosedTRIDLoanProductDescription;
                metadata.DocVendorApr = newData.APR;
                metadata.SignedDate = newData.SignedDate;

                foreach (var borrower in newData.BorrowerDisclosureMetadata)
                {
                    var borrowerMetadata = new VendorProvidedBorrowerMetadata();
                    borrowerMetadata.ConsumerId = borrower.ConsumerID;
                    borrowerMetadata.BorrFirstName = borrower.BorrFirstName;
                    borrowerMetadata.BorrMiddleName = borrower.BorrMiddleName;
                    borrowerMetadata.BorrLastName = borrower.BorrLastName;
                    borrowerMetadata.BorrSuffix = borrower.BorrSuffix;
                    borrowerMetadata.BorrFullName = borrower.BorrFullName;
                    borrowerMetadata.BorrEmail = borrower.BorrEmail;
                    borrowerMetadata.DeliveryMethod = borrower.DeliveryMethod;
                    borrowerMetadata.IssuedDate = borrower.IssuedDate;
                    borrowerMetadata.ReceivedDate = borrower.ReceivedDate;
                    borrowerMetadata.SignedDate = borrower.SignedDate;

                    metadata.BorrowerMetadata.Add(borrowerMetadata);
                }
            }
            else if (disclosureData is LQBDocumentResponse.ClosingDisclosureMetadata)
            {
                var newData = disclosureData as LQBDocumentResponse.ClosingDisclosureMetadata;
                metadata.IsClosingDisclosure = true;

                metadata.IssuedDate = newData.IssuedDate;
                metadata.DeliveryMethod = newData.DeliveryMethod;
                metadata.ReceivedDate = newData.ReceivedDate;
                metadata.IsInitial = newData.IsInitial;
                metadata.IsPreview = newData.IsPreview;
                metadata.IsFinal = newData.IsFinal;
                metadata.LastDisclosedTRIDLoanProductDescription = newData.LastDisclosedTRIDLoanProductDescription;
                metadata.DocVendorApr = newData.APR;
                metadata.SignedDate = newData.SignedDate;
                metadata.DocCode = newData.DocCode;
                metadata.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower = newData.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower;
                metadata.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller = newData.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller;
                metadata.IsDisclosurePostClosingDueToNonNumericalClericalError = newData.IsDisclosurePostClosingDueToNonNumericalClericalError;
                metadata.IsDisclosurePostClosingDueToCureForToleranceViolation = newData.IsDisclosurePostClosingDueToCureForToleranceViolation;
                metadata.PostConsummationRedisclosureReasonDate = newData.PostConsummationRedisclosureReasonDate;
                metadata.PostConsummationKnowledgeOfEventDate = newData.PostConsummationKnowledgeOfEventDate;

                foreach (var borrower in newData.BorrowerDisclosureMetadata)
                {
                    var borrowerMetadata = new VendorProvidedBorrowerMetadata();
                    borrowerMetadata.ConsumerId = borrower.ConsumerID;
                    borrowerMetadata.BorrFirstName = borrower.BorrFirstName;
                    borrowerMetadata.BorrMiddleName = borrower.BorrMiddleName;
                    borrowerMetadata.BorrLastName = borrower.BorrLastName;
                    borrowerMetadata.BorrSuffix = borrower.BorrSuffix;
                    borrowerMetadata.BorrFullName = borrower.BorrFullName;
                    borrowerMetadata.BorrEmail = borrower.BorrEmail;
                    borrowerMetadata.DeliveryMethod = borrower.DeliveryMethod;
                    borrowerMetadata.IssuedDate = borrower.IssuedDate;
                    borrowerMetadata.ReceivedDate = borrower.ReceivedDate;
                    borrowerMetadata.SignedDate = borrower.SignedDate;

                    metadata.BorrowerMetadata.Add(borrowerMetadata);
                }
            }
            else
            {
                Tools.LogError($"A document vendor attempted to update disclosure metadata for loan {metadata.LoanId}, but the metadata XML could not be parsed.");
                return null;
            }

            return metadata;
        }

        /// <summary>
        /// Method that stores data during the serialization.
        /// </summary>
        /// <param name="info">SerializationInfo object.</param>
        /// <param name="context">StreamingContext object.</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("IsClosingDisclosure", this.IsClosingDisclosure);
            info.AddValue("LoanId", this.LoanId);
            info.AddValue("TransactionId", this.TransactionId);
            info.AddValue("IssuedDate", this.IssuedDate);
            info.AddValue("DeliveryMethod", this.DeliveryMethod);
            info.AddValue("ReceivedDate", this.ReceivedDate);
            info.AddValue("IsInitial", this.IsInitial);
            info.AddValue("IsPreview", this.IsPreview);
            info.AddValue("IsFinal", this.IsFinal);
            info.AddValue("LastDisclosedTRIDLoanProductDescription", this.LastDisclosedTRIDLoanProductDescription);
            info.AddValue("DocVendorApr", this.DocVendorApr);
            info.AddValue("SignedDate", this.SignedDate);
            info.AddValue("DocCode", this.DocCode);
            info.AddValue("IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower", this.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower);
            info.AddValue("IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller", this.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller);
            info.AddValue("IsDisclosurePostClosingDueToNonNumericalClericalError", this.IsDisclosurePostClosingDueToNonNumericalClericalError);
            info.AddValue("IsDisclosurePostClosingDueToCureForToleranceViolation", this.IsDisclosurePostClosingDueToCureForToleranceViolation);
            info.AddValue("PostConsummationRedisclosureReasonDate", this.PostConsummationRedisclosureReasonDate);
            info.AddValue("PostConsummationKnowledgeOfEventDate", this.PostConsummationKnowledgeOfEventDate);
            info.AddValue("BorrowerMetadata", this.BorrowerMetadata);
        }
    }
}
