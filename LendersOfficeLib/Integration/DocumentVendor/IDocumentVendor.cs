﻿using System;
using System.Collections.Generic;
using LendersOffice.Security;
using LqbGrammar.DataTypes;

namespace LendersOffice.Integration.DocumentVendor
{
    public interface IDocumentVendor
    {
        bool CanGenerateDocuments(AbstractUserPrincipal principal, Guid LoanId, string packageId, out string failureReasons);
        DocumentVendorResult<Audit> AuditLoan(Guid sLId, Guid AppId, string PackageId, string LoanProgramId, AbstractUserPrincipal principal, bool preventUsingTemporaryArchive);
        DocumentVendorResult<List<DocumentPackage>> GetAvailableDocumentPackages(Guid LoanId, AbstractUserPrincipal principal);
        DocumentVendorResult<Dictionary<string, IEnumerable<PlanCode>>> GetAvailablePlanCodes(Guid LoanId, AbstractUserPrincipal principal);
        DocumentVendorResult<List<FormInfo>> GetFormList(Guid LoanID, string ProgramID, string PackageID, AbstractUserPrincipal principal);
        DocumentVendorResult<Permissions> GetPermissions(Guid LoanId, bool allowEPortal, bool disableESign, bool allowEClose, bool allowManualFulfillment, string PackageType, AbstractUserPrincipal principal);
        DocumentVendorResult<List<InvestorInfo>> GetTransferToInvestors(string PlanCode);
        DocumentVendorResult<LqbAbsoluteUri> GetIdsLoanFileLink(Guid LoanId, AbstractUserPrincipal principal);
        IDocumentGenerationRequest GetDocumentGenerationRequest(Guid AppId, Guid LoanId);
        Guid BrokerId { get; }
        DVSkin Skin { get; }
        VendorConfig Config { get; }
        IDocumentVendorCredentials Credentials { get; }
        IDocumentVendorCredentials CredentialsFor(Guid UserId, Guid BranchId);
    }
}
