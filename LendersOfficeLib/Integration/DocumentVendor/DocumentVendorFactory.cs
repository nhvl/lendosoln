﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using LendersOffice.Common;

namespace LendersOffice.Integration.DocumentVendor
{
    public static class DocumentVendorFactory
    {
        public static List<VendorConfig> AvailableVendors()
        {
            var ret = new List<VendorConfig>();
            ret.Add(DocMagicDocumentVendor.DocMagicVendorConfig);

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "DOCUMENT_VENDOR_AvailableVendors"))
            {
                while (reader.Read())
                {
                    ret.Add(new VendorConfig(reader));
                }
            }

            return ret;
        }

        public static IDocumentVendor Create(Guid BrokerId, Guid VendorId)
        {
            if (VendorId == Guid.Empty)
            {
                return new DocMagicDocumentVendor(BrokerId);
            }
            else
            {
                var config = VendorConfig.Retrieve(VendorId);
                return new ConfiguredDocumentVendor(config, BrokerId);
            }
        }

        public static List<IDocumentVendor> CurrentVendors()
        {
            return CreateVendorsFor(PrincipalFactory.CurrentPrincipal.BrokerId);
        }
        public static List<IDocumentVendor> CreateVendorsFor(Guid BrokerId)
        {
            return EnumerateVendorsFor(BrokerId).ToList();
        }

        public static IEnumerable<IDocumentVendor> EnumerateVendorsFor(Guid BrokerId)
        {
            foreach (var settings in DocumentVendorBrokerSettings.ListAllForBroker(BrokerId))
            {
                yield return Create(BrokerId, settings.VendorId);
            }
        }
    }
}