﻿namespace LendersOffice.Integration.DocumentVendor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using DataAccess;
    using DocumentCapture;
    using EDocs;
    using EDocs.Contents;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Edocs.DocMagic;
    using LendersOffice.Email;
    using LendersOffice.Integration.DocumentVendor.LQBDocumentResponse;
    using LendersOffice.ObjLib.Billing;
    using LendersOffice.ObjLib.Edocs.AutoSaveDocType;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    public class ConfiguredDocumentGenerationResult : IDocumentGenerationResult
    {
        private static DocumentResponseDocumentFormat[] SupportedFormats = new[] {
            DocumentResponseDocumentFormat.PDF,
            DocumentResponseDocumentFormat.UCD,
            DocumentResponseDocumentFormat.XML
        };

        private static string[] NoBillingLastNames = new[] { 
            "SAMPLE", "Testcase", "Test"
        };

        private static void BillIfClosing(string packageType, VendorConfig config, Guid loanId, bool isPreviewOrder)
        {
            if(!isPreviewOrder && config.TriggersPtmBilling(packageType))
            {
                #region av opm 88157   Lock Document Generation after Closing Documents are Drawn 7/19/2012
                CPageData data = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(ConfiguredDocumentGenerationResult));
                data.InitSave(ConstAppDavid.SkipVersionCheck);
                data.sDocMagicHaveClosingDocsBeenGenerated = true;
                data.Save();
                #endregion

                bool isTestLoan = NoBillingLastNames.Contains(data.GetAppData(0).aBLastNm, StringComparer.CurrentCultureIgnoreCase)
                    || data.sLoanFileT == E_sLoanFileT.Sandbox
                    || data.sLoanFileT == E_sLoanFileT.Test;

                //Don't bill if the last name is a test billing name, or if the requests are going to a test account.
                if (config.IsTest || isTestLoan)
                {
                    string reason = isTestLoan ? "the loan is in test" : "";
                    reason += config.IsTest ? " the broker/user is in test" : "";

                    Tools.LogInfo("A loan [id: " + loanId + "] submitted to vendor [" + config.VendorName + "] by user [login: " +
                                    PrincipalFactory.CurrentPrincipal.LoginNm + "] was not billed for the following reasons: " + reason);

                    if (string.IsNullOrEmpty(reason)) Tools.LogError("A configured vendor PTM loan was not billed for unknown reasons");
                    return;
                }

                try
                {
                    PerTransactionBilling.BillLoan(loanId, E_BillingReason.ClosingDocs, config.PlatformType);
                }
                catch (ArgumentException)
                {
                    Tools.LogBug("A broker without PTB or No Billing enabled requested closing docs, loan id is [" + loanId + "]");
                    throw;
                }
            }
        }

        /// <summary>
        /// Retrieves the documents from a vendor response and processes them into the system.
        /// </summary>
        /// <param name="packageType">The document package type.</param>
        /// <param name="packageTypeName">The name of the document package.</param>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="appId">The application ID.</param>
        /// <param name="config">The document vendor configuration.</param>
        /// <param name="transactionId">The transaction ID.</param>
        /// <param name="responses">A list of documents.</param>
        /// <param name="dataValidation">The data validation received from the vendor, providing information about the data contained in the documents.</param>
        /// <param name="isPreviewOrder">Indicates whether the document order is for a preview package.</param>
        /// <returns>A document vendor result.</returns>
        public static DocumentVendorResult<IDocumentGenerationResult> Create(string packageType, DocumentIntegrationPackageName packageTypeName,
            Guid loanId, Guid brokerId, Guid appId, VendorConfig config, string transactionId, object[] responses, DataValidation dataValidation, bool isPreviewOrder)
        {
            BillIfClosing(packageType, config, loanId, isPreviewOrder);
            Guid? ucdDocumentId;
            Tuple<List<string>, string> tempFileNamesAndErrorMessage = ProcessDocuments(responses, loanId, brokerId, appId, transactionId, out ucdDocumentId, packageTypeName, isPreviewOrder, isAsync: false);
            List<string> tempFileNames = tempFileNamesAndErrorMessage.Item1;
            string errorMessage = tempFileNamesAndErrorMessage.Item2;
            IDocumentGenerationResult ret = null;
            if (tempFileNames.Count == 0)
            {
                return DocumentVendorResult.Error(errorMessage, ret);
            }

            List<string> keys = new List<string>();
            foreach (string filename in tempFileNames)
            {
                string key = Guid.NewGuid().ToString();
                FileDBTools.WriteFile(E_FileDB.Temp, key, filename);
                keys.Add(key);
            }

            //The viewer expects a serialized array of keys
            string fileDBKeys = ObsoleteSerializationHelper.JavascriptJsonSerialize(keys);
            string filedbKeyLocation = AutoExpiredTextCache.AddToCache(fileDBKeys, TimeSpan.FromHours(1));

            ret = new ConfiguredDocumentGenerationResult(filedbKeyLocation, packageType, packageTypeName, loanId, config, errorMessage, dataValidation, transactionId);
            var returnVal = DocumentVendorResult.Success(ret);
            returnVal.TransactionId = transactionId;
            returnVal.UcdDocumentId = ucdDocumentId ?? Guid.Empty;
            return returnVal;
        }

        /// <summary>
        /// Retrieves the documents from an asynchronous vendor response and processes them into the system.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="appId">The application ID.</param>
        /// <param name="transactionId">The transaction ID.</param>
        /// <param name="responses">A list of documents.</param>
        /// <returns>A document vendor result.</returns>
        /// <remarks>
        /// Asynchronous document vendor responses do not contain a way to determine which vendor is transmitting,
        /// so the asynchronous version works without knowledge of a package type or vendor config.  Thus, they wrap a simple object.
        /// </remarks>
        public static DocumentVendorResult<object> CreateAsync(Guid loanId,
            Guid brokerId, Guid appId, string transactionId, object[] responses)
        {
            Guid? ucdDocumentId;
            Tuple<List<string>, string> tempFileNamesAndErrorMessage = ProcessDocuments(responses, loanId, brokerId, appId, transactionId, out ucdDocumentId, packageTypeName: null, isPreviewOrder: false, isAsync: true);
            List<string> tempFileNames = tempFileNamesAndErrorMessage.Item1;
            string errorMessage = tempFileNamesAndErrorMessage.Item2;

            object ret = null;
            if (tempFileNames.Count == 0)
            {
                return DocumentVendorResult.Error(errorMessage, ret);
            }

            return DocumentVendorResult.Success(ret);
        }

        /// <summary>
        /// The document response processing logic. This method handles both synchronous and asynchronous responses.
        /// </summary>
        /// <param name="responses">A list of documents.</param>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="appId">The application ID.</param>
        /// <param name="transactionId">The transaction ID.</param>
        /// <param name="ucdDocumentId">The ID of an included UCD document, if applicable.</param>
        /// <param name="packageTypeName">The name of the document package type, used for sometimes for generating the doc type. (Async responses currently do not use this.)</param>
        /// <param name="isPreviewOrder">Indicates whether the document order is for a preview package.</param>
        /// <param name="isAsync">Indicates whether the response was received asynchronously.</param>
        /// <returns>A set of temporary file names containing the documents and an error message.  If the error was fatal, the list of temporary files should be empty.</returns>
        private static Tuple<List<string>, string> ProcessDocuments(object[] responses, Guid loanId, Guid brokerId, Guid appId, string transactionId,
            out Guid? ucdDocumentId, DocumentIntegrationPackageName? packageTypeName, bool isPreviewOrder, bool isAsync = false)
        {
            ucdDocumentId = null;
            var broker = BrokerDB.RetrieveById(brokerId);

            bool isTotalFailure = true;
            List<string> errors = new List<string>();
            List<string> files = new List<string>();

            foreach (DocumentResponse response in responses)
            {
                if (!IsValidDocumentFormat(response.DocumentFormat, isAsync))
                {
                    errors.Add("Don't know how to handle response format [" + response.DocumentFormat + "]");
                    continue;
                }

                string tempFileError;
                string pdfFilename;
                if (!WriteDocumentToTempFile(response, broker.BrokerID, out pdfFilename, out tempFileError))
                {
                    if (response.DocumentFormat == DocumentResponseDocumentFormat.UCD)
                    {
                        errors.Add(tempFileError);
                        continue;
                    }
                    else
                    {
                        return Tuple.Create(new List<string>(), tempFileError);
                    }
                }

                if (response.DocumentFormat != DocumentResponseDocumentFormat.UCD)
                {
                    // This list will be passed over to the document viewer. Don't include UCD files for
                    // viewing as they're redundant to the full PDF response containing all documents.
                    files.Add(pdfFilename);
                }

                bool autosaveDocuments = isAsync || (broker.AutoSaveDocMagicGeneratedDocs && !isPreviewOrder);
                if (autosaveDocuments)
                {
                    string autoSaveError;
                    if (!AutoSaveDocument(broker, response, loanId, appId, packageTypeName, pdfFilename, transactionId, isAsync, out autoSaveError, out ucdDocumentId))
                    {
                        if (isAsync)
                        {
                            // If a doc fails to save on an async request, we want to return an error to the sender
                            // instead of continuing to ensure the document does not get lost.
                            return Tuple.Create(new List<string>(), autoSaveError);
                        }
                        else
                        {
                            errors.Add(autoSaveError);
                            continue;
                        }
                    }
                }

                // At least one doc made it to the end, so not a total failure.
                isTotalFailure = false;
            }

            string errorMessage = string.Join(Environment.NewLine, errors);

            if (isTotalFailure)
            {
                return Tuple.Create(new List<string>(), "Response contained errors: " + errorMessage);
            }

            return Tuple.Create(files, errorMessage);
        }

        /// <summary>
        /// Attempts to write a single document to a temp file.
        /// </summary>
        /// <param name="response">The document to be written.</param>
        /// <param name="brokerId">The ID of the associated broker.</param>
        /// <param name="pdfFilename">The temp file name.</param>
        /// <param name="errorMessage">Any errors occurring during the write process.</param>
        /// <returns>A boolean indicating whether the document was successfully written to a file.</returns>
        private static bool WriteDocumentToTempFile(DocumentResponse response, Guid brokerId, out string pdfFilename, out string errorMessage)
        {
            errorMessage = string.Empty;
            switch (response.DocumentFormat)
            {
                case DocumentResponseDocumentFormat.PDF:
                    pdfFilename = Utilities.Base64StringToFile(response.EncodedDocs, "document_vendor.pdf");
                    return true;
                case DocumentResponseDocumentFormat.UCD:
                case DocumentResponseDocumentFormat.XML:
                    if (!UcdGenericEDocument.IsUcdStringValid(response.EncodedDocs, brokerId))
                    {
                        pdfFilename = string.Empty;
                        errorMessage = ErrorMessages.EDocs.InvalidUcd;
                        return false;
                    }

                    pdfFilename = EDocsUpload.ParseUcdDocFromXml(response.EncodedDocs);
                    return true;
                default:
                    throw new UnhandledEnumException(response.DocumentFormat);
            }
        }

        /// <summary>
        /// Attempts to save a document to EDocs.
        /// </summary>
        /// <param name="broker">The broker.</param>
        /// <param name="response">The document.</param>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="appId">The application ID.</param>
        /// <param name="packageTypeName">The name of the document package type.</param>
        /// <param name="pdfFilename">The temp filename holding the document contents.</param>
        /// <param name="isAsync">Indicates whether the document response was received asynchronously.</param>
        /// <param name="errorMessage">Any errors occurring during the save process.</param>
        /// <param name="ucdDocumentId">The ID of an included UCD document, if applicable.</param>
        /// <returns>A boolean indicating whether the document was saved successfully.</returns>
        private static bool AutoSaveDocument(BrokerDB broker, DocumentResponse response, Guid loanId, Guid appId, DocumentIntegrationPackageName? packageTypeName, string pdfFilename, string transactionId, bool isAsync, out string errorMessage, out Guid? ucdDocumentId)
        {
            ucdDocumentId = Guid.Empty;

            try
            {
                var brokerId = broker.BrokerID;
                var format = response.DocumentFormat;
                errorMessage = string.Empty;

                switch (format)
                {
                    case DocumentResponseDocumentFormat.PDF:
                        var useCaptureMethod = broker.EnableKtaIntegration
                            && broker.DocMagicDocumentSavingOption == E_DocMagicDocumentSavingOption.DocumentCapture;
                        var useCaptureFallbackMethod = false;

                        if (useCaptureMethod)
                        {
                            if (OcrEnabledBranchGroupManager.IsLoanBranchGroupEnabledForOcr(broker, loanId))
                            {
                                var parameters = KtaDocumentUploadParameters.FromUserPrincipal(PrincipalFactory.CurrentPrincipal);
                                parameters.LoanId = loanId;
                                parameters.ApplicationId = appId;
                                parameters.DocumentSource = E_EDocumentSource.GeneratedDocs;
                                parameters.PdfPath = pdfFilename;

                                var result = KtaUtilities.UploadDocument(parameters);
                                if (result.Success)
                                {
                                    return true;
                                }

                                var messageData = $"for auto save doc upload loan {loanId}, app {appId}, transaction {transactionId}, async {isAsync} ";
                                if (result.MissingCredentials)
                                {
                                    // Allow falling back to existing upload behavior only when service credentials are missing.
                                    Tools.LogError("Missing service credentials " + messageData);
                                    useCaptureFallbackMethod = true;
                                }
                                else
                                {
                                    Tools.LogError("Error encountered " + messageData + result.ErrorMessage);
                                    errorMessage = ErrorMessages.Generic;
                                    return false;
                                }
                            }
                            else
                            {
                                useCaptureFallbackMethod = true;
                            }
                        }

                        bool canUseSplitDocsForThisSigningType = broker.DocMagicSplitUnsignedDocs;
                        bool useSplitDocsAsPrimaryMethod = broker.DocMagicDocumentSavingOption == E_DocMagicDocumentSavingOption.Split;
                        bool useSplitDocsAsFallbackMethod = useCaptureFallbackMethod && broker.BackupDocumentSavingOption == E_DocMagicDocumentSavingOption.Split;

                        var useSplitDocsMethod = canUseSplitDocsForThisSigningType && (useSplitDocsAsPrimaryMethod || useSplitDocsAsFallbackMethod);

                        if (useSplitDocsMethod)
                        {
                            DocMagicPDFManager.UploadDocument(PrincipalFactory.CurrentPrincipal, loanId, appId, "", "Autosaved: " + DateTime.Now.ToShortTimeString(), pdfFilename);
                        }
                        else
                        {
                            // At this point we haven't saved the documents using Capture or Split Docs. Save as Single Document
                            // regardless of the save method settings to ensure the documents are not lost.
                            int docType = broker.DocMagicDefaultDocTypeID ?? GetFallbackDocType(brokerId, packageTypeName, isAsync);
                            CreateEdoc(pdfFilename, docType, brokerId, loanId, appId, isAsync);
                        }
                        break;
                    case DocumentResponseDocumentFormat.UCD:
                    case DocumentResponseDocumentFormat.XML:
                        DefaultDocTypeInfo docTypeInfo = AutoSaveDocTypeFactory.GetDocTypeForPageId(E_AutoSavePage.UniformClosingDataset, brokerId, E_EnforceFolderPermissions.False);
                        bool foundDocType = docTypeInfo != null && docTypeInfo.DocType != null;
                        int docTypeId = foundDocType ? docTypeInfo.DocType.Id : GetFallbackDocType(brokerId, packageTypeName, isAsync);

                        // Save PDF as EDoc.
                        Guid docId = CreateEdoc(pdfFilename, docTypeId, brokerId, loanId, appId, isAsync);
                        ucdDocumentId = docId;

                        // Save UCD XML as linked generic doc.
                        string xmlFilename = TempFileUtils.NewTempFilePath();
                        TextFileHelper.WriteString(xmlFilename, response.EncodedDocs, false);
                        CreateGenericDoc(xmlFilename, E_FileType.UniformClosingDataset, docTypeId, docId, brokerId, loanId, appId, isAsync);

                        if (isAsync)
                        {
                            // We only try to link the UCD file here if the response is asynchronous,
                            // otherwise the CD row will not yet be created and the link will fail.
                            string associationMessage;
                            if (!AssociateUcdToClosingDisclosure(loanId, docId, transactionId, out associationMessage))
                            {
                                Tools.LogError(associationMessage);
                                errorMessage = associationMessage;
                                return false;
                            }
                        }

                        break;
                    default:
                        throw new UnhandledEnumException(response.DocumentFormat);
                }

                return true;
            }
            catch (InvalidPDFFileException e)
            {
                Tools.LogError("Invalid edoc received, check the most recent DocumentVendorServer logs.", e);
                errorMessage = "Invalid document in response";
                return false;
            }
            catch (XmlException ex)
            {
                Tools.LogError("Invalid edoc received, check the most recent DocumentVendorServer logs.", ex);
                errorMessage = ErrorMessages.EDocs.InvalidUcd;
                return false;
            }
            catch (CBaseException cbe) when (cbe.UserMessage == ErrorMessages.EDocs.InvalidUcd)
            {
                Tools.LogError("Invalid edoc received, check the most recent DocumentVendorServer logs.", cbe);
                errorMessage = ErrorMessages.EDocs.InvalidUcd;
                return false;
            }
        }

        /// <summary>
        /// Associates the newly created UCD EDoc with the matching loan and CD metadata row.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="docId">The document ID.</param>
        /// <param name="transactionId">The transaction ID.</param>
        /// <returns>A boolean indicating whether the association was successful.</returns>
        private static bool AssociateUcdToClosingDisclosure(Guid loanId, Guid docId, string transactionId, out string errorMessage)
        {
            errorMessage = string.Empty;

            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(ConfiguredDocumentGenerationResult));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            Guid transaction;
            if (!Guid.TryParse(transactionId, out transaction))
            {
                errorMessage = $"Transaction ID {transactionId} could not be parsed into a GUID.";
            }

            bool cdFound = false;
            foreach (var closingDisclosure in dataLoan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList)
            {
                Guid cdTransaction;
                Guid.TryParse(closingDisclosure.TransactionId, out cdTransaction);

                if (transaction != Guid.Empty && transaction == cdTransaction)
                {
                    closingDisclosure.UcdDocument = docId;
                    dataLoan.Save();
                    cdFound = true;
                    break;
                }
            }

            if (!cdFound)
            {
                errorMessage = $"The transaction ID {transactionId} could not be found in loan {loanId}.";
            }

            return cdFound;
        }

        /// <summary>
        /// Gets the fallback document type for saving.
        /// </summary>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="packageTypeName">The name of the document package type.</param>
        /// <param name="isAsync">Indicates whether the response was received asynchronously.</param>
        /// <returns>An integer document type.</returns>
        private static int GetFallbackDocType(Guid brokerId, DocumentIntegrationPackageName? packageTypeName, bool isAsync = false)
        {
            if (isAsync)
            {
                return EDocumentDocType.GetOrCreatePartnerUpload(brokerId);
            }

            return EDocumentDocType.GetOrCreatePackageDocType(brokerId, packageTypeName.ForceValue().Name);
        }

        /// <summary>
        /// Determines whether the document is a valid format type. Currently the asynchronous response
        /// handler does not support PDFs.
        /// </summary>
        /// <param name="format">The document format.</param>
        /// <param name="isAsync">Indicates whether the response was received asynchronously.</param>
        /// <returns>A boolean indicating whether the document format is valid.</returns>
        private static bool IsValidDocumentFormat(DocumentResponseDocumentFormat format, bool isAsync)
        {
            if (isAsync)
            {
                return format != DocumentResponseDocumentFormat.PDF;
            }

            return SupportedFormats.Contains(format);
        }

        private static Guid CreateEdoc(string path, int docType, Guid brokerId, Guid loanId, Guid appId, bool isAsync)
        {
            var edoc = EDocument.CreateEdoc(
                path, 
                docType, 
                brokerId, 
                loanId, 
                appId, 
                isAsync ? null : (Guid?)PrincipalFactory.CurrentPrincipal.UserId, 
                isAsync ? false : PrincipalFactory.CurrentPrincipal.Type == "P", 
                E_EDocumentSource.GeneratedDocs,
                "Autosaved: " + DateTime.Now.ToShortTimeString());

            return edoc.DocumentId;
        }

        private static void CreateGenericDoc(string path, E_FileType fileType, int docTypeId, Guid linkedDocId, Guid brokerId, Guid loanId, Guid appId, bool isAsync)
        {
            GenericEDocument.CreateGenericDoc(
                path, 
                fileType, 
                docTypeId, 
                linkedDocId, 
                brokerId, 
                loanId, 
                appId, 
                isAsync ? Guid.Empty : PrincipalFactory.CurrentPrincipal.UserId,
                "Autosaved: " + DateTime.Now.ToShortTimeString());
        }

        private readonly Guid LoanId;

        public readonly string PdfFileKey;

        public string PdfFileDbKey
        {
            get
            {
                string json = AutoExpiredTextCache.GetFromCache(PdfFileKey);

                if (string.IsNullOrEmpty(json))
                {
                    return string.Empty;
                }

                List<string> pdfFileDBKeys = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(json);
                return pdfFileDBKeys[0];
            }
        }
        public string PackageType { get; }

        public DocumentIntegrationPackageName PackageName { get; }

        private readonly VendorConfig Config;

        public string ErrorMessage { get; private set; }

        public decimal? DocVendorApr { get; }

        public string DocCode { get; }

        public decimal? DocVendorLateChargePercent { get; }

        public VendorProvidedDisclosureMetadata VendorProvidedDisclosureMetadata { get; set; }

        private ConfiguredDocumentGenerationResult(string pdfFileKey, string packageType, DocumentIntegrationPackageName packageName, Guid loanId, VendorConfig config, string errorMessage, DataValidation dataValidation, string transactionId)
        {
            this.Config = config;
            this.LoanId = loanId;
            this.PackageType = packageType;
            this.PackageName = packageName;
            this.PdfFileKey = pdfFileKey;
            this.ErrorMessage = errorMessage;
            this.DocVendorApr = dataValidation?.APR.ToNullable<decimal>(decimal.TryParse);
            this.DocCode = dataValidation?.DocCode;
            this.DocVendorLateChargePercent = dataValidation?.LateChargePercent.ToNullable<decimal>(decimal.TryParse);
            this.VendorProvidedDisclosureMetadata = VendorProvidedDisclosureMetadata.ParseLqbDocumentResponseMetadata(dataValidation, transactionId, loanId);
        }

        #region IDocumentGenerationResult Members

        public bool DocsAreForDownload
        {
            get { return false; }
        }

        public bool DocsAreForViewing
        {
            get { return true; }
        }

        public string DownloadUrl
        {
            get { throw new NotImplementedException(); }
        }

        public bool IsDisclosurePackage
        {
            get { return Config.IsDisclosurePackage(PackageType); }
        }

        public bool IsInitialDisclosurePackage
        {
            get { return Config.IsInitialDisclosurePackage(PackageType); }
        }

        public bool IsClosingPackage
        {
            get { return Config.IsClosingPackage(PackageType); }
        }
        public string ViewUrl
        {
            get
            {
                return $"DocumentViewer.aspx?loanid={LoanId}&d={PdfFileKey}&VendorId={Config.VendorId}";
            }
        }

        public bool CreatedArchiveInUnknownStatus { get; set; }
        public ClosingCostArchive.E_ClosingCostArchiveType? UnknownArchiveType { get; set; }
        public string UnknownArchiveDate { get; set; }
        public bool UnknownArchiveWasSourcedFromPendingCoC { get; set; }
        public bool HasBothCDAndLEArchiveInUnknownStatus { get; set; }

        #endregion

        [Obsolete("Data Validation is no longer in use, but we aren't deleting the code yet in case a similar usage comes up in the future.")]
        private static class DataValidationFunctions
        {
            [Obsolete("Data Validation is no longer in use, but we aren't deleting the code yet in case a similar usage comes up in the future.")]
            public static DocumentVendorResult<IDocumentGenerationResult> CheckDataValidation(string packageType, Guid loanId, Guid appId, VendorConfig config, string transactionId, DocumentResponse response, DataValidation dataValidation, IDocumentGenerationResult ret, BrokerDB broker)
            {
                // 3/26/2014 dd - OPM 174204 - Add Data Validation to the notification message.
                if (dataValidation != null)
                {
                    switch (broker.DocumentValidationOptionT)
                    {
                        case E_BrokerDocumentValidationOptionT.WarningOnValidationMismatch:
                        case E_BrokerDocumentValidationOptionT.BlockOnValidationMismatch:
                            string message = string.Empty;
                            bool isValidate = ValidateWithDataLayer(dataValidation, config.IsClosingPackage(packageType), loanId, appId, transactionId, out message);

                            if (isValidate == false)
                            {
                                if (broker.DocumentValidationOptionT == E_BrokerDocumentValidationOptionT.BlockOnValidationMismatch)
                                {
                                    return DocumentVendorResult.ValidationError("ERROR: The loan documents did not pass data validation checks.  Please contact your LendingQB system administrator to help resolve this issue."
                                        + Environment.NewLine + Environment.NewLine + message, ret);
                                }
                                else
                                {
                                    // Save DocumentResponse in Temp FileDB

                                    string tempFile = TempFileUtils.NewTempFilePath();

                                    SerializationHelper.XmlSerializeToFile(response, tempFile);


                                    string fileDBKey = loanId + "_" + appId + "_" + config.VendorId + "_TEMP_DOCRESPONSE";

                                    FileDBTools.WriteFile(E_FileDB.Temp, fileDBKey, tempFile);
                                    DocumentVendorResult<IDocumentGenerationResult> result = DocumentVendorResult.ValidationError("Warning: A data mismatch was detected between LQB data and the generated documents.  The generated documents may not be accurate."
    + Environment.NewLine + Environment.NewLine + message, ret);
                                    result.TempFileDbKey = fileDBKey;
                                    result.TransactionId = transactionId;

                                    return result;
                                }
                            }
                            break;
                        case E_BrokerDocumentValidationOptionT.NoValidation:
                            // NO-OP. Do not perform validation
                            if (broker.CustomerCode == "PML0223")
                            {
                                // 4/16/2014 dd - For debugging purpose always turn on silence validation for FirstTech
                                // 4/16/2014 dd - THis block is temporary.
                                string tmpmessage = string.Empty;
                                ValidateWithDataLayer(dataValidation, config.IsClosingPackage(packageType), loanId, appId, transactionId, out tmpmessage);
                            }
                            break;

                        default:
                            throw new UnhandledEnumException(broker.DocumentValidationOptionT);
                    }
                }

                return null;
            }

            private static bool ValidateWithDataLayer(DataValidation dataValidation, bool isClosingPackage, Guid loanId, Guid appId, string requestId, out string message)
            {
                // 3/27/2014 dd - OPM 174204 - Perform data validation return from Doc Vendor vs data in our datalayer.
                message = string.Empty;

                StringBuilder messageLog = new StringBuilder();

                StringBuilder debuggingMessage = new StringBuilder();

                // 3/27/2014 dd - Use security by-pass so we can compare fields without worry restriction.

                CPageData dataLoan = new CFullAccessPageData(loanId,
                        CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(ConfiguredDocumentGenerationResult)).Union(
                        CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release)
                    );
                dataLoan.InitLoad();

                bool currentValidationResult = true;

                // 4/12/2014 dd - The order of validation is important.
                // There are some section we want to compare before ApplyGFEArchive is call.
                // As of today these sections must use live data:
                //     MI Data


                if (dataValidation.MI_Data != null)
                {


                    currentValidationResult = CompareData("Initial Monthly MI Rate",
                        isClosingPackage,
                        dataValidation.MI_Data.InitialMonthlyMIRate,
                        dataLoan.sProMInsR_rep,
                        dataLoan.sProMInsR_rep,
                        messageLog, debuggingMessage, currentValidationResult);

                    string sProMInsMon_rep = dataLoan.sProMInsMon_rep;
                    // 4/18/2014 dd - Per Joseph J - To match with our export (GenericMismoClosing26Exporter)
                    // if this this value is zero and rate is non zero then default to sDue.
                    if (sProMInsMon_rep == "0")
                    {
                        if (dataLoan.sProMInsR != 0)
                        {
                            sProMInsMon_rep = dataLoan.sDue_rep;
                        }
                    }
                    currentValidationResult = CompareData("Initial Monthly MI Term",
                        isClosingPackage,
                        dataValidation.MI_Data.InitialMonthlyMITerm,
                        sProMInsMon_rep,
                        sProMInsMon_rep,
                        messageLog, debuggingMessage, currentValidationResult);

                    currentValidationResult = CompareData("MI Renewal Rate",
                        isClosingPackage,
                        dataValidation.MI_Data.MIRenewalRate,
                        dataLoan.sProMInsR2_rep,
                        dataLoan.sProMInsR2_rep,
                        messageLog, debuggingMessage, currentValidationResult);

                    currentValidationResult = CompareData("MI Renewal Term",
                        isClosingPackage,
                        dataValidation.MI_Data.MIRenewalTerm,
                        dataLoan.sProMIns2Mon_rep,
                        dataLoan.sProMIns2Mon_rep,
                        messageLog, debuggingMessage, currentValidationResult);

                    currentValidationResult = CompareData("Cancel LTV Percent",
                        isClosingPackage,
                        dataValidation.MI_Data.CancelAtLTVPercentage,
                        dataLoan.sProMInsCancelLtv_rep,
                        dataLoan.sProMInsCancelLtv_rep,
                        messageLog, debuggingMessage, currentValidationResult);

                }

                //currentValidationResult = CompareData("Min Escrow Balance Amount", dataValidation.MinEscrowBalanceAmount,
                //    dataLoan.sProMInsCancelLtv_rep, messageLog, currentValidationResult);



                //debuggingMessage.AppendLine("Before Apply GFE Archive. sSettlementInitialImpoundDeposit_rep=" + dataLoan.sSettlementInitialImpoundDeposit_rep);
                if (isClosingPackage == false &&
                    dataLoan.BrokerDB.IsGFEandCoCVersioningEnabled &&
                    dataLoan.LastDisclosedGFEArchive != null)
                {
                    // 4/3/2014 dd - For disclosure package use the GFE Archive version.
                    IGFEArchive gfeArchive = dataLoan.LastDisclosedGFEArchive;

                    debuggingMessage.AppendLine("Applying GFE Archive Date: " + gfeArchive.DateArchived);
                    dataLoan.ApplyGFEArchive(gfeArchive);
                }



                string sApr = string.Empty;
                try
                {
                    if (isClosingPackage == false && dataLoan.BrokerDB.IsGFEandCoCVersioningEnabled
                        && dataLoan.sLastDisclosedGFEArchiveD != null)
                    {
                        // 4/12/2014 dd - Get last disclosure APR instead of live one.
                        sApr = GetLastDisclosedAPR(dataLoan.sLId, dataLoan.LastDisclosedGFEArchive);

                        debuggingMessage.AppendLine("LastDisclosedAPR=" + sApr);
                        debuggingMessage.AppendLine("sApr=" + dataLoan.sApr.ToString());

                    }
                    else
                    {
                        sApr = dataLoan.sApr.ToString(); // Get more precision
                    }
                }
                catch (CBaseException)
                {
                }
                currentValidationResult = CompareData("APR", isClosingPackage, dataValidation.APR, sApr, sApr, messageLog, debuggingMessage, currentValidationResult);


                if (dataValidation.MI_Data != null)
                {
                    currentValidationResult = CompareData("Upfront MIP Amount",
        isClosingPackage,
        dataValidation.MI_Data.UpfrontMIPAmount,
        dataLoan.sFfUfmip1003_rep,
        dataLoan.sFfUfmip1003_rep,
        messageLog, debuggingMessage, currentValidationResult);
                }
                if (isClosingPackage)
                {
                    // 4/18/2014 dd - We only validate Cure information on closing package.
                    if (dataValidation.Respa_Fee_Tolerances != null)
                    {
                        currentValidationResult = CompareData("Tolerance Cure A1",
                            isClosingPackage,
                            dataValidation.Respa_Fee_Tolerances.ToleranceCureLine801Amount,
                            dataLoan.sToleranceCureA1Fees_rep,
                            dataLoan.sToleranceCureA1Fees_rep,
                            messageLog, debuggingMessage, currentValidationResult);

                        currentValidationResult = CompareData("Tolerance Cure A2",
                            isClosingPackage,
                            dataValidation.Respa_Fee_Tolerances.ToleranceCureLine802Amount,
                            dataLoan.sToleranceCureA2Fees_rep,
                            dataLoan.sToleranceCureA2Fees_rep,
                            messageLog, debuggingMessage, currentValidationResult);

                        currentValidationResult = CompareData("Tolerance Cure B8",
                            isClosingPackage,
                            dataValidation.Respa_Fee_Tolerances.ToleranceCureLine1203Amount,
                            dataLoan.sToleranceCureB8Fees_rep,
                            dataLoan.sToleranceCureB8Fees_rep,
                            messageLog, debuggingMessage, currentValidationResult);

                        currentValidationResult = CompareData("10% Tolerance Cure",
                            isClosingPackage,
                            dataValidation.Respa_Fee_Tolerances.TenPercentToleranceCure,
                            dataLoan.sToleranceTenPercentCure_rep,
                            dataLoan.sToleranceTenPercentCure_rep,
                            messageLog, debuggingMessage, currentValidationResult);

                    }
                }

                if (dataValidation.InterimInterest != null)
                {
                    currentValidationResult = CompareData("Number of Prepaid Interest Days",
                        isClosingPackage,
                        dataValidation.InterimInterest.NumDays,
                        dataLoan.sIPiaDy_rep,
                        dataLoan.sSettlementIPiaDy_rep,
                        messageLog, debuggingMessage, currentValidationResult);

                    currentValidationResult = CompareData("Prepaid Interest",
                        isClosingPackage,
                        dataValidation.InterimInterest.TotalAmount,
                        dataLoan.sIPia_rep,
                        dataLoan.sSettlementIPia_rep,
                        messageLog, debuggingMessage, currentValidationResult);
                }

                currentValidationResult = CompareData("Aggregate Escrow Amount",
                    isClosingPackage,
                    dataValidation.AggregateEscrowAmount,
                    dataLoan.sAggregateAdjRsrv_rep,
                    dataLoan.sSettlementAggregateAdjRsrv_rep,
                    messageLog, debuggingMessage, currentValidationResult);


                if (dataValidation.GFE_Respa_Fee_Summary != null)
                {

                    currentValidationResult = CompareData("A1 Fees Amount",
                        isClosingPackage,
                        dataValidation.GFE_Respa_Fee_Summary.A1FeesAmount,
                        dataLoan.sGfeOriginationF_rep,
                        dataLoan.sSettlementOriginationF_rep,

                        messageLog, debuggingMessage, currentValidationResult);

                    currentValidationResult = CompareData("A2 Fees Amount",
                        isClosingPackage,
                        dataValidation.GFE_Respa_Fee_Summary.A2FeesAmount,
                        dataLoan.sLDiscnt_rep,
                        dataLoan.sSettlementLDiscnt_rep,
                        messageLog, debuggingMessage, currentValidationResult);

                    currentValidationResult = CompareData("B3 Fees Amount",
                        isClosingPackage,
                        dataValidation.GFE_Respa_Fee_Summary.B3FeesAmount,
                        dataLoan.sGfeRequiredServicesTotalFee_rep,
                        dataLoan.sSettlementRequiredServicesTotalFee_rep,
                        messageLog, debuggingMessage, currentValidationResult);

                    currentValidationResult = CompareData("B4 Fees Amount",
                        isClosingPackage,
                        dataValidation.GFE_Respa_Fee_Summary.B4FeesAmount,
                        dataLoan.sGfeLenderTitleTotalFee_rep,
                        dataLoan.sSettlementLenderTitleTotalFee_rep,
                        messageLog, debuggingMessage, currentValidationResult);

                    currentValidationResult = CompareData("B5 Fees Amount",
                        isClosingPackage,
                        dataValidation.GFE_Respa_Fee_Summary.B5FeesAmount,
                        dataLoan.sGfeOwnerTitleTotalFee_rep,
                        dataLoan.sSettlementOwnerTitleTotalFee_rep,
                        messageLog, debuggingMessage, currentValidationResult);

                    currentValidationResult = CompareData("B6 Fees Amount",
                        isClosingPackage,
                        dataValidation.GFE_Respa_Fee_Summary.B6FeesAmount,
                        dataLoan.sGfeServicesYouShopTotalFee_rep,
                        dataLoan.sSettlementServicesYouShopTotalFee_rep,
                        messageLog, debuggingMessage, currentValidationResult);

                    currentValidationResult = CompareData("B7 Fees Amount",
                        isClosingPackage,
                        dataValidation.GFE_Respa_Fee_Summary.B7FeesAmount,
                        dataLoan.sGfeGovtRecTotalFee_rep,
                        dataLoan.sSettlementGovtRecTotalFee_rep,
                        messageLog, debuggingMessage, currentValidationResult);

                    currentValidationResult = CompareData("B8 Fees Amount",
                        isClosingPackage,
                        dataValidation.GFE_Respa_Fee_Summary.B8FeesAmount,
                        dataLoan.sGfeTransferTaxTotalFee_rep,
                        dataLoan.sSettlementTransferTaxTotalFee_rep,
                        messageLog, debuggingMessage, currentValidationResult);

                    currentValidationResult = CompareData("B9 Fees Amount",
                        isClosingPackage,
                        dataValidation.GFE_Respa_Fee_Summary.B9FeesAmount,
                        dataLoan.sGfeInitialImpoundDeposit_rep,
                        dataLoan.sSettlementInitialImpoundDeposit_rep,
                        messageLog, debuggingMessage, currentValidationResult);

                    currentValidationResult = CompareData("B10 Fees Amount",
                        isClosingPackage,
                        dataValidation.GFE_Respa_Fee_Summary.B10FeesAmount,
                        dataLoan.sGfeDailyInterestTotalFee_rep,
                        dataLoan.sSettlementDailyInterestTotalFee_rep,
                        messageLog, debuggingMessage, currentValidationResult);

                    currentValidationResult = CompareData("B11 Fees Amount",
                        isClosingPackage,
                        dataValidation.GFE_Respa_Fee_Summary.B11FeesAmount,
                        dataLoan.sGfeHomeOwnerInsuranceTotalFee_rep,
                        dataLoan.sSettlementHomeOwnerInsuranceTotalFee_rep,
                        messageLog, debuggingMessage, currentValidationResult);


                }


                message = messageLog.ToString();

                if (currentValidationResult == false)
                {
                    // 4/15/2014 dd - We are sending debug information to Joseph J on every fail validation.

                    CBaseEmail email = new CBaseEmail(dataLoan.sBrokerId);
                    email.To = "josephj@meridianlink.com";
                    email.From = ConstStage.SenderEmailAddressOfSupport;
                    email.Subject = "[DocumentValidationFailure] - " + dataLoan.BrokerDB.CustomerCode + " sLNm=[" + dataLoan.sLNm + "], Is Closing Package=" + isClosingPackage;



                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Broker: " + dataLoan.BrokerDB.CustomerCode);
                    sb.AppendLine("sLNm=" + dataLoan.sLNm);
                    sb.AppendLine();
                    sb.AppendLine("Validation Warning Message:");
                    sb.AppendLine();
                    sb.AppendLine(message);
                    sb.AppendLine();
                    email.AddAttachment("ValidationResponse.xml", SerializationHelper.XmlSerialize(dataValidation));

                    string requestFilePath = ConstApp.TempFolder + @"\" + requestId + "_request.xml";
                    if (FileOperationHelper.Exists(requestFilePath))
                    {
                        email.AddAttachment("Request.xml", TextFileHelper.ReadFile(requestFilePath));
                    }
                    else
                    {
                        sb.AppendLine("Could not locate Request XML in LOTemp. TransactionID=[" + requestId + "]");
                    }

                    sb.AppendLine();
                    sb.Append(debuggingMessage.ToString());
                    email.Message = sb.ToString();

                    email.Send();
                }
                return currentValidationResult;
            }

            private static bool CompareData(string fieldDesc, bool isClosingPackage,
                string docVendorValue,
                string dataLayerGfeValue,
                string dataLayerSettlementValue,
                StringBuilder messageLog,
                StringBuilder debugLog,
                bool currentValidationResult)
            {
                // 3/27/2014 dd - NOTE: If the currentValidationResult is false then this method should never return true.

                if (string.IsNullOrEmpty(docVendorValue) == true)
                {
                    // 3/27/2014 dd - If doc vendor does not provide value then there is nothing to compare.
                    return currentValidationResult;
                }

                string dataLayerValue = isClosingPackage ? dataLayerSettlementValue : dataLayerGfeValue;

                if (docVendorValue.Equals(dataLayerValue))
                {
                    return currentValidationResult; // Value match.
                }

                // 3/31/2014 dd - Try to compare decimal value.
                decimal v1 = 0;
                decimal v2 = 0;

                //if (decimal.TryParse(docVendorValue.Replace("$", "").Replace("%", "").Replace(",", ""), out v1) == true)
                if (decimal.TryParse(docVendorValue.Replace("%", ""), System.Globalization.NumberStyles.Currency, null, out v1) == true)
                {
                    //if (decimal.TryParse(dataLayerValue.Replace("$", "").Replace("%", "").Replace(",", ""), out v2) == true)
                    if (decimal.TryParse(dataLayerValue.Replace("%", ""), System.Globalization.NumberStyles.Currency, null, out v2) == true)
                    {
                        if (v1 == v2)
                        {
                            return currentValidationResult;
                        }
                        else if (Math.Abs(v1 - v2) <= 0.0001M)
                        {
                            // 4/2/2014 dd - check to see if the different within tolerance level.
                            // The reason for this is DocuTech return a lot more digit in APR and we cannot match it exactly.
                            // 4/17/2014 dd - Only match up to 4 decimal digits
                            // 12/8/2014 dd - Per Joseph J request we only compare to 3rd decimal place to avoid false error like APR: 4.11790629 vs APR 4.1178941
                            return currentValidationResult;
                        }
                    }
                }
                messageLog.AppendLine(fieldDesc + " DOES NOT MATCH. Doc=[" + docVendorValue + "], LendingQB=[" + dataLayerValue + "]");
                debugLog.AppendLine(fieldDesc + " DOES NOT MATCH. Doc=[" + docVendorValue + "], GFE Value=[" + dataLayerGfeValue + "], Settlement GFE Value=[" + dataLayerSettlementValue + "]");
                return false;
            }

            private static string GetLastDisclosedAPR(Guid sLId, IGFEArchive lastDisclosedGFEArchive)
            {
                // 4/12/2014 dd - WARNING WARNING: Copy the bellow logic from SeamlessDocumentGenerationOptions.GetLastDisclosedAPR
                // 4/12/2014 dd - THIS LOGIC SHOULD BE IN DATALAYER. HOWEVER I AM DOING HOTFIX THEREFORE WILL NOT 
                // CONSOLIDATE CODE.
                // 
                var dataLoanWithGFEArchiveApplied = new CPageData(sLId, "DocGenerationResult", CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(ConfiguredDocumentGenerationResult)).Union(
        CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release));
                dataLoanWithGFEArchiveApplied.InitLoad();
                dataLoanWithGFEArchiveApplied.ApplyGFEArchiveExcludingFields(lastDisclosedGFEArchive, ConstApp.FieldsToExcludeWhenApplyingGFEArchiveForAprCalc);
                try
                {
                    return dataLoanWithGFEArchiveApplied.sApr.ToString(); // Get more precision
                }
                catch (CBaseException)
                {
                    return string.Empty;
                }
            }
        }
    }
}
