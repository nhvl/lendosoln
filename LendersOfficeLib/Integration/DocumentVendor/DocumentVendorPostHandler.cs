﻿namespace LendersOffice.Integration.DocumentVendor
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Xml;
    using Common;
    using DataAccess;
    using LqbCommunication;
    using LQBDocumentResponse;

    /// <summary>
    /// A POST handler to process asynchronous document transmissions.
    /// </summary>
    public class DocumentVendorPostHandler : IHttpHandler
    {
        /// <summary>
        /// Gets a value indicating whether another request can use the DocumentVendorPostHandler instance.
        /// </summary>
        /// <value>True, since the handler is reusable.</value>
        public bool IsReusable
        {
            get { return true; }
        }

        /// <summary>
        /// Receives an asynchronous POST from a document vendor.
        /// </summary>
        /// <param name="context">The request context.</param>
        public void ProcessRequest(HttpContext context)
        {
            HttpRequest request = context.Request;
            var responseHandler = new DocumentCommunicationResponseHandler(context.Response);

            // Despite the transmission being initiated by the doc vendor, it is treated as a document response.
            LQBDocumentResponse.LQBDocumentResponse vendorResponse;

            try
            {
                using (var reader = new StreamReader(request.InputStream, request.ContentEncoding))
                {
                    string responseRaw = reader.ReadToEnd();
                    vendorResponse = DocumentVendorServer.ProcessRawResponse(responseRaw);
                    Tools.LogInfo("=== DOCUMENT FRAMEWORK ASYNC POST ===" + Environment.NewLine + DocumentVendorServer.FormatResponseForLogging(vendorResponse));
                }

                if (vendorResponse.Items == null || vendorResponse.AuditResponse != null || !vendorResponse.Items.Any() || vendorResponse.Items.Any(i => !this.IsValidItemForAsync(i)))
                {
                    responseHandler.Status = Status.Error;
                    responseHandler.Message = ErrorMessages.DocumentFramework.AsyncInvalidFormat;
                    responseHandler.SendResponse();
                    return;
                }
            }
            catch (XmlException)
            {
                responseHandler.Status = Status.Error;
                responseHandler.Message = ErrorMessages.DocumentFramework.AsyncProcessingFailure;
                responseHandler.SendResponse();
                return;
            }
            catch (InvalidOperationException)
            {
                responseHandler.Status = Status.Error;
                responseHandler.Message = ErrorMessages.DocumentFramework.AsyncInvalidFormat;
                responseHandler.SendResponse();
                return;
            }

            Guid loanId;
            if (!Guid.TryParse(vendorResponse.LoanID, out loanId))
            {
                responseHandler.Status = Status.Error;
                responseHandler.Message = ErrorMessages.DocumentFramework.AsyncRoutingFailure;
                responseHandler.SendResponse();
                return;
            }

            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(DocumentVendorPostHandler));
            dataLoan.InitLoad();

            Guid transactionID;
            if (!Guid.TryParse(vendorResponse.TransactionID, out transactionID)
                || !dataLoan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.Any(cd => cd.TransactionId == transactionID.ToString()))
            {
                responseHandler.Status = Status.Error;
                responseHandler.Message = ErrorMessages.DocumentFramework.AsyncRoutingFailure;
                responseHandler.SendResponse();
                return;
            }

            Guid brokerId = dataLoan.BrokerDB.BrokerID;
            Guid appId = dataLoan.GetAppData(0).aAppId;

            var result = ConfiguredDocumentGenerationResult.CreateAsync(loanId, brokerId, appId, vendorResponse.TransactionID, vendorResponse.Items);

            if (result.HasError)
            {
                responseHandler.Status = Status.Error;
                responseHandler.Message = result.ErrorMessage;
                responseHandler.SendResponse();
                return;
            }

            responseHandler.Status = Status.Ok;
            responseHandler.Message = ErrorMessages.DocumentFramework.AsyncSuccess;
            responseHandler.SendResponse();
        }

        /// <summary>
        /// Determines whether the response item is valid for the async handler. Currently PDFs
        /// cannot be processed asynchronously.
        /// </summary>
        /// <param name="document">The response item.</param>
        /// <returns>A boolean indicating whether the item is valid to be processed asynchronously.</returns>
        private bool IsValidItemForAsync(object document)
        {
            var documentResponse = document as DocumentResponse;
            return documentResponse != null && documentResponse.DocumentFormat != DocumentResponseDocumentFormat.PDF;
        }
    }
}
