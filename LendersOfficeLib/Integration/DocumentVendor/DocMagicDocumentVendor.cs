﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using DocMagic.DsiDocRequest;
using DocMagic.DsiDocResponse;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.ConfigSystem;
using LendersOffice.ConfigSystem.Operations;
using LendersOffice.Constants;
using LendersOffice.Conversions;
using LendersOffice.ObjLib.DocMagicLib;
using LendersOffice.Security;
using DsiDocRequest = DocMagic.DsiDocRequest;

namespace LendersOffice.Integration.DocumentVendor
{
    using DocMagicTransaction = DocumentVendorTransaction<DsiDocumentServerRequest, DsiDocumentServerResponse>;
    public class DocMagicDocumentVendor : IDocumentVendor
    {
        private DocMagicAuthentication x_docMagicAuthentication;
        
        private DocMagicAuthentication docMagicAuthentication
        {
            get
            {
                if (x_docMagicAuthentication == null)
                {
                    x_docMagicAuthentication = DocMagicAuthentication.Current;
                }
                return x_docMagicAuthentication;
            }
        }

        public DocMagicDocumentVendor(Guid brokerId)
        {
            this.BrokerId = brokerId;
        }

        public Guid BrokerId { get; private set; }

        private BrokerDB Broker
        {
            get
            {
                //This is cached, so we don't need to remember it
                return BrokerDB.RetrieveById(this.BrokerId);
            }
        }

        public DocumentVendorResult<List<DocumentPackage>> GetAvailableDocumentPackages(Guid LoanId_unused, AbstractUserPrincipal principal)
        {
            var request = DocMagicMismoRequest.CreatePackageTypeListRequest(docMagicAuthentication.CustomerId, docMagicAuthentication.UserName, docMagicAuthentication.Password);
            DsiDocumentServerResponse response;
            var result = new List<DocumentPackage>();

            var transaction = SubmitAndVerify(request, out response);
            if (transaction.HasError)
            {
                return DocumentVendorResult.Error(transaction.Error, result);
            }

            // Don't display packages of type LoanModification
            foreach (PackageType msg in response.InitResponse.PackageTypeList.Where(p => p.Type != E_PackageTypeType.FloodCertification && p.Type != E_PackageTypeType.Undefined))
            {
                var allowEPortal = msg.allowEPortalService == XmlSerializableCommon.E_YNIndicator.Y;
                var disableESign = false;
                var description = LqbGrammar.DataTypes.DocumentIntegrationPackageName.Create(msg.Type == E_PackageTypeType.FormList ? "Form List" : msg.Description).ForceValue();
                result.Add(new DocumentPackage(description, msg.Type.ToString(), disableESign, allowEPortal, allowEClose: false));
            }

            return DocumentVendorResult.Success(result);
        }

        public DocumentVendorResult<Dictionary<string, IEnumerable<PlanCode>>> GetAvailablePlanCodes(Guid LoanId_unused, AbstractUserPrincipal principal)
        {
            var planCodeRequest = DocMagicMismoRequest.CreateLoadCurrentPlansRequest(docMagicAuthentication.CustomerId, docMagicAuthentication.UserName, docMagicAuthentication.Password);
            DsiDocumentServerResponse planCodeResponse;
            var PlanCodes = new Dictionary<string, IEnumerable<PlanCode>>();

            var transaction = SubmitAndVerify(planCodeRequest, out planCodeResponse);
            if (transaction.HasError)
            {
                return DocumentVendorResult.Error(transaction.Error, PlanCodes);
            }

            if (!planCodeResponse.PlanResponseList.Any() || !planCodeResponse.PlanResponseList.First().PlanList.Any())
            {
                return DocumentVendorResult.Error("No plan codes available from server", PlanCodes);
            }

            

            //Custom plan codes are ones that DocMagic has but we don't.
            var ourPlanCodes = DocMagicPlanCode.ListAllDocMagicPlanCodes(this.Broker, true /*onlyApprovedPlanCodes */)
                                                .GroupBy(a => a.PlanCodeNm)
                                                .ToDictionary(a => a.Key, a => a.First());

            var customPlanCodes = from pc in planCodeResponse.PlanResponseList.First().PlanList
                                  where !ourPlanCodes.ContainsKey(pc.Code)
                                  select new PlanCode(pc.Description, pc.Code, null);

            PlanCodes["Custom"] = customPlanCodes.ToList();


            if (Broker.AllowInvestorDocMagicPlanCodes)
            {
                PlanCodes["Investor"] = from pc in ourPlanCodes.Values
                                        where pc.InvestorNm != "GENERIC"
                                        select new PlanCode(pc.PlanCodeDesc, pc.PlanCodeNm, pc.InvestorNm);

            }

            PlanCodes["Generic"] = from pc in ourPlanCodes.Values
                                   where pc.InvestorNm == "GENERIC"
                                   select new PlanCode(pc.PlanCodeDesc, pc.PlanCodeNm, null);


            return DocumentVendorResult.Success(PlanCodes);
        }

        public bool CanGenerateDocuments(AbstractUserPrincipal principal, Guid loanId, string packageId, out string failureReasons)
        {
            failureReasons = string.Empty;

            // No need to run workflow on system user principals. System has permission to generate any docs.
            if (principal is SystemUserPrincipal)
            {
                return true;
            }

            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(principal.BrokerId, loanId, WorkflowOperations.GenerateDocs);
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));

            bool canGenerateDocs = LendingQBExecutingEngine.CanPerform(WorkflowOperations.GenerateDocs, valueEvaluator);
            if (!canGenerateDocs)
            {
                failureReasons = LendingQBExecutingEngine.GetUserFriendlyMessage(WorkflowOperations.GenerateDocs, valueEvaluator);
            }

            return canGenerateDocs;
        }

        // The preventUsingTemporaryArchive bool is not used. It is required by
        // the interface, but the legacy DM vendor will NEVER use the temp archive.
        public DocumentVendorResult<Audit> AuditLoan(Guid sLId, Guid AppId, string PackageID, string LoanProgramID, AbstractUserPrincipal principal, bool preventUsingTemporaryArchive)
        {
            //OPM 110077: We need to let users in readonly loans audit as well.
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(DocMagicDocumentVendor));

            dataLoan.InitLoad();

            DsiDocRequest.E_AuditPackageType auditPackageType = (DsiDocRequest.E_AuditPackageType)Enum.Parse(typeof(DsiDocRequest.E_AuditPackageType), PackageID);

            string websheetNumber = dataLoan.sDocMagicFileId;
            DsiDocRequest.DsiDocumentServerRequest request = null;
            List<string> feeDiscrepancies;
            request = DocMagicMismoRequest.CreateAuditRequest(
                docMagicAuthentication.CustomerId,
                docMagicAuthentication.UserName,
                docMagicAuthentication.Password,
                sLId,
                String.IsNullOrEmpty(websheetNumber) ? "NEW" : websheetNumber,
                auditPackageType,
                out feeDiscrepancies);

            DsiDocumentServerResponse response;
            var ignoreGenericFailure = true;
            //On the audit call, we'll get ResponseStatus = Failure if there's any fatal errors in the loan. We don't want to let that stop us.
            var transaction = SubmitAndVerify(request, out response, ignoreGenericFailure);
            if (transaction.HasError)
            {
                return DocumentVendorResult.Error(transaction.Error, new Audit());
            }

            LogUnknownXPaths(response.MessageList);

            string newWebsheetNumber = GetWebsheetNumberFromResponse(response, websheetNumber);//will be set to null if not found/not needed

            if (!string.IsNullOrEmpty(newWebsheetNumber) || (dataLoan.sIsUseManualApr && response.Status != E_DsiDocumentServerResponseStatus.Failure))
            {
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                if (!string.IsNullOrEmpty(newWebsheetNumber))
                    dataLoan.sDocMagicFileId = newWebsheetNumber;

                if (dataLoan.sIsUseManualApr && response.Status != E_DsiDocumentServerResponseStatus.Failure) // True if this lender uses manual apr and this is a construction loan.
                {
                    if (response.WebsheetResponseList.First().Audit != null &&
                         response.WebsheetResponseList.First().Audit.AprPaymentCalculation != null)
                    {
                        decimal apr;
                        if (Decimal.TryParse(response.WebsheetResponseList.First().Audit.AprPaymentCalculation.Apr.Replace("%", ""), out apr))
                        {
                            dataLoan.sApr_rep = dataLoan.m_convertLos.ToRateString(apr * 100);
                        }
                        else
                            throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, "Cannot parse APR: " + response.WebsheetResponseList.First().Audit.AprPaymentCalculation.Apr);
                    }
                }

                dataLoan.Save();
            }

            var AuditLines = (from msg in response.MessageList
                             where (msg.Type == E_MessageType.Fatal || (msg.Type == E_MessageType.Warning && msg.Category == E_MessageCategory.Audit))
                             select AuditLine.CreateFromDocMagicMessage(msg, sLId, AppId)).ToList();

            // 3/3/14 gf - opm 172303, we want to add audit warnings for each fee discrepancy.
            if (feeDiscrepancies != null)
            {
                foreach (var feeDiscrepancy in feeDiscrepancies)
                {
                    AuditLines.Add(new AuditLine(feeDiscrepancy, AuditSeverity.Warning, null, null, null, sLId, AppId));
                }
            }

            return DocumentVendorResult.Success(new Audit(AuditLines));
        }
        /// <summary>
        /// Safely fetches Websheet number if it exists and string.IsNullOrEmpty(websheetNumber); returns null otherwise.
        /// </summary>
        private string GetWebsheetNumberFromResponse(DsiDocumentServerResponse response, string websheetNumber)
        {
            if (!string.IsNullOrEmpty(websheetNumber)) return null;
            var websheetResponse = response.WebsheetResponseList.FirstOrDefault();
            if (websheetResponse == null) return null;
            var websheetInfo = websheetResponse.WebsheetInfoList.FirstOrDefault();
            if (websheetInfo == null) return null;
            return websheetInfo.WebsheetNumber;
        }

        public DocumentVendorResult<List<FormInfo>> GetFormList(Guid LoanID, string unused1, string unused2, AbstractUserPrincipal principal)
        {
            var dataLoan = CPageData.CreateUsingSmartDependency(LoanID, typeof(DocMagicDocumentVendor));
            dataLoan.InitLoad();
            var typeExample = new List<FormInfo>();

            if (dataLoan.sDocMagicFileId == "")
            {
                return DocumentVendorResult.Error("Can't make a forms request when we haven't exported a loan to DocMagic", typeExample);
            }

            var auth = DocMagicAuthentication.Current;
            var request = DocMagicMismoRequest.CreateFormListRequest(
                auth.CustomerId,
                auth.UserName,
                auth.Password,
                dataLoan.sDocMagicFileId
                );

            // If you want to look at the xml that was sent and received,
            //   stick some LogInfos somewhere in here
            DsiDocumentServerResponse response;
            var transaction = SubmitAndVerify(request, out response);
            if (transaction.HasError)
            {
                return DocumentVendorResult.Error(transaction.Error, typeExample);
            }

            if (response.FormResponseList.Count == 0)
            {
                return DocumentVendorResult.Error("No forms available", typeExample);
            }
            
            var formResponse = response.FormResponseList.First();
            var Forms = from format in formResponse.FormatList
                        select new FormInfo(format.Form.BorrowerNumber, format.Description, format.Form.InnerText, "");

            return DocumentVendorResult.Success(Forms.ToList());
        }


        private static void LogUnknownXPaths(List<Message> list)
        {
            var XPaths = from msg in list
                         where (msg.Type == E_MessageType.Fatal || (msg.Type == E_MessageType.Warning && msg.Category == E_MessageCategory.Audit))
                         select new { XPath = msg.MismoRef, Msg = msg.InnerText, ErrorId = msg.MessageExtId };

            var NotFoundXPaths = DocMagicXpathMapping.Retrieve(new HashSet<string>(XPaths.Select(a => a.XPath).Distinct())).Where(a => a.Value == null).Select(a => a.Key);

            StringBuilder sb = new StringBuilder("The following XPaths had no mappings in the database: \r\n");
            foreach (var entry in from message in XPaths where NotFoundXPaths.Contains(message.XPath) select message)
            {
                string xpathMsg = entry.XPath;
                if (string.IsNullOrEmpty(xpathMsg))
                {
                    xpathMsg = entry.ErrorId;
                }
                sb.AppendLine(xpathMsg + "\t" + entry.Msg);
            }

            Tools.LogWarning(sb.ToString());
        }

        private DocMagicTransaction SubmitAndVerify(DsiDocumentServerRequest req, out DsiDocumentServerResponse response)
        {
            var ignoreGenericFailure = false;
            return SubmitAndVerify(req, out response, ignoreGenericFailure);
        }

        private DocMagicTransaction SubmitAndVerify(DsiDocumentServerRequest req, out DsiDocumentServerResponse response, bool ignoreGenericFailure)
        {
            response = DocMagicServer2.Submit(req);
            string error = VerifyResponse(response, ignoreGenericFailure);

            return DocumentVendorTransaction.Create(req, response, error, false);
        }

        private string VerifyResponse(DsiDocumentServerResponse response, bool ignoreGenericFailure)
        {
            if (response.Status == DocMagic.DsiDocResponse.E_DsiDocumentServerResponseStatus.Failure)
            {
                if (response.MessageList.Any(m => m.InnerText.Equals("Login failure")) || response.MessageList.Any(m => m.InnerText.Equals("User name must be a properly formatted email address")))
                {
                    return "Invalid username or password when logging in to your document vendor";
                }
                else
                {
                    if (ignoreGenericFailure) return null;

                    return string.Join("\n", (from x in response.MessageList
                                              where x.Type == E_MessageType.Fatal
                                              select x.InnerText.TrimWhitespaceAndBOM()).ToArray());
                }
            }
            
            return null;
        }

        public DocumentVendorResult<Permissions> GetPermissions(Guid LoanID, bool allowEPortal, bool disableESign, bool allowEClose, bool allowManualUnused, string PackageType, AbstractUserPrincipal principal)
        {
            bool enableMERS = PackageType == "Closing";
            bool enableEmail = PackageType != "Predisclosure" && PackageType != "Redisclosure";

            // The eClose feature is not supported in the legacy DocMagic seamless.
            allowEClose = false;

            return DocumentVendorResult.Success(new Permissions(enableMERS, allowEPortal, !disableESign, allowEClose, enableEmail));
        }

        public IDocumentGenerationRequest GetDocumentGenerationRequest(Guid AppId, Guid LoanId)
        {
            var dataLoan = CPageData.CreateUsingSmartDependency(LoanId, typeof(DocMagicDocumentVendor));
            dataLoan.InitLoad();
            var sLNm = dataLoan.sLNm;
            var Websheet = dataLoan.sDocMagicFileId.ToString();

            return new DocMagicDocumentGenerationRequestHandler(this.BrokerId, docMagicAuthentication.CustomerId,
                docMagicAuthentication.UserName,
                docMagicAuthentication.Password,
                LoanId,
                sLNm,
                AppId,
                Websheet
                );
        }

        private static DVSkin skin = new DVSkin("DocMagic", "Plan Code", "DSI Print and Deliver", true, true, true, false, true);
        public DVSkin Skin
        {
            get
            {
                return skin;
            }
        }

        public static VendorConfig DocMagicVendorConfig = new VendorConfig()
        {
            VendorName = skin.VendorName,
            SupportsBarcodes = true,
            SupportsOnlineInterface = true,
            EnableMismo34 = true,
            UsesAccountId = true,
            UsesPassword = true,
            UsesUsername = true,

            IsActive = true
        };

        public VendorConfig Config
        {
            get
            {
                return DocMagicVendorConfig;
            }
        }

        public IDocumentVendorCredentials Credentials
        {
            get
            {
                return docMagicAuthentication;
            }
        }

        public IDocumentVendorCredentials CredentialsFor(Guid UserId, Guid BranchId)
        {
            return DocMagicSavedAuthenticationForPTM.GetUserCredentialsOnly(UserId, this.BrokerId);
        }

        public DocumentVendorResult<List<InvestorInfo>> GetTransferToInvestors(string PlanCode)
        {
            var planCodeRequest = DocMagicMismoRequest.CreateGetDefaultValuesPlanRequest(docMagicAuthentication.CustomerId, docMagicAuthentication.UserName, docMagicAuthentication.Password, PlanCode);
            DsiDocumentServerResponse planCodeResponse;
            var result = new List<InvestorInfo>();
            
            var transaction = SubmitAndVerify(planCodeRequest, out planCodeResponse);
            if (transaction.HasError)
            {
                return DocumentVendorResult.Error(transaction.Error, result);
            }

            if (planCodeResponse.PlanResponseList.Any()
                && planCodeResponse.PlanResponseList.First().PlanList.Any()
                && planCodeResponse.PlanResponseList.First().PlanList.First().Defaults != null
                && planCodeResponse.PlanResponseList.First().PlanList.First().Defaults.TransferToInvestorList.Any())
            {
                var investors = from transfer in planCodeResponse.PlanResponseList.First().PlanList.First().Defaults.TransferToInvestorList
                                select new InvestorInfo(transfer.Description, transfer.Code);
                result.AddRange(investors);
            }

            return DocumentVendorResult.Success(result);
        }

        public DocumentVendorResult<LqbGrammar.DataTypes.LqbAbsoluteUri> GetIdsLoanFileLink(Guid loanId, AbstractUserPrincipal principal)
        {
            throw new CBaseException(ErrorMessages.Generic, new NotSupportedException("Cannot get an IDS loan file link from DocMagic vendor."));
        }

    }
}
