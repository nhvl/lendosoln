﻿using DocRequest = LendersOffice.Integration.DocumentVendor.LQBDocumentRequest.LQBDocumentRequest;
using DocResponse = LendersOffice.Integration.DocumentVendor.LQBDocumentResponse.LQBDocumentResponse;

namespace LendersOffice.Integration.DocumentVendor
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Serialization;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Conversions;
    using LendersOffice.Conversions.GenericMismoClosing26;
    using LendersOffice.Conversions.Mismo3.Version3;
    using LendersOffice.Conversions.Mismo3.Version4;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Integration.DocumentVendor.LQBDocumentRequest;
    using LendersOffice.Integration.DocumentVendor.LQBDocumentResponse;
    using LendersOffice.Integration.DocumentVendor.MISMOClosingV2;
    using LendersOffice.Security;
    using ConfiguredVendorTransaction = DocumentVendorTransaction<DocRequest, DocResponse>;

    public class DocumentVendorServer
    {
        List<ServerMessageType> ErrorTypes = new List<ServerMessageType>
        {
            ServerMessageType.DocumentError,
            ServerMessageType.LoginError,
            ServerMessageType.ServerError
        };

        #region Some helper classes
        private class UTF8StringWriter : StringWriter
        {
            public override Encoding Encoding
            {
                get { return Encoding.UTF8; }
            }
        }

        private class FixedNamespaceXmlReader : XmlTextReader
        {
            private string NameSpace;
            public FixedNamespaceXmlReader(System.IO.TextReader r, string nameSpace)
                : base(r)
            {
                NameSpace = nameSpace;
            }

            public override string NamespaceURI
            {
                get
                {
                    if (this.NodeType == XmlNodeType.Element)
                    {
                        return NameSpace;
                    }
                    return base.NamespaceURI;
                }
            }
        }

        private class UriStatistics
        {
            public string Description;
            public Uri Uri;
            // You have to manually set this to false on success
            public bool Failed = true;
        }
        #endregion
        
        private const string LogWarningHeader = "=== DOCUMENT FRAMEWORK WARNING ===";
        private const string LogErrorHeader = "=== DOCUMENT FRAMEWORK ERROR ===";
        private const string LogInfoHeader = "=== DOCUMENT FRAMEWORK INFO ===";
        public readonly VendorConfig Config;
        private readonly VendorCredentials Credentials;
        private readonly Guid brokerId;
        private readonly Lazy<BrokerDB> brokerDB;
        private bool IsProduction;

        public DocumentVendorServer(VendorConfig config, Guid BrokerId, VendorCredentials credentials)
        {
            Config = config;
            Credentials = credentials;
            this.brokerId = BrokerId;
            this.brokerDB = new Lazy<BrokerDB>(() => BrokerDB.RetrieveById(this.brokerId));

            if (config.IsTest)
            {
                IsProduction = false;
            }
            else
            {
                IsProduction = true;
            }
        }

        protected BrokerDB BrokerDB
        {
            get { return this.brokerDB.Value; }
        }
        
        private static byte[] GetRequestBytes(string requestXml, VendorConfig config)
        {
            string extraParams = string.IsNullOrEmpty(config.ExtraParameters) ? "" : config.ExtraParameters;
            
            switch (config.Protocol)
            {
                case (DVProtocol.HTTP_POST):
                    if (config.SendAsApplicationXml)
                    {
                        return Encoding.UTF8.GetBytes(requestXml);
                    }
                    else
                    {
                        return Encoding.UTF8.GetBytes(config.ParameterName + "=" + HttpUtility.UrlEncode(requestXml) + extraParams);
                    }
                default:
                    throw new NotImplementedException();
            }
        }

        private static string GetContentType(VendorConfig sc)
        {

            if (sc.SendAsApplicationXml)
            {
                return "application/xml"; 
            }

            switch (sc.Protocol)
            {
                case (DVProtocol.HTTP_POST):
                    return "application/x-www-form-urlencoded";
                default:
                    throw new NotImplementedException();
            }
        }

        private static HttpWebRequest CreateWebRequest(byte[] bytes, VendorConfig sc, Uri exportPath)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(exportPath);
            webRequest.KeepAlive = false; // 11/4/2004 dd - DO NOT REMOVE THIS LINE. Else you will get "Cannot access a disposed object named "System.Net.TlsStream"" error randomly.
            webRequest.Method = "POST";
            webRequest.Timeout = 200000;
            webRequest.ContentType = GetContentType(sc);
            webRequest.ContentLength = bytes.Length;



            if (sc.PrimaryExportPath.GetLeftPart(UriPartial.Authority).Equals("https://www.docsondemand.net", StringComparison.OrdinalIgnoreCase))
            {
                if (PrincipalFactory.CurrentPrincipal != null)
                {
                    BrokerDB db = PrincipalFactory.CurrentPrincipal.BrokerDB;

                    if (db.IsUseDODLimitsEnabled)
                    {
                        webRequest.ServicePoint.ConnectionLimit = 1;
                        webRequest.ProtocolVersion = HttpVersion.Version10;
                    }
                }
            }

            return webRequest;
        }

        public static DocResponse ProcessRawResponse(string responseRaw)
        {
            XDocument responseWrapperXml;

            try
            {
                responseWrapperXml = XDocument.Parse(responseRaw);
            }
            catch (Exception)
            {
                Tools.LogInfo("Raw response: " + responseRaw);
                throw;
            }

            var lqbRootNode = responseWrapperXml.Root
                .DescendantNodesAndSelf()
                .OfType<XElement>()
                .Where(n => n.Name.LocalName.Equals(nameof(LQBDocumentResponse)));

            DocResponse response;
            XmlSerializer deserializer = new XmlSerializer(typeof(DocResponse));

            if (lqbRootNode.Any())
            {
                var rootString = lqbRootNode.First().ToString(SaveOptions.DisableFormatting);
                response = (DocResponse)deserializer.Deserialize(
                    new FixedNamespaceXmlReader(new StringReader(rootString),
                        "http://tempuri.org/LQBDocumentResponse"));
                return response;
            }

            var textNodes = responseWrapperXml.DescendantNodes()
                .Where(n => n.NodeType == XmlNodeType.Text);

            var responseXml = HttpUtility.HtmlDecode(textNodes.First().ToString());

            try
            {
                response = (DocResponse)deserializer.Deserialize(
                    new FixedNamespaceXmlReader(
                        new StringReader(responseXml),
                        "http://tempuri.org/LQBDocumentResponse")
                );
            }
            catch (InvalidOperationException)
            {
                Tools.LogInfo("Raw response: " + responseRaw);
                Tools.LogInfo("responseXml: " + responseXml);
                throw;
            }

            return response;
        }

        private static DocResponse GetResponse(WebRequest webRequest, Guid LoanId)
        {
            DocResponse response;
            using (WebResponse webResponse = webRequest.GetResponse())
            {
                using (StreamReader sr = new StreamReader(webResponse.GetResponseStream()))
                {
                    string responseRaw = sr.ReadToEnd();

                    if (ConstStage.EnableDocVendorRequestSaving)
                    {
                        string tempFilePath = TempFileUtils.NewTempFilePath();
                        TextFileHelper.WriteString(tempFilePath, responseRaw, false);
                        string key = string.Concat("DocVendorResponse_", LoanId.ToString("N"), "_", Guid.NewGuid().ToString("N"));
                        AutoExpiredTextCache.AddFileToCache(tempFilePath, TimeSpan.FromDays(8), key);
                        Tools.LogInfo("VendorRequestResponse stored in temp file "  + tempFilePath + " with key " + key);
                    }

                    response = ProcessRawResponse(responseRaw);
                }
            }

            return response;
        }

        private static RequestRequestType[] SendLoanInfoRequests = new[]{
            RequestRequestType.Audit,
            RequestRequestType.RequestDocuments,
            RequestRequestType.FormListRequest
        };

        /// <summary>
        /// Submits a request to the document vendor. Use this version if you don't care about attaching loan data.
        /// </summary>
        /// <param name="req">The request to submit</param>
        /// <returns>The client/server transaction</returns>
        public ConfiguredVendorTransaction Submit(DocRequest req, AbstractUserPrincipal principal, bool isPreviewOrder = false)
        {
            return Submit(req, Guid.Empty, principal, isPreviewOrder);
        }

        /// <summary>
        /// Submits a request to the document vendor. Use this version if you need to attach loan data.
        /// </summary>
        /// <param name="req">The request to submit</param>
        /// <param name="LoanId">The loan for which the request is being submitted</param>
        /// <returns>The client/server transaction</returns>
        public ConfiguredVendorTransaction Submit(DocRequest req, Guid LoanId, AbstractUserPrincipal principal, bool isPreviewOrder = false)
        {
            List<string> feeDiscrepancies;

            return Submit(
                req, 
                LoanId, 
                out feeDiscrepancies, 
                principal,
                preventUsingTemporaryArchive: false, 
                isPreviewOrder: isPreviewOrder);
        }

        public string GetDocumentFrameworkPayload(DocRequest req, Guid loanId, Guid? transactionId, out List<string> feeDiscrepancies, AbstractUserPrincipal principal, bool preventUsingTemporaryArchive = false, bool isPreviewOrder = false, VendorConfig.DocumentPackage package = null)
        {
            feeDiscrepancies = null;

            if (Credentials != null)
            {
                req.Authentication = new Authentication();
                req.Authentication.UserName = Credentials.UserName;
                req.Authentication.Password = Credentials.Password;
                req.Authentication.AccountID = Credentials.CustomerId;
            }

            if (this.Config.PlatformType == E_DocumentVendor.DocMagic && this.BrokerDB.AllActiveDocumentVendors
                .Single(vendorSettings => vendorSettings.VendorId == this.Config.VendorId).DisclosureBilling)
            {
                req.PartnerInformation = new PartnerInformation();
                req.PartnerInformation.PartnerID = "lendingqb";
                req.PartnerInformation.Password = "4LQBDocs";
            }

            req.RequestSource = new RequestSource();
            req.RequestSource.SourceName = IsProduction ? Config.LiveAccountId : Config.TestAccountId;
            if (transactionId.HasValue)
            {
                req.RequestSource.TransactionID = transactionId.Value.ToString();
            }

            req.RequestSource.LoanID = loanId.ToString();

            var vendor = this.Config.PlatformType;
            string payload = string.Empty;
            MismoPayloadFormat payloadFormat = MismoPayloadFormat.Mismo26;

            if (SendLoanInfoRequests.Contains(req.Request.RequestType))
            {
                req.LoanData = new LoanData();
                if (loanId == Guid.Empty)
                {
                    Tools.LogWarning(LogWarningHeader + Environment.NewLine + "Did not get a loan id when submitting a request that requires loan data.");
                }

                var vendorPackage = package ?? Config.RetrievePackage(((IHavePackageID)req.Request.Item).PackageID);
                var dataLayerMode = E_sClosingCostFeeVersionT.ClosingCostFee2015;
                var regulationType = E_sDisclosureRegulationT.Blank;
                var isLoanUlad = false;

                if (!string.IsNullOrEmpty(VendorConfig.LOXmlExportFields.TrimWhitespaceAndBOM()))
                {
                    bool useTempArchiveForTridFile = !preventUsingTemporaryArchive &&
                        vendorPackage != VendorConfig.DocumentPackage.InvalidPackage;

                    string sloxml = LOFormatExporter.Export(
                        loanId,
                        PrincipalFactory.CurrentPrincipal,
                        VendorConfig.LOXmlExportFields,
                        FormatTarget.MismoClosing,
                        bypassFieldSecurity: true,
                        isClosingDisclosure: vendorPackage.IsOfType(VendorConfig.PackageType.Closing),
                        useTempArchiveForTridFile: useTempArchiveForTridFile,
                        excludeZeroAdjustmentForDocGenInLoXml: this.BrokerDB.ExcludeZeroAdjustmentForDocGenInLoXml);

                    const string LOXmlHeader = @"LOXml xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns=""http://tempuri.org/LQBDocumentRequest""";
                    var resultString = "<?xml version=\"1.0\" encoding=\"utf-16\"?>" +
                                       // We call it "LOXml", so replace that.
                                       sloxml.Replace("LOXmlFormat version=\"1.0\"", LOXmlHeader)
                                             .Replace("LOXmlFormat", "LOXml")
                                             // We also need to strip out this <loan> element
                                             .Replace("<loan>", "")
                                             .Replace("</loan>", "");

                    if (vendor == E_DocumentVendor.DocuTech) //OPM 146326: Temporary GFE Redisclosure package for DocuTech
                    {
                        IHavePackageID requestItem = (IHavePackageID)req.Request.Item;
                        bool isGfeRedisclosurePackage = requestItem.PackageID == ConstAppDavid.DocuTech_Temp_GFE_Redisclosure_PackageID;
                        if (isGfeRedisclosurePackage)
                        {
                            requestItem.PackageID = "203";
                            resultString = resultString.Replace("</LOXml>", "<field id=\"sIsGFERedisclosure\">Y</field></LOXml>");
                        }
                        else
                        {
                            resultString = resultString.Replace("</LOXml>", "<field id=\"sIsGFERedisclosure\">N</field></LOXml>");
                        }
                    }

                    using (var source = new XmlTextReader(new StringReader(resultString)))
                    {
                        XmlSerializer loxmlSerializer = new XmlSerializer(typeof(LOXml));
                        var extraData = (LOXml)loxmlSerializer.Deserialize(source);
                        if (extraData == null || (extraData.field == null && extraData.applicant == null))
                        {
                            Tools.LogWarning(LogWarningHeader + Environment.NewLine + "LOXml Supplemental data deserialized " + (extraData == null ? "as null!" : "without any values!"));
                        }

                        req.LoanData.LOXml = extraData;

                        bool foundRegulationType = false;
                        bool foundDataLayerMode = false;
                        bool foundIsLoanUlad = false;
                        foreach (field lqbField in req.LoanData.LOXml.field)
                        {
                            if (lqbField.id.Equals(nameof(CPageData.sClosingCostFeeVersionT), StringComparison.OrdinalIgnoreCase))
                            {
                                dataLayerMode = GetEnum(lqbField.Value, E_sClosingCostFeeVersionT.Legacy);
                                foundDataLayerMode = true;

                                if (foundRegulationType && foundIsLoanUlad)
                                {
                                    break;
                                }
                            }
                            else if (lqbField.id.Equals(nameof(CPageData.sDisclosureRegulationT), StringComparison.OrdinalIgnoreCase))
                            {
                                regulationType = GetEnum(lqbField.Value, E_sDisclosureRegulationT.GFE);
                                foundRegulationType = true;

                                if (foundDataLayerMode && foundIsLoanUlad)
                                {
                                    break;
                                }
                            }
                            else if (lqbField.id.Equals(nameof(CPageData.sBorrowerApplicationCollectionT), StringComparison.OrdinalIgnoreCase))
                            {
                                isLoanUlad = GetEnum(lqbField.Value, E_sLqbCollectionT.Legacy) != E_sLqbCollectionT.Legacy;
                                foundIsLoanUlad = true;

                                if (foundDataLayerMode && foundRegulationType)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }

                payloadFormat = DeterminePayloadFormat(this.BrokerDB, this.Config, dataLayerMode, regulationType, isLoanUlad);

                bool includeIntegratedDisclosureExtension = vendor == E_DocumentVendor.DocsOnDemand
                    || vendor == E_DocumentVendor.IDS
                    || vendor == E_DocumentVendor.ClosingXpress;

                bool useTemporaryArchive = !preventUsingTemporaryArchive &&
                    regulationType == E_sDisclosureRegulationT.TRID &&
                    vendorPackage != VendorConfig.DocumentPackage.InvalidPackage;

                bool useMismo3DefaultNamespace = vendor == E_DocumentVendor.DocuTech;

                if (payloadFormat == MismoPayloadFormat.Mismo34)
                {
                    var payloadOptions = new Mismo34ExporterOptions
                    {
                        LoanId = loanId,
                        VendorAccountId = req.Authentication?.AccountID,
                        TransactionId = req.RequestSource?.TransactionID,
                        DocumentPackage = vendorPackage,
                        IncludeIntegratedDisclosureExtension = includeIntegratedDisclosureExtension,
                        Principal = principal,
                        DocumentVendor = vendor,
                        UseTemporaryArchive = useTemporaryArchive,
                        UseMismo3DefaultNamespace = useMismo3DefaultNamespace
                    };

                    var exporter = new Mismo34Exporter(payloadOptions);
                    payload = exporter.SerializePayload();
                    payload = payload.Replace(@"xmlns:lendingqb=""http://www.lendingqb.com""", "");
                }
                else if (payloadFormat == MismoPayloadFormat.Mismo33)
                {
                    payload = Mismo33RequestProvider.SerializeMessage(
                        loanId,
                        req.Authentication?.AccountID,
                        req.RequestSource?.TransactionID,
                        vendorPackage,
                        includeIntegratedDisclosureExtension,
                        principal,
                        vendor,
                        useTemporaryArchive,
                        useMismo3DefaultNamespace: useMismo3DefaultNamespace);

                    payload = payload.Replace(@"xmlns:lendingqb=""http://www.lendingqb.com""", "");
                }
                else
                {
                    // 3/3/14 gf - opm 172303, we want to supply a warning and not a FATAL when we detect
                    // fee discrepancies.
                    payload = GenericMismoClosing26Exporter.ExportCustom(
                        loanId,
                        Config.VendorName,
                        vendorPackage,
                        out feeDiscrepancies,
                        principal,
                        useTemporaryArchive: useTemporaryArchive);

                    Regex FixMismo = new Regex("<LOAN", RegexOptions.Compiled);
                    payload = FixMismo.Replace(payload, @"<LOAN xmlns=""http://tempuri.org/MISMOClosingV2""", 1);

                    using (var source = new StringReader(payload))
                    {
                        XmlSerializer mismoSerializer = new XmlSerializer(typeof(LOAN));

                        req.LoanData.LOAN = (LOAN)mismoSerializer.Deserialize(source);
                    }
                }

                // The Preview attribute should be excluded entirely unless the vendor has a preview environment URL.
                req.Request.PreviewSpecified = req.Request.RequestType == RequestRequestType.RequestDocuments && Config.PreviewExportPath != default(Uri);
            }

            // 2/20/2015 BB - Migrates request serialization to the helper while preserving the namespaces formerly included at the LQBDocumentRequest root.
            string request = SerializationHelper.XmlSerialize(req, true, GetNamespaces(payloadFormat));

            if (payloadFormat == MismoPayloadFormat.Mismo33 || payloadFormat == MismoPayloadFormat.Mismo34)
            {
                payload = payload.Replace("$", "$$").TrimWhitespaceAndBOM();
                request = Regex.Replace(request, "<LoanData>", "<LoanData>" + payload);
            }
            else if (payloadFormat == MismoPayloadFormat.Mismo26 && vendor == E_DocumentVendor.DocuTech)
            {
                string ticket = AuthServiceHelper.GetGenericFrameworkUserAuthTicket(PrincipalFactory.CurrentPrincipal, Tools.GetLoanRefNmByLoanId(this.brokerId, loanId), null).ToString();
                ticket = ticket.Replace("$", "$$").TrimWhitespaceAndBOM();
                request = Regex.Replace(request, "</Authentication>", ticket + "</Authentication>");
            }

            return request;
        }

        public ConfiguredVendorTransaction Submit(DocRequest req, Guid LoanId, out List<string> feeDiscrepancies, AbstractUserPrincipal principal, bool preventUsingTemporaryArchive = false, bool isPreviewOrder = false)
        {
            List<UriStatistics> Attempts = new List<UriStatistics>(2);

            DocResponse response = null;
            feeDiscrepancies = null;
            try
            {
                Guid transactionId = Guid.NewGuid();
                string request = this.GetDocumentFrameworkPayload(req, LoanId, transactionId, out feeDiscrepancies, principal, preventUsingTemporaryArchive, isPreviewOrder);

                if (req.Request.RequestType == RequestRequestType.RequestDocuments)
                {
                    // 4/15/2014 dd - Log Request straight to LOTemp folder to retrieve at later time.
                    TextFileHelper.WriteString(ConstApp.TempFolder + @"\" + transactionId + "_request.xml", request, false);
                }

                if (isPreviewOrder && Config.PreviewExportPath != default(Uri) && req.Request.RequestType == RequestRequestType.RequestDocuments)
                {
                    Attempts.Add(new UriStatistics()
                    {
                        Description = "Preview",
                        Uri = Config.PreviewExportPath
                    });

                    response = SubmitRequest(Config, Config.PreviewExportPath, request, req.Request.RequestType.ToString(), LoanId);

                    Attempts.Last().Failed = false;
                }
                else if (!IsProduction && Config.TestingExportPath != default(Uri))
                {
                    Attempts.Add(new UriStatistics()
                    {
                        Description = "Test",
                        Uri = Config.TestingExportPath
                    });

                    response = SubmitRequest(Config, Config.TestingExportPath, request, req.Request.RequestType.ToString(), LoanId);

                    Attempts.Last().Failed = false;
                }
                else
                {
                    try
                    {
                        Attempts.Add(new UriStatistics()
                        {
                            Description = "Primary",
                            Uri = Config.PrimaryExportPath
                        });

                        response = SubmitRequest(Config, Config.PrimaryExportPath, request, req.Request.RequestType.ToString(), LoanId);

                        Attempts.Last().Failed = false;
                    }
                    catch (Exception e)
                    {
                        string switchToBackupMsg = "Error while submitting to primary export path of vendor [" + Config.VendorName + "]. " +
                                                        "Primary export path: [" + Config.PrimaryExportPath +
                                                        "], re-trying with backup: [" + Config.BackupExportPath +
                                                        "]. Exception is: " + e.Message;

                        Tools.LogErrorWithCriticalTracking(LogErrorHeader + Environment.NewLine + switchToBackupMsg, e);

                        if (Config.BackupExportPath == default(Uri))
                        {
                            Tools.LogErrorWithCriticalTracking(LogErrorHeader + Environment.NewLine + "Submitting to primary export path [" + Config.PrimaryExportPath + "] failed for vendor [" +
                                            Config.VendorName + "] and no backup export path is configured.");
                            throw;
                        }

                        try
                        {
                            Attempts.Add(new UriStatistics()
                            {
                                Description = "Backup",
                                Uri = Config.BackupExportPath
                            });

                            response = SubmitRequest(Config, Config.BackupExportPath, request, req.Request.RequestType.ToString(), LoanId);

                            Attempts.Last().Failed = false;
                        }
                        catch (Exception backupException)
                        {
                            Tools.LogErrorWithCriticalTracking(LogErrorHeader + Environment.NewLine + "Submitting to both primary [" + Config.PrimaryExportPath +
                                           "] and backup [" + Config.BackupExportPath +
                                           "] export paths failed for vendor [" + Config.VendorName + "]", backupException);
                            throw;
                        }
                    }
                }

                bool isError = response.RequestStatus == LQBDocumentResponseRequestStatus.Failure;
                string errorMessage = null;
                bool ignoreError = false;

                if (isError)
                {
                    var messages = (response.ServerResponse ?? new ServerMessage[] { });
                    var errors = from r in messages
                                 where ErrorTypes.Contains(r.Type)
                                 select r.Type.ToString() + ": " + r.Description;

                    //if we didn't find any errors but we know it failed, just grab whatever we can.
                    if (!errors.Any())
                    {
                        errors = from r in messages
                                 select r.Type.ToString() + ": " + r.Description;
                    }

                    string errorPrefix = "Document server returned an error: ";
                    if (errors.Count() > 1)
                    {
                        errorPrefix = "Document server returned errors: ";
                    }

                    if (errors.Count() > 0)
                    {
                        errorMessage = errorPrefix + Environment.NewLine + string.Join(Environment.NewLine, errors.ToArray());
                    }
                    else
                    {
                        errorMessage = "Document server returned an error, but no error messages.";
                    }
                }

                if (!string.IsNullOrEmpty(errorMessage))
                {
                    Tools.LogInfo(LogErrorHeader + Environment.NewLine + errorMessage);
                }

                if (response.RequestStatus == LQBDocumentResponseRequestStatus.Success)
                {
                    var messages = (response.ServerResponse ?? new ServerMessage[] { });
                    var otherMessages = from r in messages
                                 where r.Type == ServerMessageType.Other
                                 select r.Description;
                    if (otherMessages.Count() > 0)
                    {
                        errorMessage = string.Join(Environment.NewLine, otherMessages.ToArray());
                        ignoreError = true;
                        Tools.LogInfo(LogInfoHeader + Environment.NewLine + errorMessage);
                    }
                }

                Tools.LogInfo(GetAttemptsLog(Attempts));
                
                return DocumentVendorTransaction.Create(req, response, errorMessage, ignoreError);
            }
            catch (WebException e)
            {
                Tools.LogInfo(GetAttemptsLog(Attempts));
                Tools.LogErrorWithCriticalTracking(LogErrorHeader, e);
                return DocumentVendorTransaction.Create(req, default(DocResponse), e.Message, false);
            }
            catch (Exception e)
            {
                Tools.LogInfo(GetAttemptsLog(Attempts));
                Tools.LogErrorWithCriticalTracking(LogErrorHeader, e);
                throw;
            }
        }

        /// <summary>
        /// Get the namespaces to include at the LQBDocumentRequest root.
        /// </summary>
        /// <param name="hasMismo">Whether the export contains MISMO 3-branch content.</param>
        /// <returns>The list of namespaces to include when serializing the LQBDocumentRequest.</returns>
        /// <remarks>
        /// We used to reorder these namespaces using string replacement in the <see cref="SubmitRequest"/> method (Case 220238).
        /// That no longer seems to be necessary with IDS.
        /// </remarks>
        private static XmlSerializerNamespaces GetNamespaces(MismoPayloadFormat payloadFormat)
        {
            XmlSerializerNamespaces nameSpaces = new XmlSerializerNamespaces();
            if (payloadFormat == MismoPayloadFormat.Mismo33 || payloadFormat == MismoPayloadFormat.Mismo34)
            {
                nameSpaces.Add("lendingqb", "http://www.lendingqb.com");
            }

            nameSpaces.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            nameSpaces.Add("xsd", "http://www.w3.org/2001/XMLSchema");
            nameSpaces.Add("", "http://tempuri.org/LQBDocumentRequest");

            return nameSpaces;
        }

        /// <summary>
        /// Converts the given string value from the LOXML to the corresponding enum value of type <see cref="TEnum"/>.
        /// </summary>
        /// <param name="value">A string representation of <see cref="TEnum"/>.</param>
        /// <param name="defaultValue">The default value to return if parsing does not yield a result.</param>
        /// <returns>The corresponding <see cref="TEnum"/>, or null if no conversion was possible.</returns>
        private static TEnum GetEnum<TEnum>(string value, TEnum defaultValue) where TEnum : struct, IConvertible
        {
            try
            {
                return XmlSerializationExtensions.TryParseEnum<TEnum>(value) ?? defaultValue;
            }
            catch (ArgumentException)
            {
                Tools.LogError(LogErrorHeader + Environment.NewLine + "Failed to parse the " + typeof(TEnum).Name + " value [" + value + "] from the LOXML field list.");
            }
            catch (OverflowException)
            {
                Tools.LogError(LogErrorHeader + Environment.NewLine + "Failed to parse the " + typeof(TEnum).Name + " value [" + value + "] from the LOXML field list.");
            }

            return defaultValue;
        }

        private static string GetAttemptsLog(IEnumerable<UriStatistics> Attempts)
        {
            StringBuilder sb = new StringBuilder(LogInfoHeader + Environment.NewLine + "Connection attempts: " + Environment.NewLine);
            for(var i = 0; i < Attempts.Count(); i++)
            {
                var att = Attempts.ElementAt(i);
                sb.AppendLine("Attempt " + (i + 1) + ", " + att.Description + ": ");
                sb.AppendLine("URL: [" + att.Uri.ToStringOrEmpty() + "], " + (att.Failed ? "Failed" : "Succeeded"));
            }

            return sb.ToString();
        }

        /// <summary>
        /// Determine which MISMO export to use based on the data-layer mode and the regulation type.
        /// </summary>
        /// <param name="brokerDb">The broker settings.</param>
        /// <param name="vendorConfig">The vendor settings.</param>
        /// <param name="dataLayerMode">The <see cref="E_sClosingCostFeeVersionT"/> of the loan.</param>
        /// <param name="regulationType">The <see cref="E_sDisclosureRegulationT"/> of the loan.</param>
        /// <param name="loanIsUlad">Indicates whether the loan is under ULAD.</param>
        /// <returns>The payload format that should be exported.</returns>
        private static MismoPayloadFormat DeterminePayloadFormat(BrokerDB brokerDb, VendorConfig vendorConfig, E_sClosingCostFeeVersionT dataLayerMode, E_sDisclosureRegulationT regulationType, bool loanIsUlad)
        {
            bool forceMismo26 = false;
            if (vendorConfig.PlatformType == E_DocumentVendor.DocuTech)
            {
                var enabledDocutechMismo33Vendors = brokerDb.GetEnableSendingDocuTechMISMO33Vendors();
                forceMismo26 = !enabledDocutechMismo33Vendors.Any() || !enabledDocutechMismo33Vendors.Contains(vendorConfig.VendorName);
            }

            bool loanShouldUseMismo26 = dataLayerMode == E_sClosingCostFeeVersionT.Legacy
                || regulationType == E_sDisclosureRegulationT.GFE;

            bool forceMismo3Branch = vendorConfig.PlatformType == E_DocumentVendor.DocsOnDemand;

            if (forceMismo26 || (loanShouldUseMismo26 && !forceMismo3Branch))
            {
                return MismoPayloadFormat.Mismo26;
            }
            else if (loanIsUlad && vendorConfig.EnableMismo34)
            {
                return MismoPayloadFormat.Mismo34;
            }
            else
            {
                return MismoPayloadFormat.Mismo33;
            }
        }

        private DocResponse SubmitRequest(VendorConfig config, Uri exportPath, string request, string requestType, Guid LoanId)
        {
            if (ConstStage.EnableDocVendorRequestSaving)
            {
                string tempFilePath = TempFileUtils.NewTempFilePath();
                string logRequest = FormatRequestForLogging(request);
                TextFileHelper.WriteString(tempFilePath, logRequest, false);
                string key = string.Concat("DocVendorRequest_", LoanId.ToString("N"),"_", Guid.NewGuid().ToString("N"));
                AutoExpiredTextCache.AddFileToCache(tempFilePath, TimeSpan.FromDays(8), key);
                Tools.LogInfo("VendorRequest stored in " + tempFilePath + " with key " + key);
            }

            Tools.LogInfo(string.Format(
                  "{0}{1}Vendor: {2} RequestType: {3}{4}{5}",
                  "=== DOCUMENT FRAMEWORK REQUEST ===",
                  Environment.NewLine,
                  Config.VendorName,
                  requestType,
                  Environment.NewLine,
                  FormatRequestForLogging(request)));

            var bytes = GetRequestBytes(request, config);

            var webRequest = CreateWebRequest(bytes, config, exportPath);
            
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            
            // Send out the data - don't use a StreamWriter, those don't work nice with request streams.
            using (Stream stream = webRequest.GetRequestStream())
            {
                stream.Write(bytes, 0, bytes.Length);
            }

            DocResponse response;
            try
            {
                response = GetResponse(webRequest, LoanId);
            }
            catch (WebException ex)
            {
                throw ex;
            }
            catch (InvalidOperationException ioe)
            {
                throw ioe;
            }
            finally
            {
                sw.Stop();
                Tools.LogInfo(LogInfoHeader + Environment.NewLine + string.Format("GetResponseTook: {0}ms", sw.ElapsedMilliseconds));
            }

            Tools.LogInfo("=== DOCUMENT FRAMEWORK RESPONSE ===" + Environment.NewLine + FormatResponseForLogging(response));
            return response;
        }

        public static string FormatResponseForLogging(DocResponse resp)
        {
            if (resp == null) return "Response is null";

            string result;

            // OPM 247161 - Support multiple DocumentRespose objects.
            string[] temp = null;
            bool hasDocumentResponseItems = resp.Items?[0] is DocumentResponse;
            if (hasDocumentResponseItems)
            {
                temp = new string[resp.Items.Length];
                for (int i = 0; i < resp.Items.Length; i++)
                {
                    DocumentResponse documents = resp.Items[i] as DocumentResponse;

                    temp[i] = documents.EncodedDocs;
                    documents.EncodedDocs = "Redacted to save space. Original size: " + temp[i].Length;
                }
            }

            using (StringWriter sw = new StringWriter())
            {
                XmlSerializer responseSerializer = new XmlSerializer(typeof(DocResponse));
                responseSerializer.Serialize(sw, resp);
                result = sw.ToString();
            }

            if (hasDocumentResponseItems)
            {
                for (int i = 0; i < temp.Length; i++)
                {
                    DocumentResponse documents = resp.Items[i] as DocumentResponse;
                    documents.EncodedDocs = temp[i];
                }
            }

            return result;
        }

        /// <summary>
        /// Masks sensitive data out of the doc request prior to logging.
        /// </summary>
        /// <param name="requestXML">The request XML string.</param>
        /// <returns>A string that has been prepped for PB logging.</returns>
        public static string FormatRequestForLogging(string requestXML)
        {
            if (string.IsNullOrEmpty(requestXML))
            {
                return "Request is empty.";
            }

            requestXML = Regex.Replace(requestXML, "<Password>[^<]*?</Password>", "<Password>*****</Password>");

            return requestXML;
        }
    }
}

namespace LendersOffice.Integration.DocumentVendor.LQBDocumentRequest
{
    public interface IHavePackageID
    {
        string PackageID { get; set; }
    }

    public partial class AuditRequest : IHavePackageID
    {
    }

    public partial class FormListRequest : IHavePackageID
    {
    }

    public partial class DocumentRequest : IHavePackageID
    {
    }
}
