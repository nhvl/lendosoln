﻿using System;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using DocMagic.DsiDocRequest;
using DocMagic.DsiDocResponse;
using LendersOffice.ObjLib.DocMagicLib;
using System.Xml.Serialization;
using System.IO;
using DataAccess;
using LendersOffice.Integration.DocumentVendor.LQBDocumentResponse;
using System.Collections;
using System.Collections.Generic;
using LendersOffice.Common;

namespace LendersOffice.Integration.DocumentVendor
{
    public static class DocumentVendorExtensions
    {
        public static AuditSeverity ToAuditSeverity(this E_MessageType m)
        {
            switch (m)
            {
                case (E_MessageType.Fatal):
                    return AuditSeverity.Fatal;
                case (E_MessageType.Warning):
                    return AuditSeverity.Warning;
                case (E_MessageType.Info):
                    return AuditSeverity.Misc;
            }

            return AuditSeverity.Undefined;
        }

        public static string FormatXmlString(this string xml)
        {
            string result;
            try
            {
                XDocument xdoc = XDocument.Parse(xml);
                result = xdoc.ToString(SaveOptions.None);
            }
            catch (FormatException e)
            {
                result = "[There was a FormatException while parsing this XML: " + e.Message + " ] -- " + xml;
            }
            catch (XmlException e)
            {
                result = "[There was an XmlException while parsing this XML: " + e.Message + "] -- " + xml;
            }
            return Environment.NewLine + result + Environment.NewLine;
        }
    }

    public enum AuditSeverity
    {
        Undefined,
        Misc,
        Info,
        Warning,
        Critical,
        Fatal
    }

    public struct DVSkin
    {
        public readonly string VendorName;
        public readonly string NameForPlanCodes;
        public readonly bool ChargeForManualFulfillment;
        public readonly bool PickDocumentFormat;
        public readonly string NameForManualFulfillment;
        public readonly bool AutomaticPlanCodes;
        public readonly bool AutomaticDocTypes;
        public readonly bool SupportsTransferToInvestors;

        public DVSkin(string name, string plancodes, string fulfillment, bool chargeForPrint, bool pickFormat, bool autoPlanCodes, bool automaticDocTypes, bool supportsTransferToInvestors)
        {
            VendorName = name;
            NameForPlanCodes = plancodes;
            NameForManualFulfillment = fulfillment;
            ChargeForManualFulfillment = chargeForPrint;
            PickDocumentFormat = pickFormat;
            AutomaticPlanCodes = autoPlanCodes;
            AutomaticDocTypes = automaticDocTypes;
            SupportsTransferToInvestors = supportsTransferToInvestors;
        }

    }

    public struct Permissions
    {
        public readonly bool EnableMERSRegistration;
        public readonly bool EnableEDisclose;
        public readonly bool EnableESign;
        public readonly bool EnableEClose;
        public readonly bool EnableManualFulfillment;
        public readonly bool EnableEmailDocuments;
        public readonly bool EnableEmailPasswords;
        public readonly bool EnableEmailRetrievalNotification;
        public readonly bool EnableIndividualDocumentGeneration;
        public readonly bool HideDisabledOptions;

        public Permissions(PermissionsList perms, Package package)
        {
            EnableMERSRegistration = false;
            if (perms.EnableMERSRegistrationSpecified)
            {
                EnableMERSRegistration = perms.EnableMERSRegistration == PermissionsListEnableMERSRegistration.Y;
            }

            EnableEDisclose = false;
            if (perms.EnableEDiscloseSpecified)
            {
                EnableEDisclose = perms.EnableEDisclose == PermissionsListEnableEDisclose.Y;
            }

            EnableESign = false;
            if (perms.EnableESignSpecified)
            {
                EnableESign = perms.EnableESign == PermissionsListEnableESign.Y;
            }

            EnableEClose = false;
            if (perms.EnableECloseSpecified)
            {
                EnableEClose = perms.EnableEClose == PermissionsListEnableEClose.Y;
            }

            EnableManualFulfillment = false;
            if (perms.EnableManualFulfillmentSpecified)
            {
                EnableManualFulfillment = perms.EnableManualFulfillment == PermissionsListEnableManualFulfillment.Y;
            }

            EnableEmailDocuments = false;
            if (perms.EnableEmailDocumentsSpecified)
            {
                EnableEmailDocuments = perms.EnableEmailDocuments == PermissionsListEnableEmailDocuments.Y;
            }

            EnableEmailPasswords = false;
            if (perms.EnableEmailPasswordsSpecified)
            {
                EnableEmailPasswords = perms.EnableEmailPasswords == PermissionsListEnableEmailPasswords.Y;
            }

            EnableEmailRetrievalNotification = false;
            if (perms.EnableEmailRetrievalNotificationSpecified)
            {
                EnableEmailRetrievalNotification = perms.EnableEmailRetrievalNotification == PermissionsListEnableEmailRetrievalNotification.Y;
            }

            EnableIndividualDocumentGeneration = false;
            if (perms.EnableIndividualDocumentGenerationSpecified)
            {
                EnableIndividualDocumentGeneration = perms.EnableIndividualDocumentGeneration == PermissionsListEnableIndividualDocumentGeneration.Y;
            }

            // Generally, if the user doesn't have the permission we don't care what the package says, they can't do it.
            if (this.EnableEDisclose && package.IsEDisclosableSpecified)
            {
                this.EnableEDisclose = package.IsEDisclosable == PackageIsEDisclosable.Y;
            }

            if (this.EnableESign && package.IsEsignableSpecified)
            {
                this.EnableESign = package.IsEsignable == PackageIsEsignable.Y;
            }

            if (this.EnableEClose && package.IsEClosableSpecified)
            {
                this.EnableEClose = package.IsEClosable == PackageIsEClosable.Y;
            }

            if (this.EnableManualFulfillment && package.EnableFulfillmentSpecified)
            {
                this.EnableManualFulfillment = package.EnableFulfillment == PackageEnableFulfillment.Y;
            }

            HideDisabledOptions = true;
        }

        public Permissions(bool enableMERS, bool enableEDisclose, bool enableESign, bool enableEClose, bool enableEMail)
        {
            EnableMERSRegistration = enableMERS;
            EnableEDisclose = enableEDisclose;
            EnableESign = enableESign;
            EnableEClose = enableEClose;
            
            EnableEmailDocuments = enableEMail;
            EnableEmailPasswords = enableEMail;
            EnableEmailRetrievalNotification = enableEMail;

            EnableIndividualDocumentGeneration = false;
            EnableManualFulfillment = true;
            HideDisabledOptions = false;
        }
    }

    public struct FormInfo
    {
        public readonly string BorrowerNumber;
        public readonly string Name;
        public readonly string ID;
        public readonly string Description;

        public FormInfo(string borrowerNum, string formName, string formId, string formDesc)
        {
            BorrowerNumber = borrowerNum;
            Name = formName;
            ID = formId;
            Description = formDesc;
        }

        public FormInfo(string borrowerNumber, string id)
        {
            BorrowerNumber = borrowerNumber;
            ID = id;
            Name = null;
            Description = null;
        }
    }

    public class Audit
    {
        public readonly AuditLine[] Results;
        public readonly Uri FurtherInfo;
        public readonly Uri DocumentPortal;

        public Audit() { }

        public Audit(IEnumerable<AuditLine> Messages)
        {
            Results = Messages.ToArray();
        }

        public Audit(IEnumerable<AuditLine> Messages, string furtherInfoUrl, string portalUrl)
        {
            FurtherInfo = furtherInfoUrl.ToUriOrNull();
            DocumentPortal = portalUrl.ToUriOrNull();

            Results = Messages.ToArray();
        }
    }

    public struct AuditLine
    {
        public readonly string Message;
        public readonly AuditSeverity Severity;
        public readonly string AuditDetails;
        public readonly string EditPage;
        public readonly string FieldId;
        public readonly Guid LoanId;
        public readonly Guid AppId;

        public AuditLine(string message, AuditSeverity severity, string editpage, string fieldid, string details, Guid loanid, Guid appid)
        {
            Message = message;
            Severity = severity;
            EditPage = editpage;
            FieldId = fieldid;
            LoanId = loanid;
            AppId = appid;
            AuditDetails = details;
        }

        public static AuditLine CreateFromDocMagicMessage(Message msg, Guid loanid, Guid appid)
        {
            var message = msg.InnerText;
            var severity = msg.Type.ToAuditSeverity();
            var XPathMapping = DocMagicXpathMapping.Retrieve(msg.MismoRef.Trim('"'));

            string fieldid = null;
            string editpage = null;

            if (XPathMapping != null)
            {
                fieldid = XPathMapping.FieldId;
                editpage = XPathMapping.DefaultURL;
            }

            return new AuditLine(message, severity, editpage, fieldid, "", loanid, appid);
        }
    }

    public struct PlanCode
    {
        public readonly string Code;
        public readonly string Description;
        public readonly string Investor;

        public PlanCode(string desc, string code, string investor)
        {
            Code = code;
            Description = desc;
            Investor = investor;
        }
    }

    public struct InvestorInfo
    {
        public readonly string Code;
        public readonly string Description;

        public InvestorInfo(string desc, string code)
        {
            Code = code;
            Description = desc;
        }
    }

    public struct DocumentPackage
    {
        public readonly bool DisableESign;
        public readonly bool AllowEPortal;
        public readonly bool AllowEClose;
        public readonly bool AllowManualFulfillment;
        public readonly LqbGrammar.DataTypes.DocumentIntegrationPackageName Description;
        public readonly string Type;

        public DocumentPackage(LqbGrammar.DataTypes.DocumentIntegrationPackageName desc, string type, bool disableSign, bool allowPortal, bool allowEClose)
        {
            AllowManualFulfillment = true;
            DisableESign = disableSign;
            AllowEPortal = allowPortal;
            AllowEClose = allowEClose;
            Description = desc;
            Type = type;
        }

        public DocumentPackage(Package p, bool vendorIsDocMagic = false)
        {
            // For DocMagic only, AllowEPortal should default to true.
            AllowEPortal = vendorIsDocMagic;
            DisableESign = false;
            AllowEClose = false;
            AllowManualFulfillment = false;

            Description = LqbGrammar.DataTypes.DocumentIntegrationPackageName.Create(p.PackageName).ForceValue();
            Type = p.PackageID;

            if (p.IsEDisclosableSpecified)
            {
                AllowEPortal = p.IsEDisclosable == PackageIsEDisclosable.Y;
            }
            if (p.IsEsignableSpecified)
            {
                //Really need to flip this at some point, all the logic is backwards...
                DisableESign = !(p.IsEsignable == PackageIsEsignable.Y);
            }
            if (p.IsEClosableSpecified)
            {
                AllowEClose = p.IsEClosable == PackageIsEClosable.Y;
            }
            if (p.EnableFulfillmentSpecified)
            {
                AllowManualFulfillment = p.EnableFulfillment == PackageEnableFulfillment.Y;
            }
        }
    }

    public class DocumentVendorTransaction
    {
        public static DocumentVendorTransaction<REQ, RESP> Create<REQ, RESP>(REQ request, RESP response, string err, bool ignoreError)
            where REQ : new()
            where RESP : new()
        {
            return new DocumentVendorTransaction<REQ, RESP>(request, response, err, ignoreError);
        }
    }

    public class DocumentVendorTransaction<REQ, RESP>
        where REQ: new() 
        where RESP : new()
    {
        public readonly REQ Request;
        public readonly RESP Response;
        public readonly string Error;
        private bool m_ignoreError; //Transaction has no error, but has error (warning) messages to relay
        public bool HasError
        {
            get
            {
                return Error != null && !m_ignoreError;
            }
        }
        public DocumentVendorTransaction(REQ req, RESP resp, string err, bool ignoreError)
        {
            Request = req;
            Response = resp;
            Error = err;
            m_ignoreError = ignoreError;
        }

        private static string XmlToFormattedString(REQ req)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(REQ));
            using (StringWriter sw = new StringWriter())
            {
                serializer.Serialize(sw, req);
                return sw.ToString().FormatXmlString();
            }
        }

        private static string XmlToFormattedString(RESP resp)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(RESP));
            using (StringWriter sw = new StringWriter())
            {
                serializer.Serialize(sw, resp);
                return sw.ToString().FormatXmlString();
            }
        }

        public string GenerateLog()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Request: ");
            sb.AppendLine(XmlToFormattedString(Request));
            sb.AppendLine();
            sb.AppendLine("Response: ");
            sb.AppendLine(XmlToFormattedString(Response));

            return sb.ToString();
        }
    }

    public class DocumentVendorResult
    {
        public static DocumentVendorResult<T> Success<T>(T result)
        {
            return new DocumentVendorResult<T>(result, null);
        }

        public static DocumentVendorResult<T> Error<T>(string error, T typeExample)
        {
            Tools.LogWarning("Configured document vendor transaction encountered the following error: " + error);
            return new DocumentVendorResult<T>(default(T), error);
        }

        public static DocumentVendorResult<T> ValidationError<T>(string error, T typeExample)
        {
            DocumentVendorResult<T> ret = new DocumentVendorResult<T>(default(T), error);
            ret.IsValidationError = true;
            return ret;
        }
    }

    public class DocumentVendorResult<T>
    {
        public readonly T Result;
        public bool HasError
        {
            get
            {
                return !string.IsNullOrEmpty(ErrorMessage);
            }
        }

        public readonly string ErrorMessage;
        public bool IsValidationError { get; set; }
        public string TempFileDbKey { get; set; }
        public string TransactionId { get; set; }
        public Guid UcdDocumentId { get; set; } = Guid.Empty;
        public DocumentVendorResult(T result, string errorMsg)
        {
            Result = result;
            ErrorMessage = errorMsg;
        }
    }
}