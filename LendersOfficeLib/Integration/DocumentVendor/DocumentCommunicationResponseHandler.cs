﻿namespace LendersOffice.Integration.DocumentVendor
{
    using System.Web;
    using LendersOffice.Integration.VendorCommunication;
    using LqbCommunication;

    /// <summary>
    /// A version of the communication response handler for the document framework.
    /// </summary>
    public class DocumentCommunicationResponseHandler : CommunicationResponseHandler
    {
        /// <summary>
        /// The log header for a response to an asynchronous post from a document vendor.
        /// </summary>
        private const string ResponseHeader = "=== DOCUMENT FRAMEWORK ASYNC RESPONSE ===";

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentCommunicationResponseHandler"/> class.
        /// </summary>
        /// <param name="httpResponse">The HTTP context to populate with a response.</param>
        public DocumentCommunicationResponseHandler(HttpResponse httpResponse)
            : base(httpResponse)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentCommunicationResponseHandler"/> class.
        /// </summary>
        /// <param name="httpResponse">The HTTP context to populate with a response.</param>
        /// <param name="status">The status of the response.</param>
        /// <param name="message">A supplementary message.</param>
        public DocumentCommunicationResponseHandler(HttpResponse httpResponse, Status status, string message = null)
            : base(httpResponse, status, message)
        {
        }

        /// <summary>
        /// Logs a response to an async vendor POST.
        /// </summary>
        /// <param name="response">The response string.</param>
        protected override void LogResponse(string response)
        {
            CommunicationLogs.LogPostResponse(response, ResponseHeader);
        }
    }
}
