﻿namespace LendersOffice.Integration.DocumentVendor
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Integration.DocumentVendor.LQBDocumentRequest;
    using LendersOffice.Integration.DocumentVendor.LQBDocumentResponse;
    using LendersOffice.Security;
    using DocRequest = LendersOffice.Integration.DocumentVendor.LQBDocumentRequest.LQBDocumentRequest;
    using DocResponse = LendersOffice.Integration.DocumentVendor.LQBDocumentResponse.LQBDocumentResponse;

    public class ConfiguredDocumentGenerationRequest : IDocumentGenerationRequest
    {
        private readonly Guid AppId;
        private readonly Guid LoanId;
        private readonly Guid BrokerId;
        private DocumentVendorServer server;
        private DocumentRequest docs = new DocumentRequest();

        internal ConfiguredDocumentGenerationRequest(Guid appId, Guid loanid, Guid brokerId, DocumentVendorServer s)
        {
            AppId = appId;
            BrokerId = brokerId;
            LoanId = loanid;
            server = s;
            this.IndividualForms = new List<FormInfo>();
        }

        public void SetEsignNotificationEmail(string email)
        {
            if (!string.IsNullOrEmpty(email))
            {
                docs.ESignNotification = new ESignNotification[] { new ESignNotification() { Email = email } };
            }
        }

        private void PopulateForms()
        {
            if (this.IndividualForms.Any())
            {
                docs.Form = (from f in this.IndividualForms
                             select new LendersOffice.Integration.DocumentVendor.LQBDocumentRequest.Form()
                             {
                                 FormID = f.ID
                             }).ToArray();
            }
        }

        public List<FormInfo> IndividualForms { get; set; }

        public string DocumentFormat
        {
            get
            {
                return Enum.GetName(typeof(DocumentRequestDocumentFormat), docs.DocumentFormat);
            }
            set
            {
                docs.DocumentFormat = (DocumentRequestDocumentFormat)Enum.Parse(typeof(DocumentRequestDocumentFormat), value);
            }
        }

        public void EmailDocumentsTo(string email, string requiredPassword, bool notifyOnRetrieval, string hint)
        {
            var emailTo = new EmailDocs();
            emailTo.EmailAddress = email;
            
            if(!string.IsNullOrEmpty(requiredPassword.TrimWhitespaceAndBOM()))
            {
                emailTo.RequirePassword = EmailDocsRequirePassword.Y;
                emailTo.RequirePasswordSpecified = true;

                emailTo.PasswordInfo = new PasswordInfo();
                emailTo.PasswordInfo.Password = requiredPassword.TrimWhitespaceAndBOM();
                emailTo.PasswordInfo.PasswordHint = hint;
            }

            docs.EmailDocs = new[] { emailTo };
            docs.EmailDocuments = DocumentRequestEmailDocuments.Y;
            docs.EmailDocumentsSpecified = true;
        }

        public bool IsEmailDocumentsSelected
        {
            get { return this.docs.EmailDocumentsSpecified && this.docs.EmailDocuments == DocumentRequestEmailDocuments.Y; }
        }

        public void EnableDsiPrintAndDeliver(string attention, string name, string phone, string notes, string street, string city, string state, string zip)
        {
            var delivery = new DeliveryInformation();

            delivery.Name = name;
            delivery.Phone = phone;
            delivery.Notes = notes;
            delivery.Street = street;
            delivery.City = city;
            delivery.State = state;
            delivery.Zip = zip;

            docs.ManualFulfillment = DocumentRequestManualFulfillment.Y;
            docs.ManualFulfillmentSpecified = true;

            docs.DeliveryInformation = delivery;
        }

        public bool IsManualFulfillment
        {
            get { return this.docs.ManualFulfillmentSpecified; }
        }

        public bool IsMersRegistrationEnabled
        {
            get
            {
                return docs.MERSRegistration == DocumentRequestMERSRegistration.Y;
            }
            set
            {
                docs.MERSRegistration = value ? DocumentRequestMERSRegistration.Y : DocumentRequestMERSRegistration.N;
                docs.MERSRegistrationSpecified = true;
            }
        }

        public string PackageType
        {
            get
            {
                return docs.PackageID;
            }
            set
            {
                docs.PackageID = value;
            }
        }

        public LqbGrammar.DataTypes.DocumentIntegrationPackageName PackageName { get; set; }

        public string LoanProgramId
        {
            get
            {
                return docs.LoanProgramID;
            }
            set
            {
                docs.LoanProgramID = value;
            }
        }

        public DocumentVendorResult<IDocumentGenerationResult> RequestDocuments(bool isPreviewOrder, AbstractUserPrincipal principal)
        {
            DocRequest request = new DocRequest();
            request.Request = new Request();
            request.Request.RequestType = RequestRequestType.RequestDocuments;
            request.Request.Item = docs;
            request.Request.Preview = isPreviewOrder ? RequestPreview.Y : RequestPreview.N;
            request.Request.PostbackUrl = Constants.ConstStage.DocFrameworkPostbackUrl;

            this.PopulateForms();

            var response = server.Submit(request, LoanId, principal, isPreviewOrder);
            return CreateFromResponse(response, isPreviewOrder);
        }

        private DocumentVendorResult<IDocumentGenerationResult> CreateFromResponse(DocumentVendorTransaction<DocRequest, DocResponse> response, bool isPreviewOrder = false)
        {
            IDocumentGenerationResult example = null;
            if (response.HasError)
            {
                return DocumentVendorResult.Error(response.Error, example);
            }

            var rawResult = response.Response;

            // OPM 247161 - Support UCDs and multiple DocumentRespose objects.
            if (rawResult.Items?[0] as DocumentResponse == null)
            {
                return DocumentVendorResult.Error("Got the wrong kind of response", example);
            }

            // 10/23/2015 BS - Case 229862. If vendor is DocMagic and eSign is enabled, then we add entry to pending eSign tracker
            if (this.server.Config.VendorName.Contains("DocMagic") && this.docs.EnableESignSpecified && this.docs.EnableESign == DocumentRequestEnableESign.Y)
            {
                TrackESignRequest(response.Request.Authentication);
            }

            bool isManualFulfillment = this.docs.ManualFulfillmentSpecified && this.docs.ManualFulfillment == DocumentRequestManualFulfillment.Y;
            bool isEDisclosure = this.docs.EDiscloseSpecified && this.docs.EDisclose == DocumentRequestEDisclose.Y;
            return ConfiguredDocumentGenerationResult.Create(this.PackageType, this.PackageName, this.LoanId, this.BrokerId, this.AppId, this.server.Config, rawResult.TransactionID, rawResult.Items, rawResult.DataValidation, isPreviewOrder);
        }

        public void SendEDisclosures(bool allowSigning)
        {
            docs.EDisclose = DocumentRequestEDisclose.Y;
            docs.EDiscloseSpecified = true;

            docs.EnableESign = allowSigning ? DocumentRequestEnableESign.Y : DocumentRequestEnableESign.N;
            docs.EnableESignSpecified = true;
        }

        public void SetEClose(bool eCloseEnabled)
        {
            docs.EClose = eCloseEnabled ? DocumentRequestEClose.Y : DocumentRequestEClose.N;
            docs.ECloseSpecified = true;
        }

        public bool IsSendEDisclosures
        {
            get { return this.docs.EDiscloseSpecified && this.docs.EDisclose == DocumentRequestEDisclose.Y; }
        }

        public bool IsEClosing
        {
            get { return this.docs.ECloseSpecified && this.docs.EClose == DocumentRequestEClose.Y; }
        }

        public bool IsESignEnabled
        {
            get { return this.docs.EnableESignSpecified && this.docs.EnableESign == DocumentRequestEnableESign.Y; }
        }

        // 10/23/2015 BS - Case 229862. Add DocMagic eDisclosure request to pending esign request tracker
        private void TrackESignRequest(Authentication authenticationFields)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@DocMagicAccountNumber", authenticationFields.AccountID),
                                            new SqlParameter("@sLId", LoanId),
                                            new SqlParameter("@sLNm", LoanNm(LoanId)),
                                            new SqlParameter("@EmployeeId", PrincipalFactory.CurrentPrincipal.EmployeeId),
                                            new SqlParameter("@BrokerId", BrokerId),
                                            new SqlParameter("@Username", authenticationFields.UserName),
                                            new SqlParameter("@Password", EncryptionHelper.Encrypt(authenticationFields.Password))
                                        };

            StoredProcedureHelper.ExecuteNonQuery(BrokerId, "DocMagic_TrackESignRequest", 3, parameters);
        }

        // 10/23/2015 BS - Case 229862. Get loanNm from loadId
        private string LoanNm (Guid loanId)
        {
            string templateName = "";
            SqlParameter[] parameters = { new SqlParameter("@LoanID", loanId) };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerId, "GetLoanNumberByLoanID", parameters))
            {
                if (reader.Read())
                {
                    templateName = (string)reader["sLNm"];
                }
            }
            return templateName;
        }
    }
}
