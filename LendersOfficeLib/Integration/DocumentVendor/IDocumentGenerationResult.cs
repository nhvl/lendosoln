﻿namespace LendersOffice.Integration.DocumentVendor
{
    using LendersOffice.Common.SerializationTypes;
    using LQBDocumentResponse;

    public interface IDocumentGenerationResult
    {
        bool DocsAreForDownload { get; }
        bool DocsAreForViewing { get; }
        string DownloadUrl { get; }
        string PackageType { get; }
        LqbGrammar.DataTypes.DocumentIntegrationPackageName PackageName { get; }
        string PdfFileDbKey { get; }
        bool IsDisclosurePackage { get; }
        bool IsInitialDisclosurePackage { get; }
        bool IsClosingPackage { get; }
        string ViewUrl { get; }
        bool CreatedArchiveInUnknownStatus { get; set; }
        ClosingCostArchive.E_ClosingCostArchiveType? UnknownArchiveType { get; set; }
        string UnknownArchiveDate { get; set; }
        bool UnknownArchiveWasSourcedFromPendingCoC { get; set; }
        bool HasBothCDAndLEArchiveInUnknownStatus { get; set; }
        string ErrorMessage { get; }
        decimal? DocVendorApr { get; }
        string DocCode { get; }
        decimal? DocVendorLateChargePercent { get; }
        VendorProvidedDisclosureMetadata VendorProvidedDisclosureMetadata { get; }
    }
}
