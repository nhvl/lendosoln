@echo off
echo Note: LQBDocumentRequest automatically pulls MISMOClosing into itself when you generate it. 
echo       If you regenerate LQBDocumentRequest, you'll have to go in and remove the MISMO stuff.
for /r %%i in (*.xsd) do xsd "%%i" /namespace:LendersOffice.Integration.DocumentVendor.%%~ni /nologo /c

echo If you are generating code and the attributes are using an older version of generation, try 
echo using the fully qualified path directly, e.g.
echo    "C:\Program Files (x86)\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.6 Tools\xsd.exe"

pause
REM If you get "'xsd' is not recognized as an internal or external command, operable program or batch file"
REM     locate xsd.exe and add the containing folder to the environment variables' paths list (semicolon-separated list).