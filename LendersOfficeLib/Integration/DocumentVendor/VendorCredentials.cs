﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;
using LendersOffice.Admin;
using LendersOffice.Security;

namespace LendersOffice.Integration.DocumentVendor
{
    public class VendorCredentials : IDocumentVendorCredentials
    {

        private string m_encryptedPassword = null;
        private Guid UserId;
        private Guid VendorId;
        private Guid BrokerId;

        public bool UserEnabled { get; private set; }
        public string CustomerId { get; set; }
        public string UserName { get; set; }
        public string Password
        {
            get
            {
                try { return EncryptionHelper.Decrypt(m_encryptedPassword); }
                catch (FormatException)
                {
                    Tools.LogBug("User had a bad encrypted password, probably from setting too long a password.");
                    return "";
                }
            }
            set
            {
                if (value == ConstAppDavid.FakePasswordDisplay)
                {
                    throw new ArgumentException($"Can't assign a password of {ConstAppDavid.FakePasswordDisplay}.");
                }
                string tmp = EncryptionHelper.Encrypt(value);
                if (tmp.Length > 150)
                {
                    throw new ArgumentException("Can't assign a password such that its encrypted form is longer than 150 characters.");
                }
                m_encryptedPassword = tmp;
            }
        }
        public static VendorCredentials GetUserCredentialsOnly(Guid userId, Guid brokerId, Guid vendorId)
        {
            return new VendorCredentials(userId, brokerId, vendorId);
        }
        private VendorCredentials(Guid userId, Guid brokerId, Guid vendorId)
        {
            var userOnly = true;
            Init(userId, Guid.Empty, brokerId, vendorId, userOnly);
        }

        //For internal users only, doesn't actually have any credentials so they'll probably get an error if they try to actually request anything.
        private VendorCredentials() { }

        internal static VendorCredentials GetInternalUserCredentials()
        {
            return new VendorCredentials();
        }

        internal static VendorCredentials GetBrokerLevelCredentials(Guid brokerId, Guid vendorId)
        {
            return new VendorCredentials(Guid.Empty, Guid.Empty, brokerId, vendorId);
        }

        public VendorCredentials(Guid userId, Guid branchId, Guid brokerId, Guid vendorId)
        {
            var userOnly = false;
            Init(userId, branchId, brokerId, vendorId, userOnly);
        }

        private void Init(Guid userId, Guid branchId, Guid brokerId, Guid vendorId, bool userOnly)
        {
            this.UserId = userId;
            this.VendorId = vendorId;
            this.BrokerId = brokerId;

            var ds = StoredProcedureHelper.ExecuteDataSet(brokerId, "DOCUMENT_VENDOR_GetCredentialLevelsNew", new[] {
                new SqlParameter("@UserId", userId),
                new SqlParameter("@BranchId", branchId),
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@VendorId", vendorId)
            });
            var toCheck = new[]
            {
                ds.Tables[0], //user
                ds.Tables[1], //branch
                ds.Tables[2]  //broker
            };

            if (userOnly)
            {
                toCheck = new[] { 
                    ds.Tables[0], //user
                };
            }

            this.UserEnabled = ds.Tables[0].Rows.Count > 0;

            if (vendorId != Guid.Empty)
            {
                var config = VendorConfig.Retrieve(vendorId);
                foreach (var table in toCheck)
                {
                    if (AssignCredentials(table, config)) return;
                }
            }

            UserName = null;
            Password = null;
            CustomerId = null;
        }

        private bool AssignCredentials(DataTable dt, VendorConfig c)
        {
            if (dt.Rows.Count == 0) return false;

            var row = dt.Rows[0];
            string username = Convert.ToString(row["DocVendorUserName"]);
            string password = Convert.ToString(row["DocVendorPassword"]);
            string id = Convert.ToString(row["DocVendorCustomerId"]);

            //If we don't have any of the used entries available, don't use this account.
            if ((!c.UsesAccountId || string.IsNullOrEmpty(id)) &&
                (!c.UsesPassword || (string.IsNullOrEmpty(password) || string.IsNullOrEmpty(EncryptionHelper.Decrypt(password)))) &&
                (!c.UsesUsername || string.IsNullOrEmpty(username)))
            {
                return false;
            }

            if (c.UsesUsername) UserName = username;
            if (c.UsesPassword) m_encryptedPassword = password;
            if (c.UsesAccountId) CustomerId = id;

            return true;
        }

        public void SaveUserCredentials()
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@UserId", UserId),
                new SqlParameter("@VendorId", VendorId),
                new SqlParameter("@BrokerId", BrokerId),
                new SqlParameter("@Login", UserName),
                new SqlParameter("@AccountId", CustomerId)
            };

            if (!string.IsNullOrEmpty(m_encryptedPassword))
            {
                parameters.Add(new SqlParameter("@Password", m_encryptedPassword));
            }
            StoredProcedureHelper.ExecuteNonQuery(BrokerId, "DOCUMENT_VENDOR_UpdateUserCredentialsNew", 3,
                parameters);
        }

        public static void DisableForUser(Guid brokerId, Guid userId, Guid vendorId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", userId),
                                            new SqlParameter("@VendorId", vendorId)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "DOCUMENT_VENDOR_DisableUserCredentialsNew", 3, parameters);
        }
    }
}
