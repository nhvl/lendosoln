﻿using System;
namespace LendersOffice.Integration.DocumentVendor
{
    public interface IDocumentVendorCredentials
    {
        string CustomerId { get; set; }
        string Password { get; set; }
        string UserName { get; set; }
        bool UserEnabled { get; }

        void SaveUserCredentials();
    }
}
