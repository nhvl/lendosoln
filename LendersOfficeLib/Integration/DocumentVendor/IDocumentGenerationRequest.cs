namespace LendersOffice.Integration.DocumentVendor
{
    using System.Collections.Generic;
    using LqbGrammar.DataTypes;
    using Security;

    public interface IDocumentGenerationRequest
    {
        List<FormInfo> IndividualForms { get; set; }
        string DocumentFormat { get; set; }
        string LoanProgramId { get; set; }
        void EmailDocumentsTo(string email, string requiredPassword, bool notifyOnRetrieval, string hint);
        bool IsEmailDocumentsSelected { get; }
        void EnableDsiPrintAndDeliver(string attention, string name, string phone, string notes, string street, string city, string state, string zip);
        bool IsMersRegistrationEnabled { get; set; }
        string PackageType { get; set; }
        DocumentIntegrationPackageName PackageName { get; set; }
        DocumentVendorResult<IDocumentGenerationResult> RequestDocuments(bool isPreviewOrder, AbstractUserPrincipal principal);
        void SendEDisclosures(bool allowSigning);
        void SetEClose(bool eCloseEnabled);
        bool IsSendEDisclosures { get; }
        bool IsEClosing { get; }
        bool IsESignEnabled { get; }
        bool IsManualFulfillment { get; }
    }
}
