﻿namespace LendersOffice.Integration.DocumentVendor
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Runtime.Serialization;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using System.Linq;
    using LqbGrammar.DataTypes;

    [DataContractAttribute]
    [Serializable]
    public class DocumentVendorBrokerSettings
    {
        [DataMemberAttribute]
        public Guid BrokerId { get; set; }

        [DataMemberAttribute]
        public Guid VendorId { get; set; }

        [DataMemberAttribute]
        public string Login { get; set; }

        [DataMemberAttribute]
        public string AccountId { get; set; }

        private string m_password = string.Empty;

        [DataMemberAttribute(Order = 1)]
        //Read/Write the old password first
        public string EPassword
        {
            get //Use GetPassword instead for usable password
            {
                return m_password;
            }
            set
            {
                m_password = value;
            }
        }

        [DataMemberAttribute(Order = 2)]
        //Overwrite the old password
        public string Password
        {
            get //Use GetPassword instead for usable password
            {
                return string.Empty;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    m_password = EncryptionHelper.Encrypt(value);
                }
            }
        }

        [DataMemberAttribute]
        public string ServicesLinkNm { get; set; }

        [DataMemberAttribute]
        public bool DisclosureInTest { get; set; }

        [DataMemberAttribute]
        public bool DisclosureBilling { get; set; }

        [DataMemberAttribute]
        public bool DisclosureOnlineInterface { get; set; }

        // OPM 223227
        [DataMemberAttribute]
        public bool IsEnableForProductionLoans { get; set; }

        [DataMemberAttribute]
        public bool IsEnableForTestLoans { get; set; }

        /// <summary>
        /// Indicates whether the Manual Fulfillment option should be disabled for this vendor.
        /// </summary>
        [DataMemberAttribute]
        public bool BlockManualFulfillment { get; set; }

        /// <summary>
        /// Indicates whether the broker uses custom package data rather than the vendor's stock packages.
        /// </summary>
        [DataMemberAttribute]
        public bool HasCustomPackageData { get; set; }

        /// <summary>
        /// Custom document vendor package data in JSON format.
        /// </summary>
        [DataMemberAttribute]
        public string CustomPackageDataJSON { get; set; }

        /// <summary>
        /// Enables appraisal delivery.
        /// </summary>
        public bool EnableAppraisalDelivery { get; set; }

        private DocumentVendorBrokerSettings(DbDataReader reader)
        {
            BrokerId = (Guid)reader["BrokerId"];
            VendorId = (Guid)reader["VendorId"];
            Login = reader["Login"].ToString();
            EPassword = reader["Password"].ToString();
            AccountId = reader["AccountId"].ToString();
            ServicesLinkNm = reader["ServicesLinkNm"].ToString();
            DisclosureInTest = (bool)reader["DisclosureInTest"];
            DisclosureBilling = (bool)reader["DisclosureBilling"];
            DisclosureOnlineInterface = (bool)reader["DisclosureOnlineInterface"];
            IsEnableForProductionLoans = (bool)reader["IsEnableForProductionLoans"];
            IsEnableForTestLoans = (bool)reader["IsEnableForTestLoans"];
            BlockManualFulfillment = (bool)reader["BlockManualFulfillment"];
            HasCustomPackageData = (bool)reader["HasCustomPackageData"];
            CustomPackageDataJSON = reader["CustomPackageDataJSON"].ToString();
            this.EnableAppraisalDelivery = reader.AsNullableBool("EnableAppraisalDelivery") ?? false;
        }

        public DocumentVendorBrokerSettings()
        {
        }

        public Sensitive<string> GetPassword()
        {
            return new Sensitive<string>(EncryptionHelper.Decrypt(m_password));
        }

        /// <summary>
        /// Checks whether the current broker has manual fulfillment disabled for the given vendor.
        /// </summary>
        /// <param name="vendorId">The vendor to validate.</param>
        /// <returns>True if manual fulfillment should be blocked, false otherwise.</returns>
        public static bool BlockManualFulfillmentForVendor(Guid vendorId)
        {
            var vendorSettings = ListAllForBroker(PrincipalFactory.CurrentPrincipal.BrokerId).FirstOrDefault(v => v.VendorId == vendorId);
            return vendorSettings == null ? false : vendorSettings.BlockManualFulfillment;
        }

        /// <summary>
        /// Returns list containing each document vendor enabled for this broker
        /// </summary>
        public static List<DocumentVendorBrokerSettings> ListAllForBroker(Guid BrokerId)
        {
            var list = new List<DocumentVendorBrokerSettings>();
            if (BrokerDB.RetrieveById(BrokerId).BillingVersion != E_BrokerBillingVersion.PerTransaction)
            {
                //No vendors if broker is not on PTM model
                return list;
            }
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(BrokerId, "DocumentVendorBrokerSettings_ListByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    list.Add(new DocumentVendorBrokerSettings(reader));
                }
            }

            return list;
        }

        public static void SaveAllForBroker(Guid BrokerId, List<DocumentVendorBrokerSettings> list, CStoredProcedureExec spExec)
        {
            spExec.ExecuteNonQuery("DocumentVendorBrokerSettings_Clear", 3, new SqlParameter("@BrokerId", BrokerId));
            int index = 0;
            foreach (DocumentVendorBrokerSettings item in list)
            {
                spExec.ExecuteNonQuery("DocumentVendorBrokerSettings_Save", 3,
                    new SqlParameter("@BrokerId", BrokerId),
                    new SqlParameter("@VendorId", item.VendorId),
                    new SqlParameter("@Login", item.Login),
                    new SqlParameter("@Password", item.EPassword),
                    new SqlParameter("@AccountId", item.AccountId),
                    new SqlParameter("@ServicesLinkNm", item.ServicesLinkNm),
                    new SqlParameter("@DisclosureInTest", item.DisclosureInTest),
                    new SqlParameter("@DisclosureBilling", item.DisclosureBilling),
                    new SqlParameter("@DisclosureOnlineInterface", item.DisclosureOnlineInterface),
                    new SqlParameter("@VendorIndex", index++),
                    new SqlParameter("@IsEnableForProductionLoans", item.IsEnableForProductionLoans),
                    new SqlParameter("@IsEnableForTestLoans", item.IsEnableForTestLoans),
                    new SqlParameter("@BlockManualFulfillment", item.BlockManualFulfillment),
                    new SqlParameter("@HasCustomPackageData", item.HasCustomPackageData),
                    new SqlParameter("@CustomPackageDataJSON", item.CustomPackageDataJSON),
                    new SqlParameter("@EnableAppraisalDelivery", item.EnableAppraisalDelivery)
                    );
            }
        }
        public static DocumentVendorBrokerSettings GetDocMagicSettings(Guid BrokerId)
        {

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId)
                                        };

            DocumentVendorBrokerSettings brokerSettings = null;

            using (var reader = StoredProcedureHelper.ExecuteReader(BrokerId, "DocumentVendorBrokerSettings_GetDocMagicSettingsByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    if (brokerSettings == null)
                    {
                        brokerSettings = new DocumentVendorBrokerSettings(reader);
                    }
                    else
                    {
                        Tools.LogErrorWithCriticalTracking(string.Format("Broker {0} had more than 1 Guid.Empty (DocMagic) vendor.  Using the one with lowest vendorIndex", BrokerId));
                    }
                }
            }

            if (brokerSettings == null)
            {
                brokerSettings = new DocumentVendorBrokerSettings();
            }

            return brokerSettings;
        }

        public static bool IsTest(Guid VendorId, Guid BrokerId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(BrokerId, "DocumentVendorBrokerSettings_ListByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    var settings = new DocumentVendorBrokerSettings(reader);

                    if (settings.VendorId == VendorId)
                    {
                        return settings.DisclosureInTest;
                    }
                }
            }

            return true; //shouldn't happen, but safer default
        }
    }

}
