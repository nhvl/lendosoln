﻿namespace LendersOffice.Integration.DocumentVendor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ConfigSystem;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Integration.DocumentVendor.LQBDocumentRequest;
    using LendersOffice.Integration.DocumentVendor.LQBDocumentResponse;
    using LendersOffice.ObjLib.DocMagicLib;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using DocRequest = LQBDocumentRequest.LQBDocumentRequest;
    using DocResponse = LQBDocumentResponse.LQBDocumentResponse;

    public class ConfiguredDocumentVendor : IDocumentVendor
    {
        private DocumentVendorServer Server;

        public ConfiguredDocumentVendor(VendorConfig config, Guid brokerId)
            : this(config, GetCredentials(PrincipalFactory.CurrentPrincipal, config.VendorId), brokerId)
        {
        }

        public ConfiguredDocumentVendor(VendorConfig config, Guid brokerId, string userName, string password, string accountId)
            : this(config, GetCredentials(null, config.VendorId), brokerId)
        {
            this.Credentials.UserName = userName;
            this.Credentials.Password = password;
            this.Credentials.CustomerId = accountId;
        }

        public ConfiguredDocumentVendor(VendorConfig config, VendorCredentials credentials, Guid brokerId)
        {
            this.Config = config;
            this.Credentials = credentials;
            this.BrokerId = brokerId;
            this.Server = new DocumentVendorServer(this.Config, this.BrokerId, credentials);
            this.skin = new DVSkin(config.VendorName, "Loan Program", "Manual Fulfillment", false, false, false, true, false);
        }

        private static VendorCredentials GetCredentials(AbstractUserPrincipal user, Guid VendorId)
        {
            if (user == null || user is InternalUserPrincipal || user is SystemUserPrincipal)
            {
                //Since we don't have a valid user or branch, we can't really get credentials
                return VendorCredentials.GetInternalUserCredentials();
            }
            else
            {
                return new VendorCredentials(user.UserId, user.BranchId, user.BrokerId, VendorId);
            }
        }

        public bool CanGenerateDocuments(AbstractUserPrincipal principal, Guid loanId, string packageId, out string failureReasons)
        {
            failureReasons = string.Empty;

            // No need to run workflow on system user principals. System has permission to generate any docs.
            if (principal is SystemUserPrincipal)
            {
                return true;
            }

            VendorConfig.DocumentPackage package = this.Config.RetrievePackage(packageId);
            
            NonLoanValueEvaluator nonLoanValueEvaluator = new NonLoanValueEvaluator();
            nonLoanValueEvaluator.Add(ParameterReporting.GenerateDocsVendorPackageIdFieldId, $"{this.Config.VendorId}_{package.Name}");

            // Package Type is a flag enum. So, in order to get it to work with a Workflow "In"
            // comparison we must separate out the individual bits from the actual value.
            List<int> packageTypes;
            if (package.IsUndefinedType)
            {
                packageTypes = new List<int>()
                {
                    (int)VendorConfig.PackageType.Undefined
                };
            }
            else
            {
                packageTypes = Enum.GetValues(typeof(VendorConfig.PackageType))
                    .Cast<VendorConfig.PackageType>()
                    .Where(type => type != VendorConfig.PackageType.Undefined && package.IsOfType(type))
                    .Cast<int>().ToList();
            }

            nonLoanValueEvaluator.Add(ParameterReporting.GenerateDocsPackageTypeFieldId, packageTypes);

            LoanValueEvaluator valueEvaluator = new LoanValueEvaluator(principal.BrokerId, loanId, WorkflowOperations.GenerateDocs);
            valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));
            valueEvaluator.SetNonLoanValues(nonLoanValueEvaluator);

            bool canGenerateDocs = LendingQBExecutingEngine.CanPerform(WorkflowOperations.GenerateDocs, valueEvaluator);
            if (!canGenerateDocs)
            {
                failureReasons = LendingQBExecutingEngine.GetUserFriendlyMessage(WorkflowOperations.GenerateDocs, valueEvaluator);
            }

            return canGenerateDocs;
        }

        public DocumentVendorResult<Audit> AuditLoan(Guid sLId, Guid AppId, string PackageID, string LoanProgramID, AbstractUserPrincipal principal, bool preventUsingTemporaryArchive)        {
            DocRequest req = new DocRequest();

            req.Request = new Request();
            req.Request.RequestType = RequestRequestType.Audit;
            req.Request.PostbackUrl = Constants.ConstStage.DocFrameworkPostbackUrl;
            var requestSpecifics = new AuditRequest();

            requestSpecifics.PackageID = PackageID;
            requestSpecifics.LoanProgramID = LoanProgramID;

            req.Request.Item = requestSpecifics;

            List<string> feeDiscrepancies;
            var transaction = Server.Submit(
                req,
                sLId,
                out feeDiscrepancies,
                principal,
                preventUsingTemporaryArchive);

            if (transaction.HasError)
            {
                return DocumentVendorResult.Error(transaction.Error, new Audit());
            }

            if (transaction.Response.AuditResponse == null)
            {
                return DocumentVendorResult.Error("Could not request an audit from the remote server", new Audit());
            }

            var response = transaction.Response.AuditResponse;
            List<AuditMessage> messages = new List<AuditMessage>();
            if (response.AuditMessage != null)
            {
                messages = response.AuditMessage.ToList();
            }

            var auditResponse = (from audit in messages where audit != null
                                select MakeAuditLine(audit, sLId, AppId)).ToList();

            // 3/3/14 gf - opm 172303, we want to add audit warnings for each fee discrepancy.
            if (feeDiscrepancies != null)
            {
                foreach (var feeDiscrepancy in feeDiscrepancies)
                {
                    auditResponse.Add(new AuditLine(feeDiscrepancy, AuditSeverity.Warning, null, null, null, sLId, AppId));
                }
            }

            return DocumentVendorResult.Success(new Audit(auditResponse, response.CustomDataURL, response.CustomDataURL2));
        }

        private struct AccountInfo
        {
            public static AccountInfo PackageInfo
            {
                get
                {
                    return new AccountInfo(true, false, false);
                }
            }

            public static AccountInfo ProgramInfo
            {
                get
                {
                    return new AccountInfo(false, true, false);
                }
            }

            public static AccountInfo PermissionInfo
            {
                get
                {
                    return new AccountInfo(false, false, true);
                }
            }

            public static AccountInfo AllInfo
            {
                get
                {
                    return new AccountInfo(true, true, true);
                }
            }

            public readonly bool EnabledPackages;
            public readonly bool Permissions;
            public readonly bool LoanPrograms;

            public AccountInfo(bool packages, bool programs, bool permissions)
            {
                EnabledPackages = packages;
                Permissions = permissions;
                LoanPrograms = programs;
            }
        }
        private DocumentVendorResult<AccountInfoResponse> GetAccountInfo(AccountInfo info, Guid LoanId, AbstractUserPrincipal principal)
        {
            var ret = new AccountInfoResponse();

            DocRequest req = new DocRequest();
            req.Request = new Request();
            req.Request.RequestType = RequestRequestType.AccountInfo;

            var requestSpecifics = new AccountInfoRequest();

            requestSpecifics.RequestEnabledPackages = info.EnabledPackages ? AccountInfoRequestRequestEnabledPackages.Y : AccountInfoRequestRequestEnabledPackages.N;
            requestSpecifics.RequestEnabledPackagesSpecified = true;
            requestSpecifics.RequestLoanPrograms = info.LoanPrograms ? AccountInfoRequestRequestLoanPrograms.Y : AccountInfoRequestRequestLoanPrograms.N;
            requestSpecifics.RequestLoanProgramsSpecified = true;
            requestSpecifics.RequestPermissions = info.Permissions ? AccountInfoRequestRequestPermissions.Y : AccountInfoRequestRequestPermissions.N;
            requestSpecifics.RequestPermissionsSpecified = true;

            //OPM 115489: Users will need to be able to read these fields even if they don't have access to them normally,
            //and that's okay because only the document vendor sees these fields (not the user).
            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(LoanId, typeof(ConfiguredDocumentVendor));
            dataLoan.InitLoad();

            dataLoan.ByPassFieldSecurityCheck = true;

            requestSpecifics.InvestorName = dataLoan.sCalcInvestorNm;

            if (Config.ExportRegisteredProgram)
            {
                requestSpecifics.RegisteredProgramName = dataLoan.sLpTemplateNm;
            }
            
            dataLoan.ByPassFieldSecurityCheck = false;

            req.Request.Item = requestSpecifics;

            var transaction = Server.Submit(req, principal);

            if (transaction.HasError)
            {
                return DocumentVendorResult.Error(transaction.Error, ret);
            }

            ret = transaction.Response.Items?[0] as AccountInfoResponse;

            if (ret == null)
            {
                return DocumentVendorResult.Error("Could not retrieve account information", ret);
            }

            return DocumentVendorResult.Success(ret);
        }

        public DocumentVendorResult<List<DocumentPackage>> GetAvailableDocumentPackages(Guid LoanId, AbstractUserPrincipal principal)
        {
            var ret = new List<DocumentPackage>();
           
            var result = GetAccountInfo(AccountInfo.PackageInfo, LoanId, principal);
            if (result.HasError)
            {
                return DocumentVendorResult.Error(result.ErrorMessage, ret);
            }

            var accountInfo = result.Result;

            if (accountInfo.PackageList == null)
            {
                return DocumentVendorResult.Error("Could not retrieve document package list", ret);
            }

            if (accountInfo.PackageList.Length == 0)
            {
                return DocumentVendorResult.Error("No document packages are available", ret);
            }

            // 8/19/2015 bs - get new package with packageID = 12473 and rename it to "LE Redisclosure"
            
            if (Config.VendorName.Contains("DocuTech"))
            {
                var redisclosurePackage = accountInfo.PackageList.FirstOrDefault(o => o.PackageID == "12473");
                if (redisclosurePackage != null)
                {
                    redisclosurePackage.PackageName = "LE Redisclosure";
                }
            }

            ret = (from info in accountInfo.PackageList
                   select new DocumentPackage(info, Config.VendorName.Contains("DocMagic"))).ToList();

            if (Config.VendorName.Contains("DocuTech")) //OPM 146326: Temporary GFE Redisclosure package for DocuTech
            {
                Package GFERedisclosurePackage = accountInfo.PackageList.FirstOrDefault(o => o.PackageID == "203");

                // Only add if Initial Disclosure package found in PackageList (ie Package with PackageID="203" in PackageList)
                if (GFERedisclosurePackage != null)
                {
                    GFERedisclosurePackage.PackageName = "GFE Redisclosure";

                    GFERedisclosurePackage.PackageID = LendersOffice.Constants.ConstAppDavid.DocuTech_Temp_GFE_Redisclosure_PackageID;
                    ret.Add(new DocumentPackage(GFERedisclosurePackage));
                }
            }
            return DocumentVendorResult.Success(ret);
        }

        public DocumentVendorResult<Dictionary<string, IEnumerable<PlanCode>>> GetAvailablePlanCodes(Guid LoanId, AbstractUserPrincipal principal)
        {
            var ret = new Dictionary<string, IEnumerable<PlanCode>>();

            var result = GetAccountInfo(AccountInfo.ProgramInfo, LoanId, principal);
            if (result.HasError)
            {
                return DocumentVendorResult.Error(result.ErrorMessage, ret);
            }

            var accountInfo = result.Result;
            if (accountInfo.LoanProgramList == null)
            {
                return DocumentVendorResult.Error("Could not retrieve loan program list", ret);
            }

            if (accountInfo.LoanProgramList.Length == 0)
            {
                return DocumentVendorResult.Error("No loan programs available", ret);
            }

            //Pretty sure loan programs map to plan codes
            var planCodes = from info in accountInfo.LoanProgramList
                            select new PlanCode(info.LoanProgramName, info.LoanProgramID, null);

            ret["All"] = planCodes;

            return DocumentVendorResult.Success(ret);
        }

        public DocumentVendorResult<List<FormInfo>> GetFormList(Guid LoanID, string ProgramID, string PackageID, AbstractUserPrincipal principal)
        {
            var ret = new List<FormInfo>();

            var request = new DocRequest();
            request.Request = new Request();
            request.Request.RequestType = RequestRequestType.FormListRequest;

            var specifics = new FormListRequest();
            specifics.Type = FormListRequestType.List;
            specifics.LoanProgramID = ProgramID;
            specifics.PackageID = PackageID;
            request.Request.Item = specifics;

            var response = Server.Submit(request, LoanID, principal);

            if (response.HasError)
            {
                return DocumentVendorResult.Error(response.Error, ret);
            }

            var forms = response.Response.Items?[0] as FormListResponse;

            if (forms == null)
            {
                return DocumentVendorResult.Error("Did not get the right sort of response when requesting form list", ret);
            }

            if (forms.Form == null)
            {
                ret = new List<FormInfo>(); // return empty list.
            }
            else
            {
                ret = (from f in forms.Form
                       select new FormInfo("0", f.FormName, f.FormID, f.FormDescription)).ToList();
            }

            return DocumentVendorResult.Success(ret);
        }

        public DocumentVendorResult<Permissions> GetPermissions(Guid LoanId, bool allowEPortal, bool disableESign, bool allowEClose, bool allowManual, string PackageType, AbstractUserPrincipal principal)
        {
            var info = GetAccountInfo(AccountInfo.PermissionInfo, LoanId, principal);
            if (info.HasError)
            {
                return DocumentVendorResult.Error(info.ErrorMessage, new Permissions());
            }

            if (info.Result.PermissionsList == null)
            {
                return DocumentVendorResult.Error("Could not load permissions", new Permissions());
            }

            //Fake up a package - eventually this should be refactored so that we receive it as a parameter.
            var package = new Package();
            package.PackageID = PackageType;
            package.IsEsignableSpecified = true;
            package.IsEDisclosableSpecified = true;
            package.IsEClosableSpecified = true;
            package.EnableFulfillmentSpecified = true;

            package.IsEsignable = !disableESign ? PackageIsEsignable.Y : PackageIsEsignable.N;
            package.IsEDisclosable = allowEPortal ? PackageIsEDisclosable.Y : PackageIsEDisclosable.N;
            package.IsEClosable = allowEClose ? PackageIsEClosable.Y : PackageIsEClosable.N;
            package.EnableFulfillment = allowManual ? PackageEnableFulfillment.Y : PackageEnableFulfillment.N;

            return DocumentVendorResult.Success(new Permissions(info.Result.PermissionsList, package));
        }

        private static AuditLine MakeAuditLine(AuditMessage audit, Guid LoanId, Guid AppId)
        {
            string message = audit.Description;
            AuditSeverity severity = ConvertFromDocRequest(audit.Severity);
            string editPage;
            string fieldId;

            if (string.IsNullOrEmpty(audit.FieldID))
            {
                var mapping = DocMagicXpathMapping.Retrieve(audit.MISMOPath);
                if (mapping == null)
                {
                    editPage = null;
                    fieldId = null;
                }
                else
                {
                    editPage = mapping.DefaultURL;
                    fieldId = mapping.FieldId;
                }
            }
            else
            {
                fieldId = audit.FieldID;
                editPage = null;
            }

            return new AuditLine(message, severity, editPage, fieldId, audit.AuditDetails ?? "", LoanId, AppId);
        }

        private static AuditSeverity ConvertFromDocRequest(AuditMessageSeverity auditMessageSeverity)
        {
            switch (auditMessageSeverity)
            {
                case (AuditMessageSeverity.FATAL):
                    return AuditSeverity.Fatal;
                case (AuditMessageSeverity.WARNING):
                    return AuditSeverity.Warning;
                case (AuditMessageSeverity.MISC):
                    return AuditSeverity.Misc;
                case(AuditMessageSeverity.INFO):
                    return AuditSeverity.Info;
                case(AuditMessageSeverity.CRITICAL):
                    return AuditSeverity.Critical;
                default:
                    Tools.LogWarning("Unknown audit message severity: " + auditMessageSeverity);
                    return AuditSeverity.Undefined;
            }
        }

        public IDocumentGenerationRequest GetDocumentGenerationRequest(Guid AppId, Guid LoanId)
        {
            return new ConfiguredDocumentGenerationRequest(AppId, LoanId, this.BrokerId, this.Server);
        }

        public Guid BrokerId { get; private set; }

        private DVSkin skin;
        public DVSkin Skin
        {
            get
            {
                return skin;
            }
        }

        public VendorConfig Config
        {
            get;
            private set;
        }

        public IDocumentVendorCredentials Credentials
        {
            get;
            private set;
        }

        public IDocumentVendorCredentials CredentialsFor(Guid UserId, Guid branchId)
        {
            return VendorCredentials.GetUserCredentialsOnly(UserId, this.BrokerId, this.Server.Config.VendorId);
        }

        public DocumentVendorResult<List<InvestorInfo>> GetTransferToInvestors(string PlanCode)
        {
            throw new CBaseException(ErrorMessages.Generic, "This is only intended for use with DocMagic.");
        }

        /// <summary>
        /// Get an IDS "Loan File Link" for the IDS document portal.
        /// </summary>
        /// <param name="loanId"></param>
        /// <returns></returns>
        public DocumentVendorResult<LqbAbsoluteUri> GetIdsLoanFileLink(Guid loanId, AbstractUserPrincipal principal)
        {
            if (this.Config.PlatformType != E_DocumentVendor.IDS)
            {
                throw new CBaseException(ErrorMessages.Generic, "Loan file link request should only occur for IDS.");
            }

            DocRequest request = new DocRequest
            {
                Request = new Request
                {
                    RequestType = RequestRequestType.LoanFileLink
                }
            };

            DocumentVendorTransaction<DocRequest, DocResponse> transaction = 
                Server.Submit(request, loanId, principal);

            if (transaction.HasError)
            {
                return DocumentVendorResult.Error(transaction.Error, LqbAbsoluteUri.BadURI);
            }

            var response = transaction.Response.Items[0] as LoanFileLinkResponse;
            if (response as LoanFileLinkResponse == null)
            {
                return DocumentVendorResult.Error("Did not receive a loan file link from the remote server.", LqbAbsoluteUri.BadURI);
            }
            else
            {
                var uri = LqbAbsoluteUri.Create(response.CustomDataURL);
                if (uri == null)
                {
                    return DocumentVendorResult.Error("Loan file link did not parse as valid URI.", LqbAbsoluteUri.BadURI);
                }
                return DocumentVendorResult.Success(uri.Value);
            }
        }
    }
}
