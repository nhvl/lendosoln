﻿namespace LendersOffice.Integration.DocumentVendor
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;

    public enum DVProtocol
    {
        SOAP1_1,
        SOAP1_2,
        HTTP_POST
    }

    public static class VendorConfigExtensions
    {
        public static string ToStringOrEmpty(this Uri input)
        {
            if (input == null) return "";
            return input.AbsoluteUri;
        }

        public static Uri ToUriOrNull(this string input)
        {
            Uri ret;

            if (!Uri.TryCreate(input, UriKind.Absolute, out ret)) ret = null;
            return ret;
        }
    }

    public class VendorConfig
    {
        private static string x_LOXmlExportFields = null;
        private static readonly string LOXmlExportFieldsKey = "VendorConfigLOXmlExportFieldsKey";
        private static DateTime ExportFieldsNextRefresh = DateTime.MinValue;

        public bool SendAsApplicationXml
        {
            get
            {
                return ParameterName.Equals("**APPLICATION/XML**", StringComparison.OrdinalIgnoreCase);
            }
        }

        public bool IsTest
        {
            get
            {
                return this.VendorIsTest || this.UserIsTest;
            }
        }

        /// <summary>
        /// Gets the platform type of this document vendor.
        /// </summary>
        public E_DocumentVendor PlatformType
        {
            get; set;
        }

        /// <summary>
        /// Indicates whether this is a vendor created for the purpose of testing.
        /// </summary>
        public bool IsTestVendor
        {
            get; set;
        }

        /// <summary>
        /// Gets a value indicating whether the document vendor is set to use test environment.
        /// </summary>
        public bool VendorIsTest
        {
            get
            {
                var currentUser = PrincipalFactory.CurrentPrincipal;
                return DocumentVendorBrokerSettings.IsTest(VendorId, currentUser.BrokerId);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the user has the "Send all document requests to Test Environment"
        /// permission set.
        /// </summary>
        public bool UserIsTest
        {
            get
            {
                var currentUser = PrincipalFactory.CurrentPrincipal;

                //If the user is an internal user, just say we're testing (this shouldn't get used anyway, and testing is the safest default).
                if (currentUser is InternalUserPrincipal || currentUser is SystemUserPrincipal || currentUser == null)
                {
                    return true;
                }

                return currentUser.HasPermission(Permission.ExportDocRequestsToTestingPath);
            }
        }

        /// <summary>
        /// The setter will throw an XmlException if the value being assigned is not valid xml.
        /// </summary>
        public static string LOXmlExportFields
        {
            get
            {
                if (ExportFieldsNextRefresh < DateTime.Now)
                {
                    try
                    {
                        x_LOXmlExportFields = FileDBTools.ReadDataText(E_FileDB.Normal, LOXmlExportFieldsKey);
                    }
                    catch (FileNotFoundException)
                    {
                        x_LOXmlExportFields = null;
                    }
                    //We don't need to refresh very often because all changes should come through here.
                    ExportFieldsNextRefresh = DateTime.Now.AddDays(1);
                }

                if (x_LOXmlExportFields == null)
                {
                    Tools.LogError("Don't have a value for LOXmlExportFields, returning default.");
                    return @"<loan><field id=""slnm"" /></loan>";
                }

                return x_LOXmlExportFields;
            }
            set
            {
                XDocument doc = XDocument.Parse(value);
                x_LOXmlExportFields = value;
                FileDBTools.WriteData(E_FileDB.Normal, LOXmlExportFieldsKey, value);
                ExportFieldsNextRefresh = DateTime.Now;
            }
        }

        public Guid VendorId { get; set; }
        public string VendorName { get; set; }
        public string TestAccountId { get; set; }
        public string LiveAccountId { get; set; }
        public Uri PrimaryExportPath { get; set; }
        public Uri BackupExportPath { get; set; }
        public Uri TestingExportPath { get; set; }
        public Uri PreviewExportPath { get; set; }

        /// <summary>
        /// A list of packages supported by the vendor but unconfigured in LendingQB.
        /// </summary>
        private List<string> unconfiguredPackages;

        /// <summary>
        /// Gets or sets a list of packages supported by the vendor but unconfigured in LendingQB.
        /// </summary>
        public List<string> UnconfiguredPackages
        {
            get
            {
                if (this.unconfiguredPackages == null)
                {
                    this.unconfiguredPackages = new List<string>();
                }

                return this.unconfiguredPackages;
            }

            set
            {
                this.unconfiguredPackages = value;
            }
        }

        private string packageDataListJSON;
        public string PackageDataListJSON
        {
            get
            {
                return this.packageDataListJSON;
            }

            private set
            {
                this.packageData = null;
                DeserializePackageData(value);
                this.packageDataListJSON = value;
            }
        }

        /// <summary>
        /// Attempts to set the value of <see cref="PackageDataListJSON"/>.  On failure, it reports the error message.
        /// </summary>
        /// <param name="json">The JSON string to set as the value.</param>
        /// <param name="errorMessage">The error that occured while deserializing <paramref name="json"/>.</param>
        /// <returns><see langword="true"/> if <see cref="PackageDataListJSON"/> was set, <see langword="false"/> otherwise.</returns>
        /// <remarks>
        /// This should go in the app, but we're catching a <see cref="Newtonsoft.Json.JsonException"/>,
        /// which would have meant an additional otherwise unneeded reference.
        /// </remarks>
        public bool TrySetPackageDataListJSON(string json, out string errorMessage)
        {
            try
            {
                this.PackageDataListJSON = json;
                errorMessage = null;
                return true;
            }
            catch (Newtonsoft.Json.JsonException exc)
            {
                errorMessage = exc.Message;
                return false;
            }
        }

        private Dictionary<string, DocumentPackage> packageData;

        /// <summary>
        /// Retrieves the package data, including any custom packages specified for the lender.
        /// </summary>
        protected Dictionary<string, DocumentPackage> PackageData
        {
            get
            {
                if (this.packageData == null)
                {
                    // 08/17/18 - je - Maintaining current behavior for now, since a full refactor is outside of the scope of my work,
                    // but this really needs to be refactored to not rely on CurrentPrincipal. Always pass in principal to the backend.
                    Guid brokerId = PrincipalFactory.CurrentPrincipal?.BrokerId ?? Guid.Empty;
                    this.packageData = this.GetPackageDataForBroker(brokerId);
                } 

                return this.packageData;
            }
        }

        /// <summary>
        /// Retrieves the package data, including any custom packages specified for the lender.
        /// </summary>
        /// <param name="brokerId"></param>
        /// <returns>A read only dictionary of <see cref="DocumentPackage"/> by package name.</returns>
        public Dictionary<string, DocumentPackage> GetPackageDataForBroker(Guid brokerId)
        {
            var packageData = DeserializePackageData(this.packageDataListJSON);

            if (brokerId != Guid.Empty)
            {
                var brokerSettings = DocumentVendorBrokerSettings.ListAllForBroker(brokerId).First(v => v.VendorId == this.VendorId);

                if (brokerSettings.HasCustomPackageData)
                {
                    foreach (var kvp in DeserializePackageData(brokerSettings.CustomPackageDataJSON))
                    {
                        // We can auto-insert all packages since default packages should always be
                        // overwritten by custom packages if they have a matching name.
                        packageData[kvp.Key] = kvp.Value;
                    }
                }
            }

            return packageData;
        }

        private Dictionary<string, DocumentPackage> vendorPackageData;

        /// <summary>
        /// Retrieves the vendor's configured packages, not including any custom packages defined
        /// for the lender. (Use PackageData to retrieve all packages).
        /// </summary>
        protected Dictionary<string, DocumentPackage> VendorPackageData
        {
            get
            {
                if (this.vendorPackageData == null)
                {
                    this.vendorPackageData = DeserializePackageData(this.packageDataListJSON);
                }

                return this.vendorPackageData;
            }
        }

        private static Dictionary<string, DocumentPackage> DeserializePackageData(string packageDataListJSON)
        {
            return (SerializationHelper.JsonNetDeserialize<List<DocumentPackage>>(packageDataListJSON) ?? new List<DocumentPackage>())
                        .ToDictionary(package => package.Name, StringComparer.OrdinalIgnoreCase);
        }

        private void FlushPackageData()
        {
            if (this.packageData != null)
            {
                this.packageDataListJSON = SerializationHelper.JsonNetSerializeBeautifully<List<DocumentPackage>>(this.PackageData.Values.ToList());
            }
        }

        public bool UsesAccountId { get; set; }
        public bool UsesUsername { get; set; }
        public bool UsesPassword { get; set; }
        public bool SupportsBarcodes { get; set; }
        public bool DisableManualFulfilmentAddr { get; set; }
        public bool SupportsOnlineInterface { get; set; }
        public bool EnableMismo34 { get; set; }
        public bool IsActive { get; set; }
        public bool EnableConnectionTest { get; set; }
        public bool ExportRegisteredProgram
        {
            get
            {
                //OPM 116549: For now, we'll always export this. Turn it into a vendor config bit later.
                return true;
            }
        }

        public DVProtocol Protocol { get; set; }
        public string ParameterName { get; set; }
        public string ExtraParameters { get; set; }

        public VendorConfig(DbDataReader row)
        {
            Protocol = DVProtocol.HTTP_POST;
            ParameterName = "xmlRequest";

            VendorId = (Guid)row["VendorId"];
            VendorName = (string)row["VendorName"];

            TestAccountId = (string)row["TestAccountId"];
            LiveAccountId = (string)row["LiveAccountId"];

            PrimaryExportPath = new Uri((string)row["ExportPath"]);

            BackupExportPath = ((string)row["BackupExportPath"]).ToUriOrNull();
            TestingExportPath = ((string)row["TestingExportPath"]).ToUriOrNull();
            PreviewExportPath = ((string)row["PreviewExportPath"]).ToUriOrNull();
            ExtraParameters = (string)row["ExtraParameters"];

            string parameter = (string)row["ParameterName"];
            ParameterName = string.IsNullOrEmpty(parameter) ? "xmlRequest" : parameter;

            this.packageDataListJSON = (string)row["PackageDataJSON"];

            UnconfiguredPackages = ObsoleteSerializationHelper.JsonDeserialize<List<string>>((string)row["UnconfiguredPackages"]) ?? new List<string>();

            UsesAccountId = (bool)row["UsesAccountId"];
            UsesUsername = (bool)row["UsesUsername"];
            UsesPassword = (bool)row["UsesPassword"];

            SupportsBarcodes = (bool)row["SupportsBarcodes"];
            DisableManualFulfilmentAddr = (bool)row["DisableManualFulfilmentAddr"];
            SupportsOnlineInterface = (bool)row["SupportsOnlineInterface"];
            EnableMismo34 = (bool)row["EnableMismo34"];

            IsActive = (bool)row["IsActive"];
            EnableConnectionTest = (bool)row["EnableConnectionTest"];
            PlatformType = (E_DocumentVendor)row["PlatformType"];
            IsTestVendor = (bool)row["IsTestVendor"];
        }

        /// <summary>
        /// Use this constructor when you are creating a new vendor configuration.
        /// </summary>
        public VendorConfig()
        {

        }

        public static VendorConfig Retrieve(Guid VendorId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@VendorId", VendorId)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "DOCUMENT_VENDOR_GetConfig", parameters))
            {
                if (reader.Read())
                {
                    return new VendorConfig(reader);
                }
            }

            throw new NotFoundException("Unable to locate document vendor", "Unable to locate document vendor. VendorId=" + VendorId);
        }

        /// <summary>
        /// Saves a VendorConfig to the database. If VendorId == Guid.Empty, it's treated as a new one and a guid is assigned.
        /// </summary>
        /// <param name="toSave">The configuration to save</param>
        public static void Save(VendorConfig toSave)
        {
            if (toSave.VendorId == Guid.Empty)
            {
                toSave.VendorId = Guid.NewGuid();
            }

            var result = StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShareROnly, "DOCUMENT_VENDOR_Save", 3, toSave.GetParams());
        }

        private SqlParameter[] GetParams()
        {
            this.FlushPackageData();
            return new[]{
                new SqlParameter("@VendorId", VendorId),
                new SqlParameter("@VendorName", VendorName),
                new SqlParameter("@TestAccountId", TestAccountId),
                new SqlParameter("@LiveAccountId", LiveAccountId),
                new SqlParameter("@ExportPath", PrimaryExportPath.ToString()),
                new SqlParameter("@PackageDataJSON", this.packageDataListJSON),
                new SqlParameter("@UsesAccountId", UsesAccountId),
                new SqlParameter("@UsesUsername", UsesUsername),
                new SqlParameter("@UsesPassword", UsesPassword),
                new SqlParameter("@SupportsBarcodes", SupportsBarcodes),
                new SqlParameter("@DisableManualFulfilmentAddr", DisableManualFulfilmentAddr),
                new SqlParameter("@SupportsOnlineInterface", SupportsOnlineInterface),
                new SqlParameter("@EnableMismo34", this.EnableMismo34),
                new SqlParameter("@IsActive", IsActive),
                new SqlParameter("@BackupExportPath", BackupExportPath.ToStringOrEmpty()),
                new SqlParameter("@TestingExportPath", TestingExportPath.ToStringOrEmpty()),
                new SqlParameter("@PreviewExportPath", PreviewExportPath.ToStringOrEmpty()),
                new SqlParameter("@ExtraParameters", ExtraParameters),
                new SqlParameter("@ParameterName", ParameterName),
                new SqlParameter("@UnconfiguredPackages", ObsoleteSerializationHelper.JsonSerialize(UnconfiguredPackages)),
                new SqlParameter("@EnableConnectionTest", EnableConnectionTest),
                new SqlParameter("@PlatformType", this.PlatformType),
                new SqlParameter("@IsTestVendor", this.IsTestVendor)
            };
        }

        /// <summary>
        /// Checks whether the package specified by <paramref name="packageName"/> is defined in the vendor config as an initial or re-disclosure package.
        /// </summary>
        /// <param name="packageName">The name of the document package.</param>
        /// <returns><see langword="true"/> if <paramref name="packageName"/> is defined as an initial or re-disclosure package in the vendor config, <see langword="false"/> otherwise.</returns>
        public bool IsDisclosurePackage(string packageName)
        {
            DocumentPackage package;
            return this.PackageData.TryGetValue(packageName, out package) && (package.IsOfType(PackageType.Initial) || package.IsOfType(PackageType.Redisclosure));
        }

        /// <summary>
        /// Checks whether the package specified by <paramref name="packageName"/> is defined in the vendor config as an initial disclosure package.
        /// </summary>
        /// <param name="packageName">The name of the document package.</param>
        /// <returns><see langword="true"/> if <paramref name="packageName"/> is defined as an initial disclosure package in the vendor config, <see langword="false"/> otherwise.</returns>
        public bool IsInitialDisclosurePackage(string packageName)
        {
            DocumentPackage package;
            return this.PackageData.TryGetValue(packageName, out package) && package.IsOfType(PackageType.Initial);
        }

        /// <summary>
        /// Checks whether the package specified by <paramref name="packageName"/> is defined in the vendor config as a closing package.
        /// </summary>
        /// <param name="packageName">The name of the document package.</param>
        /// <returns><see langword="true"/> if <paramref name="packageName"/> is defined as a closing package in the vendor config, <see langword="false"/> otherwise.</returns>
        public bool IsClosingPackage(string packageName)
        {
            DocumentPackage package;
            return this.PackageData.TryGetValue(packageName, out package) && package.IsOfType(PackageType.Closing);
        }

        /// <summary>
        /// Checks whether the package specified by <paramref name="packageName"/> is set in the vendor config to trigger PTM Billing.
        /// </summary>
        /// <param name="packageName">The name of the document package.</param>
        /// <returns><see langword="true"/> if <paramref name="packageName"/> is set to trigger PTM Billing in the vendor config, <see langword="false"/> otherwise.</returns>
        public bool TriggersPtmBilling(string packageName)
        {
            DocumentPackage package;
            return this.PackageData.TryGetValue(packageName, out package) && package.TriggersPtmBilling;
        }

        /// <summary>
        /// Checks whether the given package has a configuration. To be configured, there should be a package
        /// with the given name and that package's type should not be Undefined. The package must be configured
        /// for the vendor and cannot be considered configured if it only has a custom configuration.
        /// </summary>
        /// <param name="packageName">The name of the document package.</param>
        /// <returns></returns>
        public bool PackageExists(string packageName)
        {
            return VendorPackageData.ContainsKey(packageName);
        }

        /// <summary>
        /// Looks for the package as defined by the document vendor. <see cref="DocumentPackage.InvalidPackage"/> is returned in the event of no match.
        /// </summary>
        /// <param name="packageName">The name of the document package.</param>
        /// <param name="includeCustomPackages">Indicates whether to include the lender's custom document packages.</param>
        /// <returns>The document package data as defined by the document vendor, or <see cref="DocumentPackage.InvalidPackage"/> if no matching package was found.</returns>
        public DocumentPackage RetrievePackage(string packageName, bool includeCustomPackages = true)
        {
            DocumentPackage package;

            if (includeCustomPackages)
            {
                // If the custom package turns out to be invalid, attempt to retrieve the default vendor package.
                return this.PackageData.TryGetValue(packageName, out package) ? package : RetrievePackage(packageName, false);
        }
            else
            {
                return this.VendorPackageData.TryGetValue(packageName, out package) ? package : DocumentPackage.InvalidPackage;
            }
        }

        /// <summary>
        /// Valid options for the type of the document package.
        /// </summary>
        [Flags]
        public enum PackageType
        {
            /// <summary>
            /// The document package is not well-defined in our system.
            /// </summary>
            Undefined = 0,

            /// <summary>
            /// The document package is an initial disclosure package.
            /// </summary>
            Initial = 1 << 0,

            /// <summary>
            /// The document package is a re-disclosure package.
            /// </summary>
            Redisclosure = 1 << 1,

            /// <summary>
            /// The document package is a closing document package.
            /// </summary>
            Closing = 1 << 2,
        }

        /// <summary>
        /// Represents a document package, as specified by a document vendor.<para />
        /// Instances of this class are immutable.
        /// </summary>
        [DataContract]
        public class DocumentPackage
        {
            /// <summary>
            /// Represents an empty document package.
            /// </summary>
            /// <remarks>This safe to be public since it is static and readonly.</remarks>
            public static readonly DocumentPackage InvalidPackage = new DocumentPackage(string.Empty, PackageType.Undefined, false, false, false, false, false);

            /// <summary>
            /// Initializes a new instance of the <see cref="DocumentPackage"/> class.
            /// </summary>
            /// <param name="name">The name of the package.</param>
            /// <param name="type">The type or types of the package.</param>
            /// <param name="hasGFE">A value indicating whether or not the package contains a GFE.</param>
            /// <param name="hasTIL">A value indicating whether or not the package contains a TIL.</param>
            /// <param name="hasLoanEstimate">A value indicating whether or not the package contains a Loan Estimate.</param>
            /// <param name="hasClosingDisclosure">A value indicating whether or not the package contains a Closing Disclosure.</param>
            [Newtonsoft.Json.JsonConstructor]
            public DocumentPackage(string name, PackageType type, bool hasGFE, bool hasTIL, bool hasLoanEstimate, bool hasClosingDisclosure, bool triggersPtmBilling)
            {
                this.Name = name;
                this.Type = type;
                this.HasGFE = hasGFE;
                this.HasTIL = hasTIL;
                this.HasLoanEstimate = hasLoanEstimate;
                this.HasClosingDisclosure = hasClosingDisclosure;
                this.TriggersPtmBilling = triggersPtmBilling;
            }

            /// <summary>
            /// Gets the name of the package, specified by the document vendor in<para/>
            /// <seealso cref="LendersOffice.Integration.DocumentVendor.LQBDocumentResponse.Package.PackageName"/>.
            /// </summary>
            /// <value>The name of the package.</value>
            [DataMember(IsRequired = true)]
            public string Name { get; private set; }

            /// <summary>
            /// Gets a value indicating whether the document package contains a Good Faith Estimate form.
            /// </summary>
            /// <value>A value indicating whether the document package contains a Good Faith Estimate form.</value>
            [DataMember]
            public bool HasGFE { get; private set; }

            /// <summary>
            /// Gets a value indicating whether the document package contains a Truth In Lending form.
            /// </summary>
            /// <value>A value indicating whether the document package contains a Truth In Lending form.</value>
            [DataMember]
            public bool HasTIL { get; private set; }

            /// <summary>
            /// Gets a value indicating whether the document package contains a Loan Estimate form.
            /// </summary>
            /// <value>A value indicating whether the document package contains a Loan Estimate form.</value>
            [DataMember]
            public bool HasLoanEstimate { get; private set; }

            /// <summary>
            /// Gets a value indicating whether the document package contains a Closing Disclosure form.
            /// </summary>
            /// <value>A value indicating whether the document package contains a Closing Disclosure form.</value>
            [DataMember]
            public bool HasClosingDisclosure { get; private set; }

            /// <summary>
            /// Gets a value indicating whether the document package triggers PTM Billing.
            /// </summary>
            /// <value>A value indicating whether the document package triggers PTM Billing.</value>
            [DataMember]
            public bool TriggersPtmBilling { get; private set; }

            /// <summary>
            /// Gets or sets the type of the document package.
            /// </summary>
            /// <remarks><see cref="PackageType"/> is a Flags enum.  Access should be through the method <see cref="IsOfType"/>.</remarks>
            /// <value>The type of the document package.</value>
            [DataMember]
            private PackageType Type { get; set; }

            /// <summary>
            /// Checks if the <see cref="Type"/> for this instance includes <paramref name="packageType"/>.
            /// </summary>
            /// <param name="packageType">The type of package to check for.</param>
            /// <returns><see langword="true"/> if <see cref="Type"/> includes <paramref name="packageType"/>; <see langword="false"/> otherwise.</returns>
            public bool IsOfType(PackageType packageType)
            {
                return (this.Type & packageType) == packageType;
            }

            public bool IsUndefinedType
            {
                get { return this.Type == PackageType.Undefined; }
            }
        }
    }
}
