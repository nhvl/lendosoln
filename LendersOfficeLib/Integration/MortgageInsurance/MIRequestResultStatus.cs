﻿namespace LendersOffice.Integration.MortgageInsurance
{
    /// <summary>
    /// The results statuses for an MI request.
    /// </summary>
    public enum MIRequestResultStatus
    {
        /// <summary>
        /// The MI Request could not be completed.
        /// </summary>
        Failure = 0,

        /// <summary>
        /// The MI Request has completed.
        /// </summary>
        Success = 1,

        /// <summary>
        /// The request is being processed. The request is being handled by the background job processor.
        /// </summary>
        Processing = 2
    }
}
