﻿// <copyright file="MIResponseProvider.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   10/16/2014 5:24:40 PM
// </summary>
namespace LendersOffice.Integration.MortgageInsurance
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Common;
    using Mismo231.MI;
    using Mismo231.MI.MortgageInsuranceResponse;

    /// <summary>
    /// Provides access to the data included in a MISMO 2.3.1 mortgage insurance ResponseGroup object.
    /// </summary>
    public class MIResponseProvider
    {
        /// <summary>
        /// A default MI rate quote disclaimer to be displayed if the MI vendor does not include their own disclaimer in the quote response.
        /// </summary>
        private const string DefaultQuoteDisclaimer = @"The rate quote provided herein is a quote only and does not constitute an offer of insurance, a commitment of insurance, a certificate of insurance, or a policy of insurance. Provision of a quote does not guarantee that an application will be approved or that insurance coverage will be issued. Rates are subject to change. The rate quote is based on the information you provide; further information may be required in order for a quote to be provided.";

        /// <summary>
        /// The mortgage insurance response as a MISMO 2.3.1 ResponseGroup object.
        /// </summary>
        private ResponseGroup responseGroup = null;

        /// <summary>
        /// The source of the mortgage insurance response, which is either transmitted by the vendor or loaded from the LQB database.
        /// </summary>
        private MIResponseDataSource dataSource;

        /// <summary>
        /// Gets the status included in the MI response by the vendor.
        /// </summary>
        private string statusCondition = string.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="MIResponseProvider"/> class.
        /// </summary>
        /// <param name="vendorResponse">The mortgage insurance response as a MISMO 2.3.1 ResponseGroup object.</param>
        /// <param name="source">The source of the MI response, which is either received from the vendor or loaded from the LQB database.</param>
        public MIResponseProvider(ResponseGroup vendorResponse, MIResponseDataSource source)
        {
            this.responseGroup = vendorResponse;
            this.dataSource = source;
            this.Init();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MIResponseProvider"/> class.
        /// </summary>
        /// <param name="responseXML">The mortgage insurance response as a string.</param>
        /// <param name="source">The source of the MI response, which is either received from the vendor or loaded from the LQB database.</param>
        public MIResponseProvider(string responseXML, MIResponseDataSource source)
        {
            this.responseGroup = (ResponseGroup)SerializationHelper.XmlDeserialize(responseXML, typeof(ResponseGroup));
            this.dataSource = source;
            this.Init();
        }

        /// <summary>
        /// Gets a value indicating whether the MI vendor included any error messages in the MI response.
        /// </summary>
        /// <value>True if the response includes error messages. Otherwise false.</value>
        public bool HasError { get; private set; }

        /// <summary>
        /// Gets a value indicating whether error messages has FATAL error message or not in the MI response.
        /// </summary>
        /// <value>True if the response is FATAL error messages. Otherwise false.</value>
        public bool HasFatalError { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the response provider has a valid MI response.
        /// </summary>
        /// <value>True if the response is not null. False if it is null.</value>
        public bool HasResponse
        {
            get
            {
                return this.responseGroup != null;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the response provider has a valid MI response with MI data.
        /// </summary>
        /// <value>True if the response included an <see cref="MIResponse"/>. Otherwise false.</value>
        public bool HasResponseInfo
        {
            get
            {
                return this.HasResponse && (this.ResponseInfo != null);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the vendor response is a quote response.
        /// </summary>
        /// <value>True if the response is a quote response. False if it is a policy response.</value>
        public bool IsQuoteResponse
        {
            get
            {
                return this.HasResponseInfo ? MIMismoConvertor.IsQuote(this.ResponseInfo.MIApplicationType) : true;
            }
        }

        /// <summary>
        /// Gets the message(s) included in the MI response by the vendor.
        /// </summary>
        /// <value>The message(s) from the MI vendor.</value>
        public string Message { get; private set; }

        /// <summary>
        /// Gets the status included in the MI response by the vendor.
        /// </summary>
        /// <value>The status from the MI vendor.</value>
        public string Status
        {
            get
            {
                if (this.HasError)
                {
                    return "ERROR";
                }
                else
                {
                    return this.statusCondition;
                }
            }
        }

        /// <summary>
        /// Gets the PDF document(s) included in the MI response by the vendor.
        /// </summary>
        /// <value>A list of base64 encoded PDF files.</value>
        public List<string> EncodedPDF { get; private set; }

        /// <summary>
        /// Gets an MIResponse object with the mortgage insurance data, documents, and messages from the MI vendor.
        /// </summary>
        /// <value>The mortgage insurance data, documents, and messages from the MI vendor as an MIResponse object.</value>
        public MIResponse ResponseInfo { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the MI response includes a split premium.
        /// </summary>
        /// <value>True if the response includes a key/value indicating a split premium.</value>
        public bool SplitPremium { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the MI response is for a relocation loan.
        /// </summary>
        /// <value>True if the response indicates a relocation loan.</value>
        public bool Relocation { get; private set; }

        /// <summary>
        /// Gets a disclaimer for display in the quote response dialog.
        /// </summary>
        /// <value>The rate quote disclaimer.</value>
        public string QuoteDisclaimer { get; private set; }

        /// <summary>
        /// Gets the LQB order number from the response. For use when handling the asynchronous quote/policy response.
        /// </summary>
        /// <value>The order number reflected back from the MI vendor.</value>
        public string OrderNumber { get; private set; }

        /// <summary>
        /// Gets the loan ID (sLId) of the loan from the vendor response.
        /// </summary>
        /// <value>The sLId of the loan that the vendor response belongs to.</value>
        public Guid LoanID { get; private set; }

        /// <summary>
        /// Gets the transaction ID from the vendor response. RESPONSE_GROUP/RESPONSE/RESPONSE_DATA/MI_RESPONSE/@MITransactionIdentifier.
        /// </summary>
        /// <value>A guid identifying the MI transaction.</value>
        public Guid TransactionID { get; private set; }

        /// <summary>
        /// Gets the date and time of the vendor response.
        /// </summary>
        /// <value>The date and time of the response from the MI vendor.</value>
        public DateTime ResponseDateTime { get; private set; }

        /// <summary>
        /// Gets the type of MI premium converted from the vendor response /MI_RESPONSE/@MIDurationType, /MI_RESPONSE/@MIPremiumPaymentType, and split premium indicators.
        /// </summary>
        /// <value>The MI premium type (borrower-paid monthly, borrower-paid single, etc).</value>
        public E_sProdConvMIOptionT PremiumType
        {
            get
            {
                if (this.HasResponseInfo)
                {
                    bool mioptionFound = false;
                    E_sProdConvMIOptionT mioption = MIMismoConvertor.MIOptionType(this.ResponseInfo.MIDurationType, this.ResponseInfo.MIPremiumPaymentType, this.SplitPremium, out mioptionFound);

                    return mioptionFound ? mioption : E_sProdConvMIOptionT.NoMI;
                }

                return E_sProdConvMIOptionT.NoMI;
            }
        }

        /// <summary>
        /// Gets whether the MI premium was prepaid or deferred.
        /// </summary>
        /// <value>Whether the MI premium was prepaid or deferred.</value>
        public MI_MIInitialPremiumAtClosingTypeEnumerated PremiumPrepaidAtClosing
        {
            get
            {
                if (this.HasResponseInfo)
                {
                    return this.ResponseInfo.MIInitialPremiumAtClosingType;
                }

                return MI_MIInitialPremiumAtClosingTypeEnumerated.None;
            }
        }

        /// <summary>
        /// Gets the number of months for which the intial rate applies.
        /// </summary>
        /// <value>The number of months for which the intial rate applies.</value>
        public int InitialPremiumTerm
        {
            get
            {
                if (this.HasResponseInfo)
                {
                    int result;
                    if (int.TryParse(this.ResponseInfo.MIInitialPremiumRateDurationMonths, out result))
                    {
                        return result;
                    }
                }

                return 0;
            }
        }

        /// <summary>
        /// Gets the duration of the monthly MI premium in months.
        /// </summary>
        /// <value>The number of months that the monthly MI premium will last.</value>
        public int MonthlyPremiumTerm { get; private set; }

        /// <summary>
        /// Gets the duration of the monthly MI renewal premium in months.
        /// </summary>
        /// <value>The number of months that the monthly MI renewal premium will last.</value>
        public int RenewalPremiumTerm { get; private set; }

        /// <summary>
        /// Gets the list of MI premiums and/or taxes included in the vendor response.
        /// </summary>
        /// <value>The list of MI premiums and/or taxes from the MI vendor.</value>
        public List<MortgageInsurancePremium> Premiums { get; private set; }

        /// <summary>
        /// Gets the MI Provide quote number from the response. MI provider uses it to accurately provide MI policy.
        /// </summary>
        /// <value>The quote number reflected back from the MI vendor.</value>
        public string QuoteNumber { get; private set; }

        /// <summary>
        /// Masks sensitive data out of a mortgage insurance response.
        /// </summary>
        /// <param name="provider">
        /// The provider with the response.
        /// </param>
        /// <returns>
        /// A string that has had sensitive data masked.
        /// </returns>
        public static string MaskSensitiveResponseData(MIResponseProvider provider)
        {
            string responseXML = string.Empty;

            //// Use serialization to clone the response object so that the masking only occurs within the logged copy. 
            if (provider.HasResponse)
            {
                responseXML = SerializationHelper.XmlSerializeStripDefaultNamespace(provider.responseGroup, true);
            }

            return MaskSensitiveResponseData(responseXML);
        }

        /// <summary>
        /// Masks sensitive data out of mortgage insurance response XML.
        /// </summary>
        /// <param name="responseXml">
        /// The response XML.
        /// </param>
        /// <returns>
        /// A string that has had sensitive data masked.
        /// </returns>
        public static string MaskSensitiveResponseData(string responseXml)
        {
            if (!string.IsNullOrEmpty(responseXml))
            {
                ResponseGroup clonedResponse = (ResponseGroup)SerializationHelper.XmlDeserialize(responseXml, typeof(ResponseGroup));

                foreach (Response response in clonedResponse.ResponseList)
                {
                    response.LoginAccountPassword = string.IsNullOrEmpty(response.LoginAccountPassword) ? string.Empty : "******";

                    //// Not all responses are expected to include a list of ResponseData objects. For example an error response may include an Error STATUS w/o any response data.
                    foreach (ResponseData responseData in response.ResponseDataList ?? new List<ResponseData>())
                    {
                        if (responseData.MIResponse != null)
                        {
                            //// Not all responses are expected to include PDF's. For example a quote response will just include the quoted MI data w/o any policy certificate.
                            foreach (EmbeddedFile file in responseData.MIResponse.EmbeddedFileList ?? new List<EmbeddedFile>())
                            {
                                if (file.Document != null)
                                {
                                    file.Document.PDF = "[Base64 encoded file data redacted to save memory.]";
                                }
                            }

                            if (responseData.MIResponse.Borrower != null)
                            {
                                var semanticSsn = LqbGrammar.DataTypes.SocialSecurityNumber.Create(responseData.MIResponse.Borrower._SSN);
                                if (semanticSsn.HasValue)
                                {
                                    responseData.MIResponse.Borrower._SSN = semanticSsn.Value.MaskedSsn;
                                }
                            }
                        }
                    }
                }

                responseXml = SerializationHelper.XmlSerializeStripDefaultNamespace(clonedResponse, true);
            }

            return responseXml;
        }

        /// <summary>
        /// Initializes the response provider from the MI vendor response.
        /// </summary>
        private void Init()
        {
            this.HasError = false;
            this.EncodedPDF = new List<string>();
            this.SplitPremium = false;
            this.Relocation = false;
            this.QuoteDisclaimer = DefaultQuoteDisclaimer;
            this.OrderNumber = string.Empty;
            this.LoanID = Guid.Empty;
            this.TransactionID = Guid.Empty;
            this.ResponseDateTime = DateTime.MinValue;
            this.MonthlyPremiumTerm = 0;
            this.RenewalPremiumTerm = 0;
            this.Premiums = new List<MortgageInsurancePremium>();
            this.HasFatalError = false;
            this.QuoteNumber = string.Empty;

            this.ParseResponse();
        }

        /// <summary>
        /// Parses the MI response from the MI vendor.
        /// </summary>
        private void ParseResponse()
        {
            StringBuilder vendorMessages = new StringBuilder();

            foreach (Response response in this.responseGroup.ResponseList)
            {
                this.ResponseDateTime = MIUtil.SafeDateTime(response.ResponseDateTime);

                //// Status messages should be presented at the RESPONSE level, but could be placed within the MI_RESPONSE instead.
                this.ParseStatuses(response.StatusList, vendorMessages);
                this.ParseKeyValues(response.KeyList, vendorMessages);

                //// Not all responses are expected to include a list of ResponseData objects. For example an error response may include an Error STATUS w/o any response data.
                foreach (ResponseData responseData in response.ResponseDataList ?? new List<ResponseData>())
                {
                    if (responseData.MIResponse == null)
                    {
                        continue;
                    }

                    this.ResponseInfo = responseData.MIResponse;
                    this.TransactionID = MIUtil.SafeGuid(this.ResponseInfo.MITransactionIdentifier);

                    if (this.ResponseInfo.MIPremiumRatePlanType == MI_MIPremiumRatePlanTypeEnumerated.SplitPremium1 ||
                        this.ResponseInfo.MIPremiumRatePlanType == MI_MIPremiumRatePlanTypeEnumerated.SplitPremium2 ||
                        this.ResponseInfo.MIPremiumRatePlanType == MI_MIPremiumRatePlanTypeEnumerated.SplitPremium3)
                    {
                        this.SplitPremium = true;
                    }

                    this.ParseEmbeddedFiles();
                    string msg = this.ParseMIResponseMessage();

                    if (!string.IsNullOrEmpty(msg))
                    {
                        vendorMessages.AppendLine(msg);
                    }

                    this.ParseKeyValues(this.ResponseInfo.KeyList, vendorMessages);
                    this.ParsePremiums();
                }
            }

            this.Message = vendorMessages.ToString();
        }

        /// <summary>
        /// Extracts any PDF documents included by the MI vendor in the MIResponse element.
        /// </summary>
        private void ParseEmbeddedFiles()
        {
            //// Responses loaded from the LQB database will already have had the PDF data masked out.
            if (this.dataSource == MIResponseDataSource.Vendor)
            {
                //// Not all responses are expected to include PDF's. For example a quote response will just include the quoted MI data w/o any policy certificate.
                foreach (EmbeddedFile file in this.ResponseInfo.EmbeddedFileList ?? new List<EmbeddedFile>())
                {
                    if (file.Document == null)
                    {
                        continue;
                    }

                    if (!string.IsNullOrEmpty(file.Document.PDF))
                    {
                        this.EncodedPDF.Add(file.Document.PDF);
                    }
                    else if (file.Document.Payload != null && !string.IsNullOrEmpty(file.Document.Payload.PDF))
                    {
                        this.EncodedPDF.Add(file.Document.Payload.PDF);
                    }
                }
            }
        }

        /// <summary>
        /// Extracts the message included by the MI vendor in the MIResponse element, if any.
        /// </summary>
        /// <returns>A message from the MI vendor, or an empty string if no message was included.</returns>
        private string ParseMIResponseMessage()
        {
            Status status = this.ResponseInfo.Status;
            bool hasMessage = status != null;

            if (hasMessage)
            {
                hasMessage = !string.IsNullOrEmpty(status._Condition);
            }

            if (hasMessage)
            {
                this.statusCondition = status._Condition;
                this.CheckForError(status._Condition);
                hasMessage = !string.IsNullOrEmpty(status._Description);
            }

            return hasMessage ? status._Description : string.Empty;
        }

        /// <summary>
        /// Parses the list of statuses in the response.
        /// </summary>
        /// <param name="statuses">The list of statuses.</param>
        /// <param name="vendorMessages">A set of messages from the vendor.</param>
        private void ParseStatuses(List<Status> statuses, StringBuilder vendorMessages)
        {
            bool isFirstStatus = true;
            foreach (Status statusMessage in statuses ?? new List<Status>())
            {
                if (string.IsNullOrEmpty(statusMessage._Condition))
                {
                    continue;
                }

                if (!string.IsNullOrEmpty(statusMessage._Description))
                {
                    if (isFirstStatus)
                    {
                        isFirstStatus = false;
                    }
                    else
                    {
                        vendorMessages.AppendLine(Environment.NewLine);
                    }

                    vendorMessages.AppendLine(statusMessage._Description);
                }

                this.statusCondition = statusMessage._Condition;
                this.CheckForError(statusMessage._Condition);
            }
        }

        /// <summary>
        /// Parses the list of key/value pairs in the MI response.
        /// </summary>
        /// <param name="keys">A list of Key objects with key/value pairs.</param>
        /// <param name="vendorMessages">A set of messages from the vendor.</param>
        private void ParseKeyValues(List<Key> keys, StringBuilder vendorMessages)
        {
            foreach (Key key in keys ?? new List<Key>())
            {
                MIKey keyname = MIMismoConvertor.KeyName(key._Name);

                if (keyname == MIKey.LendingQBLoanID)
                {
                    this.LoanID = MIUtil.SafeGuid(key._Value);
                }
                else if (keyname == MIKey.MIRelocationLoanIndicator)
                {
                    this.Relocation = MIMismoConvertor.ParseYNValue(key._Value);
                }
                else if (keyname == MIKey.OrderNumber)
                {
                    this.OrderNumber = MIUtil.SafeGuid(key._Value).ToString("N");
                }
                else if (keyname == MIKey.RateQuoteDisclaimer)
                {
                    this.QuoteDisclaimer = key._Value;
                }
                else if (keyname == MIKey.SplitPremium)
                {
                    this.SplitPremium = true;
                }
                else if (keyname == MIKey.Error || keyname == MIKey.Abort || keyname == MIKey.Fatal)
                {
                    this.HasError = true;
                    vendorMessages.AppendLine(key._Value);

                    if (keyname == MIKey.Fatal)
                    {
                        this.HasFatalError = true;
                    }
                }
                else if (keyname == MIKey.QuoteNumber)
                {
                    this.QuoteNumber = key._Value;
                }
            }
        }

        /// <summary>
        /// Parses the MI premiums and factors from the MI response.
        /// </summary>
        private void ParsePremiums()
        {
            decimal totalTaxRate = this.ParseTaxes();

            string initialPremiumDescription;
            if (this.PremiumType == E_sProdConvMIOptionT.BorrPaidMonPrem)
            {
                initialPremiumDescription = "Initial Premium";
            }
            else if (this.PremiumType == E_sProdConvMIOptionT.LendPaidSinglePrem || this.PremiumType == E_sProdConvMIOptionT.BorrPaidSinglePrem)
            {
                initialPremiumDescription = "Single Premium";
            }
            else
            {
                initialPremiumDescription = "Upfront Premium";
            }

            string initialPremiumDescriptionWithPrepaidType = initialPremiumDescription;
            if (this.PremiumType == E_sProdConvMIOptionT.BorrPaidMonPrem && this.PremiumPrepaidAtClosing != MI_MIInitialPremiumAtClosingTypeEnumerated.None)
            {
                if (this.PremiumPrepaidAtClosing == MI_MIInitialPremiumAtClosingTypeEnumerated.Deferred)
                {
                    initialPremiumDescriptionWithPrepaidType += " (deferred)";
                }
                else
                {
                    initialPremiumDescriptionWithPrepaidType += " (prepaid)";
                }
            }

            decimal initialPremium = MIUtil.ConvertPremium(this.ResponseInfo.MIInitialPremiumRatePercent, false);
            decimal initialPremiumAmount = MIUtil.ConvertDollarAmount(this.ResponseInfo.MIInitialPremiumAmount);
            decimal monthlyBasis = initialPremium == 0 ? 0 : initialPremiumAmount / initialPremium;

            this.Premiums.Add(new MortgageInsurancePremium(initialPremiumDescriptionWithPrepaidType, initialPremium, initialPremiumAmount));
            this.AddPremiumWithTaxes(totalTaxRate, initialPremium, monthlyBasis, initialPremiumDescription);
            
            //// 3/30/2015 BB - The factors provided by the MI vendor are annual. Divide by 12 for calculation of the monthly amounts.
            if (this.PremiumType == E_sProdConvMIOptionT.BorrPaidSplitPrem)
            {
                monthlyBasis = monthlyBasis / 12;
            }

            if (this.PremiumType == E_sProdConvMIOptionT.BorrPaidMonPrem || this.PremiumType == E_sProdConvMIOptionT.BorrPaidSplitPrem)
            {
                foreach (MIRenewalPremium premium in this.ResponseInfo.MIRenewalPremiumList ?? new List<MIRenewalPremium>())
                {
                    int term = 0;
                    decimal factor = MIUtil.ConvertPremium(premium._Rate, true);
                    decimal amount = monthlyBasis * factor;

                    int.TryParse(premium._RateDurationMonths, out term);

                    switch (premium._Sequence)
                    {
                        case MI_MIRenewalPremiumSequenceEnumerated.First:
                            this.MonthlyPremiumTerm = term;
                            this.Premiums.Add(new MortgageInsurancePremium("Monthly Premium", factor, amount));
                            this.AddPremiumWithTaxes(totalTaxRate, factor, monthlyBasis, "Monthly Premium");
                            break;
                        case MI_MIRenewalPremiumSequenceEnumerated.Second:
                            this.RenewalPremiumTerm = term;
                            this.Premiums.Add(new MortgageInsurancePremium("Renewal Monthly Premium", factor, amount));
                            this.AddPremiumWithTaxes(totalTaxRate, factor, monthlyBasis, "Renewal Monthly Premium");
                            break;
                        case MI_MIRenewalPremiumSequenceEnumerated.Third:
                        case MI_MIRenewalPremiumSequenceEnumerated.Fourth:
                        case MI_MIRenewalPremiumSequenceEnumerated.Fifth:
                        case MI_MIRenewalPremiumSequenceEnumerated.None:
                            //// Not supported.
                            break;
                        default:
                            throw new UnhandledEnumException(premium._Sequence);
                    }
                }
            }

            this.ValidatePremiums();
        }

        /// <summary>
        /// Parses the list of taxes from the vendor.
        /// </summary>
        /// <returns>The total tax rate.</returns>
        private decimal ParseTaxes()
        {
            decimal totalTaxRate = 0.00M;
            LosConvert valueConvertor = new LosConvert(FormatTarget.MismoClosing);
            bool foundAllTaxes = false;

            //// Taxes will not always be levied. It's subject to the locale.
            foreach (MIPremiumTax tax in this.ResponseInfo.MIPremiumTaxList ?? new List<MIPremiumTax>())
            {
                string description = string.Empty;
                decimal factor = valueConvertor.ToRate(tax._CodePercent);

                if (tax._CodeType == MI_MIPremiumTaxCodeTypeEnumerated.AllTaxes)
                {
                    description = "Total Taxes (as a % of premium)";
                    totalTaxRate = factor;
                    foundAllTaxes = true;
                }
                else
                {
                    description = tax._CodeType.ToString("G") + " Tax (as a % of premium)";

                    if (!foundAllTaxes)
                    {
                        totalTaxRate += factor;
                    }
                }

                this.Premiums.Add(new MortgageInsurancePremium(description, factor, valueConvertor.ToMoney(tax._CodeAmount)));
            }

            return totalTaxRate;
        }

        /// <summary>
        /// Checks a status message for errors. HasError is set if the message contains any of "ERROR", "ABORT", or "FATAL".
        /// HasFatalError is set only if the message contains "FATAL".
        /// </summary>
        /// <param name="status">The status message.</param>
        private void CheckForError(string status)
        {
            if (status.Contains("ERROR", StringComparison.OrdinalIgnoreCase) ||
                status.Contains("FATAL", StringComparison.OrdinalIgnoreCase) ||
                status.Contains("ABORT", StringComparison.OrdinalIgnoreCase))
            {
                this.HasError = true;

                if (status.Contains("FATAL", StringComparison.OrdinalIgnoreCase))
                {
                    this.HasFatalError = true;
                }
            }
        }

        /// <summary>
        /// For quote responses only, raises an error if there are no premiums, or if there are no
        /// non-zero premium amounts or factors.
        /// </summary>
        private void ValidatePremiums()
        {
            if (this.HasError || this.HasFatalError)
            {
                // If an error already exists, no need to validate premiums since the call has already failed.
                return;
            }
            else if (this.ResponseInfo.MIApplicationType == MI_MIApplicationTypeEnumerated.RateQuote
                && (this.Premiums == null || this.Premiums.All(p => p.Amount == 0 && p.Factor == 0)))
            {
                this.HasError = true;
                this.HasFatalError = true;
            }
        }

        /// <summary>
        /// Adds taxes to the given base premium and then adds the sum with taxes to the list of premiums.
        /// </summary>
        /// <param name="taxRate">The total tax rate.</param>
        /// <param name="basePremium">The base MI premium.</param>
        /// <param name="monthlyBasis">The basis used to calculate the monthly premium amount.</param>
        /// <param name="premiumDescription">A description for the MI premium.</param>
        private void AddPremiumWithTaxes(decimal taxRate, decimal basePremium, decimal monthlyBasis, string premiumDescription)
        {
            if (taxRate > 0.00M)
            {
                decimal factorWithTaxes = MIUtil.AddTaxesToPremium(basePremium, taxRate, false);
                decimal amountWithTaxes = factorWithTaxes * monthlyBasis;
                this.Premiums.Add(new MortgageInsurancePremium(premiumDescription + " with taxes", factorWithTaxes, amountWithTaxes));
            }
        }
    }
}
