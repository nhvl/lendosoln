﻿namespace LendersOffice.Integration.MortgageInsurance
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;

    /// <summary>
    /// Represents a split premium option offered by an MI vendor.
    /// </summary>
    public class MISplitPremiumOption
    {
        /// <summary>
        /// Historically we used this value for all split premium orders, despite it meaning
        /// different things to different vendors. It should generally be avoided, but we do
        /// still use it in cases where there is no vendor associated with an order.
        /// </summary>
        public const string DefaultMismoValue = "SplitPremium1";

        /// <summary>
        /// Initializes a new instance of the <see cref="MISplitPremiumOption"/> class.
        /// </summary>
        /// <param name="upfrontPercentage">The upfront percentage.</param>
        /// <param name="mismoValue">The MISMO value that corresponds to the split premium option for this vendor.</param>
        public MISplitPremiumOption(decimal upfrontPercentage, string mismoValue)
        {
            this.UpfrontPercentage = upfrontPercentage;
            this.MismoValue = mismoValue;
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="MISplitPremiumOption"/> class from being created.
        /// </summary>
        private MISplitPremiumOption()
        {
        }

        /// <summary>
        /// Gets the upfront percentage portion of the split premium.
        /// </summary>
        public decimal UpfrontPercentage { get; private set; }

        /// <summary>
        /// Gets the "SplitPremium#" MISMO value that corresponds to the upfront percentage value.
        /// </summary>
        public string MismoValue { get; private set; }

        /// <summary>
        /// Retrieves all split premium options associated with a given vendor.
        /// </summary>
        /// <param name="vendorId">A vendor ID.</param>
        /// <returns>A collection of split premium options associated with the vendor.</returns>
        public static List<MISplitPremiumOption> RetrieveByVendor(Guid vendorId)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@VendorId", vendorId)
            };

            var options = new List<MISplitPremiumOption>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "MORTGAGE_INSURANCE_VENDOR_SPLIT_PREMIUMS_RetrieveByVendor", parameters))
            {
                while (reader.Read())
                {
                    var upfrontPercentage = (decimal)reader["UpfrontPremiumPercentage"];
                    var mismoValue = (string)reader["UpfrontPremiumMismoValue"];
                    options.Add(new MISplitPremiumOption(upfrontPercentage, mismoValue));
                }
            }

            return options;
        }

        /// <summary>
        /// Saves a set of split premium options.
        /// </summary>
        /// <param name="vendorId">The associated vendor ID.</param>
        /// <param name="options">A list of split premium options.</param>
        /// <param name="exec">A SQL transaction.</param>
        public static void Save(Guid vendorId, List<MISplitPremiumOption> options, CStoredProcedureExec exec)
        {
            // Replace any existing options with the new set.
            DeleteByVendor(vendorId, exec);

            foreach (var option in options)
            {
                var parameters = new SqlParameter[]
                {
                    new SqlParameter("@VendorId", vendorId),
                    new SqlParameter("@UpfrontPremiumPercentage", option.UpfrontPercentage),
                    new SqlParameter("@UpfrontPremiumMismoValue", option.MismoValue)
                };

                exec.ExecuteNonQuery("MORTGAGE_INSURANCE_VENDOR_SPLIT_PREMIUMS_Save", 3, parameters);
            }
        }

        /// <summary>
        /// Converts this object to a UI view.
        /// </summary>
        /// <returns>A view object.</returns>
        public MISplitPremiumOptionView ToView()
        {
            return new MISplitPremiumOptionView(this.UpfrontPercentage, this.MismoValue);
        }

        /// <summary>
        /// Deletes all split premium options associated with a given vendor.
        /// </summary>
        /// <param name="vendorId">A vendor ID.</param>
        /// <param name="exec">A SQL transaction.</param>
        private static void DeleteByVendor(Guid vendorId, CStoredProcedureExec exec)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@VendorId", vendorId)
            };

            exec.ExecuteNonQuery("MORTGAGE_INSURANCE_VENDOR_SPLIT_PREMIUMS_DeleteByVendor", 3, parameters);
        }
    }
}