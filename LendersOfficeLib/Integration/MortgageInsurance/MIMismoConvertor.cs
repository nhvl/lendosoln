﻿// <copyright file="MIMismoConvertor.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   10/7/2014 2:14:27 PM
// </summary>
namespace LendersOffice.Integration.MortgageInsurance
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using Mismo231.MI;

    /// <summary>
    /// Provides helper classes that convert LQB loan values to MISMO 2.3.1 format.
    /// </summary>
    public static class MIMismoConvertor
    {
        /// <summary>
        /// Converts the LQB $$AVM$$ type <see cref="E_sSpAvmModelT"/> to MISMO 2.3.1.
        /// </summary>
        /// <param name="avmType">The loan's $$sSpAvmModelT$$.</param>
        /// <returns>The MISMO 2.3.1 formatted $$AVM$$ model name.</returns>
        public static MI_AVMModelNameTypeEnumerated ToMismo(E_sSpAvmModelT avmType)
        {
            switch (avmType)
            {
                case E_sSpAvmModelT.AutomatedPropertyService:
                    return MI_AVMModelNameTypeEnumerated.AutomatedPropertyService;
                case E_sSpAvmModelT.Casa:
                    return MI_AVMModelNameTypeEnumerated.Casa;
                case E_sSpAvmModelT.FidelityHansen:
                    return MI_AVMModelNameTypeEnumerated.Other;
                case E_sSpAvmModelT.HomePriceAnalyzer:
                    return MI_AVMModelNameTypeEnumerated.HomePriceAnalyzer;
                case E_sSpAvmModelT.HomePriceIndex:
                    return MI_AVMModelNameTypeEnumerated.HomePriceIndex;
                case E_sSpAvmModelT.HomeValueExplorer:
                    return MI_AVMModelNameTypeEnumerated.HomeValueExplorer;
                case E_sSpAvmModelT.Indicator:
                    return MI_AVMModelNameTypeEnumerated.Indicator;
                case E_sSpAvmModelT.LeaveBlank:
                    return MI_AVMModelNameTypeEnumerated.None;
                case E_sSpAvmModelT.MTM:
                    return MI_AVMModelNameTypeEnumerated.Other;
                case E_sSpAvmModelT.NetValue:
                    return MI_AVMModelNameTypeEnumerated.NetValue;
                case E_sSpAvmModelT.Pass:
                    return MI_AVMModelNameTypeEnumerated.Pass;
                case E_sSpAvmModelT.PropertySurveyAnalysisReport:
                    return MI_AVMModelNameTypeEnumerated.PropertySurveyAnalysisReport;
                case E_sSpAvmModelT.ValueFinder:
                    return MI_AVMModelNameTypeEnumerated.ValueFinder;
                case E_sSpAvmModelT.ValuePoint:
                    return MI_AVMModelNameTypeEnumerated.ValuePoint;
                case E_sSpAvmModelT.ValuePoint4:
                    return MI_AVMModelNameTypeEnumerated.ValuePoint4;
                case E_sSpAvmModelT.ValuePointPlus:
                    return MI_AVMModelNameTypeEnumerated.ValuePointPlus;
                case E_sSpAvmModelT.ValueSure:
                    return MI_AVMModelNameTypeEnumerated.ValueSure;
                case E_sSpAvmModelT.ValueWizard:
                    return MI_AVMModelNameTypeEnumerated.ValueWizard;
                case E_sSpAvmModelT.ValueWizardPlus:
                    return MI_AVMModelNameTypeEnumerated.ValueWizardPlus;
                case E_sSpAvmModelT.VeroIndexPlus:
                    return MI_AVMModelNameTypeEnumerated.VeroIndexPlus;
                case E_sSpAvmModelT.VeroValue:
                    return MI_AVMModelNameTypeEnumerated.VeroValue;
                default:
                    throw new UnhandledEnumException(avmType);
            }
        }

        /// <summary>
        /// Converts a credit score model string to MISMO 2.3.1.
        /// </summary>
        /// <param name="creditScoreModel">The score model as a string.</param>
        /// <param name="otherModelName">If the score model does not belong to the enumerated type then it will be written to this string.</param>
        /// <returns>The MISMO 2.3.1 formatted credit score model.</returns>
        /// <remarks>ToMismoModelName is only used prior to loan version 26. After that ToMismoModelType is used.</remarks>
        public static MI_CreditScoreModelNameTypeEnumerated ToMismoModelName(string creditScoreModel, out string otherModelName)
        {
            otherModelName = string.Empty;
            creditScoreModel = string.IsNullOrEmpty(creditScoreModel) ? string.Empty : creditScoreModel.TrimWhitespaceAndBOM();

            if (string.IsNullOrEmpty(creditScoreModel))
            {
                return MI_CreditScoreModelNameTypeEnumerated.None;
            }

            if (Enum.IsDefined(typeof(MI_CreditScoreModelNameTypeEnumerated), creditScoreModel))
            {
                return (MI_CreditScoreModelNameTypeEnumerated)Enum.Parse(typeof(MI_CreditScoreModelNameTypeEnumerated), creditScoreModel);
            }
            else
            {
                otherModelName = creditScoreModel;
            }

            return MI_CreditScoreModelNameTypeEnumerated.Other;
        }

        /// <summary>
        /// Converts a credit score model to MISMO 2.3.1.
        /// </summary>
        /// <param name="creditScoreModel">The score model.</param>
        /// <returns>The MISMO 2.3.1 formatted credit score model.</returns>
        /// <remarks>ToMismoModelType is only used after loan version 26. Prior to that ToMismoModelName is used.</remarks>
        public static MI_CreditScoreModelNameTypeEnumerated ToMismoModelType(E_CreditScoreModelT creditScoreModel)
        {
            switch (creditScoreModel)
            {
                case E_CreditScoreModelT.LeaveBlank:
                    return MI_CreditScoreModelNameTypeEnumerated.None;
                case E_CreditScoreModelT.EquifaxBeacon:
                    return MI_CreditScoreModelNameTypeEnumerated.EquifaxBeacon;
                case E_CreditScoreModelT.EquifaxBeacon5:
                    return MI_CreditScoreModelNameTypeEnumerated.EquifaxBeacon5;
                case E_CreditScoreModelT.EquifaxBeacon5Auto:
                    return MI_CreditScoreModelNameTypeEnumerated.EquifaxBeacon5Auto;
                case E_CreditScoreModelT.EquifaxBeacon5BankCard:
                    return MI_CreditScoreModelNameTypeEnumerated.EquifaxBeacon5BankCard;
                case E_CreditScoreModelT.EquifaxBeacon5Installment:
                    return MI_CreditScoreModelNameTypeEnumerated.EquifaxBeacon5Installment;
                case E_CreditScoreModelT.EquifaxBeacon5PersonalFinance:
                    return MI_CreditScoreModelNameTypeEnumerated.EquifaxBeacon5PersonalFinance;
                case E_CreditScoreModelT.EquifaxBeaconAuto:
                    return MI_CreditScoreModelNameTypeEnumerated.EquifaxBeaconAuto;
                case E_CreditScoreModelT.EquifaxBeaconBankcard:
                    return MI_CreditScoreModelNameTypeEnumerated.EquifaxBeaconBankcard;
                case E_CreditScoreModelT.EquifaxBeaconInstallment:
                    return MI_CreditScoreModelNameTypeEnumerated.EquifaxBeaconInstallment;
                case E_CreditScoreModelT.EquifaxBeaconPersonalFinance:
                    return MI_CreditScoreModelNameTypeEnumerated.EquifaxBeaconPersonalFinance;
                case E_CreditScoreModelT.EquifaxDAS:
                    return MI_CreditScoreModelNameTypeEnumerated.EquifaxDAS;
                case E_CreditScoreModelT.EquifaxEnhancedBeacon:
                    return MI_CreditScoreModelNameTypeEnumerated.EquifaxEnhancedBeacon;
                case E_CreditScoreModelT.EquifaxEnhancedDAS:
                    return MI_CreditScoreModelNameTypeEnumerated.EquifaxEnhancedDAS;
                case E_CreditScoreModelT.EquifaxMarketMax:
                    return MI_CreditScoreModelNameTypeEnumerated.EquifaxMarketMax;
                case E_CreditScoreModelT.EquifaxMortgageScore:
                    return MI_CreditScoreModelNameTypeEnumerated.EquifaxMortgageScore;
                case E_CreditScoreModelT.EquifaxPinnacle:
                    return MI_CreditScoreModelNameTypeEnumerated.EquifaxPinnacle;
                case E_CreditScoreModelT.EquifaxPinnacle2:
                    return MI_CreditScoreModelNameTypeEnumerated.EquifaxPinnacle2;
                case E_CreditScoreModelT.ExperianFairIsaac:
                    return MI_CreditScoreModelNameTypeEnumerated.ExperianFairIsaac;
                case E_CreditScoreModelT.ExperianFairIsaacAdvanced:
                    return MI_CreditScoreModelNameTypeEnumerated.ExperianFairIsaacAdvanced;
                case E_CreditScoreModelT.ExperianFairIsaacAuto:
                    return MI_CreditScoreModelNameTypeEnumerated.ExperianFairIsaacAuto;
                case E_CreditScoreModelT.ExperianFairIsaacBankcard:
                    return MI_CreditScoreModelNameTypeEnumerated.ExperianFairIsaacBankcard;
                case E_CreditScoreModelT.ExperianFairIsaacInstallment:
                    return MI_CreditScoreModelNameTypeEnumerated.ExperianFairIsaacInstallment;
                case E_CreditScoreModelT.ExperianFairIsaacPersonalFinance:
                    return MI_CreditScoreModelNameTypeEnumerated.ExperianFairIsaacPersonalFinance;
                case E_CreditScoreModelT.ExperianMDSBankruptcyII:
                    return MI_CreditScoreModelNameTypeEnumerated.ExperianMDSBankruptcyII;
                case E_CreditScoreModelT.ExperianNewNationalEquivalency:
                    return MI_CreditScoreModelNameTypeEnumerated.ExperianNewNationalEquivalency;
                case E_CreditScoreModelT.ExperianNewNationalRisk:
                    return MI_CreditScoreModelNameTypeEnumerated.ExperianNewNationalRisk;
                case E_CreditScoreModelT.ExperianOldNationalRisk:
                    return MI_CreditScoreModelNameTypeEnumerated.ExperianOldNationalRisk;
                case E_CreditScoreModelT.TransUnionDelphi:
                    return MI_CreditScoreModelNameTypeEnumerated.TransUnionDelphi;
                case E_CreditScoreModelT.TransUnionEmpirica:
                    return MI_CreditScoreModelNameTypeEnumerated.TransUnionEmpirica;
                case E_CreditScoreModelT.TransUnionEmpiricaAuto:
                    return MI_CreditScoreModelNameTypeEnumerated.TransUnionEmpiricaAuto;
                case E_CreditScoreModelT.TransUnionEmpiricaBankcard:
                    return MI_CreditScoreModelNameTypeEnumerated.TransUnionEmpiricaBankcard;
                case E_CreditScoreModelT.TransUnionEmpiricaInstallment:
                    return MI_CreditScoreModelNameTypeEnumerated.TransUnionEmpiricaInstallment;
                case E_CreditScoreModelT.TransUnionEmpiricaPersonalFinance:
                    return MI_CreditScoreModelNameTypeEnumerated.TransUnionEmpiricaPersonalFinance;
                case E_CreditScoreModelT.TransUnionNewDelphi:
                    return MI_CreditScoreModelNameTypeEnumerated.TransUnionNewDelphi;
                case E_CreditScoreModelT.TransUnionPrecision:
                    return MI_CreditScoreModelNameTypeEnumerated.TransUnionPrecision;
                case E_CreditScoreModelT.Beacon09MortgageIndustryOption:
                case E_CreditScoreModelT.EquifaxBankruptcyNavigatorIndex02781:
                case E_CreditScoreModelT.EquifaxBankruptcyNavigatorIndex02782:
                case E_CreditScoreModelT.EquifaxBankruptcyNavigatorIndex02783:
                case E_CreditScoreModelT.EquifaxBankruptcyNavigatorIndex02784:
                case E_CreditScoreModelT.EquifaxVantageScore:
                case E_CreditScoreModelT.EquifaxVantageScore3:
                case E_CreditScoreModelT.ExperianFairIsaacAdvanced2:
                case E_CreditScoreModelT.ExperianFICOClassicV3:
                case E_CreditScoreModelT.ExperianScorexPLUS:
                case E_CreditScoreModelT.ExperianVantageScore:
                case E_CreditScoreModelT.ExperianVantageScore3:
                case E_CreditScoreModelT.FICOExpansionScore:
                case E_CreditScoreModelT.FICORiskScoreClassic04:
                case E_CreditScoreModelT.FICORiskScoreClassic98:
                case E_CreditScoreModelT.FICORiskScoreClassicAuto98:
                case E_CreditScoreModelT.FICORiskScoreClassicBankcard98:
                case E_CreditScoreModelT.FICORiskScoreClassicInstallmentLoan98:
                case E_CreditScoreModelT.FICORiskScoreClassicPersonalFinance98:
                case E_CreditScoreModelT.FICORiskScoreNextGen00:
                case E_CreditScoreModelT.FICORiskScoreNextGen03:
                case E_CreditScoreModelT.TransUnionPrecision03:
                case E_CreditScoreModelT.TransUnionVantageScore:
                case E_CreditScoreModelT.TransUnionVantageScore30:
                case E_CreditScoreModelT.VantageScore2:
                case E_CreditScoreModelT.VantageScore3:
                case E_CreditScoreModelT.MoreThanOneCreditScoringModel:
                case E_CreditScoreModelT.Other:
                    return MI_CreditScoreModelNameTypeEnumerated.Other;
                default:
                    throw new UnhandledEnumException(creditScoreModel);
            }
        }

        /// <summary>
        /// Determines the MI Application Type based on the request type and the delegated/non-delegated status of the requesting party.
        /// </summary>
        /// <param name="quote">True if the request is a quote request. False if the request is for a policy.</param>
        /// <param name="delegationType">Delegation type.</param>
        /// <returns>The MI Application Type for the request.</returns>
        public static MI_MIApplicationTypeEnumerated ToMismoMIApplicationType(bool quote, DelegationType delegationType)
        {
            if (quote)
            {
                return MI_MIApplicationTypeEnumerated.RateQuote;
            }

            switch (delegationType)
            {
                case DelegationType.Blank:
                    // This shouldn't be possible. For now, return none.
                    return MI_MIApplicationTypeEnumerated.None;
                case DelegationType.Delegated:
                    return MI_MIApplicationTypeEnumerated.Delegated;
                case DelegationType.NonDelegated:
                    return MI_MIApplicationTypeEnumerated.Standard;
                default:
                    throw new UnhandledEnumException(delegationType);
            }
        }

        /// <summary>
        /// Converts the given mortgage insurance type to MISMO 2.3.1 MI Duration Type.
        /// </summary>
        /// <param name="mortgageInsuranceType">The mortgage insurance option (BPMI monthly, BPMI single, etc).</param>
        /// <returns>The MI Duration Type converted from the given MI option.</returns>
        public static MI_MIDurationTypeEnumerated ToMismoMIDurationType(E_sProdConvMIOptionT mortgageInsuranceType)
        {
            switch (mortgageInsuranceType)
            {
                case E_sProdConvMIOptionT.BorrPaidMonPrem:
                    return MI_MIDurationTypeEnumerated.PeriodicMonthly;
                case E_sProdConvMIOptionT.BorrPaidSinglePrem:
                    return MI_MIDurationTypeEnumerated.SingleLifeOfLoan;
                case E_sProdConvMIOptionT.BorrPaidSplitPrem:
                    return MI_MIDurationTypeEnumerated.PeriodicMonthly;
                case E_sProdConvMIOptionT.LendPaidSinglePrem:
                    return MI_MIDurationTypeEnumerated.SingleLifeOfLoan;
                case E_sProdConvMIOptionT.NoMI:
                case E_sProdConvMIOptionT.Blank:
                    return MI_MIDurationTypeEnumerated.NotApplicable;
                default:
                    throw new UnhandledEnumException(mortgageInsuranceType);
            }
        }

        /// <summary>
        /// Converts the given mortgage insurance type to MISMO 2.3.1 MI Premium Payment Type.
        /// </summary>
        /// <param name="mortgageInsuranceType">The mortgage insurance option (BPMI monthly, BPMI single, etc).</param>
        /// <returns>The MI Premium Payment Type converted from the given MI option.</returns>
        public static MI_MIPremiumPaymentTypeEnumerated ToMismoMIPremiumPaymentType(E_sProdConvMIOptionT mortgageInsuranceType)
        {
            switch (mortgageInsuranceType)
            {
                case E_sProdConvMIOptionT.BorrPaidMonPrem:
                case E_sProdConvMIOptionT.BorrPaidSinglePrem:
                case E_sProdConvMIOptionT.BorrPaidSplitPrem:
                    return MI_MIPremiumPaymentTypeEnumerated.BorrowerPaid;
                case E_sProdConvMIOptionT.LendPaidSinglePrem:
                    return MI_MIPremiumPaymentTypeEnumerated.LenderPaid;
                case E_sProdConvMIOptionT.NoMI:
                case E_sProdConvMIOptionT.Blank:
                    return MI_MIPremiumPaymentTypeEnumerated.None;
                default:
                    throw new UnhandledEnumException(mortgageInsuranceType);
            }
        }

        /// <summary>
        /// Converts the given loan documentation type to MISMO 2.3.1 Reduced Loan Documentation Type.
        /// </summary>
        /// <param name="doctype">The loan documentation type (Full, Alt, Streamline, etc).</param>
        /// <returns>The reduced loan documentation type converted from the given documentation type.</returns>
        public static MI_MIReducedLoanDocumentationTypeEnumerated ToMismo(E_sProdDocT doctype)
        {
            switch (doctype)
            {
                case E_sProdDocT.Alt:
                case E_sProdDocT.Full:
                case E_sProdDocT.Light:
                    return MI_MIReducedLoanDocumentationTypeEnumerated.None;
                case E_sProdDocT.NINA:
                case E_sProdDocT.NINANE:
                    return MI_MIReducedLoanDocumentationTypeEnumerated.NoIncomeNoAsset;
                case E_sProdDocT.NISA:
                    return MI_MIReducedLoanDocumentationTypeEnumerated.StatedAssets;
                case E_sProdDocT.NIVA:
                case E_sProdDocT.NIVANE:
                    return MI_MIReducedLoanDocumentationTypeEnumerated.NoIncomeNoRatio;
                case E_sProdDocT.SISA:
                    return MI_MIReducedLoanDocumentationTypeEnumerated.StatedIncomeStatedAsset;
                case E_sProdDocT.SIVA:
                    return MI_MIReducedLoanDocumentationTypeEnumerated.StatedIncomeWithoutIRSForm4506;
                case E_sProdDocT.Streamline:
                case E_sProdDocT.VINA:
                case E_sProdDocT.VISA:
                    return MI_MIReducedLoanDocumentationTypeEnumerated.None;
                case E_sProdDocT._12MoPersonalBankStatements:
                case E_sProdDocT._24MoPersonalBankStatements:
                case E_sProdDocT._12MoBusinessBankStatements:
                case E_sProdDocT._24MoBusinessBankStatements:
                case E_sProdDocT.OtherBankStatements:
                case E_sProdDocT._1YrTaxReturns:
                case E_sProdDocT.Voe:
                    return MI_MIReducedLoanDocumentationTypeEnumerated.None;
                case E_sProdDocT.AssetUtilization:
                case E_sProdDocT.DebtServiceCoverage:
                case E_sProdDocT.NoIncome:
                    return MI_MIReducedLoanDocumentationTypeEnumerated.NoIncomeNoRatio;
                default:
                    throw new UnhandledEnumException(doctype);
            }
        }

        /// <summary>
        /// Converts the given Desktop Underwriter result to MISMO 2.3.1 Desktop Underwriter Recommendation Type.
        /// </summary>
        /// <param name="desktopUWResult">The underwriting result from DU.</param>
        /// <returns>The recommendation converted from the given DU result.</returns>
        public static MI_DesktopUnderwriterRecommendationTypeEnumerated ToMismoDURecommendationType(E_sProd3rdPartyUwResultT desktopUWResult)
        {
            switch (desktopUWResult)
            {
                case E_sProd3rdPartyUwResultT.DU_ApproveEligible: return MI_DesktopUnderwriterRecommendationTypeEnumerated.ApproveEligible;
                case E_sProd3rdPartyUwResultT.DU_ApproveIneligible: return MI_DesktopUnderwriterRecommendationTypeEnumerated.ApproveIneligible;
                case E_sProd3rdPartyUwResultT.DU_EAIEligible: return MI_DesktopUnderwriterRecommendationTypeEnumerated.ExpandedApproval1Eligible;
                case E_sProd3rdPartyUwResultT.DU_EAIIEligible: return MI_DesktopUnderwriterRecommendationTypeEnumerated.ExpandedApproval2Eligible;
                case E_sProd3rdPartyUwResultT.DU_EAIIIEligible: return MI_DesktopUnderwriterRecommendationTypeEnumerated.ExpandedApproval3Eligible;
                case E_sProd3rdPartyUwResultT.DU_ReferEligible: return MI_DesktopUnderwriterRecommendationTypeEnumerated.ReferEligible;
                case E_sProd3rdPartyUwResultT.DU_ReferIneligible: return MI_DesktopUnderwriterRecommendationTypeEnumerated.ReferIneligible;
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible: 
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible:
                    return MI_DesktopUnderwriterRecommendationTypeEnumerated.ReferWithCaution4;
                case E_sProd3rdPartyUwResultT.GUS_AcceptEligible:
                case E_sProd3rdPartyUwResultT.GUS_AcceptIneligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferEligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferIneligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferWCautionEligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferWCautionIneligible:
                case E_sProd3rdPartyUwResultT.LP_AcceptEligible:
                case E_sProd3rdPartyUwResultT.LP_AcceptIneligible:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level1:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level2:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level3:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level4:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level5:
                case E_sProd3rdPartyUwResultT.LP_CautionEligible:
                case E_sProd3rdPartyUwResultT.LP_CautionIneligible:
                case E_sProd3rdPartyUwResultT.Lp_Refer:
                case E_sProd3rdPartyUwResultT.NA:
                    return MI_DesktopUnderwriterRecommendationTypeEnumerated.None;
                case E_sProd3rdPartyUwResultT.OutOfScope:
                    return MI_DesktopUnderwriterRecommendationTypeEnumerated.OutOfScope;
                case E_sProd3rdPartyUwResultT.Total_ApproveEligible:
                case E_sProd3rdPartyUwResultT.Total_ApproveIneligible:
                case E_sProd3rdPartyUwResultT.Total_ReferEligible:
                case E_sProd3rdPartyUwResultT.Total_ReferIneligible:
                    return MI_DesktopUnderwriterRecommendationTypeEnumerated.None;
                default:
                    throw new UnhandledEnumException(desktopUWResult);
            }
        }

        /// <summary>
        /// Converts the given Loan Prospector result to MISMO 2.3.1 Freddie Mac Purchase Eligibility Type.
        /// </summary>
        /// <param name="loanProspectorResult">The underwriting result from Loan Prospector.</param>
        /// <returns>The purchase eligibility converted from the given LP result.</returns>
        public static MI_FreddieMacPurchaseEligibilityTypeEnumerated ToMismoFreddiePurchaseEligibilityType(E_sProd3rdPartyUwResultT loanProspectorResult)
        {
            switch (loanProspectorResult)
            {
                case E_sProd3rdPartyUwResultT.DU_ApproveEligible:
                case E_sProd3rdPartyUwResultT.DU_ApproveIneligible:
                case E_sProd3rdPartyUwResultT.DU_EAIEligible:
                case E_sProd3rdPartyUwResultT.DU_EAIIEligible:
                case E_sProd3rdPartyUwResultT.DU_EAIIIEligible:
                case E_sProd3rdPartyUwResultT.DU_ReferEligible:
                case E_sProd3rdPartyUwResultT.DU_ReferIneligible:
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible:
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible:
                case E_sProd3rdPartyUwResultT.GUS_AcceptEligible:
                case E_sProd3rdPartyUwResultT.GUS_AcceptIneligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferEligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferIneligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferWCautionEligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferWCautionIneligible:
                    return MI_FreddieMacPurchaseEligibilityTypeEnumerated.None;
                case E_sProd3rdPartyUwResultT.LP_AcceptEligible:
                    return MI_FreddieMacPurchaseEligibilityTypeEnumerated.FreddieMacEligible;
                case E_sProd3rdPartyUwResultT.LP_AcceptIneligible:
                    return MI_FreddieMacPurchaseEligibilityTypeEnumerated.FreddieMacIneligible;
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level1:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level2:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level3:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level4:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level5:
                    return MI_FreddieMacPurchaseEligibilityTypeEnumerated.FreddieMacEligibleLPAMinusOffering;
                case E_sProd3rdPartyUwResultT.LP_CautionEligible:
                    return MI_FreddieMacPurchaseEligibilityTypeEnumerated.FreddieMacEligible;
                case E_sProd3rdPartyUwResultT.LP_CautionIneligible:
                    return MI_FreddieMacPurchaseEligibilityTypeEnumerated.FreddieMacIneligible;
                case E_sProd3rdPartyUwResultT.Lp_Refer:
                    return MI_FreddieMacPurchaseEligibilityTypeEnumerated.None;
                case E_sProd3rdPartyUwResultT.NA:
                case E_sProd3rdPartyUwResultT.OutOfScope:
                case E_sProd3rdPartyUwResultT.Total_ApproveEligible:
                case E_sProd3rdPartyUwResultT.Total_ApproveIneligible:
                case E_sProd3rdPartyUwResultT.Total_ReferEligible:
                case E_sProd3rdPartyUwResultT.Total_ReferIneligible:
                    return MI_FreddieMacPurchaseEligibilityTypeEnumerated.None;
                default:
                    throw new UnhandledEnumException(loanProspectorResult);
            }
        }

        /// <summary>
        /// Converts the given Loan Prospector result to MISMO 2.3.1 Loan Prospector Credit Risk Classification Type.
        /// </summary>
        /// <param name="loanProspectorResult">The underwriting result from Loan Prospector.</param>
        /// <returns>The credit risk converted from the given LP result.</returns>
        public static MI_LoanProspectorCreditRiskClassificationTypeEnumerated ToMismoLoanProspectorCreditRiskClassType(E_sProd3rdPartyUwResultT loanProspectorResult)
        {
            switch (loanProspectorResult)
            {
                case E_sProd3rdPartyUwResultT.DU_ApproveEligible:
                case E_sProd3rdPartyUwResultT.DU_ApproveIneligible:
                case E_sProd3rdPartyUwResultT.DU_EAIEligible:
                case E_sProd3rdPartyUwResultT.DU_EAIIEligible:
                case E_sProd3rdPartyUwResultT.DU_EAIIIEligible:
                case E_sProd3rdPartyUwResultT.DU_ReferEligible:
                case E_sProd3rdPartyUwResultT.DU_ReferIneligible:
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible:
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible:
                case E_sProd3rdPartyUwResultT.GUS_AcceptEligible:
                case E_sProd3rdPartyUwResultT.GUS_AcceptIneligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferEligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferIneligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferWCautionEligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferWCautionIneligible:
                    return MI_LoanProspectorCreditRiskClassificationTypeEnumerated.None;
                case E_sProd3rdPartyUwResultT.LP_AcceptEligible:
                case E_sProd3rdPartyUwResultT.LP_AcceptIneligible:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level1:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level2:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level3:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level4:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level5:
                    return MI_LoanProspectorCreditRiskClassificationTypeEnumerated.Accept;
                case E_sProd3rdPartyUwResultT.LP_CautionEligible:
                case E_sProd3rdPartyUwResultT.LP_CautionIneligible:
                    return MI_LoanProspectorCreditRiskClassificationTypeEnumerated.Caution;
                case E_sProd3rdPartyUwResultT.Lp_Refer:
                    return MI_LoanProspectorCreditRiskClassificationTypeEnumerated.Refer;
                case E_sProd3rdPartyUwResultT.NA:
                case E_sProd3rdPartyUwResultT.OutOfScope:
                case E_sProd3rdPartyUwResultT.Total_ApproveEligible:
                case E_sProd3rdPartyUwResultT.Total_ApproveIneligible:
                case E_sProd3rdPartyUwResultT.Total_ReferEligible:
                case E_sProd3rdPartyUwResultT.Total_ReferIneligible:
                    return MI_LoanProspectorCreditRiskClassificationTypeEnumerated.None;
                default:
                    throw new UnhandledEnumException(loanProspectorResult);
            }
        }

        /// <summary>
        /// Converts the given Loan Prospector result to MISMO 2.3.1 Loan Prospector Credit Risk Classification Description.
        /// </summary>
        /// <param name="loanProspectorResult">The underwriting result from Loan Prospector.</param>
        /// <returns>The credit risk converted from the given LP result.</returns>
        public static string ToMismoLoanProspectorCreditRiskClassDescription(E_sProd3rdPartyUwResultT loanProspectorResult)
        {
            switch (loanProspectorResult)
            {
                case E_sProd3rdPartyUwResultT.DU_ApproveEligible:
                case E_sProd3rdPartyUwResultT.DU_ApproveIneligible:
                case E_sProd3rdPartyUwResultT.DU_EAIEligible:
                case E_sProd3rdPartyUwResultT.DU_EAIIEligible:
                case E_sProd3rdPartyUwResultT.DU_EAIIIEligible:
                case E_sProd3rdPartyUwResultT.DU_ReferEligible:
                case E_sProd3rdPartyUwResultT.DU_ReferIneligible:
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible:
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible:
                case E_sProd3rdPartyUwResultT.GUS_AcceptEligible:
                case E_sProd3rdPartyUwResultT.GUS_AcceptIneligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferEligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferIneligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferWCautionEligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferWCautionIneligible:
                    return string.Empty;
                case E_sProd3rdPartyUwResultT.LP_AcceptEligible:
                case E_sProd3rdPartyUwResultT.LP_AcceptIneligible:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level1:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level2:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level3:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level4:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level5:
                    return "Accept";
                case E_sProd3rdPartyUwResultT.LP_CautionEligible:
                case E_sProd3rdPartyUwResultT.LP_CautionIneligible:
                    return "Caution";
                case E_sProd3rdPartyUwResultT.Lp_Refer:
                    return "Refer";
                case E_sProd3rdPartyUwResultT.NA:
                case E_sProd3rdPartyUwResultT.OutOfScope:
                case E_sProd3rdPartyUwResultT.Total_ApproveEligible:
                case E_sProd3rdPartyUwResultT.Total_ApproveIneligible:
                case E_sProd3rdPartyUwResultT.Total_ReferEligible:
                case E_sProd3rdPartyUwResultT.Total_ReferIneligible:
                    return string.Empty;
                default:
                    throw new UnhandledEnumException(loanProspectorResult);
            }
        }

        /// <summary>
        /// Converts the given documentation class ($$sLpDocClass$$) to MISMO 2.3.1 Loan Prospector Documentation Classification Type.
        /// </summary>
        /// <param name="docClass">The documentation class returned by Loan Prospector.</param>
        /// <returns>The documentation class converted to Loan Prospector Documentation Classification Type.</returns>
        public static MI_LoanProspectorDocumentationClassificationTypeEnumerated ToMismoLoanProspectorDocClassType(string docClass)
        {
            docClass = docClass.ToLower().TrimWhitespaceAndBOM();

            if (docClass.Equals("accept", StringComparison.OrdinalIgnoreCase)
                || docClass.Equals("streamlined accept", StringComparison.OrdinalIgnoreCase)
                || docClass.Equals("standard", StringComparison.OrdinalIgnoreCase))
            {
                return MI_LoanProspectorDocumentationClassificationTypeEnumerated.Accept;
            }
            else if (docClass.Equals("accept plus", StringComparison.OrdinalIgnoreCase))
            {
                return MI_LoanProspectorDocumentationClassificationTypeEnumerated.AcceptPlus;
            }
            else if (docClass.Equals("caution", StringComparison.OrdinalIgnoreCase))
            {
                return MI_LoanProspectorDocumentationClassificationTypeEnumerated.Caution;
            }
            else if (docClass.Equals("n/a", StringComparison.OrdinalIgnoreCase)
                || docClass.Equals("na", StringComparison.OrdinalIgnoreCase))
            {
                return MI_LoanProspectorDocumentationClassificationTypeEnumerated.NA;
            }
            else if (docClass.Equals("refer", StringComparison.OrdinalIgnoreCase))
            {
                return MI_LoanProspectorDocumentationClassificationTypeEnumerated.Refer;
            }

            return MI_LoanProspectorDocumentationClassificationTypeEnumerated.None;
        }

        /// <summary>
        /// Determines the name of the automated underwriting system that was used to produce the underwriting recommendation.
        /// </summary>
        /// <param name="desktopUnderwriter">The $$DU$$ indicator on the 1008 ($$sIsDuUw$$).</param>
        /// <param name="loanProspector">The $$LP$$ indicator on the 1008 ($$sIsLpUw$$).</param>
        /// <param name="other">The other AUS indicator on the 1008 ($$sIsOtherUw$$).</param>
        /// <param name="otherDescription">The other AUS description on the 1008 ($$sOtherUwDesc$$).</param>
        /// <returns>The name of the automated underwriting system.</returns>
        public static string ToMismoAUSName(bool desktopUnderwriter, bool loanProspector, bool other, string otherDescription)
        {
            string name = string.Empty;

            if (desktopUnderwriter)
            {
                name = "Desktop Underwriter";
            }
            else if (loanProspector)
            {
                name = "Loan Prospector";
            }
            else if (other)
            {
                name = otherDescription;
            }

            return name;
        }

        /// <summary>
        /// Determines the MISMO 2.3.1 AUS result value on the loan file.
        /// </summary>
        /// <param name="ratedAcceptedByTotalScorecard">Was the loan rated accept by Total Scorecard ($$sFHARatedAcceptedByTotalScorecard$$).</param>
        /// <param name="ratedReferByTotalScorecard">Was the loan file rated refer by Total Scorecard ($$sFHARatedReferByTotalScorecard$$).</param>
        /// <param name="thirdPartyUnderwritingResult">The AUS result on the loan file ($$sProd3rdPartyUwResultT$$).</param>
        /// <returns>Returns the AUS result value as a string.</returns>
        public static string ToMismoAUSResult(bool ratedAcceptedByTotalScorecard, bool ratedReferByTotalScorecard, E_sProd3rdPartyUwResultT thirdPartyUnderwritingResult)
        {
            string result = string.Empty;

            if (ratedAcceptedByTotalScorecard)
            {
                result = "AA";
            }
            else if (ratedReferByTotalScorecard)
            {
                result = "Refer";
            }
            else
            {
                switch (thirdPartyUnderwritingResult)
                {
                    case E_sProd3rdPartyUwResultT.DU_ApproveEligible:
                    case E_sProd3rdPartyUwResultT.Total_ApproveEligible:
                        result = "ApproveEligible";
                        break;
                    case E_sProd3rdPartyUwResultT.DU_ApproveIneligible:
                    case E_sProd3rdPartyUwResultT.Total_ApproveIneligible:
                        result = "ApproveIneligible";
                        break;
                    case E_sProd3rdPartyUwResultT.DU_EAIEligible:
                        result = "EAIEligible";
                        break;
                    case E_sProd3rdPartyUwResultT.DU_EAIIEligible:
                        result = "EAIIEligible";
                        break;
                    case E_sProd3rdPartyUwResultT.DU_EAIIIEligible:
                        result = "EAIIIEligible";
                        break;
                    case E_sProd3rdPartyUwResultT.DU_ReferEligible:
                    case E_sProd3rdPartyUwResultT.Total_ReferEligible:
                    case E_sProd3rdPartyUwResultT.GUS_ReferEligible:
                        result = "ReferEligible";
                        break;
                    case E_sProd3rdPartyUwResultT.DU_ReferIneligible:
                    case E_sProd3rdPartyUwResultT.Total_ReferIneligible:
                    case E_sProd3rdPartyUwResultT.GUS_ReferIneligible:
                        result = "ReferIneligible";
                        break;
                    case E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible:
                    case E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible:
                        result = "ReferWithCautionIV";
                        break;
                    case E_sProd3rdPartyUwResultT.LP_AcceptEligible:
                    case E_sProd3rdPartyUwResultT.LP_AcceptIneligible:
                        result = "Accept";
                        break;
                    case E_sProd3rdPartyUwResultT.Lp_AMinus_Level1:
                    case E_sProd3rdPartyUwResultT.Lp_AMinus_Level2:
                    case E_sProd3rdPartyUwResultT.Lp_AMinus_Level3:
                    case E_sProd3rdPartyUwResultT.Lp_AMinus_Level4:
                    case E_sProd3rdPartyUwResultT.Lp_AMinus_Level5:
                        result = "CautionEligibleForAMinus";
                        break;
                    case E_sProd3rdPartyUwResultT.LP_CautionEligible:
                    case E_sProd3rdPartyUwResultT.LP_CautionIneligible:
                        result = "C1Caution";
                        break;
                    case E_sProd3rdPartyUwResultT.Lp_Refer:
                        result = "Unknown";
                        break;
                    case E_sProd3rdPartyUwResultT.GUS_AcceptEligible:
                        result = "AcceptEligible";
                        break;
                    case E_sProd3rdPartyUwResultT.GUS_AcceptIneligible:
                        result = "AcceptIneligible";
                        break;
                    case E_sProd3rdPartyUwResultT.GUS_ReferWCautionEligible:
                    case E_sProd3rdPartyUwResultT.GUS_ReferWCautionIneligible:
                        result = "ReferWithCaution";
                        break;
                    case E_sProd3rdPartyUwResultT.OutOfScope:
                        result = "OutofScope";
                        break;
                    case E_sProd3rdPartyUwResultT.NA:
                        result = string.Empty;
                        break;
                    default:
                        throw new UnhandledEnumException(thirdPartyUnderwritingResult);
                }
            }

            return result;
        }

        /// <summary>
        /// Determines the MISMO 2.3.1 renewal calculation type from the MI premium basis and type.
        /// </summary>
        /// <param name="premiumBasis">The basis for the MI premium ($$sProMInsT$$).</param>
        /// <param name="mioption">The type of MI premium ($$sProdConvMIOptionT$$).</param>
        /// <returns>The renewal calculation type converted from the given loan values.</returns>
        public static MI_MIRenewalCalculationTypeEnumerated ToMismo(E_PercentBaseT premiumBasis, E_sProdConvMIOptionT mioption)
        {
            MI_MIRenewalCalculationTypeEnumerated renewal = MI_MIRenewalCalculationTypeEnumerated.NoRenewals;

            if (premiumBasis != E_PercentBaseT.DecliningRenewalsAnnually
                && premiumBasis != E_PercentBaseT.DecliningRenewalsMonthly
                && (mioption == E_sProdConvMIOptionT.BorrPaidMonPrem || mioption == E_sProdConvMIOptionT.BorrPaidSplitPrem))
            {
                renewal = MI_MIRenewalCalculationTypeEnumerated.Constant;
            }
            else if ((premiumBasis == E_PercentBaseT.DecliningRenewalsMonthly || premiumBasis == E_PercentBaseT.DecliningRenewalsAnnually)
                && (mioption == E_sProdConvMIOptionT.BorrPaidMonPrem || mioption == E_sProdConvMIOptionT.BorrPaidSplitPrem))
            {
                renewal = MI_MIRenewalCalculationTypeEnumerated.Declining;
            }

            return renewal;
        }

        /// <summary>
        /// Converts the loan's business channel to the MISMO loan originator type.
        /// </summary>
        /// <param name="channel">The business channel.</param>
        /// <returns>The loan originator type converted from the channel.</returns>
        public static LoanOriginatorType ToMismo(E_BranchChannelT channel)
        {
            switch (channel)
            {
                case E_BranchChannelT.Blank:
                    return LoanOriginatorType.None;
                case E_BranchChannelT.Broker:
                    return LoanOriginatorType.Broker;
                case E_BranchChannelT.Correspondent:
                    return LoanOriginatorType.Correspondent;
                case E_BranchChannelT.Retail:
                    return LoanOriginatorType.Lender;
                case E_BranchChannelT.Wholesale:
                    return LoanOriginatorType.Broker;
                default:
                    throw new UnhandledEnumException(channel);
            }
        }

        /// <summary>
        /// Converts the given boolean to MISMO 2.3.1 yes/no indicator.
        /// </summary>
        /// <param name="value">The boolean to be converted.</param>
        /// <returns>A MISMO 2.3.1 YNIndicator converted from the given value.</returns>
        public static E_YNIndicator ToMismo(bool value)
        {
            return value ? E_YNIndicator.Y : E_YNIndicator.N;
        }

        /// <summary>
        /// Converts the given Y/N value to a boolean. Assumes false if the input is null/empty/unexpected.
        /// </summary>
        /// <param name="value">The Y/N value as a string.</param>
        /// <returns>True if Y. Otherwise false.</returns>
        public static bool ParseYNValue(string value)
        {
            bool result = false;

            if (!string.IsNullOrEmpty(value))
            {
                try
                {
                    result = (E_YNIndicator)Enum.Parse(typeof(E_YNIndicator), value, true) == E_YNIndicator.Y;
                }
                catch (ArgumentException)
                {
                }
                catch (OverflowException)
                {
                }
            }

            return result;
        }

        /// <summary>
        /// Indicates whether the vendor response was in response to a quote order. The response could either belong to a quote order or a policy order.
        /// </summary>
        /// <param name="applicationType">The mortgage insurance application type included in the vendor response.</param>
        /// <returns>A value indicating whether the vendor response was a response to a quote order.</returns>
        public static bool IsQuote(MI_MIApplicationTypeEnumerated applicationType)
        {
            switch (applicationType)
            {
                case MI_MIApplicationTypeEnumerated.Delegated:
                    return false;
                case MI_MIApplicationTypeEnumerated.None:
                    return true;
                case MI_MIApplicationTypeEnumerated.Prequalification:
                    return false;
                case MI_MIApplicationTypeEnumerated.RateQuote:
                    return true;
                case MI_MIApplicationTypeEnumerated.Standard:
                    return false;
                default:
                    throw new UnhandledEnumException(applicationType);
            }
        }

        /// <summary>
        /// Indicates whether the vendor approved the MI request.
        /// </summary>
        /// <param name="decision">The decision type returned by the MI vendor.</param>
        /// <returns>True if the vendor returned any type of approval. False otherwise.</returns>
        public static bool Approved(MI_MIDecisionTypeEnumerated decision)
        {
            switch (decision)
            {
                case MI_MIDecisionTypeEnumerated.Approved:
                case MI_MIDecisionTypeEnumerated.ApprovedAfterReevaluation:
                case MI_MIDecisionTypeEnumerated.ConditionedApproval:
                    return true;
                case MI_MIDecisionTypeEnumerated.Declined:
                case MI_MIDecisionTypeEnumerated.None:
                case MI_MIDecisionTypeEnumerated.Suspended:
                    return false;
                default:
                    throw new UnhandledEnumException(decision);
            }
        }

        /// <summary>
        /// Converts the payment type included in the MI response to the LQB MI insurance type.
        /// </summary>
        /// <param name="paymentType">The payment type included in the MI response.</param>
        /// <returns>The LQB MI insurance type converted from the MISMO 2.3.1 MI Premium Payment type.</returns>
        public static E_sMiInsuranceT MiInsuranceType(MI_MIPremiumPaymentTypeEnumerated paymentType)
        {
            switch (paymentType)
            {
                case MI_MIPremiumPaymentTypeEnumerated.BorrowerPaid:
                case MI_MIPremiumPaymentTypeEnumerated.BothBorrowerAndLenderPaid:
                    return E_sMiInsuranceT.BorrowerPaid;
                case MI_MIPremiumPaymentTypeEnumerated.LenderPaid:
                    return E_sMiInsuranceT.LenderPaid;
                case MI_MIPremiumPaymentTypeEnumerated.None:
                    return E_sMiInsuranceT.None;
                default:
                    throw new UnhandledEnumException(paymentType);
            }
        }

        /// <summary>
        /// Converts the MI duration, payment type, and split premium indicator to an LQB MI option type <see cref="E_sProdConvMIOptionT"/>.
        /// </summary>
        /// <param name="duration">The MI duration type.</param>
        /// <param name="paymentType">The MI payment type.</param>
        /// <param name="splitPremium">True if the MI response included a split premium indicator. Otherwise false.</param>
        /// <param name="resolved">True if the MI option could be resolved. Otherwise false.</param>
        /// <returns>The MI option type converted from the given values.</returns>
        public static E_sProdConvMIOptionT MIOptionType(MI_MIDurationTypeEnumerated duration, MI_MIPremiumPaymentTypeEnumerated paymentType, bool splitPremium, out bool resolved)
        {
            E_sMiInsuranceT insuranceType = MiInsuranceType(paymentType);
            resolved = false;

            switch (duration)
            {
                case MI_MIDurationTypeEnumerated.Annual:
                case MI_MIDurationTypeEnumerated.None:
                case MI_MIDurationTypeEnumerated.NotApplicable:
                    return E_sProdConvMIOptionT.NoMI;
                case MI_MIDurationTypeEnumerated.PeriodicMonthly:
                    if (insuranceType == E_sMiInsuranceT.BorrowerPaid)
                    {
                        resolved = true;
                        return splitPremium ? E_sProdConvMIOptionT.BorrPaidSplitPrem : E_sProdConvMIOptionT.BorrPaidMonPrem;
                    }
                    else
                    {
                        return E_sProdConvMIOptionT.NoMI;
                    }

                case MI_MIDurationTypeEnumerated.SingleLifeOfLoan:
                    if (insuranceType == E_sMiInsuranceT.BorrowerPaid)
                    {
                        resolved = true;
                        return E_sProdConvMIOptionT.BorrPaidSinglePrem;
                    }
                    else if (insuranceType == E_sMiInsuranceT.LenderPaid)
                    {
                        resolved = true;
                        return E_sProdConvMIOptionT.LendPaidSinglePrem;
                    }
                    else
                    {
                        return E_sProdConvMIOptionT.NoMI;
                    }

                case MI_MIDurationTypeEnumerated.SingleSpecific:
                    return E_sProdConvMIOptionT.NoMI;
                default:
                    throw new UnhandledEnumException(duration);
            }
        }

        /// <summary>
        /// Converts the MI duration and renewal calculation to the LQB monthly MI basis ($$sProMInsT$$).
        /// </summary>
        /// <param name="duration">The MI duration type.</param>
        /// <param name="renewal">The renewal calculation type.</param>
        /// <returns>The monthly MI basis converted from the duration and renewal calculation type.</returns>
        public static E_PercentBaseT MonthlyMIBasis(MI_MIDurationTypeEnumerated duration, MI_MIRenewalCalculationTypeEnumerated renewal)
        {
            switch (duration)
            {
                case MI_MIDurationTypeEnumerated.Annual:
                    if (renewal == MI_MIRenewalCalculationTypeEnumerated.Constant)
                    {
                        return E_PercentBaseT.LoanAmount;
                    }
                    else if (renewal == MI_MIRenewalCalculationTypeEnumerated.Declining)
                    {
                        return E_PercentBaseT.DecliningRenewalsAnnually;
                    }
                    else
                    {
                        return E_PercentBaseT.LoanAmount;
                    }

                case MI_MIDurationTypeEnumerated.None:
                case MI_MIDurationTypeEnumerated.NotApplicable:
                    return E_PercentBaseT.LoanAmount;
                case MI_MIDurationTypeEnumerated.PeriodicMonthly:
                    return renewal == MI_MIRenewalCalculationTypeEnumerated.Declining ? E_PercentBaseT.DecliningRenewalsMonthly : E_PercentBaseT.LoanAmount;
                case MI_MIDurationTypeEnumerated.SingleLifeOfLoan:
                case MI_MIDurationTypeEnumerated.SingleSpecific:
                    return E_PercentBaseT.LoanAmount;
                default:
                    throw new UnhandledEnumException(duration);
            }
        }

        /// <summary>
        /// Converts the given key name to an LQB defined MIKey type.
        /// </summary>
        /// <param name="name">The name of the MI key name/value pair.</param>
        /// <returns>The enumerated type of the given key.</returns>
        public static MIKey KeyName(string name)
        {
            name = string.IsNullOrEmpty(name) ? string.Empty : name.TrimWhitespaceAndBOM();

            if (!string.IsNullOrEmpty(name))
            {
                if (Enum.IsDefined(typeof(MIKey), name))
                {
                    return (MIKey)Enum.Parse(typeof(MIKey), name, true);
                }
            }

            return MIKey.None;
        }
    }
}
