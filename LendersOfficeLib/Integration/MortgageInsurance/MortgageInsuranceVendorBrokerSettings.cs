﻿// <copyright file="MortgageInsuranceVendorBrokerSettings.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Isaac Ribakoff
//  Date:   9/17/2014 1:31:51 PM
// </summary>
namespace LendersOffice.Integration.MortgageInsurance
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Represents a lender's Mortgage Insurance Vendor (PMI Provider) settings.
    /// </summary>
    public class MortgageInsuranceVendorBrokerSettings
    {
        /// <summary>
        /// The identifier for the encryption key used to encrypt <see cref="Password"/>. 
        /// </summary>
        private EncryptionKeyIdentifier encryptionKeyId;

        /// <summary>
        /// A lazy value of the password, used to delay decryption and loading until necessary.
        /// </summary>
        private Lazy<string> lazyPassword;

        /// <summary>
        /// Initializes a new instance of the <see cref="MortgageInsuranceVendorBrokerSettings" /> class.
        /// </summary>
        /// <param name="vendorId">MI Vendor ID to be associated with this broker setting instance.</param>
        /// <param name="brokerId">Broker ID to be associated with this broker setting instance.</param>
        public MortgageInsuranceVendorBrokerSettings(Guid vendorId, Guid brokerId)
        {
            this.VendorId = vendorId;
            this.BrokerId = brokerId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MortgageInsuranceVendorBrokerSettings" /> class from database.
        /// </summary>
        /// <param name="reader"><see cref="IDataRecord" /> containing column values to create <see cref="MortgageInsuranceVendorBrokerSettings" /> object.</param>
        /// <param name="brokerId">Unique identifier associated with the broker account.</param>
        private MortgageInsuranceVendorBrokerSettings(IDataRecord reader, Guid brokerId)
        {
            this.VendorId = (Guid)reader["VendorId"];
            this.BrokerId = brokerId;
            this.IsProduction = (bool)reader["IsProduction"];
            this.UserName = (string)reader["UserName"];
            byte[] passwordBytes = (byte[])reader["Password"];
            EncryptionKeyIdentifier? encryptionKeyId = EncryptionKeyIdentifier.Create((Guid)reader["EncryptionKeyId"]);

            // the DB life cycle for this object begins in SetEntry, before we make any encryption key
            if (encryptionKeyId.HasValue)
            {
                this.lazyPassword = new Lazy<string>(() => Drivers.Encryption.EncryptionHelper.DecryptString(encryptionKeyId.Value, passwordBytes));
                this.encryptionKeyId = encryptionKeyId.Value;
            }

            this.AccountId = (string)reader["AccountId"];
            this.PolicyId = (string)reader["PolicyId"];
            this.IsDelegated = (bool)reader["IsDelegated"]; 
        }

        /// <summary>Gets MI Vendor ID of this Broker Setting instance.</summary>
        /// <value>Unique Identifier representing MI Vendor ID of this Broker Setting instance.</value>
        public Guid VendorId { get; private set; }

        /// <summary>Gets the lender account (Broker ID) of this Broker Setting instance.</summary>
        /// <value>Unique identifier representing the lender account (Broker ID) of this Broker Setting instance.</value>
        public Guid BrokerId { get; private set; }

        /// <summary>Gets or sets a value indicating whether lender can submit to MI Vendor's production environment.</summary>
        /// <value>Boolean indicating whether lender can submit to MI Vendor's production environment.</value>
        public bool IsProduction { get; set; }

        /// <summary>Gets or sets the UserName of this Broker Setting instance.</summary>
        /// <value>String representing the UserName of this Broker Setting instance.</value>
        public string UserName { get; set; }

        /// <summary>Gets or sets the Password of this Broker Setting instance.</summary>
        /// <value>String representing the Password of this Broker Setting instance.</value>
        public string Password
        {
            get
            {
                return this.lazyPassword?.Value ?? string.Empty;
            }

            set
            {
                if (value == ConstAppDavid.FakePasswordDisplay || value.ToCharArray().All(c => c == '*'))
                {
                    return;
                }

                this.lazyPassword = new Lazy<string>(() => value);
            }
        }

        /// <summary>Gets or sets the Account ID of this Broker Setting instance.</summary>
        /// <value>String representing the Account ID of this Broker Setting instance.</value>
        public string AccountId { get; set; }

        /// <summary>Gets or sets the Policy ID of this Broker Setting instance.</summary>
        /// <value>String representing the Policy ID of this Broker Setting instance.</value>
        public string PolicyId { get; set; }

        /// <summary>Gets or sets a value indicating whether lender can submit delegated policy orders for this instance.</summary>
        /// <value>Boolean indicating whether lender can submit delegated policy orders for this instance.</value>
        public bool IsDelegated { get; set; }

        /// <summary>
        /// Retrieves a list of Active MI Vendors Associated with lender (broker) account.
        /// </summary>
        /// <param name="brokerId">BrokerId associated with lender account.</param>
        /// <returns>List of Active MI Vendors Associated with lender (broker) account.</returns>
        public static IEnumerable<MortgageInsuranceVendorBrokerSettings> ListActiveVendorByBrokerId(Guid brokerId)
        {
            List<MortgageInsuranceVendorBrokerSettings> list = new List<MortgageInsuranceVendorBrokerSettings>();
            SqlParameter[] parameters = 
                                        {
                                            new SqlParameter("@BrokerId", brokerId)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "MORTGAGE_INSURANCE_VENDOR_BROKER_SETTINGS_ListActiveVendors", parameters))
            {
                while (reader.Read())
                {
                    list.Add(new MortgageInsuranceVendorBrokerSettings(reader, brokerId));
                }
            }

            return list;
        }

        /// <summary>
        /// Updates or deletes Broker setting in database.
        /// </summary>
        /// <param name="vendorId">MI Vendor unique identifier.</param>
        /// <param name="brokerId">BrokerId associated with lender account.</param>
        /// <param name="setting">Determines whether to delete entry, update to test environment, or production.</param>
        public static void SetEntry(Guid vendorId, Guid brokerId, E_MIVendorBrokerSettingStatusT setting)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@VendorId", vendorId));
            parameters.Add(new SqlParameter("@BrokerId", brokerId));
            string storedProc = string.Empty;
            
            switch (setting)
            {
                case E_MIVendorBrokerSettingStatusT.Disabled:
                    storedProc = "MORTGAGE_INSURANCE_VENDOR_BROKER_SETTINGS_ClearBrokerCredential";
                    break;
                case E_MIVendorBrokerSettingStatusT.Test:
                    storedProc = "MORTGAGE_INSURANCE_VENDOR_BROKER_SETTINGS_SetEntry";
                    parameters.Add(new SqlParameter("@IsProduction", false));
                    break;
                case E_MIVendorBrokerSettingStatusT.Production:
                    storedProc = "MORTGAGE_INSURANCE_VENDOR_BROKER_SETTINGS_SetEntry";
                    parameters.Add(new SqlParameter("@IsProduction", true));
                    break;
                default:
                    throw new UnhandledEnumException(setting);
            }

            StoredProcedureHelper.ExecuteNonQuery(brokerId, storedProc, 3, parameters);
        }

        /// <summary>
        /// Retrieves lender settings.
        /// </summary>
        /// <param name="brokerId">Broker id.</param>
        /// <param name="vendorId">Vendor id.</param>
        /// <returns>MI vendor branch settings.</returns>
        public static MortgageInsuranceVendorBrokerSettings RetrieveLenderSettings(Guid brokerId, Guid vendorId)
        {
            try
            {
                return ListActiveVendorByBrokerId(brokerId).FirstOrDefault(vendor => vendor.VendorId == vendorId);
            }
            catch (ArgumentNullException)
            {
            }
            catch (InvalidOperationException)
            {
            }

            return null;
        }

        /// <summary>
        /// Saves broker login credentials for given MI Vendor instance to database.
        /// </summary>
        public void SaveLogin()
        {
            if (this.encryptionKeyId == default(EncryptionKeyIdentifier))
            {
                this.encryptionKeyId = Drivers.Encryption.EncryptionHelper.GenerateNewKey();
            }

            SqlParameter[] parameters = 
            {
                new SqlParameter("@VendorId", this.VendorId),
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@UserName", this.UserName),
                new SqlParameter("@Password", Drivers.Encryption.EncryptionHelper.EncryptString(this.encryptionKeyId, this.Password)),
                new SqlParameter("@EncryptionKeyId", this.encryptionKeyId.Value),
                new SqlParameter("@AccountId", this.AccountId),
                new SqlParameter("@PolicyId", this.PolicyId),
                new SqlParameter("@IsDelegated", this.IsDelegated)
            };
            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "MORTGAGE_INSURANCE_VENDOR_BROKER_SETTINGS_SaveLogin", 3, parameters);
        }        
    }
}