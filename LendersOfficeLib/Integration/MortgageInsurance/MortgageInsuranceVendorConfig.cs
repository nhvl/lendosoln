﻿// <copyright file="MortgageInsuranceVendorConfig.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Isaac Ribakoff
//  Date:   9/16/2014 04:17:00 PM 
// </summary>
namespace LendersOffice.Integration.MortgageInsurance
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Represents a Mortgage Insurance Vendor (PMI Provider).
    /// Vendor instances are defined by VendorId. This can be adopted for multiple vendors of same MI company if needed.
    /// </summary>
    public class MortgageInsuranceVendorConfig
    {
        /// <summary>
        /// A lazy initializer for the split premium options associated with the vendor, if any.
        /// </summary>
        private Lazy<List<MISplitPremiumOption>> splitPremiumOptionsLazyInitializer;

        /// <summary>
        /// Initializes a new instance of the <see cref="MortgageInsuranceVendorConfig" /> class.
        /// </summary>
        /// <param name="vendorType">The MI Vendor Company.</param>
        /// <param name="exportPath_Test">URL of test environment.</param>
        /// <param name="exportPath_Production">URL of production environment.</param>
        /// <param name="isUseAccountId">Determines whether account ID is required.</param>
        /// <param name="sendAuthTicketWithOrders">Determines whether vendor will receive Generic Framework authenticated ticket.</param>
        /// <param name="enableConnectionTest">Whether to run associated connection unit tests for the vendor.</param>
        /// <param name="usesSplitPremiumPlans">Indicates whether the vendor uses split premium plans.</param>
        /// <param name="splitPremiumOptions">The split premium options offered by the vendor, if applicable.</param>
        public MortgageInsuranceVendorConfig(
            E_sMiCompanyNmT vendorType,
            string exportPath_Test,
            string exportPath_Production,
            bool isUseAccountId,
            bool sendAuthTicketWithOrders,
            bool enableConnectionTest,
            bool usesSplitPremiumPlans,
            List<MISplitPremiumOption> splitPremiumOptions = null)
        {
            this.VendorType = vendorType;
            this.IsValid = true;
            this.ExportPath_Test = exportPath_Test;
            this.ExportPath_Production = exportPath_Production;
            this.IsUseAccountId = isUseAccountId;
            this.SendAuthTicketWithOrders = sendAuthTicketWithOrders;
            this.EnableConnectionTest = enableConnectionTest;
            this.UsesSplitPremiumPlans = usesSplitPremiumPlans;
            this.SplitPremiumOptions = splitPremiumOptions ?? new List<MISplitPremiumOption>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MortgageInsuranceVendorConfig" /> class.
        /// </summary>
        /// <param name="reader"><see cref="DbDataReader" /> containing column values to create <see cref="MortgageInsuranceVendorConfig" /> object.</param>
        private MortgageInsuranceVendorConfig(DbDataReader reader)
        {
            this.VendorId = (Guid)reader["VendorId"];
            this.VendorType = (E_sMiCompanyNmT)reader["VendorType"];
            this.IsValid = (bool)reader["IsValid"];
            this.ExportPath_Test = (string)reader["ExportPath_Test"];
            this.ExportPath_Production = (string)reader["ExportPath_Production"];
            this.IsUseAccountId = (bool)reader["IsUseAccountId"];
            this.SendAuthTicketWithOrders = (bool)reader["SendAuthTicketWithOrders"];
            this.EnableConnectionTest = (bool)reader["EnableConnectionTest"];
            this.UsesSplitPremiumPlans = (bool)reader["UsesSplitPremiumPlans"];
            this.splitPremiumOptionsLazyInitializer = new Lazy<List<MISplitPremiumOption>>(() => MISplitPremiumOption.RetrieveByVendor(this.VendorId));
        }

        /// <summary>Gets the ID of this particular MI Vendor instance.</summary>
        /// <value>Unique identifier representing ID of this particular MI Vendor instance.</value>
        public Guid VendorId { get; private set; }

        /// <summary>Gets or sets the value representing the MI Company associated with this vendor instance.</summary>
        /// <value>Enumeration representing the MI Company associated with this vendor instance.</value>
        public E_sMiCompanyNmT VendorType { get; set; }

        /// <summary>Gets the value representing the MI Company associated with this vendor instance as a user-friendly string.</summary>
        /// <value>MI Company as user-friendly string.</value>
        public string VendorTypeFriendlyDisplay => Tools.Get_sMiCompanyNmTFriendlyDisplay(this.VendorType);
        
        /// <summary>Gets or sets a value indicating whether this vendor instance is available to lenders (brokers).</summary>
        /// <value>Boolean indicating whether this vendor instance is available to lenders (brokers).</value>
        public bool IsValid { get; set; }

        /// <summary>Gets or sets URL of vendor instance test environment.</summary>
        /// <value>String representing URL of vendor instance test environment.</value>
        public string ExportPath_Test { get; set; }
        
        /// <summary>Gets or sets URL of vendor instance production environment.</summary>
        /// <value>String representing URL of vendor instance production environment.</value>
        public string ExportPath_Production { get; set; }

        /// <summary>Gets or sets a value indicating whether account ID is required for this vendor instance.</summary>
        /// <value>Boolean indicating whether account ID is required for this vendor instance.</value>
        public bool IsUseAccountId { get; set; }

        /// <summary>Gets or sets a value indicating whether Generic Framework authenticated ticket is included in MISMO request.</summary>
        /// <value>Boolean indicating whether Generic Framework authenticated ticket is included in MISMO request.</value>
        public bool SendAuthTicketWithOrders { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to enable connection tests involving this vendor.
        /// </summary>
        /// <value>Whether to enable connection tests involving this vendor.</value>
        public bool EnableConnectionTest { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the vendor uses split premium plans.
        /// </summary>
        public bool UsesSplitPremiumPlans { get; set; }

        /// <summary>
        /// Gets or sets a collection of split premium plans offered by the vendor.
        /// </summary>
        public List<MISplitPremiumOption> SplitPremiumOptions
        {
            get
            {
                return this.splitPremiumOptionsLazyInitializer.Value;
            }

            set
            {
                this.splitPremiumOptionsLazyInitializer = new Lazy<List<MISplitPremiumOption>>(() => value);
            }
        }

        /// <summary>
        /// Loads active Mortgage Insurance Vendors (i.e. PMI Providers) from database.
        /// Active MI Vendors are those that are available to the lender (broker).
        /// </summary>
        /// <returns>List of active Mortgage Insurance Vendors from database.</returns>
        public static IEnumerable<MortgageInsuranceVendorConfig> ListActiveVendors()
        {
            List<MortgageInsuranceVendorConfig> list = new List<MortgageInsuranceVendorConfig>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "MORTGAGE_INSURANCE_VENDOR_CONFIGURATION_ListAllValid"))
            {
                while (reader.Read())
                {
                    list.Add(new MortgageInsuranceVendorConfig(reader));
                }
            }

            return list;
        }

        /// <summary>
        /// Loads all Mortgage Insurance Vendors (i.e. PMI Providers) from database.
        /// This includes disabled vendors.
        /// </summary>
        /// <returns>List of active Mortgage Insurance Vendors from database.</returns>
        public static IEnumerable<MortgageInsuranceVendorConfig> ListAllVendors()
        {
            List<MortgageInsuranceVendorConfig> list = new List<MortgageInsuranceVendorConfig>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "MORTGAGE_INSURANCE_VENDOR_CONFIGURATION_ListAll"))
            {
                while (reader.Read())
                {
                    list.Add(new MortgageInsuranceVendorConfig(reader));
                }
            }

            return list;
        }

        /// <summary>
        /// Retrieves specific MI Vendor Instance.
        /// </summary>
        /// <param name="vendorId">Unique identifier of the specific MI Vendor instance.</param>
        /// <param name="allowInvalid">Whether to allow invalid (disabled) vendors to be returned.</param>
        /// <returns><see cref="MortgageInsuranceVendorConfig" /> object from the database.</returns>
        public static MortgageInsuranceVendorConfig RetrieveById(Guid vendorId, bool allowInvalid = false)
        {
            SqlParameter[] parameters = 
                                        {
                                            new SqlParameter("@VendorId", vendorId),
                                            new SqlParameter("@AllowInvalid", allowInvalid)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "MORTGAGE_INSURANCE_VENDOR_CONFIGURATION_Get", parameters))
            {
                if (reader.Read())
                {
                    return new MortgageInsuranceVendorConfig(reader);
                }
            }

            throw new NotFoundException("MI Vendor '" + vendorId + "' is not found.");
        }

        /// <summary>
        /// Saves MI Vendor entry to the database.
        /// </summary>
        public void Save()
        {
            if (this.IsValid)
            {
                if (string.IsNullOrEmpty(this.ExportPath_Production.TrimWhitespaceAndBOM()) || string.IsNullOrEmpty(this.ExportPath_Test.TrimWhitespaceAndBOM()))
                {
                    throw new CBaseException(
                        "Test and Production Export Paths are required.",
                        "LOAdmin user failed to enter a Test and/or Production Export Path for Mortgage Insurance Vendor Configruation.");
                }
            }
            else
            {
                var associatedBrokers = this.FindAssociatedBrokers();
                if (associatedBrokers.Any())
                {
                    string associatedBrokersString = string.Join(", ", associatedBrokers.ToArray<string>());
                    throw new CBaseException(
                        "This MI Vendor is still associated with " + associatedBrokersString + ". Please disable this vendor in the broker editor",
                        "VendorId " + this.VendorId + " is still associated with " + associatedBrokersString);
                }
            }

            using (var exec = new CStoredProcedureExec(DataSrc.LOShare))
            {
                exec.BeginTransactionForWrite();

                try
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@VendorId", this.VendorId),
                        new SqlParameter("@VendorType", (int)this.VendorType),
                        new SqlParameter("@IsValid", this.IsValid),
                        new SqlParameter("ExportPath_Test", this.ExportPath_Test),
                        new SqlParameter("ExportPath_Production", this.ExportPath_Production),
                        new SqlParameter("@IsUseAccountId", this.IsUseAccountId),
                        new SqlParameter("@SendAuthTicketWithOrders", this.SendAuthTicketWithOrders),
                        new SqlParameter("@EnableConnectionTest", this.EnableConnectionTest),
                        new SqlParameter("@UsesSplitPremiumPlans", this.UsesSplitPremiumPlans)
                    };

                    exec.ExecuteNonQuery("MORTGAGE_INSURANCE_VENDOR_CONFIGURATION_Save", 3, parameters);

                    if (this.UsesSplitPremiumPlans)
                    {
                        MISplitPremiumOption.Save(this.VendorId, this.SplitPremiumOptions, exec);
                    }

                    exec.CommitTransaction();
                }
                catch
                {
                    exec.RollbackTransaction();
                    throw;
                }
            }
        }

        /// <summary>
        /// Finds Brokers that are associated with a VendorId.
        /// </summary>
        /// <returns>Collection of Brokers associated with the current object's VendorId.</returns>
        private IEnumerable<string> FindAssociatedBrokers()
        {
            List<string> associatedBrokers = new List<string>();

            if (this.VendorId == null)
            {
                return associatedBrokers;
            }

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters = 
                                            {
                                                new SqlParameter("@VendorId", this.VendorId)
                                            };

                using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "MORTGAGE_INSURANCE_VENDOR_CONFIGURATION_ListBrokerAssociations", parameters))
                {
                    while (reader.Read())
                    {
                        associatedBrokers.Add((string)reader["CustomerCode"]);
                    }
                }
            }

            return associatedBrokers;
        }
    }
}