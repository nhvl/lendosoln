﻿namespace LendersOffice.Integration.MortgageInsurance
{
    /// <summary>
    /// Represents an MI Split Premium option in the UI.
    /// </summary>
    public class MISplitPremiumOptionView
    {
        /// <summary>
        /// Split premium upfront percentages are stored to two decimal places.
        /// </summary>
        private const int SplitPremiumDecimalPlaces = 2;

        /// <summary>
        /// Initializes a new instance of the <see cref="MISplitPremiumOptionView"/> class.
        /// </summary>
        /// <param name="upfrontPercentage">The upfront percentage.</param>
        /// <param name="mismoValue">The MISMO value that corresponds to the upfront percentage value for a specific vendor.</param>
        [Newtonsoft.Json.JsonConstructor]
        public MISplitPremiumOptionView(string upfrontPercentage, string mismoValue)
        {
            this.UpfrontPercentage = upfrontPercentage;
            this.MismoValue = mismoValue;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MISplitPremiumOptionView"/> class.
        /// </summary>
        /// <param name="upfrontPercentage">The upfront percentage.</param>
        /// <param name="mismoValue">The MISMO value that corresponds to the upfront percentage value for a specific vendor.</param>
        public MISplitPremiumOptionView(decimal upfrontPercentage, string mismoValue)
        {
            this.UpfrontPercentage = upfrontPercentage.ToString($"0.{new string('0', SplitPremiumDecimalPlaces)}");
            this.MismoValue = mismoValue;
        }

        /// <summary>
        /// Gets the upfront percentage value of the split premium.
        /// </summary>
        public string UpfrontPercentage { get; private set; }

        /// <summary>
        /// Gets the "SplitPremium#" MISMO value that corresponds to the upfront percentage value.
        /// </summary>
        public string MismoValue { get; private set; }
    }
}
