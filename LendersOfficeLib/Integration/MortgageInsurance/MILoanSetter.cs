﻿// <copyright file="MILoanSetter.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   10/16/2014 2:25:33 PM
// </summary>
namespace LendersOffice.Integration.MortgageInsurance
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Constants;
    using Mismo231.MI;
    using Mismo231.MI.MortgageInsuranceResponse;

    /// <summary>
    /// Saves mortgage insurance data from an MI vendor to the loan file.
    /// </summary>
    public class MILoanSetter
    {
        /// <summary>
        /// An instance of the loan data object.
        /// </summary>
        private CPageData dataLoan = null;

        /// <summary>
        /// The sLId of the loan file.
        /// </summary>
        private Guid loanID = Guid.Empty;

        /// <summary>
        /// Contains mortgage insurance data from the vendor.
        /// </summary>
        private MIResponse responseData = null;

        /// <summary>
        /// True if the MI response indicated a split premium. 
        /// </summary>
        private bool hasSplitPremium = false;

        /// <summary>
        /// True if the upfront mortgage insurance premium is financed.
        /// </summary>
        private bool hasFinancedUFMIP = false;

        /// <summary>
        /// True if the request is for a quote, false if for a policy order.
        /// </summary>
        private bool isQuote = true;

        /// <summary>
        /// The ID assigned to the MI vendor by LQB.
        /// </summary>
        private Guid vendorID = Guid.Empty;

        /// <summary>
        /// The MI vendor.
        /// </summary>
        private E_sMiCompanyNmT vendorType = E_sMiCompanyNmT.LeaveBlank;

        /// <summary>
        /// Initializes a new instance of the <see cref="MILoanSetter"/> class.
        /// </summary>
        /// <param name="loanIdent">The sLId of the LQB loan file.</param>
        /// <param name="responseInfo">An MIResponse object with the data from the MI vendor.</param>
        /// <param name="vendorIdent">The guid assigned to the MI vendor by LQB.</param>
        /// <param name="splitPremium">True if the MI response included a Split Premium indicator. Otherwise false.</param>
        /// <param name="financedUFMIP">True if the upfront MI premium is financed.</param>
        /// <param name="isQuote">Indicates whether the loan is being updated with a quote or a policy order.</param>
        public MILoanSetter(Guid loanIdent, MIResponse responseInfo, Guid vendorIdent, bool splitPremium, bool financedUFMIP, bool isQuote)
        {
            this.responseData = responseInfo;
            this.vendorID = vendorIdent;
            this.hasSplitPremium = splitPremium;
            this.hasFinancedUFMIP = financedUFMIP;
            this.isQuote = isQuote;
            this.Init(loanIdent);
        }

        /// <summary>
        /// Save the values from the MIResponse section of the policy response from the MI vendor to the loan file.
        /// </summary>
        /// <param name="setCommitmentRequestDate">True to set the MI commitment requested date. Otherwise false.</param>
        public void UpdateLoanWithPolicyInfo(bool setCommitmentRequestDate)
        {
            this.UpdateLoan(shouldSaveQuoteData: false, setCommitmentRequestDate: setCommitmentRequestDate);
        }

        /// <summary>
        /// Save the values from the MIResponse section of the quote response from the MI vendor to the loan file.
        /// </summary>
        public void UpdateLoanWithQuoteInfo()
        {
            this.UpdateLoan(shouldSaveQuoteData: true, setCommitmentRequestDate: false);
        }

        /// <summary>
        /// Save the given values to the loan file.
        /// </summary>
        /// <param name="shouldSaveQuoteData">True if intending to save quote data. False otherwise. Quote data is saved prior to placing a policy order, which is based on the quote in turn.</param>
        /// <param name="setCommitmentRequestDate">True to set the MI commitment requested date. Otherwise false.</param>
        private void UpdateLoan(bool shouldSaveQuoteData, bool setCommitmentRequestDate)
        {
            if (this.responseData == null)
            {
                this.InitSaveAndSetCommitmentData(null, setCommitmentRequestDate);
            
                return;
            }
            
            if (this.isQuote && !shouldSaveQuoteData)
            {
                return;
            }

            //// Don't wipe out the existing data if the policy order is declined. The decision type should be omitted by the vendor on quotes.
            if (!MIMismoConvertor.Approved(this.responseData.MIDecisionType) && !this.isQuote)
            {
                string certID = string.Empty;

                ////Save the cert ID to the loan file on suspended orders so that subsequent requests will route to the same order on the vendor side.
                if (this.responseData.MIDecisionType == MI_MIDecisionTypeEnumerated.Suspended)
                {
                    certID = this.responseData.MICertificateIdentifier;
                }

                this.InitSaveAndSetCommitmentData(certID, setCommitmentRequestDate);

                return;
            }

            this.dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            this.ClearMIPData();

            this.dataLoan.sFfUfMipIsBeingFinanced = this.hasFinancedUFMIP;

            if (setCommitmentRequestDate)
            {
                this.dataLoan.sMiCommitmentRequestedD_rep = DateTime.Now.ToString();
            }

            this.dataLoan.sMiCompanyNmT = this.vendorType;

            ////this.responseData.Borrower;
            ////this.responseData.ContactDetailList;
            ////this.responseData.EmbeddedFileList;
            ////this.responseData.KeyList;
            ////this.responseData.LenderCaseIdentifier;
            ////this.responseData.MI_LTVPercent;

            if (!this.isQuote)
            {
                this.dataLoan.sMiCommitmentReceivedD_rep = DateTime.Now.ToString();
                this.dataLoan.sMiCommitmentExpirationD_rep = this.responseData.MICertificateExpirationDate;
                this.dataLoan.sMiCertId = this.responseData.MICertificateIdentifier;
            }

            ////this.responseData.MICertificateType;
            ////this.responseData.MICommentDescription;
            ////this.responseData.MICompanyName;
            this.dataLoan.sMiLenderPaidCoverage_rep = this.responseData.MICoveragePercent;

            bool mioptionFound = false;
            E_sProdConvMIOptionT mioption = MIMismoConvertor.MIOptionType(this.responseData.MIDurationType, this.responseData.MIPremiumPaymentType, this.hasSplitPremium, out mioptionFound);
            
            if (mioptionFound)
            {
                this.dataLoan.sProdConvMIOptionT = mioption;
                this.SetPremiums(
                            mioption, 
                            this.responseData.MIInitialPremiumAtClosingType, 
                            this.responseData.MIInitialPremiumRateDurationMonths, 
                            this.responseData.MIInitialPremiumRatePercent, 
                            this.responseData.MIRenewalPremiumList, 
                            this.responseData.MIPremiumTaxList);
                this.SetUFMIPBasis(mioption, this.responseData.MIInitialPremiumAtClosingType);
            }

            ////this.responseData.MIInitialPremiumAmount;
            ////this.responseData.MIInitialPremiumAtClosingType;
            ////this.responseData.MILenderIdentifier;
            ////this.responseData.MIPremiumFromClosingAmount;

            E_sMiInsuranceT insuranceType = MIMismoConvertor.MiInsuranceType(this.responseData.MIPremiumPaymentType);
            
            if (insuranceType != E_sMiInsuranceT.None)
            {
                this.dataLoan.sMiInsuranceT = insuranceType;
            }

            ////this.responseData.MIPremiumRatePlanType;
            ////this.responseData.MIRateQuoteExpirationDate;
            this.dataLoan.sProMInsT = MIMismoConvertor.MonthlyMIBasis(this.responseData.MIDurationType, this.responseData.MIRenewalCalculationType);
            ////this.responseData.MISMOVersionID;
            ////this.responseData.MITransactionIdentifier;
            ////this.responseData.Status;

            this.dataLoan.Save();
        }

        /// <summary>
        /// Clear the MI factors, terms, and amounts so that old data is not retained when the user swaps quotes or policy types (e.g. borrower paid monthly to single, borrower paid to lender paid).
        /// <para>It might be better to do this in the data layer so that the related MI fields are calculated based on the MI type ($$sProdConvMIOptionT$$), and non-relevant fields for a given type return zero/false.</para>
        /// </summary>
        private void ClearMIPData()
        {
            this.dataLoan.sFfUfmipR = 0;
            this.dataLoan.sFfUfmip1003Lckd = false;
            this.dataLoan.sFfUfMipIsBeingFinanced = false;
            this.dataLoan.sUfCashPdLckd = false;
            this.dataLoan.sProMInsR = 0;
            this.dataLoan.sProMInsMb = 0;
            this.dataLoan.sProMInsLckd = false;
            this.dataLoan.sProMInsMon = 0;
            this.dataLoan.sProMIns2Mon = 0;
            this.dataLoan.sProMInsR2 = 0;
            this.dataLoan.sLenderUfmipR = 0;
            this.dataLoan.sLenderUfmipLckd = false;
            this.dataLoan.sMiCompanyNmT = E_sMiCompanyNmT.LeaveBlank;
            this.dataLoan.sMiLenderPaidCoverage = 0;
            this.dataLoan.sMiInsuranceT = E_sMiInsuranceT.None;
            this.dataLoan.sProMInsT = E_PercentBaseT.LoanAmount;
        }

        /// <summary>
        /// Sets the MI premiums and factors from the MI response.
        /// </summary>
        /// <param name="mioption">The mortgage insurance option (BPMI monthly, BPMI single, etc).</param>
        /// <param name="premiumAtClosing">The premium paid at closing.</param>
        /// <param name="durationMonths">The premium rate duration months.</param>
        /// <param name="initialPremiumRatePercent">The rate for the initial MI premium.</param>
        /// <param name="renewals">A list of annual MI premiums.</param>
        /// <param name="taxes">A list of MI taxes.</param>
        private void SetPremiums(E_sProdConvMIOptionT mioption, MI_MIInitialPremiumAtClosingTypeEnumerated premiumAtClosing, string durationMonths, string initialPremiumRatePercent, List<MIRenewalPremium> renewals, List<MIPremiumTax> taxes)
        {
            decimal totalTaxRate = MIUtil.GetTaxRate(taxes);
            
            initialPremiumRatePercent = MIUtil.AddTaxesToPremium(initialPremiumRatePercent, totalTaxRate, false);

            if (mioption == E_sProdConvMIOptionT.BorrPaidMonPrem && premiumAtClosing == MI_MIInitialPremiumAtClosingTypeEnumerated.Deferred)
            {
                this.dataLoan.sFfUfmipR = 0;
            }
            else if (mioption == E_sProdConvMIOptionT.LendPaidSinglePrem)
            {
                this.dataLoan.sLenderUfmipR_rep = initialPremiumRatePercent;
            }
            else
            {
                this.dataLoan.sFfUfmipR_rep = initialPremiumRatePercent;
            }

            foreach (MIRenewalPremium premium in renewals ?? new List<MIRenewalPremium>())
            {
                switch (premium._Sequence)
                {
                    case MI_MIRenewalPremiumSequenceEnumerated.First:
                        if (this.AddRateDurationToFullTerm(mioption, premiumAtClosing))
                        {
                            this.dataLoan.sProMInsMon = this.dataLoan.m_convertLos.ToCount(durationMonths) + this.dataLoan.m_convertLos.ToCount(premium._RateDurationMonths);
                        }
                        else if (mioption == E_sProdConvMIOptionT.BorrPaidMonPrem && premiumAtClosing == MI_MIInitialPremiumAtClosingTypeEnumerated.Prepaid)
                        {
                            this.dataLoan.sProMInsMon = this.dataLoan.m_convertLos.ToCount(durationMonths) - 1 + this.dataLoan.m_convertLos.ToCount(premium._RateDurationMonths);
                        }
                        else
                        {
                            this.dataLoan.sProMInsMon_rep = premium._RateDurationMonths;
                        }

                        if (mioption == E_sProdConvMIOptionT.BorrPaidMonPrem || mioption == E_sProdConvMIOptionT.BorrPaidSplitPrem)
                        {
                            this.dataLoan.sProMInsR_rep = MIUtil.AddTaxesToPremium(premium._Rate, totalTaxRate, true);
                        }
                        
                        break;
                    case MI_MIRenewalPremiumSequenceEnumerated.Second:
                        this.dataLoan.sProMIns2Mon_rep = premium._RateDurationMonths;

                        if (mioption == E_sProdConvMIOptionT.BorrPaidMonPrem || mioption == E_sProdConvMIOptionT.BorrPaidSplitPrem)
                        {
                            this.dataLoan.sProMInsR2_rep = MIUtil.AddTaxesToPremium(premium._Rate, totalTaxRate, true);
                        }

                        break;
                    case MI_MIRenewalPremiumSequenceEnumerated.Third:
                    case MI_MIRenewalPremiumSequenceEnumerated.Fourth:
                    case MI_MIRenewalPremiumSequenceEnumerated.Fifth:
                    case MI_MIRenewalPremiumSequenceEnumerated.None:
                        //// Not supported.
                        break;
                    default:
                        throw new UnhandledEnumException(premium._Sequence);
                }
            }
        }

        /// <summary>
        /// Determines whether the term for MI should add the remainder months
        /// to the initial term for a premium.
        /// </summary>
        /// <param name="mioption">
        /// The <see cref="E_sProdConvMIOptionT"/> for the premium.
        /// </param>
        /// <param name="premiumAtClosing">
        /// The <see cref="MI_MIInitialPremiumAtClosingTypeEnumerated"/> for 
        /// the premium.
        /// </param>
        /// <returns>
        /// True if the remainder months should be added to the initial term,
        /// false otherwise.
        /// </returns>
        private bool AddRateDurationToFullTerm(E_sProdConvMIOptionT mioption, MI_MIInitialPremiumAtClosingTypeEnumerated premiumAtClosing)
        {
            var isNonMgicSplitPremium = this.hasSplitPremium && this.vendorType != E_sMiCompanyNmT.MGIC;

            var isDeferredBorrPaidMonPrem = mioption == E_sProdConvMIOptionT.BorrPaidMonPrem &&
                premiumAtClosing == MI_MIInitialPremiumAtClosingTypeEnumerated.Deferred;

            return isNonMgicSplitPremium || isDeferredBorrPaidMonPrem;
        }

        /// <summary>
        /// Sets the basis for the UFMIP calculation (single payment | annual rate for x months) from the MI response.
        /// </summary>
        /// <param name="mioption">The mortgage insurance option (BPMI monthly, BPMI single, etc).</param>
        /// <param name="premiumAtClosing">The premium at closing type (deferred, prepaid).</param>
        private void SetUFMIPBasis(E_sProdConvMIOptionT mioption, MI_MIInitialPremiumAtClosingTypeEnumerated premiumAtClosing)
        {
            if (mioption != E_sProdConvMIOptionT.LendPaidSinglePrem)
            {
                int ufmipDuration = this.dataLoan.m_convertLos.ToCount(this.responseData.MIInitialPremiumRateDurationMonths);

                if (mioption == E_sProdConvMIOptionT.BorrPaidMonPrem && premiumAtClosing == MI_MIInitialPremiumAtClosingTypeEnumerated.Deferred)
                {
                    this.dataLoan.sMipFrequency = E_MipFrequency.AnnualRateXMonths;
                    this.dataLoan.sMipPiaMon = 0;
                }
                else if (mioption == E_sProdConvMIOptionT.BorrPaidMonPrem && premiumAtClosing == MI_MIInitialPremiumAtClosingTypeEnumerated.Prepaid)
                {
                    this.dataLoan.sMipFrequency = E_MipFrequency.AnnualRateXMonths;
                    this.dataLoan.sMipPiaMon = 1;
                }
                else if (ufmipDuration == 0 || mioption == E_sProdConvMIOptionT.BorrPaidSinglePrem || mioption == E_sProdConvMIOptionT.BorrPaidSplitPrem)
                {
                    this.dataLoan.sMipFrequency = E_MipFrequency.SinglePayment;
                }
                else
                {
                    this.dataLoan.sMipFrequency = E_MipFrequency.AnnualRateXMonths;
                    this.dataLoan.sMipPiaMon_rep = this.responseData.MIInitialPremiumRateDurationMonths;
                }
            }
        }

        /// <summary>
        /// Sets $$sMiCommitmentRequestedD$$ to the current date and/or save the certificate ID to $$sMiCertId$$. This method should only be called if these are the only data being saved.
        /// </summary>
        /// <param name="certID">The certificate ID returned by the vendor.</param>
        /// <param name="setCommitmentRequestDate">True to save the current date to the commitment request date field. Otherwise false.</param>
        private void InitSaveAndSetCommitmentData(string certID, bool setCommitmentRequestDate)
        {
            bool hasCertID = !string.IsNullOrEmpty(certID);

            if (hasCertID || setCommitmentRequestDate)
            {
                this.dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                if (hasCertID)
                {
                    this.dataLoan.sMiCertId = certID;
                }

                if (setCommitmentRequestDate)
                {
                    this.dataLoan.sMiCommitmentRequestedD_rep = DateTime.Now.ToString();
                }

                this.dataLoan.Save();
            }
        }

        /// <summary>
        /// Initializes the MILoanSetter.
        /// </summary>
        /// <param name="loanIdent">The sLId of the LQB loan file.</param>
        private void Init(Guid loanIdent)
        {
            this.loanID = loanIdent;
            this.dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanID, typeof(MILoanSetter));
            this.dataLoan.InitLoad();

            this.vendorType = MortgageInsuranceVendorConfig.RetrieveById(this.vendorID).VendorType;
        }
    }
}
