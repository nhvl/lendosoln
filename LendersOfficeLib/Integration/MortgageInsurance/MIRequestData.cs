﻿namespace LendersOffice.Integration.MortgageInsurance
{
    using System;
    using System.Data;
    using DataAccess;
    using Mismo231.MI;
    using Security;

    /// <summary>
    /// The data for submitting an MI framework request.
    /// </summary>
    public class MIRequestData
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="MIRequestData"/> class from being created.
        /// </summary>
        private MIRequestData()
        {
        }

        /// <summary>
        /// Gets the loan id.
        /// </summary>
        public Guid LoanId { get; private set; }

        /// <summary>
        /// Gets the application id.
        /// </summary>
        public Guid ApplicationId { get; private set; }

        /// <summary>
        /// Gets the principal of the user initiating the request.
        /// </summary>
        public AbstractUserPrincipal Principal { get; private set; }

        /// <summary>
        /// Gets the branch id.
        /// </summary>
        public Guid BranchId { get; private set; }

        /// <summary>
        /// Gets the vendor id.
        /// </summary>
        public Guid VendorId { get; private set; }

        /// <summary>
        /// Gets the premium type.
        /// </summary>
        public E_sProdConvMIOptionT PremiumType { get; private set; }

        /// <summary>
        /// Gets the split premium MISMO value expected by the vendor.
        /// </summary>
        /// <remarks>May be null if the premium type is not split.</remarks>
        public string SplitPremiumMismoValue { get; private set; }

        /// <summary>
        /// Gets the refundability.
        /// </summary>
        public MI_MIPremiumRefundableTypeEnumerated Refundability { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the UFMIP is financed.
        /// </summary>
        public bool UFMIPFinanced { get; private set; }

        /// <summary>
        /// Gets the master policy number.
        /// </summary>
        public string MasterPolicyNumber { get; private set; }

        /// <summary>
        /// Gets the coverage percent.
        /// </summary>
        public decimal CoveragePercent { get; private set; } = -1;

        /// <summary>
        /// Gets the renewl type.
        /// </summary>
        public MI_MIRenewalCalculationTypeEnumerated RenewalType { get; private set; }

        /// <summary>
        /// Gets the premium at closing.
        /// </summary>
        public MI_MIInitialPremiumAtClosingTypeEnumerated PremiumAtClosing { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this is a relocation loan.
        /// </summary>
        public bool IsRelocationLoan { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this is a quote request (true) or a policy request (false).
        /// </summary>
        public bool IsQuote { get; private set; }

        /// <summary>
        /// Gets the quote number. Only for policy requests.
        /// </summary>
        public string QuoteNumber { get; private set; } = string.Empty;

        /// <summary>
        /// Gets the original quote number. Only for policy requests.
        /// </summary>
        public string OriginalQuoteNumber { get; private set; } = string.Empty;

        /// <summary>
        /// Gets the polling interval in seconds.
        /// </summary>
        public int PollingIntervalInSeconds { get; private set; }

        /// <summary>
        /// Gets the mortgage insurance delegation type.
        /// </summary>
        public DelegationType DelegationType { get; private set; }

        /// <summary>
        /// Gets the type of the request.
        /// </summary>
        public E_MiRequestType RequestType { get; private set; }

        /// <summary>
        /// Creates the request data for a quote request.
        /// </summary>
        /// <param name="principal">The principal of the user making the request.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="appId">The application id.</param>
        /// <param name="vendorId">The vendor id.</param>
        /// <param name="branchId">The branch id.</param>
        /// <param name="premiumType">The premium type.</param>
        /// <param name="splitPremiumMismoValue">The split premium MISMO value, if applicable.</param>
        /// <param name="refundability">The refundability type.</param>
        /// <param name="ufmipFinanced">Is UFMIP financed.</param>
        /// <param name="masterPolicyNumber">The master policy number.</param>
        /// <param name="coveragePercent">The coverage percent.</param>
        /// <param name="renewalType">The renewal type.</param>
        /// <param name="premiumAtClosing">The premium at closing.</param>
        /// <param name="isRelocationLoan">Whether this is a relocation loan.</param>
        /// <param name="pollingIntervalInSeconds">The polling interval that will be used when checking to see if an MI request has been processed.</param>
        /// <param name="requestType">The type of the request.</param>
        /// <returns>The request data.</returns>
        public static MIRequestData CreateQuoteRequestData(
            AbstractUserPrincipal principal,
            Guid loanId,
            Guid appId,
            Guid vendorId,
            Guid branchId,
            E_sProdConvMIOptionT premiumType,
            string splitPremiumMismoValue,
            MI_MIPremiumRefundableTypeEnumerated refundability,
            bool ufmipFinanced,
            string masterPolicyNumber,
            decimal coveragePercent,
            MI_MIRenewalCalculationTypeEnumerated renewalType,
            MI_MIInitialPremiumAtClosingTypeEnumerated premiumAtClosing,
            bool isRelocationLoan,
            int pollingIntervalInSeconds,
            E_MiRequestType requestType)
        {
            return new MIRequestData()
            {
                Principal = principal,
                LoanId = loanId,
                ApplicationId = appId,
                VendorId = vendorId,
                BranchId = branchId,
                PremiumType = premiumType,
                SplitPremiumMismoValue = splitPremiumMismoValue,
                Refundability = refundability,
                UFMIPFinanced = ufmipFinanced,
                CoveragePercent = coveragePercent,
                MasterPolicyNumber = masterPolicyNumber,
                RenewalType = renewalType,
                PremiumAtClosing = premiumAtClosing,
                IsRelocationLoan = isRelocationLoan,
                IsQuote = true,
                PollingIntervalInSeconds = pollingIntervalInSeconds,
                DelegationType = DelegationType.Blank, // for now set the quote's delegation type to blank. it won't be used with the quote, but just with the orders.
                RequestType = requestType
            };
        }

        /// <summary>
        /// Creates a request data for a policy request.
        /// </summary>
        /// <param name="principal">The principal of the user creating the request.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="applicationId">The application id.</param>
        /// <param name="branchId">The branch id.</param>
        /// <param name="quote">The original quote.</param>
        /// <param name="pollingIntervalInSeconds">The polling interval that will be used when checking to see if an MI request has been processed.</param>
        /// <returns>The request data.</returns>
        public static MIRequestData CreatePolicyRequestData(AbstractUserPrincipal principal, Guid loanId, Guid applicationId, Guid branchId, MIOrderInfo quote, int pollingIntervalInSeconds)
        {
            return new MIRequestData()
            {
                Principal = principal,
                LoanId = loanId,
                ApplicationId = applicationId,
                VendorId = quote.VendorID,
                BranchId = branchId,
                PremiumType = quote.PremiumType,
                SplitPremiumMismoValue = quote.SplitPremiumMismoValue,
                Refundability = quote.Refundability,
                UFMIPFinanced = quote.UfmipFinanced,
                MasterPolicyNumber = quote.MasterPolicyNumber,
                CoveragePercent = quote.CoveragePercent,
                RenewalType = quote.RenewalType,
                PremiumAtClosing = quote.PremiumAtClosing,
                IsRelocationLoan = quote.RelocationLoan,
                IsQuote = false,
                QuoteNumber = quote.QuoteNumber,
                OriginalQuoteNumber = quote.OrderNumber,
                PollingIntervalInSeconds = pollingIntervalInSeconds,
                DelegationType = quote.DelegationType,
                RequestType = E_MiRequestType.User // Policy requests cannot be for pricing.
            };
        }

        /// <summary>
        /// Loads the request data from the DB using a data reader.
        /// </summary>
        /// <param name="reader">The reader.</param>
        /// <returns>The request data loaded from the DB.</returns>
        public static MIRequestData CreateFromReader(IDataReader reader)
        {
            Guid loanId = (Guid)reader["LoanId"];
            Guid applicationId = (Guid)reader["ApplicationId"];
            Guid brokerId = (Guid)reader["BrokerId"];
            Guid userId = (Guid)reader["UserId"];
            string userType = (string)reader["UserType"];
            Guid branchId = (Guid)reader["BranchId"];
            Guid vendorId = (Guid)reader["VendorId"];
            E_sProdConvMIOptionT premiumType = reader.GetNullableIntEnum<E_sProdConvMIOptionT>("PremiumType").Value;
            string splitPremiumMismoValue = reader.SafeString("UpfrontPremiumMismoValue");
            Mismo231.MI.MI_MIPremiumRefundableTypeEnumerated refundability = reader.GetNullableIntEnum<Mismo231.MI.MI_MIPremiumRefundableTypeEnumerated>("Refundability").Value;
            bool ufmipFinanced = (bool)reader["UFMIPFinanced"];
            string masterPolicyNumber = reader.AsNullableString("MasterPolicyNumber") ?? null;
            decimal coveragePercent = (decimal)reader["CoveragePercent"];
            Mismo231.MI.MI_MIRenewalCalculationTypeEnumerated renewalType = reader.GetNullableIntEnum<Mismo231.MI.MI_MIRenewalCalculationTypeEnumerated>("RenewalType").Value;
            Mismo231.MI.MI_MIInitialPremiumAtClosingTypeEnumerated premiumAtClosing = reader.GetNullableIntEnum<Mismo231.MI.MI_MIInitialPremiumAtClosingTypeEnumerated>("PremiumAtClosing").Value;
            bool isRelocationLoan = (bool)reader["IsRelocationLoan"];
            bool isQuote = (bool)reader["IsQuote"];
            string quoteNumber = reader.AsNullableString("QuoteNumber") ?? string.Empty;
            string originalQuoteNumber = reader.AsNullableString("OriginalQuoteNumber") ?? string.Empty;
            int pollingIntervalInSeconds = (int)reader["PollingIntervalInSeconds"];
            var principal = PrincipalFactory.RetrievePrincipalForUser(brokerId, userId, userType);
            DelegationType delegationType = (DelegationType)Convert.ToInt32(reader["DelegationType"]);
            E_MiRequestType requestType = (E_MiRequestType)Convert.ToInt32(reader["RequestType"]);

            return new MIRequestData()
            {
                LoanId = loanId,
                ApplicationId = applicationId,
                Principal = principal,
                BranchId = branchId,
                VendorId = vendorId,
                PremiumType = premiumType,
                SplitPremiumMismoValue = splitPremiumMismoValue,
                Refundability = refundability,
                UFMIPFinanced = ufmipFinanced,
                MasterPolicyNumber = masterPolicyNumber,
                CoveragePercent = coveragePercent,
                RenewalType = renewalType,
                PremiumAtClosing = premiumAtClosing,
                IsRelocationLoan = isRelocationLoan,
                IsQuote = isQuote,
                QuoteNumber = quoteNumber,
                OriginalQuoteNumber = originalQuoteNumber,
                PollingIntervalInSeconds = pollingIntervalInSeconds,
                DelegationType = delegationType,
                RequestType = requestType
            };
        }
    }
}
