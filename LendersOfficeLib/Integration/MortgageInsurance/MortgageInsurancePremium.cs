﻿// <copyright file="MortgageInsurancePremium.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   10/16/2014 6:45:40 PM
// </summary>
namespace LendersOffice.Integration.MortgageInsurance
{
    /// <summary>
    /// Implements a container for mortgage insurance premium data.
    /// </summary>
    public class MortgageInsurancePremium
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MortgageInsurancePremium"/> class.
        /// </summary>
        /// <param name="description">The premium description.</param>
        /// <param name="factor">The premium factor or rate.</param>
        /// <param name="amount">The dollar amount of the premium.</param>
        public MortgageInsurancePremium(string description, decimal factor, decimal amount)
        {
            this.Description = description;
            this.Factor = factor;
            this.Amount = amount;
        }

        /// <summary>
        /// Gets the premium description.
        /// </summary>
        /// <value>A description of the MI premium.</value>
        public string Description { get; private set; }

        /// <summary>
        /// Gets the factor or rate of the premium.
        /// </summary>
        /// <value>The premium factor as a decimal.</value>
        public decimal Factor { get; private set; }

        /// <summary>
        /// Gets the dollar amount of the premium.
        /// </summary>
        /// <value>The premium as a dollar amount.</value>
        public decimal Amount { get; private set; }
    }
}
