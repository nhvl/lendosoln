﻿// <copyright file="MIOrderInfo.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/29/2014 5:49:29 PM
// </summary>
namespace LendersOffice.Integration.MortgageInsurance
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Common;
    using Mismo231.MI;
    using Mismo231.MI.MortgageInsuranceRequest;
    using Mismo231.MI.MortgageInsuranceResponse;

    /// <summary>
    /// A container for information related to mortgage insurance framework orders.
    /// </summary>
    public class MIOrderInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MIOrderInfo" /> class.
        /// </summary>
        /// <param name="loanID">The sLId of the loan file.</param>
        /// <param name="applicationID">The aAppId of the loan application.</param>
        /// <param name="vendorID">The ID assigned to the MI vendor by LQB.</param>
        /// <param name="premiumType">The MI premium type associated with the MI transaction.</param>
        /// <param name="splitPremiumMismoValue">The split premium MISMO value expected by the vendor, if applicable.</param>
        /// <param name="refundability">The $$refundability$$ type associated with the MI transaction.</param>
        /// <param name="ufmipFinanced">An indicator as to whether the upfront MI premium is added to the loan amount.</param>
        /// <param name="brokerID">The brokerID of the lender associated with the MI transaction.</param>
        /// <param name="userID">The userID of the user placing the MI order, or null if the transaction is asynchronous.</param>
        /// <param name="masterPolicyNumber">The master policy number assigned to the lender/branch by the MI vendor.</param>
        /// <param name="coveragePercent">The percentage of the loan amount to be insured.</param>
        /// <param name="renewalType">Indicates how the MI renewal premiums will be calculated.</param>
        /// <param name="premiumAtClosing">Indicates whether the initial MI premium will be prepaid upon closing or deferred.</param>
        /// <param name="relocationLoan">Indicates whether the loan is a relocation loan.</param>
        /// <param name="quote">Indicates whether the order is a quote order or a policy order.</param>
        /// <param name="quoteNumber">Quote number returned by MI vendor for rate quote request.</param>
        /// <param name="originalQuoteOrderNumber">The order number for the original MI Quote this order was ordered from.</param>
        /// <param name="delegationType">Delegation type.</param>
        /// <param name="requestType">Request type.</param>
        public MIOrderInfo(
            Guid loanID,
            Guid applicationID,
            Guid vendorID,
            E_sProdConvMIOptionT premiumType,
            string splitPremiumMismoValue,
            MI_MIPremiumRefundableTypeEnumerated refundability,
            bool ufmipFinanced,
            Guid brokerID,
            Guid? userID,
            string masterPolicyNumber,
            decimal coveragePercent,
            MI_MIRenewalCalculationTypeEnumerated renewalType,
            MI_MIInitialPremiumAtClosingTypeEnumerated premiumAtClosing,
            bool relocationLoan,
            bool quote,
            string quoteNumber,
            string originalQuoteOrderNumber,
            DelegationType delegationType,
            E_MiRequestType requestType)
        {
            this.TransactionID = Guid.NewGuid();
            this.LoanID = loanID;
            this.ApplicationID = applicationID;
            this.VendorID = vendorID;
            this.OrderNumber = Guid.NewGuid().ToString("N");
            this.OrderedDate = DateTime.Now;
            this.PremiumType = premiumType;
            this.SplitPremiumMismoValue = splitPremiumMismoValue;
            this.Refundability = refundability;
            this.UfmipFinanced = ufmipFinanced;
            this.BrokerID = brokerID;
            this.UserID = userID;
            this.MasterPolicyNumber = masterPolicyNumber;
            this.CoveragePercent = coveragePercent;
            this.RenewalType = renewalType;
            this.PremiumAtClosing = premiumAtClosing;
            this.RelocationLoan = relocationLoan;
            this.IsQuoteRequest = quote;
            this.QuoteNumber = quoteNumber;
            this.OriginalQuoteOrderNumber = originalQuoteOrderNumber;
            this.DelegationType = delegationType;
            this.RequestType = requestType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MIOrderInfo" /> class.
        /// </summary>
        /// <param name="reader">A DbDataReader containing the order information from the database.</param>
        /// <param name="brokerID">The brokerID of the lender associated with the MI transaction.</param>
        /// <param name="applicationID">The aAppId of the loan application.</param>
        /// <param name="userID">The userID of the user placing the MI order, or null if the transaction is asynchronous.</param>
        public MIOrderInfo(DbDataReader reader, Guid brokerID, Guid applicationID, Guid? userID)
        {
            this.TransactionID = (Guid)reader["TransactionId"];
            this.LoanID = (Guid)reader["sLId"];
            this.VendorID = (Guid)reader["VendorId"];
            this.OrderNumber = (string)reader["OrderNumber"];
            this.OrderedDate = (DateTime)reader["OrderedDate"];
            this.PremiumType = (E_sProdConvMIOptionT)reader["MIPremiumType"];
            this.SplitPremiumMismoValue = reader.SafeString("UpfrontPremiumMismoValue");
            this.Refundability = (MI_MIPremiumRefundableTypeEnumerated)reader["PremiumRefundability"];
            this.UfmipFinanced = (bool)reader["UFMIPFinanced"];
            string responseXML = (string)reader["ResponseXmlContent"];
            this.MasterPolicyNumber = (string)reader["LenderIdentifier"];
            this.QuoteNumber = (string)reader["QuoteNumber"];
            this.IsQuoteRequest = (bool)reader["IsQuote"];
            this.OriginalQuoteOrderNumber = (string)reader["OriginalQuoteOrderNumber"];
            this.DelegationType = (DelegationType)Convert.ToInt32(reader["DelegationType"]);

            this.BrokerID = brokerID;
            this.ApplicationID = applicationID;
            this.UserID = userID;

            this.CoveragePercent = 0.00m;
            this.PremiumAtClosing = MI_MIInitialPremiumAtClosingTypeEnumerated.None;
            this.RelocationLoan = false;
            this.RenewalType = MI_MIRenewalCalculationTypeEnumerated.None;

            if (!string.IsNullOrEmpty(responseXML))
            {
                this.ResponseProvider = new MIResponseProvider(responseXML, MIResponseDataSource.LQBDB);
                this.PopulateResponseData();
            }
        }

        /// <summary>
        /// Gets the transaction ID for the MI transaction. REQUEST_GROUP/REQUEST/REQUEST_DATA/MI_APPLICATION/MI_REQUEST/@MITransactionIdentifier.
        /// </summary>
        /// <value>A guid generated by LQB for the MI request.</value>
        public Guid TransactionID { get; private set; }

        /// <summary>
        /// Gets the sLId of the loan file associated with the MI transaction.
        /// </summary>
        /// <value>The loan file's sLId.</value>
        public Guid LoanID { get; private set; }

        /// <summary>
        /// Gets the aAppId of the application associated with the MI transaction.
        /// </summary>
        /// <value>The application's aAppId.</value>
        public Guid ApplicationID { get; private set; }

        /// <summary>
        /// Gets the ID of the MI vendor associated with the MI transaction.
        /// </summary>
        /// <value>A guid assigned to the MI vendor by LQB.</value>
        public Guid VendorID { get; private set; }

        /// <summary>
        /// Gets the order number associated with the MI transaction.
        /// </summary>
        /// <value>A string generated by LQB.</value>
        public string OrderNumber { get; private set; }

        /// <summary>
        /// Gets the date that the MI order was placed or that the transaction occurred. REQUEST_GROUP/REQUEST/@RequestDateTime.
        /// </summary>
        /// <value>The date-time associated with the MI transaction.</value>
        public DateTime OrderedDate { get; private set; }

        /// <summary>
        /// Gets the MI premium type associated with the MI transaction.
        /// </summary>
        /// <value>The type of MI premium requested, e.g. BPMI Monthly, LPMI Single, etc.</value>
        public E_sProdConvMIOptionT PremiumType { get; private set; }

        /// <summary>
        /// Gets the split premium MISMO value expected by the vendor.
        /// </summary>
        /// <remarks>May be null if the premium type is not split.</remarks>
        public string SplitPremiumMismoValue { get; private set; }

        /// <summary>
        /// Gets the $$refundability$$ type associated with the MI transaction. REQUEST_GROUP/REQUEST/REQUEST_DATA/MI_APPLICATION/MI_REQUEST/@MIPremiumRefundableType.
        /// </summary>
        /// <value>Whether the MI premium is refundable.</value>
        public MI_MIPremiumRefundableTypeEnumerated Refundability { get; private set; }
        
        /// <summary>
        /// Gets a value indicating whether the upfront MI premium is added to the loan amount. REQUEST_GROUP/REQUEST/REQUEST_DATA/MI_APPLICATION/MI_REQUEST/@MIPremiumFinancedIndicator.
        /// </summary>
        /// <value>True if the UFMIP is financed. Otherwise false.</value>
        public bool UfmipFinanced { get; private set; }

        /// <summary>
        /// Gets the brokerID of the lender associated with the MI transaction.
        /// </summary>
        /// <value>The guid assigned to the lender by LQB.</value>
        public Guid BrokerID { get; private set; }

        /// <summary>
        /// Gets or sets the MISMO 2.3.1 MI request object associated with the transaction.
        /// </summary>
        /// <value>The MISMO 2.3.1 MI request as a RequestGroup object.</value>
        public RequestGroup MortgageInsuranceRequest { get; set; }

        /// <summary>
        /// Gets the user ID of the user placing the MI order, or null if the transaction is asynchronous.
        /// </summary>
        /// <value>The guid assigned to the ordering user by LQB, or null if the transaction is asynchronous.</value>
        public Guid? UserID { get; private set; }

        /// <summary>
        /// Gets the master policy number assigned to the lender/branch by the MI vendor. REQUEST_GROUP/REQUEST/REQUEST_DATA/MI_APPLICATION/MI_REQUEST/@MILenderIdentifier.
        /// </summary>
        /// <value>The master policy number of the lender or branch.</value>
        public string MasterPolicyNumber { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the order has a split premium type.
        /// </summary>
        public bool IsSplitPremiumOrder
        {
            get
            {
                if (this.ResponseProvider != null && this.ResponseProvider.HasResponseInfo)
                {
                    return this.ResponseProvider.SplitPremium;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets the upfront split premium percentage.
        /// </summary>
        public decimal SplitPremiumUpfrontPercent
        {
            get
            {
                if (this.ResponseProvider != null && this.ResponseProvider.HasResponseInfo && this.ResponseProvider.SplitPremium)
                {
                    var upfrontPremium = this.ResponseProvider.Premiums?.FirstOrDefault(p => p.Description.Equals("Upfront Premium"));
                    if (upfrontPremium != null)
                    {
                        return upfrontPremium.Factor;
                    }
                }

                return 0m;
            }
        }

        /// <summary>
        /// Gets the percentage of the loan amount to be insured. /LOAN_APPLICATION/LOAN_PRODUCT_DATA/LOAN_FEATURES/@MICoveragePercent.
        /// </summary>
        /// <value>The coverage percentage associated with the MI request.</value>
        public decimal CoveragePercent { get; private set; }

        /// <summary>
        /// Gets the calculation type for the MI renewal premiums. REQUEST_GROUP/REQUEST/REQUEST_DATA/MI_APPLICATION/MI_REQUEST/@MIRenewalCalculationType.
        /// </summary>
        /// <value>The calculation type for the MI renewal premiums.</value>
        public MI_MIRenewalCalculationTypeEnumerated RenewalType { get; private set; }

        /// <summary>
        /// Gets an indicator as to whether the initial MI premium will be prepaid upon closing or deferred until the first principle payment. REQUEST_GROUP/REQUEST/REQUEST_DATA/MI_APPLICATION/MI_REQUEST/@MIInitialPremiumAtClosingType.
        /// </summary>
        /// <value>An indicator as to whether the initial MI premium will be prepaid upon closing or deferred.</value>
        public MI_MIInitialPremiumAtClosingTypeEnumerated PremiumAtClosing { get; private set; }

        /// <summary>
        /// Gets the order number for the original MI Quote this order was ordered from.
        /// </summary>
        /// <value>A string generated by LQB (Guid in string from w/o dashes).</value>
        /// <remarks>Only used by MI Policies.</remarks>
        public string OriginalQuoteOrderNumber { get; private set; }

        /// <summary>
        /// Gets or sets the mortgage insurance delegation type.
        /// </summary>
        public DelegationType DelegationType { get; set; }

        /// <summary>
        /// Gets or sets the mortgage insurance request type.
        /// </summary>
        public E_MiRequestType RequestType { get; set; } = E_MiRequestType.User;

        /// <summary>
        /// Gets or sets a value indicating whether the loan is a relocation loan. REQUEST_GROUP/REQUEST/REQUEST_DATA/MI_APPLICATION/MI_REQUEST/@MIRelocationLoanIndicator.
        /// </summary>
        /// <value>True if the loan is a relocation loan. False otherwise.</value>
        public bool RelocationLoan { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the request is for a quote or policy.
        /// </summary>
        /// <value>True if requesting a quote. False if requesting a policy.</value>
        public bool IsQuoteRequest { get; set; }
        
        /// <summary>
        /// Gets the status condition included in the MI response by the vendor.
        /// </summary>
        /// <value>The status condition as a string, e.g. ERROR, Complete, Suspended.</value>
        public string Status 
        {
            get 
            {
                return this.HasResponse ? this.ResponseProvider.Status : string.Empty;
            }
        }

        /// <summary>
        /// Gets the message(s) returned by the MI vendor.
        /// </summary>
        /// <value>The message(s) from the MI vendor as a string.</value>
        public string VendorMessage 
        {
            get 
            {
                return this.HasResponse ? this.ResponseProvider.Message : string.Empty;
            }
        }

        /// <summary>
        /// Gets the certificate number returned by the MI vendor. Policy only.
        /// </summary>
        /// <value>The certificate number for the MI certificate.</value>
        public string CertificateNumber 
        { 
            get
            {
                return (this.IsQuoteRequest || !this.HasResponse) ? string.Empty : this.ResponseProvider.ResponseInfo.MICertificateIdentifier;
            }
        }

        /// <summary>
        /// Gets the lender's specific settings for the order, including vendor credentials.
        /// </summary>
        /// <value>Lender settings for a vendor.</value>
        public Lazy<MortgageInsuranceVendorBrokerSettings> LenderSettings =>
            new Lazy<MortgageInsuranceVendorBrokerSettings>(() =>
                MortgageInsuranceVendorBrokerSettings.ListActiveVendorByBrokerId(this.BrokerID).FirstOrDefault(c => c.VendorId == this.VendorID));

        /// <summary>
        /// Gets the quote number returned by the MI vendor. Quote only.
        /// </summary>
        /// <value>The quote number for the MI certificate.</value>
        public string QuoteNumber
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the expiration date of the MI certificate. Policy only.
        /// </summary>
        /// <value>The expiration date of the MI certificate.</value>
        public string CertExpirationDate 
        {
            get
            {
                return (this.IsQuoteRequest || !this.HasResponse) ? string.Empty : this.ResponseProvider.ResponseInfo.MICertificateExpirationDate;
            }
        }
            
        /// <summary>
        /// Gets the decision from the MI vendor. Policy only.
        /// </summary>
        /// <value>The decision made by the MI vendor, e.g. Approved, Declined.</value>
        public MI_MIDecisionTypeEnumerated Decision
        {
            get
            {
                return (this.IsQuoteRequest || !this.HasResponse) ? MI_MIDecisionTypeEnumerated.None : this.ResponseProvider.ResponseInfo.MIDecisionType;
            }
        }

        /// <summary>
        /// Gets a response provider with the associated MI response from the vendor. Will only be populated when the response is loaded from the database.
        /// </summary>
        /// <value>A response provider with the MI response.</value>
        public MIResponseProvider ResponseProvider { get; private set; }

        /// <summary>
        /// Gets an HTML string with a link to the quote response viewer for the current quote.
        /// </summary>
        /// <value>An HTML string with a link to the quote response viewer, or an empty string if this is a policy order.</value>
        public string QuoteResponseViewerLink
        {
            get
            {
                if (!this.HasResponse)
                {
                    return string.Empty;
                }

                if (this.ResponseProvider.HasError)
                {
                    string error = string.IsNullOrEmpty(this.VendorMessage) ? ErrorMessages.MIFramework.GenericBadQuote() : this.VendorMessage;
                    error = string.Format("{0}{1}Transaction ID: {2}{1}Order Number: {3}", error, System.Environment.NewLine, this.TransactionID, this.OrderNumber);

                    return string.Format(
                        "<a href=\"#\" onclick=\"return f_display_message({0});\">view error</a>",
                        LendersOffice.AntiXss.AspxTools.JsString(error));
                }

                return this.IsQuoteRequest ? string.Format(
                    "<a href=\"#\" onclick=\"window.open('{0}', '_blank', 'width=800px, height=650px, top=100px, left=100px, resizable');return false\">view quote</a>",
                    MIUtil.QuoteResponseViewerURL(this.LoanID, this.ApplicationID, this.OrderNumber, this.TransactionID)) : string.Empty;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this order has been populated with an MI response.
        /// </summary>
        /// <value>True if the response has been populated. Otherwise false.</value>
        public bool HasResponse
        {
            get
            {
                return this.ResponseProvider != null && this.ResponseProvider.HasResponseInfo;
            }
        }

        /// <summary>
        /// Retrieves a list of MI orders for a given loan file.
        /// </summary>
        /// <param name="loanID">The sLId of the loan file.</param>
        /// <param name="brokerID">The brokerID of the lender associated with the MI transaction.</param>
        /// <param name="applicationID">The aAppId of the loan application.</param>
        /// <param name="userID">The userID of the user accessing the orders, or null if the transaction is asynchronous.</param>
        /// <returns>A list of MI quote and policy orders.</returns>
        public static IEnumerable<MIOrderInfo> RetrieveOrdersByLoanId(Guid loanID, Guid brokerID, Guid applicationID, Guid? userID)
        {
            List<MIOrderInfo> orders = new List<MIOrderInfo>();
            SqlParameter[] parameters = 
                                        {
                                            new SqlParameter("@sLId", loanID)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerID, "MORTGAGE_INSURANCE_ORDER_INFO_ListByLoan", parameters))
            {
                while (reader.Read())
                {
                    orders.Add(new MIOrderInfo(reader, brokerID, applicationID, userID));
                }
            }

            return orders;
        }

        /// <summary>
        /// Retrieves an MI transaction with the given order number and transaction ID.
        /// </summary>
        /// <param name="transactionID">The ID assigned to the MI transaction by LQB.</param>
        /// <param name="orderNumber">The order number assigned to the MI order by LQB.</param>
        /// <param name="loanID">The sLId of the loan file.</param>
        /// <param name="brokerID">The brokerID of the lender associated with the MI transaction.</param>
        /// <param name="applicationID">The aAppId of the loan application.</param>
        /// <returns>An MI order containing data for the given transaction.</returns>
        public static MIOrderInfo RetrieveTransaction(Guid transactionID, string orderNumber, Guid loanID, Guid brokerID, Guid applicationID)
        {
            foreach (MIOrderInfo order in RetrieveTransactions(orderNumber, loanID, brokerID, applicationID))
            {
                if (order.TransactionID == transactionID)
                {
                    return order;
                }
            }

            return null;
        }

        /// <summary>
        /// Retrieves a list of MI transactions for a given order number.
        /// </summary>
        /// <param name="orderNumber">The order number assigned to the MI order by LQB.</param>
        /// <param name="loanID">The sLId of the loan file.</param>
        /// <param name="brokerID">The brokerID of the lender associated with the MI transaction.</param>
        /// <param name="applicationID">The aAppId of the loan application.</param>
        /// <returns>A list of MI orders containing transaction data.</returns>
        public static IEnumerable<MIOrderInfo> RetrieveTransactions(string orderNumber, Guid loanID, Guid brokerID, Guid applicationID)
        {
            List<MIOrderInfo> orders = new List<MIOrderInfo>();
            SqlParameter[] parameters = 
                                        {
                                            new SqlParameter("@sLId", loanID),
                                            new SqlParameter("@OrderNumber", orderNumber)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerID, "MORTGAGE_INSURANCE_ORDER_INFO_Retrieve", parameters))
            {
                while (reader.Read())
                {
                    orders.Add(new MIOrderInfo(reader, brokerID, applicationID, null));
                }
            }

            return orders;
        }

        /// <summary>
        /// Generates an XML string from the MISMO 2.3.1 MI request.
        /// </summary>
        /// <returns>The MISMO 2.3.1 MI request as an XML string.</returns>
        public string SerializeRequest()
        {
            if (this.MortgageInsuranceRequest == null)
            {
                return string.Empty;
            }

            byte[] bytes = null;
            int contentLength = 0;

            using (MemoryStream stream = new MemoryStream(5000))
            {
                XmlWriterSettings writerSettings = new XmlWriterSettings();
                writerSettings.Encoding = new UTF8Encoding(false); // Skip BOM
                writerSettings.OmitXmlDeclaration = true;

                using (XmlWriter writer = XmlWriter.Create(stream, writerSettings))
                {
                    LendersOffice.Conversions.Mismo23.LoanMismo23Exporter exporter = new LendersOffice.Conversions.Mismo23.LoanMismo23Exporter(this.LoanID, true, true);
                    exporter.Export(writer);

                    writer.Flush();
                    contentLength = (int)stream.Position;
                }

                bytes = stream.GetBuffer();
            }

            string mismoLoanXml = Encoding.UTF8.GetString(bytes, 0, contentLength);
            mismoLoanXml = mismoLoanXml.Replace("$", "$$").TrimWhitespaceAndBOM(); // 9/9/2014 BB - This escapes any $ characters so that they will be treated as literals by Regex.Replace

            //// 10/14/2014 BB - Replace the MI coverage % from the loan file with that from the MI order page. The MI vendor only accepts a whole percentage without decimals.
            if (this.IsQuoteRequest)
            {
                LosConvert valueConvertor = new LosConvert(FormatTarget.MismoClosing);

                int roundedCoverage = (int)Math.Round(this.CoveragePercent);
                string coverageAttr = string.Format("MICoveragePercent=\"{0}\"", valueConvertor.ToCountString(roundedCoverage));
                coverageAttr = coverageAttr.Replace("$", "$$").TrimWhitespaceAndBOM();
                mismoLoanXml = Regex.Replace(mismoLoanXml, @"MICoveragePercent=""[^""]*""", coverageAttr);
            }

            string requestXML = SerializationHelper.XmlSerializeStripDefaultNamespace(this.MortgageInsuranceRequest, true);
            requestXML = Regex.Replace(requestXML, "<LOAN_PLACEHOLDER[\\s]*/>", mismoLoanXml);

            return requestXML;
        }

        /// <summary>
        /// Store the response from the MI vendor in the MORTGAGE_INSURANCE_ORDER_INFO table.
        /// </summary>
        /// <param name="maskedResponseXML">The response from the MI vendor in serialized XML format. Mask the passwords and strip the base64encoded PDF data prior to calling this method.</param>
        public void StoreResponse(string maskedResponseXML)
        {
            List<SqlParameter> parameters = new List<SqlParameter>(9);
            parameters.Add(new SqlParameter("TransactionId", this.TransactionID));
            parameters.Add(new SqlParameter("sLId", this.LoanID));
            parameters.Add(new SqlParameter("VendorId", this.VendorID));
            parameters.Add(new SqlParameter("OrderNumber", this.OrderNumber));
            parameters.Add(new SqlParameter("OrderedDate", this.OrderedDate));
            parameters.Add(new SqlParameter("ResponseXmlContent", maskedResponseXML));
            parameters.Add(new SqlParameter("MIPremiumType", (int)this.PremiumType));
            parameters.Add(new SqlParameter("@UpfrontPremiumMismoValue", this.SplitPremiumMismoValue));
            parameters.Add(new SqlParameter("PremiumRefundability", (int)this.Refundability));
            parameters.Add(new SqlParameter("UFMIPFinanced", this.UfmipFinanced));
            parameters.Add(new SqlParameter("LenderIdentifier", this.MasterPolicyNumber));
            parameters.Add(new SqlParameter("QuoteNumber", this.QuoteNumber));
            parameters.Add(new SqlParameter("IsQuote", this.IsQuoteRequest));
            parameters.Add(new SqlParameter("OriginalQuoteOrderNumber", this.OriginalQuoteOrderNumber));
            parameters.Add(new SqlParameter("DelegationType", this.DelegationType));

            StoredProcedureHelper.ExecuteNonQuery(this.BrokerID, "MORTGAGE_INSURANCE_ORDER_INFO_Save", 3, parameters);
        }

        /// <summary>
        /// Populates MI order information from the given prior response from the vendor.
        /// </summary>
        /// <param name="responseProvider">An <see cref="MIResponseProvider" /> object with the MI response data.</param>
        public void PopulateResponseData(MIResponseProvider responseProvider)
        {
            this.ResponseProvider = responseProvider;
            this.PopulateResponseData();
        }

        /// <summary>
        /// Populates MI order information from the MI response.
        /// </summary>
        public void PopulateResponseData()
        {
            if (this.ResponseProvider.HasResponseInfo)
            {
                MIResponse midata = this.ResponseProvider.ResponseInfo;

                decimal coverage = 0.00m;
                decimal.TryParse(midata.MICoveragePercent, out coverage);
                this.CoveragePercent = coverage;

                if (this.ResponseProvider.ResponseInfo.MIApplicationType != MI_MIApplicationTypeEnumerated.None)
                {
                    this.IsQuoteRequest = this.ResponseProvider.IsQuoteResponse;
                }

                this.PremiumAtClosing = midata.MIInitialPremiumAtClosingType;
                this.RenewalType = midata.MIRenewalCalculationType;
                this.RelocationLoan = this.ResponseProvider.Relocation;
                this.QuoteNumber = this.ResponseProvider.QuoteNumber;

                if (string.IsNullOrEmpty(this.MasterPolicyNumber))
                {
                    this.MasterPolicyNumber = midata.MILenderIdentifier;
                }

                if (this.ResponseProvider.PremiumType != E_sProdConvMIOptionT.NoMI && this.ResponseProvider.PremiumType != E_sProdConvMIOptionT.Blank)
                {
                    this.PremiumType = this.ResponseProvider.PremiumType;
                }
            }
        }
    }
}