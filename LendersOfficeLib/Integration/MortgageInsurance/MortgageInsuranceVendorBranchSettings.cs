﻿// <copyright file="MortgageInsuranceVendorBranchSettings.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Isaac Ribakoff
//  Date:   10/1/2014 7:16:09 PM
// </summary>
namespace LendersOffice.Integration.MortgageInsurance
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Represents a branch's Mortgage Insurance Vendor (PMI Provider) settings.
    /// </summary>
    public class MortgageInsuranceVendorBranchSettings
    {
        /// <summary>
        /// The identifier for the encryption key used to encrypt <see cref="Password"/>. 
        /// </summary>
        private EncryptionKeyIdentifier encryptionKeyId;

        /// <summary>
        /// A lazy value of the password, used to delay decryption and loading until necessary.
        /// </summary>
        private Lazy<string> lazyPassword;

        /// <summary>
        /// Initializes a new instance of the <see cref="MortgageInsuranceVendorBranchSettings" /> class.
        /// </summary>
        /// <param name="vendorId">MI Vendor ID to be associated with this branch setting instance.</param>
        /// <param name="branchId">Branch ID to be associated with this branch setting instance.</param>
        public MortgageInsuranceVendorBranchSettings(Guid vendorId, Guid branchId)
        {
            this.VendorId = vendorId;
            this.BranchId = branchId;
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="MortgageInsuranceVendorBranchSettings" /> class from database.
        /// </summary>
        /// <param name="reader"><see cref="IDataRecord" /> containing column values to create <see cref="MortgageInsuranceVendorBranchSettings" /> object.</param>
        /// <param name="branchId">Unique identifier associated with the lender branch.</param>
        private MortgageInsuranceVendorBranchSettings(IDataRecord reader, Guid branchId)
        {
            this.VendorId = (Guid)reader["VendorId"];
            this.BranchId = branchId;
            this.UserName = (string)reader["UserName"];
            byte[] passwordBytes = (byte[])reader["Password"];
            EncryptionKeyIdentifier encryptionKeyId = EncryptionKeyIdentifier.Create((Guid)reader["EncryptionKeyId"]).ForceValue();
            this.lazyPassword = new Lazy<string>(() => Drivers.Encryption.EncryptionHelper.DecryptString(encryptionKeyId, passwordBytes));
            this.encryptionKeyId = encryptionKeyId;
            this.AccountId = (string)reader["AccountId"];
            this.PolicyId = (string)reader["PolicyId"];
            this.IsDelegated = (bool)reader["IsDelegated"];
        }

        /// <summary>Gets MI Vendor ID of this Branch Setting instance.</summary>
        /// <value>Unique Identifier representing MI Vendor ID of this Branch Setting instance.</value>
        public Guid VendorId { get; private set; }

        /// <summary>Gets the lending branch (Branch ID) of this Branch Setting instance.</summary>
        /// <value>Unique identifier representing the lending branch (Branch ID) of this Branch Setting instance.</value>
        public Guid BranchId { get; private set; }

        /// <summary>Gets or sets the UserName of this Branch Setting instance.</summary>
        /// <value>String representing the UserName of this Branch Setting instance.</value>
        public string UserName { get; set; }

        /// <summary>Gets or sets the Password of this Branch Setting instance.</summary>
        /// <value>String representing the Password of this Branch Setting instance.</value>
        public string Password
        {
            get
            {
                return this.lazyPassword?.Value;
            }

            set
            {
                if (value == ConstAppDavid.FakePasswordDisplay || value.ToCharArray().All(c => c == '*'))
                {
                    return;
                }

                this.lazyPassword = new Lazy<string>(() => value);
            }
        }

        /// <summary>Gets or sets the Account ID of this Branch Setting instance.</summary>
        /// <value>String representing the Account ID of this Branch Setting instance.</value>
        public string AccountId { get; set; }

        /// <summary>Gets or sets the Policy ID of this Branch Setting instance.</summary>
        /// <value>String representing the Policy ID of this Branch Setting instance.</value>
        public string PolicyId { get; set; }

        /// <summary>Gets or sets a value indicating whether lending branch can submit delegated policy orders for this instance.</summary>
        /// <value>Boolean indicating whether lending branch can submit delegated policy orders for this instance.</value>
        public bool IsDelegated { get; set; }

        /// <summary>
        /// Retrieves a list of Active MI Vendors Associated with a given branch.
        /// </summary>
        /// <param name="brokerId">Broker id of the branch.</param>
        /// <param name="branchId">The branch's unique identifier.</param>
        /// <returns>List of Active MI Vendors Associated with given branch.</returns>
        public static IEnumerable<MortgageInsuranceVendorBranchSettings> ListCredentialsByBranchId(Guid brokerId, Guid branchId)
        {
            List<MortgageInsuranceVendorBranchSettings> list = new List<MortgageInsuranceVendorBranchSettings>();
            SqlParameter[] parameters = 
                                        {
                                            new SqlParameter("@BranchId", branchId)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "MORTGAGE_INSURANCE_VENDOR_BRANCH_SETTINGS_ListVendors", parameters))
            {
                while (reader.Read())
                {
                    list.Add(new MortgageInsuranceVendorBranchSettings(reader, branchId));
                }
            }

            return list;
        }

        /// <summary>
        /// Retrieves lender settings.
        /// </summary>
        /// <param name="brokerId">Broker id.</param>
        /// <param name="branchId">Branch id.</param>
        /// <param name="vendorId">Vendor id.</param>
        /// <returns>MI vendor branch settings.</returns>
        public static MortgageInsuranceVendorBranchSettings RetrieveLenderSettings(Guid brokerId, Guid branchId, Guid vendorId)
        {
            try
            {
                return ListCredentialsByBranchId(brokerId, branchId).FirstOrDefault(vendor => vendor.VendorId == vendorId);
            }
            catch (ArgumentNullException)
            {
            }
            catch (InvalidOperationException)
            {
            }

            return null;
        }

        /// <summary>
        /// Remove MI Vendor Branch Setting instance from database.
        /// </summary>
        /// <param name="brokerId">Broker Id of the mortgage insurance branch settings.</param>
        public void ClearEntry(Guid brokerId)
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@VendorId", this.VendorId),
                new SqlParameter("@BranchId", this.BranchId),
            };
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "MORTGAGE_INSURANCE_VENDOR_BRANCH_SETTINGS_ClearBranchCredential", 3, parameters);
        }

        /// <summary>
        /// Saves branch login credentials for given MI Vendor instance to database.
        /// </summary>
        /// <param name="brokerId">Broker Id of the mortgage insurance branch settings.</param>
        public void SaveEntry(Guid brokerId)
        {
            if (this.encryptionKeyId == default(EncryptionKeyIdentifier))
            {
                this.encryptionKeyId = Drivers.Encryption.EncryptionHelper.GenerateNewKey();
            }

            SqlParameter[] parameters =
            {
                new SqlParameter("@VendorId", this.VendorId),
                new SqlParameter("@BranchId", this.BranchId),
                new SqlParameter("@UserName", this.UserName),
                new SqlParameter("@Password", Drivers.Encryption.EncryptionHelper.EncryptString(this.encryptionKeyId, this.Password)),
                new SqlParameter("@EncryptionKeyId", this.encryptionKeyId.Value),
                new SqlParameter("@AccountId", this.AccountId),
                new SqlParameter("@PolicyId", this.PolicyId),
                new SqlParameter("@IsDelegated", this.IsDelegated)
            };
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "MORTGAGE_INSURANCE_VENDOR_BRANCH_SETTINGS_Save", 3, parameters);
        }        
    }
}
