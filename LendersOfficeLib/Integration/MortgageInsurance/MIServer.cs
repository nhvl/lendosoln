﻿// <copyright file="MIServer.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/22/2014 11:49:10 AM
// </summary>
namespace LendersOffice.Integration.MortgageInsurance
{
    using System;
    using DataAccess;
    using LendersOffice.Commands;
    using LendersOffice.Common;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Implements the web requests to the MI vendor and handles the responses.
    /// </summary>
    public class MIServer
    {
        /// <summary>
        /// Transmits a request XML packet to the Mortgage Insurance vendor.
        /// </summary>
        /// <param name="orderInfo">An MIOrderInfo object containing the MI request, order options, and related information.</param>
        /// <param name="vendorId">The Guid assigned to the MI vendor by LQB.</param>
        /// <param name="production">True if the request should be sent to the vendor's production URL. False to send to their test environment URL.</param>
        /// <param name="submissionResult">A summary of the submission result.</param>
        /// <param name="edocUploadFailed">A bool value indicating whether edocs were successfully uploaded.</param>
        /// <returns>True if the submission was successful. False if there was an error.</returns>
        public static bool SubmitRequest(MIOrderInfo orderInfo, Guid vendorId, bool production, out string submissionResult, out bool edocUploadFailed)
        {
            var vendor = MortgageInsuranceVendorConfig.RetrieveById(vendorId);
            var exportPath = LqbAbsoluteUri.Create(production ? vendor.ExportPath_Production : vendor.ExportPath_Test).ForceValue();
            MortgageInsuranceHttpCommunication communication = new MortgageInsuranceHttpCommunication(orderInfo, vendor, exportPath);

            IIntegrationDriver<MIResponseProvider> driver = GenericLocator<IIntegrationDriverFactory<MIResponseProvider>>.Factory.Create();
            Result<MIResponseProvider> result = driver.ExecuteCommunication(communication);
            if (result.HasError)
            {
                submissionResult = (result.Error as CBaseException)?.UserMessage ?? (result.Error as LqbException)?.Context?.Serialize() ?? ErrorMessages.Generic;

                edocUploadFailed = false;
                return false;
            }

            return ProcessResponse(result.Value, orderInfo, false, out submissionResult, out edocUploadFailed);
        }

        /// <summary>
        /// Stores the data included in an MI response on the loan file, and uploads any included documents to EDocs.
        /// </summary>
        /// <param name="responseProvider">An <see cref="MIResponseProvider"/> object with the response from an MI vendor.</param>
        /// <param name="orderInfo">An <see cref="MIOrderInfo"/> object containing the MI request XML, order options, and related information.</param>
        /// <param name="asynchronous">True if the mortgage insurance vendor responded asynchronously. False if the response was synchronous.</param>
        /// <param name="submissionResult">A string to which the summary of the submission result will be written.</param>
        /// <param name="edocUploadFailed">A bool value indicating if edocs uploaded successfully.</param>
        /// <returns>A boolean indicating whether the response was successfully processed.</returns>
        public static bool ProcessResponse(MIResponseProvider responseProvider, MIOrderInfo orderInfo, bool asynchronous, out string submissionResult, out bool edocUploadFailed)
        {
            bool success = true;
            int edocsUploaded = 0;

            // OPM 473112. Pricing MI requests cannot be stored or logged
            // due to their call volume.
            bool isPricingMode = orderInfo.RequestType == E_MiRequestType.PricingEngine;
            submissionResult = string.Empty;
            edocUploadFailed = false;

            if (success)
            {
                success = !responseProvider.HasError;
                submissionResult = responseProvider.Message;

                // 10/27/2014 BB - There's no need to store a busted quote/synchronous policy error in the database since those are logged and displayed to the user immediately.
                // But we should capture all asynchronous policy responses as long as there is a success/failure to store.
                if (!isPricingMode && asynchronous && responseProvider.HasResponse)
                {
                    // For synchronous responses, see MortgageInsuranceHttpCommunication.ParseResponse(HttpRequestOptions)
                    orderInfo.StoreResponse(MIResponseProvider.MaskSensitiveResponseData(responseProvider));
                }

                // 6/2/2015 BB - OPM 213965. Upload any documents from the vendor even if there is an Error status. E.G. Declination letter.
                // If the AutoSave has not been set up for MI, don't bother attempting to upload.
                if (!isPricingMode && MortgageInsuranceHttpCommunication.AutoSaveIsEnabledForDocType(responseProvider.IsQuoteResponse, orderInfo.BrokerID))
                {
                    edocsUploaded = MIUtil.UploadDocuments(responseProvider, orderInfo, asynchronous);
                }
                else if (!isPricingMode)
                {
                    // Only display a message if there were actual documents to upload.
                    if (responseProvider.EncodedPDF.Count > 0)
                    {
                        edocUploadFailed = true;
                    }
                }
            }

            if (success)
            {
                //// Only set loan fields if the response is a policy response. Quote responses are saved to the DB above, and then the data is used at policy order time.
                if (!responseProvider.IsQuoteResponse)
                {
                    MILoanSetter setter = new MILoanSetter(orderInfo.LoanID, responseProvider.ResponseInfo, orderInfo.VendorID, responseProvider.SplitPremium, orderInfo.UfmipFinanced, orderInfo.IsQuoteRequest);
                    setter.UpdateLoanWithPolicyInfo(!asynchronous);
                }
            }

            if (edocsUploaded > 0)
            {
                submissionResult += string.Format("Process complete. {0} documents were uploaded to EDocs.", edocsUploaded);
            }
            else if (edocUploadFailed)
            {
                submissionResult += "Warning: Documents returned have not been automatically uploaded to eDocs because eDocs AutoSave is not set up for Mortgage Insurance. Please have your admin user with access to the eDocs Configuration to enable this.";
            }

            //// 2/19/2015 BB - Add a generic success/error message if there was a status from the vendor w/o a message/description component. This is fine on order OK.
            //// If the result was error w/o any error message then the generic processing failure will prompt the user to contact LQB. LQB can then ping the vendor for a fix, i.e. please include an error message in your response.
            if (string.IsNullOrEmpty(submissionResult))
            {
                submissionResult = success ? "Submission process complete." : ErrorMessages.MIFramework.GenericProcessingFailure();
            }

            return success;
        }
    }
}
