﻿namespace LendersOffice.Integration.MortgageInsurance
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using Mismo231.MI;
    using ObjLib.BackgroundJobs;
    using Security;

    /// <summary>
    /// Request handler for MI framework.
    /// </summary>
    public class MIRequestHandler
    {
        /// <summary>
        /// The request data.
        /// </summary>
        private MIRequestData requestData;
        
        /// <summary>
        /// The MIOrderInfo that can be serailized to reate the request payload.
        /// </summary>
        private MIOrderInfo serializableOrderInfo = null;
        
        /// <summary>
        /// Gets lender MI settings.
        /// </summary>
        private Lazy<MortgageInsuranceVendorBrokerSettings> lenderSettings;

        /// <summary>
        /// Gets branch MI settings.
        /// </summary>
        private Lazy<MortgageInsuranceVendorBranchSettings> branchSettings;

        /// <summary>
        /// Initializes a new instance of the <see cref="MIRequestHandler"/> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        public MIRequestHandler(MIRequestData requestData)
        {
            this.requestData = requestData;

            this.lenderSettings = new Lazy<MortgageInsuranceVendorBrokerSettings>(() =>
                MortgageInsuranceVendorBrokerSettings.ListActiveVendorByBrokerId(this.requestData.Principal.BrokerId).FirstOrDefault(vendor => vendor.VendorId == this.requestData.VendorId));

            this.branchSettings = new Lazy<MortgageInsuranceVendorBranchSettings>(() =>
                MortgageInsuranceVendorBranchSettings.ListCredentialsByBranchId(this.requestData.Principal.BrokerId, this.requestData.BranchId).FirstOrDefault(vendor => vendor.VendorId == this.requestData.VendorId));
        }

        /// <summary>
        /// Checks the background job processor to see if an MI request has finished.
        /// </summary>
        /// <param name="publicJobId">The job id of the MI request job.</param>
        /// <returns>The results. Can be Error, Processing, or Completed results.</returns>
        public static MIRequestResult CheckForRequestResults(Guid publicJobId)
        {
            return MIFrameworkRequestJob.CheckJob(publicJobId);
        }

        /// <summary>
        /// Submits the MI request synchronously.
        /// </summary>
        /// <param name="checkWorkflow">Whether to check or skip workflow.</param>
        /// <returns>The MI request result.</returns>
        public MIRequestResult SubmitSynchronously(bool checkWorkflow)
        {
            List<string> errors;
            if (!this.InitializeSerializableOrderInfo(checkWorkflow, out errors))
            {
                return MIRequestResult.CreateErrorResult(errors);
            }

            string response;
            bool edocUploadFailed;
            if (MIServer.SubmitRequest(this.serializableOrderInfo, this.serializableOrderInfo.VendorID, this.lenderSettings.Value?.IsProduction ?? false, out response, out edocUploadFailed))
            {
                return MIRequestResult.CreateSuccessResult(response, this.serializableOrderInfo.OrderNumber, this.serializableOrderInfo.TransactionID, edocUploadFailed);
            }
            else
            {
                errors.Add(response);
                return MIRequestResult.CreateErrorResult(errors);
            }
        }

        /// <summary>
        /// Submits the MI request to the BackgroundJobProcessor. The caller will need to poll at a later time to check if the MI request has completed.
        /// </summary>
        /// <param name="checkWorkflow">Whether to check or skip workflow.</param>
        /// <returns>The result. Error result if something went wrong before being passed to the processor. A processing result if successfully passed to processor queue.</returns>
        public MIRequestResult SubmitToProcessor(bool checkWorkflow)
        {
            List<string> errors;
            if (!this.ValidateRequestData(checkWorkflow, out errors))
            {
                // We'll do preliminary checks in the front end server so users don't have to wait for the processor just to see if certain inputs are correct.
                return MIRequestResult.CreateErrorResult(errors);
            }

            return MIRequestResult.CreateProcessingResult(MIFrameworkRequestJob.AddMIFrameworkRequestJob(this.requestData));
        }

        /// <summary>
        /// Gets the payload xml that will be used in the MI request.
        /// </summary>
        /// <param name="checkWorkflow">Whether to check or skip workflow.</param>
        /// <param name="errors">The errors encountered.</param>
        /// <returns>The payload XML. Null if there was an error.</returns>
        public string GetRequestXml(bool checkWorkflow, out List<string> errors)
        {
            errors = new List<string>();
            if (!this.InitializeSerializableOrderInfo(checkWorkflow, out errors))
            {
                return null;
            }

            return this.serializableOrderInfo.SerializeRequest();
        }

        /// <summary>
        /// Runs basic validation on the request data object.
        /// </summary>
        /// <param name="checkWorkflow">Whether to check or skip workflow.</param>
        /// <param name="errors">Validation errors found.</param>
        /// <returns>True if vald, false otherwise.</returns>
        private bool ValidateRequestData(bool checkWorkflow, out List<string> errors)
        {
            errors = new List<string>();
            if (this.requestData == null)
            {
                errors.Add("Incomplete request data provided.");
                return false;
            }

            if (checkWorkflow && this.requestData.Principal != null)
            {
                ConfigSystem.Operations.WorkflowOperation operationToCheck = null;
                if (this.requestData.IsQuote)
                {
                    operationToCheck = ConfigSystem.Operations.WorkflowOperations.OrderMIQuote;
                }
                else if (!this.requestData.IsQuote && this.requestData.DelegationType == DelegationType.NonDelegated)
                {
                    operationToCheck = ConfigSystem.Operations.WorkflowOperations.OrderMIPolicyNonDelegated;
                }
                else if (!this.requestData.IsQuote && this.requestData.DelegationType == DelegationType.Delegated)
                {
                    operationToCheck = ConfigSystem.Operations.WorkflowOperations.OrderMIPolicyDelegated;
                }

                if (operationToCheck == null)
                {
                    errors.Add(ErrorMessages.Generic);
                }
                else
                {
                    var result = Tools.IsWorkflowOperationAuthorized(this.requestData.Principal, this.requestData.LoanId, operationToCheck);
                    if (!result.Item1)
                    {
                        errors.Add(result.Item2);
                    }
                }
            }

            if (this.requestData.IsQuote)
            {
                if (this.requestData.CoveragePercent <= 0.00M)
                {
                    errors.Add("Please enter a positive percentage for the MI Coverage %.");
                }

                if (this.requestData.PremiumAtClosing == MI_MIInitialPremiumAtClosingTypeEnumerated.None)
                {
                    errors.Add("Please select a Premium at Closing option.");
                }

                if (string.IsNullOrEmpty(this.requestData.MasterPolicyNumber))
                {
                    errors.Add("Please enter a Master Policy Number");
                }

                if (this.requestData.PremiumType == E_sProdConvMIOptionT.BorrPaidSplitPrem && string.IsNullOrEmpty(this.requestData.SplitPremiumMismoValue))
                {
                    errors.Add("Please select an Upfront Percentage amount");
                }
            }
            else if (this.requestData.RequestType == DataAccess.E_MiRequestType.PricingEngine)
            {
                // Pricing must only ever use quotes.  This is a programmer error
                // rather than a user error, but we do not want this going through.
                errors.Add("Incorrect MI request format.");
            }

            return !errors.Any();
        }

        /// <summary>
        /// Initializes the serializable MIOrderInfo object.
        /// </summary>
        /// <param name="checkWorkflow">Whether to check or skip workflow.</param>
        /// <param name="errors">Any errors found.</param>
        /// <returns>True if initialized. False otherwise.</returns>
        private bool InitializeSerializableOrderInfo(bool checkWorkflow, out List<string> errors)
        {
            errors = new List<string>();

            if (this.serializableOrderInfo != null)
            {
                return true;
            }

            if (!this.ValidateRequestData(checkWorkflow, out errors))
            {
                return false;
            }

            if (this.lenderSettings.Value == null && this.branchSettings.Value == null)
            {
                errors.Add(ErrorMessages.MIFramework.NoCredentials());
                return false;
            }

            MIOrderInfo baseOrderInfo = new MIOrderInfo(
                this.requestData.LoanId,
                this.requestData.ApplicationId,
                this.requestData.VendorId,
                this.requestData.PremiumType,
                this.requestData.SplitPremiumMismoValue,
                this.requestData.Refundability,
                this.requestData.UFMIPFinanced,
                this.requestData.Principal.BrokerId,
                this.requestData.Principal.UserId,
                this.requestData.MasterPolicyNumber,
                this.requestData.CoveragePercent,
                this.requestData.RenewalType,
                this.requestData.PremiumAtClosing,
                this.requestData.IsRelocationLoan,
                this.requestData.IsQuote,
                this.requestData.QuoteNumber,
                this.requestData.OriginalQuoteNumber,
                this.requestData.DelegationType,
                this.requestData.RequestType);

            MIRequestProvider requestProvider = new MIRequestProvider(baseOrderInfo.LoanID, this.requestData.Principal);
            if (this.lenderSettings != null)
            {
                this.serializableOrderInfo = requestProvider.CreateMortgageInsuranceRequest(baseOrderInfo, this.lenderSettings.Value);
            }
            else
            {
                this.serializableOrderInfo = requestProvider.CreateMortgageInsuranceRequest(baseOrderInfo, this.branchSettings.Value);
            }

            return true;
        }
    }
}
