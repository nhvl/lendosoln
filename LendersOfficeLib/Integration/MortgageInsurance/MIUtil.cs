﻿// <copyright file="MIUtil.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   9/25/2014 3:00:12 PM
// </summary>
namespace LendersOffice.Integration.MortgageInsurance
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web.UI.WebControls;
    using Adapter;
    using DataAccess;
    using EDocs;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.ObjLib.Edocs.AutoSaveDocType;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using Mismo231.MI;
    using Mismo231.MI.MortgageInsuranceResponse;

    /// <summary>
    /// Provides the source of the MI response, which will either come from the vendor or be loaded from the LQB database.
    /// </summary>
    public enum MIResponseDataSource
    {
        /// <summary>
        /// The response was received directly from the vendor.
        /// </summary>
        Vendor = 0,

        /// <summary>
        /// The response was loaded from the LQB database.
        /// </summary>
        LQBDB = 1
    }

    /// <summary>
    /// An error type for the asynchronous MI response handler.
    /// </summary>
    public enum MIAsyncResponseError
    {
        /// <summary>
        /// Failed to deserialize the response.
        /// </summary>
        DeserializationFailure = 0,

        /// <summary>
        /// The primary application ID could not be retrieved.
        /// </summary>
        NoAppID = 1,

        /// <summary>
        /// The lender's broker ID could not be retrieved.
        /// </summary>
        NoBrokerID = 2,

        /// <summary>
        /// The loan ID could not be resolved.
        /// </summary>
        NoLoanID = 3,

        /// <summary>
        /// No order with matching order number and transaction ID was found.
        /// </summary>
        NoOrder = 4,

        /// <summary>
        /// General response processing error.
        /// </summary>
        ProcessingError = 5
    }

    /// <summary>
    /// Provides helper methods for the mortgage insurance framework.
    /// </summary>
    public static class MIUtil
    {
        /// <summary>
        /// Binds the given dropdown list with the set of enabled MI vendors.
        /// </summary>
        /// <param name="ddl">The dropdown list to be bound.</param>
        /// <param name="enabledVendors">The list of MI vendors enabled for the lender.</param>
        /// <param name="activeVendors">The list of MI vendors configured in LendingQB.</param>
        public static void Bind_MIProvider(DropDownList ddl, IEnumerable<MortgageInsuranceVendorBrokerSettings> enabledVendors, IEnumerable<MortgageInsuranceVendorConfig> activeVendors)
        {
            ddl.Items.Add(new ListItem(string.Empty, string.Empty));

            var vendors = enabledVendors.Join(
                activeVendors,
                enabled => enabled.VendorId,
                all => all.VendorId,
                (enabled, all) => new ListItem(all.VendorTypeFriendlyDisplay, all.VendorId.ToString()))
                .OrderBy(item => item.Text);

            foreach (var vendor in vendors)
            {
                ddl.Items.Add(vendor);
            }

            Tools.SetDropDownListValue(ddl, string.Empty);
        }

        /// <summary>
        /// Binds the given dropdown list with the set of MI premium type options.
        /// </summary>
        /// <param name="ddl">The dropdown list to be bound.</param>
        /// <param name="singlePremiumEnabled">Indicates whether single premium is enabled for the lender..</param>
        /// <param name="splitPremiumEnabled">Indicates whether split premium is enabled for the lender.</param>
        public static void Bind_MIPremiumType(DropDownList ddl, bool singlePremiumEnabled, bool splitPremiumEnabled)
        {
            ddl.Items.Clear();
            Tools.Bind_sProdConvMIOptionT(ddl, singlePremiumEnabled, splitPremiumEnabled);

            ddl.Items.Remove(ddl.Items.FindByValue(E_sProdConvMIOptionT.NoMI.ToString("D")));
            ddl.Items.Remove(ddl.Items.FindByValue(E_sProdConvMIOptionT.Blank.ToString("D")));
            ddl.Items.Insert(0, new ListItem(string.Empty, string.Empty));

            Tools.SetDropDownListValue(ddl, string.Empty);
        }

        /// <summary>
        /// Binds the given dropdown list with the set of upfront premium amounts supported by the vendor.
        /// </summary>
        /// <param name="ddl">The dropdown list to be bound.</param>
        /// <param name="vendor">The vendor data.</param>
        public static void Bind_MISplitPremiumUpfrontPercentage(DropDownList ddl, MortgageInsuranceVendorConfig vendor)
        {
            ddl.Items.Clear();
            if (vendor != null && vendor.UsesSplitPremiumPlans)
            {
                foreach (var option in vendor.SplitPremiumOptions.OrderBy(o => o.UpfrontPercentage))
                {
                    ddl.Items.Add(new ListItem(option.UpfrontPercentage.ToString() + "%", option.MismoValue));
                }
            }

            ddl.Items.Insert(index: 0, item: new ListItem(string.Empty, string.Empty));
            Tools.SetDropDownListValue(ddl, E_sProdConvMIOptionT.BorrPaidSplitPrem.ToString("D"));
        }

        /// <summary>
        /// Binds the given dropdown list with the set of applicable premium $$refundability$$ options. This
        /// dropdown will be rebound dynamically based on the MI premium type.
        /// </summary>
        /// <param name="ddl">The dropdown list to be bound.</param>
        /// <param name="includeRefundableOptions">Indicates whether refundable options should be included.</param>
        public static void Bind_PremiumRefundability(DropDownList ddl, bool includeRefundableOptions)
        {
            ddl.Items.Clear();
            ddl.Items.Add(Tools.CreateEnumListItem(GetMIPremiumRefundableTypeDisplay(MI_MIPremiumRefundableTypeEnumerated.NotRefundable), MI_MIPremiumRefundableTypeEnumerated.NotRefundable));

            if (includeRefundableOptions)
            {
                ddl.Items.Add(Tools.CreateEnumListItem(GetMIPremiumRefundableTypeDisplay(MI_MIPremiumRefundableTypeEnumerated.Refundable), MI_MIPremiumRefundableTypeEnumerated.Refundable));
                ddl.Items.Add(Tools.CreateEnumListItem(GetMIPremiumRefundableTypeDisplay(MI_MIPremiumRefundableTypeEnumerated.RefundableWithLimits), MI_MIPremiumRefundableTypeEnumerated.RefundableWithLimits));
            }
        }

        /// <summary>
        /// Converts the given MI premium refundable type to a user-friendly string.
        /// </summary>
        /// <param name="value">The MI premium refundable type to convert.</param>
        /// <returns>A string representation of the given type.</returns>
        public static string GetMIPremiumRefundableTypeDisplay(MI_MIPremiumRefundableTypeEnumerated value)
        {
            switch (value)
            {
                case MI_MIPremiumRefundableTypeEnumerated.None:
                    return string.Empty;
                case MI_MIPremiumRefundableTypeEnumerated.NotRefundable:
                    return "Non-Refundable";
                case MI_MIPremiumRefundableTypeEnumerated.Refundable:
                    return "Refundable";
                case MI_MIPremiumRefundableTypeEnumerated.RefundableWithLimits:
                    return "Refundable with Limits";
                default:
                    throw new UnhandledEnumException(value);
            }
        }

        /// <summary>
        /// Binds the given dropdown list with the set of applicable MI renewal options. This
        /// dropdown will be rebound dynamically based on the premium type.
        /// </summary>
        /// <param name="ddl">The dropdown list to be bound.</param>
        /// <param name="bindRenewalOptions">Indicates whether to bind the renewal options Constant and
        /// Declining. If false, No Renewals will be bound instead.</param>
        public static void Bind_RenewalOption(DropDownList ddl, bool bindRenewalOptions)
        {
            ddl.Items.Clear();

            if (bindRenewalOptions)
            {
                ddl.Items.Add(Tools.CreateEnumListItem(GetMIRenewalCalculationTypeDisplay(MI_MIRenewalCalculationTypeEnumerated.Constant), MI_MIRenewalCalculationTypeEnumerated.Constant));
                ddl.Items.Add(Tools.CreateEnumListItem(GetMIRenewalCalculationTypeDisplay(MI_MIRenewalCalculationTypeEnumerated.Declining), MI_MIRenewalCalculationTypeEnumerated.Declining));
            }
            else
            {
                ddl.Items.Add(Tools.CreateEnumListItem(GetMIRenewalCalculationTypeDisplay(MI_MIRenewalCalculationTypeEnumerated.NoRenewals), MI_MIRenewalCalculationTypeEnumerated.NoRenewals));
            }
        }

        /// <summary>
        /// Converts the given MI renewal calculation type to a user-friendly string.
        /// </summary>
        /// <param name="value">The MI renewal calculation type to convert.</param>
        /// <returns>A string representation of the given type.</returns>
        public static string GetMIRenewalCalculationTypeDisplay(MI_MIRenewalCalculationTypeEnumerated value)
        {
            switch (value)
            {
                case MI_MIRenewalCalculationTypeEnumerated.Constant:
                    return "Constant";
                case MI_MIRenewalCalculationTypeEnumerated.Declining:
                    return "Declining";
                case MI_MIRenewalCalculationTypeEnumerated.None:
                    return string.Empty;
                case MI_MIRenewalCalculationTypeEnumerated.NoRenewals:
                    return "No Renewals";
                case MI_MIRenewalCalculationTypeEnumerated.NotApplicable:
                    return "Not Applicable";
                default:
                    throw new UnhandledEnumException(value);
            }
        }

        /// <summary>
        /// Binds the given dropdown list with the set of applicable payment options for the initial MI premium.
        /// This dropdown will be rebound dynamically based on the premium type.
        /// </summary>
        /// <param name="ddl">The dropdown list to be bound.</param>
        /// <param name="includeDeferred">Indicates whether to include Deferred. If false, only Prepaid
        /// will be bound.</param>
        public static void Bind_PremiumAtClosing(DropDownList ddl, bool includeDeferred)
        {
            ddl.Items.Clear();

            if (includeDeferred)
            {
                ddl.Items.Add(Tools.CreateEnumListItem(GetMIInitialPremiumAtClosingTypeDisplay(MI_MIInitialPremiumAtClosingTypeEnumerated.Deferred), MI_MIInitialPremiumAtClosingTypeEnumerated.Deferred));
            }

            ddl.Items.Add(Tools.CreateEnumListItem(GetMIInitialPremiumAtClosingTypeDisplay(MI_MIInitialPremiumAtClosingTypeEnumerated.Prepaid), MI_MIInitialPremiumAtClosingTypeEnumerated.Prepaid));
        }

        /// <summary>
        /// Converts the given MI initial premium at closing type to a user-friendly string.
        /// </summary>
        /// <param name="value">The MI initial premium at closing type to convert.</param>
        /// <returns>A string representation of the given type.</returns>
        public static string GetMIInitialPremiumAtClosingTypeDisplay(MI_MIInitialPremiumAtClosingTypeEnumerated value)
        {
            switch (value)
            {
                case MI_MIInitialPremiumAtClosingTypeEnumerated.Deferred:
                    return "Deferred";
                case MI_MIInitialPremiumAtClosingTypeEnumerated.None:
                    return string.Empty;
                case MI_MIInitialPremiumAtClosingTypeEnumerated.Prepaid:
                    return "Prepaid";
                default:
                    throw new UnhandledEnumException(value);
            }
        }

        /// <summary>
        /// Converts the given MI decision type to a user-friendly string.
        /// </summary>
        /// <param name="value">The MI decision type to convert.</param>
        /// <returns>A string representation of the given type.</returns>
        public static string GetMIDecisionTypeDisplay(MI_MIDecisionTypeEnumerated value)
        {
            switch (value)
            {
                case MI_MIDecisionTypeEnumerated.Approved:
                    return "Approved";
                case MI_MIDecisionTypeEnumerated.ApprovedAfterReevaluation:
                    return "Approved After Re-evaluation";
                case MI_MIDecisionTypeEnumerated.ConditionedApproval:
                    return "Conditioned Approval";
                case MI_MIDecisionTypeEnumerated.Declined:
                    return "Declined";
                case MI_MIDecisionTypeEnumerated.None:
                    return string.Empty;
                case MI_MIDecisionTypeEnumerated.Suspended:
                    return "Suspended";
                default:
                    throw new UnhandledEnumException(value);
            }
        }

        /// <summary>
        /// Parses the list of taxes for the all-in tax rate.
        /// </summary>
        /// <param name="taxes">A list of MI tax premiums.</param>
        /// <returns>The total tax rate.</returns>
        public static decimal GetTaxRate(List<MIPremiumTax> taxes)
        {
            decimal totalTaxRate = 0.00m;
            LosConvert valueConvertor = new LosConvert(FormatTarget.MismoClosing);

            //// Taxes will not always be levied. It's subject to the locale.
            foreach (MIPremiumTax tax in taxes ?? new List<MIPremiumTax>())
            {
                if (tax._CodeType == MI_MIPremiumTaxCodeTypeEnumerated.AllTaxes)
                {
                    totalTaxRate = valueConvertor.ToRate(tax._CodePercent);
                    break;
                }
                else
                {
                    totalTaxRate += valueConvertor.ToRate(tax._CodePercent);
                }

                ////tax._CodeAmount;
            }

            return totalTaxRate;
        }

        /// <summary>
        /// Adds the given tax rate to the given premium.
        /// </summary>
        /// <param name="premium">The premium rate or percentage as a string.</param>
        /// <param name="taxRate">The tax rate as a percentage of the premium.</param>
        /// <param name="premiumIsRate">True if the premium is given as a rate. False if given as a percentage.</param>
        /// <returns>A string representation of the total rate inclusive of premium and taxes.</returns>
        public static string AddTaxesToPremium(string premium, decimal taxRate, bool premiumIsRate)
        {
            LosConvert valueConvertor = new LosConvert(FormatTarget.MismoClosing);

            decimal premiumRate = AddTaxesToPremium(valueConvertor.ToRate(premium), taxRate, premiumIsRate);

            return valueConvertor.ToRateString9DecimalDigits(premiumRate);
        }

        /// <summary>
        /// Adds the given tax rate to the given premium.
        /// </summary>
        /// <param name="premium">The premium rate or percentage.</param>
        /// <param name="taxRate">The tax rate as a percentage of the premium.</param>
        /// <param name="premiumIsRate">True if the premium is given as a rate. False if given as a percentage.</param>
        /// <returns>The total rate inclusive of premium and taxes.</returns>
        public static decimal AddTaxesToPremium(decimal premium, decimal taxRate, bool premiumIsRate)
        {
            premium += (taxRate / 100) * premium;

            if (premiumIsRate)
            {
                premium *= 100;
            }

            return premium;
        }

        /// <summary>
        /// Converts the given MI premium to a decimal.
        /// </summary>
        /// <param name="premium">The premium rate as a string.</param>
        /// <param name="premiumIsRate">True if the premium is given as a rate. False if given as a percentage.</param>
        /// <returns>The premium as a decimal.</returns>
        public static decimal ConvertPremium(string premium, bool premiumIsRate)
        {
            LosConvert valueConvertor = new LosConvert(FormatTarget.MismoClosing);

            decimal premiumRate = valueConvertor.ToRate(premium);

            if (premiumIsRate)
            {
                premiumRate *= 100;
            }

            return premiumRate;
        }

        /// <summary>
        /// Converts the given dollar amount to a decimal.
        /// </summary>
        /// <param name="amount">The dollar amount as a string.</param>
        /// <returns>The dollar amount as a decimal.</returns>
        public static decimal ConvertDollarAmount(string amount)
        {
            LosConvert valueConvertor = new LosConvert(FormatTarget.MismoClosing);

            return valueConvertor.ToMoney(amount);
        }

        /// <summary>
        /// Masks sensitive data out of the mortgage insurance request prior to logging.
        /// </summary>
        /// <param name="requestXML">The request XML string.</param>
        /// <returns>A string that has been prepped for PB logging.</returns>
        public static string FormatRequestForLogging(string requestXML)
        {
            if (string.IsNullOrEmpty(requestXML))
            {
                return "Request is empty.";
            }

            requestXML = Regex.Replace(requestXML, "Password=\"[^\"]*?\"", "Password=\"*****\"");

            return requestXML;
        }

        /// <summary>
        /// Uses the loanId to lookup the BrokerID of the associated lender.
        /// </summary>
        /// <param name="loanId">The sLId of the loan file.</param> 
        /// <returns>The BrokerID of the given loan file. Guid.Empty if the BrokerID was not found.</returns>
        public static Guid GetBrokerIdByLoanId(Guid loanId)
        {
            try
            {
                Guid brokerId = Guid.Empty;

                DbConnectionInfo.GetConnectionInfoByLoanId(loanId, out brokerId);

                return brokerId;
            }
            catch (LoanNotFoundException)
            {
            }

            return Guid.Empty;
        }

        /// <summary>
        /// Uses the loanId to lookup the application ID [aAppId] of the primary application on the loan file.
        /// </summary>
        /// <param name="brokerId">Broker id of the loan file.</param>
        /// <param name="loanId">The sLId of the loan file.</param> 
        /// <returns>The application ID of the primary app on the given loan file. Guid.Empty if the primary app was not found.</returns>
        public static Guid GetPrimaryAppIdByLoanId(Guid brokerId, Guid loanId)
        {
            SqlParameter[] para = new SqlParameter[] { new SqlParameter("@LoanId", loanId) };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "App_GetPrimaryAppIdFromLoanId", para))
            {
                if (reader.Read())
                {
                    return (Guid)reader["aAppId"];
                }
            }

            return Guid.Empty;
        }

        /// <returns>A list of MI quote and policy orders.</returns>
        /// <summary>
        /// Determines whether the loan file has any existing policy orders with the same MI vendor.
        /// </summary>
        /// <param name="loanID">The sLId of the loan file.</param>
        /// <param name="brokerID">The brokerID of the lender associated with the MI transaction.</param>
        /// <param name="applicationID">The aAppId of the loan application.</param>
        /// <param name="userID">The userID of the user accessing the orders, or null if the transaction is asynchronous.</param>
        /// <param name="vendorID">The ID that LQB has assigned to the MI vendor.</param>
        /// <returns>A value indicating whether the loan file has any existing policy orders with the given vendor.</returns>
        public static bool HasExistingPolicyOrder(Guid loanID, Guid brokerID, Guid applicationID, Guid? userID, Guid vendorID)
        {
            bool hasOrder = false;

            try
            {
                hasOrder = MIOrderInfo.RetrieveOrdersByLoanId(loanID, brokerID, applicationID, userID).Any(order => order.VendorID.Equals(vendorID) && !order.IsQuoteRequest);
            }
            catch (ArgumentNullException)
            {
                hasOrder = false;
            }

            return hasOrder;
        }

        /// <summary>
        /// Convert the given string to a Guid.
        /// </summary>
        /// <param name="guidValue">The string to convert.</param>
        /// <returns>The given string converted to a Guid, or Guid.Empty if the value is not a valid Guid.</returns>
        public static Guid SafeGuid(string guidValue)
        {
            if (string.IsNullOrEmpty(guidValue))
            {
                return Guid.Empty;
            }
            
            try
            {
                return new Guid(guidValue);
            }
            catch (FormatException) 
            { 
            }
            catch (OverflowException) 
            { 
            }

            return Guid.Empty;
        }

        /// <summary>
        /// Parses the given string for date and time.
        /// </summary>
        /// <param name="value">A string representation of a <see cref="DateTime"/>.</param>
        /// <returns>A <see cref="DateTime"/> object parsed from the given string. DateTime.MinValue if the value cannot be parsed or is invalid.</returns>
        public static DateTime SafeDateTime(string value)
        {
            try
            {
                return string.IsNullOrEmpty(value) ? DateTime.MinValue : DateTime.Parse(value);
            }
            catch (FormatException)
            {
                return DateTime.MinValue;
            }
        }

        /// <summary>
        /// Uploads all PDF's contained in the MISMO 2.3.1 formatted response from the MI vendor to EDocs.
        /// </summary>
        /// <param name="responseProvider">An <see cref="MIResponseProvider"/> object with the response from an MI vendor.</param>
        /// <param name="orderInfo">An <see cref="MIOrderInfo"/> object containing the MI request XML, order options, and related information.</param>
        /// <param name="asynchronous">True if the mortgage insurance vendor responded asynchronously. False if the response was synchronous.</param>
        /// <returns>The number of EDocs uploaded.</returns>
        public static int UploadDocuments(MIResponseProvider responseProvider, MIOrderInfo orderInfo, bool asynchronous)
        {
            int docsUploaded = 0;
            bool isQuote = responseProvider.IsQuoteResponse;

            // OPM 252290 - Description should be "{Vendor Name}, {MI Premium Payment Type} - {MI Duration Type}".
            var vendorConfig = MortgageInsuranceVendorConfig.RetrieveById(orderInfo.VendorID);
            string edocDescription = $"{vendorConfig.VendorTypeFriendlyDisplay}, {Tools.Get_sProdConvMIOptionTFriendlyDisplay(orderInfo.PremiumType)}";

            // Get Auto-save doc type ID.
            E_AutoSavePage docTypeToAutoSave = isQuote ? E_AutoSavePage.MortgageInsuranceQuoteDocuments : E_AutoSavePage.MortgageInsurancePolicyDocuments;

            DefaultDocTypeInfo docTypeInfo = AutoSaveDocTypeFactory.GetDocTypeForPageId(docTypeToAutoSave, orderInfo.BrokerID, E_EnforceFolderPermissions.True);
            if (docTypeInfo == null || docTypeInfo.DocType == null)
            {
                throw new CBaseException(ErrorMessages.MIFramework.EDocAutoSaveConfigError, ErrorMessages.MIFramework.EDocAutoSaveConfigErrorDevMessage);
            }

            foreach (string pdf in responseProvider.EncodedPDF)
            {
                if (SaveEDoc(orderInfo, pdf, edocDescription, asynchronous, docTypeToAutoSave))
                {
                    docsUploaded++;
                }
            }

            // OPM 252290 - Clean up MI Quotes from EDocs.
            if (!isQuote && !responseProvider.HasError && !BrokerDB.RetrieveById(orderInfo.BrokerID).DisableEDocsMIQuoteAutoObsoleteAndUnObsolete)
            {
                // Get MI Doc Type ID
                DefaultDocTypeInfo miDocTypeInfo = AutoSaveDocTypeFactory.GetDocTypeForPageId(E_AutoSavePage.MortgageInsuranceQuoteDocuments, orderInfo.BrokerID, E_EnforceFolderPermissions.True);
                if (miDocTypeInfo == null || miDocTypeInfo.DocType == null)
                {
                    throw new CBaseException(ErrorMessages.MIFramework.EDocAutoSaveConfigError, ErrorMessages.MIFramework.EDocAutoSaveConfigErrorDevMessage);
                }

                // OPM 252290 - Manage MI Quote Edocs
                List<Guid> obsoleteableDocIds = new List<Guid>();
                List<Guid> unobsoleteableDocIds = new List<Guid>();

                // Get IDs to obsolete.
                SqlParameter[] obsoleteCheckparameters =
                {
                    new SqlParameter("@sLId", orderInfo.LoanID),
                    new SqlParameter("@BrokerId", orderInfo.BrokerID),
                    new SqlParameter("@MiQuoteDocTypeId", miDocTypeInfo.DocType.DocTypeId)
                };

                using (var connection = DbAccessUtils.GetConnection(orderInfo.BrokerID))
                using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, StoredProcedureName.Create("GetObsoleteableMiQuoteEdocIds").Value, obsoleteCheckparameters, TimeoutInSeconds.Thirty))
                {
                    while (reader.Read())
                    {
                        obsoleteableDocIds.Add((Guid)reader["DocumentId"]);
                    }
                }

                // Get IDs to unobsolete.
                SqlParameter[] unobsoleteCheckparameters =
                {
                    new SqlParameter("@sLId", orderInfo.LoanID),
                    new SqlParameter("@BrokerId", orderInfo.BrokerID),
                    new SqlParameter("@MiQuoteDocTypeId", miDocTypeInfo.DocType.DocTypeId),
                    new SqlParameter("@MiQuoteOrderNumber", orderInfo.OriginalQuoteOrderNumber)
                };

                using (var connection = DbAccessUtils.GetConnection(orderInfo.BrokerID))
                using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, StoredProcedureName.Create("GetUnObsoleteableMiQuoteEdocIdsByQuoteOrderNumber").Value, unobsoleteCheckparameters, TimeoutInSeconds.Thirty))
                {
                    while (reader.Read())
                    {
                        unobsoleteableDocIds.Add((Guid)reader["DocumentId"]);
                    }
                }

                EDocumentRepository repo = EDocumentRepository.GetSystemRepository(orderInfo.BrokerID);

                // Obsolete Docs.
                foreach (Guid docId in obsoleteableDocIds)
                {
                    EDocument doc = repo.GetDocumentById(docId);
                    doc.DocStatus = E_EDocStatus.Obsolete;
                    repo.Save(doc);
                }

                // Unobsolete Docs.
                foreach (Guid docId in unobsoleteableDocIds)
                {
                    EDocument doc = repo.GetDocumentById(docId);
                    doc.DocStatus = E_EDocStatus.Blank;
                    repo.Save(doc);
                }
            }
            
            return docsUploaded;
        }

        /// <summary>
        /// Constructs a URL for launching the quote response viewer. 
        /// </summary>
        /// <param name="loanID">The sLId of the loan file.</param>
        /// <param name="applicationID">The aAppId of the application.</param>
        /// <param name="orderNumber">The order number assigned to the MI order by LQB.</param>
        /// <param name="transactionID">The ID assigned to the MI transaction by LQB.</param>
        /// <returns>A URL for the quote response viewer.</returns>
        public static string QuoteResponseViewerURL(Guid loanID, Guid applicationID, string orderNumber, Guid transactionID)
        {
            return string.Format(
                    @"{0}/newlos/Services/OrderMIQuoteResponseViewer.aspx?loanId={1}&appId={2}&OrderNumber={3}&TransactionId={4}",
                    Tools.VRoot,
                    loanID,
                    applicationID,
                    orderNumber,
                    transactionID);
        }

        /// <summary>
        /// Determines if the delegated button is visible.
        /// </summary>
        /// <param name="principal">Abstract user principal.</param>
        /// <param name="vendorId">Vendor id.</param>
        /// <returns>Whether the delegated button is visible.</returns>
        public static bool CanRequestBeDelegated(AbstractUserPrincipal principal, Guid vendorId)
        {
            var branchSettings = MortgageInsuranceVendorBranchSettings.RetrieveLenderSettings(principal.BrokerId, principal.BranchId, vendorId);
            var brokerSettings = MortgageInsuranceVendorBrokerSettings.RetrieveLenderSettings(principal.BrokerId, vendorId);

            if (branchSettings == null && brokerSettings == null)
            {
                throw new CBaseException(ErrorMessages.MIFramework.NoCredentials(), ErrorMessages.MIFramework.NoCredentials());
            }

            if (branchSettings != null)
            {
                return branchSettings.IsDelegated;
            }
            else
            {
                return brokerSettings.IsDelegated;
            }
        }

        /// <summary>
        /// Saves a PDF as a mortgage insurance EDoc.
        /// </summary>
        /// <param name="orderInfo">An <see cref="MIOrderInfo"/> object containing the MI request XML, order options, and related information.</param>
        /// <param name="encodedPDF">The base64 encoded PDF.</param>
        /// <param name="edocDescription">String description for EDoc created from encoded PDF.</param>
        /// <param name="asynchronous">True if the mortgage insurance vendor responded asynchronously. False if the response was synchronous.</param>
        /// <param name="docTypeToAutoSave">Doc type to auto-save EDoc to.</param>
        /// <returns>True if the PDF was uploaded to EDocs. Otherwise false.</returns>
        private static bool SaveEDoc(MIOrderInfo orderInfo, string encodedPDF, string edocDescription, bool asynchronous, E_AutoSavePage docTypeToAutoSave)
        {
            string decodedFile = string.Empty;
            string fileDbKey = string.Empty;
            E_FileDB fileDB = asynchronous ? E_FileDB.Temp : E_FileDB.Normal;
            const string FileTag = "_MORTGAGE_INSURANCE_DOCUMENT";

            if (!string.IsNullOrEmpty(encodedPDF))
            {
                decodedFile = LendersOffice.Common.Utilities.Base64StringToFile(encodedPDF, FileTag + "_pdf");
                fileDbKey = orderInfo.LoanID.ToString("N") + FileTag;

                FileDBTools.WriteFile(fileDB, fileDbKey, decodedFile);

                Guid docId;
                if (asynchronous)
                {
                    docId = AutoSaveDocTypeFactory.SavePdfDocAsSystem(docTypeToAutoSave, fileDbKey, fileDB, orderInfo.BrokerID, orderInfo.LoanID, orderInfo.ApplicationID, edocDescription);
                }
                else
                {
                    docId = AutoSaveDocTypeFactory.SavePdfDoc(docTypeToAutoSave, fileDbKey, fileDB, orderInfo.BrokerID, orderInfo.LoanID, orderInfo.ApplicationID, orderInfo.UserID, edocDescription);
                }

                // OPM 252290 - Keep track of EDocs created from MI Orders.
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@OrderNumber", orderInfo.OrderNumber));
                parameters.Add(new SqlParameter("@DocumentId", docId));

                using (DbConnection conn = DbConnectionInfo.GetConnection(orderInfo.BrokerID))
                {
                    conn.OpenWithRetry();
                    LendersOffice.Drivers.SqlServerDB.StoredProcedureDriverHelper.ExecuteNonQuery(conn, null, StoredProcedureName.Create("MORTGAGE_INSURANCE_ORDER_INFO_X_EDOCS_DOCUMENT_CREATE").Value, parameters, TimeoutInSeconds.Default);
                }

                return true;
            }

            return false;
        }                
    }
}
