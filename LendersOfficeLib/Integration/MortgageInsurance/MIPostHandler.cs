﻿// <copyright file="MIPostHandler.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   10/27/2014 3:19:45 PM
// </summary>
namespace LendersOffice.Integration.MortgageInsurance
{
    using System;
    using System.IO;
    using System.Web;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using Mismo231.MI.MortgageInsuranceResponse;

    /// <summary>
    /// Handles asynchronous posts from the mortgage insurance vendor to the MI framework.
    /// </summary>
    public class MIPostHandler : IHttpHandler
    {
        /// <summary>
        /// Gets a value indicating whether another request can use the MIPostHandler instance.
        /// </summary>
        /// <value>True, since the handler is reusable.</value>
        public bool IsReusable
        {
            get { return true; }
        }

        /// <summary>
        /// Implements the System.Web.IHttpHandler interface to process HTTP web requests (POSTS) from MI vendors.
        /// </summary>
        /// <param name="context">An HttpContext object that provides references to the intrinsic server objects (e.g. Request, Response, Session, and Server) used to process HTTP requests.</param>
        public void ProcessRequest(HttpContext context)
        {
            string result = string.Empty;
            bool edocUploadFailed = false;
            ResponseGroup vendorResponse = this.DeserializeRequest(context.Request);

            if (vendorResponse == null)
            {
                this.LogVendorResponse(null);
                this.NACK(null, MIAsyncResponseError.DeserializationFailure, context.Response);
                return;
            }

            MIResponseProvider provider = new MIResponseProvider(vendorResponse, MIResponseDataSource.Vendor);
            this.LogVendorResponse(provider);

            if (provider.LoanID == Guid.Empty)
            {
                this.NACK(provider, MIAsyncResponseError.NoLoanID, context.Response);
                return;
            }

            if (provider.TransactionID == Guid.Empty || string.IsNullOrEmpty(provider.OrderNumber))
            {
                this.NACK(provider, MIAsyncResponseError.NoOrder, context.Response);
                return;
            }

            Guid brokerID = MIUtil.GetBrokerIdByLoanId(provider.LoanID);

            if (brokerID == Guid.Empty)
            {
                if (!string.IsNullOrEmpty(ConstStage.AsyncPostbackForwardingHostname))
                {
                    RequestHelper.Forwarding(ConstStage.AsyncPostbackForwardingHostname, context);
                    return;
                }

                this.NACK(provider, MIAsyncResponseError.NoBrokerID, context.Response);
                return;
            }

            Guid appID = MIUtil.GetPrimaryAppIdByLoanId(brokerID, provider.LoanID);

            if (appID == Guid.Empty)
            {
                this.NACK(provider, MIAsyncResponseError.NoAppID, context.Response);
                return;
            }

            MIOrderInfo order = MIOrderInfo.RetrieveTransaction(provider.TransactionID, provider.OrderNumber, provider.LoanID, brokerID, appID);

            if (order == null)
            {
                this.NACK(provider, MIAsyncResponseError.NoOrder, context.Response);
                return;
            }

            order.PopulateResponseData(provider);

            try
            {
                MIServer.ProcessResponse(provider, order, true, out result, out edocUploadFailed);
            }
            catch (CBaseException cbexc)
            {
                Tools.LogErrorWithCriticalTracking(cbexc);
                this.NACK(provider, MIAsyncResponseError.ProcessingError, context.Response);
                return;
            }
            catch (InvalidOperationException ioexc)
            {
                Tools.LogErrorWithCriticalTracking(ioexc);
                this.NACK(provider, MIAsyncResponseError.ProcessingError, context.Response);
                return;
            }

            if (result.Contains(ErrorMessages.MIFramework.UnexpectedResponse, StringComparison.OrdinalIgnoreCase))
            {
                this.ACKNACK(ErrorMessages.MIFramework.DeserializationFailure(), 1, context.Response, provider.LoanID, provider.OrderNumber, provider.TransactionID);
            }
            else
            {
                this.ACKNACK("This is an acknowledgement that the MI response was received and processed.", 0, context.Response, provider.LoanID, provider.OrderNumber, provider.TransactionID);
            }
        }

        /// <summary>
        /// Deserialize the MI response from the HTTP request.
        /// </summary>
        /// <param name="request">The HTTP request (post) from the MI vendor.</param>
        /// <returns>The MI response as a MISMO 2.3.1 ResponseGroup object.</returns>
        private ResponseGroup DeserializeRequest(HttpRequest request)
        {
            try
            {
                // dd 10-21-2018 - Need to make a copy of InputStream. This way XmlDeserialize does not close InputStream.
                // We may need InputStream to be available if we need to forward to another server.
                using (MemoryStream stream = new MemoryStream())
                {
                    request.InputStream.CopyTo(stream);
                    stream.Seek(0, SeekOrigin.Begin);
                    return (ResponseGroup)SerializationHelper.XmlDeserialize(stream, typeof(ResponseGroup));
                }
            }
            catch (InvalidOperationException)
            {
            }

            return null;
        }

        /// <summary>
        /// Log the error to PB and send a negative acknowledgement to the MI vendor.
        /// </summary>
        /// <param name="vendorResponse">The MI response from the vendor.</param>
        /// <param name="errorType">The type of error encountered by the response handler.</param>
        /// <param name="response">The HttpResponse object from the current Http context.</param>
        private void NACK(MIResponseProvider vendorResponse, MIAsyncResponseError errorType, HttpResponse response)
        {
            string error = string.Empty;
            int errorCode = 0;

            // 10/29/2014 BB - If-else conditional is used to avoid cpu/memory hit for exception thrown from default case.
            if (errorType == MIAsyncResponseError.DeserializationFailure)
            {
                error = ErrorMessages.MIFramework.DeserializationFailure();
                errorCode = 1;
            }
            else if (errorType == MIAsyncResponseError.NoAppID || errorType == MIAsyncResponseError.NoLoanID)
            {
                error = ErrorMessages.MIFramework.LoanOrAppRetrievalFailure();
                errorCode = 2;
            }
            else if (errorType == MIAsyncResponseError.NoBrokerID)
            {
                error = ErrorMessages.MIFramework.LenderRetrievalFailure();
                errorCode = 3;
            }
            else if (errorType == MIAsyncResponseError.NoOrder)
            {
                error = ErrorMessages.MIFramework.OrderRetrievalFailure();
                errorCode = 5;
            }
            else if (errorType == MIAsyncResponseError.ProcessingError)
            {
                error = ErrorMessages.MIFramework.GenericProcessingFailure();
                errorCode = 6;
            }

            if (vendorResponse == null)
            {
                this.NACK(error, errorCode, response);
            }
            else 
            {
                this.ACKNACK(error, errorCode, response, vendorResponse.LoanID, vendorResponse.OrderNumber, vendorResponse.TransactionID);
            }
        }

        /// <summary>
        /// Logs the MI response from the vendor to PB.
        /// </summary>
        /// <param name="vendorResponse">The MI response from the vendor.</param>
        private void LogVendorResponse(MIResponseProvider vendorResponse)
        {
            string logTitle = "=== MORTGAGE INSURANCE RESPONSE ===" + Environment.NewLine;
            string responseToLog = vendorResponse == null ? string.Empty : MIResponseProvider.MaskSensitiveResponseData(vendorResponse);

            if (string.IsNullOrEmpty(responseToLog))
            {
                responseToLog = "The vendor response was either empty or the format was unexpected.";
            }

            Tools.LogInfo(logTitle + responseToLog);
        }

        /// <summary>
        /// Sends a negative acknowledgement to the MI vendor.
        /// </summary>
        /// <param name="errorMessage">The error message to include.</param>
        /// <param name="errorCode">The LQB MI error code corresponding to the error message.</param>
        /// <param name="response">The HttpResponse object from the current Http context.</param>
        private void NACK(string errorMessage, int errorCode, HttpResponse response)
        {
            this.ACKNACK(errorMessage, errorCode, response, Guid.Empty, string.Empty, Guid.Empty);
        }

        /// <summary>
        /// Sends a (positive or negative) acknowledgement to the MI vendor.
        /// </summary>
        /// <param name="message">The message to include.</param>
        /// <param name="statusCode">The LQB MI status code corresponding to the message.</param>
        /// <param name="response">The HttpResponse object from the current Http context.</param>
        /// <param name="loanID">The sLId of the loan file associated with the MI response.</param>
        /// <param name="orderNumber">The order number associated with the MI response.</param>
        /// <param name="transactionID">The transaction ID associated with the MI response.</param>
        private void ACKNACK(string message, int statusCode, HttpResponse response, Guid loanID, string orderNumber, Guid transactionID)
        {
            response.ContentType = "text/xml";
            response.ContentEncoding = System.Text.Encoding.UTF8;

            MIACKProvider provider = new MIACKProvider(message, statusCode, loanID, orderNumber, transactionID);
            string ackXML = provider.SerializeACK();

            Tools.LogInfo(string.Format("=== MORTGAGE INSURANCE POST HANDLER {0}==={1}{2}", statusCode > 0 ? "ERROR " : string.Empty, Environment.NewLine, ackXML));
            response.Output.Write(ackXML);

            response.Flush();
            response.End();
        }
    }
}
