﻿// <copyright file="MIRequestProvider.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   10/1/2014 11:27:44 AM
// </summary>
namespace LendersOffice.Integration.MortgageInsurance
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using Migration;
    using Mismo231.MI;
    using Mismo231.MI.MortgageInsuranceRequest;

    /// <summary>
    /// Instantiates a MISMO 2.3.1 mortgage insurance RequestGroup object and maps the required data from the lender account and loan file.
    /// </summary>
    public class MIRequestProvider
    {
        /// <summary>
        /// An instance of the loan data object.
        /// </summary>
        private CPageData dataLoan = null;

        /// <summary>
        /// The sLId of the loan file.
        /// </summary>
        private Guid loanID = Guid.Empty;

        /// <summary>
        /// A container for mortgage insurance order options and relevant loan/account data. 
        /// </summary>
        private MIOrderInfo orderInfo = null;

        /// <summary>
        /// The vendor who will process the mortgage insurance request.
        /// </summary>
        private MortgageInsuranceVendorConfig vendor = null;

        /// <summary>
        /// The lender's vendor configuration with credentials, master policy number, etc.
        /// </summary>
        private MortgageInsuranceVendorBrokerSettings lenderMISettings = null;

        /// <summary>
        /// The loan branch's vendor configuration with branch-level credentials, master policy, etc.
        /// </summary>
        private MortgageInsuranceVendorBranchSettings branchMISettings = null;

        /// <summary>
        /// Indicates whether the vendor config is lender-level or branch-level.
        /// </summary>
        private bool useLenderConfig = true;

        /// <summary>
        /// A <see cref="BrokerDB"/> object with the lender's LQB account information.
        /// </summary>
        private BrokerDB lender = null;

        /// <summary>
        /// A <see cref="BranchDB"/> object with the loan branch's contact information.
        /// </summary>
        private BranchDB branch = null;

        /// <summary>
        /// A <see cref="RequestingParty"/> object to store the contact info for the lender and user. Confines the required EmployeeDB look-up to a single instance.
        /// </summary>
        private RequestingParty requestor = null;

        /// <summary>
        /// The principal of the user initiating the request.
        /// </summary>
        private AbstractUserPrincipal principal;

        /// <summary>
        /// Initializes a new instance of the <see cref="MIRequestProvider"/> class.
        /// </summary>
        /// <param name="loanIdentifier">The sLId of the LQB loan file.</param>
        /// <param name="principal">The principal the user intiating the request.</param>
        public MIRequestProvider(Guid loanIdentifier, AbstractUserPrincipal principal)
        {
            this.loanID = loanIdentifier;
            this.principal = principal;
            this.GetLoanDataWithDependencies();
            this.InitLenderAndBranch();
        }

        /// <summary>
        /// Creates a MISMO 2.3.1 mortgage insurance request.
        /// </summary>
        /// <param name="orderInformation">An <see cref="MIOrderInfo"/> object containing the order options and account and loan information needed to generate a mortgage insurance request.</param>
        /// <param name="lenderSettings">A <see cref="MortgageInsuranceVendorBrokerSettings"/> object with the lender's config for the MI vendor.</param>
        /// <returns>An MIOrderInfo object containing a MISMO 2.3.1 mortgage insurance request.</returns>
        public MIOrderInfo CreateMortgageInsuranceRequest(MIOrderInfo orderInformation, MortgageInsuranceVendorBrokerSettings lenderSettings)
        {
            this.lenderMISettings = lenderSettings;
            this.useLenderConfig = true;

            this.CreateMortgageInsuranceRequestImpl(orderInformation);

            return this.orderInfo;
        }

        /// <summary>
        /// Creates a MISMO 2.3.1 mortgage insurance request.
        /// </summary>
        /// <param name="orderInformation">An <see cref="MIOrderInfo"/> object containing the order options and account and loan information needed to generate a mortgage insurance request.</param>
        /// <param name="branchSettings">A <see cref="MortgageInsuranceVendorBranchSettings"/> object with the loan branch's config for the MI vendor.</param>
        /// <returns>An MIOrderInfo object containing a MISMO 2.3.1 mortgage insurance request.</returns>
        public MIOrderInfo CreateMortgageInsuranceRequest(MIOrderInfo orderInformation, MortgageInsuranceVendorBranchSettings branchSettings)
        {
            this.branchMISettings = branchSettings;
            this.useLenderConfig = false;

            this.CreateMortgageInsuranceRequestImpl(orderInformation);

            return this.orderInfo;
        }

        /// <summary>
        /// Implements the creation of a MISMO 2.3.1 mortgage insurance request.
        /// </summary>
        /// <param name="orderInformation">An <see cref="MIOrderInfo"/> object containing the order options and account and loan information needed to generate a mortgage insurance request.</param>
        private void CreateMortgageInsuranceRequestImpl(MIOrderInfo orderInformation)
        {
            this.orderInfo = orderInformation;
            this.vendor = MortgageInsuranceVendorConfig.RetrieveById(this.orderInfo.VendorID);

            this.orderInfo.MortgageInsuranceRequest = this.CreateRequestGroup();
        }

        /// <summary>
        /// Creates a root-level RequestGroup object for the MISMO 2.3.1 mortgage insurance request.
        /// </summary>
        /// <returns>A RequestGroup object with the populated mortgage insurance request.</returns>
        private RequestGroup CreateRequestGroup()
        {
            RequestGroup requestGroup = new RequestGroup();
            
            ////requestGroup._ID;
            requestGroup.MISMOVersionID = "2.3.1";
            requestGroup.ReceivingParty = this.CreateReceivingParty();
            requestGroup.Request = this.CreateRequest();
            requestGroup.RequestingParty = this.CreateRequestingParty();
            requestGroup.SubmittingParty = this.CreateSubmittingParty();

            return requestGroup;
        }

        /// <summary>
        /// Creates a ReceivingParty object.
        /// </summary>
        /// <returns>A ReceivingParty object with the MI vendor's info.</returns>
        private ReceivingParty CreateReceivingParty()
        {
            ReceivingParty receivingParty = new ReceivingParty();
            
            ////receivingParty._City;
            ////receivingParty._Identifier;
            receivingParty._Name = this.vendor.VendorType.ToString("G");
            ////receivingParty._PostalCode;
            ////receivingParty._State;
            ////receivingParty._StreetAddress;
            ////receivingParty._StreetAddress2;
            ////receivingParty.ContactDetailList;

            return receivingParty;
        }

        /// <summary>
        /// Creates a RequestingParty object.
        /// </summary>
        /// <returns>A RequestingParty object with the lender or branch contact info.</returns>
        private RequestingParty CreateRequestingParty()
        {
            RequestingParty requestingParty = new RequestingParty();

            if (this.requestor == null)
            {
                requestingParty._Identifier = this.lender.CustomerCode;
                CommonLib.Address institutionAddress = this.useLenderConfig ? this.lender.Address : this.branch.Address;

                requestingParty._City = institutionAddress.City;
                requestingParty._Name = this.useLenderConfig ? this.lender.Name : this.branch.DisplayNm;
                requestingParty._PostalCode = institutionAddress.Zipcode;
                requestingParty._State = institutionAddress.State;
                requestingParty._StreetAddress = institutionAddress.StreetAddress;
                ////requestingParty._StreetAddress2;
                requestingParty.ContactDetailList.Add(this.CreateRequestingPartyContactDetail());
                requestingParty.PreferredResponseList.Add(this.CreatePreferredResponseList());

                this.requestor = requestingParty;
            }
            else
            {
                requestingParty = this.requestor;
            }

            return requestingParty;
        }

        /// <summary>
        /// Creates a ContactDetail object with contact points for the user placing the MI order.
        /// </summary>
        /// <returns>A ContactDetail object populated with user's contact points.</returns>
        private ContactDetail CreateRequestingPartyContactDetail()
        {
            if (this.orderInfo.UserID == null)
            {
                return null;
            }

            ContactDetail contact = new ContactDetail();
            EmployeeDB employee = EmployeeDB.RetrieveByUserId(this.orderInfo.BrokerID, this.orderInfo.UserID ?? Guid.Empty);

            contact._Name = employee.FullName;
            this.PopulateContactPoints(contact.ContactPointList, employee.Phone, employee.Fax, employee.Email, employee.CellPhone);

            return contact;
        }

        /// <summary>
        /// Populates the given ContactPoint list with the given contact points.
        /// </summary>
        /// <param name="contactList">The ContactPoint list to which the contact points should be added.</param>
        /// <param name="phone">The contact's phone number, or blank if there is none.</param>
        /// <param name="fax">The contact's fax number, or blank if there is none.</param>
        /// <param name="email">The contact's email address, or blank if there is none.</param>
        /// <param name="mobile">The contact's cell phone number, or blank if there is none.</param>
        private void PopulateContactPoints(List<ContactPoint> contactList, string phone, string fax, string email, string mobile)
        {
            if (!string.IsNullOrEmpty(phone))
            {
                contactList.Add(this.CreateContactPoint(phone, MI_ContactPointRoleTypeEnumerated.Work, MI_ContactPointTypeEnumerated.Phone));
            }

            if (!string.IsNullOrEmpty(fax))
            {
                contactList.Add(this.CreateContactPoint(fax, MI_ContactPointRoleTypeEnumerated.Work, MI_ContactPointTypeEnumerated.Fax));
            }

            if (!string.IsNullOrEmpty(email))
            {
                contactList.Add(this.CreateContactPoint(email, MI_ContactPointRoleTypeEnumerated.Work, MI_ContactPointTypeEnumerated.Email));
            }

            if (!string.IsNullOrEmpty(mobile))
            {
                contactList.Add(this.CreateContactPoint(mobile, MI_ContactPointRoleTypeEnumerated.Mobile, MI_ContactPointTypeEnumerated.Phone));
            }
        }

        /// <summary>
        /// Creates a ContactPoint object.
        /// </summary>
        /// <param name="value">The value of the contact point, e.g. the phone number, fax number, email address.</param>
        /// <param name="location">The location of the contact point, e.g. home, work, mobile.</param>
        /// <param name="type">The type of contact point, e.g. phone, fax, email.</param>
        /// <returns>A ContactPoint with the given contact information.</returns>
        private ContactPoint CreateContactPoint(string value, MI_ContactPointRoleTypeEnumerated location, MI_ContactPointTypeEnumerated type)
        {
            ContactPoint contact = new ContactPoint();

            ////contact._TypeOtherDescription;
            contact._Value = value;
            ////contact.preferenceIndicator;
            contact.roleType = location;
            contact.type = type;

            return contact;
        }

        /// <summary>
        /// Creates a ContactPoint object. For use with contact types that fall outside of the MISMO enumerated types.
        /// </summary>
        /// <param name="value">The value of the contact point, e.g. the phone number, fax number, email address.</param>
        /// <param name="location">The location of the contact point, e.g. home, work, mobile.</param>
        /// <param name="otherType">A description of the contact type.</param>
        /// <returns>A ContactPoint with the given contact information.</returns>
        private ContactPoint CreateContactPoint(string value, MI_ContactPointRoleTypeEnumerated location, string otherType)
        {
            ContactPoint contact = new ContactPoint();

            contact._TypeOtherDescription = otherType;
            contact._Value = value;
            ////contact.preferenceIndicator;
            contact.roleType = location;
            contact.type = MI_ContactPointTypeEnumerated.Other;

            return contact;
        }

        /// <summary>
        /// Creates a SubmittingParty object.
        /// </summary>
        /// <returns>A SubmittingParty object identifying LendingQB as the LOS provider.</returns>
        private SubmittingParty CreateSubmittingParty()
        {
            SubmittingParty submittingParty = new SubmittingParty();
            CommonLib.Address lqbAddress = ConstApp.GetLendingQBAddress();

            submittingParty._City = lqbAddress.City;
            ////submittingParty._Identifier;
            submittingParty._Name = "LendingQB";
            submittingParty._PostalCode = lqbAddress.Zipcode;
            submittingParty._State = lqbAddress.State;
            submittingParty._StreetAddress = lqbAddress.StreetAddress;
            ////submittingParty._StreetAddress2;
            submittingParty.ContactDetailList.Add(this.CreateLQBContactDetail());
            ////submittingParty.LoginAccountIdentifier;
            ////submittingParty.LoginAccountPassword;
            submittingParty.PreferredResponseList.Add(this.CreatePreferredResponseList());
            ////submittingParty.SequenceIdentifier;

            return submittingParty;
        }

        /// <summary>
        /// Creates a ContactDetail object with contact points for LendingQB.
        /// </summary>
        /// <returns>A ContactDetail object populated with LendingQB contact points.</returns>
        private ContactDetail CreateLQBContactDetail()
        {
            ContactDetail contact = new ContactDetail();
            
            contact._Name = "LendingQB Support";
            this.PopulateContactPoints(contact.ContactPointList, ConstApp.lqbPhoneNumber, string.Empty, ConstStage.LQBIntegrationEmail, string.Empty);

            return contact;
        }

        /// <summary>
        /// Creates a PreferredResponse object.
        /// </summary>
        /// <returns>A PreferredResponse object specifying the response that LQB expects from the MI vendor.</returns>
        private PreferredResponse CreatePreferredResponseList()
        {
            PreferredResponse preferredResponse = new PreferredResponse();

            preferredResponse._Destination = Constants.ConstStage.MIFrameworkPostBackUrl;
            ////preferredResponse._FormatOtherDescription;
            ////preferredResponse._MethodOther;
            preferredResponse._VersionIdentifier = "MISMO 2.3.1";
            preferredResponse.format = MI_PreferredResponseFormatEnumerated.PDF;
            preferredResponse.method = MI_PreferredResponseMethodEnumerated.HTTPS;
            ////preferredResponse.MIMEType;
            preferredResponse.useEmbeddedFileIndicator = E_YNIndicator.Y;

            return preferredResponse;
        }

        /// <summary>
        /// Creates a Request object.
        /// </summary>
        /// <returns>A Request object with the MI credentials and the bulk of the request data.</returns>
        private Request CreateRequest()
        {
            Request request = new Request();

            ////request._ID;
            request.RequestData = this.CreateRequestData();
            request.RequestDatetime = LendersOffice.CreditReport.MismoUtilities.ToDateTimeString(this.orderInfo.OrderedDate);
            request.RequestingPartyBranchIdentifier = this.branch.BranchCode;

            if (this.useLenderConfig)
            {
                request.InternalAccountIdentifier = this.vendor.IsUseAccountId ? this.lenderMISettings.AccountId : string.Empty;
                request.LoginAccountIdentifier = this.lenderMISettings.UserName;
                request.LoginAccountPassword = this.lenderMISettings.Password;
            }
            else
            {
                request.InternalAccountIdentifier = this.vendor.IsUseAccountId ? this.branchMISettings.AccountId : string.Empty;
                request.LoginAccountIdentifier = this.branchMISettings.UserName;
                request.LoginAccountPassword = this.branchMISettings.Password;
            }

            if (!this.orderInfo.IsQuoteRequest && !this.orderInfo.QuoteNumber.Equals(string.Empty))
            {
                request.KeyList.Add(this.CreateKey(MIKey.QuoteNumber, this.orderInfo.QuoteNumber));
            }

            if (!string.IsNullOrEmpty(this.dataLoan.sSpAppraisalId))
            {
                request.KeyList.Add(this.CreateKey(MIKey.AppraisalIdentifier, this.dataLoan.sSpAppraisalId));
            }

            if (!string.IsNullOrEmpty(this.dataLoan.sSpCuScore_rep))
            {
                request.KeyList.Add(this.CreateKey(MIKey.CURiskScore, this.dataLoan.sSpCuScore_rep));
            }

            request.KeyList.Add(this.CreateKey(MIKey.OrderNumber, this.orderInfo.OrderNumber));
            request.KeyList.Add(this.CreateKey(MIKey.LendingQBLoanID, this.loanID.ToString("D")));

            return request;
        }

        /// <summary>
        /// Creates a Key object with the given name and value.
        /// </summary>
        /// <param name="name">The name of the key.</param>
        /// <param name="value">The value of the key.</param>
        /// <returns>A Key object with the given name and value.</returns>
        private Key CreateKey(MIKey name, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            Key key = new Key();

            key._Name = LendersOffice.Common.Utilities.ConvertEnumToString(name);
            key._Value = value;

            return key;
        }

        /// <summary>
        /// Creates a Key object with the given name and value.
        /// </summary>
        /// <param name="name">The name of the key.</param>
        /// <param name="value">The value of the key as an enumerated type.</param>
        /// <returns>A Key object with the given name and value.</returns>
        private Key CreateKey(MIKey name, Enum value)
        {
            return this.CreateKey(name, LendersOffice.Common.Utilities.ConvertEnumToString(value));
        }

        /// <summary>
        /// Creates a RequestData object.
        /// </summary>
        /// <returns>A RequestData object with an MIApplication.</returns>
        private RequestData CreateRequestData()
        {
            RequestData requestData = new RequestData();

            requestData.MIApplication = this.CreateMIApplication();

            return requestData;
        }

        /// <summary>
        /// Creates an MIApplication object.
        /// </summary>
        /// <returns>An MIApplication object with MI request and loan data.</returns>
        private MIApplication CreateMIApplication()
        {
            MIApplication application = new MIApplication();

            int apps = this.dataLoan.nApps;
            for (int applicationIndex = 0; applicationIndex < apps; applicationIndex++)
            {
                CAppData dataApp = this.dataLoan.GetAppData(applicationIndex);
                string sequenceIdentifier = string.Empty;

                if (dataApp.aBIsValidNameSsn)
                {
                    dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
                    application.CreditScoreList.Add(this.CreateCreditScore(dataApp.aMismoId, MI_CreditRepositorySourceTypeEnumerated.Equifax, dataApp.aEquifaxScore_rep, dataApp.aEquifaxCreatedD_rep, dataApp.aEquifaxFactors, dataApp.aEquifaxModelName, dataApp.aEquifaxModelT, dataApp.aEquifaxModelTOtherDescription));
                    application.CreditScoreList.Add(this.CreateCreditScore(dataApp.aMismoId, MI_CreditRepositorySourceTypeEnumerated.Experian, dataApp.aExperianScore_rep, dataApp.aExperianCreatedD_rep, dataApp.aExperianFactors, dataApp.aExperianModelName, dataApp.aExperianModelT, dataApp.aExperianModelTOtherDescription));
                    application.CreditScoreList.Add(this.CreateCreditScore(dataApp.aMismoId, MI_CreditRepositorySourceTypeEnumerated.TransUnion, dataApp.aTransUnionScore_rep, dataApp.aTransUnionCreatedD_rep, dataApp.aTransUnionFactors, dataApp.aTransUnionModelName, dataApp.aTransUnionModelT, dataApp.aTransUnionModelTOtherDescription));
                }

                if (dataApp.aCIsValidNameSsn)
                {
                    dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
                    application.CreditScoreList.Add(this.CreateCreditScore(dataApp.aMismoId, MI_CreditRepositorySourceTypeEnumerated.Equifax, dataApp.aEquifaxScore_rep, dataApp.aEquifaxCreatedD_rep, dataApp.aEquifaxFactors, dataApp.aEquifaxModelName, dataApp.aEquifaxModelT, dataApp.aEquifaxModelTOtherDescription));
                    application.CreditScoreList.Add(this.CreateCreditScore(dataApp.aMismoId, MI_CreditRepositorySourceTypeEnumerated.Experian, dataApp.aExperianScore_rep, dataApp.aExperianCreatedD_rep, dataApp.aExperianFactors, dataApp.aExperianModelName, dataApp.aExperianModelT, dataApp.aExperianModelTOtherDescription));
                    application.CreditScoreList.Add(this.CreateCreditScore(dataApp.aMismoId, MI_CreditRepositorySourceTypeEnumerated.TransUnion, dataApp.aTransUnionScore_rep, dataApp.aTransUnionCreatedD_rep, dataApp.aTransUnionFactors, dataApp.aTransUnionModelName, dataApp.aTransUnionModelT, dataApp.aTransUnionModelTOtherDescription));
                }
            }

            if (this.orderInfo.PremiumType == E_sProdConvMIOptionT.BorrPaidSplitPrem)
            {
                application.KeyList.Add(this.CreateKey(MIKey.SplitPremium, this.orderInfo.SplitPremiumMismoValue));
            }

            if (this.dataLoan.sFinMethT == E_sFinMethT.ARM && !string.IsNullOrEmpty(this.dataLoan.sArmIndexNameVstr))
            {
                application.KeyList.Add(this.CreateKey(MIKey.IndexTypeOtherDescription, this.dataLoan.sArmIndexNameVstr));
            }

            application.KeyList.Add(this.CreateKey(MIKey.DTI, this.dataLoan.sQualBottomR_rep));
            application.KeyList.Add(this.CreateKey(MIKey.LoanOriginatorType, MIMismoConvertor.ToMismo(this.dataLoan.sBranchChannelT)));

            if (this.vendor.SendAuthTicketWithOrders)
            {
                XElement genericFrameworkUserTicket = AuthServiceHelper.GetGenericFrameworkUserAuthTicket(this.principal, this.dataLoan.sLRefNm, null);
                System.Xml.Linq.XAttribute authXML = genericFrameworkUserTicket.Attribute("EncryptedTicket");

                if (authXML != null)
                {
                    application.KeyList.Add(this.CreateKey(MIKey.GenericFrameworkUserTicketName, AuthServiceHelper.GenericFrameworkTicketName));
                    application.KeyList.Add(this.CreateKey(MIKey.GenericFrameworkUserTicketEncryptedTicket, authXML.Value));
                    application.KeyList.Add(this.CreateKey(MIKey.GenericFrameworkUserTicketWebServiceDomain, ConstStage.GenericFrameworkWebServiceDomain));
                    application.KeyList.Add(this.CreateKey(MIKey.GenericFrameworkUserTicketSiteCode, genericFrameworkUserTicket.Attribute("Site").Value));
                }
            }

            if (this.dataLoan.sSpGseCollateralProgramT == E_sSpGseCollateralProgramT.PropertyInspectionWaiver)
            {
                application.KeyList.Add(this.CreateKey(MIKey.PropertyInspectionWaiver, "Y"));
            }

            application.LoanPlaceholder = new LoanPlaceholder();
            application.MIRequest = this.CreateMIRequest();
            application.MISMOVersionID = "2.3.1";
            application.RequestingParty = this.CreateRequestingParty();
            application.SubmittingParty = this.CreateSubmittingParty();

            return application;
        }

        /// <summary>
        /// Creates a CreditScore object.
        /// </summary>
        /// <param name="borrowerId">The MISMO formatted borrower ID.</param>
        /// <param name="repository">The credit bureau that provided the credit file for the credit report.</param>
        /// <param name="score">The borrower's credit score.</param>
        /// <param name="creditDate">The date that the credit report was generated.</param>
        /// <param name="factors">The credit factors reported by the bureau.</param>
        /// <param name="modelName">The model name reported by the bureau.</param>
        /// <param name="modelType">The credit model type (to be used after loan version 26).</param>
        /// <param name="modelDescription">The other description of the credit model type (to be used after loan version 26).</param>
        /// <returns>A CreditScore object with data related to the borrower's credit score.</returns>
        private CreditScore CreateCreditScore(string borrowerId, MI_CreditRepositorySourceTypeEnumerated repository, string score, string creditDate, string factors, string modelName, E_CreditScoreModelT modelType, string modelDescription)
        {
            if (string.IsNullOrEmpty(score))
            {
                return null;
            }

            CreditScore creditScore = new CreditScore();

            creditScore._Date = creditDate;
            creditScore._Value = score;
            creditScore.BorrowerID = borrowerId;
            ////creditScore.CreditFileID;
            ////creditScore.CreditReportIdentifier;
            ////creditScore.creditReportType;
            ////creditScore.CreditReportTypeOtherDescription;
            creditScore.creditRepositorySourceType = repository;
            creditScore.CreditScoreID = borrowerId + "_" + repository.ToString();
            ////creditScore.exclusionReasonType;
            creditScore.MISMOVersionID = "2.3";

            var mismoCreditModel = MI_CreditScoreModelNameTypeEnumerated.None;
            var otherModelName = string.Empty;

            if (LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.dataLoan.sLoanVersionT, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums))
            {
                mismoCreditModel = MIMismoConvertor.ToMismoModelType(modelType);

                if (mismoCreditModel == MI_CreditScoreModelNameTypeEnumerated.Other)
                {
                    // If the LendingQB model is not Other but the resultant MISMO type is, there's no valid
                    // mapping and we'll just use the string description of the LendingQB type instead.
                    if (modelType == E_CreditScoreModelT.Other && !string.IsNullOrEmpty(modelDescription))
                    {
                        otherModelName = modelDescription;
                    }
                    else
                    {
                        otherModelName = EnumUtilities.GetDescription(modelType);
                    }
                }
            }
            else
            {
                mismoCreditModel = MIMismoConvertor.ToMismoModelName(modelName, out otherModelName);
            }

            if (repository == MI_CreditRepositorySourceTypeEnumerated.Equifax ||
                repository == MI_CreditRepositorySourceTypeEnumerated.Experian ||
                repository == MI_CreditRepositorySourceTypeEnumerated.TransUnion)
            {
                creditScore.modelNameType = mismoCreditModel;

                if (mismoCreditModel == MI_CreditScoreModelNameTypeEnumerated.Other)
                {
                    creditScore._ModelNameTypeOtherDescription = otherModelName;
                }
            }

            if (!string.IsNullOrEmpty(factors))
            {
                string[] parts = factors.Split('\n');
                foreach (string factor in parts)
                {
                    creditScore.FactorList.Add(this.CreateFactor(factor));
                }
            }

            return creditScore;
        }

        /// <summary>
        /// Creates a Factor object.
        /// </summary>
        /// <param name="factor">The credit factor as a string.</param>
        /// <returns>A credit Factor object with the given factor.</returns>
        private Factor CreateFactor(string factor)
        {
            if (string.IsNullOrEmpty(factor))
            {
                return null;
            }

            factor = factor.TrimEnd('\r', '\n');

            Factor creditFactor = new Factor();

            ////creditFactor._Code;
            creditFactor._Text = factor;

            return creditFactor;
        }

        /// <summary>
        /// Creates an MIRequest object.
        /// </summary>
        /// <returns>An MIRequest object with data for the request.</returns>
        private MIRequest CreateMIRequest()
        {
            MIRequest request = new MIRequest();

            request.AutomatedUnderwritingSystemName = MIMismoConvertor.ToMismoAUSName(this.dataLoan.sIsDuUw, this.dataLoan.sIsLpUw, this.dataLoan.sIsOtherUw, this.dataLoan.sOtherUwDesc);
            request.AutomatedUnderwritingSystemResultValue = MIMismoConvertor.ToMismoAUSResult(this.dataLoan.sFHARatedAcceptedByTotalScorecard, this.dataLoan.sFHARatedReferByTotalScorecard, this.dataLoan.sProd3rdPartyUwResultT);
            request.DesktopUnderwriterCaseFileIdentifier = this.dataLoan.sDuCaseId;
            request.DesktopUnderwriterRecommendationType = MIMismoConvertor.ToMismoDURecommendationType(this.dataLoan.sProd3rdPartyUwResultT);
            request.FreddieMacPurchaseEligibilityType = MIMismoConvertor.ToMismoFreddiePurchaseEligibilityType(this.dataLoan.sProd3rdPartyUwResultT);
            ////request.InvestorProgramNameType;
            ////request.InvestorProgramNameTypeOtherDescription;
            request.LoanProspectorAcceptPlusEligibleIndicator = this.dataLoan.sIsLpUw ? MIMismoConvertor.ToMismo(this.dataLoan.sLpDocClass.ToLower().Contains("accept plus")) : E_YNIndicator.None;
            request.LoanProspectorCreditRiskClassificationDescription = MIMismoConvertor.ToMismoLoanProspectorCreditRiskClassDescription(this.dataLoan.sProd3rdPartyUwResultT);
            request.LoanProspectorCreditRiskClassificationType = MIMismoConvertor.ToMismoLoanProspectorCreditRiskClassType(this.dataLoan.sProd3rdPartyUwResultT);
            request.LoanProspectorDocumentationClassificationDescription = this.dataLoan.sIsLpUw ? this.dataLoan.sLpDocClass : string.Empty;
            request.LoanProspectorDocumentationClassificationType = MIMismoConvertor.ToMismoLoanProspectorDocClassType(this.dataLoan.sLpDocClass);
            request.LoanProspectorKeyIdentifier = this.dataLoan.sLpAusKey;
            ////request.LoanProspectorRiskGradeAssignedType;
            request.MIApplicationType = MIMismoConvertor.ToMismoMIApplicationType(this.orderInfo.IsQuoteRequest, this.orderInfo.DelegationType);
            request.MICaptiveReinsuranceIndicator = E_YNIndicator.N;
            request.MICertificateIdentifier = this.dataLoan.sMiCertId;
            request.MICertificateType = MI_MICertificateTypeEnumerated.Primary;
            request.MICompanyName = this.vendor.VendorType == E_sMiCompanyNmT.Arch ? "Arch_MICompany" : this.vendor.VendorTypeFriendlyDisplay;
            request.MICoveragePlanType = MI_MICoveragePlanTypeEnumerated.StandardPrimary;
            request.MIDurationType = MIMismoConvertor.ToMismoMIDurationType(this.orderInfo.PremiumType);
            request.MIEmployeeLoanIndicator = MIMismoConvertor.ToMismo(this.dataLoan.sIsEmployeeLoan);
            request.MIInitialPremiumAtClosingType = this.orderInfo.PremiumAtClosing;
            request.MILenderIdentifier = this.orderInfo.MasterPolicyNumber;
            ////request.MILenderSpecialProgramType;
            request.MILoanLevelCreditScoreValue = this.dataLoan.sCreditScoreLpeQual_rep;
            request.MIPremiumFinancedIndicator = MIMismoConvertor.ToMismo(this.orderInfo.UfmipFinanced);
            request.MIPremiumPaymentType = MIMismoConvertor.ToMismoMIPremiumPaymentType(this.orderInfo.PremiumType);
            request.MIPremiumRatePlanType = MI_MIPremiumRatePlanTypeEnumerated.Level;
            request.MIPremiumRefundableType = this.orderInfo.Refundability;
            ////request.MIPremiumTermMonths;
            request.MIReducedLoanDocumentationType = MIMismoConvertor.ToMismo(this.dataLoan.sProdDocT);
            request.MIRelocationLoanIndicator = MIMismoConvertor.ToMismo(this.orderInfo.RelocationLoan);
            request.MIRenewalCalculationType = this.orderInfo.RenewalType;

            bool hasExistingPolicyOrder = MIUtil.HasExistingPolicyOrder(this.loanID, this.lender.BrokerID, this.orderInfo.ApplicationID, this.orderInfo.UserID, this.vendor.VendorId);
            request.MIRequestType = hasExistingPolicyOrder ? MI_MIRequestTypeEnumerated.Resubmission : MI_MIRequestTypeEnumerated.OriginalRequest;
            
            ////request.MISpecialPricingType;
            ////request.MISubPrimeProgramType;
            request.MITransactionIdentifier = this.orderInfo.TransactionID.ToString("D");

            if (this.dataLoan.sGfeIsTPOTransaction)
            {
                request.ThirdPartyOriginator = this.CreateThirdPartyOriginator();
            }

            return request;
        }

        /// <summary>
        /// Creates a ThirdPartyOriginator object.
        /// </summary>
        /// <returns>A ThirdPartyOriginator object with contact data for the broker that originated the loan file.</returns>
        private ThirdPartyOriginator CreateThirdPartyOriginator()
        {
            CAgentFields mortgageBroker = this.dataLoan.GetAgentOfRole(E_AgentRoleT.Broker, E_ReturnOptionIfNotExist.CreateNewDoNotFallBack);

            //// If there is no existing broker record but the loan is marked as a TPO transaction then look for a loan officer agent record.
            if (mortgageBroker.IsNewRecord && this.dataLoan.sGfeIsTPOTransaction)
            {
                mortgageBroker = this.dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            }

            if (!mortgageBroker.IsValid || mortgageBroker.IsNewRecord)
            {
                return null;
            }

            ThirdPartyOriginator originator = new ThirdPartyOriginator();

            originator._City = mortgageBroker.City;
            originator._Name = mortgageBroker.CompanyName;
            originator._PostalCode = mortgageBroker.Zip;
            originator._State = mortgageBroker.State;
            originator._StreetAddress = mortgageBroker.StreetAddr;
            ////originator._StreetAddress2;
            originator.ContactDetailList.Add(this.CreateTPOCompanyContactDetail(mortgageBroker));
            originator.ContactDetailList.Add(this.CreateTPOContactDetail(mortgageBroker));
            ////originator.InternalAccountIdentifier;

            return originator;
        }

        /// <summary>
        /// Creates a ContactDetail object with contact points for the Third Party Originator (company, i.e. broker) on file.
        /// </summary>
        /// <param name="originatingCompany">The TPO company on file.</param>
        /// <returns>A ContactDetail object populated with the contact points for the given company.</returns>
        private ContactDetail CreateTPOCompanyContactDetail(CAgentFields originatingCompany)
        {
            ContactDetail contact = new ContactDetail();

            contact._Name = originatingCompany.CompanyName;
            this.PopulateContactPoints(contact.ContactPointList, originatingCompany.PhoneOfCompany, originatingCompany.FaxOfCompany, string.Empty, string.Empty);

            return contact;
        }

        /// <summary>
        /// Creates a ContactDetail object with contact points for the Third Party Originator on file.
        /// </summary>
        /// <param name="originator">The Third Party Originator on file.</param>
        /// <returns>A ContactDetail object populated with the contact points for the given TPO.</returns>
        private ContactDetail CreateTPOContactDetail(CAgentFields originator)
        {
            ContactDetail contact = new ContactDetail();

            contact._Name = originator.AgentName;
            this.PopulateContactPoints(contact.ContactPointList, originator.Phone, originator.FaxNum, originator.EmailAddr, originator.CellPhone);
            contact.ContactPointList.Add(this.CreateContactPoint(originator.LoanOriginatorIdentifier, MI_ContactPointRoleTypeEnumerated.Work, "LoanOriginatorIdentifier"));

            return contact;
        }

        /// <summary>
        /// Initializes a CPageData object with the necessary dependencies to load all of the fields for the MI request.
        /// </summary>
        private void GetLoanDataWithDependencies()
        {
            this.dataLoan = new CFullAccessPageData(this.loanID, CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(MIRequestProvider)));
            this.dataLoan.ByPassFieldSecurityCheck = true;
            this.dataLoan.InitLoad();
            this.dataLoan.SetFormatTarget(FormatTarget.MismoClosing);
        }

        /// <summary>
        /// Initializes the BrokerDB and BranchDB objects required by the MI request.
        /// </summary>
        private void InitLenderAndBranch()
        {
            this.lender = BrokerDB.RetrieveById(this.dataLoan.sBrokerId);
            this.branch = new BranchDB(this.dataLoan.sBranchId, this.dataLoan.sBrokerId);
            this.branch.Retrieve();
        }
    }
}
