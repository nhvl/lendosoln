﻿namespace LendersOffice.Integration.MortgageInsurance
{
    using System.ComponentModel;

    /// <summary>
    /// Mortgage Insurance Delegation Type.
    /// </summary>
    public enum DelegationType
    {
        /// <summary>
        /// Blank option.
        /// </summary>
        [Description("")]
        Blank = 0,

        /// <summary>
        /// Delegated option.
        /// </summary>
        [Description("Delegated")]
        Delegated = 1,

        /// <summary>
        /// Non-Delegated option.
        /// </summary>
        [Description("Non-Delegated")]
        NonDelegated = 2
    }
}
