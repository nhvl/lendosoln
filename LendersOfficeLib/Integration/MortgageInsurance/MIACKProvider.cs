﻿// <copyright file="MIACKProvider.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Brian Beery
//  Date:   10/30/2014 10:29:36 AM
// </summary>
namespace LendersOffice.Integration.MortgageInsurance
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.Constants;
    using Mismo231.MI;
    using Mismo231.MI.MortgageInsuranceACK;

    /// <summary>
    /// Instantiates a MISMO 2.3.1 mortgage insurance ResponseGroup object and maps the required data for a positive or negative acknowledgement. Used as a response to an asynchronous post from the MI vendor.
    /// </summary>
    public class MIACKProvider
    {
        /// <summary>
        /// The message or status description to be included in the acknowledgement to the MI vendor.
        /// </summary>
        private string statusDescription = string.Empty;

        /// <summary>
        /// The code corresponding to the status. 
        /// </summary>
        private int statusCode = 0;

        /// <summary>
        /// The sLId of the loan file.
        /// </summary>
        private Guid loanID = Guid.Empty;
        
        /// <summary>
        /// The order number associated with the MI order.
        /// </summary>
        private string orderNum = string.Empty;

        /// <summary>
        /// The transaction ID associated with the MI order.
        /// </summary>
        private Guid transactionID = Guid.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="MIACKProvider"/> class.
        /// </summary>
        /// <param name="message">The message to include.</param>
        /// <param name="code">The LQB MI status code corresponding to the message.</param>
        /// <param name="loanId">The sLId of the loan file associated with the MI response.</param>
        /// <param name="orderNumber">The order number associated with the MI response.</param>
        /// <param name="transactionId">The transaction ID associated with the MI response.</param>
        /// <returns>An MI ResponseGroup object with information required for an MI acknowledgement.</returns>
        public MIACKProvider(string message, int code, Guid loanId, string orderNumber, Guid transactionId)
        {
            this.statusDescription = message;
            this.statusCode = code;
            this.loanID = loanId;
            this.orderNum = orderNumber;
            this.transactionID = transactionId;
        }

        /// <summary>
        /// Instantiates and serializes the MI acknowledgement.
        /// </summary>
        /// <returns>An MI ACK/NACK as an XML string.</returns>
        public string SerializeACK()
        {
            return LendersOffice.Common.SerializationHelper.XmlSerializeStripDefaultNamespace(this.CreateACK(), true);
        }

        /// <summary>
        /// Creates a MISMO 2.3.1 MI ResponseGroup object for a (positive / negative) acknowledgement from LQB to the MI provider.
        /// </summary>
        /// <returns>An MI ResponseGroup object with information required for an MI acknowledgement.</returns>
        private ResponseGroup CreateACK()
        {
            ResponseGroup ack = new ResponseGroup();

            ////ack._ID;
            ack.MISMOVersionID = "2.3.1";
            ack.RespondingParty = this.CreateRespondingParty();
            ////ack.RespondToParty;
            ack.Response = this.CreateResponse();

            return ack;
        }

        /// <summary>
        /// Creates a Responding Party object with contact information for LQB.
        /// </summary>
        /// <returns>A Responding Party object with contact information for LQB.</returns>
        private RespondingParty CreateRespondingParty()
        {
            RespondingParty party = new RespondingParty();
            CommonLib.Address lqbAddress = ConstApp.GetLendingQBAddress();

            party._City = lqbAddress.City;
            ////party._Identifier;
            party._Name = "LendingQB";
            party._PostalCode = lqbAddress.Zipcode;
            party._State = lqbAddress.State;
            party._StreetAddress = lqbAddress.StreetAddress;
            ////party._StreetAddress2;
            party.ContactDetailList.Add(this.CreateLQBContactDetail());

            return party;
        }

        /// <summary>
        /// Creates a ContactDetail object with contact points for LendingQB.
        /// </summary>
        /// <returns>A ContactDetail object populated with LendingQB contact points.</returns>
        private ContactDetail CreateLQBContactDetail()
        {
            ContactDetail contact = new ContactDetail();

            contact._Name = "LendingQB Support";
            this.PopulateContactPoints(contact.ContactPointList, ConstApp.lqbPhoneNumber, string.Empty, ConstStage.LQBIntegrationEmail, string.Empty);

            return contact;
        }

        /// <summary>
        /// Populates the given ContactPoint list with the given contact points.
        /// </summary>
        /// <param name="contactList">The ContactPoint list to which the contact points should be added.</param>
        /// <param name="phone">The contact's phone number, or blank if there is none.</param>
        /// <param name="fax">The contact's fax number, or blank if there is none.</param>
        /// <param name="email">The contact's email address, or blank if there is none.</param>
        /// <param name="mobile">The contact's cell phone number, or blank if there is none.</param>
        private void PopulateContactPoints(List<ContactPoint> contactList, string phone, string fax, string email, string mobile)
        {
            if (!string.IsNullOrEmpty(phone))
            {
                contactList.Add(this.CreateContactPoint(phone, MI_ContactPointRoleTypeEnumerated.Work, MI_ContactPointTypeEnumerated.Phone));
            }

            if (!string.IsNullOrEmpty(fax))
            {
                contactList.Add(this.CreateContactPoint(fax, MI_ContactPointRoleTypeEnumerated.Work, MI_ContactPointTypeEnumerated.Fax));
            }

            if (!string.IsNullOrEmpty(email))
            {
                contactList.Add(this.CreateContactPoint(email, MI_ContactPointRoleTypeEnumerated.Work, MI_ContactPointTypeEnumerated.Email));
            }

            if (!string.IsNullOrEmpty(mobile))
            {
                contactList.Add(this.CreateContactPoint(mobile, MI_ContactPointRoleTypeEnumerated.Mobile, MI_ContactPointTypeEnumerated.Phone));
            }
        }

        /// <summary>
        /// Creates a ContactPoint object.
        /// </summary>
        /// <param name="value">The value of the contact point, e.g. the phone number, fax number, email address.</param>
        /// <param name="location">The location of the contact point, e.g. home, work, mobile.</param>
        /// <param name="type">The type of contact point, e.g. phone, fax, email.</param>
        /// <returns>A ContactPoint with the given contact information.</returns>
        private ContactPoint CreateContactPoint(string value, MI_ContactPointRoleTypeEnumerated location, MI_ContactPointTypeEnumerated type)
        {
            ContactPoint contact = new ContactPoint();

            ////contact._TypeOtherDescription;
            contact._Value = value;
            ////contact.preferenceIndicator;
            contact.roleType = location;
            contact.type = type;

            return contact;
        }

        /// <summary>
        /// Creates a Response object for the acknowledgement. 
        /// </summary>
        /// <returns>A Response object.</returns>
        private Response CreateResponse()
        {
            Response response = new Response();

            ////response._ID;
            ////response.InternalAccountIdentifier;

            if (!string.IsNullOrEmpty(this.orderNum))
            {
                response.KeyList.Add(this.CreateKey(MIKey.OrderNumber, this.orderNum));
            }

            if (this.loanID != Guid.Empty)
            {
                response.KeyList.Add(this.CreateKey(MIKey.LendingQBLoanID, this.loanID.ToString("D")));
            }

            ////response.LoginAccountIdentifier;
            ////response.LoginAccountPassword;
            response.ResponseData = this.CreateResponseData();
            response.ResponseDateTime = LendersOffice.CreditReport.MismoUtilities.ToDateTimeString(DateTime.Now);
            response.StatusList.Add(this.CreateStatus());

            return response;
        }

        /// <summary>
        /// Creates a Status object with the ACK / NACK status information.
        /// </summary>
        /// <returns>A Status object with the ACK / NACK status information.</returns>
        private Status CreateStatus()
        {
            Status status = new Status();

            status._Code = this.statusCode.ToString();
            status._Condition = this.statusCode == 0 ? "PROCESSED" : "ERROR";
            status._Description = this.statusDescription;
            ////status._Name;

            return status;
        }

        /// <summary>
        /// Creates a Key object with the given name and value.
        /// </summary>
        /// <param name="name">The name of the key.</param>
        /// <param name="value">The value of the key.</param>
        /// <returns>A Key object with the given name and value.</returns>
        private Key CreateKey(MIKey name, string value)
        {
            Key key = new Key();

            key._Name = LendersOffice.Common.Utilities.ConvertEnumToString(name);
            key._Value = value;

            return key;
        }

        /// <summary>
        /// Creates a ResponseData object with the MIResponse.
        /// </summary>
        /// <returns>A ResponseData object.</returns>
        private ResponseData CreateResponseData()
        {
            ResponseData data = new ResponseData();

            data.MIResponse = this.CreateMIResponse();

            return data;
        }

        /// <summary>
        /// Creates an MIResponse object with the transaction ID for the acknowledgement.
        /// </summary>
        /// <returns>An MIResponse object.</returns>
        private MIResponse CreateMIResponse()
        {
            MIResponse response = new MIResponse();

            ////response.KeyList;
            response.MISMOVersionID = "2.3.1";

            if (this.transactionID != Guid.Empty)
            {
                response.MITransactionIdentifier = this.transactionID.ToString("D");
            }

            ////response.Status;

            return response;
        }
    }
}
