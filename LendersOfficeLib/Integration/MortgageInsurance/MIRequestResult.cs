﻿namespace LendersOffice.Integration.MortgageInsurance
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;

    /// <summary>
    /// Holds the results from an MI Framework request.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class MIRequestResult
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="MIRequestResult"/> class from being created.
        /// </summary>
        [JsonConstructor]
        private MIRequestResult()
        {
        }

        /// <summary>
        /// Gets the status of the request.
        /// </summary>
        [JsonProperty]
        public MIRequestResultStatus Status { get; private set; }

        /// <summary>
        /// Gets the result message.
        /// </summary>
        [JsonProperty]
        public string Message { get; private set; }

        /// <summary>
        /// Gets the error messages produced while processing the request.
        /// </summary>
        [JsonProperty]
        public IEnumerable<string> Errors { get; private set; }

        /// <summary>
        /// Gets the order number.
        /// </summary>
        [JsonProperty]
        public string OrderNumber { get; private set; }

        /// <summary>
        /// Gets the order transaction id.
        /// </summary>
        [JsonProperty]
        public Guid TransactionId { get; private set; }

        /// <summary>
        /// Gets a value indicating whether edoc upload has failed.
        /// </summary>
        [JsonProperty]
        public bool EdocUploadFailed { get; private set; }

        /// <summary>
        /// Gets the public job id that can be used to poll the Background Job Processor.
        /// </summary>
        [JsonProperty]
        public Guid PublicJobId { get; private set; }

        /// <summary>
        /// Creates an error result.
        /// </summary>
        /// <param name="errors">The errors produced during processing.</param>
        /// <returns>The error result.</returns>
        public static MIRequestResult CreateErrorResult(IEnumerable<string> errors)
        {
            return new MIRequestResult()
            {
                Status = MIRequestResultStatus.Failure,
                Errors = errors
            };
        }

        /// <summary>
        /// Creates success results.
        /// </summary>
        /// <param name="message">The success message.</param>
        /// <param name="orderNumber">The order number.</param>
        /// <param name="transactionId">The transaction id of the order.</param>
        /// <param name="edocUploadFailed">Whether uploading to edocs failed.</param>
        /// <returns>The success result.</returns>
        public static MIRequestResult CreateSuccessResult(string message, string orderNumber, Guid transactionId, bool edocUploadFailed)
        {
            return new MIRequestResult()
            {
                Status = MIRequestResultStatus.Success,
                Message = message,
                OrderNumber = orderNumber,
                TransactionId = transactionId,
                EdocUploadFailed = edocUploadFailed
            };
        }

        /// <summary>
        /// Creates processing results.
        /// </summary>
        /// <param name="publicJobId">The public job id that can be used to poll the background job processor.</param>
        /// <returns>The processing results.</returns>
        public static MIRequestResult CreateProcessingResult(Guid publicJobId)
        {
            return new MIRequestResult()
            {
                Status = MIRequestResultStatus.Processing,
                PublicJobId = publicJobId
            };
        }
    }
}
