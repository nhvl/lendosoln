﻿namespace LendersOffice.Integration.Irs4506T
{
    using System;
    using System.IO;
    using System.Xml;
    using Common;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.HttpRequest;
    using OpenFinancialExchange;

    /// <summary>
    /// Represents a response from Equifax in response to a 4506-T order submission.
    /// </summary>
    public class Equifax4506TSubmitOrderResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Equifax4506TSubmitOrderResponse"/> class.
        /// </summary>
        /// <param name="xmlString">The xml string to parse while initializing the object.</param>
        private Equifax4506TSubmitOrderResponse(string xmlString)
        {
            this.XmlContent = xmlString;

            using (TextReader tr = new StringReader(xmlString))
            using (XmlReader xr = XmlReader.Create(tr))
            {
                this.OfxContent = new OFX();
                this.OfxContent.ReadXml(xr);
            }

            this.ServerId = this.OfxContent.EIVVERMSGSRSV1.EIVTAXTRANSCRIPTTRNRS?.EIVTAXTRANSCRIPTRS?.SRVRTID;

            this.Status = E_STATUSCODE.Success;
            if (this.OfxContent.EIVVERMSGSRSV1.EIVTAXTRANSCRIPTTRNRS.STATUS.CODE != E_STATUSCODE.Success)
            {
                this.Status = E_STATUSCODE.Error;

                this.Message = string.IsNullOrEmpty(this.Message) ? this.OfxContent.EIVVERMSGSRSV1.EIVTAXTRANSCRIPTTRNRS.STATUS.MESSAGE
                    : this.Message + Environment.NewLine + this.OfxContent.EIVVERMSGSRSV1.EIVTAXTRANSCRIPTTRNRS.STATUS.MESSAGE;
            }

            if (this.OfxContent.SIGNONMSGSRSV1.SONRS.STATUS.CODE != E_STATUSCODE.Success)
            {
                this.Status = E_STATUSCODE.Error;
                this.Message = string.IsNullOrEmpty(this.Message) ? this.OfxContent.SIGNONMSGSRSV1.SONRS.STATUS.MESSAGE
                    : this.Message + Environment.NewLine + this.OfxContent.SIGNONMSGSRSV1.SONRS.STATUS.MESSAGE;
            }
        }

        /// <summary>
        /// Gets or sets the unparsed OFX XML returned from Equifax.
        /// </summary>
        /// <value>Unparsed XML content.</value>
        public string XmlContent { get; set; }

        /// <summary>
        /// Gets or sets the parsed OFX content returned from Equifax.
        /// </summary>
        /// <value>OFX object content.</value>
        public OFX OfxContent { get; set; }

        /// <summary>
        /// Gets or sets the Equifax server ID for the order.
        /// </summary>
        /// <value>Server ID.</value>
        public string ServerId { get; set; }

        /// <summary>
        /// Gets or sets the status of the request to Equifax.
        /// </summary>
        /// <value>Response status.</value>
        public E_STATUSCODE Status { get; set; }

        /// <summary>
        /// Gets or sets the error message from Equifax.
        /// </summary>
        /// <value>Response message.</value>
        public string Message { get; set; }

        /// <summary>
        /// Populates a new instance of the <see cref="Equifax4506TSubmitOrderResponse"/> object 
        /// with parsed content from the response fields of the given <see cref="HttpRequestOptions"/> object.
        /// </summary>
        /// <param name="response">The HttpRequestOptions object with HTTP response data.</param>
        /// <returns>A new <see cref="Equifax4506TSubmitOrderResponse"/>. If the response data could not be parsed, returns null.</returns>
        public static Equifax4506TSubmitOrderResponse FromHttpResponse(HttpRequestOptions response)
        {
            MimeMultipartContent contentParts = Equifax4506TServer.ResponseToMultipart(response);
            if (contentParts != null)
            {
                string xmlBody = contentParts.Parts[0].Content;
                return new Equifax4506TSubmitOrderResponse(xmlBody);
            }

            if (response.ResponseHeaders["Content-Type"].StartsWith("application/x-ofx", StringComparison.OrdinalIgnoreCase))
            {
                return new Equifax4506TSubmitOrderResponse(response.ResponseBody);
            }

            return null;
        }
    }
}
