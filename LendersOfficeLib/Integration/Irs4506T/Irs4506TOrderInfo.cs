﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Drivers.NetFramework;
using LQB4506T.LQB4506TPost;
using LqbGrammar.DataTypes;

namespace LendersOffice.Integration.Irs4506T
{
    public class Irs4506TOrderInfo
    {
        private Irs4506TOrderInfo(IDataReader reader)
        {
            TransactionId = (Guid)reader["TransactionId"];
            sLId = (Guid)reader["sLId"];
            VendorId = (Guid)reader["VendorId"];
            aAppId = (Guid)reader["aAppId"];
            ApplicationName = (string)reader["ApplicationName"];
            OrderNumber = (string)reader["OrderNumber"];
            string storedStatus = (string)reader["Status"];
            this.StatusT = (Irs4506TOrderStatus)reader["StatusT"];
            OrderDate = (DateTime)reader["OrderDate"];
            StatusDate = (DateTime)reader["StatusDate"];
            sBrokerId = (Guid)reader["sBrokerId"];

            if (this.StatusT == Irs4506TOrderStatus.Unknown)
            {
                // Determine new status type from parsing description, if we can.
                Match regexMatch = Regex.Match(storedStatus, @"^((?<statustype>\w+)|(?<statustype>\w+) - (?<statusdesc>.+)|(?<statusdesc>.+))$");
                Irs4506TOrderStatus statusT;
                if (regexMatch.Success && Enum.TryParse(regexMatch.Groups["statustype"].Value, out statusT))
                {
                    this.StatusT = statusT;
                    this.StatusDescription = regexMatch.Groups["statusdesc"].Value;
                }
                else
                {
                    this.StatusDescription = storedStatus;
                }
            }
            else
            {
                this.StatusDescription = storedStatus;
            }

            string xml = (string)reader["UploadedFilesXmlContent"];
            if (!string.IsNullOrEmpty(xml))
                UploadedFiles = Irs4506TDocument.DeserializeList(xml);
        }
        private List<Irs4506TDocument> m_uploadedFiles = null;

        public Irs4506TCommunicationModel CommunicationModel { get; private set; }
        public Guid TransactionId { get; private set; }
        public Guid sLId { get; private set; }
        public Guid VendorId { get; private set; }
        public Guid aAppId { get; private set; }
        public string ApplicationName { get; private set; }
        public string OrderNumber { get; private set; }
        public Irs4506TOrderStatus StatusT { get; private set; }
        public string StatusDescription { get; private set; }
        public DateTime OrderDate { get; private set; }
        public DateTime StatusDate { get; private set; }
        public Guid sBrokerId { get; private set; }

        public List<Irs4506TDocument> UploadedFiles
        {
            get
            {
                if (m_uploadedFiles == null)
                    m_uploadedFiles = new List<Irs4506TDocument>();
                return m_uploadedFiles;
            }
            set { m_uploadedFiles = value; }
        }

        public string UploadDocumentsLinksHtml
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach (var file in UploadedFiles)
                {
                    sb.AppendFormat("{0} <a href=\"Order4506T.aspx?loanId={1}&TransactionId={2}&docKey={3}\" target=\"_parent\">view</a><br/>",
                        AntiXss.AspxTools.HtmlString(file.DocumentName), sLId, TransactionId, System.Net.WebUtility.UrlEncode(file.FileDBKey)); // sLId and TransactionId are both Guids, which are safe in a URL without encoding
                }
                return sb.ToString();
            }
        }

        public static void InsertOrder(Guid brokerId, Guid transactionId, Guid sLId, Guid aAppId, Guid vendorId, string applicationName, string orderNumber, string status, Irs4506TOrderStatus statusT)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@TransactionId", transactionId),
                                            new SqlParameter("@sLId", sLId),
                                            new SqlParameter("@aAppId", aAppId),
                                            new SqlParameter("@VendorId", vendorId),
                                            new SqlParameter("@ApplicationName", applicationName),
                                            new SqlParameter("@OrderNumber", orderNumber),
                                            new SqlParameter("@Status", status),
                                            new SqlParameter("@StatusT", (int)statusT)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "IRS_4506T_ORDER_INFO_Insert", 3, parameters);
        }

        public static IEnumerable<Irs4506TOrderInfo> RetrieveByLoanId(Guid sLId)
        {
            Guid brokerId;

            DbConnectionInfo.GetConnectionInfoByLoanId(sLId, out brokerId);

            SqlParameter[] parameters = {
                                            new SqlParameter("@sLId", sLId)
                                        };

            List<Irs4506TOrderInfo> list = new List<Irs4506TOrderInfo>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "IRS_4506T_ORDER_INFO_ListByLoan", parameters))
            {
                while (reader.Read())
                {
                    list.Add(new Irs4506TOrderInfo(reader));
                }
            }
            return list;
        }

        /// <summary>
        /// Retrieves a single order info object from the database, using its composite primary key.
        /// </summary>
        /// <param name="vendorId">The vendor ID of the order.</param>
        /// <param name="sLId">The loan ID associated with the order.</param>
        /// <param name="orderNumber">The order number identifying the order.</param>
        /// <returns>The identified order info object, or null if none was found.</returns>
        public static Irs4506TOrderInfo Retrieve(Guid vendorId, Guid sLId, string orderNumber, Guid brokerId = default(Guid))
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@sLId", sLId),
                new SqlParameter("@VendorId", vendorId),
                new SqlParameter("@OrderNumber", orderNumber)
            };

            DbConnectionInfo connInfo;
            if (brokerId != default(Guid))
            {
                connInfo = DbConnectionInfo.GetConnectionInfo(brokerId);
            }
            else
            {
                connInfo = DbConnectionInfo.GetConnectionInfoByLoanId(sLId, out brokerId);
            }

            using (DbConnection conn = connInfo.GetConnection())
            using (IDataReader reader = Drivers.SqlServerDB.StoredProcedureDriverHelper.ExecuteReader(
                conn: conn,
                transaction: null,
                procedureName: LqbGrammar.DataTypes.StoredProcedureName.Create("IRS_4506T_ORDER_INFO_Retrieve").Value,
                parameters: parameters,
                timeout: LqbGrammar.DataTypes.TimeoutInSeconds.Default))
            {
                if (reader.Read())
                {
                    return new Irs4506TOrderInfo(reader);
                }
            }
            return null;
        }

        /// <summary>
        /// Creates a FileDB key for the response XML of an order.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker for the order.
        /// </param>
        /// <param name="transactionId">
        /// The ID of the transaction for the order.
        /// </param>
        /// <returns>
        /// The FileDB key.
        /// </returns>
        public static string CreateFileDbKeyForResponseXml(Guid brokerId, Guid transactionId)
        {
            return Guid.NewGuid().ToString("N") + "_" + brokerId.ToString("N") + "_" + transactionId.ToString("N");
        }

        /// <summary>
        /// Masks sensitive data out of an LQB 4506-T XML response.
        /// </summary>
        /// <param name="responseXML">
        /// The response XML.
        /// </param>
        /// <returns>
        /// A string that has had sensitive data masked.
        /// </returns>
        public static string MaskSensitiveLqbPostResponseData(string responseXml)
        {
            if (!string.IsNullOrEmpty(responseXml))
            {
                var xmlDoc = Tools.CreateXmlDoc(responseXml);
                responseXml = MaskSensitiveLqbPostResponseData(xmlDoc);
            }

            return responseXml;
        }

        /// <summary>
        /// Masks sensitive data out of an LQB 4506-T XML response.
        /// </summary>
        /// <param name="responseXmlDocument">
        /// The response XML document.
        /// </param>
        /// <returns>
        /// A string that has had sensitive data masked.
        /// </returns>
        public static string MaskSensitiveLqbPostResponseData(System.Xml.XmlDocument responseXmlDocument)
        {
            var responseXml = string.Empty;

            if (responseXmlDocument != null)
            {
                var borrowerNode = responseXmlDocument.SelectSingleNode("LQB4506TPost/Data/Borrower");
                if (!string.IsNullOrEmpty(borrowerNode?.Attributes["_SSN"]?.Value))
                {
                    var semanticSsn = SocialSecurityNumber.Create(borrowerNode.Attributes["_SSN"].Value);
                    if (semanticSsn.HasValue)
                    {
                        borrowerNode.Attributes["_SSN"].Value = semanticSsn.Value.MaskedSsn;
                    }
                }

                responseXml = responseXmlDocument.OuterXml;
            }

            return responseXml;
        }

        public Irs4506TDocument AddDocument(EmbeddedFile d)
        {
            var doc = new Irs4506TDocument(d);
            UploadedFiles.Add(doc);
            return doc;
        }

        /// <summary>
        /// Update the response and status of an existing 4506-T order.
        /// </summary>
        /// <param name="statusT">The new status type to assign. <see cref="Irs4506TOrderStatus.Unknown"/> is treated as blank and will not be saved to the DB.</param>
        /// <param name="statusDescription">The new status description to assign.</param>
        /// <param name="responseXmlContent">The response XML to save as the vendor response.</param>
        public void UpdateResponse(Irs4506TOrderStatus statusT, string statusDescription, string responseXmlContent)
        {
            DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfo(this.sBrokerId);
            var previousFileDbKey = this.GetPreviousResponseFileKey(connInfo);

            var fileDbKey = CreateFileDbKeyForResponseXml(this.sBrokerId, this.TransactionId);
            FileDBTools.WriteData(E_FileDB.Normal, fileDbKey, responseXmlContent);

            SqlParameter[] parameters = {
                                            new SqlParameter("@TransactionId", this.TransactionId),
                                            new SqlParameter("@Status", statusDescription),
                                            new SqlParameter("@StatusT", statusT),
                                            new SqlParameter("@ResponseXmlContent", fileDbKey),
                                            new SqlParameter("@UploadedFilesXmlContent", XmlSerialize(this.UploadedFiles))
                                        };

            using (DbConnection conn = connInfo.GetConnection())
            {
                Drivers.SqlServerDB.StoredProcedureDriverHelper.ExecuteNonQuery(
                    conn,
                    transaction: null,
                    procedureName: StoredProcedureName.Create("IRS_4506T_ORDER_INFO_UpdateResponse").ForceValue(),
                    parameters: parameters,
                    timeout: TimeoutInSeconds.Default);
            }

            if (!string.IsNullOrEmpty(previousFileDbKey))
            {
                FileDBTools.Delete(E_FileDB.Normal, previousFileDbKey);
            }
        }

        /// <summary>
        /// Returns the FileDB key for a previous 4506-T response, if any.
        /// </summary>
        /// <param name="connInfo">
        /// The connection info to the database for the order.
        /// </param>
        /// <returns>
        /// The previous FileDB key or null if no previous order exists.
        /// </returns>
        private string GetPreviousResponseFileKey(DbConnectionInfo connInfo)
        {
            SqlParameter[] parameters = { new SqlParameter("@TransactionId", this.TransactionId) };

            using (var conn = connInfo.GetReadOnlyConnection())
            {
                var result = Drivers.SqlServerDB.StoredProcedureDriverHelper.ExecuteScalar(
                   conn,
                   transaction: null,
                   procedureName: StoredProcedureName.Create("IRS_4506T_ORDER_INFO_RetrieveResponseXmlContent").ForceValue(),
                   parameters: parameters,
                   timeout: TimeoutInSeconds.Default);

                return result as string;
            }
        }

        public class Irs4506TDocument
        {
            internal EmbeddedFile Document;
            //"_" is an invalid character in Base64, so this is guaranteed to never be in a real Base64 string.
            private const string FileDBPrefix = "IRS4506TDOCUMENT_FILEDBKEY_";
            public Irs4506TDocument(XElement xe, XmlSerializer docSer)
            {
                using (var xr = xe.CreateReader())
                {
                    SetDoc((EmbeddedFile)docSer.Deserialize(xr));
                }
            }
            public Irs4506TDocument(EmbeddedFile doc)
            {
                SetDoc(doc);
            }

            private void SetDoc(EmbeddedFile doc)
            {
                Document = doc;
                StoreDocumentToFileDB(doc);
            }


            public string FileDBKey
            {
                get
                {
                    return Document.EncodedDoc;
                }
            }

            public string DocumentName
            {
                get
                {
                    return Document.Name;
                }
            }

            public string DocumentFormat
            {
                get
                {
                    return Document.Type;
                }
            }

            private void StoreDocumentToFileDB(EmbeddedFile doc)
            {
                //Don't need to do anything if it's already been processed, or there isn't anything here
                if (string.IsNullOrEmpty(doc.EncodedDoc.TrimWhitespaceAndBOM())) return;
                if (doc.EncodedDoc.StartsWith(FileDBPrefix, StringComparison.CurrentCultureIgnoreCase)) return;

                //Otherwise save the actual file to FileDB, then replace the content with the FileDB key.
                Guid docKeyId = Guid.NewGuid();
                string file = Utilities.Base64StringToFile(doc.EncodedDoc, "_Irs4506T_" + docKeyId);
                string docFileDBKey = FileDBPrefix + docKeyId;

                FileDBTools.WriteFile(E_FileDB.Normal, docFileDBKey, file);
                doc.EncodedDoc = docFileDBKey;
            }

            public void UseFile(Action<FileInfo> toDo)
            {
                FileDBTools.UseFile(E_FileDB.Normal, Document.EncodedDoc, toDo);
            }

            public T UseFile<T>(Func<FileInfo, T> toDo)
            {
                return FileDBTools.UseFile<T>(E_FileDB.Normal, Document.EncodedDoc, toDo);
            }

            internal static List<Irs4506TDocument> DeserializeList(string xml)
            {
                //For some reason, deserializing with the XmlSerializer made with 
                //TypeOf(List<UploadedDocument>) causes out of memory errors. Doing it this way works.
                var doc = XDocument.Parse(xml);
                var nodes = doc.XPathSelectElements("//EmbeddedFile[@_Name]");
                return nodes.Select(d => new Irs4506TDocument(d, new XmlSerializer(typeof(EmbeddedFile))))
                            .ToList();
            }
        }

        internal static string XmlSerialize(List<Irs4506TDocument> l)
        {
            using (StringWriter8 sw = new StringWriter8())
            {
                XmlSerializer xs = new XmlSerializer(typeof(List<EmbeddedFile>));
                var docs = l.Select(d => d.Document).ToList();
                xs.Serialize(sw, docs);

                return sw.ToString();
            }
        }
    }
}
