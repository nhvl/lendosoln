﻿using System;
using System.Text;
using LQB4506T.LQB4506TResponse;
using LQB4506T.LQB4506TRequest;
using DataAccess;
using System.IO;
using System.Xml;
using System.Net;
using LendersOffice.Common;
using LQB4506T;
using LendersOffice.Constants;
using LendersOffice.Drivers.Gateways;
using EDocs;
using LqbGrammar;
using LqbGrammar.Exceptions;
using LqbGrammar.DataTypes;
using System.Text.RegularExpressions;

namespace LendersOffice.Integration.Irs4506T
{
    public class Irs4506TServer : I4506TServer
    {
        private Irs4506TVendorConfiguration vendorConfig;
        
        public Irs4506TServer(Irs4506TVendorConfiguration vendor)
        {
            this.vendorConfig = vendor;
        }

        public Equifax4506TStatus<bool> SubmitNewOrder(Guid loanId, Guid appId, E_BorrowerModeT borrowerMode, EDocument document, IUserPrincipal user, Irs4506TVendorCredential credential, string applicationName)
        {
            Lqb4506TOrderResponse response = SubmitLqb(user, credential, loanId, appId, borrowerMode, document, this.vendorConfig, applicationName);

            return new Equifax4506TStatus<bool>
            {
                Status = response.OrderStatus.RequestStatus == E_OrderStatusRequestStatus.Success,
                Messages = response.OrderStatus.ErrorList
            };
        }

        /// <summary>
        /// Not supported.
        /// </summary>
        /// <exception cref="NotSupportedException"></exception>
        public Equifax4506TStatus<Irs4506TOrderStatus> CheckOrderStatus(Guid loanId, Irs4506TOrderInfo order, IUserPrincipal user, Irs4506TVendorCredential credential)
        {
            throw new NotSupportedException("IRS 4506-T server with Communication Model = 'LQB' does not support checking order status.");
        }

        public class BuiltRequestInfo
        {
            public Encoding Encoding { get; private set; }
            public byte[] Bytes { get; private set; }
            public RequestBuilderType RequestBuilderType { get; private set; }
            
            public BuiltRequestInfo(Encoding encoding, byte[] bytes, RequestBuilderType requestBuilderType)
            {
                this.Encoding = encoding;
                this.Bytes = bytes;
                this.RequestBuilderType = requestBuilderType;
            }
        }

        public enum RequestBuilderType
        {
            CreateRequestObject = 0,
            CreateRequestWithXslTransform = 1
        }

        private static BuiltRequestInfo CreateRequest(IUserPrincipal principal, Guid transactionId, Irs4506TVendorCredential credential, Guid loanId, Guid appId, E_BorrowerModeT borrowerMode, EDocument edoc)
        {
            var requestFormatDetails = Irs4506TVendorConfiguration.Temp_GetOnlyVendorIntegrationRequestFormat(principal);

            if (requestFormatDetails != null && requestFormatDetails.VendorId == credential.VendorId)
            {
                try
                {
                    return CreateRequestWithXslTransform(requestFormatDetails, principal, transactionId, credential, loanId, appId, borrowerMode, edoc);
                }
                catch (Exception e)
                {
                    Tools.LogError(e);

                    // ideally we validate prior to going live with their setup, but for phase 1 here we are emailing the ise team. (opm 463659).
                    EmailUtilities.SendToIntegrationTeamWithoutCreatingTicket("Irs4506TRequestSetup",
                        $@"Failed to create Irs4506TRequest using new xslt approach for vendor {requestFormatDetails.VendorId} 
while using LoXml {requestFormatDetails.LoXmlFileName} and Xsl {requestFormatDetails.XslFileName}

Immediate Actionable Item: Go to LoAdmin 'Manage 4506T Vendors', select 'None' for vendor using xslt and hit 'Update vendor and files for xsl transform'.
After that: Fix your LoXml and Xsl files on a loan's test page, and upload fixed files when ready, perhaps after business hours to prevent further interruption.");

                    throw;
                }
            }
            else
            {
                var request = CreateRequestObject(principal, transactionId, credential, loanId, appId, borrowerMode, edoc);
                byte[] bytes = null;

                using (MemoryStream stream = new MemoryStream(5000))
                {
                    var settings = new XmlWriterSettings();
                    settings.Encoding = System.Text.Encoding.ASCII;
                    using (XmlWriter writer = XmlTextWriter.Create(stream, settings))
                    {
                        request.WriteXml(writer);
                    }

                    bytes = stream.ToArray();
                    return new BuiltRequestInfo(Encoding.ASCII, bytes, RequestBuilderType.CreateRequestObject);
                }
            }
        }

        /// <summary>
        /// Made this public for use in testing.  It has credentials so be careful where you use it.
        /// </summary>
        public static Lqb4506TOrderRequest CreateRequestObject(IUserPrincipal user, Guid transactionId, Irs4506TVendorCredential credential, Guid loanId, Guid appId, E_BorrowerModeT borrowerMode, EDocument edoc)
        {
            if (credential == null)
            {
                throw new ArgumentNullException(nameof(credential));
            }

            var loanData = CPageData.CreateUsingSmartDependency(loanId, typeof(Irs4506TServer));
            loanData.InitLoad();
            if (loanData.sBrokerId != user.BrokerId)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            CAppData appData = loanData.GetAppData(appId);
            if (appData == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"LoanId:{loanId} and AppId:{appId} don't match."));
            }

            appData.BorrowerModeT = borrowerMode;

            Lqb4506TOrderRequest orderRequest = new Lqb4506TOrderRequest();

            orderRequest.AccountId = credential.AccountId;
            orderRequest.Username = credential.UserName;
            orderRequest.Password = credential.DecryptedPassword;
            orderRequest.Source.LoanId = loanData.sLId.ToString();
            orderRequest.Source.TransactionId = transactionId.ToString();
            orderRequest.Source.SourceName = "LendingQB";

            orderRequest.Source.PostBackUrl = ConstStage.Irs4056TPostBackUrl; // 7/23/2014 dd - OPM 184383 - Allow CMG to have different postback URL.

            orderRequest.Source.LoanNumber = loanData.sLNm;
            orderRequest.TranscriptData.BorrInfo.SpousesFilingType =
                appData.aIs4506TFiledTaxesSeparately ? E_BorrInfoSpousesFilingType.Separately : E_BorrInfoSpousesFilingType.Jointly;

            orderRequest.TranscriptData.BorrInfo.BorrName = appData.aB4506TNmPdfDisplay;
            orderRequest.TranscriptData.BorrInfo.BorrowerSsn = appData.aB4506TSsnTinEinCalc;

            orderRequest.TranscriptData.BorrInfo.CoBorrName = appData.aC4506TNmPdfDisplay;
            orderRequest.TranscriptData.BorrInfo.CoBorrowerSsn = appData.aC4506TSsnTinEinCalc;

            orderRequest.TranscriptData.CurrentAddress.StreetAddr = appData.a4506TStreetAddr;
            orderRequest.TranscriptData.CurrentAddress.City = appData.a4506TCity;
            orderRequest.TranscriptData.CurrentAddress.State = appData.a4506TState;
            orderRequest.TranscriptData.CurrentAddress.Zip = appData.a4506TZip;

            orderRequest.TranscriptData.PreviousAddress.StreetAddr = appData.a4506TPrevStreetAddr;
            orderRequest.TranscriptData.PreviousAddress.City = appData.a4506TPrevCity;
            orderRequest.TranscriptData.PreviousAddress.State = appData.a4506TPrevState;
            orderRequest.TranscriptData.PreviousAddress.Zip = appData.a4506TPrevZip;

            orderRequest.TranscriptData.ThirdPartyMailTo.Name = appData.a4506TThirdPartyName;
            orderRequest.TranscriptData.ThirdPartyMailTo.StreetAddr = appData.a4506TThirdPartyStreetAddr;
            orderRequest.TranscriptData.ThirdPartyMailTo.City = appData.a4506TThirdPartyCity;
            orderRequest.TranscriptData.ThirdPartyMailTo.State = appData.a4506TThirdPartyState;
            orderRequest.TranscriptData.ThirdPartyMailTo.Zip = appData.a4506TThirdPartyZip;
            orderRequest.TranscriptData.ThirdPartyMailTo.Phone = appData.a4506TThirdPartyPhone;

            if (string.IsNullOrEmpty(appData.a4506TYear1_rep) == false)
            {
                orderRequest.TranscriptData.TranscriptOptions.YearRequestedList.Add(appData.a4506TYear1_rep);
            }
            if (string.IsNullOrEmpty(appData.a4506TYear2_rep) == false)
            {
                orderRequest.TranscriptData.TranscriptOptions.YearRequestedList.Add(appData.a4506TYear2_rep);
            }
            if (string.IsNullOrEmpty(appData.a4506TYear3_rep) == false)
            {
                orderRequest.TranscriptData.TranscriptOptions.YearRequestedList.Add(appData.a4506TYear3_rep);
            }
            if (string.IsNullOrEmpty(appData.a4506TYear4_rep) == false)
            {
                orderRequest.TranscriptData.TranscriptOptions.YearRequestedList.Add(appData.a4506TYear4_rep);
            }

            orderRequest.TranscriptData.TranscriptOptions.RequestedTranscript = appData.a4506TTranscript;
            orderRequest.TranscriptData.TranscriptOptions.ReturnTranscriptIndicator = ToYN(appData.a4056TIsReturnTranscript);
            orderRequest.TranscriptData.TranscriptOptions.AccountTranscriptIndicator = ToYN(appData.a4506TIsAccountTranscript);
            orderRequest.TranscriptData.TranscriptOptions.RecordOfAccountIndicator = ToYN(appData.a4056TIsRecordAccountTranscript);
            orderRequest.TranscriptData.TranscriptOptions.VerificationOfNonfilingIndicator = ToYN(appData.a4506TVerificationNonfiling);
            orderRequest.TranscriptData.TranscriptOptions.W2109910985498Indicator = ToYN(appData.a4506TSeriesTranscript);
            orderRequest.TranscriptData.TranscriptOptions.IdentityTheftIndicator = ToYN(appData.a4506TRequestYrHadIdentityTheft);

            if (edoc != null)
            {
                orderRequest.Document.Name = edoc.DocTypeName;
                orderRequest.Document.Type = "PDF";
                orderRequest.Document.EncodingType = E_DocumentEncodingType.Base64;
                orderRequest.Document.EncodedDoc = Convert.ToBase64String(BinaryFileHelper.ReadAllBytes(edoc.GetPDFTempFile_Current()));
            }

            return orderRequest;
        }

        // TODO: Ideally for memory this isn't returning anything, but rather writing to a stream, but also not tying up the stream while writing to it.
        public static BuiltRequestInfo CreateRequestWithXslTransform(
            VendorSpecificIntegrationRequestFormat requestFormatDetails, 
            IUserPrincipal principal, 
            Guid transactionId, 
            Irs4506TVendorCredential credential, 
            Guid sLId, 
            Guid aAppId, 
            E_BorrowerModeT borrowerMode,
            EDocument edoc)
        {
            if (requestFormatDetails == null)
            {
                throw new ArgumentNullException(nameof(requestFormatDetails));
            }
            if (credential == null)
            {
                throw new ArgumentNullException(nameof(credential));
            }
            if (requestFormatDetails.VendorId != credential.VendorId)
            {
                throw new DeveloperException(
                    ErrorMessage.SystemError, 
                    new SimpleContext($"requestFormatDetails.vendorId:{requestFormatDetails.VendorId} and credential.vendorid:{credential.VendorId} don't match."));
            }

            var loXmlFileName = SharedDbBackedFilesManager.GetFileKey(SharedDbBackedFileType.LoXml, requestFormatDetails.LoXmlFileName);
            FileDBTools.EnsureDiskHasCopy(E_FileDB.Normal, loXmlFileName, loXmlFileName);
            var loXml = File.ReadAllText(TempFileUtils.Name2Path(loXmlFileName));
            
            var xmlWithLoanData = Conversions.LOFormatExporter.Export(
                sLId, 
                (Security.AbstractUserPrincipal)principal,
                loXml,
                default(FormatTarget),
                isClosingDisclosure:false,
                bypassFieldSecurity:false,
                useTempArchiveForTridFile:false,
                excludeZeroAdjustmentForDocGenInLoXml:false,
                borrowerMode:borrowerMode);

            var transformationArguments = new System.Xml.Xsl.XsltArgumentList();
            var transformationNs = string.Empty;
            transformationArguments.AddParam("username", transformationNs, credential.UserName);
            transformationArguments.AddParam("password", transformationNs, credential.DecryptedPassword);
            transformationArguments.AddParam("AccountID", transformationNs, credential.AccountId);
            transformationArguments.AddParam("ApplicationID", transformationNs, aAppId.ToString());
            transformationArguments.AddParam("TransactionID", transformationNs, transactionId.ToString());
            transformationArguments.AddParam("BorrowerMode", transformationNs, borrowerMode == E_BorrowerModeT.Coborrower ? "C" : "B");
            transformationArguments.AddParam("PostBackURL", transformationNs, ConstStage.Irs4056TPostBackUrl);
            transformationArguments.AddParam("SourceName", transformationNs, "LendingQB");

            if (edoc != null)
            {
                transformationArguments.AddParam("DocumentName", transformationNs, edoc.DocTypeName);
                transformationArguments.AddParam("DocumentContents", transformationNs, Convert.ToBase64String(BinaryFileHelper.ReadAllBytes(edoc.GetPDFTempFile_Current())));
            }

            var transformName = SharedDbBackedFilesManager.GetFileKey(SharedDbBackedFileType.Xsl, requestFormatDetails.XslFileName);
            var transformPath = TempFileUtils.Name2Path(transformName);
            
            // TODO: ideally get rid of the memory stream, if the request gets large this just eats memory.  However LogRequest takes the bytes.
            byte[] bytes = null;
            using (MemoryStream stream = new MemoryStream(5000))
            {
                // don't need the encoding since it's specificied as an attribute of the xsl:output element.
                // the compiled transform itself gets cached in the helper.

                // it's ok to use this method here since Xsl files are write-once and the name doesn't change.
                FileDBTools.EnsureDiskHasCopy(E_FileDB.Normal, transformName, transformName);

                XslTransformHelper.Transform(transformPath, xmlWithLoanData, stream, transformationArguments);

                bytes = stream.ToArray();
            }

            var encoding = XslTransformHelper.GetOutputEncoding(transformPath);
            return new BuiltRequestInfo(encoding, bytes, RequestBuilderType.CreateRequestWithXslTransform);
        }
        
        private static Lqb4506TOrderResponse SubmitLqb(IUserPrincipal user, Irs4506TVendorCredential credential, Guid loanId, Guid appId, E_BorrowerModeT borrowerMode, EDocument edoc, Irs4506TVendorConfiguration vendorConfig, string applicationName)
        { 
            Guid transactionId = Guid.NewGuid();
            // If not supplied, load the credentials on file for the user.
            if (credential == null)
            {
                credential = Irs4506TVendorCredential.Retrieve(user.BrokerId, user.UserId, vendorConfig.VendorId);
            }

            var builtRequestInfo = CreateRequest(user, transactionId, credential, loanId, appId, borrowerMode, edoc);
            var encoding = builtRequestInfo.Encoding;
            var bytes = builtRequestInfo.Bytes;

            var url = vendorConfig.ExportPath;
            Tools.LogInfo("Irs4506TServer", url);

            try
            {
                LogRequest(builtRequestInfo);
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.KeepAlive = false; // 11/4/2004 dd - DO NOT REMOVE THIS LINE. Else you will get "Cannot access a disposed object named "System.Net.TlsStream"" error randomly.
                webRequest.Method = "POST";

                webRequest.ContentType = "text/xml";
                webRequest.ContentLength = bytes.Length;
                LendersOffice.ObjLib.Security.ThirdParty.ThirdPartyClientCertificateManager.AppendCertificate(webRequest);

                // Send out the data
                using (Stream stream = webRequest.GetRequestStream())
                {
                    stream.Write(bytes, 0, bytes.Length);
                }

                StringBuilder sb = new StringBuilder();

                using (WebResponse webResponse = webRequest.GetResponse())
                {
                    using (Stream stream = webResponse.GetResponseStream())
                    {
                        byte[] buffer = new byte[60000];
                        int size = stream.Read(buffer, 0, buffer.Length);

                        while (size > 0)
                        {
                            string chunk = System.Text.Encoding.UTF8.GetString(buffer, 0, size);
                            sb.Append(chunk);
                            size = stream.Read(buffer, 0, buffer.Length);
                        }

                    }
                }
                string rawXml = sb.ToString();
                if (!string.IsNullOrEmpty(rawXml) && rawXml[0] != '<')
                {
                    // This is to prevent an extra BOM characters in the request stream.
                    // http://en.wikipedia.org/wiki/Byte_order_mark
                    rawXml = rawXml.TrimWhitespaceAndBOM();
                }

                if (Tools.IsLogLargeRequest(rawXml.Length))
                {
                    Tools.LogInfo("Irs4506TServerResponse", rawXml);
                }
                else
                {
                    Tools.LogInfo("Irs4506TServerResponse", rawXml.Length + " bytes exceed threshold for logging.");
                }

                Lqb4506TOrderResponse response = new Lqb4506TOrderResponse();
                XmlReaderSettings readerSettings = new XmlReaderSettings();
#if LQB_NET45
                readerSettings.DtdProcessing = DtdProcessing.Parse;
#else
                readerSettings.ProhibitDtd = false;
#endif
                readerSettings.IgnoreProcessingInstructions = true;

                using (XmlReader reader = XmlReader.Create(new StringReader(rawXml), readerSettings))
                {
                    response.ReadXml(reader);
                }

                if (response.OrderStatus.RequestStatus == E_OrderStatusRequestStatus.Success)
                {
                    Irs4506TOrderInfo.InsertOrder(
                        user.BrokerId,
                        transactionId,
                        loanId,
                        appId,
                        vendorConfig.VendorId,
                        applicationName,
                        response.OrderStatus.OrderNumber,
                        status: "Ordered",
                        statusT: Irs4506TOrderStatus.Ordered);
                }

                return response;
            }
            catch (XmlException exc)
            {
                Tools.LogError(exc);

                if (builtRequestInfo.RequestBuilderType == RequestBuilderType.CreateRequestWithXslTransform && ConstStage.AlertIsesOfWebFailuresOnXsltBuiltRequests)
                {
                    EmailUtilities.SendToIntegrationTeamWithoutCreatingTicket(
                        "Irs4506TServerXmlExceptionWithXslt",
$@"Got invalid response (XmlException) from vendor {vendorConfig.VendorId} ({vendorConfig.VendorName}) when making Irs4506TRequest using new xslt approach.

Immediate Actionable Item: 
    A)  Check vendor's format:
         i. Find out if the vendor has changed their response format (check PB logs for logged response).  
        ii.  If so, feel free to disable ConstStage.AlertIsesOfWebFailuresOnXsltBuiltRequest in stage config until they can fix the issue to reduce emails.
OR  B)  Undo and redo the format association:
         i. Go to LoAdmin 'Manage 4506T Vendors', select 'None' for vendor using xslt and hit 'Update vendor and files for xsl transform'.
        ii. Fix your LoXml and Xsl files on a loan's test page, and upload fixed files when ready, perhaps after business hours to prevent further interruption.");
                }

                return BuildErrorResponse("Invalid Response from Vendor");
            }
            catch (WebException exc)
            {
                Tools.LogError(exc);

                if (builtRequestInfo.RequestBuilderType == RequestBuilderType.CreateRequestWithXslTransform && ConstStage.AlertIsesOfWebFailuresOnXsltBuiltRequests)
                {
                    EmailUtilities.SendToIntegrationTeamWithoutCreatingTicket(
                        "Irs4506TServerWebRequestExceptionWithXslt",
$@"Got WebException for vendor {vendorConfig.VendorId} ({vendorConfig.VendorName}) when making Irs4506TRequest using new xslt approach.

Immediate Actionable Item: 
    A)  Check vendor's format:
        i.  Find out if the vendor has connectivity (Check URL connectivity page).
        ii.  If non connective, feel free to disable ConstStage.AlertIsesOfWebFailuresOnXsltBuiltRequest in stage config until they can fix the issue to reduce emails.
OR  B)  Undo and redo the format association:
         i. Go to LoAdmin 'Manage 4506T Vendors', select 'None' for vendor using xslt and hit 'Update vendor and files for xsl transform'.
        ii. Fix your LoXml and Xsl files on a loan's test page, and upload fixed files when ready, perhaps after business hours to prevent further interruption.");
                }

                return BuildErrorResponse("Unable to connect to vendor. Please try again.");
            }
            catch (Exception exc)
            {
                Tools.LogError(exc);

                throw;
            }
        }

        private static void LogRequest(BuiltRequestInfo request)
        {
            var bytes = request.Bytes;

            if (!Tools.IsLogLargeRequest(bytes.Length))
            {
                Tools.LogInfo("Irs4506TServerRequest", bytes.Length + " bytes exceed threshold for logging.");
                return;
            }

            switch (request.RequestBuilderType)
            {
                case RequestBuilderType.CreateRequestWithXslTransform:
                    string bytesString = request.Encoding.GetString(bytes);
                    // use nongreedy matching so we don't stomp over values
                    bytesString = Regex.Replace(bytesString,
                        " Password=\"[^\"]+?\"", " Password=\"******\"");
                    Tools.LogInfo("Irs4056TServerRequest", bytesString);

                    return;
                case RequestBuilderType.CreateRequestObject:
                    Lqb4506TOrderRequest requestForLogging = new Lqb4506TOrderRequest();
                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        using (XmlReader reader = XmlReader.Create(ms))
                        {
                            requestForLogging.ReadXml(reader);
                        }
                    }

                    StringBuilder debugLog = new StringBuilder();
                    using (XmlWriter writer = XmlWriter.Create(debugLog))
                    {
                        requestForLogging.Password = "******";
                        requestForLogging.Document.EncodedDoc = "[Base64EncodedData]";
                        requestForLogging.WriteXml(writer);
                    }
                    Tools.LogInfo("Irs4056TServerRequest", debugLog.ToString());

                    return;
                default:
                    throw new UnhandledEnumException(request.RequestBuilderType);
            }
        }

        private static Lqb4506TOrderResponse BuildErrorResponse(string errorMessage)
        {
            Lqb4506TOrderResponse response = new Lqb4506TOrderResponse();
            response.OrderStatus.RequestStatus = E_OrderStatusRequestStatus.Failure;
            response.OrderStatus.ErrorList.Add(errorMessage);

            return response;
        }

        private static  E_YNIndicator ToYN(bool p)
        {
            return p ? E_YNIndicator.Y : E_YNIndicator.N;
        }
    }
}
