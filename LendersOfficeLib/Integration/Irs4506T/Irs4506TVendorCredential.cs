﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using System.Data;
using System.Data.SqlClient;
using DataAccess;
using LqbGrammar.DataTypes;

namespace LendersOffice.Integration.Irs4506T
{
    public class Irs4506TVendorCredential
    {
        public Guid BrokerId { get; private set; }

        public Guid UserId { get; private set; }
        public Guid VendorId { get; private set; }

        public string AccountId { get; set; }
        public string UserName { get; set; }

        private Lazy<string> lazyPassword;

        private EncryptionKeyIdentifier encryptionKeyId;

        public string DecryptedPassword => this.lazyPassword?.Value ?? string.Empty;

        private Irs4506TVendorCredential(Guid brokerId, IDataReader reader)
        {
            this.UserId = (Guid)reader["UserId"];
            this.VendorId = (Guid)reader["VendorId"];
            this.AccountId = (string)reader["AccountId"];
            this.UserName = (string)reader["UserName"];
            byte[] encryptedPassword = (byte[])reader["Password"];
            EncryptionKeyIdentifier encryptionKeyId = EncryptionKeyIdentifier.Create((Guid)reader["EncryptionKeyId"]).ForceValue();
            this.lazyPassword = new Lazy<string>(() => Drivers.Encryption.EncryptionHelper.DecryptString(encryptionKeyId, encryptedPassword));
            this.encryptionKeyId = encryptionKeyId;
            this.BrokerId = brokerId;
        }

        private Irs4506TVendorCredential(Guid brokerId)
        {
            this.BrokerId = brokerId;
        }

        public Irs4506TVendorCredential(Guid brokerId, string username, string password, string accountId, Guid vendorId, Guid userId)
        {
            this.UserName = username;
            this.lazyPassword = new Lazy<string>(() => password);
            this.AccountId = accountId;
            this.VendorId = vendorId;
            this.UserId = userId;
            this.BrokerId = brokerId;
        }

        public void SetPassword(string password)
        {
            this.lazyPassword = new Lazy<string>(() => password);
        }

        public void Save()
        {
            if (this.encryptionKeyId == default(EncryptionKeyIdentifier))
            {
                this.encryptionKeyId = Drivers.Encryption.EncryptionHelper.GenerateNewKey();
            }

            if (string.IsNullOrEmpty(this.AccountId) &&
                string.IsNullOrEmpty(this.UserName) &&
                string.IsNullOrEmpty(this.DecryptedPassword))
            {

                // 8/12/2013 dd - There are no save credential. No need to update.
                return;
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", UserId),
                                            new SqlParameter("@VendorId", VendorId),
                                            new SqlParameter("@AccountId", AccountId ?? string.Empty),
                                            new SqlParameter("@UserName", UserName ?? string.Empty),
                                            new SqlParameter("@Password", Drivers.Encryption.EncryptionHelper.EncryptString(this.encryptionKeyId, this.DecryptedPassword)),
                                            new SqlParameter("@EncryptionKeyId", this.encryptionKeyId.Value),
                                        };

            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "IRS_4506T_VENDOR_EMPLOYEE_INFO_Save", 2, parameters);
        }

        public static Irs4506TVendorCredential Retrieve(Guid brokerId, Guid userId, Guid vendorId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@UserId", userId),
                                            new SqlParameter("@VendorId", vendorId)

                                        };

            Irs4506TVendorCredential credential = null;

            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "IRS_4506T_VENDOR_EMPLOYEE_INFO_Retrieve", parameters))
            {
                if (reader.Read())
                {
                    credential = new Irs4506TVendorCredential(brokerId, reader);
                }
            }

            if (credential == null)
            {
                credential = new Irs4506TVendorCredential(brokerId);
                credential.UserId = userId;
                credential.VendorId = vendorId;
            }
            return credential;
        }

    }
}
