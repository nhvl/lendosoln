﻿namespace LendersOffice.Integration.Irs4506T
{
    using System;
    using DataAccess;
    using EDocs;
    using LqbGrammar;

    /// <summary>
    /// Defines a set of common methods for different 4506-T communication model servers to implement.
    /// </summary>
    public interface I4506TServer
    {
        /// <summary>
        /// Submits a new order to the 4506-T (Tax Return Verification) vendor.
        /// </summary>
        /// <param name="loanId">
        /// The identifier of the loan data object that 4506-T order data should be retrieved from.
        /// Identifier is passed so that smart dependency loading can be done within the server object.
        /// </param>
        /// <param name="appId">
        /// The identifier of the borrower application for which tax return data is being requested.
        /// Identifier is passed so that smart dependency loading can be done within the server object.
        /// </param>
        /// <param name="borrowerMode">The borrower mode touse for the application data to get the proper borrower fields.</param>
        /// <param name="document">The document corresponding to the signed borrower 4506-T consent form.</param>
        /// <param name="user">The user submitting the request.</param>
        /// <param name="credential">The credential to use for authenticating with the vendor. If null, the credential will be loaded for the user.</param>
        /// <param name="applicationName">The application name selected in the dropdown for our reference.  The application name depends on their tax filing mode.</param>
        /// <returns>A status details container with whether the submission succeeded, and any error messages.</returns>
        Equifax4506TStatus<bool> SubmitNewOrder(Guid loanId, Guid appId, E_BorrowerModeT borrowerMode, EDocument document, IUserPrincipal user, Irs4506TVendorCredential credential, string applicationName);

        /// <summary>
        /// Checks the status of an existing 4506-T order with Equifax.
        /// </summary>
        /// <param name="loanId">
        /// The identifier for the loan data object to load information from. 
        /// Identifier is passed so that smart dependency loading can be done within the server object.
        /// </param>
        /// <param name="order">The order in our system.</param>
        /// <param name="user">The user making the request.</param>
        /// <param name="credential">The credential to use for authenticating with the vendor. If null, the credential will be loaded for the user.</param>
        /// <returns>An object representing status and messages from the submission of the request (errors or order numbers).</returns>
        Equifax4506TStatus<Irs4506TOrderStatus> CheckOrderStatus(Guid loanId, Irs4506TOrderInfo order, IUserPrincipal user, Irs4506TVendorCredential credential);
    }
}
