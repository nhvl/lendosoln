﻿namespace LendersOffice.Integration.Irs4506T
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Common;
    using DataAccess;
    using EDocs;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.HttpRequest;
    using OpenFinancialExchange;
    using OpenFinancialExchange.Request;

    /// <summary>
    /// Organizes all the data necessary to generate an Equifax "Submit Order" request with an attached 4506-T tax transcript consent form.
    /// </summary>
    public class Equifax4506TSubmitOrderRequest
    {
        /// <summary>
        /// The loan data object to get loan data from.
        /// </summary>
        private CPageData loanData;

        /// <summary>
        /// The application data to get borrower information from.
        /// </summary>
        private CAppData appData;

        /// <summary>
        /// The vendor credential to use when authenticating with Equifax.
        /// </summary>
        private Irs4506TVendorCredential credential;

        /// <summary>
        /// Initializes a new instance of the <see cref="Equifax4506TSubmitOrderRequest"/> class.
        /// </summary>
        /// <param name="credential">The vendor credential to authenticate with Equifax.</param>
        /// <param name="loanData">The loan data object to get data from.</param>
        /// <param name="appData">The application data to get borrower information from.</param>
        /// <param name="signed4506TPdf">The EDocument to send with the request.</param>
        public Equifax4506TSubmitOrderRequest(Irs4506TVendorCredential credential, CPageData loanData, CAppData appData, EDocument signed4506TPdf)
        {
            this.DocumentData = signed4506TPdf;
            string sanitizedDescription = null;
            if (!string.IsNullOrWhiteSpace(signed4506TPdf?.PublicDescription))
            {
                // Remove all but a whitelist of characters.
                sanitizedDescription = Regex.Replace(signed4506TPdf.PublicDescription, @"[^A-Za-z0-9._:() -]", string.Empty);
            }

            this.DocumentName = string.IsNullOrEmpty(sanitizedDescription) ? $"{appData.aNm.Replace(' ', '-')}.pdf" : sanitizedDescription;
            if (!this.DocumentName.EndsWith(".pdf"))
            {
                this.DocumentName = this.DocumentName + ".pdf";
            }

            this.loanData = loanData;
            this.appData = appData;
            this.credential = credential;
        }

        /// <summary>
        /// Gets or sets the EDocument representing the signed IRS 4506-T form authorizing the order.
        /// </summary>
        private EDocument DocumentData { get; set; }

        /// <summary>
        /// Gets or sets the document name to use for <see cref="DocumentData"/> in the export.
        /// </summary>
        private string DocumentName { get; set; }

        /// <summary>
        /// Gets the OFX xml to send, containing information about the order submission request.
        /// </summary>
        /// <returns>The generated Open Financial Exchange Metadata for this request.</returns>
        public OFX BuildOpenFinancialExchangeMetadata()
        {
            return Equifax4506THelper.BuildOfxRequest(
                this.credential,
                new EIVVERMSGSRQV1
                {
                    EIVTAXTRANSCRIPTTRNRQ = new EIVTAXTRANSCRIPTTRNRQ()
                    {
                        TRNUID = this.loanData.sLenderCaseNum,
                        CLTCOOKIE = this.loanData.sLRefNm,
                        PLATFORM = Equifax4506TServer.LqbVendorId,
                        INTERMEDIARY = this.loanData.Branch.DisplayNm,
                        ENDUSER = PrincipalFactory.CurrentPrincipal.DisplayName,
                        TRNPURPOSE = new TRNPURPOSE
                        {
                            CODE = E_PPCODE.PPCREDIT
                        },
                        EIVTAXTRANSCRIPTRQ = CreateTaxTranscriptRequest(this.appData, this.DocumentName)
                    }
                });
        }

        /// <summary>
        /// Converts this request into a <see cref="HttpRequestOptions"/> object ready to send to Equifax.
        /// </summary>
        /// <param name="forLogging">Indicates whether the request is for logging. Logging mode will mask SSN and use a placeholder instead of base64 PDF request data.</param>
        /// <returns>The corresponding options object ready to send.</returns>
        public HttpRequestOptions GetHttpRequestOptions(bool forLogging)
        {
            var postData = this.ToMultipartContent(forLogging);

            return new HttpRequestOptions
            {
                PostData = new StringContent(postData.ToString()),
                MimeType = MimeType.MultiPart.CreateRelated(postData.Boundary, type: "application/x-ofx", capitalize: true),
                Method = HttpMethod.Post
            };
        }

        /// <summary>
        /// Converts this object into a MIME multipart content body for adding to an Http request.
        /// </summary>
        /// <param name="forLogging">Indicates whether the request is for logging. Logging mode will mask SSN and use a placeholder instead of base64 PDF request data.</param>
        /// <returns>This object as a MIME multipart content object.</returns>
        public MimeMultipartContent ToMultipartContent(bool forLogging)
        {
            MimeMultipartContent content = new MimeMultipartContent
            {
                Boundary = Equifax4506TServer.EquifaxMultipartBoundary,
            };
            content.Parts.Add(
                new MultipartPart
                {
                    Headers = new Dictionary<string, string>
                    {
                        { "Content-Type", "application/x-ofx" }
                    },
                    Content = forLogging ? Equifax4506THelper.MaskOfxXmlForLogging(this.BuildOpenFinancialExchangeMetadata().ToString()) : this.BuildOpenFinancialExchangeMetadata().ToString()
                });

            content.Parts.Add(
                new MultipartPart
                {
                    Headers = new Dictionary<string, string>
                    {
                        { "Content-Type", "application/pdf" },
                        { "Content-Transfer-Encoding", "base64" },
                        { "Content-Location", this.DocumentName }
                    },
                    Content = forLogging  ? "[Base64 PDF content has been omitted for logging.]" :
                        Convert.ToBase64String(Drivers.FileSystem.BinaryFileHelper.ReadAllBytes(LocalFilePath.Create(this.DocumentData.GetPDFTempFile_Current()) ?? LocalFilePath.Invalid))
                });

            return content;
        }

        /// <summary>
        /// Creates the tax transcript request portion of the OFX xml.
        /// </summary>
        /// <param name="appData">The application object to pull borrower and 4506-T data from.</param>
        /// <param name="documentName">The Edocument name referencing the base64 file.</param>
        /// <returns>An OFX element with information from the 4506-T form.</returns>
        private static EIVTAXTRANSCRIPTRQ CreateTaxTranscriptRequest(CAppData appData, string documentName)
        {
            bool isBusinessTranscript;
            E_VERIFICATIONATTACHMENTTYPE attachmentType = GetAttachmentType(appData, out isBusinessTranscript);

            var request = new EIVTAXTRANSCRIPTRQ
            {
                TAXYEAR = new List<string> { appData.a4506TYear1_rep, appData.a4506TYear2_rep, appData.a4506TYear3_rep, appData.a4506TYear4_rep }.Where(yr => !string.IsNullOrEmpty(yr)).ToList(),
                STATEDDOCOPTION = new STATEDDOCOPTION
                {
                    DOCOPTION = E_STATEDDOCOPTION.NONSTATEDDOC
                },
                BORROWEREMAIL = appData.aEmail,
                VERIFICATIONATTACHMENTS = new VERIFICATIONATTACHMENTS(
                    new List<VERIFICATIONATTACHMENT>
                    {
                        new VERIFICATIONATTACHMENT
                        {
                            VERIFICATIONATTACHMENTTYPE = new VERIFICATIONATTACHMENTTYPE
                            {
                                ATTACHMENTTYPE = attachmentType,
                            },
                            ATTACHEDFILES = new ATTACHEDFILES(
                                new List<ATTACHEDFILE>
                                {
                                    new ATTACHEDFILE
                                    {
                                        ATTACHMENTNAME = documentName,
                                        ATTACHMENTENCODING = "BASE64"
                                    }
                                })
                        }
                    })
            };

            if (isBusinessTranscript)
            {
                request.BUSINESSDETAILS = new BUSINESSDETAILS
                {
                    FEDEIN = appData.aB4506TSsnTinEinCalc,
                    BUSINESSNAME = appData.aB4506TNmPdfDisplay
                };
            }
            else
            {
                if (!string.IsNullOrEmpty(appData.a4506TStreetAddr)
                    && !string.IsNullOrEmpty(appData.a4506TCity)
                    && !string.IsNullOrEmpty(appData.a4506TState)
                    && !string.IsNullOrEmpty(appData.a4506TZip))
                {
                    request.BORROWERADDRESS = new BORROWERADDRESS
                    {
                        ADDR1 = appData.a4506TStreetAddr,
                        CITY = appData.a4506TCity,
                        STATE = appData.a4506TState,
                        POSTALCODE = appData.a4506TZip
                    };
                }

                AddEmployeeDetails(appData, request);
            }

            return request;
        }

        /// <summary>
        /// Determines the proper values for the <see cref="EIVTAXTRANSCRIPTRQ.EMPLOYEEDETAILS"/> and <see cref="EIVTAXTRANSCRIPTRQ.EMPLOYEEDETAILS"/> containers
        /// and adds them to the given parent container.
        /// </summary>
        /// <param name="appData">The borrower application object to use when populating the employee details information.</param>
        /// <param name="parentContainer">The parent container to populate with the resulting employee details.</param>
        private static void AddEmployeeDetails(CAppData appData, EIVTAXTRANSCRIPTRQ parentContainer)
        {
            Regex simpleNameRegex = new Regex(@"(?<firstname>\S+)\s?((?<middlename>\S+)\s)*(?<lastname>\S+)?");

            // aB4506T... fields automatically calculate the value of the active borrower (and coborrower, if applicable).
            Match primaryEmployeeNameMatch = simpleNameRegex.Match(appData.aB4506TNmPdfDisplay);
            if (primaryEmployeeNameMatch.Success)
            {
                parentContainer.EMPLOYEEDETAILS = new EMPLOYEEDETAILS
                {
                    EIVEMPLOYEEID = appData.aB4506TSsnTinEinCalc,
                    FIRSTNAME = primaryEmployeeNameMatch.Groups["firstname"].ToString(),
                    LASTNAME = primaryEmployeeNameMatch.Groups["lastname"].Success ? primaryEmployeeNameMatch.Groups["lastname"].ToString() : string.Empty
                };
            }

            Match coborrowerNameMatch = simpleNameRegex.Match(appData.aC4506TNmPdfDisplay);
            if (coborrowerNameMatch.Success)
            {
                parentContainer.EMPLOYEE2DETAILS = new EMPLOYEEDETAILS
                {
                    ElementName = "EMPLOYEE2DETAILS",
                    EIVEMPLOYEEID = appData.aC4506TSsnTinEinCalc,
                    FIRSTNAME = coborrowerNameMatch.Groups["firstname"].ToString(),
                    LASTNAME = coborrowerNameMatch.Groups["lastname"].Success ? coborrowerNameMatch.Groups["lastname"].ToString() : string.Empty
                };
            }
        }

        /// <summary>
        /// Converts the <see cref="CAppData.a4506TTranscript"/> value and other CAppData 4506-T options into a verification attachment type recognized by Equifax.
        /// </summary>
        /// <param name="application">The CAppData borrower application from which to pull 4506-T fields.</param>
        /// <param name="isBusinessType">Indicates whether the returned attachment type corresponds to a business (true) or individual (false) tax document.</param>
        /// <remarks>There's a lot of string parsing here that could be removed with better UI on the 4506-T page.</remarks>
        /// <returns>A <see cref="E_VERIFICATIONATTACHMENTTYPE"/> enum value understood by Equifax.</returns>
        private static E_VERIFICATIONATTACHMENTTYPE GetAttachmentType(CAppData application, out bool isBusinessType)
        {
            if (application.a4506TTranscript.Contains("all income"))
            {
                isBusinessType = false;
                return E_VERIFICATIONATTACHMENTTYPE.IRS_ALL_INCOME;
            }

            if (application.a4506TTranscript.Contains("1040"))
            {
                isBusinessType = false;
                if (application.a4056TIsReturnTranscript)
                {
                    return E_VERIFICATIONATTACHMENTTYPE.IRS1040;
                }
                else if (application.a4506TIsAccountTranscript)
                {
                    return E_VERIFICATIONATTACHMENTTYPE.RAT1040;
                }
                else if (application.a4056TIsRecordAccountTranscript)
                {
                    return E_VERIFICATIONATTACHMENTTYPE.ROA1040;
                }
            }

            if (application.a4506TTranscript.Contains("1065"))
            {
                isBusinessType = true;
                if (application.a4056TIsReturnTranscript)
                {
                    return E_VERIFICATIONATTACHMENTTYPE.IRS1065;
                }
                else if (application.a4506TIsAccountTranscript)
                {
                    return E_VERIFICATIONATTACHMENTTYPE.RAT1065;
                }
                else if (application.a4056TIsRecordAccountTranscript)
                {
                    return E_VERIFICATIONATTACHMENTTYPE.ROA1065;
                }
            }

            // This appears to be the nicest way to do a case-insensitive "contains" operation.
            if (CultureInfo.InvariantCulture.CompareInfo.IndexOf(application.a4506TTranscript, "1120S", CompareOptions.OrdinalIgnoreCase) >= 0)
            {
                isBusinessType = true;
                if (application.a4056TIsReturnTranscript)
                {
                    return E_VERIFICATIONATTACHMENTTYPE.IRS1120S;
                }
                else if (application.a4506TIsAccountTranscript)
                {
                    return E_VERIFICATIONATTACHMENTTYPE.RAT1120S;
                }
                else if (application.a4056TIsRecordAccountTranscript)
                {
                    return E_VERIFICATIONATTACHMENTTYPE.ROA1120S;
                }
            }

            if (application.a4506TTranscript.Contains("1120"))
            {
                isBusinessType = true;
                if (application.a4056TIsReturnTranscript)
                {
                    return E_VERIFICATIONATTACHMENTTYPE.IRS1120;
                }
                else if (application.a4506TIsAccountTranscript)
                {
                    return E_VERIFICATIONATTACHMENTTYPE.RAT1120;
                }
                else if (application.a4056TIsRecordAccountTranscript)
                {
                    return E_VERIFICATIONATTACHMENTTYPE.ROA1120;
                }
            }

            if (CultureInfo.InvariantCulture.CompareInfo.IndexOf(application.a4506TTranscript, "W-2", CompareOptions.OrdinalIgnoreCase) >= 0
                || CultureInfo.InvariantCulture.CompareInfo.IndexOf(application.a4506TTranscript, "W2", CompareOptions.OrdinalIgnoreCase) >= 0)
            {
                isBusinessType = false;
                return E_VERIFICATIONATTACHMENTTYPE.IRSW2;
            }

            if (application.a4506TTranscript.Contains("1099"))
            {
                isBusinessType = false;
                return E_VERIFICATIONATTACHMENTTYPE.IRS1099;
            }

            if (CultureInfo.InvariantCulture.CompareInfo.IndexOf(application.a4506TTranscript, "K-1", CompareOptions.OrdinalIgnoreCase) >= 0
                || CultureInfo.InvariantCulture.CompareInfo.IndexOf(application.a4506TTranscript, "K1", CompareOptions.OrdinalIgnoreCase) >= 0)
            {
                isBusinessType = true;
                return E_VERIFICATIONATTACHMENTTYPE.K1;
            }

            if (application.a4506TTranscript.Contains("990"))
            {
                isBusinessType = true;
                return E_VERIFICATIONATTACHMENTTYPE.IRS990;
            }

            // Input did not match known good attachment types for 4506-T
            throw new CBaseException("Form number could not be determined. Please verify line 6 of the Request for Transcript page.", $"Form number text: {application.a4506TTranscript}");
        }
    }
}
