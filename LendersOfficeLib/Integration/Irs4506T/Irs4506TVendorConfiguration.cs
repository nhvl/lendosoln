﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using LqbGrammar;

namespace LendersOffice.Integration.Irs4506T
{
    public class Irs4506TVendorConfiguration
    {
        public Guid VendorId { get; protected set; }
        public string VendorName { get; set; }
        public bool IsValid { get; set; }
        public string ExportPath { get; set; }
        public bool IsUseAccountId { get; set; }
        public bool EnableConnectionTest { get; set; }
        public Irs4506TCommunicationModel CommunicationModel { get; set;}

        private Irs4506TVendorConfiguration(DbDataReader reader)
        {
            this.VendorId = (Guid)reader["VendorId"];
            this.VendorName = (string)reader["VendorName"];
            this.IsValid = (bool)reader["IsValid"];
            this.ExportPath = (string)reader["ExportPath"];
            this.IsUseAccountId = (bool)reader["IsUseAccountId"];
            this.EnableConnectionTest = (bool)reader["EnableConnectionTest"];
            this.CommunicationModel = (Irs4506TCommunicationModel)(byte)reader["CommunicationModel"];

            switch (this.CommunicationModel)
            {
                case Irs4506TCommunicationModel.EquifaxVerificationServices:
                    this.Server = new Equifax4506TServer(this);
                    break;
                case Irs4506TCommunicationModel.LendingQB:
                    this.Server = new Irs4506TServer(this);
                    break;
                default:
                    throw new UnhandledEnumException(this.CommunicationModel);
            }
        }

        public I4506TServer Server { get; set; }

        protected Irs4506TVendorConfiguration() { }

        public static IEnumerable<Irs4506TVendorConfiguration> ListActiveVendor()
        {
            List<Irs4506TVendorConfiguration> list = new List<Irs4506TVendorConfiguration>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "IRS_4506T_VENDOR_CONFIGURATION_ListActive"))
            {
                while (reader.Read())
                {
                    list.Add(new Irs4506TVendorConfiguration(reader));
                }
            }
            return list;
        }

        public static IEnumerable<Irs4506TVendorConfiguration> ListActiveVendorByBrokerId(Guid brokerId)
        {

            List<Irs4506TVendorConfiguration> list = new List<Irs4506TVendorConfiguration>();
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId)
                                        };

            HashSet<Guid> activeBrokerVendorSet = new HashSet<Guid>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "IRS_4506T_VENDOR_CONFIGURATION_ListActiveByBroker", parameters))
            {
                while (reader.Read())
                {
                    activeBrokerVendorSet.Add((Guid)reader["VendorId"]);

                }
            }

            foreach (var vendor in ListActiveVendor())
            {
                if (activeBrokerVendorSet.Contains(vendor.VendorId))
                {
                    list.Add(vendor);
                }
            }

            return list;

        }

        /// <summary>
        /// Gets the format which has been associated with a vendor and the Irs4506T order request, or returns null if no format is in effect. <para></para>
        /// TODO: Remove this method and replace it at the general vendor configuration level when we come to a later phase of opm 463659.
        /// </summary>
        /// <returns>The information or null if it's not enabled for any vendor.</returns>
        public static VendorSpecificIntegrationRequestFormat Temp_GetOnlyVendorIntegrationRequestFormat(IUserPrincipal principal)
        {
            if (principal == null)
            {
                throw new ArgumentNullException(nameof(principal));
            }

            var p = (AbstractUserPrincipal)principal;            

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "TEMP_GetOnlyVendorIntegrationRequestFormat"))
            {
                if (reader.Read())
                {
                    return new VendorSpecificIntegrationRequestFormat()
                    {
                        VendorId = (Guid)reader["VendorId"],
                        IntegrationRequestType = (IntegrationRequestType)(int)reader["IntegrationRequestType"],
                        LoXmlFileId = (int)reader["LoXmlFileId"],
                        XslFileId = (int)reader["XslFileId"],
                        LoXmlFileName = (string)reader["LoXmlFileName"],                        
                        XslFileName = (string)reader["XslFileName"]
                    };
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Removes the only vendor-specific integration request format. <para></para>
        /// TODO: Remove this method and replace it at the general vendor configuration level when we come to a later phase of opm 463659.
        /// </summary>
        public static void Temp_RemoveOnlyVendorIntegrationRequestFormat(AbstractUserPrincipal principal)
        {
            if (principal == null)
            {
                throw new ArgumentNullException(nameof(principal));
            }

            if (principal == null)
            {
                throw new ArgumentNullException(nameof(principal));
            }

            if (!(principal is InternalUserPrincipal || principal is SystemUserPrincipal))
            {
                throw new AccessDenied("You do not have access to remove integration request formats.");
            }

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShareROnly, "TEMP_RemoveOnlyVendorIntegrationRequestFormat", 0);
        }

        /// <summary>
        /// Sets the format to be associated with a vendor for Irs4506T order requests. <para></para>
        /// TODO: Remove this method and replace it at the general vendor configuration level when we come to a later phase of opm 463659.
        /// </summary>
        public static void Temp_SetOnlyVendorIntegrationRequestFormat(AbstractUserPrincipal principal, VendorSpecificIntegrationRequestFormat details)
        {
            if (principal == null)
            {
                throw new ArgumentNullException(nameof(principal));
            }

            if (details == null)
            {
                throw new ArgumentNullException(nameof(details));
            }

            if (!(principal is InternalUserPrincipal || principal is SystemUserPrincipal))
            {
                throw new AccessDenied("You do not have access to set integration request formats.");
            }
            
            var parameters = new List<SqlParameter>
            {
                new SqlParameter("@VendorId", System.Data.SqlDbType.UniqueIdentifier) {Value = details.VendorId },
                new SqlParameter("@IntegrationRequestType", System.Data.SqlDbType.Int) { Value = (int)details.IntegrationRequestType },
                new SqlParameter("@LoXmlFileId", System.Data.SqlDbType.Int) { Value = details.LoXmlFileId },
                new SqlParameter("@XslFileId", System.Data.SqlDbType.Int) { Value =  details.XslFileId }
            };
             
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShareROnly, "TEMP_SetOnlyVendorIntegrationRequestFormat", 0, parameters);
        }
        
        public static void AssociateVendorWithBrokerId(Guid brokerId, IEnumerable<Guid> vendorIdList)
        {

            using (CStoredProcedureExec spTransaction = new CStoredProcedureExec(brokerId))
            {
                spTransaction.BeginTransactionForWrite();
                try
                {
                    SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId)
                                        };

                    spTransaction.ExecuteNonQuery("IRS_4506T_VENDOR_BROKER_SETTINGS_RemoveAssociationByBrokerId",
                        parameters);

                    foreach (Guid vendorId in vendorIdList)
                    {
                        parameters = new SqlParameter[] {
                            new SqlParameter("@BrokerId", brokerId),
                            new SqlParameter("@VendorId", vendorId)
                        };
                        spTransaction.ExecuteNonQuery("IRS_4506T_VENDOR_BROKER_SETTINGS_AddAssociation",
                            parameters);
                    }
                    spTransaction.CommitTransaction();
                }
                catch
                {

                    spTransaction.RollbackTransaction();
                    throw;
                }
            }


        }

        public static void CreateNew(string vendorName, string exportPath, bool isUseAccountId, bool enableConnectionTest, Irs4506TCommunicationModel communicationModel)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@VendorName", vendorName),
                                            new SqlParameter("@ExportPath", exportPath),
                                            new SqlParameter("@IsUseAccountId", isUseAccountId),
                                            new SqlParameter("@IsValid", true),
                                            new SqlParameter("@EnableConnectionTest", enableConnectionTest),
                                            new SqlParameter("@CommunicationModel", communicationModel)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "IRS_4506T_VENDOR_CONFIGURATION_Create", 3, parameters);
        }

        public static Irs4506TVendorConfiguration RetrieveById(Guid vendorId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@VendorId", vendorId)
                                        };

            using (DbDataReader reader =
                StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "IRS_4506T_VENDOR_CONFIGURATION_RetrieveById", parameters))
            {
                if (reader.Read())
                {
                    return new Irs4506TVendorConfiguration(reader);
                }
            }
            throw new NotFoundException("4506-T Vendor is not found.", vendorId + " is not found.");
        }

        public void Save()
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@VendorId", VendorId),
                                            new SqlParameter("@VendorName", VendorName),
                                            new SqlParameter("@IsValid", IsValid),
                                            new SqlParameter("@ExportPath", ExportPath),
                                            new SqlParameter("@IsUseAccountId", IsUseAccountId),
                                            new SqlParameter("@EnableConnectionTest", EnableConnectionTest),
                                            new SqlParameter("@CommunicationModel", this.CommunicationModel)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "IRS_4506T_VENDOR_CONFIGURATION_Update", 3, parameters);
        }
    }
}
