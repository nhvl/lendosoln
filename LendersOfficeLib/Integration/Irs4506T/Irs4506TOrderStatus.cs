﻿namespace LendersOffice.Integration.Irs4506T
{
    using System;
    using OpenFinancialExchange;

    /// <summary>
    /// Represents the possible returned order statuses for IRS 4506-T orders.
    /// </summary>
    /// <remarks>
    /// Some values should be the same as <see cref="OFFLINESTATETYPE"/> values, for easy conversion.
    /// It would probably be nicer for <see cref="OFFLINESTATETYPE"/> to reference this type, but LendingQBInfrequentChangeLib should not have a reference to LendersOfficeLib.
    /// </remarks>
    public enum Irs4506TOrderStatus
    {
        /// <summary>
        /// Unknown status, for default cases where parsing fails.
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// LendingQB Ordered status.
        /// </summary>
        Ordered = 5,

        /// <summary>
        /// Equifax: The request is either waiting for E-signature or is submitted to the IRS and waiting for a response.
        ///     If the order is in this status, we need to query EVS to check for an update of the status. 
        /// </summary>
        Pending = OFFLINESTATETYPE.PENDING,

        /// <summary>
        /// Equifax: The submission did not pass EVS Quality Control checks.
        /// LQB: The order was cancelled by the vendor.
        /// </summary>
        Cancelled = OFFLINESTATETYPE.CANCELLED,

        /// <summary>
        /// LQB: The vendor is taking additional time to process the order.
        /// </summary>
        OnHold = 6,

        /// <summary>
        /// Equifax: The IRS returned errors.
        /// </summary>
        Rejected = OFFLINESTATETYPE.REJECTED,

        /// <summary>
        /// Equifax: The order completed successfully.
        /// LQB: The order completed successfully.
        /// </summary>
        Completed = OFFLINESTATETYPE.COMPLETE,

        /// <summary>
        /// Some error status not covered by the above.
        /// </summary>
        Error = 7
    }
}
