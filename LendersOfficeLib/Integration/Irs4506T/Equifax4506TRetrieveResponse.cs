﻿namespace LendersOffice.Integration.Irs4506T
{
    using System;
    using System.IO;
    using System.Xml;
    using Common;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.HttpRequest;
    using OpenFinancialExchange;

    /// <summary>
    /// Represents a response from Equifax to a 4506-T order retrieve request.
    /// </summary>
    public class Equifax4506TRetrieveResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Equifax4506TRetrieveResponse"/> class.
        /// </summary>
        /// <param name="xmlString">The xml string to parse while initializing the object.</param>
        /// <param name="base64Pdf">The base64-Encoded PDF document from the server response.</param>
        /// <param name="pdfFileName">The filename for the returned base64 PDF.</param>
        private Equifax4506TRetrieveResponse(string xmlString, string base64Pdf, string pdfFileName)
        {
            this.XmlContent = xmlString;

            using (TextReader tr = new StringReader(xmlString))
            using (XmlReader xr = XmlReader.Create(tr))
            {
                this.OfxContent = new OFX();
                this.OfxContent.ReadXml(xr);
            }

            this.ServerId = this.OfxContent.EIVVERMSGSRSV1.EIVTAXTRANSCRIPTTRNRS?.EIVTAXTRANSCRIPTRS?.SRVRTID;
            this.Base64TranscriptDocument = base64Pdf;
            this.DocumentFileName = pdfFileName;
            this.Status = this.OfxContent.EIVVERMSGSRSV1.EIVTAXTRANSCRIPTTRNRS.STATUS.CODE;
            this.StatusMessage = this.OfxContent.EIVVERMSGSRSV1.EIVTAXTRANSCRIPTTRNRS.STATUS.MESSAGE;
        }

        /// <summary>
        /// Gets or sets the status returned from Equifax.
        /// </summary>
        /// <value>Status type.</value>
        public E_STATUSCODE Status { get; set; }

        /// <summary>
        /// Gets or sets the status message accompanying the status from Equifax.
        /// </summary>
        /// <value>Status message.</value>
        public string StatusMessage { get; set; }

        /// <summary>
        /// Gets or sets the unparsed OFX XML returned from Equifax.
        /// </summary>
        /// <value>Unparsed XML content.</value>
        public string XmlContent { get; set; }

        /// <summary>
        /// Gets or sets the parsed OFX content returned from Equifax.
        /// </summary>
        /// <value>OFX object content.</value>
        public OFX OfxContent { get; set; }

        /// <summary>
        /// Gets or sets the Equifax server ID for the order.
        /// </summary>
        /// <value>Server ID.</value>
        public string ServerId { get; set; }

        /// <summary>
        /// Gets or sets the base-64 encoded transcript PDF content returned from Equifax.
        /// </summary>
        /// <value>Base64 document.</value>
        public string Base64TranscriptDocument { get; set; }

        /// <summary>
        /// Gets or sets the specified file name for <see cref="Base64TranscriptDocument"/>.
        /// </summary>
        /// <value>Document file name.</value>
        public string DocumentFileName { get; set; }

        /// <summary>
        /// Populates a new instance of the <see cref="Equifax4506TRetrieveResponse"/> object 
        /// with parsed content from the response fields of the given <see cref="HttpRequestOptions"/> object.
        /// </summary>
        /// <param name="response">The HttpRequestOptions object with HTTP response data.</param>
        /// <returns>A new <see cref="Equifax4506TRetrieveResponse"/>. If the response data could not be parsed, returns null.</returns>
        public static Equifax4506TRetrieveResponse FromHttpResponse(HttpRequestOptions response)
        {
            MimeMultipartContent contentParts = Equifax4506TServer.ResponseToMultipart(response);

            if (contentParts != null)
            {
                string xmlBody = contentParts.Parts[0].Content;

                if (contentParts.Parts.Count >= 2)
                {
                    return new Equifax4506TRetrieveResponse(xmlBody, contentParts.Parts[1]?.Content, contentParts.Parts[1]?.Headers["Content-Location"]);
                }
                else
                {
                    return new Equifax4506TRetrieveResponse(xmlBody, null, null);
                }
            }

            return null;
        }
    }
}
