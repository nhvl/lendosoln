﻿namespace LendersOffice.Integration.Irs4506T
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common;
    using Constants;
    using DataAccess;
    using Drivers.HttpRequest;
    using EDocs;
    using LqbGrammar;
    using LqbGrammar.Commands;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.HttpRequest;
    using LqbGrammar.Exceptions;
    using ObjLib.Edocs.AutoSaveDocType;
    using ObjLib.Security.ThirdParty;
    using OpenFinancialExchange;

    /// <summary>
    /// This class handles the organization and execution of data transactions with Equifax Verification Services Tax Return Verification system.
    /// </summary>
    public class Equifax4506TServer : I4506TServer
    {
        /// <summary>
        /// This should be appended to the user's ID for TRV orders.
        /// </summary>
        public static readonly string UserIdSuffix = "@50005";

        /// <summary>
        /// This is the vendor ID assigned to LendingQB by Equifax.
        /// </summary>
        public static readonly string LqbVendorId = "9k";

        /// <summary>
        /// The boundary used to delimit parts in Equifax's MIME multipart content transmissions.
        /// </summary>
        public static readonly string EquifaxMultipartBoundary = "--=C642062B70FB498299F0F50D2A190B3C";

        /// <summary>
        /// The vendor which this server is targeting.
        /// </summary>
        private Irs4506TVendorConfiguration vendorConfig;

        /// <summary>
        /// Initializes a new instance of the <see cref="Equifax4506TServer"/> class.
        /// </summary>
        /// <param name="vendor">The vendor that the created server will target.</param>
        public Equifax4506TServer(Irs4506TVendorConfiguration vendor)
        {
            if (vendor.CommunicationModel != Irs4506TCommunicationModel.EquifaxVerificationServices)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            this.vendorConfig = vendor;
        }

        /// <summary>
        /// Converts the response part of an HttpRequestOptions that has received a response into a <see cref="MimeMultipartContent"/> instance.
        /// </summary>
        /// <param name="response">The <see cref="HttpRequestOptions"/> to convert.</param>
        /// <returns>If the HTTP response was able to be converted, the multipart content is returned. Otherwise, null.</returns>
        public static MimeMultipartContent ResponseToMultipart(HttpRequestOptions response)
        {
            if (response?.ResponseHeaders == null || !response.ResponseHeaders.AllKeys.Contains("Content-Type"))
            {
                return null;
            }

            if (response.ResponseHeaders["Content-Type"].StartsWith("multipart", StringComparison.OrdinalIgnoreCase))
            {
                return MimeMultipartContent.Parse(response.ResponseBody, MimeMultipartContent.ExtractBoundary(response.ResponseHeaders["Content-Type"]) ?? EquifaxMultipartBoundary);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Submits a new order to Equifax with an attached 4506-T consent PDF.
        /// </summary>
        /// <param name="loanId">The ID for the active loan data object that 4506-T order data should be retrieved from.</param>
        /// <param name="appId">The ID of the active borrower application for which tax return data is being requested.</param>
        /// <param name="borrowerMode">The borrower mode touse for the application data to get the proper borrower fields.</param>
        /// <param name="document">The document corresponding to the signed borrower 4506-T consent form.</param>
        /// <param name="user">The user making the order submission request.</param>
        /// <param name="credential">The credential to use for authenticating with the vendor. If null, the credential will be loaded for the user.</param>
        /// <param name="applicationName">The application name selected in the dropdown for our reference.  The application name depends on their tax filing mode.</param>
        /// <returns>A string representing messages from the submission of the request (errors or order numbers).</returns>
        public Equifax4506TStatus<bool> SubmitNewOrder(Guid loanId, Guid appId, E_BorrowerModeT borrowerMode, EDocument document, IUserPrincipal user, Irs4506TVendorCredential credential, string applicationName)
        {
            var loanData = CPageData.CreateUsingSmartDependency(loanId, this.GetType());
            loanData.InitLoad();
            if (loanData.sBrokerId != user.BrokerId)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            CAppData appData = loanData.GetAppData(appId);
            if (appData == null)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"LoanId:{loanId} and AppId:{appId} don't match."));
            }

            appData.BorrowerModeT = borrowerMode;
            return this.SubmitNewOrder(loanData, appData, document, user, credential, applicationName);
        }

        /// <summary>
        /// Checks the status of an existing 4506-T order with Equifax.
        /// </summary>
        /// <param name="loanId">The ID for the active loan data object that 4506-T order data should be retrieved from.</param>
        /// <param name="order">The order to check the status of.</param>
        /// <param name="user">The user making the order submission request.</param>
        /// <param name="credential">The credential to use for authenticating with the vendor. If null, the credential will be loaded for the user.</param>
        /// <returns>An object representing status and messages from the submission of the request (errors or order numbers).</returns>
        public Equifax4506TStatus<Irs4506TOrderStatus> CheckOrderStatus(Guid loanId, Irs4506TOrderInfo order, IUserPrincipal user, Irs4506TVendorCredential credential = null)
        {
            var loanData = CPageData.CreateUsingSmartDependency(loanId, this.GetType());
            loanData.InitLoad();
            if (loanData.sBrokerId != user.BrokerId)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            Irs4506TVendorConfiguration vendorConfig = Irs4506TVendorConfiguration.RetrieveById(order.VendorId);
            if (vendorConfig.CommunicationModel != Irs4506TCommunicationModel.EquifaxVerificationServices)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            return this.CheckOrderStatus(loanData, order, user, credential);
        }

        /// <summary>
        /// Checks the status of an existing 4506-T order with Equifax.
        /// </summary>
        /// <param name="loanData">The loan data object to load information from.</param>
        /// <param name="order">The order in our system.</param>
        /// <param name="user">The user making the request.</param>
        /// <param name="credential">The credential to use for authenticating with the vendor. If null, the credential will be loaded for the user.</param>
        /// <returns>An object representing status and messages from the submission of the request (errors or order numbers).</returns>
        public Equifax4506TStatus<Irs4506TOrderStatus> CheckOrderStatus(CPageData loanData, Irs4506TOrderInfo order, IUserPrincipal user, Irs4506TVendorCredential credential)
        {
            if (credential == null)
            {
                credential = Irs4506TVendorCredential.Retrieve(user.BrokerId, user.UserId, this.vendorConfig.VendorId);
            }

            // If still no credential after loading, Return an error status.
            if (string.IsNullOrEmpty(credential.DecryptedPassword) || string.IsNullOrEmpty(credential.UserName))
            {
                return new Equifax4506TStatus<Irs4506TOrderStatus>
                {
                    Status = Irs4506TOrderStatus.Error,
                    Messages = new List<string>
                    {
                        $"You don't have a saved credential for {this.vendorConfig.VendorName}."
                    }
                };
            }

            Equifax4506TStatusRequest request = new Equifax4506TStatusRequest(credential, loanData, order.OrderNumber);
            HttpRequestOptions requestOptions = request.GetHttpRequestOptions(forLogging: false);
            var uri = LqbAbsoluteUri.Create(this.vendorConfig.ExportPath).Value;
            requestOptions.ClientCertificates = ThirdPartyClientCertificateManager.RetrieveCertificate(uri);
            Tools.LogInfo($"Equifax 4506-T Order Status Request: {Environment.NewLine}{request.ToMultipartContent(forLogging: true).ToString()}");

            if (WebRequestHelper.ExecuteCommunication(LqbAbsoluteUri.Create(this.vendorConfig.ExportPath).Value, requestOptions))
            {
                var response = Equifax4506TStatusResponse.FromHttpResponse(requestOptions);
                Tools.LogInfo($"Equifax 4506-T Check Status Response XML: {Environment.NewLine}{Equifax4506THelper.MaskOfxXmlForLogging(response.XmlContent)}");
                if (response.SignOnSucceeded && response.OrderStatus == OFFLINESTATETYPE.COMPLETE)
                {
                    // If completed, automatically retrieve completed transcript from server.
                    IStatusDetails<bool, string> retrieveStatus = this.RetrieveOrder(loanData, order, user, credential);

                    if (retrieveStatus.Status == true)
                    {
                        return new Equifax4506TStatus<Irs4506TOrderStatus>
                        {
                            Status = (Irs4506TOrderStatus)response.OrderStatus,
                            Messages = new List<string>
                            {
                            }
                        };
                    }
                    else
                    {
                        order.UpdateResponse((Irs4506TOrderStatus)response.OrderStatus, retrieveStatus.Messages[0] ?? string.Empty, response.XmlContent);

                        return new Equifax4506TStatus<Irs4506TOrderStatus>
                        {
                            Status = (Irs4506TOrderStatus)response.OrderStatus,
                            Messages = retrieveStatus.Messages
                        };
                    }
                }
                else
                {
                    order.UpdateResponse(response.OrderStatus == OFFLINESTATETYPE.Unknown ? order.StatusT : (Irs4506TOrderStatus)response.OrderStatus, response.StatusMessage ?? string.Empty, response.XmlContent);

                    return new Equifax4506TStatus<Irs4506TOrderStatus>
                    {
                        Status = (Irs4506TOrderStatus)response.OrderStatus,
                        Messages = new List<string>
                        {
                            response.StatusMessage
                        }
                    };
                }
            }
            else
            {
                return new Equifax4506TStatus<Irs4506TOrderStatus>
                {
                    Status = Irs4506TOrderStatus.Unknown,
                    Messages = new List<string>
                    {
                        requestOptions.ResponseBody
                    }
                };
            }
        }

        /// <summary>
        /// Retrieve a completed order from the Equifax server.
        /// </summary>
        /// <param name="loanId">The ID of the loan associated with the order.</param>
        /// <param name="order">The order to retrieve transcript documents for.</param>
        /// <param name="user">The user requesting the update.</param>
        /// <param name="credential">The credential to use for authenticating with the vendor. If null, the credential will be loaded for the user.</param>
        /// <returns>A status details container with whether the submission succeeded, and any error messages.</returns>
        public Equifax4506TStatus<bool> RetrieveOrder(Guid loanId, Irs4506TOrderInfo order, IUserPrincipal user, Irs4506TVendorCredential credential)
        {
            var loanData = CPageData.CreateUsingSmartDependency(loanId, this.GetType());
            loanData.InitLoad();
            if (loanData.sBrokerId != user.BrokerId)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            Irs4506TVendorConfiguration orderVendor = Irs4506TVendorConfiguration.RetrieveById(order.VendorId);
            if (orderVendor.VendorId != this.vendorConfig.VendorId || this.vendorConfig.CommunicationModel != Irs4506TCommunicationModel.EquifaxVerificationServices)
            {
                throw new DeveloperException(ErrorMessage.SystemError);
            }

            return this.RetrieveOrder(loanData, order, user, credential);
        }

        /// <summary>
        /// Retrieve a completed order from the Equifax server.
        /// </summary>
        /// <param name="loanData">The loan associated with the order.</param>
        /// <param name="order">The order to retrieve transcript documents for.</param>
        /// <param name="user">The user making the request.</param>
        /// <param name="credential">The credential to use for authenticating with the vendor. If null, the credential will be loaded for the user.</param>
        /// <returns>Debug information.</returns>
        public Equifax4506TStatus<bool> RetrieveOrder(CPageData loanData, Irs4506TOrderInfo order, IUserPrincipal user, Irs4506TVendorCredential credential)
        {
            if (credential == null)
            {
                credential = Irs4506TVendorCredential.Retrieve(user.BrokerId, user.UserId, this.vendorConfig.VendorId);
            }

            Equifax4506TRetrieveRequest request = new Equifax4506TRetrieveRequest(credential, loanData, order.OrderNumber);
            var requestOptions = request.GetHttpRequestOptions(forLogging: false);
            var uri = LqbAbsoluteUri.Create(this.vendorConfig.ExportPath).Value;
            requestOptions.ClientCertificates = ThirdPartyClientCertificateManager.RetrieveCertificate(uri);
            Tools.LogInfo($"Equifax 4506-T Retrieve Order Request: {Environment.NewLine}{request.ToMultipartContent(forLogging: true).ToString()}");

            if (WebRequestHelper.ExecuteCommunication(LqbAbsoluteUri.Create(this.vendorConfig.ExportPath).Value, requestOptions))
            {
                var response = Equifax4506TRetrieveResponse.FromHttpResponse(requestOptions);
                Tools.LogInfo($"Equifax 4506-T Retrieve Order Response XML: {Environment.NewLine}{Equifax4506THelper.MaskOfxXmlForLogging(response.XmlContent)}");
                if (response.Status == E_STATUSCODE.Success && !string.IsNullOrEmpty(response.Base64TranscriptDocument))
                {
                    Guid docId = SaveRetrievedTranscript(order, response);

                    // Success: Status = true
                    return new Equifax4506TStatus<bool>()
                    {
                        Status = true,
                        Messages = new List<string>
                        {
                            docId.ToString()
                        }
                    };
                }
                else
                {
                    // Error: Status = false
                    return new Equifax4506TStatus<bool>()
                    {
                        Status = false,
                        Messages = new List<string>
                        {
                            response.StatusMessage
                        }
                    };
                }
            }

            return new Equifax4506TStatus<bool>()
            {
                // Success: true
                Status = false,
                Messages = new List<string>
                        {
                            "Transcript retrieval failed."
                        }
            };
        }

        /// <summary>
        /// Saves the retrieved transcript from Equifax back to the database.
        /// </summary>
        /// <param name="order">The order object the document will be saved to.</param>
        /// <param name="response">Represents Equifax's response to the retrieval request, including the base64 pdf.</param>
        /// <returns>The new DocumentID for the saved transcript document.</returns>
        private static Guid SaveRetrievedTranscript(Irs4506TOrderInfo order, Equifax4506TRetrieveResponse response)
        {
            var file = new LQB4506T.LQB4506TPost.EmbeddedFile()
            {
                Name = response.DocumentFileName,
                Type = "pdf",
                EncodedDoc = response.Base64TranscriptDocument,
                EncodingType = LQB4506T.LQB4506TPost.E_EmbeddedFileEncodingType.Base64
            };

            Guid docId = Guid.Empty;
            var document = order.AddDocument(file);
            if (!string.IsNullOrEmpty(document.FileDBKey))
            {
                docId = AutoSaveDocTypeFactory.SavePdfDocAsSystem(E_AutoSavePage.Irs4506TDocuments, document.FileDBKey, E_FileDB.Normal, order.sBrokerId, order.sLId, order.aAppId, document.DocumentName);

                // If auto-saving the doc failed, undo changes to the order.
                if (docId == Guid.Empty)
                {
                    order.UploadedFiles.Remove(document);
                }

                order.UpdateResponse(Irs4506TOrderStatus.Completed, response.StatusMessage ?? string.Empty, response.XmlContent);
            }

            return docId;
        }

        /// <summary>
        /// Submits a new order to Equifax with an attached 4506-T consent PDF.
        /// </summary>
        /// <param name="loanData">The loan data object to associate with the order.</param>
        /// <param name="appData">The app data object to associate with the order.</param>
        /// <param name="document">The EDocument to load the pdf data from.</param>
        /// <param name="user">The user requesting the order submission.</param>
        /// <param name="credential">The 4506-T credential to use for authenticating with the vendor. If not supplied, one will be loaded from the database.</param>
        /// <param name="applicationName">The application name selected in the dropdown for our reference.  The application name depends on their tax filing mode.</param>
        /// <returns>A status details container with whether the submission succeeded, and any error messages.</returns>
        private Equifax4506TStatus<bool> SubmitNewOrder(CPageData loanData, CAppData appData, EDocument document, IUserPrincipal user, Irs4506TVendorCredential credential, string applicationName)
        {
            // If not supplied, load the credentials on file for the user.
            if (credential == null)
            {
                credential = Irs4506TVendorCredential.Retrieve(user.BrokerId, user.UserId, this.vendorConfig.VendorId);
            }

            Equifax4506TSubmitOrderRequest request = new Equifax4506TSubmitOrderRequest(credential, loanData, appData, document);
            HttpRequestOptions requestOptions = request.GetHttpRequestOptions(forLogging: false);
            var uri = LqbAbsoluteUri.Create(this.vendorConfig.ExportPath).Value;
            requestOptions.ClientCertificates = ThirdPartyClientCertificateManager.RetrieveCertificate(uri);
            Tools.LogInfo($"Equifax 4506-T New Order Request: {Environment.NewLine}{request.ToMultipartContent(true).ToString()}");
            if (WebRequestHelper.ExecuteCommunication(LqbAbsoluteUri.Create(this.vendorConfig.ExportPath).Value, requestOptions))
            {
                var response = Equifax4506TSubmitOrderResponse.FromHttpResponse(requestOptions);
                Tools.LogInfo($"Equifax 4506-T New Order Response XML: {Environment.NewLine}{Equifax4506THelper.MaskOfxXmlForLogging(response.XmlContent)}");

                if (response.Status == E_STATUSCODE.Success && !string.IsNullOrEmpty(response.ServerId))
                {
                    Irs4506TOrderInfo.InsertOrder(
                        brokerId: user.BrokerId,
                        transactionId: Guid.NewGuid(),
                        sLId: loanData.sLId,
                        aAppId: appData.aAppId,
                        vendorId: this.vendorConfig.VendorId,
                        applicationName: applicationName,
                        orderNumber: response.ServerId,
                        status: "Successful submission.",
                        statusT: Irs4506TOrderStatus.Pending);

                    // Assume pending status, since we got a successful response and haven't checked for status yet.
                    return new Equifax4506TStatus<bool>
                    {
                        Status = true,
                        Messages = new List<string> { response.ServerId }
                    };
                }
                else
                {
                    return new Equifax4506TStatus<bool>
                    {
                        Status = false,
                        Messages = new List<string> { response.Message }
                    };
                }
            }
            else
            {
                return new Equifax4506TStatus<bool>
                {
                    Status = false,
                    Messages = new List<string> { "There was a communication error.", requestOptions.ResponseBody }
                };
            }
        }
    }
}
