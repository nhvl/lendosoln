﻿namespace LendersOffice.Integration.Irs4506T
{
    using System.Collections.Generic;
    using Common;
    using DataAccess;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.HttpRequest;
    using OpenFinancialExchange;
    using OpenFinancialExchange.Request;

    /// <summary>
    /// A request to get the 4506-T order result from Equifax.
    /// </summary>
    public class Equifax4506TRetrieveRequest
    {
        /// <summary>
        /// The vendor credential to authenticate with Equifax.
        /// </summary>
        private Irs4506TVendorCredential credential;

        /// <summary>
        /// The loan data object to get data from.
        /// </summary>
        private CPageData loanData;

        /// <summary>
        /// The transaction ID that Equifax uses to identify the order.
        /// </summary>
        private string serverTransactionId;

        /// <summary>
        /// Initializes a new instance of the <see cref="Equifax4506TRetrieveRequest"/> class.
        /// </summary>
        /// <param name="credential">The vendor credential to authenticate with Equifax.</param>
        /// <param name="loanData">The loan data object to get data from.</param>
        /// <param name="serverTransactionId">The transaction ID that Equifax uses to identify the order.</param>
        public Equifax4506TRetrieveRequest(Irs4506TVendorCredential credential, CPageData loanData, string serverTransactionId)
        {
            this.credential = credential;
            this.loanData = loanData;
            this.serverTransactionId = serverTransactionId;
        }

        /// <summary>
        /// Gets the OFX xml to send, containing information about the retrieval request.
        /// </summary>
        /// <returns>The generated Open Financial Exchange Metadata for this request.</returns>
        public OFX BuildOpenFinancialExchangeMetadata()
        {
            return Equifax4506THelper.BuildOfxRequest(
                   this.credential,
                   new EIVVERMSGSRQV1
                   {
                       EIVRETRIEVETAXTRANSCRIPTRESULTTRNRQ = new EIVRETRIEVETAXTRANSCRIPTRESULTTRNRQ
                       {
                           TRNUID = this.loanData.sLenderCaseNum,
                           CLTCOOKIE = this.loanData.sLRefNm,
                           EIVRETRIEVETAXTRANSCRIPTRESULTRQ = new EIVRETRIEVETAXTRANSCRIPTRESULTRQ
                           {
                               COMPLETEDREQUESTSONLY = true,
                               SRVRTID = this.serverTransactionId,
                           }
                       }
                   });
        }

        /// <summary>
        /// Converts this request into a <see cref="HttpRequestOptions"/> object ready to send to Equifax.
        /// </summary>
        /// <param name="forLogging">Whether the body is being created for logging purposes and sensitive or large data should be removed.</param>
        /// <returns>The corresponding options object ready to send.</returns>
        public HttpRequestOptions GetHttpRequestOptions(bool forLogging)
        {
            var postData = this.ToMultipartContent(forLogging);

            return new HttpRequestOptions
            {
                PostData = new StringContent(postData.ToString()),
                MimeType = MimeType.MultiPart.CreateRelated(postData.Boundary, type: "application/x-ofx", capitalize: true),
                Method = HttpMethod.Post
            };
        }

        /// <summary>
        /// Converts this object into a MIME multipart content body for adding to an Http request.
        /// </summary>
        /// <param name="forLogging">Whether the body is being created for logging purposes and sensitive or large data should be removed.</param>
        /// <returns>This object as a MIME multipart content object.</returns>
        public MimeMultipartContent ToMultipartContent(bool forLogging)
        {
            MimeMultipartContent content = new MimeMultipartContent
            {
                Boundary = Equifax4506TServer.EquifaxMultipartBoundary
            };
            content.Parts.Add(
                new MultipartPart
                {
                    Headers = new Dictionary<string, string>
                    {
                        { "Content-Type", "application/x-ofx" }
                    },
                    Content = forLogging ? Equifax4506THelper.MaskOfxXmlForLogging(this.BuildOpenFinancialExchangeMetadata().ToString()) : this.BuildOpenFinancialExchangeMetadata().ToString()
                });
            return content;
        }
    }
}
