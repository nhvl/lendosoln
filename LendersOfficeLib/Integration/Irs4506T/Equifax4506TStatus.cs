﻿namespace LendersOffice.Integration.Irs4506T
{
    using System.Collections.Generic;
    using LqbGrammar.Commands;
    using OpenFinancialExchange;

    /// <summary>
    /// A simple container for status information returned from Equifax about the status of a 4506-T order.
    /// </summary>
    /// <typeparam name="TStatus">A representation of the status type returned from Equifax.</typeparam>
    public class Equifax4506TStatus<TStatus> : IStatusDetails<TStatus, string>
    {
        /// <summary>
        /// Gets or sets the state type representing the status returned by Equifax for the 4506-T order.
        /// </summary>
        /// <value>Status type.</value>
        public TStatus Status
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the message(s) returned by Equifax about the 4506-T order status.
        /// </summary>
        /// <value>Status messages.</value>
        public IList<string> Messages
        {
            get; set;
        }
    }
}
