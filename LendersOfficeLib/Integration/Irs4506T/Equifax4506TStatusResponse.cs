﻿namespace LendersOffice.Integration.Irs4506T
{
    using System;
    using System.IO;
    using System.Xml;
    using Common;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.HttpRequest;
    using OpenFinancialExchange;

    /// <summary>
    /// Represents a response from Equifax in response to a 4506-T status request.
    /// </summary>
    public class Equifax4506TStatusResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Equifax4506TStatusResponse"/> class.
        /// </summary>
        /// <param name="xmlString">The xml string to parse while initializing the object.</param>
        private Equifax4506TStatusResponse(string xmlString)
        {
            this.XmlContent = xmlString;

            using (TextReader tr = new StringReader(xmlString))
            using (XmlReader xr = XmlReader.Create(tr))
            {
                this.OfxContent = new OFX();
                this.OfxContent.ReadXml(xr);
            }

            if (this.OfxContent.SIGNONMSGSRSV1.SONRS.STATUS.CODE == E_STATUSCODE.Success)
            {
                this.SignOnSucceeded = true;
                this.ServerId = this.OfxContent.EIVVERMSGSRSV1.EIVTAXTRANSCRIPTTRNRS?.EIVTAXTRANSCRIPTRS?.SRVRTID;
                this.OrderStatus = this.OfxContent.EIVVERMSGSRSV1.EIVOFFLINEVERIFICATIONSTATUSTRNRS.EIVOFFLINEVERIFICATIONSTATUSRS.OFFLINETAXTRANSCRIPTINFO.OFFLINEVERIFICATIONSTATE.OFFLINESTATETYPE;
                this.StatusMessage = this.OfxContent.EIVVERMSGSRSV1.EIVOFFLINEVERIFICATIONSTATUSTRNRS.EIVOFFLINEVERIFICATIONSTATUSRS.OFFLINETAXTRANSCRIPTINFO.OFFLINEVERIFICATIONSTATE.OFFLINESTATEMESSAGE;
            }
            else
            {
                this.SignOnSucceeded = false;
                this.OrderStatus = OFFLINESTATETYPE.Unknown;
                this.StatusMessage = $"CODE {this.OfxContent.SIGNONMSGSRSV1.SONRS.STATUS.CODE:D} ({this.OfxContent.SIGNONMSGSRSV1.SONRS.STATUS.SEVERITY}): {this.OfxContent.SIGNONMSGSRSV1.SONRS.STATUS.MESSAGE}";
            }
        }

        /// <summary>
        /// Gets or sets the unparsed OFX XML returned from Equifax.
        /// </summary>
        /// <value>Unparsed XML content.</value>
        public string XmlContent { get; set; }

        /// <summary>
        /// Gets or sets the parsed OFX content returned from Equifax.
        /// </summary>
        /// <value>OFX object content.</value>
        public OFX OfxContent { get; set; }

        /// <summary>
        /// Gets or sets the Equifax server ID for the order.
        /// </summary>
        /// <value>Server ID.</value>
        public string ServerId { get; set; }

        /// <summary>
        /// Gets or sets the status of the order with Equifax.
        /// </summary>
        /// <value>Status type.</value>
        public OFFLINESTATETYPE OrderStatus { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the sign-on request succeeded.
        /// </summary>
        /// <value>Whether the sign-on request succeeded.</value>
        public bool SignOnSucceeded { get; set; }

        /// <summary>
        /// Gets or sets the message accompanying the <see cref="OrderStatus"/>.
        /// </summary>
        /// <value>Status msg.</value>
        public string StatusMessage { get; set; }

        /// <summary>
        /// Populates a new instance of the <see cref="Equifax4506TStatusResponse"/> object 
        /// with parsed content from the response fields of the given <see cref="HttpRequestOptions"/> object.
        /// </summary>
        /// <param name="response">The HttpRequestOptions object with HTTP response data.</param>
        /// <returns>A new <see cref="Equifax4506TStatusResponse"/>. If the response data could not be parsed, returns null.</returns>
        public static Equifax4506TStatusResponse FromHttpResponse(HttpRequestOptions response)
        {
            MimeMultipartContent contentParts = Equifax4506TServer.ResponseToMultipart(response);

            if (contentParts != null)
            {
                string xmlBody = contentParts.Parts[0].Content;
                return new Equifax4506TStatusResponse(xmlBody);
            }

            if (response.ResponseHeaders["Content-Type"].StartsWith("application/x-ofx", StringComparison.OrdinalIgnoreCase))
            {
                return new Equifax4506TStatusResponse(response.ResponseBody);
            }

            return null;
        }
    }
}
