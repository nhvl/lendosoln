﻿namespace LendersOffice.Integration.Irs4506T
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml.Linq;
    using System.Xml.XPath;
    using Common;
    using OpenFinancialExchange;
    using OpenFinancialExchange.Request;

    /// <summary>
    /// Defines methods to help communicating with Equifax's TRV API.
    /// </summary>
    public static class Equifax4506THelper
    {
        /// <summary>
        /// Mask to replace SSN with for logging.
        /// </summary>
        private const string SensitiveDataMask = "****";

        /// <summary>
        /// Builds the base of the OFX for a TRV request, with the sign-on element.
        /// </summary>
        /// <param name="credential">The 4506-T credential to use.</param>
        /// <param name="requestSpecificData">The request-specific OFX part (EIVVERMSGSRQV1) to insert.</param>
        /// <returns>An OFX instance ready to serialize to XML.</returns>
        public static OFX BuildOfxRequest(Irs4506TVendorCredential credential, EIVVERMSGSRQV1 requestSpecificData)
        {
            DateTime requestDateTime = DateTime.Now;
            return new OFX
            {
                SIGNONMSGSRQV1 = new SIGNONMSGSRQV1
                {
                    SONRQ = new SONRQ
                    {
                        DTCLIENT = requestDateTime.ToString("yyyyMMddHHmmss"),
                        USERID = credential.UserName + Equifax4506TServer.UserIdSuffix,
                        USERPASS = credential.DecryptedPassword,
                        LANGUAGE = "ENG",
                        APPID = "LENDINGQB",
                        APPVER = requestDateTime.ToString("yyyy")
                    }
                },
                EIVVERMSGSRQV1 = requestSpecificData
            };
        }

        /// <summary>
        /// Masks sensitive data points in the given OFX XML text for logging. Also removes the tax transcript response if it is too long.
        /// </summary>
        /// <param name="ofxXml">The OFX Xml string to mask.</param>
        /// <returns>A string with sensitive data removed, suitable for logging.</returns>
        public static string MaskOfxXmlForLogging(string ofxXml)
        {
            XDocument doc = XDocument.Parse(ofxXml);
            XElement root = doc.Root;

            MaskXPath(root, "//USERPASS");
            MaskXPath(root, "//EIVEMPLOYEEID");
            MaskXPath(root, "//SSNPROVIDED");
            MaskXPath(root, "//EINPROVIDED");
            MaskXPath(root, "//EMPLOYEESSN");
            MaskXPath(root, "//RECIPIENTIDENTIFICATIONNUMBER");
            MaskXPath(root, "//FEDEIN");
            MaskXPath(root, "//PERSONALIDENTIFIER");

            IEnumerable<XElement> formElements = root.XPathSelectElements("EIVVERMSGSRSV1/EIVRETRIEVETAXTRANSCRIPTRESULTTRNRS/EIVRETRIEVETAXTRANSCRIPTRESULTRS/EIVTAXTRANSCRIPTS/EIVTAXTRANSCRIPT_V100/FORM");

            foreach (XElement formElement in formElements)
            {
                if (formElement != null)
                {
                    formElement.ReplaceNodes($"[FORM contents have been omitted from logging.]");
                }
            }

            return doc.Declaration.ToString() + doc.ToString(SaveOptions.DisableFormatting);
        }

        /// <summary>
        /// Masks the value of all elements within the XElement which are found at the given XPath.
        /// </summary>
        /// <param name="root">The context or root for the XPath expression.</param>
        /// <param name="xpath">The XPath expression describing the elements whose values should be masked.</param>
        private static void MaskXPath(XElement root, string xpath)
        {
            if (root == null || xpath == null)
            {
                return;
            }

            IEnumerable<XElement> elements = root.XPathSelectElements(xpath);
            foreach (XElement element in elements)
            {
                if (element != null)
                {
                    element.Value = SensitiveDataMask;
                }
            }
        }
    }
}
