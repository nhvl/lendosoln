﻿namespace LendersOffice.Integration.Irs4506T
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Xml;
    using DataAccess;
    using DataAccess.FannieMae;
    using EDocs;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Edocs.AutoSaveDocType;
    using LQB4506T.LQB4506TPost;
    using LQB4506T.LQB4506TPostResponse;

    public class Irs4506TPostHandler : IHttpHandler
    {
        private StringBuilder m_log;
        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            m_log = new StringBuilder();
            string rawXml = string.Empty;
            Lqb4506TPostResponse lqb4506TPostResponse = null;
            try
            {
                Lqb4506TPost lqb4506TPost = ParseFromRequest(context.Request, out rawXml);

                lqb4506TPostResponse = Process(lqb4506TPost, rawXml);
                m_log.AppendLine("[4506TPostHandler]");
                m_log.AppendLine();
                Log(lqb4506TPost); //Only log once Base64 is removed.
            }
            catch (Exception exc)
            {
                lqb4506TPostResponse = CreateErrorResponse(lqb4506TPostResponse, exc);
                throw;
            }
            finally
            {
                SendResponse(context.Response, lqb4506TPostResponse);
                if (!string.IsNullOrEmpty(rawXml))
                {
                    if (m_log.Length == 0)
                    {
                        m_log.AppendLine("[4506TPostHandler] - Failed To Log LQB4506TPost");
                        m_log.AppendLine("Including raw XML request: ");
                        m_log.AppendLine(rawXml);
                        m_log.AppendLine();
                    }

                    Log(lqb4506TPostResponse);
                    Tools.LogInfo(m_log.ToString());
                }
            }
        }

        private Lqb4506TPostResponse CreateErrorResponse(Lqb4506TPostResponse lqb4506TPostResponse, Exception exc)
        {
            if (lqb4506TPostResponse == null)
            {
                lqb4506TPostResponse = new Lqb4506TPostResponse();
            }

            if (exc is CBaseException)
            {
                CBaseException lqbException = (CBaseException)exc;
                AddErrorMessage(lqb4506TPostResponse, lqbException.UserMessage);
                AddErrorMessage(lqb4506TPostResponse, "Error Reference Number: " + lqbException.ErrorReferenceNumber);
            }
            else if (exc is XmlException)
            {
                AddErrorMessage(lqb4506TPostResponse, exc.Message); // XmlException's message is safe, and will help debug without support.
            }
            else
            {
                AddErrorMessage(lqb4506TPostResponse, LendersOffice.Common.ErrorMessages.Generic);
            }

            return lqb4506TPostResponse;
        }

        #endregion

        private static Guid SafeParseGuid(string id, string elementName)
        {
            if (string.IsNullOrEmpty(id))
            {
                string errorMessage = elementName + " must be a valid unique identifier (GUID).";
                throw new CBaseException(errorMessage, errorMessage);
            }

            try
            {
                return new Guid(id);
            }
            catch (FormatException)
            {
                return Guid.Empty;
            }
        }

        private Lqb4506TPostResponse Process(Lqb4506TPost post, string rawXml)
        {
            Lqb4506TPostResponse postResponse = new Lqb4506TPostResponse();

            postResponse.Source.LoanId = post.Source.LoanId;
            postResponse.Source.LoanNumber = post.Source.LoanNumber;
            postResponse.Source.SourceName = post.Source.SourceName;
            postResponse.Source.TransactionId = post.Source.TransactionId;

            Guid loanId = SafeParseGuid(post.Source.LoanId, "LoanID");
            Guid transactionId = SafeParseGuid(post.Source.TransactionId, "TransactionID");

            Irs4506TOrderInfo orderInfo = FindTransaction(loanId, transactionId);
            if (orderInfo == null)
            {
                AddErrorMessage(postResponse, "Transaction not found");
                return postResponse;
            }

            List<Tuple<DuServiceProviders.DuServiceProvider?, DuValidation>> duProviders = post.Data.DuValidation
                .Select(duValidation => Tuple.Create(DuServiceProviders.RetrieveByProviderName(duValidation.ServiceProviderName), duValidation))
                .ToList();
            IReadOnlyCollection<string> unknownDuProviderNames = duProviders
                .Where(provider => !provider.Item1.HasValue)
                .Select(provider => provider.Item2.ServiceProviderName)
                .ToList();
            if (unknownDuProviderNames.Any())
            {
                AddErrorMessage(
                    postResponse,
                    $"DU Validation - Unknown third party service provider{(unknownDuProviderNames.Count > 1 ? "s" : "")}: {string.Join(", ", unknownDuProviderNames)}");
                return postResponse;
            }

            SaveToFileDBAndEdocs(orderInfo, post.EmbeddedFileList);

            SaveDuServiceProviders(orderInfo, duProviders, rawXml);

            UpdateOrderResponse(orderInfo, post, rawXml);

            postResponse.Status.RequestStatus = E_StatusRequestStatus.Success;

            return postResponse;
        }

        private void SaveDuServiceProviders(Irs4506TOrderInfo orderInfo, List<Tuple<DuServiceProviders.DuServiceProvider?, DuValidation>> associatedServiceProviders, string rawXml)
        {
            if (associatedServiceProviders.Count == 0)
            {
                return;
            }

            CPageData loanData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(orderInfo.sLId, typeof(Irs4506TPostHandler));
            loanData.InitSave(Constants.ConstAppDavid.SkipVersionCheck);
            var xml = new XmlDocument();
            xml.LoadXml(rawXml);
            var ssn = xml.SelectSingleNode("/LQB4506TPost/Data/Borrower/@_SSN")?.Value?.Replace("-", string.Empty);
            loanData.sDuThirdPartyProviders.AddProviders(associatedServiceProviders.Select(provider => new FannieMaeThirdPartyProvider(provider.Item1.Value, $"{ssn}:{provider.Item2.ReferenceNumber}")));
            if (loanData.sDuThirdPartyProviders.IsDirty)
            {
                loanData.Save();
            }
        }

        private void UpdateOrderResponse(Irs4506TOrderInfo orderInfo, LQB4506T.LQB4506TPost.Lqb4506TPost post, string rawXml)
        {
            string statusText = "";
            Irs4506TOrderStatus statusT;

            switch (post.Status.OrderStatus)
            {
                case E_StatusOrderStatus.Cancelled:
                    statusText = post.Status.StatusComments;
                    statusT = Irs4506TOrderStatus.Cancelled;
                    break;
                case E_StatusOrderStatus.Completed:
                    statusText = "Completed";
                    statusT = Irs4506TOrderStatus.Completed;
                    break;
                case E_StatusOrderStatus.OnHold:
                    statusText = post.Status.StatusComments;
                    statusT = Irs4506TOrderStatus.OnHold;
                    break;

                default:
                    throw new UnhandledEnumException(post.Status.OrderStatus);
            }

            string responseXmlContent;
            try
            {
                using (StringWriter stringWriter = new StringWriter())
                using (XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter))
                {
                    post.WriteXml(xmlTextWriter);
                    responseXmlContent = stringWriter.ToString(); //Don't save Base64 file data to the db if we can avoid it.
                }
            }
            catch (OutOfMemoryException)
            {
                responseXmlContent = rawXml;
            }

            string uploadedFilesXmlContent = Irs4506TOrderInfo.XmlSerialize(orderInfo.UploadedFiles);

            Guid loanId = SafeParseGuid(post.Source.LoanId, "LoanID");
            Guid brokerId;
            DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfoByLoanId(loanId, out brokerId);

            SqlParameter[] parameters = {
                                            new SqlParameter("@TransactionId", orderInfo.TransactionId),
                                            new SqlParameter("@Status", statusText),
                                            new SqlParameter("@StatusT", statusT),
                                            new SqlParameter("@ResponseXmlContent", Irs4506TOrderInfo.MaskSensitiveLqbPostResponseData(responseXmlContent)),
                                            new SqlParameter("@UploadedFilesXmlContent", uploadedFilesXmlContent)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(connInfo, "IRS_4506T_ORDER_INFO_UpdateResponse", 3, parameters);
        }

        private Irs4506TOrderInfo FindTransaction(Guid loanId, Guid transactionId)
        {
            var list = Irs4506TOrderInfo.RetrieveByLoanId(loanId);

            foreach (var o in list)
            {
                if (o.TransactionId == transactionId)
                {
                    return o;
                }
            }
            return null;
        }

        private static void AddErrorMessage(Lqb4506TPostResponse postResponse, string errorMessage)
        {
            postResponse.Status.RequestStatus = E_StatusRequestStatus.Failure;
            postResponse.Status.ErrorList.Add(errorMessage);
        }

        private Lqb4506TPost ParseFromRequest(HttpRequest request, out string rawXml)
        {
            rawXml = string.Empty;

            byte[] bytes = Tools.ReadFully(request.InputStream, 10000);

            
            rawXml = System.Text.Encoding.UTF8.GetString(bytes);

            if (string.IsNullOrEmpty(rawXml))
                throw new CBaseException(Common.ErrorMessages.Generic, "Irs4506TPostHandler does not handle null or empty requests");
            else if (rawXml[0] != '<')
            {
                // 8/8/2013 dd - This is to prevent an extra BOM characters in the request stream.
                // http://en.wikipedia.org/wiki/Byte_order_mark
                rawXml = rawXml.TrimWhitespaceAndBOM();
            }
            try
            {
                Lqb4506TPost postResponse = new Lqb4506TPost();
                var settings = new XmlReaderSettings();

                using (XmlReader reader = XmlReader.Create(new StringReader(rawXml), settings))
                {
                    postResponse.ReadXml(reader);
                }
                return postResponse;
            }
            catch (XmlException exc)
            {
                Tools.LogError("Unable to Parse Response: " + Environment.NewLine + rawXml + Environment.NewLine + Environment.NewLine + exc.ToString());
                throw;
            }
        }

        /// <summary>
        /// Saves Files to FileDB (and Edocs if configured).  Adds each EmbeddedFile to orderInfo.UploadedFiles
        /// </summary>
        private void SaveToFileDBAndEdocs(Irs4506TOrderInfo orderInfo, List<EmbeddedFile> list)
        {
            foreach (var embeddedFile in list)
            {
                Irs4506TOrderInfo.Irs4506TDocument doc = orderInfo.AddDocument(embeddedFile);
                if (!string.IsNullOrEmpty(doc.FileDBKey))
                {
                    AutoSaveDocTypeFactory.SavePdfDocAsSystem(E_AutoSavePage.Irs4506TDocuments, doc.FileDBKey,
                        E_FileDB.Normal, orderInfo.sBrokerId, orderInfo.sLId, orderInfo.aAppId, doc.DocumentName);
                }
            }
        }

        private void SendResponse(HttpResponse response, Lqb4506TPostResponse postResponse)
        {
            response.ContentType = "text/xml";

            using (XmlWriter writer = XmlWriter.Create(response.OutputStream))
            {
                postResponse.WriteXml(writer);
            }
            response.Flush();
        }

        private void Log(LQB4506T.AbstractXmlSerializable xmlSerializable)
        {
            if (xmlSerializable == null)
            {
                m_log.AppendLine("Unable to log null value");
            }
            else
            {
                if (xmlSerializable.GetType() == typeof(Lqb4506TPost))
                {
                    m_log.AppendLine("LQB4506TPost: ");
                }
                else if (xmlSerializable.GetType() == typeof(Lqb4506TPostResponse))
                {
                    m_log.AppendLine("LQB4506TPostResponse: ");
                }

                using (StringWriter stringWriter = new StringWriter(m_log))
                using (XmlTextWriter writer = new XmlTextWriter(stringWriter))
                {
                    xmlSerializable.WriteXml(writer);
                }
            }

            m_log.AppendLine();
            m_log.AppendLine();
        }

    }
}
