﻿namespace LendersOffice.Integration.Irs4506T
{
    using DataAccess;

    /// <summary>
    /// Represents the different communication models used for IRS 4506-T Tax Return Verification data.
    /// </summary>
    public enum Irs4506TCommunicationModel
    {
        /// <summary>
        /// LendingQB Proprietary format.
        /// </summary>
        [OrderedDescription("LendingQB Proprietary")]
        LendingQB = 0,

        /// <summary>
        /// Equifax Verification Services format.
        /// </summary>
        [OrderedDescription("Equifax Verification Services")]
        EquifaxVerificationServices = 1
    }
}
