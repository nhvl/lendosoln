﻿namespace LendersOffice.Integration
{
    /// <summary>
    /// We could have an integration type and a request type, but not every combination is valid so this keeps the valid combinations for easy lookup.<para></para>
    /// When ready, we should add others like Appraisal_Info, Appraisal_Order, etc. <para></para>
    /// Created for opm 463659.
    /// </summary>
    public enum IntegrationRequestType
    {
        /// <summary>
        /// Irs 4506T order request.
        /// </summary>
        Irs4506T_Order = 0
    }
}
