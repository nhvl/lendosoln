﻿// Generated via GenerateRecordClass.ttinclude
namespace LendersOffice.Integration.DocMagicAppraisalDelivery
{
    /// <summary>
    /// Encapsulates the options to pass to DocMagic for their electronic disclosure portal.
    /// </summary>
    public partial class EPortalOptions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EPortalOptions"/> class.
        /// </summary>
        /// <param name="enableElectronicSignature">A value indicating whether electronic signatures and initials are enabled for the disclosure event.</param>
        /// <param name="enableMobileApplication">A value indicating whether the DocMagic mobile application is enabled for the disclosure event.</param>
        /// <param name="disableSigningInvitation">A value indicating whether the signing invitation/notification should be omitted.</param>
        /// <param name="previewDocumentsPriorToSigning">A value indicating whether the documents can be reviewed prior to the activation of signing.</param>
        /// <param name="sendConfirmationEmail">A value indicating whether DocMagic should send the user a confirmation email.</param>
        public EPortalOptions(bool enableElectronicSignature, bool enableMobileApplication, bool disableSigningInvitation, bool previewDocumentsPriorToSigning, bool sendConfirmationEmail)
        {
            this.EnableElectronicSignature = enableElectronicSignature;
            this.EnableMobileApplication = enableMobileApplication;
            this.DisableSigningInvitation = disableSigningInvitation;
            this.PreviewDocumentsPriorToSigning = previewDocumentsPriorToSigning;
            this.SendConfirmationEmail = sendConfirmationEmail;
        }

        /// <summary>
        /// Gets a value indicating whether electronic signatures and initials are enabled for the disclosure event.
        /// </summary>
        public bool EnableElectronicSignature { get; }

        /// <summary>
        /// Gets a value indicating whether the DocMagic mobile application is enabled for the disclosure event.
        /// </summary>
        public bool EnableMobileApplication { get; }

        /// <summary>
        /// Gets a value indicating whether the signing invitation/notification should be omitted.
        /// </summary>
        public bool DisableSigningInvitation { get; }

        /// <summary>
        /// Gets a value indicating whether the documents can be reviewed prior to the activation of signing.
        /// </summary>
        public bool PreviewDocumentsPriorToSigning { get; }

        /// <summary>
        /// Gets a value indicating whether DocMagic should send the user a confirmation email.
        /// </summary>
        public bool SendConfirmationEmail { get; }
    }
}
