﻿// Generated via GenerateRecordClass.ttinclude
namespace LendersOffice.Integration.DocMagicAppraisalDelivery
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Encapsulates the data received in response to the delivery of an appraisal to DocMagic.
    /// </summary>
    public partial class DocMagicAppraisalDeliveryResultData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocMagicAppraisalDeliveryResultData"/> class.
        /// </summary>
        /// <param name="success">A value indicating whether the request was successful.</param>
        /// <param name="errorMessages">The error messages to display to the user.</param>
        public DocMagicAppraisalDeliveryResultData(bool success, IEnumerable<string> errorMessages)
        {
            this.Success = success;
            this.ErrorMessages = errorMessages;
        }

        /// <summary>
        /// Gets a value indicating whether the request was successful.
        /// </summary>
        public bool Success { get; }

        /// <summary>
        /// Gets the error messages to display to the user.
        /// </summary>
        public IEnumerable<string> ErrorMessages { get; }
    }
}
