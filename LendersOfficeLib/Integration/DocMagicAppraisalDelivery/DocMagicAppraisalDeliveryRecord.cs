﻿namespace LendersOffice.Integration.DocMagicAppraisalDelivery
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using Common;
    using DataAccess;
    using Security;

    /// <summary>
    /// Extends the main partial class representing appraisal delivery requests sent to DocMagic by adding DB interaction methods.
    /// </summary>
    public partial class DocMagicAppraisalDeliveryRecord
    {
        /// <summary>
        /// Retrieves all records of requests for DocMagic appraisal delivery for a given loan.
        /// </summary>
        /// <param name="brokerId">The lender of the loan.</param>
        /// <param name="loanId">The identifier of the loan.</param>
        /// <returns>A collection of delivery records.</returns>
        public static IReadOnlyCollection<DocMagicAppraisalDeliveryRecord> RetrieveAllForLoan(Guid brokerId, Guid loanId)
        {
            var parameters = new[]
            {
                new System.Data.SqlClient.SqlParameter("@BrokerId", brokerId),
                new System.Data.SqlClient.SqlParameter("@LoanId", loanId),
            };

            var recordsLoaded = new List<IReadOnlyDictionary<string, object>>();
            var docRecordsLoaded = new List<IReadOnlyDictionary<string, object>>();
            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "DOCMAGIC_APPRAISAL_DELIVERY_RetrieveAllForLoan", parameters))
            {
                while (reader.Read())
                {
                    recordsLoaded.Add(reader.ToDictionary());
                }

                if (reader.NextResult())
                {
                    while (reader.Read())
                    {
                        docRecordsLoaded.Add(reader.ToDictionary());
                    }
                }
            }

            var deliveryRecords = new List<DocMagicAppraisalDeliveryRecord>(recordsLoaded.Count);
            foreach (IReadOnlyDictionary<string, object> record in recordsLoaded)
            {
                Guid appraisalDeliveryId = (Guid)record["AppraisalDeliveryId"];
                var documentIds = docRecordsLoaded
                        .Where(r => (Guid)r["AppraisalDeliveryId"] == appraisalDeliveryId)
                        .Select(r => (Guid)r["DocumentId"]).ToList();

                deliveryRecords.Add(new DocMagicAppraisalDeliveryRecord(
                    id: appraisalDeliveryId,
                    brokerId: (Guid)record["BrokerId"],
                    loanId: (Guid)record["LoanId"],
                    appraisalOrderId: (Guid)record["AppraisalOrderId"],
                    docVendorId: (Guid)record["DocVendorId"],
                    sentDate: (DateTime)record["SentDate"],
                    sentByName: (string)record["SentByName"],
                    sentByUserId: (Guid)record["SentByUserId"],
                    websheetNumber: (string)record["WebsheetNumber"],
                    documentIds: documentIds));
            }

            return deliveryRecords;
        }

        /// <summary>
        /// Creates a record of the appraisal delivery order in the database and returns the instance.
        /// </summary>
        /// <param name="request">The request sent to DocMagic.</param>
        /// <param name="user">The user who placed the request.</param>
        /// <returns>The record of the order created.</returns>
        public static DocMagicAppraisalDeliveryRecord Create(DocMagicAppraisalDeliveryRequestData request, AbstractUserPrincipal user)
        {
            var record = new DocMagicAppraisalDeliveryRecord(
                id: Guid.NewGuid(),
                brokerId: user.BrokerId,
                loanId: request.LoanId,
                appraisalOrderId: request.AppraisalOrderId,
                docVendorId: request.VendorId,
                sentDate: DateTime.Now,
                sentByName: user.DisplayName,
                sentByUserId: user.UserId,
                websheetNumber: request.WebsheetNumber,
                documentIds: request.Documents);

            var createRecordProcedureName = LqbGrammar.DataTypes.StoredProcedureName.Create("DOCMAGIC_APPRAISAL_DELIVERY_Create").ForceValue();
            var createDocRecordProcedureName = LqbGrammar.DataTypes.StoredProcedureName.Create("DOCMAGIC_APPRAISAL_DELIVERY_DOCUMENT_Create").ForceValue();
            using (var connection = user.ConnectionInfo.GetConnection())
            using (var transaction = connection.OpenDbConnection().BeginTransaction())
            {
                try
                {
                    var parameters = new[]
                    {
                        new System.Data.SqlClient.SqlParameter("@AppraisalDeliveryId", record.Id),
                        new System.Data.SqlClient.SqlParameter("@BrokerId", record.BrokerId),
                        new System.Data.SqlClient.SqlParameter("@LoanId", record.LoanId),
                        new System.Data.SqlClient.SqlParameter("@AppraisalOrderId", record.AppraisalOrderId),
                        new System.Data.SqlClient.SqlParameter("@DocVendorId", record.DocVendorId),
                        new System.Data.SqlClient.SqlParameter("@SentDate", record.SentDate),
                        new System.Data.SqlClient.SqlParameter("@SentByName", record.SentByName),
                        new System.Data.SqlClient.SqlParameter("@SentByUserId", record.SentByUserId),
                        new System.Data.SqlClient.SqlParameter("@WebsheetNumber", record.WebsheetNumber),
                    };
                    Drivers.SqlServerDB.StoredProcedureDriverHelper.ExecuteNonQuery(connection, transaction, createRecordProcedureName, parameters, LqbGrammar.DataTypes.TimeoutInSeconds.Default);

                    foreach (Guid documentId in record.DocumentIds)
                    {
                        parameters = new[]
                        {
                            new System.Data.SqlClient.SqlParameter("@AppraisalDeliveryId", record.Id),
                            new System.Data.SqlClient.SqlParameter("@DocumentId", documentId),
                        };
                        Drivers.SqlServerDB.StoredProcedureDriverHelper.ExecuteNonQuery(connection, transaction, createDocRecordProcedureName, parameters, LqbGrammar.DataTypes.TimeoutInSeconds.Default);
                    }

                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }

            return record;
        }
    }
}
