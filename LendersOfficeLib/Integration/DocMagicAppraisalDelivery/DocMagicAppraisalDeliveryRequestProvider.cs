﻿namespace LendersOffice.Integration.DocMagicAppraisalDelivery
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DocMagic.DsiDocRequest;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides the data and request of DocMagic appraisal delivery.
    /// </summary>
    public class DocMagicAppraisalDeliveryRequestProvider
    {
        /// <summary>
        /// Loads the DocMagic credentials for the specified parameters.
        /// </summary>
        /// <param name="user">The user using the credentials.</param>
        /// <param name="vendorId">The identifier of the vendor who the credentials will authenticate against.</param>
        /// <param name="loanId">The identifier of the loan the credentials will be used for.</param>
        /// <returns>The DocMagic credentials, or null if no non-empty credentials were found.</returns>
        public static DocMagicCredentials LoadSavedCredentials(Security.AbstractUserPrincipal user, Guid vendorId, Guid loanId)
        {
            Guid branchId = DataAccess.Tools.GetLoanBranchIdByLoanId(user.BrokerId, loanId);
            var vendorCreds = new DocumentVendor.VendorCredentials(user.UserId, branchId, user.BrokerId, vendorId);
            if (string.IsNullOrEmpty(vendorCreds.UserName) && string.IsNullOrEmpty(vendorCreds.Password))
            {
                return null;
            }

            return new DocMagicCredentials(vendorCreds.UserName, vendorCreds.Password, vendorCreds.CustomerId);
        }

        /// <summary>
        /// Delivers the documents specified by <paramref name="requestData"/> to DocMagic.
        /// </summary>
        /// <param name="requestData">The request data from the user.</param>
        /// <param name="user">The user to deliver the Documents as.</param>
        /// <returns>The result of appraisal delivery request to DocMagic.</returns>
        public static DocMagicAppraisalDeliveryResultData DeliverDocuments(DocMagicAppraisalDeliveryRequestData requestData, Security.AbstractUserPrincipal user)
        {
            var repository = EDocs.EDocumentRepository.GetUserRepository(user);
            DocMagicCredentials savedCredentials = LoadSavedCredentials(user, requestData.VendorId, requestData.LoanId);
            var vendor = DocumentVendor.VendorConfig.Retrieve(requestData.VendorId);
            Guid transactionId = Guid.NewGuid();

            IEnumerable<DocumentData> documentsToSend = LoadDocuments(requestData, repository);
            foreach (var document in documentsToSend)
            {
                DsiDocumentServerRequest uploadRequest = CreateUploadRequest(requestData, document, transactionId, savedCredentials);
                var uploadResponse = Conversions.DocMagicServer2.Submit(uploadRequest, Conversions.DocMagicServer2.RequestType.AppraisalDelivery, vendor);
                if (uploadResponse.Status == DocMagic.DsiDocResponse.E_DsiDocumentServerResponseStatus.Failure)
                {
                    return CreateErrorResult(uploadResponse);
                }
            }

            DsiDocumentServerRequest configureRequest = CreateConfigureRequest(requestData, transactionId, savedCredentials);
            configureRequest.DocSetRequest.AttachmentList.AddRange(documentsToSend.Select(doc => new DocMagic.Common.Attachment { Name = doc.Name }));
            var configureResponse = Conversions.DocMagicServer2.Submit(configureRequest, Conversions.DocMagicServer2.RequestType.AppraisalDelivery, vendor);
            if (configureResponse.Status == DocMagic.DsiDocResponse.E_DsiDocumentServerResponseStatus.Failure)
            {
                return CreateErrorResult(configureResponse);
            }

            var record = DocMagicAppraisalDeliveryRecord.Create(requestData, user);
            return new DocMagicAppraisalDeliveryResultData(success: true, errorMessages: null);
        }

        /// <summary>
        /// Loads the documents for the request.
        /// </summary>
        /// <param name="requestData">The request data from the user.</param>
        /// <param name="repository">The repository containing the documents.</param>
        /// <returns>The document data for the request.</returns>
        private static IEnumerable<DocumentData> LoadDocuments(DocMagicAppraisalDeliveryRequestData requestData, EDocs.EDocumentRepository repository)
        {
            var docs = new List<DocumentData>();
            int documentIndex = 0;
            foreach (var documentId in requestData.Documents)
            {
                var document = repository.GetDocumentById(documentId);
                if (document.LoanId != requestData.LoanId)
                {
                    DataAccess.Tools.LogWarning("DocMagicAppraisalDelivery attempted to deliver document " + documentId + " which does not belong to " + requestData.LoanId);
                    throw new Security.AccessDenied();
                }

                docs.Add(new DocumentData(
                    name: ++documentIndex + "_" + SanitizeFileName(document.DocTypeName) + ".pdf", // if the doc doesn't end in .pdf, DocMagic says "Content type of application/pdf is not supported for attachment"
                    description: !string.IsNullOrWhiteSpace(document.PublicDescription) ? document.PublicDescription : document.DocTypeName,
                    getContentBase64: () => Drivers.Base64Encoding.Base64EncodingDriverHelper.Encode(Drivers.Gateways.BinaryFileHelper.ReadAllBytes(document.GetPDFTempFile_Current()))));
            }

            return docs;
        }

        /// <summary>
        /// Creates an "Upload" request.
        /// </summary>
        /// <param name="requestData">The request data from the user.</param>
        /// <param name="document">The document being uploaded.</param>
        /// <param name="transactionId">The transaction id for the request.</param>
        /// <param name="savedCredentials">The user's saved credentials.</param>
        /// <returns>The request specified by the parameters.</returns>
        private static DsiDocumentServerRequest CreateUploadRequest(DocMagicAppraisalDeliveryRequestData requestData, DocumentData document, Guid transactionId, DocMagicCredentials savedCredentials)
        {
            DsiDocumentServerRequest request = CreateBaseRequest(transactionId, savedCredentials ?? requestData.InputCredentials);
            request.DocSetRequest = new DocSetRequest
            {
                PackageType = E_DocSetRequestPackageType.Appraisal,
                Type = E_DocSetRequestType.UploadAttachment,
                ConfirmationEmail = ToYN(requestData.EPortalOptions.SendConfirmationEmail),
                WebsheetNumber = requestData.WebsheetNumber,
                Attachment = new DocMagic.Common.Attachment
                {
                    Name = document.Name,
                    Description = document.Description,
                    Content = new DocMagic.Common.Content
                    {
                        Type = "application/pdf",
                        Encoding = DocMagic.Common.E_ContentEncoding.base64,
                        InnerText = document.GetContentBase64().Value,
                    }
                }
            };

            return request;
        }

        /// <summary>
        /// Creates a "Configure" request.
        /// </summary>
        /// <param name="requestData">The request data from the user.</param>
        /// <param name="transactionId">The transaction id for the request.</param>
        /// <param name="savedCredentials">The user's saved credentials.</param>
        /// <returns>The request specified by the parameters.</returns>
        private static DsiDocumentServerRequest CreateConfigureRequest(DocMagicAppraisalDeliveryRequestData requestData, Guid transactionId, DocMagicCredentials savedCredentials)
        {
            DsiDocumentServerRequest request = CreateBaseRequest(transactionId, savedCredentials ?? requestData.InputCredentials);
            request.DocSetRequest = new DocSetRequest
            {
                PackageType = E_DocSetRequestPackageType.Appraisal,
                Type = E_DocSetRequestType.Configure,
                ConfirmationEmail = ToYN(requestData.EPortalOptions.SendConfirmationEmail),
                WebsheetNumber = requestData.WebsheetNumber,
                EPortal = new EPortal
                {
                    ClickSign = ToYN(requestData.EPortalOptions.EnableElectronicSignature),
                    BorrowerMobileService = ToYN(requestData.EPortalOptions.EnableMobileApplication),
                    DisableSigningInvitationIndicator = ToYN(requestData.EPortalOptions.DisableSigningInvitation),
                    EnablePreviewIndicator = ToYN(requestData.EPortalOptions.PreviewDocumentsPriorToSigning),
                },
            };

            return request;
        }

        /// <summary>
        /// Converts a boolean to Y/N.
        /// </summary>
        /// <param name="b">A boolean.</param>
        /// <returns>Y or N, for true or false, respectively.</returns>
        private static XmlSerializableCommon.E_YNIndicator ToYN(bool b)
        {
            return b ? XmlSerializableCommon.E_YNIndicator.Y : XmlSerializableCommon.E_YNIndicator.N;
        }

        /// <summary>
        /// Creates the basic data for a DocMagic Appraisal Delivery request.
        /// </summary>
        /// <param name="transactionId">The transaction id to pass to <see cref="DocMagic.Common.DataSource.TransactionId"/>.</param>
        /// <param name="credentials">The credentials to pass to <see cref="DsiDocumentServerRequest.CustomerInformation"/>.</param>
        /// <returns>The basic data of the DocMagic request.</returns>
        private static DsiDocumentServerRequest CreateBaseRequest(Guid transactionId, DocMagicCredentials credentials)
        {
            return new DsiDocumentServerRequest
            {
                RequestOriginator = E_DsiDocumentServerRequestRequestOriginator.ThirdPartyServer,
                DataSource = new DocMagic.Common.DataSource
                {
                    DsName = "LENDINGQB",
                    TransactionId = transactionId.ToString(),
                },
                CustomerInformation = new DocMagic.Common.CustomerInformation
                {
                    CustomerId = credentials.CustomerId,
                    UserName = credentials.Username,
                    Password = credentials.Password,
                },
            };
        }

        /// <summary>
        /// Creates an error result from a failed DocMagic response.
        /// </summary>
        /// <param name="response">The response from DocMagic.</param>
        /// <returns>The error result.</returns>
        private static DocMagicAppraisalDeliveryResultData CreateErrorResult(DocMagic.DsiDocResponse.DsiDocumentServerResponse response)
        {
            List<string> errorMessages = response.MessageList
                .Where(m => m.Category == DocMagic.DsiDocResponse.E_MessageCategory.Server)
                .Select(m => m.InnerText)
                .ToList();
            if (errorMessages.Count == 0)
            {
                errorMessages.Add(ErrorMessages.Generic);
            }

            return new DocMagicAppraisalDeliveryResultData(success: false, errorMessages: errorMessages);
        }

        /// <summary>
        /// Converts the given value into a "safe" value for a file name by replacing invalid characters with underscore.
        /// </summary>
        /// <param name="originalFileName">The original value, to be sanitized.</param>
        /// <returns>The "safe" file name.</returns>
        private static string SanitizeFileName(string originalFileName)
        {
            return System.Text.RegularExpressions.Regex.Replace(originalFileName ?? string.Empty, @"[^A-Za-z0-9 _-]", "_");
        }

        /// <summary>
        /// Encapsulates the data of an individual document sent as an attachment to DocMagic.
        /// </summary>
        /// <remarks>
        /// Generated from
        /// <![CDATA[
        /// <recordType name='DocumentData' namespace='LendersOffice.Integration.DocMagicAppraisalDelivery' doc='Encapsulates the data of an individual document sent as an attachment to DocMagic.'>
        ///     <using value='System'/>
        ///     <using value='LqbGrammar.DataTypes'/>
        ///     <property name = 'Name' type='string' doc='The value of &lt;see cref=""DocMagic.Common.Attachment.Name""/>.' />
        ///     <property name = 'Description' type='string' doc='The value of &lt;see cref=""DocMagic.Common.Attachment.Description""/>.' />
        ///     <property name = 'GetContentBase64' type='Func&lt;Base64EncodedData>' doc='A function providing the base64 encoded data for &lt;see cref=""DocMagic.Common.Attachment.InnerText""/>.' />
        /// </recordType>
        /// ]]>
        /// </remarks>
        public partial class DocumentData
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="DocumentData"/> class.
            /// </summary>
            /// <param name="name">The value of <see cref="DocMagic.Common.Attachment.Name"/>.</param>
            /// <param name="description">The value of <see cref="DocMagic.Common.Attachment.Description"/>.</param>
            /// <param name="getContentBase64">A function providing the base64 encoded data for <see cref="DocMagic.Common.Attachment.InnerText"/>.</param>
            public DocumentData(string name, string description, Func<Base64EncodedData> getContentBase64)
            {
                this.Name = name;
                this.Description = description;
                this.GetContentBase64 = getContentBase64;
            }

            /// <summary>
            /// Gets the value of <see cref="DocMagic.Common.Attachment.Name"/>.
            /// </summary>
            public string Name { get; }

            /// <summary>
            /// Gets the value of <see cref="DocMagic.Common.Attachment.Description"/>.
            /// </summary>
            public string Description { get; }

            /// <summary>
            /// Gets a function providing the base64 encoded data for <see cref="DocMagic.Common.Attachment.InnerText"/>.
            /// </summary>
            public Func<Base64EncodedData> GetContentBase64 { get; }
        }
    }
}
