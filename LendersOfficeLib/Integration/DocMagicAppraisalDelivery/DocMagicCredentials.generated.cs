﻿// Generated via GenerateRecordClass.ttinclude
namespace LendersOffice.Integration.DocMagicAppraisalDelivery
{
    /// <summary>
    /// Encapsulates the credentials to pass to DocMagic.
    /// </summary>
    public partial class DocMagicCredentials
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocMagicCredentials"/> class.
        /// </summary>
        /// <param name="username">The user name value of the credential.</param>
        /// <param name="password">The password value of the credential.</param>
        /// <param name="customerId">The customer id value of the credential.</param>
        public DocMagicCredentials(string username, string password, string customerId)
        {
            this.Username = username;
            this.Password = password;
            this.CustomerId = customerId;
        }

        /// <summary>
        /// Gets the user name value of the credential.
        /// </summary>
        public string Username { get; }

        /// <summary>
        /// Gets the password value of the credential.
        /// </summary>
        public string Password { get; }

        /// <summary>
        /// Gets the customer id value of the credential.
        /// </summary>
        public string CustomerId { get; }
    }
}
