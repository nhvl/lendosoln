﻿// Generated via GenerateRecordClass.ttinclude
namespace LendersOffice.Integration.DocMagicAppraisalDelivery
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Encapsulates the data necessary to request delivery of an appraisal to DocMagic.
    /// </summary>
    public partial class DocMagicAppraisalDeliveryRequestData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocMagicAppraisalDeliveryRequestData"/> class.
        /// </summary>
        /// <param name="loanId">The identifier of the loan.</param>
        /// <param name="appraisalOrderId">The identifier of the appraisal order on the loan.</param>
        /// <param name="vendorId">The identifier of the DocMagic document vendor to use.</param>
        /// <param name="inputCredentials">The credentials provided by the user for use.</param>
        /// <param name="ePortalOptions">The options for the DocMagic electronic portal.</param>
        /// <param name="websheetNumber">The DocMagic WebsheetNumber used to identify the loan containing the request.</param>
        /// <param name="documents">The <see cref="EDocs.EDocument.DocumentId"/> values for the documents to include to DocMagic.</param>
        public DocMagicAppraisalDeliveryRequestData(Guid loanId, Guid appraisalOrderId, Guid vendorId, DocMagicCredentials inputCredentials, EPortalOptions ePortalOptions, string websheetNumber, IEnumerable<Guid> documents)
        {
            this.LoanId = loanId;
            this.AppraisalOrderId = appraisalOrderId;
            this.VendorId = vendorId;
            this.InputCredentials = inputCredentials;
            this.EPortalOptions = ePortalOptions;
            this.WebsheetNumber = websheetNumber;
            this.Documents = documents;
        }

        /// <summary>
        /// Gets the identifier of the loan.
        /// </summary>
        public Guid LoanId { get; }

        /// <summary>
        /// Gets the identifier of the appraisal order on the loan.
        /// </summary>
        public Guid AppraisalOrderId { get; }

        /// <summary>
        /// Gets the identifier of the DocMagic document vendor to use.
        /// </summary>
        public Guid VendorId { get; }

        /// <summary>
        /// Gets the credentials provided by the user for use.
        /// </summary>
        public DocMagicCredentials InputCredentials { get; }

        /// <summary>
        /// Gets the options for the DocMagic electronic portal.
        /// </summary>
        public EPortalOptions EPortalOptions { get; }

        /// <summary>
        /// Gets the DocMagic WebsheetNumber used to identify the loan containing the request.
        /// </summary>
        public string WebsheetNumber { get; }

        /// <summary>
        /// Gets the <see cref="EDocs.EDocument.DocumentId"/> values for the documents to include to DocMagic.
        /// </summary>
        public IEnumerable<Guid> Documents { get; }
    }
}
