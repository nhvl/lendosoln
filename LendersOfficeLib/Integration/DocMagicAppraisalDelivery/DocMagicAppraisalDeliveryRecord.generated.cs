﻿// Generated via GenerateRecordClass.ttinclude
namespace LendersOffice.Integration.DocMagicAppraisalDelivery
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// A record of an appraisal delivery package sent to DocMagic.
    /// </summary>
    public partial class DocMagicAppraisalDeliveryRecord
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocMagicAppraisalDeliveryRecord"/> class.
        /// </summary>
        /// <param name="id">The identifier of a delivery record in the set of all records.</param>
        /// <param name="brokerId">The identifier of the lender who had the appraisal delivered.</param>
        /// <param name="loanId">The identifier of the loan with the appraisal.</param>
        /// <param name="appraisalOrderId">The identifier of the appraisal order record.</param>
        /// <param name="docVendorId">The identifier of the document vendor who received the appraisal delivery.</param>
        /// <param name="sentDate">The date the appraisal delivery was sent.</param>
        /// <param name="sentByName">The name of the user who sent the appraisal delivery.</param>
        /// <param name="sentByUserId">The identifier of the user who sent the appraisal delivery.</param>
        /// <param name="websheetNumber">The DocMagic WebsheetNumber the appraisal will be delivered through.</param>
        /// <param name="documentIds">The identifiers of the <see cref="EDocs.EDocument"/> instances sent in the appraisal delivery package.</param>
        public DocMagicAppraisalDeliveryRecord(Guid id, Guid brokerId, Guid loanId, Guid appraisalOrderId, Guid docVendorId, DateTime sentDate, string sentByName, Guid sentByUserId, string websheetNumber, IEnumerable<Guid> documentIds)
        {
            this.Id = id;
            this.BrokerId = brokerId;
            this.LoanId = loanId;
            this.AppraisalOrderId = appraisalOrderId;
            this.DocVendorId = docVendorId;
            this.SentDate = sentDate;
            this.SentByName = sentByName;
            this.SentByUserId = sentByUserId;
            this.WebsheetNumber = websheetNumber;
            this.DocumentIds = documentIds;
        }

        /// <summary>
        /// Gets the identifier of a delivery record in the set of all records.
        /// </summary>
        public Guid Id { get; }

        /// <summary>
        /// Gets the identifier of the lender who had the appraisal delivered.
        /// </summary>
        public Guid BrokerId { get; }

        /// <summary>
        /// Gets the identifier of the loan with the appraisal.
        /// </summary>
        public Guid LoanId { get; }

        /// <summary>
        /// Gets the identifier of the appraisal order record.
        /// </summary>
        public Guid AppraisalOrderId { get; }

        /// <summary>
        /// Gets the identifier of the document vendor who received the appraisal delivery.
        /// </summary>
        public Guid DocVendorId { get; }

        /// <summary>
        /// Gets the date the appraisal delivery was sent.
        /// </summary>
        public DateTime SentDate { get; }

        /// <summary>
        /// Gets the name of the user who sent the appraisal delivery.
        /// </summary>
        public string SentByName { get; }

        /// <summary>
        /// Gets the identifier of the user who sent the appraisal delivery.
        /// </summary>
        public Guid SentByUserId { get; }

        /// <summary>
        /// Gets the DocMagic WebsheetNumber the appraisal will be delivered through.
        /// </summary>
        public string WebsheetNumber { get; }

        /// <summary>
        /// Gets the identifiers of the <see cref="EDocs.EDocument"/> instances sent in the appraisal delivery package.
        /// </summary>
        public IEnumerable<Guid> DocumentIds { get; }
    }
}
