﻿namespace LendersOffice.Integration.TotalScorecard
{
    using DataAccess;
    using LendersOffice.Common;

    public class PasswordErrorException : CBaseException
    {
        public PasswordErrorException()
            : base(ErrorMessages.Generic, "Invalid Total Scorecard Credentials - please check the stage config key.")
        {
        }
    }
}
