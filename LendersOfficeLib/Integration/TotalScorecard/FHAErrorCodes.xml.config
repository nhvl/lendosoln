﻿<?xml version="1.0" encoding="utf-8" ?>
<FHAErrorCodes>
    <error errorcode="001" message="The TOTAL Scorecard server was not set up correctly" recommendation="Contact the FHA Loan Information Line at 1-800-CALL FHA (1-800-225-5342)"></error>
    <error errorcode="004" message="One SSN must be entered" recommendation="Verify that loan application data is correct"></error>
    <error errorcode="005" message="SSN for {0} is not valid" recommendation="Verify that loan application data is correct"></error>
    <error errorcode="006" message="SSN for {1} is not valid" recommendation="Verify that loan application data is correct"></error>
    <error errorcode="007" message="SSN for {2} is not valid" recommendation="Verify that loan application data is correct"></error>
    <error errorcode="008" message="SSN for {3} is not valid" recommendation="Verify that loan application data is correct"></error>
    <error errorcode="009" message="SSN for {4} is not valid" recommendation="Verify that loan application data is correct"></error>
    <error errorcode="010" message="SSN for {1} is required" recommendation="Contact the FHA Loan Information Line at 1-800-CALL FHA (1-800-225-5342)"></error>
    <error errorcode="011" message="SSN for {2} is required" recommendation="Contact the FHA Loan Information Line at 1-800-CALL FHA (1-800-225-5342)"></error>
    <error errorcode="012" message="SSN for {3} is required" recommendation="Contact the FHA Loan Information Line at 1-800-CALL FHA (1-800-225-5342)"></error>
    <error errorcode="014" message="The FICO score is less than 620 AND the Back-End Ratio (DTI) is greater than 43. (Score used is the lowest FICO of all borrowers' median FICO scores. Includes all borrowers with a score)"></error>
    <error errorcode="015" message="Required Investment is less than zero and the loan is a purchase transaction."></error>
    <error errorcode="235" message="A zero FICO Score is only permitted for one applicant" recommendation="Verify that loan application data is correct/Review credit report"></error>
    <error errorcode="240" message="One FICO Score is required when only one applicant" recommendation="Verify that loan application data is correct/Review credit report"></error>
    <error errorcode="290" message="Invalid FHA Case Number" recommendation="Review the FHA Case Number input and try again"></error>
    <error errorcode="300" message="Applicant(s) combined monthly income must be greater than 0" recommendation="Verify that loan application data is correct"></error>
    <error errorcode="305" message="Appraised Value must be between $0 and $9,999,999." recommendation="Verify that loan application data is correct"></error>
    <error errorcode="310" message="Loan amount including MIP must be greater than 0" recommendation="Verify that loan application data is correct"></error>
    <error errorcode="315" message="Monthly payment, including Principal, Interest, Taxes and Insurance, must be greater than $0 and less than $15,000" recommendation="Verify that loan application data is correct"></error>
    <error errorcode="316" message="Refinance Type must be a valid value." recommendation="Verify that loan application data is correct"></error>
    <error errorcode="320" message="Mortgage Insurance Premium cannot be less than 0" recommendation="Contact the FHA Loan Information Line at 1-800-CALL FHA (1-800-225-5342)"></error>
    <error errorcode="325" message="Mortgage term in months must be between 48 and 360" recommendation="Verify that the loan application indicates a 30 year fixed term."></error>
    <error errorcode="330" message="Sale price must be between $0 and $9,999,999." recommendation="Verify that loan application data is correct"></error>
    <error errorcode="335" message="Either Appraised Value or Sale Price is required" recommendation="Verify that loan application data is correct"></error>
    <error errorcode="340" message="Total number of applicants must be between 1 and 5" recommendation="Verify that loan application data is correct"></error>
    <error errorcode="345" message="Total number of applicants does not match number of SSNs entered" recommendation="Contact the FHA Loan Information Line at 1-800-CALL FHA (1-800-225-5342)"></error>
    <error errorcode="350" message="Unique identification for AUS must be entered" recommendation="Contact the FHA Loan Information Line at 1-800-CALL FHA (1-800-225-5342)"></error>
    <error errorcode="355" message="Unique identification for loan application assigned by AUS must be entered" recommendation="Contact the FHA Loan Information Line at 1-800-CALL FHA (1-800-225-5342)"></error>
    <error errorcode="360" message="Loan-To-Value Ratio must be between 0.1 and 125" recommendation="Verify that loan application data is correct" ></error>
    <error errorcode="365" message="Front End Ratio must be greater than 0 and less than 100" recommendation="Verify that loan application data is correct"></error>
    <error errorcode="370" message="Back End Ratio must be greater than 0 and less than 100" recommendation="Verify that loan application data is correct"></error>
    <error errorcode="372" message="Back End Ratio must be greater than Front End Ratio" recommendation="Verify that loan application data is correct"></error>
    <error errorcode="375" message="Underwriting P&amp;I must be greater than $0 and less than $15,000" recommendation="Verify that loan application data is correct"></error>
    <error errorcode="380" message="Underwriting Interest must be greater than 0" recommendation="Verify that loan application data is correct"></error>
    <error errorcode="385" message="Either Lender ID or Sponsored Originator EIN is required" recommendation="Review the Lender ID or Sponsored Originator EIN input and try again"></error>
    <error errorcode="386" message="Both Lender ID and Sponsored Originator EIN not allowed. Please submit either Lender ID or Sponsored Originator EIN." recommendation="Review the Lender ID or Sponsored Originator EIN input and try again"></error>
    <error errorcode="387" message="Sponsored Originator EIN is invalid. Sponsored Originator EIN must be nine numeric digits." recommendation="Review the Sponsored Originator EIN input and try again"></error>
    <error errorcode="388" message="Sponsored Originator EIN is invalid. Sponsored Originator EIN must be in the CHUMS Sponsored Originator list." recommendation="Review the Sponsored Originator EIN input and try again"></error>
    <error errorcode="390" message="Lender ID must contain 10 digits" recommendation="Review the Lender ID input and try again"></error>
    <error errorcode="391" message="Invalid Lender ID Please ensure lender ID is entered correctly. If it is entered correctly, please contact Lender Approval Division at (202)708-3976." recommendation="Review the Lender ID input and try again. If it is entered correctly, please contact Lender Approval Division at (202)708-3976."></error>
    <error errorcode="392" message="Lender ID is not active" recommendation="Contact the FHA Loan Information Line at 1-800-CALL FHA (1-800-225-5342)"></error>
    <error errorcode="393" message="Sponsor ID is required" recommendation="Contact the FHA Loan Information Line at 1-800-CALL FHA (1-800-225-5342)"></error>
    <error errorcode="394" message="Sponsor ID must contain 10 digits" recommendation="Review the Sponsor ID and try again"></error>
    <error errorcode="395" message="Invalid Sponsor ID" recommendation="Review the Sponsor ID and try again"></error>
    <error errorcode="396" message="Sponsor ID is not active" recommendation="Contact the FHA Loan Information Line at 1-800-CALL FHA (1-800-225-5342)"></error>
    <error errorcode="397" message="Both Lender ID and Sponsor ID cannot be mortgagee type 4" recommendation="Contact the FHA Loan Information Line at 1-800-CALL FHA (1-800-225-5342)"></error>
    <error errorcode="398" message="Lender ID is not certified through FHA Connection Certification Screen" recommendation="Contact the FHA Loan Information Line at 1-800-CALL FHA (1-800-225-5342)"></error>
    <error errorcode="399" message="Sponsor ID is not certified through FHA Connection Certification Screen" recommendation="Contact the FHA Loan Information Line at 1-800-CALL FHA (1-800-225-5342)"></error>
    <error errorcode="400" message="Credit Report is required" recommendation="Contact the FHA Loan Information Line at 1-800-CALL FHA (1-800-225-5342)"></error>
    <error errorcode="405" message="Each Credit Report must include a Credit Report Type and a Credit Report Data" recommendation="Contact the FHA Loan Information Line at 1-800-CALL FHA (1-800-225-5342)"></error>
    <error errorcode="415" message="First Time Homebuyer must be “Y” or “N”" recommendation="Contact the FHA Loan Information Line at 1-800-CALL FHA (1-800-225-5342)"></error>
    <error errorcode="420" message="Counsel Type is not valid" recommendation="Contact the FHA Loan Information Line at 1-800-CALL FHA (1-800-225-5342)"></error>
    <error errorcode="425" message="Current Housing Expense must be between $0 and $99,999.99" recommendation="Verify that loan application data is correct"></error>
    <error errorcode="430" message="Gift Letter Amount must be between $0 and $999,999" recommendation="Verify that loan application data is correct"></error>
    <error errorcode="435" message="Gift Letter Source must be 00, 01, 03, 06, or 15" recommendation="Verify that loan application data is correct"></error>
    <error errorcode="436" message="Secondary Financing amount must be provided (zero or greater)." recommendation="Verify that loan application data is correct"></error>
    <error errorcode="437" message="If Secondary Financing Source is provided, it must be a valid value or blank." recommendation="Verify that loan application data is correct"></error>
    <error errorcode="440" message="Required Investment must be greater than the Negative Loan Amount" recommendation="Verify that loan application data is correct"></error>
    <error errorcode="445" message="For a refinance Required Investment must be less than or equal to $999,999.  For a purchase, Required Investment must be less than 120% (Sale Price) - Mortgage Amount." recommendation="Verify that loan application data is correct"/>
    <error errorcode="450" message="Down Payment is required" recommendation="Verify that loan application data is correct"/>
    <error errorcode="465" message="Total Fixed Payment must be between $0 and Monthly Income" recommendation="Verify that loan application data is correct"/>
    <error errorcode="470" message="Condo Indicator must be &quot;Y&quot; or &quot;N&quot;" recommendation="Verify that loan application data is correct"/>
    <error errorcode="475" message="Total Closing Costs cannot exceed 20% of Sale Price on a purchase.  Total Closing Costs cannot exceed 20% of Appraised Value on a refinance" recommendation="Verify that loan application data is correct"/>
    <error errorcode="480" message="Seller Concession Amount must be between $0 and 20% of Sales Price" recommendation="Verify that loan application data is correct"/>
    <error errorcode="485" message="Borrower Closing Costs must be between $0 and 20% of the Appraised Value." recommendation="Verify that loan application data is correct"/>
    <error errorcode="490" message="If Gift Letter Source is 01, 03, 06, or 15, Gift Letter Amount must be greater than zero." recommendation="Verify that loan application data is correct"/>
    <error errorcode="495" message="If Gift Letter Amount is greater than zero, then Gift Letter Source must be 01, 03, 06, or 15" recommendation="Verify that loan application data is correct"/>
    <error errorcode="500" message="Citizenship is required for all borrowers" recommendation="Verify that loan application data is correct"/>
    <error errorcode="505" message="Citizenship must be “USCitizen, “PermanentResidentAlien” “NonPermanentResidentAlien”" recommendation="Verify that loan application data is correct"/>
    <error errorcode="510" message="Full borrower address including street address, city, state, and postal code are required for all borrowers" recommendation="Verify that loan application data is correct"/>
    <error errorcode="515" message="Invalid State Abbreviation for Borrower {0} current address" recommendation="Verify that loan application data is correct"/>
    <error errorcode="520" message="Invalid State Abbreviation for Borrower {1} current address" recommendation="Verify that loan application data is correct"/>
    <error errorcode="525" message="Invalid State Abbreviation for Borrower {2} current address" recommendation="Verify that loan application data is correct"/>
    <error errorcode="530" message="Invalid State Abbreviation for Borrower {3} current address" recommendation="Verify that loan application data is correct"/>
    <error errorcode="535" message="Invalid State Abbreviation for Borrower {4} current address" recommendation="Verify that loan application data is correct"/>
    <error errorcode="540" message="Invalid Zip Code for Borrower {0} current address" recommendation="Verify that loan application data is correct"/>
    <error errorcode="545" message="Invalid Zip Code for Borrower {1} current address" recommendation="Verify that loan application data is correct"/>
    <error errorcode="550" message="Invalid Zip Code for Borrower {2} current address" recommendation="Verify that loan application data is correct"/>
    <error errorcode="555" message="Invalid Zip Code for Borrower {3} current address" recommendation="Verify that loan application data is correct"/>
    <error errorcode="560" message="Invalid Zip Code for Borrower {4} current address" recommendation="Verify that loan application data is correct"/>
    <error errorcode="595" message="Invalid State Name for Borrower {0} employment" recommendation="Verify that loan application data is correct"/>
    <error errorcode="600" message="Invalid State Name for Borrower {1} employment" recommendation="Verify that loan application data is correct"/>
    <error errorcode="610" message="Invalid State Name for Borrower {2} employment" recommendation="Verify that loan application data is correct"/>
    <error errorcode="615" message="Invalid State Name for Borrower {3} employment" recommendation="Verify that loan application data is correct"/>
    <error errorcode="620" message="Invalid State Name for Borrower {4} employment" recommendation="Verify that loan application data is correct"/>
    <error errorcode="625" message="Invalid Zip Code for Borrower {0} employment" recommendation="Verify that loan application data is correct"/>
    <error errorcode="630" message="Invalid Zip Code for Borrower {1} employment" recommendation="Verify that loan application data is correct"/>
    <error errorcode="635" message="Invalid Zip Code for Borrower {2} employment" recommendation="Verify that loan application data is correct"/>
    <error errorcode="640" message="Invalid Zip Code for Borrower {3} employment" recommendation="Verify that loan application data is correct"/>
    <error errorcode="645" message="Invalid Zip Code for Borrower {4} employment" recommendation="Verify that loan application data is correct"/>
    <error errorcode="650" message="Borrower {0} Self Employment Indicator should be True or False" recommendation="Verify that loan application data is correct"/>
    <error errorcode="655" message="Borrower {1} Self Employment Indicator should be True or False" recommendation="Verify that loan application data is correct"/>
    <error errorcode="660" message="Borrower {2} Self Employment Indicator should be True or False" recommendation="Verify that loan application data is correct"/>
    <error errorcode="665" message="Borrower {3} Self Employment Indicator should be True or False" recommendation="Verify that loan application data is correct"/>
    <error errorcode="670" message="Borrower {4} Self Employment Indicator should be True or False" recommendation="Verify that loan application data is correct"/>
    <error errorcode="675" message="Years on Current Job or Months on Current Job for Borrower {0} must be numeric and greater than zero if employment is indicated" recommendation="Verify that loan application data is correct"/>
    <error errorcode="680" message="Years on Current Job or Months on Current Job for Borrower {1} must be numeric and greater than zero if employment is indicated" recommendation="Verify that loan application data is correct"/>
    <error errorcode="685" message="Years on Current Job or Months on Current Job for Borrower {2} must be numeric and greater than zero if employment is indicated" recommendation="Verify that loan application data is correct"/>
    <error errorcode="690" message="Years on Current Job or Months on Current Job for Borrower {3} must be numeric and greater than zero if employment is indicated" recommendation="Verify that loan application data is correct"/>
    <error errorcode="695" message="Years on Current Job or Months on Current Job for Borrower {4} must be numeric and greater than zero if employment is indicated" recommendation="Verify that loan application data is correct"/>
    <error errorcode="700" message="Partial listing real estate agent information supplied. If any listing real estate agent information is provided both First Name and Last Name of the listing real estate agent are required" recommendation="Verify that loan application data is correct"/>
    <error errorcode="710" message="Listing real estate agent information is not allowed on a refinance." recommendation="Verify that loan application data is correct"/>
    <error errorcode="715" message="Partial selling real estate agent information supplied. If any selling real estate agent information is provided both First Name and Last Name of the selling real estate agent are required" recommendation="Verify that loan application data is correct"/>
    <error errorcode="720" message="Selling real estate agent information is not allowed on a refinance." recommendation="Verify that loan application data is correct"/>
    <error errorcode="725" message="Partial closing agent information supplied. If any closing agent information is provided both First Name and Last Name of the closing agent are required" recommendation="Verify that loan application data is correct"/>
    <error errorcode="730" message="Partial notary information supplied. If any notary information is provided both First Name and Last Name of the notary are required" recommendation="Verify that loan application data is correct"/>
    <error errorcode="735" message="Partial seller information supplied. If any seller information is provided both First Name and Last Name of the seller are required" recommendation="Verify that loan application data is correct"/>
    <error errorcode="745" message="Invalid lender loan originator State" recommendation="Verify that loan application data is correct"/>
    <error errorcode="755" message="Invalid broker loan originator State" recommendation="Verify that loan application data is correct"/>
    <error errorcode="765" message="Invalid broker company State" recommendation="Verify that loan application data is correct"/>
    <error errorcode="770" message="Partial gift donor information supplied. If any gift donor information is provided both First Name and Last Name of the gift donor are required" recommendation="Verify that loan application data is correct"/>
    <error errorcode="775" message="Partial loan processor information supplied. If any loan processor information is provided both First Name and Last Name of the loan processor are required" recommendation="Verify that loan application data is correct"/>
    <error errorcode="780" message="Partial landlord information supplied. If any landlord information is provided both First Name and Last Name of the landlord are required" recommendation="Verify that loan application data is correct"/>
    <error errorcode="785" message="Loan Application Data is Required" recommendation="Verify that loan application data is correct"/>
    <error errorcode="999" message="AUS system unable to score loan with data provided" recommendation="Contact the FHA Loan Information Line at 1-800-CALL FHA (1-800-225-5342)"></error>
</FHAErrorCodes>
