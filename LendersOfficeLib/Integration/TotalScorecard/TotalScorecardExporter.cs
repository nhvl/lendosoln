﻿namespace LendersOffice.Integration.TotalScorecard
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using TotalScoreCard;

    public class TotalScorecardExporter
    {

        public static TotalScorecardRequest CreateRequest(Guid sLId)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(TotalScorecardExporter));
            dataLoan.InitLoad();


            TotalScorecardRequest scorecardRequest = new TotalScorecardRequest();

            int nApps = dataLoan.nApps;

            for (int i = 0; i < nApps; i++)
            {
                CAppData dataApp = dataLoan.GetAppData(i);

                TotalScorecardRequestBorrower borr = new TotalScorecardRequestBorrower();

                borr.BorrName = dataApp.aBNm;
                // TODO: Do more mapping
            }

            scorecardRequest.AppraisedValue = dataLoan.sApprVal_rep;
            // TODO: DO MORE MAPPING
            return scorecardRequest;

        }
    }
}
