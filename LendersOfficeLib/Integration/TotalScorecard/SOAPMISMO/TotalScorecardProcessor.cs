﻿// <copyright file="TotalScorecardProcessor.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Isaac Ribakoff
//  Date:   4/20/2015 06:13:00 PM 
// </summary>
namespace LendersOffice.Integration.TotalScorecard.SOAPMISMO
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Xml.Serialization;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.TOTALScorecard.MISMO;
    using ObjLib.Extensions;
    using TotalScoreCard;

    /// <summary>
    /// Static class used to transact with Total Scorecard using SOAP protocol with MISMO 3.3 data.
    /// </summary>
    public static class TotalScorecardProcessor
    {
        /// <summary>
        /// Default namespace for FHA TOTAL Scorecard SOAP xml.
        /// </summary>
        private const string F17CTOTALSCORECARDNAMESPACE = "http://xmlns.oracle.com/F17CTOTALScorecard/F17CTOTALScorecard/F17CTOTALScorecard";

        /// <summary>
        /// URL of TOTAL Scorecard SOAP MISMO test environment. Production url is in the referenced service by default.
        /// </summary>
        private const string TESTURL = "https://totalscorecardtest.hud.gov/TOTALMISMO/ProxyService/F17cTOTALScorecard";

        /// <summary>
        /// SOAP Client for TOTAL Scorecard SOAP MISMO production environment.
        /// </summary>
        private static LqbSingleThreadInitializeLazy<F17CTOTALScorecardClient> productionF17CTOTALScorecardClient;

        /// <summary>
        /// SOAP Client for TOTAL Scorecard SOAP MISMO test environment.
        /// </summary>
        private static LqbSingleThreadInitializeLazy<F17CTOTALScorecardClient> testF17CTOTALScorecardClient;

        /// <summary>
        /// XmlSerializer object for SOAP requests.
        /// </summary>
        private static XmlSerializer soapRequestSerializer;

        /// <summary>
        /// XmlSerializer object for SOAP responses.
        /// </summary>
        private static XmlSerializer soapResponseSerializer;

        /// <summary>
        /// Initializes static members of the <see cref="TotalScorecardProcessor" /> class.
        /// </summary>
        static TotalScorecardProcessor()
        {
            Func<string, string, F17CTOTALScorecardClient> clientFactory = (username, password) =>
            {
                var client = new F17CTOTALScorecardClient();
                client.ClientCredentials.UserName.UserName = username;
                client.ClientCredentials.UserName.Password = password;
                return client;
            };

            productionF17CTOTALScorecardClient = new LqbSingleThreadInitializeLazy<F17CTOTALScorecardClient>(
                () => clientFactory(ConstStage.FHATOTALProductionLogin_SOAPMISMO, ConstStage.FHATOTALProductionPassword_SOAPMISMO));

            testF17CTOTALScorecardClient = new LqbSingleThreadInitializeLazy<F17CTOTALScorecardClient>(() =>
            {
                var client = clientFactory(ConstStage.FHATOTALTestLogin_SOAPMISMO, ConstStage.FHATOTALTestPassword_SOAPMISMO);
                client.Endpoint.Address = new System.ServiceModel.EndpointAddress(TESTURL);
                return client;
            });

            soapRequestSerializer = new XmlSerializer(typeof(process), F17CTOTALSCORECARDNAMESPACE);
            soapResponseSerializer = new XmlSerializer(typeof(processResponse), F17CTOTALSCORECARDNAMESPACE);
        }

        /// <summary>
        /// Submits request to TOTAL Scorecard through SOAP MISMO protocol using legacy/REST protocol wrappers.
        /// </summary>
        /// <param name="restRequest">TOTAL Scorecard legacy/REST protocol request object.</param>
        /// <param name="loanId">Loan Identifier used to generate FNMA 3.2 or MISMO export.</param>
        /// <param name="useFHATotalProductionAccount">Indicates whether to submit to TOTAL Scorecard production environment.</param>
        /// <returns>TOTAL Scorecard SOAP MISMO response wrapped in a legacy/REST protocol response object.</returns>
        public static TotalScorecardResponse ProcessUsingLegacyRESTWrappers(TotalScorecardRequest restRequest, Guid loanId, bool useFHATotalProductionAccount)
        {
            process soapRequest = TotalScorecardRequestGenerator.ConvertFromRestRequest(restRequest, loanId);
            processResponse soapResponse = Process(soapRequest, useFHATotalProductionAccount);
            TotalScorecardResponse restResponse = TotalScorecardResponseGenerator.ConvertToRestResponse(soapResponse);

            return restResponse;
        }

        /// <summary>
        /// Submits request to  Scorecard through SOAP MISMO protocol.
        /// </summary>
        /// <param name="soapRequest">TOTAL Scorecard SOAP request object.</param>
        /// <param name="useFHATotalProductionAccount">Indicates whether to submit to TOTAL Scorecard production environment.</param>
        /// <returns>TOTAL Scorecard SOAP response object.</returns>
        public static processResponse Process(process soapRequest, bool useFHATotalProductionAccount)
        {
            processResponse soapResponse;
            using (StringWriter stringWriter = new StringWriter())
            {
                stringWriter.WriteLine(string.Format("[FHATotalScoreCard - SOAP MISMO {0}]", useFHATotalProductionAccount ? "Production" : "Test"));
                try
                {                
                    WriteString(soapRequest, stringWriter);
                    F17CTOTALScorecardClient soapClient = SelectClient(useFHATotalProductionAccount);
                    soapResponse = soapClient.process(soapRequest);
                    WriteString(soapResponse, stringWriter);                    
                }
                catch (System.ServiceModel.FaultException exception)
                {
                    WriteString(exception, stringWriter);
                    throw;
                }
                catch (System.TimeoutException exception)
                {
                    WriteString(exception, stringWriter);
                    throw;
                }
                finally
                {
                    Tools.LogInfo(stringWriter.ToString());
                }
            }

            return soapResponse;
        }

        /// <summary>
        /// Selects and configures either the production or the test TOTAL Scorecard SOAP client, used for submission.
        /// </summary>
        /// <param name="useFHATotalProductionAccount">Indicates whether to submit to TOTAL Scorecard production environment.</param>
        /// <returns>Configured production or test TOTAL Scorecard SOAP client.</returns>
        private static F17CTOTALScorecardClient SelectClient(bool useFHATotalProductionAccount)
        {
            return useFHATotalProductionAccount ? productionF17CTOTALScorecardClient.Value : testF17CTOTALScorecardClient.Value;
        }

        /// <summary>
        /// Writes soap request into stringWriter.
        /// </summary>
        /// <param name="soapRequest">The soap request to be converted.</param>
        /// <param name="stringWriter">StringWriter object used to perform the conversion.</param>
        private static void WriteString(process soapRequest, StringWriter stringWriter)
        {    
            stringWriter.WriteLine("------Request------");
            soapRequestSerializer.Serialize(stringWriter, soapRequest);
            stringWriter.WriteLine();
        }

        /// <summary>
        /// Writes soap response into stringWriter.
        /// </summary>
        /// <param name="soapResponse">The soap response to be converted.</param>
        /// <param name="stringWriter">StringWriter object used to perform the conversion.</param>
        private static void WriteString(processResponse soapResponse, StringWriter stringWriter)
        {
            ExposeAllElementsForSerialization(soapResponse);
            stringWriter.WriteLine("------Response------");
            soapResponseSerializer.Serialize(stringWriter, soapResponse);
            stringWriter.WriteLine();
        }

        /// <summary>
        /// Writes exception into stringWriter.
        /// </summary>
        /// <param name="exception">The exception to be logged.</param>
        /// <param name="stringWriter">StringWriter object used to perform the conversion.</param>
        private static void WriteString(System.Exception exception, StringWriter stringWriter)
        {
            stringWriter.WriteLine("------Exception------");
            stringWriter.WriteLine(exception.ToString());
            stringWriter.WriteLine();
        }

        /// <summary>
        /// Exposes all relevant soapResponse properties for serialization.
        /// </summary>
        /// <param name="soapResponse">The soap response to be modified.</param>
        private static void ExposeAllElementsForSerialization(processResponse soapResponse)
        {
            soapResponse.NumReviewsSpecified = true;
            soapResponse.limit1Specified = true;
            soapResponse.limit2Specified = true;
            soapResponse.limit3Specified = true;
            soapResponse.limit4Specified = true;
            if (soapResponse.Borrowers != null)
            {
                soapResponse.Borrowers.All(b => { b.LFicoSpecified = true; return true; });
            }
        }
    }
}
