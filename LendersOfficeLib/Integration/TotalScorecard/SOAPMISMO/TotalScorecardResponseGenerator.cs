﻿// <copyright file="TotalScorecardResponseGenerator.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Isaac Ribakoff
//  Date:   4/19/2015 05:49:00 PM 
// </summary>
namespace LendersOffice.Integration.TotalScorecard.SOAPMISMO
{
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.TOTALScorecard.MISMO;
    using TotalScoreCard;

    /// <summary>
    /// Static class used to wrap SOAP-MISMO TOTAL Scorecard responses in legacy/REST protocol TOTAL Scorecard responses.
    /// </summary>
    internal static class TotalScorecardResponseGenerator
    {
        /// <summary>
        /// Wraps a SOAP-MISMO TOTAL Scorecard response in a legacy/REST protocol TOTAL Scorecard response.
        /// </summary>
        /// <param name="soapResponse">A SOAP-MISMO TOTAL Scorecard response object.</param>
        /// <returns>A legacy/REST protocol TOTAL Scorecard response object.</returns>
        internal static TotalScorecardResponse ConvertToRestResponse(processResponse soapResponse)
        {
            TotalScorecardResponse restResponse = new TotalScorecardResponse();
            restResponse.NumReviews = soapResponse.NumReviews.ToString();
            restResponse.ErrCde = soapResponse.Errcde ?? string.Empty;
            restResponse.ReviewRules = soapResponse.ReviewRules ?? string.Empty;
            restResponse.PostReview = soapResponse.PostReview ?? string.Empty;
            restResponse.PreReview = soapResponse.PreReview ?? string.Empty;
            restResponse.Version = soapResponse.Version ?? string.Empty;
            restResponse.LoanNumber = soapResponse.loan_number ?? string.Empty;
            restResponse.MipBumpIndicator = soapResponse.MIPBumpIndicator ?? string.Empty;
            if (soapResponse.Borrowers != null && soapResponse.Borrowers.Length > 0)
            {
                restResponse.BorrowerList = CreateRestBorrowers(soapResponse.Borrowers).ToList();
            }

            return restResponse;
        }

        /// <summary>
        /// Wraps an array of SOAP-MISMO TOTAL Scorecard response borrower elements in a collection of legacy/REST protocol TOTAL Scorecard response borrower elements.
        /// </summary>
        /// <param name="soapBorrowers">An array of SOAP-MISMO TOTAL Scorecard Response borrower elements.</param>
        /// <returns>A translated collection of legacy/REST protocol TOTAL Scorecard Response borrower elements.</returns>
        private static IEnumerable<TotalScorecardResponseBorrower> CreateRestBorrowers(processResponseBorrower[] soapBorrowers)
        {
            return soapBorrowers.Select(b => CreateRestBorrower(b));
        }

        /// <summary>
        /// Wraps a SOAP-MISMO TOTAL Scorecard response borrower in a legacy/REST protocol TOTAL Scorecard response borrower.
        /// </summary>
        /// <param name="soapBorrower">A SOAP-MISMO TOTAL Scorecard Response borrower object.</param>
        /// <returns>A translated legacy/REST protocol TOTAL Scorecard Response borrower object.</returns>
        private static TotalScorecardResponseBorrower CreateRestBorrower(processResponseBorrower soapBorrower)
        {
            TotalScorecardResponseBorrower restBorrower = new TotalScorecardResponseBorrower();
            restBorrower.LFico = soapBorrower.LFico.ToString();
            restBorrower.Ssn = soapBorrower.ssn ?? string.Empty;
            restBorrower.Repository = soapBorrower.repository ?? string.Empty;

            return restBorrower;
        }
    }
}
