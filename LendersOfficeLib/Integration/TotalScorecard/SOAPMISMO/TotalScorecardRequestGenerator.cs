﻿// <copyright file="TotalScorecardRequestGenerator.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Isaac Ribakoff
//  Date:   4/16/2015 05:03:00 PM 
// </summary>
namespace LendersOffice.Integration.TotalScorecard.SOAPMISMO
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.TOTALScorecard.MISMO;
    using TotalScoreCard;

    /// <summary>
    /// Static class used to wrap legacy/REST protocol TOTAL Scorecard requests in SOAP-MISMO TOTAL Scorecard requests.
    /// </summary>
    public static class TotalScorecardRequestGenerator
    {
        /// <summary>
        /// Wraps a legacy/REST protocol TOTAL Scorecard request in a SOAP-MISMO TOTAL Scorecard request.
        /// </summary>
        /// <param name="restRequest">A legacy/REST protocol TOTAL Scorecard request object.</param>
        /// <param name="loanId">Loan Identifier used to generate FNMA 3.2 or MISMO export.</param>
        /// <returns>A SOAP-MISMO TOTAL Scorecard request object.</returns>
        public static process ConvertFromRestRequest(TotalScorecardRequest restRequest, Guid loanId)
        {
            process soapRequest = new process();
            soapRequest.borrower = CreateBorrowers(restRequest.RetrieveBorrowers());
            soapRequest.creditreport = CreateCreditReports(restRequest.RetrieveCreditReports());
            soapRequest.monthly_income = restRequest.MonthlyIncome;
            soapRequest.appraised_value = restRequest.AppraisedValue;
            soapRequest.assets_after_clsg = restRequest.AssetsAfterClsg;
            soapRequest.fha_case_number = restRequest.FhaCaseNumber;
            soapRequest.loan_amount = restRequest.LoanAmount;
            soapRequest.piti = restRequest.Piti;
            soapRequest.mip = restRequest.Mip;
            soapRequest.term = restRequest.Term;
            soapRequest.sale_price = restRequest.SalePrice;
            soapRequest.applicants = restRequest.Applicants;
            soapRequest.loan_number = restRequest.LoanNumber;
            soapRequest.ltv = restRequest.Ltv;
            soapRequest.front_end_ratio = restRequest.FrontEndRatio;
            soapRequest.back_end_ratio = restRequest.BackEndRatio;
            soapRequest.amort_type = EnumMapping.Map_TotalScorecardRequestAmortType[restRequest.AmortType];
            soapRequest.underwriting_pi = restRequest.UnderwritingPi;
            soapRequest.contract_pi = restRequest.ContractPi;
            soapRequest.underwriting_interest = restRequest.UnderwritingInterest;
            soapRequest.contract_interest = restRequest.ContractInterest;
            soapRequest.borr_marital_status = EnumMapping.Map_TotalScorecardRequestBorrMaritalStatus[restRequest.BorrMaritalStatus];
            soapRequest.first_time_buyer = EnumMapping.Map_TotalScorecardRequestYN[restRequest.FirstTimeBuyer];
            soapRequest.loan_purpose = EnumMapping.Map_TotalScorecardLoanPurpose[restRequest.LoanPurpose];
            soapRequest.prop_address = restRequest.PropAddress;
            soapRequest.prop_city = restRequest.PropCity;
            soapRequest.prop_state = restRequest.PropState;
            soapRequest.prop_zip = restRequest.PropZip;
            soapRequest.prop_county = restRequest.PropCounty;
            soapRequest.self_employ = EnumMapping.Map_TotalScorecardRequestYN[restRequest.SelfEmploy];
            soapRequest.veteran = EnumMapping.Map_TotalScorecardRequestYN[restRequest.Veteran];
            soapRequest.borr_clsg_costs = restRequest.BorrClsgCosts;
            soapRequest.borr_type = EnumMapping.Map_TotalScorecardBorrType[restRequest.BorrType];
            soapRequest.monthly_expense = restRequest.MonthlyExpense;
            soapRequest.eem = EnumMapping.Map_TotalScorecardRequestYN[restRequest.Eem];
            soapRequest.first_pay_date = restRequest.FirstPayDate;
            soapRequest.gift_amt = restRequest.GiftAmt;
            soapRequest.gift_source = EnumMapping.Map_TotalScorecardGiftSource[restRequest.GiftSource];
            soapRequest.dependents = restRequest.Dependents;
            soapRequest.req_invest = restRequest.ReqInvest;
            soapRequest.solar = EnumMapping.Map_TotalScorecardRequestYN[restRequest.Solar];
            soapRequest.total_req = restRequest.TotalReq;
            soapRequest.years_at_job = restRequest.YearsAtJob;
            soapRequest.years_renting = restRequest.YearsRenting;
            soapRequest.clsg_costs = restRequest.ClsgCosts;
            soapRequest.unpaid_balance = restRequest.UnpaidBalance;
            soapRequest.mortgage_basis = restRequest.MortgageBasis;
            soapRequest.assets_avail = restRequest.AssetsAvail;
            soapRequest.total_fixed = restRequest.TotalFixed;
            soapRequest.counsel_type = EnumMapping.Map_TotalScorecardCounselType[restRequest.CounselType];
            soapRequest.closing_date = restRequest.ClosingDate;
            soapRequest.maturity_date = restRequest.MaturityDate;
            soapRequest.buydown_interest = restRequest.BuydownInterest;
            soapRequest.secondary_financing_amt = restRequest.SecondaryFinancingAmt;
            soapRequest.secondary_financing_src = EnumMapping.Map_TotalScorecardSecondaryFinancingSrc[restRequest.SecondaryFinancingSrc];
            soapRequest.eem_escrow_amt = restRequest.EemEscrowAmt;
            soapRequest.seller_concessions = restRequest.SellerConcessions;
            soapRequest.version = restRequest.Version;
            soapRequest.lender_id = restRequest.LenderId;
            soapRequest.sponsor_id = restRequest.SponsorId;
            soapRequest.down_payment = restRequest.DownPayment;
            soapRequest.base_mortgage_amount = restRequest.BaseMortgageAmount;
            soapRequest.base_LTV = restRequest.BaseLtv;
            soapRequest.manufactured_housing = EnumMapping.Map_TotalScorecardRequestYN[restRequest.ManufacturedHousing];
            soapRequest.living_units = restRequest.LivingUnits;
            soapRequest.ARM_type = EnumMapping.Map_TotalScorecardArmType[restRequest.ArmType];
            soapRequest.refinance_type = EnumMapping.Map_TotalScorecardRefinanceType[restRequest.RefinanceType];
            if (string.IsNullOrEmpty(restRequest.SponsoredOriginatorEIN) == false)
            {
                soapRequest.LOAN_ORIGINATOR = new processLOAN_ORIGINATOR();
                soapRequest.LOAN_ORIGINATOR._EmployerIdentificationNumber = restRequest.SponsoredOriginatorEIN;
            }

            soapRequest.condo_ind = EnumMapping.Map_TotalScorecardRequestYN[restRequest.CondoInd];
            soapRequest.loanapp = new[] { CreateLoanApp(loanId) };

            return soapRequest;
        }

        /// <summary>
        /// Wraps a collection of legacy/REST protocol TOTAL Scorecard request borrower elements in an array of SOAP-MISMO TOTAL Scorecard request borrower elements.
        /// </summary>
        /// <param name="restBorrowers">A collection of legacy/REST protocol TOTAL Scorecard request borrower elements.</param>
        /// <returns>A translated array of SOAP-MISMO TOTAL Scorecard request borrower elements.</returns>
        private static processBorrower[] CreateBorrowers(IEnumerable<TotalScorecardRequestBorrower> restBorrowers)
        {
            return restBorrowers.Select(b => CreateBorrower(b)).ToArray();
        }

        /// <summary>
        /// Wraps a legacy/REST protocol TOTAL Scorecard request borrower in a SOAP-MISMO TOTAL Scorecard request borrower.
        /// </summary>
        /// <param name="restBorrower">A legacy/REST protocol TOTAL Scorecard request borrower object.</param>
        /// <returns>A translated SOAP-MISMO TOTAL Scorecard request borrower object.</returns>
        private static processBorrower CreateBorrower(TotalScorecardRequestBorrower restBorrower)
        {
            processBorrower soapBorrower = new processBorrower();
            soapBorrower.ssn = restBorrower.Ssn;
            soapBorrower.borr_age = restBorrower.BorrAge;
            soapBorrower.borr_name = restBorrower.BorrName;
            soapBorrower.borr_race = restBorrower.RaceList.Select(r => EnumMapping.Map_TotalScorecardBorrRace[r]).ToArray();
            soapBorrower.borr_sex = EnumMapping.Map_TotalScorecardBorrSex[restBorrower.BorrSex];
            soapBorrower.borr_ethnicity = EnumMapping.Map_TotalScorecardRequestYN[restBorrower.BorrEthnicity];
            soapBorrower.borr_birth_date = restBorrower.BorrBirthDate;

            return soapBorrower;
        }

        /// <summary>
        /// Wraps a collection of legacy/REST protocol TOTAL Scorecard request credit report elements in an array of SOAP-MISMO TOTAL Scorecard request credit report elements.
        /// </summary>
        /// <param name="restCreditReports">A collection of legacy/REST protocol TOTAL Scorecard request credit report elements.</param>
        /// <returns>A translated array of SOAP-MISMO TOTAL Scorecard request credit report elements.</returns>
        private static processCreditreport[] CreateCreditReports(IEnumerable<TotalScorecardRequestCreditReport> restCreditReports)
        {
            return restCreditReports.Select(c => CreateCreditReport(c)).ToArray();
        }

        /// <summary>
        /// Wraps a legacy/REST protocol TOTAL Scorecard request credit report in a SOAP-MISMO TOTAL Scorecard request credit report.
        /// </summary>
        /// <param name="restCreditReport">A legacy/REST protocol TOTAL Scorecard request credit report object.</param>
        /// <returns>A translated SOAP-MISMO TOTAL Scorecard request credit report object.</returns>
        private static processCreditreport CreateCreditReport(TotalScorecardRequestCreditReport restCreditReport)
        {
            processCreditreport soapCreditReport = new processCreditreport();
            soapCreditReport.creditreporttype = EnumMapping.Map_TotalScorecardRequestCreditReportType[restCreditReport.CreditReportType];
            soapCreditReport.creditreportdata = restCreditReport.CreditReportData;

            return soapCreditReport;
        }

        /// <summary>
        /// Creates process loan application object populated with loan file MISMO data for SOAP request.
        /// </summary>
        /// <param name="loanId">Loan Identifier used to generate FNMA 3.2 or MISMO export.</param>
        /// <returns>A process loan application object populated with loan file MISMO data.</returns>
        private static processLoanapp CreateLoanApp(Guid loanId)
        {
            processLoanapp loanApp = new processLoanapp();
            loanApp.loanapptype = processLoanappLoanapptype.FannieMaeRLD32;
            loanApp.loanapptypeSpecified = true;
            LendersOffice.Conversions.FannieMae32Exporter exporter = new LendersOffice.Conversions.FannieMae32Exporter(loanId);
            loanApp.loanappdata = exporter.ExportToString();

            return loanApp;
        }
    }
}