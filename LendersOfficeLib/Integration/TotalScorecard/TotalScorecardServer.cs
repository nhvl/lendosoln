﻿namespace LendersOffice.Integration.TotalScorecard
{
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Xml;
    using DataAccess;
    using TotalScoreCard;

    public class TotalScorecardServer
    {
        public static TotalScorecardResponse Submit(string userName, string password, string url, TotalScorecardRequest request)
        {
            StringBuilder logBuffer = new StringBuilder();
            logBuffer.AppendLine("[FHATotalScoreCard]");

            try
            {
                byte[] bytes = null;

                using (MemoryStream stream = new MemoryStream(5000))
                {
                    using (XmlTextWriter writer = new XmlTextWriter(stream, System.Text.Encoding.ASCII))
                    {
                        writer.WriteProcessingInstruction("xml", "version=\"1.0\" standalone=\"yes\"");
                        request.WriteXml(writer);
                    }

                    bytes = stream.ToArray();

                    System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
                    string str = enc.GetString(bytes);

                    logBuffer.AppendLine("------Request------");
                    logBuffer.AppendLine(str);
                    logBuffer.AppendLine();
                }

                string bytesString = System.Text.Encoding.ASCII.GetString(bytes);

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.KeepAlive = false;
                webRequest.ContentType = "text/xml";
                webRequest.Method = "POST";
                //webRequest.ContentLength = bytes.Length; // Removing per OPM 52869

                using (Stream stream = webRequest.GetRequestStream())
                {
                    stream.Write(bytes, 0, bytes.Length);
                }
                webRequest.Credentials = new NetworkCredential(userName, password);

                StringBuilder sb = new StringBuilder();
                using (WebResponse webResponse = webRequest.GetResponse())
                {
                    using (Stream stream = webResponse.GetResponseStream())
                    {
                        byte[] buffer = new byte[6000];
                        int size = stream.Read(buffer, 0, buffer.Length);
                        while (size > 0)
                        {
                            sb.Append(System.Text.Encoding.UTF8.GetString(buffer, 0, size));
                            size = stream.Read(buffer, 0, buffer.Length);
                        }
                    }
                }
                logBuffer.AppendLine("------Response------");
                logBuffer.AppendLine(sb.ToString());
                logBuffer.AppendLine();

                TotalScorecardResponse response = new TotalScorecardResponse();
                using (XmlReader reader = XmlReader.Create(new StringReader(sb.ToString())))
                {
                    response.ReadXml(reader);
                }
                return response;
            }
            catch (WebException exc)
            {
                logBuffer.AppendLine("------Exception------");
                logBuffer.AppendLine(exc.ToString());
                logBuffer.AppendLine();
                HttpWebResponse _res = exc.Response as HttpWebResponse;
                if (null != _res)
                {
                    if (_res.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        throw new PasswordErrorException();
                    }
                }
                throw;
            }
            finally
            {
                Tools.LogInfo(logBuffer.ToString());
            }
        }
    }
}
