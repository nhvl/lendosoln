﻿namespace LendersOffice.Integration.TotalScorecard
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Common;

    public static class ErrorCodeHandler
    {

        public class FHATotalError
        {
            public string Code { get; set; }
            public string Message { get; set; }
            public string Recommendation { get; set; }
        }

        private static Dictionary<string, FHATotalError> m_errorCodes = new Dictionary<string, FHATotalError>();
        private static string m_fileName = "FHAErrorCodes.xml.config";
        
        static ErrorCodeHandler()
        {
            if (m_errorCodes.Count != 0)
            {
                return;
            }
            
            LoadDictionaryFromXml();
        }

        static StreamReader GetConfigFileStream()
        {
            Assembly assembly = typeof(SchemaFolder).Assembly;
            return new StreamReader(assembly.GetManifestResourceStream("LendersOffice.Integration.TotalScorecard." + m_fileName));
        }

        public static FHATotalError GetError(string errorCode)
        {
            FHATotalError error;

            if (m_errorCodes.TryGetValue(errorCode.TrimWhitespaceAndBOM(), out error))
            {
                return error; 
            }
            
            throw new CBaseException("Unable to retrieve FHA errors, please contact us if this happens again.", "Unhandled FHA error code: " + errorCode);
        }

        public static string GetMessage(string errorCode)
        {
            FHATotalError error = GetError(errorCode);
            return error.Message;
        }

        private static void LoadDictionaryFromXml()
        {
            XmlDocument doc = new XmlDocument();
            StreamReader reader = null;
            try
            {
                reader = GetConfigFileStream();
                doc.Load(reader);
            }
            catch (Exception e)
            {
                Tools.LogError("Unable to retrieve FHA errors from XML file: " + e.ToString());
            }
            finally
            {
                reader.Close();
            }
            
            XmlElement root = (XmlElement)doc.SelectSingleNode("//FHAErrorCodes");

            if (root == null)
            {
                return;
            }

            foreach (XmlElement errorElement in root.ChildNodes) //FHAErrorCodes//error
            {
                try
                {
                    var error = new FHATotalError()
                    {
                        Code =  errorElement.GetAttribute("errorcode"),
                        Message = errorElement.GetAttribute("message"),
                        Recommendation = errorElement.GetAttribute("recommendation")
                    };
                    m_errorCodes.Add(error.Code, error);
                }
                catch (Exception e)
                {
                    Tools.LogBug("Unable to load all FHA Error messages from XML file: " + e.ToString());
                }
            }
        }
    }
}
