﻿namespace LendersOffice.Integration.TotalScorecard
{
    using System;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Integration.TotalScorecard.SOAPMISMO;
    using LendersOffice.Integration.Underwriting;
    using LendersOffice.ObjLib.Audit;
    using LendersOffice.Security;
    using LendersOfficeApp.los.RatePrice;
    using TotalScoreCard;

    public enum E_DataSourcePage
    {
        LeaveBlank = 0,
        Form_1003 = 1,
        FHA_Transmittal_Summary = 2
    }
    
    public class CreditRiskEvaluation
    {
        public bool SumbitToTotalScoreOnly(Guid sLId, E_CalcModeT calcModeT, E_DataSourcePage dataConflictPage, out string serverCacheKey)
        {
            serverCacheKey = "";

            CFHATotalAuditData dataLoan = new CFHATotalAuditData(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sTotalScoreDataConflictSourceT = dataConflictPage;

            #region Create TOTAL Scorecard request
            switch (calcModeT)
            {
                case E_CalcModeT.LendersOfficePageEditing:
                    // No-OP
                    break;
                case E_CalcModeT.PriceMyLoan:
                    dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
                    dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);
                    dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                    break;
                default:
                    throw new UnhandledEnumException(calcModeT);
            }
            TotalScorecardRequest totalScorecardRequest = TotalScorecardRequestBase.GenerateTotalScorecardRequest(dataLoan);
            #endregion

            #region Determine REST or SOAP protocol, use TOTAL test or production environment.
            bool totalScorecardSOAPMISMOEnabled = ConstStage.TotalScorecardSOAPMISMOEnabled;
            BrokerDB brokerDB = BrokerDB.RetrieveById(dataLoan.sBrokerId);
            bool useFHATotalProductionAccount = brokerDB.UseFHATOTALProductionAccount;
            #endregion

            #region Submit to TOTAL
            TotalScorecardResponse totalScorecardResponse;
            if (totalScorecardSOAPMISMOEnabled)
            {
                totalScorecardResponse = TotalScorecardProcessor.ProcessUsingLegacyRESTWrappers(totalScorecardRequest, sLId, useFHATotalProductionAccount);
            }
            else
            {
                string login, pw, restUrl;
                RetrieveLoginInfo(out login, out pw, out restUrl, useFHATotalProductionAccount);
                totalScorecardResponse = TotalScorecardServer.Submit(login, pw, restUrl, totalScorecardRequest);
            }

            string errorCodesStr = totalScorecardResponse.ErrCde;
            string[] errorCodes = errorCodesStr.Split(',');
           
            foreach (string errorCode in errorCodes)
            {
                switch (errorCode)
                {
                    //case "435": // 9/12/2012 dd - Allow these error to return instead of throw exception.
                    //case "450":// 9/12/2012 dd - Allow these error to return instead of throw exception.
                    case "470":
                        //case "490":// 9/12/2012 dd - Allow these error to return instead of throw exception.
                        //case "495":// 9/12/2012 dd - Allow these error to return instead of throw exception.
                        ErrorCodeHandler.FHATotalError error = ErrorCodeHandler.GetError(errorCode);
                        string userErrorMsg = String.Format("{0}\n{1}", error.Message, error.Recommendation);
                        string devErrorMsg = "Dev Error in FHA integration " + errorCode + " Full: " + errorCodesStr;
                        throw new CBaseException(userErrorMsg, devErrorMsg);
                    default:   //not fatal
                        break;
                }
            }

            dataLoan.sTotalScoreCreditRiskResultT = MapRiskResultType(totalScorecardResponse.PostReview);
            dataLoan.sTotalScorePreReviewResultT = MapRiskResultType(totalScorecardResponse.PreReview);

            dataLoan.sTotalScoreVersion = totalScorecardResponse.Version;
            dataLoan.sTotalScoreReviewRules = totalScorecardResponse.ReviewRules;

            dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
            dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);
            dataLoan.Save();

            if (totalScorecardResponse.ErrCde.Length > 0)
            {
                serverCacheKey = AutoExpiredTextCache.AddToCache(totalScorecardResponse.ErrCde, TimeSpan.FromMinutes(60));
                return false;
            }
            #endregion

            return true;
        }



        /// <summary>
        /// Submit to TOTAL Score system and then run pricing on Total loan program.
        /// </summary>
        /// <param name="loanID"></param>
        /// <param name="calcMode"></param>
        /// <param name="dataConflictPage"></param>
        /// <param name="serverCacheKey"></param>
        /// <returns></returns>
        public bool Evaluate(Guid loanID, E_CalcModeT calcMode, E_DataSourcePage dataConflictPage, out string serverCacheKey)
        {
            bool bSubmitSuccessful = SumbitToTotalScoreOnly(loanID, calcMode, dataConflictPage, out serverCacheKey);

            if (bSubmitSuccessful == false)
            {
                return false;
            }

            RunTotalScorePricing(loanID, calcMode);

            return true;
        }

        public void RunTotalScorePricing(Guid sLId, E_CalcModeT calcMode)
        {
            CFHATotalAuditData dataLoan = new CFHATotalAuditData(sLId);
            dataLoan.CalcModeT = calcMode;
            dataLoan.InitLoad();

            // Run pricing with returned TOTAL Scorecard results
            CApplicantPriceXml applicantPriceXml = TotalScorecardPricer.PriceLoan(dataLoan.sLId);

            #region Generate the full TOTAL fundings for display via the TOTAL certificate
            TotalScorecardFindings findings = new TotalScorecardFindings(applicantPriceXml, dataLoan);
            string certXml = findings.GenerateFindingsXml();
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sTotalScorecardFirstRunD_rep = findings.SubmitDate.ToString("G");
            dataLoan.sTotalScorecardLastRunD_rep = findings.SubmitDate.ToString("G");
            dataLoan.sTotalScoreCertificateXmlContent = certXml;
            dataLoan.sAusFindingsPull = true; // OPM 90052: Set it when we generate the certificate
            dataLoan.sTotalScoreEvalStatusT = applicantPriceXml.Status;

            if (dataLoan.BrokerDB.IsTotalAutoPopulateAUSResponse)
            {
                dataLoan.sProd3rdPartyUwResultT = GetsProd3rdPartyUwResultT(findings.CreditRiskAssessment, findings.EligilityAssessment);
            }

            dataLoan.Save();
            #endregion

            #region Create audit event for the TOTAL submission
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            AbstractUserPrincipal userPrincipal = context.User as AbstractUserPrincipal;
            E_DataSourcePage dataConflictPage = dataLoan.sTotalScoreDataConflictSourceT;
            LendersOffice.Audit.AbstractAuditItem audit = new FHATotalScoreCardAuditItem(userPrincipal, (dataConflictPage == E_DataSourcePage.Form_1003), (dataConflictPage == E_DataSourcePage.FHA_Transmittal_Summary), certXml, dataLoan.sAgencyCaseNum);
            AuditManager.RecordAudit(dataLoan.sLId, audit);
            #endregion

            AusResultHandler.CreateSystemTotalOrder(
                findings.FHACaseNumber,
                sLId,
                findings.EligilityAssessment,
                findings.CreditRiskAssessment,
                userPrincipal);
        }

        private E_sProd3rdPartyUwResultT  GetsProd3rdPartyUwResultT(E_sTotalScoreCreditRiskResultT result, E_EvalStatus elgibStatus )
        {
            if (result == E_sTotalScoreCreditRiskResultT.Approve && elgibStatus == E_EvalStatus.Eval_Eligible)
            {
                return E_sProd3rdPartyUwResultT.Total_ApproveEligible;
            }
            else if( result == E_sTotalScoreCreditRiskResultT.Approve && elgibStatus == E_EvalStatus.Eval_Ineligible ) 
            {
                return E_sProd3rdPartyUwResultT.Total_ApproveIneligible;
            }
            else if ( result == E_sTotalScoreCreditRiskResultT.Refer &&  elgibStatus == E_EvalStatus.Eval_Eligible)
            {
                return E_sProd3rdPartyUwResultT.Total_ReferEligible;
            }
            else if (result == E_sTotalScoreCreditRiskResultT.Refer && elgibStatus == E_EvalStatus.Eval_Ineligible)
            {
                return E_sProd3rdPartyUwResultT.Total_ReferIneligible;
            }
            else
            {
                return E_sProd3rdPartyUwResultT.OutOfScope;
            }
        }

        private static E_sTotalScoreCreditRiskResultT MapRiskResultType(string postReview)
        {
            switch (postReview.TrimWhitespaceAndBOM())
            {
                case "U":
                    return E_sTotalScoreCreditRiskResultT.Unknown;
                case "A":
                    return E_sTotalScoreCreditRiskResultT.Approve;
                case "R":
                    return E_sTotalScoreCreditRiskResultT.Refer;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unhandled value for TotalScorecardResponse - PostReview: " + postReview);
            }
        }

        public static void GenerateTemporaryTotalScoreCertificate(Guid sLId, Func<CPageData, E_sTotalScoreCreditRiskResultT> TotalOutcome)
        {
            

            CPageData dataLoan = new CFHATotalAuditData(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            E_sTotalScoreCreditRiskResultT old_sTotalScoreCerditRiskResultT = dataLoan.sTotalScoreCreditRiskResultT;
            E_sTotalScoreFhaProductT old_sTotalScoreFhaProductT = dataLoan.sTotalScoreFhaProductT;

            decimal old_sTotalScorecardUpfrontMipFactor = dataLoan.sTotalScorecardUpfrontMipFactor;
            decimal old_sTotalScorecardAnnualMipFactor = dataLoan.sTotalScorecardAnnualMipFactor;
            /*
             //opm 47226 fs 03/30/10
             The Upfront MIP is simple enough: always populate with 2.25.
             For the annual MIP, ideally we would calculate the correct value:
                If TERM > 180
                  If LTV > 95 Then annual MIP = 0.55%
                  If LTV <= 95 Then annual MIP = 0.50%
                If TERM <= 180
                  If LTV > 90 Then annual MIP = 0.25%
                  If LTV <= 90 Then annual MIP = 0.00%
             */
            dataLoan.sTotalScorecardUpfrontMipFactor = (decimal)2.25;

            if (dataLoan.sTerm > 180)
                dataLoan.sTotalScorecardAnnualMipFactor = (dataLoan.sLtvR > 95) ? (decimal)0.55 : (decimal)0.5;
            else
                dataLoan.sTotalScorecardAnnualMipFactor = (dataLoan.sLtvR > 90) ? (decimal)0.25 : (decimal)0;

            if (old_sTotalScoreFhaProductT == E_sTotalScoreFhaProductT.LeaveBlank)
            {
                dataLoan.sTotalScoreFhaProductT = E_sTotalScoreFhaProductT.Standard;
            }

            dataLoan.sTotalScoreCreditRiskResultT = TotalOutcome(dataLoan);

            dataLoan.Save();

            CApplicantPriceXml applicantPriceXml = TotalScorecardPricer.PriceLoan(sLId);
            TotalScorecardFindings findings = new TotalScorecardFindings(applicantPriceXml, dataLoan);

            dataLoan = new CFHATotalAuditData(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            
            // Revert back value.
            dataLoan.sTotalScoreTempCertificateXmlContent = findings.GenerateFindingsXml();
            dataLoan.sTotalScoreFhaProductT = old_sTotalScoreFhaProductT;
            dataLoan.sTotalScoreCreditRiskResultT = old_sTotalScoreCerditRiskResultT;
            dataLoan.sTotalScorecardUpfrontMipFactor = old_sTotalScorecardUpfrontMipFactor;
            dataLoan.sTotalScorecardAnnualMipFactor = old_sTotalScorecardAnnualMipFactor;
            dataLoan.Save();
        }


        private static void RetrieveLoginInfo(out string login, out string pw, out string restUrl, bool useFHATotalProductionAccount)
        {
            if (useFHATotalProductionAccount)
            {
                login = ConstStage.FHATOTALProductionLogin;
                pw = ConstStage.FHATOTALProductionPassword;
                restUrl = ConstStage.FHATOTALProductionURL;
            }
            else
            {
                login = ConstStage.FHATOTALTestLogin;
                pw = ConstStage.FHATOTALTestPassword;
                restUrl = ConstStage.FHATOTALTestURL;
            }
        }

        private static E_sTotalScoreCreditRiskResultT defaultDUDelegate(CPageData dataLoan)
        {
            switch (dataLoan.sProd3rdPartyUwResultT)
            {
                case E_sProd3rdPartyUwResultT.DU_ApproveEligible:
                case E_sProd3rdPartyUwResultT.DU_ApproveIneligible:
                    return E_sTotalScoreCreditRiskResultT.Approve;
                case E_sProd3rdPartyUwResultT.DU_ReferEligible:
                case E_sProd3rdPartyUwResultT.DU_ReferIneligible:
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible:
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible:
                    return E_sTotalScoreCreditRiskResultT.Refer;
                case E_sProd3rdPartyUwResultT.NA:
                case E_sProd3rdPartyUwResultT.DU_EAIEligible:
                case E_sProd3rdPartyUwResultT.DU_EAIIEligible:
                case E_sProd3rdPartyUwResultT.DU_EAIIIEligible:
                case E_sProd3rdPartyUwResultT.LP_AcceptEligible:
                case E_sProd3rdPartyUwResultT.LP_AcceptIneligible:
                case E_sProd3rdPartyUwResultT.LP_CautionEligible:
                case E_sProd3rdPartyUwResultT.LP_CautionIneligible:
                case E_sProd3rdPartyUwResultT.OutOfScope:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level1:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level2:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level3:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level4:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level5:
                case E_sProd3rdPartyUwResultT.Lp_Refer:
                case E_sProd3rdPartyUwResultT.Total_ApproveEligible:
                case E_sProd3rdPartyUwResultT.Total_ApproveIneligible:
                case E_sProd3rdPartyUwResultT.Total_ReferEligible:
                case E_sProd3rdPartyUwResultT.Total_ReferIneligible:
                    return E_sTotalScoreCreditRiskResultT.Unknown;
                default:
                    throw new UnhandledEnumException(dataLoan.sProd3rdPartyUwResultT);
            }
        }

        
        public static void GenerateTemporaryTotalScoreCertificate(Guid sLId) {

            GenerateTemporaryTotalScoreCertificate(sLId, defaultDUDelegate);

        }
    }
}
