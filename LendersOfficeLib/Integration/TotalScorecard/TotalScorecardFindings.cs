namespace LendersOffice.Integration.TotalScorecard
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    using System.Xml.Linq;
    using CommonProjectLib.Common.Lib;
    using DataAccess;
    using Toolbox;
    using TotalScoreCard;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.CreditReport;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Reminders;
    using LendersOffice.Security;
    using LendersOfficeApp.los.RatePrice;

    #region Value Mapping
    internal static class ValueMapping
    {
        internal static Dictionary<E_sTotalScoreCreditRiskResultT, string> Map_TotalScorecardDecision = new Dictionary<E_sTotalScoreCreditRiskResultT, string>()
        {
            { E_sTotalScoreCreditRiskResultT.Unknown, "Unknown" },
            { E_sTotalScoreCreditRiskResultT.Approve, "Accept" },
            { E_sTotalScoreCreditRiskResultT.Refer, "Refer" },
        };

        internal static Dictionary<E_sOccT, string> Map_TotalScorecardOccT = new Dictionary<E_sOccT, string>()
        {
            { E_sOccT.Investment, "Investment" },
            { E_sOccT.PrimaryResidence, "Primary Residence" },
            { E_sOccT.SecondaryResidence, "Secondary Residence" },
        };

        internal static Dictionary<E_EvalStatus, string> Map_TotalScorecardEligibilityAssessment = new Dictionary<E_EvalStatus, string>()
        {
            { E_EvalStatus.Eval_Ineligible, "Ineligible" },
            { E_EvalStatus.Eval_Eligible, "Eligible" },
            { E_EvalStatus.Eval_HideFromResult, "" },
            { E_EvalStatus.Eval_InsufficientInfo, "Insufficient Info" }
        };

        internal static Dictionary<string, string> Map_TotalScorecardReviewRules = new Dictionary<string, string>()
        {
            //in case theres another key not found go to http://www.hud.gov/pub/chums/aus-developers-guide.pdf and search for 07/08 av opm  64566
            { "01", "Front-End Ratio" },
            { "02", "Back-end Ratio" },
            { "04", "Foreclosure" },
            { "05", "Bankruptcy" },
            { "06", "Late Mortgage Payments" },
            { "07", "Ineligible for FHA financing based on values submitted to TOTAL - See Upfront Premiums Chart" },
            { "08", "Ineligible for FHA financing based on values submitted to TOTAL unless the loan is 203(h) or complies with Mortgagee Letter 2010-23 'FHA Refinance of Borrowers in Negative Equity Positions'"},
            { "09", "Current Housing Expense exceeds Monthly Income."},
            { "10", "Gift Amount exceeds 150% of Required Investment."},
            { "11", "Seller Concession Amount exceeds 6% of Sale Price."},
            { "12", "There is more than one 30-day late mortgage payment in the last 12 months and the transaction is a cash-out refinance."},
            { "13", "There are one or more borrowers without an available credit score."},
            { "14", "The lowest median FICO score is less than 620 and the Back-End Ratio (DTI) is greater than 43.00. Please see ML 2013-05."},
            { "15", "Required Investment is less than zero and the loan is a purchase transaction."},
        };
    }

    #endregion
    
    public class TotalScorecardFindings : AbstractXmlSerializable
    {
        #region Variables
        public string[] CreditReviewRules = null;
        public IEnumerable<CStipulation> IneligibilityReasons = new List<CStipulation>();
        public List<string> ApprovalCondtitions = new List<string>();

        public DateTime SubmitDate { get; set; }
        public string FHACaseNumber { get; set; }
        public string TotalLoanNumber { get; set; }
        public string LenderID { get; set; }
        public string SponsorID { get; set; }
        public E_sTotalScoreCreditRiskResultT CreditRiskAssessment { get; set; }
        public E_EvalStatus EligilityAssessment { get; set; }
        public string PrimaryBorrower { get; set; }
        public string RepresentativeScore { get; set; }
        public string TotalMonthlyIncome { get; set; }
        public string SalesPrice { get; set; }
        public string AppraisedValue { get; set; }
        public string LoanAmount { get; set; }
        public string TotalAssets { get; set; }
        public string UpfrontMIPFinanced { get; set; }
        public string TotalMortgagePayment { get; set; }
        public string TotalLoanAmount { get; set; }
        public string TotalFixedPayment { get; set; }
        public string InterestRate { get; set; }
        public string LoanPurpose { get; set; }
        public string ActSection { get; set; }
        public string Term { get; set; }
        public string Due { get; set; }
        public string LTV { get; set; }
        public string CLTV { get; set; }
        public string TopRatio { get; set; }
        public string BottomRatio { get; set; }
        public string AmortizationType { get; set; }
        public string ARM1stChange { get; set; }
        public string AnnualMIRate { get; set; }
        public string QRate { get; set; }
        public string QualifyingMortgagePayment { get; set; }
        public string QualifyingRatios { get; set; }
        public string UpfrontMIPRate { get; set; }
        public string UpfrontMIP { get; set; }
        public string CashFromToBorrower { get; set; }
        public string AssetsAfterClosing { get; set; }
        public string SellerContributionAmount { get; set; }
        public string SellerContributionPercent { get; set; }
        public string BorrowersTotalMonthlyIncome { get; set; }
        public string BorrowersTotalAssets { get; set; }
        public string BorrowersTotalNonMortgagePayments { get; set; }
        public string TotalScorecardVersion { get; set; }
        public string PrimaryResidenceMonthlyHousingExpense { get; set; }
        public string MortgageInsurance { get; set; }
        public string OtherHousingExpenses { get; set; }
        public string OtherMonthlyExpenses { get; set; }
        public string QualifyingPI { get; set; }
        public string Reserves { get; set; }
        public string NetRentalIncome { get; set; }
        public Toolbox.CSortedListOfGroups ApprovalConditions { get;set; }
        public string NoteRate { get; set; }
        public string ContractPI { get; set; }
        public CPageData DataLoan { get; set; }
        public string sCreditScoreType2 { get; set; }
        public string sH4HPresentMortgageTopRatio { get; set; }
        public string sProThisMPmt { get; set; }
        public string sH4HPresentMortgagePaymentTotal { get; set; }
        public string LenderName { get; set; }
        public string SubmittedBy { get; set; }
        public string sTotGiftFundAsset { get; set; }
        public string sPrimaryGiftFundSource { get; set; }
        public string sTotalScorecardBorrPaidCc { get; set; }
        public string sTotEstCcNoDiscnt1003 { get; set; }

        public string sLTotBaseI { get; set; }
        public string sLTotOvertimeI { get; set; }
        public string sLTotBonusesI { get; set; }
        public string sLTotCommisionI { get; set; }
        public string sLTotDividendI { get; set; }
        public string sLTotNetRentI1003 { get; set; }
        public string sLTotSpPosCfPlussLTotOI { get; set; }
        #endregion

        // Adding for H4H
        public TotalScorecardFindings(CApplicantPriceXml applicantPriceXml, CPageData dataLoan, string submittedBy) : this(applicantPriceXml, dataLoan)
        {
            SubmittedBy = submittedBy;
        }
        
        public TotalScorecardFindings(CApplicantPriceXml applicantPriceXml, CPageData dataLoan)
        {
            
            dataLoan.SetFormatTarget(FormatTarget.Webform);
            DataLoan = dataLoan;
            CreditReviewRules = dataLoan.sTotalScoreReviewRules.Split(',');
            LenderName = dataLoan.BranchNm;
            IneligibilityReasons = applicantPriceXml.DenialReasons;
            ApprovalConditions = applicantPriceXml.Stips;
            SubmitDate = DateTime.Now;
            FHACaseNumber = dataLoan.sAgencyCaseNum;
            TotalLoanNumber = dataLoan.sTotalScoreUniqueLoanId;
            LenderID = dataLoan.sFHALenderIdCode;
            SponsorID = dataLoan.sFHASponsorAgentIdCode;
            NoteRate = dataLoan.sNoteIR_rep;
            CreditRiskAssessment = dataLoan.sTotalScoreCreditRiskResultT; // A, R, or U
            MortgageInsurance = dataLoan.sProMIns_rep;
            ContractPI = dataLoan.sTotalScoreContractMPmt_rep;
            QualifyingPI = dataLoan.sTotalScoreUnderwritingMPmt_rep;
            OtherHousingExpenses = dataLoan.sTotalScoreOtherHousingExpenses_rep;
            EligilityAssessment = applicantPriceXml.Status;
            RepresentativeScore = dataLoan.sCreditScoreType2_rep;
            TotalMonthlyIncome = dataLoan.sTotalScoreMonthlyIncome_rep;
            SalesPrice = dataLoan.sPurchPrice_rep;
            AppraisedValue = dataLoan.sApprVal_rep;
            LoanAmount = dataLoan.sLAmtCalc_rep;
            OtherMonthlyExpenses = dataLoan.sTotalScoreOtherMonthlyExpenses_rep;
            TotalAssets = dataLoan.sTotalScoreAssetsAvail_rep;
            Reserves = dataLoan.sTotalScoreReserves_rep;
            UpfrontMIPFinanced = dataLoan.sFfUfmipFinanced_rep;
            NetRentalIncome = dataLoan.sTotalScoreNetRentalIncome_rep;
            TotalMortgagePayment = dataLoan.sTotalScoreQualifyingMonthlyPmt_rep;
            TotalLoanAmount = dataLoan.sFinalLAmt_rep;

            TotalFixedPayment = dataLoan.sTotalScoreTotalFixedPayment_rep;
            
            InterestRate = dataLoan.sNoteIR_rep; ;
            LoanPurpose = dataLoan.sLPurposeT_rep;
            ActSection = dataLoan.sTotalScoreFhaSectionOfTheAct;
            Term = dataLoan.sTerm_rep;
            Due = dataLoan.sDue_rep;
            
            if (dataLoan.BrokerDB.BrokerID == new Guid("baf07293-f626-44c6-9750-706199a7c0b6") )// REMN (PML0200)
            {
                LTV = dataLoan.sTotalScoreLtvR_rep;
                CLTV = dataLoan.sTotalScoreCLtvR_rep;
            }
            else
            {
                LTV = dataLoan.sLtvR_rep;
                CLTV = dataLoan.sCltvR_rep;
            }
            TopRatio = dataLoan.sTotalScoreFrontEndRatio_rep;
            BottomRatio = dataLoan.sTotalScoreBackEndRatio_rep;
            
            AmortizationType = dataLoan.sFinMethT_rep;
            ARM1stChange = dataLoan.sTotalScoreArm1stChangeDisplay;
            AnnualMIRate = dataLoan.sProMInsR_rep;

            QRate = dataLoan.sTotalScoreUnderwritingNoteIR_rep;
            
            QualifyingMortgagePayment = dataLoan.sTotalScoreQualifyingMonthlyPmt_rep;
            QualifyingRatios = dataLoan.sTotalScoreQualifyingTopR_BottomR;
            UpfrontMIPRate = dataLoan.sFfUfmipR_rep;
            UpfrontMIP = dataLoan.sFfUfmip1003_rep;
            CashFromToBorrower = dataLoan.sTransNetCash_rep;
            AssetsAfterClosing = dataLoan.sTotalScoreAssetsAfterClosing_rep;
            SellerContributionAmount = dataLoan.sTotalScoreSellerConcessions_rep;

            SellerContributionPercent = dataLoan.sTotalScoreSellerConcessionsPc_rep;
            BorrowersTotalMonthlyIncome = dataLoan.sLTotI_rep;
            BorrowersTotalNonMortgagePayments = dataLoan.sLiaMonLTot_rep;

            sCreditScoreType2 = dataLoan.sCreditScoreType2_rep;
            sProThisMPmt = dataLoan.sProThisMPmt_rep;
            sH4HPresentMortgagePaymentTotal = dataLoan.sH4HPresentMortgagePaymentTotal_rep;
            sH4HPresentMortgageTopRatio = dataLoan.sH4HPresentMortgageTopRatio_rep;

            TotalScorecardVersion = dataLoan.sTotalScoreVersion;
            SetAppSpecificData(dataLoan);

            if (dataLoan.sOccT == E_sOccT.PrimaryResidence && dataLoan.sPresLTotPersistentHExp == 0)
            {
                PrimaryResidenceMonthlyHousingExpense = "xslt_hide"; // Per Doug's request in case 41253, hide this if the "if" criteria are met
            }
            else
            {
                PrimaryResidenceMonthlyHousingExpense = dataLoan.sPresLTotPersistentHExp_rep;
            }

            sTotGiftFundAsset = dataLoan.sTotGiftFundAsset_rep;
            switch (dataLoan.sPrimaryGiftFundSource)
            {
                case E_GiftFundSourceT.Blank:
                    sPrimaryGiftFundSource =  "N/A";
                    break;
                default:
                    sPrimaryGiftFundSource = dataLoan.sPrimaryGiftFundSource.ToString();
                    break;
            }

            sTotalScorecardBorrPaidCc = dataLoan.sTotalScorecardBorrPaidCc_rep;
            sTotEstCcNoDiscnt1003 = dataLoan.sTotEstCcNoDiscnt1003_rep;

            sLTotBaseI = dataLoan.sLTotBaseI_rep;
            sLTotOvertimeI = dataLoan.sLTotOvertimeI_rep;
            sLTotBonusesI = dataLoan.sLTotBonusesI_rep;
            sLTotCommisionI = dataLoan.sLTotCommisionI_rep;
            sLTotDividendI = dataLoan.sLTotDividendI_rep;
            sLTotNetRentI1003 = dataLoan.sLTotNetRentI1003_rep;
            sLTotSpPosCfPlussLTotOI = dataLoan.sLTotSpPosCfPlussLTotOI_rep;           
        }

        private void SetAppSpecificData(CPageData dataLoan)
        {
            CAppData dataApp = null;
            string tempPrimaryBorrowerName = "";
            for (int nApps = 0; nApps < dataLoan.nApps; nApps++)
            {
                dataApp = dataLoan.GetAppData(nApps);
                if (nApps == 0)
                {
                    tempPrimaryBorrowerName = dataApp.aBNm;
                }
                if (dataApp.aIsPrimary)
                {
                    PrimaryBorrower = dataApp.aBNm;
                }
            }

            // If PML does not have the primary borrower set, use the borrower name from the first app
            if (PrimaryBorrower == "")
            {
                PrimaryBorrower = tempPrimaryBorrowerName;
            }

            BorrowersTotalAssets = dataLoan.sAsstLiqTotal_rep;
        }

        private string GetPrimaryBorrowerName(CPageData dataLoan)
        {
            CAppData dataApp = null;

            for (int nApps = 0; nApps < dataLoan.nApps; nApps++)
            {
                dataApp = dataLoan.GetAppData(nApps);
                if (dataApp.aBIsValidNameSsn)
                {
                    return dataApp.aBNm;
                }

                if (dataApp.aCIsValidNameSsn)
                {
                    return dataApp.aCNm;
                }
            }

            return "";
        }

        public string GenerateFindingsXml()
        {
            string bytesString = "";

            using (MemoryStream stream = new MemoryStream(5000))
            {
                XmlTextWriter xmlWriter = new XmlTextWriter(stream, Encoding.ASCII);
                xmlWriter.WriteProcessingInstruction("xml", "version=\"1.0\" standalone=\"yes\"");
                WriteXml(xmlWriter);
                xmlWriter.Flush();
                bytesString = System.Text.Encoding.ASCII.GetString(stream.GetBuffer(), 0, (int)stream.Position);
            }
            
            return bytesString;
        }

        public override void  ReadXml(XmlReader reader)
        {
 	        throw new NotImplementedException();
        }

        private string GetFTHBCounselingDesc(E_aTotalScoreFhtbCounselingT fthbDescT)
        {
            switch (fthbDescT)
            {
                case E_aTotalScoreFhtbCounselingT.HudApprovedCounseling:
                    return "HUD Approved Counseling";
                case E_aTotalScoreFhtbCounselingT.LeaveBlank:
                    return "";
                case E_aTotalScoreFhtbCounselingT.NotApplicable:
                    return "N/A";
                case E_aTotalScoreFhtbCounselingT.NotCounseled:
                    return "No Counseling";
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unhandled E_aTotalScoreFhtbCounselingT type in TotalScorecardFindings: " + fthbDescT);
            }
        }

        private string GetCounselingTypeDesc(E_aTotalScoreFhtbCounselingT fthbDescT)
        {
            switch (fthbDescT)
            {
                case E_aTotalScoreFhtbCounselingT.HudApprovedCounseling:
                    return "Approved";
                case E_aTotalScoreFhtbCounselingT.LeaveBlank:
                    return "";
                case E_aTotalScoreFhtbCounselingT.NotApplicable:
                    return "N/A";
                case E_aTotalScoreFhtbCounselingT.NotCounseled:
                    return "Not Counseled";
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unhandled E_aTotalScoreFhtbCounselingT type in TotalScorecardFindings: " + fthbDescT);
            }
        }

        private string GetOccTDesc(E_sOccT occT)
        {
            switch (occT)
            {
                case E_sOccT.Investment:
                    return "Investment Property";
                case E_sOccT.PrimaryResidence:
                    return "Primary Residence";
                case E_sOccT.SecondaryResidence:
                    return "Secondary Residence";
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unhandled E_sOccT: " + occT);
            }
        }

        private string GetPropTypeDesc(E_sProdSpT propType)
        {
            switch (propType)
            {
                case E_sProdSpT.Commercial:
                case E_sProdSpT.Condo:
                case E_sProdSpT.Manufactured:
                case E_sProdSpT.Modular:
                case E_sProdSpT.PUD:
                case E_sProdSpT.Rowhouse:
                case E_sProdSpT.Townhouse:
                    return propType.ToString();
                case E_sProdSpT.CoOp:
                    return "Co-op";
                case E_sProdSpT.FourUnits:
                    return "Four Units";
                case E_sProdSpT.MixedUse:
                    return "Mixed Use";
                case E_sProdSpT.SFR:
                    return "Single Family Residence";
                case E_sProdSpT.ThreeUnits:
                    return "Three Units";
                case E_sProdSpT.TwoUnits:
                    return "Two Units";
                default:
                    throw new UnhandledEnumException(propType);
            }
        }

        private bool HasCreditReviewRules()
        {
            foreach (string reviewRule in CreditReviewRules)
            {
                if (reviewRule.TrimWhitespaceAndBOM() != "")
                {
                    return true;
                }
            }

            return false;
        }

        public override void  WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("Findings");

            SafeElementWrite(writer, "Date", SubmitDate.ToString());
            SafeElementWrite(writer, "FHACaseNumber", FHACaseNumber);
            SafeElementWrite(writer, "TotalLoanNumber", TotalLoanNumber);
            SafeElementWrite(writer, "LenderID", LenderID);
            SafeElementWrite(writer, "SponsorID", SponsorID);
            SafeElementWrite(writer, "CreditRiskAssessment", CreditRiskAssessment, ValueMapping.Map_TotalScorecardDecision);
            SafeElementWrite(writer, "EligibilityAssessment", EligilityAssessment, ValueMapping.Map_TotalScorecardEligibilityAssessment);
            
            SafeElementWrite(writer, "ContractPI", ContractPI);
            SafeElementWrite(writer, "NetRentalIncome", NetRentalIncome);
            SafeElementWrite(writer, "QualifyingPI", QualifyingPI);
            SafeElementWrite(writer, "MortgageInsurance", MortgageInsurance);
            SafeElementWrite(writer, "OtherHousingExpenses", OtherHousingExpenses);
            SafeElementWrite(writer, "OtherMonthlyExpenses", OtherMonthlyExpenses);
            SafeElementWrite(writer, "Reserves", Reserves);

            SafeElementWrite(writer, "sCreditScoreType2", sCreditScoreType2);
            SafeElementWrite(writer, "sProThisMPmt", sProThisMPmt);
            SafeElementWrite(writer, "sH4HPresentMortgageTopRatio", sH4HPresentMortgageTopRatio);
            SafeElementWrite(writer, "sH4HPresentMortgagePaymentTotal", sH4HPresentMortgagePaymentTotal);
            SafeElementWrite(writer, "LenderName", LenderName);
            SafeElementWrite(writer, "SubmittedBy", SubmittedBy);
            SafeElementWrite(writer, "Total3", "Total3"); //use new asset table

            #region Loan Information
            writer.WriteStartElement("LoanInformation");
            SafeElementWrite(writer, "PrimaryBorrower", PrimaryBorrower);
            SafeElementWrite(writer, "RepresentativeScore", RepresentativeScore);
            SafeElementWrite(writer, "TotalMonthlyIncome", TotalMonthlyIncome);
            SafeElementWrite(writer, "SalesPrice", SalesPrice);
            SafeElementWrite(writer, "AppraisedValue", AppraisedValue);
            SafeElementWrite(writer, "LoanAmount", LoanAmount);
            SafeElementWrite(writer, "TotalAssets", TotalAssets);
            SafeElementWrite(writer, "UpfrontMIPFinanced", UpfrontMIPFinanced);
            SafeElementWrite(writer, "TotalMortgagePayment", TotalMortgagePayment);
            SafeElementWrite(writer, "TotalLoanAmount", TotalLoanAmount);
            SafeElementWrite(writer, "TotalFixedPayment", TotalFixedPayment);
            SafeElementWrite(writer, "InterestRate", InterestRate);
            SafeElementWrite(writer, "LoanPurpose", LoanPurpose);
            SafeElementWrite(writer, "ActSection", ActSection);
            SafeElementWrite(writer, "Term", Term);
            SafeElementWrite(writer, "Due", Due);
            SafeElementWrite(writer, "LTV", LTV);
            SafeElementWrite(writer, "CLTV", CLTV);
            SafeElementWrite(writer, "TopRatio", TopRatio);
            SafeElementWrite(writer, "BottomRatio", BottomRatio);
            SafeElementWrite(writer, "AmortizationType", AmortizationType);
            SafeElementWrite(writer, "Arm1stChange", ARM1stChange);
            SafeElementWrite(writer, "AnnualMiRate", AnnualMIRate);
            SafeElementWrite(writer, "QRate", QRate);
            SafeElementWrite(writer, "QualifyingMortgagePayment", QualifyingMortgagePayment);
            SafeElementWrite(writer, "QualifyingRatios", QualifyingRatios);
            SafeElementWrite(writer, "UpfrontMIPRate", UpfrontMIPRate);
            SafeElementWrite(writer, "UpfrontMIP", UpfrontMIP);
            SafeElementWrite(writer, "CashFromToBorrower", CashFromToBorrower);
            SafeElementWrite(writer, "AssetsAfterClosing", AssetsAfterClosing);
            SafeElementWrite(writer, "SellerContributionAmount", SellerContributionAmount);
            SafeElementWrite(writer, "SellerContributionPercent", SellerContributionPercent);
            SafeElementWrite(writer, "NoteRate", NoteRate);
            SafeElementWrite(writer, "sTotGiftFundAsset", sTotGiftFundAsset);
            SafeElementWrite(writer, "sPrimaryGiftFundSource", sPrimaryGiftFundSource);
            SafeElementWrite(writer, "sTotalScorecardBorrPaidCc", sTotalScorecardBorrPaidCc);
            SafeElementWrite(writer, "sTotEstCcNoDiscnt1003", sTotEstCcNoDiscnt1003);
            SafeElementWrite(writer, "sLTotBaseI", sLTotBaseI);
            SafeElementWrite(writer, "sLTotOvertimeI", sLTotOvertimeI);
            SafeElementWrite(writer, "sLTotBonusesI", sLTotBonusesI);
            SafeElementWrite(writer, "sLTotCommisionI", sLTotCommisionI);
            SafeElementWrite(writer, "sLTotDividendI", sLTotDividendI);
            SafeElementWrite(writer, "sLTotNetRentI1003", sLTotNetRentI1003);
            SafeElementWrite(writer, "sLTotSpPosCfPlussLTotOI", sLTotSpPosCfPlussLTotOI);

            writer.WriteEndElement();// </LoanInformation>
            #endregion

            #region Credit Review Rules
            if (HasCreditReviewRules())
            {
                writer.WriteStartElement("CreditReviewRules");
                foreach (string reviewRule in CreditReviewRules)
                {
                    if (reviewRule.TrimWhitespaceAndBOM() != "")
                    {
                        SafeElementWrite(writer, "rule", reviewRule.TrimWhitespaceAndBOM(), ValueMapping.Map_TotalScorecardReviewRules);
                    }
                }
                writer.WriteEndElement(); // </CreditReviewRules>
            }
            #endregion

            #region Ineligibility Information
            if (IneligibilityReasons != null && IneligibilityReasons.Count() > 0)
            {
                writer.WriteStartElement("IneligibilityReasons");

                foreach (CStipulation denialReason in IneligibilityReasons)
                {
                    SafeElementWrite(writer, "reason", denialReason.DescriptionForCondition);
                }
                writer.WriteEndElement();// </IneligibilityReasons>

            }
            #endregion

            #region Approval Conditions
            writer.WriteStartElement("ApprovalConditions");
            
            // Taken from CertificateXmlData.cs
            if (DataLoan.BrokerDB.IsUseNewTaskSystem)
            {
                List<Task> conditions = Task.GetAllConditionsByLoanId(DataLoan.BrokerDB.BrokerID, DataLoan.sLId);

                List<Toolbox.CStipulation> miscList = new List<Toolbox.CStipulation>();
                int condNum = 1;

                foreach (string category in ApprovalConditions.Keys)
                {
                    if (category == null)
                    {
                        continue;
                    }

                    if (category.Equals("CREDIT", StringComparison.OrdinalIgnoreCase))
                    {

                    }
                    
                    if (category != CApplicantPrice.MISC_CATEGORY_STR)
                    {
                        writer.WriteStartElement("Category");
                        writer.WriteAttributeString("name", category);

                        if (category.Equals("CREDIT", StringComparison.OrdinalIgnoreCase))
                        {
                            condNum = WriteCreditReportNumber(writer, condNum);
                            condNum =  WriteCreditConditions(writer, condNum);
                        }
                        else if (category.Equals("ASSETS", StringComparison.OrdinalIgnoreCase))
                        {
                            condNum = WriteAssets(writer, condNum);
                        }

                        foreach (Toolbox.CStipulation stip in ApprovalConditions.GetGroupByKeyAndSort(category))
                        {
                            if (IsCreditIgnoreStip(category, stip))
                            {
                                continue;
                            }
                            Task condition = conditions.FirstOrDefault(
                                task => task.CondCategoryId_rep.Equals(category, StringComparison.OrdinalIgnoreCase)
                                    && task.TaskSubject.Equals(stip.DescriptionForCondition, StringComparison.OrdinalIgnoreCase));
                            string doneDate = string.Empty;
                            if (condition != null)
                            {
                                //if (condition.CondIsDeleted)
                                //{
                                //    continue; // This stip's condition was previously deleted.
                                //}
                                if (condition.TaskStatus == E_TaskStatus.Closed && condition.TaskClosedDate.HasValue)
                                {
                                    doneDate = condition.TaskClosedDate.Value.ToShortDateString();
                                }
                            }

                            writer.WriteStartElement("condition");
                            writer.WriteAttributeString("number", condNum++.ToString());
                            if (doneDate != string.Empty)
                            {
                                writer.WriteAttributeString("donedate", doneDate);
                            }
                            //writer.WriteAttributeString("debug", stip.DebugString);
                            writer.WriteString(stip.DescriptionForCert);
                            writer.WriteEndElement();
                        }
                        writer.WriteEndElement(); // end Category element
                    }
                    else
                    {
                        foreach (Toolbox.CStipulation stip in ApprovalConditions.GetGroupByKeyAndSort(category))
                        {
                            miscList.Add(stip);
                        }
                    }

                    if (miscList.Count > 0)
                    {
                        foreach (Toolbox.CStipulation stip in miscList)
                        {
                            Task condition = conditions.FirstOrDefault(
                                task => task.CondCategoryId_rep.Equals(CApplicantPrice.MISC_CATEGORY_STR, StringComparison.OrdinalIgnoreCase)
                                    && task.TaskSubject.Equals(stip.DescriptionForCondition, StringComparison.OrdinalIgnoreCase));

                            string doneDate = string.Empty;
                            if (condition != null)
                            {
                            //    if (condition.CondIsDeleted)
                            //    {
                            //        continue; // This stip's condition was previously deleted.
                            //    }
                                if (condition.TaskStatus == E_TaskStatus.Closed && condition.TaskClosedDate.HasValue)
                                {
                                    doneDate = condition.TaskClosedDate.Value.ToShortDateString();
                                }
                            }

                            if (stip.DescriptionForCert == null)
                            {
                                continue;
                            }
                            writer.WriteStartElement("Category");
                            writer.WriteAttributeString("name", CApplicantPrice.MISC_CATEGORY_STR);

                            writer.WriteStartElement("condition");
                            writer.WriteAttributeString("number", condNum++.ToString());
                            if (doneDate != string.Empty)
                            {
                                writer.WriteAttributeString("donedate", doneDate);
                            }
                            //writer.WriteAttributeString("debug", stip.DebugString);
                            writer.WriteString(stip.DescriptionForCert);
                            writer.WriteEndElement();
                            writer.WriteEndElement();

                        }
                    }
                }
            }

            else
            {
                LendersOffice.Reminders.LoanConditionSetObsolete conditions = new LendersOffice.Reminders.LoanConditionSetObsolete();
                conditions.RetrieveAll(DataLoan.sLId, false); // All conditions, no need for filedb notes

                ArrayList miscList = new ArrayList();
                int condNum = 1;

                foreach (string category in ApprovalConditions.Keys)
                {
                    if (category == null)
                    {
                        continue;
                    }

                    if (category != CApplicantPrice.MISC_CATEGORY_STR)
                    {
                        writer.WriteStartElement("Category");
                        writer.WriteAttributeString("name", category);
                        if (category.Equals("CREDIT", StringComparison.OrdinalIgnoreCase))
                        {
                            condNum = WriteCreditReportNumber(writer, condNum);
                            condNum = WriteCreditConditions(writer, condNum);
                        }
                        else if (category.Equals("ASSETS", StringComparison.OrdinalIgnoreCase))
                        {
                            condNum = WriteAssets(writer, condNum);
                        }
                        foreach (Toolbox.CStipulation stip in ApprovalConditions.GetGroupByKeyAndSort(category))
                        {
                            if (IsCreditIgnoreStip(category, stip))
                            {
                                continue;
                            }
                            LendersOffice.Reminders.CLoanConditionObsolete condition = conditions.GetMatch(category, stip.DescriptionForCondition);
                            string doneDate = string.Empty;
                            if (condition != null)
                            {
                                //if (!condition.CondIsValid)
                                //{
                                //    continue; // This stip's condition was previously deleted.
                                //}
                                if (condition.CondStatus == LendersOffice.Reminders.E_CondStatus.Done)
                                {
                                    doneDate = condition.CondStatusDate.ToShortDateString();
                                }
                            }

                            writer.WriteStartElement("condition");
                            writer.WriteAttributeString("number", condNum++.ToString());
                            if (doneDate != string.Empty)
                            {
                                writer.WriteAttributeString("donedate", doneDate);
                            }
                            //writer.WriteAttributeString("debug", stip.DebugString);
                            writer.WriteString(stip.DescriptionForCert);
                            writer.WriteEndElement();
                        }
                        writer.WriteEndElement(); // end Category element
                    }
                    else
                    {
                        foreach (Toolbox.CStipulation stip in ApprovalConditions.GetGroupByKeyAndSort(category))
                        {
                            miscList.Add(stip);
                        }
                    }

                    if (miscList.Count > 0)
                    {
                        foreach (Toolbox.CStipulation stip in miscList)
                        {
                            LendersOffice.Reminders.CLoanConditionObsolete condition = conditions.GetMatch(CApplicantPrice.MISC_CATEGORY_STR, stip.DescriptionForCondition);
                            string doneDate = string.Empty;
                            if (condition != null)
                            {
                                //if (!condition.CondIsValid)
                                //{
                                //    continue; // This stip's condition was previously deleted.
                                //}
                                if (condition.CondStatus == LendersOffice.Reminders.E_CondStatus.Done)
                                {
                                    doneDate = condition.CondStatusDate.ToShortDateString();
                                }
                            }

                            if (stip.DescriptionForCert == null)
                            {
                                continue;
                            }
                            writer.WriteStartElement("Category");
                            writer.WriteAttributeString("name", CApplicantPrice.MISC_CATEGORY_STR);

                            writer.WriteStartElement("condition");
                            writer.WriteAttributeString("number", condNum++.ToString());
                            if (doneDate != string.Empty)
                            {
                                writer.WriteAttributeString("donedate", doneDate);
                            }
                            //writer.WriteAttributeString("debug", stip.DebugString);
                            writer.WriteString(stip.DescriptionForCert);
                            writer.WriteEndElement();
                            writer.WriteEndElement();

                        }
                    }
                }
            }
            writer.WriteEndElement();// </ApprovalConditions>
            #endregion

            #region Borrowers
            writer.WriteStartElement("Borrowers");
            writer.WriteAttributeString("TotalMonthlyIncome", BorrowersTotalMonthlyIncome);
            writer.WriteAttributeString("TotalAssets", BorrowersTotalAssets);
            writer.WriteAttributeString("TotalNonMortgagePayments", BorrowersTotalNonMortgagePayments);
            
            if (PrimaryResidenceMonthlyHousingExpense != "xslt_hide")
            {
                writer.WriteAttributeString("PrimaryResidenceMonthlyHousingExpense", PrimaryResidenceMonthlyHousingExpense);
            }
            
            CAppData dataApp = null;

            for (int nApps = 0; nApps < DataLoan.nApps; nApps++)
            {
                writer.WriteStartElement("application");
                
                dataApp = DataLoan.GetAppData(nApps);
                writer.WriteAttributeString("number", (nApps+1).ToString()); // The indexing starts at 1 for the display

                if (dataApp.aIsPrimary)
                {
                    writer.WriteAttributeString("isPrimaryApp", "true");
                }

                if (dataApp.aBIsValidNameSsn)
                {
                    writer.WriteAttributeString("borrower", dataApp.aBNm);
                    writer.WriteAttributeString("borrowerSsn", dataApp.aBSsnMasked);
                    writer.WriteAttributeString("borrowerCitizenship", dataApp.aBCitizenDescription);
                    writer.WriteAttributeString("borrowerIsSelfEmployed", (dataApp.aBIsSelfEmplmt)?"Yes":"No");
                    writer.WriteAttributeString("borrowerCreditScore", string.Format("XP: {0} TU: {1} EF: {2}", dataApp.aBExperianScore_rep, dataApp.aBTransUnionScore_rep, dataApp.aBEquifaxScore_rep));
                    writer.WriteAttributeString("borrowerIsOcc", dataApp.aBDecOcc == "Y" ? "Yes" : "No");
                    writer.WriteAttributeString("borrowerIsFTHB", (dataApp.aBTotalScoreIsFthb) ? "Yes" : "No");
                    writer.WriteAttributeString("borrowerFTHBCounseling", GetFTHBCounselingDesc(dataApp.aBTotalScoreFhtbCounselingT));
                    writer.WriteAttributeString("aBH4HNetworthLessRetirement", dataApp.aBH4HNetworthLessRetirement_rep);
                }

                if (dataApp.aCIsValidNameSsn)
                {
                    writer.WriteAttributeString("coborrower", dataApp.aCNm);
                    writer.WriteAttributeString("coborrowerSsn", dataApp.aCSsnMasked);
                    writer.WriteAttributeString("coborrowerCitizenship", dataApp.aCCitizenDescription);
                    writer.WriteAttributeString("coborrowerIsSelfEmployed", (dataApp.aCIsSelfEmplmt) ? "Yes" : "No");
                    writer.WriteAttributeString("coborrowerCreditScore", string.Format("XP: {0} TU: {1} EF: {2}", dataApp.aCExperianScore_rep, dataApp.aCTransUnionScore_rep, dataApp.aCEquifaxScore_rep));
                    writer.WriteAttributeString("coborrowerIsOcc", dataApp.aCDecOcc == "Y" ? "Yes" : "No");
                    writer.WriteAttributeString("coborrowerIsFTHB", (dataApp.aCTotalScoreIsFthb) ? "Yes" : "No");
                    writer.WriteAttributeString("coborrowerFTHBCounseling", GetFTHBCounselingDesc(dataApp.aCTotalScoreFhtbCounselingT));
                    writer.WriteAttributeString("aCH4HNetworthLessRetirement", dataApp.aCH4HNetworthLessRetirement_rep);
                }

                writer.WriteAttributeString("MonthlyIncome", dataApp.aTotI_rep);
                writer.WriteAttributeString("Assets", dataApp.aAsstLiqTot_rep);
                writer.WriteAttributeString("NonMortgagePayment", dataApp.aLiaMonTot_rep);

                writer.WriteEndElement(); // </application>
            }

            writer.WriteEndElement();// </Borrowers>
            #endregion

            #region PropertyInfo
            writer.WriteStartElement("PropertyInfo");
            writer.WriteAttributeString("state", DataLoan.sSpState);
            writer.WriteAttributeString("type", GetPropTypeDesc(DataLoan.sProdSpT));
            writer.WriteAttributeString("structureType", DataLoan.sProdSpStructureT.ToString());
            writer.WriteAttributeString("occRate", DataLoan.sOccR_rep);
            writer.WriteAttributeString("grossRent", DataLoan.sGrossProfit_rep);
            writer.WriteAttributeString("proposedPropertyTax", DataLoan.sProRealETx_rep);
            writer.WriteAttributeString("otherProposedHousingExpense", DataLoan.sProOHExp_rep);
            writer.WriteAttributeString("address", DataLoan.sSpAddr); 
            writer.WriteAttributeString("city", DataLoan.sSpCity);
            writer.WriteAttributeString("zip", DataLoan.sSpZip);
            writer.WriteAttributeString("county", DataLoan.sSpCounty);
            writer.WriteAttributeString("numberOfStories", DataLoan.sProdCondoStories_rep);
            writer.WriteAttributeString("isInRural", (DataLoan.sProdIsSpInRuralArea)?"Yes":"No");
            writer.WriteAttributeString("isCondotel", (DataLoan.sProdIsCondotel) ? "Yes" : "No");
            writer.WriteAttributeString("isNon-WarrantableProj", (DataLoan.sProdIsNonwarrantableProj) ? "Yes" : "No");
            writer.WriteAttributeString("purpose", GetOccTDesc(DataLoan.sOccT));

            writer.WriteEndElement();// </PropertyInfo>
            #endregion
            SafeElementWrite(writer, "TotalScoreVersion", TotalScorecardVersion);

            writer.WriteEndElement(); // </Findings>

        }

        private int WriteCreditReportNumber(XmlWriter writer, int number)
        {
            List<CreditNumberInfo> items = new List<CreditNumberInfo>();
            

            for (int appIndex = 0; appIndex < DataLoan.nApps; appIndex++)
            {
                CAppData app = DataLoan.GetAppData(appIndex);
                SqlParameter[] parameters = { new SqlParameter("@ApplicationID", app.aAppId) };
                
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataLoan.BrokerDB.BrokerID, "RetrieveCreditReport", parameters))
                {
                    if (reader.Read())
                    {

                        ICreditReport report = app.CreditReportData.Value;
                        string dateIssued = app.aCrRd_rep;

                        if (report != null)
                        {
                            dateIssued = report.CreditReportFirstIssuedDate.ToShortDateString();
                        }
                        
                        Guid lastCra;
                        string fileId = (string)reader["ExternalFileID"];
                        if (reader["CrAccProxyId"] is DBNull)
                        {
                            lastCra = (Guid)reader["ComId"];
                        }
                        else
                        {
                            lastCra = (Guid)reader["CrAccProxyId"];
                        }


                        items.Add(new CreditNumberInfo()
                        {
                            AppNumber = appIndex,
                            BorrowerName = GetOwnerName(E_AssetOwnerT.Joint, appIndex).ToUpper(),
                            DateIssued = dateIssued,
                            ReportNumber = fileId,
                            CRAId = lastCra
                        });
                    }
                }
            }

            Dictionary<Guid, string> craNames = new Dictionary<Guid, string>();

            foreach (CreditNumberInfo info in items)
            {
                if (craNames.ContainsKey(info.CRAId))
                {
                    info.CRAName = craNames[info.CRAId];
                    continue;
                }

                CRA cra =  MasterCRAList.FindById(info.CRAId, false);
                if (cra == null)
                {
                    var proxy = CreditReportAccountProxy.Helper.RetrieveCreditProxyByProxyID(this.DataLoan.sBrokerId, info.CRAId);
                    if (proxy != null)
                    {
                        cra = MasterCRAList.FindById(proxy.CraId, false);
                    }
                }

                string vendorName = string.Empty;
                if(cra != null)
                {
                    vendorName = cra.VendorName;
                }

                craNames.Add(info.CRAId, vendorName);
                info.CRAName = vendorName;
            }

            return WriteCreditFileNumber(writer, items, "INFORMATION FROM THE FOLLOWING CREDIT REPORTS WAS USED TO GENERATE THIS TOTAL RECOMMENDATION AND CONDITIONS:", number);
        }



        private int WriteCreditConditions(XmlWriter writer, int number)
        {

            List<CreditLiabilityInfo> usedInDti = new List<CreditLiabilityInfo>();
            List<CreditLiabilityInfo> notUsedInDti = new List<CreditLiabilityInfo>();


            for (int appIndex = 0; appIndex < DataLoan.nApps; appIndex++)
            {

                CAppData currentDataApp = DataLoan.GetAppData(appIndex);
                ILiaCollection liaColl = currentDataApp.aLiaCollection;
                int countLia = liaColl.CountRegular;

                for (int i = 0; i < countLia; ++i)
                {
                    ILiabilityRegular lia = liaColl.GetRegularRecordAt(i);
                    if ( lia.WillBePdOff || lia.DebtT.EqualsOneOf(E_DebtRegularT.Mortgage, E_DebtRegularT.Heloc)) //dont show subject property 
                    {
                        continue;
                    }

            

                    var list = lia.NotUsedInRatio ? notUsedInDti : usedInDti;

                    list.Add(new CreditLiabilityInfo()
                    {
                        AppNumber = appIndex,
                        BorrowerName = GetOwnerName(lia.OwnerT, appIndex).ToUpper(),
                        BorrowerNumber = GetOwnerNumber(lia.OwnerT),
                        CreditorName = lia.ComNm.ToUpper(),
                        MonthlyPayment = lia.Pmt_rep,
                        RemainingBalance = lia.Bal_rep,
                        Type =  lia.DebtT_rep.ToUpper()
                    });
                }
            }

            number += WriteCreditConditionsImpl(writer, usedInDti, "THE MINIMUM MONTHLY PAYMENTS FROM THE FOLLOWING ACCOUNTS HAVE BEEN USED TO CALCULATE THE TOTAL DEBT-TO-INCOME RATIO.", number);
            number += WriteCreditConditionsImpl(writer, notUsedInDti, "THE FOLLOWING ACCOUNTS HAVE BEEN EXCLUDED FROM THE DEBT RATIO CALCULATION.  FOR EACH ACCOUNT, PROVIDE DOCUMENTATION THAT SUPPORTS THE EXCLUSION.", number);
        
            return number;
        }

        private int WriteAssets(XmlWriter writer, int number)
        {
            List<AssetInfo> assets = new List<AssetInfo>();
            for (int appIndex = 0; appIndex < DataLoan.nApps; appIndex++)
            {
                CAppData currentDataApp = DataLoan.GetAppData(appIndex);
                int assetCount = currentDataApp.aAssetCollection.CountRegular;

                for (int i = 0; i < assetCount; ++i)
                {
                    var asset = currentDataApp.aAssetCollection.GetRegularRecordAt(i);

                    assets.Add(new AssetInfo()
                    {
                        Amount = asset.Val_rep,
                        AppNumber = appIndex,
                        AssetType = asset.AssetT_rep.ToUpper(),
                        BorrowerName = GetOwnerName(asset.OwnerT, appIndex).ToUpper(),
                        BorrowerNumber = GetOwnerNumber(asset.OwnerT),
                        CompanyName = asset.ComNm.ToUpper()
                    });
                }
            }

            number += WriteAssetConditionsImpl(writer, assets, "THE FOLLOWING ASSETS WERE USED TO QUALIFY THE BORROWER(S) FOR THIS LOAN.", number);
            return number;
        }

        private int WriteCreditFileNumber(XmlWriter writer, List<CreditNumberInfo> credit, string msg, int number)
        {
            if (credit.Count == 0)
            {
                return 0;
            }

            writer.WriteStartElement("condition");
            writer.WriteAttributeString("number", number.ToString());
            writer.WriteAttributeString("doNotImport", "True");
            writer.WriteAttributeString("type", "CREDITNUMBER1");
            writer.WriteAttributeString("desc", msg);

            foreach (CreditNumberInfo info in credit.OrderBy(p => p.AppNumber))
            {
                writer.WriteStartElement("Entry");
                writer.WriteAttributeString("borrowerName", info.BorrowerName);
                writer.WriteAttributeString("dateIssued", info.DateIssued);
                writer.WriteAttributeString("reportNumber", info.ReportNumber);
                writer.WriteAttributeString("craName", info.CRAName);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            return 1;
        }
        private int WriteCreditConditionsImpl(XmlWriter writer, List<CreditLiabilityInfo> credit, string msg, int number)
        {
            if (credit.Count == 0)
            {
                return 0;
            }

            writer.WriteStartElement("condition");
            writer.WriteAttributeString("number", number.ToString());
            writer.WriteAttributeString("doNotImport", "True");
            writer.WriteAttributeString("type", "CREDIT");
            writer.WriteAttributeString("desc", msg);

            foreach (CreditLiabilityInfo info in credit.OrderBy(p => p.AppNumber).ThenBy(p => p.BorrowerNumber))
            {
                writer.WriteStartElement("Entry");
                writer.WriteAttributeString("borrowerName", info.BorrowerName);
                writer.WriteAttributeString("creditorName", info.CreditorName);
                writer.WriteAttributeString("type", info.Type);
                writer.WriteAttributeString("remainingBalance", info.RemainingBalance);
                writer.WriteAttributeString("monthlyPayment", info.MonthlyPayment);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            return 1;
        }

        private int WriteAssetConditionsImpl(XmlWriter writer, List<AssetInfo> assets, string msg, int number)
        {
            if (assets.Count == 0)
            {
                return 0 ;
            }

            writer.WriteStartElement("condition");
            writer.WriteAttributeString("number", number.ToString());
            writer.WriteAttributeString("doNotImport", "True");
            writer.WriteAttributeString("type", "ASSET");
            writer.WriteAttributeString("desc", msg);

            foreach (AssetInfo info in assets.OrderBy(p => p.AppNumber).ThenBy(p => p.BorrowerNumber))
            {
                writer.WriteStartElement("Entry");
                writer.WriteAttributeString("borrowerName", info.BorrowerName);
                writer.WriteAttributeString("companyName", info.CompanyName);
                writer.WriteAttributeString("assetType", info.AssetType);
                writer.WriteAttributeString("amount", info.Amount);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            return 1;
        }

        private bool IsCreditIgnoreStip(string category, CStipulation stip)
        {
            return category.Equals("CREDIT", StringComparison.OrdinalIgnoreCase) && stip.DescriptionForCert.EndsWith("HAS BEEN EXCLUDED FROM DEBT RATIO. EVIDENCE OR EXPLANATION MUST BE PROVIDED.");
        }

        public string GetOwnerName(E_LiaOwnerT owner, int applicationIndex)
        {
            CAppData app = DataLoan.GetAppData(applicationIndex);

            switch (owner)
            {
                case E_LiaOwnerT.Borrower:
                    return app.aBNm;
                case E_LiaOwnerT.CoBorrower:
                    return app.aCNm;
                case E_LiaOwnerT.Joint:
                    if (app.aCNm.TrimWhitespaceAndBOM() != "")
                        return app.aBNm + " / " + app.aCNm;
                    return app.aBNm;
            }
            return string.Empty;
        }

        public int GetOwnerNumber(E_LiaOwnerT owner)
        {

            switch (owner)
            {
                case E_LiaOwnerT.Borrower:
                    return 0;
                case E_LiaOwnerT.CoBorrower:
                    return 1;
                case E_LiaOwnerT.Joint:
                    return 0;
            }
            return 2;
        }

        public string GetOwnerName(E_AssetOwnerT owner, int applicationIndex)
        {
            CAppData app = DataLoan.GetAppData(applicationIndex);

            switch (owner)
            {
                case E_AssetOwnerT.Borrower:
                    return app.aBNm;
                case E_AssetOwnerT.CoBorrower:
                    return app.aCNm;
                case E_AssetOwnerT.Joint:
                    if (app.aCNm.TrimWhitespaceAndBOM() != "")
                        return app.aBNm + " / " + app.aCNm;
                    return app.aBNm;
            }
            return string.Empty;
        }

        public int GetOwnerNumber(E_AssetOwnerT owner)
        {

            switch (owner)
            {
                case E_AssetOwnerT.Borrower:
                    return 0;
                case E_AssetOwnerT.CoBorrower:
                    return 1;
                case E_AssetOwnerT.Joint:
                    return 0;
            }
            return 2;
        }
    }



    class CreditLiabilityInfo
    {
        public string BorrowerName { get; set; }
        public string CreditorName { get; set; }
        public string Type { get; set; }
        public string RemainingBalance { get; set; }
        public string MonthlyPayment { get; set; }
        public int AppNumber { get; set; }
        public int BorrowerNumber { get; set; }

    }

    class AssetInfo
    {
        public string BorrowerName { get; set; }
        public string AssetType { get; set; }
        public string CompanyName { get; set; }
        public string Amount { get; set; }
        public int AppNumber { get; set; }
        public int BorrowerNumber { get; set; }
    }

    class CreditNumberInfo
    {
        public string BorrowerName { get; set; }
        public int AppNumber { get; set; }
        public int BorrowerNumber { get; set; }

        public string ReportNumber { get; set; }
        public string DateIssued { get; set; }
        public Guid CRAId { get; set; }
        public string CRAName { get; set; }
    }

    public static class TotalCertUtilities
    {

        public static void ImportConditionsFromCert(Guid loanId)
        {
            BrokerDB db = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
            if (db.IsUseNewTaskSystem)
            {
                ImportConditionsToNewTaskSystem(loanId);
            }
            else
            {
                ImportConditionsToOldTaskSystem(loanId);
            }
        }

        private static void ImportConditionsToOldTaskSystem(Guid loanId)
        {
            CPageData data = new CFHATotalAuditData(loanId);
            data.InitLoad();

            XDocument doc = XDocument.Parse(data.sTotalScoreCertificateXmlContent.Value);

            Regex anchor = new Regex("<a href='([^']*)' target='[_]blank' >[^<]*</a>", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex br = new Regex("<br[/]*>", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            LoanConditionSetObsolete ldSet = new LoanConditionSetObsolete();
            ldSet.RetrieveAll(loanId, false);
            var savedConditions = ldSet.Digest;

            var conditionsToImport = new List<CLoanConditionObsolete>();

            foreach (XElement element in doc.Element("Findings").Element("ApprovalConditions").Elements("Category"))
            {
                foreach (XElement condition in element.Elements("condition"))
                {
                    if (condition.Attribute("doNotImport") != null)
                    {
                        continue;
                    }
                    CLoanConditionObsolete cdItem = new CLoanConditionObsolete(data.sBrokerId);
                    cdItem.CondCategoryDesc = "TOTAL-" + element.Attribute("name").Value;
                    cdItem.LoanId = loanId;
                    cdItem.CondDesc = br.Replace(anchor.Replace(condition.Value, "$1"), System.Environment.NewLine);

                    //dont save if there is a condition like this one already. Yes this is slow, but I don't think there should be too many conditions. 
                    if (savedConditions.Any(p =>
                        String.Equals(p.Category, cdItem.CondCategoryDesc, StringComparison.OrdinalIgnoreCase) &&
                        String.Equals(p.Description, cdItem.CondDesc, StringComparison.OrdinalIgnoreCase)))
                    {
                        continue;
                    }

                    conditionsToImport.Add(cdItem);
                }
            }

            if (conditionsToImport.Count > 0)
            {
                using (CStoredProcedureExec exec = new CStoredProcedureExec(data.sBrokerId))
                {
                    try
                    {
                        exec.BeginTransactionForWrite();
                        conditionsToImport.ForEach(p => p.Save(exec));
                        exec.CommitTransaction();
                    }

                    catch (Exception e)
                    {
                        Tools.LogError("Importing FHA Total Submissions", e);
                        exec.RollbackTransaction();
                        throw new CBaseException(ErrorMessages.Generic, "Could not save conditions.");
                    }
                }
            }
        }

        private static void ImportConditionsToNewTaskSystem(Guid loanId)
        {
            CPageData data = new CFHATotalAuditData(loanId);
            data.InitSave(ConstAppDavid.SkipVersionCheck);

            XDocument doc = XDocument.Parse(data.sTotalScoreCertificateXmlContent.Value);

            Regex anchor = new Regex("<a href='([^']*)' target='[_]blank' >[^<]*</a>", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex br = new Regex("<br[/]*>", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            IConditionImporter importer = data.GetConditionImporter("FHA TOTAL SCORECARD");
            importer.IsSkipCategoryCheck = true; // 9/7/2011 dd - OPM 70845 don't check category for condition uniqueness.

            var conditionsToImport = new List<CLoanConditionObsolete>();

            foreach (XElement element in doc.Element("Findings").Element("ApprovalConditions").Elements("Category"))
            {
                foreach (XElement condition in element.Elements("condition"))
                {
                    if (condition.Attribute("doNotImport") != null)
                    {
                        continue;
                    }
                    string category = element.Attribute("name").Value;
                    string desc = br.Replace(anchor.Replace(condition.Value, "$1"), System.Environment.NewLine);
                    importer.AddUnique(category, desc);
                }
            }

            data.Save();
        }
    }
}
