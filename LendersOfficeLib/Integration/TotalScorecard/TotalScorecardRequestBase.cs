﻿namespace LendersOffice.Integration.TotalScorecard
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using DataAccess;
    using TotalScoreCard;

    public class TotalScorecardRequestBase
    {
        public static E_TotalScorecardRequestYN MapYN(bool b)
        {
            return b ? E_TotalScorecardRequestYN.Yes : E_TotalScorecardRequestYN.No;
        }

        public static E_TotalScorecardRequestYN IsCondo(E_sGseSpT sGseSpT)
        {
            switch (sGseSpT)
            {
                case E_sGseSpT.LeaveBlank:
                case E_sGseSpT.Attached:
                case E_sGseSpT.Cooperative:
                case E_sGseSpT.Detached:
                case E_sGseSpT.ManufacturedHomeCondominium:
                case E_sGseSpT.ManufacturedHomeMultiwide:
                case E_sGseSpT.ManufacturedHousing:
                case E_sGseSpT.ManufacturedHousingSingleWide:
                case E_sGseSpT.Modular:
                case E_sGseSpT.PUD:
                    return E_TotalScorecardRequestYN.No;
                case E_sGseSpT.Condominium:
                case E_sGseSpT.DetachedCondominium:
                case E_sGseSpT.HighRiseCondominium:
                    return E_TotalScorecardRequestYN.Yes;
                default:
                    throw new UnhandledEnumException(sGseSpT);
            }
        }

        public static E_TotalScorecardGiftSource Convert(E_GiftFundSourceT sGiftFundSourceT)
        {
            switch (sGiftFundSourceT)
            {
                case E_GiftFundSourceT.Blank:
                    return E_TotalScorecardGiftSource.NA;
                case E_GiftFundSourceT.Relative:
                    return E_TotalScorecardGiftSource.Relative;
                case E_GiftFundSourceT.Government:
                    return E_TotalScorecardGiftSource.GovernmentAssistance;
                case E_GiftFundSourceT.Employer:
                    return E_TotalScorecardGiftSource.Employer;
                case E_GiftFundSourceT.Nonprofit:
                    return E_TotalScorecardGiftSource.NonprofitReligious;
                case E_GiftFundSourceT.Other:
                    return E_TotalScorecardGiftSource.NA;
                default:
                    throw new UnhandledEnumException(sGiftFundSourceT);
            }
        }
        public static E_TotalScorecardRefinanceType Convert(E_sTotalScoreRefiT sTotalScoreRefiT, bool sIsRefinancing)
        {
            if (false == sIsRefinancing)
            {
                return E_TotalScorecardRefinanceType.NotRefinance;
            }
            switch (sTotalScoreRefiT)
            {
                case E_sTotalScoreRefiT.ConventionalToFHA:
                    return E_TotalScorecardRefinanceType.Conventional;
                case E_sTotalScoreRefiT.FHAToFHANonStreamline:
                    return E_TotalScorecardRefinanceType.PriorFHA;
                case E_sTotalScoreRefiT.Streamline:
                    return E_TotalScorecardRefinanceType.StreamlineRefinance;
                case E_sTotalScoreRefiT.HOPEForHomeowners:
                    return E_TotalScorecardRefinanceType.HOPEForHomeowners;
                case E_sTotalScoreRefiT.Unknown:
                case E_sTotalScoreRefiT.LeaveBlank:
                    return E_TotalScorecardRefinanceType.Unknown;
                default:
                    throw new UnhandledEnumException(sTotalScoreRefiT);
            }
        }
        public static E_TotalScorecardBorrSex Convert(E_GenderT genderT)
        {
            switch (genderT)
            {
                case E_GenderT.Female:
                    return E_TotalScorecardBorrSex.Female;
                case E_GenderT.Male:
                    return E_TotalScorecardBorrSex.Male;
                default:
                    // Don't throw exception here because there really shouldn't be anything new added here
                    // and we don't need to handle the "undefined" type of values.
                    return E_TotalScorecardBorrSex.Undefined;
            }
        }

        public static E_TotalScorecardRequestBorrMaritalStatus Convert(E_aBMaritalStatT aBMaritalStatT)
        {
            switch (aBMaritalStatT)
            {
                case E_aBMaritalStatT.Married:
                    return E_TotalScorecardRequestBorrMaritalStatus.Married;
                case E_aBMaritalStatT.NotMarried:
                    return E_TotalScorecardRequestBorrMaritalStatus.Unmarried;
                case E_aBMaritalStatT.Separated:
                    return E_TotalScorecardRequestBorrMaritalStatus.Separated;
                case E_aBMaritalStatT.LeaveBlank:
                    return E_TotalScorecardRequestBorrMaritalStatus.Undefined;
                default:
                    throw new UnhandledEnumException(aBMaritalStatT);
            }
        }
        public static void ApplyExtraBorrowerDataForFHACaseNumFilledOut(TotalScorecardRequestBorrower borrower, CAppData dataApp)
        {

            borrower.BorrAge = dataApp.aAge_rep;
            borrower.BorrName = dataApp.aTotalScoreFormatNm;
            borrower.BorrSex = Convert(dataApp.aGenderFallback);
            borrower.BorrEthnicity = MapYN(dataApp.aHispanicTFallback == E_aHispanicT.Hispanic);
            borrower.BorrBirthDate = dataApp.aDob_rep;

            var list = new[] {
                           new { TotalScoreRace = E_TotalScorecardBorrRace.AmericanIndian, IsRace = dataApp.aIsAmericanIndian},
                           new { TotalScoreRace = E_TotalScorecardBorrRace.Asian, IsRace = dataApp.aIsAsian},
                           new { TotalScoreRace = E_TotalScorecardBorrRace.BlackOrAfricanAmerican, IsRace = dataApp.aIsBlack},
                           new { TotalScoreRace = E_TotalScorecardBorrRace.NativeHawaiian, IsRace = dataApp.aIsPacificIslander},
                           new { TotalScoreRace = E_TotalScorecardBorrRace.White, IsRace = dataApp.aIsWhite},
                           new { TotalScoreRace = E_TotalScorecardBorrRace.NotDisclosed, IsRace = dataApp.aNoFurnish}
                       };
            foreach (var o in list)
            {
                if (o.IsRace)
                {
                    borrower.AddRace(o.TotalScoreRace);
                }
            }

        }

        // From the spec: LO, FHA Case Number is blank – Send only the fields in the ‘Total Submission’ 
        // Fields indicated as ‘Required for Scoring’ (column H).
        // 
        // LO, FHA Case Number is filled out – Send the data from the ‘1003’ 
        // and TOTAL submission fields’ columns. If the ‘FHA Transmittal Summary (HUD-92900-LT)’ 
        // checkbox on the Submit to FHA TOTAL Scorecard screen is checked, include the data from 
        // the ‘FHA Transmittal Summary’ column.
        public static TotalScorecardRequest GenerateTotalScorecardRequest(CPageData dataLoan)
        {
            bool isFHACaseNumFilledOut = !dataLoan.sAgencyCaseNum.Equals("");
            dataLoan.SetFormatTarget(FormatTarget.FannieMaeMornetPlus);

            TotalScorecardRequest totalScorecardRequest = new TotalScorecardRequest();

            #region AppData
            CAppData dataApp = null;

            for (int nApps = 0; nApps < dataLoan.nApps; nApps++)
            {
                dataApp = dataLoan.GetAppData(nApps);

                string fannieMaeCreditXml = dataApp.aCreditReportRawFnmaFormat.Value;
                if (!string.IsNullOrEmpty(fannieMaeCreditXml))
                {
                    TotalScorecardRequestCreditReport creditReport = new TotalScorecardRequestCreditReport();
                    creditReport.CreditReportType = E_TotalScorecardRequestCreditReportType.Fannie;
                    creditReport.CreditReportData = fannieMaeCreditXml;
                    totalScorecardRequest.AddCreditReport(creditReport);
                }
                E_BorrowerModeT[] list = { E_BorrowerModeT.Borrower, E_BorrowerModeT.Coborrower };
                foreach (var borrowerModeT in list)
                {
                    dataApp.BorrowerModeT = borrowerModeT;

                    // OPM 184017. Keep non-purchasing borrowers out of export
                    if (dataLoan.BrokerDB.IsUseNewNonPurchaseSpouseFeature)
                    {
                        if ( (borrowerModeT == E_BorrowerModeT.Borrower && (dataApp.aBTypeT == E_aTypeT.NonTitleSpouse || dataApp.aBTypeT == E_aTypeT.TitleOnly))
                            || (borrowerModeT == E_BorrowerModeT.Coborrower && (dataApp.aCTypeT == E_aTypeT.NonTitleSpouse || dataApp.aCTypeT == E_aTypeT.TitleOnly)))
                        {
                            continue;
                        }
                    }

                    if (dataApp.aIsValidNameSsn)
                    {
                        TotalScorecardRequestBorrower borr = new TotalScorecardRequestBorrower();
                        totalScorecardRequest.AddBorrower(borr);
                        borr.Ssn = dataApp.aSsn;

                        if (isFHACaseNumFilledOut)
                        {
                            ApplyExtraBorrowerDataForFHACaseNumFilledOut(borr, dataApp);
                        }
                    }
                }
            }

            if (dataLoan.sTotalScoreNumberApplicants == 0)
            {
                throw new CBaseException("Unable to submit to FHA TOTAL Scorecard - no borrowers on this loan have a valid (non-empty) Social Security Number.", "Unable to submit to FHA TOTAL Scorecard - no borrowers in this loan have a valid (non-empty) Social Security Number.");
            }

            if (dataLoan.sTotalScoreNumberApplicants > 5)
            {
                throw new CBaseException("Unable to submit to FHA TOTAL Scorecard - there is a maximum of 5 borrowers per loan file.", "Unable to submit to FHA TOTAL Scorecard - there is a maximum of 5 borrowers per loan file.");
            }
            #endregion
            totalScorecardRequest.BorrMaritalStatus = Convert(dataLoan.GetAppData(0).aBMaritalStatT);
            totalScorecardRequest.MonthlyIncome = dataLoan.sTotalScoreMonthlyIncome_rep;

            totalScorecardRequest.AppraisedValue = dataLoan.sTotalScoreAppraisedVal_rep;
            totalScorecardRequest.AssetsAfterClsg = dataLoan.sTotalScoreAssetsAfterClosing_rep;
            totalScorecardRequest.FhaCaseNumber = dataLoan.sTotalScoreFhaCaseNumber;
            totalScorecardRequest.LoanAmount = dataLoan.sFinalLAmt_rep;

            // From the spec: "Use underwriter's shorthand method", which is basically the method we use to calculate sProMIns for non-FHA loans
            // We want to use it here even if it is an FHA loan.
            totalScorecardRequest.Piti = dataLoan.sTotalScoreQualifyingMonthlyPmt_rep;
            totalScorecardRequest.Mip = dataLoan.sFfUfmip1003_rep;
            totalScorecardRequest.Term = dataLoan.sTerm_rep;
            totalScorecardRequest.SalePrice = dataLoan.sTotalScorePurchasePrice_rep;
            totalScorecardRequest.Applicants = dataLoan.sTotalScoreNumberApplicants_rep;
            totalScorecardRequest.LoanNumber = dataLoan.sTotalScoreUniqueLoanId;
            totalScorecardRequest.Ltv = dataLoan.sTotalScoreLtvR_rep;

            totalScorecardRequest.FrontEndRatio = dataLoan.sTotalScoreFrontEndRatio_rep;
            totalScorecardRequest.BackEndRatio = dataLoan.sTotalScoreBackEndRatio_rep;
            totalScorecardRequest.AmortType = dataLoan.sTotalScoreAmortType;


            totalScorecardRequest.UnderwritingInterest = dataLoan.sTotalScoreUnderwritingNoteIR_rep;
            totalScorecardRequest.UnderwritingPi = dataLoan.sTotalScoreUnderwritingMPmt_rep;
            totalScorecardRequest.FirstTimeBuyer = MapYN(dataLoan.sTotalScoreIsFthb);
            totalScorecardRequest.CounselType = dataLoan.sTotalScoreFhtbCounselingT;

            // OPM 48103 mf 04/13/10.  Lender can set a lender ID for TOTAL
            // that is different from the loanfile value.

            // 4/20/2011 dd - OPM 54063.
            
            switch (dataLoan.sFhaLenderIdT)
            {
                case E_sFhaLenderIdT.RegularFhaLender:
                    totalScorecardRequest.LenderId = Tools.GetFhaLenderIDFromLoanBranchOrBroker(dataLoan.sBrokerId, dataLoan.sLId, dataLoan.sBranchId); // opm 72500
                    break;
                case E_sFhaLenderIdT.SponsoredOriginatorEIN:
                    totalScorecardRequest.SponsoredOriginatorEIN = dataLoan.sFHASponsoredOriginatorEIN;
                    totalScorecardRequest.SponsorId = dataLoan.sFHASponsorAgentIdCode;
                    break;
                case E_sFhaLenderIdT.NoOriginatorId:
                    totalScorecardRequest.SponsorId = dataLoan.sFHASponsorAgentIdCode;
                    totalScorecardRequest.LenderId = "6999609996";
                    break;
                default:
                    throw new UnhandledEnumException(dataLoan.sFhaLenderIdT);
            }

            totalScorecardRequest.Version = dataLoan.sTotalScoreVersion;
            totalScorecardRequest.RefinanceType = Convert(dataLoan.sTotalScoreRefiT, dataLoan.sIsRefinancing);
            totalScorecardRequest.AssetsAvail = dataLoan.sTotalScoreAssetsAvail_rep;

            //OPM 81827 - AV Update total interface
            totalScorecardRequest.MonthlyExpense = dataLoan.sPresLTotHExp_rep;
            totalScorecardRequest.GiftAmt = dataLoan.sTotGiftFundAsset_rep;
            totalScorecardRequest.GiftSource = Convert(dataLoan.sPrimaryGiftFundSource);

            var newRequiredInvestmentDefinitionDate = new DateTime(2016, 8, 22);
            if (dataLoan.sCaseAssignmentD.IsValid && dataLoan.sCaseAssignmentD.DateTimeForComputation.CompareTo(newRequiredInvestmentDefinitionDate) < 0)
            {
                totalScorecardRequest.ReqInvest = dataLoan.sTransNetCash_rep;
            }
            else
            {
                var creditTotal = dataLoan.sLoads1003LineLFromAdjustments
                    ? dataLoan.sTRIDSellerCredits + dataLoan.sOCredit5Amt
                    : GetTotalSellerOrLenderCreditAmount(dataLoan);

                totalScorecardRequest.ReqInvest = dataLoan.m_convertLos.ToMoneyString(
                    dataLoan.sTotTransC - dataLoan.sTotCcPbs - creditTotal - dataLoan.sFinalLAmt,
                    FormatDirection.ToRep);
            }

            totalScorecardRequest.ClsgCosts = dataLoan.sTotEstCcNoDiscnt1003_rep;
            totalScorecardRequest.TotalFixed = dataLoan.sTransmTotMonPmt_rep;
            totalScorecardRequest.SellerConcessions = dataLoan.sTotCcPbs_rep;
            totalScorecardRequest.DownPayment = IncludeBlankMoney(dataLoan.sTotalScoreDownpayment_rep);
            totalScorecardRequest.CondoInd = IsCondo(dataLoan.sGseSpT);
            totalScorecardRequest.BorrClsgCosts = dataLoan.sTotalScoreBorrClosingCosts_rep;
            string secondaryFinancingAmt = dataLoan.sTotalScoreSecondaryFinancingAmt_rep;
            totalScorecardRequest.SecondaryFinancingAmt = string.IsNullOrEmpty(secondaryFinancingAmt) ? "           0.00" : secondaryFinancingAmt; // whitespace is for format target

            if (isFHACaseNumFilledOut)
            {
                totalScorecardRequest.YearsAtJob = dataLoan.sTotalScoreYearsAtCurrentJob_rep;

                totalScorecardRequest.YearsRenting = dataLoan.sTotalScoreMaxYearsRenting_rep;
                totalScorecardRequest.Dependents = dataLoan.sTotalScoreTotalDependents_rep;
                totalScorecardRequest.SelfEmploy = MapYN(dataLoan.sTotalScoreIsSelfEmplmt);
                ApplyExtraDataForFHACaseNumFilledOut(totalScorecardRequest, dataLoan);
            }

            return totalScorecardRequest;
        }

        /// <summary>
        /// Aggregates the amounts of seller and lender credits.
        /// </summary>
        /// <param name="dataLoan">The loan object.</param>
        /// <returns>The total seller or lender credit amount.</returns>
        private static decimal GetTotalSellerOrLenderCreditAmount(CPageData dataLoan)
        {
            return GetAmountOfSellerOrLenderCredit(dataLoan.sOCredit1Amt, dataLoan.sOCredit1Desc)
                + GetAmountOfSellerOrLenderCredit(dataLoan.sOCredit2Amt, dataLoan.sOCredit2Desc)
                + GetAmountOfSellerOrLenderCredit(dataLoan.sOCredit3Amt, dataLoan.sOCredit3Desc)
                + GetAmountOfSellerOrLenderCredit(dataLoan.sOCredit4Amt, dataLoan.sOCredit4Desc)
                + dataLoan.sOCredit5Amt; // Always a lender credit
        }

        /// <summary>
        /// Retrieves the amount of a seller or lender credit.
        /// </summary>
        /// <param name="amount">The credit amount.</param>
        /// <param name="description">The credit description.</param>
        /// <returns>The credit amount if it is a seller or lender credit, otherwise 0.</returns>
        private static decimal GetAmountOfSellerOrLenderCredit(decimal amount, string description)
        {
            if (description.Equals("Lender Credit", StringComparison.OrdinalIgnoreCase)
                || description.Equals("Seller Credit", StringComparison.OrdinalIgnoreCase))
            {
                return amount;
            }

            return 0;
        }

        private static string IncludeBlankMoney(string money)
        {
            if (money == "")
            {
                return "0.00";
            }
            else
            {
                return money;
            }
        }

        private static void ApplyExtraDataForFHACaseNumFilledOut(TotalScorecardRequest totalScorecardRequest, CPageData dataLoan)
        {
            totalScorecardRequest.ContractInterest = dataLoan.sNoteIR_rep;
            totalScorecardRequest.ContractPi = dataLoan.sTotalScoreContractMPmt_rep;


            if (dataLoan.sTotalScoreUploadDataFHATransmittal)
            {
                totalScorecardRequest.LoanPurpose = dataLoan.sTotalScorecardLoanPurposeT;
                totalScorecardRequest.Eem = MapYN(dataLoan.sFHAPurposeIsEnergyEfficientMortgage);
                totalScorecardRequest.ClsgCosts = dataLoan.sFHACcTot_rep;
                totalScorecardRequest.TotalFixed = dataLoan.sFHAPmtFixedTot_rep;

                totalScorecardRequest.SecondaryFinancingSrc = dataLoan.sTotalScoreSecondaryFinancingSrc;
            }
            totalScorecardRequest.TotalReq = dataLoan.sTotalScoreTotalReq_rep;
            totalScorecardRequest.SellerConcessions = dataLoan.sTotalScoreSellerConcessions_rep;

            totalScorecardRequest.PropAddress = dataLoan.sSpAddr;
            totalScorecardRequest.PropCity = dataLoan.sSpCity;
            totalScorecardRequest.PropState = dataLoan.sSpState;
            totalScorecardRequest.PropZip = dataLoan.sSpZip;
            totalScorecardRequest.PropCounty = dataLoan.sSpCountyFipsLast3Digits;
            totalScorecardRequest.UnpaidBalance = dataLoan.sSpLien_rep;
            totalScorecardRequest.BuydownInterest = dataLoan.sBuydownResultIR_rep;
            totalScorecardRequest.ManufacturedHousing = MapYN(dataLoan.sTotalScoreIsManufacturedHousing);

            totalScorecardRequest.LivingUnits = dataLoan.sUnitsNum_rep;
            totalScorecardRequest.ArmType = dataLoan.sTotalScoreArmType;
        }


    }
}
