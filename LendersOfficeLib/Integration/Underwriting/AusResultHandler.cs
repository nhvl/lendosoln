﻿namespace LendersOffice.Integration.Underwriting
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using Common;
    using DataAccess;
    using LendersOfficeApp.los.RatePrice;
    using Security;

    /// <summary>
    /// This class handles the saving and retrieving of AUS order records.
    /// </summary>
    public static class AusResultHandler
    {
        /// <summary>
        /// Saves a system AUS order when Desktop Underwriter is run in LendingQB.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="caseId">The DU case ID.</param>
        /// <param name="recommendation">The recommendation from the DU results.</param>
        /// <param name="principal">The user running DU.</param>
        public static void CreateSystemDuOrder(Guid loanId, string caseId, string recommendation, AbstractUserPrincipal principal)
        {
            var result = new AusResult();
            result.AusOrderId = null;
            result.BrokerId = principal.BrokerId;
            result.CaseId = caseId;
            result.CustomPosition = 0;
            result.DuRecommendation = GetDuRecommendationFromDescription(recommendation);
            result.FindingsDocument = Guid.Empty;
            result.GusRecommendation = GusRecommendation.LeaveBlank;
            result.GusRiskEvaluation = GusRiskEvaluation.LeaveBlank;
            result.IsManual = false;
            result.LoanId = loanId;
            result.LpPurchaseEligibility = LpPurchaseEligibility.LeaveBlank;
            result.LpRiskClass = LpRiskClass.LeaveBlank;
            result.LpFeeLevel = string.Empty;
            result.LpStatus = LpStatus.LeaveBlank;
            result.Notes = string.Empty;
            result.OrderPlacedBy = principal.DisplayName;
            result.ResultOtherDescription = string.Empty;
            result.Timestamp = DateTime.Now;
            result.TotalEligibilityAssessment = TotalEligibilityAssessment.LeaveBlank;
            result.TotalRiskClass = TotalRiskClass.LeaveBlank;
            result.UnderwritingService = UnderwritingService.DU;
            result.UnderwritingServiceOtherDescription = string.Empty;
            CPageData loanData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(AusResultHandler));
            loanData.InitLoad();
            result.LoanIsFha = loanData.sLT == E_sLT.FHA;

            SaveAusOrder(result, principal.BrokerId);
        }

        /// <summary>
        /// Saves a system AUS order when Loan Prospector is run in LendingQB.
        /// </summary>
        /// <param name="caseId">The LP case ID.</param>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="eligibility">The eligibility assessment.</param>
        /// <param name="riskClass">The risk class.</param>
        /// <param name="status">The AUS status.</param>
        /// <param name="feeLevel">The CS/LTV fee level.</param>
        /// <param name="principal">The user running LP.</param>
        public static void CreateSystemLpOrder(string caseId, Guid loanId, string eligibility, string riskClass, string status, string feeLevel, AbstractUserPrincipal principal)
        {
            var result = new AusResult();
            result.AusOrderId = null;
            result.BrokerId = principal.BrokerId;
            result.CaseId = caseId;
            result.CustomPosition = 0;
            result.DuRecommendation = DuRecommendation.LeaveBlank;
            result.FindingsDocument = Guid.Empty;
            result.GusRecommendation = GusRecommendation.LeaveBlank;
            result.GusRiskEvaluation = GusRiskEvaluation.LeaveBlank;
            result.IsManual = false;
            result.LoanId = loanId;
            result.LpPurchaseEligibility = GetLpPurchaseEligibilityFromDescription(eligibility);
            result.LpRiskClass = GetLpRiskClassFromDescription(riskClass);
            result.LpFeeLevel = feeLevel;
            result.LpStatus = GetLpStatusFromDescription(status);
            result.Notes = string.Empty;
            result.OrderPlacedBy = principal.DisplayName;
            result.ResultOtherDescription = string.Empty;
            result.Timestamp = DateTime.Now;
            result.TotalEligibilityAssessment = TotalEligibilityAssessment.LeaveBlank;
            result.TotalRiskClass = TotalRiskClass.LeaveBlank;
            result.UnderwritingService = UnderwritingService.LP;
            result.UnderwritingServiceOtherDescription = string.Empty;
            CPageData loanData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(AusResultHandler));
            loanData.InitLoad();
            result.LoanIsFha = loanData.sLT == E_sLT.FHA;

            SaveAusOrder(result, principal.BrokerId);
        }

        /// <summary>
        /// Saves a system AUS order when FHA TOTAL Scorecard is run in LendingQB.
        /// </summary>
        /// <param name="caseId">The TOTAL case ID.</param>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="eligiblity">The eligibility assessment.</param>
        /// <param name="riskClass">The risk class.</param>
        /// <param name="principal">The user running TOTAL Scorecard.</param>
        public static void CreateSystemTotalOrder(string caseId, Guid loanId, E_EvalStatus eligiblity, E_sTotalScoreCreditRiskResultT riskClass, AbstractUserPrincipal principal)
        {
            var result = new AusResult();
            result.AusOrderId = null;
            result.BrokerId = principal.BrokerId;
            result.CaseId = caseId;
            result.CustomPosition = 0;
            result.DuRecommendation = DuRecommendation.LeaveBlank;
            result.FindingsDocument = Guid.Empty;
            result.GusRecommendation = GusRecommendation.LeaveBlank;
            result.GusRiskEvaluation = GusRiskEvaluation.LeaveBlank;
            result.IsManual = false;
            result.LoanId = loanId;
            result.LpPurchaseEligibility = LpPurchaseEligibility.LeaveBlank;
            result.LpRiskClass = LpRiskClass.LeaveBlank;
            result.LpFeeLevel = string.Empty;
            result.LpStatus = LpStatus.LeaveBlank;
            result.Notes = string.Empty;
            result.OrderPlacedBy = principal.DisplayName;
            result.ResultOtherDescription = string.Empty;
            result.Timestamp = DateTime.Now;
            result.TotalEligibilityAssessment = GetTotalEligibilityAssessmentFromFindings(eligiblity);
            result.TotalRiskClass = GetTotalRiskClassFromFindings(riskClass);
            result.UnderwritingService = UnderwritingService.TOTAL;
            result.UnderwritingServiceOtherDescription = string.Empty;
            result.LoanIsFha = true;

            SaveAusOrder(result, principal.BrokerId);
        }

        /// <summary>
        /// Saves a list of AUS orders to the database.
        /// </summary>
        /// <param name="orders">A list of AUS orders.</param>
        /// <param name="brokerId">The broker ID associated with the order.</param>
        /// <remarks>This will primarily be used by the AUS Ordering Dashboard.</remarks>
        public static void SaveAusOrderList(IEnumerable<AusResult> orders, Guid brokerId)
        {
            foreach (var order in orders)
            {
                SaveAusOrder(order, brokerId);
            }
        }

        /// <summary>
        /// Delete a list of AUS orders to the database.
        /// </summary>
        /// <param name="orders">A list of AUS orders.</param>
        /// <param name="brokerId">The broker ID associated with the order.</param>
        /// <remarks>This will primarily be used by the AUS Ordering Dashboard.</remarks>
        public static void DeleteOrderList(IEnumerable<AusResult> orders, Guid brokerId)
        {
            foreach (var order in orders)
            {
                if (order.AusOrderId != null)
                {
                    DeleteAusOrder(order.AusOrderId.Value, brokerId);
                }
            }
        }

        /// <summary>
        /// Retrieves all AUS orders associated with a given loan.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="brokerId">The broker ID.</param>
        /// <returns>A list of AUS orders associated with the loan.</returns>
        public static List<AusResult> RetrieveOrdersForLoan(Guid loanId, Guid brokerId)
        {
            var results = new List<AusResult>();
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@BrokerId", brokerId)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "AUS_ORDERS_RetrieveByLoan", parameters))
            {
                while (reader.Read())
                {
                    var result = new AusResult();
                    result.AusOrderId = (int?)reader[nameof(AusResult.AusOrderId)];
                    result.BrokerId = (Guid)reader[nameof(AusResult.BrokerId)];
                    result.CaseId = (string)reader[nameof(AusResult.CaseId)];
                    result.CustomPosition = (int)reader[nameof(AusResult.CustomPosition)];
                    result.DuRecommendation = (DuRecommendation)reader[nameof(AusResult.DuRecommendation)];
                    result.FindingsDocument = (Guid)reader[nameof(AusResult.FindingsDocument)];
                    result.GusRecommendation = (GusRecommendation)reader[nameof(AusResult.GusRecommendation)];
                    result.GusRiskEvaluation = (GusRiskEvaluation)reader[nameof(AusResult.GusRiskEvaluation)];
                    result.IsManual = (bool)reader[nameof(AusResult.IsManual)];
                    result.LoanId = (Guid)reader[nameof(AusResult.LoanId)];
                    result.LpPurchaseEligibility = (LpPurchaseEligibility)reader[nameof(AusResult.LpPurchaseEligibility)];
                    result.LpRiskClass = (LpRiskClass)reader[nameof(AusResult.LpRiskClass)];
                    result.LpFeeLevel = (string)reader[nameof(AusResult.LpFeeLevel)];
                    result.LpStatus = (LpStatus)reader[nameof(AusResult.LpStatus)];
                    result.Notes = (string)reader[nameof(AusResult.Notes)];
                    result.OrderPlacedBy = (string)reader[nameof(AusResult.OrderPlacedBy)];
                    result.ResultOtherDescription = (string)reader[nameof(AusResult.ResultOtherDescription)];
                    result.Timestamp = (DateTime)reader[nameof(AusResult.Timestamp)];
                    result.TotalEligibilityAssessment = (TotalEligibilityAssessment)reader[nameof(AusResult.TotalEligibilityAssessment)];
                    result.TotalRiskClass = (TotalRiskClass)reader[nameof(AusResult.TotalRiskClass)];
                    result.UnderwritingService = (UnderwritingService)reader[nameof(AusResult.UnderwritingService)];
                    result.UnderwritingServiceOtherDescription = (string)reader[nameof(AusResult.UnderwritingServiceOtherDescription)];
                    result.LoanIsFha = reader.AsNullableBool(nameof(AusResult.LoanIsFha)) ?? false;

                    results.Add(result);
                }
            }

            results = results.OrderByDescending(r => r.Timestamp).ThenBy(r => r.CustomPosition).ToList();
            return results;
        }

        /// <summary>
        /// Saves an individual AUS order to the DB.
        /// </summary>
        /// <param name="result">The AUS result to be saved.</param>
        /// <param name="brokerId">The broker ID associated with the order.</param>
        private static void SaveAusOrder(AusResult result, Guid brokerId)
        {
            var parameters = GetParametersForAusResult(result);
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "AUS_ORDERS_SaveOrder", 3, parameters);
        }

        /// <summary>
        /// Deletes an AUS order from the DB.
        /// </summary>
        /// <param name="ausOrderId">The unique AUS order ID.</param>
        /// <param name="brokerId">The broker ID associated with the order.</param>
        private static void DeleteAusOrder(int ausOrderId, Guid brokerId)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@AusOrderId", ausOrderId),
                new SqlParameter("@BrokerId", brokerId)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "AUS_ORDERS_Delete", 3, parameters);
        }

        /// <summary>
        /// Converts an AUS result into SQL parameters.
        /// </summary>
        /// <param name="result">The AUS result.</param>
        /// <returns>A set of SQL parameters ready to be saved to the DB.</returns>
        private static SqlParameter[] GetParametersForAusResult(AusResult result)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@AusOrderId", result.AusOrderId),
                new SqlParameter("@BrokerId", result.BrokerId),
                new SqlParameter("@CaseId", result.CaseId),
                new SqlParameter("@CustomPosition", result.CustomPosition),
                new SqlParameter("@DuRecommendation", result.DuRecommendation),
                new SqlParameter("@FindingsDocument", Guid.Empty),
                new SqlParameter("@GusRecommendation", result.GusRecommendation),
                new SqlParameter("@GusRiskEvaluation", result.GusRiskEvaluation),
                new SqlParameter("@IsManual", result.IsManual),
                new SqlParameter("@LoanId", result.LoanId),
                new SqlParameter("@LpPurchaseEligibility", result.LpPurchaseEligibility),
                new SqlParameter("@LpRiskClass", result.LpRiskClass),
                new SqlParameter("@LpFeeLevel", result.LpFeeLevel),
                new SqlParameter("@LpStatus", result.LpStatus),
                new SqlParameter("@Notes", result.Notes),
                new SqlParameter("@OrderPlacedBy", result.OrderPlacedBy),
                new SqlParameter("@ResultOtherDescription", result.ResultOtherDescription),
                new SqlParameter("@Timestamp", result.Timestamp),
                new SqlParameter("@TotalEligibilityAssessment", result.TotalEligibilityAssessment),
                new SqlParameter("@TotalRiskClass", result.TotalRiskClass),
                new SqlParameter("@UnderwritingService", result.UnderwritingService),
                new SqlParameter("@UnderwritingServiceOtherDescription", result.UnderwritingServiceOtherDescription),
                new SqlParameter("@LoanIsFha", result.LoanIsFha)
            };
        }

        /// <summary>
        /// Converts the recommendation from a DU result to an enum.
        /// </summary>
        /// <param name="description">The recommendation description.</param>
        /// <returns>A DU recommendation value.</returns>
        private static DuRecommendation GetDuRecommendationFromDescription(string description)
        {
            switch (description.ToLower().TrimWhitespaceAndBOM())
            {
                case "refer/eligible":
                    return DuRecommendation.Refer_Eligible;
                case "out of scope":
                    return DuRecommendation.OutOfScope;
                case "approve/eligible":
                    return DuRecommendation.Approve_Eligible;
                case "refer/ineligible":
                    return DuRecommendation.Refer_Ineligible;
                case "approve/ineligible":
                    return DuRecommendation.Approve_Ineligible;
                case "refer with caution":
                    return DuRecommendation.ReferWithCaution;
                case "refer":
                    return DuRecommendation.Refer;
                case "ea-i/eligible":
                    return DuRecommendation.EAI_Eligible;
                case "ea-ii/eligible":
                    return DuRecommendation.EAII_Eligible;
                case "ea-iii/eligible":
                    return DuRecommendation.EAIII_Eligible;
                case "ea-iii/ineligible":
                    return DuRecommendation.EAIII_Ineligible;
                case "ea-ii/ineligible":
                    return DuRecommendation.EAII_Ineligible;
                case "ea-i/ineligible":
                    return DuRecommendation.EAI_Ineligible;
                case "unknown recommendation":
                case "unknown":
                case "":
                    return DuRecommendation.Unknown;
                case "deny":
                    return DuRecommendation.Deny;
                case "error":
                    return DuRecommendation.Error;
                case "complete":
                    return DuRecommendation.Complete;
                case "approve":
                    return DuRecommendation.Approve;
                case "ineligible":
                    return DuRecommendation.Ineligible;
                case "complete with warning":
                    return DuRecommendation.CompleteWithWarning;
                case "refer w caution/i":
                    return DuRecommendation.ReferWCaution_I;
                case "refer w caution/ii":
                    return DuRecommendation.ReferWCaution_II;
                case "refer w caution/iii":
                    return DuRecommendation.ReferWCaution_III;
                case "refer w caution/iv":
                    return DuRecommendation.ReferWCaution_IV;
                case "resubmit":
                    return DuRecommendation.Resubmit;
                case "expanded approval/i":
                    return DuRecommendation.ExpandedApproval_I;
                case "expanded approval/ii":
                    return DuRecommendation.ExpandedApproval_II;
                case "expanded approval/iii":
                    return DuRecommendation.ExpandedApproval_III;
                case "expanded approval/iv":
                    return DuRecommendation.ExpandedApproval_IV;
                case "ea-iv/eligible":
                    return DuRecommendation.EAIV_Eligible;
                case "ea-iv/ineligible":
                    return DuRecommendation.EAIV_Ineligible;
                case "rwc-iv/eligible":
                    return DuRecommendation.RWCIV_Eligible;
                case "rwc-iv/ineligible":
                    return DuRecommendation.RWCIV_Ineligible;
                default:
                    throw new ArgumentException($"The DU result \"{description}\" is not handled.");
            }
        }

        /// <summary>
        /// Converts the AUS status from an LP result to an enum.
        /// </summary>
        /// <param name="description">The LP AUS status description.</param>
        /// <returns>An LP AUS Status value.</returns>
        private static LpStatus GetLpStatusFromDescription(string description)
        {
            switch (description.ToLower().TrimWhitespaceAndBOM())
            {
                case "invalid":
                    return LpStatus.Invalid;
                case "ineligible":
                    return LpStatus.Ineligible;
                case "incomplete":
                    return LpStatus.Incomplete;
                case "complete":
                    return LpStatus.Complete;
                default:
                    throw new ArgumentException($"The LP AUS Status value \"{description}\" is not handled.");
            }
        }

        /// <summary>
        /// Converts the risk class from an LP result to an enum.
        /// </summary>
        /// <param name="description">The LP risk class description.</param>
        /// <returns>An LP risk class value.</returns>
        private static LpRiskClass GetLpRiskClassFromDescription(string description)
        {
            if (description.Equals(nameof(LpRiskClass.Accept), StringComparison.OrdinalIgnoreCase))
            {
                return LpRiskClass.Accept;
            }
            else if (description.Equals(nameof(LpRiskClass.Caution), StringComparison.OrdinalIgnoreCase))
            {
                return LpRiskClass.Caution;
            }
            else if (description.Equals(nameof(LpRiskClass.Refer), StringComparison.OrdinalIgnoreCase))
            {
                return LpRiskClass.Refer;
            }
            else
            {
                return LpRiskClass.NA;
            }
        }

        /// <summary>
        /// Converts the purchase eligibility from an LP result to an enum.
        /// </summary>
        /// <param name="description">The LP purchase eligibility description.</param>
        /// <returns>An LP purchase eligibility value.</returns>
        private static LpPurchaseEligibility GetLpPurchaseEligibilityFromDescription(string description)
        {
            // The purchase eligibility enums are prepended with an underscore since they start with numbers.
            description = $"_{description}";

            if (description.Equals(nameof(LpPurchaseEligibility._000FreddieMacEligible), StringComparison.OrdinalIgnoreCase))
            {
                return LpPurchaseEligibility._000FreddieMacEligible;
            }
            else if (description.Equals(nameof(LpPurchaseEligibility._000FreddieMacIneligible), StringComparison.OrdinalIgnoreCase))
            {
                return LpPurchaseEligibility._000FreddieMacIneligible;
            }
            else if (description.Equals(nameof(LpPurchaseEligibility._500FreddieMacEligibleLPAMinusOffering), StringComparison.OrdinalIgnoreCase))
            {
                return LpPurchaseEligibility._500FreddieMacEligibleLPAMinusOffering;
            }
            else
            {
                return LpPurchaseEligibility.NA;
            }
        }

        /// <summary>
        /// Converts the risk class from an FHA TOTAL result to an enum.
        /// </summary>
        /// <param name="riskClass">The TOTAL risk class.</param>
        /// <returns>A TOTAL risk class value.</returns>
        private static TotalRiskClass GetTotalRiskClassFromFindings(E_sTotalScoreCreditRiskResultT riskClass)
        {
            switch (riskClass)
            {
                case E_sTotalScoreCreditRiskResultT.Unknown:
                    return TotalRiskClass.Unknown;
                case E_sTotalScoreCreditRiskResultT.Approve:
                    return TotalRiskClass.Accept;
                case E_sTotalScoreCreditRiskResultT.Refer:
                    return TotalRiskClass.Refer;
                default:
                    throw new UnhandledEnumException(riskClass);
            }
        }

        /// <summary>
        /// Converts the eligiblity status from an FHA TOTAL result to an enum.
        /// </summary>
        /// <param name="status">The TOTAL eligiblity status.</param>
        /// <returns>A TOTAL eligiblity assessment value.</returns>
        private static TotalEligibilityAssessment GetTotalEligibilityAssessmentFromFindings(E_EvalStatus status)
        {
            switch (status)
            {
                case E_EvalStatus.Eval_Eligible:
                    return TotalEligibilityAssessment.Eligible;
                case E_EvalStatus.Eval_Ineligible:
                    return TotalEligibilityAssessment.Ineligible;
                case E_EvalStatus.Eval_InsufficientInfo:
                    return TotalEligibilityAssessment.InsufficientInfo;
                case E_EvalStatus.Eval_HideFromResult:
                    return TotalEligibilityAssessment.LeaveBlank;
                default:
                    throw new UnhandledEnumException(status);
            }
        }
    }
}
