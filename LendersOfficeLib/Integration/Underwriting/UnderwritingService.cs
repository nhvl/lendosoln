﻿namespace LendersOffice.Integration.Underwriting
{
    /// <summary>
    /// Represents an AUS.
    /// </summary>
    public enum UnderwritingService
    {
        /// <summary>
        /// Desktop Underwriter.
        /// </summary>
        DU = 1,

        /// <summary>
        /// Loan Prospector.
        /// Will be renamed to LPA when EnableLoanProductAdvisorRebrand bit is true.
        /// </summary>
        LP = 2,

        /// <summary>
        /// FHA TOTAL.
        /// </summary>
        TOTAL = 3,

        /// <summary>
        /// Guaranteed Underwriting Service.
        /// </summary>
        GUS = 4,

        /// <summary>
        /// Another AUS.
        /// </summary>
        Other = 5
    }
}
