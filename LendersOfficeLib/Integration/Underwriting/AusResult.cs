﻿namespace LendersOffice.Integration.Underwriting
{
    using System;
    using DataAccess;

    /// <summary>
    /// Represents a single AUS order.
    /// </summary>
    public class AusResult
    {
        /// <summary>
        /// Gets or sets the order ID, a unique identifier.
        /// </summary>
        /// <value>The order ID, a unique identifier.</value>
        public int? AusOrderId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the loan associated with the order.
        /// </summary>
        /// <value>The ID of the loan associated with the order.</value>
        public Guid LoanId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the broker associated with the order.
        /// </summary>
        /// <value>The ID of the broker associated with the order.</value>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this order was added manually by a user.
        /// </summary>
        /// <value>Indicates whether this order was added manually by a user.</value>
        public bool IsManual { get; set; }

        /// <summary>
        /// Gets or sets the custom position of the order.
        /// </summary>
        /// <value>The custom position of the order.</value>
        /// <remarks>For manually added orders on the same date, users can adjust the order in which they appear.</remarks>
        public int CustomPosition { get; set; }

        /// <summary>
        /// Gets or sets the AUS used to produce this order.
        /// </summary>
        /// <value>The AUS used to produce this order.</value>
        public UnderwritingService UnderwritingService { get; set; }

        /// <summary>
        /// Gets or sets a description of the AUS used to produce this order, if a non-supported AUS was used.
        /// </summary>
        /// <value>A description of the AUS used to produce this order, if a non-supported AUS was used.</value>
        public string UnderwritingServiceOtherDescription { get; set; }

        /// <summary>
        /// Gets or sets the recommendation for a DU order.
        /// </summary>
        /// <value>The recommendation for a DU order.</value>
        public DuRecommendation DuRecommendation { get; set; }

        /// <summary>
        /// Gets or sets the risk class for an LP order.
        /// </summary>
        /// <value>The risk class for an LP order.</value>
        public LpRiskClass LpRiskClass { get; set; }

        /// <summary>
        /// Gets or sets the purchase eligibility for an LP order.
        /// </summary>
        /// <value>The purchase eligibility for an LP order.</value>
        public LpPurchaseEligibility LpPurchaseEligibility { get; set; }

        /// <summary>
        /// Gets or sets the fee level code for an LP order.
        /// </summary>
        /// <value>The fee level code for an LP order.</value>
        public string LpFeeLevel { get; set; }

        /// <summary>
        /// Gets or sets the status for an LP order.
        /// </summary>
        /// <value>The status for an LP order.</value>
        public LpStatus LpStatus { get; set; }

        /// <summary>
        /// Gets or sets the risk class for a TOTAL order.
        /// </summary>
        /// <value>The risk class for a TOTAL order.</value>
        public TotalRiskClass TotalRiskClass { get; set; }

        /// <summary>
        /// Gets or sets the eligibility assessment for a TOTAL order.
        /// </summary>
        /// <value>The eligibility assessment for a TOTAL order.</value>
        public TotalEligibilityAssessment TotalEligibilityAssessment { get; set; }

        /// <summary>
        /// Gets or sets the recommendation for a GUS order.
        /// </summary>
        /// <value>The recommendation for a GUS order.</value>
        public GusRecommendation GusRecommendation { get; set; }

        /// <summary>
        /// Gets or sets the risk evaluation for a GUS order.
        /// </summary>
        /// <value>The risk evaluation for a GUS order.</value>
        public GusRiskEvaluation GusRiskEvaluation { get; set; }

        /// <summary>
        /// Gets or sets the result description, if the order was not run with a supported AUS.
        /// </summary>
        /// <value>The result description, if the order was not run with a supported AUS.</value>
        public string ResultOtherDescription { get; set; }

        /// <summary>
        /// Gets or sets the case ID associated with this order.
        /// </summary>
        /// <value>The case ID associated with this order.</value>
        public string CaseId { get; set; }

        /// <summary>
        /// Gets or sets the ID of the EDoc holding the findings associated with this order.
        /// </summary>
        /// <value>The ID of the EDoc holding the findings associated with this order.</value>
        public Guid FindingsDocument { get; set; }

        /// <summary>
        /// Gets or sets the user who placed the order.
        /// </summary>
        /// <value>The user who placed the order.</value>
        public string OrderPlacedBy { get; set; }

        /// <summary>
        /// Gets or sets the date and time the order was placed.
        /// </summary>
        /// <value>The date and time the order was placed.</value>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the string representation of the order placement timestamp.
        /// </summary>
        /// <value>The order placement timestamp string representation.</value>
        public string Timestamp_rep
        {
            get
            {
                return this.Timestamp.ToString();
            }

            set
            {
                DateTime input;
                if (!string.IsNullOrEmpty(value) && DateTime.TryParse(value, out input))
                {
                    this.Timestamp = input;
                }
            }
        }

        /// <summary>
        /// Gets or sets any user notes associated with the order.
        /// </summary>
        /// <value>Any user notes associated with the order.</value>
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the loan was an FHA loan at the time of the AUS run.
        /// </summary>
        public bool LoanIsFha { get; set; }

        /// <summary>
        /// Gets the Underwriting Service that should be reported for HMDA reporting. For FHA loans, DU and LPA runs should count as FHA TOTAL runs if the loan is configured for this.
        /// </summary>
        /// <param name="reportFhaGseAusRunsAsTotalRun">For HMDA Reporting, whether to report DU and LPA runs on FHA loans as TOTAL Scorecard runs.</param>
        /// <returns>The underwriting service to report as.</returns>
        public UnderwritingService ReportAs(bool reportFhaGseAusRunsAsTotalRun)
        {
            if (reportFhaGseAusRunsAsTotalRun && this.LoanIsFha && (this.UnderwritingService == UnderwritingService.LP || this.UnderwritingService == UnderwritingService.DU))
            {
                return UnderwritingService.TOTAL;
            }
            else
            {
                return this.UnderwritingService;
            }
        }
    }
}
