﻿namespace LendersOffice.Integration.Underwriting
{
    /// <summary>
    /// The eligibility assessment from an FHA TOTAL result.
    /// </summary>
    public enum TotalEligibilityAssessment
    {
        /// <summary>
        /// A blank placeholder value.
        /// </summary>
        LeaveBlank = 0,

        /// <summary>
        /// A result of Ineligible.
        /// </summary>
        Ineligible = 1,

        /// <summary>
        /// A result of Eligible.
        /// </summary>
        Eligible = 2,

        /// <summary>
        /// A result of Insufficient information.
        /// </summary>
        InsufficientInfo = 3
    }
}
