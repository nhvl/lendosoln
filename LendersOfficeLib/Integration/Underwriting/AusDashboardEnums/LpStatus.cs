﻿namespace LendersOffice.Integration.Underwriting
{
    /// <summary>
    /// The AUS status from a Loan Prospector result.
    /// </summary>
    public enum LpStatus
    {
        /// <summary>
        /// A blank placeholder value.
        /// </summary>
        LeaveBlank = 0,

        /// <summary>
        /// A result of Invalid.
        /// </summary>
        Invalid = 1,

        /// <summary>
        /// A result of Ineligible.
        /// </summary>
        Ineligible = 2,

        /// <summary>
        /// A result of Incomplete.
        /// </summary>
        Incomplete = 3,

        /// <summary>
        /// A result of Complete.
        /// </summary>
        Complete = 4
    }
}
