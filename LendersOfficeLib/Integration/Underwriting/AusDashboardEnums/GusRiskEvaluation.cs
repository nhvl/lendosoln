﻿namespace LendersOffice.Integration.Underwriting
{
    /// <summary>
    /// The risk evaluation from a Guaranteed Underwriting System result.
    /// </summary>
    public enum GusRiskEvaluation
    {
        /// <summary>
        /// A blank placeholder option.
        /// </summary>
        LeaveBlank = 0,

        /// <summary>
        /// A result of Eligible.
        /// </summary>
        Eligible = 1,

        /// <summary>
        /// A result of Ineligible.
        /// </summary>
        Ineligible = 2,

        /// <summary>
        /// A result of Unable to determine.
        /// </summary>
        UnableToDetermine = 3
    }
}
