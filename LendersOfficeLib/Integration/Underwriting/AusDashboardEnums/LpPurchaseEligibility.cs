﻿namespace LendersOffice.Integration.Underwriting
{
    /// <summary>
    /// The purchase eligibility from a Loan Prospector result.
    /// </summary>
    public enum LpPurchaseEligibility
    {
        /// <summary>
        /// A blank placeholder option.
        /// </summary>
        LeaveBlank = 0,

        /// <summary>
        /// A result of Not applicable.
        /// </summary>
        NA = 1,

        /// <summary>
        /// A result of Freddie Mac Eligible.
        /// </summary>
        _000FreddieMacEligible = 2,

        /// <summary>
        /// A result of Freddie Mac Ineligible.
        /// </summary>
        _000FreddieMacIneligible = 3,

        /// <summary>
        /// A result of Freddie Mac Eligible LPA Minus Offering.
        /// </summary>
        _500FreddieMacEligibleLPAMinusOffering = 4
    }
}
