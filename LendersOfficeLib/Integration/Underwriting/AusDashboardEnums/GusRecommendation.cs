﻿namespace LendersOffice.Integration.Underwriting
{
    /// <summary>
    /// The recommendation from a Guaranteed Underwriting System result.
    /// </summary>
    public enum GusRecommendation
    {
        /// <summary>
        /// A blank placeholder option.
        /// </summary>
        LeaveBlank = 0,

        /// <summary>
        /// A result of Accept.
        /// </summary>
        Accept = 1,

        /// <summary>
        /// A result of Refer.
        /// </summary>
        Refer = 2,

        /// <summary>
        /// A result of Refer with caution.
        /// </summary>
        ReferWithCaution = 3,

        /// <summary>
        /// A result of Ineligible.
        /// </summary>
        Ineligible = 4,

        /// <summary>
        /// A result of Unable to determine.
        /// </summary>
        UnableToDetermine = 5
    }
}
