﻿namespace LendersOffice.Integration.Underwriting
{
    /// <summary>
    /// The recommendation from a Desktop Underwriter result.
    /// </summary>
    public enum DuRecommendation
    {
        /// <summary>
        /// A blank placeholder value.
        /// </summary>
        LeaveBlank = 0,

        /// <summary>
        /// A result of Refer/Eligible.
        /// </summary>
        Refer_Eligible = 1,

        /// <summary>
        /// A result of Out of Scope.
        /// </summary>
        OutOfScope = 2,

        /// <summary>
        /// A result of Approve/Eligible.
        /// </summary>
        Approve_Eligible = 3,

        /// <summary>
        /// A result of Refer/Ineligible.
        /// </summary>
        Refer_Ineligible = 4,

        /// <summary>
        /// A result of Unknown.
        /// </summary>
        Unknown = 5,

        /// <summary>
        /// A result of Approve/Ineligible.
        /// </summary>
        Approve_Ineligible = 6,

        /// <summary>
        /// A result of Refer with caution.
        /// </summary>
        ReferWithCaution = 7,

        /// <summary>
        /// A result of Refer.
        /// </summary>
        Refer = 8,

        /// <summary>
        /// A result of Deny.
        /// </summary>
        Deny = 9,

        /// <summary>
        /// A result of Error.
        /// </summary>
        Error = 10,

        /// <summary>
        /// A result of Complete.
        /// </summary>
        Complete = 11,

        /// <summary>
        /// A result of Approve.
        /// </summary>
        Approve = 12,

        /// <summary>
        /// A result of Ineligible.
        /// </summary>
        Ineligible = 13,

        /// <summary>
        /// A result of Complete with warning.
        /// </summary>
        CompleteWithWarning = 14,

        /// <summary>
        /// A result of Refer with caution I.
        /// </summary>
        ReferWCaution_I = 15,

        /// <summary>
        /// A result of Refer with caution II.
        /// </summary>
        ReferWCaution_II = 16,

        /// <summary>
        /// A result of Refer with caution III.
        /// </summary>
        ReferWCaution_III = 17,

        /// <summary>
        /// A result of Refer with caution IV.
        /// </summary>
        ReferWCaution_IV = 18,

        /// <summary>
        /// A result of Resubmit.
        /// </summary>
        Resubmit = 19,

        /// <summary>
        /// A result of Expanded approval I.
        /// </summary>
        ExpandedApproval_I = 20,

        /// <summary>
        /// A result of Expanded approval II.
        /// </summary>
        ExpandedApproval_II = 21,

        /// <summary>
        /// A result of Expanded approval III.
        /// </summary>
        ExpandedApproval_III = 22,

        /// <summary>
        /// A result of Expanded approval IV.
        /// </summary>
        ExpandedApproval_IV = 23,

        /// <summary>
        /// A result of EA-I/Eligible.
        /// </summary>
        EAI_Eligible = 24,

        /// <summary>
        /// A result of EA-I/Ineligible.
        /// </summary>
        EAI_Ineligible = 25,

        /// <summary>
        /// A result of EA-II/Eligible.
        /// </summary>
        EAII_Eligible = 26,

        /// <summary>
        /// A result of EA-II/Ineligible.
        /// </summary>
        EAII_Ineligible = 27,

        /// <summary>
        /// A result of EA-III/Eligible.
        /// </summary>
        EAIII_Eligible = 28,

        /// <summary>
        /// A result of EA-III/Ineligible.
        /// </summary>
        EAIII_Ineligible = 29,

        /// <summary>
        /// A result of EA-IV/Eligible.
        /// </summary>
        EAIV_Eligible = 30,

        /// <summary>
        /// A result of EA-IV/Ineligible.
        /// </summary>
        EAIV_Ineligible = 31,

        /// <summary>
        /// A result of RWC-IV/Eligible.
        /// </summary>
        RWCIV_Eligible = 32,

        /// <summary>
        /// A result of RWC-IV/Ineligible.
        /// </summary>
        RWCIV_Ineligible = 33
    }
}
