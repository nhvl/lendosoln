﻿namespace LendersOffice.Integration.Underwriting
{
    /// <summary>
    /// The risk class from a Loan Prospector result.
    /// </summary>
    public enum LpRiskClass
    {
        /// <summary>
        /// A blank placeholder option.
        /// </summary>
        LeaveBlank = 0,

        /// <summary>
        /// A result of Not applicable.
        /// </summary>
        NA = 1,

        /// <summary>
        /// A result of Accept.
        /// </summary>
        Accept = 2,

        /// <summary>
        /// A result of Refer.
        /// </summary>
        Refer = 3,

        /// <summary>
        /// A result of Caution.
        /// </summary>
        Caution = 4,
    }
}
