﻿namespace LendersOffice.Integration.Underwriting
{
    /// <summary>
    /// The risk class from an FHA TOTAL result.
    /// </summary>
    public enum TotalRiskClass
    {
        /// <summary>
        /// A blank placeholder value.
        /// </summary>
        LeaveBlank = 0,

        /// <summary>
        /// A result of Unknown.
        /// </summary>
        Unknown = 1,

        /// <summary>
        /// A result of Accept.
        /// </summary>
        Accept = 2,

        /// <summary>
        /// A result of Refer.
        /// </summary>
        Refer = 3
    }
}
