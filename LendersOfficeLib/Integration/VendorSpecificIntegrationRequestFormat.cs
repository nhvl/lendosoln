﻿namespace LendersOffice.Integration
{
    using System;

    /// <summary>
    /// A format to be used for a specific vendor, integration, and request type. <para></para>
    /// The request will be the xml produced by exporting from the loan given the loXml file,
    /// then running that through the specified xsl transform, with some additional xslt params.<para></para>
    /// We will also eventually have an integration request format (base class) not specific to a vendor that will be
    /// the default for all requests unless there's a vendor-specific request format.<para></para>
    /// Created for opm 463659.
    /// </summary>
    public class VendorSpecificIntegrationRequestFormat
    {
        /// <summary>
        /// Gets or sets the id of the vendor associated with the format.
        /// </summary>
        /// <value>The id of the vendor.</value>
        public Guid VendorId { get; set; }

        /// <summary>
        /// Gets or sets the type of integration and request to be associated with the format.
        /// </summary>
        /// <value>The integration and request type.</value>
        public IntegrationRequestType IntegrationRequestType { get; set; }

        /// <summary>
        /// Gets or sets the file name to be used as the loxml for the format.
        /// </summary>
        /// <value>The name of the loxml file to be used for the format.</value>
        public string LoXmlFileName { get; set; }

        /// <summary>
        /// Gets or sets the file id to be used as the loxml for the format.
        /// </summary>
        /// <value>The id of the loxml file to be used for the format.</value>
        public int LoXmlFileId { get; set; }

        /// <summary>
        /// Gets or sets the file name to be used as the xsl for the format.
        /// </summary>
        /// <value>The name of the xsl file to be used for the format.</value>
        public string XslFileName { get; set; }

        /// <summary>
        /// Gets or sets the file id to be used as the xsl for the format.
        /// </summary>
        /// <value>The id of the xsl file to be used for the format.</value>
        public int XslFileId { get; set; }
    }
}
