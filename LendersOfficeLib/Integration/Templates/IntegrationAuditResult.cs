﻿namespace LendersOffice.Integration.Templates
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// A set of results from auditing an integration payload. May contain
    /// a number of error items.
    /// </summary>
    public class IntegrationAuditResult
    {
        /// <summary>
        /// A collection of errors.
        /// </summary>
        private List<IntegrationAuditErrorItem> errors;

        /// <summary>
        /// A collection of sections used to organize UI for audit results.
        /// </summary>
        private HashSet<string> sections;

        /// <summary>
        /// Initializes a new instance of the <see cref="IntegrationAuditResult"/> class.
        /// </summary>
        public IntegrationAuditResult()
        {
            this.errors = new List<IntegrationAuditErrorItem>();
            this.sections = new HashSet<string>();
        }

        /// <summary>
        /// Gets a value indicating whether the audit result contains any errors.
        /// </summary>
        /// <value>A boolean indicating whether the audit result contains any errors.</value>
        public bool HasErrors
        {
            get
            {
                return this.errors.Any();
            }
        }

        /// <summary>
        /// Gets the section names of any errors in the list.
        /// </summary>
        /// <value>The section names of any errors in the list.</value>
        public IEnumerable<string> SectionNames
        {
            get
            {
                return this.sections;
            }
        }

        /// <summary>
        /// Gets a list of errors by their section name.
        /// </summary>
        /// <param name="sectionName">The section name.</param>
        /// <returns>A list of errors in the given section.</returns>
        public virtual IEnumerable<IntegrationAuditErrorItem> GetErrorsForSection(string sectionName)
        {
            return this.errors.Where(item => item.SectionName.Equals(sectionName));
        }

        /// <summary>
        /// Gets all the errors.
        /// </summary>
        /// <returns>List of all the errors.</returns>
        public virtual IEnumerable<IntegrationAuditErrorItem> GetAllErrors()
        {
            return this.errors;
        }

        /// <summary>
        /// Adds an error item to the list.
        /// </summary>
        /// <param name="item">The error item.</param>
        public void AddError(IntegrationAuditErrorItem item)
        {
            this.sections.Add(item.SectionName);
            this.errors.Add(item);
        }

        /// <summary>
        /// Adds the section to the section list. 
        /// </summary>
        /// <param name="sectionName">The section name.</param>
        public void AddSection(string sectionName)
        {
            this.sections.Add(sectionName);
        }
    }
}
