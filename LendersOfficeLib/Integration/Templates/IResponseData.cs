﻿namespace LendersOffice.Integration.Templates
{
    /// <summary>
    /// The ResponseData is a system object that contains framework-specific
    /// data parsed out of a response payload.
    /// </summary>
    /// <remarks>
    /// This interface is empty because the necessary data varies by integration,
    /// but since many abstract components have a concept of the data retrieved
    /// from the response, it is useful to have a common base type.
    /// </remarks>
    public interface IResponseData
    {
    }
}
