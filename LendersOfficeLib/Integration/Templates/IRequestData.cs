﻿namespace LendersOffice.Integration.Templates
{
    /// <summary>
    /// The RequestData is an object that is passed around an integration framework,
    /// and contains all necessary configuration data for generating a request, sending
    /// it to a vendor, and processing the response.
    /// </summary>
    /// <remarks>
    /// This interface is empty because the necessary data varies by integration, but since
    /// many abstract components touch the RequestData, a central base type is still needed.
    /// </remarks>
    public interface IRequestData
    {
    }
}
