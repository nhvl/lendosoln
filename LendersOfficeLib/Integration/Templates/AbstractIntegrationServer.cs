﻿namespace LendersOffice.Integration.Templates
{
    using System.Collections.Generic;

    /// <summary>
    /// An integration server component takes in a payload, transmits it
    /// to a third-party, and returns the response for processing.
    /// </summary>
    public abstract class AbstractIntegrationServer
    {
        /// <summary>
        /// Gets any errors encountered.
        /// </summary>
        /// <returns>The list of errors encountered.</returns>
        public virtual List<string> GetErrors()
        {
            return null;
        }

        /// <summary>
        /// Places a request given the XML content.
        /// </summary>
        /// <param name="orderXml">The xml content to send.</param>
        /// <returns>The response string.</returns>
        public abstract string PlaceRequest(string orderXml);
    }
}
