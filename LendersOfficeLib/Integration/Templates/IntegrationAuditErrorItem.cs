﻿namespace LendersOffice.Integration.Templates
{
    /// <summary>
    /// An individual error to be returned in an integration audit result.
    /// </summary>
    public class IntegrationAuditErrorItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IntegrationAuditErrorItem"/> class.
        /// </summary>
        /// <param name="sectionName">The section name.</param>
        /// <param name="fieldName">The field name.</param>
        /// <param name="fieldUrl">The field url.</param>
        /// <param name="value">The value of the field.</param>
        /// <param name="errorMessage">The error message.</param>
        public IntegrationAuditErrorItem(string sectionName, string fieldName, string fieldUrl, string value, string errorMessage)
        {
            this.SectionName = sectionName;
            this.FieldName = fieldName;
            this.FieldUrl = fieldUrl;
            this.Value = value;
            this.ErrorMessage = errorMessage;
        }

        /// <summary>
        /// Gets the section name.
        /// </summary>
        /// <value>The section name.</value>
        public string SectionName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the field name.
        /// </summary>
        /// <value>The field name.</value>
        public string FieldName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the page URL for the field.
        /// </summary>
        /// <value>The page URL for the field.</value>
        public string FieldUrl
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the value of the field.
        /// </summary>
        /// <value>The value of the field.</value>
        public string Value
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the error for this field.
        /// </summary>
        /// <value>The error message.</value>
        public string ErrorMessage
        {
            get;
            private set;
        }
    }
}
