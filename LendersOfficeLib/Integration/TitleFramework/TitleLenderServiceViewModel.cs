﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;

    /// <summary>
    /// View model for Title Serivce Providers.
    /// </summary>
    public class TitleLenderServiceViewModel
    {
        /// <summary>
        /// Gets or sets a value indicating whether this service provider is new.
        /// </summary>
        /// <value>Whether this service provider is new.</value>
        public bool IsNew
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the lender service id.
        /// </summary>
        /// <value>The lender service id.</value>
        public int? LenderServiceId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the public id.
        /// </summary>
        /// <value>The public id.</value>
        public Guid? PublicId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the auto-expired text cache key to the configuration file key.
        /// </summary>
        /// <value>The auto-expired text cache key to the configuration file key.</value>
        public string ConfigurationFileKeyTempKey { get; set; }

        /// <summary>
        /// Gets or sets the associated vendor id.
        /// </summary>
        /// <value>The associated vendor id.</value>
        public int AssociatedVendorId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether non interactive quotes are enabled.
        /// </summary>
        /// <value>Whether non interactive quotes are enabled.</value>
        public bool IsNonInteractiveQuoteEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether interactive quotes are enabled.
        /// </summary>
        /// <value>Whether interactive quotes are enabled.</value>
        public bool IsInteractiveQuoteEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether policy ordering is enabled.
        /// </summary>
        /// <value>Whether policy ordering is enabled.</value>
        public bool IsPolicyEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this service is enabled for LQB users.
        /// </summary>
        /// <value>Whether this service is enabled for LQB users.</value>
        public bool IsEnabledForLqbUsers
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets which employee group this service is enabled for.
        /// </summary>
        /// <value>Which employee group this service is enabled for.</value>
        public Guid? EnabledEmployeeGroupId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this service is enabled for TPO users.
        /// </summary>
        /// <value>Whether this service is enabled for TPO users.</value>
        public bool IsEnabledForTpoUsers
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the oc groups that can use this service.
        /// </summary>
        /// <value>The OC groups that can use this service.</value>
        public Guid? EnabledOCGroupId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether provider codes are used.
        /// </summary>
        /// <value>Whether provider codes are used.</value>
        public bool UsesProviderCodes
        {
            get;
            set;
        }
    }
}
