﻿namespace LendersOffice.Integration.TitleFramework
{
    using LendersOffice.Integration.FrameworkCommon;

    /// <summary>
    /// The title platform view model.
    /// </summary>
    public class TitlePlatformViewModel : IntegrationPlatformViewModel
    {
        /// <summary>
        /// Gets or sets the title transmission model.
        /// </summary>
        /// <value>The title transmission model.</value>
        public TitleTransmissionModelT TransmissionModelT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the payload format type.
        /// </summary>
        /// <value>The payload format type.</value>
        public TitlePayloadFormatT PayloadFormatT
        {
            get;
            set;
        }
    }
}
