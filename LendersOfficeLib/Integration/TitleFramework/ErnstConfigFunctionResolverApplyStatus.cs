﻿namespace LendersOffice.Integration.TitleFramework
{
    /// <summary>
    /// The status after trying to apply a predefined function.
    /// </summary>
    public enum ErnstConfigFunctionResolverApplyStatus
    {
        /// <summary>
        /// There was an error trying to apply the function.
        /// </summary>
        Error,

        /// <summary>
        /// The operands to the operation were invalid for that operation type.
        /// </summary>
        InvalidOperands,

        /// <summary>
        /// The function was applied.
        /// </summary>
        Applied,

        /// <summary>
        /// The conditions to apply the function were not met.
        /// </summary>
        ConditionNotMet
    }
}
