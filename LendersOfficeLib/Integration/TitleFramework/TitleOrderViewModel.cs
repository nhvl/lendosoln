﻿namespace LendersOffice.Integration.TitleFramework
{
    using System.Collections.Generic;

    /// <summary>
    /// Viewmodel for the title order.
    /// </summary>
    public class TitleOrderViewModel
    {
        /// <summary>
        /// Gets or sets the order id.
        /// </summary>
        /// <value>The order id.</value>
        public int OrderId { get; set; }

        /// <summary>
        /// Gets or sets the quote provider/lender service.
        /// </summary>
        /// <value>The quote provider/lender service.</value>
        public string QuoteProvider { get; set; }

        /// <summary>
        /// Gets or sets the title provider/service provider.
        /// </summary>
        /// <value>The title provider/service provider.</value>
        public List<ServiceProviderInfo> TitleProviders { get; set; }

        /// <summary>
        /// Gets or sets the order number.
        /// </summary>
        /// <value>The order number.</value>
        public string OrderNumber { get; set; }

        /// <summary>
        /// Gets or sets who initiated the order.
        /// </summary>
        /// <value>Who initiated the order.</value>
        public string OrderedBy { get; set; }

        /// <summary>
        /// Gets or sets the order date.
        /// </summary>
        /// <value>The order date.</value>
        public string OrderedDate { get; set; }
    }
}
