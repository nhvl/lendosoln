﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;
    using DataAccess;

    /// <summary>
    /// VOX utilities class.
    /// </summary>
    public class TitleUtilities
    {
        /// <summary>
        /// The header for a request log.
        /// </summary>
        public const string RequestLogHeader = "=== TITLE FRAMEWORK REQUEST ===";

        /// <summary>
        /// The header for a response log.
        /// </summary>
        public const string ResponseLogHeader = "=== TITLE FRAMEWORK RESPONSE ===";

        /// <summary>
        /// The header for a timer log.
        /// </summary>
        public const string TimerLogHeader = "=== TITLE FRAMEWORK REQUEST TIMER ===";

        /// <summary>
        /// Logs the payload.
        /// </summary>
        /// <param name="payload">The payload.</param>
        /// <param name="isResponse">Whether this is the response or not.</param>
        public static void LogPayload(string payload, bool isResponse)
        {
            var header = isResponse ? ResponseLogHeader : RequestLogHeader;

            Tools.LogInfo(header + Environment.NewLine + Environment.NewLine + payload);
        }
    }
}