﻿namespace LendersOffice.Integration.TitleFramework
{
    using System.Xml.Linq;
    using DataAccess;

    /// <summary>
    /// Represents a service provider specified in the title framework.
    /// </summary>
    public class ServiceProviderInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceProviderInfo"/> class.
        /// </summary>
        /// <param name="agentRole">The role of the agent created from the provider, used to link this provider with fees.</param>
        /// <param name="name">The name of the provider.</param>
        /// <param name="address">The address of the provider.</param>
        /// <param name="city">The city of the provider.</param>
        /// <param name="state">The state of the provider.</param>
        /// <param name="zip">The postal code of the provider.</param>
        /// <param name="phone">The phone number of the provider.</param>
        /// <param name="website">The website of the provider.</param>
        /// <param name="disclaimer">The disclaimer of the provider.</param>
        public ServiceProviderInfo(E_AgentRoleT agentRole, string name, string address, string city, string state, string zip, string phone, string website, string disclaimer)
        {
            this.AgentRole = agentRole;
            this.Name = name;
            this.Address = address;
            this.City = city;
            this.State = state;
            this.Zip = zip;
            this.Phone = phone;
            this.Website = website;
            this.Disclaimer = disclaimer;
        }

        /// <summary>
        /// Gets the role of the agent created from the provider, used to link this provider with fees.
        /// </summary>
        /// <value>The role of the agent created from the provider, used to link this provider with fees.</value>
        public E_AgentRoleT AgentRole { get; }

        /// <summary>
        /// Gets the name of the provider.
        /// </summary>
        /// <value>The name of the provider.</value>
        public string Name { get; }

        /// <summary>
        /// Gets the address of the provider.
        /// </summary>
        /// <value>The address of the provider.</value>
        public string Address { get; }

        /// <summary>
        /// Gets the city of the provider.
        /// </summary>
        /// <value>The city of the provider.</value>
        public string City { get; }

        /// <summary>
        /// Gets the state of the provider.
        /// </summary>
        /// <value>The state of the provider.</value>
        public string State { get; }

        /// <summary>
        /// Gets the postal code of the provider.
        /// </summary>
        /// <value>The postal code of the provider.</value>
        public string Zip { get; }

        /// <summary>
        /// Gets the phone number of the provider.
        /// </summary>
        /// <value>The phone number of the provider.</value>
        public string Phone { get; }

        /// <summary>
        /// Gets the website of the provider.
        /// </summary>
        /// <value>The website of the provider.</value>
        public string Website { get; }

        /// <summary>
        /// Gets the disclaimer of the provider.
        /// </summary>
        /// <value>The disclaimer of the provider.</value>
        public string Disclaimer { get; }

        /// <summary>
        /// Serializes <paramref name="item"/> into an <seealso cref="XElement"/>.
        /// </summary>
        /// <param name="item">The item to serialize.</param>
        /// <param name="elementName">The name of the element.</param>
        /// <returns>The XML element representation.</returns>
        internal static XElement ToXml(ServiceProviderInfo item, string elementName)
        {
            return new XElement(
                elementName,
                new XAttribute("AgentRole", item.AgentRole.ToString("D")),
                item.Name != null ? new XAttribute("Name", item.Name) : null,
                item.Address != null ? new XAttribute("Address", item.Address) : null,
                item.City != null ? new XAttribute("City", item.City) : null,
                item.State != null ? new XAttribute("State", item.State) : null,
                item.Zip != null ? new XAttribute("Zip", item.Zip) : null,
                item.Phone != null ? new XAttribute("Phone", item.Phone) : null,
                item.Website != null ? new XAttribute("Website", item.Website) : null,
                item.Disclaimer != null ? new XAttribute("Disclaimer", item.Disclaimer) : null);
        }

        /// <summary>
        /// Deserializes <paramref name="element"/> into this type.
        /// </summary>
        /// <param name="element">The serialized XML.</param>
        /// <returns>The deserialized instance.</returns>
        internal static ServiceProviderInfo FromXml(XElement element)
        {
            return new ServiceProviderInfo(
                (E_AgentRoleT)(int)element.Attribute("AgentRole"),
                (string)element.Attribute("Name"),
                (string)element.Attribute("Address"),
                (string)element.Attribute("City"),
                (string)element.Attribute("State"),
                (string)element.Attribute("Zip"),
                (string)element.Attribute("Phone"),
                (string)element.Attribute("Website"),
                (string)element.Attribute("Disclaimer"));
        }
    }
}
