﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;

    /// <summary>
    /// Represents a value for an entry in the configuration for Ernst.
    /// </summary>
    public partial struct ErnstConfigurationValue
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstConfigurationValue"/> struct.
        /// </summary>
        /// <param name="value">The value of the configuration entry.</param>
        private ErnstConfigurationValue(string value)
        {
            this.Value = value;
        }

        /// <summary>
        /// Gets the value of the configuration entry.
        /// </summary>
        /// <value>The value of the configuration entry.</value>
        public string Value { get; }

        /// <summary>
        /// Parses the input value and validates it.
        /// </summary>
        /// <param name="value">The value of the configuration entry.</param>
        /// <returns>The validated value, or null.</returns>
        public static ErnstConfigurationValue? Create(string value)
        {
            return new ErnstConfigurationValue(value); // At this point, validation is handled upstream, via path resolution.
        }
    }

    /// <summary>
    /// Contains the implementation of <see cref="IEquatable{ErnstConfigurationValue}"/> using the <see cref="Value"/> property.
    /// </summary>
    public partial struct ErnstConfigurationValue : IEquatable<ErnstConfigurationValue>
    {
        /// <summary>
        /// The equality operator.
        /// </summary>
        /// <param name="lhs">The instance that is on the left side of the == operator.</param>
        /// <param name="rhs">The instance that is on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(ErnstConfigurationValue lhs, ErnstConfigurationValue rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The inequality operator.
        /// </summary>
        /// <param name="lhs">The instance that is on the left side of the != operator.</param>
        /// <param name="rhs">The instance that is on the right side of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(ErnstConfigurationValue lhs, ErnstConfigurationValue rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Determines if two instances are equal.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(ErnstConfigurationValue other)
        {
            return this.Value.Equals(other.Value);
        }

        /// <summary>
        /// Override the Equals method inherited from the object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if <paramref name="obj"/> is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            return obj is ErnstConfigurationValue && this.Equals((ErnstConfigurationValue)obj);
        }

        /// <summary>
        /// Override of the GetHashCode method inherited from the object class.
        /// </summary>
        /// <returns>An integer this is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }
    }
}
