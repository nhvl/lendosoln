﻿namespace LendersOffice.Integration.TitleFramework
{
    /// <summary>
    /// Contains the result from auditing a Title payload.
    /// </summary>
    public class TitleAuditResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TitleAuditResult"/> class.
        /// </summary>
        public TitleAuditResult()
        {
        }
    }
}
