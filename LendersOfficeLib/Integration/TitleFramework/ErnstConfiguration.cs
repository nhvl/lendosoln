﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Represents a configuration of custom settings for Ernst.
    /// </summary>
    public class ErnstConfiguration
    {
        /// <summary>
        /// The entries of the configuration, specifying values with paths.
        /// </summary>
        /// <remarks>
        /// This list should always be sorted by state, with the null state being the first entries.
        /// </remarks>
        private readonly IReadOnlyList<ErnstConfigurationEntry> entries;

        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstConfiguration"/> class.
        /// </summary>
        /// <param name="fileKey">The unique identifier for the files to be saved/loaded under.</param>
        /// <param name="uploadDate">The date and time the configuration was uploaded.</param>
        /// <param name="entries">The entries of the configuration, specifying values with paths.</param>
        private ErnstConfiguration(Guid fileKey, DateTime uploadDate, IReadOnlyList<ErnstConfigurationEntry> entries)
        {
            this.FileKey = fileKey;
            this.UploadDate = uploadDate;
            this.entries = entries;
        }

        /// <summary>
        /// Gets the date and time the configuration was uploaded.
        /// </summary>
        /// <value>The date and time the configuration was uploaded.</value>
        public DateTime UploadDate { get; }

        /// <summary>
        /// Gets the key referring to the files within the configuration.
        /// </summary>
        /// <value>The key referring to the files within the configuration.</value>
        public Guid FileKey { get; }

        /// <summary>
        /// Gets the File DB Key for the source file for the configuration.
        /// </summary>
        /// <value>The File DB Key for the source file for the configuration.</value>
        private string SourceFileDbKey => this.FileKey.ToString("N") + "_sourceErnstConfig";

        /// <summary>
        /// Gets the File DB Key for the config data file for the configuration.
        /// </summary>
        /// <value>The File DB Key for the config data file for the configuration.</value>
        private string ConfigFileDbKey => this.FileKey.ToString("N") + "_ErnstConfig";

        /// <summary>
        /// Deletes the specified configuration.
        /// </summary>
        /// <param name="fileKey">The file key of the configuration to delete.</param>
        public static void DeleteConfiguration(Guid? fileKey)
        {
            if (fileKey.HasValue && fileKey.Value != Guid.Empty)
            {
                var tempInstance = new ErnstConfiguration(fileKey.Value, default(DateTime), null);
                DataAccess.FileDBTools.Delete(DataAccess.E_FileDB.Normal, tempInstance.SourceFileDbKey);
                DataAccess.FileDBTools.Delete(DataAccess.E_FileDB.Normal, tempInstance.ConfigFileDbKey);
            }
        }

        /// <summary>
        /// Loads the saved configuration specified by <paramref name="fileKey"/>.
        /// </summary>
        /// <param name="fileKey">The unique identifier for this configuration item.</param>
        /// <returns>The loaded configuration, or null if <paramref name="fileKey"/> was not valid.</returns>
        public static ErnstConfiguration LoadSavedConfiguration(Guid? fileKey)
        {
            if (!fileKey.HasValue || fileKey.Value == Guid.Empty)
            {
                return null;
            }

            var tempInstance = new ErnstConfiguration(fileKey.Value, default(DateTime), null);
            string serializedConfig = DataAccess.FileDBTools.ReadDataText(DataAccess.E_FileDB.Normal, tempInstance.ConfigFileDbKey);
            return Deserialize(fileKey.Value, serializedConfig);
        }

        /// <summary>
        /// Creates a new configuration from the specified data and saves it to file DB.
        /// </summary>
        /// <param name="configurationEntries">The entries of the configuration.</param>
        /// <param name="sourceFile">The source file used to make the configuration entries.</param>
        /// <returns>A configuration instance for this file.</returns>
        public static ErnstConfiguration CreateNewConfiguration(IReadOnlyList<ErnstConfigurationEntry> configurationEntries, LocalFilePath sourceFile)
        {
            var newConfig = new ErnstConfiguration(Guid.NewGuid(), DateTime.Now, configurationEntries);

            string serializedConfig = newConfig.Serialize();
            DataAccess.FileDBTools.WriteFile(DataAccess.E_FileDB.Normal, newConfig.SourceFileDbKey, sourceFile.Value);
            DataAccess.FileDBTools.WriteData(DataAccess.E_FileDB.Normal, newConfig.ConfigFileDbKey, serializedConfig);
            return newConfig;
        }

        /// <summary>
        /// Gets the entries to apply based on state and loan type.
        /// Note that the enumerable does contain both normal entries and entries with predefined functions in it.
        /// </summary>
        /// <param name="state">The state to filter by.</param>
        /// <param name="loanType">The loan type to filter by.</param>
        /// <returns>An enumerable containing all the entries from the config file to apply.</returns>
        public IEnumerable<ErnstConfigurationEntry> GetPathsFor(UnitedStatesState? state, E_sLT loanType)
        {
            Dictionary<Tuple<SimpleXPath, ErnstConfigurationPredefinedFunction>, ErnstConfigurationEntry> functionEntries = new Dictionary<Tuple<SimpleXPath, ErnstConfigurationPredefinedFunction>, ErnstConfigurationEntry>();
            
            // The entries are already sorted from emtpty > loan type > state > both. This means the last entry for a path is the correct one.
            foreach (var entry in this.entries)
            {
                if ((entry.State == null || entry.State == state) &&
                    (entry.LoanType == null || entry.LoanType.Value == loanType))
                {
                    // Keying off of both the path and predefined function should suffice to pick up everything needed.
                    // Having the predefined function as part of the composite key is necessary so that predefined function entries don't override initial entries.
                    functionEntries[new Tuple<SimpleXPath, ErnstConfigurationPredefinedFunction>(entry.PathIdentifier, entry.PredefinedFunction)] = entry;
                }
            }

            return functionEntries.Values;
        }

        /// <summary>
        /// Consumes a readonly copy of the source file.  Intended for copying to the UI's download link.
        /// </summary>
        /// <param name="action">The action to perform on the copy of the source file.</param>
        public void UseSourceFile(Action<System.IO.FileInfo> action)
        {
            DataAccess.FileDBTools.UseFile(DataAccess.E_FileDB.Normal, this.SourceFileDbKey, action);
        }

        /// <summary>
        /// Loads a new configuration from a string.
        /// </summary>
        /// <param name="fileKey">The file key <paramref name="serializedConfiguration"/> was loaded from.</param>
        /// <param name="serializedConfiguration">The serialized configuration.</param>
        /// <returns>A configuration instance generated from <paramref name="serializedConfiguration"/>.</returns>
        private static ErnstConfiguration Deserialize(Guid fileKey, string serializedConfiguration)
        {
            var root = XElement.Parse(serializedConfiguration);
            if (root.Name != "ernstConfig")
            {
                var context = new LqbGrammar.Exceptions.SimpleContext("Invalid root element name. Expected \"ernstConfig\"; got \"" + root.Name + "\".");
                throw new LqbGrammar.Exceptions.DeveloperException(ErrorMessage.SystemError, context);
            }

            DateTime uploadDate = new DateTime(long.Parse(root.Attribute("uploaded").Value));

            var entries = new List<ErnstConfigurationEntry>();
            foreach (XElement entryElement in root.Elements("entry"))
            {
                entries.Add(new ErnstConfigurationEntry(
                    SimpleXPath.Create(entryElement.Attribute("path").Value).ForceValue(),
                    UnitedStatesState.CreateWithValidation(entryElement.Attribute("state")?.Value),
                    ErnstConfigurationDescription.Create(entryElement.Attribute("description").Value).ForceValue(),
                    ErnstConfigurationValue.Create(entryElement.Attribute("default").Value).ForceValue(),
                    entryElement.Attribute("loanType")?.Value.ToNullableEnum<E_sLT>(ignoreCase: true),
                    ErnstConfigurationPredefinedFunction.Create(entryElement.Attribute("predefinedFunction")?.Value)));
            }

            return new ErnstConfiguration(fileKey, uploadDate, entries);
        }

        /// <summary>
        /// Serializes the configuration to a string.
        /// </summary>
        /// <returns>The configuration as a string.</returns>
        private string Serialize()
        {
            var root = new XElement(
                "ernstConfig",
                new XAttribute("uploaded", this.UploadDate.Ticks),
                this.entries.OrderBy(entry => entry.State?.StateAbbreviation).ThenBy(entry => entry.LoanType).ThenBy(entry => entry.PredefinedFunction?.Text)
                .Select(entry => new XElement(
                    "entry",
                    new XAttribute("path", entry.PathIdentifier.ToString()),
                    entry.State.HasValue ? new XAttribute("state", entry.State.Value.StateAbbreviation) : null,
                    new XAttribute("description", entry.Description.Text),
                    new XAttribute("default", entry.DefaultValue.Value),
                    entry.PredefinedFunction != null ? new XAttribute("predefinedFunction", entry.PredefinedFunction.Text) : null,
                    entry.LoanType.HasValue ? new XAttribute("loanType", entry.LoanType.Value.ToString("D")) : null)));
            return root.ToString(SaveOptions.DisableFormatting);
        }
    }
}
