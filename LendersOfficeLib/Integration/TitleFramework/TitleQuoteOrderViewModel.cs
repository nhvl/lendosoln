﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;

    /// <summary>
    /// Models the UI inputs from the "Order Title Quote" page.
    /// </summary>
    public class TitleQuoteOrderViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TitleQuoteOrderViewModel"/> class.
        /// </summary>
        /// <param name="loanId">The associated loan file ID.</param>
        /// <param name="lenderServiceId">The ID of the lender-level configuration to use for the request.</param>
        /// <param name="titleProviderId">The ID of the RolodexDB entry corresponding to the selected title provider.</param>
        /// <param name="titleProviderPreviousOrderId">The <see cref="TitleOrder.OrderId"/> of the previous order to pull Title information from.</param>
        /// <param name="feesToInclude">A flags enum indicating which fees should be included in the quote.</param>
        /// <param name="accountId">The account id to use to authenticate with the vendor.</param>
        /// <param name="username">The username to use to authenticate with the vendor.</param>
        /// <param name="password">The password to use to authenticate with the vendor.</param>
        public TitleQuoteOrderViewModel(Guid loanId, int lenderServiceId, Guid? titleProviderId, int? titleProviderPreviousOrderId, ErnstQuoteFeeCategoryFlags feesToInclude, string accountId, string username, string password)
        {
            this.LoanId = loanId;
            this.LenderServiceId = lenderServiceId;
            this.TitleProviderId = titleProviderId;
            this.TitleProviderPreviousOrderId = titleProviderPreviousOrderId;
            this.FeesToInclude = feesToInclude;
            this.AccountId = accountId;
            this.Username = username;
            this.Password = password;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TitleQuoteOrderViewModel"/> class.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="lenderServiceId">The lender service id.</param>
        /// <param name="titleProviderCode">The title provider code.</param>
        /// <param name="feesToInclude">The fees to include.</param>
        /// <param name="accountId">The account id.</param>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        public TitleQuoteOrderViewModel(Guid loanId, int lenderServiceId, string titleProviderCode, ErnstQuoteFeeCategoryFlags feesToInclude, string accountId, string username, string password)
        {
            this.LoanId = loanId;
            this.LenderServiceId = lenderServiceId;
            this.TitleProviderCode = titleProviderCode;
            this.FeesToInclude = feesToInclude;
            this.AccountId = accountId;
            this.Username = username;
            this.Password = password;
        }

        /// <summary>
        /// Gets the associated loan file ID.
        /// </summary>
        /// <value>The loan ID.</value>
        public Guid LoanId { get; }

        /// <summary>
        /// Gets the ID of the lender-level configuration to use for the request.
        /// </summary>
        /// <value>Lender service ID.</value> 
        public int LenderServiceId { get; }

        /// <summary>
        /// Gets the ID of the RolodexDB entry corresponding to the selected title provider
        /// whose provider code will be sent to the title vendor.
        /// </summary>
        /// <value>Rolodex ID holding Title Provider Code.</value>
        public Guid? TitleProviderId { get; }

        /// <summary>
        /// Gets the <see cref="TitleOrder.OrderId"/> of an existing order to copy title provider information from.
        /// </summary>
        /// <value>The <see cref="TitleOrder.OrderId"/> of an existing order to copy title provider information from.</value>
        public int? TitleProviderPreviousOrderId { get; }

        /// <summary>
        /// Gets the title provider code.
        /// </summary>
        /// <value>The title provider code.</value>
        /// <remarks>
        /// Either the raw title provider code can be added OR the <see cref="TitleProviderPreviousOrderId"/> OR the <see cref="TitleProviderId"/> can be used to extract a title provider code.
        /// </remarks>
        public string TitleProviderCode { get; }

        /// <summary>
        /// Gets a flags enum indicating which fees should be included in the quote.
        /// </summary>
        /// <value>Fees to include.</value>
        public ErnstQuoteFeeCategoryFlags FeesToInclude { get; }

        /// <summary>
        /// Gets the account id to use to authenticate with the vendor.
        /// </summary>
        /// <value>The account id to use to authenticate with the vendor.</value>
        public string AccountId { get; }

        /// <summary>
        /// Gets the username to use to authenticate with the vendor.
        /// </summary>
        /// <value>The username to use to authenticate with the vendor.</value>
        public string Username { get; }

        /// <summary>
        /// Gets the password to use to authenticate with the vendor.
        /// </summary>
        /// <value>The password to use to authenticate with the vendor.</value>
        public string Password { get; }
    }
}