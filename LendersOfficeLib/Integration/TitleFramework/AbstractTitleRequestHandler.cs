﻿namespace LendersOffice.Integration.TitleFramework
{
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.ConfigSystem.Operations;
    using LendersOffice.Conversions.Templates;
    using Templates;

    /// <summary>
    /// Title request handler.
    /// </summary>
    public abstract class AbstractTitleRequestHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractTitleRequestHandler"/> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        protected AbstractTitleRequestHandler(AbstractTitleRequestData requestData)
        {
            this.RequestData = requestData;
        }

        /// <summary>
        /// Gets the request data.
        /// </summary>
        /// <value>The request data.</value>
        protected AbstractTitleRequestData RequestData { get; }

        /// <summary>
        /// Gets the required workflow operation to order Title quotes.
        /// </summary>
        /// <value>The required workflow operation.</value>
        protected WorkflowOperation RequiredOperation => WorkflowOperations.OrderTitleQuote;

        /// <summary>
        /// Gets or sets the request provider.
        /// </summary>
        /// <value>The request provider.</value>
        protected IRequestProvider RequestProvider { get; set; }

        /// <summary>
        /// Gets or sets the server.
        /// </summary>
        /// <value>The server.</value>
        protected virtual ITitleServer Server { get; set; }

        /// <summary>
        /// Creates the appropriate request handler based on values from the request data.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        /// <returns>The appropriate request handler.</returns>
        /// <remarks>We need to use this since we can't create a request handler directly like in VOX since it'll be based off of the PayloadFormat of the chosen LenderService.</remarks>
        public static AbstractTitleRequestHandler CreateRequestHandler(AbstractTitleRequestData requestData)
        {
            switch (requestData.PayloadFormat)
            {
                case TitlePayloadFormatT.ErnstXml:
                    return new ErnstTitleRequestHandler(requestData);
                default:
                    throw new UnhandledEnumException(requestData.PayloadFormat);
            }
        }

        /// <summary>
        /// Audits the request.
        /// </summary>
        /// <returns>The audit results.</returns>
        public IntegrationAuditResult AuditRequest()
        {
            return this.RequestProvider.AuditRequest();
        }

        /// <summary>
        /// Submits the request.
        /// </summary>
        /// <param name="errors">Any errors found.</param>
        /// <param name="doAudit">Whether this should perform an audit before submission.</param>
        /// <returns>The title order if successful, null otherwise.</returns>
        public TitleOrder SubmitRequest(out List<string> errors, bool doAudit = true)
        {
            errors = new List<string>();

            var workflowResults = Tools.IsWorkflowOperationAuthorized(this.RequestData.Principal, this.RequestData.LoanId, this.RequiredOperation);
            if (!workflowResults.Item1)
            {
                errors.Add(workflowResults.Item2);
                return null;
            }

            var payload = this.RequestProvider.SerializeRequest();
            this.RequestProvider.LogRequest(payload);

            if (doAudit)
            {
                var auditErrors = this.AuditRequest();
                if (auditErrors.HasErrors)
                {
                    errors.AddRange(auditErrors.GetAllErrors().Select(auditItem => auditItem.ErrorMessage).ToList());
                    return null;
                }
            }

            TitleOrderSubmittedConfiguration configuration = this.GetExportedConfigItems();
            string response = this.Server.PlaceRequest(payload);
            ITitleQuoteResponseData responseData = this.ParseResponse(response, out errors);
            if (responseData == null)
            {
                return null;
            }

            TitleOrder titleOrder = TitleOrder.CreateNewOrder(this.RequestData, responseData, configuration);
            titleOrder.Save();

            return titleOrder;
        }

        /// <summary>
        /// Parses the vendor response.
        /// </summary>
        /// <param name="response">The string response from the vendor.</param>
        /// <param name="errors">A list of errors encountered while parsing the response.</param>
        /// <returns>A parsed vendor response.</returns>
        protected abstract ITitleQuoteResponseData ParseResponse(string response, out List<string> errors);

        /// <summary>
        /// Gets the configuration items sent in the request.
        /// </summary>
        /// <returns>The configuration items sent over in the request.</returns>
        protected abstract TitleOrderSubmittedConfiguration GetExportedConfigItems();
    }
}
