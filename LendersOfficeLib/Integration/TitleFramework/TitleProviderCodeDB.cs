﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Common;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Handles database operations for the Title provider codes.
    /// </summary>
    public static class TitleProviderCodeDB
    {
        /// <summary>
        /// The name of the stored procedure for saving.
        /// </summary>
        private const string SaveSp = "TITLE_FRAMEWORK_PROVIDER_CODE_Save";

        /// <summary>
        /// The name of the stored procedure for retrieving.
        /// </summary>
        private const string RetrieveSp = "TITLE_FRAMEWORK_PROVIDER_CODE_Retrieve";

        /// <summary>
        /// The name of the stored procedure for deleting.
        /// </summary>
        private const string DeleteSp = "TITLE_FRAMEWORK_PROVIDER_CODE_Delete";

        /// <summary>
        /// Saves a provider code to the database.
        /// </summary>
        /// <param name="exec">The context of the DB call.</param>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="agentId">The agent ID.</param>
        /// <param name="providerCode">The provider code.</param>
        public static void Save(CStoredProcedureExec exec, Guid brokerId, Guid agentId, string providerCode)
        {
            var parameters = GetParameters(brokerId, agentId, providerCode);
            var spName = StoredProcedureName.Create(SaveSp).ForceValue();

            exec.ExecuteNonQuery(spName.ToString(), parameters.ToArray());
        }

        /// <summary>
        /// Retrieves a provider code from the database.
        /// </summary>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="agentId">The agent ID.</param>
        /// <returns>The provider code, or null if none was found.</returns>
        public static string Retrieve(Guid brokerId, Guid agentId)
        {
            var parameters = GetParameters(brokerId, agentId);
            var spName = StoredProcedureName.Create(RetrieveSp).ForceValue();

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);

            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            using (var reader = driver.ExecuteReader(connection, null, spName, parameters))
            {
                if (reader.Read())
                {
                    return (string)reader["ProviderCode"];
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Deletes a provider code from the database.
        /// </summary>
        /// <param name="exec">The context of the DB call.</param>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="agentId">The agent ID.</param>
        public static void Delete(CStoredProcedureExec exec, Guid brokerId, Guid agentId)
        {
            var parameters = GetParameters(brokerId, agentId);
            var spName = StoredProcedureName.Create(DeleteSp).ForceValue();

            exec.ExecuteNonQuery(spName.ToString(), parameters.ToArray());
        }

        /// <summary>
        /// Gets the SQL parameters for a procedure.
        /// </summary>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="agentId">The agent ID.</param>
        /// <param name="providerCode">The provider code.</param>
        /// <returns>A list of SQL parameters for a procedure.</returns>
        private static List<SqlParameter> GetParameters(Guid brokerId, Guid agentId, string providerCode = null)
        {
            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@BrokerId", brokerId));
            parameters.Add(new SqlParameter("@AgentId", agentId));

            if (!string.IsNullOrEmpty(providerCode))
            {
                parameters.Add(new SqlParameter("@ProviderCode", providerCode));
            }

            return parameters;
        }
    }
}
