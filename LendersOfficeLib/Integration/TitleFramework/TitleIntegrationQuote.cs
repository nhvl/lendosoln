﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.TitleProvider;

    /// <summary>
    /// Represents a quote via the title integration.
    /// </summary>
    public class TitleIntegrationQuote
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TitleIntegrationQuote"/> class.
        /// </summary>
        /// <param name="feeTypes">The types of fees on the quote.</param>
        /// <param name="providers">The providers specified on the quote.</param>
        /// <param name="messages">The messages specified on the quote.</param>
        /// <param name="errorInQuoteForSettlement">A value indicating whether there was an error getting a quote for the settlement fee.</param>
        /// <param name="errorInQuoteForEndorsement">A value indicating whether there was an error getting a quote for the endorsement fee.</param>
        /// <param name="errorInQuoteForLendersPolicy">A value indicating whether there was an error getting a quote for the lender's policy.</param>
        /// <param name="errorInQuoteForOwnersPolicy">A value indicating whether there was an error getting a quote for the owner's policy.</param>
        public TitleIntegrationQuote(
            IReadOnlyList<TitleIntegrationFeeType> feeTypes,
            IReadOnlyList<ServiceProviderInfo> providers,
            IReadOnlyList<string> messages,
            bool errorInQuoteForSettlement,
            bool errorInQuoteForEndorsement,
            bool errorInQuoteForLendersPolicy,
            bool errorInQuoteForOwnersPolicy)
        {
            this.FeeTypes = feeTypes;
            this.Providers = providers;
            this.Messages = messages;
            this.ErrorInQuoteForSettlement = errorInQuoteForSettlement;
            this.ErrorInQuoteForEndorsement = errorInQuoteForEndorsement;
            this.ErrorInQuoteForLendersPolicy = errorInQuoteForLendersPolicy;
            this.ErrorInQuoteForOwnersPolicy = errorInQuoteForOwnersPolicy;
        }

        /// <summary>
        /// Gets the types of fees on the quote.
        /// </summary>
        /// <value>The types of fees on the quote.</value>
        public IReadOnlyList<TitleIntegrationFeeType> FeeTypes { get; }

        /// <summary>
        /// Gets the providers specified on the quote.
        /// </summary>
        /// <value>The providers specified on the quote.</value>
        public IReadOnlyList<ServiceProviderInfo> Providers { get; }

        /// <summary>
        /// Gets the messages specified on the quote.
        /// </summary>
        /// <value>The messages specified on the quote.</value>
        public IReadOnlyList<string> Messages { get; }

        /// <summary>
        /// Gets a value indicating whether there was an error getting a quote for the settlement fee.
        /// </summary>
        /// <value>A value indicating whether there was an error getting a quote for the settlement fee.</value>
        public bool ErrorInQuoteForSettlement { get; }

        /// <summary>
        /// Gets a value indicating whether there was an error getting a quote for the endorsement fee.
        /// </summary>
        /// <value>A value indicating whether there was an error getting a quote for the endorsement fee.</value>
        public bool ErrorInQuoteForEndorsement { get; }

        /// <summary>
        /// Gets a value indicating whether there was an error getting a quote for the lender's policy.
        /// </summary>
        /// <value>A value indicating whether there was an error getting a quote for the lender's policy.</value>
        public bool ErrorInQuoteForLendersPolicy { get; }

        /// <summary>
        /// Gets a value indicating whether there was an error getting a quote for the owner's policy.
        /// </summary>
        /// <value>A value indicating whether there was an error getting a quote for the owner's policy.</value>
        public bool ErrorInQuoteForOwnersPolicy { get; }

        /// <summary>
        /// Gets the warning messages from this quote to display to the user.
        /// </summary>
        /// <value>The warning messages from this quote to display to the user.</value>
        public IReadOnlyList<string> WarningMessages
        {
            get
            {
                if (this.Messages.Any())
                {
                    return this.Messages;
                }
                else if (this.ErrorInQuoteForSettlement || this.ErrorInQuoteForEndorsement || this.ErrorInQuoteForLendersPolicy || this.ErrorInQuoteForOwnersPolicy)
                {
                    return new List<string>(1) { "One or more fees are not currently available online. Please contact the service provider listed." };
                }

                return new List<string>(0);
            }
        }

        /// <summary>
        /// Serializes <paramref name="quote"/> into an XML string.
        /// </summary>
        /// <param name="quote">The instance to serialize.</param>
        /// <returns>The XML representation of the instance.</returns>
        public static string ToXml(TitleIntegrationQuote quote)
        {
            if (quote == null)
            {
                return null;
            }

            var quoteRoot = new XElement(
                "quote",
                new XAttribute("ErrorForSettlement", quote.ErrorInQuoteForSettlement),
                new XAttribute("ErrorForEndorsement", quote.ErrorInQuoteForEndorsement),
                new XAttribute("ErrorForLenderPolicy", quote.ErrorInQuoteForLendersPolicy),
                new XAttribute("ErrorForOwnerPolicy", quote.ErrorInQuoteForOwnersPolicy),
                quote.FeeTypes.Select(type => TitleIntegrationFeeType.ToXml(type, "feeType")),
                quote.Providers.Select(provider => ServiceProviderInfo.ToXml(provider, "provider")),
                quote.Messages.Select(message => new XElement("message", message)));
            return quoteRoot.ToString(SaveOptions.DisableFormatting);
        }

        /// <summary>
        /// Deserializes <paramref name="xml"/> into an instance of <see cref="TitleIntegrationQuote"/>.
        /// </summary>
        /// <param name="xml">The XML representation of the instance.</param>
        /// <returns>An instance of this type, provided <paramref name="xml"/> is valid.</returns>
        public static TitleIntegrationQuote FromXml(string xml)
        {
            if (string.IsNullOrEmpty(xml))
            {
                return null;
            }

            var root = XElement.Parse(xml);
            if (root.Name != "quote")
            {
                throw DataAccess.CBaseException.GenericException("Expected a quote root element, but got " + root.Name + " instead.");
            }

            return new TitleIntegrationQuote(
                root.Elements("feeType").Select(element => TitleIntegrationFeeType.FromXml(element)).ToList(),
                root.Elements("provider").Select(element => ServiceProviderInfo.FromXml(element)).ToList(),
                root.Elements("message").Select(element => (string)element).ToList(),
                (bool)root.Attribute("ErrorForSettlement"),
                (bool)root.Attribute("ErrorForEndorsement"),
                (bool)root.Attribute("ErrorForLenderPolicy"),
                (bool)root.Attribute("ErrorForOwnerPolicy"));
        }

        /// <summary>
        /// Applies the values from this instance to the loan file.
        /// </summary>
        /// <param name="loan">The loan to apply the values to.</param>
        /// <returns>The fees modified by applying the quote to the loan.</returns>
        internal List<ModifiedFeeData> ApplyQuoteToLoan(CPageData loan)
        {
            // This application process is largely a copy-paste of the work in LendersOffice.ObjLib.TitleProvider.TitleService
            // Please be sure to update both locations for fee-related changes.
            bool useLegacyClosingCosts = loan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy;
            bool enableSubFees = loan.BrokerDB.EnableSubFees;
            AvailableSettlementServiceProviders loanSettlementServiceProviders = loan.sAvailableSettlementServiceProviders;
            bool loanSettlementServiceProvidersUpdated = false;

            Dictionary<E_AgentRoleT, CAgentFields> quoteAgents = new Dictionary<E_AgentRoleT, CAgentFields>();
            foreach (ServiceProviderInfo serviceProvider in this.Providers)
            {
                var agentsOfRole = loan.GetAgentsOfRole(serviceProvider.AgentRole, E_ReturnOptionIfNotExist.ReturnEmptyObject)
                    .Where(a => a.IsValid);
                bool providerIsDuplicate = false;

                foreach (var agent in agentsOfRole)
                {
                    if (IsProviderDuplicateOfAgent(serviceProvider, agent))
                    {
                        providerIsDuplicate = true;
                        quoteAgents.Add(agent.AgentRoleT, agent);
                        break;
                    }
                }
                
                if (!agentsOfRole.Any() || !providerIsDuplicate)
                {
                    var agent = GenerateNewAgent(loan, serviceProvider);
                    quoteAgents.Add(agent.AgentRoleT, agent);
                }
            }

            List<ModifiedFeeData> modifiedFees = new List<ModifiedFeeData>();
            foreach (TitleIntegrationFeeType feeType in this.FeeTypes)
            {
                CAgentFields associatedAgent = feeType.ProviderRole.HasValue ? quoteAgents[feeType.ProviderRole.Value] : null;
                var sspAssociator = new FeePaidToAssociator(associatedAgent, loanSettlementServiceProviders);
                if (useLegacyClosingCosts)
                {
                    modifiedFees.Add(ApplyLegacyClosingCost(loan, feeType));
                }
                else if (enableSubFees)
                {
                    modifiedFees.AddRange(ApplySplitTitleFeeToClosingCostSet(loan, feeType, sspAssociator));
                }
                else
                {
                    TitleIntegrationFee totalFee = feeType.TotalFee;
                    if (totalFee.BorrowerResponsibleAmount.HasValue)
                    {
                        bool wasDeleted;
                        var modifiedFee = CreateTitleFee(feeType.FeeType, totalFee.BorrowerResponsibleAmount.Value, loan.sClosingCostSet, loan.sBrokerId, sspAssociator, out wasDeleted);
                        modifiedFees.Add(new ModifiedFeeData(modifiedFee, wasDeleted: wasDeleted));
                    }

                    if (totalFee.SellerResponsibleAmount.HasValue)
                    {
                        bool wasDeleted;
                        var modifiedFee = CreateTitleFee(feeType.FeeType, totalFee.SellerResponsibleAmount.Value, loan.sSellerResponsibleClosingCostSet, loan.sBrokerId, sspAssociator, out wasDeleted);
                        modifiedFees.Add(new ModifiedFeeData(modifiedFee, wasDeleted: wasDeleted));
                    }
                }

                loanSettlementServiceProvidersUpdated |= sspAssociator.IsSettlementServiceProviderListUpdated;
            }

            if (loanSettlementServiceProvidersUpdated)
            {
                loan.sAvailableSettlementServiceProviders = loanSettlementServiceProviders;
            }

            return modifiedFees;
        }

        /// <summary>
        /// Indicates whether the service provider result is a duplicate of the given agent.
        /// </summary>
        /// <param name="provider">The service provider from a quote result.</param>
        /// <param name="agent">The agent to overwrite.</param>
        /// <returns>A boolean indicating whether the service provider result is a duplicate of the given agent.</returns>
        private static bool IsProviderDuplicateOfAgent(ServiceProviderInfo provider, CAgentFields agent)
        {
            if (!string.IsNullOrEmpty(provider.Name) && !provider.Name.Equals(agent.CompanyName, StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            if (!string.IsNullOrEmpty(provider.Address) && !provider.Address.Equals(agent.StreetAddr, StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            if (!string.IsNullOrEmpty(provider.City) && !provider.City.Equals(agent.City, StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            if (!string.IsNullOrEmpty(provider.State) && !provider.State.Equals(agent.State, StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            if (!string.IsNullOrEmpty(provider.Zip) && !provider.Zip.Equals(agent.Zip, StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            if (!string.IsNullOrEmpty(provider.Phone))
            {
                // Check phone number irrespective of format.
                var nonDigitRegexPattern = "[^0-9]";
                var providerPhoneDigitsOnly = Regex.Replace(provider.Phone, nonDigitRegexPattern, string.Empty);
                var agentPhoneDigitsOnly = Regex.Replace(agent.Phone, nonDigitRegexPattern, string.Empty);
                if (!providerPhoneDigitsOnly.Equals(agentPhoneDigitsOnly, StringComparison.OrdinalIgnoreCase))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Applies the data from a service provider result to the given agent.
        /// </summary>
        /// <param name="loan">The loan to which an agent is being added.</param>
        /// <param name="provider">The service provider from a quote result.</param>
        /// <returns>The newly generated agent.</returns>
        private static CAgentFields GenerateNewAgent(CPageData loan, ServiceProviderInfo provider)
        {
            var agent = loan.GetAgentFields(-1);
            agent.AgentRoleT = provider.AgentRole;
            agent.CompanyName = provider.Name;
            agent.StreetAddr = provider.Address;
            agent.City = provider.City;
            agent.State = provider.State;
            agent.Zip = provider.Zip;
            agent.Phone = provider.Phone;
            agent.Update();

            return agent;
        }

        /// <summary>
        /// Applies the fee type to the loan as a legacy fees.
        /// </summary>
        /// <param name="loan">The loan to apply the fees to.</param>
        /// <param name="feeType">The fee type collection of fees to apply.</param>
        /// <returns>The modified fee.</returns>
        private static ModifiedFeeData ApplyLegacyClosingCost(CPageData loan, TitleIntegrationFeeType feeType)
        {
            TitleIntegrationFee totalFee = feeType.TotalFee;
            if (!totalFee.BorrowerResponsibleAmount.HasValue)
            {
                return null;
            }

            decimal total = totalFee.BorrowerResponsibleAmount.Value;

            // I need the _rep formatted amount.
            switch (feeType.FeeType)
            {
                case E_LegacyGfeFieldT.sRecDeed:
                    loan.sRecFLckd = false;
                    loan.sRecDeed_rep = total.ToString();
                    break;
                case E_LegacyGfeFieldT.sRecMortgage:
                    loan.sRecFLckd = false;
                    loan.sRecMortgage_rep = total.ToString();
                    break;
                case E_LegacyGfeFieldT.sRecRelease:
                    loan.sRecFLckd = false;
                    loan.sRecRelease_rep = total.ToString();
                    break;
                case E_LegacyGfeFieldT.sStateRtc:
                    loan.sCountyRtcPc = 0;
                    loan.sStateRtcMb = total;
                    break;
                case E_LegacyGfeFieldT.sCountyRtc:
                    loan.sCountyRtcPc = 0;
                    loan.sCountyRtcMb = total;
                    break;
                case E_LegacyGfeFieldT.sEscrowF:
                    loan.sEscrowF = total;
                    break;
                case E_LegacyGfeFieldT.sTitleInsF:
                    loan.sTitleInsF = total;
                    break;
                case E_LegacyGfeFieldT.sOwnerTitleInsF:
                    loan.sOwnerTitleInsF = total;
                    break;
                default:
                    throw new UnhandledEnumException(feeType.FeeType);
            }

            // We're going to pull from the legacy fee list for these.
            var defaultFee = DefaultSystemClosingCostFee.LegacyFeeList.First(fee => fee.LegacyGfeFieldT == feeType.FeeType);
            var hudline = defaultFee.HudLine;
            var description = defaultFee.Description;

            return new ModifiedFeeData(hudline, description, total, null, null, null, responsibleParty: ModifiedFeeResponsibleParty.Borrower, wasDeleted: false);
        }

        /// <summary>
        /// Applies the fee type to the loan as a split fees; that is, every individual sub item is created as a sub fee.
        /// </summary>
        /// <param name="loan">The loan to apply the fees to.</param>
        /// <param name="feeType">The fee type collection of fees to apply.</param>
        /// <param name="sspAssociator">An object capable of associating a fee to a locally bound settlement service provider list.</param>
        /// <returns>List of modified fees.</returns>
        private static List<ModifiedFeeData> ApplySplitTitleFeeToClosingCostSet(CPageData loan, TitleIntegrationFeeType feeType, FeePaidToAssociator sspAssociator)
        {
            TitleIntegrationFee totalFee = feeType.TotalFee;
            if (totalFee.BorrowerResponsibleAmount.HasValue)
            {
                TitleService.DeleteExistingFeesFromClosingCostSet(feeType.FeeType, loan.sClosingCostSet);
            }

            if (totalFee.SellerResponsibleAmount.HasValue)
            {
                TitleService.DeleteExistingFeesFromClosingCostSet(feeType.FeeType, loan.sSellerResponsibleClosingCostSet);
            }

            // Generate list of used HUD lines.
            HashSet<int> hudlinesInUse = new HashSet<int>(loan.sClosingCostSet.GetFees(null).Select(f => f.HudLine));
            hudlinesInUse.UnionWith(loan.sSellerResponsibleClosingCostSet.GetFees(null).Select(f => f.HudLine));

            // Get list of reserved HUD lines (may intersect with used HUD lines, which is fine). 
            HashSet<int> reservedHudLines = new HashSet<int>(loan.sDynamicClosingCostFeeHistory.Items.Values.Select(i => i.HudLine));
            reservedHudLines.UnionWith(loan.BrokerDB.GetUnlinkedClosingCostSet().GetFees(null).Select(f => f.HudLine));

            List<ModifiedFeeData> modifiedFees = new List<ModifiedFeeData>();
            foreach (TitleIntegrationFee fee in feeType.Fees)
            {
                DynamicClosingCostFeeHistoryItem historyItem = loan.sDynamicClosingCostFeeHistory.Items.GetValueOrNull(fee.Description);
                if (fee.BorrowerResponsibleAmount.HasValue && fee.BorrowerResponsibleAmount.Value != 0)
                {
                    var createdFee = CreateSplitTitleFee(
                                 feeType.FeeType,
                                 fee.Description,
                                 fee.BorrowerResponsibleAmount.Value,
                                 sspAssociator,
                                 loan.sClosingCostSet,
                                 loan.sBrokerId,
                                 historyItem,
                                 hudlinesInUse,
                                 reservedHudLines);
                    modifiedFees.Add(new ModifiedFeeData(createdFee, wasDeleted: false));
                }

                if (fee.SellerResponsibleAmount.HasValue && fee.SellerResponsibleAmount.Value != 0)
                {
                    var createdFee = CreateSplitTitleFee(
                                feeType.FeeType,
                                fee.Description,
                                fee.SellerResponsibleAmount.Value,
                                sspAssociator,
                                loan.sSellerResponsibleClosingCostSet,
                                loan.sBrokerId,
                                historyItem,
                                hudlinesInUse,
                                reservedHudLines);
                    modifiedFees.Add(new ModifiedFeeData(createdFee, wasDeleted: false));
                }
            }

            return modifiedFees;
        }

        /// <summary>
        /// Creates a split fee from some part of a general fee and adds it to the specified closing cost set.
        /// </summary>
        /// <param name="feeType">The type of fee to create.</param>
        /// <param name="description">The description of the fee.</param>
        /// <param name="feeAmount">The amount of the fee.</param>
        /// <param name="sspAssociator">An object capable of associating a fee to a locally bound settlement service provider list.</param>
        /// <param name="closingCostSet">The closing cost set to add the fee to.</param>
        /// <param name="brokerId">The lender's identifier.</param>
        /// <param name="historyItem">The history item that contains previous hudline or type information, or null if no history was found.</param>
        /// <param name="hudlinesInUse">The HUD lines already in use in the loan file.</param>
        /// <param name="reservedHudLines">The reserved or special HUD lines which have either appeared previously on the loan or in the lender's fee type setup.</param>
        /// <returns>The created fee.</returns>
        private static LoanClosingCostFee CreateSplitTitleFee(
            E_LegacyGfeFieldT feeType,
            string description,
            decimal feeAmount,
            FeePaidToAssociator sspAssociator,
            LoanClosingCostSet closingCostSet,
            Guid brokerId,
            DynamicClosingCostFeeHistoryItem historyItem,
            HashSet<int> hudlinesInUse,
            HashSet<int> reservedHudLines)
        {
            LoanClosingCostFee newFee = closingCostSet.GetBrokerFee(brokerId, feeType, addFeeToSet: false);

            newFee.SourceFeeTypeId = newFee.ClosingCostFeeTypeId;
            newFee.ClosingCostFeeTypeId = historyItem?.ClosingCostFeeTypeId ?? Guid.NewGuid();
            newFee.Description = description;
            newFee.OriginalDescription = description;
            newFee.FormulaT = E_ClosingCostFeeFormulaT.MinBaseOnly;
            newFee.BaseAmount = feeAmount;
            sspAssociator.AssociatePaidToAndSettlementServiceProvider(newFee);
            var hudline = TitleService.GetHudLine(newFee.HudLine, closingCostSet, feeType, historyItem, hudlinesInUse, reservedHudLines);
            newFee.HudLine = hudline;

            closingCostSet.AddOrUpdate(newFee);
            return newFee;
        }

        /// <summary>
        /// Creates a normal fee in the closing cost set with the specified values.
        /// </summary>
        /// <param name="feeType">The type of fee to create.</param>
        /// <param name="feeAmount">The amount of the fee.</param>
        /// <param name="closingCostSet">The closing cost set to add the fee to.</param>
        /// <param name="brokerId">The lender's identifier.</param>
        /// <param name="sspAssociator">An object capable of associating a fee to a locally bound settlement service provider list.</param>
        /// <param name="wasDeleted">Whether the fee was deleted or not.</param>
        /// <returns>The closing cost object that was modified.</returns>
        private static LoanClosingCostFee CreateTitleFee(E_LegacyGfeFieldT feeType, decimal feeAmount, LoanClosingCostSet closingCostSet, Guid brokerId, FeePaidToAssociator sspAssociator, out bool wasDeleted)
        {
            LoanClosingCostFee loanFee = (LoanClosingCostFee)closingCostSet.GetFirstFeeByLegacyType(feeType, brokerId, addFeeToSet: true);
            loanFee.FormulaT = E_ClosingCostFeeFormulaT.MinBaseOnly;
            loanFee.BaseAmount = feeAmount;
            sspAssociator.AssociatePaidToAndSettlementServiceProvider(loanFee);

            wasDeleted = false;
            if (loanFee.TotalAmount == 0)
            {
                wasDeleted = true;
                closingCostSet.Remove(loanFee);
            }

            return loanFee;
        }

        /// <summary>
        /// An operator for associating a fee with an agent and settlement service provider.  This object allows us to cleanly close the list of
        /// service providers and an agent into this object, without simply passing a set of objects around.
        /// </summary>
        private class FeePaidToAssociator
        {
            /// <summary>
            /// The agent record to associate with the fee.
            /// </summary>
            private readonly CAgentFields agent;

            /// <summary>
            /// The list of settlement service providers on the loan.
            /// </summary>
            private readonly AvailableSettlementServiceProviders settlementServiceProviders;

            /// <summary>
            /// Initializes a new instance of the <see cref="FeePaidToAssociator"/> class.
            /// </summary>
            /// <param name="agent">The agent record to associate with the fee.</param>
            /// <param name="settlementServiceProviders">The list of settlement service providers on the loan.</param>
            public FeePaidToAssociator(CAgentFields agent, AvailableSettlementServiceProviders settlementServiceProviders)
            {
                this.agent = agent;
                this.settlementServiceProviders = settlementServiceProviders;
            }

            /// <summary>
            /// Gets a value indicating whether this instance has updated the list of settlement service providers it holds a reference to.
            /// </summary>
            public bool IsSettlementServiceProviderListUpdated { get; private set; }

            /// <summary>
            /// Applies the data contained in this instance to the fee and the settlement service provider for that fee.
            /// </summary>
            /// <param name="fee">The fee which will receive a settlement service provider from this instance.</param>
            public void AssociatePaidToAndSettlementServiceProvider(LoanClosingCostFee fee)
            {
                if (this.agent == null)
                {
                    return;
                }

                fee.BeneficiaryAgentId = this.agent.RecordId;
                fee.Beneficiary = this.agent.AgentRoleT;
                fee.DisableBeneficiaryAutomation = false;

                // Seller-Responsible Closing Costs and non-section C fees are not associated with Settlement Service Providers currently
                if (fee is BorrowerClosingCostFee && fee.IntegratedDisclosureSectionT == E_IntegratedDisclosureSectionT.SectionC)
                {
                    this.settlementServiceProviders.AddFeeType(fee.ClosingCostFeeTypeId);
                    this.settlementServiceProviders.AddProviderForFeeType(fee.ClosingCostFeeTypeId, new SettlementServiceProvider(this.agent));
                    this.IsSettlementServiceProviderListUpdated = true;
                }
            }
        }
    }
}
