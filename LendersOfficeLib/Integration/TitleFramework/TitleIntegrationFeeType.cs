﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using DataAccess;

    /// <summary>
    /// Represents a fee type in the title integration, which contains a collection of fees of this type.
    /// </summary>
    /// <remarks>
    /// The TitleIntegration prefix here is to prevent generic naming that might lead to confusion.
    /// </remarks>
    public class TitleIntegrationFeeType
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TitleIntegrationFeeType"/> class.
        /// </summary>
        /// <param name="feeType">The type of the fee.</param>
        /// <param name="providerRole">The <see cref="ServiceProviderInfo.AgentRole"/> for the corresponding <see cref="ServiceProviderInfo"/> instance, or null if none exists.</param>
        /// <param name="fees">The fees of this fee type.</param>
        public TitleIntegrationFeeType(E_LegacyGfeFieldT feeType, E_AgentRoleT? providerRole, IReadOnlyList<TitleIntegrationFee> fees)
        {
            this.FeeType = feeType;
            this.ProviderRole = providerRole;
            this.Fees = fees;
        }

        /// <summary>
        /// Gets the type of the fee.
        /// </summary>
        /// <value>The type of the fee.</value>
        public E_LegacyGfeFieldT FeeType { get; }

        /// <summary>
        /// Gets the <see cref="ServiceProviderInfo.AgentRole"/> for the corresponding <see cref="ServiceProviderInfo"/> instance, or null if none exists.
        /// </summary>
        /// <value>The <see cref="ServiceProviderInfo.AgentRole"/> for the corresponding <see cref="ServiceProviderInfo"/> instance, or null if none exists.</value>
        public E_AgentRoleT? ProviderRole { get; }

        /// <summary>
        /// Gets the fees of this fee type.
        /// </summary>
        /// <value>The fees of this fee type.</value>
        public IReadOnlyList<TitleIntegrationFee> Fees { get; }

        /// <summary>
        /// Gets a fee representing the summation of all entries in <see cref="Fees"/>.  This will summation respect null values, however.
        /// </summary>
        /// <value>A fee representing the summation of all entries in <see cref="Fees"/>.  This will summation respect null values, however.</value>
        public TitleIntegrationFee TotalFee
        {
            get
            {
                decimal? borrTotal = null;
                decimal? sellerTotal = null;
                decimal? lenderTotal = null;
                bool? errorIndicator = null;
                foreach (var fee in this.Fees)
                {
                    borrTotal = fee.BorrowerResponsibleAmount.HasValue ? (borrTotal ?? 0M) + fee.BorrowerResponsibleAmount.Value : borrTotal;
                    sellerTotal = fee.SellerResponsibleAmount.HasValue ? (borrTotal ?? 0M) + fee.SellerResponsibleAmount.Value : sellerTotal;
                    lenderTotal = fee.LenderResponsibleAmount.HasValue ? (borrTotal ?? 0M) + fee.LenderResponsibleAmount.Value : lenderTotal;
                    errorIndicator = fee.ErrorIndicator.HasValue ? (errorIndicator ?? false) || fee.ErrorIndicator.Value : errorIndicator;
                }

                return new TitleIntegrationFee(null, borrTotal, sellerTotal, lenderTotal, errorIndicator);
            }
        }

        /// <summary>
        /// Serializes <paramref name="item"/> into an <seealso cref="XElement"/>.
        /// </summary>
        /// <param name="item">The item to serialize.</param>
        /// <param name="elementName">The name of the element.</param>
        /// <returns>The XML element representation.</returns>
        internal static XElement ToXml(TitleIntegrationFeeType item, string elementName)
        {
            return new XElement(
                elementName,
                new XAttribute("Type", item.FeeType.ToString("D")),
                item.ProviderRole.HasValue ? new XAttribute("ProviderRole", item.ProviderRole.Value.ToString("D")) : null,
                item.Fees.Select(fee => TitleIntegrationFee.ToXml(fee, "fee")));
        }

        /// <summary>
        /// Deserializes <paramref name="element"/> into this type.
        /// </summary>
        /// <param name="element">The serialized XML.</param>
        /// <returns>The deserialized instance.</returns>
        internal static TitleIntegrationFeeType FromXml(XElement element)
        {
            return new TitleIntegrationFeeType(
                (E_LegacyGfeFieldT)(int)element.Attribute("Type"),
                (E_AgentRoleT?)(int?)element.Attribute("ProviderRole"),
                element.Elements().Where(el => el.Name == "fee").Select(el => TitleIntegrationFee.FromXml(el)).ToList());
        }
    }
}
