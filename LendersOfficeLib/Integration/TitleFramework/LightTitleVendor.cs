﻿namespace LendersOffice.Integration.TitleFramework
{
    /// <summary>
    /// Light class for TitleVendors.
    /// </summary>
    public class LightTitleVendor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LightTitleVendor"/> class.
        /// </summary>
        public LightTitleVendor()
        {
        }

        /// <summary>
        /// Gets or sets the vendor id.
        /// </summary>
        /// <value>The vendor id.</value>
        public int VendorId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the company name.
        /// </summary>
        /// <value>The company name.</value>
        public string CompanyName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this vendor is enabled.
        /// </summary>
        /// <value>Whether this vendor is enabled.</value>
        public bool IsEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this vendor is a test vendor.
        /// </summary>
        /// <value>Whether this is a test vendor.</value>
        public bool IsTestVendor
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether quick quoting is enabled.
        /// </summary>
        /// <value>Whether quick quoting is enabled.</value>
        public bool IsQuickQuotingEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether detailed quoting is enabled.
        /// </summary>
        /// <value>Whether detailed quoting is enabled.</value>
        public bool IsDetailedQuotingEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether policy ordering is enabled.
        /// </summary>
        /// <value>Whether policy ordering is enabled.</value>
        public bool IsPolicyOrderingEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the quoting platform uses an account id.
        /// </summary>
        /// <value>Whether the quoting platform uses an account id.</value>
        public bool? QuotingPlatformUsesAccountId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the quoting platform requires an account id.
        /// </summary>
        /// <value>Whether the quoting platform requires an account id.</value>
        public bool? QuotingPlatformRequiresAccountId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the policy platform uses an account id.
        /// </summary>
        /// <value>Whether the policy platform uses an account id.</value>
        public bool? PolicyPlatformUsesAccountId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the policy platform requires an account id.
        /// </summary>
        /// <value>Whether the policy platform uses an account id.</value>
        public bool? PolicyPlatformRequiresAccountId
        {
            get;
            set;
        }
    }
}
