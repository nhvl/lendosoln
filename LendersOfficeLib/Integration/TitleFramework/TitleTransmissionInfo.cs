﻿namespace LendersOffice.Integration.TitleFramework
{
    using System.Data;
    using LendersOffice.Integration.FrameworkCommon;

    /// <summary>
    /// The transmission info class for the title framework.
    /// </summary>
    public class TitleTransmissionInfo : IntegrationTransmissionInfo
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="TitleTransmissionInfo"/> class from being created.
        /// </summary>
        private TitleTransmissionInfo()
        {
        }

        /// <summary>
        /// Gets the create SP.
        /// </summary>
        /// <value>The create SP.</value>
        protected override string CreateSp
        {
            get
            {
                return "TITLE_CreateTransmission";
            }
        }

        /// <summary>
        /// Gets the update SP.
        /// </summary>
        /// <value>The update SP.</value>
        protected override string UpdateSp
        {
            get
            {
                return "TITLE_UpdateTransmission";
            }
        }

        /// <summary>
        /// Creates a title transmission info from the reader.
        /// </summary>
        /// <param name="reader">The reader to load from.</param>
        /// <returns>The title transmission info.</returns>
        internal static TitleTransmissionInfo LoadTransmissionFromReader(IDataReader reader)
        {
            var info = new TitleTransmissionInfo();
            info.LoadFromReader(reader);
            return info;
        }

        /// <summary>
        /// Creates a transmission info from the parameters.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="errors">Any errors during creation.</param>
        /// <returns>The created transmission info if successful. Null otherwise.</returns>
        internal static TitleTransmissionInfo CreatePlatformTransmissionFromViewModel(IntegrationTransmissionInfoViewModel viewModel, out string errors)
        {
            var info = new TitleTransmissionInfo();
            if (!info.PopulateNewPlatformTransmissionFromViewModel(viewModel, out errors))
            {
                return null;
            }

            return info;
        }

        /// <summary>
        /// Creates a copy of the current instance for the vendor instance to override.
        /// </summary>
        /// <returns>A copy of the current instance.</returns>
        internal TitleTransmissionInfo CreateVendorCopy()
        {
            var copy = new TitleTransmissionInfo();
            this.CopyTo(copy, VOXFramework.TransmissionAssociationT.Vendor);
            return copy;
        }
    }
}
