﻿namespace LendersOffice.Integration.TitleFramework
{
    using System.Collections.Generic;

    /// <summary>
    /// The configuration saved to a title order.
    /// </summary>
    public class TitleOrderSubmittedConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TitleOrderSubmittedConfiguration"/> class.
        /// </summary>
        public TitleOrderSubmittedConfiguration()
        {
            this.ConfigurationItems = new List<TitleOrderSubmittedConfigurationItem>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TitleOrderSubmittedConfiguration"/> class.
        /// </summary>
        /// <param name="configurationItems">The configuration items.</param>
        public TitleOrderSubmittedConfiguration(List<TitleOrderSubmittedConfigurationItem> configurationItems)
        {
            this.ConfigurationItems = configurationItems;
        }

        /// <summary>
        /// Gets a list of configuration items saved to a title order.
        /// </summary>
        /// <value>A list of configuration items saved to a title order.</value>
        public List<TitleOrderSubmittedConfigurationItem> ConfigurationItems { get; }

        /// <summary>
        /// Adds a configuration item to the list.
        /// </summary>
        /// <param name="item">The item to add.</param>
        public void AddConfigurationItem(TitleOrderSubmittedConfigurationItem item)
        {
            this.ConfigurationItems.Add(item);
        }
    }
}
