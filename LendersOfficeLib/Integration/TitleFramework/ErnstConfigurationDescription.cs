﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;

    /// <summary>
    /// Represents a description for an entry in the configuration for Ernst.
    /// </summary>
    public partial struct ErnstConfigurationDescription
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstConfigurationDescription"/> struct.
        /// </summary>
        /// <param name="text">The text describing the configuration entry.</param>
        private ErnstConfigurationDescription(string text)
        {
            this.Text = text;
        }

        /// <summary>
        /// Gets the text describing the configuration entry.
        /// </summary>
        /// <value>The text describing the configuration entry.</value>
        public string Text { get; }

        /// <summary>
        /// Parses the input description text and validates it.
        /// </summary>
        /// <param name="text">The text describing the configuration entry.</param>
        /// <returns>The validated description, or null.</returns>
        public static ErnstConfigurationDescription? Create(string text)
        {
            if (!string.IsNullOrWhiteSpace(text))
            {
                return new ErnstConfigurationDescription(text);
            }

            return null;
        }

        /// <summary>
        /// Returns the text of the description.
        /// </summary>
        /// <returns>The text of the description.</returns>
        public override string ToString()
        {
            return this.Text;
        }
    }

    /// <summary>
    /// Contains the implementation of <see cref="IEquatable{ErnstConfigurationDescription}"/> using the <see cref="Text"/> property.
    /// </summary>
    public partial struct ErnstConfigurationDescription : IEquatable<ErnstConfigurationDescription>
    {
        /// <summary>
        /// The equality operator.
        /// </summary>
        /// <param name="lhs">The instance that is on the left side of the == operator.</param>
        /// <param name="rhs">The instance that is on the right side of the == operator.</param>
        /// <returns>True if the instances are equal, false otherwise.</returns>
        public static bool operator ==(ErnstConfigurationDescription lhs, ErnstConfigurationDescription rhs)
        {
            return lhs.Equals(rhs);
        }

        /// <summary>
        /// The inequality operator.
        /// </summary>
        /// <param name="lhs">The instance that is on the left side of the != operator.</param>
        /// <param name="rhs">The instance that is on the right side of the != operator.</param>
        /// <returns>True if the two instances are not equal, false otherwise.</returns>
        public static bool operator !=(ErnstConfigurationDescription lhs, ErnstConfigurationDescription rhs)
        {
            return !lhs.Equals(rhs);
        }

        /// <summary>
        /// Determines if two instances are equal.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(ErnstConfigurationDescription other)
        {
            return this.Text.Equals(other.Text);
        }

        /// <summary>
        /// Override the Equals method inherited from the object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if <paramref name="obj"/> is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            return obj is ErnstConfigurationDescription && this.Equals((ErnstConfigurationDescription)obj);
        }

        /// <summary>
        /// Override of the GetHashCode method inherited from the object class.
        /// </summary>
        /// <returns>An integer this is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return this.Text.GetHashCode();
        }
    }
}
