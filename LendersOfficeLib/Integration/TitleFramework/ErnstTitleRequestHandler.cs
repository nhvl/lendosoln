﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common;
    using Conversions.TitleFramework;
    using DataAccess;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Processes a title request and response.
    /// </summary>
    public class ErnstTitleRequestHandler : AbstractTitleRequestHandler
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstTitleRequestHandler"/> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        public ErnstTitleRequestHandler(AbstractTitleRequestData requestData)
            : base(requestData)
        {
            base.RequestProvider = new ErnstTitleRequestProvider(this.RequestData);
            base.Server = new ErnstServer(this.RequestData.Vendor.QuotingTransmission.TargetUrl);
        }

        /// <summary>
        /// Gets the request data.
        /// </summary>
        /// <value>The request data.</value>
        protected new ErnstQuoteRequestData RequestData => (ErnstQuoteRequestData)base.RequestData;

        /// <summary>
        /// Gets the request provider.
        /// </summary>
        /// <value>The request provider.</value>
        protected new ErnstTitleRequestProvider RequestProvider => (ErnstTitleRequestProvider)base.RequestProvider;

        /// <summary>
        /// Gets the server.
        /// </summary>
        /// <value>The server.</value>
        protected new ErnstServer Server => (ErnstServer)base.Server;

        /// <summary>
        /// Parses the response from Ernst.
        /// </summary>
        /// <param name="response">The string response.</param>
        /// <param name="errors">A list of errors encountered while parsing the response.</param>
        /// <returns>The parsed response.</returns>
        protected override ITitleQuoteResponseData ParseResponse(string response, out List<string> errors)
        {
            errors = new List<string>();
            Result<ErnstQuoteResponseData> responseDataParsingResult = ErnstQuoteResponseData.Create(response);
            if (responseDataParsingResult.HasError)
            {
                Tools.LogWarning(responseDataParsingResult.Error);
                errors.Add((responseDataParsingResult.Error as LqbException)?.Message ?? ErrorMessage.SystemError.ToString());
                return null;
            }
            else if (responseDataParsingResult.Value.VendorError != null)
            {
                errors.Add(responseDataParsingResult.Value.VendorError);
                return null;
            }
            else if (responseDataParsingResult.Value.QuotedFees.FeeTypes.Select(feeType => feeType.TotalFee).All(fee => (fee.BorrowerResponsibleAmount ?? 0) == 0 && (fee.SellerResponsibleAmount ?? 0) == 0))
            {
                errors.Add("No fees found.");
                return null;
            }

            return responseDataParsingResult.Value;
        }

        /// <summary>
        /// Retrieve Ernst configuration items that were sent in the request.
        /// </summary>
        /// <returns>An Ernst configuration.</returns>
        protected override TitleOrderSubmittedConfiguration GetExportedConfigItems()
        {
            var configItems = this.RequestData.ExportedConfigurationItems
                .Select(entry => new TitleOrderSubmittedConfigurationItem(entry.State.ToString(), entry.Description.Text, entry.LoanType, entry.PredefinedFunction?.Text, entry.DefaultValue.Value))
                .ToList();

            return new TitleOrderSubmittedConfiguration(configItems);
        }
    }
}
