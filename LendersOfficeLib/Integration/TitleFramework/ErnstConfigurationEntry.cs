﻿namespace LendersOffice.Integration.TitleFramework
{
    using DataAccess;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Encapsulates an entry in the custom configuration of values for Ernst.
    /// </summary>
    public class ErnstConfigurationEntry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstConfigurationEntry"/> class.
        /// </summary>
        /// <param name="pathIdentifier">The identifier for the path.</param>
        /// <param name="state">The state under which the path applies, or null if it applies to all states.</param>
        /// <param name="description">A description of the configuration entry.</param>
        /// <param name="defaultValue">The default value of the configuration entry.</param>
        /// <param name="loanType">The loan type of the configuration entry.</param>
        /// <param name="predefinedFunction">The predefined function of the configuration entry.</param>
        public ErnstConfigurationEntry(
            SimpleXPath pathIdentifier, 
            UnitedStatesState? state, 
            ErnstConfigurationDescription description, 
            ErnstConfigurationValue defaultValue, 
            E_sLT? loanType,
            ErnstConfigurationPredefinedFunction predefinedFunction)
        {
            this.PathIdentifier = pathIdentifier;
            this.State = state;
            this.Description = description;
            this.DefaultValue = defaultValue;
            this.PredefinedFunction = predefinedFunction;
            this.LoanType = loanType;
        }

        /// <summary>
        /// Gets the identifier for the path.
        /// </summary>
        /// <value>The identifier for the path.</value>
        public SimpleXPath PathIdentifier { get; }

        /// <summary>
        /// Gets the state under which the path applies, or null if it applies to all states.
        /// </summary>
        /// <value>The state under which the path applies, or null if it applies to all states.</value>
        public UnitedStatesState? State { get; }

        /// <summary>
        /// Gets a description of the configuration entry.
        /// </summary>
        /// <value>A description of the configuration entry.</value>
        public ErnstConfigurationDescription Description { get; }

        /// <summary>
        /// Gets the default value of the configuration entry.
        /// </summary>
        /// <value>The default value of the configuration entry.</value>
        public ErnstConfigurationValue DefaultValue { get; }

        /// <summary>
        /// Gets the predefined function of the configuration entry.
        /// </summary>
        /// <value>The predefined function of the configuration entry.</value>
        public ErnstConfigurationPredefinedFunction PredefinedFunction { get; }

        /// <summary>
        /// Gets the loan type of the configuration entry.
        /// </summary>
        /// <value>The loan type of the configuration entry.</value>
        public E_sLT? LoanType
        {
            get;
        }
    }
}
