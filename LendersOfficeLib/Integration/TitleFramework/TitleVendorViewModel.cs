﻿namespace LendersOffice.Integration.TitleFramework
{
    using System.Text;
    using FrameworkCommon;

    /// <summary>
    /// Title vendor view model.
    /// </summary>
    public class TitleVendorViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TitleVendorViewModel"/> class.
        /// </summary>
        public TitleVendorViewModel()
        {
            this.QuotingTransmission = new IntegrationTransmissionInfoViewModel();
            this.PolicyOrderingTransmission = new IntegrationTransmissionInfoViewModel();
        }

        /// <summary>
        /// Gets or sets the vendor id.
        /// </summary>
        /// <value>The vendor id.</value>
        public int? VendorId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the company name.
        /// </summary>
        /// <value>The company name.</value>
        public string CompanyName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this vendor is enabled.
        /// </summary>
        /// <value>Whether this vendor is enabled.</value>
        public bool IsEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this vendor is a test vendor.
        /// </summary>
        /// <value>Whether this is a test vendor.</value>
        public bool IsTestVendor
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether quick quoting is enabled.
        /// </summary>
        /// <value>Whether quick quoting enabled.</value>
        public bool IsQuickQuotingEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether detailed quoting is enabled.
        /// </summary>
        /// <value>Whether detailed quoting is enabled.</value>
        public bool IsDetailedQuotingEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether policy ordering is enabled.
        /// </summary>
        /// <value>Whether policy ordering is enabled.</value>
        public bool IsPolicyOrderingEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this vendor uses provider codes.
        /// </summary>
        /// <value>A value indicating whether this vendor uses provider codes.</value>
        public bool UsesProviderCodes
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the quoting platform id.
        /// </summary>
        /// <value>The quoting platform id.</value>
        public int? QuotingPlatformId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the quoting platform is being overriden.
        /// </summary>
        /// <value>Whether the quoting platform is being overriden.</value>
        public bool IsOverridingQuotingPlatform
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the quoting transmission being used.
        /// </summary>
        /// <value>The quoting transmission.</value>
        public IntegrationTransmissionInfoViewModel QuotingTransmission
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the policy ordering platform id.
        /// </summary>
        /// <value>The policy ordering platform id.</value>
        public int? PolicyOrderingPlatformId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the policy ordering platform is being overriden.
        /// </summary>
        /// <value>Whether the policy ordering platform is being overriden.</value>
        public bool IsOverridingPolicyOrderingPlatform
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the policy ordering transmission in use.
        /// </summary>
        /// <value>The policy ordering transmission in use.</value>
        public IntegrationTransmissionInfoViewModel PolicyOrderingTransmission
        {
            get;
            set;
        }

        /// <summary>
        /// Checks if the vendor view model is valid.
        /// </summary>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if valid. False otherwise.</returns>
        public bool Verify(out string errors)
        {
            string invalidString = "Invalid field {0}: {1}";
            StringBuilder errorBuilder = new StringBuilder();
            if (string.IsNullOrEmpty(this.CompanyName))
            {
                errorBuilder.AppendLine(string.Format(invalidString, "Company Name", this.CompanyName));
            }

            if ((this.IsDetailedQuotingEnabled || this.IsQuickQuotingEnabled) &&
                (!this.QuotingPlatformId.HasValue || this.QuotingPlatformId.Value < 1))
            {
                errorBuilder.AppendLine("Quoting services are enabled but no Quoting Platform is selected.");
            }

            if (this.IsPolicyOrderingEnabled && (!this.PolicyOrderingPlatformId.HasValue || this.PolicyOrderingPlatformId.Value < 1))
            {
                errorBuilder.AppendLine("Policy ordering services are enabled but not Policy Ordering Platform is selected.");
            }

            errors = errorBuilder.ToString();
            return errorBuilder.Length == 0;
        }
    }
}
