﻿namespace LendersOffice.Integration.TitleFramework
{
    /// <summary>
    /// The available operations for an Ernst Configuration Predefined Function.
    /// </summary>
    public enum ErnstConfigFunctionOperation
    {
        /// <summary>
        /// Adds if the condition is met.
        /// </summary>
        Add
    }
}
