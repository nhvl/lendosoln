﻿namespace LendersOffice.Integration.TitleFramework
{
    /// <summary>
    /// The payload formats specific to title.
    /// </summary>
    public enum TitlePayloadFormatT
    {
        /// <summary>
        /// Ernst XML format.
        /// </summary>
        ErnstXml = 0
    }
}
