﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.Security;

    /// <summary>
    /// Loads and holds references to the data necessary for creating a quote request to a title provider.
    /// </summary>
    public class ErnstQuoteRequestData : AbstractTitleRequestData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstQuoteRequestData"/> class.
        /// </summary>
        /// <param name="principal">The user placing the request.</param>
        /// <param name="lenderServiceId">The lender service id to be used for this request.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="feesToInclude">The fee category flags specifying which fees to include in the quote.</param>
        /// <param name="accountId">The account id to use to authenticate with the vendor.</param>
        /// <param name="username">The username to use to authenticate with the vendor.</param>
        /// <param name="password">The password to use to authenticate with the vendor.</param>
        /// <param name="systemGenerated">Whether the request is a non-interactive/system-generated one (true) or interactive(false).</param>
        public ErnstQuoteRequestData(AbstractUserPrincipal principal, int lenderServiceId, Guid loanId, ErnstQuoteFeeCategoryFlags feesToInclude, string accountId, string username, string password, bool systemGenerated = false)
            : base(principal, lenderServiceId, loanId, accountId, username, password, TitleProductType.Quote, systemGenerated)
        {
            this.ConfigurationDefaultValues = ErnstConfiguration.LoadSavedConfiguration(this.LenderService.ConfigurationFileDbKey);
            this.FeesToInclude = feesToInclude;
        }

        /// <summary>
        /// Gets the payload format to use in the request.
        /// </summary>
        /// <value>Always returns <see cref="TitlePayloadFormatT.ErnstXml"/>.</value>
        public override TitlePayloadFormatT PayloadFormat => TitlePayloadFormatT.ErnstXml;

        /// <summary>
        /// Gets the configuration default values to use for the request.
        /// </summary>
        /// <value>The default value configuration object.</value>
        public ErnstConfiguration ConfigurationDefaultValues { get; }

        /// <summary>
        /// Gets a flags enum specifying which fee categories to request.
        /// </summary>
        /// <value>Fees to include.</value>
        public ErnstQuoteFeeCategoryFlags FeesToInclude { get; }

        /// <summary>
        /// Gets a set of configuration items sent in the request provider.
        /// </summary>
        /// <value>A set of configuration items sent in the request provider.</value>
        public List<ErnstConfigurationEntry> ExportedConfigurationItems { get; } = new List<ErnstConfigurationEntry>();

        /// <summary>
        /// Creates a quote request data from the OrderTitleQuote page UI.
        /// </summary>
        /// <param name="vm">The view model to validate and convert.</param>
        /// <param name="user">The user making the request.</param>
        /// <param name="errors">Outputs any errors encountered.</param>
        /// <returns>
        /// If the view model fails to validate, null. 
        /// If it validates, a new quote request data from the view model.
        /// </returns>
        public static ErnstQuoteRequestData CreateFromViewModel(TitleQuoteOrderViewModel vm, AbstractUserPrincipal user, out string errors)
        {
            var requestData = new ErnstQuoteRequestData(user, vm.LenderServiceId, vm.LoanId, vm.FeesToInclude, vm.AccountId, vm.Username, vm.Password, systemGenerated: false);

            // We will prioritize pulling from the TitleProviderId provided in the viewmodel.
            if (vm.TitleProviderId != null && requestData.LenderService.UsesProviderCodes)
            {
                var titleProvider = new LendersOffice.Rolodex.RolodexDB(vm.TitleProviderId.Value, user.BrokerId, DataAccess.E_BypassReadPermissionCheckT.True);

                // The RolodexDB doesn't need to be Retrieved because TitleProviderCode is loaded separately through TitleProviderCodeDB.
                requestData.TitleProviderCode = titleProvider.TitleProviderCode;
                requestData.TitleProviderName = titleProvider.CompanyName;
                requestData.TitleProviderRolodexId = vm.TitleProviderId;
            }
            else if (vm.TitleProviderPreviousOrderId.HasValue && requestData.LenderService.UsesProviderCodes)
            {
                var previousOrder = TitleOrder.LoadOrderById(vm.TitleProviderPreviousOrderId.Value, vm.LoanId, user.BrokerId);

                requestData.TitleProviderCode = previousOrder.TitleProviderCode;
                requestData.TitleProviderName = previousOrder.TitleProviderName;
                requestData.TitleProviderRolodexId = previousOrder.TitleProviderRolodexId;
            }
            else if (!string.IsNullOrEmpty(vm.TitleProviderCode) && requestData.LenderService.UsesProviderCodes)
            {
                requestData.TitleProviderCode = vm.TitleProviderCode;
                requestData.TitleProviderName = null;
                requestData.TitleProviderRolodexId = null;
            }

            if (!requestData.ValidateRequestData(out errors))
            {
                return null;
            }

            return requestData;
        }

        /// <summary>
        /// Checks whether this RequestData is valid after creating from a view model.
        /// </summary>
        /// <param name="errors">Outputs messages for any validation errors.</param>
        /// <returns>Whether the view model is valid.</returns>
        public override bool ValidateRequestData(out string errors)
        {
            if (!base.ValidateRequestData(out errors))
            {
                return false;
            }

            if (this.FeesToInclude == ErnstQuoteFeeCategoryFlags.None)
            {
                errors = "No fees were selected to be included in the quote.";
                return false;
            }

            return true;
        }
    }
}
