﻿namespace LendersOffice.Integration.TitleFramework
{
    /// <summary>
    /// Indicates a Title product type.
    /// </summary>
    public enum TitleProductType
    {
        /// <summary>
        /// A quote request.
        /// </summary>
        Quote = 0,

        /// <summary>
        /// A policy order.
        /// </summary>
        Policy = 1
    }
}
