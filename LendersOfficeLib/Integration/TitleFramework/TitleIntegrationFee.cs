﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;
    using System.Xml.Linq;

    /// <summary>
    /// Represents a fee returned via the title integration.
    /// </summary>
    public class TitleIntegrationFee
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TitleIntegrationFee"/> class.
        /// </summary>
        /// <param name="description">The description of the fee.</param>
        /// <param name="borrowerResponsibleAmount">The borrower's component of the fee.</param>
        /// <param name="sellerResponsibleAmount">The seller's component of the fee.</param>
        /// <param name="lenderResponsibleAmount">The lender's component of the fee.</param>
        /// <param name="errorIndicator">A value indicating whether the fee has an error flag associated with it.</param>
        public TitleIntegrationFee(string description, decimal? borrowerResponsibleAmount, decimal? sellerResponsibleAmount, decimal? lenderResponsibleAmount, bool? errorIndicator)
        {
            const string TitleFeePrefix = "Title - "; // This prefix will be added in the UI and doc exports based on the IsTitle bit
            if (description != null
                && description.StartsWith(TitleFeePrefix)
                && (description.Length - TitleFeePrefix.Length) > 0)
            {
                description = description.Substring(TitleFeePrefix.Length);
            }

            this.Description = description;
            this.BorrowerResponsibleAmount = borrowerResponsibleAmount;
            this.SellerResponsibleAmount = sellerResponsibleAmount;
            this.LenderResponsibleAmount = lenderResponsibleAmount;
            this.ErrorIndicator = errorIndicator;
        }

        /// <summary>
        /// Gets the description of the fee.
        /// </summary>
        /// <value>The description of the fee.</value>
        public string Description { get; }

        /// <summary>
        /// Gets the borrower's component of the fee.
        /// </summary>
        /// <value>The borrower's component of the fee.</value>
        public decimal? BorrowerResponsibleAmount { get; }

        /// <summary>
        /// Gets the seller's component of the fee.
        /// </summary>
        /// <value>The seller's component of the fee.</value>
        public decimal? SellerResponsibleAmount { get; }

        /// <summary>
        /// Gets the lender's component of the fee.
        /// </summary>
        /// <value>The lender's component of the fee.</value>
        public decimal? LenderResponsibleAmount { get; }

        /// <summary>
        /// Gets a value indicating whether the fee has an error flag associated with it.
        /// </summary>
        /// <value>A value indicating whether the fee has an error flag associated with it.</value>
        public bool? ErrorIndicator { get; }

        /// <summary>
        /// Serializes <paramref name="item"/> into an <seealso cref="XElement"/>.
        /// </summary>
        /// <param name="item">The item to serialize.</param>
        /// <param name="elementName">The name of the element.</param>
        /// <returns>The XML element representation.</returns>
        internal static XElement ToXml(TitleIntegrationFee item, string elementName)
        {
            return new XElement(
                elementName,
                item.Description != null ? new XAttribute("Desc", item.Description) : null,
                item.BorrowerResponsibleAmount.HasValue ? new XAttribute("BorrAmt", item.BorrowerResponsibleAmount) : null,
                item.SellerResponsibleAmount.HasValue ? new XAttribute("SellerAmt", item.SellerResponsibleAmount) : null,
                item.LenderResponsibleAmount.HasValue ? new XAttribute("LenderAmt", item.LenderResponsibleAmount) : null,
                item.ErrorIndicator.HasValue ? new XAttribute("Error", item.ErrorIndicator) : null);
        }

        /// <summary>
        /// Deserializes <paramref name="element"/> into this type.
        /// </summary>
        /// <param name="element">The serialized XML.</param>
        /// <returns>The deserialized instance.</returns>
        internal static TitleIntegrationFee FromXml(XElement element)
        {
            return new TitleIntegrationFee(
                (string)element.Attribute("Desc"),
                (decimal?)element.Attribute("BorrAmt"),
                (decimal?)element.Attribute("SellerAmt"),
                (decimal?)element.Attribute("LenderAmt"),
                (bool?)element.Attribute("Error"));
        }
    }
}
