﻿namespace LendersOffice.Integration.TitleFramework
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using FrameworkCommon;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using VOXFramework;

    /// <summary>
    /// Platforms for Title framework.
    /// </summary>
    public class TitlePlatform : IntegrationPlatform<TitleTransmissionInfo>
    {
        /// <summary>
        /// The SP for loading a platform.
        /// </summary>
        private const string LoadPlatformByIdSp = "TITLE_LoadPlatformById";

        /// <summary>
        /// The SP for loading all Title platforms.
        /// </summary>
        private const string LoadAllPlatformsSp = "TITLE_LoadAllPlatforms";

        /// <summary>
        /// The SP for deleting a platform.
        /// </summary>
        private const string DeletePlatformById = "TITLE_DeletePlatformById";

        /// <summary>
        /// Prevents a default instance of the <see cref="TitlePlatform"/> class from being created.
        /// </summary>
        private TitlePlatform()
        {
        }

        /// <summary>
        /// Gets or sets the transmission model for the title platform.
        /// </summary>
        /// <value>The transmission model for the title platform.</value>
        public TitleTransmissionModelT TransmissionModelT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the payload model for the title platform.
        /// </summary>
        /// <value>The payload format for the title platform.</value>
        public TitlePayloadFormatT PayloadFormatT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the SP for creating a title platform.
        /// </summary>
        /// <value>The SP for creating a title platform.</value>
        protected override string CreatePlatformSP
        {
            get
            {
                return "TITLE_CreatePlatform";
            }
        }

        /// <summary>
        /// Gets the SP for updating a title platform.
        /// </summary>
        /// <value>The SP for updating a title platform.</value>
        protected override string UpdatePlatformSP
        {
            get
            {
                return "TITLE_UpdatePlatform";
            }
        }

        /// <summary>
        /// Loads a title platform using an ID.
        /// </summary>
        /// <param name="platformId">The platform id.</param>
        /// <returns>The title platform created.</returns>
        public static TitlePlatform LoadPlatform(int platformId)
        {
            StoredProcedureName? procedureName = StoredProcedureName.Create(LoadPlatformByIdSp);
            if (!procedureName.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {LoadPlatformByIdSp}."));
            }

            return LoadPlatform(platformId, procedureName.Value, () => new TitlePlatform()) as TitlePlatform;
        }

        /// <summary>
        /// Loads all title platforms.
        /// </summary>
        /// <returns>Enumerable of all title platforms.</returns>
        public static IEnumerable<TitlePlatform> LoadPlatforms()
        {
            StoredProcedureName? procedureName = StoredProcedureName.Create(LoadAllPlatformsSp);
            if (!procedureName.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {LoadAllPlatformsSp}."));
            }

            return LoadPlatforms(procedureName.Value, () => new TitlePlatform()).Cast<TitlePlatform>();
        }

        /// <summary>
        /// Deletes a title platform.
        /// </summary>
        /// <param name="platformId">The platform id.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if deleted, false otherwise.</returns>
        public static bool DeletePlatform(int platformId, out string errors)
        {
            StoredProcedureName? procedureName = StoredProcedureName.Create(DeletePlatformById);
            if (!procedureName.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {DeletePlatformById}."));
            }

            return DeletePlatform(platformId, procedureName.Value, out errors);
        }

        /// <summary>
        /// Creates a platform from the viewmodel.
        /// </summary>
        /// <param name="viewModel">The view model of the platform.</param>
        /// <param name="errors">Any errors found during creation.</param>
        /// <returns>The created platform if successful. Null otherwise.</returns>
        public static TitlePlatform CreateFromViewModel(TitlePlatformViewModel viewModel, out string errors)
        {
            bool isPlatformNew = !viewModel.PlatformId.HasValue || viewModel.PlatformId.Value < 1;

            if (!viewModel.Verify(out errors))
            {
                return null;
            }

            TitlePlatform loadedPlatform = null;
            if (!isPlatformNew)
            {
                loadedPlatform = LoadPlatform(viewModel.PlatformId.Value);
                if (!loadedPlatform.TransmissionInfo.PopulateFromViewModel(viewModel.TransmissionInfo, out errors))
                {
                    return null;
                }
            }
            else
            {
                TitleTransmissionInfo createdTransmission = TitleTransmissionInfo.CreatePlatformTransmissionFromViewModel(viewModel.TransmissionInfo, out errors);
                if (createdTransmission == null)
                {
                    return null;
                }

                loadedPlatform = new TitlePlatform();
                loadedPlatform.PlatformId = -1;
                loadedPlatform.LinkedTransmissionId = -1;
                loadedPlatform.TransmissionInfo = createdTransmission;
            }

            loadedPlatform.PlatformName = viewModel.PlatformName;
            loadedPlatform.TransmissionModelT = viewModel.TransmissionModelT;
            loadedPlatform.PayloadFormatT = viewModel.PayloadFormatT;
            loadedPlatform.UsesAccountId = viewModel.UsesAccountId;
            loadedPlatform.RequiresAccountId = viewModel.RequiresAccountId;
            loadedPlatform.IsNew = isPlatformNew;

            return loadedPlatform;
        }

        /// <summary>
        /// Creates a viewmodel out of this title platform.
        /// </summary>
        /// <returns>The created view model.</returns>
        public TitlePlatformViewModel ToViewModel()
        {
            TitlePlatformViewModel viewModel = new TitlePlatformViewModel()
            {
                PlatformId = this.PlatformId,
                PlatformName = this.PlatformName,
                TransmissionModelT = this.TransmissionModelT,
                PayloadFormatT = this.PayloadFormatT,
                UsesAccountId = this.UsesAccountId,
                RequiresAccountId = this.RequiresAccountId,
                TransmissionInfo = this.TransmissionInfo.ToViewModel()
            };

            return viewModel;
        }

        /// <summary>
        /// Gets any additional parameters needed to Save/Update a platform.
        /// </summary>
        /// <returns>Additional parameters to save/update a platform.</returns>
        protected override IEnumerable<SqlParameter> GetAdditionalPlatformSaveParameters()
        {
            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@TransmissionModelT", this.TransmissionModelT),
                new SqlParameter("@PayloadFormatT", this.PayloadFormatT),
            };

            parameters.AddRange(base.GetAdditionalPlatformSaveParameters());
            return parameters;
        }

        /// <summary>
        /// Loads and sets data from a reader.
        /// </summary>
        /// <param name="reader">The reader to load from.</param>
        protected override void LoadFromReader(IDataReader reader)
        {
            base.LoadFromReader(reader);
            TitleTransmissionInfo transmissionInfo = TitleTransmissionInfo.LoadTransmissionFromReader(reader);
            this.TransmissionModelT = (TitleTransmissionModelT)reader["TransmissionModelT"];
            this.PayloadFormatT = (TitlePayloadFormatT)reader["PayloadFormatT"];
            this.TransmissionInfo = transmissionInfo;
        }
    }
}
