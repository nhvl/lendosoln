﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The predefined functions in the Ernst Title Configuration file.
    /// </summary>
    public partial class ErnstConfigurationPredefinedFunction
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstConfigurationPredefinedFunction"/> class.
        /// </summary>
        /// <param name="text">The full text of the function.</param>
        /// <param name="operation">The operation to perform.</param>
        /// <param name="condition">The condition to check.</param>
        private ErnstConfigurationPredefinedFunction(string text, ErnstConfigFunctionOperation operation, ErnstConfigFunctionCondition condition)
        {
            this.Text = text;
            this.Operation = operation;
            this.Condition = condition;
        }

        /// <summary>
        /// Gets the full text of the predefined function.
        /// </summary>
        /// <value>The full text of the predefined function.</value>
        public string Text
        {
            get;
        }

        /// <summary>
        /// Gets the operation of the predefined function.
        /// </summary>
        /// <value>The operation of the predefined function.</value>
        public ErnstConfigFunctionOperation Operation
        {
            get;
        }

        /// <summary>
        /// Gets the condition of the predefined function.
        /// </summary>
        /// <value>The condition of the predefined function.</value>
        public ErnstConfigFunctionCondition Condition
        {
            get;
        }

        /// <summary>
        /// Parses the text into a predefined function.
        /// </summary>
        /// <param name="text">The text describing the configuration entry.</param>
        /// <returns>The predefined function.</returns>
        public static ErnstConfigurationPredefinedFunction Create(string text)
        {
            if (string.IsNullOrEmpty(text))
            {
                return null;
            }

            var parts = text.Split('_');
            if (parts.Length != 2)
            {
                return null;
            }

            var operationString = parts[0];
            var conditionString = parts[1];

            ErnstConfigFunctionOperation operation;
            if (!Enum.TryParse(operationString, ignoreCase: true, result: out operation) || !Enum.IsDefined(typeof(ErnstConfigFunctionOperation), operation))
            {
                return null;
            }

            ErnstConfigFunctionCondition condition;
            if (!Enum.TryParse(conditionString, ignoreCase: true, result: out condition) || !Enum.IsDefined(typeof(ErnstConfigFunctionCondition), condition))
            {
                return null;
            }

            return new ErnstConfigurationPredefinedFunction(text, operation, condition);
        }

        /// <summary>
        /// Returns the text of the description.
        /// </summary>
        /// <returns>The text of the description.</returns>
        public override string ToString()
        {
            return this.Text;
        }
    }

    /// <summary>
    /// Contains the implementation of <see cref="IEquatable{ErnstConfigurationPredefinedFunction}"/>.
    /// </summary>
    public partial class ErnstConfigurationPredefinedFunction : IEquatable<ErnstConfigurationPredefinedFunction>
    {
        /// <summary>
        /// Determines if two instances are equal.
        /// </summary>
        /// <param name="other">Instance that is checked for equality with this instance.</param>
        /// <returns>True if other is equal to this instance, false otherwise.</returns>
        public bool Equals(ErnstConfigurationPredefinedFunction other)
        {
            if (other == null)
            {
                return false;
            }

            return this.Condition == other.Condition && this.Operation == other.Operation;
        }

        /// <summary>
        /// Override the Equals method inherited from the object class.
        /// </summary>
        /// <param name="obj">Instance that is checked for equality with this instance.</param>
        /// <returns>True if <paramref name="obj"/> is equal to this instance, false otherwise.</returns>
        public override bool Equals(object obj)
        {
            return obj is ErnstConfigurationPredefinedFunction && this.Equals((ErnstConfigurationPredefinedFunction)obj);
        }

        /// <summary>
        /// Override of the GetHashCode method inherited from the object class.
        /// </summary>
        /// <returns>An integer this is the hash code for this instance.</returns>
        public override int GetHashCode()
        {
            int hash = 17;
            hash *= 29 + this.Condition.GetHashCode();
            hash *= 29 + this.Operation.GetHashCode();

            return hash;
        }
    }
}
