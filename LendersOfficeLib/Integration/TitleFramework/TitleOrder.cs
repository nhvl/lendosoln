﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Represents an order in the Title Framework.
    /// </summary>
    public class TitleOrder
    {
        /// <summary>
        /// The name of the LoadByLoan stored procedure.
        /// </summary>
        private static StoredProcedureName loadByLoanIdSp = StoredProcedureName.Create("TITLE_FRAMEWORK_ORDER_LoadByLoanId").Value;

        /// <summary>
        /// The name of the CreateOrder stored procedure.
        /// </summary>
        private static StoredProcedureName createOrderSp = StoredProcedureName.Create("TITLE_FRAMEWORK_ORDER_CreateOrder").Value;

        /// <summary>
        /// The name of the UpdateOrder stored procedure.
        /// </summary>
        private static StoredProcedureName updateOrderSp = StoredProcedureName.Create("TITLE_FRAMEWORK_ORDER_UpdateOrder").Value;

        /// <summary>
        /// The name of the LoadByOrderId stored procedure.
        /// </summary>
        private static StoredProcedureName loadByOrderIdSp = StoredProcedureName.Create("TITLE_FRAMEWORK_ORDER_LoadByOrderId").Value;

        /// <summary>
        /// The SP to load an order by public id.
        /// </summary>
        private static StoredProcedureName loadByPublicIdSp = StoredProcedureName.Create("TITLE_FRAMEWORK_ORDER_LoadByPublicId").Value;

        /// <summary>
        /// The results FileDb key.
        /// </summary>
        private Guid resultsFileDbKey;

        /// <summary>
        /// The results file.
        /// </summary>
        private Lazy<string> resultsFile;

        /// <summary>
        /// The file db key for the quote data containing fees and other data to apply to the loan.
        /// </summary>
        /// <value>The FileDb key for the quote data containing fees and other data to apply to the loan.</value>
        private Guid quotedFeesFileDbKey;

        /// <summary>
        /// The quote data containing fees and other data to apply to the loan.
        /// </summary>
        private Lazy<TitleIntegrationQuote> quotedFees;

        /// <summary>
        /// The FileDb key for the configuration file.
        /// </summary>
        private Guid configFileDbKey;

        /// <summary>
        /// The configuration file.
        /// </summary>
        private Lazy<TitleOrderSubmittedConfiguration> configFile;

        /// <summary>
        /// Initializes a new instance of the <see cref="TitleOrder"/> class.
        /// </summary>
        /// <param name="reader">A data reader.</param>
        private TitleOrder(IDataReader reader)
        {
            this.OrderId = (int)reader[nameof(this.OrderId)];
            this.PublicId = (Guid)reader[nameof(this.PublicId)];
            this.BrokerId = (Guid)reader[nameof(this.BrokerId)];
            this.LoanId = (Guid)reader[nameof(this.LoanId)];
            this.TransactionId = (Guid)reader[nameof(this.TransactionId)];
            this.OrderNumber = (string)reader[nameof(this.OrderNumber)];
            this.Status = (TitleOrderStatus)reader[nameof(this.Status)];
            this.OrderedBy = (string)reader[nameof(this.OrderedBy)];
            this.OrderedDate = (DateTime)reader[nameof(this.OrderedDate)];
            this.IsTestOrder = (bool)reader[nameof(this.IsTestOrder)];
            this.ResultsFileDbKey = (Guid)reader[nameof(this.ResultsFileDbKey)];
            this.QuotedFeesFileDbKey = (Guid)reader["FeesFileDbKey"];
            this.ConfigFileDbKey = (Guid)reader[nameof(this.ConfigFileDbKey)];
            this.LenderServiceId = (int)reader[nameof(this.LenderServiceId)];
            this.LenderServiceName = (string)reader[nameof(this.LenderServiceName)];
            this.ProductType = (TitleProductType)reader[nameof(this.ProductType)];

            var titleProviderCode = reader[nameof(this.TitleProviderCode)];
            this.TitleProviderCode = titleProviderCode == DBNull.Value ? null : (string)titleProviderCode;

            var titleProviderName = reader[nameof(this.TitleProviderName)];
            this.TitleProviderName = titleProviderName == DBNull.Value ? null : (string)titleProviderName;

            var titleProviderRolodexId = reader[nameof(this.TitleProviderRolodexId)];
            this.TitleProviderRolodexId = titleProviderRolodexId == DBNull.Value ? (Guid?)null : (Guid)titleProviderRolodexId;

            object appliedDate = reader["AppliedDate"];
            this.AppliedDate = appliedDate == DBNull.Value ? default(DateTime?) : (DateTime)appliedDate;
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="TitleOrder"/> class from being created.
        /// </summary>
        private TitleOrder()
        {
        }

        /// <summary>
        /// Gets a value indicating whether this order is new, or retrieved from the DB.
        /// </summary>
        /// <value>A boolean indicating whether this order is new.</value>
        public bool IsNew { get; private set; } = false;

        /// <summary>
        /// Gets the order ID.
        /// </summary>
        /// <value>The order ID.</value>
        public int OrderId { get; private set; }

        /// <summary>
        /// Gets an ID that can be shared publicly (ie webservices).
        /// </summary>
        /// <value>A public id.</value>
        public Guid PublicId { get; private set; }

        /// <summary>
        /// Gets the transaction ID.
        /// </summary>
        /// <value>The transaction ID.</value>
        public Guid TransactionId { get; private set; }

        /// <summary>
        /// Gets the broker ID.
        /// </summary>
        /// <value>The broker ID.</value>
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets the loan ID.
        /// </summary>
        /// <value>The loan ID.</value>
        public Guid LoanId { get; private set; }

        /// <summary>
        /// Gets the order number.
        /// </summary>
        /// <value>The order number.</value>
        public string OrderNumber { get; private set; }

        /// <summary>
        /// Gets the name of the person who placed the order.
        /// </summary>
        /// <value>The name of the person who placed the order.</value>
        public string OrderedBy { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the order is a test order.
        /// </summary>
        /// <value>A boolean indicating whether the order is a test order.</value>
        public bool IsTestOrder { get; private set; }

        /// <summary>
        /// Gets or sets the status of the order.
        /// </summary>
        /// <value>The status of the order.</value>
        public TitleOrderStatus Status { get; set; }

        /// <summary>
        /// Gets the product type.
        /// </summary>
        /// <value>The product type.</value>
        public TitleProductType ProductType { get; private set; }

        /// <summary>
        /// Gets the date the order was placed.
        /// </summary>
        /// <value>The date the order was placed.</value>
        public DateTime OrderedDate { get; private set; }

        /// <summary>
        /// Gets the date the order was applied to the loan.
        /// </summary>
        /// <value>The date the order was applied to the loan.</value>
        public DateTime? AppliedDate { get; private set; }

        /// <summary>
        /// Gets the FileDb key to the order's full response.
        /// </summary>
        /// <value>The FileDb key for the order's full response.</value>
        public Guid ResultsFileDbKey
        {
            get
            {
                return this.resultsFileDbKey;
            }

            private set
            {
                this.resultsFileDbKey = value;
                this.resultsFile = new Lazy<string>(() =>
                {
                    if (value == Guid.Empty)
                    {
                        return null;
                    }
                    else
                    {
                        return FileDBTools.ReadDataText(E_FileDB.Normal, this.GetResultsFullFileDbKey(value));
                    }
                });
            }
        }

        /// <summary>
        /// Gets the results file.
        /// </summary>
        /// <value>The results file.</value>
        public string ResultsFile => this.resultsFile.Value;

        /// <summary>
        /// Gets the FileDb key to the quote data for this order.
        /// </summary>
        /// <value>The FileDb key to the quote data for this order.</value>
        public Guid QuotedFeesFileDbKey
        {
            get
            {
                return this.quotedFeesFileDbKey;
            }

            private set
            {
                this.quotedFeesFileDbKey = value;
                this.quotedFees = new Lazy<TitleIntegrationQuote>(() =>
                {
                    if (value == Guid.Empty)
                    {
                        return null;
                    }
                    else
                    {
                        return TitleIntegrationQuote.FromXml(FileDBTools.ReadDataText(E_FileDB.Normal, this.GetQuoteFullFileDbKey(value)));
                    }
                });
            }
        }

        /// <summary>
        /// Gets the quote data containing fees and other data to apply to the loan.
        /// </summary>
        /// <value>The quote data containing fees and other data to apply to the loan.</value>
        public TitleIntegrationQuote QuotedFees => this.quotedFees.Value;

        /// <summary>
        /// Gets the FileDb key to the questions and answers provided by the user.
        /// </summary>
        /// <value>The FileDb key to the questions and answers provided by the user.</value>
        public Guid ConfigFileDbKey
        {
            get
            {
                return this.configFileDbKey;
            }

            private set
            {
                this.configFileDbKey = value;
                this.configFile = new Lazy<TitleOrderSubmittedConfiguration>(() =>
                {
                    if (value == Guid.Empty)
                    {
                        return null;
                    }
                    else
                    {
                        return FileDBTools.ReadJsonNetObject<TitleOrderSubmittedConfiguration>(E_FileDB.Normal, this.GetConfigFullFileDbKey(value));
                    }
                });
            }
        }

        /// <summary>
        /// Gets the configuration file.
        /// </summary>
        /// <value>The configuration file.</value>
        public TitleOrderSubmittedConfiguration ConfigFile => this.configFile.Value;

        /// <summary>
        /// Gets the id of the lender service used for the order.
        /// </summary>
        /// <value>The id of the lender service used for the order.</value>
        public int LenderServiceId { get; private set; }

        /// <summary>
        /// Gets the name of the lender service used for the order.
        /// </summary>
        /// <value>The name of the lender service used for the order.</value>
        public string LenderServiceName { get; private set; }

        /// <summary>
        /// Gets the title provider code used for the order.
        /// </summary>
        /// <value>The title provider code used for the order.</value>
        public string TitleProviderCode { get; private set; }

        /// <summary>
        /// Gets the title provider rolodex id.
        /// </summary>
        /// <value>The title provider rolodex id.</value>
        public Guid? TitleProviderRolodexId { get; private set; }

        /// <summary>
        /// Gets the title provider name used for the order.
        /// </summary>
        /// <value>The title provider name used for the order.</value>
        public string TitleProviderName { get; private set; }

        /// <summary>
        /// Retrieves a value indicating if the loan has orders or lender services.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="brokerId">The broker ID.</param>
        /// <returns>A value indicating if the loan would make any sense to show the title order page.</returns>
        public static bool LoanHasOrdersOrLenderServices(Guid loanId, Guid brokerId)
        {
            var parameters = new[]
            {
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@BrokerId", brokerId)
            };
            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "TITLE_FRAMEWORK_CountLenderServicesAndLoanOrders", parameters))
            {
                if (reader.Read())
                {
                    return (int)reader["OrderCount"] > 0 || (int)reader["LenderServiceCount"] > 0;
                }
            }

            return false;
        }

        /// <summary>
        /// Loads all orders associated with the given loan ID.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="brokerId">The broker ID.</param>
        /// <returns>A list of associated loans.</returns>
        public static List<TitleOrder> LoadOrdersByLoanId(Guid loanId, Guid brokerId)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", loanId)
            };

            var orders = new List<TitleOrder>();
            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);

            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            using (var reader = driver.ExecuteReader(connection, null, loadByLoanIdSp, parameters))
            {
                while (reader.Read())
                {
                    orders.Add(new TitleOrder(reader));
                }
            }

            return orders;
        }

        /// <summary>
        /// Loads an order by its id.
        /// </summary>
        /// <param name="orderId">The order id.</param>
        /// <param name="loanId">The loan that the order belongs to.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <returns>The title order if found. Null otherwise.</returns>
        public static TitleOrder LoadOrderById(int orderId, Guid loanId, Guid brokerId)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@OrderId", orderId),
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", loanId)
            };

            var orders = new List<TitleOrder>();
            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);

            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            using (var reader = driver.ExecuteReader(connection, null, loadByOrderIdSp, parameters))
            {
                if (reader.Read())
                {
                    return new TitleOrder(reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Loads an order by public id.
        /// </summary>
        /// <param name="publicId">The public id.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <returns>The title order. Null if not found.</returns>
        public static TitleOrder LoadOrderByPublicId(Guid publicId, Guid loanId, Guid brokerId)
        {
            var parameters = new SqlParameter[]
            {
                new SqlParameter("@PublicId", publicId),
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", loanId)
            };

            var orders = new List<TitleOrder>();
            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);

            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            using (var reader = driver.ExecuteReader(connection, null, loadByPublicIdSp, parameters))
            {
                if (reader.Read())
                {
                    return new TitleOrder(reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Creates a new title order.
        /// </summary>
        /// <param name="requestData">The request data used to send the order.</param>
        /// <param name="responseData">The response to the request for a quote in this order.</param>
        /// <param name="configuration">The configuration items submitted in the request.</param>
        /// <returns>The new title order.</returns>
        public static TitleOrder CreateNewOrder(AbstractTitleRequestData requestData, ITitleQuoteResponseData responseData, TitleOrderSubmittedConfiguration configuration)
        {
            TitleOrder newTitleOrder = new TitleOrder();
            newTitleOrder.OrderId = -1;
            newTitleOrder.PublicId = Guid.NewGuid();
            newTitleOrder.BrokerId = requestData.BrokerId;
            newTitleOrder.LoanId = requestData.LoanId;
            newTitleOrder.Status = TitleOrderStatus.QuoteNotApplied;
            newTitleOrder.OrderedBy = requestData.Principal.DisplayName;
            newTitleOrder.OrderedDate = requestData.TransactionDate;
            newTitleOrder.IsTestOrder = requestData.Vendor.IsTestVendor;
            newTitleOrder.LenderServiceId = requestData.LenderServiceId;
            newTitleOrder.LenderServiceName = requestData.LenderService.DisplayName;
            newTitleOrder.TitleProviderCode = requestData.TitleProviderCode;
            newTitleOrder.TitleProviderRolodexId = requestData.TitleProviderRolodexId;
            newTitleOrder.TitleProviderName = requestData.TitleProviderName;
            newTitleOrder.ProductType = requestData.ProductType;
            newTitleOrder.IsNew = true;
            newTitleOrder.TransactionId = requestData.TransactionId;

            newTitleOrder.OrderNumber = responseData.OrderNumber;
            newTitleOrder.resultsFile = new Lazy<string>(() => responseData.RawResponse);
            newTitleOrder.quotedFees = new Lazy<TitleIntegrationQuote>(() => responseData.QuotedFees);
            newTitleOrder.configFile = new Lazy<TitleOrderSubmittedConfiguration>(() => configuration);

            return newTitleOrder;
        }

        /// <summary>
        /// Turns this title order into a view model.
        /// </summary>
        /// <returns>The view model.</returns>
        public TitleOrderViewModel ToViewModel()
        {
            TitleOrderViewModel viewModel = new TitleOrderViewModel();
            viewModel.OrderId = this.OrderId;
            viewModel.QuoteProvider = this.LenderServiceName;
            viewModel.TitleProviders = this.QuotedFees.Providers.ToList();
            viewModel.OrderNumber = this.OrderNumber;
            viewModel.OrderedBy = this.OrderedBy;
            viewModel.OrderedDate = this.OrderedDate.ToString("G");

            return viewModel;
        }

        /// <summary>
        /// Creates or updates the order in the database.
        /// </summary>
        public void Save()
        {
            if (this.IsNew)
            {
                this.SaveFilesToFileDb();
            }

            SqlParameter orderIdParam;
            var parameters = this.GetParameters(out orderIdParam);

            var sp = this.IsNew ? createOrderSp : updateOrderSp;
            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);

            using (var connection = DbConnectionInfo.GetConnection(this.BrokerId))
            {
                driver.ExecuteNonQuery(connection, null, sp, parameters);
            }

            if (this.IsNew)
            {
                this.OrderId = (int)orderIdParam.Value;
                this.IsNew = false;
            }
        }

        /// <summary>
        /// Applies the quote from this entry to <paramref name="loan"/>.
        /// </summary>
        /// <param name="loan">The loan to update, which must only be in the <see cref="E_DataState.InitLoad"/> state.</param>
        /// <returns>The fee data that was modified. Null if not successful.</returns>
        public List<ModifiedFeeData> ApplyQuoteToLoanForCalculation(CPageData loan)
        {
            if (loan.DataState == E_DataState.InitLoad)
            {
                return this.QuotedFees?.ApplyQuoteToLoan(loan);
            }

            return null;
        }

        /// <summary>
        /// Applies the quote from this entry to <paramref name="loan"/>.  This will require saving both the loan and this <see cref="TitleOrder"/> instance.
        /// </summary>
        /// <param name="loan">The loan to update, already in  <see cref="E_DataState.InitSave"/> mode.  Note that since workflow is checked inside this method, the object passed will not have workflow constraints.</param>
        /// <param name="principal">The principal to use for workflow and audit information.</param>
        /// <param name="auditHtml">The audit HTML to save for an audit event, or null if no audit event should be made.</param>
        /// <param name="checkResult">The workflow check result, previously run and cached in this object, or null to run it live.</param>
        /// <returns>The modified fee data.</returns>
        public List<ModifiedFeeData> ApplyAndSaveQuoteToLoan(CFullAccessPageData loan, Security.AbstractUserPrincipal principal, string auditHtml, Tuple<bool, string> checkResult = null)
        {
            if (loan.DataState != E_DataState.InitSave)
            {
                throw CBaseException.GenericException("Expected loan ready to be saved.");
            }

            checkResult = checkResult ?? this.CheckCanApplyAndSaveQuoteToLoan(loan, principal);
            if (!checkResult.Item1)
            {
                throw CBaseException.GenericException("Permission check failed.");
            }

            var modifiedFees = this.QuotedFees.ApplyQuoteToLoan(loan);
            if (auditHtml != null)
            {
                loan.RecordAuditOnSave(new Audit.TitleQuoteImportAuditItem(principal, auditHtml));
            }

            this.Status = TitleOrderStatus.QuoteApplied;
            this.AppliedDate = DateTime.Now;
            loan.Save();
            this.Save();
            return modifiedFees;
        }

        /// <summary>
        /// Checks whether the quote in this instance can be applied to the specified loan.
        /// </summary>
        /// <param name="loan">The loan to apply the quote to.</param>
        /// <param name="principal">The user to apply the quote.</param>
        /// <returns>A boolean indicating whether the quote can be applied to the loan and the error message associated with the lack of permission.</returns>
        public Tuple<bool, string> CheckCanApplyAndSaveQuoteToLoan(CPageData loan, Security.AbstractUserPrincipal principal)
        {
            if (this.QuotedFees == null)
            {
                return Tuple.Create(false, "No quoted fees found.");
            }

            return Tools.IsWorkflowOperationAuthorized(principal, loan.sLId, ConfigSystem.Operations.WorkflowOperations.ApplyTitleQuote);
        }

        /// <summary>
        /// Saves the XML results to FileDB.
        /// </summary>
        private void SaveFilesToFileDb()
        {
            if (!string.IsNullOrEmpty(this.ResultsFile) && this.ResultsFileDbKey == Guid.Empty)
            {
                Guid newResultsKey = Guid.NewGuid();
                FileDBTools.WriteData(E_FileDB.Normal, this.GetResultsFullFileDbKey(newResultsKey), this.ResultsFile);

                // Don't want to set the property since the lazy instance is already holding what we want.
                this.resultsFileDbKey = newResultsKey;
            }

            if (this.QuotedFees != null && this.QuotedFeesFileDbKey == Guid.Empty)
            {
                Guid newQuoteKey = Guid.NewGuid();
                FileDBTools.WriteData(E_FileDB.Normal, this.GetQuoteFullFileDbKey(newQuoteKey), TitleIntegrationQuote.ToXml(this.QuotedFees));

                // Don't want to set the property since the lazy instance is already holding what we want.
                this.quotedFeesFileDbKey = newQuoteKey;
            }

            if (this.ConfigFile != null && this.ConfigFileDbKey == Guid.Empty)
            {
                Guid newConfigKey = Guid.NewGuid();
                FileDBTools.WriteJsonNetObject<TitleOrderSubmittedConfiguration>(E_FileDB.Normal, this.GetConfigFullFileDbKey(newConfigKey), this.ConfigFile);

                // Don't want to set the property since the lazy instance is already holding what we want.
                this.configFileDbKey = newConfigKey;
            }
        }

        /// <summary>
        /// Generates a list of SQL parameters representing this order.
        /// </summary>
        /// <param name="orderIdParam">Persists a references to the OrderId parameter.</param>
        /// <returns>A list of SQL parameters.</returns>
        private List<SqlParameter> GetParameters(out SqlParameter orderIdParam)
        {
            var parameters = new List<SqlParameter>();

            if (this.IsNew)
            {
                orderIdParam = new SqlParameter
                {
                    ParameterName = "@OrderId",
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Output
                };

                parameters.Add(new SqlParameter("@PublicId", this.PublicId));
                parameters.Add(new SqlParameter("@TransactionId", this.TransactionId));
                parameters.Add(new SqlParameter("@OrderNumber", this.OrderNumber));
                parameters.Add(new SqlParameter("@OrderedBy", this.OrderedBy));
                parameters.Add(new SqlParameter("@OrderedDate", this.OrderedDate));
                parameters.Add(new SqlParameter("@IsTestOrder", this.IsTestOrder));
                parameters.Add(new SqlParameter("@ResultsFileDbKey", this.ResultsFileDbKey));
                parameters.Add(new SqlParameter("@FeesFileDbKey", this.QuotedFeesFileDbKey));
                parameters.Add(new SqlParameter("@ConfigFileDbKey", this.ConfigFileDbKey));
                parameters.Add(new SqlParameter("@LenderServiceId", this.LenderServiceId));
                parameters.Add(new SqlParameter("@LenderServiceName", this.LenderServiceName));
                parameters.Add(new SqlParameter("@TitleProviderCode", this.TitleProviderCode));
                parameters.Add(new SqlParameter("@TitleProviderName", this.TitleProviderName));
                parameters.Add(new SqlParameter("@TitleProviderRolodexId", this.TitleProviderRolodexId));
                parameters.Add(new SqlParameter("@ProductType", this.ProductType));
            }
            else
            {
                orderIdParam = new SqlParameter("@OrderId", this.OrderId);
            }

            parameters.Add(orderIdParam);
            parameters.Add(new SqlParameter("@Status", this.Status));
            parameters.Add(new SqlParameter("@AppliedDate", this.AppliedDate));
            parameters.Add(new SqlParameter("@BrokerId", this.BrokerId));
            parameters.Add(new SqlParameter("@LoanId", this.LoanId));

            return parameters;
        }

        /// <summary>
        /// Gets the full FileDb key for the results.
        /// </summary>
        /// <param name="key">The guid key.</param>
        /// <returns>The full FileDb key for the results.</returns>
        private string GetResultsFullFileDbKey(Guid key)
        {
            return $"{key.ToString("N")}_titleOrderResults";
        }

        /// <summary>
        /// Gets the full FileDb key for the fees.
        /// </summary>
        /// <param name="key">The guid key.</param>
        /// <returns>The full FileDb key for the fees.</returns>
        private string GetQuoteFullFileDbKey(Guid key)
        {
            return $"{key.ToString("N")}_titleOrderFees";
        }

        /// <summary>
        /// Gets the full FileDb key for the config.
        /// </summary>
        /// <param name="key">The guid key.</param>
        /// <returns>The full FileDb key for the configuration.</returns>
        private string GetConfigFullFileDbKey(Guid key)
        {
            return $"{key.ToString("N")}_titleOrderConfig";
        }
    }
}
