﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;
    using DataAccess;

    /// <summary>
    /// Lists the fee categories that can be included in Ernst's quote.
    /// </summary>
    [Flags]
    public enum ErnstQuoteFeeCategoryFlags
    {
        /// <summary>
        /// No fee categories selected.
        /// </summary>
        [OrderedDescription("")]
        None = 0,

        /// <summary>
        /// Title fees.
        /// </summary>
        [OrderedDescription("Title")]
        Title = 1 << 0,

        /// <summary>
        /// Inspection fees.
        /// </summary>
        [OrderedDescription("Inspection")]
        Inspection = 1 << 1,

        /// <summary>
        /// Government fees and taxes.
        /// </summary>
        [OrderedDescription("Government Fees and Taxes")]
        Government = 1 << 2,

        /// <summary>
        /// Property tax.
        /// </summary>
        [OrderedDescription("Property Tax")]
        PropertyTax = 1 << 3
    }
}
