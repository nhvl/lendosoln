﻿namespace LendersOffice.Integration.TitleFramework
{
    /// <summary>
    /// Server interface for title.
    /// </summary>
    /// <remarks>
    /// We may want to pull this out to be an IIntegrationServer instead.
    /// </remarks>
    public interface ITitleServer
    {
        /// <summary>
        /// Places a request given the XML content.
        /// </summary>
        /// <param name="orderXml">The xml content to send.</param>
        /// <returns>The response string.</returns>
        string PlaceRequest(string orderXml);

        /// <summary>
        /// Checks the health of the connection.
        /// </summary>
        /// <returns>If the connection was made, true. Otherwise, false.</returns>
        bool HealthCheck();
    }
}
