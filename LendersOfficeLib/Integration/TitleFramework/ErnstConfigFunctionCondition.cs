﻿namespace LendersOffice.Integration.TitleFramework
{
    /// <summary>
    /// The available conditions for an Ernst Configuration predefined function.
    /// </summary>
    public enum ErnstConfigFunctionCondition
    {
        /// <summary>
        /// If sFinMethT == ARM.
        /// </summary>
        IfARM,

        /// <summary>
        /// If sFinMethT == Fixed.
        /// </summary>
        IfFixed,

        /// <summary>
        /// If sProdIsTexas50a6Loan.
        /// </summary>
        If50a6,

        /// <summary>
        /// If sGseSpT==Condominium or sGseSpT==DetachedCondominium or sGseSpT==HighRiseCondominium or sGseSpT==ManufacturedHomeCondominium.
        /// </summary>
        IfCondo,

        /// <summary>
        /// If !sProdIsTexas50a6Loan AND (sGseSpT==Condominium or sGseSpT==DetachedCondominium or sGseSpT==HighRiseCondominium or sGseSpT==ManufacturedHomeCondominium).
        /// </summary>
        IfNon50a6Condo,

        /// <summary>
        /// If sProdIsTexas50a6Loan AND (sGseSpT==Condominium or sGseSpT==DetachedCondominium or sGseSpT==HighRiseCondominium or sGseSpT==ManufacturedHomeCondominium).
        /// </summary>
        If50a6Condo,

        /// <summary>
        /// If sSpIsInPud.
        /// </summary>
        IfPUD,

        /// <summary>
        /// If !sProdIsTexas50a6Loan AND sSpIsInPud.
        /// </summary>
        IfNon50a6PUD,

        /// <summary>
        /// If sProdIsTexas50a6Loan AND sSpIsInPud.
        /// </summary>
        If50a6PUD,

        /// <summary>
        /// If sDue less than sTerm.
        /// </summary>
        IfBalloon,

        /// <summary>
        /// If sOccT==SecondaryResidence.
        /// </summary>
        IfSecondHome,

        /// <summary>
        /// If <see cref="DataAccess.CPageData.sIsRefinancing"/> is false.
        /// </summary>
        IfPurchase,

        /// <summary>
        /// If <see cref="DataAccess.CPageData.sIsRefinancing"/> is true.
        /// </summary>
        IfRefinance,

        /// <summary>
        /// If <see cref="DataAccess.CPageData.sOccT"/> is <see cref="DataAccess.E_sOccT.PrimaryResidence"/>.
        /// </summary>
        IfPrimaryResidence,

        /// <summary>
        /// If <see cref="DataAccess.CPageData.sOccT"/> is <see cref="DataAccess.E_sOccT.Investment"/>.
        /// </summary>
        IfInvestmentProperty,

        /// <summary>
        /// If <see cref="DataAccess.CPageData.sLienPosT"/> is <see cref="DataAccess.E_sLienPosT.Second"/>.
        /// </summary>
        IfSecondLien,

        /// <summary>
        /// If <see cref="DataAccess.CPageData.sUnitsNum"/> is greater than 1.
        /// </summary>
        IfMultiUnit,

        /// <summary>
        /// If <see cref="DataAccess.CPageData.sHardPlusSoftPrepmtPeriodMonths"/> is greater than 0.
        /// </summary>
        IfPrepaymentPenalty,
    }
}
