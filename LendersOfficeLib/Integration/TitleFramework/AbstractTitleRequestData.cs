﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;
    using System.Text;
    using LendersOffice.Security;

    /// <summary>
    /// An abstract implementation of the data needed to place a request to a title framework vendor.
    /// </summary>
    public abstract class AbstractTitleRequestData
    {
        /// <summary>
        /// Lazy-loaded lender service object.
        /// </summary>
        private Lazy<TitleLenderService> lenderService;

        /// <summary>
        /// Lazy-loaded vendor object.
        /// </summary>
        private Lazy<TitleVendor> vendor;

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractTitleRequestData"/> class.
        /// </summary>
        /// <param name="principal">The user placing the request.</param>
        /// <param name="lenderServiceId">The lender service id to be used for this request.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="accountId">The account id to use to authenticate with the vendor.</param>
        /// <param name="username">The username to use to authenticate with the vendor.</param>
        /// <param name="password">The password to use to authenticate with the vendor.</param>
        /// <param name="productType">The request type.</param>
        /// <param name="isSystemGenerated">Whether the request is system-generated.</param>
        protected AbstractTitleRequestData(AbstractUserPrincipal principal, int lenderServiceId, Guid loanId, string accountId, string username, string password, TitleProductType productType, bool isSystemGenerated)
        {
            this.Principal = principal;
            this.BrokerId = principal.BrokerId;
            this.LenderServiceId = lenderServiceId;
            this.lenderService = new Lazy<TitleLenderService>(() => TitleLenderService.GetLenderServiceById(this.BrokerId, this.LenderServiceId));
            this.vendor = new Lazy<TitleVendor>(() => TitleVendor.LoadTitleVendor(this.LenderService.VendorId));
            this.LoanId = loanId;
            this.AccountId = accountId;
            this.Username = username;
            this.Password = password;
            this.ProductType = productType;
            this.IsSystemGeneratedRequest = isSystemGenerated;
            this.TransactionId = Guid.NewGuid();
        }

        /// <summary>
        /// Gets the user placing the request.
        /// </summary>
        /// <value>Requesting user.</value>
        public AbstractUserPrincipal Principal { get; private set; }

        /// <summary>
        /// Gets the broker ID.
        /// </summary>
        /// <value>Broker ID.</value>
        public Guid BrokerId { get; }

        /// <summary>
        /// Gets the transaction id.
        /// </summary>
        /// <value>The transaction id.</value>
        public Guid TransactionId { get; }

        /// <summary>
        /// Gets the loan ID to pull request data from.
        /// </summary>
        /// <value>Loan Identifier.</value>
        public Guid LoanId { get; private set; }

        /// <summary>
        /// Gets the title vendor being used.
        /// </summary>
        /// <value>Title vendor.</value>
        public TitleVendor Vendor => this.vendor.Value;

        /// <summary>
        /// Gets or sets the product type (Quote, Policy, etc).
        /// </summary>
        /// <value>Product type.</value>
        public TitleProductType ProductType { get; set; }

        /// <summary>
        /// Gets or sets the service provider code used to identify the title company (rolodex entry) to the title vendor.
        /// </summary>
        /// <value>Provider code.</value>
        public string TitleProviderCode { get; set; }

        /// <summary>
        /// Gets or sets the name of the service provider.
        /// </summary>
        /// <value>The name of the service provider.</value>
        public string TitleProviderName { get; set; }

        /// <summary>
        /// Gets or sets the title provider rolodex id.
        /// </summary>
        /// <value>The title provider rolodex id.</value>
        public Guid? TitleProviderRolodexId { get; set; }

        /// <summary>
        /// Gets the ID of the lender service to use.
        /// </summary>
        /// <value>Lender service ID.</value>
        public int LenderServiceId { get; }

        /// <summary>
        /// Gets the lender service object.
        /// </summary>
        /// <value>Lender service.</value>
        public TitleLenderService LenderService => this.lenderService.Value;

        /// <summary>
        /// Gets or sets the AccountId for the request.
        /// </summary>
        /// <value>Request AccountId.</value>
        public string AccountId { get; set; }

        /// <summary>
        /// Gets or sets the username for the request.
        /// </summary>
        /// <value>Request username.</value>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the password for the request.
        /// </summary>
        /// <value>Request password.</value>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the order is generated by the system.
        /// If a request is initiated by direct action, this will be false and we should enforce access control.
        /// If it is part of another action like registering a loan program, different logic may apply.
        /// </summary>
        /// <value>A boolean indicating whether the order is generated by the system.</value>
        public bool IsSystemGeneratedRequest { get; set; } = false;

        /// <summary>
        /// Gets the transaction date to send in the XML.
        /// </summary>
        /// <value>Transaction date.</value>
        public DateTime TransactionDate { get; } = DateTime.Now;

        /// <summary>
        /// Gets the format for the request payload, based on vendor and platform.
        /// </summary>
        /// <value>Payload format.</value>
        public abstract TitlePayloadFormatT PayloadFormat
        {
            get;
        }

        /// <summary>
        /// Validates the request data.
        /// </summary>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if validated, false otherwise.</returns>
        public virtual bool ValidateRequestData(out string errors)
        {
            if (this.LenderServiceId < 1)
            {
                errors = "Invalid service provider chosen.";
                return false;
            }

            if (this.LenderService == null)
            {
                errors = "Service provider not found.";
                return false;
            }

            StringBuilder errorLog = new StringBuilder();
            if (string.IsNullOrEmpty(this.Username) || string.IsNullOrEmpty(this.Password))
            {
                errorLog.AppendLine("Authentication information required.");
            }

            errors = errorLog.ToString();
            return errorLog.Length == 0;
        }
    }
}
