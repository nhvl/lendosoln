﻿namespace LendersOffice.Integration.TitleFramework
{
    /// <summary>
    /// The transmission models specific to title.
    /// </summary>
    public enum TitleTransmissionModelT
    {
        /// <summary>
        /// The Ernst synchronous model.
        /// </summary>
        ErnstSynchronousModel = 0
    }
}
