﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;
    using DataAccess;

    /// <summary>
    /// The party responsible for the fee in question.
    /// </summary>
    /// <remarks>
    /// This type is only for use with <see cref="ModifiedFeeData"/>.
    /// </remarks>
    public enum ModifiedFeeResponsibleParty
    {
        /// <summary>
        /// The fee is borrower-responsible.
        /// </summary>
        Borrower = 0,

        /// <summary>
        /// The fee is seller-responsible.
        /// </summary>
        Seller = 1
    }

    /// <summary>
    /// Class to hold summary data about modified fees.
    /// </summary>
    public class ModifiedFeeData
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ModifiedFeeData"/> class.
        /// </summary>
        /// <param name="hudline">The hudline.</param>
        /// <param name="description">The description.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="uniqueId">The unique id.</param>
        /// <param name="closingCostFeeTypeId">The closing cost fee type id.</param>
        /// <param name="sourceFeeTypeId">The source fee type id.</param>
        /// <param name="responsibleParty">The responsible party for the fee.</param>
        /// <param name="wasDeleted">Whether the fee was deleted or not.</param>
        public ModifiedFeeData(int hudline, string description, decimal amount, Guid? uniqueId, Guid? closingCostFeeTypeId, Guid? sourceFeeTypeId, ModifiedFeeResponsibleParty responsibleParty, bool wasDeleted)
        {
            this.Hudline = hudline;
            this.Description = description;
            this.Amount = amount;
            this.UniqueId = uniqueId;
            this.ClosingCostFeeTypeId = closingCostFeeTypeId;
            this.SourceFeeTypeId = sourceFeeTypeId;
            this.ResponsibleParty = responsibleParty;
            this.WasDeleted = wasDeleted;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ModifiedFeeData"/> class.
        /// </summary>
        /// <param name="fee">The fee that was changed.</param>
        /// <param name="wasDeleted">Whether it was deleted or not.</param>
        public ModifiedFeeData(LoanClosingCostFee fee, bool wasDeleted)
            : this(fee.HudLine, fee.Description, fee.TotalAmount, fee.UniqueId, fee.ClosingCostFeeTypeId, fee.SourceFeeTypeId, fee is SellerClosingCostFee ? ModifiedFeeResponsibleParty.Seller : ModifiedFeeResponsibleParty.Borrower, wasDeleted)
        {
        }

        /// <summary>
        /// Gets the hudline of the fee.
        /// </summary>
        /// <value>The hudline of the fee.</value>
        public int Hudline
        {
            get;
        }

        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <value>The description of the fee.</value>
        public string Description
        {
            get;
        }

        /// <summary>
        /// Gets the amount.
        /// </summary>
        /// <value>The total amount of the fee.</value>
        public decimal Amount
        {
            get;
        }

        /// <summary>
        /// Gets the unique id.
        /// </summary>
        /// <value>The unique id of the fee.</value>
        public Guid? UniqueId
        {
            get;
        }

        /// <summary>
        /// Gets the closing cost fee type id.
        /// </summary>
        /// <value>The closing cost fee type id of the fee.</value>
        public Guid? ClosingCostFeeTypeId
        {
            get;
        }

        /// <summary>
        /// Gets the source fee type id.
        /// </summary>
        /// <value>The source fee type id of the fee.</value>
        public Guid? SourceFeeTypeId
        {
            get;
        }

        /// <summary>
        /// Gets the responsible party for the fee.
        /// </summary>
        /// <value>The responsible party for the fee.</value>
        public ModifiedFeeResponsibleParty ResponsibleParty { get; }

        /// <summary>
        /// Gets a value indicating whether the fee was deleted or not.
        /// </summary>
        /// <value>Whether the fee was deleted or not.</value>
        public bool WasDeleted
        {
            get;
        }
    }
}
