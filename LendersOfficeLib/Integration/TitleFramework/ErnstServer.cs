﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;
    using System.Web;
    using System.Xml.Linq;
    using LendersOffice.Drivers.HttpRequest;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.HttpRequest;

    /// <summary>
    /// Provides an interface with Ernst's Title API.
    /// </summary>
    public class ErnstServer : ITitleServer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstServer"/> class.
        /// </summary>
        /// <param name="targetUrl">The target url.</param>
        public ErnstServer(LqbAbsoluteUri targetUrl)
        {
            this.TargetUrl = targetUrl;
        }

        /// <summary>
        /// Gets the target web service URL for the server.
        /// </summary>
        /// <value>Target URL.</value>
        public LqbAbsoluteUri TargetUrl { get; }

        /// <summary>
        /// Places an order to Ernst using the given XML post content.
        /// </summary>
        /// <param name="orderXml">The xml content to send to Ernst.</param>
        /// <returns>The response string from Ernst.</returns>
        public string PlaceRequest(string orderXml)
        {
            HttpRequestOptions options = new HttpRequestOptions
            {
                Method = HttpMethod.Post,
                PostData = new StringContent("xmlRequest=" + HttpUtility.UrlEncode(orderXml)),
                MimeType = MimeType.Application.UrlEncoded,
                Timeout = TimeoutInSeconds.Sixty,
                KeepAlive = false
            };

            try
            {
                if (!WebRequestHelper.ExecuteCommunication(this.TargetUrl, options))
                {
                    return null;
                }
            }
            catch (LqbGrammar.Exceptions.LqbException exc) when (exc.InnerException is System.Net.WebException)
            {
                var webException = (System.Net.WebException)exc.InnerException;
                string responseBody = ReadFullResponseStream(webException.Response);
                DataAccess.Tools.LogWarning(
                    this.TargetUrl.ToString() + " resulted in " + webException.Status
                    + (responseBody != null ? "\r\n\r\nRaw Response:\r\n" + responseBody : null));
                throw;
            }

            return XElement.Parse(options.ResponseBody).Value; // Ernst uses a SOAP webservice that wraps the response in <string></string>
        }

        /// <summary>
        /// Checks the health of the connection to Ernst.
        /// </summary>
        /// <returns>If the connection was made, true. Otherwise, false.</returns>
        public bool HealthCheck()
        {
            string returnedTime = null;
            try
            {
                returnedTime = this.PlaceRequest("health");
            }
            catch (SystemException)
            {
                return false;
            }

            DateTime time;
            return DateTime.TryParse(returnedTime, out time);
        }

        /// <summary>
        /// Reads the full response into a string.
        /// </summary>
        /// <param name="response">The response to read.</param>
        /// <returns>The response body as a string, or null if it could not be read.</returns>
        private static string ReadFullResponseStream(System.Net.WebResponse response)
        {
            using (var responseStream = response?.GetResponseStream())
            {
                if (responseStream == null)
                {
                    return null;
                }

                using (var reader = new System.IO.StreamReader(responseStream))
                {
                    return reader.ReadToEnd();
                }
            }
        }
    }
}
