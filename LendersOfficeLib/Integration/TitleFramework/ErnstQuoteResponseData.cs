﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;
    using Common;
    using DataAccess;
    using Ernst.Response;
    using LendersOffice.Conversions.TitleFramework;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Transforms the text response from Ernst into a data object making the needed information accessible.
    /// </summary>
    public class ErnstQuoteResponseData : ITitleQuoteResponseData
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="ErnstQuoteResponseData"/> class from being created.
        /// </summary>
        private ErnstQuoteResponseData()
        {
        }

        /// <summary>
        /// Gets the error message returned from Ernst, if there is one.
        /// </summary>
        /// <value>The vendor error.</value>
        public string VendorError { get; private set; } = null;

        /// <summary>
        /// Gets the Ernst-generated "TransactionID", which we store in the TitleOrder table as OrderNumber.
        /// </summary>
        /// <value>Order number.</value>
        public string OrderNumber => this.Response?.TransactionID;

        /// <summary>
        /// Gets and parses the LQB-generated transaction ID echoed back in Ernst's response.
        /// </summary>
        /// <value>Parsed transaction ID, or null if parsing failed.</value>
        public Guid? TransactionId => this.Response?.ErnstResponse?.Request?.ClientTransactionID?.Value.ToNullable<Guid>(Guid.TryParse);

        /// <summary>
        /// Gets the parsed fee quote object.
        /// </summary>
        /// <value>Quoted fees.</value>
        public TitleIntegrationQuote QuotedFees { get; private set; }

        /// <summary>
        /// Gets the raw response string returned from Ernst.
        /// </summary>
        /// <value>Raw response string.</value>
        public string RawResponse { get; private set; }

        /// <summary>
        /// Gets or sets the parsed response data class.
        /// </summary>
        /// <value>Response data from XML.</value>
        private Response Response { get; set; }

        /// <summary>
        /// Creates a new instance of the <see cref="ErnstQuoteResponseData"/> class by parsing the string response from Ernst.
        /// </summary>
        /// <param name="ernstResponseString">The string response from Ernst's web service.</param>
        /// <returns>A result encapsulating either a new instance of <see cref="ErnstQuoteResponseData"/> or an error encountered while creating one.</returns>
        public static Result<ErnstQuoteResponseData> Create(string ernstResponseString)
        {
            ErnstQuoteResponseData newProvider = new ErnstQuoteResponseData();
            newProvider.RawResponse = ernstResponseString;
            LogResponse(ernstResponseString);

            try
            {
                newProvider.Response = SerializationHelper.XmlDeserialize(ernstResponseString, typeof(Response)) as Response;
            }
            catch (InvalidOperationException invalidEx)
            {
                return Result<ErnstQuoteResponseData>.Failure(invalidEx);
            }

            if (newProvider.Response == null)
            {
                return Result<ErnstQuoteResponseData>.Failure(new LqbGrammar.Exceptions.DeveloperException(ErrorMessage.SystemError));
            }

            newProvider.VendorError = newProvider.Response?.Error?.Description
                ?? newProvider.Response?.ErnstResponse?.Error?.Description
                ?? newProvider.Response?.TitleResponse?.Error?.Description;

            newProvider.QuotedFees = ErnstQuoteResponseImporter.GetQuoteData(newProvider.Response);

            return Result<ErnstQuoteResponseData>.Success(newProvider);
        }
        
        /// <summary>
        /// Log the response from Ernst, filtering out any sensitive information.
        /// </summary>
        /// <remarks>
        /// For the Ernst response, there's no authentication or personally identifying information returned,
        /// and it's been about 30k chars in testing (our max is 50k) so this method just logs the full string.
        /// </remarks>
        /// <param name="response">The response from Ernst.</param>
        private static void LogResponse(string response)
        {
            TitleUtilities.LogPayload(response, isResponse: true);
        }
    }
}
