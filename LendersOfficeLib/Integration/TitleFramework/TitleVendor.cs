﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using DataAccess;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Class for Title framework vendors.
    /// </summary>
    public class TitleVendor
    {
        /// <summary>
        /// The SP for pulling title vendors by id.
        /// </summary>
        private const string GetVendorByIdSp = "TITLE_LoadVendorById";

        /// <summary>
        /// The SP for deleting title vendors.
        /// </summary>
        private const string DeleteVendorByIdSp = "TITLE_DeleteVendorById";

        /// <summary>
        /// The SP for creating a new title vendor.
        /// </summary>
        private const string CreateVendorSp = "TITLE_CreateVendor";

        /// <summary>
        /// The SP for updating a new title vendor.
        /// </summary>
        private const string UpdateVendorSp = "TITLE_UpdateVendor";

        /// <summary>
        /// The SP for loading the title vendors. Loads the light version.
        /// </summary>
        private const string LoadTitleVendorsLightSP = "TITLE_LoadVendorsLight";

        /// <summary>
        /// The SP for checking how many lender services are using a particular vendor.
        /// </summary>
        private const string GetLenderServiceCountByVendorIdSp = "TITLE_GetLenderServiceCountByVendorId";

        /// <summary>
        /// Whether the vendor is overriding the quoting platform.
        /// </summary>
        private bool isOverridingQuotingPlatform;

        /// <summary>
        /// The quoting platform id.
        /// </summary>
        private int? quotingPlatformId;

        /// <summary>
        /// The quoting platform.
        /// </summary>
        private Lazy<TitlePlatform> quotingPlatform;

        /// <summary>
        /// The quoting platform transmission info.
        /// </summary>
        private TitleTransmissionInfo quotingTransmission;

        /// <summary>
        /// Whether this is overriding the policy ordering platform.
        /// </summary>
        private bool isOverridingPolicyOrderingPlatform;

        /// <summary>
        /// The policy ordering platform id.
        /// </summary>
        private int? policyOrderingPlatformId;

        /// <summary>
        /// The policy ordering platform.
        /// </summary>
        private Lazy<TitlePlatform> policyOrderingPlatform;

        /// <summary>
        /// The policy ordering platform transmission info.
        /// </summary>
        private TitleTransmissionInfo policyOrderingTransmission;

        /// <summary>
        /// Prevents a default instance of the <see cref="TitleVendor"/> class from being created.
        /// </summary>
        private TitleVendor()
        {
        }

        /// <summary>
        /// Gets a value indicating whether this vendor is new.
        /// </summary>
        /// <value>Indicating whether this vendor is new.</value>
        public bool IsNew
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the id of the vender level service.
        /// </summary>
        /// <value>The id of the vendor levels ervice.</value>
        public int VendorId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the vendor name.
        /// </summary>
        /// <value>The vendor name.</value>
        public string CompanyName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the vendor is enabled.
        /// </summary>
        /// <value>If the vendor is enabled.</value>
        public bool IsEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the vendor is a test vendor.
        /// </summary>
        /// <value>Whether the vendor is a test vendor.</value>
        public bool IsTestVendor
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether quick quoting is enabled.
        /// </summary>
        /// <value>Whether quick quoting is enabled.</value>
        public bool IsQuickQuotingEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether detailed quoting is enabled.
        /// </summary>
        /// <value>Whether detailed quoting is enabled.</value>
        public bool IsDetailedQuotingEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether policy ordering is enabled.
        /// </summary>
        /// <value>Whether policy ordering is enabled.</value>
        public bool IsPolicyOrderingEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether product codes are used for this vendor.
        /// </summary>
        /// <value>Whether product codes are used for this vendor.</value>
        public bool UsesProviderCodes
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the quoting platform id. Can be null if quoting is not enabled.
        /// </summary>
        /// <value>The quoting platform id.</value>
        public int? QuotingPlatformId
        {
            get
            {
                if (!this.IsDetailedQuotingEnabled && !this.IsQuickQuotingEnabled)
                {
                    return null;
                }

                return this.quotingPlatformId;
            }

            set
            {
                this.quotingPlatformId = value.HasValue && value.Value < 0 ? null : value;
                if (value.HasValue && value.Value > 0 &&
                    (this.IsDetailedQuotingEnabled || this.IsQuickQuotingEnabled))
                {
                    this.quotingPlatform = new Lazy<TitlePlatform>(() => TitlePlatform.LoadPlatform(value.Value));
                }
                else
                {
                    this.quotingPlatform = new Lazy<TitlePlatform>(() => null);
                }
            }
        }

        /// <summary>
        /// Gets the quoting platform.
        /// </summary>
        /// <value>The quoting platform.</value>
        public TitlePlatform QuotingPlatform
        {
            get
            {
                return this.quotingPlatform.Value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the quoting platform is being overriden.
        /// </summary>
        /// <value>Whether the quoting platform is being overriden.</value>
        public bool IsOverridingQuotingPlatform
        {
            get
            {
                if (!this.IsQuickQuotingEnabled && !this.IsDetailedQuotingEnabled)
                {
                    return false;
                }

                return this.isOverridingQuotingPlatform;
            }

            set
            {
                this.isOverridingQuotingPlatform = value;
            }
        }

        /// <summary>
        /// Gets the quoting transmission info id.
        /// </summary>
        /// <value>The quoting transmission info id.</value>
        public int? QuotingTransmissionId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the quoting transmission info.
        /// </summary>
        /// <value>The quoting transmission info.</value>
        public TitleTransmissionInfo QuotingTransmission
        {
            get
            {
                if (this.IsOverridingQuotingPlatform)
                {
                    return this.quotingTransmission;
                }

                return this.QuotingPlatform?.TransmissionInfo;
            }
        }

        /// <summary>
        /// Gets or sets the policy ordering platform id. Can be null if not enabled.
        /// </summary>
        /// <value>The policy ordering platform id.</value>
        public int? PolicyOrderingPlatformId
        {
            get
            {
                if (!this.IsPolicyOrderingEnabled)
                {
                    return null;
                }

                return this.policyOrderingPlatformId;
            }

            set
            {
                this.policyOrderingPlatformId = value.HasValue && value.Value < 0 ? null : value;
                if (value.HasValue && value.Value > 0 && this.IsPolicyOrderingEnabled)
                {
                    this.policyOrderingPlatform = new Lazy<TitlePlatform>(() => TitlePlatform.LoadPlatform(value.Value));
                }
                else
                {
                    this.policyOrderingPlatform = new Lazy<TitlePlatform>(() => null);
                }
            }
        }

        /// <summary>
        /// Gets the policy ordering platform.
        /// </summary>
        /// <value>The policy ordering platform.</value>
        public TitlePlatform PolicyOrderingPlatform
        {
            get
            {
                return this.policyOrderingPlatform.Value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the policy ordering platform is being overriden.
        /// </summary>
        /// <value>Whether the policy ordering platform is being overriden.</value>
        public bool IsOverridingPolicyOrderingPlatform
        {
            get
            {
                if (!this.IsPolicyOrderingEnabled)
                {
                    return false;
                }

                return this.isOverridingPolicyOrderingPlatform;
            }

            set
            {
                this.isOverridingPolicyOrderingPlatform = value;
            }
        }

        /// <summary>
        /// Gets the policy ordering transmision id.
        /// </summary>
        /// <value>The policy ordering transmission id.</value>
        public int? PolicyOrderingTransmissionId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the policy ordering transmission.
        /// </summary>
        /// <value>The policy ordering transmission.</value>
        public TitleTransmissionInfo PolicyOrderingTransmission
        {
            get
            {
                if (this.IsOverridingPolicyOrderingPlatform)
                {
                    return this.policyOrderingTransmission;
                }
                else if (this.PolicyOrderingPlatform != null)
                {
                    return this.PolicyOrderingPlatform.TransmissionInfo;
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Loads a title vendor.
        /// </summary>
        /// <param name="vendorId">The vendor id.</param>
        /// <returns>The created title vendor. Null if unsuccessful.</returns>
        public static TitleVendor LoadTitleVendor(int vendorId)
        {
            if (vendorId < 1)
            {
                return null;
            }

            StoredProcedureName? procedureName = StoredProcedureName.Create(GetVendorByIdSp);
            if (!procedureName.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {GetVendorByIdSp}."));
            }

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@VendorId", vendorId)
            };

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);
            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShareROnly))
            using (var reader = driver.ExecuteReader(connection, null, procedureName.Value, parameters))
            {
                TitleVendor vendor = new TitleVendor();
                if (reader.Read())
                {
                    vendor.LoadVendorInfoFromReader(reader);
                }
                else
                {
                    return null;
                }

                if (reader.NextResult())
                {
                    if ((vendor.IsQuickQuotingEnabled || vendor.IsDetailedQuotingEnabled) && reader.Read())
                    {
                        vendor.quotingTransmission = TitleTransmissionInfo.LoadTransmissionFromReader(reader);
                    }
                }

                if (reader.NextResult())
                {
                    if (vendor.IsPolicyOrderingEnabled && reader.Read())
                    {
                        vendor.policyOrderingTransmission = TitleTransmissionInfo.LoadTransmissionFromReader(reader);
                    }
                }

                return vendor;
            }
        }

        /// <summary>
        /// Loads a light version of all TitleVendors.
        /// </summary>
        /// <returns>An enumerable of all light versions of all TitleVendors.</returns>
        public static IEnumerable<LightTitleVendor> LoadTitleVendors_Light()
        {
            StoredProcedureName? procedureName = StoredProcedureName.Create(LoadTitleVendorsLightSP);
            if (!procedureName.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {LoadTitleVendorsLightSP}."));
            }

            List<LightTitleVendor> vendors = new List<LightTitleVendor>();

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);
            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShareROnly))
            using (var reader = driver.ExecuteReader(connection, null, procedureName.Value, null))
            {
                while (reader.Read())
                {
                    LightTitleVendor vendor = new LightTitleVendor();
                    vendor.CompanyName = (string)reader["CompanyName"];
                    vendor.IsEnabled = (bool)reader["IsEnabled"];
                    vendor.IsTestVendor = (bool)reader["IsTestVendor"];
                    vendor.VendorId = (int)reader["VendorId"];
                    vendor.IsQuickQuotingEnabled = (bool)reader["IsQuickQuotingEnabled"];
                    vendor.IsDetailedQuotingEnabled = (bool)reader["IsDetailedQuotingEnabled"];
                    vendor.IsPolicyOrderingEnabled = (bool)reader["IsPolicyOrderingEnabled"];
                    vendor.QuotingPlatformUsesAccountId = reader.AsNullableBool("QuotingPlatformUsesAccountId");
                    vendor.QuotingPlatformRequiresAccountId = reader.AsNullableBool("QuotingPlatformRequiresAccountId");
                    vendor.PolicyPlatformUsesAccountId = reader.AsNullableBool("PolicyPlatformUsesAccountId");
                    vendor.PolicyPlatformRequiresAccountId = reader.AsNullableBool("PolicyPlatformRequiresAccountId");

                    vendors.Add(vendor);
                }
            }

            return vendors;
        }

        /// <summary>
        /// Deletes a vendor with a vendor id.
        /// </summary>
        /// <param name="vendorId">The id of the vendor to delete.</param>
        /// <param name="errorMessage">Any error messages encountered.</param>
        /// <returns>True if deleted. False otherwise.</returns>
        public static bool DeleteVendor(int vendorId, out string errorMessage)
        {
            if (vendorId < 1)
            {
                errorMessage = $"Invalid vendor Id: {vendorId}";
                return false;
            }

            StoredProcedureName countServicesSp = StoredProcedureName.Create(GetLenderServiceCountByVendorIdSp).Value;
            SqlParameter[] countServicesSpParams = new SqlParameter[]
            {
                new SqlParameter("@VendorId", vendorId)
            };

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);
            int serviceCount = 0;
            foreach (var connection in DbConnectionInfo.ListAll())
            {
                using (var sqlConnection = connection.GetReadOnlyConnection())
                using (var reader = driver.ExecuteReader(sqlConnection, null, countServicesSp, countServicesSpParams))
                {
                    if (reader.Read())
                    {
                        serviceCount += (int)reader["ServiceCount"];
                    }
                }
            }

            if (serviceCount > 0)
            {
                errorMessage = "Unable to delete vendor. Lender services are using this vendor.";
                return false;
            }

            StoredProcedureName? deleteSp = StoredProcedureName.Create(DeleteVendorByIdSp);
            if (!deleteSp.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {DeleteVendorByIdSp}."));
            }

            SqlParameter[] deleteParams = new SqlParameter[]
            {
                new SqlParameter("@VendorId", vendorId)
            };

            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShareROnly))
            {
                driver.ExecuteNonQuery(connection, null, deleteSp.Value, deleteParams);
            }

            errorMessage = string.Empty;
            return true;
        }

        /// <summary>
        /// Creates a vendor object from the model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>The vendor if successful. Null otherwise.</returns>
        public static TitleVendor CreateFromViewModel(TitleVendorViewModel viewModel, out string errors)
        {
            bool vendorIsNew = !viewModel.VendorId.HasValue || viewModel.VendorId.Value < 1;

            if (!viewModel.Verify(out errors))
            {
                return null;
            }

            TitleVendor loadedVendor;
            if (!vendorIsNew)
            {
                loadedVendor = LoadTitleVendor(viewModel.VendorId.Value);
            }
            else
            {
                loadedVendor = new TitleVendor();
                loadedVendor.VendorId = -1;
                loadedVendor.QuotingPlatformId = null;
                loadedVendor.PolicyOrderingPlatformId = null;
            }

            loadedVendor.CompanyName = viewModel.CompanyName;
            loadedVendor.IsEnabled = viewModel.IsEnabled;
            loadedVendor.IsTestVendor = viewModel.IsTestVendor;
            loadedVendor.IsDetailedQuotingEnabled = viewModel.IsDetailedQuotingEnabled;
            loadedVendor.IsQuickQuotingEnabled = viewModel.IsQuickQuotingEnabled;
            loadedVendor.IsPolicyOrderingEnabled = viewModel.IsPolicyOrderingEnabled;
            loadedVendor.UsesProviderCodes = viewModel.UsesProviderCodes;

            bool isDifferentQuotingPlatform = loadedVendor.QuotingPlatformId != viewModel.QuotingPlatformId;
            loadedVendor.QuotingPlatformId = viewModel.QuotingPlatformId;
            loadedVendor.IsOverridingQuotingPlatform = viewModel.IsOverridingQuotingPlatform;
            if (loadedVendor.IsOverridingQuotingPlatform)
            {
                if (isDifferentQuotingPlatform || loadedVendor.quotingTransmission == null)
                {
                    loadedVendor.quotingTransmission = loadedVendor.QuotingPlatform.TransmissionInfo.CreateVendorCopy();
                }

                if (!loadedVendor.quotingTransmission.PopulateFromViewModel(viewModel.QuotingTransmission, out errors))
                {
                    return null;
                }
            }

            bool isDifferentPolicyOrderingPlatform = loadedVendor.PolicyOrderingPlatformId != viewModel.PolicyOrderingPlatformId;
            loadedVendor.PolicyOrderingPlatformId = viewModel.PolicyOrderingPlatformId;
            loadedVendor.IsOverridingPolicyOrderingPlatform = viewModel.IsOverridingPolicyOrderingPlatform;
            if (loadedVendor.IsOverridingPolicyOrderingPlatform)
            {
                if (isDifferentPolicyOrderingPlatform || loadedVendor.policyOrderingTransmission == null)
                {
                    loadedVendor.policyOrderingTransmission = loadedVendor.PolicyOrderingPlatform.TransmissionInfo.CreateVendorCopy();
                }

                if (!loadedVendor.policyOrderingTransmission.PopulateFromViewModel(viewModel.PolicyOrderingTransmission, out errors))
                {
                    return null;
                }
            }

            loadedVendor.IsNew = vendorIsNew;

            return loadedVendor;
        }

        /// <summary>
        /// Saves this vendor info and any associated transmission infos.
        /// </summary>
        public void Save()
        {
            using (var exec = new CStoredProcedureExec(DataSrc.LOShare))
            {
                try
                {
                    exec.BeginTransactionForWrite();

                    if (this.IsDetailedQuotingEnabled || this.IsQuickQuotingEnabled)
                    {
                        this.QuotingTransmissionId = this.QuotingTransmission.Save(this.IsNew ? null : this.QuotingTransmissionId, exec);
                    }

                    if (this.IsPolicyOrderingEnabled)
                    {
                        this.PolicyOrderingTransmissionId = this.PolicyOrderingTransmission.Save(this.IsNew ? null : this.PolicyOrderingTransmissionId, exec);
                    }

                    this.Save(exec);
                    exec.CommitTransaction();
                }
                catch (SystemException)
                {
                    exec.RollbackTransaction();
                    throw;
                }
            }

            this.IsNew = false;
        }

        /// <summary>
        /// Creates a viewmodel out of this TitleVendor.
        /// </summary>
        /// <returns>The viewmodel.</returns>
        public TitleVendorViewModel ToViewModel()
        {
            TitleVendorViewModel viewModel = new TitleVendorViewModel();
            viewModel.VendorId = this.VendorId;
            viewModel.CompanyName = this.CompanyName;
            viewModel.IsEnabled = this.IsEnabled;
            viewModel.IsTestVendor = this.IsTestVendor;
            viewModel.IsQuickQuotingEnabled = this.IsQuickQuotingEnabled;
            viewModel.IsDetailedQuotingEnabled = this.IsDetailedQuotingEnabled;
            viewModel.IsPolicyOrderingEnabled = this.IsPolicyOrderingEnabled;
            viewModel.QuotingPlatformId = this.QuotingPlatformId;
            viewModel.IsOverridingQuotingPlatform = this.IsOverridingQuotingPlatform;
            viewModel.PolicyOrderingPlatformId = this.PolicyOrderingPlatformId;
            viewModel.IsOverridingPolicyOrderingPlatform = this.IsOverridingPolicyOrderingPlatform;
            viewModel.UsesProviderCodes = this.UsesProviderCodes;

            if (this.QuotingTransmission != null)
            {
                viewModel.QuotingTransmission = this.QuotingTransmission.ToViewModel();
            }

            if (this.PolicyOrderingTransmission != null)
            {
                viewModel.PolicyOrderingTransmission = this.PolicyOrderingTransmission.ToViewModel();
            }

            return viewModel;
        }

        /// <summary>
        /// Saves the vendor info.
        /// </summary>
        /// <param name="exec">The transaction.</param>
        private void Save(CStoredProcedureExec exec)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@CompanyName", this.CompanyName),
                new SqlParameter("@IsEnabled", this.IsEnabled),
                new SqlParameter("@IsTestVendor", this.IsTestVendor),
                new SqlParameter("@QuotingPlatformId", this.QuotingPlatformId),
                new SqlParameter("@IsOverridingQuotingPlatform", this.IsOverridingQuotingPlatform),
                new SqlParameter("@QuotingTransmissionId", this.QuotingTransmissionId),
                new SqlParameter("@PolicyOrderingPlatformId", this.PolicyOrderingPlatformId),
                new SqlParameter("@IsOverridingPolicyOrderingPlatform", this.IsOverridingPolicyOrderingPlatform),
                new SqlParameter("@PolicyOrderingTransmissionId", this.PolicyOrderingTransmissionId),
                new SqlParameter("@IsQuickQuotingEnabled", this.IsQuickQuotingEnabled),
                new SqlParameter("@IsDetailedQuotingEnabled", this.IsDetailedQuotingEnabled),
                new SqlParameter("@IsPolicyOrderingEnabled", this.IsPolicyOrderingEnabled),
                new SqlParameter("@UsesProviderCodes", this.UsesProviderCodes)
            };

            SqlParameter vendorIdOutput = null;
            string sp = this.IsNew ? CreateVendorSp : UpdateVendorSp;
            if (!StoredProcedureName.Create(sp).HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {sp}."));
            }

            if (!this.IsNew)
            {
                parameters.Add(new SqlParameter("@VendorId", this.VendorId));
            }
            else
            {
                vendorIdOutput = new SqlParameter("@VendorId", System.Data.SqlDbType.Int);
                vendorIdOutput.Direction = System.Data.ParameterDirection.Output;
                parameters.Add(vendorIdOutput);
            }

            exec.ExecuteNonQuery(sp, 2, parameters.ToArray());

            if (this.IsNew)
            {
                this.VendorId = (int)vendorIdOutput.Value;
            }
        }

        /// <summary>
        /// Populates this TitleVendor with info from the reader.
        /// </summary>
        /// <param name="reader">The reader to load from.</param>
        private void LoadVendorInfoFromReader(IDataReader reader)
        {
            this.VendorId = (int)reader["VendorId"];
            this.CompanyName = (string)reader["CompanyName"];
            this.IsEnabled = (bool)reader["IsEnabled"];
            this.IsTestVendor = (bool)reader["IsTestVendor"];
            this.IsQuickQuotingEnabled = (bool)reader["IsQuickQuotingEnabled"];
            this.IsDetailedQuotingEnabled = (bool)reader["IsDetailedQuotingEnabled"];
            this.IsPolicyOrderingEnabled = (bool)reader["IsPolicyOrderingEnabled"];
            this.UsesProviderCodes = (bool)reader["UsesProviderCodes"];

            this.QuotingPlatformId = reader["QuotingPlatformId"] == DBNull.Value ? (int?)null : (int)reader["QuotingPlatformId"];
            this.IsOverridingQuotingPlatform = reader["IsOverridingQuotingPlatform"] == DBNull.Value ? false : (bool)reader["IsOverridingQuotingPlatform"];
            this.QuotingTransmissionId = reader["QuotingTransmissionId"] == DBNull.Value ? (int?)null : (int)reader["QuotingTransmissionId"];

            this.PolicyOrderingPlatformId = reader["PolicyOrderingPlatformId"] == DBNull.Value ? (int?)null : (int)reader["PolicyOrderingPlatformId"];
            this.IsOverridingPolicyOrderingPlatform = reader["IsOverridingPolicyOrderingPlatform"] == DBNull.Value ? false : (bool)reader["IsOverridingPolicyOrderingPlatform"];
            this.PolicyOrderingTransmissionId = reader["PolicyOrderingTransmissionId"] == DBNull.Value ? (int?)null : (int)reader["PolicyOrderingTransmissionId"];
        }
    }
}
