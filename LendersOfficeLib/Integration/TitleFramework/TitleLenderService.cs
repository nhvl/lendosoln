﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using Common;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;
    using Security;

    /// <summary>
    /// Represents the lender-level configuration for a particular title vendor.
    /// </summary>
    public class TitleLenderService
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="TitleLenderService"/> class from being created.
        /// </summary>
        private TitleLenderService()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TitleLenderService"/> class.
        /// </summary>
        /// <param name="isNew">Whether the service is newly created.</param>
        /// <param name="lenderServiceId">The lender service's ID.</param>
        /// <param name="publicId">The public id.</param>
        /// <param name="brokerId">The associated broker ID.</param>
        /// <param name="vendorId">The associated vendor ID.</param>
        /// <param name="vendorName">The associated vendor's name.</param>
        /// <param name="isNonInteractiveQuoteEnabled">Whether non-interactive quotes are enabled.</param>
        /// <param name="isInteractiveQuoteEnabled">Whether interactive quotes are enabled.</param>
        /// <param name="isPolicyEnabled">Whether policy orders are enabled.</param>
        /// <param name="configurationFileDbKey">The unique key to retrieve the configuration file from FileDB.</param>
        /// <param name="isEnabledForLqbUsers">Whether the vendor is enabled for LQB users.</param>
        /// <param name="enabledEmployeeGroupId">If enabled for LQB users, which employee group to enable for.</param>
        /// <param name="isEnabledForTpoUsers">Whether the vendor is enabled for PML users.</param>
        /// <param name="enabledOcGroupId">If enabled for PML users, which OC groups to enable for.</param>
        /// <param name="usesProviderCodes">Whether to use provider codes with the vendor.</param>
        private TitleLenderService(
            bool isNew,
            int lenderServiceId,
            Guid publicId,
            Guid brokerId,
            int vendorId,
            string vendorName,
            bool isNonInteractiveQuoteEnabled,
            bool isInteractiveQuoteEnabled,
            bool isPolicyEnabled,
            Guid? configurationFileDbKey,
            bool isEnabledForLqbUsers,
            Guid? enabledEmployeeGroupId,
            bool isEnabledForTpoUsers,
            Guid? enabledOcGroupId,
            bool usesProviderCodes)
        {
            this.IsNew = isNew;
            this.LenderServiceId = lenderServiceId;
            this.PublicId = publicId;
            this.BrokerId = brokerId;
            this.VendorId = vendorId;
            this.VendorName = vendorName;
            this.IsNonInteractiveQuoteEnabled = isNonInteractiveQuoteEnabled;
            this.IsInteractiveQuoteEnabled = isInteractiveQuoteEnabled;
            this.IsPolicyEnabled = isPolicyEnabled;
            this.ConfigurationFileDbKey = configurationFileDbKey;
            this.IsEnabledForLqbUsers = isEnabledForLqbUsers;
            this.EnabledEmployeeGroupId = enabledEmployeeGroupId;
            this.IsEnabledForTpoUsers = isEnabledForLqbUsers;
            this.EnabledOcGroupId = enabledOcGroupId;
            this.UsesProviderCodes = usesProviderCodes;
        }

        /// <summary>
        /// Gets or sets the display name of the Lender Service configuration.
        /// </summary>
        /// <value>Display name.</value>
        public string DisplayName => this.VendorName;

        /// <summary>
        /// Gets a value indicating whether this is a new lender service without a record in the database.
        /// </summary>
        /// <value>Whether this is a new lender service.</value>
        public bool IsNew { get; private set; }

        /// <summary>
        /// Gets the id of the lender level service.
        /// </summary>
        /// <value>Lender service ID.</value>
        public int LenderServiceId { get; private set; }

        /// <summary>
        /// Gets the id that can be used publicly (webservices).
        /// </summary>
        /// <value>The public id.</value>
        public Guid PublicId { get; private set; }

        /// <summary>
        /// Gets the broker ID of the lender associated with this configuration.
        /// </summary>
        /// <value>Broker ID.</value>
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets the vendor ID of the vendor being configured.
        /// </summary>
        /// <value>Vendor ID.</value>
        public int VendorId { get; private set; }

        /// <summary>
        /// Gets or sets the name of the associated vendor.
        /// </summary>
        /// <value>Vendor name.</value>
        public string VendorName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether Non-interactive (automatic) quoting is enabled.
        /// </summary>
        /// <value>Is non-interactive quote enabled?.</value>
        public bool IsNonInteractiveQuoteEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether interactive quoting through the UI is enabled.
        /// </summary>
        /// <value>Is interactive quote enabled?.</value>
        public bool IsInteractiveQuoteEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether policy orders are enabled (assuming the vendor supports policy orders).
        /// </summary>
        /// <value>Is policy enabled?.</value>
        public bool IsPolicyEnabled { get; set; }

        /// <summary>
        /// Gets or sets the FileDB key for retrieving the lender's configuration file.
        /// </summary>
        /// <value>Configuration file FileDB key.</value>
        public Guid? ConfigurationFileDbKey { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this vendor is enabled for "B" users.
        /// </summary>
        /// <value>Whether the vendor is enabled for LQB users.</value>
        public bool IsEnabledForLqbUsers { get; set; }

        /// <summary>
        /// Gets or sets the identifier for a specific employee group for which the vendor should be enabled.
        /// The title vendor should be disabled for other employee groups on the associated lender.
        /// Null if allowed for all LQB employees.
        /// Has no meaning if <see cref="IsEnabledForLqbUsers"/> is false.
        /// </summary>
        /// <value>ID of enabled employee group.</value>
        public Guid? EnabledEmployeeGroupId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this vendor is enabled for users in the TPO portal.
        /// </summary>
        /// <value>Whether the vendor is enabled for PML users.</value>
        public bool IsEnabledForTpoUsers { get; set; }

        /// <summary>
        /// Gets or sets the identifier for a specific Originating Company group for which the vendor should be enabled.
        /// The title vendor should be disabled for other OC groups on the associated lender.
        /// Null if allowed for all TPO employees.
        /// Has no meaning if <see cref="IsEnabledForTpoUsers"/> is false.
        /// </summary>
        /// <value>ID of enabled originating company group.</value>
        public Guid? EnabledOcGroupId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the lender will be using provider codes to specify title companies to the vendor.
        /// </summary>
        /// <remarks>
        /// Ernst allows the user to specify a title provider (local title company) to use for a particular quote.
        /// However, some lenders might not want to use this feature, so it has been made a lender-level configuration option.
        /// </remarks>
        /// <value>Whether to use provider codes.</value>
        public bool UsesProviderCodes { get; set; }

        /// <summary>
        /// Creates an instance of the <see cref="TitleLenderService"/> class from the UI view model.
        /// </summary>
        /// <param name="vm">The view model gathered from UI input.</param>
        /// <param name="user">The user making the request. This user's brokerId will be associated with the configuration.</param>
        /// <param name="errors">Any errors encountered while creating the lender service.</param>
        /// <returns>A new lender service object with values populated from the view model.</returns> 
        public static TitleLenderService CreateFromViewModel(TitleLenderServiceViewModel vm, AbstractUserPrincipal user, out string errors)
        {
            if (!VerifyViewModel(vm, user.BrokerId, out errors))
            {
                return null;
            }

            Guid? configurationKey = null;
            if (!string.IsNullOrEmpty(vm.ConfigurationFileKeyTempKey))
            {
                string keyFromCache = AutoExpiredTextCache.GetUserString(user, vm.ConfigurationFileKeyTempKey);
                if (keyFromCache == null)
                {
                    errors = "Configuration file upload has expired. Please upload again.";
                    return null;
                }

                configurationKey = Guid.Parse(keyFromCache);
            }

            if (vm.IsNew || !vm.LenderServiceId.HasValue || vm.LenderServiceId.Value < 1)
            {
                return new TitleLenderService(
                    isNew: true,
                    lenderServiceId: -1,
                    publicId: Guid.NewGuid(),
                    brokerId: user.BrokerId,
                    vendorId: vm.AssociatedVendorId,
                    vendorName: TitleVendor.LoadTitleVendor(vm.AssociatedVendorId).CompanyName,
                    isNonInteractiveQuoteEnabled: vm.IsNonInteractiveQuoteEnabled,
                    isInteractiveQuoteEnabled: vm.IsInteractiveQuoteEnabled,
                    isPolicyEnabled: vm.IsPolicyEnabled,
                    configurationFileDbKey: configurationKey,
                    usesProviderCodes: vm.UsesProviderCodes,
                    isEnabledForLqbUsers: vm.IsEnabledForLqbUsers,
                    enabledEmployeeGroupId: vm.EnabledEmployeeGroupId,
                    isEnabledForTpoUsers: vm.IsEnabledForTpoUsers,
                    enabledOcGroupId: vm.EnabledOCGroupId);
            }
            else
            {
                var lenderService = GetLenderServiceById(user.BrokerId, vm.LenderServiceId.Value);
                if (lenderService == null)
                {
                    errors = "Unable to find title service provider.";
                    return null;
                }

                lenderService.VendorId = vm.AssociatedVendorId;
                lenderService.VendorName = TitleVendor.LoadTitleVendor(vm.AssociatedVendorId).CompanyName;
                lenderService.IsNonInteractiveQuoteEnabled = vm.IsNonInteractiveQuoteEnabled;
                lenderService.IsInteractiveQuoteEnabled = vm.IsInteractiveQuoteEnabled;
                lenderService.IsPolicyEnabled = vm.IsPolicyEnabled;
                lenderService.ConfigurationFileDbKey = configurationKey ?? lenderService.ConfigurationFileDbKey;
                lenderService.UsesProviderCodes = vm.UsesProviderCodes;
                lenderService.IsEnabledForLqbUsers = vm.IsEnabledForLqbUsers;
                lenderService.EnabledEmployeeGroupId = vm.EnabledEmployeeGroupId;
                lenderService.IsEnabledForTpoUsers = vm.IsEnabledForTpoUsers;
                lenderService.EnabledOcGroupId = vm.EnabledOCGroupId;

                return lenderService;
            }
        }

        /// <summary>
        /// Loads a lender service by id.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="lenderServiceId">The lender service id.</param>
        /// <returns>The service if found. Null otherwise.</returns>
        public static TitleLenderService GetLenderServiceById(Guid brokerId, int lenderServiceId)
        {
            StoredProcedureName procedureName = StoredProcedureName.Create("TITLE_FRAMEWORK_LENDER_SERVICE_Load").ForceValue();

            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LenderServiceId", lenderServiceId)
            };

            using (var connection = DbAccessUtils.GetConnection(brokerId))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(
                    connection,
                    transaction: null,
                    procedureName: procedureName,
                    parameters: parameters,
                    timeout: TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    return LoadFromReader(reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the lender service id using the public id.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="publicId">The public id.</param>
        /// <returns>The title lender service id.</returns>
        public static int? GetLenderServiceIdByPublicId(Guid brokerId, Guid publicId)
        {
            StoredProcedureName procedureName = StoredProcedureName.Create("TITLE_FRAMEWORK_LENDER_SERVICE_LoadByPublicId").ForceValue();

            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@PublicId", publicId)
            };

            using (var connection = DbAccessUtils.GetConnection(brokerId))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(
                    connection,
                    transaction: null,
                    procedureName: procedureName,
                    parameters: parameters,
                    timeout: TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    return (int)reader["LenderServiceId"];
                }
            }

            return null;
        }

        /// <summary>
        /// Gets a list of all of the title lender services for a given broker ID.
        /// </summary>
        /// <param name="brokerId">The broker ID of the lender to get services for.</param>
        /// <returns>A list of all of the title lender service configurations associated with the lender.</returns>
        public static List<TitleLenderService> GetLenderServicesByBrokerId(Guid brokerId)
        {
            StoredProcedureName procedureName = StoredProcedureName.Create("TITLE_FRAMEWORK_LENDER_SERVICE_ListForBroker").ForceValue();

            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            List<TitleLenderService> lenderServices = new List<TitleLenderService>();
            using (var connection = DbAccessUtils.GetConnection(brokerId))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(
                    connection,
                    transaction: null,
                    procedureName: procedureName,
                    parameters: parameters,
                    timeout: TimeoutInSeconds.Thirty))
            {
                while (reader.Read())
                {
                    lenderServices.Add(LoadFromReader(reader));
                }
            }

            return lenderServices;
        }

        /// <summary>
        /// Gets the title lender services based on user principal and requested service type.
        /// </summary>
        /// <param name="principal">The user principal.</param>
        /// <param name="forPolicy">Whether to grab services enabled for policy ordering.</param>
        /// <param name="forInteractive">Whether to grab services enabled for interactive quote ordering.</param>
        /// <param name="forNonInteractive">Whether to grab services enabled for non-interactive quote ordering.</param>
        /// <returns>A collection of eligible TitleLenderServices.</returns>
        public static IEnumerable<TitleLenderService> GetLenderServicesByBrokerId(AbstractUserPrincipal principal, bool forPolicy, bool forInteractive, bool forNonInteractive)
        {
            return FilterForPrincipal(GetLenderServicesByBrokerId(principal.BrokerId), principal, forPolicy, forInteractive, forNonInteractive);
        }

        /// <summary>
        /// Filters the set of lender services for the specified principal.
        /// </summary>
        /// <param name="lenderServices">The lender services to filter.</param>
        /// <param name="principal">The user principal.</param>
        /// <param name="forPolicy">Whether to grab services enabled for policy ordering.</param>
        /// <param name="forInteractive">Whether to grab services enabled for interactive quote ordering.</param>
        /// <param name="forNonInteractive">Whether to grab services enabled for non-interactive quote ordering.</param>
        /// <returns>A collection of eligible TitleLenderServices.</returns>
        public static IEnumerable<TitleLenderService> FilterForPrincipal(IEnumerable<TitleLenderService> lenderServices, AbstractUserPrincipal principal, bool forPolicy, bool forInteractive, bool forNonInteractive)
        {
            bool isPUser = principal.Type.Equals("P", StringComparison.OrdinalIgnoreCase);
            return lenderServices.Where(service =>
            {
                bool enabledForUser;
                if (isPUser)
                {
                    enabledForUser = service.IsEnabledForTpoUsers && (service.EnabledOcGroupId == null || principal.IsInPmlBrokerGroup(service.EnabledOcGroupId.Value));
                }
                else
                {
                    enabledForUser = service.IsEnabledForLqbUsers && (service.EnabledEmployeeGroupId == null || principal.IsInEmployeeGroup(service.EnabledEmployeeGroupId.Value));
                }

                bool enabledForType = false;
                if (forPolicy)
                {
                    enabledForType |= service.IsPolicyEnabled;
                }

                if (forInteractive)
                {
                    enabledForType |= service.IsInteractiveQuoteEnabled;
                }

                if (forNonInteractive)
                {
                    enabledForType |= service.IsNonInteractiveQuoteEnabled;
                }

                return enabledForUser && enabledForType;
            });
        }

        /// <summary>
        /// Deletes the lender service.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="lenderServiceId">The lender service id.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if deleted. False otherwise.</returns>
        public static bool DeleteService(Guid brokerId, int lenderServiceId, out string errors)
        {
            if (lenderServiceId < 1)
            {
                errors = $"Invalid id {lenderServiceId}";
                return false;
            }

            // Load service for deleting fileDB entries.
            var service = TitleLenderService.GetLenderServiceById(brokerId, lenderServiceId);

            StoredProcedureName procedureName = StoredProcedureName.Create("TITLE_FRAMEWORK_LENDER_SERVICE_Delete").ForceValue();

            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LenderServiceId", lenderServiceId)
            };

            bool deleted;
            using (var connection = DbAccessUtils.GetConnection(brokerId))
            {
                int modifiedRowCount = StoredProcedureDriverHelper.ExecuteNonQuery(
                    connection,
                    transaction: null,
                    procedureName: procedureName,
                    parameters: parameters,
                    timeout: TimeoutInSeconds.Thirty).Value;
                if (modifiedRowCount > 0)
                {
                    errors = string.Empty;
                    deleted = true;
                }
                else
                {
                    errors = "No service was found to delete.";
                    deleted = false;
                }
            }

            if (deleted)
            {
                ErnstConfiguration.DeleteConfiguration(service.ConfigurationFileDbKey);
            }

            return deleted;
        }

        /// <summary>
        /// Saves the lender service.
        /// </summary>
        /// <returns>Whether the save was successful.</returns>
        public bool Save()
        {
            string spName = this.IsNew ? "TITLE_FRAMEWORK_LENDER_SERVICE_Insert" : "TITLE_FRAMEWORK_LENDER_SERVICE_Update";
            StoredProcedureName procedureName = StoredProcedureName.Create(spName).ForceValue();

            SqlParameter lenderServiceIdParameter = new SqlParameter("@LenderServiceID", SqlDbType.Int)
            {
                Direction = this.IsNew ? ParameterDirection.Output : ParameterDirection.Input,
                Value = this.LenderServiceId
            };
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                lenderServiceIdParameter,
                new SqlParameter("@PublicId", this.PublicId),
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@VendorId", this.VendorId),
                new SqlParameter("@VendorName", this.VendorName),
                new SqlParameter("@IsNonInteractiveQuoteEnabled", this.IsNonInteractiveQuoteEnabled),
                new SqlParameter("@IsInteractiveQuoteEnabled", this.IsInteractiveQuoteEnabled),
                new SqlParameter("@IsPolicyEnabled", this.IsPolicyEnabled),
                new SqlParameter("@ConfigurationFileDbKey", this.ConfigurationFileDbKey == Guid.Empty ? null : this.ConfigurationFileDbKey),
                new SqlParameter("@IsEnabledForLqbUsers", this.IsEnabledForLqbUsers),
                new SqlParameter("@EnabledEmployeeGroupId", this.EnabledEmployeeGroupId == Guid.Empty ? null : this.EnabledEmployeeGroupId),
                new SqlParameter("@IsEnabledForTpoUsers", this.IsEnabledForTpoUsers),
                new SqlParameter("@EnabledOcGroupId", this.EnabledOcGroupId == Guid.Empty ? null : this.EnabledOcGroupId),
                new SqlParameter("@UsesProviderCodes", this.UsesProviderCodes)
            };

            using (var connection = DbAccessUtils.GetConnection(this.BrokerId))
            {
                StoredProcedureDriverHelper.ExecuteNonQuery(
                    connection,
                    transaction: null,
                    procedureName: procedureName,
                    parameters: parameters,
                    timeout: TimeoutInSeconds.Thirty);
            }

            if (this.IsNew)
            {
                this.LenderServiceId = (int)lenderServiceIdParameter.Value;
            }

            this.IsNew = false;
            return true;
        }

        /// <summary>
        /// Loads a new instance of the <see cref="TitleLenderService"/> class from an <see cref="IDataRecord"/> instance with the necessary fields.
        /// </summary>
        /// <param name="titleLenderServiceRow">The data record to read from.</param>
        /// <returns>A new instance of the <see cref="TitleLenderService"/> class with values read from the data record.</returns>
        private static TitleLenderService LoadFromReader(IDataRecord titleLenderServiceRow)
        {
            return new TitleLenderService(
                isNew: false,
                lenderServiceId: titleLenderServiceRow.SafeInt("LenderServiceId"),
                publicId: titleLenderServiceRow.SafeGuid("PublicId"),
                brokerId: titleLenderServiceRow.SafeGuid("BrokerId"),
                vendorId: titleLenderServiceRow.SafeInt("VendorId"),
                vendorName: titleLenderServiceRow.SafeString("VendorName"),
                isNonInteractiveQuoteEnabled: titleLenderServiceRow.SafeBool("IsNonInteractiveQuoteEnabled"),
                isInteractiveQuoteEnabled: titleLenderServiceRow.SafeBool("IsInteractiveQuoteEnabled"),
                isPolicyEnabled: titleLenderServiceRow.SafeBool("IsPolicyEnabled"),
                configurationFileDbKey: titleLenderServiceRow.AsNullableGuid("ConfigurationFileDbKey"),
                isEnabledForLqbUsers: titleLenderServiceRow.SafeBool("IsEnabledForLqbUsers"),
                enabledEmployeeGroupId: titleLenderServiceRow.AsNullableGuid("EnabledEmployeeGroupId"),
                isEnabledForTpoUsers: titleLenderServiceRow.SafeBool("IsEnabledForTpoUsers"),
                enabledOcGroupId: titleLenderServiceRow.AsNullableGuid("EnabledOcGroupId"),
                usesProviderCodes: titleLenderServiceRow.SafeBool("UsesProviderCodes"));
        }

        /// <summary>
        /// Verifies the view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="brokerId">The broker ID of the lender whose configuration this is.</param>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if verified. False otherwise.</returns>
        private static bool VerifyViewModel(TitleLenderServiceViewModel viewModel, Guid brokerId, out string errors)
        {
            if (viewModel.AssociatedVendorId <= 0)
            {
                errors = "Please select a vendor.";
                return false;
            }

            if (TitleVendor.LoadTitleVendor(viewModel.AssociatedVendorId) == null)
            {
                errors = "The specified vendor could not be found.";
                return false;
            }

            if (!viewModel.IsNew 
                && ((viewModel.LenderServiceId == null || viewModel.LenderServiceId <= 0)
                  || GetLenderServiceById(brokerId, viewModel.LenderServiceId.Value) == null))
            {
                errors = ErrorMessage.ValidationError.ToString();
                return false;
            }

            errors = string.Empty;
            return true;
        }
    }
}