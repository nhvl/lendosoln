﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;

    /// <summary>
    /// Provides an interface specifying the expected components of a Title Framework response data object.
    /// </summary>
    public interface ITitleQuoteResponseData
    {
        /// <summary>
        /// Gets the error message returned from the vendor, if any.
        /// </summary>
        /// <value>Vendor error.</value>
        string VendorError { get; }

        /// <summary>
        /// Gets the Vendor-generated order identifier, which they send as "TransactionID" in their XML.
        /// </summary>
        /// <value>Order number.</value>
        string OrderNumber { get; }

        /// <summary>
        /// Gets the LQB-generated transaction GUID.
        /// </summary>
        /// <value>Transaction ID.</value>
        Guid? TransactionId { get; }

        /// <summary>
        /// Gets the parsed fee quote object.
        /// </summary>
        /// <value>Quoted fees.</value>
        TitleIntegrationQuote QuotedFees { get; }

        /// <summary>
        /// Gets the raw response string returned from Ernst.
        /// </summary>
        /// <value>Raw response string.</value>
        string RawResponse { get; }
    }
}
