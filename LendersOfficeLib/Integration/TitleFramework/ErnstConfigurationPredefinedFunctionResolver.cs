﻿namespace LendersOffice.Integration.TitleFramework
{
    using DataAccess;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Resolves the Ernst Configuration predefined function.
    /// </summary>
    public class ErnstConfigurationPredefinedFunctionResolver
    {
        /// <summary>
        /// Validates if the string value can be used with the predefined function's operation.
        /// </summary>
        /// <param name="stringValue">The string value to check.</param>
        /// <param name="function">The function to apply.</param>
        /// <returns>True if can be used or if there is no function to apply. False otherwise.</returns>
        public bool ValidateStringAgainstOperation(string stringValue, ErnstConfigurationPredefinedFunction function)
        {
            if (function == null)
            {
                return true;
            }

            string finalValue;
            switch (function.Operation)
            {
                case ErnstConfigFunctionOperation.Add:
                    return this.ApplyAddOperation(string.Empty, stringValue, out finalValue) == ErnstConfigFunctionResolverApplyStatus.Applied;
                default:
                    throw new UnhandledEnumException(function.Operation);
            }
        }

        /// <summary>
        /// Runs the function on an existing value using the modifier value.
        /// </summary>
        /// <param name="existingValue">The existing value.</param>
        /// <param name="modifierValue">The modifier value.</param>
        /// <param name="dataLoan">The data loan.</param>
        /// <param name="function">The function.</param>
        /// <param name="finalValue">The value after the function is applied. Null if unable to be applied.</param>
        /// <param name="error">The error if the function couldn't be applied.</param>
        /// <returns>Null if the function couldn't be applied. A string representation of the results otherwise.</returns>
        public ErnstConfigFunctionResolverApplyStatus ApplyFunction(string existingValue, string modifierValue, CPageData dataLoan, ErnstConfigurationPredefinedFunction function, out string finalValue, out string error)
        {
            error = null;
            if (dataLoan == null || function == null)
            {
                error = "Data loan or function is null.";
                finalValue = null;
                return ErnstConfigFunctionResolverApplyStatus.Error;
            }

            if (this.IsConditionMet(dataLoan, function.Condition))
            {
                ErnstConfigFunctionResolverApplyStatus operationStatus;
                switch (function.Operation)
                {
                    case ErnstConfigFunctionOperation.Add:
                        operationStatus = this.ApplyAddOperation(existingValue, modifierValue, out finalValue);
                        error = operationStatus == ErnstConfigFunctionResolverApplyStatus.InvalidOperands ? "Invalid operands to addition function." : null;
                        break;
                    default:
                        throw new UnhandledEnumException(function.Operation);
                }

                return operationStatus;
            }
            else
            {
                finalValue = existingValue;
                return ErnstConfigFunctionResolverApplyStatus.ConditionNotMet;
            }
        }

        /// <summary>
        /// Checks if the condition has been met.
        /// </summary>
        /// <param name="dataLoan">The data loan.</param>
        /// <param name="condition">The condition to run.</param>
        /// <returns>True if met. False otherwise.</returns>
        private bool IsConditionMet(CPageData dataLoan, ErnstConfigFunctionCondition condition)
        {
            switch (condition)
            {
                case ErnstConfigFunctionCondition.IfARM:
                    return dataLoan.sFinMethT == E_sFinMethT.ARM;
                case ErnstConfigFunctionCondition.IfFixed:
                    return dataLoan.sFinMethT == E_sFinMethT.Fixed;
                case ErnstConfigFunctionCondition.If50a6:
                    return dataLoan.sProdIsTexas50a6Loan;
                case ErnstConfigFunctionCondition.IfCondo:
                    return dataLoan.sGseSpT == E_sGseSpT.Condominium || dataLoan.sGseSpT == E_sGseSpT.DetachedCondominium || dataLoan.sGseSpT == E_sGseSpT.HighRiseCondominium || dataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeCondominium;
                case ErnstConfigFunctionCondition.IfNon50a6Condo:
                    return !dataLoan.sProdIsTexas50a6Loan && (dataLoan.sGseSpT == E_sGseSpT.Condominium || dataLoan.sGseSpT == E_sGseSpT.DetachedCondominium || dataLoan.sGseSpT == E_sGseSpT.HighRiseCondominium || dataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeCondominium);
                case ErnstConfigFunctionCondition.If50a6Condo:
                    return dataLoan.sProdIsTexas50a6Loan && (dataLoan.sGseSpT == E_sGseSpT.Condominium || dataLoan.sGseSpT == E_sGseSpT.DetachedCondominium || dataLoan.sGseSpT == E_sGseSpT.HighRiseCondominium || dataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeCondominium);
                case ErnstConfigFunctionCondition.IfPUD:
                    return dataLoan.sSpIsInPud;
                case ErnstConfigFunctionCondition.IfNon50a6PUD:
                    return !dataLoan.sProdIsTexas50a6Loan && dataLoan.sSpIsInPud;
                case ErnstConfigFunctionCondition.If50a6PUD:
                    return dataLoan.sProdIsTexas50a6Loan && dataLoan.sSpIsInPud;
                case ErnstConfigFunctionCondition.IfBalloon:
                    return dataLoan.sDue < dataLoan.sTerm;
                case ErnstConfigFunctionCondition.IfSecondHome:
                    return dataLoan.sOccT == E_sOccT.SecondaryResidence;
                case ErnstConfigFunctionCondition.IfPurchase:
                    return !dataLoan.sIsRefinancing;
                case ErnstConfigFunctionCondition.IfRefinance:
                    return dataLoan.sIsRefinancing;
                case ErnstConfigFunctionCondition.IfPrimaryResidence:
                    return dataLoan.sOccT == E_sOccT.PrimaryResidence;
                case ErnstConfigFunctionCondition.IfInvestmentProperty:
                    return dataLoan.sOccT == E_sOccT.Investment;
                case ErnstConfigFunctionCondition.IfSecondLien:
                    return dataLoan.sLienPosT == E_sLienPosT.Second;
                case ErnstConfigFunctionCondition.IfMultiUnit:
                    return dataLoan.sUnitsNum > 1;
                case ErnstConfigFunctionCondition.IfPrepaymentPenalty:
                    return dataLoan.sHardPlusSoftPrepmtPeriodMonths > 0;
                default:
                    throw new UnhandledEnumException(condition);
            }
        }

        /// <summary>
        /// Adds the two int strings together.
        /// </summary>
        /// <param name="intStringToModify">The int string to modify.</param>
        /// <param name="intStringToAdd">The int string to apply.</param>
        /// <param name="intStringFinal">The final value as a string.</param>
        /// <returns>The status after trying to apply the operation.</returns>
        private ErnstConfigFunctionResolverApplyStatus ApplyAddOperation(string intStringToModify, string intStringToAdd, out string intStringFinal)
        {
            int intToModify;
            int intToAdd;

            if (string.IsNullOrWhiteSpace(intStringToModify))
            {
                // For adds, we'll treat this as a 0 value.
                intStringToModify = "0";
            }

            if (string.IsNullOrEmpty(intStringToAdd) || string.IsNullOrEmpty(intStringToModify) ||
                !int.TryParse(intStringToModify, out intToModify) || !int.TryParse(intStringToAdd, out intToAdd))
            {
                intStringFinal = null;
                return ErnstConfigFunctionResolverApplyStatus.InvalidOperands;
            }

            intStringFinal = (intToModify + intToAdd).ToString();
            return ErnstConfigFunctionResolverApplyStatus.Applied;
        }
    }
}
