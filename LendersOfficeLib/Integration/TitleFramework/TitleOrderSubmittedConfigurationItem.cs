﻿namespace LendersOffice.Integration.TitleFramework
{
    using DataAccess;

    /// <summary>
    /// A very basic class that holds a configuration item sent in a title order request.
    /// Barebones since this will only be used for storage and display.
    /// </summary>
    public class TitleOrderSubmittedConfigurationItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TitleOrderSubmittedConfigurationItem"/> class.
        /// </summary>
        /// <param name="state">The state.</param>
        /// <param name="description">The description.</param>
        /// <param name="loanType">The loan type.</param>
        /// <param name="predefinedFunction">The predefined function.</param>
        /// <param name="value">The value.</param>
        public TitleOrderSubmittedConfigurationItem(string state, string description, E_sLT? loanType, string predefinedFunction, string value)
        {
            this.State = state;
            this.Description = description;
            this.Value = description;
            this.LoanType = loanType;
            this.PredefinedFunction = predefinedFunction;
        }

        /// <summary>
        /// Gets the state. Can be null.
        /// </summary>
        /// <value>The state.</value>
        public string State
        {
            get;
        }

        /// <summary>
        /// Gets the loan type.
        /// </summary>
        /// <value>The loan type.</value>
        public E_sLT? LoanType
        {
            get;
        }

        /// <summary>
        /// Gets the predefined function.
        /// </summary>
        /// <value>The predefined function.</value>
        public string PredefinedFunction
        {
            get;
        }

        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <value>The description.</value>
        public string Description
        {
            get;
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>The value.</value>
        public string Value
        {
            get;
        }
    }
}
