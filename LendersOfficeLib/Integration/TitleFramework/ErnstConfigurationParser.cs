﻿namespace LendersOffice.Integration.TitleFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ClosedXML.Excel;
    using DataAccess;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Contains a set of methods relating to parsing the configuration data for an Ernst configuration from an Excel SpreadSheet.
    /// </summary>
    public static class ErnstConfigurationParser
    {
        /// <summary>
        /// The name for the "Path" column in the worksheet.
        /// </summary>
        private const string PathColumnName = "Path";

        /// <summary>
        /// The name for the "State" column in the worksheet.
        /// </summary>
        private const string StateColumnName = "State";

        /// <summary>
        /// The name for the "Description" column in the worksheet.
        /// </summary>
        private const string DescriptionColumnName = "Description";

        /// <summary>
        /// The name for the "Default" column in the worksheet.
        /// </summary>
        private const string DefaultValueColumnName = "Default";

        /// <summary>
        /// The name for the "PredefinedFunction" column in the worksheet.
        /// </summary>
        private const string PredefinedFunctionColumnName = "PredefinedFunction";

        /// <summary>
        /// The name for the "LoanType" column in the worksheet.
        /// </summary>
        private const string LoanTypeColumnName = "LoanType";

        /// <summary>
        /// Other valid strings for loan type for the the ernst configuration file.
        /// </summary>
        private static readonly IReadOnlyDictionary<string, E_sLT> OtherValidLoanTypeStrings = new Dictionary<string, E_sLT>(StringComparer.OrdinalIgnoreCase)
        {
            { "usda", E_sLT.UsdaRural }
        };

        /// <summary>
        /// Writes a blank configuration file to a temporary file path.
        /// </summary>
        /// <returns>A blank configuration file to a temporary file path.</returns>
        public static LocalFilePath WriteBlankConfigurationToTempFile()
        {
            LocalFilePath tempFilePath = LqbGrammar.Utils.TempFileUtils.NewTempFilePath();
            using (XLWorkbook workbook = new XLWorkbook())
            using (IXLWorksheet worksheet = workbook.AddWorksheet("Sheet1"))
            {
                IXLRow row = worksheet.FirstRow();
                row.Cell(1).Value = DescriptionColumnName;
                row.Cell(2).Value = StateColumnName;
                row.Cell(3).Value = LoanTypeColumnName;
                row.Cell(4).Value = PredefinedFunctionColumnName;
                row.Cell(5).Value = DefaultValueColumnName;
                row.Cell(6).Value = PathColumnName;
                workbook.SaveAs(tempFilePath.Value);
            }

            return tempFilePath;
        }

        /// <summary>
        /// Uploads a file into the specified service configuration.
        /// </summary>
        /// <param name="tempFilePath">The file path to the temporary file containing the data to be uploaded.</param>
        /// <param name="clientFilePath">The path for the file on the client's machine.</param>
        /// <returns>A result containing the uploaded configuration or an error.</returns>
        public static Result<ErnstConfiguration> UploadFile(LocalFilePath tempFilePath, LocalFilePath clientFilePath)
        {
            string extension = System.IO.Path.GetExtension(clientFilePath.Value);
            if (!StringComparer.OrdinalIgnoreCase.Equals(extension, ".xlsx"))
            {
                var validationMessage = ValidationErrorMessage.Create("Expected a file with extension .xlsx but got " + extension).ForceValue();
                return Result<ErnstConfiguration>.Failure(new ValidationException(validationMessage));
            }

            Result<IReadOnlyList<UnvalidatedDataRow>> rawRowDataResult = ReadRawDataFromExcelSpreadSheet(tempFilePath);
            if (rawRowDataResult.HasError)
            {
                return Result<ErnstConfiguration>.Failure(rawRowDataResult.Error);
            }

            Result<IReadOnlyList<ErnstConfigurationEntry>> validatedRowResult = ValidateRawDataRows(rawRowDataResult.Value);
            if (validatedRowResult.HasError)
            {
                return Result<ErnstConfiguration>.Failure(validatedRowResult.Error);
            }

            var configuration = ErnstConfiguration.CreateNewConfiguration(validatedRowResult.Value, tempFilePath);
            return Result<ErnstConfiguration>.Success(configuration);
        }

        /// <summary>
        /// Reads raw values from an Excel SpreadSheet into object representing the relevant string data from each row.
        /// </summary>
        /// <param name="excelFilePath">The path to the excel file.</param>
        /// <returns>A result containing raw string data from each row or an error.</returns>
        private static Result<IReadOnlyList<UnvalidatedDataRow>> ReadRawDataFromExcelSpreadSheet(LocalFilePath excelFilePath)
        {
            // This should use a an adapter to ClosedXML, but there's no time and ClosedXML is pretty quick
            using (XLWorkbook workbook = new XLWorkbook(excelFilePath.Value, XLEventTracking.Disabled))
            using (IXLWorksheet worksheet = workbook.Worksheet(1))
            {
                Dictionary<string, int> columnsByName = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
                using (IXLRow headerRow = worksheet.FirstRow())
                {
                    foreach (IXLCell cell in headerRow.CellsUsed())
                    {
                        string value = cell.GetString();
                        if (!columnsByName.ContainsKey(value))
                        {
                            columnsByName.Add(value, cell.Address.ColumnNumber);
                        }
                    }
                }

                List<string> missingColumns = new List<string>();

                int pathColumnNumber = columnsByName.GetNullableValue(PathColumnName).GetValueOrDefault(); // it's useful that default(int) == 0 while Excel/ClosedXML uses a 1-based index
                if (pathColumnNumber == 0)
                {
                    missingColumns.Add(PathColumnName);
                }

                int stateColumnNumber = columnsByName.GetNullableValue(StateColumnName).GetValueOrDefault();
                if (stateColumnNumber == 0)
                {
                    missingColumns.Add(StateColumnName);
                }

                int descriptionColumnNumber = columnsByName.GetNullableValue(DescriptionColumnName).GetValueOrDefault();
                if (descriptionColumnNumber == 0)
                {
                    missingColumns.Add(DescriptionColumnName);
                }

                int defaultValueColumnNumber = columnsByName.GetNullableValue(DefaultValueColumnName).GetValueOrDefault();
                if (defaultValueColumnNumber == 0)
                {
                    missingColumns.Add(DefaultValueColumnName);
                }

                int predefinedFunctionColumnNumber = columnsByName.GetNullableValue(PredefinedFunctionColumnName).GetValueOrDefault();
                if (predefinedFunctionColumnNumber == 0)
                {
                    missingColumns.Add(PredefinedFunctionColumnName);
                }

                int loanTypeColumnNumber = columnsByName.GetNullableValue(LoanTypeColumnName).GetValueOrDefault();
                if (loanTypeColumnNumber == 0)
                {
                    missingColumns.Add(LoanTypeColumnName);
                }

                if (missingColumns.Count > 0)
                {
                    var validationMessage = ValidationErrorMessage.Create("One or more required columns was missing: " + string.Join(", ", missingColumns)).ForceValue();
                    return Result<IReadOnlyList<UnvalidatedDataRow>>.Failure(new ValidationException(validationMessage));
                }

                List<UnvalidatedDataRow> data = new List<UnvalidatedDataRow>();
                foreach (IXLRow row in worksheet.RowsUsed().Skip(1))
                {
                    data.Add(new UnvalidatedDataRow()
                    {
                        RowNumber = row.RowNumber(),
                        Path = row.Cell(pathColumnNumber).GetString(),
                        State = row.Cell(stateColumnNumber).GetString(),
                        Description = row.Cell(descriptionColumnNumber).GetString(),
                        DefaultValue = row.Cell(defaultValueColumnNumber).GetString(),
                        PredefinedFunction = row.Cell(predefinedFunctionColumnNumber).GetString(),
                        LoanType = row.Cell(loanTypeColumnNumber).GetString()
                    });
                }

                return Result<IReadOnlyList<UnvalidatedDataRow>>.Success(data);
            }
        }

        /// <summary>
        /// Validates the raw data for the specified rows into actual configuration entry data.
        /// </summary>
        /// <param name="rawRowData">The raw data for the rows of the spreadsheet.</param>
        /// <returns>A result containing validated configuration entries or an error.</returns>
        private static Result<IReadOnlyList<ErnstConfigurationEntry>> ValidateRawDataRows(IReadOnlyList<UnvalidatedDataRow> rawRowData)
        {
            List<ErnstConfigurationEntry> configurationEntries = new List<ErnstConfigurationEntry>(rawRowData.Count);
            var errors = new List<ValidationErrorMessage>();

            var entries = new HashSet<Tuple<SimpleXPath, UnitedStatesState?, E_sLT?, ErnstConfigurationPredefinedFunction>>();
            foreach (var rowData in rawRowData.Where(row => !string.IsNullOrWhiteSpace(row.Path)))
            {
                bool invalidData = false;
                var maybePath = SimpleXPath.Create(rowData.Path.Trim());
                if (!maybePath.HasValue)
                {
                    errors.Add(InvalidValueMessage(rowData.Path, PathColumnName, rowData.RowNumber));
                    invalidData = true;
                }

                var maybeState = UnitedStatesState.CreateWithValidation(rowData.State.Trim()); // state is optional
                if (!maybeState.HasValue && !string.IsNullOrWhiteSpace(rowData.State))
                {
                    errors.Add(InvalidValueMessage(rowData.State, StateColumnName, rowData.RowNumber));
                    invalidData = true;
                }

                var maybeDescription = ErnstConfigurationDescription.Create(rowData.Description.Trim());
                if (!maybeDescription.HasValue)
                {
                    errors.Add(InvalidValueMessage(rowData.Description, DescriptionColumnName, rowData.RowNumber));
                    invalidData = true;
                }

                var maybeLoanType = ParseLoanType(rowData.LoanType.Trim());
                if (!maybeLoanType.HasValue && !string.IsNullOrWhiteSpace(rowData.LoanType))
                {
                    errors.Add(InvalidValueMessage(rowData.LoanType, LoanTypeColumnName, rowData.RowNumber));
                    invalidData = true;
                }

                var maybeDefaultValue = ErnstConfigurationValue.Create(rowData.DefaultValue);
                if (!maybeDefaultValue.HasValue)
                {
                    errors.Add(InvalidValueMessage(rowData.DefaultValue, DefaultValueColumnName, rowData.RowNumber));
                    invalidData = true;
                }
                else if (maybePath.HasValue)
                {
                    var setter = new LendersOffice.Conversions.TitleFramework.ErnstRequestValueSetter(maybePath.Value, maybeDefaultValue.Value);
                    try
                    {
                        setter.SetValue(new Ernst.Request.Request());
                    }
                    catch (ValidationException exc)
                    {
                        errors.AddRange(exc.Context.ValidationErrors);
                        invalidData = true;
                    }
                }

                var maybePredefinedFunction = ErnstConfigurationPredefinedFunction.Create(rowData.PredefinedFunction.Trim());
                if (maybePredefinedFunction == null && !string.IsNullOrWhiteSpace(rowData.PredefinedFunction))
                {
                    errors.Add(InvalidValueMessage(rowData.PredefinedFunction, PredefinedFunctionColumnName, rowData.RowNumber));
                    invalidData = true;
                }
                else if (maybePredefinedFunction != null && maybeDefaultValue.HasValue)
                {
                    var functionResolver = new ErnstConfigurationPredefinedFunctionResolver();
                    if (!functionResolver.ValidateStringAgainstOperation(maybeDefaultValue.Value.Value, maybePredefinedFunction))
                    {
                        errors.Add(InvalidValueMessage(maybeDefaultValue.Value.Value, DefaultValueColumnName, rowData.RowNumber));
                        invalidData = true;
                    }
                }

                if (maybePath.HasValue && !entries.Add(new Tuple<SimpleXPath, UnitedStatesState?, E_sLT?, ErnstConfigurationPredefinedFunction>(maybePath.Value, maybeState, maybeLoanType, maybePredefinedFunction)))
                {
                    var stateError = maybeState.HasValue ? ", State " + rowData.State : null;
                    var predefinedFunctionError = maybePredefinedFunction != null ? ", PredefinedFunction " + rowData.PredefinedFunction : null;
                    var loanTypeError = maybeLoanType.HasValue ? ", LoanType " + rowData.LoanType : null;

                    errors.Add(ValidationErrorMessage.Create($"Duplicate entry for Path {rowData.Path}{stateError}{predefinedFunctionError}{loanTypeError} on row {rowData.RowNumber}").ForceValue());
                    invalidData = true;
                }

                if (!invalidData)
                {
                    configurationEntries.Add(new ErnstConfigurationEntry(maybePath.Value, maybeState, maybeDescription.Value, maybeDefaultValue.Value, maybeLoanType, maybePredefinedFunction));
                }
            }

            if (errors.Count > 0)
            {
                return Result<IReadOnlyList<ErnstConfigurationEntry>>.Failure(new ValidationException(new ValidationErrorContext(errors)));
            }

            return Result<IReadOnlyList<ErnstConfigurationEntry>>.Success(configurationEntries);
        }

        /// <summary>
        /// Parses the loan type as a string. 
        /// This function is for catching cases that Enum.Parse won't handle.
        /// </summary>
        /// <param name="loanTypeString">The string to parse.</param>
        /// <returns>The loan type if valid. Null otherwise.</returns>
        private static E_sLT? ParseLoanType(string loanTypeString)
        {
            if (string.IsNullOrEmpty(loanTypeString))
            {
                return null;
            }

            E_sLT loanType;
            if (!Enum.TryParse(loanTypeString, ignoreCase: true, result: out loanType) && !OtherValidLoanTypeStrings.TryGetValue(loanTypeString, out loanType))
            {
                return null;
            }

            return loanType;
        }

        /// <summary>
        /// Creates a validation error message indicating that the specified value was not valid.
        /// </summary>
        /// <param name="value">The value in the document.</param>
        /// <param name="columnName">The name of the column in the document.</param>
        /// <param name="rowNumber">The index (1-based) of the row in the document.</param>
        /// <returns>A validation error message for the user.</returns>
        private static ValidationErrorMessage InvalidValueMessage(string value, string columnName, int rowNumber)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return ValidationErrorMessage.Create($"{columnName} in row {rowNumber} cannot be blank").ForceValue();
            }

            return ValidationErrorMessage.Create($"Invalid value \"{value}\" for {columnName} in row {rowNumber}").ForceValue();
        }

        /// <summary>
        /// A simple data class to allow us to validate the data after closing the Excel workbook.
        /// </summary>
        /// <remarks>
        /// This object is a mutable state bag using non-semantic types.  It shouldn't be made public.
        /// </remarks>
        private class UnvalidatedDataRow
        {
            /// <summary>
            /// Gets or sets the row number of the worksheet.
            /// </summary>
            public int RowNumber { get; set; }

            /// <summary>
            /// Gets or sets the value of the path column.
            /// </summary>
            public string Path { get; set; }

            /// <summary>
            /// Gets or sets the value of the state column.
            /// </summary>
            public string State { get; set; }

            /// <summary>
            /// Gets or sets the value of the description column.
            /// </summary>
            public string Description { get; set; }

            /// <summary>
            /// Gets or sets the value of the default column.
            /// </summary>
            public string DefaultValue { get; set; }

            /// <summary>
            /// Gets or sets the value of the predefined function column.
            /// </summary>
            public string PredefinedFunction { get; set; }

            /// <summary>
            /// Gets or sets the value of the loan type column.
            /// </summary>
            public string LoanType { get; set; }
        }
    }
}
