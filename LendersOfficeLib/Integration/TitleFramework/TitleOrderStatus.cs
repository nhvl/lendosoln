﻿namespace LendersOffice.Integration.TitleFramework
{
    using System.ComponentModel;

    /// <summary>
    /// Whether the quotes from the title order have been aplied.
    /// </summary>
    public enum TitleOrderStatus
    {
        /// <summary>
        /// Quote not applied.
        /// </summary>
        [Description("Quote not applied.")]
        QuoteNotApplied = 0,

        /// <summary>
        /// Quote applied.
        /// </summary>
        [Description("Quote applied.")]
        QuoteApplied = 1
    }
}
