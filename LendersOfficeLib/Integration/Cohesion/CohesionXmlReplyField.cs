﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LendersOffice.Integration.Cohesion
{
    public class CohesionXmlReplyField
    {
        public string FieldNbr { get; private set; }
        public string FieldData { get; private set; }

        /// <summary>
        /// Represent a FIELD element return in MiserXMLReply element.
        /// </summary>
        /// <param name="el">
        /// Sample format
        /// &lt;FIELD&gt;
        ///     &lt;FieldNbr&gt;xxxxx&lt;/FieldNbr&gt;
        ///     &lt;FieldData&gt;Text Here&lt;/FieldData&gt;
        /// &lt;/FIELD&gt;
        /// </param>
        public CohesionXmlReplyField(XElement el)
        {
            if (el == null)
            {
                return;
            }

            XElement fieldNbr = el.Element("FieldNbr");
            if (fieldNbr != null)
            {
                FieldNbr = fieldNbr.Value;
            }

            XElement fieldData = el.Element("FieldData");
            if (fieldData != null)
            {
                FieldData = fieldData.Value;
            }
        }
        public override string ToString()
        {
            return "[" + FieldNbr + "] - " + FieldData;
        }
    }
}
