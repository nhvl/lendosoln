﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;

namespace LendersOffice.Integration.Cohesion
{
    public class CohesionXmlReply
    {
        public string Mode { get; private set; }
        public string Status { get; private set; }
        public string StatusDescription
        {
            get
            {
                // 5/9/2013 dd - Base from MISER documentation.
                switch (Status)
                {
                    case "A": return "Input accepted";
                    case "B": return "Last transaction aborted system";
                    case "C": return "Continuation";
                    case "L": return "Long transaction";
                    case "M": return "Message format error";
                    case "P": return "Re--send to process";
                    case "R": return "Rejected";
                    case "S": return "Supervisor override required";
                    case "T": return "Teller override required";
                    default:
                        return Status;
                }
            }
        }
        public string RawErrorMessage { get; private set; }
        public IEnumerable<CohesionXmlReplyField> FieldList { get; private set; }

        internal XElement Element { get; private set; }

        internal CohesionXmlReply(string xml)
        {
            try
            {
                XElement el = XElement.Parse(xml);

                this.Element = el;

                ParseHeaderData(el.Element("HeaderData"));


                List<CohesionXmlReplyField> fieldList = new List<CohesionXmlReplyField>();

                this.FieldList = fieldList;

                foreach (XElement fieldEl in el.Descendants("FIELD"))
                {
                    fieldList.Add(new CohesionXmlReplyField(fieldEl));
                }
            }
            catch (XmlException)
            {
                RawErrorMessage = xml;
            }

        }

        private void ParseHeaderData(XElement headerDataElement)
        {
            if (headerDataElement == null)
            {
                return;
            }

            XElement modeElement = headerDataElement.Element("Mode");
            XElement statusElement = headerDataElement.Element("Status");

            if (modeElement != null)
            {
                this.Mode = modeElement.Value;
            }

            if (statusElement != null)
            {
                this.Status = statusElement.Value;
            }


        }


    }
}
