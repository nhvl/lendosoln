﻿// <copyright file="CohesionRequest.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is CohesionRequest class.</summary>
namespace LendersOffice.Integration.Cohesion
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.XsltExportReport;
    using Security;

    /// <summary>
    /// Class represent a list of transactions to send to Cohesion Server.
    /// </summary>
    public class CohesionRequest
    {
        /// <summary>
        /// Internal representation of the XML request.
        /// </summary>
        private XDocument xdoc = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="CohesionRequest" /> class.
        /// </summary>
        /// <param name="userId">Id of the user.</param>
        /// <param name="loanId">Id of the loan.</param>
        /// <param name="mapName">XSLT map name.</param>
        /// <param name="principal">User principal.</param>
        public CohesionRequest(Guid userId, Guid loanId, string mapName, AbstractUserPrincipal principal)
        {
            XsltExportRequest request = new XsltExportRequest(userId, new Guid[] { loanId }, mapName, null, null);

            XsltExportWorker worker = new XsltExportWorker(request, false, principal);

            XsltExportResult result = worker.Execute();

            if (result.HasError)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var o in result.ErrorList)
                {
                    sb.AppendLine(o);
                }

                throw CBaseException.GenericException(sb.ToString());
            }

            this.Url = request.MapItem.ExportUrl;

            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();

            string transXmlText = TextFileHelper.ReadFile(result.OutputFileLocation);

            // 5/2/2013 dd - Could not figure out how to get rid of this from Xsl result.
            transXmlText = transXmlText.Replace(@" xmlns=""http://www.w3.org/1999/xhtml""", string.Empty);

            this.xdoc = XDocument.Parse(transXmlText);

            foreach (XElement el in this.xdoc.Root.Elements("MiserXMLInput"))
            {
                string applicationCode = el.Element("HeaderData").Element("ApplicationCode").Value;
                string transactionCode = el.Element("HeaderData").Element("TransactionCode").Value;

                string key = applicationCode + transactionCode;
                string value = el.ToString(SaveOptions.None);

                list.Add(new KeyValuePair<string, string>(key, value));
            }

            this.TransactionList = list;
        }

        /// <summary>
        /// Gets the URL to submit request to Cohesion Server.
        /// </summary>
        /// <value>The URL to submit request to Cohesion Server.</value>
        public string Url { get; private set; }

        /// <summary>
        /// Gets a list of transactions to send to Cohesion server.
        /// </summary>
        /// <value>A list of transactions to send to Cohesion server.</value>
        public IEnumerable<KeyValuePair<string, string>> TransactionList { get; private set; }

        /// <summary>
        /// Generate the full list of transactions in XML for debugging purpose.
        /// </summary>
        /// <returns>The full list of transactions in XML for debugging purpose.</returns>
        public string ToXmlString()
        {
            return this.xdoc.ToString(SaveOptions.None);
        }
    }
}
