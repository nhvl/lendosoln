﻿// <copyright file="CohesionResponse.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is CohesionResponse class.</summary>
namespace LendersOffice.Integration.Cohesion
{
    using System.Collections.Generic;
    using System.Xml.Linq;

    /// <summary>
    /// A class represents response from Cohesion server.
    /// </summary>
    public class CohesionResponse
    {
        /// <summary>
        /// List of Cohesion server reply.
        /// </summary>
        private List<KeyValuePair<string, CohesionXmlReply>> replyList = new List<KeyValuePair<string, CohesionXmlReply>>();

        /// <summary>
        /// Gets a list of Cohesion server reply.
        /// </summary>
        /// <value>A list of Cohesion server reply.</value>
        public IEnumerable<KeyValuePair<string, CohesionXmlReply>> ReplyList
        {
            get { return this.replyList; }
        }

        /// <summary>
        /// Add reply return from Cohesion server.
        /// </summary>
        /// <param name="transactionKey">Cohesion transaction key.</param>
        /// <param name="reply">Xml reply.</param>
        public void Add(string transactionKey, string reply)
        {
            this.replyList.Add(new KeyValuePair<string, CohesionXmlReply>(transactionKey, new CohesionXmlReply(reply)));
        }
    }
}