	<xsl:stylesheet xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:lqb="urn:lqb-xslt" version="1.0">
	<xsl:output indent="yes" method="html" />

	<xsl:template match="MiserTransactionList">

	<html>
		<head>
			<style>
			caption
			{
			font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
			color:#333333;
			font-size:11px;
			font-weight:bold;
			}
			th
			{
			font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
			color:#333333;
			font-size:11px;
			font-weight:bold;		
			border-left: 1px solid #dddddd;
			border-bottom: 1px solid #dddddd;			
			}
			table
			{
				border: 1px solid #dddddd;
				border-collapse:separate;
				
			}
			td 
			{
			font-family:"Helvetica Neue", Helvetica, Arial, sans-serif;
			color:#333333;
			font-size:11px;
			border-left: 1px solid #dddddd;
			border-bottom: 1px solid #dddddd;
			padding:3px;
			}
			</style>
		</head>
		<body>
			<xsl:apply-templates select="MiserXMLInput"/>
		</body>
	</html>
	</xsl:template>
	<xsl:template match="MiserXMLInput">
		<table >
			<caption>Transaction #<xsl:value-of select="position()"/> - <xsl:value-of select="HeaderData/ApplicationCode"/>-<xsl:value-of select="HeaderData/TransactionCode"/>
			<xsl:if test="HeaderData/SupervisorOverride"><br/>SupervisorOverride:<xsl:value-of select="HeaderData/SupervisorOverride"/></xsl:if>
			<xsl:if test="HeaderData/FundType"><br/>FundType:<xsl:value-of select="HeaderData/FundType"/></xsl:if>
			<xsl:if test="HeaderData/ReasonCode"><br/>ReasonCode:<xsl:value-of select="HeaderData/ReasonCode"/></xsl:if>
			</caption>
		<thead>
		<tr>
			<th>Idx #</th>
			<th>FieldNbr</th>
			<th style="width:200px">FieldName</th>
			<th style="width:300px">FieldDesc</th>
			<th>FieldData</th>
		</tr>
		</thead>
		<xsl:apply-templates select="InputField"/>
		</table>
		<hr/>
	</xsl:template>
	<xsl:template match="InputField">
		<tr>
			<td><xsl:value-of select="position()"/></td>
			<td><xsl:value-of select="FieldNbr"/></td>
			<td><xsl:value-of select="FieldName"/></td>
			<td><xsl:value-of select="FieldDesc"/></td>
			<td><xsl:value-of select="FieldData"/></td>
		</tr>
	</xsl:template>
	</xsl:stylesheet>