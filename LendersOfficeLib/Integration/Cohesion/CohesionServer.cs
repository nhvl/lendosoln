﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Xml.Linq;

namespace LendersOffice.Integration.Cohesion
{
    /// <summary>
    /// Cohesion / Miser Integration.
    /// </summary>
    public static class CohesionServer
    {

        public static CohesionResponse Submit(CohesionRequest request)
        {
            StringBuilder debugString = new StringBuilder();

            bool hasError = false;
            try
            {
                CohesionResponse response = new CohesionResponse();

                CohesionConnect server = new CohesionConnect();

                server.Url = request.Url;

                debugString.Append("[CohesionConnect] " + server.Url);

                foreach (var trans in request.TransactionList)
                {
                    string replyString = SubmitTransaction(server, trans.Value, debugString);
                    response.Add(trans.Key, replyString);
                }
                return response;
            }
            catch (Exception exc)
            {
                debugString.AppendLine();
                debugString.AppendLine();
                debugString.AppendLine("ERROR:");
                debugString.AppendLine(exc.ToString());
                hasError = true;
                throw;
            }
            finally
            {
                if (hasError)
                {
                    Tools.LogError(debugString.ToString());
                }
                else
                {
                    Tools.LogInfo(debugString.ToString());
                }
            }
        }

        private static string SubmitTransaction(CohesionConnect server, string transXml, StringBuilder debugString)
        {
            debugString.AppendLine();
            debugString.AppendLine("Request:");
            debugString.AppendLine();

            debugString.AppendLine("[" + transXml + "]");
            debugString.AppendLine();

            string sReply = server.GetHostReply(transXml);
            debugString.AppendLine();
            debugString.AppendLine("Response:");
            debugString.AppendLine("[" + sReply + "]");
            //FormatXml(debugString, sReply);

            return sReply;

        }
        private static void FormatXml(StringBuilder sb, string xml)
        {
            try
            {
                if (string.IsNullOrEmpty(xml))
                {
                    return;
                }

                if (xml.Length > 100000)
                {
                    // 5/2/2013 dd - Avoiding using too much memory on large string.
                    sb.AppendLine(xml);
                    return;
                }

                XDocument xdoc = XDocument.Parse(xml);
                sb.AppendLine(xdoc.ToString(SaveOptions.None));
            }
            catch (FormatException)
            {
                sb.AppendLine(xml);
            }
        }
    }
}
