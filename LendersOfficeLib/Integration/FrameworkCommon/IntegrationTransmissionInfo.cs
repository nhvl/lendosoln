﻿namespace LendersOffice.Integration.FrameworkCommon
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using Common;
    using Constants;
    using DataAccess;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using VOXFramework;

    /// <summary>
    /// Transmission Info class for all integrations.
    /// </summary>
    public abstract class IntegrationTransmissionInfo
    {
        /// <summary>
        /// Gets or sets the id of the response certificate.
        /// </summary>
        /// <value>The id of the response certificate.</value>
        private Guid? responseCertificateId;

        /// <summary>
        /// Gets or sets the request credential login.
        /// </summary>
        /// <value>The request credential login.</value>
        private string requestCredentialName;

        /// <summary>
        /// The lazy decrypted password for the request credentials.
        /// </summary>
        private Lazy<string> lazyRequestPassword;

        /// <summary>
        /// Gets or sets the response credential login.
        /// </summary>
        /// <value>The response credential login.</value>
        private string responseCredentialName;

        /// <summary>
        /// The lazy decrypted password for the response credentials.
        /// </summary>
        private Lazy<string> lazyResponsePassword;

        /// <summary>
        /// The encryption key used to encrypt members of this class.
        /// </summary>
        private EncryptionKeyIdentifier encryptionKeyId;

        /// <summary>
        /// Gets or sets an enum indicating what type of object this transmission info is associated with.
        /// </summary>
        /// <value>Whether this transmission info is associated with a platform or vendor.</value>
        public TransmissionAssociationT TransmissionAssociationT { get; protected set; }

        /// <summary>
        /// Gets or sets the target URL.
        /// </summary>
        /// <value>The target URL.</value>
        public LqbAbsoluteUri TargetUrl { get; set; }

        /// <summary>
        /// Gets or sets the authentication method.
        /// </summary>
        /// <value>The authentication method.</value>
        public TransmissionAuthenticationT TransmissionAuthenticationT { get; set; }

        /// <summary>
        /// Gets or sets the response certificate id.
        /// </summary>
        /// <value>The response certificate id.</value>
        public Guid? ResponseCertificateId
        {
            get
            {
                if (this.TransmissionAuthenticationT != TransmissionAuthenticationT.DigitalCert)
                {
                    return null;
                }

                return this.responseCertificateId;
            }

            set
            {
                this.responseCertificateId = value;
            }
        }

        /// <summary>
        /// Gets or sets the request credential name.
        /// </summary>
        /// <value>The request credential name.</value>
        public string RequestCredentialName
        {
            get
            {
                if (this.TransmissionAuthenticationT != TransmissionAuthenticationT.BasicAuthentication)
                {
                    return null;
                }

                return this.requestCredentialName;
            }

            set
            {
                this.requestCredentialName = value;
            }
        }

        /// <summary>
        /// Gets or sets the request credential password.
        /// </summary>
        /// <value>The request credential password.</value>
        public Sensitive<string>? RequestCredentialPassword
        {
            get
            {
                if (this.TransmissionAuthenticationT != TransmissionAuthenticationT.BasicAuthentication)
                {
                    return null;
                }

                return this.lazyRequestPassword?.Value;
            }

            set
            {
                this.lazyRequestPassword = new Lazy<string>(() => value?.Value);
            }
        }

        /// <summary>
        /// Gets or sets the response credential name.
        /// </summary>
        /// <value>The response credential name.</value>
        public string ResponseCredentialName
        {
            get
            {
                if (this.TransmissionAuthenticationT != TransmissionAuthenticationT.BasicAuthentication)
                {
                    return null;
                }

                return this.responseCredentialName;
            }

            set
            {
                this.responseCredentialName = value;
            }
        }

        /// <summary>
        /// Gets or sets the response credential password.
        /// </summary>
        /// <value>The response credential password.</value>
        public Sensitive<string>? ResponseCredentialPassword
        {
            get
            {
                if (this.TransmissionAuthenticationT != TransmissionAuthenticationT.BasicAuthentication)
                {
                    return null;
                }

                return this.lazyResponsePassword?.Value;
            }

            set
            {
                this.lazyResponsePassword = new Lazy<string>(() => value?.Value);
            }
        }

        /// <summary>
        /// Gets the SP used to create a transmission info.
        /// </summary>
        /// <value>The SP used to create a transmission info.</value>
        protected abstract string CreateSp { get; }

        /// <summary>
        /// Gets the SP used to update a transmission info.
        /// </summary>
        /// <value>The SP used to update the transmission info.</value>
        protected abstract string UpdateSp { get; }

        /// <summary>
        /// Populates an existing transmission from a view model.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="errors">Any errors.</param>
        /// <returns>True if populated. False otherwise.</returns>
        internal bool PopulateFromViewModel(IntegrationTransmissionInfoViewModel viewModel, out string errors)
        {
            if (!viewModel.Verify(out errors))
            {
                return false;
            }

            this.RequestCredentialName = viewModel.RequestCredentialName;
            this.ResponseCredentialName = viewModel.ResponseCredentialName;
            this.TargetUrl = LqbAbsoluteUri.Create(viewModel.TargetUrl).Value;
            this.TransmissionAuthenticationT = viewModel.TransmissionAuthenticationT;
            Guid respCertId;
            if (Guid.TryParse(viewModel.ResponseCertificateId, out respCertId))
            {
                this.ResponseCertificateId = respCertId;
            }

            if (this.TransmissionAuthenticationT == TransmissionAuthenticationT.BasicAuthentication)
            {
                if (viewModel.RetrieveRequestPassword() != ConstAppDavid.FakePasswordDisplay)
                {
                    this.RequestCredentialPassword = viewModel.RetrieveRequestPassword();
                }

                if (viewModel.RetrieveResponsePassword() != ConstAppDavid.FakePasswordDisplay)
                {
                    this.ResponseCredentialPassword = viewModel.RetrieveResponsePassword();
                }
            }

            return true;
        }

        /// <summary>
        /// Populates a new transmission for a platform from a view model.  Note: vendor transmissions
        /// will always be copied, rather than created as a new object.
        /// </summary>
        /// <param name="viewModel">The view model.</param>
        /// <param name="errors">Any errors.</param>
        /// <returns>True if populated. False otherwise.</returns>
        internal bool PopulateNewPlatformTransmissionFromViewModel(IntegrationTransmissionInfoViewModel viewModel, out string errors)
        {
            this.TransmissionAssociationT = TransmissionAssociationT.Platform;
            if (!this.PopulateFromViewModel(viewModel, out errors))
            {
                return false;
            }

            if (this.TransmissionAuthenticationT == TransmissionAuthenticationT.BasicAuthentication)
            {
                if (viewModel.RetrieveRequestPassword() == ConstAppDavid.FakePasswordDisplay)
                {
                    errors = "Invalid request password";
                    return false;
                }
                else if (viewModel.RetrieveResponsePassword() == ConstAppDavid.FakePasswordDisplay)
                {
                    errors = "Invalid response password";
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Constructs the sql parameters for this transmission info to prepare for saving.
        /// </summary>
        /// <param name="isNew">Whether this is a new transmission.</param>
        /// <returns>The sql parameters to pass into the stored procedure.</returns>
        internal virtual IEnumerable<SqlParameter> ConstructSqlParameters(bool isNew)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@TargetUrl", this.TargetUrl.ToString() ?? string.Empty),
                new SqlParameter("@TransmissionAuthenticationT", this.TransmissionAuthenticationT),
                new SqlParameter("@TransmissionAssociationT", this.TransmissionAssociationT)
            };

            if (this.encryptionKeyId == default(EncryptionKeyIdentifier))
            {
                this.encryptionKeyId = Drivers.Encryption.EncryptionHelper.GenerateNewKey();
            }

            parameters.Add(new SqlParameter("@EncryptionKeyId", this.encryptionKeyId.Value));
            if (this.TransmissionAuthenticationT == TransmissionAuthenticationT.BasicAuthentication)
            {
                parameters.Add(new SqlParameter("@RequestCredentialName", this.RequestCredentialName));
                parameters.Add(new SqlParameter("@ResponseCredentialName", this.ResponseCredentialName));
                parameters.Add(new SqlParameter("@RequestCredentialPassword", Drivers.Encryption.EncryptionHelper.EncryptString(this.encryptionKeyId, this.RequestCredentialPassword?.Value)));
                parameters.Add(new SqlParameter("@ResponseCredentialPassword", Drivers.Encryption.EncryptionHelper.EncryptString(this.encryptionKeyId, this.ResponseCredentialPassword?.Value)));
            }
            else if (this.TransmissionAuthenticationT == TransmissionAuthenticationT.DigitalCert)
            {
                parameters.Add(new SqlParameter("@ResponseCertId", this.ResponseCertificateId?.ToString()));
            }

            return parameters;
        }

        /// <summary>
        /// Copies the properties of the current instance to <paramref name="target"/>.
        /// </summary>
        /// <param name="target">The instance to receive the copied property data.</param>
        /// <param name="transmissionAssociation">The transmission association for the copy.</param>
        internal virtual void CopyTo(IntegrationTransmissionInfo target, TransmissionAssociationT transmissionAssociation)
        {
            target.responseCertificateId = this.responseCertificateId;
            target.requestCredentialName = this.requestCredentialName;
            target.lazyRequestPassword = this.lazyRequestPassword;
            target.responseCredentialName = this.responseCredentialName;
            target.lazyResponsePassword = this.lazyResponsePassword;
            target.TransmissionAssociationT = transmissionAssociation;
            target.TargetUrl = this.TargetUrl;
            target.TransmissionAuthenticationT = this.TransmissionAuthenticationT;
            ////target.encryptionKeyId = this.encryptionKeyId; // Encryption keys should be unique per record, so they do not get copied
        }

        /// <summary>
        /// Constructs a view model out of this transmission info object.
        /// </summary>
        /// <returns>The view model.</returns>
        internal IntegrationTransmissionInfoViewModel ToViewModel()
        {
            IntegrationTransmissionInfoViewModel viewModel = new IntegrationTransmissionInfoViewModel();
            viewModel.TargetUrl = this.TargetUrl.ToString();
            viewModel.TransmissionAuthenticationT = this.TransmissionAuthenticationT;
            viewModel.ResponseCertificateId = this.ResponseCertificateId?.ToString() ?? string.Empty;
            viewModel.RequestCredentialName = this.RequestCredentialName ?? string.Empty;
            viewModel.RequestCredentialPassword = this.RequestCredentialPassword?.Value ?? string.Empty;
            viewModel.ResponseCredentialName = this.ResponseCredentialName ?? string.Empty;
            viewModel.ResponseCredentialPassword = this.ResponseCredentialPassword?.Value ?? string.Empty;

            return viewModel;
        }

        /// <summary>
        /// Saves the transmission info.
        /// </summary>
        /// <param name="transmissionId">The transmission info id. These are held by the parent object.</param>
        /// <param name="exec">The transaction.</param>
        /// <returns>The transmission id.</returns>
        internal int Save(int? transmissionId, CStoredProcedureExec exec)
        {
            var isNew = !transmissionId.HasValue;
            List<SqlParameter> parameters = new List<SqlParameter>();
            int finalTransmissionId = 0;
            SqlParameter transmissionIdOutput = null;
            if (!isNew)
            {
                parameters.Add(new SqlParameter("@TransmissionInfoId", transmissionId.Value));
                finalTransmissionId = transmissionId.Value;
            }
            else
            {
                transmissionIdOutput = new SqlParameter("@TransmissionInfoId", SqlDbType.Int);
                transmissionIdOutput.Direction = ParameterDirection.Output;
                parameters.Add(transmissionIdOutput);
            }

            parameters.AddRange(this.ConstructSqlParameters(isNew));

            var sp = isNew ? this.CreateSp : this.UpdateSp;
            if (!StoredProcedureName.Create(sp).HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {sp}."));
            }

            exec.ExecuteNonQuery(sp, 2, parameters.ToArray());

            if (isNew)
            {
                finalTransmissionId = (int)transmissionIdOutput.Value;
            }

            return finalTransmissionId;
        }

        /// <summary>
        /// Loads data from the reader.
        /// </summary>
        /// <param name="reader">The reader to load from.</param>
        protected virtual void LoadFromReader(IDataReader reader)
        {
            var certId = reader["ResponseCertId"];
            var reqCredName = reader["RequestCredentialName"];
            var reqPass = reader["RequestCredentialPassword"];
            var resCredName = reader["ResponseCredentialName"];
            var resPass = reader["ResponseCredentialPassword"];

            this.TransmissionAssociationT = (TransmissionAssociationT)reader["TransmissionAssociationT"];
            this.TransmissionAuthenticationT = (TransmissionAuthenticationT)reader["TransmissionAuthenticationT"];
            this.TargetUrl = LqbAbsoluteUri.Create(reader["TargetUrl"].ToString()) ?? LqbAbsoluteUri.BadURI;

            EncryptionKeyIdentifier databaseEncryptionKeyId = EncryptionKeyIdentifier.Create((Guid)reader["EncryptionKeyId"]).ForceValue();
            this.encryptionKeyId = databaseEncryptionKeyId;
            byte[] requestPasswordBytes = reqPass == DBNull.Value ? null : (byte[])reqPass;
            byte[] responsePasswordBytes = resPass == DBNull.Value ? null : (byte[])resPass;

            this.ResponseCertificateId = certId == DBNull.Value ? (Guid?)null : (Guid)certId;
            this.RequestCredentialName = reqCredName == DBNull.Value ? null : reqCredName.ToString();
            this.lazyRequestPassword = new Lazy<string>(() => Drivers.Encryption.EncryptionHelper.DecryptString(databaseEncryptionKeyId, requestPasswordBytes));
            this.ResponseCredentialName = resCredName == DBNull.Value ? null : resCredName.ToString();
            this.lazyResponsePassword = new Lazy<string>(() => Drivers.Encryption.EncryptionHelper.DecryptString(databaseEncryptionKeyId, responsePasswordBytes));
        }
    }
}
