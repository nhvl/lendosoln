﻿namespace LendersOffice.Integration.FrameworkCommon
{
    using System;
    using System.Text;
    using LendersOffice.Constants;
    using LendersOffice.Integration.VOXFramework;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Base view model for the integration transmission model class.
    /// </summary>
    public class IntegrationTransmissionInfoViewModel
    {
        /// <summary>
        /// The request password.
        /// </summary>
        private string requestPassword;

        /// <summary>
        /// The response password.
        /// </summary>
        private string responsePassword;

        /// <summary>
        /// Gets or sets the target url.
        /// </summary>
        /// <value>The target url.</value>
        public string TargetUrl
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the transmission authentication type.
        /// </summary>
        /// <value>The transmission authentication type.</value>
        public TransmissionAuthenticationT TransmissionAuthenticationT
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the response certificate id.
        /// </summary>
        /// <value>The response certificate id.</value>
        public string ResponseCertificateId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the request credential login.
        /// </summary>
        /// <value>The requet credential login.</value>
        public string RequestCredentialName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the request credential password.
        /// </summary>
        /// <value>The request credential password.</value>
        public string RequestCredentialPassword
        {
            get
            {
                if (string.IsNullOrEmpty(this.requestPassword))
                {
                    return string.Empty;
                }

                return ConstAppDavid.FakePasswordDisplay;
            }

            set
            {
                this.requestPassword = value;
            }
        }

        /// <summary>
        /// Gets or sets the response credential login.
        /// </summary>
        /// <value>The response credential login.</value>
        public string ResponseCredentialName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the response credential password.
        /// </summary>
        /// <value>The response credential password.</value>
        public string ResponseCredentialPassword
        {
            get
            {
                if (string.IsNullOrEmpty(this.responsePassword))
                {
                    return string.Empty;
                }

                return ConstAppDavid.FakePasswordDisplay;
            }

            set
            {
                this.responsePassword = value;
            }
        }

        /// <summary>
        /// Gets the raw request password.
        /// </summary>
        /// <returns>The raw request password.</returns>
        public string RetrieveRequestPassword()
        {
            return this.requestPassword;
        }

        /// <summary>
        /// Gets the raw response password.
        /// </summary>
        /// <returns>The raw response password.</returns>
        public string RetrieveResponsePassword()
        {
            return this.responsePassword;
        }

        /// <summary>
        /// Verify view model.
        /// </summary>
        /// <param name="errors">Any errors found during verification.</param>
        /// <returns>True if verified, false otherwise.</returns>
        public virtual bool Verify(out string errors)
        {
            string invalidError = "Invalid value for field {0}: {1}";
            StringBuilder errorMessages = new StringBuilder();
            LqbAbsoluteUri? uri = LqbAbsoluteUri.Create(this.TargetUrl);
            if (!uri.HasValue || uri.Value.GetScheme() != Uri.UriSchemeHttps)
            {
                errorMessages.AppendLine(string.Format(invalidError, "Target Url", this.TargetUrl));
            }

            if (this.TransmissionAuthenticationT == TransmissionAuthenticationT.DigitalCert)
            {
                Guid respCertId;
                if (!string.IsNullOrWhiteSpace(this.ResponseCertificateId) && !Guid.TryParse(this.ResponseCertificateId, out respCertId))
                {
                    errorMessages.AppendLine(string.Format(invalidError, "Response Cert Id", this.ResponseCertificateId));
                }
            }

            if (this.TransmissionAuthenticationT == TransmissionAuthenticationT.BasicAuthentication)
            {
                if (!string.IsNullOrEmpty(this.RequestCredentialName) && string.IsNullOrEmpty(this.RetrieveRequestPassword()))
                {
                    errorMessages.AppendLine("Empty Request Password.");
                }

                if (!string.IsNullOrEmpty(this.ResponseCredentialName) && string.IsNullOrEmpty(this.RetrieveResponsePassword()))
                {
                    errorMessages.AppendLine("Empty Response Password.");
                }
            }

            errors = errorMessages.ToString();
            return errorMessages.Length == 0;
        }
    }
}
