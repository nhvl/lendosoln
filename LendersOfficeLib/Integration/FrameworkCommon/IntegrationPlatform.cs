﻿namespace LendersOffice.Integration.FrameworkCommon
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using DataAccess;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Platform class for all integration frameworks.
    /// </summary>
    /// <typeparam name="TTransmissionInfo">The transmission info linked to this platform.</typeparam>
    public abstract class IntegrationPlatform<TTransmissionInfo> where TTransmissionInfo : IntegrationTransmissionInfo
    {
        /// <summary>
        /// Whether this platform requires an account ID.
        /// </summary>
        private bool requiresAccountId;

        /// <summary>
        /// Gets or sets a value indicating whether this platform is new.
        /// </summary>
        /// <value>Whether this platform is new.</value>
        public bool IsNew
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets a unique id for this platform.
        /// </summary>
        /// <value>The unique id for this platform.</value>
        public int PlatformId
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets the name of the platform.
        /// </summary>
        /// <value>The name of the platform.</value>
        public string PlatformName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether an account id is used.
        /// </summary>
        /// <value>Whether an account id is used.</value>
        public bool UsesAccountId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether an account id is required.
        /// </summary>
        /// <value>Whether an account id is required.</value>
        public bool RequiresAccountId
        {
            get
            {
                if (!this.UsesAccountId)
                {
                    return false;
                }

                return this.requiresAccountId;
            }

            set
            {
                this.requiresAccountId = value;
            }
        }

        /// <summary>
        /// Gets or sets the transmission info associated with this platform.
        /// </summary>
        /// <value>The transmission info associated with this platform.</value>
        public TTransmissionInfo TransmissionInfo
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets the id of the transmission linked to this platform.
        /// </summary>
        /// <value>The id of the transmission linked to this platform.</value>
        public int LinkedTransmissionId
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the stored procedure used for creating a new platform. Note that this stored procedure also saves the transmission.
        /// </summary>
        /// <value>The stored procedure used for creating a new platform.</value>
        protected abstract string CreatePlatformSP
        {
            get;
        }

        /// <summary>
        /// Gets the stored procedure used for updating a new platform. Note that this stored procedure also saves the transmission.
        /// </summary>
        /// <value>The stored procedure used for updating a new platform.</value>
        protected abstract string UpdatePlatformSP
        {
            get;
        }

        /// <summary>
        /// Saves this platform.
        /// </summary>
        public void SavePlatform()
        {
            string spName = this.IsNew ? this.CreatePlatformSP : this.UpdatePlatformSP;
            StoredProcedureName? procedureName = StoredProcedureName.Create(spName);
            if (!procedureName.HasValue)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid stored procedure name {spName}."));
            }

            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@PlatformName", this.PlatformName),
                new SqlParameter("@UsesAccountId", this.UsesAccountId),
                new SqlParameter("@RequiresAccountId", this.RequiresAccountId)
            };

            parameters.AddRange(this.GetAdditionalPlatformSaveParameters());

            SqlParameter platformIdOutput = null;
            SqlParameter transmissionIdOutput = null;
            if (!this.IsNew)
            {
                parameters.Add(new SqlParameter("@PlatformId", this.PlatformId));
                parameters.Add(new SqlParameter("@TransmissionId", this.LinkedTransmissionId));
            }
            else
            {
                platformIdOutput = new SqlParameter("@PlatformId", System.Data.SqlDbType.Int);
                platformIdOutput.Direction = System.Data.ParameterDirection.Output;
                parameters.Add(platformIdOutput);

                transmissionIdOutput = new SqlParameter("@TransmissionId", System.Data.SqlDbType.Int);
                transmissionIdOutput.Direction = System.Data.ParameterDirection.Output;
                parameters.Add(transmissionIdOutput);
            }

            parameters.AddRange(this.TransmissionInfo.ConstructSqlParameters(this.IsNew));

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);
            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShareROnly))
            {
                driver.ExecuteNonQuery(connection, null, procedureName.Value, parameters);
            }

            if (this.IsNew)
            {
                this.PlatformId = (int)platformIdOutput.Value;
                this.LinkedTransmissionId = (int)transmissionIdOutput.Value;
            }

            // Platforms aren't new anymore once they've been saved to the DB.
            this.IsNew = false;
        }

        /// <summary>
        /// Loads data using the stored procedure and creates it using the passed in loader.
        /// </summary>
        /// <param name="platformId">The platform to load.</param>
        /// <param name="procedureName">The stored procedure.</param>
        /// <param name="constructor">Constructor for the platform.</param>
        /// <returns>The loaded platform.</returns>
        protected static IntegrationPlatform<TTransmissionInfo> LoadPlatform(int platformId, StoredProcedureName procedureName, Func<IntegrationPlatform<TTransmissionInfo>> constructor)
        {
            if (platformId < 1)
            {
                return null;
            }

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@PlatformId", platformId)
            };

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);
            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShareROnly))
            using (var reader = driver.ExecuteReader(connection, null, procedureName, parameters))
            {
                if (reader.Read())
                {
                    var platform = constructor();
                    platform.LoadFromReader(reader);
                    return platform;
                }
            }

            return null;
        }

        /// <summary>
        /// Loads all platforms using the passed in stored procedure.
        /// </summary>
        /// <param name="procedureName">The stored procedure.</param>
        /// <param name="constructor">The constructor for the platform.</param>
        /// <returns>An enumerable of platforms.</returns>
        protected static IEnumerable<IntegrationPlatform<TTransmissionInfo>> LoadPlatforms(StoredProcedureName procedureName, Func<IntegrationPlatform<TTransmissionInfo>> constructor)
        {
            List<IntegrationPlatform<TTransmissionInfo>> platforms = new List<IntegrationPlatform<TTransmissionInfo>>();

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);
            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShareROnly))
            using (var reader = driver.ExecuteReader(connection, null, procedureName, null))
            {
                while (reader.Read())
                {
                    var platform = constructor();
                    platform.LoadFromReader(reader);
                    platforms.Add(platform);
                }
            }

            return platforms;
        }

        /// <summary>
        /// Deletes the platform using the passed in stored procedure.
        /// </summary>
        /// <param name="platformId">The platform id.</param>
        /// <param name="procedureName">The stored procedure.</param>
        /// <param name="errors">Any errors encountered during deletion.</param>
        /// <returns>True if deleted, false otherwise.</returns>
        protected static bool DeletePlatform(int platformId, StoredProcedureName procedureName, out string errors)
        {
            if (platformId < 1)
            {
                errors = "Invalid platform.";
                return false;
            }

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@PlatformId", platformId)
            };

            var factory = GenericLocator<IStoredProcedureDriverFactory>.Factory;
            var driver = factory.Create(TimeoutInSeconds.Thirty);
            try
            {
                using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShareROnly))
                {
                    driver.ExecuteNonQuery(connection, null, procedureName, parameters);
                }
            }
            catch (SqlException exc)
            {
                Tools.LogError(exc);
                errors = "Unable to delete platform. A vendor may be using this platform.";
                return false;
            }

            errors = string.Empty;
            return true;
        }

        /// <summary>
        /// Gets any additional parameters that the child platform needs in order to create/update.
        /// </summary>
        /// <returns>Enumerable of SQL parameters to pass into the save SP.</returns>
        protected virtual IEnumerable<SqlParameter> GetAdditionalPlatformSaveParameters()
        {
            return new List<SqlParameter>();
        }

        /// <summary>
        /// Loads basic platform properties from the reader.
        /// </summary>
        /// <param name="reader">The reader to load from.</param>
        protected virtual void LoadFromReader(IDataReader reader)
        {
            var platformName = reader["PlatformName"];
            this.PlatformId = (int)reader["PlatformId"];
            this.PlatformName = platformName == DBNull.Value ? string.Empty : platformName.ToString();
            this.UsesAccountId = (bool)reader["UsesAccountId"];
            this.RequiresAccountId = (bool)reader["RequiresAccountId"];
            this.LinkedTransmissionId = (int)reader["TransmissionId"];
        }
    }
}
