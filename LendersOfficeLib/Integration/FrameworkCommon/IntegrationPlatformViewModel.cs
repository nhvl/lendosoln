﻿namespace LendersOffice.Integration.FrameworkCommon
{
    using System.Text;

    /// <summary>
    /// Base viewmodel for all integration platforms.
    /// </summary>
    public abstract class IntegrationPlatformViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IntegrationPlatformViewModel" /> class.
        /// </summary>
        protected IntegrationPlatformViewModel()
        {
            this.TransmissionInfo = new IntegrationTransmissionInfoViewModel();
        }

        /// <summary>
        /// Gets or sets the platform id.
        /// </summary>
        /// <value>The platform id.</value>
        public int? PlatformId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the platform name.
        /// </summary>
        /// <value>The platform name.</value>
        public string PlatformName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether an account id is used.
        /// </summary>
        /// <value>Whether an account id is used.</value>
        public bool UsesAccountId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether an account id is required.
        /// </summary>
        /// <value>Whether an account id is required.</value>
        public bool RequiresAccountId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the transmission info.
        /// </summary>
        /// <value>The transmission info.</value>
        public IntegrationTransmissionInfoViewModel TransmissionInfo
        {
            get;
            set;
        }

        /// <summary>
        /// Verifies the viewmodel.
        /// </summary>
        /// <param name="errors">Any errors encountered.</param>
        /// <returns>True if verified. False otherwise.</returns>
        public virtual bool Verify(out string errors)
        {
            string invalidError = "Invalid value for field {0}: {1}";
            StringBuilder errorMessages = new StringBuilder();
            if (string.IsNullOrEmpty(this.PlatformName))
            {
                errorMessages.AppendLine(string.Format(invalidError, "Platform Name", this.PlatformName));
            }

            errors = errorMessages.ToString();
            return errors.Length == 0;
        }
    }
}
