﻿// <copyright file="GenericFrameworkVendor.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   8/25/2014 11:43:39 AM
// </summary>
namespace LendersOffice.Integration.GenericFramework
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Xml.Linq;
    using System.Xml.Serialization;
    using DataAccess;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Defines the type of service provided by a Generic Framework Vendor.  Used to sort vendors for billing.<para />
    /// Note that this enum is exposed in Web Services; additions here will appear on the user's Web Service Definition.
    /// </summary>
    public enum TypeOfService
    {
        /// <summary>
        /// Specifies the vendor as a provider of several service types, meaning we expect
        /// them to specify during operation which service type they are acting as.
        /// </summary>
        Multiple = 0,

        /// <summary>
        /// Represents the Appraisal service type.
        /// </summary>
        Appraisal = 1,

        /// <summary>
        /// Represents the Automated valuation model service type.
        /// </summary>
        AVM = 2,

        /// <summary>
        /// Represents the Compliance service type.
        /// </summary>
        Compliance = 3,

        /// <summary>
        /// Represents the Credit service type.
        /// </summary>
        Credit = 4,

        /// <summary>
        /// Represents the Business Credit Report service type.
        /// </summary>
        [XmlEnum("Business Credit Report")]
        BusinessCreditReport = 5,

        /// <summary>
        /// Represents the Credit Analyzer service type.
        /// </summary>
        [XmlEnum("Credit Analyzer")]
        CreditAnalyzer = 6,

        /// <summary>
        /// Represents the Credit Assure service type.
        /// </summary>
        [XmlEnum("Credit Assure")]
        CreditAssure = 7,

        /// <summary>
        /// Represents the Credit Supplements service type.
        /// </summary>
        [XmlEnum("Credit Supplements")]
        CreditSupplements = 8,

        /// <summary>
        /// Represents the Criminal Record Reports service type.
        /// </summary>
        [XmlEnum("Criminal Record Reports")]
        CriminalRecordReports = 9,

        /// <summary>
        /// Represents the Eviction Reports service type.
        /// </summary>
        [XmlEnum("Eviction Reports")]
        EvictionReports = 10,

        /// <summary>
        /// Represents the Flood service type.
        /// </summary>
        Flood = 11,

        /// <summary>
        /// Represents the Fraud service type.
        /// </summary>
        Fraud = 12,

        /// <summary>
        /// Represents the HMDA service type.
        /// </summary>
        HMDA = 13,

        /// <summary>
        /// Represents the LDP and GSA Reports (Limited Denials of Participation, Government Services Administration) service type.
        /// </summary>
        [XmlEnum("LDP and GSA Reports")]
        LDPAndGSAReports = 14,

        /// <summary>
        /// Represents the MERS Report service type.
        /// </summary>
        [XmlEnum("MERS Report")]
        MERSReport = 15,

        /// <summary>
        /// Represents the Mortgage Insurance service type.
        /// </summary>
        [XmlEnum("Mortgage Insurance")]
        MortgageInsurance = 16,

        /// <summary>
        /// Represents the Mortgage Participants Report service type.
        /// </summary>
        [XmlEnum("Mortgage Participants Report")]
        MortgageParticipantsReport = 17,

        /// <summary>
        /// Represents the SSA-89 service type.
        /// </summary>
        [XmlEnum("SSA-89")]
        SSA89 = 18,

        /// <summary>
        /// Represents the Title service type.
        /// </summary>
        Title = 19,

        /// <summary>
        /// Represents the Verification Of Deposits (assets) service type.
        /// </summary>
        [XmlEnum("Verification Of Deposits (assets)")]
        VerificationOfDeposits = 20,

        /// <summary>
        /// Represents the Verification Of Employment service type.
        /// </summary>
        [XmlEnum("Verification Of Employment")]
        VerificationOfEmployment = 21,

        /// <summary>
        /// Represents the Tax Return Verification (4506-T) service type.
        /// </summary>
        [XmlEnum("Tax Return Verification (4506-T)")]
        TaxReturnVerification4506T = 22,

        /// <summary>
        /// Represents the Trailing Docs service type.
        /// </summary>
        [XmlEnum("Trailing Docs")]
        TrailingDocs = 23,

        /// <summary>
        /// Represents the Undisclosed Debt Notification service type.
        /// </summary>
        [XmlEnum("Undisclosed Debt Notification")]
        UndisclosedDebtNotification = 24,

        /// <summary>
        /// Represents the USPS Check service type.
        /// </summary>
        [XmlEnum("USPS Check")]
        USPSCheck = 25,

        /// <summary>
        /// Represents the What If Simulator service type.
        /// </summary>
        [XmlEnum("What If Simulator")]
        WhatIfSimulator = 26,

        /// <summary>
        /// Represents the optical character recognition service type.
        /// </summary>
        [XmlEnum("Optical Character Recognition")]
        OCR = 27,

        /// <summary>
        /// Represents the Income Analyzer service type.
        /// </summary>
        [XmlEnum("Income Analyzer")]
        IncomeAnalyzer = 28,

        /// <summary>
        /// Represents the Homeowner Insurance service type.
        /// </summary>
        [XmlEnum("Homeowner Insurance")]
        HomeownerInsurance = 29,
    }

    /// <summary>
    /// Indicates whether the XML being validated is credential or Launch Link XML.
    /// </summary>
    public enum XmlType
    {
        /// <summary>
        /// Indicates vendor or lender-level credential XML.
        /// </summary>
        Credential,

        /// <summary>
        /// Indicates Launch Link Config XML.
        /// </summary>
        LaunchLink
    }

    /// <summary>
    /// Used to indicate which loan identifier to send to the generic framework vendor.
    /// </summary>
    [Flags]
    public enum TypeOfLoanIdentifier
    {
        /// <summary>
        /// Indicates the loan number, meaning <seealso cref="CPageData.sLNm"/>.
        /// </summary>
        LoanNumber = 1 << 0,

        /// <summary>
        /// Indicates the loan reference number, meaning <seealso cref="CPageData.sLRefNm"/>.
        /// </summary>
        LoanReferenceNumber = 1 << 1,

        /// <summary>
        /// Indicates both the <see cref="LoanNumber"/> and <see cref="LoanReferenceNumber"/> options.
        /// </summary>
        BothLoanNumberAndLoanReferenceNumber = LoanNumber | LoanReferenceNumber
    }

    /// <summary>
    /// Represents a Generic Framework Vendor, possibly a broker-specific instance (specified by <see cref="IsBrokerInstance"/>).
    /// </summary>
    public class GenericFrameworkVendor
    {
        /// <summary>
        /// Stored Procedure: Retrieves the Generic Framework vendors, optionally restricted to a <see cref="ProviderID"/>.<para />
        /// Query takes optional parameter <c>@ProviderID varchar(7)</c>, selecting at most one record if parameter is provided, zero or more if not,
        /// of <c>ProviderID varchar(7)</c>, <c>Name varchar(255)</c>, <c>LaunchURL varchar(255)</c>, <c>CredentialXML varchar(max)</c>,
        /// <c>IncludeUsername bit</c>, <c>ServiceType int</c>, and <c>LoanIdentifierTypeToSend int</c> from <c>GENERIC_FRAMEWORK_VENDOR_CONFIG</c>.
        /// </summary>
        private static readonly string SpRetrieveGeneralVendor = "GENERIC_FRAMEWORK_VENDOR_Retrieve";

        /// <summary>
        /// Stored Procedure: Saves a Generic Framework Vendor, inserting if not currently present.<para />
        /// Non-query takes parameters <c>@ProviderID varchar(7)</c>, <c>@Name varchar(255)</c>,
        /// <c>@LaunchURL varchar(255)</c>, <c>@CredentialXML varchar(max)</c>, <c>@IncludeUsername bit</c>,
        /// <c>@ServiceType int</c>, and <c>@LoanIdentifierTypeToSend int</c> to <c>GENERIC_FRAMEWORK_VENDOR_CONFIG</c>.
        /// </summary>
        private static readonly string SpSaveGeneralVendor = "GENERIC_FRAMEWORK_VENDOR_Save";

        /// <summary>
        /// Stored Procedure: Retrieves Broker's data for a specific Generic Framework Vendor.<para />
        /// Query takes parameters <c>@BrokerID uniqueidentifier</c> and <c>@ProviderID varchar(7)</c>, selecting at
        /// most one record of <c>CredentialXML varbinary(max)</c> and <c>LaunchLinkConfigurationXML varchar(max)</c>
        /// from <c>GENERIC_FRAMEWORK_VENDOR_BROKER_CONFIG</c>.
        /// </summary>
        private static readonly string SpRetrieveBrokerInstance = "GENERIC_FRAMEWORK_VENDOR_BROKER_Retrieve";

        /// <summary>
        /// Stored Procedure: Retrieves all Broker-level data for all Generic Framework vendors on a specified Broker.<para />
        /// Query takes parameter <c>@BrokerID uniqueidentifier</c>, selecting zero or more records of
        /// <c>ProviderID varchar(7)</c>, <c>CredentialXML varbinary(max)</c>, and <c>LaunchLinkConfigurationXML varchar(max)</c>
        /// from <c>GENERIC_FRAMEWORK_VENDOR_BROKER_CONFIG</c>.
        /// </summary>
        /// <remarks>Due to the DB split, global-level configuration will have to be retrieved separately.</remarks>
        private static readonly string SpRetrieveAllBrokerInstances = "GENERIC_FRAMEWORK_VENDOR_BROKER_RetrieveAll";

        /// <summary>
        /// Stored Procedure: Saves the Broker data for a Generic Framework vendor, inserting if not currently present.<para />
        /// Non-query takes parameters <c>@BrokerID uniqueidentifier</c>, <c>@ProviderID varchar(7)</c>, <c>@CredentialXML varbinary(max)</c>,
       ///  and <c>LaunchLinkConfigurationXML varchar(max)</c> to <c>GENERIC_FRAMEWORK_VENDOR_BROKER_CONFIG</c>.
        /// </summary>
        private static readonly string SpSaveBrokerInstance = "GENERIC_FRAMEWORK_VENDOR_BROKER_Save";

        /// <summary>
        /// Stored Procedure: Deletes the Broker data for a Generic Framework vendor.<para />
        /// Non-query takes parameters <c>@BrokerID uniqueidentifier</c> and <c>@ProviderID varchar(7)</c> to <c>GENERIC_FRAMEWORK_VENDOR_BROKER_CONFIG</c>.
        /// </summary>
        private static readonly string SpDeleteBrokerInstance = "GENERIC_FRAMEWORK_VENDOR_BROKER_Delete";

        /// <summary>
        /// The XML representation of <see cref="LaunchLinkConfig"/>
        /// </summary>
        private XElement launchLinkConfigXML;

        /// <summary>
        /// The launch link configuration, loaded from the XML.
        /// </summary>
        private LaunchLinkConfiguration launchLinkConfig;

        /// <summary>
        /// The identifier of the encryption key used to encrypt <see cref="CredentialXML"/> for lender instances.
        /// </summary>
        private EncryptionKeyIdentifier encryptionKeyId;

        /// <summary>
        /// A lazy interface to get the credential XML.  This is used to save us from having to decrypt and parse unnecessarily.
        /// </summary>
        private Lazy<XElement> lazyCredentialXML;

        /// <summary>
        /// Prevents a default instance of the <see cref="GenericFrameworkVendor"/> class from being created.
        /// </summary>
        private GenericFrameworkVendor()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericFrameworkVendor"/> class, from an existing system-level vendor.
        /// </summary>
        /// <param name="vendor">Containing the database values for the system-level vendor.</param>
        /// <param name="brokerID">The broker ID of the broker to whom the created instance of the vendor belongs.</param>
        /// <param name="encryptionKeyId">The identifier of the encryption key used to encrypt <see cref="CredentialXML"/>.</param>
        /// <param name="credentialXML">The encrypt xml credential of broker account.</param>
        /// <param name="launchLinkConfigXMLString">The xml of the link configuration.</param>
        private GenericFrameworkVendor(GenericFrameworkVendor vendor, Guid brokerID, EncryptionKeyIdentifier encryptionKeyId, byte[] credentialXML, string launchLinkConfigXMLString)
        {
            this.ProviderID = vendor.ProviderID;
            this.ServiceType = vendor.ServiceType;
            this.LoanIdentifierTypeToSend = vendor.LoanIdentifierTypeToSend;
            this.Name = vendor.Name;
            this.LaunchURL = vendor.LaunchURL;
            this.IncludeUsername = vendor.IncludeUsername;

            this.MakeBrokerInstance(brokerID);
            this.encryptionKeyId = encryptionKeyId;

            this.LaunchLinkConfigXMLstring = launchLinkConfigXMLString;

            this.lazyCredentialXML = new Lazy<XElement>(() => this.ConvertXMLFromDatabase(Drivers.Encryption.EncryptionHelper.DecryptString(encryptionKeyId, credentialXML), "CredentialXML"));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericFrameworkVendor"/> class.
        /// </summary>
        /// <param name="reader">The data reader containing the database values for the vendor.</param>
        private GenericFrameworkVendor(IDataRecord reader)
        {
            this.ProviderID = reader["ProviderID"] as string;
            this.ServiceType = (TypeOfService)reader["ServiceType"];
            this.LoanIdentifierTypeToSend = (TypeOfLoanIdentifier)reader["LoanIdentifierTypeToSend"];
            this.Name = reader["Name"] as string;
            this.LaunchURL = reader["LaunchURL"] as string;
            this.IncludeUsername = (bool)reader["IncludeUsername"];
            string credentialXML = reader["CredentialXML"] as string;
            byte[] encryptedCredentialXML = (byte[])reader["EncryptedCredentialXML"];
            var maybeEncryptionKeyId = EncryptionKeyIdentifier.Create((Guid)reader["EncryptionKeyId"]);
            this.encryptionKeyId = maybeEncryptionKeyId ?? default(EncryptionKeyIdentifier);
            var lazyCredentialXmlString = maybeEncryptionKeyId.HasValue
                ? new Lazy<string>(() => EncryptionHelper.DecryptString(maybeEncryptionKeyId.Value, encryptedCredentialXML))
                : new Lazy<string>(() => credentialXML);

            this.lazyCredentialXML = new Lazy<XElement>(() => this.ConvertXMLFromDatabase(lazyCredentialXmlString.Value, "CredentialXML"));
        }

        /// <summary>
        /// Gets the identifier specific to this vendor.
        /// </summary>
        /// <value>The identifier specific to this vendor.</value>
        public string ProviderID { get; private set; }

        /// <summary>
        /// Gets the type of service provided by this vendor.
        /// </summary>
        /// <value>The type of service provided by this vendor.</value>
        public TypeOfService ServiceType { get; private set; }

        /// <summary>
        /// Gets the loan identifier the vendor will consume.
        /// </summary>
        /// <value>The loan identifier the vendor will consume.</value>
        public TypeOfLoanIdentifier LoanIdentifierTypeToSend { get; private set; }

        /// <summary>
        /// Gets or sets the name of the vendor.
        /// </summary>
        /// <value>The name of the vendor.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the url that data is sent to before the popup is created.
        /// </summary>
        /// <value>The url that data is sent to before the popup is created.</value>
        public string LaunchURL { get; set; }

        /// <summary>
        /// Gets or sets the lender credential XML as an XElement.
        /// </summary>
        /// <value>The lender credential XML.</value>
        public XElement CredentialXML
        {
            get { return this.lazyCredentialXML?.Value; }
            set { this.lazyCredentialXML = new Lazy<XElement>(() => value); }
        }

        /// <summary>
        /// Gets a value indicating whether a lender XML credential exists for this vendor.
        /// </summary>
        /// <value>A boolean indicating whether a lender XML credential exists for this vendor.</value>
        public bool HasLenderCredential
        {
            get { return this.CredentialXML != null; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether user-level authentication is used for this vendor.
        /// </summary>
        /// <value>A boolean indicating whether user-level authentication is used for this vendor.</value>
        public bool IncludeUsername { get; set; }

        /// <summary>
        /// Gets the broker ID for this generic vendor's instance.
        /// </summary>
        /// <value>The broker ID of the present instance, or null if this is the generic vendor's configuration (independent from the broker-level).</value>
        public Guid? BrokerID { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this instance is tied to a broker.
        /// </summary>
        /// <value><see langword="true"/> if this instance is tied to a broker, <see langword="false"/> otherwise (generic vendor configuration).</value>
        public bool IsBrokerInstance
        {
            get { return this.BrokerID.HasValue; }
        }

        /// <summary>
        /// Gets the launch link configuration.
        /// </summary>
        /// <value>The launch link configuration, used to determine whether the link is visible.</value>
        public LaunchLinkConfiguration LaunchLinkConfig
        {
            get
            {
                this.EnsureBrokerInstance();
                if (this.launchLinkConfig == null)
                {
                    this.launchLinkConfig = new LaunchLinkConfiguration(this.LaunchLinkConfigXML, this.Name);
                }

                return this.launchLinkConfig;
            }
        }

        /// <summary>
        /// Gets or sets the XML representation of <see cref="LaunchLinkConfig"/>.
        /// </summary>
        /// <value>The XML representation of <see cref="LaunchLinkConfig"/>.</value>
        public XElement LaunchLinkConfigXML
        {
            get
            {
                this.EnsureBrokerInstance();
                if (this.launchLinkConfigXML == null)
                {
                    this.launchLinkConfigXML = this.ConvertXMLFromDatabase(this.LaunchLinkConfigXMLstring, "LaunchLinkConfiguration") ?? LaunchLinkConfiguration.EmptyXML;
                }

                return this.launchLinkConfigXML;
            }

            set
            {
                this.EnsureBrokerInstance();
                this.launchLinkConfig = new LaunchLinkConfiguration(value, this.Name);
                this.launchLinkConfigXML = value;
                this.LaunchLinkConfigXMLstring = value.ToString(SaveOptions.DisableFormatting);
            }
        }

        /// <summary>
        /// Gets or sets the XML string of the <see cref="LaunchLinkConfig"/>.
        /// </summary>
        private string LaunchLinkConfigXMLstring { get; set; }

        /// <summary>
        /// Evaluate the permissions for whether the launch link is visible for the user and the loan on each ProviderID provided.
        /// </summary>
        /// <param name="loanID">The loan ID (<see cref="CPageData.sLId"/>) to use to evaluate the permissions.</param>
        /// <param name="providerIDs">The list of provider identifiers to evaluate.</param>
        /// <param name="location">The location of the Generic Framework launch link in the UI.</param>
        /// <returns>A mapping for each unique ProviderID to a boolean value indicating whether the link should be visible.</returns>
        public static Dictionary<string, bool> EvaluatePermissions(Guid loanID, string[] providerIDs, LinkLocation location)
        {
            Dictionary<string, bool> providerIDMappings = new Dictionary<string, bool>(providerIDs.Length);
            foreach (var id in providerIDs)
            {
                if (!providerIDMappings.ContainsKey(id))
                {
                    providerIDMappings.Add(id, false);
                }
            }

            Guid brokerId = LendersOffice.Security.PrincipalFactory.CurrentPrincipal.BrokerId;
            if (providerIDMappings.Count == 1)
            {
                string providerID = providerIDMappings.First().Key;
                var vendor = LoadVendor(brokerId, providerID);
                if (vendor != null && vendor.IsBrokerInstance && vendor.LaunchLinkConfig.IsDisplayLinkForLoanAndUser(loanID, location))
                {
                    providerIDMappings[providerID] = true;
                }
            }
            else if (providerIDMappings.Count > 1)
            {
                var vendors = LoadVendors(brokerId);
                foreach (string providerID in providerIDMappings.Keys.ToList())
                {
                    var vendor = vendors.FirstOrDefault(v => v.ProviderID == providerID);
                    if (vendor != null && vendor.LaunchLinkConfig.IsDisplayLinkForLoanAndUser(loanID, location))
                    {
                        providerIDMappings[providerID] = true;
                    }
                }
            }

            return providerIDMappings;
        }

        /// <summary>
        /// Saves a system-level vendor instance that does not relate to a broker.
        /// </summary>
        /// <param name="providerID">The <see cref="ProviderID"/> of the vendor.</param>
        /// <param name="name">The <see cref="Name"/> of the vendor.</param>
        /// <param name="launchURL">The <see cref="LaunchURL"/> of the vendor.</param>
        /// <param name="credentialXML">The <see cref="CredentialXML"/> of the vendor. </param>
        /// <param name="includeUsername">Indicates whether user-level authentication is allowed for this vendor.</param>
        /// <param name="serviceType">The <see cref="ServiceType"/> of the vendor.</param>
        /// <param name="loanIdentifierTypeToSend">The <see cref="LoanIdentifierTypeToSend"/> of the vendor.</param>
        /// <returns>The vendor instance that was saved.</returns>
        public static GenericFrameworkVendor SaveSystemLevelVendor(string providerID, string name, string launchURL, string credentialXML, bool includeUsername, TypeOfService serviceType, TypeOfLoanIdentifier loanIdentifierTypeToSend)
        {
            GenericFrameworkVendor vendor = LoadVendor(providerID) ?? new GenericFrameworkVendor();
            vendor.Name = name;
            vendor.LaunchURL = launchURL;
            vendor.IncludeUsername = includeUsername;
            vendor.CredentialXML = vendor.ValidateXml(credentialXML, XmlType.Credential); // we don't want the validation to happen lazily
            vendor.ServiceType = serviceType;
            vendor.LoanIdentifierTypeToSend = loanIdentifierTypeToSend;
            vendor.Save();
            return vendor;
        }

        /// <summary>
        /// Loads all Generic Framework vendors from the database.
        /// </summary>
        /// <returns>A list of all Generic Framework vendors.</returns>
        public static List<GenericFrameworkVendor> LoadAllVendors()
        {
            var vendors = new List<GenericFrameworkVendor>();
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, SpRetrieveGeneralVendor))
            {
                while (reader.Read())
                {
                    vendors.Add(new GenericFrameworkVendor(reader));
                }
            }

            return vendors;
        }

        /// <summary>
        /// Loads all Generic Framework vendors belonging to the specified broker from the database.
        /// </summary>
        /// <param name="brokerID">The broker ID of the broker to whom the instances of the vendor belong.</param>
        /// <returns>A list of all the broker-specific vendor instances for the broker.</returns>
        public static List<GenericFrameworkVendor> LoadVendors(Guid brokerID)
        {
            Dictionary<string, GenericFrameworkVendor> dictionary = LoadAllVendors().ToDictionary(o => o.ProviderID);

            SqlParameter[] parameters =
                                        {
                                            new SqlParameter("@BrokerID", brokerID)
                                        };

            List<Dictionary<string, object>> databaseRecords = new List<Dictionary<string, object>>();
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerID, SpRetrieveAllBrokerInstances, parameters))
            {
                while (reader.Read())
                {
                    databaseRecords.Add(reader.ToDictionary());
                }
            }

            var vendors = new List<GenericFrameworkVendor>();
            foreach (Dictionary<string, object> reader in databaseRecords)
            {
                string providerID = reader["ProviderID"] as string;
                EncryptionKeyIdentifier encryptionKeyId = EncryptionKeyIdentifier.Create((Guid)reader["EncryptionKeyId"]).ForceValue();
                byte[] credentialXML = reader["CredentialXML"] as byte[];
                string launchLinkConfigurationXml = reader["LaunchLinkConfigurationXML"] as string;

                GenericFrameworkVendor vendor = null;

                if (dictionary.TryGetValue(providerID, out vendor))
                {
                    vendors.Add(new GenericFrameworkVendor(vendor, brokerID, encryptionKeyId, credentialXML, launchLinkConfigurationXml));
                }
            }

            return vendors;
        }

        /// <summary>
        /// Loads the specified Generic Framework vendor instance corresponding to a broker from the database.
        /// </summary>
        /// <param name="brokerID">The broker ID of the broker to whom the instance of the vendor should belong.</param>
        /// <param name="providerID">The provider ID of the vendor.</param>
        /// <returns>The Generic Framework vendor instance matching the provider and broker IDs, just the vendor instance matching the provider ID, or null if no record is found.</returns>
        public static GenericFrameworkVendor LoadVendor(Guid brokerID, string providerID)
        {
            GenericFrameworkVendor systemLevelVendor = LoadVendor(providerID);
            if (systemLevelVendor == null)
            {
                return null;
            }

            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerID", brokerID),
                new SqlParameter("@ProviderID", providerID)
            };
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerID, SpRetrieveBrokerInstance, parameters))
            {
                if (reader.Read())
                {
                    EncryptionKeyIdentifier encryptionKeyId = EncryptionKeyIdentifier.Create((Guid)reader["EncryptionKeyId"]).ForceValue();
                    byte[] encryptedCredentialXml = reader["CredentialXML"] as byte[];
                    string launchLinkConfigXMLstring = reader["LaunchLinkConfigurationXML"] as string;
                    return new GenericFrameworkVendor(systemLevelVendor, brokerID, encryptionKeyId, encryptedCredentialXml, launchLinkConfigXMLstring);
                }
            }

            return systemLevelVendor;
        }

        /// <summary>
        /// Tests whether an XML string can be parsed. The string must parse unless it is a credential and
        /// user-level authentication is allowed, in which case a blank credential validates.
        /// </summary>
        /// <param name="xmlString">The XML to be validated.</param>
        /// <param name="type">Indicates whether the XML is a credential or Launch Link XML.</param>
        /// <returns>The XElement representation of the string.</returns>
        /// <exception cref="System.Xml.XmlException">
        /// Throws if the XML string cannot be parsed. Will contain an explanation in Data["ErrorMessage"].
        /// </exception>
        public XElement ValidateXml(string xmlString, XmlType type)
        {
            try
            {
                return XElement.Parse(xmlString);
            }
            catch (System.Xml.XmlException exc)
            {
                if (string.IsNullOrEmpty(xmlString) && type == XmlType.Credential)
                {
                    if (this.IncludeUsername)
                    {
                        // An empty credential is allowed in this case.
                        return null;
                    }

                    exc.Data.Add("ErrorMessage", "Lender Credential XML cannot be empty unless Include Username is checked.");
                    throw;
                }
                else
                {
                    exc.Data.Add("ErrorMessage", "Invalid " + type.ToString() + " XML: " + exc.Message);
                    throw;
                }
            }
        }

        /// <summary>
        /// Deletes a broker-specific instance from the database.
        /// </summary>
        /// <exception cref="LendersOffice.Common.GenericUserErrorMessageException"><see cref="IsBrokerInstance"/> is false, i.e. this is not a broker-specific instance.</exception>
        public void Delete()
        {
            this.EnsureBrokerInstance();

            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerID", this.BrokerID.Value),
                new SqlParameter("@ProviderID", this.ProviderID)
            };

            StoredProcedureHelper.ExecuteNonQuery(this.BrokerID.Value, SpDeleteBrokerInstance, 3, parameters);
        }

        /// <summary>
        /// Converts the current instance to a broker-specific instance.
        /// </summary>
        /// <param name="brokerID">The broker ID of the broker to whom the instance of the vendor belongs.</param>
        public void MakeBrokerInstance(Guid brokerID)
        {
            if (this.IsBrokerInstance)
            {
                throw new GenericUserErrorMessageException("Vendor must not be a broker instance.");
            }

            this.BrokerID = brokerID;
            this.encryptionKeyId = default(EncryptionKeyIdentifier); // This will cause the instance to use a new encryption key when it saves
        }

        /// <summary>
        /// Saves the Generic Framework vendor (whether a broker-specific instance or not) to the database.<para />
        /// If this instance is not broker-specific and <see cref="ProviderID"/> is null or empty, it will assign a new value.
        /// </summary>
        public void Save()
        {
            string credentialXML = this.CredentialXML == null ? string.Empty : this.CredentialXML.ToString(SaveOptions.DisableFormatting);
            if (this.IsBrokerInstance)
            {
                if (this.encryptionKeyId == default(EncryptionKeyIdentifier))
                {
                    this.encryptionKeyId = Drivers.Encryption.EncryptionHelper.GenerateNewKey();
                }

                byte[] encryptedCredentialXML = Drivers.Encryption.EncryptionHelper.EncryptString(this.encryptionKeyId, credentialXML);

                SqlParameter[] parameters =
                {
                    new SqlParameter("@BrokerID", this.BrokerID.Value),
                    new SqlParameter("@ProviderID", this.ProviderID),
                    new SqlParameter("@CredentialXML", encryptedCredentialXML),
                    new SqlParameter("@EncryptionKeyId", this.encryptionKeyId.Value),
                    new SqlParameter("@LaunchLinkConfigurationXML", this.LaunchLinkConfigXMLstring)
                };

                StoredProcedureHelper.ExecuteNonQuery(this.BrokerID.Value, SpSaveBrokerInstance, 3, parameters);
            }
            else
            {
                if (string.IsNullOrEmpty(this.ProviderID))
                {
                    const string ProviderIdPrefix = "VEN";
                    int vendorNumber = 1 + LoadAllVendors()
                        .Where(vendor => vendor.ProviderID.StartsWith(ProviderIdPrefix, StringComparison.OrdinalIgnoreCase)) // Looking for VEN0001, et cetera
                        .Select(vendor => vendor.ProviderID.Substring(ProviderIdPrefix.Length).ToNullable<int>(int.TryParse) ?? 0)
                        .Max();
                    string providerID = ProviderIdPrefix + vendorNumber.ToString("D4");
                    if (providerID.Length != 7)
                    {
                        throw new GenericUserErrorMessageException("Tried to create a vendor with an invalid ProviderID.  Attempted value: [" + providerID + "].  ProviderID must be 7 characters, like VEN0001.");
                    }

                    this.ProviderID = providerID;
                    this.encryptionKeyId = EncryptionHelper.GenerateNewKey();
                }

                byte[] encryptedCredentialXML = null;
                if (this.encryptionKeyId != default(EncryptionKeyIdentifier))
                {
                    encryptedCredentialXML = EncryptionHelper.EncryptString(this.encryptionKeyId, credentialXML);
                }

                SqlParameter[] parameters =
                {
                    new SqlParameter("@ProviderID", this.ProviderID),
                    new SqlParameter("@Name", this.Name),
                    new SqlParameter("@LaunchURL", this.LaunchURL),
                    new SqlParameter("@CredentialXML", credentialXML),
                    new SqlParameter("@EncryptedCredentialXML", encryptedCredentialXML ?? new byte[0]),
                    new SqlParameter("@EncryptionKeyId", this.encryptionKeyId.Value),
                    new SqlParameter("@IncludeUsername", this.IncludeUsername),
                    new SqlParameter("@ServiceType", this.ServiceType),
                    new SqlParameter("@LoanIdentifierTypeToSend", this.LoanIdentifierTypeToSend),
                };

                StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, SpSaveGeneralVendor, 3, parameters);
            }
        }

        /// <summary>
        /// Loads a vendor from the Database. This instance is not tied to a broker.
        /// </summary>
        /// <param name="providerID">The provider ID of the vendor.</param>
        /// <returns>The Generic Framework vendor instance matching the provider ID, or null if no record is found.</returns>
        private static GenericFrameworkVendor LoadVendor(string providerID)
        {
            if (string.IsNullOrEmpty(providerID))
            {
                return null;
            }

            SqlParameter[] parameters =
            {
                new SqlParameter("@ProviderID", providerID)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, SpRetrieveGeneralVendor, parameters))
            {
                if (reader.Read())
                {
                    return new GenericFrameworkVendor(reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Converts XML from the database to the XElement corresponding to the passed XML string, catching the expected cases of XML errors.
        /// </summary>
        /// <param name="xml">The XML string to read.</param>
        /// <param name="fieldName">The name of the field being read, used for debugging.</param>
        /// <returns>The XElement representing the XML string, or null if there was a <seealso cref="System.Xml.XmlException"/>.</returns>
        private XElement ConvertXMLFromDatabase(string xml, string fieldName)
        {
            if (!string.IsNullOrEmpty(xml))
            {
                try
                {
                    return XElement.Parse(xml);
                }
                catch (System.Xml.XmlException exc)
                {
                    Tools.LogWarning("[GenericFramework] Invalid XML read in field " + fieldName + " for " + this.ProviderID + " and BrokerID=" + this.BrokerID + ".  Value was [" + xml + "].", exc);
                }
            }

            return null;
        }

        /// <summary>
        /// Throws an exception if <see cref="IsBrokerInstance"/> is false.
        /// </summary>
        /// <exception cref="LendersOffice.Common.GenericUserErrorMessageException"><see cref="IsBrokerInstance"/> is false, i.e. this is not a broker-specific instance.</exception>
        private void EnsureBrokerInstance()
        {
            if (!this.IsBrokerInstance)
            {
                throw new GenericUserErrorMessageException("Vendor must be a broker instance.");
            }
        }
    }
}
