﻿// <copyright file="LaunchLinkConfiguration.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   9/17/2014 5:08:26 PM
// </summary>
namespace LendersOffice.Integration.GenericFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// The location of the Generic Framework launch link in the UI.
    /// </summary>
    public enum LinkLocation // Each item's name must be a valid XML element name
    {
        /// <summary>
        /// The primary loan editor in LendingQB, linked to the B user type.
        /// </summary>
        LoanEditor = 0,

        /// <summary>
        /// The pipeline screen in LendingQB, linked to the B user type.
        /// </summary>
        LQBPipeline = 1,

        /// <summary>
        /// The Third-Party Originator Portal, linked to Price My Loan and the P user type.
        /// </summary>
        TPOPipeline = 2,

        /// <summary>
        /// The "Status and Agents" page within the Third-Party Originator Portal.
        /// </summary>
        TpoLoanNavigationStatusAndAgents = 3,

        /// <summary>
        /// The "Application Information" page within the Third-Party Originator Portal.
        /// </summary>
        TpoLoanNavigationApplicationInformation = 4,

        /// <summary>
        /// The "Closing Costs" page within the Third-Party Originator Portal.
        /// </summary>
        TpoLoanNavigationClosingCosts = 5,

        /// <summary>
        /// The "GFE Fee Editor" page within the Third-Party Originator Portal.
        /// </summary>
        TpoLoanNavigationFeeEditor = 6,

        /// <summary>
        /// The "Pricing" page within the Third-Party Originator Portal.
        /// </summary>
        TpoLoanNavigationPricing = 7,

        /// <summary>
        /// The "Loan Information" page within the Third-Party Originator Portal.
        /// </summary>
        TpoLoanNavigationLoanTerms = 8,

        /// <summary>
        /// The "Rate Lock" page within the Third-Party Originator Portal.
        /// </summary>
        TpoLoanNavigationRateLock = 9,

        /// <summary>
        /// The "QM" page within the Third-Party Originator Portal.
        /// </summary>
        TpoLoanNavigationQm = 10,

        /// <summary>
        /// The "E-docs" page within the Third-Party Originator Portal.
        /// </summary>
        TpoLoanNavigationEdocs = 11,

        /// <summary>
        /// The "Tasks" page within the Third-Party Originator Portal.
        /// </summary>
        TpoLoanNavigationTasks = 12,

        /// <summary>
        /// The "Conditions" page within the Third-Party Originator Portal.
        /// </summary>
        TpoLoanNavigationConditions = 13,

        /// <summary>
        /// The "Order Services" page within the Third-Party Originator Portal.
        /// </summary>
        TpoLoanNavigationOrderServices = 14,

        /// <summary>
        /// The "Disclosures" page within the Third-Party Originator Portal.
        /// </summary>
        TpoLoanNavigationDisclosures = 15
    }

    /// <summary>
    /// Defines the configuration of the launch links for the Generic Framework in the places that they may appear.
    /// </summary>
    public class LaunchLinkConfiguration
    {
        /// <summary>
        /// The name of the root element in the XML representation of the configuration.
        /// </summary>
        private const string RootElementName = "launchLinkConfiguration";

        /// <summary>
        /// The configurations for each location within the <see cref="LinkLocation"/> choices.
        /// </summary>
        private LocationConfiguration[] locationConfigurations;

        /// <summary>
        /// Initializes a new instance of the <see cref="LaunchLinkConfiguration"/> class.
        /// </summary>
        internal LaunchLinkConfiguration()
        {
            this.locationConfigurations = new LocationConfiguration[Enum.GetValues(typeof(LinkLocation)).Length];
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LaunchLinkConfiguration"/> class from XML.
        /// </summary>
        /// <param name="element">The XML to read into a new object.</param>
        /// <param name="vendorName">The name of the vendor, to be used as a default in case <see cref="LocationConfiguration.DisplayName"/> is not specified.</param>
        /// <exception cref="LaunchLinkConfigurationException">The XML does not match the required format.</exception>
        internal LaunchLinkConfiguration(XElement element, string vendorName)
            : this()
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }

            if (!string.Equals(element.Name.LocalName, RootElementName, StringComparison.Ordinal))
            {
                throw new LaunchLinkConfigurationException("Root element must be \"" + RootElementName + "\"");
            }

            foreach (var child in element.Elements("locationConfiguration"))
            {
                var config = new LocationConfiguration(child, vendorName);
                this.locationConfigurations[(int)config.Location] = config;
            }
        }

        /// <summary>
        /// Gets an XML representation of an empty launch link configuration.
        /// </summary>
        /// <value>An XML representation of an empty launch link configuration.</value>
        public static XElement EmptyXML
        {
            get { return (new LaunchLinkConfiguration()).ToXml(); }
        }

        /// <summary>
        /// Creates a XML representation of a new launch link configuration, with each location accessible.
        /// </summary>
        /// <param name="vendorName">The name of the vendor to be used for <see cref="LocationConfiguration.DisplayName"/>.</param>
        /// <returns>An XML representation of a launch link configuration with all locations accessible.</returns>
        public static XElement NewXML(string vendorName)
        {
            var config = new LaunchLinkConfiguration();
            foreach (LinkLocation location in Enum.GetValues(typeof(LinkLocation)))
            {
                XElement emptyElement = new XElement("locationConfiguration", new XAttribute("location", location));
                config.locationConfigurations[(int)location] = new LocationConfiguration(emptyElement, vendorName);
            }

            return config.ToXml();
        }

        /// <summary>
        /// Retrieves the name the link will show in the UI.
        /// </summary>
        /// <param name="location">The location of the Generic Framework launch link in the UI.</param>
        /// <returns>The display name specified by the configuration, or null if no configuration exists.</returns>
        public string LinkDisplayName(LinkLocation location)
        {
            var config = this.locationConfigurations[(int)location];
            return config == null ? null : config.DisplayName;
        }

        /// <summary>
        /// Retrieves the setting indicating whether the link will display as a button.
        /// </summary>
        /// <param name="location">The location of the Generic Framework launch link in the UI.</param>
        /// <returns>True if the link should display as a button, false otherwise.</returns>
        public bool IsLinkDisplayAsButton(LinkLocation location)
        {
            var config = this.locationConfigurations[(int)location];
            return config != null && config.DisplayAsButton;
        }

        /// <summary>
        /// Checks the user against the rules for when to display the link.
        /// </summary>
        /// <param name="location">The location of the Generic Framework launch link in the UI.</param>
        /// <returns><see langword="true"/> if the link should be displayed for the user, <see langword="false"/> otherwise.</returns>
        public bool IsDisplayLinkForUser(LinkLocation location)
        {
            var config = this.locationConfigurations[(int)location];
            return config != null && config.IsDisplayLink(Guid.Empty);
        }

        /// <summary>
        /// Checks the loan and user against the rules for when to display the link.
        /// </summary>
        /// <param name="loanID">The loan ID (<see cref="CPageData.sLId"/>) of the loan.</param>
        /// <param name="location">The location of the Generic Framework launch link in the UI.</param>
        /// <returns><see langword="true"/> if the link should be displayed for the loan and user, <see langword="false"/> otherwise.</returns>
        /// <exception cref="GenericUserErrorMessageException"><paramref name="loanID"/> is Guid.Empty.</exception>
        public bool IsDisplayLinkForLoanAndUser(Guid loanID, LinkLocation location)
        {
            if (loanID == Guid.Empty)
            {
                throw new GenericUserErrorMessageException("Loan ID cannot be Guid.Empty.");
            }

            var config = this.locationConfigurations[(int)location];
            return config != null && config.IsDisplayLink(loanID);
        }

        /// <summary>
        /// Serialize the current element into xml. Note that this will remove any extra data (comments, ignored elements, et cetera).
        /// </summary>
        /// <returns>The XML representation of the item.</returns>
        private XElement ToXml()
        {
            return new XElement(
                RootElementName,
                this.locationConfigurations.Select(locationConfig => locationConfig == null ? null : locationConfig.ToXml()));
        }

        /// <summary>
        /// An exception that occurred while loading the configuration details for the launch link. Uses <seealso cref="ErrorMessages.Generic"/>
        /// for the user message.
        /// </summary>
        public class LaunchLinkConfigurationException : GenericUserErrorMessageException
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="LaunchLinkConfigurationException"/> class.
            /// </summary>
            /// <param name="developerMessage">The message to pass to the developer.</param>
            internal LaunchLinkConfigurationException(string developerMessage)
                : base(developerMessage)
            {
            }
        }

        /// <summary>
        /// Represents the configuration for a particular location within the LendingQB system.
        /// </summary>
        private class LocationConfiguration
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="LocationConfiguration"/> class.
            /// </summary>
            /// <param name="element">The XML to read into a new object.</param>
            /// <param name="vendorName">The name of the vendor, to be used as a default in case <see cref="DisplayName"/> is not specified.</param>
            internal LocationConfiguration(XElement element, string vendorName)
            {
                string location = XmlUtils.GetXAttributeValue(element, "location") ?? string.Empty;
                try
                {
                    this.Location = (LinkLocation)Enum.Parse(typeof(LinkLocation), location, true);
                }
                catch (ArgumentException)
                {
                    throw new LaunchLinkConfigurationException("Unable to parse LocationConfiguration location attribute value: \"" + location + "\"");
                }

                // Link configurations will display as links by default, making this parameter optional.
                string displayAsButton = XmlUtils.GetXAttributeValue(element, "displayAsButton") ?? bool.FalseString;
                try
                {
                    this.DisplayAsButton = bool.Parse(displayAsButton);
                }
                catch (FormatException)
                {
                    throw new LaunchLinkConfigurationException("Unable to parse LocationConfiguration displayAsButton attribute value: \"" + displayAsButton + "\"");
                }

                this.DisplayName = XmlUtils.GetXAttributeValue(element, "displayName") ?? vendorName;
                this.FieldIDs = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                this.RestrictionCases = new List<RestrictionCase>(element.Elements("restrictionCase").Select(el => new RestrictionCase(el, this.FieldIDs)));
            }

            /// <summary>
            /// Gets the location of the link in the UI.
            /// </summary>
            internal LinkLocation Location { get; private set; }

            /// <summary>
            /// Gets the name the link will show in the UI.
            /// </summary>
            internal string DisplayName { get; private set; }

            /// <summary>
            /// Gets a value indicating whether the configuration will display
            /// as a button.
            /// </summary>
            internal bool DisplayAsButton { get; private set; }

            /// <summary>
            /// Gets or sets the list of cases under which to show the link in the UI.
            /// </summary>
            private List<RestrictionCase> RestrictionCases { get; set; }

            /// <summary>
            /// Gets or sets the set of field IDs used by all restriction cases in this location in the UI.
            /// </summary>
            private HashSet<string> FieldIDs { get; set; }

            /// <summary>
            /// Determines whether the link should be displayed in the UI in the location this represents.
            /// </summary>
            /// <param name="loanID">The loan ID to check, or Guid.Empty if only making the role based check.</param>
            /// <returns><see langword="true"/> if the roles and loan meet a particular restriction case or there are no specified restriction cases, <see langword="false"/> otherwise.</returns>
            internal bool IsDisplayLink(Guid loanID)
            {
                if (this.RestrictionCases.Count == 0)
                {
                    return true;
                }

                LoanValueEvaluator loanValueEvaluator = loanID == Guid.Empty ? null : new LoanValueEvaluator(this.FieldIDs, loanID);
                return this.RestrictionCases.Any(restrictionCase => restrictionCase.IsInCase(loanValueEvaluator));
            }

            /// <summary>
            /// Serialize the current element into xml. Note that this will remove any extra data (comments, ignored elements, et cetera).
            /// </summary>
            /// <returns>The XML representation of the item.</returns>
            internal XElement ToXml()
            {
                return new XElement(
                    "locationConfiguration",
                    new XAttribute("location", this.Location),
                    string.IsNullOrEmpty(this.DisplayName) ? null : new XAttribute("displayName", this.DisplayName),
                    this.RestrictionCases.Select(restrictionCase => restrictionCase.ToXml()),
                    new XAttribute("displayAsButton", this.DisplayAsButton));
            }
        }

        /// <summary>
        /// Represents a specific case of when to show the link, relating to the situation of the current user's role and the loan's fields.
        /// </summary>
        private class RestrictionCase
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="RestrictionCase"/> class.
            /// </summary>
            /// <param name="element">The XML to read into a new object.</param>
            /// <param name="fieldIDs">A set of field IDs used to evaluate this location's configuration.</param>
            internal RestrictionCase(XElement element, HashSet<string> fieldIDs)
            {
                this.Roles = new List<E_RoleT>();
                foreach (string roleValue in element.Elements("role").Select(el => el.Value))
                {
                    int value;
                    if (int.TryParse(roleValue, out value) && Enum.IsDefined(typeof(E_RoleT), value))
                    {
                        this.Roles.Add((E_RoleT)value);
                    }
                    else
                    {
                        throw new LaunchLinkConfigurationException("Unhandled role: \"" + roleValue + "\"");
                    }
                }

                this.FieldRestrictions = new List<FieldRestriction>(element.Elements("field").Select(el => new FieldRestriction(el, fieldIDs)));
            }

            /// <summary>
            /// Gets or sets the only roles allowed through in this case.  If empty, any role is permitted.
            /// </summary>
            private List<E_RoleT> Roles { get; set; } // Conceptually this is a HashSet, but since we don't expect it to be large, a list is likely cheaper & faster.

            /// <summary>
            /// Gets or sets the field and value pairs to allow for this case.
            /// </summary>
            private List<FieldRestriction> FieldRestrictions { get; set; }

            /// <summary>
            /// Checks if this case applies to the current role and specified loan.
            /// </summary>
            /// <param name="loanValueEvaluator">The loan to check for the values, or null if skipping the loan value check.</param>
            /// <returns><see langword="true"/> if the necessary roles and loan field values are correct; <see langword="false"/> otherwise.</returns>
            internal bool IsInCase(LoanValueEvaluator loanValueEvaluator)
            {
                bool hasNeededRole = this.Roles.Count == 0 || PrincipalFactory.CurrentPrincipal.HasAtLeastOneRole(this.Roles);
                if (!hasNeededRole || loanValueEvaluator == null || this.FieldRestrictions.Count == 0)
                {
                    return hasNeededRole;
                }

                bool hasNeededValues = this.FieldRestrictions.All(field => field.Values.Contains(loanValueEvaluator.GetSafeScalarString(field.ID, null)));
                return hasNeededValues;
            }

            /// <summary>
            /// Serialize the current element into xml. Note that this will remove any extra data (comments, ignored elements, et cetera).
            /// </summary>
            /// <returns>The XML representation of the item.</returns>
            internal XElement ToXml()
            {
                return new XElement(
                    "RestrictionCase",
                    this.Roles.Select(role => new XElement("role", role.ToString("D"))),
                    this.FieldRestrictions.Select(field => field.ToXml()));
            }
        }

        /// <summary>
        /// Represents a restriction of a field to a list of values.
        /// </summary>
        private class FieldRestriction
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="FieldRestriction"/> class.
            /// </summary>
            /// <param name="element">The XML to read into a new object.</param>
            /// <param name="fieldIDs">A set of field IDs used to evaluate this location's configuration.</param>
            internal FieldRestriction(XElement element, HashSet<string> fieldIDs)
            {
                string fieldName = XmlUtils.GetXAttributeValue(element, "id");
                if (!PageDataUtilities.ContainsField(fieldName))
                {
                    throw new LaunchLinkConfigurationException("Unable to locate field: \"" + fieldName + "\"");
                }

                this.ID = fieldName;
                fieldIDs.Add(this.ID);
                this.Values = new List<string>(element.Elements("value").Select(el => el.Value));
                if (this.Values.Count == 0)
                {
                    throw new LaunchLinkConfigurationException("Field \"" + this.ID + "\" does not have any values. Expected at least one.");
                }
            }

            /// <summary>
            /// Gets the field ID for the loan field.
            /// </summary>
            internal string ID { get; private set; }

            /// <summary>
            /// Gets the list of values as strings for the field.
            /// </summary>
            internal List<string> Values { get; private set; } // Conceptually this is a HashSet, but since we don't expect it to be large, a list is likely cheaper & faster.

            /// <summary>
            /// Serialize the current element into xml. Note that this will remove any extra data (comments, ignored elements, et cetera).
            /// </summary>
            /// <returns>The XML representation of the item.</returns>
            internal XElement ToXml()
            {
                return new XElement(
                    "field",
                    new XAttribute("id", this.ID),
                    this.Values.Select(value => new XElement("value", value)));
            }
        }
    }
}
