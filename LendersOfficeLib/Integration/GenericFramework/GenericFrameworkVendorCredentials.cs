﻿// <copyright file="GenericFrameworkVendorCredentials.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Justin Lara
//  Date:   March 6, 2015
// </summary>
namespace LendersOffice.Integration.GenericFramework
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Represents a set of credentials used by an employee to authenticate with a
    /// generic framework vendor.
    /// </summary>
    public class GenericFrameworkVendorCredentials
    {
        /// <summary>
        /// Stored Procedure: Retrieves a set of user credentials associated with a generic framework vendor.
        /// </summary>
        private static readonly string SpRetrieveGenericFrameworkUserCredentials = "GENERIC_FRAMEWORK_VENDOR_EMPLOYEE_RetrieveLogin";

        /// <summary>
        /// Stored Procedure: Saves a set of user credentials associated with a generic framework vendor.
        /// </summary>
        private static readonly string SpSaveGenericFrameworkUserCredentials = "GENERIC_FRAMEWORK_VENDOR_EMPLOYEE_SaveLogin";

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericFrameworkVendorCredentials" /> class,
        /// which holds a set of credentials used by an employee to authenticate with a generic
        /// framework vendor.
        /// </summary>
        /// <param name="employeeId">Identifies the employee who owns the credentials.</param>
        /// <param name="providerId">Identifies the generic framework vendor associated with the credentials.</param>
        /// <param name="username">The username for the generic framework provider.</param>
        /// <param name="brokerId">Identifies the lender for whom the employee works.</param>
        public GenericFrameworkVendorCredentials(Guid employeeId, string providerId, string username, Guid brokerId)
        {
            this.EmployeeId = employeeId;
            this.ProviderId = providerId;
            this.Username = username;
            this.BrokerId = brokerId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericFrameworkVendorCredentials" /> class,
        /// which holds a set of credentials used by an employee to authenticate with a generic
        /// framework vendor.
        /// </summary>
        /// <param name="employeeId">Identifies the employee who owns the credentials.</param>
        /// <param name="providerId">Identifies the generic framework vendor associated with the credentials.</param>
        /// <param name="brokerId">Identifies the lender for whom the employee works.</param>
        public GenericFrameworkVendorCredentials(Guid employeeId, string providerId, Guid brokerId)
        {
            this.BrokerId = brokerId;
            this.EmployeeId = employeeId;
            this.ProviderId = providerId;

            SqlParameter[] parameters = 
            {
                new SqlParameter("@EmployeeId", this.EmployeeId), 
                new SqlParameter("@ProviderId", this.ProviderId),
                new SqlParameter("@BrokerId", this.BrokerId)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, SpRetrieveGenericFrameworkUserCredentials, parameters))
            {
                this.Username = reader.Read() ? reader["Username"].ToString() : string.Empty;
            }
        }

        /// <summary>
        /// Gets an <see cref="System.Xml.Linq.XElement" /> representation of the user credentials
        /// of the form: &lt;user_credentials Username="______" /&gt;.
        /// </summary>
        /// <value>An <see cref="System.Xml.Linq.XElement" /> representation of the user credentials.</value>
        public XElement UserCredential
        {
            get
            {
                if (string.IsNullOrEmpty(this.Username))
                {
                    return null;
                }

                return new XElement("user_credentials", new XAttribute("Username", this.Username));
            }
        }

        /// <summary>
        /// Gets or sets an identifier for the employee who owns the credentials.
        /// </summary>
        /// <value>An identifier for the employee who owns the credentials.</value>
        public Guid EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets an identifier for the generic framework vendor associated with the credentials.
        /// </summary>
        /// <value>An identifier for the generic framework vendor associated with the credentials.</value>
        public string ProviderId { get; set; }

        /// <summary>
        /// Gets or sets the username for the generic framework provider.
        /// </summary>
        /// <value>The username for the generic framework provider.</value>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets an identifier for the lender for whom the employee works.
        /// </summary>
        /// <value>An identifier for the lender for whom the employee works.</value>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Allows <see cref="GenericFrameworkVendorCredentials" /> object to be cast to
        /// an <see cref="System.Xml.Linq.XElement" /> representation.
        /// </summary>
        /// <param name="login">An instance of <see cref="GenericFrameworkVendorCredentials" /></param>
        /// <returns>An <see cref="System.Xml.Linq.XElement" /> of the form: &lt;user_credentials Username="xxxxxx" /&gt;.</returns>
        public static explicit operator XElement(GenericFrameworkVendorCredentials login)
        {
            return login.UserCredential;
        }

        /// <summary>
        /// Static entry point to retrieve a set of user credentials associated with a vendor.
        /// </summary>
        /// <param name="employeeId">Identifies the employee who owns the credentials.</param>
        /// <param name="providerId">Identifies the generic framework vendor associated with the credentials.</param>
        /// <param name="brokerId">Identifies the lender for whom the employee works.</param>
        /// <returns>An XElement representation of the credentials.</returns>
        public static XElement GetUserCredentials(Guid employeeId, string providerId, Guid brokerId)
        {
            var credentials = new GenericFrameworkVendorCredentials(employeeId, providerId, brokerId);
            return credentials.UserCredential;
        }

        /// <summary>
        /// Saves this credential object to the database.
        /// </summary>
        public void SaveCredentials()
        {
            SqlParameter[] parameters = 
            {
                new SqlParameter("@EmployeeId", this.EmployeeId),
                new SqlParameter("@ProviderId", this.ProviderId),
                new SqlParameter("@Username", this.Username),
                new SqlParameter("@BrokerId", this.BrokerId),
            };

            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, SpSaveGenericFrameworkUserCredentials, 3, parameters);
        }
    }
}
