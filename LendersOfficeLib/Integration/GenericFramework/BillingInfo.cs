﻿// <copyright file="BillingInfo.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   2/28/2015 2:21:37 PM
// </summary>
namespace LendersOffice.Integration.GenericFramework
{
    using System;
    using System.Data.Common;
using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// Represents Generic Framework data to be used for billing on a per transaction basis.
    /// </summary>
    public class BillingInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BillingInfo"/> class.
        /// </summary>
        /// <param name="vendor">The vendor involved in the Generic Framework Billing Transaction.</param>
        public BillingInfo(GenericFrameworkVendor vendor)
            : this(vendor.ProviderID, vendor.ServiceType)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BillingInfo"/> class.
        /// </summary>
        /// <param name="providerID">The <see cref="GenericFrameworkVendor.ProviderID"/> of the vendor.</param>
        /// <param name="serviceType">The service type of the vendor.</param>
        internal BillingInfo(string providerID, TypeOfService serviceType)
        {
            this.ProviderID = providerID;
            this.ServiceType = serviceType;
        }

        /// <summary>
        /// Gets the <see cref="GenericFrameworkVendor.ProviderID"/>.
        /// </summary>
        /// <value>The <see cref="GenericFrameworkVendor.ProviderID"/> of the vendor.</value>
        public string ProviderID { get; private set; }

        /// <summary>
        /// Gets or sets the service type used in this transaction, which is <see cref="GenericFrameworkVendor.ServiceType"/> for the vendor if the vendor only provides a single service type.
        /// </summary>
        /// <value>The service type used in this transaction, which is <see cref="GenericFrameworkVendor.ServiceType"/> for the vendor if the vendor only provides a single service type.</value>
        public TypeOfService ServiceType { get; set; }

        /// <summary>
        /// Gets a value indicating whether a <see cref="ServiceType"/> needs to be specified in order to bill the transaction.
        /// </summary>
        /// <value><see langword="true"/> if the current value of <see cref="ServiceType"/> is Multiple; <see langword="false"/> otherwise.</value>
        public bool NeedsServiceType
        {
            get { return this.ServiceType == TypeOfService.Multiple; }
        }

        /// <summary>
        /// Records a document upload transaction to the database.
        /// </summary>
        /// <param name="loanID">The <see cref="CPageData.sLId"/> of the loan the document was added to.</param>
        /// <param name="documentId">The document id of the document that was added to EDocs.</param>
        /// <exception cref="GenericUserErrorMessageException"><see cref="NeedsServiceType"/> is <see langword="true"/>.</exception>
        public void RecordDocumentUpload(Guid loanID, Guid documentId)
        {
            if (this.NeedsServiceType)
            {
                throw new GenericUserErrorMessageException("Service Type must be specified to record the transaction.");
            }

            Guid brokerId = PrincipalFactory.CurrentPrincipal.BrokerId;
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("LoanID", loanID),
                new SqlParameter("BrokerID", brokerId),
                new SqlParameter("ServiceType", (int)this.ServiceType),
                new SqlParameter("ProviderID", this.ProviderID),
                new SqlParameter("DocumentID", documentId)
            };
            StoredProcedureHelper.ExecuteNonQuery(brokerId, "GENERIC_FRAMEWORK_BILLING_Insert", 5, parameters);
        }
    }
}
