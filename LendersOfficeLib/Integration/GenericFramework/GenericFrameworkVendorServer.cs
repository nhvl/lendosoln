﻿// <copyright file="GenericFrameworkVendorServer.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   9/3/2014 3:03:59 PM
// </summary>
namespace LendersOffice.Integration.GenericFramework
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// Provides connection to Generic Framework vendors.
    /// </summary>
    public static class GenericFrameworkVendorServer
    {
        /// <summary>
        /// Stores the value of <see cref="UTF8"/>. Not to be accessed directly.
        /// </summary>
        private static Encoding utf8;

        /// <summary>
        /// Gets the UTF-8 encoding without a byte-order mark.
        /// </summary>
        private static Encoding UTF8
        {
            get
            {
                if (utf8 == null)
                {
                    utf8 = new UTF8Encoding(false);
                }

                return utf8;
            }
        }

        /// <summary>
        /// Sends the request to the launch url to load the information for the pop-up window.
        /// </summary>
        /// <param name="brokerID">The broker ID of the lender making the request.</param>
        /// <param name="providerID">The provider ID identifying the generic vendor to send.</param>
        /// <param name="loanID">The loan ID of the loan, used for the restriction of the vendor's authentication ticket.</param>
        /// <param name="location">The location in the UI sending the request. Used to check permission settings.</param>
        /// <returns>An object representing the vendor's response.</returns>
        /// <exception cref="LendersOffice.Common.GenericUserErrorMessageException">The specified provider ID does not correspond to a broker-specific instance of a generic framework vendor.</exception>
        public static LaunchUrlResponse SubmitRequest(Guid brokerID, string providerID, Guid loanID, LinkLocation location)
        {
            var vendor = GenericFrameworkVendor.LoadVendor(brokerID, providerID);
            if (vendor == null || !vendor.IsBrokerInstance)
            {
                throw new GenericUserErrorMessageException("Generic vendor " + providerID + " failed to load the broker-specific data for " + PrincipalFactory.CurrentPrincipal.BrokerDB.CustomerCode + ". General vendor was " + (vendor == null ? "not " : null) + "loaded.");
            }
            else if (!vendor.LaunchLinkConfig.IsDisplayLinkForLoanAndUser(loanID, location))
            {
                throw new AccessDenied("Permission for this Generic Framework Vendor not accepted for LoanID=[" + loanID + "], User=[" + PrincipalFactory.CurrentPrincipal.UserId + "], Login=[" + PrincipalFactory.CurrentPrincipal.LoginNm + "]");
            }

            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;
            var userCredentials = GenericFrameworkVendorCredentials.GetUserCredentials(principal.EmployeeId, vendor.ProviderID, principal.BrokerId);

            if (vendor.IncludeUsername && userCredentials == null)
            {
                throw new DataAccess.CBaseException(ErrorMessages.GenericFramework.UsernameRequired, "Occurred for vendor with ProviderId " + vendor.ProviderID);
            }
            else if (vendor.CredentialXML == null && userCredentials == null)
            {
                throw new GenericUserErrorMessageException("Cannot send request to " + vendor.Name + ". There are no vendor or user credentials on file associated with ProviderId " + vendor.ProviderID + ".");
            }

            BillingInfo billingInfo = new BillingInfo(vendor);
            string loanNumber = vendor.LoanIdentifierTypeToSend.HasFlag(TypeOfLoanIdentifier.LoanNumber) ? DataAccess.Tools.GetLoanNameByLoanId(brokerID, loanID) : null;
            string loanRefNumber = DataAccess.Tools.GetLoanRefNmByLoanId(brokerID, loanID); // ideally, we would combine the data loading for sLNm and sLRefNm, but this is still cheaper than a loan object
            XElement xmlRequest = GenerateLaunchXml(vendor.CredentialXML, userCredentials, principal, vendor.LoanIdentifierTypeToSend, loanNumber, loanRefNumber, billingInfo);
            DataAccess.Tools.LogInfo("[GenericFramework] " + vendor.ProviderID + ", LaunchURL=" + vendor.LaunchURL + Environment.NewLine + "Request: " + Environment.NewLine + GenerateLaunchXml(null, userCredentials, principal, vendor.LoanIdentifierTypeToSend, loanNumber, loanRefNumber, billingInfo));

            string response;
            using (WebResponse webResponse = SendWebRequest(vendor.LaunchURL, xmlRequest))
            {
                response = ReadResponse(webResponse);
            }

            DataAccess.Tools.LogInfo("[GenericFramework] Response:" + Environment.NewLine + response);
            return LaunchUrlResponse.DeserializeLaunchUrlResponse(response, vendor.Name);
        }

        /// <summary>
        /// Creates the XML of the request to be sent to the launch URL.
        /// </summary>
        /// <param name="lenderCredentialXML">The lender-level credential to be sent.</param>
        /// <param name="userCredentialXML">The user-level credential to be sent.</param>
        /// <param name="principal">The current <see cref="AbstractUserPrincipal" /></param>
        /// <param name="loanIdentifierTypeToSend">The type of loan identifier the vendor will consume.</param>
        /// <param name="loanNumber">The loan number to be sent.</param>
        /// <param name="loanReferenceNumber">The loan reference number, to be sent in the XML and also used for the restriction of the vendor's authentication ticket.</param>
        /// <param name="billingInfo">The billing information of the vendor of the current transaction.</param>
        /// <returns>The xml string to be sent to the launch url for the vendor.</returns>
        private static XElement GenerateLaunchXml(XElement lenderCredentialXML, XElement userCredentialXML, AbstractUserPrincipal principal, TypeOfLoanIdentifier loanIdentifierTypeToSend, string loanNumber, string loanReferenceNumber, BillingInfo billingInfo)
        {
            return new XElement(
                "LQBGenericFrameworkRequest",
                new XElement("CredentialXML", lenderCredentialXML, userCredentialXML),
                loanIdentifierTypeToSend.HasFlag(TypeOfLoanIdentifier.LoanNumber) ? new XElement("LoanNumber", loanNumber) : null,
                loanIdentifierTypeToSend.HasFlag(TypeOfLoanIdentifier.LoanReferenceNumber) ? new XElement("LoanReferenceNumber", loanReferenceNumber) : null,
                new XElement("UserLogin", principal.LoginNm),
                new XElement("LendingQBLoanCredential", AuthServiceHelper.GetGenericFrameworkUserAuthTicket(principal, loanReferenceNumber, billingInfo)),
                new XElement("LendingQBWebServiceDomain", Constants.ConstStage.GenericFrameworkWebServiceDomain));
        }

        /// <summary>
        /// Creates a new web request to the specified url and posts the xml data, returning the response.
        /// </summary>
        /// <param name="url">The url receiving the web request.</param>
        /// <param name="xmlRequest">The xmlRequest to send.</param>
        /// <returns>The web response returned by the request.</returns>
        private static WebResponse SendWebRequest(string url, XElement xmlRequest)
        {
            byte[] bytes = UTF8.GetBytes(xmlRequest.ToString(SaveOptions.DisableFormatting));
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.KeepAlive = false; // 11/4/2004 dd - DO NOT REMOVE THIS LINE. Else you will get "Cannot access a disposed object named "System.Net.TlsStream"" error randomly.
            webRequest.Method = "POST";
            webRequest.ContentType = "text/xml";
            webRequest.ContentLength = bytes.Length;
            LendersOffice.ObjLib.Security.ThirdParty.ThirdPartyClientCertificateManager.AppendCertificate(webRequest);

            using (Stream stream = webRequest.GetRequestStream())
            {
                stream.Write(bytes, 0, bytes.Length);
            }

            return webRequest.GetResponse();
        }

        /// <summary>
        /// Reads a web response to a string, assuming utf-8 encoding.
        /// </summary>
        /// <param name="webResponse">The web response to read.</param>
        /// <returns>The response as a string.</returns>
        private static string ReadResponse(WebResponse webResponse)
        {
            StringBuilder sb = new StringBuilder();
            using (Stream stream = webResponse.GetResponseStream())
            {
                byte[] buffer = new byte[60000];
                int size = stream.Read(buffer, 0, buffer.Length);
                while (size > 0)
                {
                    string chunk = UTF8.GetString(buffer, 0, size);
                    sb.Append(chunk);
                    size = stream.Read(buffer, 0, buffer.Length);
                }
            }

            return sb.ToString();
        }
    }
}
