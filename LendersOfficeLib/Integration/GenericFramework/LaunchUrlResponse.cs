﻿// <copyright file="LaunchUrlResponse.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Timothy Jewell
//  Date:   9/4/2014 5:19:01 PM
// </summary>
namespace LendersOffice.Integration.GenericFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Represents the response from the initial post to the Launch URL of the vendor.
    /// </summary>
    public class LaunchUrlResponse
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LaunchUrlResponse"/> class.
        /// </summary>
        /// <param name="responseXml">The xml response sent by the vendor.</param>
        /// <param name="vendorName">The name of the vendor, used for user error messages.</param>
        private LaunchUrlResponse(XElement responseXml, string vendorName)
        {
            var window = responseXml.Element("Window");
            if (window != null)
            {
                this.Window = new Popup(window, vendorName);
            }
            else
            {
                var error = responseXml.Element("Error");
                if (error != null)
                {
                    this.ErrorMessage = XmlUtils.GetXAttributeValue(error, "message");
                }
            }
        }

        /// <summary>
        /// Gets or sets the error message returned by the vendor.
        /// </summary>
        /// <value>The error message returned by the vendor.</value>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Gets the specified information about a pop-up window stated by the vendor.
        /// </summary>
        /// <value>The pop-up window's information.</value>
        public Popup Window { get; private set; }

        /// <summary>
        /// Reads the XML response from a generic framework vendor's launch url into an object.
        /// </summary>
        /// <param name="xmlResponse">The XML to read.</param>
        /// <param name="vendorName">The name of the vendor, used for user error messages.</param>
        /// <returns>An object representing the response of the vendor.</returns>
        public static LaunchUrlResponse DeserializeLaunchUrlResponse(string xmlResponse, string vendorName)
        {
            if (string.IsNullOrEmpty(xmlResponse))
            {
                throw new CBaseException(ErrorMessages.GenericFramework.ReceivedNoData(vendorName), "xmlResponse from Generic Framework vendor " + vendorName + " was null or empty");
            }

            try
            {
                return new LaunchUrlResponse(XElement.Parse(xmlResponse), vendorName);
            }
            catch (System.Xml.XmlException exc)
            {
                Tools.LogWarning("[GenericFramework] LaunchURL returned invalid xml:" + Environment.NewLine + xmlResponse, exc);
                throw new CBaseException(ErrorMessages.GenericFramework.ReceivedInvalidXml(vendorName), "Generic Framework LaunchURL returned invalid xml: " + xmlResponse);
            }
        }

        /// <summary>
        /// Represents the specifications of an external pop-up window to be created for the vendor.
        /// </summary>
        public class Popup
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="Popup"/> class.
            /// </summary>
            /// <param name="popupXml">The XML element corresponding to the pop-up window.</param>
            /// <param name="vendorName">The name of the vendor, used for user error messages.</param>
            internal Popup(XElement popupXml, string vendorName)
            {
                Uri popupUrl;
                if (Uri.TryCreate(XmlUtils.GetXAttributeValue(popupXml, "url"), UriKind.Absolute, out popupUrl)
                    && (popupUrl.Scheme == Uri.UriSchemeHttp || popupUrl.Scheme == Uri.UriSchemeHttps))
                {
                    this.Url = popupUrl;
                }
                else
                {
                    throw new CBaseException(ErrorMessages.GenericFramework.ReceivedInvalidUrl(vendorName), "Unable to load GenericFramework vendor popup due to invalid url from vendor.  URL=" + XmlUtils.GetXAttributeValue(popupXml, "url"));
                }

                int value;
                const int DefaultWindowDimension = 300;
                const int MinimumWindowDimension = 100;
                const int MaximumWindowDimension = int.MaxValue;
                this.Height = int.TryParse(XmlUtils.GetXAttributeValue(popupXml, "height"), out value)
                    && MinimumWindowDimension <= value && value <= MaximumWindowDimension ? value : DefaultWindowDimension;
                this.Width = int.TryParse(XmlUtils.GetXAttributeValue(popupXml, "width"), out value)
                    && MinimumWindowDimension <= value && value <= MaximumWindowDimension ? value : DefaultWindowDimension;

                this.IsModal = (XmlUtils.GetXAttributeValue(popupXml, "modalIndicator") ?? string.Empty).StartsWith("Y", StringComparison.OrdinalIgnoreCase);
            }

            /// <summary>
            /// Gets the url of the pop-up window.
            /// </summary>
            /// <value>The url of the pop-up window.</value>
            public Uri Url { get; private set; }

            /// <summary>
            /// Gets the vertical dimension of the pop-up window in pixels.
            /// </summary>
            /// <value>The vertical dimension of the pop-up window in pixels.</value>
            public int Height { get; private set; }

            /// <summary>
            /// Gets the horizontal dimension of the pop-up window in pixels.
            /// </summary>
            /// <value>The horizontal dimension of the pop-up window in pixels.</value>
            public int Width { get; private set; }

            /// <summary>
            /// Gets a value indicating whether the pop-up window is a modal dialog.
            /// </summary>
            /// <value>A value indicating whether the pop-up window is a modal dialog.</value>
            public bool IsModal { get; private set; }
        }
    }
}
