﻿namespace LendersOffice.Integration.LoanProductAdvisor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Conversions.LoanProspector;
    using LendersOffice.ObjLib.ServiceCredential;

    /// <summary>
    /// A helper to call the loan transfer request operation.
    /// </summary>
    public static class LoanTransferRequest
    {
        /// <summary>
        /// Performs the loan transfer request operation and subsequent import operation into a loan.
        /// </summary>
        /// <param name="request">The request from the UI or other part of the system.</param>
        /// <returns>The result, which indicates if the data was successfully loaded from LPA and imported into a loan.</returns>
        public static LoanTransferRequestOutput RunLoanTransferRequest(LoanTransferRequestInput request)
        {
            LpaTransferRequestData requestData = CreateRequestData(request);

            string error;
            if (!requestData.Validate(out error))
            {
                return LoanTransferRequestOutput.CreateErrorResult(new List<string>() { error });
            }

            LpaRequestHandler requestHandler = new LpaRequestHandler(requestData);
            var results = requestHandler.SubmitRequest();

            if (results.Status != LpaRequestResultStatus.Completed)
            {
                return LoanTransferRequestOutput.CreateErrorResult(((LpaRequestErrorResult)results).Errors);
            }

            Guid loanId = request.LoanResolution.LoanId;
            CLoanFileCreator loanCreator = null;
            if (request.LoanResolution.CreateNewLoan)
            {
                loanCreator = CLoanFileCreator.GetCreator(request.Principal, LendersOffice.Audit.E_LoanCreationSource.FreddieMacImport);
                if (request.LoanResolution.CreateLead)
                {
                    loanId = loanCreator.BeginCreateLead(request.LoanResolution.CreateFromTemplateId);
                }
                else
                {
                    loanId = loanCreator.BeginCreateImportBaseLoanFile(
                        sourceFileId: request.LoanResolution.CreateFromTemplateId,
                        setInitialEmployeeRoles: true,
                        addEmployeeOfficialAgent: true,
                        assignEmployeesFromRelationships: true,
                        branchIdToUse: request.LoanBranchId);
                }
            }

            LoanProspectorImporter importer = LoanProspectorImporter.CreateImporterForTransfer(results.Response, isImport1003: true, isImportCreditReport: request.ShouldImportCredit, loanId: loanId);
            importer.Import();

            loanCreator?.CommitFileCreation(false);
            return LoanTransferRequestOutput.CreateSuccessfulResult(loanId);
        }

        /// <summary>
        /// Creates the raw request data for the backend to process.
        /// </summary>
        /// <param name="request">The request for the loan transfer request operation.</param>
        /// <returns>A request data for processing with the necessary data loaded for it.</returns>
        private static LpaTransferRequestData CreateRequestData(LoanTransferRequestInput request)
        {
            var ausServiceCredentials = ServiceCredential.ListAvailableServiceCredentials(request.Principal, request.LoanBranchId, ServiceCredentialService.AusSubmission);
            var userCredentials = ausServiceCredentials.FirstOrDefault(credential => credential.ChosenAusOption == AusOption.LpaUserLogin);
            var sellerCredentials = ausServiceCredentials.FirstOrDefault(credential => credential.ChosenAusOption == AusOption.LpaSellerCredential);
            string lpaUserId = request.LpaUserId;
            string lpaUserPassword = request.LpaUserPassword;
            if (userCredentials != null)
            {
                lpaUserId = userCredentials.UserName;
                lpaUserPassword = userCredentials.UserPassword.Value;
            }

            string lpaSellerNumber = request.LpaSellerNumber;
            string lpaSellerPassword = request.LpaSellerPassword;
            if (sellerCredentials != null)
            {
                lpaSellerNumber = sellerCredentials.UserName;
                lpaSellerPassword = sellerCredentials.UserPassword.Value;
            }

            var requestData = new LpaTransferRequestData(lpaUserId, lpaUserPassword, request.LoanResolution.LoanId, request.Principal);
            requestData.LpaSellerNumber = lpaSellerNumber;
            requestData.LpaSellerPassword = lpaSellerPassword;
            requestData.LpaAusKey = request.LpaAusKey;
            requestData.LpaLoanId = request.LpaLoanId;
            requestData.LpaTransactionId = request.LpaTransactionId;
            requestData.LpaNotpNumber = request.LpaNotpNumber;
            return requestData;
        }
    }
}
