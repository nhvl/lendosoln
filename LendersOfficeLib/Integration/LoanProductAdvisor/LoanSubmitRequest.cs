﻿namespace LendersOffice.Integration.LoanProductAdvisor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common;
    using Conversions.LoanProspector;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Conversions.Aus;
    using LendersOffice.ObjLib.Conversions.LoanProspector;
    using LendersOffice.ObjLib.Conversions.Templates;
    using ObjLib.BackgroundJobs;
    using ObjLib.Conversions.LoanProspector.Seamless;
    using ObjLib.ServiceCredential;
    using Security;

    /// <summary>
    /// Provides methods to call into backend processes from the SeamlessLpaService.aspx.cs service page.
    /// </summary>
    public static class LoanSubmitRequest
    {
        /// <summary>
        /// Gets the Freddie Mac CRA info key for FileDB.
        /// </summary>
        /// <param name="loanId">The loan ID for the loan to retrieve Freddie CRA info for.</param>
        /// <returns>The Freddie Mac CRA key.</returns>
        public static string FreddieCraKey(Guid loanId)
        {
            return "FreddieCredit_" + loanId;
        }
        
        /// <summary>
        /// Saves the CRA information to the auto expire text cache for 5 minutes. This will be used when calling an LPA export on the given loan.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="creditInfo">The CRA information.</param>
        public static void SaveTemporaryCRAInformation(Guid loanId, AusCreditOrderingInfo creditInfo)
        {
            FileDBTools.WriteJsonNetObject(E_FileDB.Normal, "FreddieCredit_" + loanId, creditInfo);
        }

        /// <summary>
        /// Pulls the temporary CRA information for the loan.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <returns>The <see cref="AusCreditOrderingInfo"/> containing the cra information.</returns>
        public static AusCreditOrderingInfo GetTemporaryCraInformation(Guid loanId)
        {
            string key = FreddieCraKey(loanId);
            if (FileDBTools.DoesFileExist(E_FileDB.Normal, key))
            {
                return FileDBTools.ReadJsonNetObject<AusCreditOrderingInfo>(E_FileDB.Normal, key);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Saves the login options from the UI login model, verifies that the LPA audit passes, and then 
        /// submits an LPA underwriting request to the LPA system, using data from the LPA seamless login page.
        /// </summary>
        /// <param name="loginModel">The login model object sent from angular in the UI.</param>
        /// <param name="loanId">The loan ID of the loan to be submitted for underwriting.</param>
        /// <param name="user">The user making the submission request.</param>
        /// <param name="isPml">Whether or not the request is being made from within PML.</param>
        /// <param name="prioritizeUiCredentials">Whether or not we should should prioritize the UI credentials over the ServiceCredentials.</param>
        /// <param name="auditErrorResult">If there are any audit errors after the loan has been modified, then they will be returned here.</param>
        /// <returns>A model object representing the response data to be returned to the UI.</returns>
        public static LpaResponseViewModel SaveOptionsAndSubmit(LpaSeamlessLoginModel loginModel, Guid loanId, AbstractUserPrincipal user, bool isPml, bool prioritizeUiCredentials, out LoanAuditResult auditErrorResult)
        {
            auditErrorResult = null;
            SaveTemporaryCRAInformation(loanId, loginModel.CreditInfo);
            var loanData = CPageData.CreateUsingSmartDependency(loanId, typeof(LoanSubmitRequest));
            loanData.InitSave(ConstAppDavid.SkipVersionCheck);
            loginModel.ApplyToLoanFile(loanData);
            loanData.Save();

            LoanAuditResult auditResult = LoanDataAuditor.RunAudit(loanId, LpaSeamlessAuditConditions.Singleton, isPml);
            int errorCount = auditResult.LoanErrors.Count() + auditResult.BorrowerAudits.Sum(borrAudit => borrAudit.Errors.Count());
            if (errorCount > 0)
            {
                var errorResult = new LpaResponseViewModel()
                {
                    Status = AusResponseStatus.AuditFailure,
                    ErrorMessage = $"{errorCount} Audit condition{(errorCount == 1 ? string.Empty : "s")} failed. Please fix invalid data points and re-run."
                };

                auditErrorResult = auditResult;
                return errorResult;
            }

            string validationError;
            LpaRequestResult result = SubmitRequest(loanData, loginModel, user, prioritizeUiCredentials, out validationError);
            if (result == null)
            {
                return new LpaResponseViewModel()
                {
                    Status = AusResponseStatus.Error,
                    ErrorMessage = validationError,
                };
            }

            return CreateResponseViewModel(result, loanId, isPml, user);
        }

        /// <summary>
        /// Checks the background polling job for Seamless LPA.
        /// </summary>
        /// <param name="publicJobId">The public job id.</param>
        /// <param name="loanId">The LQB loan ID of the loan being polled for.</param>
        /// <param name="isPml">Whether or not the request is being made from within PML.</param>
        /// <param name="principal">The principal creating the request.</param>
        /// <returns>A model object representing the response data to be returned to the UI.</returns>
        public static LpaResponseViewModel Poll(Guid publicJobId, Guid loanId, bool isPml, AbstractUserPrincipal principal)
        {
            SeamlessLpaPollJobResult pollResult = SeamlessLpaPollJob.Check(publicJobId);
            if (pollResult == null)
            {
                // No job was found in this case.
                var errorResult = new LpaResponseViewModel()
                {
                    Status = AusResponseStatus.Error,
                    ErrorMessage = "Unable to retrieve LPA underwriting response. The request may have expired."
                };

                return errorResult;
            }

            var jobStatus = pollResult.ProcessingStatus;
            switch (jobStatus)
            {
                case ProcessingStatus.Completed:
                    return CreateResponseViewModel(pollResult.CompletedRequestResult, loanId, isPml, principal);
                case ProcessingStatus.Invalid:
                    var errorResult = new LpaResponseViewModel()
                    {
                        Status = AusResponseStatus.Error,
                        ErrorMessage = string.Join(Environment.NewLine, pollResult.InvalidErrorResult),
                    };

                    return errorResult;
                case ProcessingStatus.Incomplete:
                    // We checked the background job processor and the job isn't done. We'll switch to checking more frequently in case we just barely
                    // missed grabbing the job results in the first pass.
                    int fasterPollingIntervalInSeconds = pollResult.PollingIntervalInSeconds / 2;
                    if (pollResult.PollingIntervalInSeconds + (fasterPollingIntervalInSeconds * 19) < 2 * 60)
                    {
                        // We want to poll a minimum of 2 minutes so we'll adjust the faster interval to pad out the rest of the time.
                        // Getting this exact isn't too big of an issue.
                        fasterPollingIntervalInSeconds = ((2 * 60) - pollResult.PollingIntervalInSeconds) / 19;
                    }
                    
                    return CreatePartialViewModel(publicJobId, pollResult.PollAsyncId, fasterPollingIntervalInSeconds * 1000, loanId, isPml);
                default:
                    throw new UnhandledEnumException(jobStatus);
            }
        }

        /// <summary>
        /// Imports the last seamless LPA response into the loan file.
        /// </summary>
        /// <param name="loanId">The loan to import into.</param>
        /// <param name="importFindings">Whether to import findings.</param>
        /// <param name="importCreditReport">Whether to import credit report.</param>
        /// <param name="importLiabilities">Whether to import liabilities.</param>
        /// <returns>The feedback edoc id if saved. Null if not.</returns>
        public static Guid? ImportSubmissionResponse(Guid loanId, bool importFindings, bool importCreditReport, bool importLiabilities)
        {
            var savedResponse = LoanProspectorResponseParser.Parse(FileDBTools.ReadDataText(E_FileDB.Normal, $"LpaResponse_{loanId:N}"));
            var importer = LoanProspectorImporter.CreateImporterForSeamless(savedResponse, importFindings, importCreditReport, importLiabilities, loanId);
            importer.Import();

            return importer.FeedbackEdocId;
        }

        /// <summary>
        /// Builds a submission request data object and submits it to LPA through the <see cref="LpaRequestHandler"/>.
        /// </summary>
        /// <param name="loanData">A loan data object representing the loan to be submitted.</param>
        /// <param name="loginModel">The login model information passed in from the UI.</param>
        /// <param name="user">The user making the submission request.</param>
        /// <param name="prioritizeUiCredentials">Whether we should prioritize the UI credentials over the service credentials.</param>
        /// <param name="validationError">The error if the request data is not valid.</param>
        /// <returns>The result of the submission to LPA. Null if the request data is not valid.</returns>
        private static LpaRequestResult SubmitRequest(CPageData loanData, LpaSeamlessLoginModel loginModel, AbstractUserPrincipal user, bool prioritizeUiCredentials, out string validationError)
        {
            var requestData = CreateSubmitRequestData(loanData, loginModel, user, prioritizeUiCredentials, out validationError);
            if (requestData == null)
            {
                return null;
            }

            return new LpaRequestHandler(requestData).SubmitRequest();
        }

        /// <summary>
        /// Creates the submit request data.
        /// </summary>
        /// <param name="loanData">The data loan to pull from.</param>
        /// <param name="loginModel">The login model from the UI.</param>
        /// <param name="user">The user principal.</param>
        /// <param name="prioritizeUiCreds">Whether the credentials from the UI should be prioritized.</param>
        /// <param name="validationError">Any validation errors.</param>
        /// <returns>The request data if valid. Null otherwise.</returns>
        private static LpaSubmitRequestData CreateSubmitRequestData(CPageData loanData, LpaSeamlessLoginModel loginModel, AbstractUserPrincipal user, bool prioritizeUiCreds, out string validationError)
        {
            var ausCredentials = ServiceCredential.ListAvailableServiceCredentials(user, loanData.sBranchId, ServiceCredentialService.AusSubmission);
            var userCredentials = ausCredentials.FirstOrDefault(cred => cred.ChosenAusOption == AusOption.LpaUserLogin);
            var sellerCredentials = ausCredentials.FirstOrDefault(cred => cred.ChosenAusOption == AusOption.LpaSellerCredential);

            string userId;
            string userPassword;
            string sellerNumber;
            string sellerPassword;

            // If prioritizing UI credentials, we want to pull from the login model first.
            if (prioritizeUiCreds)
            {
                userId = string.IsNullOrEmpty(loginModel.LpaCredentials.LpaUserId) ? userCredentials?.UserName : loginModel.LpaCredentials.LpaUserId;
                userPassword = string.IsNullOrEmpty(loginModel.LpaCredentials.LpaPassword) ? userCredentials?.UserPassword.Value : loginModel.LpaCredentials.LpaPassword;
                sellerNumber = string.IsNullOrEmpty(loginModel.LpaCredentials.LpaSellerNumber) ? (sellerCredentials?.UserName ?? loanData.sFreddieSellerNum) : loginModel.LpaCredentials.LpaSellerNumber;
                sellerPassword = string.IsNullOrEmpty(loginModel.LpaCredentials.LpaSellerPassword) ? (sellerCredentials?.UserPassword.Value ?? loanData.sFreddieLpPassword.Value) : loginModel.LpaCredentials.LpaSellerPassword;
            }
            else
            {
                userId = userCredentials?.UserName ?? loginModel.LpaCredentials.LpaUserId;
                userPassword = userCredentials?.UserPassword.Value ?? loginModel.LpaCredentials.LpaPassword;
                sellerNumber = sellerCredentials?.UserName ?? (string.IsNullOrEmpty(loginModel.LpaCredentials.LpaSellerNumber) ? loanData.sFreddieSellerNum : loginModel.LpaCredentials.LpaSellerNumber);
                sellerPassword = sellerCredentials?.UserPassword.Value ?? (string.IsNullOrEmpty(loginModel.LpaCredentials.LpaSellerPassword) ? loanData.sFreddieLpPassword.Value : loginModel.LpaCredentials.LpaSellerPassword);
            }

            LpaSubmitRequestData requestData = new LpaSubmitRequestData(
                LpaRequestType.SeamlessLpaSubmit,
                username: userId,
                password: userPassword,
                sellerNumber: sellerNumber,
                sellerPassword: sellerPassword,
                loanId: loanData.sLId,
                principal: user,
                craInfo: loginModel.CreditInfo);

            if (!requestData.Validate(out validationError))
            {
                return null;
            }

            return requestData;
        }

        /// <summary>
        /// Converts a <see cref="LpaRequestResult"/> to a <see cref="LpaResponseViewModel"/> for returning to the UI.
        /// </summary>
        /// <param name="requestResult">The request result to convert.</param>
        /// <param name="loanId">The loan ID of the loan.</param>
        /// <param name="isPml">Whether or not the response is being displayed in PML.</param>
        /// <param name="principal">The principal.</param>
        /// <returns>A new response view model to be sent to the UI.</returns>
        private static LpaResponseViewModel CreateResponseViewModel(LpaRequestResult requestResult, Guid loanId, bool isPml, AbstractUserPrincipal principal)
        {
            if (requestResult is LpaRequestErrorResult)
            {
                var errorResult = (LpaRequestErrorResult)requestResult;
                var responseViewModel = new LpaResponseViewModel()
                {
                    Status = AusResponseStatus.Error,
                    ErrorMessage = string.Join(Environment.NewLine, errorResult.Errors),
                    Errors = errorResult.Errors
                };

                var errorResultFromFreddie = errorResult?.Response?.Input;
                if (!string.IsNullOrEmpty(errorResultFromFreddie))
                {
                    string errorKey = AutoExpiredTextCache.InsertUserString(principal, errorResultFromFreddie, TimeSpan.FromHours(12));
                    responseViewModel.LpaResponseUrl = ResponseUrlBase(isPml) + "error=" + errorKey;
                }

                return responseViewModel;
            }
            else if (requestResult is LpaRequestPartialResult)
            {
                var partialResult = (LpaRequestPartialResult)requestResult;
                return CreatePartialViewModel(partialResult.PublicJobid, partialResult.AsyncId, partialResult.PollingInterval * 1000, loanId, isPml);
            }
            else if (requestResult is LpaRequestCompleteResult)
            {
                return CreateCompleteResponseViewModel((LpaRequestCompleteResult)requestResult, loanId, isPml, principal);
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, "Unexpected LpaRequestResult type: " + requestResult.GetType());
            }
        }

        /// <summary>
        /// Creates a viewmodel for a partial/poll result.
        /// </summary>
        /// <param name="publicJobId">The public background job id.</param>
        /// <param name="asyncId">The async id from Freddie.</param>
        /// <param name="pollingPeriodMs">The polling interval in milliseconds.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="isPml">Whether this is from PML or not.</param>
        /// <returns>The view model.</returns>
        private static LpaResponseViewModel CreatePartialViewModel(Guid publicJobId, string asyncId, int pollingPeriodMs, Guid loanId, bool isPml)
        {
            return new LpaResponseViewModel()
            {
                PublicJobId = publicJobId,
                Status = AusResponseStatus.Processing,
                AsyncIdentifier = asyncId,
                PollingPeriodMs = pollingPeriodMs,
                LpaResponseUrl = ResponseUrlBase(isPml) + "loanid=" + loanId.ToString()
            };
        }

        /// <summary>
        /// Gets the URL for viewing the response from LPA.
        /// </summary>
        /// <param name="isPml">Whether the request is from PML.</param>
        /// <returns>The response URL to view the response from LPA.</returns>
        private static string ResponseUrlBase(bool isPml)
        {
            return isPml ? "/main/ViewLPFeedback.aspx?" : "/newlos/services/ViewLPFeedback.aspx?";
        }

        /// <summary>
        /// Converts a <see cref="LpaRequestCompleteResult"/> to a <see cref="LpaResponseViewModel"/> for returning to the UI.
        /// There is special handling for the complete result, so it's separated into a separate method here.
        /// </summary>
        /// <param name="completeResult">The completed request's result.</param>
        /// <param name="loanId">The loan ID of the loan to save a successful response for.</param>
        /// <param name="isPml">Whether or not the response is being displayed in PML.</param>
        /// <param name="principal">The user principal.</param>
        /// <returns>A new response view model to be sent to the UI.</returns>
        private static LpaResponseViewModel CreateCompleteResponseViewModel(LpaRequestCompleteResult completeResult, Guid loanId, bool isPml, AbstractUserPrincipal principal)
        {
            var lpaResultsModel = new LpaResponseViewModel
            {
                Status = AusResponseStatus.Done,
                LpaResponseUrl = ResponseUrlBase(isPml) + "loanid=" + loanId.ToString()
            };
            if (completeResult.Response.IsSuccessful && completeResult.Response.ValueOrDefault?.Status.Processing == LoanProspectorResponse.E_MismoStatusProcessing.Completed)
            {
                lpaResultsModel.UnderwritingSuccess = true;
                DateTime? resultsDate = completeResult.Response.ValueOrDefault.LoanFeedback.LoanProspectorEvaluationDate.ToNullable<DateTime>(DateTime.TryParse);
                lpaResultsModel.ImportFindings = principal.BrokerDB.ImportAusFindingsByDefault;
                lpaResultsModel.LpaResults = new Dictionary<string, string>()
                    {
                        { "Loan Application Number", completeResult.Response.ValueOrDefault.LoanFeedback.LenderCaseIdentifier },
                        { "Results Date", resultsDate.HasValue ? Tools.GetEasternTimeDescription(resultsDate.Value) : resultsDate.ToString() },
                        { "AUS Status", completeResult.Response.ValueOrDefault.LoanFeedback.LoanProspectorEvaluationStatusDescription },
                        { "Risk Class", completeResult.Response.ValueOrDefault.LoanFeedback.LoanProspectorCreditRiskClassificationDescription },
                        { "Documentation Level", completeResult.Response.ValueOrDefault.LoanFeedback.FreDocumentationLevelDescription },
                        { "LPA Aus Key", completeResult.Response.ValueOrDefault.LoanFeedback.LoanProspectorKeyIdentifier },
                        { "Loan Processing Stage", LoanProspectorHelper.FromLoanProspectorFormat(completeResult.Response.ValueOrDefault?.LoanApplication?.AdditionalCaseData?.TransmittalData?.CaseStateType)?.GetDescription() }
                    };

                lpaResultsModel.HasCreditReport = !string.IsNullOrEmpty(completeResult.Response.ValueOrDefault.LoanFeedback?.MergedCreditReport?.InnerText);
                lpaResultsModel.ImportCreditReport = lpaResultsModel.HasCreditReport && (completeResult.CreditReportType == CreditReportType.OrderNew || completeResult.CreditReportType == CreditReportType.Reissue) &&
                                                     principal.BrokerDB.ImportAusCreditReportByDefault;

                FileDBTools.WriteData(E_FileDB.Normal, $"LpaResponse_{loanId:N}", completeResult.Response.Input);
            }
            else
            {
                lpaResultsModel.UnderwritingSuccess = false;
                lpaResultsModel.ErrorMessage = completeResult.Response.ErrorMessage;
                lpaResultsModel.LpaResults = new Dictionary<string, string>()
                    {
                        { "AUS Status", "Error" },
                        { "Error Message", completeResult.Response.ErrorMessage },
                    };
            }

            return lpaResultsModel;
        }
    }
}
