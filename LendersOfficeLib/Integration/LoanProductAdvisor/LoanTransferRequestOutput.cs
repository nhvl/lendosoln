﻿namespace LendersOffice.Integration.LoanProductAdvisor
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The data passed out of a loan transfer request operation.
    /// </summary>
    public class LoanTransferRequestOutput
    {
        /// <summary>
        /// Gets a value indicating whether the loan transfer request operation was successful.
        /// </summary>
        /// <value>A value indicating whether the loan transfer request operation was successful.</value>
        public bool Success { get; private set; }

        /// <summary>
        /// Gets the error messages to display to the user.
        /// </summary>
        /// <value>The error messages to display to the user.</value>
        public IEnumerable<string> Errors { get; private set; }

        /// <summary>
        /// Gets the <see cref="DataAccess.CPageData.sLId"/> of the loan updated or created by the loan transfer request operation.
        /// </summary>
        /// <value>The <see cref="DataAccess.CPageData.sLId"/> of the loan updated or created by the loan transfer request operation.</value>
        public Guid LoanId { get; private set; }

        /// <summary>
        /// Creates an error result to a loan transfer request operation.
        /// </summary>
        /// <param name="errors">The error messages to display to the user.</param>
        /// <returns>An object representing an error result at some point during the process.</returns>
        public static LoanTransferRequestOutput CreateErrorResult(IEnumerable<string> errors)
        {
            return new LoanTransferRequestOutput
            {
                Success = false,
                Errors = errors
            };
        }

        /// <summary>
        /// Creates a succesful result to a loan transfer request operation.
        /// </summary>
        /// <param name="loanId">The loan id (<see cref="DataAccess.CPageData.sLId"/>) of the loan that was updated or created by the loan transfer request operation.</param>
        /// <returns>An object representing a successful processing of the loan transfer request operation.</returns>
        public static LoanTransferRequestOutput CreateSuccessfulResult(Guid loanId)
        {
            return new LoanTransferRequestOutput
            {
                Success = true,
                LoanId = loanId
            };
        }
    }
}
