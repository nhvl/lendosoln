﻿namespace LendersOffice.Integration.LoanProductAdvisor
{
    using System;

    /// <summary>
    /// Provides a managed way to either reference an existing loan or to encapsulate the data for a loan that will be
    /// created during our import of the results of LPA's loan transfer request.
    /// </summary>
    public class LoanTransferLoanResolver
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanTransferLoanResolver"/> class.
        /// </summary>
        /// <param name="loanId">The loan id for an existing file.</param>
        private LoanTransferLoanResolver(Guid loanId)
        {
            if (loanId == Guid.Empty)
            {
                throw DataAccess.CBaseException.GenericException("Invalid LoanId");
            }

            this.LoanId = loanId;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanTransferLoanResolver"/> class.
        /// </summary>
        /// <param name="createLead">Whether the created loan should be a lead.</param>
        /// <param name="templateId">The template id to base the loan on.</param>
        private LoanTransferLoanResolver(bool createLead, Guid templateId)
        {
            this.CreateLead = createLead;
            this.CreateFromTemplateId = templateId;
        }

        /// <summary>
        /// Gets the <see cref="DataAccess.CPageData.sLId"/> of the existing loan or <seealso cref="Guid.Empty"/> if this does not represent an existing loan.
        /// </summary>
        /// <value>The <see cref="DataAccess.CPageData.sLId"/> of the existing loan or <seealso cref="Guid.Empty"/> if this does not represent an existing loan.</value>
        public Guid LoanId { get; }

        /// <summary>
        /// Gets a value indicating whether this instance represents a new loan to be created by the current process.
        /// </summary>
        /// <value>A value indicating whether this instance represents a new loan to be created by the current process.</value>
        public bool CreateNewLoan => this.LoanId == Guid.Empty;

        /// <summary>
        /// Gets a value indicating whether this instance should create a lead file if <seealso cref="CreateNewLoan"/> is true.
        /// </summary>
        /// <value>A value indicating whether this instance should create a lead file if <seealso cref="CreateNewLoan"/> is true.</value>
        public bool CreateLead { get; }

        /// <summary>
        /// Gets the template to create a new loan from if <seealso cref="CreateNewLoan"/> is true.
        /// </summary>
        /// <value>The template to create a new loan from if <seealso cref="CreateNewLoan"/> is true.</value>
        public Guid CreateFromTemplateId { get; }

        /// <summary>
        /// Creates a new instance representing an existing loan in LQB.
        /// </summary>
        /// <param name="loanId">The id of the loan in LQB.</param>
        /// <returns>An instance representing an existing loan in LQB.</returns>
        public static LoanTransferLoanResolver ExistingLoan(Guid loanId)
        {
            return new LoanTransferLoanResolver(loanId);
        }

        /// <summary>
        /// Creates a new instance representing a new, not yet created loan in LQB.
        /// </summary>
        /// <param name="createLead">Whether the created loan should be a lead.</param>
        /// <param name="templateId">The template id to base the loan on.</param>
        /// <returns>An instance representing a new, not yet created loan in LQB.</returns>
        public static LoanTransferLoanResolver NewLoan(bool createLead, Guid templateId)
        {
            return new LoanTransferLoanResolver(createLead, templateId);
        }
    }
}
