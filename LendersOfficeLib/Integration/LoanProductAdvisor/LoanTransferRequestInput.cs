﻿namespace LendersOffice.Integration.LoanProductAdvisor
{
    using System;
    using LendersOffice.Security;

    /// <summary>
    /// The data to pass into a loan transfer request operation.
    /// </summary>
    public class LoanTransferRequestInput
    {
        /// <summary>
        /// Gets or sets the user placing this request.
        /// </summary>
        /// <value>The user placing this request.</value>
        public AbstractUserPrincipal Principal { get; set; }

        /// <summary>
        /// Gets or sets the object containing the context to resolve the desired loan target to either an existing loan
        /// or a new loan to be created.
        /// </summary>
        /// <value>The loan resolver for this request.</value>
        public LoanTransferLoanResolver LoanResolution { get; set; }

        /// <summary>
        /// Gets or sets the LPA user id.
        /// </summary>
        /// <value>The LPA user id.</value>
        public string LpaUserId { get; set; }

        /// <summary>
        /// Gets or sets the LPA user password.
        /// </summary>
        /// <value>The LPA user password.</value>
        public string LpaUserPassword { get; set; }

        /// <summary>
        /// Gets or sets the LPA seller number, commonly stored as <see cref="DataAccess.CPageData.sFreddieSellerNum"/>.
        /// </summary>
        /// <value>The LPA seller number.</value>
        public string LpaSellerNumber { get; set; }

        /// <summary>
        /// Gets or sets the LPA seller password, commonly stored as <see cref="DataAccess.CPageData.sFreddieLpPassword"/>.
        /// </summary>
        /// <value>The LPA seller password.</value>
        public string LpaSellerPassword { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the request should import credit data.
        /// </summary>
        /// <value>A value indicating whether the request should import credit data.</value>
        public bool ShouldImportCredit { get; set; }

        /// <summary>
        /// Gets or sets the LPA loan id, commonly stored as <see cref="DataAccess.CPageData.sFreddieLoanId"/>.
        /// </summary>
        /// <value>The LPA loan id.</value>
        public string LpaLoanId { get; set; }

        /// <summary>
        /// Gets or sets the LPA transaction id, commonly stored as <see cref="DataAccess.CPageData.sFreddieTransactionId"/>.
        /// </summary>
        /// <value>The LPA transaction id.</value>
        public string LpaTransactionId { get; set; }

        /// <summary>
        /// Gets or sets the LPA AUS key, commonly stored as <see cref="DataAccess.CPageData.sLpAusKey"/>.
        /// </summary>
        /// <value>The LPA AUS key.</value>
        public string LpaAusKey { get; set; }

        /// <summary>
        /// Gets or sets the LPA NOTP number, commonly stored as <see cref="DataAccess.CPageData.sFreddieNotpNum"/>.
        /// </summary>
        /// <value>The LPA NOTP number.</value>
        public string LpaNotpNumber { get; set; }

        /// <summary>
        /// Gets or sets the current loan context's branch id, commonly stored as <see cref="DataAccess.CPageData.sBranchId"/>.
        /// </summary>
        /// <value>The current loan context's branch id.</value>
        public Guid LoanBranchId { get; set; }
    }
}
