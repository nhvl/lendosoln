﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integration.Encompass
{
    public class EPassCreditScores
    {
        #region Variables
        private Dictionary<int, List<string>> m_dScoresPerBorrower = null;
        #endregion

        public EPassCreditScores()
        {
            m_dScoresPerBorrower = new Dictionary<int, List<string>>();
        }

        // Inserts a score triplet {Equifax, TransUnion, Experian} for the borrower, to the score dictionary
        public void InsertScores(int iBorrIndex, string sEQ, string sTU, string sXP)
        {
            if (!m_dScoresPerBorrower.ContainsKey(iBorrIndex))
            {
                List<string> lScores = new List<string>(3);
                lScores.Add(sEQ);
                lScores.Add(sTU);
                lScores.Add(sXP);

                m_dScoresPerBorrower.Add(iBorrIndex, lScores);
            }
        }

        // Retrieves three scores {Equifax, TransUnion, Experian}, for the given borrower
        // If borrower is not found, returns a list of three "N/A" values
        public List<string> GetScores(int iAppNum, bool bIsCoborrower)
        {
            // See EPassDerogInfo.BorrowerIndex for calculation notes
            int iBorrIndex = iAppNum * 2;
            if (bIsCoborrower)
                iBorrIndex++;

            List<string> lScores = new List<string>(3);
            if (!m_dScoresPerBorrower.TryGetValue(iBorrIndex, out lScores))
            {
                lScores = new List<string>(3);
                for (int i = 0; i < 3; i++)
                {
                    lScores.Add("N/A");
                }
            }
            return lScores;
        }
    }
}
