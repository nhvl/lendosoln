﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace Integration.Encompass
{
    public class EPassDerogInfo
    {
        #region Variables
        public enum E_BKorFCData
        {
            BKType = 0,
            BKFileDate = 1,
            BKSatisfiedDate = 2,
            BKStatus = 3,
            FCOnFile = 4,
            FCFileDate = 5,
            FCSatisfiedDate = 6,
            FCStatus = 7
        };
        private Dictionary<int, List<string>> m_dMtgLatesPerApp = null;
        private Dictionary<int, List<string>> m_dFactDataPerApp = null;
        #endregion

        public EPassDerogInfo()
        {
            m_dMtgLatesPerApp = new Dictionary<int, List<string>>();
            m_dFactDataPerApp = new Dictionary<int, List<string>>();
        }

        #region Mtg Lates
        public void InsertMtgLates(int iBorrIndex, string sX30, string sX60, string sX90, string sX120)
        {
            if (!m_dMtgLatesPerApp.ContainsKey(iBorrIndex))
            {
                List<string> lMtgLates = new List<string>(4);
                lMtgLates.Add(sX30);
                lMtgLates.Add(sX60);
                lMtgLates.Add(sX90);
                lMtgLates.Add(sX120);

                m_dMtgLatesPerApp.Add(iBorrIndex, lMtgLates);
            }
        }
        public string X30Late(int iAppNum)
        {
            return GetMtgLate(iAppNum, 30);    
        }
        public string X60Late(int iAppNum)
        {
            return GetMtgLate(iAppNum, 60);
        }
        public string X90Late(int iAppNum)
        {
            return GetMtgLate(iAppNum, 90);
        }
        public string X120Late(int iAppNum)
        {
            return GetMtgLate(iAppNum, 120);
        }
        #endregion

        #region BK / FC
        public void InsertBKAndFCData(int iBorrIndex, string sBKType, string sBKFileDate, string sBKSatisfiedDate,
            string sBKStatus, string sFCOnFile, string sFCFileDate, string sFCSatisfiedDate, string sFCStatus)
        {
            if (!m_dFactDataPerApp.ContainsKey(iBorrIndex))
            {
                List<string> lFactData = new List<string>(8);
                lFactData.Add(sBKType);
                lFactData.Add(sBKFileDate);
                lFactData.Add(sBKSatisfiedDate);
                lFactData.Add(sBKStatus);
                lFactData.Add(sFCOnFile);
                lFactData.Add(sFCFileDate);
                lFactData.Add(sFCSatisfiedDate);
                lFactData.Add(sFCStatus);

                m_dFactDataPerApp.Add(iBorrIndex, lFactData);
            }
        }
        public string BKType(int iAppNum)
        {
            return GetBKOrFCData(iAppNum, E_BKorFCData.BKType);
        }
        public string BKFileDate(int iAppNum)
        {
            return GetBKOrFCData(iAppNum, E_BKorFCData.BKFileDate);
        }
        public string BKSatisfiedDate(int iAppNum)
        {
            return GetBKOrFCData(iAppNum, E_BKorFCData.BKSatisfiedDate);
        }
        public string BKStatus(int iAppNum)
        {
            return GetBKOrFCData(iAppNum, E_BKorFCData.BKStatus);
        }
        public string FCOnFile(int iAppNum)
        {
            return GetBKOrFCData(iAppNum, E_BKorFCData.FCOnFile);
        }
        public string FCFileDate(int iAppNum)
        {
            return GetBKOrFCData(iAppNum, E_BKorFCData.FCFileDate);
        }
        public string FCSatisfiedDate(int iAppNum)
        {
            return GetBKOrFCData(iAppNum, E_BKorFCData.FCSatisfiedDate);
        }
        public string FCStatus(int iAppNum)
        {
            return GetBKOrFCData(iAppNum, E_BKorFCData.FCStatus);
        }
        #endregion

        private string GetMtgLate(int iAppNum, int iDays)
        {
            List<string> lMtgLates = new List<string>(4);
            string sMtgLate = "0";
            if (m_dMtgLatesPerApp.TryGetValue(BorrIndex(iAppNum), out lMtgLates))
            {
                int iLateIndex = (iDays >= 30) ? (iDays / 30) - 1 : 0;
                if (lMtgLates.Count > iLateIndex)
                {
                    sMtgLate = lMtgLates[iLateIndex];
                }
            }
            return sMtgLate;
        }

        private string GetBKOrFCData(int iAppNum, E_BKorFCData eDataType)
        {
            List<string> lFactData = new List<string>(8);
            string sFactData = "";
            if (m_dFactDataPerApp.TryGetValue(BorrIndex(iAppNum), out lFactData))
            {
                int iFactIndex = FactDataIndex(eDataType);
                if (lFactData.Count > iFactIndex)
                {
                    sFactData = lFactData[iFactIndex];
                }
            }
            return sFactData;
        }

        private int BorrIndex(int iAppNum)
        {
            return iAppNum * 2;
        }

        private int FactDataIndex(E_BKorFCData eDataType)
        {
            switch (eDataType)
            {
                case E_BKorFCData.BKType: return 0;
                case E_BKorFCData.BKFileDate: return 1;
                case E_BKorFCData.BKSatisfiedDate: return 2;
                case E_BKorFCData.BKStatus: return 3;
                case E_BKorFCData.FCOnFile: return 4;
                case E_BKorFCData.FCFileDate: return 5;
                case E_BKorFCData.FCSatisfiedDate: return 6;
                case E_BKorFCData.FCStatus: return 7;
                default:
                    throw new UnhandledEnumException(eDataType);
            }
        }
    }
}
