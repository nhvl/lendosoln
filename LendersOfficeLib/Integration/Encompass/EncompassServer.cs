﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Xml;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Audit;
using LendersOffice.Security;
using LendersOffice.Conversions.MismoClosing231;
using LendersOffice.Common;
using LendersOffice.Constants;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace Integration.Encompass
{
    public class EncompassServer
    {
        #region Variables
        private string m_sErrorCode = string.Empty;
        private Guid m_gLoanId = Guid.Empty;
        private AbstractUserPrincipal m_auPrincipal = null;
        private EPassInfo m_EPI = null;
        //private CPageData m_cpLoanData = null;
        private bool m_bIsPUser = true;
        private E_LpeRunModeT m_eLpeRunModeT = E_LpeRunModeT.NotAllowed;

        public AbstractUserPrincipal Principal
        {
            get { return m_auPrincipal; }
        }
        public string ErrorCode
        {
            get { return m_sErrorCode; }
        }
        public Guid LoanID
        {
            get { return m_gLoanId; }
        }
        public bool IsPUser
        {
            get { return m_bIsPUser; }
        }
        public Guid UserID
        {
            get { return m_auPrincipal.UserId; }
        }
        public E_Encompass360RequestT RequestType
        {
            get { return m_EPI.RequestType; }
        }
        public E_LpeRunModeT LPERunMode
        {
            get { return m_eLpeRunModeT; }
        }
        public Guid EncompassLoanId
        {
            get { return m_EPI.EncompassLoanID; }
        }
        public string EncompassLoanNumber
        {
            get { return m_EPI.EncompassLoanNumber; }
        }
        public string PropertyAddress
        {
            get { return m_EPI.PropertyAddress; }
        }
        public string BorrowerFirstName
        {
            get { return m_EPI.BorrowerFirstName; }
        }
        public string BorrowerLastName
        {
            get { return m_EPI.BorrowerLastName; }
        }
        public string BorrowerSSN
        {
            get { return m_EPI.BorrowerSsn; }
        }
        #endregion

        public EncompassServer(string sXMLtext)
        {
            m_EPI = new EPassInfo(CreateXmlDoc(sXMLtext));
        }

        public bool AuthenticateUser()
        {
            PrincipalFactory.E_LoginProblem eLoginProblem = PrincipalFactory.E_LoginProblem.None;
            bool bSuccess = false;
            bool bValidLicense = false;
            AbstractUserPrincipal iPrincipal = null;
            bool bIsLoanOfficer = !EncompassAuthHelper.IsSellSide(m_EPI.RequestType);
            Guid brokerId = Tools.GetBrokerIdByCustomerCode(m_EPI.CustomerCode);

            if (bIsLoanOfficer)
            {
                iPrincipal = PrincipalFactory.CreatePmlUserWithFailureType(m_EPI.UserName, m_EPI.Password, m_EPI.CustomerCode, out eLoginProblem, true, brokerId, LoginSource.Website);
                bValidLicense = true;
            }

            if(!bIsLoanOfficer || 
                ((iPrincipal == null) && EncompassAuthHelper.ShouldAttemptBUserAuth(eLoginProblem)))
            {
                iPrincipal = PrincipalFactory.CreateWithFailureType(m_EPI.UserName, m_EPI.Password, out eLoginProblem, true /* allowDuplicateLogin */, true /* isStoreToCookie */, LoginSource.Website);
                bValidLicense = EncompassAuthHelper.IsLicenseValid(iPrincipal);
                m_bIsPUser = false;
            }

            if (iPrincipal != null)
            {
                if (bValidLicense)
                    bSuccess = EncompassAuthHelper.CheckForExpiredPWAndRecordSuccess(iPrincipal, m_bIsPUser, m_EPI.UserName, m_EPI.CustomerCode);

                if (bSuccess)
                {
                    m_auPrincipal = iPrincipal as AbstractUserPrincipal;
                    bSuccess = IsIntegrationEnabled();
                }
                else
                    m_sErrorCode = "InvalidLoginPassword";
            }
            else
                m_sErrorCode = EncompassAuthHelper.GetErrorCode(eLoginProblem);

            return bSuccess;
        }

        public bool HasLPEAccess()
        {
            return EncompassAuthHelper.HasLPEAccess(m_eLpeRunModeT);
        }

        public bool ImportLoanFile()
        {
            m_gLoanId = CPageData.RetrieveLoanIdByEncompassLoanId(m_EPI.EncompassLoanID, m_auPrincipal.BrokerId);
            if (m_gLoanId == Guid.Empty)
            {
                BrokerDB bdbAccountData = BrokerDB.RetrieveById(m_auPrincipal.BrokerId);
                CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(m_auPrincipal, E_LoanCreationSource.UserCreateFromBlank);

                m_gLoanId = fileCreator.BeginCreateImportBaseLoanFile(
                    sourceFileId: bdbAccountData.PmlLoanTemplateID,
                    setInitialEmployeeRoles: true,
                    addEmployeeOfficialAgent: true,
                    assignEmployeesFromRelationships: true,
                    branchIdToUse: Guid.Empty);
                fileCreator.CommitFileCreation(false);
            }

            AccessRights aRights = new AccessRights();
            bool bHasWriteAccess = aRights.HasWriteAccess(m_gLoanId);
            m_eLpeRunModeT = aRights.LPERunMode;

            if (bHasWriteAccess)
            {
                try
                {
                    EncompassMismoImporter.Import(m_gLoanId, m_EPI, false);
                }
                catch(BlankSSNException)
                {
                    m_sErrorCode = "MissingSSN";
                    return false;
                }
                //ParseEpassData();
            }
            return true;
        }

        private bool IsIntegrationEnabled()
        {
            bool bIsEnabled = m_auPrincipal.IsEncompassIntegrationEnabled;
            if (!bIsEnabled)
                m_sErrorCode = "NotEnabled";

            return bIsEnabled;
        }

        // Taken from LendersOffice LosUtils
        private static XmlDocument CreateXmlDoc(string xmlText)
        {
            XmlDocument xmlDoc = new XmlDocument();
            if (null == xmlText || xmlText.TrimWhitespaceAndBOM() == "")
                return xmlDoc;

            XmlTextReader readerData = null;
            try
            {
                readerData = new XmlTextReader(new StringReader(xmlText));
                xmlDoc.Load(readerData);
            }
            finally
            {
                if (null != readerData)
                    readerData.Close();
            }
            return xmlDoc;
        }

        //private void ParseEpassData()
        //{
        //    MismoClosing231Importer mcImporter = new MismoClosing231Importer();
        //    mcImporter.Import(m_gLoanId, m_EPI.MISMO);

        //    m_cpLoanData = CPageData.CreateUsingSmartDependency(m_gLoanId, typeof(EncompassServer));
        //    m_cpLoanData.InitSave(ConstAppDavid.SkipVersionCheck);
        //    m_cpLoanData.SetFormatTarget(FormatTarget.MismoClosing);

        //    // Wrapper data
        //    m_cpLoanData.sEncompassLoanId = m_EPI.EncompassLoanID;
        //    m_cpLoanData.sSpGrossRent_rep = m_EPI.SubjPropGrossRent;

        //    for (int i = 0; i < m_cpLoanData.nApps; i++)
        //    {
        //        CAppData cApp = m_cpLoanData.GetAppData(i);
        //        ParseDerogs(cApp, i);
        //        ParseScores(cApp, i);
        //    }

        //    if (EncompassAuthHelper.IsSellSide(m_EPI.RequestType))
        //    {
        //        ParseSellSideInfo();
        //    }

        //    m_cpLoanData.Save();
        //}

        //private void ParseDerogs(CAppData cApp, int iAppIndex) 
        //{
        //    EPassDerogInfo eDerogs = m_EPI.Derogs;
        //    cApp.BorrowerModeT = E_BorrowerModeT.Borrower;
        //    SetMtgLates(cApp, eDerogs, iAppIndex);
            
        //    ParseBK(cApp, eDerogs, iAppIndex);
            
        //    ParseFC(cApp, eDerogs, iAppIndex);
        //}

        //private void SetMtgLates(CAppData cApp, EPassDerogInfo eDerogs, int iAppIndex)
        //{
        //    cApp.aProdCrManualNonRolling30MortLateCount_rep = eDerogs.X30Late(iAppIndex);
        //    cApp.aProdCrManual30MortLateCount_rep = eDerogs.X30Late(iAppIndex);
        //    cApp.aProdCrManual60MortLateCount_rep = eDerogs.X60Late(iAppIndex);
        //    cApp.aProdCrManualRolling60MortLateCount_rep = eDerogs.X60Late(iAppIndex);
        //    cApp.aProdCrManual90MortLateCount_rep = eDerogs.X90Late(iAppIndex);
        //    cApp.aProdCrManualRolling90MortLateCount_rep = eDerogs.X90Late(iAppIndex);
        //    cApp.aProdCrManual120MortLateCount_rep = eDerogs.X120Late(iAppIndex);
        //}

        //private void ParseBK(CAppData cApp, EPassDerogInfo eDerogs, int iAppIndex)
        //{
        //    // Encompass only includes one set of BK info per app;
        //    // Clear data in case the user corrects their former BK entry in Encompass
        //    ClearBKFields(cApp);

        //    List<string> lMnthYr = null;
        //    string sBKType = eDerogs.BKType(iAppIndex).TrimWhitespaceAndBOM().ToUpper();
        //    switch (sBKType)
        //    {
        //        case "7":
        //        case "13":
        //            bool bChSeven = sBKType.Equals("7");
        //            cApp.aProdCrManualBk7Has = bChSeven;
        //            cApp.aProdCrManualBk13Has = !bChSeven;

        //            lMnthYr = GetMonthAndYear(eDerogs.BKFileDate(iAppIndex));
        //            if (lMnthYr.Count > 1)
        //            {
        //                if (bChSeven)
        //                {
        //                    cApp.aProdCrManualBk7RecentFileMon_rep = lMnthYr[0];
        //                    cApp.aProdCrManualBk7RecentFileYr_rep = lMnthYr[1];
        //                }
        //                else
        //                {
        //                    cApp.aProdCrManualBk13RecentFileMon_rep = lMnthYr[0];
        //                    cApp.aProdCrManualBk13RecentFileYr_rep = lMnthYr[1];
        //                }
        //            }

        //            ParseBKorFCStatus(cApp, eDerogs.BKStatus(iAppIndex), bChSeven, false);

        //            lMnthYr = GetMonthAndYear(eDerogs.BKSatisfiedDate(iAppIndex));
        //            if (lMnthYr.Count > 1)
        //            {
        //                if (bChSeven)
        //                {
        //                    cApp.aProdCrManualBk7RecentSatisfiedMon_rep = lMnthYr[0];
        //                    cApp.aProdCrManualBk7RecentSatisfiedYr_rep = lMnthYr[1];
        //                }
        //                else
        //                {
        //                    cApp.aProdCrManualBk13RecentSatisfiedMon_rep = lMnthYr[0];
        //                    cApp.aProdCrManualBk13RecentSatisfiedYr_rep = lMnthYr[1];
        //                }
        //            }
        //            break;

        //        case "11":
        //        case "NONE":
        //        case "":
        //            break;

        //        default:
        //            throw new CBaseException(ErrorMessages.Encompass_BKorFCTypeorStatusUnexpected, string.Format("The BK type from Encompass was: {0}", eDerogs.BKType(iAppIndex)));
        //    }
        //}

        //private void ClearBKFields(CAppData cApp)
        //{
        //    cApp.aProdCrManualBk7Has = false;
        //    cApp.aProdCrManualBk7RecentFileMon_rep = "";
        //    cApp.aProdCrManualBk7RecentFileYr_rep = "";
        //    cApp.aProdCrManualBk7RecentSatisfiedMon_rep = "";
        //    cApp.aProdCrManualBk7RecentSatisfiedYr_rep = "";
        //    cApp.aProdCrManualBk7RecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
        //    cApp.aProdCrManualBk13Has = false;
        //    cApp.aProdCrManualBk13RecentFileMon_rep = "";
        //    cApp.aProdCrManualBk13RecentFileYr_rep = "";
        //    cApp.aProdCrManualBk13RecentSatisfiedMon_rep = "";
        //    cApp.aProdCrManualBk13RecentSatisfiedYr_rep = "";
        //    cApp.aProdCrManualBk13RecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
        //}

        //private void ParseBKorFCStatus(CAppData cApp, string sBKorFCStatus, bool bChSeven, bool bFC)
        //{
        //    switch (sBKorFCStatus.TrimWhitespaceAndBOM().ToUpper())
        //    {
        //        case "":
        //            if (bFC)
        //                cApp.aProdCrManualForeclosureRecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
        //            else if (bChSeven)
        //                cApp.aProdCrManualBk7RecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
        //            else
        //                cApp.aProdCrManualBk13RecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
        //            break;

        //        case "DISCHARGED":
        //            if (bFC)
        //                cApp.aProdCrManualForeclosureRecentStatusT = E_sProdCrManualDerogRecentStatusT.Discharged;
        //            else if (bChSeven)
        //                cApp.aProdCrManualBk7RecentStatusT = E_sProdCrManualDerogRecentStatusT.Discharged;
        //            else
        //                cApp.aProdCrManualBk13RecentStatusT = E_sProdCrManualDerogRecentStatusT.Discharged;
        //            break;

        //        case "DISMISSED":
        //            if (bFC)
        //                cApp.aProdCrManualForeclosureRecentStatusT = E_sProdCrManualDerogRecentStatusT.Dismissed;
        //            else if (bChSeven)
        //                cApp.aProdCrManualBk7RecentStatusT = E_sProdCrManualDerogRecentStatusT.Dismissed;
        //            else
        //                cApp.aProdCrManualBk13RecentStatusT = E_sProdCrManualDerogRecentStatusT.Dismissed;
        //            break;

        //        default:
        //            if (bFC)
        //                throw new CBaseException(ErrorMessages.Encompass_BKorFCTypeorStatusUnexpected, string.Format("The FC status from Encompass was: {0}", sBKorFCStatus));
        //            else
        //                throw new CBaseException(ErrorMessages.Encompass_BKorFCTypeorStatusUnexpected, string.Format("The BK status from Encompass was: {0}", sBKorFCStatus));
        //    }
        //}

        //private void ParseFC(CAppData cApp, EPassDerogInfo eDerogs, int iAppIndex)
        //{
        //    bool bHasFC = eDerogs.FCOnFile(iAppIndex).TrimWhitespaceAndBOM().ToUpper().Equals("Y");
        //    cApp.aProdCrManualForeclosureHas = bHasFC;
        //    if (bHasFC)
        //    {
        //        List<string> lMnthYr = GetMonthAndYear(eDerogs.FCFileDate(iAppIndex));
        //        if (lMnthYr.Count > 1)
        //        {
        //            cApp.aProdCrManualForeclosureRecentFileMon_rep = lMnthYr[0];
        //            cApp.aProdCrManualForeclosureRecentFileYr_rep = lMnthYr[1];
        //        }

        //        ParseBKorFCStatus(cApp, eDerogs.FCStatus(iAppIndex), false, true);

        //        lMnthYr = GetMonthAndYear(eDerogs.FCSatisfiedDate(iAppIndex));
        //        if (lMnthYr.Count > 1)
        //        {
        //            cApp.aProdCrManualForeclosureRecentSatisfiedMon_rep = lMnthYr[0];
        //            cApp.aProdCrManualForeclosureRecentSatisfiedYr_rep = lMnthYr[1];
        //        }
        //    }
        //    else
        //    {
        //        ClearFCFields(cApp);
        //    }
        //}

        //private void ClearFCFields(CAppData cApp)
        //{
        //    cApp.aProdCrManualForeclosureHas = false;
        //    cApp.aProdCrManualForeclosureRecentFileMon_rep = "";
        //    cApp.aProdCrManualForeclosureRecentFileYr_rep = "";
        //    cApp.aProdCrManualForeclosureRecentSatisfiedMon_rep = "";
        //    cApp.aProdCrManualForeclosureRecentSatisfiedYr_rep = "";
        //    cApp.aProdCrManualForeclosureRecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
        //}

        //private void ParseScores(CAppData cApp, int iAppIndex)
        //{
        //    cApp.BorrowerModeT = E_BorrowerModeT.Borrower;
        //    SetScores(cApp, m_EPI.Scores.GetScores(iAppIndex, false));
        //    cApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
        //    SetScores(cApp, m_EPI.Scores.GetScores(iAppIndex, true));
        //}

        //private void SetScores(CAppData cApp, List<string> lScores)
        //{
        //    if (lScores.Count >= 3)
        //    {
        //        cApp.aEquifaxScore_rep = lScores[0];
        //        cApp.aTransUnionScore_rep = lScores[1];
        //        cApp.aExperianScore_rep = lScores[2];
        //    }
        //}

        //private void ParseSellSideInfo()
        //{
        //    EPassSellSideInfo eSellSideInfo = m_EPI.SellSideInfo;
        //    m_cpLoanData.sProdIsSpInRuralArea = eSellSideInfo.IsRural;
        //    m_cpLoanData.sProdIsCondotel = eSellSideInfo.IsCondotel;
        //    m_cpLoanData.sProdIsNonwarrantableProj = eSellSideInfo.IsNonWarrantable;
        //    m_cpLoanData.sHas1stTimeBuyer = eSellSideInfo.IsFTHB;
        //    if(eSellSideInfo.IsFTHB)
        //    {
        //        m_cpLoanData.sProdHasHousingHistory = eSellSideInfo.HasHousingHistory;
        //    }

        //    m_cpLoanData.sProdAvailReserveMonths_rep = eSellSideInfo.ReservesMonths;
        //    m_cpLoanData.sProd3rdPartyUwResultT = AUSResponse(eSellSideInfo.AUSResponse);
        //    m_cpLoanData.sProdIsDuRefiPlus = eSellSideInfo.IsDURefiPlus;
            
        //    ParseFilters(eSellSideInfo.Term, AmortType(eSellSideInfo.Amortization), eSellSideInfo.FixedTerm);

        //    ParseAUSProcessingTypes(eSellSideInfo.AUSProcessingTypes);

        //    if (!string.IsNullOrEmpty(eSellSideInfo.OriginatorCompensationPercentage))
        //    {
        //        ParseOriginatorCompensation(eSellSideInfo.OriginatorCompensationPaymentSource,
        //            eSellSideInfo.OriginatorCompensationPercentage, eSellSideInfo.OriginatorCompensationBasis,
        //            eSellSideInfo.OriginatorCompensationFixedAdjAmount, eSellSideInfo.OriginatorCompensationMin,
        //            eSellSideInfo.OriginatorCompensationMax);
        //    }
        //}

        //private void ParseFilters(string sTerm, E_sFinMethT eAmortType, string sFixedTerm)
        //{
        //    if(!string.IsNullOrEmpty(sTerm))
        //    {
        //        sTerm = sTerm.TrimWhitespaceAndBOM();
        //        bool b15 = (sTerm == "180");
        //        bool b30 = (sTerm == "360");
        //        bool b40 = (sTerm == "480");
        //        m_cpLoanData.sProdFilterDue15Yrs = b15;
        //        m_cpLoanData.sProdFilterDue30Yrs = b30;
        //        m_cpLoanData.sProdFilterDue40Yrs = b40;
        //        m_cpLoanData.sProdFilterDueOther = (!b15 && !b30 && !b40);
        //    }

        //    m_cpLoanData.sProdFilterFinMethFixed = (eAmortType == E_sFinMethT.Fixed);
        //    if (eAmortType == E_sFinMethT.ARM)
        //    {
        //        ParseARMTypes(sFixedTerm);
        //    }
        //    else
        //    {
        //        ClearARMTypes();
        //    }
        //}

        //private void ParseARMTypes(string sFixedTerm)
        //{
        //    bool bLTOneYr = false;
        //    bool bTwoYr = false;
        //    bool bThreeYr = false;
        //    bool bFiveYr = false;
        //    bool bOther = false;

        //    if (!string.IsNullOrEmpty(sFixedTerm))
        //    {
        //        sFixedTerm = sFixedTerm.TrimWhitespaceAndBOM();
        //        int iFixedTerm = 0;
        //        if(Int32.TryParse(sFixedTerm, out iFixedTerm))
        //        {
        //            bLTOneYr = (iFixedTerm < 12);
        //            bTwoYr = (iFixedTerm == 24);
        //            bThreeYr = (iFixedTerm == 36);
        //            bFiveYr = (iFixedTerm == 60);
        //            bOther = (iFixedTerm > 60);
        //        }
        //    }

        //    m_cpLoanData.sProdFilterFinMethLess1YrArm = bLTOneYr;
        //    m_cpLoanData.sProdFilterFinMeth2YrsArm = bTwoYr;
        //    m_cpLoanData.sProdFilterFinMeth3YrsArm = bThreeYr;
        //    m_cpLoanData.sProdFilterFinMeth5YrsArm = bFiveYr;
        //    m_cpLoanData.sProdFilterFinMethOther = bOther;
        //}

        //private void ClearARMTypes()
        //{
        //    m_cpLoanData.sProdFilterFinMethLess1YrArm =
        //    m_cpLoanData.sProdFilterFinMeth2YrsArm =
        //    m_cpLoanData.sProdFilterFinMeth3YrsArm =
        //    m_cpLoanData.sProdFilterFinMeth5YrsArm =
        //    m_cpLoanData.sProdFilterFinMethOther = false;
        //}

        //private void ParseAUSProcessingTypes(List<string> lProcessingTypes)
        //{
        //    m_cpLoanData.sProdIncludeNormalProc = m_cpLoanData.sProdIncludeMyCommunityProc =
        //        m_cpLoanData.sProdIncludeHomePossibleProc = m_cpLoanData.sProdIncludeFHATotalProc =
        //        m_cpLoanData.sProdIncludeVAProc = false;

        //    foreach (string sType in lProcessingTypes)
        //    {
        //        switch (sType)
        //        {
        //            case "MCM":
        //                m_cpLoanData.sProdIncludeMyCommunityProc = true;
        //                break;
        //            case "HomePossible":
        //                m_cpLoanData.sProdIncludeHomePossibleProc = true;
        //                break;
        //            case "FHA":
        //                m_cpLoanData.sProdIncludeFHATotalProc = true;
        //                break;
        //            case "VA":
        //                m_cpLoanData.sProdIncludeVAProc = true;
        //                break;
        //            case "Conventional":
        //            default:
        //                m_cpLoanData.sProdIncludeNormalProc = true;
        //                break;
        //        }
        //    }
        //}

        //private void ParseOriginatorCompensation(string sPaymentSource, string sPercentage, string sBasis, string sFixedAmount, string sMin, string sMax)
        //{
        //    E_sOriginatorCompensationPaymentSourceT ePaymentSourceT = OrigCompPaymentSourceType(sPaymentSource);
        //    m_cpLoanData.sOriginatorCompensationPaymentSourceT = ePaymentSourceT;
        //    if (ePaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
        //    {
        //        m_cpLoanData.ApplyCompensationManually(DateTime.Today.ToString(), DateTime.Today.ToString(), 
        //            sPercentage, OrigCompBasisType(sBasis), sMin, sMax, sFixedAmount, "");
        //    }
        //    else
        //    {
        //        m_cpLoanData.SetOriginatorCompensation(ePaymentSourceT, sPercentage, OrigCompBasisType(sBasis), sFixedAmount);
        //    }
        //}

        //private static E_sOriginatorCompensationPaymentSourceT OrigCompPaymentSourceType(string sPaymentSource)
        //{
        //    if (string.IsNullOrEmpty(sPaymentSource))
        //        return E_sOriginatorCompensationPaymentSourceT.SourceNotSpecified;

        //    switch (sPaymentSource.TrimWhitespaceAndBOM().ToUpper())
        //    {
        //        case "BORROWERPAID": return E_sOriginatorCompensationPaymentSourceT.BorrowerPaid;
        //        case "LENDERPAID": return E_sOriginatorCompensationPaymentSourceT.LenderPaid;
        //        case "SOURCENOTSPECIFIED":
        //        default:
        //            return E_sOriginatorCompensationPaymentSourceT.SourceNotSpecified;
        //    }
        //}

        //private static E_PercentBaseT OrigCompBasisType(string sBasis)
        //{
        //    if (string.IsNullOrEmpty(sBasis))
        //        return E_PercentBaseT.TotalLoanAmount;

        //    switch (sBasis.TrimWhitespaceAndBOM().ToUpper())
        //    {
        //        case "LOANAMOUNT": return E_PercentBaseT.LoanAmount;
        //        case "TOTALLOANAMOUNT": return E_PercentBaseT.TotalLoanAmount;
        //        case "SALESPRICE": return E_PercentBaseT.SalesPrice;
        //        case "APPRAISALVALUE": return E_PercentBaseT.AppraisalValue;
        //        case "ORIGINALCOST": return E_PercentBaseT.OriginalCost;
        //        case "AVERAGEOUTSTANDINGBALANCE": return E_PercentBaseT.AverageOutstandingBalance;
        //        case "ALLYSP": return E_PercentBaseT.AllYSP;
        //        default: return E_PercentBaseT.TotalLoanAmount;
        //    }
        //}

        //private static E_sProd3rdPartyUwResultT AUSResponse(string sResponse)
        //{
        //    if (string.IsNullOrEmpty(sResponse))
        //        return E_sProd3rdPartyUwResultT.NA;

        //    switch(sResponse.TrimWhitespaceAndBOM().ToUpper())
        //    {
        //        case "NA": return E_sProd3rdPartyUwResultT.NA;
        //        case "DU_APPROVEELIGIBLE": return E_sProd3rdPartyUwResultT.DU_ApproveEligible;
        //        case "DU_APPROVEINELIGIBLE": return E_sProd3rdPartyUwResultT.DU_ApproveIneligible;
        //        case "DU_REFERELIGIBLE": return E_sProd3rdPartyUwResultT.DU_ReferEligible;
        //        case "DU_REFERINELIGIBLE": return E_sProd3rdPartyUwResultT.DU_ReferIneligible;
        //        case "DU_REFERWCAUTIONELIGIBLE": return E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible;
        //        case "DU_REFERWCAUTIONINELIGIBLE": return E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible;
        //        case "DU_EAIELIGIBLE": return E_sProd3rdPartyUwResultT.DU_EAIEligible;
        //        case "DU_EAIIELIGIBLE": return E_sProd3rdPartyUwResultT.DU_EAIIEligible;
        //        case "DU_EAIIIELIGIBLE": return E_sProd3rdPartyUwResultT.DU_EAIIIEligible;
        //        case "LP_ACCEPTELIGIBLE": return E_sProd3rdPartyUwResultT.LP_AcceptEligible;
        //        case "LP_ACCEPTINELIGIBLE": return E_sProd3rdPartyUwResultT.LP_AcceptIneligible;
        //        case "LP_CAUTIONELIGIBLE": return E_sProd3rdPartyUwResultT.LP_CautionEligible;
        //        case "LP_CAUTIONINELIGIBLE": return E_sProd3rdPartyUwResultT.LP_CautionIneligible;
        //        case "OUTOFSCOPE": return E_sProd3rdPartyUwResultT.OutOfScope;
        //        case "LP_AMINUS_LEVEL1": return E_sProd3rdPartyUwResultT.Lp_AMinus_Level1;
        //        case "LP_AMINUS_LEVEL2": return E_sProd3rdPartyUwResultT.Lp_AMinus_Level2;
        //        case "LP_AMINUS_LEVEL3": return E_sProd3rdPartyUwResultT.Lp_AMinus_Level3;
        //        case "LP_AMINUS_LEVEL4": return E_sProd3rdPartyUwResultT.Lp_AMinus_Level4;
        //        case "LP_AMINUS_LEVEL5": return E_sProd3rdPartyUwResultT.Lp_AMinus_Level5;
        //        case "LP_REFER": return E_sProd3rdPartyUwResultT.Lp_Refer;
        //        case "TOTAL_APPROVEELIGIBLE": return E_sProd3rdPartyUwResultT.Total_ApproveEligible;
        //        case "TOTAL_APPROVEINELIGIBLE": return E_sProd3rdPartyUwResultT.Total_ApproveIneligible;
        //        case "TOTAL_REFERELIGIBLE": return E_sProd3rdPartyUwResultT.Total_ReferEligible;
        //        case "TOTAL_REFERINELIGIBLE": return E_sProd3rdPartyUwResultT.Total_ReferIneligible;
        //        default: return E_sProd3rdPartyUwResultT.NA;
        //    }
        //}

        //private static E_sFinMethT AmortType(string sAmortType)
        //{
        //    if (string.IsNullOrEmpty(sAmortType))
        //        return E_sFinMethT.Fixed;

        //    switch(sAmortType.TrimWhitespaceAndBOM().ToUpper())
        //    {
        //        case "ARM": return E_sFinMethT.ARM;
        //        case "FIXED": return E_sFinMethT.Fixed;
        //        case "GRADUATED": return E_sFinMethT.Graduated;
        //        default: return E_sFinMethT.Fixed;
        //    }
        //}

        //private static List<string> GetMonthAndYear(string sDate)
        //{
        //    List<string> lMnthYr = new List<string>(2);
        //    DateTime dt;
        //    if (DateTime.TryParse(sDate, out dt))
        //    {
        //        lMnthYr.Add(dt.Month.ToString());
        //        lMnthYr.Add(dt.Year.ToString());
        //    }
        //    return lMnthYr;
        //}
    }
}
