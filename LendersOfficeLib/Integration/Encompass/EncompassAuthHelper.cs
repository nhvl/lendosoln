﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Principal;
using System.Text;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using LendersOfficeApp.ObjLib.Licensing;
using LqbGrammar.Drivers.SecurityEventLogging;
using LqbGrammar;
using LendersOffice.ObjLib.Security;

namespace Integration.Encompass
{
    public static class EncompassAuthHelper
    {
        public static bool CheckForExpiredPWAndRecordSuccess(IPrincipal iPrincipal, bool bIsPUser, string sUserName, string sCustomerCode)
        {
            BrokerUserPrincipal bPrincipal = (BrokerUserPrincipal)iPrincipal;
            bool bSuccess = !bPrincipal.IsPasswordExpired;
            if (bSuccess)
            {
                InsertAuditRecord((AbstractUserPrincipal)iPrincipal);
                ResetLoginFailureCount(((AbstractUserPrincipal)iPrincipal).BrokerId, bIsPUser, sUserName, sCustomerCode);
            }

            return bSuccess;
        }

        public static bool IsLicenseValid(AbstractUserPrincipal aPrincipal)
        {
            bool bValid = false;

            if (aPrincipal != null)
            {
                BrokerUserPrincipal bpPrincipal = aPrincipal as BrokerUserPrincipal;
                if (bpPrincipal != null)
                {
                    if (bpPrincipal.BillingVersion == LendersOffice.Admin.E_BrokerBillingVersion.NoBilling ||
                        bpPrincipal.BillingVersion == LendersOffice.Admin.E_BrokerBillingVersion.PerTransaction)
                    {
                        return true;
                    }
                }
                BrokerUser brokerUser = new BrokerUser(aPrincipal.UserId, aPrincipal.BrokerId, aPrincipal.EmployeeId, aPrincipal.DisplayName, aPrincipal.Permissions, aPrincipal.Type);
                if (brokerUser.EnforceLicensing())
                    bValid = brokerUser.HasValidLicense();
                else
                    bValid = true;
            }
            return bValid;
        }

        public static string GetErrorCode(PrincipalFactory.E_LoginProblem eLoginProblem)
        {
            string sErrorCode = string.Empty;
            switch (eLoginProblem)
            {
                case PrincipalFactory.E_LoginProblem.IsDisabled:
                    sErrorCode = "IsDisabled";
                    break;
                case PrincipalFactory.E_LoginProblem.IsLocked:
                    sErrorCode = "IsLocked";
                    break;
                case PrincipalFactory.E_LoginProblem.NeedsToWait:
                    sErrorCode = "NeedsToWait";
                    break;
                case PrincipalFactory.E_LoginProblem.InvalidAndWait:
                    sErrorCode = "InvalidAndWait";
                    break;
                case PrincipalFactory.E_LoginProblem.InvalidLoginPassword:
                case PrincipalFactory.E_LoginProblem.ActiveDirectorySetupIncomplete:
                case PrincipalFactory.E_LoginProblem.InvalidCustomerCode:
                case PrincipalFactory.E_LoginProblem.MustLoginAsActiveDirectoryUserType:
                case PrincipalFactory.E_LoginProblem.TempPasswordExpired:
                    sErrorCode = "InvalidLoginPassword";
                    break;
                case PrincipalFactory.E_LoginProblem.None:
                default:
                    sErrorCode = "InvalidLoginPassword";
                    Tools.LogBug("The IPrincipal is null, but no login problem error code was returned - EncompassServer");
                    break;
            }
            return sErrorCode;
        }

        public static bool ShouldAttemptBUserAuth(PrincipalFactory.E_LoginProblem eLoginProblem)
        {
            bool bShouldAttempt = true;
            switch (eLoginProblem)
            {
                case PrincipalFactory.E_LoginProblem.IsDisabled:
                    bShouldAttempt = true;
                    break;
                case PrincipalFactory.E_LoginProblem.IsLocked:
                    bShouldAttempt = false;
                    break;
                case PrincipalFactory.E_LoginProblem.NeedsToWait:
                    bShouldAttempt = false;
                    break;
                case PrincipalFactory.E_LoginProblem.InvalidAndWait:
                    bShouldAttempt = false;
                    break;
                case PrincipalFactory.E_LoginProblem.InvalidLoginPassword:
                    bShouldAttempt = true;
                    break;
                case PrincipalFactory.E_LoginProblem.None:
                case PrincipalFactory.E_LoginProblem.ActiveDirectorySetupIncomplete:
                case PrincipalFactory.E_LoginProblem.InvalidCustomerCode:
                case PrincipalFactory.E_LoginProblem.MustLoginAsActiveDirectoryUserType:
                case PrincipalFactory.E_LoginProblem.TempPasswordExpired:
                default:
                    bShouldAttempt = true;
                    break;
            }
            return bShouldAttempt;
        }

        public static bool HasLPEAccess(E_LpeRunModeT eLpeRunModeT)
        {
            bool bHasAccess = false;
            switch (eLpeRunModeT)
            {
                case E_LpeRunModeT.Full:
                case E_LpeRunModeT.OriginalProductOnly_Direct:
                    bHasAccess = true;
                    break;
                case E_LpeRunModeT.NotAllowed:
                default:
                    bHasAccess = false;
                    break;
            }
            return bHasAccess;
        }

        public static bool IsSellSide(E_Encompass360RequestT eRequestType)
        {
            return (eRequestType == E_Encompass360RequestT.SellsideLock) ? true : false;
        }

        private static void InsertAuditRecord(AbstractUserPrincipal aPrincipal)
        {
            SecurityEventLogHelper.CreateLoginLog(aPrincipal);
        }
        
        private static void ResetLoginFailureCount(Guid brokerId, bool bIsPUser, string sUserName, string sCustomerCode)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@LoginNm", GetSafeString(sUserName)));
            parameters.Add(new SqlParameter("@LoginBlockExpirationD", SmallDateTime.MinValue));
            parameters.Add(new SqlParameter("@Type", bIsPUser ? "P" : "B"));
            if (bIsPUser)
            {
                parameters.Add(new SqlParameter("@BrokerPmlSiteId", GetPMLSiteID(brokerId, sCustomerCode)));
            }

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "ResetLoginFailureCount", 3, parameters);

        }

        private static Guid GetPMLSiteID(Guid brokerId, string sCustomerCode)
        {
            Guid gSiteID = Guid.Empty;
            SqlParameter[] parameters = {
                                            new SqlParameter("@CustomerCode", GetSafeString(sCustomerCode).ToUpper())
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveBrokerPmlSiteIdByCustomerCode", parameters))
            {
                if (reader.Read() != false)
                {
                    gSiteID = new Guid(reader["BrokerPmlSiteId"].ToString());
                }
            }
            return gSiteID;
        }

        private static string GetSafeString(string s)
        {
            return Utilities.SafeSQLString(s);
        }
    }
}
