﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using DataAccess;
using LendersOffice.Common;
using Mismo.Closing2_4;
using XmlSerializableCommon;

namespace Integration.Encompass
{
    public static class EncompassResponseHelper
    {
        public static List<string> FACTData(string sFileMonth, string sFileYear, E_sProdCrManualDerogRecentStatusT eStatusType, string sSatisfiedMonth, string sSatisfiedYear)
        {
            List<string> lFactData = new List<string>(3);
            lFactData.Add(string.Format("{0}/{1}", sFileMonth, sFileYear));
            lFactData.Add(BKorFCStatus(eStatusType));
            if (eStatusType != E_sProdCrManualDerogRecentStatusT.NotSatisfied)
            {
                lFactData.Add(string.Format("{0}/{1}", sSatisfiedMonth, sSatisfiedYear));
            }
            else
            {
                lFactData.Add("");
            }
            return lFactData;
        }

        public static void WriteBorrowerIDAttr(XmlWriter xmlWriter, int iAppNumber, bool bWriteBoth, bool bIsBorrowerFirst)
        {
            xmlWriter.WriteAttributeString("BorrowerID", MismoBorrowerID(iAppNumber, bIsBorrowerFirst));
            if (bWriteBoth)
            {
                xmlWriter.WriteAttributeString("JointAssetBorrowerID", MismoBorrowerID(iAppNumber, !bIsBorrowerFirst));
            }
        }

        public static string MtgType(E_sLT eLoanType)
        {
            switch (eLoanType)
            {
                case E_sLT.Conventional: return E_MortgageTermsMortgageType.Conventional.ToString();
                case E_sLT.FHA: return E_MortgageTermsMortgageType.FHA.ToString();
                case E_sLT.VA: return E_MortgageTermsMortgageType.VA.ToString();
                case E_sLT.UsdaRural: return E_MortgageTermsMortgageType.FarmersHomeAdministration.ToString();
                case E_sLT.Other:
                default:
                    return E_MortgageTermsMortgageType.Other.ToString();
            }
        }

        public static string AmortType(E_sFinMethT eAmortType)
        {
            switch (eAmortType)
            {
                case E_sFinMethT.ARM: return E_MortgageTermsLoanAmortizationType.AdjustableRate.ToString();
                case E_sFinMethT.Fixed: return E_MortgageTermsLoanAmortizationType.Fixed.ToString();
                case E_sFinMethT.Graduated: return E_MortgageTermsLoanAmortizationType.GraduatedPaymentMortgage.ToString();
                default:
                    throw new CBaseException(ErrorMessages.Encompass_GenericExportErrorMessage, string.Format("Unexpected amortization type on file: {0}", eAmortType.ToString()));
            }
        }

        public static decimal ConvertToDecimal(string sValue)
        {
            decimal d = 0.000m;

            if (decimal.TryParse(TrimString(sValue), out d) == false)
            {
                d = 0.000M;
            }
            return d;
        }

        public static string TrimElement(XmlElement xElement)
        {
            return TrimString(xElement.InnerText);
        }
        public static string TrimString(string sValue)
        {
            return sValue.TrimWhitespaceAndBOM().Trim('%');
        }

        public static string MismoBorrowerID(int iAppNumber, bool bIsBorrower)
        {
            iAppNumber++;
            string sBorPrefix = bIsBorrower ? "BOR" : "COB";
            return String.Concat(sBorPrefix, iAppNumber);
        }

        public static List<string> AUSProcessingTypes(bool bConventional, bool bMCM, bool bHP, bool bFHA, bool bVA)
        {
            List<string> lProcessingTypes = new List<string>(5);
            if (bConventional)
            {
                lProcessingTypes.Add("Conventional");
            }

            if (bMCM)
            {
                lProcessingTypes.Add("MCM");
            }

            if (bHP)
            {
                lProcessingTypes.Add("HomePossible");
            }

            if (bFHA)
            {
                lProcessingTypes.Add("FHA");
            }

            if (bVA)
            {
                lProcessingTypes.Add("VA");
            }
            return lProcessingTypes;
        }

        public static string ConvertToYNString(bool bValue)
        {
            return ConvertToYNIndicator(bValue).ToString();
        }
        
        public static string BKorFCDate(CDateTime cDT)
        {
            string sDate = "";
            if (cDT.IsValid)
            {
                sDate = cDT.ToString("MM/dd/yyyy");
            }
            return sDate;
        }
        public static string BKorFCStatus(E_sProdCrManualDerogRecentStatusT eManualDerogStatus)
        {
            switch (eManualDerogStatus)
            {
                case E_sProdCrManualDerogRecentStatusT.Discharged: return "DISCHARGED";
                case E_sProdCrManualDerogRecentStatusT.Dismissed: return "DISMISSED";
                case E_sProdCrManualDerogRecentStatusT.NotSatisfied: return "";
                default:
                    throw new CBaseException(ErrorMessages.Encompass_GenericExportErrorMessage, string.Format("Unexpected BK or FC status on file: {0}", eManualDerogStatus.ToString()));
            }
        }

        private static E_YNIndicator ConvertToYNIndicator(bool bValue)
        {
            return bValue ? E_YNIndicator.Y : E_YNIndicator.N;
        }
    }
}
