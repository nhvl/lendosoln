﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.CreditReport;
using System.Xml.Linq;

namespace Integration.Encompass
{
    public class EPassInfo
    {
        #region Variables
        private string m_sAction = "";
        private bool m_bIsAUSRequest = false;
        private string m_sAUSAction = "";
        private string m_sUserName = "";
        private string m_sPassword = "";
        private string m_sCCode = "";
        private Guid m_gEncompassLoanID = Guid.Empty;
        private EPassDerogInfo m_epDerogs = null;
        private string m_sSubjGrossRentalIncome = "";
        private EPassCreditScores m_epScores = null;
        private string m_sMISMO231 = "";
        private EPassSellSideInfo m_epSellSideInfo = null;
        private List<ICreditReportResponse> m_creditReportList = new List<ICreditReportResponse>();
        private string m_sFTHBCounseling = "";

        public List<ICreditReportResponse> CreditReportList
        {
            get { return m_creditReportList; }
        }
        public E_Encompass360RequestT RequestType
        {
            get {
                bool bIsPricingRequest = true;
                switch (m_sAUSAction.TrimWhitespaceAndBOM().ToUpper())
                {
                    case "AUS_ONLY":
                        bIsPricingRequest = false;
                        break;
                    case "AUS_AND_PRICING":
                    default:
                        break;
                }

                E_Encompass360RequestT eRequestT = E_Encompass360RequestT.QuoteRequest;
                switch(m_sAction.TrimWhitespaceAndBOM().ToUpper())
                {
                    case "ELLIEMAEBUYREQUEST": 
                        eRequestT = E_Encompass360RequestT.BuysideLock;
                        break;
                    case "ELLIEMAESELLREQUEST": 
                        eRequestT = E_Encompass360RequestT.SellsideLock;
                        break;
                    case "ELLIEMAEQUOTEREQUEST":
                        if (m_bIsAUSRequest)
                            eRequestT = (bIsPricingRequest) ? E_Encompass360RequestT.RegisterOrLock : E_Encompass360RequestT.UnderwriteOnly;
                        else
                            eRequestT = E_Encompass360RequestT.QuoteRequest;
                        break;
                    default: 
                        eRequestT = E_Encompass360RequestT.QuoteRequest;
                        break;
                }
                return eRequestT;
            }
        }
        public string UserName
        {
            get { return m_sUserName; }
        }
        public string Password
        {
            get { return m_sPassword; }
        }
        public string CustomerCode
        {
            get { return m_sCCode; }
        }
        public Guid EncompassLoanID
        {
            get { return m_gEncompassLoanID; }
        }
        public EPassDerogInfo Derogs
        {
            get { return m_epDerogs; }
        }
        public string SubjPropGrossRent
        {
            get { return m_sSubjGrossRentalIncome; }
        }
        public EPassCreditScores Scores
        {
            get { return m_epScores; }
        }
        public string MISMO
        {
            get { return m_sMISMO231; }
        }
        public EPassSellSideInfo SellSideInfo
        {
            get { return m_epSellSideInfo; }
        }
        public string FTHBCounseling
        {
            get { return m_sFTHBCounseling; }
        }

        public string EncompassLoanNumber { get; private set; }
        public string BorrowerFirstName { get; private set; }
        public string BorrowerLastName { get; private set; }
        public string BorrowerSsn { get; private set; }
        public string PropertyAddress { get; private set; }
        #endregion

        public EPassInfo(XmlDocument xmlDoc)
        {
            XmlElement xRoot = xmlDoc.DocumentElement;
            m_sAction = xRoot.Attributes["action"].Value;

            if (xRoot.HasAttribute("AUS_Action"))
            {
                m_bIsAUSRequest = true;
                m_sAUSAction = xRoot.Attributes["AUS_Action"].Value;
            }

            XmlNode xNode = xRoot.SelectSingleNode("descendant::authentication");
            m_sUserName = xNode.Attributes["userName"].Value;
            m_sPassword = xNode.Attributes["password"].Value;

            xNode = xRoot.SelectSingleNode("descendant::request");
            m_sCCode = xNode.Attributes["PML_Client_Code"].Value;

            xNode = xRoot.SelectSingleNode("descendant::body");
            
            if(null != xNode.Attributes.GetNamedItem("FTHBcounseling"))
                m_sFTHBCounseling = xNode.Attributes["FTHBcounseling"].Value;

            m_epDerogs = new EPassDerogInfo();
            m_epScores = new EPassCreditScores();
            m_epSellSideInfo = new EPassSellSideInfo();
            string sBorrID = "";
            foreach (XmlNode xLeaf in xNode.ChildNodes)
            {
                switch (xLeaf.LocalName.ToUpper())
                {
                    case "LOANID":
                        m_gEncompassLoanID = new Guid(xLeaf.Attributes["GUID"].Value);
                        break;
                    case "MORTGAGELATE":
                        sBorrID = BorrowerID(xLeaf);
                        m_epDerogs.InsertMtgLates(BorrowerIndex(sBorrID),
                            xLeaf.Attributes["Days_30"].Value, xLeaf.Attributes["Days_60"].Value,
                            xLeaf.Attributes["Days_90"].Value, xLeaf.Attributes["Days_120"].Value);
                        break;
                    case "SUBJECTPROPERTY":
                        m_sSubjGrossRentalIncome = xLeaf.Attributes["RentalIncomeGrossAmount"].Value;
                        break;
                    case "FACT":
                        sBorrID = BorrowerID(xLeaf);
                        m_epDerogs.InsertBKAndFCData(BorrowerIndex(sBorrID),
                            xLeaf.Attributes["Bankrupt_Open_Type"].Value,
                            xLeaf.Attributes["Bankrupt_Open_Date"].Value,
                            xLeaf.Attributes["Bankrupt_Prior_Date"].Value,
                            xLeaf.Attributes["Bankrupt_Prior_Status"].Value,
                            xLeaf.Attributes["Foreclosure_Had"].Value,
                            xLeaf.Attributes["Foreclosure_Date_Filed"].Value,
                            xLeaf.Attributes["Foreclosure_Date_Satisfied"].Value,
                            xLeaf.Attributes["Foreclosure_Status"].Value);
                        break;
                    case "BORROWER":
                        // BorrowerID = BOR1, COB1, etc.
                        m_epScores.InsertScores(BorrowerIndex(xLeaf.Attributes["BorrowerID"].Value), 
                            xLeaf.Attributes["Equifax"].Value, xLeaf.Attributes["Empirica"].Value, 
                            xLeaf.Attributes["Experian"].Value);
                        break;
                    case "LOAN":
                        m_sMISMO231 = xLeaf.OuterXml;
                        ParseSomeMismoData();
                        break;
                    case "PML":
                        GetSellSideData(xLeaf);
                        break;
                    case "RESPONSE_GROUP":
                        XmlDocument mismoDoc = Tools.CreateXmlDoc(xLeaf.OuterXml);
                        ICreditReportResponse credit = new LendersOffice.CreditReport.Mismo2_3.Mismo2_3CreditReportResponse(mismoDoc);
                        m_creditReportList.Add(credit);
                        break;
                    default:
                        // No-op; Encompass will include elements in the request that are only used as a reference; there's no need to map each of them
                        break;
                }
            }
        }
        private void ParseSomeMismoData()
        {
            XmlDocument xdoc = Tools.CreateXmlDoc(m_sMISMO231);

            XmlElement transmittalData = (XmlElement) xdoc.SelectSingleNode("/LOAN/_APPLICATION/ADDITIONAL_CASE_DATA/TRANSMITTAL_DATA");
            if (transmittalData != null)
            {
                this.EncompassLoanNumber = transmittalData.GetAttribute("LoanOriginationSystemLoanIdentifier");
            }

            XmlElement property = (XmlElement)xdoc.SelectSingleNode("/LOAN/_APPLICATION/PROPERTY");
            if (property != null)
            {
                this.PropertyAddress = property.GetAttribute("_StreetAddress");
            }

            XmlElement primaryBorrower = (XmlElement)xdoc.SelectSingleNode("/LOAN/_APPLICATION/BORROWER[@BorrowerID='BOR1']");
            if (primaryBorrower != null)
            {
                this.BorrowerFirstName = primaryBorrower.GetAttribute("_FirstName");
                this.BorrowerLastName = primaryBorrower.GetAttribute("_LastName");
                this.BorrowerSsn = primaryBorrower.GetAttribute("_SSN");
            }

        }
        private string BorrowerID(XmlNode xNode)
        {
            string sBorrID = "BOR1";
            foreach (XmlAttribute xAttr in xNode.Attributes)
            {
                switch(xAttr.LocalName.ToUpper())
                {
                    case "BORROWERID":
                        sBorrID = xAttr.Value;
                        break;
                    default:
                        break;
                }
            }
            return sBorrID;
        }

        private void GetSellSideData(XmlNode xNode)
        {
            foreach (XmlNode xLeaf in xNode.ChildNodes)
            {
                switch (xLeaf.LocalName.ToUpper())
                {
                    case "PROPERTY":
                        m_epSellSideInfo.IsRural = ConvertToBool(xLeaf.Attributes["IsRural"].Value);
                        m_epSellSideInfo.IsCondotel = ConvertToBool(xLeaf.Attributes["IsCondotel"].Value);
                        m_epSellSideInfo.IsNonWarrantable = ConvertToBool(xLeaf.Attributes["IsNonWarrantable"].Value);
                        break;
                    case "BORROWER":
                        m_epSellSideInfo.IsFTHB = ConvertToBool(xLeaf.Attributes["IsFTHB"].Value);
                        m_epSellSideInfo.HasHousingHistory = ConvertToBool(xLeaf.Attributes["HasHousingHistory"].Value);
                        m_epSellSideInfo.ReservesMonths = xLeaf.Attributes["Reserves"].Value;
                        break;
                    case "PRODUCT":
                        m_epSellSideInfo.AUSResponse = xLeaf.Attributes["AUS"].Value;
                        m_epSellSideInfo.IsDURefiPlus = ConvertToBool(xLeaf.Attributes["DURefiPlus"].Value);
                        m_epSellSideInfo.Term = xLeaf.Attributes["Term"].Value;
                        m_epSellSideInfo.Amortization = xLeaf.Attributes["Amort"].Value;
                        m_epSellSideInfo.FixedTerm = xLeaf.Attributes["FixedTerm"].Value;
                        GetAUSProcessingTypes(xLeaf);
                        break;
                    case "ORIGINATORCOMPENSATION":
                        m_epSellSideInfo.OriginatorCompensationPaymentSource = xLeaf.Attributes["PaymentSource"].Value;
                        m_epSellSideInfo.OriginatorCompensationPercentage = xLeaf.Attributes["CompensationPercentage"].Value;
                        m_epSellSideInfo.OriginatorCompensationBasis = xLeaf.Attributes["CompensationBasis"].Value;
                        m_epSellSideInfo.OriginatorCompensationFixedAdjAmount = xLeaf.Attributes["CompensationFixedAdjAmount"].Value;
                        m_epSellSideInfo.OriginatorCompensationMin = xLeaf.Attributes["CompensationMinimum"].Value;
                        m_epSellSideInfo.OriginatorCompensationMax = xLeaf.Attributes["CompensationMaximum"].Value;
                        break;
                    default:
                        break;
                }
            }
        }

        private void GetAUSProcessingTypes(XmlNode xNode)
        {
            foreach (XmlNode xLeaf in xNode.ChildNodes)
            {
                switch(xLeaf.LocalName.ToUpper())
                {
                    case "AUSPROCESSINGTYPE":
                        m_epSellSideInfo.AddAUSProcessingType(xLeaf.Attributes["Type"].Value);
                        break;
                    default:
                        break;
                }
            }
        }

        // Returns a borrower index aligned to our app numbering, from Encompass's (MISMO) BorrowerID
        private int BorrowerIndex(string sBorrowerID)
        {
            // Encompass: BOR1, COB1, BOR2, COB2, ... , BORN, COBN
            // Our Apps are 0 based: App0 (B1, C1), App1 (B2, C2), ... , AppN (BN+1, CN+1)
            // For a borrower, index = 2N - 2 = 2 * App#
            // For a coborrower, index = 2N - 1 = (2 * App#) + 1
            sBorrowerID = sBorrowerID.TrimWhitespaceAndBOM().ToUpper();
            bool bIsCoborrower = sBorrowerID.StartsWith("C");
            int iBorrIndex = 0;
            if (sBorrowerID.Length > 3)
            {
                Int32.TryParse(sBorrowerID.Substring(3), out iBorrIndex);
                if (iBorrIndex <= 0)
                    throw new CBaseException(ErrorMessages.Encompass_BorrowerIDUnexpected, string.Format("The BorrowerID from Encompass was: {0}", sBorrowerID));

                iBorrIndex = (2 * iBorrIndex) - 2;
                if (bIsCoborrower)
                    iBorrIndex++;
            }
            else
                throw new CBaseException(ErrorMessages.Encompass_BorrowerIDUnexpected, string.Format("The BorrowerID from Encompass was: {0}", sBorrowerID));

            return iBorrIndex;
        }

        private static bool ConvertToBool(string sYNValue)
        {
            bool bValue = false;
            if (!string.IsNullOrEmpty(sYNValue))
            {
                bValue = (sYNValue.TrimWhitespaceAndBOM().ToUpper() == "Y");
            }
            return bValue;
        }
    }
}
