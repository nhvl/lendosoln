﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;
using DataAccess;
using LendersOffice.CreditReport;
using LendersOffice.Common;
using Mismo.Closing2_4;

namespace Integration.Encompass
{
    public class EncompassResponse
    {
        #region Variables
        private CPageData m_dataLoan = null;
        private bool m_bIsSellSide = false;
        private bool m_bIsCompIncludedInPrice = false;
        private bool m_bIsLockRequest = true;
        #endregion

        public string Export(Guid sLId, E_Encompass360RequestT eRequestType)
        {
            m_dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(EncompassResponse));
            m_dataLoan.InitLoad();
            m_dataLoan.SetFormatTarget(FormatTarget.MismoClosing);

            m_bIsSellSide = EncompassAuthHelper.IsSellSide(eRequestType);
            m_bIsCompIncludedInPrice = m_dataLoan.sIsQualifiedForOriginatorCompensation
                        && m_dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                        && m_dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;

            m_bIsLockRequest = (eRequestType != E_Encompass360RequestT.UnderwriteOnly);
            
            StringBuilder sb = new StringBuilder();
            using (XmlWriter xmlWriter = XmlWriter.Create(sb))
            {
                WritePriceMyLoanEnvelope(xmlWriter, eRequestType);
            }
            return sb.ToString();
        }

        private void WritePriceMyLoanEnvelope(XmlWriter xmlWriter, E_Encompass360RequestT eRequestType)
        {
            xmlWriter.WriteStartElement("PML_envelope");
            xmlWriter.WriteAttributeString("source", "Ellie Mae");
            xmlWriter.WriteAttributeString("action", ConvertRequestTypeToAction(eRequestType));
            
            WriteBody(xmlWriter);
            
            xmlWriter.WriteEndElement(); // </pricemyloan_envelope>
        }
        private static string ConvertRequestTypeToAction(E_Encompass360RequestT eRequestType)
        {
            switch(eRequestType)
            {
                case E_Encompass360RequestT.BuysideLock:
                    return "EllieMaeBuyResponse";
                case E_Encompass360RequestT.SellsideLock:
                    return "EllieMaeSellResponse";
                case E_Encompass360RequestT.RegisterOrLock:
                case E_Encompass360RequestT.UnderwriteOnly:
                case E_Encompass360RequestT.QuoteRequest:
                default:
                    return "EllieMaeQuoteResponse";
            }
        }
        #region Body
        private void WriteBody(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("body");
            WriteFICO(xmlWriter);
            WriteCredit(xmlWriter);
            WriteDTI(xmlWriter);
            WriteEncompassLoanID(xmlWriter);
            WriteMtgLates(xmlWriter);
            WriteSubjProperty(xmlWriter);
            WriteFactACTData(xmlWriter);
            WriteScores(xmlWriter);
            WriteMismoLoan(xmlWriter);
            WriteProduct(xmlWriter);
            WritePML(xmlWriter);
            WriteAUS(xmlWriter);
            xmlWriter.WriteEndElement(); // </body>
        }
        private void WriteFICO(XmlWriter xmlWriter)
        {
            string score = m_dataLoan.sCreditScoreType2_rep;  //eopm  316597
            if( string.IsNullOrEmpty(score))
            {
                score = "0";
            }
            xmlWriter.WriteStartElement("fico");
            xmlWriter.WriteAttributeString("representative_score", score);
            xmlWriter.WriteEndElement(); // </fico>
        }
        #region Credit
        private void WriteCredit(XmlWriter xmlWriter)
        {
            CAppData cApp = null;
            for (int i = 0; i < m_dataLoan.nApps; i++)
            {
                cApp = m_dataLoan.GetAppData(i);
                if (cApp.aIsCreditReportOnFile)
                {
                    string sCreditRefNum = CreditRefNumber(m_dataLoan.sBrokerId, cApp.aAppId);
                    if (!string.IsNullOrEmpty(sCreditRefNum))
                    {
                        xmlWriter.WriteStartElement("Credit");
                        EncompassResponseHelper.WriteBorrowerIDAttr(xmlWriter, i, true, true);
                        xmlWriter.WriteAttributeString("reference_number", sCreditRefNum);
                        xmlWriter.WriteEndElement(); // </Credit>
                    }
                }
            }
        }
        private string CreditRefNumber(Guid brokerId, Guid gAppID)
        {
            SqlParameter[] parameters = {new SqlParameter("@ApplicationID", gAppID)};
            string sRefNum = "";
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveCreditReport", parameters))
            {
                if (reader.Read())
                {
                    sRefNum = (string)reader["ExternalFileID"];
                }
            }
            return sRefNum;
        }
        #endregion
        private void WriteDTI(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("DTI");
            xmlWriter.WriteAttributeString("FrontEnd", m_dataLoan.sQualTopR_rep);
            xmlWriter.WriteAttributeString("BackEnd", m_dataLoan.sQualBottomR_rep);
            xmlWriter.WriteEndElement(); // </DTI>
        }
        private void WriteEncompassLoanID(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("loanID");
            xmlWriter.WriteAttributeString("GUID", m_dataLoan.sEncompassLoanId.ToString());
            xmlWriter.WriteEndElement(); // </loanID>
        }
        #region MtgLates
        private void WriteMtgLates(XmlWriter xmlWriter)
        {
            CAppData cApp = null;
            for (int i = 0; i < m_dataLoan.nApps; i++)
            {
                cApp = m_dataLoan.GetAppData(i);
                cApp.BorrowerModeT = E_BorrowerModeT.Borrower;
                WriteMtgLatesPerApp(xmlWriter, cApp, i);
            }
        }
        private void WriteMtgLatesPerApp(XmlWriter xmlWriter, CAppData cApp, int iAppNumber)
        {
            xmlWriter.WriteStartElement("MortgageLate");
            EncompassResponseHelper.WriteBorrowerIDAttr(xmlWriter, iAppNumber, true, true);

            string sX30, sX60, sX90, sX120;
            sX30 = sX60 = sX90 = sX120 = "";
            if (cApp.aIsCreditReportOnFile)
            {
                ICreditReport creditReport = cApp.CreditReportData.Value;
                sX30 = creditReport.GetMortgage30Lates(24).ToString();
                sX60 = creditReport.GetMortgage60Lates(24).ToString();
                sX90 = creditReport.GetMortgage90Lates(24).ToString();
                sX120 = creditReport.GetMortgage120Lates(24).ToString();
            }
            else
            {
                sX30 = cApp.aProdCrManualNonRolling30MortLateCount_rep;
                sX60 = cApp.aProdCrManual60MortLateCount_rep;
                sX90 = cApp.aProdCrManual90MortLateCount_rep;
                sX120 = cApp.aProdCrManual120MortLateCount_rep;
            }
            xmlWriter.WriteAttributeString("Days_120", sX120);
            xmlWriter.WriteAttributeString("Days_90", sX90);
            xmlWriter.WriteAttributeString("Days_60", sX60);
            xmlWriter.WriteAttributeString("Days_30", sX30);
            xmlWriter.WriteEndElement(); // </MortgageLate>
        }
        #endregion
        private void WriteSubjProperty(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("SubjectProperty");
            xmlWriter.WriteAttributeString("RentalIncomeGrossAmount", m_dataLoan.sSpGrossRent_rep);
            xmlWriter.WriteEndElement(); // </SubjectProperty>
        }
        #region BK/FC
        private void WriteFactACTData(XmlWriter xmlWriter)
        {
            CAppData cApp = null;
            for (int i = 0; i < m_dataLoan.nApps; i++)
            {
                cApp = m_dataLoan.GetAppData(i);
                cApp.BorrowerModeT = E_BorrowerModeT.Borrower;
                WriteFactACTDataPerApp(xmlWriter, cApp, i);
            }
        }
        private void WriteFactACTDataPerApp(XmlWriter xmlWriter, CAppData cApp, int iAppNumber)
        {
            xmlWriter.WriteStartElement("FACT");
            EncompassResponseHelper.WriteBorrowerIDAttr(xmlWriter, iAppNumber, true, true);

            bool bCreditOnFile = cApp.aIsCreditReportOnFile;
            if (bCreditOnFile)
            {
                WriteBKFromCredit(xmlWriter, cApp);
                WriteFCFromCredit(xmlWriter, cApp);
            }
            else
            {
                WriteBK(xmlWriter, cApp);
                WriteFC(xmlWriter, cApp);
            }
                        
            xmlWriter.WriteEndElement(); // </FACT>
        }
        private void WriteBKFromCredit(XmlWriter xmlWriter, CAppData cApp)
        {
            string sBKType, sBKFileDate, sBKStatusDate, sBKStatus;
            sBKType = sBKFileDate = sBKStatusDate = sBKStatus = "";

            List<string> lBKData = new List<string>(4);
            int iTypeOfLatestBK = TypeOfMostRecentBK(cApp);
            if (iTypeOfLatestBK == 7)
            {
                lBKData = BKFromCredit(true, cApp);
            }
            else if (iTypeOfLatestBK == 13)
            {
                lBKData = BKFromCredit(false, cApp);
            }

            if (lBKData.Count > 3)
            {
                sBKType = lBKData[0];
                sBKFileDate = lBKData[1];
                sBKStatus = lBKData[2];
                sBKStatusDate = lBKData[3];
            }

            WriteBKData(xmlWriter, sBKType, sBKFileDate, sBKStatusDate, sBKStatus);
        }
        private void WriteBKData(XmlWriter xmlWriter, string sBKType, string sBKFileDate, string sBKStatusDate, string sBKStatus)
        {
            xmlWriter.WriteAttributeString("Bankrupt_Open_Type", sBKType);
            xmlWriter.WriteAttributeString("Bankrupt_Open_Date", sBKFileDate);
            xmlWriter.WriteAttributeString("Bankrupt_Prior_Date", sBKStatusDate);
            xmlWriter.WriteAttributeString("Bankrupt_Prior_Status", sBKStatus);
        }
        private void WriteBK(XmlWriter xmlWriter, CAppData cApp)
        {
            string sBKType, sBKFileDate, sBKStatusDate, sBKStatus;
            sBKType = sBKFileDate = sBKStatusDate = sBKStatus = "";
            if (cApp.aProdCrManualBk7Has || cApp.aProdCrManualBk13Has)
            {
                List<string> lFactData = new List<string>(3);
                switch (cApp.aProdCrManualMostRecentBkT)
                {
                    case E_BkType.Ch11:
                        sBKType = "11";
                        break;
                    case E_BkType.Ch12:
                        sBKType = "12";
                        break;
                    case E_BkType.Ch13:
                        sBKType = "13";
                        break;
                    case E_BkType.Ch7:
                        sBKType = "7";
                        break;
                    case E_BkType.Any:
                    case E_BkType.UnknownType:
                        break;
                    default:
                        throw new CBaseException(ErrorMessages.Encompass_GenericExportErrorMessage, string.Format("Unexpected BK type on file: {0}", m_dataLoan.sProdCrManualMostRecentBkT.ToString()));
                }
                lFactData = EncompassResponseHelper.FACTData(cApp.aProdCrManualMostRecentFileMon_rep,
                    cApp.aProdCrManualMostRecentFileYr_rep, cApp.aProdCrManualMostRecentStatusT,
                    cApp.aProdCrManualMostRecentSatisfiedMon_rep, 
                    cApp.aProdCrManualMostRecentSatisfiedYr_rep);

                if (lFactData.Count > 2)
                {
                    sBKFileDate = lFactData[0];
                    sBKStatus = lFactData[1];
                    sBKStatusDate = lFactData[2];
                }
            }

            WriteBKData(xmlWriter, sBKType, sBKFileDate, sBKStatusDate, sBKStatus);
        }
        private void WriteFCFromCredit(XmlWriter xmlWriter, CAppData cApp)
        {
            string sFCFileDate, sFCStatusDate, sFCStatus;
            sFCFileDate = sFCStatusDate = sFCStatus = "";
            bool bHasFC = !string.IsNullOrEmpty(cApp.aFileDOfLastForeclosure_rep);

            List<string> lFactData = new List<string>(3);
            if (bHasFC)
            {
                lFactData = FCFromCredit(cApp);
            }

            if (lFactData.Count > 2)
            {
                sFCFileDate = lFactData[0];
                sFCStatus = lFactData[1];
                sFCStatusDate = lFactData[2];
            }
            WriteFCData(xmlWriter, bHasFC, sFCFileDate, sFCStatusDate, sFCStatus);
        }
        private void WriteFC(XmlWriter xmlWriter, CAppData cApp)
        {
            string sFCFileDate, sFCStatusDate, sFCStatus;
            sFCFileDate = sFCStatusDate = sFCStatus = "";
            bool bHasFC = cApp.aProdCrManualForeclosureHas;

            List<string> lFactData = new List<string>(3);
            if (bHasFC)
            {
                lFactData = EncompassResponseHelper.FACTData(cApp.aProdCrManualForeclosureRecentFileMon_rep,
                    cApp.aProdCrManualForeclosureRecentFileYr_rep, cApp.aProdCrManualForeclosureRecentStatusT,
                    cApp.aProdCrManualForeclosureRecentSatisfiedMon_rep, cApp.aProdCrManualForeclosureRecentSatisfiedYr_rep);
            }

            if (lFactData.Count > 2)
            {
                sFCFileDate = lFactData[0];
                sFCStatus = lFactData[1];
                sFCStatusDate = lFactData[2];
            }
            WriteFCData(xmlWriter, bHasFC, sFCFileDate, sFCStatusDate, sFCStatus);
        }
        private void WriteFCData(XmlWriter xmlWriter, bool bHasFC, string sFCFileDate, string sFCStatusDate, string sFCStatus)
        {
            xmlWriter.WriteAttributeString("Foreclosure_Had", bHasFC ? "Y" : "N");
            xmlWriter.WriteAttributeString("Foreclosure_Date_Filed", sFCFileDate);
            xmlWriter.WriteAttributeString("Foreclosure_Date_Satisfied", sFCStatusDate);
            xmlWriter.WriteAttributeString("Foreclosure_Status", sFCStatus);
        }
        private static List<string> BKFromCredit(bool IsChSeven, CAppData cApp)
        {
            List<string> lBKData = new List<string>(4);
            string sBKStatusDate = "";
            if (IsChSeven)
            {
                lBKData.Add("7");
                lBKData.Add(EncompassResponseHelper.BKorFCDate(cApp.aFileDOfLastBk7));
                sBKStatusDate = EncompassResponseHelper.BKorFCDate(cApp.aDischargedDOfLastBk7);
            }
            else
            {
                lBKData.Add("13");
                lBKData.Add(EncompassResponseHelper.BKorFCDate(cApp.aFileDOfLastBk13));
                sBKStatusDate = EncompassResponseHelper.BKorFCDate(cApp.aDischargedDOfLastBk13);
            }

            lBKData.Add(string.IsNullOrEmpty(sBKStatusDate) ?
                    EncompassResponseHelper.BKorFCStatus(E_sProdCrManualDerogRecentStatusT.NotSatisfied) :
                    EncompassResponseHelper.BKorFCStatus(E_sProdCrManualDerogRecentStatusT.Discharged));
            lBKData.Add(sBKStatusDate);
            return lBKData;
        }
        private static int TypeOfMostRecentBK(CAppData cApp)
        {
            bool bHasCh7 = !string.IsNullOrEmpty(cApp.aFileDOfLastBk7_rep);
            bool bHasCh13 = !string.IsNullOrEmpty(cApp.aFileDOfLastBk13_rep);

            if (bHasCh7 && !bHasCh13)
            {
                return 7;
            }
            else if (bHasCh13 && !bHasCh7)
            {
                return 13;
            }
            else if (bHasCh7 && bHasCh13)
            {
                // BB - 10/12/2009
                // If only one BK is not satisfied, count it as most recent
                // Else if both are satisfied, the BK w/most recent satisfied date is most recent
                // If both have the same satisfied date, ch7 is most recent
                // If neither are satisfied, the BK w/most recent filed date is most recent
                // If same filed date, ch7 is most recent
                int iComparison = cApp.aDischargedDOfLastBk7.CompareTo(cApp.aDischargedDOfLastBk13, false);
                if (iComparison > 0)
                {
                    return 7;
                }
                else if (iComparison < 0)
                {
                    return 13;
                }
                else
                {
                    if (cApp.aDischargedDOfLastBk7.IsValid)
                    {
                        return 7;
                    }

                    iComparison = cApp.aFileDOfLastBk7.CompareTo(cApp.aFileDOfLastBk13, false);
                }

                if (iComparison >= 0)
                {
                    return 7;
                }
                else if (iComparison < 0)
                {
                    return 13;
                }
            }

            return 0;
        }
        private static List<string> FCFromCredit(CAppData cApp)
        {
            List<string> lFCData = new List<string>(3);
            lFCData.Add(EncompassResponseHelper.BKorFCDate(cApp.aFileDOfLastForeclosure));
            string sFCStatusDate = EncompassResponseHelper.BKorFCDate(cApp.aDischargedDOfLastForeclosesure);

            lFCData.Add(string.IsNullOrEmpty(sFCStatusDate) ?
                EncompassResponseHelper.BKorFCStatus(E_sProdCrManualDerogRecentStatusT.NotSatisfied) :
                EncompassResponseHelper.BKorFCStatus(E_sProdCrManualDerogRecentStatusT.Discharged));
            lFCData.Add(sFCStatusDate);
            return lFCData;
        }
        #endregion
        #region Credit Scores
        private void WriteScores(XmlWriter xmlWriter)
        {
            CAppData cApp = null;
            for (int i = 0; i < m_dataLoan.nApps; i++)
            {
                cApp = m_dataLoan.GetAppData(i);
                cApp.BorrowerModeT = E_BorrowerModeT.Borrower;
                WriteScoresPerApp(xmlWriter, cApp, i);
            }
        }
        private void WriteScoresPerApp(XmlWriter xmlWriter, CAppData cApp, int iAppIndex)
        {
            WriteScoresPerBorrower(xmlWriter, cApp, iAppIndex, false);
            if (cApp.aBHasSpouse)
            {
                WriteScoresPerBorrower(xmlWriter, cApp, iAppIndex, true);
            }
        }
        private void WriteScoresPerBorrower(XmlWriter xmlWriter, CAppData cApp, int iAppIndex, bool bIsCoborrower)
        {
            cApp.BorrowerModeT = bIsCoborrower ? E_BorrowerModeT.Coborrower : E_BorrowerModeT.Borrower;
            xmlWriter.WriteStartElement("BORROWER");
            EncompassResponseHelper.WriteBorrowerIDAttr(xmlWriter, iAppIndex, false, !bIsCoborrower);
            xmlWriter.WriteAttributeString("Equifax", cApp.aEquifaxScore_rep); 
            xmlWriter.WriteAttributeString("Empirica", cApp.aTransUnionScore_rep);
            xmlWriter.WriteAttributeString("Experian", cApp.aExperianScore_rep);
            xmlWriter.WriteEndElement(); //</BORROWER>
        }
        #endregion
        private void WriteMismoLoan(XmlWriter xmlWriter)
        {
            EncompassMismoExporter mismo = new EncompassMismoExporter();
            Loan loan = mismo.CreateMismoLoan(m_dataLoan.sLId);

            loan.WriteXml(xmlWriter);
        }
        #region Loan Product
        private void WriteProduct(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("Product");
            xmlWriter.WriteAttributeString("Name", m_dataLoan.sLpTemplateNm);
            //xmlWriter.WriteAttributeString("Investor", m_dataLoan.sLpInvestorNm); //BB 12-29-09; Encompass populates the "Lender" field with this in addition to fields that should have the investor name; the Lender field should keep the lender name instead
            xmlWriter.WriteAttributeString("MortgageType", EncompassResponseHelper.MtgType(m_dataLoan.sLT));
            xmlWriter.WriteAttributeString("LoanAmortizationType", EncompassResponseHelper.AmortType(m_dataLoan.sFinMethT));
            xmlWriter.WriteAttributeString("FirstRateAdjustmentMonths", m_dataLoan.sRAdj1stCapMon_rep);
            xmlWriter.WriteAttributeString("LoanAmortizationTermMonths", m_dataLoan.sTerm_rep);
            xmlWriter.WriteAttributeString("BalloonLoanMaturityTermMonths", m_dataLoan.sDue_rep);
            WritePriceQuote(xmlWriter);
            xmlWriter.WriteEndElement(); // </Product>
        }
        private void WritePriceQuote(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("Price_quote");

            if (m_bIsLockRequest)
                xmlWriter.WriteAttributeString("RateLockPeriodDays", m_dataLoan.sProdRLckdDays_rep);
            
            PMLCertInfo pmlCert = new PMLCertInfo(m_dataLoan.sPmlCertXmlContent.Value,
                m_dataLoan.sNoteIR_rep, m_dataLoan.sRAdjMarginR_rep, m_dataLoan.sBrokComp1Pc_rep,
                m_bIsSellSide, m_bIsCompIncludedInPrice);
            
            WriteRate(xmlWriter, pmlCert);
            if (m_dataLoan.sFinMethT != E_sFinMethT.Fixed)
            {
                WriteARMMargin(xmlWriter, pmlCert);
            }

            if (m_bIsLockRequest)
                WritePrice(xmlWriter, pmlCert);

            xmlWriter.WriteEndElement(); // </Price_quote>
        }
        private void WriteRate(XmlWriter xmlWriter, PMLCertInfo pmlCert)
        {
            xmlWriter.WriteStartElement("Rate");
            xmlWriter.WriteAttributeString("BaseRatePercent", pmlCert.BaseRate);

            if (EncompassResponseHelper.ConvertToDecimal(m_dataLoan.sQualIR_rep) != 0.000m)
            {
                xmlWriter.WriteAttributeString("QualifyingRatePercent", m_dataLoan.sQualIR_rep);
            }
            else
            {
                xmlWriter.WriteAttributeString("QualifyingRatePercent", m_dataLoan.sNoteIR_rep);
            }

            if (!m_bIsSellSide)
            {
                xmlWriter.WriteAttributeString("RequestedInterestRatePercent", m_dataLoan.sNoteIR_rep);
            }
            else
            {
                xmlWriter.WriteAttributeString("RequestedInterestRatePercent", pmlCert.AllInRate);
            }

            foreach ( List<string> lDescrValuePair in pmlCert.RateAdjCollection())
            {
                if(lDescrValuePair.Count > 1)
                    WriteLineItem(xmlWriter, lDescrValuePair[0], lDescrValuePair[1]);
            }
            xmlWriter.WriteEndElement(); // </Rate>
        }
        private void WriteLineItem(XmlWriter xmlWriter, string sDescription, string sValue)
        {
            WriteLineItem(xmlWriter, sDescription, sValue, false);
        }
        private void WriteLineItem(XmlWriter xmlWriter, string sDescription, string sValue, bool bIsSRP)
        {
            xmlWriter.WriteStartElement(bIsSRP ? "SRP" : "Line_item");
            xmlWriter.WriteAttributeString("Description", sDescription);
            xmlWriter.WriteAttributeString("Percent", sValue);
            xmlWriter.WriteEndElement(); // </Line_item>
        }
        private void WriteARMMargin(XmlWriter xmlWriter, PMLCertInfo pmlCert)
        {
            xmlWriter.WriteStartElement("ARMMargin");
            xmlWriter.WriteAttributeString("BaseARMMarginPercent", pmlCert.BaseARMMargin);
            if (!m_bIsSellSide)
            {
                xmlWriter.WriteAttributeString("RequestedARMMarginPercent", m_dataLoan.sRAdjMarginR_rep);
            }
            else
            {
                xmlWriter.WriteAttributeString("RequestedARMMarginPercent", pmlCert.AllInARMMargin);
            }

            foreach (List<string> lDescrValuePair in pmlCert.ARMMarginCollection())
            {
                if (lDescrValuePair.Count > 1)
                    WriteLineItem(xmlWriter, lDescrValuePair[0], lDescrValuePair[1]);
            }
            xmlWriter.WriteEndElement(); // </ARMMargin>
        }
        private void WritePrice(XmlWriter xmlWriter, PMLCertInfo pmlCert)
        {
            xmlWriter.WriteStartElement("Price");
            xmlWriter.WriteAttributeString("BasePricePercent", pmlCert.BasePrice);
            decimal dPriceBaseHundred = 100.000m - EncompassResponseHelper.ConvertToDecimal(pmlCert.BasePrice);
            xmlWriter.WriteAttributeString("BasePriceBaseHundred", dPriceBaseHundred.ToString());
            
            string sAllInPrice = GetAllInPrice(pmlCert);
            if (string.IsNullOrEmpty(sAllInPrice))
            {
                sAllInPrice = "0.000";
            }
            string sAllInPriceBHundred = GetAllInPriceBaseHundred(pmlCert);
            if (string.IsNullOrEmpty(sAllInPriceBHundred))
            {
                sAllInPriceBHundred = "0.000";
            }
            xmlWriter.WriteAttributeString("AllInPricePercent", sAllInPrice);
            xmlWriter.WriteAttributeString("AllInPriceBaseHundred", sAllInPriceBHundred);

            foreach (List<string> lDescrValuePair in pmlCert.PriceAdjCollection())
            {
                if (lDescrValuePair.Count > 1)
                    WritePriceLineItem(xmlWriter, lDescrValuePair[0], lDescrValuePair[1]);
            }
            xmlWriter.WriteEndElement(); // </Price>
        }
        private string GetAllInPrice(PMLCertInfo pmlCert)
        {
            return (m_bIsCompIncludedInPrice || m_bIsSellSide) ? 
                pmlCert.AllInPrice : m_dataLoan.sBrokComp1Pc_rep;
        }
        private string GetAllInPriceBaseHundred(PMLCertInfo pmlCert)
        {
            if(m_bIsCompIncludedInPrice || m_bIsSellSide)
            {
                decimal dPriceBaseHundred = 100.000m - EncompassResponseHelper.ConvertToDecimal(pmlCert.AllInPrice);
                return dPriceBaseHundred.ToString();
            }
            return m_dataLoan.sBrokerLockFinalBrokComp1PcPrice_rep;
        }
        private void WritePriceLineItem(XmlWriter xmlWriter, string sDescription, string sValue)
        {
            decimal dValue = EncompassResponseHelper.ConvertToDecimal(sValue) * -1.000m;
            sValue = dValue.ToString();

            WriteLineItem(xmlWriter, sDescription, sValue, sDescription.ToUpper().Contains("SRP"));
        }
        #endregion
        #region PML
        private void WritePML(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("PML");
            xmlWriter.WriteAttributeString("PMLLoanNumber", m_dataLoan.sLNm.ToString());
            WriteSSProperty(xmlWriter);
            WriteSSBorrower(xmlWriter);
            WriteSSProduct(xmlWriter);
            WriteSSOriginatorCompensation(xmlWriter);
            xmlWriter.WriteEndElement(); // </PML>
        }
        private void WriteSSProperty(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("Property");
            xmlWriter.WriteAttributeString("IsRural", EncompassResponseHelper.ConvertToYNString(m_dataLoan.sProdIsSpInRuralArea));
            xmlWriter.WriteAttributeString("IsCondotel", EncompassResponseHelper.ConvertToYNString(m_dataLoan.sProdIsCondotel));
            xmlWriter.WriteAttributeString("IsNonWarrantable", EncompassResponseHelper.ConvertToYNString(m_dataLoan.sProdIsNonwarrantableProj));
            xmlWriter.WriteEndElement(); // </Property>
        }
        private void WriteSSBorrower(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("Borrower");
            xmlWriter.WriteAttributeString("IsFTHB", EncompassResponseHelper.ConvertToYNString(m_dataLoan.sHas1stTimeBuyer));
            xmlWriter.WriteAttributeString("HasHousingHistory", EncompassResponseHelper.ConvertToYNString(m_dataLoan.sProdHasHousingHistory));
            xmlWriter.WriteAttributeString("Reserves", m_dataLoan.sProdAvailReserveMonths_rep);
            xmlWriter.WriteEndElement(); // </Borrower>
        }
        private void WriteSSProduct(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("Product");
            xmlWriter.WriteAttributeString("AUS", m_dataLoan.sProd3rdPartyUwResultT.ToString());
            xmlWriter.WriteAttributeString("DURefiPlus", EncompassResponseHelper.ConvertToYNString(m_dataLoan.sProdIsDuRefiPlus));
            xmlWriter.WriteAttributeString("Term", m_dataLoan.sTerm_rep);
            xmlWriter.WriteAttributeString("Amort", m_dataLoan.sFinMethT_rep);
            xmlWriter.WriteAttributeString("FixedTerm", m_dataLoan.sRAdj1stCapMon_rep);
            List<string> lProcessingTypes = EncompassResponseHelper.AUSProcessingTypes(
                m_dataLoan.sProdIncludeNormalProc, m_dataLoan.sProdIncludeMyCommunityProc,
                m_dataLoan.sProdIncludeHomePossibleProc, m_dataLoan.sProdIncludeFHATotalProc,
                m_dataLoan.sProdIncludeVAProc);
            foreach (string sType in lProcessingTypes)
            {
                WriteAUSProcessingType(xmlWriter, sType);
            }
            xmlWriter.WriteEndElement(); // </Product>
        }
        private void WriteAUSProcessingType(XmlWriter xmlWriter, string sType)
        {
            xmlWriter.WriteStartElement("AUSProcessingType");
            xmlWriter.WriteAttributeString("Type", sType);
            xmlWriter.WriteEndElement(); // </AUSProcessingType>
        }
        private void WriteSSOriginatorCompensation(XmlWriter xmlWriter)
        {
            string sPercentage, sBasis, sBaseAmount, sFixedAdjAmount, sTotalAmount, sMin, sMax;
            sPercentage = sBasis = sBaseAmount = sFixedAdjAmount = sTotalAmount = sMin = sMax = "";
            xmlWriter.WriteStartElement("OriginatorCompensation");
            xmlWriter.WriteAttributeString("PaymentSource", m_dataLoan.sOriginatorCompensationPaymentSourceT.ToString());

            if (m_dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
            {
                sPercentage = m_dataLoan.sOriginatorCompensationPercent_rep;
                sBasis = m_dataLoan.sOriginatorCompensationBaseT.ToString();
                sBaseAmount = m_dataLoan.sOriginatorCompensationAdjBaseAmount_rep;
                sFixedAdjAmount = m_dataLoan.sOriginatorCompensationFixedAmount_rep;
                sTotalAmount = m_dataLoan.sOriginatorCompensationTotalAmount_rep;
                sMin = m_dataLoan.sOriginatorCompensationMinAmount_rep;
                sMax = m_dataLoan.sOriginatorCompensationMaxAmount_rep;
            }
            else
            {
                sPercentage = m_dataLoan.sOriginatorCompensationBorrPaidPc_rep;
                sBasis = m_dataLoan.sOriginatorCompensationBorrPaidBaseT.ToString();
                sBaseAmount = m_dataLoan.sOriginatorCompensationBorrPaidBaseAmt_rep;
                sFixedAdjAmount = m_dataLoan.sOriginatorCompensationBorrPaidMb_rep;
                sTotalAmount = m_dataLoan.sOriginatorCompensationBorrTotalAmount_rep;
            }
            xmlWriter.WriteAttributeString("CompensationPercentage", sPercentage);
            xmlWriter.WriteAttributeString("CompensationBasis", sBasis);
            xmlWriter.WriteAttributeString("CompensationBaseAmount", sBaseAmount);
            xmlWriter.WriteAttributeString("CompensationFixedAdjAmount", sFixedAdjAmount);
            xmlWriter.WriteAttributeString("CompensationTotalAmount", sTotalAmount);
            xmlWriter.WriteAttributeString("CompensationMinimum", sMin);
            xmlWriter.WriteAttributeString("CompensationMaximum", sMax);
            xmlWriter.WriteEndElement(); // </OriginatorCompensation>
        }
        #endregion
        #region AUS
        private void WriteAUS(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("AUS");
            string sFindings = m_dataLoan.sDuFindingsHtml;
            if (!string.IsNullOrEmpty(sFindings))
                WriteDODUFindings(xmlWriter, sFindings);

            // BB 3/23/11 - The html certificate is never empty. It either contains findings or 
            //  a "no findings" message.
            if (!string.IsNullOrEmpty(m_dataLoan.sFreddieFeedbackResponseXml.Value))
                WriteLPFindings(xmlWriter, m_dataLoan.sFreddieFeedbackHtml.Value);
            
            if (!string.IsNullOrEmpty(m_dataLoan.sTotalScoreCertificateXmlContent.Value))
                WriteTOTALFindings(xmlWriter, m_dataLoan.sTotalScoreCertificateHtml.Value);
            xmlWriter.WriteEndElement(); // </AUS>
        }
        private void WriteDODUFindings(XmlWriter xmlWriter, string sHTML)
        {
            xmlWriter.WriteStartElement("DO_DUFindings");
            xmlWriter.WriteCData(sHTML);
            xmlWriter.WriteEndElement(); // </DO_DUFindings>
        }
        private void WriteLPFindings(XmlWriter xmlWriter, string sXML)
        {
            xmlWriter.WriteStartElement("LPFindings");
            xmlWriter.WriteCData(sXML);
            xmlWriter.WriteEndElement(); // </LPFindings>
        }
        private void WriteTOTALFindings(XmlWriter xmlWriter, string sHTML)
        {
            xmlWriter.WriteStartElement("TOTALFindings");
            xmlWriter.WriteAttributeString("CreditRiskAssessment", m_dataLoan.sTotalScoreCreditRiskResultT.ToString());
            xmlWriter.WriteAttributeString("EligibilityAssessment", m_dataLoan.sTotalScoreEvalStatusT.ToString());
            xmlWriter.WriteAttributeString("TOTALLoanID", m_dataLoan.sTotalScoreUniqueLoanId);
            xmlWriter.WriteAttributeString("CaseNumber", m_dataLoan.sAgencyCaseNum);
            xmlWriter.WriteAttributeString("OrderDate", m_dataLoan.sTotalScorecardLastRunD.ToStringWithTime("G"));
            xmlWriter.WriteCData(sHTML);
            xmlWriter.WriteEndElement(); // </TOTALFindings>
        }
        #endregion
        #endregion
    }
}
