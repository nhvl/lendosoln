﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using DataAccess;
using LendersOffice.Common;

namespace Integration.Encompass
{
    public class PMLCertInfo
    {
        #region Variables
        // Would use Dictionary, but neither values nor descriptions are guaranteed to be unique
        private List<string> m_lRateAdj = null;
        private List<string> m_lARMMarginAdj = null;
        private List<string> m_lPriceAdj = null;
        private decimal m_dBaseRate = 0.000m;
        private decimal m_dBaseARMMargin = 0.000m;
        private decimal m_dBasePrice = 0.000m;
        private string m_sAllinRate = "";
        private string m_sAllinARMMargin = "";
        private string m_sAllinPrice = "";
        private enum eAdjustmentType { RATE = 0, ARMMARGIN, PRICE } ;
        private string m_sPMLCertificate;
        private string m_sPerRateOptionAdjCodes = "";
        private string m_sPerRateOptionHiddenAdjCodes = "";
        private XmlNode m_xNodeSelectedRateOption = null;

        public string BaseRate
        {
            get { return m_dBaseRate.ToString(); }
        }
        public string BaseARMMargin
        {
            get { return m_dBaseARMMargin.ToString(); }
        }
        public string BasePrice
        {
            get { return m_dBasePrice.ToString(); }
        }
        public string AllInRate
        {
            get { return m_sAllinRate; }
        }
        public string AllInARMMargin
        {
            get { return m_sAllinARMMargin; }
        }
        public string AllInPrice
        {
            get { return m_sAllinPrice; }
        }
        #endregion

        public IEnumerable RateAdjCollection()
        {
            for (int i = 0; i < m_lRateAdj.Count - 1; i += 2)
            {
                List<string> lRateAdjs = new List<string>(2);
                lRateAdjs.Add(m_lRateAdj[i]);
                lRateAdjs.Add(m_lRateAdj[i + 1]);
                yield return lRateAdjs;
            }
        }
        public IEnumerable ARMMarginCollection()
        {
            for (int i = 0; i < m_lARMMarginAdj.Count - 1; i += 2)
            {
                List<string> lMarginAdjs = new List<string>(2);
                lMarginAdjs.Add(m_lARMMarginAdj[i]);
                lMarginAdjs.Add(m_lARMMarginAdj[i + 1]);
                yield return lMarginAdjs;
            }
        }
        public IEnumerable PriceAdjCollection()
        {
            for (int i = 0; i < m_lPriceAdj.Count - 1; i += 2)
            {
                List<string> lPriceAdjs = new List<string>(2);
                lPriceAdjs.Add(m_lPriceAdj[i]);
                lPriceAdjs.Add(m_lPriceAdj[i + 1]);
                yield return lPriceAdjs;
            }
        }

        public PMLCertInfo(string sPMLCertificate, string sRequestedRate, string sRequestedARMMargin, string sRequestedPrice, bool bIsSellSide, bool bIsCompInclInPrice)
        {
            m_sPMLCertificate = sPMLCertificate;
            m_dBaseRate = EncompassResponseHelper.ConvertToDecimal(sRequestedRate);
            m_sAllinRate = sRequestedRate;
            m_dBaseARMMargin = EncompassResponseHelper.ConvertToDecimal(sRequestedARMMargin);
            m_sAllinARMMargin = sRequestedARMMargin;

            GetSelectedRateOption(); //BB 11/13/12 Need to grab the selected rate option in order to parse any per-rate-option codes

            if (bIsCompInclInPrice && !bIsSellSide)
            {
                sRequestedPrice = LookupRequestedPrice();
            }
            m_dBasePrice = EncompassResponseHelper.ConvertToDecimal(sRequestedPrice);
            m_sAllinPrice = sRequestedPrice;

            if (sPMLCertificate != null)
            {
                m_lRateAdj = new List<string>();
                m_lARMMarginAdj = new List<string>();
                m_lPriceAdj = new List<string>();

                XmlDocument xdocCert = Tools.CreateXmlDoc(m_sPMLCertificate);
                XmlElement xRoot = (XmlElement)xdocCert.SelectSingleNode("//pml_cert/pricing/adjustments");
                if (xRoot != null)
                {
                    LookupPerRateOptionCodes();
                    ParseAdjustments(xdocCert, bIsSellSide);
                }
            }
        }

        private void GetSelectedRateOption()
        {
            XmlAttributeCollection xAttributes = null;
            XmlDocument xdocCert = Tools.CreateXmlDoc(m_sPMLCertificate);
            foreach (XmlNode xNodeRateOption in xdocCert.SelectNodes("//pml_cert/pricing/rate_options/rate_option"))
            {
                xAttributes = xNodeRateOption.Attributes;
                if (xAttributes.Count != 0)
                {
                    XmlAttribute xAttribute = (XmlAttribute)xAttributes.GetNamedItem("selected");
                    if (xAttribute != null)
                    {
                        if (0 == string.Compare(xAttribute.Value.TrimWhitespaceAndBOM(), "TRUE", true))
                        {
                            m_xNodeSelectedRateOption = xNodeRateOption;
                        }
                    }
                }
            }
        }

        private string LookupRequestedPrice()
        {
            string sPrice = "";
            if (m_xNodeSelectedRateOption != null)
            {
                sPrice = EncompassResponseHelper.TrimElement(m_xNodeSelectedRateOption["comp_point"]);
            }
            return sPrice;
        }

        private void LookupPerRateOptionCodes()
        {
            if (m_xNodeSelectedRateOption != null)
            {
                XmlAttributeCollection xAttributes = m_xNodeSelectedRateOption.Attributes;
                XmlAttribute xAttribute = (XmlAttribute)xAttributes.GetNamedItem("perOptionAdjStr");
                if (xAttribute != null)
                {
                    m_sPerRateOptionAdjCodes = xAttribute.Value.TrimWhitespaceAndBOM();
                }

                xAttribute = (XmlAttribute)xAttributes.GetNamedItem("perOptionAdjHiddenStr");
                if (xAttribute != null)
                {
                    m_sPerRateOptionHiddenAdjCodes = xAttribute.Value.TrimWhitespaceAndBOM();
                }
            }
        }

        private void ParseAdjustments(XmlDocument xdocCert, bool bIsSellSide)
        {
            ParseVisibleOrHiddenAdjs(xdocCert, true, bIsSellSide);
            ParseVisibleOrHiddenAdjs(xdocCert, false, bIsSellSide);
        }

        private void ParseVisibleOrHiddenAdjs(XmlDocument xdocCert, bool bVisible, bool bIsSellSide)
        {
            string sNodeLoc = bVisible ? "//pml_cert/pricing/adjustments/adjustment" :
                           "//pml_cert/pricing/hiddendadjustments/adjustment";
            foreach (XmlNode xNodeAdj in xdocCert.SelectNodes(sNodeLoc))
            {
                XmlAttributeCollection xAttributes = xNodeAdj.Attributes;
                if (IsAdjApplicableToSelectedRateOption((XmlAttribute)xAttributes.GetNamedItem("perOptionAdjStr"), bVisible))
                {
                    XmlElement xRate = xNodeAdj["rate"];
                    XmlElement xARMMargin = xNodeAdj["margin"];
                    XmlElement xPrice = xNodeAdj["fee"];
                    XmlElement xDescription = xNodeAdj["description"];

                    ParseAdjustment(xDescription, xRate, bVisible, bIsSellSide, eAdjustmentType.RATE);
                    ParseAdjustment(xDescription, xARMMargin, bVisible, bIsSellSide, eAdjustmentType.ARMMARGIN);
                    ParseAdjustment(xDescription, xPrice, bVisible, bIsSellSide, eAdjustmentType.PRICE);
                }
            }
        }

        private bool IsAdjApplicableToSelectedRateOption(XmlAttribute xAdjCode, bool bVisible)
        {
            bool bApplicable = true;
            string sRateOptionCodes = bVisible ? m_sPerRateOptionAdjCodes : m_sPerRateOptionHiddenAdjCodes;
            if (xAdjCode != null)
            {
                string sAdjCode = xAdjCode.Value.TrimWhitespaceAndBOM();
                if (!String.IsNullOrEmpty(sAdjCode))
                {
                    bApplicable = false;
                    string[] sROCodes = sRateOptionCodes.Split(',');
                    foreach (string sCode in sROCodes)
                    {
                        if(sCode.TrimWhitespaceAndBOM().Equals(sAdjCode, StringComparison.CurrentCultureIgnoreCase))
                            bApplicable = true;
                    }
                }
            }
            return bApplicable;
        }

        private void ParseAdjustment(XmlElement xAdjDescr, XmlElement xAdjValue, bool bVisible, bool bIsSellSide, eAdjustmentType eAdjType)
        {
            string sAdjVal = "";
            string sAdjDescr = xAdjDescr.InnerText.TrimWhitespaceAndBOM();
            if (!IsEmpty(xAdjValue))
            {
                sAdjVal = EncompassResponseHelper.TrimElement(xAdjValue);
                decimal dAdjVal = EncompassResponseHelper.ConvertToDecimal(sAdjVal);
                if (ShouldDisplayAdj(bVisible, bIsSellSide, sAdjDescr))
                {
                    switch (eAdjType)
                    {
                        case eAdjustmentType.RATE:
                            m_lRateAdj.Add(sAdjDescr);
                            m_lRateAdj.Add(sAdjVal);
                            m_dBaseRate = m_dBaseRate - dAdjVal;
                            break;
                        case eAdjustmentType.ARMMARGIN:
                            m_lARMMarginAdj.Add(sAdjDescr);
                            m_lARMMarginAdj.Add(sAdjVal);
                            m_dBaseARMMargin = m_dBaseARMMargin - dAdjVal;
                            break;
                        case eAdjustmentType.PRICE:
                            m_lPriceAdj.Add(sAdjDescr);
                            m_lPriceAdj.Add(sAdjVal);
                            m_dBasePrice = m_dBasePrice - dAdjVal;
                            break;
                        default:
                            throw new CBaseException(ErrorMessages.Encompass_GenericExportErrorMessage, string.Format("Unexpected adjustment type on file: {0}", eAdjType.ToString()));
                    }
                }
                else if(bIsSellSide && IsPriceGroupAdj(sAdjDescr))
                {
                    decimal dAllinVal;
                    switch(eAdjType) 
                    {
                        case eAdjustmentType.RATE:
                            m_dBaseRate = m_dBaseRate - dAdjVal;
                            dAllinVal = EncompassResponseHelper.ConvertToDecimal(m_sAllinRate) - dAdjVal;
                            m_sAllinRate = dAllinVal.ToString();
                            break;
                        case eAdjustmentType.ARMMARGIN:
                            m_dBaseARMMargin = m_dBaseARMMargin - dAdjVal;
                            dAllinVal = EncompassResponseHelper.ConvertToDecimal(m_sAllinARMMargin) - dAdjVal;
                            m_sAllinARMMargin = dAllinVal.ToString();
                            break;
                        case eAdjustmentType.PRICE:
                            m_dBasePrice = m_dBasePrice - dAdjVal;
                            dAllinVal = EncompassResponseHelper.ConvertToDecimal(m_sAllinPrice) - dAdjVal;
                            m_sAllinPrice = dAllinVal.ToString();
                            break;
                        default:
                            throw new CBaseException(ErrorMessages.Encompass_GenericExportErrorMessage, string.Format("Unexpected adjustment type on file: {0}", eAdjType.ToString()));
                    }
                }
            }
        }

        private static bool ShouldDisplayAdj(bool bVisible, bool bIsSellSide, string sAdjDescr)
        {
            bool bDisplay = false;
            bDisplay = (bVisible || bIsSellSide) && !IsPriceGroupAdj(sAdjDescr);
            return bDisplay;
        }
        private static bool IsPriceGroupAdj(string sAdj)
        {
            bool bIsPGAdj = false;
            if (!String.IsNullOrEmpty(sAdj))
            {
                bIsPGAdj = sAdj.ToUpper().IndexOf("PRICE GROUP") >= 0;
            }
            return bIsPGAdj;
        }

        private static bool IsEmpty(XmlElement xAdjustment)
        {
            if (xAdjustment == null)
                return true;

            if (xAdjustment.InnerText.TrimWhitespaceAndBOM().Equals(""))
                return true;

            decimal v;

            if (decimal.TryParse(EncompassResponseHelper.TrimElement(xAdjustment), out v) == false)
            {
                return false;
            }
            if (v == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
