﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Integration.Encompass
{
    public class EPassSellSideInfo
    {
        #region Variables
        private bool m_bRural = false;
        private bool m_bCondotel = false;
        private bool m_bNonWarrantable = false;
        private bool m_bFTHB = false;
        private bool m_bHasHousingHistory = true;
        private string m_sReservesMo = "";
        private string m_sAUSResponse = "";
        private bool m_bDURefiPlus = false;
        private string m_sTerm = "";
        private string m_sAmort = "";
        private string m_sFixedTerm = "";
        private List<string> m_lAUSProcessingTypes = new List<string>();
        private string m_sOriginatorCompensationPaymentSource = "";
        private string m_sOriginatorCompensationPercentage = "";
        private string m_sOriginatorCompensationBasis = "";
        private string m_sOriginatorCompensationFixedAdjAmount = "";
        private string m_sOriginatorCompensationMin = "";
        private string m_sOriginatorCompensationMax = "";

        public bool IsRural
        {
            get { return m_bRural; }
            set { m_bRural = value; }
        }
        public bool IsCondotel
        {
            get { return m_bCondotel; }
            set { m_bCondotel = value; }
        }
        public bool IsNonWarrantable
        {
            get { return m_bNonWarrantable; }
            set { m_bNonWarrantable = value; }
        }
        public bool IsFTHB
        {
            get { return m_bFTHB; }
            set { m_bFTHB = value; }
        }
        public bool HasHousingHistory
        {
            get { return m_bHasHousingHistory; }
            set { m_bHasHousingHistory = value; }
        }
        public string ReservesMonths
        {
            get { return m_sReservesMo; }
            set { m_sReservesMo = value; }
        }
        public string AUSResponse
        {
            get { return m_sAUSResponse; }
            set { m_sAUSResponse = value; }
        }
        public bool IsDURefiPlus
        {
            get { return m_bDURefiPlus; }
            set { m_bDURefiPlus = value; }
        }
        public string Term
        {
            get { return m_sTerm; }
            set { m_sTerm = value; }
        }
        public string Amortization
        {
            get { return m_sAmort; }
            set { m_sAmort = value; }
        }
        public string FixedTerm
        {
            get { return m_sFixedTerm; }
            set { m_sFixedTerm = value; }
        }
        public List<string> AUSProcessingTypes
        {
            get { return m_lAUSProcessingTypes; }
        }
        public string OriginatorCompensationPaymentSource
        {
            get { return m_sOriginatorCompensationPaymentSource; }
            set { m_sOriginatorCompensationPaymentSource = value; }
        }
        public string OriginatorCompensationPercentage
        {
            get { return m_sOriginatorCompensationPercentage; }
            set { m_sOriginatorCompensationPercentage = value; }
        }
        public string OriginatorCompensationBasis
        {
            get { return m_sOriginatorCompensationBasis; }
            set { m_sOriginatorCompensationBasis = value; }
        }
        public string OriginatorCompensationFixedAdjAmount
        {
            get { return m_sOriginatorCompensationFixedAdjAmount; }
            set { m_sOriginatorCompensationFixedAdjAmount = value; }
        }
        public string OriginatorCompensationMin
        {
            get { return m_sOriginatorCompensationMin; }
            set { m_sOriginatorCompensationMin = value; }
        }
        public string OriginatorCompensationMax
        {
            get { return m_sOriginatorCompensationMax; }
            set { m_sOriginatorCompensationMax = value; }
        }
        #endregion

        public EPassSellSideInfo()
        { }

        public void AddAUSProcessingType(string sType)
        {
            if(!string.IsNullOrEmpty(sType))
                m_lAUSProcessingTypes.Add(sType);
        }
    }
}
