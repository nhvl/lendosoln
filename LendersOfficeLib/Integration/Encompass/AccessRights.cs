﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace Integration.Encompass
{
    public class AccessRights
    {
        #region Variables
        private E_LpeRunModeT m_eLPERunModeT = E_LpeRunModeT.NotAllowed;
        private CPageData m_cpLoanData = null;

        public E_LpeRunModeT LPERunMode
        { 
            get { return m_eLPERunModeT; } 
        }
        #endregion

        public AccessRights()
        {}

        public bool HasWriteAccess(Guid gLoanID)
        {
            m_cpLoanData = CPageData.CreateUsingSmartDependency(gLoanID, typeof(AccessRights));
            m_cpLoanData.InitLoad();
            m_cpLoanData.SetFormatTarget(FormatTarget.MismoClosing);

            m_eLPERunModeT = m_cpLoanData.lpeRunModeT;

            return m_eLPERunModeT == E_LpeRunModeT.Full;
        }
    }
}
