﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Conversions.MismoClosing231;
using LendersOffice.Constants;
using LendersOffice.Common;
using LendersOffice.CreditReport;
using LendersOffice.Security;
using System.Threading;
using LendersOffice.Audit;

namespace Integration.Encompass
{
    public static class EncompassMismoImporter
    {
        public static void Import(Guid sLId, string xml, bool isSyncEncompassLoanNumber)
        {
            EPassInfo ePassInfo = new EPassInfo(Tools.CreateXmlDoc(xml));
            Import(sLId, ePassInfo, isSyncEncompassLoanNumber);
        }

        public static void Import(Guid sLId, EPassInfo ePassInfo, bool isSyncEncompassLoanNumber)
        {
            MismoClosing231Importer mcImporter = new MismoClosing231Importer();
            mcImporter.IsSyncLoanOriginationSystemLoanIdentifierWithLoanNumber = isSyncEncompassLoanNumber;
            mcImporter.Import(sLId, ePassInfo.MISMO);

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(EncompassMismoImporter));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.SetFormatTarget(FormatTarget.MismoClosing);

            // Wrapper data
            dataLoan.sEncompassLoanId = ePassInfo.EncompassLoanID;
            dataLoan.sSpGrossRentLckd = true;
            dataLoan.sSpGrossRent_rep = ePassInfo.SubjPropGrossRent;
            E_aTotalScoreFhtbCounselingT eFTHBCounseling = ParseFTHBCounseling(ePassInfo);

            for (int i = 0; i < dataLoan.nApps; i++)
            {
                bool bHasCreditReport = false;
                CAppData cApp = dataLoan.GetAppData(i);
                SetFTHBCounseling(dataLoan, cApp, eFTHBCounseling);

                // 3/2/2011 dd - Import the credit report here if it is available.
                if (i < ePassInfo.CreditReportList.Count)
                {
                    // 3/2/2011 dd - EllieMae will order the credit report in sequence.
                    
                    ICreditReportResponse mismoCreditResponse = ePassInfo.CreditReportList[i];
                    if (null != mismoCreditResponse)
                    {
                        AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;

                        Guid comId = ConstAppDavid.DummyFannieMaeServiceCompany;

                        CreditReportUtilities.SaveXmlCreditReport(mismoCreditResponse, principal, dataLoan.sLId, cApp.aAppId, comId, Guid.Empty, E_CreditReportAuditSourceT.ImportFromDoDu);
                        CreditReportUtilities.ImportLiabilities(dataLoan.sLId, cApp.aAppId, true /* skipZeroBalance */, false /* deleteExistingLiabilities */, true /* importBorrowerInfo */);
                        bHasCreditReport = true;
                    }
                }

                if (bHasCreditReport == false)
                {
                    // 10/17/2011 dd - Only parse manual credit report data when actual credit report is missing.
                    ParseDerogs(cApp, i, ePassInfo);
                    ParseScores(cApp, i, ePassInfo);
                }
            }

            if (EncompassAuthHelper.IsSellSide(ePassInfo.RequestType))
            {
                ParseSellSideInfo(dataLoan, ePassInfo);
            }

            dataLoan.Save();

        }

        private static void ParseDerogs(CAppData cApp, int iAppIndex, EPassInfo ePassInfo)
        {
            EPassDerogInfo eDerogs = ePassInfo.Derogs;
            cApp.BorrowerModeT = E_BorrowerModeT.Borrower;
            SetMtgLates(cApp, eDerogs, iAppIndex);

            ParseBK(cApp, eDerogs, iAppIndex);

            ParseFC(cApp, eDerogs, iAppIndex);
        }
        private static void SetMtgLates(CAppData cApp, EPassDerogInfo eDerogs, int iAppIndex)
        {
            cApp.aProdCrManualNonRolling30MortLateCount_rep = eDerogs.X30Late(iAppIndex);
            cApp.aProdCrManual30MortLateCount_rep = eDerogs.X30Late(iAppIndex);
            cApp.aProdCrManual60MortLateCount_rep = eDerogs.X60Late(iAppIndex);
            cApp.aProdCrManualRolling60MortLateCount_rep = eDerogs.X60Late(iAppIndex);
            cApp.aProdCrManual90MortLateCount_rep = eDerogs.X90Late(iAppIndex);
            cApp.aProdCrManualRolling90MortLateCount_rep = eDerogs.X90Late(iAppIndex);
            cApp.aProdCrManual120MortLateCount_rep = eDerogs.X120Late(iAppIndex);
        }

        private static void ParseBK(CAppData cApp, EPassDerogInfo eDerogs, int iAppIndex)
        {
            // Encompass only includes one set of BK info per app;
            // Clear data in case the user corrects their former BK entry in Encompass
            ClearBKFields(cApp);

            List<string> lMnthYr = null;
            string sBKType = eDerogs.BKType(iAppIndex).TrimWhitespaceAndBOM().ToUpper();
            switch (sBKType)
            {
                case "7":
                case "13":
                    bool bChSeven = sBKType.Equals("7");
                    cApp.aProdCrManualBk7Has = bChSeven;
                    cApp.aProdCrManualBk13Has = !bChSeven;

                    lMnthYr = GetMonthAndYear(eDerogs.BKFileDate(iAppIndex));
                    if (lMnthYr.Count > 1)
                    {
                        if (bChSeven)
                        {
                            cApp.aProdCrManualBk7RecentFileMon_rep = lMnthYr[0];
                            cApp.aProdCrManualBk7RecentFileYr_rep = lMnthYr[1];
                        }
                        else
                        {
                            cApp.aProdCrManualBk13RecentFileMon_rep = lMnthYr[0];
                            cApp.aProdCrManualBk13RecentFileYr_rep = lMnthYr[1];
                        }
                    }

                    ParseBKorFCStatus(cApp, eDerogs.BKStatus(iAppIndex), bChSeven, false);

                    lMnthYr = GetMonthAndYear(eDerogs.BKSatisfiedDate(iAppIndex));
                    if (lMnthYr.Count > 1)
                    {
                        if (bChSeven)
                        {
                            cApp.aProdCrManualBk7RecentSatisfiedMon_rep = lMnthYr[0];
                            cApp.aProdCrManualBk7RecentSatisfiedYr_rep = lMnthYr[1];
                        }
                        else
                        {
                            cApp.aProdCrManualBk13RecentSatisfiedMon_rep = lMnthYr[0];
                            cApp.aProdCrManualBk13RecentSatisfiedYr_rep = lMnthYr[1];
                        }
                    }
                    break;

                case "11":
                case "NONE":
                case "":
                    break;

                default:
                    throw new CBaseException(ErrorMessages.Encompass_BKorFCTypeorStatusUnexpected, string.Format("The BK type from Encompass was: {0}", eDerogs.BKType(iAppIndex)));
            }
        }

        private static void ClearBKFields(CAppData cApp)
        {
            cApp.aProdCrManualBk7Has = false;
            cApp.aProdCrManualBk7RecentFileMon_rep = "";
            cApp.aProdCrManualBk7RecentFileYr_rep = "";
            cApp.aProdCrManualBk7RecentSatisfiedMon_rep = "";
            cApp.aProdCrManualBk7RecentSatisfiedYr_rep = "";
            cApp.aProdCrManualBk7RecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
            cApp.aProdCrManualBk13Has = false;
            cApp.aProdCrManualBk13RecentFileMon_rep = "";
            cApp.aProdCrManualBk13RecentFileYr_rep = "";
            cApp.aProdCrManualBk13RecentSatisfiedMon_rep = "";
            cApp.aProdCrManualBk13RecentSatisfiedYr_rep = "";
            cApp.aProdCrManualBk13RecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
        }
        private static List<string> GetMonthAndYear(string sDate)
        {
            List<string> lMnthYr = new List<string>(2);
            DateTime dt;
            if (DateTime.TryParse(sDate, out dt))
            {
                lMnthYr.Add(dt.Month.ToString());
                lMnthYr.Add(dt.Year.ToString());
            }
            return lMnthYr;
        }
        private static void ParseBKorFCStatus(CAppData cApp, string sBKorFCStatus, bool bChSeven, bool bFC)
        {
            switch (sBKorFCStatus.TrimWhitespaceAndBOM().ToUpper())
            {
                case "":
                    if (bFC)
                        cApp.aProdCrManualForeclosureRecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
                    else if (bChSeven)
                        cApp.aProdCrManualBk7RecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
                    else
                        cApp.aProdCrManualBk13RecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
                    break;

                case "DISCHARGED":
                    if (bFC)
                        cApp.aProdCrManualForeclosureRecentStatusT = E_sProdCrManualDerogRecentStatusT.Discharged;
                    else if (bChSeven)
                        cApp.aProdCrManualBk7RecentStatusT = E_sProdCrManualDerogRecentStatusT.Discharged;
                    else
                        cApp.aProdCrManualBk13RecentStatusT = E_sProdCrManualDerogRecentStatusT.Discharged;
                    break;

                case "DISMISSED":
                    if (bFC)
                        cApp.aProdCrManualForeclosureRecentStatusT = E_sProdCrManualDerogRecentStatusT.Dismissed;
                    else if (bChSeven)
                        cApp.aProdCrManualBk7RecentStatusT = E_sProdCrManualDerogRecentStatusT.Dismissed;
                    else
                        cApp.aProdCrManualBk13RecentStatusT = E_sProdCrManualDerogRecentStatusT.Dismissed;
                    break;

                default:
                    if (bFC)
                        throw new CBaseException(ErrorMessages.Encompass_BKorFCTypeorStatusUnexpected, string.Format("The FC status from Encompass was: {0}", sBKorFCStatus));
                    else
                        throw new CBaseException(ErrorMessages.Encompass_BKorFCTypeorStatusUnexpected, string.Format("The BK status from Encompass was: {0}", sBKorFCStatus));
            }
        }
        private static void ParseFC(CAppData cApp, EPassDerogInfo eDerogs, int iAppIndex)
        {
            bool bHasFC = eDerogs.FCOnFile(iAppIndex).TrimWhitespaceAndBOM().ToUpper().Equals("Y");
            cApp.aProdCrManualForeclosureHas = bHasFC;
            if (bHasFC)
            {
                List<string> lMnthYr = GetMonthAndYear(eDerogs.FCFileDate(iAppIndex));
                if (lMnthYr.Count > 1)
                {
                    cApp.aProdCrManualForeclosureRecentFileMon_rep = lMnthYr[0];
                    cApp.aProdCrManualForeclosureRecentFileYr_rep = lMnthYr[1];
                }

                ParseBKorFCStatus(cApp, eDerogs.FCStatus(iAppIndex), false, true);

                lMnthYr = GetMonthAndYear(eDerogs.FCSatisfiedDate(iAppIndex));
                if (lMnthYr.Count > 1)
                {
                    cApp.aProdCrManualForeclosureRecentSatisfiedMon_rep = lMnthYr[0];
                    cApp.aProdCrManualForeclosureRecentSatisfiedYr_rep = lMnthYr[1];
                }
            }
            else
            {
                ClearFCFields(cApp);
            }
        }
        private static void ClearFCFields(CAppData cApp)
        {
            cApp.aProdCrManualForeclosureHas = false;
            cApp.aProdCrManualForeclosureRecentFileMon_rep = "";
            cApp.aProdCrManualForeclosureRecentFileYr_rep = "";
            cApp.aProdCrManualForeclosureRecentSatisfiedMon_rep = "";
            cApp.aProdCrManualForeclosureRecentSatisfiedYr_rep = "";
            cApp.aProdCrManualForeclosureRecentStatusT = E_sProdCrManualDerogRecentStatusT.NotSatisfied;
        }
        private static void ParseScores(CAppData cApp, int iAppIndex, EPassInfo ePassInfo)
        {
            cApp.BorrowerModeT = E_BorrowerModeT.Borrower;
            SetScores(cApp, ePassInfo.Scores.GetScores(iAppIndex, false));
            cApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
            SetScores(cApp, ePassInfo.Scores.GetScores(iAppIndex, true));
        }

        private static void SetScores(CAppData cApp, List<string> lScores)
        {
            if (lScores.Count >= 3)
            {
                // 11/16/2012 dd - Per OPM 105771 - We only override credit score from Encompass if it is non-zero.
                if (string.IsNullOrEmpty(lScores[0]) == false && lScores[0] != "0")
                {
                    cApp.aEquifaxScore_rep = lScores[0];
                }
                if (string.IsNullOrEmpty(lScores[1]) == false && lScores[1] != "0")
                {
                    cApp.aTransUnionScore_rep = lScores[1];
                }
                if (string.IsNullOrEmpty(lScores[2]) == false && lScores[2] != "0")
                {
                    cApp.aExperianScore_rep = lScores[2];
                }
            }
        }
        private static E_aTotalScoreFhtbCounselingT ParseFTHBCounseling(EPassInfo ePassInfo)
        {
            E_aTotalScoreFhtbCounselingT FTHBCounseling = E_aTotalScoreFhtbCounselingT.LeaveBlank;
            if (!string.IsNullOrEmpty(ePassInfo.FTHBCounseling))
            {
                switch (ePassInfo.FTHBCounseling.TrimWhitespaceAndBOM().ToUpper())
                {
                    case "HUD APPROVED COUNSELING":
                        FTHBCounseling = E_aTotalScoreFhtbCounselingT.HudApprovedCounseling;
                        break;
                    case "NOT COUNSELED":
                        FTHBCounseling = E_aTotalScoreFhtbCounselingT.NotCounseled;
                        break;
                    default:
                        FTHBCounseling = E_aTotalScoreFhtbCounselingT.LeaveBlank;
                        break;
                }
            }
            return FTHBCounseling;
        }
        private static void SetFTHBCounseling(CPageData dataLoan, CAppData cApp, E_aTotalScoreFhtbCounselingT FTHBCounseling)
        {
            if (cApp.aBIsValidNameSsn 
                && (cApp.aBTotalScoreIsFthb || !dataLoan.sUseLegacyTotalCounselTypeDefinition))
            {
                cApp.aBTotalScoreFhtbCounselingT = FTHBCounseling;
            }
            if (cApp.aCIsValidNameSsn 
                && (cApp.aCTotalScoreIsFthb || !dataLoan.sUseLegacyTotalCounselTypeDefinition))
            {
                cApp.aCTotalScoreFhtbCounselingT = FTHBCounseling;
            }
        }
        private static void ParseSellSideInfo(CPageData dataLoan, EPassInfo ePassInfo)
        {
            EPassSellSideInfo eSellSideInfo = ePassInfo.SellSideInfo;
            dataLoan.sProdIsSpInRuralArea = eSellSideInfo.IsRural;
            dataLoan.sProdIsCondotel = eSellSideInfo.IsCondotel;
            dataLoan.sProdIsNonwarrantableProj = eSellSideInfo.IsNonWarrantable;

            dataLoan.sProdAvailReserveMonths_rep = eSellSideInfo.ReservesMonths;
            dataLoan.sProd3rdPartyUwResultT = AUSResponse(eSellSideInfo.AUSResponse);
            dataLoan.sProdIsDuRefiPlus = eSellSideInfo.IsDURefiPlus;

            ParseFilters(dataLoan, eSellSideInfo.Term, AmortType(eSellSideInfo.Amortization), eSellSideInfo.FixedTerm);

            ParseAUSProcessingTypes(dataLoan, eSellSideInfo.AUSProcessingTypes);

            if (!string.IsNullOrEmpty(eSellSideInfo.OriginatorCompensationPercentage))
            {
                ParseOriginatorCompensation(dataLoan, eSellSideInfo.OriginatorCompensationPaymentSource,
                    eSellSideInfo.OriginatorCompensationPercentage, eSellSideInfo.OriginatorCompensationBasis,
                    eSellSideInfo.OriginatorCompensationFixedAdjAmount, eSellSideInfo.OriginatorCompensationMin,
                    eSellSideInfo.OriginatorCompensationMax);
            }
        }
        private static E_sProd3rdPartyUwResultT AUSResponse(string sResponse)
        {
            if (string.IsNullOrEmpty(sResponse))
                return E_sProd3rdPartyUwResultT.NA;

            switch (sResponse.TrimWhitespaceAndBOM().ToUpper())
            {
                case "NA": return E_sProd3rdPartyUwResultT.NA;
                case "DU_APPROVEELIGIBLE": return E_sProd3rdPartyUwResultT.DU_ApproveEligible;
                case "DU_APPROVEINELIGIBLE": return E_sProd3rdPartyUwResultT.DU_ApproveIneligible;
                case "DU_REFERELIGIBLE": return E_sProd3rdPartyUwResultT.DU_ReferEligible;
                case "DU_REFERINELIGIBLE": return E_sProd3rdPartyUwResultT.DU_ReferIneligible;
                case "DU_REFERWCAUTIONELIGIBLE": return E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible;
                case "DU_REFERWCAUTIONINELIGIBLE": return E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible;
                case "DU_EAIELIGIBLE": return E_sProd3rdPartyUwResultT.DU_EAIEligible;
                case "DU_EAIIELIGIBLE": return E_sProd3rdPartyUwResultT.DU_EAIIEligible;
                case "DU_EAIIIELIGIBLE": return E_sProd3rdPartyUwResultT.DU_EAIIIEligible;
                case "LP_ACCEPTELIGIBLE": return E_sProd3rdPartyUwResultT.LP_AcceptEligible;
                case "LP_ACCEPTINELIGIBLE": return E_sProd3rdPartyUwResultT.LP_AcceptIneligible;
                case "LP_CAUTIONELIGIBLE": return E_sProd3rdPartyUwResultT.LP_CautionEligible;
                case "LP_CAUTIONINELIGIBLE": return E_sProd3rdPartyUwResultT.LP_CautionIneligible;
                case "OUTOFSCOPE": return E_sProd3rdPartyUwResultT.OutOfScope;
                case "LP_AMINUS_LEVEL1": return E_sProd3rdPartyUwResultT.Lp_AMinus_Level1;
                case "LP_AMINUS_LEVEL2": return E_sProd3rdPartyUwResultT.Lp_AMinus_Level2;
                case "LP_AMINUS_LEVEL3": return E_sProd3rdPartyUwResultT.Lp_AMinus_Level3;
                case "LP_AMINUS_LEVEL4": return E_sProd3rdPartyUwResultT.Lp_AMinus_Level4;
                case "LP_AMINUS_LEVEL5": return E_sProd3rdPartyUwResultT.Lp_AMinus_Level5;
                case "LP_REFER": return E_sProd3rdPartyUwResultT.Lp_Refer;
                case "TOTAL_APPROVEELIGIBLE": return E_sProd3rdPartyUwResultT.Total_ApproveEligible;
                case "TOTAL_APPROVEINELIGIBLE": return E_sProd3rdPartyUwResultT.Total_ApproveIneligible;
                case "TOTAL_REFERELIGIBLE": return E_sProd3rdPartyUwResultT.Total_ReferEligible;
                case "TOTAL_REFERINELIGIBLE": return E_sProd3rdPartyUwResultT.Total_ReferIneligible;
                default: return E_sProd3rdPartyUwResultT.NA;
            }
        }

        private static void ParseFilters(CPageData dataLoan, string sTerm, E_sFinMethT eAmortType, string sFixedTerm)
        {
            if (!string.IsNullOrEmpty(sTerm))
            {
                sTerm = sTerm.TrimWhitespaceAndBOM();
                bool b15 = (sTerm == "180");
                bool b20 = (sTerm == "240");
                bool b30 = (sTerm == "360");                
                dataLoan.sProdFilterDue15Yrs = b15;
                dataLoan.sProdFilterDue20Yrs = b20;
                dataLoan.sProdFilterDue30Yrs = b30;                
                dataLoan.sProdFilterDueOther = (!b15 && !b20 && !b30);
            }

            dataLoan.sProdFilterFinMethFixed = (eAmortType == E_sFinMethT.Fixed);
            if (eAmortType == E_sFinMethT.ARM)
            {
                ParseARMTypes(dataLoan, sFixedTerm);
            }
            else
            {
                ClearARMTypes(dataLoan);
            }
        }

        private static E_sFinMethT AmortType(string sAmortType)
        {
            if (string.IsNullOrEmpty(sAmortType))
                return E_sFinMethT.Fixed;

            switch (sAmortType.TrimWhitespaceAndBOM().ToUpper())
            {
                case "ARM": return E_sFinMethT.ARM;
                case "FIXED": return E_sFinMethT.Fixed;
                case "GRADUATED": return E_sFinMethT.Graduated;
                default: return E_sFinMethT.Fixed;
            }
        }
        private static void ParseAUSProcessingTypes(CPageData dataLoan, List<string> lProcessingTypes)
        {
            dataLoan.sProdIncludeNormalProc = dataLoan.sProdIncludeMyCommunityProc =
                dataLoan.sProdIncludeHomePossibleProc = dataLoan.sProdIncludeFHATotalProc =
                dataLoan.sProdIncludeVAProc = false;

            foreach (string sType in lProcessingTypes)
            {
                switch (sType)
                {
                    case "MCM":
                        dataLoan.sProdIncludeMyCommunityProc = true;
                        break;
                    case "HomePossible":
                        dataLoan.sProdIncludeHomePossibleProc = true;
                        break;
                    case "FHA":
                        dataLoan.sProdIncludeFHATotalProc = true;
                        break;
                    case "VA":
                        dataLoan.sProdIncludeVAProc = true;
                        break;
                    case "Conventional":
                    default:
                        dataLoan.sProdIncludeNormalProc = true;
                        break;
                }
            }
        }

        private static void ParseARMTypes(CPageData dataLoan, string sFixedTerm)
        {            
            bool bThreeYr = false;
            bool bFiveYr = false;
            bool bSevenYr = false;
            bool bTenYr = false;
            bool bOther = false;

            if (!string.IsNullOrEmpty(sFixedTerm))
            {
                sFixedTerm = sFixedTerm.TrimWhitespaceAndBOM();
                int iFixedTerm = 0;
                if (Int32.TryParse(sFixedTerm, out iFixedTerm))
                {
                    if(iFixedTerm == 36)
                        bThreeYr = true;
                    else if (iFixedTerm == 60)
                        bFiveYr = true;
                    else if (iFixedTerm == 84)
                        bSevenYr = true;
                    else if (iFixedTerm == 120)
                        bTenYr = true;
                    else
                        bOther = true;
                }
            }            
            dataLoan.sProdFilterFinMeth3YrsArm = bThreeYr;
            dataLoan.sProdFilterFinMeth5YrsArm = bFiveYr;
            dataLoan.sProdFilterFinMeth7YrsArm = bSevenYr;
            dataLoan.sProdFilterFinMeth10YrsArm = bTenYr;
            dataLoan.sProdFilterFinMethOther = bOther;
        }
        private static void ClearARMTypes(CPageData dataLoan)
        {
            dataLoan.sProdFilterFinMethLess1YrArm =
            dataLoan.sProdFilterFinMeth2YrsArm =
            dataLoan.sProdFilterFinMeth3YrsArm =
            dataLoan.sProdFilterFinMeth5YrsArm =
            dataLoan.sProdFilterFinMeth7YrsArm =
            dataLoan.sProdFilterFinMeth10YrsArm =
            dataLoan.sProdFilterFinMethOther = false;
        }
        private static void ParseOriginatorCompensation(CPageData dataLoan, string sPaymentSource, string sPercentage, string sBasis, string sFixedAmount, string sMin, string sMax)
        {
            E_sOriginatorCompensationPaymentSourceT ePaymentSourceT = OrigCompPaymentSourceType(sPaymentSource);
            dataLoan.sOriginatorCompensationPaymentSourceT = ePaymentSourceT;
            if (ePaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
            {
                dataLoan.ApplyCompensationManually(DateTime.Today.ToString(), DateTime.Today.ToString(),
                    sPercentage, OrigCompBasisType(sBasis), sMin, sMax, sFixedAmount, "", false);
            }
            else
            {
                dataLoan.SetOriginatorCompensation(ePaymentSourceT, sPercentage, OrigCompBasisType(sBasis), sFixedAmount);
            }
        }

        private static E_sOriginatorCompensationPaymentSourceT OrigCompPaymentSourceType(string sPaymentSource)
        {
            if (string.IsNullOrEmpty(sPaymentSource))
                return E_sOriginatorCompensationPaymentSourceT.SourceNotSpecified;

            switch (sPaymentSource.TrimWhitespaceAndBOM().ToUpper())
            {
                case "BORROWERPAID": return E_sOriginatorCompensationPaymentSourceT.BorrowerPaid;
                case "LENDERPAID": return E_sOriginatorCompensationPaymentSourceT.LenderPaid;
                case "SOURCENOTSPECIFIED":
                default:
                    return E_sOriginatorCompensationPaymentSourceT.SourceNotSpecified;
            }
        }
        private static E_PercentBaseT OrigCompBasisType(string sBasis)
        {
            if (string.IsNullOrEmpty(sBasis))
                return E_PercentBaseT.TotalLoanAmount;

            switch (sBasis.TrimWhitespaceAndBOM().ToUpper())
            {
                case "LOANAMOUNT": return E_PercentBaseT.LoanAmount;
                case "TOTALLOANAMOUNT": return E_PercentBaseT.TotalLoanAmount;
                case "SALESPRICE": return E_PercentBaseT.SalesPrice;
                case "APPRAISALVALUE": return E_PercentBaseT.AppraisalValue;
                case "ORIGINALCOST": return E_PercentBaseT.OriginalCost;
                case "AVERAGEOUTSTANDINGBALANCE": return E_PercentBaseT.AverageOutstandingBalance;
                case "ALLYSP": return E_PercentBaseT.AllYSP;
                default: return E_PercentBaseT.TotalLoanAmount;
            }
        }
    }
}
