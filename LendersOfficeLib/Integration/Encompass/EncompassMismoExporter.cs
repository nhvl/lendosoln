﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mismo.Closing2_4;
using DataAccess;
using LendersOffice.Reminders;
using LendersOffice.Security;
using LendersOffice.Admin;
using XmlSerializableCommon;
using LendersOffice.ObjLib.Task;
using LendersOffice.Common;

namespace Integration.Encompass
{
    public class EncompassMismoExporter
    {
        private CPageData m_dataLoan = null;

        public Loan CreateMismoLoan(Guid sLId)
        {
            m_dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(EncompassMismoExporter));
            m_dataLoan.InitLoad();
            m_dataLoan.SetFormatTarget(FormatTarget.MismoClosing);

            Loan loan = CreateLoan();
            return loan;
        }
        private Loan CreateLoan()
        {
            Loan loan = new Loan();
            loan.MismoVersionIdentifier = E_LoanMismoVersionIdentifier._2_3_1;
            loan.Application = CreateApplication();
            loan.ClosingDocuments = CreateClosingDocuments();
            return loan;
        }
        private ClosingDocuments CreateClosingDocuments()
        {
            ClosingDocuments closingDocuments = new ClosingDocuments();
            closingDocuments.ClosingInstructionsList.Add(CreateClosingInstructions()); // TODO: Need to create a loop

            //closingDocuments.AllongeToNote = CreateAllongeToNote();
            //closingDocuments.Beneficiary = CreateBeneficiary();
            //closingDocuments.ClosingAgentList.Add(CreateClosingAgent()); // TODO: Need to create a loop
            
            //closingDocuments.CompensationList.Add(CreateCompensation()); // TODO: Need to create a loop
            //closingDocuments.CosignerList.Add(CreateCosigner()); // TODO: Need to create a loop
            //closingDocuments.EscrowAccountDetailList.Add(CreateEscrowAccountDetail()); // TODO: Need to create a loop
            //closingDocuments.Execution = CreateExecution();
            //closingDocuments.Investor = CreateInvestor();
            //closingDocuments.Lender = CreateLender();
            //closingDocuments.LenderBranch = CreateLenderBranch();
            //closingDocuments.LoanDetails = CreateLoanDetails();
            //closingDocuments.LossPayeeList.Add(CreateLossPayee()); // TODO: Need to create a loop
            //closingDocuments.MortgageBroker = CreateMortgageBroker();
            //closingDocuments.PaymentDetails = CreatePaymentDetails();
            //closingDocuments.PayoffList.Add(CreatePayoff()); // TODO: Need to create a loop
            //closingDocuments.RecordableDocumentList.Add(CreateRecordableDocument()); // TODO: Need to create a loop
            //closingDocuments.RespaHudDetailList.Add(CreateRespaHudDetail()); // TODO: Need to create a loop
            //closingDocuments.RespaServicingData = CreateRespaServicingData();
            //closingDocuments.RespaSummary = CreateRespaSummary();
            //closingDocuments.SellerList.Add(CreateSeller()); // TODO: Need to create a loop
            //closingDocuments.ServicerList.Add(CreateServicer()); // TODO: Need to create a loop
            //closingDocuments.BuilderList.Add(CreateBuilder()); // TODO: Need to create a loop
            //closingDocuments.ClosingCostList.Add(CreateClosingCost()); // TODO: Need to create a loop
            //closingDocuments.TrustList.Add(CreateTrust()); // TODO: Need to create a loop
            return closingDocuments;
        }
        private Trust CreateTrust()
        {
            Trust trust = new Trust();
            //trust.Id = null;
            //trust.NonPersonEntityIndicator = null;
            //trust.EstablishedDate = null;
            //trust.Name = null;
            //trust.State = null;
            //trust.NonObligatedIndicator = null;
            //trust.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            //trust.BeneficiaryList.Add(CreateBeneficiary()); // TODO: Need to create a loop
            //trust.TrusteeList.Add(CreateTrustee()); // TODO: Need to create a loop
            //trust.GrantorList.Add(CreateGrantor()); // TODO: Need to create a loop
            return trust;
        }
        private Grantor CreateGrantor()
        {
            Grantor grantor = new Grantor();
            //grantor.Id = null;
            //grantor.MaritalStatusType = null;
            //grantor.NonPersonEntityIndicator = null;
            //grantor.CapacityDescription = null;
            //grantor.City = null;
            //grantor.Country = null;
            //grantor.County = null;
            //grantor.FirstName = null;
            //grantor.LastName = null;
            //grantor.MiddleName = null;
            //grantor.NameSuffix = null;
            //grantor.PostalCode = null;
            //grantor.SequenceIdentifier = null;
            //grantor.State = null;
            //grantor.StreetAddress = null;
            //grantor.StreetAddress2 = null;
            //grantor.UnparsedName = null;
            //grantor.AliasList.Add(CreateAlias()); // TODO: Need to create a loop
            return grantor;
        }
        private Alias CreateAlias()
        {
            Alias alias = new Alias();
            //alias.Id = null;
            //alias.AccountIdentifier = null;
            //alias.CreditorName = null;
            //alias.FirstName = null;
            //alias.LastName = null;
            //alias.MiddleName = null;
            //alias.NameSuffix = null;
            //alias.SequenceIdentifier = null;
            //alias.Type = null;
            //alias.TypeOtherDescription = null;
            //alias.UnparsedName = null;
            return alias;
        }
        private Trustee CreateTrustee()
        {
            Trustee trustee = new Trustee();
            //trustee.Id = null;
            //trustee.NonPersonEntityIndicator = null;
            //trustee.City = null;
            //trustee.Country = null;
            //trustee.County = null;
            //trustee.PostalCode = null;
            //trustee.State = null;
            //trustee.StreetAddress = null;
            //trustee.StreetAddress2 = null;
            //trustee.Type = null;
            //trustee.TypeOtherDescription = null;
            //trustee.UnparsedName = null;
            //trustee.ContactDetail = CreateContactDetail();
            //trustee.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return trustee;
        }
        private NonPersonEntityDetail CreateNonPersonEntityDetail()
        {
            NonPersonEntityDetail nonPersonEntityDetail = new NonPersonEntityDetail();
            //nonPersonEntityDetail.Id = null;
            //nonPersonEntityDetail.MersOrganizationIdentifier = null;
            //nonPersonEntityDetail.OrganizationLicensingTypeDescription = null;
            //nonPersonEntityDetail.OrganizationType = null;
            //nonPersonEntityDetail.OrganizationTypeOtherDescription = null;
            //nonPersonEntityDetail.OrganizedUnderTheLawsOfJurisdictionName = null;
            //nonPersonEntityDetail.SuccessorClauseTextDescription = null;
            //nonPersonEntityDetail.TaxIdentificationNumberIdentifier = null;
            //nonPersonEntityDetail.AuthorizedRepresentativeList.Add(CreateAuthorizedRepresentative()); // TODO: Need to create a loop
            return nonPersonEntityDetail;
        }
        private AuthorizedRepresentative CreateAuthorizedRepresentative()
        {
            AuthorizedRepresentative authorizedRepresentative = new AuthorizedRepresentative();
            //authorizedRepresentative.Id = null;
            //authorizedRepresentative.AuthorizedToSignIndicator = null;
            //authorizedRepresentative.TitleDescription = null;
            //authorizedRepresentative.UnparsedName = null;
            //authorizedRepresentative.ContactDetail = CreateContactDetail();
            return authorizedRepresentative;
        }
        private ContactDetail CreateContactDetail()
        {
            ContactDetail contactDetail = new ContactDetail();
            //contactDetail.Id = null;
            //contactDetail.FirstName = null;
            //contactDetail.Identifier = null;
            //contactDetail.LastName = null;
            //contactDetail.MiddleName = null;
            //contactDetail.Name = null;
            //contactDetail.NameSuffix = null;
            //contactDetail.SequenceIdentifier = null;
            //contactDetail.ContactPointList.Add(CreateContactPoint()); // TODO: Need to create a loop
            return contactDetail;
        }
        private ContactPoint CreateContactPoint()
        {
            ContactPoint contactPoint = new ContactPoint();
            //contactPoint.Id = null;
            //contactPoint.PreferenceIndicator = null;
            //contactPoint.RoleType = null;
            //contactPoint.RoleTypeOtherDescription = null;
            //contactPoint.Type = null;
            //contactPoint.TypeOtherDescription = null;
            //contactPoint.Value = null;
            return contactPoint;
        }
        private Beneficiary CreateBeneficiary()
        {
            Beneficiary beneficiary = new Beneficiary();
            //beneficiary.Id = null;
            //beneficiary.NonPersonEntityIndicator = null;
            //beneficiary.City = null;
            //beneficiary.Country = null;
            //beneficiary.County = null;
            //beneficiary.PostalCode = null;
            //beneficiary.State = null;
            //beneficiary.StreetAddress = null;
            //beneficiary.StreetAddress2 = null;
            //beneficiary.UnparsedName = null;
            //beneficiary.ContactDetail = CreateContactDetail();
            //beneficiary.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return beneficiary;
        }
        private ClosingCost CreateClosingCost()
        {
            ClosingCost closingCost = new ClosingCost();
            //closingCost.Id = null;
            //closingCost.ContributionAmount = null;
            //closingCost.FinancedIndicator = null;
            //closingCost.FundsType = null;
            //closingCost.FundsTypeOtherDescription = null;
            //closingCost.SourceType = null;
            //closingCost.SourceTypeOtherDescription = null;
            return closingCost;
        }
        private Builder CreateBuilder()
        {
            Builder builder = new Builder();
            //builder.Id = null;
            //builder.NonPersonEntityIndicator = null;
            //builder.City = null;
            //builder.Country = null;
            //builder.County = null;
            //builder.LIcenseIdentifier = null;
            //builder.LicenseState = null;
            //builder.PostalCode = null;
            //builder.State = null;
            //builder.StreetAddress = null;
            //builder.StreetAddress2 = null;
            //builder.UnparsedName = null;
            //builder.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return builder;
        }
        private Servicer CreateServicer()
        {
            Servicer servicer = new Servicer();
            //servicer.Id = null;
            //servicer.NonPersonEntityIndicator = null;
            //servicer.City = null;
            //servicer.Country = null;
            //servicer.DaysOfTheWeekDescription = null;
            //servicer.DirectInquiryToDescription = null;
            //servicer.HoursOfTheDayDescription = null;
            //servicer.InquiryTelephoneNumber = null;
            //servicer.Name = null;
            //servicer.PaymentAcceptanceEndDate = null;
            //servicer.PaymentAcceptanceStartDate = null;
            //servicer.PostalCode = null;
            //servicer.State = null;
            //servicer.StreetAddress = null;
            //servicer.StreetAddress2 = null;
            //servicer.TransferEffectiveDate = null;
            //servicer.Type = null;
            //servicer.TypeOtherDescription = null;
            //servicer.ContactDetail = CreateContactDetail();
            //servicer.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            //servicer.QualifiedWrittenRequestMailTo = CreateQualifiedWrittenRequestMailTo();
            return servicer;
        }
        private QualifiedWrittenRequestMailTo CreateQualifiedWrittenRequestMailTo()
        {
            QualifiedWrittenRequestMailTo qualifiedWrittenRequestMailTo = new QualifiedWrittenRequestMailTo();
            //qualifiedWrittenRequestMailTo.Id = null;
            //qualifiedWrittenRequestMailTo.City = null;
            //qualifiedWrittenRequestMailTo.Country = null;
            //qualifiedWrittenRequestMailTo.PostalCode = null;
            //qualifiedWrittenRequestMailTo.State = null;
            //qualifiedWrittenRequestMailTo.StreetAddress = null;
            //qualifiedWrittenRequestMailTo.StreetAddress2 = null;
            return qualifiedWrittenRequestMailTo;
        }
        private Seller CreateSeller()
        {
            Seller seller = new Seller();
            //seller.Id = null;
            //seller.NonPersonEntityIndicator = null;
            //seller.City = null;
            //seller.Country = null;
            //seller.FirstName = null;
            //seller.Identifier = null;
            //seller.LastName = null;
            //seller.MiddleName = null;
            //seller.NameSuffix = null;
            //seller.PostalCode = null;
            //seller.SequenceIdentifier = null;
            //seller.State = null;
            //seller.StreetAddress = null;
            //seller.StreetAddress2 = null;
            //seller.UnparsedName = null;
            //seller.ContactDetail = CreateContactDetail();
            //seller.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            //seller.PowerOfAttorney = CreatePowerOfAttorney();
            return seller;
        }
        private PowerOfAttorney CreatePowerOfAttorney()
        {
            PowerOfAttorney powerOfAttorney = new PowerOfAttorney();
            //powerOfAttorney.Id = null;
            //powerOfAttorney.SigningCapacityTextDescription = null;
            //powerOfAttorney.TitleDescription = null;
            //powerOfAttorney.UnparsedName = null;
            return powerOfAttorney;
        }
        private RespaSummary CreateRespaSummary()
        {
            RespaSummary respaSummary = new RespaSummary();
            //respaSummary.Id = null;
            //respaSummary.Apr = null;
            //respaSummary.DisclosedTotalSalesPriceAmount = null;
            //respaSummary.TotalAprFeesAmount = null;
            //respaSummary.TotalAmountFinancedAmount = null;
            //respaSummary.TotalDepositedReservesAmount = null;
            //respaSummary.TotalFilingRecordingFeeAmount = null;
            //respaSummary.TotalFinanceChargeAmount = null;
            //respaSummary.TotalNetBorrowerFeesAmount = null;
            //respaSummary.TotalNetProceedsForFundingAmount = null;
            //respaSummary.TotalNetSellerFeesAmount = null;
            //respaSummary.TotalNonAprFeesAmount = null;
            //respaSummary.TotalOfAllPaymentsAmount = null;
            //respaSummary.TotalPaidToOthersAmount = null;
            //respaSummary.TotalPrepaidFinanceChargeAmount = null;
            //respaSummary.TotalFeesPaidToList.Add(CreateTotalFeesPaidTo()); // TODO: Need to create a loop
            //respaSummary.TotalFeesPaidByList.Add(CreateTotalFeesPaidBy()); // TODO: Need to create a loop
            return respaSummary;
        }
        private TotalFeesPaidBy CreateTotalFeesPaidBy()
        {
            TotalFeesPaidBy totalFeesPaidBy = new TotalFeesPaidBy();
            //totalFeesPaidBy.Id = null;
            //totalFeesPaidBy.Type = null;
            //totalFeesPaidBy.TypeAmount = null;
            //totalFeesPaidBy.TypeOtherDescription = null;
            return totalFeesPaidBy;
        }
        private TotalFeesPaidTo CreateTotalFeesPaidTo()
        {
            TotalFeesPaidTo totalFeesPaidTo = new TotalFeesPaidTo();
            //totalFeesPaidTo.Id = null;
            //totalFeesPaidTo.Type = null;
            //totalFeesPaidTo.TypeAmount = null;
            //totalFeesPaidTo.TypeOtherDescription = null;
            return totalFeesPaidTo;
        }
        private RespaServicingData CreateRespaServicingData()
        {
            RespaServicingData respaServicingData = new RespaServicingData();
            //respaServicingData.Id = null;
            //respaServicingData.AreAbleToServiceIndicator = null;
            //respaServicingData.AssignSellOrTransferSomeServicingIndicator = null;
            //respaServicingData.DoNotServiceIndicator = null;
            //respaServicingData.ExpectToAssignSellOrTransferPercent = null;
            //respaServicingData.ExpectToAssignSellOrTransferPercentOfServicingIndicator = null;
            //respaServicingData.ExpectToRetainAllServicingIndicator = null;
            //respaServicingData.ExpectToSellAllServicingIndicator = null;
            //respaServicingData.FirstTransferYear = null;
            //respaServicingData.FirstTransferYearValue = null;
            //respaServicingData.HaveNotDecidedToServiceIndicator = null;
            //respaServicingData.HaveNotServicedInPastThreeYearsIndicator = null;
            //respaServicingData.HavePreviouslyAssignedServicingIndicator = null;
            //respaServicingData.MayAssignServicingIndicator = null;
            //respaServicingData.PresentlyIntendToAssignSellOrTransferServicingIndicator = null;
            //respaServicingData.SecondTransferYear = null;
            //respaServicingData.SecondTransferYearValue = null;
            //respaServicingData.ThirdTransferYear = null;
            //respaServicingData.ThirdTransferYearValue = null;
            //respaServicingData.ThisEstimateIncludesAssignmentsSalesOrTransfersIndicator = null;
            //respaServicingData.ThisInformationDoesIncludeAssignmentsSalesOrTransfersIndicator = null;
            //respaServicingData.ThisIsOurRecordOfTransferringServicingIndicator = null;
            //respaServicingData.TwelveMonthPeriodTransferPercent = null;
            //respaServicingData.WillNotServiceIndicator = null;
            //respaServicingData.WillServiceIndicator = null;
            return respaServicingData;
        }
        private RespaHudDetail CreateRespaHudDetail()
        {
            RespaHudDetail respaHudDetail = new RespaHudDetail();
            //respaHudDetail.Id = null;
            //respaHudDetail.Hud1CashToOrFromBorrowerIndicator = null;
            //respaHudDetail.Hud1CashToOrFromSellerIndicator = null;
            //respaHudDetail.Hud1ConventionalInsuredIndicator = null;
            //respaHudDetail.Hud1FileNumberIdentifier = null;
            //respaHudDetail.Hud1LenderUnparsedAddress = null;
            //respaHudDetail.Hud1LenderUnparsedName = null;
            //respaHudDetail.Hud1LineItemFromDate = null;
            //respaHudDetail.Hud1LineItemToDate = null;
            //respaHudDetail.Hud1SettlementAgentUnparsedAddress = null;
            //respaHudDetail.Hud1SettlementAgentUnparsedName = null;
            //respaHudDetail.Hud1SettlementDate = null;
            //respaHudDetail.LineItemAmount = null;
            //respaHudDetail.LineItemDescription = null;
            //respaHudDetail.LineItemPaidByType = null;
            //respaHudDetail.SpecifiedHudLineNumber = null;
            return respaHudDetail;
        }
        private RecordableDocument CreateRecordableDocument()
        {
            RecordableDocument recordableDocument = new RecordableDocument();
            //recordableDocument.Id = null;
            //recordableDocument.SequenceIdentifier = null;
            //recordableDocument.Type = null;
            //recordableDocument.TypeOtherDescription = null;
            //recordableDocument.AssignFrom = CreateAssignFrom();
            //recordableDocument.AssignTo = CreateAssignTo();
            //recordableDocument.Default = CreateDefault();
            //recordableDocument.NotaryList.Add(CreateNotary()); // TODO: Need to create a loop
            //recordableDocument.PreparedBy = CreatePreparedBy();
            //recordableDocument.AssociatedDocument = CreateAssociatedDocument();
            //recordableDocument.ReturnToList.Add(CreateReturnTo()); // TODO: Need to create a loop
            //recordableDocument.Riders = CreateRiders();
            //recordableDocument.SecurityInstrument = CreateSecurityInstrument();
            //recordableDocument.TrusteeList.Add(CreateTrustee()); // TODO: Need to create a loop
            //recordableDocument.WitnessList.Add(CreateWitness()); // TODO: Need to create a loop
            return recordableDocument;
        }
        private Witness CreateWitness()
        {
            Witness witness = new Witness();
            //witness.Id = null;
            //witness.SequenceIdentifier = null;
            //witness.UnparsedName = null;
            return witness;
        }
        private SecurityInstrument CreateSecurityInstrument()
        {
            SecurityInstrument securityInstrument = new SecurityInstrument();
            //securityInstrument.Id = null;
            //securityInstrument.PropertyLongLegalDescriptionPageNumberDescription = null;
            //securityInstrument.PropertyLongLegalPageNumberDescription = null;
            //securityInstrument.RecordedDocumentIdentifier = null;
            //securityInstrument.AssumptionFeeAmount = null;
            //securityInstrument.AttorneyFeeMinimumAmount = null;
            //securityInstrument.AttorneyFeePercent = null;
            //securityInstrument.CertifyingAttorneyName = null;
            //securityInstrument.MaximumPrincipalIndebtednessAmount = null;
            //securityInstrument.MultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator = null;
            //securityInstrument.NoteHolderName = null;
            //securityInstrument.NoticeOfConfidentialtyRightsDescription = null;
            //securityInstrument.OtherFeesAmount = null;
            //securityInstrument.OtherFeesDescription = null;
            //securityInstrument.OweltyOfPartitionIndicator = null;
            //securityInstrument.PersonAuthorizedToReleaseLienName = null;
            //securityInstrument.PersonAuthorizedToReleaseLienPhoneNumber = null;
            //securityInstrument.PersonAuthorizedToReleaseLienTitle = null;
            //securityInstrument.PurchaseMoneyIndicator = null;
            //securityInstrument.RealPropertyImprovedOrToBeImprovedIndicator = null;
            //securityInstrument.RealPropertyImprovementsNotCoveredIndicator = null;
            //securityInstrument.RecordingRequestedByName = null;
            //securityInstrument.RenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator = null;
            //securityInstrument.SignerForRegistersOfficeName = null;
            //securityInstrument.TaxSerialNumberIdentifier = null;
            //securityInstrument.TrusteeFeePercent = null;
            //securityInstrument.VendorsLienDescription = null;
            //securityInstrument.VendorsLienIndicator = null;
            //securityInstrument.VestingDescription = null;
            //securityInstrument.TaxablePartyList.Add(CreateTaxableParty()); // TODO: Need to create a loop
            return securityInstrument;
        }
        private TaxableParty CreateTaxableParty()
        {
            TaxableParty taxableParty = new TaxableParty();
            //taxableParty.Id = null;
            //taxableParty.City = null;
            //taxableParty.Country = null;
            //taxableParty.CountryCode = null;
            //taxableParty.County = null;
            //taxableParty.PostalCode = null;
            //taxableParty.SequenceIdentifier = null;
            //taxableParty.State = null;
            //taxableParty.StreetAddress = null;
            //taxableParty.StreetAddress2 = null;
            //taxableParty.TitleDescription = null;
            //taxableParty.UnparsedName = null;
            //taxableParty.PreferredResponseList.Add(CreatePreferredResponse()); // TODO: Need to create a loop
            //taxableParty.ContactDetail = CreateContactDetail();
            return taxableParty;
        }
        private PreferredResponse CreatePreferredResponse()
        {
            PreferredResponse preferredResponse = new PreferredResponse();
            //preferredResponse.Id = null;
            //preferredResponse.MimeType = null;
            //preferredResponse.Destination = null;
            //preferredResponse.Format = null;
            //preferredResponse.FormatOtherDescription = null;
            //preferredResponse.Method = null;
            //preferredResponse.MethodOther = null;
            //preferredResponse.UseEmbeddedFileIndicator = null;
            //preferredResponse.VersionIdentifier = null;
            return preferredResponse;
        }
        private Riders CreateRiders()
        {
            Riders riders = new Riders();
            //riders.Id = null;
            //riders.AdjustableRateRiderIndicator = null;
            //riders.BalloonRiderIndicator = null;
            //riders.BiweeklyPaymentRiderIndicator = null;
            //riders.CondominiumRiderIndicator = null;
            //riders.GraduatedPaymentRiderIndicator = null;
            //riders.GrowingEquityRiderIndicator = null;
            //riders.NonOwnerOccupancyRiderIndicator = null;
            //riders.OneToFourFamilyRiderIndicator = null;
            //riders.OtherRiderDescription = null;
            //riders.OtherRiderIndicator = null;
            //riders.PlannedUnitDevelopmentRiderIndicator = null;
            //riders.RateImprovementRiderIndicator = null;
            //riders.RehabilitationLoanRiderIndicator = null;
            //riders.SecondHomeRiderIndicator = null;
            //riders.VaRiderIndicator = null;
            return riders;
        }
        private ReturnTo CreateReturnTo()
        {
            ReturnTo returnTo = new ReturnTo();
            //returnTo.Id = null;
            //returnTo.NonPersonEntityIndicator = null;
            //returnTo.City = null;
            //returnTo.Country = null;
            //returnTo.CountryCode = null;
            //returnTo.County = null;
            //returnTo.CountyFipsCode = null;
            //returnTo.ElectronicRoutingAddress = null;
            //returnTo.ElectronicRoutingMethodType = null;
            //returnTo.PostalCode = null;
            //returnTo.State = null;
            //returnTo.StateFipsCode = null;
            //returnTo.StreetAddress = null;
            //returnTo.StreetAddress2 = null;
            //returnTo.TitleDescription = null;
            //returnTo.UnparsedName = null;
            //returnTo.ContactDetail = CreateContactDetail();
            //returnTo.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return returnTo;
        }
        private AssociatedDocument CreateAssociatedDocument()
        {
            AssociatedDocument associatedDocument = new AssociatedDocument();
            //associatedDocument.Id = null;
            //associatedDocument.BookNumber = null;
            //associatedDocument.BookType = null;
            //associatedDocument.BookTypeOtherDescription = null;
            //associatedDocument.CountyOfRecordationName = null;
            //associatedDocument.InstrumentNumber = null;
            //associatedDocument.Number = null;
            //associatedDocument.OfficeOfRecordationName = null;
            //associatedDocument.PageNumber = null;
            //associatedDocument.RecordingDate = null;
            //associatedDocument.RecordingJurisdictionName = null;
            //associatedDocument.StateOfRecordationName = null;
            //associatedDocument.TitleDescription = null;
            //associatedDocument.Type = null;
            //associatedDocument.TypeOtherDescription = null;
            //associatedDocument.VolumeNumber = null;
            return associatedDocument;
        }
        private PreparedBy CreatePreparedBy()
        {
            PreparedBy preparedBy = new PreparedBy();
            //preparedBy.Id = null;
            //preparedBy.NonPersonEntityIndicator = null;
            //preparedBy.City = null;
            //preparedBy.Country = null;
            //preparedBy.CountryCode = null;
            //preparedBy.County = null;
            //preparedBy.CountyFipsCode = null;
            //preparedBy.ElectronicRoutingAddress = null;
            //preparedBy.ElectronicRoutingMethodType = null;
            //preparedBy.PostalCode = null;
            //preparedBy.State = null;
            //preparedBy.StateFipsCode = null;
            //preparedBy.StreetAddress = null;
            //preparedBy.StreetAddress2 = null;
            //preparedBy.TelephoneNumber = null;
            //preparedBy.TitleDescription = null;
            //preparedBy.UnparsedName = null;
            //preparedBy.ContactDetail = CreateContactDetail();
            return preparedBy;
        }
        private Notary CreateNotary()
        {
            Notary notary = new Notary();
            //notary.Id = null;
            //notary.AppearanceDate = null;
            //notary.AppearedBeforeNamesDescription = null;
            //notary.AppearedBeforeTitlesDescription = null;
            //notary.City = null;
            //notary.CommissionBondNumberIdentifier = null;
            //notary.CommissionCity = null;
            //notary.CommissionCounty = null;
            //notary.CommissionExpirationDate = null;
            //notary.CommissionNumberIdentifier = null;
            //notary.CommissionState = null;
            //notary.County = null;
            //notary.PostalCode = null;
            //notary.State = null;
            //notary.StreetAddress = null;
            //notary.StreetAddress2 = null;
            //notary.TitleDescription = null;
            //notary.UnparsedName = null;
            //notary.CertificateList.Add(CreateCertificate()); // TODO: Need to create a loop
            //notary.DataVersionList.Add(CreateDataVersion()); // TODO: Need to create a loop
            return notary;
        }
        private DataVersion CreateDataVersion()
        {
            DataVersion dataVersion = new DataVersion();
            //dataVersion.Id = null;
            //dataVersion.Name = null;
            //dataVersion.Number = null;
            return dataVersion;
        }
        private Certificate CreateCertificate()
        {
            Certificate certificate = new Certificate();
            //certificate.Id = null;
            //certificate.SignerCompanyName = null;
            //certificate.SignerFirstName = null;
            //certificate.SignerLastName = null;
            //certificate.SignerMiddleName = null;
            //certificate.SignerNameSuffix = null;
            //certificate.SignerTitleDescription = null;
            //certificate.SignerUnparsedName = null;
            //certificate.SigningCounty = null;
            //certificate.SigningDate = null;
            //certificate.SigningState = null;
            //certificate.SignerIdentification = CreateSignerIdentification();
            return certificate;
        }
        private SignerIdentification CreateSignerIdentification()
        {
            SignerIdentification signerIdentification = new SignerIdentification();
            //signerIdentification.Id = null;
            //signerIdentification.Description = null;
            //signerIdentification.Type = null;
            return signerIdentification;
        }
        private Default CreateDefault()
        {
            Default _default = new Default();
            //_default.Id = null;
            //_default.AcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator = null;
            //_default.ApplicationFeesAmount = null;
            //_default.ClosingPreparationFeesAmount = null;
            //_default.LendingInstitutionPostOfficeBoxAddress = null;
            return _default;
        }
        private AssignTo CreateAssignTo()
        {
            AssignTo assignTo = new AssignTo();
            //assignTo.Id = null;
            //assignTo.NonPersonEntityIndicator = null;
            //assignTo.City = null;
            //assignTo.Country = null;
            //assignTo.County = null;
            //assignTo.CountyFipsCode = null;
            //assignTo.PostalCode = null;
            //assignTo.State = null;
            //assignTo.StreetAddress = null;
            //assignTo.StreetAddress2 = null;
            //assignTo.UnparsedName = null;
            //assignTo.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return assignTo;
        }
        private AssignFrom CreateAssignFrom()
        {
            AssignFrom assignFrom = new AssignFrom();
            //assignFrom.Id = null;
            //assignFrom.NonPersonEntityIndicator = null;
            //assignFrom.City = null;
            //assignFrom.Country = null;
            //assignFrom.County = null;
            //assignFrom.CountyFipsCode = null;
            //assignFrom.PostalCode = null;
            //assignFrom.SigningOfficialName = null;
            //assignFrom.SigningOfficialTitleDescription = null;
            //assignFrom.State = null;
            //assignFrom.StreetAddress = null;
            //assignFrom.StreetAddress2 = null;
            //assignFrom.UnparsedName = null;
            //assignFrom.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return assignFrom;
        }
        private Payoff CreatePayoff()
        {
            Payoff payoff = new Payoff();
            //payoff.Id = null;
            //payoff.AccountNumberIdentifier = null;
            //payoff.Amount = null;
            //payoff.PerDiemAmount = null;
            //payoff.SequenceIdentifier = null;
            //payoff.SpecifiedHudLineNumber = null;
            //payoff.ThroughDate = null;
            //payoff.Payee = CreatePayee();
            return payoff;
        }
        private Payee CreatePayee()
        {
            Payee payee = new Payee();
            //payee.Id = null;
            //payee.NonPersonEntityIndicator = null;
            //payee.City = null;
            //payee.Country = null;
            //payee.County = null;
            //payee.PostalCode = null;
            //payee.State = null;
            //payee.StreetAddress = null;
            //payee.StreetAddress2 = null;
            //payee.UnparsedName = null;
            //payee.ContactDetail = CreateContactDetail();
            //payee.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return payee;
        }
        private PaymentDetails CreatePaymentDetails()
        {
            PaymentDetails paymentDetails = new PaymentDetails();
            //paymentDetails.AmortizationScheduleList.Add(CreateAmortizationSchedule()); // TODO: Need to create a loop
            //paymentDetails.PaymentScheduleList.Add(CreatePaymentSchedule()); // TODO: Need to create a loop
            return paymentDetails;
        }
        private PaymentSchedule CreatePaymentSchedule()
        {
            PaymentSchedule paymentSchedule = new PaymentSchedule();
            //paymentSchedule.Id = null;
            //paymentSchedule.PaymentAmount = null;
            //paymentSchedule.PaymentSequenceIdentifier = null;
            //paymentSchedule.PaymentStartDate = null;
            //paymentSchedule.PaymentVaryingToAmount = null;
            //paymentSchedule.TotalNumberOfPaymentsCount = null;
            return paymentSchedule;
        }
        private AmortizationSchedule CreateAmortizationSchedule()
        {
            AmortizationSchedule amortizationSchedule = new AmortizationSchedule();
            //amortizationSchedule.Id = null;
            //amortizationSchedule.EndingBalanceAmount = null;
            //amortizationSchedule.InterestRatePercent = null;
            //amortizationSchedule.LoanToValuePercent = null;
            //amortizationSchedule.MiPaymentAmount = null;
            //amortizationSchedule.PaymentAmount = null;
            //amortizationSchedule.PaymentDueDate = null;
            //amortizationSchedule.PaymentNumber = null;
            //amortizationSchedule.PortionOfPaymentDistributedToInterestAmount = null;
            //amortizationSchedule.PortionOfPaymentDistributedToPrincipalAmount = null;
            return amortizationSchedule;
        }
        private MortgageBroker CreateMortgageBroker()
        {
            MortgageBroker mortgageBroker = new MortgageBroker();
            //mortgageBroker.Id = null;
            //mortgageBroker.NonPersonEntityIndicator = null;
            //mortgageBroker.City = null;
            //mortgageBroker.Country = null;
            //mortgageBroker.County = null;
            //mortgageBroker.LicenseNumberIdentifier = null;
            //mortgageBroker.PostalCode = null;
            //mortgageBroker.State = null;
            //mortgageBroker.StreetAddress = null;
            //mortgageBroker.StreetAddress2 = null;
            //mortgageBroker.UnparsedName = null;
            //mortgageBroker.ContactDetail = CreateContactDetail();
            //mortgageBroker.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            //mortgageBroker.RegulatoryAgencyList.Add(CreateRegulatoryAgency()); // TODO: Need to create a loop
            return mortgageBroker;
        }
        private RegulatoryAgency CreateRegulatoryAgency()
        {
            RegulatoryAgency regulatoryAgency = new RegulatoryAgency();
            //regulatoryAgency.Id = null;
            //regulatoryAgency.City = null;
            //regulatoryAgency.Country = null;
            //regulatoryAgency.County = null;
            //regulatoryAgency.PostalCode = null;
            //regulatoryAgency.State = null;
            //regulatoryAgency.StreetAddress = null;
            //regulatoryAgency.StreetAddress2 = null;
            //regulatoryAgency.Type = null;
            //regulatoryAgency.TypeOtherDescription = null;
            //regulatoryAgency.UnparsedName = null;
            //regulatoryAgency.ContactDetail = CreateContactDetail();
            return regulatoryAgency;
        }
        private LossPayee CreateLossPayee()
        {
            LossPayee lossPayee = new LossPayee();
            //lossPayee.Id = null;
            //lossPayee.NonPersonEntityIndicator = null;
            //lossPayee.City = null;
            //lossPayee.Country = null;
            //lossPayee.County = null;
            //lossPayee.PostalCode = null;
            //lossPayee.State = null;
            //lossPayee.StreetAddress = null;
            //lossPayee.StreetAddress2 = null;
            //lossPayee.Type = null;
            //lossPayee.TypeOtherDescription = null;
            //lossPayee.UnparsedName = null;
            //lossPayee.ContactDetail = CreateContactDetail();
            //lossPayee.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return lossPayee;
        }
        private LoanDetails CreateLoanDetails()
        {
            LoanDetails loanDetails = new LoanDetails();
            //loanDetails.Id = null;
            //loanDetails.ClosingDate = null;
            //loanDetails.DisbursementDate = null;
            //loanDetails.DocumentOrderClassificationType = null;
            //loanDetails.DocumentPreparationDate = null;
            //loanDetails.FundByDate = null;
            //loanDetails.LockExpirationDate = null;
            //loanDetails.OriginalLtvRatioPercent = null;
            //loanDetails.RescissionDate = null;
            //loanDetails.InterimInterest = CreateInterimInterest();
            //loanDetails.RequestToRescind = CreateRequestToRescind();
            return loanDetails;
        }
        private RequestToRescind CreateRequestToRescind()
        {
            RequestToRescind requestToRescind = new RequestToRescind();
            //requestToRescind.Id = null;
            //requestToRescind.NotificationCity = null;
            //requestToRescind.NotificationCountry = null;
            //requestToRescind.NotificationPostalCode = null;
            //requestToRescind.NotificationState = null;
            //requestToRescind.NotificationStreetAddress = null;
            //requestToRescind.NotificationStreetAddress2 = null;
            //requestToRescind.NotificationUnparsedName = null;
            //requestToRescind.TransactionDate = null;
            return requestToRescind;
        }
        private InterimInterest CreateInterimInterest()
        {
            InterimInterest interimInterest = new InterimInterest();
            //interimInterest.Id = null;
            //interimInterest.PaidFromDate = null;
            //interimInterest.PaidNumberOfDays = null;
            //interimInterest.PaidThroughDate = null;
            //interimInterest.PerDiemCalculationMethodType = null;
            //interimInterest.PerDiemPaymentOptionType = null;
            //interimInterest.PerDiemPaymentOptionTypeOtherDescription = null;
            //interimInterest.SinglePerDiemAmount = null;
            //interimInterest.TotalPerDiemAmount = null;
            return interimInterest;
        }
        private LenderBranch CreateLenderBranch()
        {
            LenderBranch lenderBranch = new LenderBranch();
            //lenderBranch.Id = null;
            //lenderBranch.City = null;
            //lenderBranch.Country = null;
            //lenderBranch.County = null;
            //lenderBranch.PostalCode = null;
            //lenderBranch.State = null;
            //lenderBranch.StreetAddress = null;
            //lenderBranch.StreetAddress2 = null;
            //lenderBranch.UnparsedName = null;
            //lenderBranch.ContactDetail = CreateContactDetail();
            return lenderBranch;
        }
        private Lender CreateLender()
        {
            Lender lender = new Lender();
            //lender.Id = null;
            //lender.NonPersonEntityIndicator = null;
            //lender.City = null;
            //lender.Country = null;
            //lender.County = null;
            //lender.DocumentsOrderedByName = null;
            //lender.FunderName = null;
            //lender.PostalCode = null;
            //lender.State = null;
            //lender.StreetAddress = null;
            //lender.StreetAddress2 = null;
            //lender.UnparsedName = null;
            //lender.ContactDetail = CreateContactDetail();
            //lender.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            //lender.RegulatoryAgencyList.Add(CreateRegulatoryAgency()); // TODO: Need to create a loop
            return lender;
        }
        private Investor CreateInvestor()
        {
            Investor investor = new Investor();
            //investor.Id = null;
            //investor.NonPersonEntityIndicator = null;
            //investor.City = null;
            //investor.Country = null;
            //investor.County = null;
            //investor.PostalCode = null;
            //investor.State = null;
            //investor.StreetAddress = null;
            //investor.StreetAddress2 = null;
            //investor.UnparsedName = null;
            //investor.ContactDetail = CreateContactDetail();
            //investor.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            //investor.RegulatoryAgencyList.Add(CreateRegulatoryAgency()); // TODO: Need to create a loop
            return investor;
        }
        private Execution CreateExecution()
        {
            Execution execution = new Execution();
            //execution.Id = null;
            //execution.City = null;
            //execution.County = null;
            //execution.Date = null;
            //execution.State = null;
            return execution;
        }
        private EscrowAccountDetail CreateEscrowAccountDetail()
        {
            EscrowAccountDetail escrowAccountDetail = new EscrowAccountDetail();
            //escrowAccountDetail.Id = null;
            //escrowAccountDetail.TotalMonthlyPitiAmount = null;
            //escrowAccountDetail.InitialBalanceAmount = null;
            //escrowAccountDetail.MinimumBalanceAmount = null;
            //escrowAccountDetail.EscrowAccountActivityList.Add(CreateEscrowAccountActivity()); // TODO: Need to create a loop
            return escrowAccountDetail;
        }
        private EscrowAccountActivity CreateEscrowAccountActivity()
        {
            EscrowAccountActivity escrowAccountActivity = new EscrowAccountActivity();
            //escrowAccountActivity.Id = null;
            //escrowAccountActivity.CurrentBalanceAmount = null;
            //escrowAccountActivity.DisbursementMonth = null;
            //escrowAccountActivity.DisbursementSequenceIdentifier = null;
            //escrowAccountActivity.DisbursementYear = null;
            //escrowAccountActivity.PaymentDescriptionType = null;
            //escrowAccountActivity.PaymentDescriptionTypeOtherDescription = null;
            //escrowAccountActivity.PaymentFromEscrowAccountAmount = null;
            //escrowAccountActivity.PaymentToEscrowAccountAmount = null;
            return escrowAccountActivity;
        }
        private Cosigner CreateCosigner()
        {
            Cosigner cosigner = new Cosigner();
            //cosigner.Id = null;
            //cosigner.TitleDescription = null;
            //cosigner.UnparsedName = null;
            return cosigner;
        }
        private Compensation CreateCompensation()
        {
            Compensation compensation = new Compensation();
            //compensation.Id = null;
            //compensation.Amount = null;
            //compensation.PaidByType = null;
            //compensation.PaidToType = null;
            //compensation.Percent = null;
            //compensation.Type = null;
            //compensation.TypeOtherDescription = null;
            return compensation;
        }
        private ClosingInstructions CreateClosingInstructions()
        {
            ClosingInstructions closingInstructions = new ClosingInstructions();
            #region Conditions
            Guid brokerId = ((AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal).BrokerId;
            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerId);
            if (brokerDB.IsUseNewTaskSystem)
            {
                List<Task> conditions = Task.GetActiveConditionsByLoanId(brokerDB.BrokerID, m_dataLoan.sLId, false);
                foreach (Task condition in conditions)
                {
                    if (condition.TaskStatus == E_TaskStatus.Closed)
                    {
                        continue;
                    }
                    closingInstructions.ConditionList.Add(CreateCondition(condition.TaskSubject));
                }
            }
            else {
                if (brokerDB.HasLenderDefaultFeatures)
                {
                    LoanConditionSetObsolete ldSet = new LoanConditionSetObsolete();
                    ldSet.Retrieve(m_dataLoan.sLId, false, false, false);

                    foreach (CLoanConditionObsolete condition in ldSet)
                    {
                        if (condition.CondStatus == E_CondStatus.Done)
                            continue; // Skip complete condition.

                        closingInstructions.ConditionList.Add(CreateCondition(condition.CondDesc));
                    }
                }
                else
                {
                    int count = m_dataLoan.GetCondRecordCount();
                    for (int i = 0; i < count; i++)
                    {
                        CCondFieldsObsolete condition = m_dataLoan.GetCondFieldsObsolete(i);
                        if (condition.IsDone)
                            continue;
                        closingInstructions.ConditionList.Add(CreateCondition(condition.CondDesc));
                    }
                }
            }
            #endregion

            //closingInstructions.Id = null;
            //closingInstructions.FundingCutoffTime = null;
            //closingInstructions.HoursDocumentsNeededPriorToDisbursementCount = null;
            //closingInstructions.LeadBasedPaintCertificationRequiredIndicator = null;
            //closingInstructions.PreliminaryTitleReportDate = null;
            //closingInstructions.SpecialFloodHazardAreaIndicator = null;
            //closingInstructions.SpecialFloodHazardAreaIndictor = null;
            //closingInstructions.TitleReportItemsDescription = null;
            //closingInstructions.TitleReportRequiredEndorsementsDescription = null;
            //closingInstructions.ConsolidatedClosingConditionsDescription = null;
            //closingInstructions.PropertyTaxMessageDescription = null;
            //closingInstructions.TermiteReportRequiredIndicator = null;
            //closingInstructions.ConditionList.Add(CreateCondition()); // TODO: Need to create a loop
            return closingInstructions;
        }
        private Condition CreateCondition(string desc)
        {
            Condition condition = new Condition();
            condition.Description = desc;

            //condition.Id = null;
            //condition.MetIndicator = null;
            //condition.SatisfactionTimeframeType = null;
            //condition.SatisfactionTimeframeTypeOtherDescription = null;
            //condition.SatisfactionApprovedByName = null;
            //condition.SatisfactionDate = null;
            //condition.SatisfactionResponsiblePartyType = null;
            //condition.SatisfactionResponsiblePartyTypeOtherDescription = null;
            //condition.SequenceIdentifier = null;
            //condition.WaivedIndicator = null;
            return condition;
        }
        private ClosingAgent CreateClosingAgent()
        {
            ClosingAgent closingAgent = new ClosingAgent();
            //closingAgent.Id = null;
            //closingAgent.AuthorizedToSignIndicator = null;
            //closingAgent.NonPersonEntityIndicator = null;
            //closingAgent.City = null;
            //closingAgent.Country = null;
            //closingAgent.County = null;
            //closingAgent.Identifier = null;
            //closingAgent.OrderNumberIdentifier = null;
            //closingAgent.PostalCode = null;
            //closingAgent.State = null;
            //closingAgent.StreetAddress = null;
            //closingAgent.StreetAddress2 = null;
            //closingAgent.TitleDescription = null;
            //closingAgent.Type = null;
            //closingAgent.TypeOtherDescription = null;
            //closingAgent.UnparsedName = null;
            //closingAgent.ContactDetail = CreateContactDetail();
            //closingAgent.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return closingAgent;
        }
        private AllongeToNote CreateAllongeToNote()
        {
            AllongeToNote allongeToNote = new AllongeToNote();
            //allongeToNote.Id = null;
            //allongeToNote.Date = null;
            //allongeToNote.ExecutedByDescription = null;
            //allongeToNote.InFavorOfDescription = null;
            //allongeToNote.PayToTheOrderOfDescription = null;
            //allongeToNote.Type = null;
            //allongeToNote.TypeOtherDescription = null;
            //allongeToNote.WithoutRecourseDescription = null;
            //allongeToNote.AuthorizedRepresentative = CreateAuthorizedRepresentative();
            return allongeToNote;
        }
        private Application CreateApplication()
        {
            Application application = new Application();
            int nApps = m_dataLoan.nApps;
            int iliaCount = 0;
            for (int appIndex = 0; appIndex < nApps; appIndex++)
            {
                CAppData dataApp = m_dataLoan.GetAppData(appIndex);
                if (!dataApp.aBIsValidNameSsn && !dataApp.aCIsValidNameSsn)
                {
                    continue; // 7/29/2009 dd - Skip if there is no borrower & no coborrower
                }

                #region Add Assets
                //application.AssetList.Add(CreateAsset()); // TODO: Need to create a loop

                #endregion

                #region Add Liabilities
                foreach (var item in dataApp.aLiaCollection.GetSubcollection(true, E_DebtGroupT.JobRelatedExpense))
                {
                    var liaJobExpense = (ILiabilityJobExpense)item;
                    iliaCount++;
                    application.LiabilityList.Add(CreateLiability(liaJobExpense, appIndex, iliaCount));
                }

                foreach (var item in dataApp.aLiaCollection.GetSubcollection(true, E_DebtGroupT.Alimony))
                {
                    var liaAlimony = (ILiabilityAlimony)item;
                    iliaCount++;
                    application.LiabilityList.Add(CreateLiability(liaAlimony, appIndex, iliaCount));
                }

                foreach (var item in dataApp.aLiaCollection.GetSubcollection(true, E_DebtGroupT.ChildSupport))
                {
                    var liaChildSupport = (ILiabilityChildSupport)item;
                    iliaCount++;
                    application.LiabilityList.Add(CreateLiability(liaChildSupport, appIndex, iliaCount));
                }

                foreach (var item in dataApp.aLiaCollection.GetSubcollection(true, E_DebtGroupT.Regular))
                {
                    var liaRegular = (ILiabilityRegular)item;
                    iliaCount++;
                    application.LiabilityList.Add(CreateLiability(liaRegular, appIndex, iliaCount));
                }
                #endregion

                #region Add REO Properties
                //application.ReoPropertyList.Add(CreateReoProperty()); // TODO: Need to create a loop

                #endregion 

                if (dataApp.aBIsValidNameSsn)
                {
                    dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
                    application.BorrowerList.Add(CreateBorrower(dataApp, appIndex));
                }
                if (dataApp.aCIsValidNameSsn)
                {
                    dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
                    application.BorrowerList.Add(CreateBorrower(dataApp, appIndex));
                }
            }

            application.LoanPurpose = CreateLoanPurpose();
            //application.Id = null;
            //application.DataInformation = CreateDataInformation();
            application.AdditionalCaseData = CreateAdditionalCaseData();
            //application.AffordableLending = CreateAffordableLending();
            if (m_dataLoan.sLPurposeT == E_sLPurposeT.Purchase)
            {
                application.DownPaymentList.Add(CreateDownPayment());
            }
            
            //application.EscrowList.Add(CreateEscrow()); // TODO: Need to create a loop
            //application.EscrowAccountSummary = CreateEscrowAccountSummary();
            if ((m_dataLoan.sLT == E_sLT.FHA) || (m_dataLoan.sLT == E_sLT.VA))
            {
                application.GovernmentLoan = CreateGovernmentLoan();
            }
            
            //application.GovernmentReporting = CreateGovernmentReporting();
            IPreparerFields preparerURLA = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (!preparerURLA.IsEmpty)
            {
                application.InterviewerInformation = CreateInterviewerInformation(preparerURLA);
            }

            application.LoanProductData = CreateLoanProductData();
            application.LoanQualification = CreateLoanQualification();
            //application.Mers = CreateMers();
            application.MiData = CreateMiData();

            application.MortgageTerms = CreateMortgageTerms();
            application.Property = CreateProperty();
            #region Proposed Housing Expense List
            if (m_dataLoan.sProFirstMPmt > 0)
            {
                application.ProposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest));
            }

            if (m_dataLoan.sProSecondMPmt > 0)
            {
                application.ProposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest));
            }

            if (m_dataLoan.sProHazIns > 0)
            {
                application.ProposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.HazardInsurance));
            }

            if (m_dataLoan.sProRealETx > 0)
            {
                application.ProposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.RealEstateTax));
            }

            if (m_dataLoan.sProMIns > 0)
            {
                application.ProposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.MI));
            }

            if (m_dataLoan.sProHoAssocDues > 0)
            {
                application.ProposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees));
            }

            if (m_dataLoan.sProOHExp > 0)
            {
                application.ProposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.OtherHousingExpense));
            }
            #endregion

            //application.RespaFeeList.Add(CreateRespaFee()); // TODO: Need to create a loop
            //application.TitleHolderList.Add(CreateTitleHolder()); // TODO: Need to create a loop
            application.TransactionDetail = CreateTransactionDetail();
            //application.InvestorFeatureList.Add(CreateInvestorFeature()); // TODO: Need to create a loop
            //application.LoanOriginationSystem = CreateLoanOriginationSystem();
            application.LoanUnderwritingList.Add(CreateLoanUnderwriting());
            //application.RelatedLoanList.Add(CreateRelatedLoan()); // TODO: Need to create a loop
            //application.UrlaTotalList.Add(CreateUrlaTotal()); // TODO: Need to create a loop
            return application;
        }
        private UrlaTotal CreateUrlaTotal()
        {
            UrlaTotal urlaTotal = new UrlaTotal();
            //urlaTotal.Id = null;
            //urlaTotal.BorrowerId = null;
            //urlaTotal.UrlaSubtotalLiquidAssetsAmount = null;
            //urlaTotal.AssetsAmount = null;
            //urlaTotal.BaseIncomeAmount = null;
            //urlaTotal.BonusIncomeAmount = null;
            //urlaTotal.CashFromToBorrowerAmount = null;
            //urlaTotal.CombinedPresentHousingExpenseAmount = null;
            //urlaTotal.CombinedProposedHousingExpenseAmount = null;
            //urlaTotal.CommissionsIncomeAmount = null;
            //urlaTotal.DividendsInterestIncomeAmount = null;
            //urlaTotal.LiabilityMonthlyPaymentsAmount = null;
            //urlaTotal.LiabilityUnpaidBalanceAmount = null;
            //urlaTotal.LotAndImprovementsAmount = null;
            //urlaTotal.MonthlyIncomeAmount = null;
            //urlaTotal.NetRentalIncomeAmount = null;
            //urlaTotal.NetWorthAmount = null;
            //urlaTotal.OtherTypesOfIncomeAmount = null;
            //urlaTotal.OvertimeIncomeAmount = null;
            //urlaTotal.ReoLienInstallmentAmount = null;
            //urlaTotal.ReoLienUpbAmount = null;
            //urlaTotal.ReoMaintenanceExpenseAmount = null;
            //urlaTotal.ReoMarketValueAmount = null;
            //urlaTotal.ReoRentalIncomeGrossAmount = null;
            //urlaTotal.ReoRentalIncomeNetAmount = null;
            //urlaTotal.TransactionCostAmount = null;
            //urlaTotal.HousingExpenseList.Add(CreateHousingExpense()); // TODO: Need to create a loop
            return urlaTotal;
        }
        private HousingExpense CreateHousingExpense()
        {
            HousingExpense housingExpense = new HousingExpense();
            //housingExpense.Id = null;
            //housingExpense.HousingExpenseType = null;
            //housingExpense.HousingExpenseTypeOtherDescription = null;
            //housingExpense.PaymentAmount = null;
            return housingExpense;
        }
        private RelatedLoan CreateRelatedLoan()
        {
            RelatedLoan relatedLoan = new RelatedLoan();
            //relatedLoan.RelatedLoanId = null;
            //relatedLoan.BalloonIndicator = null;
            //relatedLoan.HelocMaximumBalanceAmount = null;
            //relatedLoan.InvestorReoPropertyIdentifier = null;
            //relatedLoan.LienHolderCity = null;
            //relatedLoan.LienHolderCountry = null;
            //relatedLoan.LienHolderPostalCode = null;
            //relatedLoan.LienHolderSameAsSubjectLoanIndicator = null;
            //relatedLoan.LienHolderState = null;
            //relatedLoan.LienHolderStreetAddress = null;
            //relatedLoan.LienHolderStreetAddress2 = null;
            //relatedLoan.LienHolderType = null;
            //relatedLoan.LienHolderTypeOtherDescription = null;
            //relatedLoan.LienHolderUnparsedName = null;
            //relatedLoan.LienPriorityType = null;
            //relatedLoan.LienPriorityTypeOtherDescription = null;
            //relatedLoan.LoanAllInPricePercent = null;
            //relatedLoan.LoanAmortizationType = null;
            //relatedLoan.LoanOriginalMaturityTermMonths = null;
            //relatedLoan.MortgageType = null;
            //relatedLoan.NegativeAmortizationType = null;
            //relatedLoan.NoteDate = null;
            //relatedLoan.OriginalLoanAmount = null;
            //relatedLoan.OtherAmortizationTypeDescription = null;
            //relatedLoan.OtherMortgageTypeDescription = null;
            //relatedLoan.RelatedInvestorLoanIdentifier = null;
            //relatedLoan.RelatedLoanFinancingSourceType = null;
            //relatedLoan.RelatedLoanFinancingSourceTypeOtherDescription = null;
            //relatedLoan.RelatedLoanInvestorType = null;
            //relatedLoan.RelatedLoanInvestorTypeOtherDescription = null;
            //relatedLoan.RelatedLoanRelationshipType = null;
            //relatedLoan.RelatedLoanRelationshipTypeOtherDescription = null;
            //relatedLoan.RelatedLoanUpbAmount = null;
            //relatedLoan.ScheduledFirstPaymentDate = null;
            return relatedLoan;
        }
        private LoanUnderwriting CreateLoanUnderwriting()
        {
            LoanUnderwriting loanUnderwriting = new LoanUnderwriting();
            //loanUnderwriting.LoanUnderwritingId = null;
            //loanUnderwriting.AgencyProgramDescription = null;
            //loanUnderwriting.AutomatedUnderwritingEvaluationStatusDescription = null;
            //loanUnderwriting.AutomatedUnderwritingProcessDescription = null;
            //loanUnderwriting.AutomatedUnderwritingRecommendationDescription = null;
            //loanUnderwriting.AutomatedUnderwritingSystemName = null;
            //loanUnderwriting.AutomatedUnderwritingSystemResultValue = null;
            //loanUnderwriting.ContractUnderwritingIndicator = null;
            loanUnderwriting.HousingExpenseRatioPercent = m_dataLoan.sQualTopR_rep;
            //loanUnderwriting.LoanManualUnderwritingIndicator = null;
            //loanUnderwriting.LoanProspectorCreditRiskClassificationDescription = null;
            //loanUnderwriting.LoanProspectorDocumentationClassificationDescription = null;
            //loanUnderwriting.LoanProspectorRiskGradeAssignedDescription = null;
            loanUnderwriting.TotalDebtExpenseRatioPercent = m_dataLoan.sQualBottomR_rep;
            //loanUnderwriting.CaseIdentifier = null;
            //loanUnderwriting.DecisionDatetime = null;
            //loanUnderwriting.InvestorGuidelinesIndicator = null;
            //loanUnderwriting.MethodVersionIdentifier = null;
            //loanUnderwriting.OrganizationName = null;
            //loanUnderwriting.SubmitterType = null;
            //loanUnderwriting.SubmitterTypeOtherDescription = null;
            return loanUnderwriting;
        }
        private LoanOriginationSystem CreateLoanOriginationSystem()
        {
            LoanOriginationSystem loanOriginationSystem = new LoanOriginationSystem();
            //loanOriginationSystem.Id = null;
            //loanOriginationSystem.LoanIdentifier = null;
            //loanOriginationSystem.Name = null;
            //loanOriginationSystem.VendorIdentifier = null;
            //loanOriginationSystem.VersionIdentifier = null;
            return loanOriginationSystem;
        }
        private InvestorFeature CreateInvestorFeature()
        {
            InvestorFeature investorFeature = new InvestorFeature();
            //investorFeature.Id = null;
            //investorFeature.CategoryName = null;
            //investorFeature.Description = null;
            //investorFeature.Identifier = null;
            //investorFeature.Name = null;
            return investorFeature;
        }
        private E_BorrowerMaritalStatusType ToMismo(E_aBMaritalStatT aBMaritalStatT)
        {
            switch (aBMaritalStatT)
            {
                case E_aBMaritalStatT.Married: return E_BorrowerMaritalStatusType.Married;
                case E_aBMaritalStatT.NotMarried: return E_BorrowerMaritalStatusType.Unmarried;
                case E_aBMaritalStatT.Separated: return E_BorrowerMaritalStatusType.Separated;
                case E_aBMaritalStatT.LeaveBlank: return E_BorrowerMaritalStatusType.NotProvided;
                default:
                    return E_BorrowerMaritalStatusType.Undefined;
            }
        }
        private E_BorrowerPrintPositionType ToMismo(E_BorrowerModeT BorrowerModeT)
        {
            switch (BorrowerModeT)
            {
                case E_BorrowerModeT.Borrower: return E_BorrowerPrintPositionType.Borrower;
                case E_BorrowerModeT.Coborrower: return E_BorrowerPrintPositionType.CoBorrower;
                default:
                    return E_BorrowerPrintPositionType.Undefined;
            }
        }
        private Borrower CreateBorrower(CAppData dataApp, int iAppIndex)
        {
            Borrower borrower = new Borrower();
            bool bIsBorrower = dataApp.BorrowerModeT == E_BorrowerModeT.Borrower;
            borrower.BorrowerId = EncompassResponseHelper.MismoBorrowerID(iAppIndex, bIsBorrower);
            borrower.JointAssetBorrowerId = EncompassResponseHelper.MismoBorrowerID(iAppIndex, !bIsBorrower);
            borrower.DependentCount = dataApp.aDependNum_rep;
            borrower.JointAssetLiabilityReportingType = dataApp.aAsstLiaCompletedNotJointly ? E_BorrowerJointAssetLiabilityReportingType.NotJointly : E_BorrowerJointAssetLiabilityReportingType.Jointly;
            borrower.MaritalStatusType = ToMismo(dataApp.aMaritalStatT);
            borrower.SchoolingYears = dataApp.aSchoolYrs_rep;
            borrower.AgeAtApplicationYears = dataApp.aAge_rep;
            borrower.BirthDate = dataApp.aDob_rep;
            borrower.FirstName = dataApp.aFirstNm;
            borrower.HomeTelephoneNumber = dataApp.aHPhone;
            borrower.LastName = dataApp.aLastNm;
            borrower.MiddleName = dataApp.aMidNm;
            borrower.NameSuffix = dataApp.aSuffix;
            borrower.PrintPositionType = ToMismo(dataApp.BorrowerModeT);
            borrower.Ssn = dataApp.aSsn;
            borrower.UnparsedName = dataApp.aNm;

            //borrower.BorrowerIsCosignerIndicator = null;
            //borrower.BorrowerNonObligatedIndicator = null;
            //borrower.BorrowerNonTitleSpouseIndicator = null;
            //borrower.CreditReportIdentifier = null;
            //borrower.NonPersonEntityIndicator = null;
            //borrower.UrlaBorrowerTotalMonthlyIncomeAmount = null;
            //borrower.UrlaBorrowerTotalOtherIncomeAmount = null;
            //borrower.ApplicationSignedDate = null;
            //borrower.RelationshipTitleType = null;
            //borrower.RelationshipTitleTypeOtherDescription = null;
            //borrower.SequenceIdentifier = null;
            //borrower.AliasList.Add(CreateAlias()); // TODO: Need to create a loop
            //borrower.MailTo = CreateMailTo();
            //borrower.ResidenceList.Add(CreateResidence()); // TODO: Need to create a loop

            borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.Base, null, dataApp.aBaseI_rep));
            borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.Overtime, null, dataApp.aOvertimeI_rep));
            borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.Bonus, null, dataApp.aBonusesI_rep));
            borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.Commissions, null, dataApp.aCommisionI_rep));
            borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.DividendsInterest, null, dataApp.aDividendI_rep));
            borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.NetRentalIncome, null, dataApp.aBNetRentI1003_rep));

            foreach (var otherIncome in dataApp.aOtherIncomeList)
            {
                if (otherIncome.IsForCoBorrower == (dataApp.BorrowerModeT == E_BorrowerModeT.Coborrower))
                {
                    var incomeType = IncomeType(otherIncome.Desc);
                    var currentIncome = CreateCurrentIncome(incomeType, otherIncome.Desc, dataApp.m_convertLos.ToMoneyString(otherIncome.Amount, FormatDirection.ToRep));
                    borrower.CurrentIncomeList.Add(currentIncome);
                }
            }

            
            //borrower.Declaration = CreateDeclaration();
            //borrower.DependentList.Add(CreateDependent()); // TODO: Need to create a loop

            IEmpCollection empCollection = dataApp.aEmpCollection;
            IPrimaryEmploymentRecord empPrimaryRecord = empCollection.GetPrimaryEmp(false);
            if (empPrimaryRecord != null)
            {
                borrower.EmployerList.Add(CreateEmployer(empPrimaryRecord));
            }

            foreach (IRegularEmploymentRecord empRegRecord in empCollection.GetSubcollection(true, E_EmpGroupT.Regular))
            {
                borrower.EmployerList.Add(CreateEmployer(empRegRecord));
            }         
            
            //borrower.FhaVaBorrower = CreateFhaVaBorrower();
            //borrower.GovernmentMonitoring = CreateGovernmentMonitoring();
            //borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense()); // TODO: Need to create a loop
            //borrower.SummaryList.Add(CreateSummary()); // TODO: Need to create a loop
            //borrower.VaBorrower = CreateVaBorrower();
            //borrower.FhaBorrower = CreateFhaBorrower();
            //borrower.NearestLivingRelative = CreateNearestLivingRelative();
            //borrower.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            //borrower.PowerOfAttorney = CreatePowerOfAttorney();
            //borrower.ContactPointList.Add(CreateContactPoint()); // TODO: Need to create a loop
            //borrower.CreditScoreList.Add(CreateCreditScore()); // TODO: Need to create a loop
            return borrower;
        }

        private E_CurrentIncomeIncomeType IncomeType(string description)
        {
            E_CurrentIncomeIncomeType type;
            if (string.IsNullOrEmpty(description))
            {
                type = E_CurrentIncomeIncomeType.OtherTypesOfIncome;
            }
            if (description.Equals("SUBJECT PROPERTY NET CASH FLOW (TWO-TO-FOUR UNIT OWNER-OCCUPIED PROPERTIES)", StringComparison.OrdinalIgnoreCase))
            {
                type = E_CurrentIncomeIncomeType.SubjectPropertyNetCashFlow;
            }
            else
            {
                var descriptionType = OtherIncome.Get_aOIDescT(description);
                switch (descriptionType)
                {
                    case E_aOIDescT.AlimonyChildSupport:
                    case E_aOIDescT.Alimony:
                    case E_aOIDescT.ChildSupport:
                        type = E_CurrentIncomeIncomeType.AlimonyChildSupport;
                        break;
                    case E_aOIDescT.AutomobileExpenseAccount:
                        type = E_CurrentIncomeIncomeType.AutomobileExpenseAccount;
                        break;
                    case E_aOIDescT.FosterCare:
                        type = E_CurrentIncomeIncomeType.FosterCare;
                        break;
                    case E_aOIDescT.MilitaryBasePay:
                        type = E_CurrentIncomeIncomeType.MilitaryBasePay;
                        break;
                    case E_aOIDescT.MilitaryClothesAllowance:
                        type = E_CurrentIncomeIncomeType.MilitaryClothesAllowance;
                        break;
                    case E_aOIDescT.MilitaryCombatPay:
                        type = E_CurrentIncomeIncomeType.MilitaryCombatPay;
                        break;
                    case E_aOIDescT.MilitaryFlightPay:
                        type = E_CurrentIncomeIncomeType.MilitaryFlightPay;
                        break;
                    case E_aOIDescT.MilitaryHazardPay:
                        type = E_CurrentIncomeIncomeType.MilitaryHazardPay;
                        break;
                    case E_aOIDescT.MilitaryOverseasPay:
                        type = E_CurrentIncomeIncomeType.MilitaryOverseasPay;
                        break;
                    case E_aOIDescT.MilitaryPropPay:
                        type = E_CurrentIncomeIncomeType.MilitaryPropPay;
                        break;
                    case E_aOIDescT.MilitaryQuartersAllowance:
                        type = E_CurrentIncomeIncomeType.MilitaryQuartersAllowance;
                        break;
                    case E_aOIDescT.MilitaryRationsAllowance:
                        type = E_CurrentIncomeIncomeType.MilitaryRationsAllowance;
                        break;
                    case E_aOIDescT.MilitaryVariableHousingAllowance:
                        type = E_CurrentIncomeIncomeType.MilitaryVariableHousingAllowance;
                        break;
                    case E_aOIDescT.NotesReceivableInstallment:
                        type = E_CurrentIncomeIncomeType.NotesReceivableInstallment;
                        break;
                    case E_aOIDescT.PensionRetirement:
                        type = E_CurrentIncomeIncomeType.Pension;
                        break;
                    case E_aOIDescT.RealEstateMortgageDifferential:
                        type = E_CurrentIncomeIncomeType.MortgageDifferential;
                        break;
                    case E_aOIDescT.SocialSecurityDisability:
                    case E_aOIDescT.Disability:
                    case E_aOIDescT.SocialSecurity:
                        type = E_CurrentIncomeIncomeType.SocialSecurity;
                        break;
                    case E_aOIDescT.SubjPropNetCashFlow:
                        type = E_CurrentIncomeIncomeType.SubjectPropertyNetCashFlow;
                        break;
                    case E_aOIDescT.Trust:
                        type = E_CurrentIncomeIncomeType.Trust;
                        break;
                    case E_aOIDescT.UnemploymentWelfare:
                        type = E_CurrentIncomeIncomeType.Unemployment;
                        break;
                    case E_aOIDescT.VABenefitsNonEducation:
                        type = E_CurrentIncomeIncomeType.VABenefitsNonEducational;
                        break;
                    case E_aOIDescT.ContractBasis:
                        type = E_CurrentIncomeIncomeType.ContractBasis;
                        break;
                    case E_aOIDescT.PublicAssistance:
                        type = E_CurrentIncomeIncomeType.PublicAssistance;
                        break;
                    case E_aOIDescT.WorkersCompensation:
                        type = E_CurrentIncomeIncomeType.WorkersCompensation;
                        break;
                    case E_aOIDescT.Other:
                    case E_aOIDescT.DefinedContributionPlan:
                    case E_aOIDescT.HousingAllowance:
                    case E_aOIDescT.MiscellaneousIncome:
                    case E_aOIDescT.AccessoryUnitIncome:
                    case E_aOIDescT.BoarderIncome:
                    case E_aOIDescT.CapitalGains:
                    case E_aOIDescT.EmploymentRelatedAssets:
                    case E_aOIDescT.ForeignIncome:
                    case E_aOIDescT.HousingChoiceVoucher:
                    case E_aOIDescT.MortgageCreditCertificate:
                    case E_aOIDescT.RoyaltyPayment:
                    case E_aOIDescT.SeasonalIncome:
                    case E_aOIDescT.NonBorrowerHouseholdIncome:
                    case E_aOIDescT.TemporaryLeave:
                    case E_aOIDescT.TipIncome:
                    case E_aOIDescT.TrailingCoBorrowerIncome:
                        type = E_CurrentIncomeIncomeType.OtherTypesOfIncome;
                        break;
                    default:
                        throw new UnhandledEnumException(descriptionType);
                }
            }

            return type;
        }

        private CreditScore CreateCreditScore()
        {
            CreditScore creditScore = new CreditScore();
            //creditScore.CreditScoreId = null;
            //creditScore.CreditReportIdentifier = null;
            //creditScore.CreditRepositorySourceType = null;
            //creditScore.CreditRepositorySourceTypeOtherDescription = null;
            //creditScore.Date = null;
            //creditScore.ExclusionReasonType = null;
            //creditScore.FactaInquiriesIndicator = null;
            //creditScore.ModelNameType = null;
            //creditScore.ModelNameTypeOtherDescription = null;
            //creditScore.Value = null;
            //creditScore.FactorList.Add(CreateFactor()); // TODO: Need to create a loop
            return creditScore;
        }
        private Factor CreateFactor()
        {
            Factor factor = new Factor();
            //factor.Id = null;
            //factor.Code = null;
            //factor.Text = null;
            return factor;
        }
        private NearestLivingRelative CreateNearestLivingRelative()
        {
            NearestLivingRelative nearestLivingRelative = new NearestLivingRelative();
            //nearestLivingRelative.Id = null;
            //nearestLivingRelative.City = null;
            //nearestLivingRelative.Name = null;
            //nearestLivingRelative.PostalCode = null;
            //nearestLivingRelative.RelationshipDescription = null;
            //nearestLivingRelative.State = null;
            //nearestLivingRelative.StreetAddress = null;
            //nearestLivingRelative.StreetAddress2 = null;
            //nearestLivingRelative.TelephoneNumber = null;
            return nearestLivingRelative;
        }
        private FhaBorrower CreateFhaBorrower()
        {
            FhaBorrower fhaBorrower = new FhaBorrower();
            //fhaBorrower.Id = null;
            //fhaBorrower.CertificationLeadPaintIndicator = null;
            //fhaBorrower.CertificationOriginalMortgageAmount = null;
            //fhaBorrower.CertificationOwn4OrMoreDwellingsIndicator = null;
            //fhaBorrower.CertificationOwnOtherPropertyIndicator = null;
            //fhaBorrower.CertificationPropertySoldCity = null;
            //fhaBorrower.CertificationPropertySoldPostalCode = null;
            //fhaBorrower.CertificationPropertySoldState = null;
            //fhaBorrower.CertificationPropertySoldStreetAddress = null;
            //fhaBorrower.CertificationPropertyToBeSoldIndicator = null;
            //fhaBorrower.CertificationRentalIndicator = null;
            //fhaBorrower.CertificationSalesPriceAmount = null;
            return fhaBorrower;
        }
        private VaBorrower CreateVaBorrower()
        {
            VaBorrower vaBorrower = new VaBorrower();
            //vaBorrower.Id = null;
            //vaBorrower.VaCoBorrowerNonTaxableIncomeAmount = null;
            //vaBorrower.VaCoBorrowerTaxableIncomeAmount = null;
            //vaBorrower.VaFederalTaxAmount = null;
            //vaBorrower.VaLocalTaxAmount = null;
            //vaBorrower.VaPrimaryBorrowerNonTaxableIncomeAmount = null;
            //vaBorrower.VaPrimaryBorrowerTaxableIncomeAmount = null;
            //vaBorrower.VaSocialSecurityTaxAmount = null;
            //vaBorrower.VaStateTaxAmount = null;
            //vaBorrower.CertificationOccupancyType = null;
            return vaBorrower;
        }
        private Summary CreateSummary()
        {
            Summary summary = new Summary();
            //summary.Id = null;
            //summary.Amount = null;
            //summary.AmountType = null;
            //summary.AmountTypeOtherDescription = null;
            return summary;
        }
        private PresentHousingExpense CreatePresentHousingExpense()
        {
            PresentHousingExpense presentHousingExpense = new PresentHousingExpense();
            //presentHousingExpense.Id = null;
            //presentHousingExpense.HousingExpenseType = null;
            //presentHousingExpense.HousingExpenseTypeOtherDescription = null;
            //presentHousingExpense.PaymentAmount = null;
            return presentHousingExpense;
        }
        private GovernmentMonitoring CreateGovernmentMonitoring()
        {
            GovernmentMonitoring governmentMonitoring = new GovernmentMonitoring();
            //governmentMonitoring.Id = null;
            //governmentMonitoring.GenderType = null;
            //governmentMonitoring.HmdaEthnicityType = null;
            //governmentMonitoring.OtherRaceNationalOriginDescription = null;
            //governmentMonitoring.RaceNationalOriginRefusalIndicator = null;
            //governmentMonitoring.RaceNationalOriginType = null;
            //governmentMonitoring.HmdaRaceList.Add(CreateHmdaRace()); // TODO: Need to create a loop
            return governmentMonitoring;
        }
        private HmdaRace CreateHmdaRace()
        {
            HmdaRace hmdaRace = new HmdaRace();
            //hmdaRace.Id = null;
            //hmdaRace.Type = null;
            return hmdaRace;
        }
        private FhaVaBorrower CreateFhaVaBorrower()
        {
            FhaVaBorrower fhaVaBorrower = new FhaVaBorrower();
            //fhaVaBorrower.Id = null;
            //fhaVaBorrower.CaivrsIdentifier = null;
            //fhaVaBorrower.FnmBankruptcyCount = null;
            //fhaVaBorrower.FnmBorrowerCreditRating = null;
            //fhaVaBorrower.FnmCreditReportScoreType = null;
            //fhaVaBorrower.FnmForeclosureCount = null;
            //fhaVaBorrower.VeteranStatusIndicator = null;
            //fhaVaBorrower.CertificationSalesPriceExceedsAppraisedValueType = null;
            return fhaVaBorrower;
        }
        private Employer CreateEmployer(IPrimaryEmploymentRecord empPrimaryRecord)
        {
            Employer employer = new Employer();
            //employer.Id = null;
            //employer.CurrentEmploymentMonthsOnJob = null;
            //employer.CurrentEmploymentStartDate = null;
            employer.CurrentEmploymentTimeInLineOfWorkYears = empPrimaryRecord.ProfLen_rep;
            //employer.CurrentEmploymentYearsOnJob = null;
            //employer.EmployedAbroadIndicator = null;
            if (empPrimaryRecord.IsSelfEmplmt)
            {
                employer.EmploymentBorrowerSelfEmployedIndicator = E_YNIndicator.Y;
            }
            else
            {
                employer.EmploymentBorrowerSelfEmployedIndicator = E_YNIndicator.N;
            }
            //employer.EmploymentCurrentIndicator = null;
            employer.EmploymentPositionDescription = empPrimaryRecord.JobTitle;
            employer.EmploymentPrimaryIndicator = E_YNIndicator.Y;
            //employer.IncomeEmploymentMonthlyAmount = null;
            //employer.PreviousEmploymentEndDate = null;
            //employer.PreviousEmploymentStartDate = null;
            //employer.SpecialBorrowerEmployerRelationshipType = null;
            //employer.SpecialBorrowerEmployerRelationshipTypeOtherDescription = null;
            employer.City = empPrimaryRecord.EmplrCity;
            //employer.Country = null;
            employer.Name = empPrimaryRecord.EmplrNm;
            employer.PostalCode = empPrimaryRecord.EmplrZip;
            employer.State = empPrimaryRecord.EmplrState;
            employer.StreetAddress = empPrimaryRecord.EmplrAddr;
            employer.TelephoneNumber = empPrimaryRecord.EmplrBusPhone;
            return employer;
        }
        private Employer CreateEmployer(IRegularEmploymentRecord empRegularRecord)
        {
            Employer employer = new Employer();
            //employer.Id = null;
            //employer.CurrentEmploymentMonthsOnJob = null;
            //employer.CurrentEmploymentStartDate = null;
            //employer.CurrentEmploymentTimeInLineOfWorkYears = null;
            //employer.CurrentEmploymentYearsOnJob = null;
            //employer.EmployedAbroadIndicator = null;
            if (empRegularRecord.IsSelfEmplmt)
            {
                employer.EmploymentBorrowerSelfEmployedIndicator = E_YNIndicator.Y;
            }
            else
            {
                employer.EmploymentBorrowerSelfEmployedIndicator = E_YNIndicator.N;
            }
            //employer.EmploymentCurrentIndicator = null;
            employer.EmploymentPositionDescription = empRegularRecord.JobTitle;
            employer.EmploymentPrimaryIndicator = E_YNIndicator.N;
            employer.IncomeEmploymentMonthlyAmount = empRegularRecord.MonI_rep;
            employer.PreviousEmploymentEndDate = empRegularRecord.EmplmtEndD_rep;
            employer.PreviousEmploymentStartDate = empRegularRecord.EmplmtStartD_rep;
            //employer.SpecialBorrowerEmployerRelationshipType = null;
            //employer.SpecialBorrowerEmployerRelationshipTypeOtherDescription = null;
            employer.City = empRegularRecord.EmplrCity;
            //employer.Country = null;
            employer.Name = empRegularRecord.EmplrNm;
            employer.PostalCode = empRegularRecord.EmplrZip;
            employer.State = empRegularRecord.EmplrState;
            employer.StreetAddress = empRegularRecord.EmplrAddr;
            employer.TelephoneNumber = empRegularRecord.EmplrBusPhone;
            return employer;
        }
        private Dependent CreateDependent()
        {
            Dependent dependent = new Dependent();
            //dependent.Id = null;
            //dependent.AgeYears = null;
            return dependent;
        }
        private Declaration CreateDeclaration()
        {
            Declaration declaration = new Declaration();
            //declaration.Id = null;
            //declaration.AlimonyChildSupportObligationIndicator = null;
            //declaration.BankruptcyIndicator = null;
            //declaration.BorrowedDownPaymentIndicator = null;
            //declaration.BorrowerFirstTimeHomebuyerIndicator = null;
            //declaration.CitizenshipResidencyType = null;
            //declaration.CoMakerEndorserOfNoteIndicator = null;
            //declaration.HomeownerPastThreeYearsType = null;
            //declaration.IntentToOccupyType = null;
            //declaration.LoanForeclosureOrJudgementIndicator = null;
            //declaration.LoanForeclosureOrJudgmentIndicator = null;
            //declaration.OutstandingJudgementsIndicator = null;
            //declaration.OutstandingJudgmentsIndicator = null;
            //declaration.PartyToLawsuitIndicator = null;
            //declaration.PresentlyDelinquentIndicator = null;
            //declaration.PriorPropertyTitleType = null;
            //declaration.PriorPropertyUsageType = null;
            //declaration.PropertyForeclosedPastSevenYearsIndicator = null;
            //declaration.ExplanationList.Add(CreateExplanation()); // TODO: Need to create a loop
            return declaration;
        }
        private Explanation CreateExplanation()
        {
            Explanation explanation = new Explanation();
            //explanation.Id = null;
            //explanation.Description = null;
            //explanation.Type = null;
            return explanation;
        }
        private CurrentIncome CreateCurrentIncome(E_CurrentIncomeIncomeType eIncomeType, string sIncomeTypeOtherDescription, string sMonthlyTotal)
        {
            CurrentIncome currentIncome = null;
            if (!String.IsNullOrEmpty(sMonthlyTotal))
            {
                if (EncompassResponseHelper.ConvertToDecimal(sMonthlyTotal) > 0.000m)
                {
                    currentIncome = new CurrentIncome();
                    //currentIncome.Id = null;
                    //currentIncome.IncomeFederalTaxExemptIndicator = null;
                    currentIncome.IncomeType = eIncomeType;
                    if (!String.IsNullOrEmpty(sIncomeTypeOtherDescription))
                    {
                        currentIncome.IncomeTypeOtherDescription = sIncomeTypeOtherDescription;
                    }
                    currentIncome.MonthlyTotalAmount = sMonthlyTotal;
                }
            }
            return currentIncome;
        }
        private Residence CreateResidence()
        {
            Residence residence = new Residence();
            //residence.Id = null;
            //residence.BorrowerResidencyBasisType = null;
            //residence.BorrowerResidencyDurationMonths = null;
            //residence.BorrowerResidencyDurationYears = null;
            //residence.BorrowerResidencyType = null;
            //residence.City = null;
            //residence.Country = null;
            //residence.County = null;
            //residence.PostalCode = null;
            //residence.State = null;
            //residence.StreetAddress = null;
            //residence.StreetAddress2 = null;
            //residence.Landlord = CreateLandlord();
            return residence;
        }
        private Landlord CreateLandlord()
        {
            Landlord landlord = new Landlord();
            //landlord.Id = null;
            //landlord.City = null;
            //landlord.Name = null;
            //landlord.PostalCode = null;
            //landlord.State = null;
            //landlord.StreetAddress = null;
            //landlord.StreetAddress2 = null;
            //landlord.ContactDetail = CreateContactDetail();
            return landlord;
        }
        private MailTo CreateMailTo()
        {
            MailTo mailTo = new MailTo();
            //mailTo.Id = null;
            //mailTo.AddressSameAsPropertyIndicator = null;
            //mailTo.City = null;
            //mailTo.Country = null;
            //mailTo.PostalCode = null;
            //mailTo.State = null;
            //mailTo.StreetAddress = null;
            //mailTo.StreetAddress2 = null;
            return mailTo;
        }
        private TransactionDetail CreateTransactionDetail()
        {
            TransactionDetail transactionDetail = new TransactionDetail();
            //transactionDetail.Id = null;
            transactionDetail.AlterationsImprovementsAndRepairsAmount = m_dataLoan.sAltCost_rep;
            transactionDetail.BorrowerPaidDiscountPointsTotalAmount = m_dataLoan.sLDiscnt1003_rep;
            transactionDetail.EstimatedClosingCostsAmount = m_dataLoan.sTotEstCcNoDiscnt1003_rep;
            //transactionDetail.FreReserveAmount = null;
            transactionDetail.FreReservesAmount = m_dataLoan.sFredieReservesAmt_rep;
            transactionDetail.MiAndFundingFeeFinancedAmount = m_dataLoan.sFfUfmipFinanced_rep;
            transactionDetail.MiAndFundingFeeTotalAmount = m_dataLoan.sFfUfmip1003_rep;
            transactionDetail.PrepaidItemsEstimatedAmount = m_dataLoan.sTotEstPp1003_rep;
            if (m_dataLoan.sLPurposeT == E_sLPurposeT.Purchase)
            {
                transactionDetail.PurchasePriceAmount = m_dataLoan.sPurchPrice_rep;
                transactionDetail.SalesConcessionAmount = m_dataLoan.sFHASalesConcessions_rep;
                transactionDetail.SellerPaidClosingCostsAmount = m_dataLoan.sTotCcPbs_rep;
            }
            else if ((m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance) ||
                (m_dataLoan.sLPurposeT == E_sLPurposeT.Refin) ||
                (m_dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout) ||
                (m_dataLoan.sLPurposeT == E_sLPurposeT.HomeEquity) ||
                (m_dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl))
            {
                transactionDetail.RefinanceIncludingDebtsToBePaidOffAmount = m_dataLoan.sRefPdOffAmt1003_rep;
            }

            if ((m_dataLoan.sLienPosT == E_sLienPosT.First) && (m_dataLoan.sHas2ndFinPe))
            {
                transactionDetail.SubordinateLienAmount = m_dataLoan.sConcurSubFin_rep;
                transactionDetail.SubordinateLienHelocAmount = m_dataLoan.sHelocBal_rep;
                //transactionDetail.SubordinateLienPurposeType = null;
                //transactionDetail.SubordinateLienPurposeTypeOtherDescription = null;
            }
            //transactionDetail.PurchaseCreditList.Add(CreatePurchaseCredit()); // TODO: Need to create a loop
            return transactionDetail;
        }
        private PurchaseCredit CreatePurchaseCredit()
        {
            PurchaseCredit purchaseCredit = new PurchaseCredit();
            //purchaseCredit.Id = null;
            //purchaseCredit.Amount = null;
            //purchaseCredit.SourceType = null;
            //purchaseCredit.SourceTypeOtherDescription = null;
            //purchaseCredit.Type = null;
            //purchaseCredit.TypeOtherDescription = null;
            return purchaseCredit;
        }
        private TitleHolder CreateTitleHolder()
        {
            TitleHolder titleHolder = new TitleHolder();
            //titleHolder.Id = null;
            //titleHolder.LandTrustType = null;
            //titleHolder.LandTrustTypeOtherDescription = null;
            //titleHolder.Name = null;
            return titleHolder;
        }
        private RespaFee CreateRespaFee()
        {
            RespaFee respaFee = new RespaFee();
            //respaFee.Id = null;
            //respaFee.RespaSectionClassificationType = null;
            //respaFee.ItemDescription = null;
            //respaFee.PaidToType = null;
            //respaFee.PaidToTypeOtherDescription = null;
            //respaFee.PercentBasisType = null;
            //respaFee.PercentBasisTypeOtherDescription = null;
            //respaFee.RequiredProviderOfServiceIndicator = null;
            //respaFee.ResponsiblePartyType = null;
            //respaFee.SpecifiedFixedAmount = null;
            //respaFee.SpecifiedHudLineNumber = null;
            //respaFee.TotalAmount = null;
            //respaFee.TotalPercent = null;
            //respaFee.Type = null;
            //respaFee.TypeOtherDescription = null;
            //respaFee.PaymentList.Add(CreatePayment()); // TODO: Need to create a loop
            //respaFee.RequiredServiceProvider = CreateRequiredServiceProvider();
            //respaFee.PaidTo = CreatePaidTo();
            return respaFee;
        }
        private PaidTo CreatePaidTo()
        {
            PaidTo paidTo = new PaidTo();
            //paidTo.Id = null;
            //paidTo.NonPersonEntityIndicator = null;
            //paidTo.City = null;
            //paidTo.Country = null;
            //paidTo.County = null;
            //paidTo.Name = null;
            //paidTo.PostalCode = null;
            //paidTo.State = null;
            //paidTo.StreetAddress = null;
            //paidTo.StreetAddress2 = null;
            //paidTo.ContactDetail = CreateContactDetail();
            //paidTo.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return paidTo;
        }
        private RequiredServiceProvider CreateRequiredServiceProvider()
        {
            RequiredServiceProvider requiredServiceProvider = new RequiredServiceProvider();
            //requiredServiceProvider.Id = null;
            //requiredServiceProvider.City = null;
            //requiredServiceProvider.Name = null;
            //requiredServiceProvider.NatureOfRelationshipDescription = null;
            //requiredServiceProvider.PostalCode = null;
            //requiredServiceProvider.ReferenceIdentifier = null;
            //requiredServiceProvider.State = null;
            //requiredServiceProvider.StreetAddress = null;
            //requiredServiceProvider.StreetAddress2 = null;
            //requiredServiceProvider.TelephoneNumber = null;
            return requiredServiceProvider;
        }
        private Payment CreatePayment()
        {
            Payment payment = new Payment();
            //payment.Id = null;
            //payment.AllowableFhaClosingCostIndicator = null;
            //payment.Amount = null;
            //payment.CollectedByType = null;
            //payment.IncludedInAprIndicator = null;
            //payment.IncludedInStateHighCostIndicator = null;
            //payment.NetDueAmount = null;
            //payment.PaidByType = null;
            //payment.PaidByTypeThirdPartyName = null;
            //payment.PaidOutsideOfClosingIndicator = null;
            //payment.Percent = null;
            //payment.ProcessType = null;
            //payment.Section32Indicator = null;
            return payment;
        }
        private ReoProperty CreateReoProperty()
        {
            ReoProperty reoProperty = new ReoProperty();
            //reoProperty.ReoId = null;
            //reoProperty.LiabilityId = null;
            //reoProperty.BorrowerId = null;
            //reoProperty.City = null;
            //reoProperty.CurrentResidenceIndicator = null;
            //reoProperty.DispositionStatusType = null;
            //reoProperty.GsePropertyType = null;
            //reoProperty.LienInstallmentAmount = null;
            //reoProperty.LienUpbAmount = null;
            //reoProperty.MaintenanceExpenseAmount = null;
            //reoProperty.MarketValueAmount = null;
            //reoProperty.PostalCode = null;
            //reoProperty.RentalIncomeGrossAmount = null;
            //reoProperty.RentalIncomeNetAmount = null;
            //reoProperty.State = null;
            //reoProperty.StreetAddress = null;
            //reoProperty.StreetAddress2 = null;
            //reoProperty.SubjectIndicator = null;
            return reoProperty;
        }
        private ProposedHousingExpense CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType eHousingExpenseT)
        {
            ProposedHousingExpense proposedHousingExpense = new ProposedHousingExpense();
            switch (eHousingExpenseT)
            {
                case E_ProposedHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest:
                    proposedHousingExpense.PaymentAmount = m_dataLoan.sProFirstMPmt_rep;
                    break;
                case E_ProposedHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest:
                    proposedHousingExpense.PaymentAmount = m_dataLoan.sProSecondMPmt_rep;
                    break;
                case E_ProposedHousingExpenseHousingExpenseType.HazardInsurance:
                    proposedHousingExpense.PaymentAmount = m_dataLoan.sProHazIns_rep;
                    break;
                case E_ProposedHousingExpenseHousingExpenseType.RealEstateTax:
                    proposedHousingExpense.PaymentAmount = m_dataLoan.sProRealETx_rep;
                    break;
                case E_ProposedHousingExpenseHousingExpenseType.MI:
                    proposedHousingExpense.PaymentAmount = m_dataLoan.sProMIns_rep;
                    break;
                case E_ProposedHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees:
                    proposedHousingExpense.PaymentAmount = m_dataLoan.sProHoAssocDues_rep;
                    break;
                case E_ProposedHousingExpenseHousingExpenseType.OtherHousingExpense:
                    proposedHousingExpense.PaymentAmount = m_dataLoan.sProOHExp_rep;
                    proposedHousingExpense.HousingExpenseTypeOtherDescription = m_dataLoan.sProOHExpDesc;
                    break;
                default:
                    throw new UnhandledEnumException(eHousingExpenseT);
            }
            //proposedHousingExpense.Id = null;
            proposedHousingExpense.HousingExpenseType = eHousingExpenseT;
            //proposedHousingExpense.HousingExpenseTypeOtherDescription = null;
            return proposedHousingExpense;
        }
        private Property CreateProperty()
        {
            Property property = new Property();
            //property.Id = null;
            //property.AssessorsParcelIdentifier = null;
            //property.AssessorsSecondParcelIdentifier = null;
            //property.BuildingStatusType = null;
            //property.BuildingStatusTypeOtherDescription = null;
            //property.CurrentVacancyStatusType = null;
            //property.GrossLivingAreaSquareFeetCount = null;
            //property.ManufacturedHomeManufactureYear = null;
            //property.NativeAmericanLandsType = null;
            //property.NativeAmericanLandsTypeOtherDescription = null;
            property.PlannedUnitDevelopmentIndicator = m_dataLoan.sSpIsInPud ? E_YNIndicator.Y : E_YNIndicator.N;
            if (m_dataLoan.sGseSpT == E_sGseSpT.Condominium ||
                m_dataLoan.sGseSpT == E_sGseSpT.DetachedCondominium ||
                m_dataLoan.sGseSpT == E_sGseSpT.HighRiseCondominium ||
                m_dataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeCondominium)
            {
                property.StoriesCount = m_dataLoan.sProdCondoStories_rep;
            }

            //property.UniqueDwellingType = null;
            //property.UniqueDwellingTypeOtherDescription = null;
            //property.AcquiredDate = null;
            //property.AcreageNumber = null;
            property.City = m_dataLoan.sSpCity;
            //property.CommunityLandTrustIndicator = null;
            //property.ConditionDescription = null;
            //property.Country = null;
            property.County = m_dataLoan.sSpCounty;
            property.FinancedNumberOfUnits = m_dataLoan.sUnitsNum_rep;
            //property.InclusionaryZoningIndicator = null;
            //property.NeighborhoodLocationType = null;
            //property.NeighborhoodLocationTypeOtherDescription = null;
            property.OwnershipType = OwnershipType();
            //property.OwnershipTypeOtherDescription = null;
            property.PostalCode = m_dataLoan.sSpZip;
            //property.PreviouslyOccupiedIndicator = null;
            property.State = m_dataLoan.sSpState;
            property.StreetAddress = m_dataLoan.sSpAddr;
            //property.StreetAddress2 = null;
            property.StructureBuiltYear = m_dataLoan.sYrBuilt;
            //property.ZoningCategoryType = null;
            //property.ZoningCategoryTypeOtherDescription = null;
            //property.LegalDescriptionList.Add(CreateLegalDescription()); // TODO: Need to create a loop
            //property.ParsedStreetAddress = CreateParsedStreetAddress();
            //property.ValuationList.Add(CreateValuation()); // TODO: Need to create a loop
            property.Details = CreateDetails();
            //property.HomeownersAssociation = CreateHomeownersAssociation();
            //property.Project = CreateProject();
            property.CategoryList.Add(CreateCategory());
            //property.DwellingUnitList.Add(CreateDwellingUnit()); // TODO: Need to create a loop
            //property.ManufacturedHomeList.Add(CreateManufacturedHome()); // TODO: Need to create a loop
            //property.PlattedLandList.Add(CreatePlattedLand()); // TODO: Need to create a loop
            //property.UnplattedLand = CreateUnplattedLand();
            //property.FloodDeterminationList.Add(CreateFloodDetermination()); // TODO: Need to create a loop
            return property;
        }
        private E_PropertyOwnershipType OwnershipType()
        {
            switch(m_dataLoan.sGseSpT)
            {
                case E_sGseSpT.Condominium:
                case E_sGseSpT.DetachedCondominium:
                case E_sGseSpT.HighRiseCondominium:
                case E_sGseSpT.ManufacturedHomeCondominium:
                    if (!m_dataLoan.sProdIsCondotel)
                    {
                        return E_PropertyOwnershipType.Condominium;
                    }
                    else
                    {
                        return E_PropertyOwnershipType.CondominiumHotel;
                    }
                case E_sGseSpT.Cooperative: 
                    return E_PropertyOwnershipType.Cooperative;
                default:
                    return E_PropertyOwnershipType.Undefined;
            }
        }
        private FloodDetermination CreateFloodDetermination()
        {
            FloodDetermination floodDetermination = new FloodDetermination();
            //floodDetermination.FloodDeterminationId = null;
            //floodDetermination.FloodCertificationIdentifier = null;
            //floodDetermination.FloodContractFeeAmount = null;
            //floodDetermination.FloodDeterminationLifeofLoanIndicator = null;
            //floodDetermination.FloodPartialIndicator = null;
            //floodDetermination.FloodProductCertifyDate = null;
            //floodDetermination.NfipCommunityIdentifier = null;
            //floodDetermination.NfipCommunityName = null;
            //floodDetermination.NfipCommunityParticipationStatusType = null;
            //floodDetermination.NfipCommunityParticipationStatusTypeOtherDescription = null;
            //floodDetermination.NfipFloodZoneIdentifier = null;
            //floodDetermination.NfipMapIdentifier = null;
            //floodDetermination.NfipMapPanelDate = null;
            //floodDetermination.NfipMapPanelIdentifier = null;
            //floodDetermination.SpecialFloodHazardAreaIndicator = null;
            return floodDetermination;
        }
        private UnplattedLand CreateUnplattedLand()
        {
            UnplattedLand unplattedLand = new UnplattedLand();
            //unplattedLand.Id = null;
            //unplattedLand.PropertyRangeIdentifier = null;
            //unplattedLand.PropertySectionIdentifier = null;
            //unplattedLand.PropertyTownshipIdentifier = null;
            //unplattedLand.AbstractNumberIdentifier = null;
            //unplattedLand.BaseIdentifier = null;
            //unplattedLand.DescriptionType = null;
            //unplattedLand.DescriptionTypeOtherDescription = null;
            //unplattedLand.LandGrantIdentifier = null;
            //unplattedLand.MeridianIdentifier = null;
            //unplattedLand.MetesAndBoundsRemainingDescription = null;
            //unplattedLand.QuarterSectionIdentifier = null;
            //unplattedLand.SequenceIdentifier = null;
            return unplattedLand;
        }
        private PlattedLand CreatePlattedLand()
        {
            PlattedLand plattedLand = new PlattedLand();
            //plattedLand.Id = null;
            //plattedLand.PlatName = null;
            //plattedLand.PropertyBlockIdentifier = null;
            //plattedLand.PropertyLotIdentifier = null;
            //plattedLand.PropertySectionIdentifier = null;
            //plattedLand.PropertySubdivisionIdentifier = null;
            //plattedLand.PropertyTractIdentifier = null;
            //plattedLand.RecordedDocumentBook = null;
            //plattedLand.RecordedDocumentPage = null;
            //plattedLand.AdditionalParcelDescription = null;
            //plattedLand.AdditionalParcelIdentifier = null;
            //plattedLand.AppurtenanceDescription = null;
            //plattedLand.AppurtenanceIdentifier = null;
            //plattedLand.BuildingIdentifier = null;
            //plattedLand.PlatCodeIdentifier = null;
            //plattedLand.PlatInstrumentIdentifier = null;
            //plattedLand.SequenceIdentifier = null;
            //plattedLand.Type = null;
            //plattedLand.TypeOtherDescription = null;
            //plattedLand.UnitNumberIdentifier = null;
            return plattedLand;
        }
        private ManufacturedHome CreateManufacturedHome()
        {
            ManufacturedHome manufacturedHome = new ManufacturedHome();
            //manufacturedHome.Id = null;
            //manufacturedHome.LengthFeetCount = null;
            //manufacturedHome.WidthFeetCount = null;
            //manufacturedHome.AttachedToFoundationIndicator = null;
            //manufacturedHome.ConditionDescriptionType = null;
            //manufacturedHome.HudCertificationLabelIdentifier = null;
            //manufacturedHome.MakeIdentifier = null;
            //manufacturedHome.ModelIdentifier = null;
            //manufacturedHome.SerialNumberIdentifier = null;
            //manufacturedHome.WidthType = null;
            return manufacturedHome;
        }
        private DwellingUnit CreateDwellingUnit()
        {
            DwellingUnit dwellingUnit = new DwellingUnit();
            //dwellingUnit.Id = null;
            //dwellingUnit.BedroomCount = null;
            //dwellingUnit.PropertyRehabilitationCompletionDate = null;
            //dwellingUnit.EligibleRentAmount = null;
            //dwellingUnit.LeaseProvidedIndicator = null;
            return dwellingUnit;
        }
        private Category CreateCategory()
        {
            Category category = new Category();
            //category.Id = null;
            category.Type = PropertyCategoryType();
            //category.TypeOtherDescription = null;
            return category;
        }
        private E_CategoryType PropertyCategoryType()
        {
            switch(m_dataLoan.sGseSpT)
            {
                case E_sGseSpT.Attached: return E_CategoryType.Attached;
                case E_sGseSpT.Condominium: return E_CategoryType.LowRise;
                case E_sGseSpT.Cooperative: return E_CategoryType.Other;
                case E_sGseSpT.Detached: return E_CategoryType.Detached;
                case E_sGseSpT.DetachedCondominium: return E_CategoryType.LowRise;
                case E_sGseSpT.HighRiseCondominium: return E_CategoryType.HighRise;
                case E_sGseSpT.ManufacturedHomeCondominium: return E_CategoryType.Manufactured;
                case E_sGseSpT.ManufacturedHomeMultiwide: return E_CategoryType.ManufacturedMultiWide;
                case E_sGseSpT.ManufacturedHousing: return E_CategoryType.Manufactured;
                case E_sGseSpT.ManufacturedHousingSingleWide: return E_CategoryType.ManufacturedSingleWide;
                case E_sGseSpT.Modular: return E_CategoryType.Modular;
                case E_sGseSpT.PUD:
                    if (m_dataLoan.sProdSpStructureT == E_sProdSpStructureT.Detached)
                    {
                        return E_CategoryType.Detached;
                    }
                    else
                    {
                        return E_CategoryType.TownhouseRowhouse;
                    }
                case E_sGseSpT.LeaveBlank:
                default:
                    return E_CategoryType.Undefined;
            }
        }
        private Project CreateProject()
        {
            Project project = new Project();
            //project.Id = null;
            //project.LivingUnitCount = null;
            //project.ClassificationType = null;
            //project.ClassificationTypeOtherDescription = null;
            //project.DesignType = null;
            //project.DesignTypeOtherDescription = null;
            return project;
        }
        private HomeownersAssociation CreateHomeownersAssociation()
        {
            HomeownersAssociation homeownersAssociation = new HomeownersAssociation();
            //homeownersAssociation.Id = null;
            //homeownersAssociation.City = null;
            //homeownersAssociation.Country = null;
            //homeownersAssociation.County = null;
            //homeownersAssociation.Name = null;
            //homeownersAssociation.PostalCode = null;
            //homeownersAssociation.State = null;
            //homeownersAssociation.StreetAddress = null;
            //homeownersAssociation.StreetAddress2 = null;
            //homeownersAssociation.ContactDetail = CreateContactDetail();
            return homeownersAssociation;
        }
        private Details CreateDetails()
        {
            Details details = new Details();
            //details.Id = null;
            if ((m_dataLoan.sGseSpT == E_sGseSpT.Condominium) ||
                (m_dataLoan.sGseSpT == E_sGseSpT.DetachedCondominium) ||
                (m_dataLoan.sGseSpT == E_sGseSpT.HighRiseCondominium) ||
                (m_dataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeCondominium))
            {
                details.CondominiumIndicator = E_YNIndicator.Y;
            }
            else
            {
                details.CondominiumIndicator = E_YNIndicator.N;
            }

            //details.CondominiumPudDeclarationsDescription = null;
            //details.JudicialDistrictName = null;
            //details.JudicialDivisionName = null;
            if ((m_dataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeCondominium) ||
                (m_dataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeMultiwide) ||
                (m_dataLoan.sGseSpT == E_sGseSpT.ManufacturedHousing) ||
                (m_dataLoan.sGseSpT == E_sGseSpT.ManufacturedHousingSingleWide))
            {
                details.ManufacturedHomeIndicator = E_YNIndicator.Y;
            }
            else
            {
                details.ManufacturedHomeIndicator = E_YNIndicator.N;
            }
            
            //details.NfipCommunityIdentifier = null;
            //details.NfipCommunityName = null;
            //details.NfipCommunityParticipationStatusType = null;
            //details.NfipCommunityParticipationStatusTypeOtherDescription = null;
            //details.NfipFloodZoneIdentifier = null;
            //details.OneToFourFamilyIndicator = null;
            //details.ProjectName = null;
            //details.ProjectTotalSharesCount = null;
            //details.PropertyUnincorporatedAreaName = null;
            //details.RecordingJurisdictionName = null;
            //details.RecordingJurisdictionType = null;
            //details.RecordingJurisdictionTypeOtherDescription = null;
            return details;
        }
        private Valuation CreateValuation()
        {
            Valuation valuation = new Valuation();
            //valuation.Id = null;
            //valuation.AppraisalFormType = null;
            //valuation.AppraisalFormTypeOtherDescription = null;
            //valuation.AppraisalFormVersionIdentifier = null;
            //valuation.AppraisalInspectionType = null;
            //valuation.AppraisalInspectionTypeOtherDescription = null;
            //valuation.MethodType = null;
            //valuation.MethodTypeOtherDescription = null;
            //valuation.AppraiserList.Add(CreateAppraiser()); // TODO: Need to create a loop
            return valuation;
        }
        private Appraiser CreateAppraiser()
        {
            Appraiser appraiser = new Appraiser();
            //appraiser.Id = null;
            //appraiser.CompanyName = null;
            //appraiser.LicenseIdentifier = null;
            //appraiser.LicenseState = null;
            //appraiser.Name = null;
            return appraiser;
        }
        private ParsedStreetAddress CreateParsedStreetAddress()
        {
            ParsedStreetAddress parsedStreetAddress = new ParsedStreetAddress();
            //parsedStreetAddress.Id = null;
            //parsedStreetAddress.ApartmentOrUnit = null;
            //parsedStreetAddress.BuildingNumber = null;
            //parsedStreetAddress.DirectionPrefix = null;
            //parsedStreetAddress.DirectionSuffix = null;
            //parsedStreetAddress.HouseNumber = null;
            //parsedStreetAddress.MilitaryApoFpo = null;
            //parsedStreetAddress.PostOfficeBox = null;
            //parsedStreetAddress.RuralRoute = null;
            //parsedStreetAddress.StreetName = null;
            //parsedStreetAddress.StreetSuffix = null;
            return parsedStreetAddress;
        }
        private LegalDescription CreateLegalDescription()
        {
            LegalDescription legalDescription = new LegalDescription();
            //legalDescription.Id = null;
            //legalDescription.TextDescription = null;
            //legalDescription.Type = null;
            //legalDescription.TypeOtherDescription = null;
            //legalDescription.PlattedLand = CreatePlattedLand();
            return legalDescription;
        }
        private MortgageTerms CreateMortgageTerms()
        {
            MortgageTerms mortgageTerms = new MortgageTerms();
            //mortgageTerms.Id = null;
            //mortgageTerms.ArmTypeDescription = null;
            mortgageTerms.AgencyCaseIdentifier = m_dataLoan.sAgencyCaseNum;
            mortgageTerms.BaseLoanAmount = m_dataLoan.sLAmtCalc_rep;
            mortgageTerms.BorrowerRequestedLoanAmount = m_dataLoan.sFinalLAmt_rep;
            mortgageTerms.LenderCaseIdentifier = m_dataLoan.sLenderCaseNum;
            //mortgageTerms.LenderLoanIdentifier = null;
            //mortgageTerms.LendersContactPrimaryTelephoneNumber = null;
            mortgageTerms.LoanAmortizationTermMonths = m_dataLoan.sTerm_rep;
            mortgageTerms.LoanAmortizationType = AmortType(m_dataLoan.sFinMethT);
            //mortgageTerms.LoanEstimatedClosingDate = null;
            mortgageTerms.MortgageType = MtgType(m_dataLoan.sLT);
            mortgageTerms.NoteRatePercent = m_dataLoan.sNoteIR_rep;
            //mortgageTerms.OriginalLoanAmount = null;
            //mortgageTerms.OtherAmortizationTypeDescription = null;
            if (m_dataLoan.sLT == E_sLT.Other)
            {
                mortgageTerms.OtherMortgageTypeDescription = m_dataLoan.sLTODesc;
            }
            
            //mortgageTerms.PaymentRemittanceDay = null;
            mortgageTerms.RequestedInterestRatePercent = m_dataLoan.sNoteIR_rep;
            return mortgageTerms;
        }
        private E_MortgageTermsLoanAmortizationType AmortType(E_sFinMethT eFinanceMethodT)
        {
            switch (eFinanceMethodT)
            {
                case E_sFinMethT.ARM: return E_MortgageTermsLoanAmortizationType.AdjustableRate;
                case E_sFinMethT.Fixed: return E_MortgageTermsLoanAmortizationType.Fixed;
                case E_sFinMethT.Graduated: return E_MortgageTermsLoanAmortizationType.GraduatedPaymentMortgage;
                default:
                    return E_MortgageTermsLoanAmortizationType.Undefined;
            }
        }
        private E_MortgageTermsMortgageType MtgType(E_sLT eMtgType)
        {
            switch (eMtgType)
            {
                case E_sLT.Conventional: return E_MortgageTermsMortgageType.Conventional;
                case E_sLT.FHA: return E_MortgageTermsMortgageType.FHA;
                case E_sLT.Other: return E_MortgageTermsMortgageType.Other;
                case E_sLT.UsdaRural: return E_MortgageTermsMortgageType.FarmersHomeAdministration;
                case E_sLT.VA: return E_MortgageTermsMortgageType.VA;
                default:
                    return E_MortgageTermsMortgageType.Undefined;
            }
        }
        private MiData CreateMiData()
        {
            MiData miData = new MiData();
            //miData.Id = null;
            //miData.BorrowerMiTerminationDate = null;
            //miData.MiCertificateIdentifier = null;
            //miData.MiCollectedNumberOfMonthsCount = null;
            //miData.MiCompanyName = null;
            //miData.MiCushionNumberOfMonthsCount = null;
            //miData.MiDurationType = null;
            //miData.MiEscrowIncludedInAggregateIndicator = null;
            //miData.MiInitialPremiumAtClosingType = null;
            miData.MiInitialPremiumRateDurationMonths = m_dataLoan.sMipPiaMon_rep;
            miData.MiInitialPremiumRatePercent = m_dataLoan.sFfUfmipR_rep;
            miData.MiPremiumFromClosingAmount = m_dataLoan.sUfCashPd_rep;
            miData.MiPremiumPaymentType = MIPPaymentType(m_dataLoan.sProdMIOptionT);
            //miData.MiPremiumRefundableType = null;
            //miData.MiPremiumTermMonths = null;
            //miData.MiRenewalCalculationType = null;
            //miData.MiScheduledTerminationDate = null;
            if(m_dataLoan.sLT == E_sLT.FHA)
            {
                miData.MiSourceType = E_MiDataMiSourceType.FHA;
                miData.MiFhaUpfrontPremiumAmount = m_dataLoan.sFfUfmip1003_rep;
                miData.MiPremiumFinancedIndicator = m_dataLoan.sProdIsFhaMipFinanced ? E_YNIndicator.Y : E_YNIndicator.N;
            }
            else
            {
                miData.MiSourceType = E_MiDataMiSourceType.PMI;
                miData.MiInitialPremiumAmount = m_dataLoan.sFfUfmip1003_rep;
            }
            miData.MiLtvCutoffPercent = m_dataLoan.sProMInsCancelLtv_rep;
            //miData.MiLtvCutoffType = null;
            //miData.ScheduledAmortizationMidpointDate = null;
            //miData.MiPremiumTax = CreateMiPremiumTax();
            if (m_dataLoan.sProMIns > 0)
            {
                miData.MiRenewalPremiumList.Add(CreateMiRenewalPremium(E_MiRenewalPremiumSequence.First));
            }
            if (m_dataLoan.sProMIns2 > 0)
            {
                miData.MiRenewalPremiumList.Add(CreateMiRenewalPremium(E_MiRenewalPremiumSequence.Second));
            }
            //miData.PaidTo = CreatePaidTo();
            return miData;
        }
        private E_MiDataMiPremiumPaymentType MIPPaymentType(E_sProdMIOptionT eMIOptionT)
        {
            switch(eMIOptionT)
            {
                case E_sProdMIOptionT.BorrowerPdPmi: return E_MiDataMiPremiumPaymentType.BorrowerPaid;
                case E_sProdMIOptionT.NoPmi: return E_MiDataMiPremiumPaymentType.LenderPaid;
                case E_sProdMIOptionT.LeaveBlank:
                default:
                    return E_MiDataMiPremiumPaymentType.Undefined;
            }
        }
        private MiRenewalPremium CreateMiRenewalPremium(E_MiRenewalPremiumSequence miRenewalPremiumSequence)
        {
            MiRenewalPremium miRenewalPremium = new MiRenewalPremium();
            switch (miRenewalPremiumSequence)
            {
                case E_MiRenewalPremiumSequence.First:
                    miRenewalPremium.MonthlyPaymentAmount = m_dataLoan.sProMIns_rep;
                    miRenewalPremium.Rate = m_dataLoan.sProMInsR_rep;
                    miRenewalPremium.RateDurationMonths = m_dataLoan.sProMInsMon_rep;
                    break;
                case E_MiRenewalPremiumSequence.Second:
                    miRenewalPremium.MonthlyPaymentAmount = m_dataLoan.sProMIns2_rep;
                    miRenewalPremium.Rate = m_dataLoan.sProMInsR2_rep;
                    miRenewalPremium.RateDurationMonths = m_dataLoan.sProMIns2Mon_rep;
                    break;
                case E_MiRenewalPremiumSequence.Third:
                case E_MiRenewalPremiumSequence.Fourth:
                case E_MiRenewalPremiumSequence.Fifth:
                case E_MiRenewalPremiumSequence.Undefined:
                default:
                    break;
            }
            //miRenewalPremium.Id = null;
            //miRenewalPremium.MonthlyPaymentRoundingType = null;
            miRenewalPremium.Sequence = miRenewalPremiumSequence;
            return miRenewalPremium;
        }
        private MiPremiumTax CreateMiPremiumTax()
        {
            MiPremiumTax miPremiumTax = new MiPremiumTax();
            //miPremiumTax.Id = null;
            //miPremiumTax.CodeAmount = null;
            //miPremiumTax.CodePercent = null;
            //miPremiumTax.CodeType = null;
            return miPremiumTax;
        }
        private Mers CreateMers()
        {
            Mers mers = new Mers();
            //mers.Id = null;
            //mers.MersMortgageeOfRecordIndicator = null;
            //mers.MersOriginalMortgageeOfRecordIndicator = null;
            //mers.MersRegistrationDate = null;
            //mers.MersRegistrationIndicator = null;
            //mers.MersRegistrationStatusType = null;
            //mers.MersRegistrationStatusTypeOtherDescription = null;
            //mers.MersTaxNumberIdentifier = null;
            //mers.MersMinNumber = null;
            return mers;
        }
        private LoanQualification CreateLoanQualification()
        {
            LoanQualification loanQualification = new LoanQualification();
            //loanQualification.Id = null;
            loanQualification.AdditionalBorrowerAssetsConsideredIndicator = (m_dataLoan.sMultiApps || 
                (m_dataLoan.GetAppData(0).aHasSpouse && !m_dataLoan.GetAppData(0).aSpouseIExcl)) ? E_YNIndicator.Y : E_YNIndicator.N;
            loanQualification.AdditionalBorrowerAssetsNotConsideredIndicator = m_dataLoan.GetAppData(0).aSpouseIExcl ? E_YNIndicator.Y : E_YNIndicator.N;
            return loanQualification;
        }
        #region Loan Purpose
        private LoanPurpose CreateLoanPurpose()
        {
            LoanPurpose loanPurpose = new LoanPurpose();
            //loanPurpose.Id = null;
            loanPurpose.GseTitleMannerHeldDescription = m_dataLoan.GetAppData(0).aManner;
            loanPurpose.OtherLoanPurposeDescription = m_dataLoan.sOLPurposeDesc;
            loanPurpose.PropertyLeaseholdExpirationDate = m_dataLoan.sLeaseHoldExpireD_rep;
            //loanPurpose.PropertyRightsType = null;
            //loanPurpose.PropertyRightsTypeOtherDescription = null;
            loanPurpose.PropertyUsageType = PropertyUsageType();
            //loanPurpose.PropertyUsageTypeOtherDescription = null;
            loanPurpose.Type = LoanPurposeType();
            loanPurpose.ConstructionRefinanceData = CreateConstructionRefinanceData();
            return loanPurpose;
        }
        private E_LoanPurposeType LoanPurposeType()
        {
            switch (m_dataLoan.sLPurposeT)
            {
                case E_sLPurposeT.Construct: return E_LoanPurposeType.ConstructionOnly;
                case E_sLPurposeT.ConstructPerm: return E_LoanPurposeType.ConstructionToPermanent;
                case E_sLPurposeT.FhaStreamlinedRefinance: return E_LoanPurposeType.Refinance;
                case E_sLPurposeT.Other: return E_LoanPurposeType.Other;
                case E_sLPurposeT.Purchase: return E_LoanPurposeType.Purchase;
                case E_sLPurposeT.Refin: return E_LoanPurposeType.Refinance;
                case E_sLPurposeT.RefinCashout: return E_LoanPurposeType.Refinance;
                case E_sLPurposeT.VaIrrrl: return E_LoanPurposeType.Refinance;
                default: return E_LoanPurposeType.Unknown;
            }
        }
        private E_LoanPurposePropertyUsageType PropertyUsageType()
        {
            switch (m_dataLoan.GetAppData(0).aOccT)
            {
                case E_aOccT.Investment: return E_LoanPurposePropertyUsageType.Investment;
                case E_aOccT.PrimaryResidence: return E_LoanPurposePropertyUsageType.PrimaryResidence;
                case E_aOccT.SecondaryResidence: return E_LoanPurposePropertyUsageType.SecondHome;
                default: return E_LoanPurposePropertyUsageType.Undefined;
            }
        }
        private ConstructionRefinanceData CreateConstructionRefinanceData()
        {
            ConstructionRefinanceData constructionRefinanceData = new ConstructionRefinanceData();

            if (m_dataLoan.sLPurposeT == E_sLPurposeT.Refin || m_dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout || 
                m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || m_dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl
                || m_dataLoan.sLPurposeT == E_sLPurposeT.HomeEquity)
            {
                constructionRefinanceData.PropertyOriginalCostAmount = m_dataLoan.sSpOrigC_rep;
                constructionRefinanceData.PropertyAcquiredYear = m_dataLoan.sSpAcqYr;
                constructionRefinanceData.PropertyExistingLienAmount = m_dataLoan.sSpLien_rep;
                constructionRefinanceData.RefinanceImprovementCostsAmount = m_dataLoan.sSpImprovC_rep;
                constructionRefinanceData.RefinanceProposedImprovementsDescription = m_dataLoan.sSpImprovDesc;
                if (m_dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout)
                {
                    constructionRefinanceData.GseRefinancePurposeType = E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutDebtConsolidation;
                }

                constructionRefinanceData.FreCashOutAmount = m_dataLoan.sProdCashoutAmt_rep;
                if (m_dataLoan.sPayingOffSubordinate)
                {
                    constructionRefinanceData.SecondaryFinancingRefinanceIndicator = E_YNIndicator.Y;
                }

                switch(m_dataLoan.sSpImprovTimeFrameT)
                {
                    case E_sSpImprovTimeFrameT.Made:
                        constructionRefinanceData.RefinanceImprovementsType = E_ConstructionRefinanceDataRefinanceImprovementsType.Made;
                        break;
                    case E_sSpImprovTimeFrameT.ToBeMade:
                        constructionRefinanceData.RefinanceImprovementsType = E_ConstructionRefinanceDataRefinanceImprovementsType.ToBeMade;
                        break;
                    case E_sSpImprovTimeFrameT.LeaveBlank:
                    default:
                        constructionRefinanceData.RefinanceImprovementsType = E_ConstructionRefinanceDataRefinanceImprovementsType.Unknown;
                        break;
                }
                constructionRefinanceData.GseRefinancePurposeType = GseRefinancePurposeType(m_dataLoan.sGseRefPurposeT);
            }
            else if (m_dataLoan.sLPurposeT == E_sLPurposeT.Construct || m_dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm)
            {
                constructionRefinanceData.ConstructionImprovementCostsAmount = m_dataLoan.sLotImprovC_rep;
                constructionRefinanceData.LandEstimatedValueAmount = m_dataLoan.sLotVal_rep;
                constructionRefinanceData.PropertyOriginalCostAmount = m_dataLoan.sLotOrigC_rep;
                constructionRefinanceData.PropertyAcquiredYear = m_dataLoan.sLotAcqYr;
                constructionRefinanceData.PropertyExistingLienAmount = m_dataLoan.sLotLien_rep;
            }

            //constructionRefinanceData.Id = null;
            //constructionRefinanceData.ConstructionPeriodInterestRatePercent = null;
            //constructionRefinanceData.ConstructionPeriodNumberOfMonthsCount = null;
            //constructionRefinanceData.ConstructionPurposeType = null;
            //constructionRefinanceData.ConstructionToPermanentClosingFeatureType = null;
            //constructionRefinanceData.ConstructionToPermanentClosingFeatureTypeOtherDescription = null;
            //constructionRefinanceData.ConstructionToPermanentClosingType = null;
            //constructionRefinanceData.ConstructionToPermanentClosingTypeOtherDescription = null;
            //constructionRefinanceData.FnmSecondMortgageFinancingOriginalPropertyIndicator = null;
            //constructionRefinanceData.GseRefinancePurposeTypeOtherDescription = null;
            //constructionRefinanceData.LandOriginalCostAmount = null;
            //constructionRefinanceData.NonStructuralAlterationsConventionalAmount = null;
            //constructionRefinanceData.StructuralAlterationsConventionalAmount = null;
            return constructionRefinanceData;
        }
        private E_ConstructionRefinanceDataGseRefinancePurposeType GseRefinancePurposeType(E_sGseRefPurposeT eGSERefiPurpose)
        {
            switch (eGSERefiPurpose)
            {
                case E_sGseRefPurposeT.CashOutDebtConsolidation: return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutDebtConsolidation;
                case E_sGseRefPurposeT.CashOutHomeImprovement: return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutHomeImprovement;
                case E_sGseRefPurposeT.CashOutLimited: return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutLimited;
                case E_sGseRefPurposeT.CashOutOther: return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutOther;
                case E_sGseRefPurposeT.ChangeInRateTerm: return E_ConstructionRefinanceDataGseRefinancePurposeType.ChangeInRateTerm;
                case E_sGseRefPurposeT.LeaveBlank: return E_ConstructionRefinanceDataGseRefinancePurposeType.Other;
                case E_sGseRefPurposeT.NoCashOutFHAStreamlinedRefinance: return E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutFHAStreamlinedRefinance;
                case E_sGseRefPurposeT.NoCashOutFREOwnedRefinance: return E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutFREOwnedRefinance;
                case E_sGseRefPurposeT.NoCashOutOther: return E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutOther;
                case E_sGseRefPurposeT.NoCashOutStreamlinedRefinance: return E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutStreamlinedRefinance;
                default: return E_ConstructionRefinanceDataGseRefinancePurposeType.Undefined;
            }
        }
        #endregion
        private LoanProductData CreateLoanProductData()
        {
            LoanProductData loanProductData = new LoanProductData();
            //loanProductData.Id = null;
            if(m_dataLoan.sFinMethT == E_sFinMethT.ARM)
            {
                loanProductData.Arm = CreateArm();
            }
            
            //loanProductData.BuydownList.Add(CreateBuydown()); // TODO: Need to create a loop
            loanProductData.LoanFeatures = CreateLoanFeatures();
            //loanProductData.PaymentAdjustmentList.Add(CreatePaymentAdjustment()); // TODO: Need to create a loop
            if (m_dataLoan.sFinMethT == E_sFinMethT.ARM)
            {
                loanProductData.RateAdjustmentList.Add(CreateRateAdjustment());
            }
            //loanProductData.Heloc = CreateHeloc();
            if (m_dataLoan.sIOnlyMon > 0)
            {
                loanProductData.InterestOnly = CreateInterestOnly();
            }

            switch (m_dataLoan.sLienPosT)
            {
                case E_sLienPosT.Second:
                    if (m_dataLoan.sProdPpmtPenaltyMon2ndLien > 0)
                    {
                        loanProductData.PrepaymentPenaltyList.Add(CreatePrepaymentPenalty(m_dataLoan.sProdPpmtPenaltyMon2ndLien_rep));
                    }
                    break;
                case E_sLienPosT.First:
                default:
                    if (m_dataLoan.sProdPpmtPenaltyMon > 0)
                    {
                        loanProductData.PrepaymentPenaltyList.Add(CreatePrepaymentPenalty(m_dataLoan.sProdPpmtPenaltyMon_rep));
                    }
                    break;
            }
            
            //loanProductData.InterestCalculationRuleList.Add(CreateInterestCalculationRule()); // TODO: Need to create a loop
            return loanProductData;
        }
        private InterestCalculationRule CreateInterestCalculationRule()
        {
            InterestCalculationRule interestCalculationRule = new InterestCalculationRule();
            //interestCalculationRule.Id = null;
            //interestCalculationRule.InterestCalculationBasisDaysInPeriodType = null;
            //interestCalculationRule.InterestCalculationBasisDaysInPeriodTypeOtherDescription = null;
            //interestCalculationRule.InterestCalculationBasisDaysInYearCount = null;
            //interestCalculationRule.InterestCalculationBasisType = null;
            //interestCalculationRule.InterestCalculationBasisTypeOtherDescription = null;
            //interestCalculationRule.InterestCalculationEffectiveDate = null;
            //interestCalculationRule.InterestCalculationEffectiveMonthsCount = null;
            //interestCalculationRule.InterestCalculationExpirationDate = null;
            //interestCalculationRule.InterestCalculationPeriodAdjustmentIndicator = null;
            //interestCalculationRule.InterestCalculationPeriodType = null;
            //interestCalculationRule.InterestCalculationPurposeType = null;
            //interestCalculationRule.InterestCalculationPurposeTypeOtherDescription = null;
            //interestCalculationRule.InterestCalculationType = null;
            //interestCalculationRule.InterestCalculationTypeOtherDescription = null;
            //interestCalculationRule.InterestInAdvanceIndicator = null;
            //interestCalculationRule.LoanInterestAccrualStartDate = null;
            return interestCalculationRule;
        }
        private PrepaymentPenalty CreatePrepaymentPenalty(string sTerm)
        {
            PrepaymentPenalty prepaymentPenalty = new PrepaymentPenalty();
            //prepaymentPenalty.Id = null;
            //prepaymentPenalty.PenaltyFixedAmount = null;
            //prepaymentPenalty.Percent = null;
            //prepaymentPenalty.PeriodSequenceIdentifier = null;
            prepaymentPenalty.TermMonths = sTerm;
            //prepaymentPenalty.TextDescription = null;
            return prepaymentPenalty;
        }
        private InterestOnly CreateInterestOnly()
        {
            InterestOnly interestOnly = new InterestOnly();
            //interestOnly.Id = null;
            //interestOnly.MonthlyPaymentAmount = null;
            interestOnly.TermMonthsCount = m_dataLoan.sIOnlyMon_rep;
            return interestOnly;
        }
        private Heloc CreateHeloc()
        {
            Heloc heloc = new Heloc();
            //heloc.Id = null;
            //heloc.AnnualFeeAmount = null;
            //heloc.CreditCardAccountIdentifier = null;
            //heloc.CreditCardIndicator = null;
            //heloc.DailyPeriodicInterestRateCalculationType = null;
            //heloc.DailyPeriodicInterestRatePercent = null;
            //heloc.DrawPeriodMonthsCount = null;
            //heloc.FirstLienBookNumber = null;
            //heloc.FirstLienDate = null;
            //heloc.FirstLienHolderName = null;
            //heloc.FirstLienIndicator = null;
            //heloc.FirstLienInstrumentNumber = null;
            //heloc.FirstLienPageNumber = null;
            //heloc.FirstLienPrincipalBalanceAmount = null;
            //heloc.FirstLienRecordedDate = null;
            //heloc.InitialAdvanceAmount = null;
            //heloc.MaximumAprRate = null;
            //heloc.MinimumAdvanceAmount = null;
            //heloc.MinimumPaymentAmount = null;
            //heloc.MinimumPaymentPercent = null;
            //heloc.RepayPeriodMonthsCount = null;
            //heloc.ReturnedCheckChargeAmount = null;
            //heloc.StopPaymentChargeAmount = null;
            //heloc.TeaserTermEndDate = null;
            //heloc.TeaserTermMonthsCount = null;
            //heloc.TerminationFeeAmount = null;
            //heloc.TerminationPeriodMonthsCount = null;
            return heloc;
        }
        private RateAdjustment CreateRateAdjustment()
        {
            RateAdjustment rateAdjustment = new RateAdjustment();
            //rateAdjustment.Id = null;
            //rateAdjustment.FirstRateAdjustmentDate = null;
            rateAdjustment.FirstRateAdjustmentMonths = m_dataLoan.sRAdj1stCapMon_rep;
            rateAdjustment.SubsequentRateAdjustmentMonths = m_dataLoan.sRAdjCapMon_rep;
            //rateAdjustment.CalculationType = null;
            //rateAdjustment.CalculationTypeOtherDescription = null;
            //rateAdjustment.DurationMonths = null;
            //rateAdjustment.FirstChangeCapRate = null;
            //rateAdjustment.FirstChangeFloorPercent = null;
            //rateAdjustment.FirstChangeFloorRate = null;
            rateAdjustment.InitialCapPercent = m_dataLoan.sRAdj1stCapR_rep;
            //rateAdjustment.Percent = null;
            //rateAdjustment.PeriodNumber = null;
            rateAdjustment.SubsequentCapPercent = m_dataLoan.sRAdjCapR_rep;
            return rateAdjustment;
        }
        private PaymentAdjustment CreatePaymentAdjustment()
        {
            PaymentAdjustment paymentAdjustment = new PaymentAdjustment();
            //paymentAdjustment.Id = null;
            //paymentAdjustment.FirstPaymentAdjustmentDate = null;
            //paymentAdjustment.FirstPaymentAdjustmentMonths = null;
            //paymentAdjustment.LastPaymentAdjustmentDate = null;
            //paymentAdjustment.SubsequentPaymentAdjustmentMonths = null;
            //paymentAdjustment.Amount = null;
            //paymentAdjustment.CalculationType = null;
            //paymentAdjustment.CalculationTypeOtherDescription = null;
            //paymentAdjustment.DurationMonths = null;
            //paymentAdjustment.Percent = null;
            //paymentAdjustment.PeriodNumber = null;
            //paymentAdjustment.PeriodicCapAmount = null;
            //paymentAdjustment.PeriodicCapPercent = null;
            return paymentAdjustment;
        }
        private LoanFeatures CreateLoanFeatures()
        {
            LoanFeatures loanFeatures = new LoanFeatures();
            //loanFeatures.Id = null;
            //loanFeatures.AssumabilityIndicator = null;
            bool bIsBalloon = (m_dataLoan.sDue != m_dataLoan.sTerm);
            loanFeatures.BalloonIndicator = bIsBalloon ? E_YNIndicator.Y : E_YNIndicator.N;
            if (bIsBalloon)
            {
                loanFeatures.BalloonLoanMaturityTermMonths = m_dataLoan.sDue_rep;
            }
            loanFeatures.BuydownTemporarySubsidyIndicator = m_dataLoan.sBuydown ? E_YNIndicator.Y : E_YNIndicator.N;
            //loanFeatures.ConditionsToAssumabilityIndicator = null;
            //loanFeatures.ConformingIndicator = null;
            //loanFeatures.CounselingConfirmationIndicator = null;
            //loanFeatures.CounselingConfirmationType = null;
            //loanFeatures.DemandFeatureIndicator = null;
            //loanFeatures.DownPaymentOptionType = null;
            //loanFeatures.DownPaymentOptionTypeOtherDescription = null;
            loanFeatures.EscrowWaiverIndicator = m_dataLoan.sProdImpound ? E_YNIndicator.N : E_YNIndicator.Y;
            //loanFeatures.EstimatedPrepaidDays = null;
            //loanFeatures.EstimatedPrepaidDaysPaidByOtherTypeDescription = null;
            //loanFeatures.EstimatedPrepaidDaysPaidByType = null;
            //loanFeatures.FnmProductPlanIdentifier = null;
            //loanFeatures.FnmProductPlanIndentifier = null;
            //loanFeatures.FnmProjectClassificationType = null;
            //loanFeatures.FnmProjectClassificationTypeOtherDescription = null;
            loanFeatures.FreOfferingIdentifier = DataAccess.Tools.sFredAffordProdIdToLoanProspectorId(m_dataLoan.sFredAffordProgId);
            //loanFeatures.FreProjectClassificationType = null;
            //loanFeatures.FreProjectClassificationTypeOtherDescription = null;
            //loanFeatures.FullPrepaymentPenaltyOptionType = null;
            //loanFeatures.GseProjectClassificationType = null;
            loanFeatures.GsePropertyType = GSEPropertyType(m_dataLoan.sGseSpT);
            //loanFeatures.GraduatedPaymentMultiplierFactor = null;
            //loanFeatures.GrowingEquityLoanPayoffYearsCount = null;
            //loanFeatures.HelocInitialAdvanceAmount = null;
            loanFeatures.HelocMaximumBalanceAmount = m_dataLoan.sHelocCreditLimit_rep;
            //loanFeatures.InitialPaymentRatePercent = null;
            //loanFeatures.InitialPaymentDiscountPercent = null;
            loanFeatures.InterestOnlyTerm = m_dataLoan.sIOnlyMon_rep;
            //loanFeatures.LenderSelfInsuredIndicator = null;

            switch(m_dataLoan.sLienPosT)
            {
                case E_sLienPosT.Second:
                    loanFeatures.LienPriorityType = E_LoanFeaturesLienPriorityType.SecondLien;
                    if (m_dataLoan.sProdPpmtPenaltyMon2ndLien > 0)
                    {
                        loanFeatures.PrepaymentPenaltyIndicator = E_YNIndicator.Y;
                        loanFeatures.PrepaymentPenaltyTermMonths = m_dataLoan.sProdPpmtPenaltyMon2ndLien_rep;
                    }
                    else
                    {
                        loanFeatures.PrepaymentPenaltyIndicator = E_YNIndicator.N;
                        loanFeatures.PrepaymentPenaltyTermMonths = "0";
                    }
                    break;
                case E_sLienPosT.First:
                default:
                    loanFeatures.LienPriorityType = E_LoanFeaturesLienPriorityType.FirstLien;
                    if (m_dataLoan.sProdPpmtPenaltyMon > 0)
                    {
                        loanFeatures.PrepaymentPenaltyIndicator = E_YNIndicator.Y;
                        loanFeatures.PrepaymentPenaltyTermMonths = m_dataLoan.sProdPpmtPenaltyMon_rep;
                    }
                    else
                    {
                        loanFeatures.PrepaymentPenaltyIndicator = E_YNIndicator.N;
                        loanFeatures.PrepaymentPenaltyTermMonths = "0";
                    }
                    break;
            }
            //loanFeatures.LienPriorityTypeOtherDescription = null;
            //loanFeatures.LoanClosingStatusType = null;
            loanFeatures.LoanDocumentationType = DocType(m_dataLoan.sProdDocT);
            //loanFeatures.LoanDocumentationTypeOtherDescription = null;
            //loanFeatures.LoanMaturityDate = null;
            //loanFeatures.LoanOriginalMaturityTermMonths = null;
            //loanFeatures.LoanRepaymentType = null;
            //loanFeatures.LoanRepaymentTypeOtherDescription = null;
            //loanFeatures.LoanScheduledClosingDate = null;
            //loanFeatures.MiCertificationStatusType = null;
            //loanFeatures.MiCertificationStatusTypeOtherDescription = null;
            //loanFeatures.MiCompanyNameType = null;
            //loanFeatures.MiCompanyNameTypeOtherDescription = null;
            //loanFeatures.MiCoveragePercent = null;
            //loanFeatures.NameDocumentsDrawnInType = null;
            //loanFeatures.NameDocumentsDrawnInTypeOtherDescription = null;
            //loanFeatures.NegativeAmortizationLimitMonthsCount = null;
            loanFeatures.NegativeAmortizationLimitPercent = m_dataLoan.sPmtAdjMaxBalPc_rep;
            loanFeatures.NegativeAmortizationType = NegAmType(m_dataLoan.sNegAmortT);
            //loanFeatures.OriginalBalloonTermMonths = null;
            //loanFeatures.OriginalPrincipalAndInterestPaymentAmount = null;
            //loanFeatures.PaymentFrequencyType = null;
            //loanFeatures.PaymentFrequencyTypeOtherDescription = null;
            //loanFeatures.PrepaymentFinanceChargeRefundableIndicator = null;
            //loanFeatures.PrepaymentRestrictionIndicator = null;
            loanFeatures.ProductDescription = m_dataLoan.sLpTemplateNm;
            //loanFeatures.ProductIdentifier = null;
            //loanFeatures.ProductName = null;
            //loanFeatures.RefundableApplicationFeeIndicator = null;
            //loanFeatures.RequiredDepositIndicator = null;
            //loanFeatures.ScheduledFirstPaymentDate = null;
            //loanFeatures.ServicingTransferStatusType = null;
            //loanFeatures.TimelyPaymentRateReductionIndicator = null;
            //loanFeatures.TimelyPaymentRateReductionPercent = null;
            //loanFeatures.LateCharge = CreateLateCharge();
            //loanFeatures.NotePayTo = CreateNotePayTo();
            return loanFeatures;
        }
        private E_LoanFeaturesLoanDocumentationType DocType(E_sProdDocT DocT)
        {
            switch(DocT)
            {
                case E_sProdDocT.Alt: return E_LoanFeaturesLoanDocumentationType.Alternative;
                case E_sProdDocT.Full: return E_LoanFeaturesLoanDocumentationType.FullDocumentation;
                case E_sProdDocT.Light: return E_LoanFeaturesLoanDocumentationType.Other;
                case E_sProdDocT.NINA: return E_LoanFeaturesLoanDocumentationType.NoIncomeOn1003;
                case E_sProdDocT.NINANE: return E_LoanFeaturesLoanDocumentationType.NoDepositVerificationEmploymentVerificationOrIncomeVerification;
                case E_sProdDocT.NISA: return E_LoanFeaturesLoanDocumentationType.NoIncomeOn1003;
                case E_sProdDocT.NIVA: return E_LoanFeaturesLoanDocumentationType.NoRatio;
                case E_sProdDocT.NIVANE: return E_LoanFeaturesLoanDocumentationType.NoEmploymentVerificationOrIncomeVerification;
                case E_sProdDocT.SISA: return E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncomeOrAssets;
                case E_sProdDocT.SIVA: return E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncome;
                case E_sProdDocT.Streamline: return E_LoanFeaturesLoanDocumentationType.StreamlineRefinance;
                case E_sProdDocT.VINA: return E_LoanFeaturesLoanDocumentationType.NoDepositVerification;
                case E_sProdDocT.VISA: return E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedAssets;
                case E_sProdDocT._12MoPersonalBankStatements:
                case E_sProdDocT._24MoPersonalBankStatements:
                case E_sProdDocT._12MoBusinessBankStatements:
                case E_sProdDocT._24MoBusinessBankStatements:
                case E_sProdDocT.OtherBankStatements:
                case E_sProdDocT._1YrTaxReturns:
                case E_sProdDocT.Voe:
                    return E_LoanFeaturesLoanDocumentationType.Alternative;
                case E_sProdDocT.AssetUtilization:
                case E_sProdDocT.DebtServiceCoverage:
                    return E_LoanFeaturesLoanDocumentationType.NoRatio;
                case E_sProdDocT.NoIncome:
                    return E_LoanFeaturesLoanDocumentationType.NoDocumentation;
                default:
                    return E_LoanFeaturesLoanDocumentationType.Undefined;
            }
        }
        private E_LoanFeaturesGsePropertyType GSEPropertyType(E_sGseSpT eGSEPropT)
        {
            switch(eGSEPropT)
            {
                case E_sGseSpT.Attached: return E_LoanFeaturesGsePropertyType.Attached;
                case E_sGseSpT.Condominium: return E_LoanFeaturesGsePropertyType.Condominium;
                case E_sGseSpT.Cooperative: return E_LoanFeaturesGsePropertyType.Cooperative;
                case E_sGseSpT.Detached: return E_LoanFeaturesGsePropertyType.Detached;
                case E_sGseSpT.DetachedCondominium: return E_LoanFeaturesGsePropertyType.DetachedCondominium;
                case E_sGseSpT.HighRiseCondominium: return E_LoanFeaturesGsePropertyType.HighRiseCondominium;
                case E_sGseSpT.ManufacturedHomeCondominium: return E_LoanFeaturesGsePropertyType.ManufacturedHomeCondominium;
                case E_sGseSpT.ManufacturedHomeMultiwide: return E_LoanFeaturesGsePropertyType.ManufacturedHousingMultiWide;
                case E_sGseSpT.ManufacturedHousing: return E_LoanFeaturesGsePropertyType.ManufacturedHousing;
                case E_sGseSpT.ManufacturedHousingSingleWide: return E_LoanFeaturesGsePropertyType.ManufacturedHousingSingleWide;
                case E_sGseSpT.Modular: return E_LoanFeaturesGsePropertyType.Modular;
                case E_sGseSpT.PUD: return E_LoanFeaturesGsePropertyType.PUD;
                case E_sGseSpT.LeaveBlank:
                default:
                    return E_LoanFeaturesGsePropertyType.Undefined;
            }
        }
        private E_LoanFeaturesNegativeAmortizationType NegAmType(E_sNegAmortT eNegAmT)
        {
            switch (m_dataLoan.sNegAmortT)
            {
                case E_sNegAmortT.NoNegAmort: return E_LoanFeaturesNegativeAmortizationType.NoNegativeAmortization;
                case E_sNegAmortT.PotentialNegAmort: return E_LoanFeaturesNegativeAmortizationType.PotentialNegativeAmortization;
                case E_sNegAmortT.ScheduledNegAmort: return E_LoanFeaturesNegativeAmortizationType.ScheduledNegativeAmortization;
                case E_sNegAmortT.LeaveBlank:
                default:
                    return E_LoanFeaturesNegativeAmortizationType.Undefined;
            }
        }
        private NotePayTo CreateNotePayTo()
        {
            NotePayTo notePayTo = new NotePayTo();
            //notePayTo.Id = null;
            //notePayTo.City = null;
            //notePayTo.Country = null;
            //notePayTo.PostalCode = null;
            //notePayTo.State = null;
            //notePayTo.StreetAddress = null;
            //notePayTo.StreetAddress2 = null;
            //notePayTo.UnparsedName = null;
            return notePayTo;
        }
        private LateCharge CreateLateCharge()
        {
            LateCharge lateCharge = new LateCharge();
            //lateCharge.Id = null;
            //lateCharge.Amount = null;
            //lateCharge.GracePeriod = null;
            //lateCharge.LoanPaymentAmount = null;
            //lateCharge.MaximumAmount = null;
            //lateCharge.MinimumAmount = null;
            //lateCharge.Rate = null;
            //lateCharge.Type = null;
            return lateCharge;
        }
        private Buydown CreateBuydown()
        {
            Buydown buydown = new Buydown();
            //buydown.Id = null;
            //buydown.BaseDateType = null;
            //buydown.BaseDateTypeOtherDescription = null;
            //buydown.ChangeFrequencyMonths = null;
            //buydown.ContributorType = null;
            //buydown.ContributorTypeOtherDescription = null;
            //buydown.DurationMonths = null;
            //buydown.IncreaseRatePercent = null;
            //buydown.LenderFundingIndicator = null;
            //buydown.OriginalBalanceAmount = null;
            //buydown.PermanentIndicator = null;
            //buydown.SubsidyCalculationType = null;
            //buydown.TotalSubsidyAmount = null;
            //buydown.ContributorList.Add(CreateContributor()); // TODO: Need to create a loop
            //buydown.SubsidyScheduleList.Add(CreateSubsidySchedule()); // TODO: Need to create a loop
            return buydown;
        }
        private SubsidySchedule CreateSubsidySchedule()
        {
            SubsidySchedule subsidySchedule = new SubsidySchedule();
            //subsidySchedule.Id = null;
            //subsidySchedule.AdjustmentPercent = null;
            //subsidySchedule.PeriodIdentifier = null;
            //subsidySchedule.PeriodicPaymentEffectiveDate = null;
            //subsidySchedule.PeriodicPaymentSubsidyAmount = null;
            //subsidySchedule.PeriodicTerm = null;
            return subsidySchedule;
        }
        private Contributor CreateContributor()
        {
            Contributor contributor = new Contributor();
            //contributor.Id = null;
            //contributor.Amount = null;
            //contributor.Percent = null;
            //contributor.RoleType = null;
            //contributor.RoleTypeOtherDescription = null;
            //contributor.UnparsedName = null;
            return contributor;
        }
        private Arm CreateArm()
        {
            Arm arm = new Arm();
            //arm.Id = null;
            //arm.FnmTreasuryYieldForCurrentIndexDivisorNumber = null;
            //arm.FnmTreasuryYieldForIndexDivisorNumber = null;
            //arm.PaymentAdjustmentLifetimeCapAmount = null;
            //arm.PaymentAdjustmentLifetimeCapPercent = null;
            arm.RateAdjustmentLifetimeCapPercent = m_dataLoan.sRAdjLifeCapR_rep;
            //arm.ConversionOptionIndicator = null;
            arm.IndexCurrentValuePercent = m_dataLoan.sRAdjIndexR_rep;
            arm.IndexMarginPercent = m_dataLoan.sRAdjMarginR_rep;
            //arm.IndexType = null;
            //arm.IndexTypeOtherDescription = null;
            //arm.InterestRateRoundingFactor = null;
            //arm.InterestRateRoundingType = null;
            //arm.LifetimeCapRate = null;
            //arm.LifetimeFloorPercent = null;
            if (EncompassResponseHelper.ConvertToDecimal(m_dataLoan.sQualIRSubmitted_rep) != 0.000m)
            {
                arm.QualifyingRatePercent = m_dataLoan.sQualIRSubmitted_rep;
            }
            else
            {
                arm.QualifyingRatePercent = m_dataLoan.sNoteIRSubmitted_rep;
            }

            //arm.ConversionOption = CreateConversionOption();
            return arm;
        }
        private ConversionOption CreateConversionOption()
        {
            ConversionOption conversionOption = new ConversionOption();
            //conversionOption.Id = null;
            //conversionOption.ConversionOptionPeriodFeePercent = null;
            //conversionOption.EndingChangeDatePeriodDescription = null;
            //conversionOption.FeeAmount = null;
            //conversionOption.NoteTermGreaterThanFifteenYearsAdditionalPercent = null;
            //conversionOption.NoteTermLessThanFifteenYearsAdditionalPercent = null;
            //conversionOption.PeriodEndDate = null;
            //conversionOption.PeriodStartDate = null;
            //conversionOption.StartingChangeDatePeriodDescription = null;
            return conversionOption;
        }
        private Liability CreateLiability(ILiabilityJobExpense liaJobExpense, int iAppIndex, int iliaCount)
        {
            Liability liability = new Liability();
            liability.Id = LiabilityID(iliaCount);
            //liability.ReoId = null;
            liability.BorrowerId = LiabilityBorrowerId(liaJobExpense.OwnerT, iAppIndex);
            //liability.AlimonyOwedToName = null;
            //liability.LiabilityDescription = null;
            //liability.SubjectLoanResubordinationIndicator = null;
            //liability.AccountIdentifier = null;
            //liability.ExclusionIndicator = null;
            //liability.HolderCity = null;
            liability.HolderName = liaJobExpense.ExpenseDesc;
            //liability.HolderPostalCode = null;
            //liability.HolderState = null;
            //liability.HolderStreetAddress = null;
            //liability.HolderStreetAddress2 = null;
            liability.MonthlyPaymentAmount = liaJobExpense.Pmt_rep;
            //liability.PayoffStatusIndicator = null;
            //liability.PayoffWithCurrentAssetsIndicator = null;
            //liability.RemainingTermMonths = null;
            liability.Type = E_LiabilityType.JobRelatedExpenses;
            //liability.TypeOtherDescription = null;
            //liability.UnpaidBalanceAmount = null;
            return liability;
        }
        private Liability CreateLiability(ILiabilityAlimony liaAlimony, int iAppIndex, int iliaCount)
        {
            Liability liability = new Liability();
            liability.Id = LiabilityID(iliaCount);
            //liability.ReoId = null;
            liability.BorrowerId = LiabilityBorrowerId(liaAlimony.OwnerT, iAppIndex);
            liability.AlimonyOwedToName = liaAlimony.OwedTo;
            //liability.LiabilityDescription = null;
            //liability.SubjectLoanResubordinationIndicator = null;
            //liability.AccountIdentifier = null;
            liability.ExclusionIndicator = liaAlimony.NotUsedInRatio? E_YNIndicator.Y : E_YNIndicator.N;
            //liability.HolderCity = null;
            //liability.HolderName = null;
            //liability.HolderPostalCode = null;
            //liability.HolderState = null;
            //liability.HolderStreetAddress = null;
            //liability.HolderStreetAddress2 = null;
            liability.MonthlyPaymentAmount = liaAlimony.Pmt_rep;
            //liability.PayoffStatusIndicator = null;
            //liability.PayoffWithCurrentAssetsIndicator = null;
            liability.RemainingTermMonths = liaAlimony.RemainMons_rep;
            liability.Type = E_LiabilityType.Alimony;
            //liability.TypeOtherDescription = null;
            //liability.UnpaidBalanceAmount = null;
            return liability;
        }
        private Liability CreateLiability(ILiabilityChildSupport liaChildSupport, int iAppIndex, int iliaCount)
        {
            Liability liability = new Liability();
            liability.Id = LiabilityID(iliaCount);
            //liability.ReoId = null;
            liability.BorrowerId = LiabilityBorrowerId(liaChildSupport.OwnerT, iAppIndex);
            //liability.LiabilityDescription = null;
            //liability.SubjectLoanResubordinationIndicator = null;
            //liability.AccountIdentifier = null;
            liability.ExclusionIndicator = liaChildSupport.NotUsedInRatio ? E_YNIndicator.Y : E_YNIndicator.N;
            //liability.HolderCity = null;
            //liability.HolderName = null;
            //liability.HolderPostalCode = null;
            //liability.HolderState = null;
            //liability.HolderStreetAddress = null;
            //liability.HolderStreetAddress2 = null;
            liability.MonthlyPaymentAmount = liaChildSupport.Pmt_rep;
            //liability.PayoffStatusIndicator = null;
            //liability.PayoffWithCurrentAssetsIndicator = null;
            liability.RemainingTermMonths = liaChildSupport.RemainMons_rep;
            liability.Type = E_LiabilityType.ChildSupport;
            //liability.TypeOtherDescription = null;
            //liability.UnpaidBalanceAmount = null;
            return liability;
        }
        private Liability CreateLiability(ILiabilityRegular liaRegular, int iAppIndex, int iliaCount)
        {
            Liability liability = new Liability();
            liability.Id = LiabilityID(iliaCount);
            //liability.ReoId = null;
            liability.BorrowerId = LiabilityBorrowerId(liaRegular.OwnerT, iAppIndex);
            //liability.AlimonyOwedToName = null;
            //liability.LiabilityDescription = null;
            //liability.SubjectLoanResubordinationIndicator = null;
            liability.AccountIdentifier = liaRegular.AccNum.Value;
            liability.ExclusionIndicator = liaRegular.NotUsedInRatio ? E_YNIndicator.Y : E_YNIndicator.N;
            liability.HolderCity = liaRegular.ComCity;
            liability.HolderName = liaRegular.ComNm;
            liability.HolderPostalCode = liaRegular.ComZip;
            liability.HolderState = liaRegular.ComState;
            liability.HolderStreetAddress = liaRegular.ComAddr;
            //liability.HolderStreetAddress2 = null;
            liability.MonthlyPaymentAmount = liaRegular.Pmt_rep;
            liability.PayoffStatusIndicator = liaRegular.WillBePdOff ? E_YNIndicator.Y : E_YNIndicator.N;
            //liability.PayoffWithCurrentAssetsIndicator = null;
            liability.RemainingTermMonths = liaRegular.RemainMons_rep;
            liability.Type = LiabilityType(liaRegular.DebtT);
            //liability.TypeOtherDescription = null;
            liability.UnpaidBalanceAmount = liaRegular.Bal_rep;
            return liability;
        }
        private string LiabilityID(int iliaCount)
        {
            return string.Format("L{0}", iliaCount);
        }
        private string LiabilityBorrowerId(E_LiaOwnerT liaOwnerT, int iAppIndex)
        {
            switch(liaOwnerT)
            {
                case E_LiaOwnerT.Borrower: return EncompassResponseHelper.MismoBorrowerID(iAppIndex, true);
                case E_LiaOwnerT.CoBorrower: return EncompassResponseHelper.MismoBorrowerID(iAppIndex, false);
                case E_LiaOwnerT.Joint: return string.Format("{0} {1}",
                    EncompassResponseHelper.MismoBorrowerID(iAppIndex, true),
                    EncompassResponseHelper.MismoBorrowerID(iAppIndex, false));
                default:
                    return EncompassResponseHelper.MismoBorrowerID(iAppIndex, true);
            }
        }
        private E_LiabilityType LiabilityType(E_DebtRegularT debtT)
        {
            switch(debtT)
            {
                case E_DebtRegularT.Installment: return E_LiabilityType.Installment;
                case E_DebtRegularT.Mortgage: return E_LiabilityType.MortgageLoan;
                case E_DebtRegularT.Open: return E_LiabilityType.Open30DayChargeAccount;
                case E_DebtRegularT.Other: return E_LiabilityType.OtherLiability;
                case E_DebtRegularT.Revolving: return E_LiabilityType.Revolving;
                default:
                    return E_LiabilityType.Undefined;
            }
        }
        private InterviewerInformation CreateInterviewerInformation(IPreparerFields preparer)
        {
            InterviewerInformation interviewerInformation = new InterviewerInformation();
            //interviewerInformation.Id = null;
            //interviewerInformation.ApplicationTakenMethodType = null;
            //interviewerInformation.InterviewerApplicationSignedDate = null;
            interviewerInformation.InterviewersEmployerCity = preparer.City;
            interviewerInformation.InterviewersEmployerName = preparer.CompanyName;
            interviewerInformation.InterviewersEmployerPostalCode = preparer.Zip;
            interviewerInformation.InterviewersEmployerState = preparer.State;
            interviewerInformation.InterviewersEmployerStreetAddress = preparer.StreetAddr;
            //interviewerInformation.InterviewersEmployerStreetAddress2 = null;
            interviewerInformation.InterviewersName = preparer.PreparerName;
            interviewerInformation.InterviewersTelephoneNumber = preparer.Phone;
            return interviewerInformation;
        }
        private GovernmentReporting CreateGovernmentReporting()
        {
            GovernmentReporting governmentReporting = new GovernmentReporting();
            //governmentReporting.Id = null;
            //governmentReporting.HmdaPreapprovalType = null;
            //governmentReporting.HmdaPurposeOfLoanType = null;
            //governmentReporting.HmdaRateSpreadPercent = null;
            //governmentReporting.HmdaHoepaLoanStatusIndicator = null;
            return governmentReporting;
        }
        private GovernmentLoan CreateGovernmentLoan()
        {
            GovernmentLoan governmentLoan = new GovernmentLoan();
            //governmentLoan.Id = null;

            //TODO: update these methods once we support FHA streamline w/o appraisal
            if (m_dataLoan.sLT == E_sLT.FHA)
            {
                governmentLoan.FhaLoan = CreateFhaLoan();
                governmentLoan.FhaVaLoan = CreateFhaVaLoan();
            }
            else if (m_dataLoan.sLT == E_sLT.VA)
            {
                governmentLoan.FhaVaLoan = CreateFhaVaLoan();
                governmentLoan.VaLoan = CreateVaLoan();
            }
            return governmentLoan;
        }
        private VaLoan CreateVaLoan()
        {
            VaLoan vaLoan = new VaLoan();
            //vaLoan.Id = null;
            vaLoan.BorrowerFundingFeePercent = m_dataLoan.sFfUfmipR_rep;
            //vaLoan.VaBorrowerCoBorrowerMarriedIndicator = null;
            //vaLoan.VaEntitlementAmount = null;
            //vaLoan.VaEntitlementCodeIdentifier = null;
            //vaLoan.VaHouseholdSizeCount = null;
            //vaLoan.VaMaintenanceExpenseMonthlyAmount = null;
            //vaLoan.VaResidualIncomeAmount = null;
            //vaLoan.VaUtilityExpenseMonthlyAmount = null;
            return vaLoan;
        }
        private FhaVaLoan CreateFhaVaLoan()
        {
            FhaVaLoan fhaVaLoan = new FhaVaLoan();
            //fhaVaLoan.Id = null;
            //fhaVaLoan.BorrowerPaidFhaVaClosingCostsAmount = null;
            //fhaVaLoan.BorrowerPaidFhaVaClosingCostsPercent = null;
            //fhaVaLoan.GovernmentMortgageCreditCertificateAmount = null;
            fhaVaLoan.GovernmentRefinanceType = GovRefiType();
            //fhaVaLoan.GovernmentRefinanceTypeOtherDescription = null;
            //fhaVaLoan.OtherPartyPaidFhaVaClosingCostsAmount = null;
            //fhaVaLoan.OtherPartyPaidFhaVaClosingCostsPercent = null;
            //fhaVaLoan.PropertyEnergyEfficientHomeIndicator = null;
            //fhaVaLoan.SellerPaidFhaVaClosingCostsPercent = null;
            //fhaVaLoan.OriginatorIdentifier = null;
            return fhaVaLoan;
        }
        private E_FhaVaLoanGovernmentRefinanceType GovRefiType()
        {
            E_FhaVaLoanGovernmentRefinanceType eGovRefiType = E_FhaVaLoanGovernmentRefinanceType.Other;
            if (m_dataLoan.sProdDocT == E_sProdDocT.Full)
            {
                eGovRefiType = E_FhaVaLoanGovernmentRefinanceType.FullDocumentation;
            }
            
            if (m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance)
            {
                if (m_dataLoan.sHasAppraisal)
                    eGovRefiType = E_FhaVaLoanGovernmentRefinanceType.StreamlineWithAppraisal;
                else
                    eGovRefiType = E_FhaVaLoanGovernmentRefinanceType.StreamlineWithoutAppraisal;
            }
            else if (m_dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl)
            {
                eGovRefiType = E_FhaVaLoanGovernmentRefinanceType.InterestRateReductionRefinanceLoan;
            }
            return eGovRefiType;
        }
        private FhaLoan CreateFhaLoan()
        {
            FhaLoan fhaLoan = new FhaLoan();
            //fhaLoan.Id = null;
            //fhaLoan.BorrowerFinancedFhaDiscountPointsAmount = null;
            //fhaLoan.BorrowerHomeInspectionChosenIndicator = null;
            //fhaLoan.DaysToFhaMiEligibilityCount = null;
            //fhaLoan.FhaAlimonyLiabilityTreatmentType = null;
            //fhaLoan.FhaCoverageRenewalRatePercent = null;
            //fhaLoan.FhaEnergyRelatedRepairsOrImprovementsAmount = null;
            //fhaLoan.FhaGeneralServicesAdministrationCodeIdentifier = null;
            //fhaLoan.FhaGeneralServicesAdminstrationCodeIdentifier = null;
            //fhaLoan.FhaLimitedDenialParticipationIdentifier = null;
            //fhaLoan.FhaNonOwnerOccupancyRiderRule248Indicator = null;
            //fhaLoan.FhaRefinanceInterestOnExistingLienAmount = null;
            //fhaLoan.FhaRefinanceOriginalExistingFhaCaseIdentifier = null;
            //fhaLoan.FhaRefinanceOriginalExistingUpFrontMipAmount = null;
            //fhaLoan.FhaUnderwriterComputerizedHomesUnderwritingSystemIdentifier = null;
            if (m_dataLoan.sProdIsFhaMipFinanced)
            {
                fhaLoan.FhaUpfrontMiPremiumPercent = m_dataLoan.sFfUfmipR_rep;
            }
            //fhaLoan.FhaMiPremiumRefundAmount = null;
            //fhaLoan.HudAdequateAvailableAssetsIndicator = null;
            //fhaLoan.HudAdequateEffectiveIncomeIndicator = null;
            //fhaLoan.HudCreditCharacteristicsIndicator = null;
            //fhaLoan.HudStableEffectiveIncomeIndicator = null;
            //fhaLoan.SectionOfActType = null;
            //fhaLoan.SectionOfActTypeOtherDescription = null;
            //fhaLoan.SoldUnderHudSingleFamilyPropertyDispositionProgramIndicator = null;
            //fhaLoan.LenderIdentifier = null;
            //fhaLoan.SponsorIdentifier = null;
            return fhaLoan;
        }
        private EscrowAccountSummary CreateEscrowAccountSummary()
        {
            EscrowAccountSummary escrowAccountSummary = new EscrowAccountSummary();
            //escrowAccountSummary.EscrowAggregateAccountingAdjustmentAmount = null;
            //escrowAccountSummary.EscrowCushionNumberOfMonthsCount = null;
            return escrowAccountSummary;
        }
        private Escrow CreateEscrow()
        {
            Escrow escrow = new Escrow();
            //escrow.Id = null;
            //escrow.AnnualPaymentAmount = null;
            //escrow.CollectedNumberOfMonthsCount = null;
            //escrow.DueDate = null;
            //escrow.InsurancePolicyIdentifier = null;
            //escrow.ItemType = null;
            //escrow.ItemTypeOtherDescription = null;
            //escrow.MonthlyPaymentAmount = null;
            //escrow.MonthlyPaymentRoundingType = null;
            //escrow.PaidByType = null;
            //escrow.PaymentFrequencyType = null;
            //escrow.PaymentFrequencyTypeOtherDescription = null;
            //escrow.PremiumAmount = null;
            //escrow.PremiumDurationMonthsCount = null;
            //escrow.PremiumPaidByType = null;
            //escrow.PremiumPaymentType = null;
            //escrow.SpecifiedHud1LineNumber = null;
            //escrow.AccountSummary = CreateAccountSummary();
            //escrow.PaymentsList.Add(CreatePayments()); // TODO: Need to create a loop
            //escrow.PaidTo = CreatePaidTo();
            return escrow;
        }
        private Payments CreatePayments()
        {
            Payments payments = new Payments();
            //payments.Id = null;
            //payments.DueDate = null;
            //payments.PaymentAmount = null;
            //payments.SequenceIdentifier = null;
            return payments;
        }
        private AccountSummary CreateAccountSummary()
        {
            AccountSummary accountSummary = new AccountSummary();
            //accountSummary.Id = null;
            //accountSummary.EscrowAggregateAccountingAdjustmentAmount = null;
            //accountSummary.EscrowCushionNumberOfMonthsCount = null;
            return accountSummary;
        }
        private DownPayment CreateDownPayment()
        {
            DownPayment downPayment = new DownPayment();
            //downPayment.Id = null;
            downPayment.Amount = m_dataLoan.sEquityCalc_rep;
            downPayment.SourceDescription = m_dataLoan.sDwnPmtSrcExplain;
            downPayment.Type = DownPaymentType(m_dataLoan.sDwnPmtSrc);
            //downPayment.TypeOtherDescription = null;
            return downPayment;
        }
        private E_DownPaymentType DownPaymentType(string sDownPmtType)
        {
            if(string.IsNullOrEmpty(sDownPmtType))
                return E_DownPaymentType.Undefined;

            switch (sDownPmtType.TrimWhitespaceAndBOM().ToUpper())
            {
                case "CHECKING/SAVINGS": return E_DownPaymentType.CheckingSavings;
                case "GIFT FUNDS": return E_DownPaymentType.GiftFunds;
                case "STOCKS & BONDS": return E_DownPaymentType.StocksAndBonds;
                case "LOT EQUITY": return E_DownPaymentType.LotEquity;
                case "BRIDGE LOAN": return E_DownPaymentType.BridgeLoan;
                case "TRUST FUNDS": return E_DownPaymentType.TrustFunds;
                case "RETIREMENT FUNDS": return E_DownPaymentType.RetirementFunds;
                case "LIFE INSURANCE CASH VALUE": return E_DownPaymentType.LifeInsuranceCashValue;
                case "SALE OF CHATTEL": return E_DownPaymentType.SaleOfChattel;
                case "TRADE EQUITY": return E_DownPaymentType.TradeEquity;
                case "SWEAT EQUITY": return E_DownPaymentType.SweatEquity;
                case "CASH ON HAND": return E_DownPaymentType.CashOnHand;
                case "DEPOSIT ON SALES CONTRACT": return E_DownPaymentType.DepositOnSalesContract;
                case "EQUITY FROM PENDING SALE": return E_DownPaymentType.EquityOnPendingSale;
                case "EQUITY FROM SUBJECT PROPERTY": return E_DownPaymentType.EquityOnSubjectProperty;
                case "EQUITY ON SOLD PROPERTY": return E_DownPaymentType.EquityOnSoldProperty;
                case "OTHER TYPE OF DOWN PAYMENT": return E_DownPaymentType.OtherTypeOfDownPayment;
                case "RENT WITH OPTION TO PURCHASE": return E_DownPaymentType.RentWithOptionToPurchase;
                case "SECURED BORROWED FUNDS": return E_DownPaymentType.SecuredBorrowedFunds;
                case "UNSECURED BORROWED FUNDS": return E_DownPaymentType.UnsecuredBorrowedFunds;
                case "CASH OR OTHER EQUITY": return E_DownPaymentType.CashOrOtherEquity;
                case "CONTRIBUTION": return E_DownPaymentType.Contribution; 
                case "CREDIT CARD": return E_DownPaymentType.CreditCard; 
                case "FORGIVABLE SECURED LOAN": return E_DownPaymentType.ForgivableSecuredLoan;
                case "HOUSING RELOCATION": return E_DownPaymentType.HousingRelocation;
                case "MORTGAGE CREDIT CERTIFICATES": return E_DownPaymentType.MortgageCreditCertificates;
                case "PLEDGED COLLATERAL": return E_DownPaymentType.PledgedCollateral; 
                case "PREMIUM FUNDS": return E_DownPaymentType.PremiumFunds;
                case "SALES PRICE ADJUSTMENT": return E_DownPaymentType.SalesPriceAdjustment;
                case "SECONDARY FINANCING": return E_DownPaymentType.SecondaryFinancing;
                default:
                    return E_DownPaymentType.Undefined;
            }
        }
        private Asset CreateAsset()
        {
            Asset asset = new Asset();
            //asset.Id = null;
            //asset.BorrowerId = null;
            //asset.AssetDescription = null;
            //asset.AutomobileMakeDescription = null;
            //asset.AutomobileModelYear = null;
            //asset.LifeInsuranceFaceValueAmount = null;
            //asset.OtherAssetTypeDescription = null;
            //asset.StockBondMutualFundShareCount = null;
            //asset.AccountIdentifier = null;
            //asset.CashOrMarketValueAmount = null;
            //asset.HolderCity = null;
            //asset.HolderName = null;
            //asset.HolderPostalCode = null;
            //asset.HolderState = null;
            //asset.HolderStreetAddress = null;
            //asset.HolderStreetAddress2 = null;
            //asset.Type = null;
            //asset.VerifiedIndicator = null;
            return asset;
        }
        private AffordableLending CreateAffordableLending()
        {
            AffordableLending affordableLending = new AffordableLending();
            //affordableLending.Id = null;
            //affordableLending.FnmCommunityLendingProductName = null;
            //affordableLending.FnmCommunityLendingProductType = null;
            //affordableLending.FnmCommunityLendingProductTypeOtherDescription = null;
            //affordableLending.FnmCommunitySecondsIndicator = null;
            //affordableLending.FnmNeighborsMortgageEligibilityIndicator = null;
            //affordableLending.FreAffordableProgramIdentifier = null;
            //affordableLending.HudIncomeLimitAdjustmentFactor = null;
            //affordableLending.HudLendingIncomeLimitAmount = null;
            //affordableLending.HudMedianIncomeAmount = null;
            //affordableLending.MsaIdentifier = null;
            return affordableLending;
        }
        private AdditionalCaseData CreateAdditionalCaseData()
        {
            AdditionalCaseData additionalCaseData = new AdditionalCaseData();
            //additionalCaseData.Id = null;
            //additionalCaseData.MortgageScoreList.Add(CreateMortgageScore()); // TODO: Need to create a loop
            additionalCaseData.TransmittalData = CreateTransmittalData();
            return additionalCaseData;
        }
        private TransmittalData CreateTransmittalData()
        {
            TransmittalData transmittalData = new TransmittalData();
            //transmittalData.Id = null;
            //transmittalData.ArmsLengthIndicator = null;
            //transmittalData.BelowMarketSubordinateFinancingIndicator = null;
            //transmittalData.BuydownRatePercent = null;
            //transmittalData.CaseStateType = null;
            //transmittalData.CaseStateTypeOtherDescription = null;
            //transmittalData.CommitmentReferenceIdentifier = null;
            //transmittalData.ConcurrentOriginationIndicator = null;
            //transmittalData.ConcurrentOriginationLenderIndicator = null;
            //transmittalData.CreditReportAuthorizationIndicator = null;
            //transmittalData.CurrentFirstMortgageHolderType = null;
            //transmittalData.CurrentFirstMortgageHolderTypeOtherDescription = null;
            //transmittalData.InvestorInstitutionIdentifier = null;
            //transmittalData.InvestorLoanIdentifier = null;
            //transmittalData.LenderBranchIdentifier = null;
            //transmittalData.LenderRegistrationIdentifier = null;
            //transmittalData.LoanOriginationSystemLoanIdentifier = null;
            //transmittalData.LoanOriginatorType = null;
            //transmittalData.LoanOriginatorTypeOtherDescription = null;
            //transmittalData.PropertiesFinancedByLenderCount = null;
            transmittalData.PropertyAppraisedValueAmount = m_dataLoan.sApprVal_rep;
            //transmittalData.PropertyEstimatedValueAmount = null;
            transmittalData.RateLockPeriodDays = m_dataLoan.sProdRLckdDays_rep;
            //transmittalData.RateLockRequestedExtensionDays = null;
            //transmittalData.RateLockType = null;
            return transmittalData;
        }
        private MortgageScore CreateMortgageScore()
        {
            MortgageScore mortgageScore = new MortgageScore();
            //mortgageScore.Id = null;
            //mortgageScore.Date = null;
            //mortgageScore.Type = null;
            //mortgageScore.TypeOtherDescription = null;
            //mortgageScore.Value = null;
            return mortgageScore;
        }
        private DataInformation CreateDataInformation()
        {
            DataInformation dataInformation = new DataInformation();
            //dataInformation.Id = null;
            //dataInformation.DataVersionList.Add(CreateDataVersion()); // TODO: Need to create a loop
            return dataInformation;
        }
    }
}
