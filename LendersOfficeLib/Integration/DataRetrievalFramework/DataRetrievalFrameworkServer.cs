﻿namespace LendersOffice.Integration.DataRetrievalFramework
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.XsltExportReport;
    using LendersOffice.Admin;
    using Security;

    public class DataRetrievalFrameworkServer
    {
        public static string SubmitUsingExportXslt(Guid userId, Guid sLId, string mapName, AbstractUserPrincipal principal)
        {
            StringBuilder debugLog = new StringBuilder();
            debugLog.AppendLine("[DataRetrievalFrameworkServer.SubmitUsingExportXslt] - sLId=" + sLId + ", mapName=[" + mapName + "]");
            bool hasError = false;
            try
            {
                XsltExportRequest request = new XsltExportRequest(userId, new Guid[] { sLId }, mapName, null, null);

                XsltExportWorker worker = new XsltExportWorker(request, false, principal);

                XsltExportResult result = worker.Execute();

                if (result.HasError)
                {
                    StringBuilder sb = new StringBuilder();
                    foreach (var o in result.ErrorList)
                    {
                        sb.AppendLine(o);
                    }

                    debugLog.AppendLine("MapError");
                    debugLog.AppendLine(sb.ToString());
                    return sb.ToString();
                }
                else
                {
                    XsltMapItem mapItem = XsltMap.Get(mapName);
                    byte[] bytes = BinaryFileHelper.ReadAllBytes(result.OutputFileLocation);
                    debugLog.AppendLine();
                    
                    debugLog.AppendLine("Url: " + mapItem.ExportUrl);
                    debugLog.AppendLine("ContentType: " + mapItem.ExportContentType);

                    debugLog.AppendLine("==== REQUEST ====");
                    debugLog.AppendLine(MaskSensitiveData(System.Text.Encoding.UTF8.GetString(bytes)));

                    HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(mapItem.ExportUrl);

                    webRequest.KeepAlive = false;
                    webRequest.Method = "POST";
                    webRequest.ContentType = mapItem.ExportContentType;
                    webRequest.ContentLength = bytes.Length;

                    using (Stream stream = webRequest.GetRequestStream())
                    {
                        stream.Write(bytes, 0, bytes.Length);
                    }

                    WebResponse webResponse = webRequest.GetResponse();
                    string sResponse = string.Empty;
                    StringBuilder sb = new StringBuilder();
                    using (Stream stream = webResponse.GetResponseStream())
                    {
                        byte[] buffer = new byte[60000];
                        int size = stream.Read(buffer, 0, buffer.Length);
                        while (size > 0)
                        {
                            sb.Append(System.Text.Encoding.UTF8.GetString(buffer, 0, size));
                            size = stream.Read(buffer, 0, buffer.Length);
                        }
                    }
                    webResponse.Close();

                    sResponse = sb.ToString();

                    debugLog.AppendLine("==== RESPONSE ====");
                    debugLog.AppendLine(sResponse);

                    return sResponse;
                }
            }
            catch (Exception exc)
            {
                hasError = true;
                debugLog.AppendLine();
                debugLog.AppendLine("==== EXCEPTION ====");
                debugLog.AppendLine(exc.ToString());
                throw;
            }
            finally
            {
                if (hasError)
                {
                    Tools.LogError(debugLog.ToString());
                }
                else
                {
                    Tools.LogInfo(debugLog.ToString());
                }
            }

        }

        private static string MaskSensitiveData(string data)
        {
            return Regex.Replace(data, " (password|userid)=\"([^ ]+)\"", " $1=\"******\"", RegexOptions.IgnoreCase);
        }
        public static void Submit(string sLNm, Guid brokerID, Guid partnerID)
        {
            DataRetrievalPartner partner = DataRetrievalPartner.RetrieveById(partnerID);
            string sUrl = partner.ExportPath;

            BrokerDB currentLender = BrokerDB.RetrieveById(brokerID);
            string sCustomerCode = currentLender.CustomerCode;
            string sQueryString = string.Format("?CustomerCode={0}&sLNm={1}", HttpUtility.UrlEncode(sCustomerCode), HttpUtility.UrlEncode(sLNm));
            string sPostData = string.Format("SendingParty=LendingQB");
            byte[] postDataBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(sPostData);

            StringBuilder debug = new StringBuilder();
            debug.AppendLine("=== SUBMIT TO THIRD PARTY FRAMEWORK - URL=" + sUrl);
            try
            {
                sUrl = new Uri(sUrl).AbsoluteUri + sQueryString;
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(sUrl);

                webRequest.Timeout = 30000;
                webRequest.Method = "POST";
                webRequest.KeepAlive = false; // 11/4/2004 dd - DO NOT REMOVE THIS LINE. Else you will get "Cannot access a disposed object named "System.Net.TlsStream"" error randomly.
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.ContentLength = postDataBytes.Length;

                using (Stream stream = webRequest.GetRequestStream())
                {
                    stream.Write(postDataBytes, 0, postDataBytes.Length);
                }

                using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse())
                {
                    using (Stream stream = webResponse.GetResponseStream())
                    {
                        string content = GetString(stream);
                        debug.AppendLine().AppendLine(content);
                    }
                }
            }
            catch (UriFormatException uriExc)
            {
                debug.AppendLine().AppendLine("Error: " + uriExc.ToString());
                throw new CBaseException(ErrorMessages.SubmitToThirdParty.BadURL, uriExc);
            }
            catch (System.Security.Authentication.AuthenticationException authExc)
            {
                debug.AppendLine().AppendLine("Error: " + authExc.ToString());
                throw new CBaseException(ErrorMessages.SubmitToThirdParty.ConnectionFailure, authExc);
            }
            catch (WebException webExc)
            {
                debug.AppendLine().AppendLine("Error: " + webExc.ToString());
                throw new CBaseException(ErrorMessages.SubmitToThirdParty.ConnectionFailure, webExc);
            }
            catch (Exception exc)
            {
                debug.AppendLine().AppendLine("Error: " + exc.ToString());
                throw new CBaseException(ErrorMessages.SubmitToThirdParty.GenericFailure, exc);
            }
            finally
            {
                Tools.LogInfo(debug.ToString());
            }
        }

        private static string GetString(Stream stream)
        {
            const int BUFFER_SIZE = 50000;
            byte[] buffer = new byte[BUFFER_SIZE];
            int n = 0;

            StringBuilder sb = new StringBuilder();
            while ((n = stream.Read(buffer, 0, BUFFER_SIZE)) > 0)
            {
                sb.Append(System.Text.ASCIIEncoding.ASCII.GetString(buffer, 0, n));
            }
            return sb.ToString();
        }
    }
}
