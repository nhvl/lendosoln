﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using CommonLib;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Integration.DataRetrievalFramework
{
    public class DataRetrievalPartner
    {
        #region Variables
        public Guid PartnerID { get; private set; }
        public string Name { get; set; }
        public string ExportPath { get; set; }
        #endregion

        public bool IsXsltExport
        {
            get
            {
                if (string.IsNullOrEmpty(ExportPath) == false)
                {
                    return ExportPath.StartsWith("XSLT_EXPORT:");
                }
                return false;
            }
        }
        public string XsltExportMapName
        {
            get
            {
                if (IsXsltExport)
                {
                    return ExportPath.Substring("XSLT_EXPORT:".Length);
                }
                return string.Empty;
            }
        }

        private DataRetrievalPartner(DbDataReader reader)
        {
            this.PartnerID = SafeConvert.ToGuid(reader["PartnerId"]);
            this.Name = SafeConvert.ToString(reader["Name"]);
            this.ExportPath = SafeConvert.ToString(reader["ExportPath"]);
        }

        public static IEnumerable<DataRetrievalPartner> ListPartners()
        {
            List<DataRetrievalPartner> list = new List<DataRetrievalPartner>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "DATA_RETRIEVAL_PARTNER_List"))
            {
                while (reader.Read())
                {
                    list.Add(new DataRetrievalPartner(reader));
                }
            }
            return list;
        }

        public static IEnumerable<DataRetrievalPartner> ListPartnersByBrokerId(Guid brokerID)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerID)
                                        };

            HashSet<Guid> partnerIdList = new HashSet<Guid>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerID, "DATA_RETRIEVAL_PARTNER_ListByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    partnerIdList.Add((Guid)reader["PartnerId"]);
                }
            }

            List<DataRetrievalPartner> list = new List<DataRetrievalPartner>();

            foreach (var partner in ListPartners())
            {
                if (partnerIdList.Contains(partner.PartnerID))
                {
                    list.Add(partner);
                }
            }

            return list;
        }

        public static void AssociatePartnersWithBrokerId(Guid brokerID, IEnumerable<Guid> partnerIDList)
        {
            using (CStoredProcedureExec spTransaction = new CStoredProcedureExec(brokerID))
            {
                spTransaction.BeginTransactionForWrite();
                try
                {
                    SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerID)
                                        };

                    spTransaction.ExecuteNonQuery("DATA_RETRIEVAL_PARTNER_BROKER_RemoveAssociationByBrokerId",
                        parameters);

                    foreach (Guid partnerID in partnerIDList)
                    {
                        parameters = new SqlParameter[] {
                            new SqlParameter("@BrokerId", brokerID),
                            new SqlParameter("@PartnerId", partnerID)
                        };
                        spTransaction.ExecuteNonQuery("DATA_RETRIEVAL_PARTNER_BROKER_AddAssociation",
                            parameters);
                    }
                    spTransaction.CommitTransaction();
                }
                catch
                {
                    spTransaction.RollbackTransaction();
                    throw;
                }
            }
        }

        private int CountAssociations()
        {
            int count = 0;
            foreach (var connInfo in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters = {
                                            new SqlParameter("@PartnerId", PartnerID)
                                        };

                count += (int)StoredProcedureHelper.ExecuteScalar(connInfo, "DATA_RETRIEVAL_PARTNER_BROKER_CountByPartnerId", parameters);
            }
            return count;
        }

        public static int CountPartnersByBrokerId(Guid brokerID)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerID)
                                        };

            return (int)StoredProcedureHelper.ExecuteScalar(brokerID, "DATA_RETRIEVAL_PARTNER_BROKER_CountByBrokerId", parameters);
        }

        public static void CreatePartner(string sPartnerName, string sExportPath)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@Name", sPartnerName),
                                            new SqlParameter("@ExportPath", sExportPath)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "DATA_RETRIEVAL_PARTNER_Create", 3, parameters);
        }

        /// <summary>
        /// Deletes a data retrieval partner and any broker associations.
        /// </summary>
        public void Delete()
        {
            if (CountAssociations() > 0)
            {
                throw new CBaseException("Cannot delete this partner. One or more lenders are using them.", "LOAdmin user tried to delete a Data Retrieval Framework Partner that is in use.");
            }

            this.DeleteAllBrokerAssociationsForPartner();
            this.DeleteDataRetrievalPartner();
        }

        /// <summary>
        /// Deletes a data retrieval partner from the database.
        /// </summary>
        private void DeleteDataRetrievalPartner()
        {
             SqlParameter[] parameters =
             {
                 new SqlParameter("@PartnerId", PartnerID)
             };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "DATA_RETRIEVAL_PARTNER_Delete", 3, parameters);
        }

        /// <summary>
        /// Deletes any broker associations to the given data retrieval partner from the database.
        /// </summary>
        private void DeleteAllBrokerAssociationsForPartner()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@PartnerId", PartnerID)
            };

            foreach (var connInfo in DbConnectionInfo.ListAll())
            {
                StoredProcedureHelper.ExecuteNonQuery(connInfo, "DATA_RETRIEVAL_PARTNER_BROKER_DeleteByPartnerId", 3, parameters);
            }
        }

        public static DataRetrievalPartner RetrieveById(Guid partnerID)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@PartnerId", partnerID)
                                        };

            using (DbDataReader reader =
                StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "DATA_RETRIEVAL_PARTNER_RetrieveById", parameters))
            {
                if (reader.Read())
                {
                    return new DataRetrievalPartner(reader);
                }
            }
            throw new NotFoundException("Data Retrieval Partner was not found.", partnerID + " was not found.");
        }

        public void Save()
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@PartnerId", PartnerID),
                                            new SqlParameter("@Name", Name),
                                            new SqlParameter("@ExportPath", ExportPath)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "DATA_RETRIEVAL_PARTNER_Update", 3, parameters);
        }
    }
}
