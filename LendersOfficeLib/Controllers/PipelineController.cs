﻿// <copyright file="PipelineController.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   8/14/2015
// </summary>

namespace LendersOffice.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Reflection;
    using System.Text.RegularExpressions;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.MortgagePool;
    using LendersOffice.QueryProcessor;
    using LendersOffice.Reports;
    using LendersOffice.Security;
    using LendersOffice.Services;
    using LendersOffice.UI;
    using LendersOfficeApp;
    using LqbGrammar.Drivers.SecurityEventLogging;

    /// <summary>
    /// This controller class is to faciliate loading and saving the Trades and Pools pages in the new UI.
    /// </summary>
    public class PipelineController : ReflectionLendingQBController
    {
        /// <summary>
        /// The main method.  It processes the op parameter and determines what method to call.
        /// </summary>
        /// <param name="op">The op should be the action that you're trying to perform.</param>
        /// <returns>Returns the serialized object.</returns>
        public override object Main(string op)
        {
            if (op == "Load")
            {
                List<string> fieldList = this.GetRequestContent<List<string>>();
                return this.LoadPipeline(fieldList, PrincipalFactory.CurrentPrincipal.BrokerDB);
            }
            else if (op == "Save")
            {
                PipelineModel pipelineModel = this.GetRequestContent<PipelineModel>();

                return this.Save(pipelineModel);
            }
            else if (op == "Calculate")
            {
                PipelineModel pipelineModel = this.GetRequestContent<PipelineModel>();

                return this.Calculate(pipelineModel);
            }
            else if (op == "CalculateZipcode")
            {
                PipelineModel pipelineModel = this.GetRequestContent<PipelineModel>();

                var returnedString = InputModelProvider.CalculateZipcode(SerializationHelper.JsonNetSerialize(pipelineModel));
                return SerializationHelper.JsonNetDeserialize<PipelineModel>(returnedString);
            }
            else
            {
                throw new NotImplementedException("Unhandled Operation.");
            }
        }

        /// <summary>
        /// Inserts the objects to be displayed in the pipeline table, and inserts it into a PipelineModel.
        /// </summary>
        /// <param name="pipelineView">The Pipeline view that contains the needed info for determining what Pipeline to retrieve data for.</param>
        /// <returns>A pipeline model containing the info for the pipeline tables.</returns>
        public TableInfo GetPipelineTable(CPipelineView pipelineView)
        {
            PipelineModel model = new PipelineModel();

            TableInfo tableInfo = new TableInfo();
            tableInfo.Rows = new List<Dictionary<string, string>>();
            tableInfo.Headers = new List<string>();

            if (pipelineView.Active.TabType == E_TabTypeT.LoanPoolPortlet)
            {
                var keyNameMapping = new Dictionary<string, string>
                {
                    { "PoolId", "PoolId" },
                    { "InternalId", "Pool ID" },
                    { "PoolNumberByAgency", "Pool Number" },
                    { "PoolStatus", "Pool Status" },
                    { "AgencyT", "Pool Agency" },
                    { "AmortizationT", "Amortization Type" },
                    { "CommitmentNum", "Commitment Number" },
                    { "DeliveredD", "Pool Delivery Date" },
                    { "SecurityR", "Security Rate" },
                    { "LoanCount", "Number of Loans" },
                    { "TotalCurrentBalance", "Total Current Balance" },
                };

                var columnIndexMapping = new Dictionary<string, int>();
                DataTable dataTable = MortgagePool.FindMortgagePools(null, null, null, string.Empty, false, string.Empty, false, string.Empty, false);
                var columnKeys = dataTable.Columns.Cast<DataColumn>()
                                                .Select(x => x.ToString())
                                                .ToList();

                foreach (var keyNamePair in keyNameMapping)
                {
                    var index = columnKeys.IndexOf(keyNamePair.Key);
                    if (index >= 0)
                    {
                        columnIndexMapping.Add(keyNamePair.Value, index);
                    }
                }             

                tableInfo.Headers = columnIndexMapping.Keys.ToList();

                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    var row = dataTable.Rows[i];
                    Dictionary<string, string> values = new Dictionary<string, string>();

                    foreach (var keyIndexPair in columnIndexMapping)
                    {
                        var index = keyIndexPair.Value;
                        if (index >= row.ItemArray.Count())
                        {
                            continue;
                        }

                        var val = row.ItemArray[index];
                        values.Add(keyIndexPair.Key, val.ToString());
                    }

                    tableInfo.Rows.Add(values);

                    if (i >= 49)
                    {
                        break;
                    }
                }
            }
            else
            {
                Report rR = new Report();
                Guid reportId = pipelineView.Active.ReportId;
                LoanReporting m_lR = new LoanReporting();

                E_ReportExtentScopeT scope = E_ReportExtentScopeT.Assign;

                if (reportId == LoanReporting.DefaultSimplePipelineReportId || reportId == LoanReporting.DefaultSimplePipelineNonPMLReportId)
                {
                    // Make sure the proper default pipeline is loaded based on broker settings.
                    reportId = PrincipalFactory.CurrentPrincipal.BrokerDB.HasLenderDefaultFeatures ? LoanReporting.DefaultSimplePipelineReportId : LoanReporting.DefaultSimplePipelineNonPMLReportId;
                }

                bool usingRoleDefaultReport = reportId == Guid.Empty;

                rR = m_lR.Pipeline(reportId, PrincipalFactory.CurrentPrincipal, string.Empty, scope);

                if (rR.Stats.HitCount == ConstAppDavid.MaxViewableRecordsInPipeline)
                {
                }

                if (rR == null)
                {
                    throw new InvalidOperationException("Pipeline result is null.");
                }

                for (int j = 0; j < rR.Columns.Items.Count; j++)
                {
                    tableInfo.Headers.Add(((Column)rR.Columns.Items[j]).Name);
                }

                Rows rowsP = (Rows)rR.Tables[0];

                for (int i = 0; i < rowsP.Items.Count; i++)
                {
                    Row rowValues = (Row)rowsP.Items[i];
                    Dictionary<string, string> values = new Dictionary<string, string>();

                    for (int j = 0; j < rR.Columns.Items.Count; j++)
                    {
                        Value val = rowValues.Items[j];
                        values.Add(((Column)rR.Columns.Items[j]).Id, val.Text);
                    }

                    values.Add("sLID", rowValues.Key.ToString());
                    tableInfo.Rows.Add(values);

                    if (i >= 49)
                    {
                        break;
                    }
                }
            }

            return tableInfo;
        }

        /// <summary>
        /// Updates the model with user input.
        /// </summary>
        /// <param name="pipelineModel">The data the updated model will calculate from.</param>
        /// <returns>An updated pool model that used the user's input.</returns>
        public PipelineModel Calculate(PipelineModel pipelineModel)
        {
            BrokerDB brokerDb = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
            return this.BindData(pipelineModel, brokerDb);
        }

        /// <summary>
        /// Saves the broker and employee fields.
        /// </summary>
        /// <param name="pipelineModel">The model sent by the client page.</param>
        /// <returns>An updated version fo the pipeline model.</returns>
        public PipelineModel Save(PipelineModel pipelineModel)
        {
            BrokerDB brokerDb = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
            PipelineModel model = this.BindData(pipelineModel, brokerDb);
            brokerDb.Save();

            var brokerInfoService = new BrokerInfoService();
            if (pipelineModel.CreditReportAgents != null)
            {
                brokerInfoService.SetBrokerCRAs(pipelineModel.CreditReportAgents, PrincipalFactory.CurrentPrincipal.BrokerId, PrincipalFactory.CurrentPrincipal.BrokerDB);
            }

            if (pipelineModel.PipelineView != null)
            {
                Tools.UpdateMainTabsSettingXmlContent(PrincipalFactory.CurrentPrincipal.ConnectionInfo, PrincipalFactory.CurrentPrincipal.UserId, pipelineModel.PipelineView);
            }

            if (pipelineModel.FairLendingNoticeAddress != null)
            {
                brokerDb.FairLendingNoticeAddress = pipelineModel.FairLendingNoticeAddress.Select(x => x.Value).ToArray();
            }

            if (pipelineModel.NamingOptions != null)
            {
                brokerInfoService.BindNamingOptions(pipelineModel.NamingOptions, PrincipalFactory.CurrentPrincipal.BrokerDB);
            }

            if (pipelineModel.HiddenCras != null)
            {
                XmlDocument doc = new XmlDocument();
                XmlElement root = doc.CreateElement("list");
                doc.AppendChild(root);

                XmlElement pmlList = doc.CreateElement("pml");
                root.AppendChild(pmlList);

                var ids = pipelineModel.HiddenCras.Select(x => x.Key);

                foreach (string s in ids)
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        XmlElement el = doc.CreateElement("item");
                        el.SetAttribute("id", s);
                        pmlList.AppendChild(el);
                    }
                }

                var broker = PrincipalFactory.CurrentPrincipal.BrokerDB;
                broker.BlockedCRAsXmlContent = root.OuterXml;
            }

            if (pipelineModel.CompanyLicenses != null)
            {
                LicenseInfoList licenseInfoList = new LicenseInfoList();

                foreach (var licenseModel in pipelineModel.CompanyLicenses)
                {
                    var licenseInfo = new LicenseInfo();
                    InputModelProvider.BindObjectFromInputModel(licenseInfo, licenseModel);
                    if (LicenseInfoList.IsValid(licenseInfo) && !licenseInfoList.IsDuplicateLicense(licenseInfo) && !licenseInfoList.IsFull)
                    {
                        licenseInfoList.Add(licenseInfo);
                    }
                }

                brokerDb.LicenseInformationList = licenseInfoList;
            }

            if (pipelineModel.BranchPrefixes != null)
            {
                brokerInfoService.SetBranchNamePrefix(PrincipalFactory.CurrentPrincipal.BrokerId, pipelineModel.BranchPrefixes);
                model.BranchPrefixes = brokerInfoService.GetBranchNamePrefix(PrincipalFactory.CurrentPrincipal.BrokerId);
            }

            return model;
        }

        /// <summary>
        /// Binds data from the user's model to the data object.
        /// </summary>
        /// <param name="pipelineModel">The model to draw the data from.</param>
        /// <param name="brokerDb">The broker db object.</param>
        /// <returns>An updated version of the pipeline model based off the user's input.</returns>
        public PipelineModel BindData(PipelineModel pipelineModel, BrokerDB brokerDb)
        {
            var brokerInfoService = new BrokerInfoService();

            foreach (string key in pipelineModel.BrokerAddress.Keys)
            {
                this.SetValue(brokerDb.Address, key, pipelineModel.BrokerAddress[key].Value);
            }

            foreach (string key in pipelineModel.BrokerFields.Keys)
            {
                this.SetValue(brokerDb, key, pipelineModel.BrokerFields[key].Value);
            }

            if (pipelineModel.PipelineView != null)
            {
                // TODO
                // Unfortunately, the JSON deserialization ends up converting tabs of type Pipeline to type LoanPool.  For now, we are not going to update the list of tabs to bypass the issue.
                // To be revisited when the rest of the tabs are implemented.
                CPipelineView originalView = Tools.GetMainTabsSettingXmlContent(PrincipalFactory.CurrentPrincipal.ConnectionInfo, PrincipalFactory.CurrentPrincipal.UserId);
                pipelineModel.PipelineView.Tabs = originalView.Tabs;
                Tools.UpdateMainTabsSettingXmlContent(PrincipalFactory.CurrentPrincipal.ConnectionInfo, PrincipalFactory.CurrentPrincipal.UserId, pipelineModel.PipelineView);
            }

            if (pipelineModel.IpWhiteList != null)
            {
                brokerInfoService.EditGlobalWhiteList(pipelineModel.IpWhiteList, PrincipalFactory.CurrentPrincipal.BrokerId);
                pipelineModel.IpWhiteList = brokerInfoService.GlobalWhiteList(PrincipalFactory.CurrentPrincipal.BrokerId);
            }

            return this.RetrieveDataModel(brokerDb, pipelineModel);
        }

        /// <summary>
        /// Retrieve caclulated model from broker data and old model.
        /// </summary>
        /// <param name="brokerDb">Caculated broker data.</param>
        /// <param name="oldModel">Old model to be calculated. </param>
        /// <returns>New caclulated pipeline model.</returns>
        public PipelineModel RetrieveDataModel(BrokerDB brokerDb, PipelineModel oldModel)
        {
            var pipelineModel = this.LoadPipeline(this.GetFieldListFromDataModel(oldModel), brokerDb);

            var brokerInfoService = new BrokerInfoService();
            if (oldModel.CreditReportAgents != null)
            {
                pipelineModel.CreditReportAgents = brokerInfoService.CalculateCraModel(oldModel.CreditReportAgents);
            }

            if (oldModel.PasswordOption != null)
            {
                pipelineModel.PasswordOption = brokerInfoService.CalculatePasswordOption(oldModel.PasswordOption);
            }

            if (pipelineModel.PipelineView != null)
            {
                // TODO
                // Unfortunately, the JSON deserialization ends up converting tabs of type Pipeline to type LoanPool.  For now, we are not going to update the list of tabs to bypass the issue.
                // To be revisited when the rest of the tabs are implemented.
                CPipelineView originalView = Tools.GetMainTabsSettingXmlContent(PrincipalFactory.CurrentPrincipal.ConnectionInfo, PrincipalFactory.CurrentPrincipal.UserId);
                pipelineModel.PipelineView.Tabs = originalView.Tabs;
                Tools.UpdateMainTabsSettingXmlContent(PrincipalFactory.CurrentPrincipal.ConnectionInfo, PrincipalFactory.CurrentPrincipal.UserId, pipelineModel.PipelineView);
            }

            if (oldModel.NamingOptions != null)
            {
                var namingOption = new DataAccess.Utilities.LoanNaming.LoanNamingOption();
                InputModelProvider.BindObjectFromInputModel(namingOption, oldModel.NamingOptions);

                pipelineModel.NamingOptions = brokerInfoService.CalculateNamingOptions(namingOption);
            }

            if (oldModel.HiddenCras != null)
            {
                var craList = brokerInfoService.GetCRAOptions(PrincipalFactory.CurrentPrincipal.BrokerId);
                var craOptions = craList.Select(x => new KeyValuePair<string, string>(x.ID.ToString(), x.VendorName));
                var filteredCraOptions = craOptions.Except(oldModel.HiddenCras);

                pipelineModel.CraList = oldModel.CraList;
                pipelineModel.CraList.Options = filteredCraOptions;
                if (pipelineModel.CraList.Options.All(x => x.Key != pipelineModel.CraList.Value))
                {
                    pipelineModel.CraList.Value = pipelineModel.CraList.Options.FirstOrDefault().Key ?? Guid.Empty.ToString();
                }

                pipelineModel.HiddenCras = oldModel.HiddenCras;
            }

            if (oldModel.BranchPrefixes != null)
            {
                pipelineModel.BranchPrefixes = brokerInfoService.CalculateBranchNamePrefix(oldModel.BranchPrefixes);
            }

            return pipelineModel;
        }

        /// <summary>
        /// Traverse through the data model and generate a list of dependency field ids.
        /// </summary>
        /// <param name="model">The data model.</param>
        /// <returns>A list of dependency field list.</returns>
        public IEnumerable<string> GetFieldListFromDataModel(PipelineModel model)
        {
            List<string> fieldList = new List<string>();

            if (model.Fields != null)
            {
                foreach (var key in model.Fields.Keys)
                {
                    fieldList.Add(key);
                }
            }

            if (model.BrokerFields != null)
            {
                foreach (var key in model.BrokerFields.Keys)
                {
                    fieldList.Add("broker." + key);
                }
            }

            if (model.BrokerAddress != null)
            {
                foreach (var key in model.BrokerAddress.Keys)
                {
                    fieldList.Add("brokerAddress." + key);
                }
            }

            if (model.PipelineView != null)
            {
                fieldList.Add("PipelineView");
            }

            if (model.IpWhiteList != null)
            {
                fieldList.Add("IpWhiteList");
            }

            return fieldList;
        }

        /// <summary>
        /// Loads the Pool object.
        /// </summary>
        /// <param name="fieldList">The list of fields to be retrived from the pool.</param>
        /// <param name="brokerDb">The broker db object.</param>
        /// <returns>REturns the newly loaded object.</returns>
        public PipelineModel LoadPipeline(IEnumerable<string> fieldList, BrokerDB brokerDb)
        {
            PipelineModel model = new PipelineModel();

            InputFieldModel field = new InputFieldModel();

            // Broker Specific fields.
            foreach (string fieldName in fieldList)
            {
                char[] splitToken = { '.' };
                var fieldNameTokens = fieldName.Split(splitToken, 2);
                SimpleObjectInputFieldModel fieldsModel;
                var brokerInfoService = new BrokerInfoService();

                switch (fieldNameTokens[0])
                {
                    case "fairLendNoticeAddress":
                        model.FairLendingNoticeAddress = brokerDb.FairLendingNoticeAddress.Select(x => new InputFieldModel(x));
                        continue;
                    case "IpWhiteList":
                        model.IpWhiteList = brokerInfoService.GlobalWhiteList(PrincipalFactory.CurrentPrincipal.BrokerId);
                        model.IpWhiteListTemplate = InputModelProvider.GetInputModel(new BrokerGlobalIpWhiteList());
                        continue;
                    case "ComLicenses":
                        model.CompanyLicenses = brokerDb.LicenseInformationList.List.Cast<LicenseInfo>().Select(InputModelProvider.GetInputModel);
                        model.CompanyLicenseTemplate = InputModelProvider.GetInputModel(new LicenseInfo());
                        continue;
                    case "creditAgents":
                        model.CreditReportAgents = brokerInfoService.GetBrokerCRAs(PrincipalFactory.CurrentPrincipal.BrokerId);
                        continue;
                    case "broker":
                        field = this.PopulateValues(fieldNameTokens[1], brokerDb);
                        fieldsModel = model.BrokerFields;
                        break;
                    case "brokerAddress":
                        field = this.PopulateValues(fieldNameTokens[1], brokerDb.Address);
                        fieldsModel = model.BrokerAddress;
                        break;
                    case "PipelineView":
                        model.PipelineView = Tools.GetMainTabsSettingXmlContent(PrincipalFactory.CurrentPrincipal.ConnectionInfo, PrincipalFactory.CurrentPrincipal.UserId);
                        model.TableInfo = this.GetPipelineTable(model.PipelineView);
                        continue;
                    case "namingOptions":
                        model.NamingOptions = brokerInfoService.GetNamingOptions(PrincipalFactory.CurrentPrincipal);
                        continue;
                    case "PasswordOption":
                        model.PasswordOption = brokerInfoService.GetPasswordOptionModel();
                        continue;
                    case nameof(model.HiddenCras):
                        model.HiddenCras = brokerInfoService.GetHiddenCRAOptions(PrincipalFactory.CurrentPrincipal.BrokerId, E_ApplicationT.PriceMyLoan).Select(x => new KeyValuePair<string, string>(x.ID.ToString(), x.VendorName));

                        var craList = brokerInfoService.GetCRAOptions(PrincipalFactory.CurrentPrincipal.BrokerId);
                        var craOptions = craList.Select(x => new KeyValuePair<string, string>(x.ID.ToString(), x.VendorName));
                        var filteredCraOptions = craOptions.Except(model.HiddenCras);

                        model.CraList = new InputFieldModel(filteredCraOptions.FirstOrDefault().Key, type: InputFieldType.DropDownList, options: filteredCraOptions);
                        model.CraList.CalcActionT = E_CalculationType.NoCalculate;

                        continue;
                    case nameof(model.CraList):
                        continue;
                    case nameof(model.BranchPrefixes):
                        model.BranchPrefixes = brokerInfoService.GetBranchNamePrefix(PrincipalFactory.CurrentPrincipal.BrokerId);
                        continue;
                    default:
                        throw new CBaseException("Field is undefined: " + fieldName, "The following field does not exist or is not meant to be handled. " + fieldName);
                }

                fieldsModel.Add(fieldNameTokens[1], field);
            }

            return model;
        }

        /// <summary>
        /// Returns the value of the field contained by the BrokerDB object.
        /// </summary>
        /// <param name="fieldName">The name of the field to retrieve the value from.</param>
        /// <param name="dataObject">The object you want to retrieve the value from.</param>
        /// <returns>Returns the value of the field.</returns>
        public InputFieldModel PopulateValues(string fieldName, object dataObject)
        {
            var field = InputModelProvider.GetInputPropertyModel(dataObject, fieldName);

            if (field == null)
            {
                return field;
            }

            var textAreaFields = new string[] { "LpeSubmitAgreement", "FthbCustomDefinition", "QuickPricerNoResultMessage", "QuickPricerDisclaimerAtResult", "ECOAAddressDefault" };
            var brokerInfoService = new BrokerInfoService();

            if (textAreaFields.Contains(fieldName))
            {
                field.Type = InputFieldType.TextArea;
            }

            if (field.CalcActionT != E_CalculationType.CalculateZip)
            {
                // BrokerDB fields are not to be calculated, unless it is the zipcode field.  This is because
                // the BrokerDB is cached, and any changes to BrokerDB will remain until the user has their
                // HttpContext cleared.  Zipcode calculations don't modify the BrokerDB object, so they're
                // allowed.
                field.CalcActionT = E_CalculationType.NoCalculate;
            }

            var requireFields = new string[] { "Name", "Phone", "City", "State", "Zipcode", "StreetAddress", "FhaLenderId" };
            if (requireFields.Contains(fieldName))
            {
                field.Required = true;
            }

            var readOnlyFields = new HashSet<string> { "CustomerCode" };
            if (readOnlyFields.Contains(fieldName))
            {
                field.IsReadOnly = true;
            }

            return field;
        }

        /// <summary>
        /// Binds a dropdown list's options.
        /// </summary>
        /// <param name="field">The model of the field.</param>
        /// <param name="fieldName">The name of the field.</param>
        public void GetOptions(InputFieldModel field, string fieldName)
        {
            if (fieldName.EndsWith(InputFieldNameSuffix.State))
            {
                field.Options = EnumUtilities.GetValuesFromField("state", PrincipalFactory.CurrentPrincipal);
            }
        }

        /// <summary>
        /// Binds a dropdown list's options.
        /// </summary>
        /// <param name="field">The name of the field.</param>
        /// <param name="propertyType">The type of the field.</param>
        public void BindOptions(InputFieldModel field, Type propertyType)
        {
            if (propertyType.IsEnum)
            {
                List<KeyValuePair<string, string>> options = new List<KeyValuePair<string, string>>();
                foreach (Enum enumVal in Enum.GetValues(propertyType))
                {
                    options.Add(new KeyValuePair<string, string>(Convert.ToInt32(enumVal).ToString(), Enum.GetName(propertyType, enumVal)));
                }

                field.Options = options;
            }
        }
    }
}