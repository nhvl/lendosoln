﻿// <copyright file="LendingQBControllerConfig.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   8/14/2015
// </summary>

namespace LendersOffice.Controllers
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// A settings for LendingQB controller.
    /// </summary>
    public static class LendingQBControllerConfig
    {
        /// <summary>
        /// Store a controller name and actual type.
        /// </summary>
        private static Dictionary<string, Type> controllerDictionary = null;

        /// <summary>
        /// Initializes static members of the <see cref="LendingQBControllerConfig" /> class.
        /// </summary>
        static LendingQBControllerConfig()
        {
            controllerDictionary = new Dictionary<string, Type>(StringComparer.OrdinalIgnoreCase);

            // Register all the controllers manually here.
            controllerDictionary.Add("Loan", typeof(BasicLoanController));
            controllerDictionary.Add("Service", typeof(BasicServiceController));
            controllerDictionary.Add("Pool", typeof(TradesAndPoolsController));
            controllerDictionary.Add("Pipeline", typeof(PipelineController));
            controllerDictionary.Add("Employee", typeof(EmployeeController));
        }

        /// <summary>
        /// Get the controller.
        /// </summary>
        /// <param name="controllerName">Controller name to look in settings.</param>
        /// <returns>The controller object.</returns>
        public static AbstractLendingQBController Get(string controllerName)
        {
            Type controllerType = null;

            if (controllerDictionary.TryGetValue(controllerName, out controllerType) == false)
            {
                throw new KeyNotFoundException("Controller=[" + controllerName + "] is not found.");
            }

            return (AbstractLendingQBController)Activator.CreateInstance(controllerType);
        }
    }
}