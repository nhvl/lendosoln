// <copyright file="BasicLoanController.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   8/14/2015
// </summary>

namespace LendersOffice.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Text.RegularExpressions;
    using DataAccess;
    using DataAccess.Sellers;
    using DataAccess.Utilities;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.ObjLib.DocMagicLib;
    using LendersOffice.Rolodex;
    using LendersOffice.Security;
    using LendersOffice.Services;
    using LendersOffice.UI;

    using AgentInputType = System.Collections.Generic.Dictionary<string, System.Collections.Generic.List<System.Collections.Generic.Dictionary<string, object>>>;

    using JsonLinq = Newtonsoft.Json.Linq;

    /// <summary>
    /// Provide basic operations for loading, saving loan data object.
    /// </summary>
    public class BasicLoanController : AbstractLendingQBController
    {
        /// <summary>
        /// The suffix to detect if a string is SettlementServiceProviders.
        /// </summary>
        private const string SettlementServiceProvidersSuffix = "SettlementServiceProviders";

        /// <summary>
        /// Minimum of other income rows will return to front-end.
        /// </summary>
        private const int MinOtherIncomeRowCount = 3;

        /// <summary>
        /// List of sInitialEscrowAcc related fields.
        /// </summary>
        private static readonly string[] InitialEscrowAccFields =
                {
                "sInitialEscrowAccMIJan",
                "sInitialEscrowAccMIFeb",
                "sInitialEscrowAccMIMar",
                "sInitialEscrowAccMIApr",
                "sInitialEscrowAccMIMay",
                "sInitialEscrowAccMIJun",
                "sInitialEscrowAccMIJul",
                "sInitialEscrowAccMIAug",
                "sInitialEscrowAccMISep",
                "sInitialEscrowAccMIOct",
                "sInitialEscrowAccMINov",
                "sInitialEscrowAccMIDec"
                };

        /// <summary>
        /// List of hard code loan summary field id.
        /// </summary>
        private static readonly string[] SummaryFieldIdList =
            {
            "sLNm",
            "sStatusT",
            "sEmployeeLoanRepName",
            "sQualTopR",
            "sQualBottomR",
            "sCltvR",
            "sLtvR",
            "sHcltvR",
            "sRateLockStatusT",
            "sFinalLAmt",
            "sLT",
            "sNoteIR",
            "aCFirstNm",
            "aCMidNm",
            "aCLastNm",
            "aCSuffix"
            };

        /// <summary>
        /// List of hard code dependent field id.
        /// </summary>
        private static readonly Dictionary<string, IEnumerable<string>> DependentFields = new Dictionary<string, IEnumerable<string>>()
        {
            { "sTitleBorrowers", new List<string>() { "sTitleBorrowers" } },
            { "sSellerCollection", new List<string>() { "sSellerCollection" } },
            { "sHomeownerCounselingOrganizationCollection", new List<string>() { "sHomeownerCounselingOrganizationXmlContent" } },
            { "sDrawSchedules", new List<string>() { "sDrawSchedules", "sDrawScheduleXmlContent" } },
            { "sClosingCosts", new List<string>() { "sFeeServiceApplicationHistoryXmlContent" } },
            { "aBAliases", new List<string>() { "aBAliases" } },
            { "aCAliases", new List<string>() { "aCAliases" } },
            { "sAgentOfRole", new List<string>() { "sBrokerLockAdjustments" } },
            { "AvailableSettlementServiceProviderGroups", new List<string>() { "sAvailableSettlementServiceProvidersJson" } },
            { "sOriginalAppraisedValue", new List<string> { "sLPurposeT", "sOriginalAppraisedValue", "sFHAPurposeIsStreamlineRefiWithoutAppr" } },
            { "cocArchivesSelect", new List<string>() { "sChangeOfCircumstanceXmlContent" } },
            { "sLoanEstimateDatesInfo", new List<string> { "sLoanEstimateDatesJSONContent" } },
        };

        /// <summary>
        /// A main entry point to controller. It is a responsible of the actual controller to route to right method.
        /// </summary>
        /// <param name="op">Method to execute.</param>
        /// <returns>A <code>JSON</code> serializable object that return to client.</returns>
        public override object Main(string op)
        {
            Guid loanId = this.GetGuid("loanid", Guid.Empty);
            Guid applicantId = this.GetGuid("appid", Guid.Empty);

            switch (op)
            {
                case "Load":
                    List<string> fieldList = this.GetRequestContent<List<string>>();
                    return this.Load(loanId, applicantId, fieldList);
                case "Save":
                    DataLoanModel savingDataLoanModel = this.GetRequestContent<DataLoanModel>();
                    return this.Save(loanId, applicantId, savingDataLoanModel);
                case "Calculate":
                    DataLoanModel calDataLoanModel = this.GetRequestContent<DataLoanModel>();
                    return this.Calculate(loanId, applicantId, calDataLoanModel);
                case "CalculateZipcode":
                    var dataModel = this.GetRequestContent<DataLoanModel>();
                    var returnedString = InputModelProvider.CalculateZipcode(SerializationHelper.JsonNetSerialize(dataModel));
                    var calcModel = SerializationHelper.JsonNetDeserialize<DataLoanModel>(returnedString);
                    return this.Calculate(loanId, applicantId, calcModel);
                case "Populate":
                    var populateDataLoanModel = this.GetRequestContent<Tuple<DataLoanModel, string>>();
                    return this.PopulateFromTemplate(loanId, applicantId, populateDataLoanModel.Item1, new Guid(populateDataLoanModel.Item2));
                default:
                    throw new NotImplementedException("BasicLoanController::op=[" + op + "]");
            }
        }

        /// <summary>
        /// Parse type of a special field.
        /// </summary>
        /// <param name="fieldId">Field name.</param>
        /// <param name="fieldType">Field type output if.</param>
        /// <returns>True if the field need to be parsed special, false in otherwise.</returns>
        private static bool GetTypeOfSpecialField(string fieldId, out InputFieldType fieldType)
        {
            if ((new string[] { "aBStateMail", "aCStateMail" }).Contains(fieldId))
            {
                fieldType = InputFieldType.DropDownList;
                return true;
            }
            else if (fieldId == "sRefPurpose")
            {
                fieldType = InputFieldType.StringWithDropDown;
                return true;
            }
            else if (fieldId == "aManner")
            {
                fieldType = InputFieldType.StringWithDropDown;
                return true;
            }
            else if (fieldId == "sDwnPmtSrc")
            {
                fieldType = InputFieldType.StringWithDropDown;
                return true;
            }
            else if (fieldId == "sBrokerLockPriceRowLockedT")
            {
                fieldType = InputFieldType.Radio;
                return true;
            }
            else if (fieldId.EndsWith("County"))
            {
                fieldType = InputFieldType.StringWithDropDown;
                return true;
            }

            if (Regex.Match(fieldId, @"sOCredit\d{1,2}Desc").Success)
            {
                fieldType = InputFieldType.StringWithDropDown;
                return true;
            }

            var textAreaFields = new string[] { "sCommitRepayTermsDesc", "sCommitRepayTermsDesc", "sTexasDiscChargeVariedDesc", "sTexasDiscWillActAsFollowsDesc", "sCircumstanceChangeExplanation", "sVarRNotes" };
            if (textAreaFields.Contains(fieldId))
            {
                fieldType = InputFieldType.TextArea;
                return true;
            }

            var dropdownFields = new string[] { "s1006ProHExpDesc", "s1007ProHExpDesc", "sU3RsrvDesc", "sU4RsrvDesc" };
            if (dropdownFields.Contains(fieldId))
            {
                fieldType = InputFieldType.StringWithDropDown;
                return true;
            }

            fieldType = InputFieldType.String;
            return false;
        }

        /// <summary>
        /// Return readonly table value of Loan Data or App Data.
        /// </summary>
        /// <param name="fieldId">Field name.</param>
        /// <param name="dataLoan">Loan page Data.</param>
        /// <param name="dataApp">Application data.</param>
        /// <param name="dataValue">Values of table return on Json.</param>
        /// <param name="pageDataClassType">Type return to check if this field is on Loan Data or Appicaton Data.</param>
        /// <returns>True if the fieldId is a table value, false in otherwise.</returns>
        private static bool GetDataValue(string fieldId, CPageData dataLoan, CAppData dataApp, out string dataValue, out Type pageDataClassType)
        {
            pageDataClassType = typeof(CPageData);
            dataValue = string.Empty;

            if (fieldId == "RateLockHistory")
            {
                pageDataClassType = typeof(CPageData);
                dataValue = dataLoan.sRateLockHistoryXmlContent;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Get styles of an input model.
        /// </summary>
        /// <param name="fieldId">The field name of that input model.</param>
        /// <returns>Return type strings, return null if there is no style for that field.</returns>
        private static IEnumerable<string> GetStyleFromField(string fieldId)
        {
            if (fieldId == "sSpState" || fieldId == "sTitleBorrowers.State")
            {
                return new string[] { "form-control-state" };
            }

            return null;
        }

        /// <summary>
        /// Perform save data loan.
        /// </summary>
        /// <param name="loanId">A loan id of the file.</param>
        /// <param name="applicantId">An applicant id of the file.</param>
        /// <param name="inputDataLoan">An input loan data.</param>
        /// <returns>A new loan data.</returns>
        private DataLoanModel Save(Guid loanId, Guid applicantId, DataLoanModel inputDataLoan)
        {
            DataLoanModel dataLoanModel = new DataLoanModel();

            var fieldList = this.GetFieldListFromDataModel(inputDataLoan);

            int expectedVersion = inputDataLoan.Version;

            if (expectedVersion <= 0)
            {
                expectedVersion = ConstAppDavid.SkipVersionCheck;
            }

            CPageData dataLoan = this.CreatePageData(loanId, fieldList);
            dataLoan.InitSave(expectedVersion);

            CAppData dataApp = dataLoan.GetAppData(applicantId);

            this.BindDataModel(dataLoan, dataApp, inputDataLoan);

            dataLoan.Save();

            return this.RetrieveDataModel(dataLoan, dataApp, inputDataLoan);
        }

        /// <summary>
        /// Perform populating.
        /// </summary>
        /// <param name="loanId">A loan id of the file.</param>
        /// <param name="applicantId">An applicant id of the file.</param>
        /// <param name="inputDataLoan">An input loan data.</param>
        /// <param name="templateId">Template id to populate from.</param>
        /// <returns>A new loan data.</returns>
        private DataLoanModel PopulateFromTemplate(Guid loanId, Guid applicantId, DataLoanModel inputDataLoan, Guid templateId)
        {
            var fieldList = this.GetFieldListFromDataModel(inputDataLoan);

            CPageData dataLoan = this.CreatePageData(loanId, fieldList);
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(applicantId);

            this.BindDataModel(dataLoan, dataApp, inputDataLoan);

            CLoanProductData loanProduct = new CLoanProductData(templateId);
            loanProduct.InitLoad();

            dataLoan.sRAdj1stCapR_rep = loanProduct.lRadj1stCapR_rep;
            dataLoan.sRAdj1stCapMon_rep = loanProduct.lRadj1stCapMon_rep;
            dataLoan.sRAdjCapR_rep = loanProduct.lRAdjCapR_rep;
            dataLoan.sRAdjCapMon_rep = loanProduct.lRAdjCapMon_rep;
            dataLoan.sRAdjLifeCapR_rep = loanProduct.lRAdjLifeCapR_rep;
            dataLoan.sRAdjRoundT = loanProduct.lRAdjRoundT;
            dataLoan.sRAdjRoundToR_rep = loanProduct.lRAdjRoundToR_rep;
            dataLoan.sPmtAdjCapR_rep = loanProduct.lPmtAdjCapR_rep;
            dataLoan.sPmtAdjCapMon_rep = loanProduct.lPmtAdjCapMon_rep;
            dataLoan.sPmtAdjRecastPeriodMon_rep = loanProduct.lPmtAdjRecastPeriodMon_rep;
            dataLoan.sPmtAdjRecastStop_rep = loanProduct.lPmtAdjRecastStop_rep;
            dataLoan.sPmtAdjMaxBalPc_rep = loanProduct.lPmtAdjMaxBalPc_rep;
            dataLoan.sGradPmtYrs_rep = loanProduct.lGradPmtYrs_rep;
            dataLoan.sGradPmtR_rep = loanProduct.lGradPmtR_rep;
            dataLoan.sIOnlyMon_rep = loanProduct.lIOnlyMon_rep;
            dataLoan.sBuydwnR1_rep = loanProduct.lBuydwnR1_rep;
            dataLoan.sBuydwnMon1_rep = loanProduct.lBuydwnMon1_rep;
            dataLoan.sBuydwnR2_rep = loanProduct.lBuydwnR2_rep;
            dataLoan.sBuydwnMon2_rep = loanProduct.lBuydwnMon2_rep;
            dataLoan.sBuydwnR3_rep = loanProduct.lBuydwnR3_rep;
            dataLoan.sBuydwnMon3_rep = loanProduct.lBuydwnMon3_rep;
            dataLoan.sBuydwnR4_rep = loanProduct.lBuydwnR4_rep;
            dataLoan.sBuydwnMon4_rep = loanProduct.lBuydwnMon4_rep;
            dataLoan.sBuydwnR5_rep = loanProduct.lBuydwnR5_rep;
            dataLoan.sBuydwnMon5_rep = loanProduct.lBuydwnMon5_rep;
            dataLoan.sAprIncludesReqDeposit = loanProduct.lAprIncludesReqDeposit;
            dataLoan.sHasDemandFeature = loanProduct.lHasDemandFeature;
            dataLoan.sHasVarRFeature = loanProduct.lHasVarRFeature;
            dataLoan.sVarRNotes = loanProduct.lVarRNotes;
            dataLoan.sFilingF = loanProduct.lFilingF;
            dataLoan.sLateDays = loanProduct.lLateDays;
            dataLoan.sLateChargePc = loanProduct.lLateChargePc;
            dataLoan.sLateChargeBaseDesc = loanProduct.lLateChargeBaseDesc;
            dataLoan.sPrepmtPenaltyT = loanProduct.lPrepmtPenaltyT;
            dataLoan.sPrepmtRefundT = loanProduct.lPrepmtRefundT;
            dataLoan.sArmIndexNameVstr = loanProduct.lArmIndexNameVstr;
            dataLoan.sRAdjIndexR = loanProduct.lRAdjIndexR;
            dataLoan.sRAdjFloorAddR = loanProduct.lRAdjFloorR;
            RateAdjFloorCalcT? floorCalcT = Tools.GetRateAdjFloorCalcFromRateAdjFloorBase(loanProduct.lRAdjFloorBaseT);
            if (floorCalcT.HasValue)
            {
                dataLoan.sRAdjFloorCalcT = floorCalcT.Value;
            }

            dataLoan.sAssumeLT = loanProduct.lAssumeLT;

            if (!loanProduct.IsLpe && !dataLoan.sIsRateLocked)
            {
                var rates = loanProduct.GetRawRateSheetWithDeltas();
                if (rates != null && rates.Length > 0)
                {
                    dataLoan.sRAdjMarginR = rates[0].Margin;
                }
            }

            return this.RetrieveDataModel(dataLoan, dataApp, inputDataLoan);
        }

        /// <summary>
        /// Perform calculation.
        /// </summary>
        /// <param name="loanId">A loan id of the file.</param>
        /// <param name="applicantId">An applicant id of the file.</param>
        /// <param name="inputDataLoan">An input loan data.</param>
        /// <returns>A new loan data.</returns>
        private DataLoanModel Calculate(Guid loanId, Guid applicantId, DataLoanModel inputDataLoan)
        {
            var fieldList = this.GetFieldListFromDataModel(inputDataLoan);

            CPageData dataLoan = this.CreatePageData(loanId, fieldList);
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(applicantId);

            this.BindDataModel(dataLoan, dataApp, inputDataLoan);

            return this.RetrieveDataModel(dataLoan, dataApp, inputDataLoan);
        }

        /// <summary>
        /// Create a data loan object. This method will append a list of fields require to populate summary data.
        /// </summary>
        /// <param name="loanId">A loan id of the file.</param>
        /// <param name="fieldList">A list of fields.</param>
        /// <returns>A data loan object.</returns>
        private CPageData CreatePageData(Guid loanId, IEnumerable<string> fieldList)
        {
            List<string> masterFieldList = new List<string>(fieldList);
            masterFieldList.AddRange(SummaryFieldIdList);

            var dependentLists = fieldList.Where(x => DependentFields.ContainsKey(x)).Select(y => DependentFields[y]);

            foreach (var list in dependentLists)
            {
                masterFieldList.AddRange(list);
            }

            return new CPageData(loanId, masterFieldList);
        }

        /// <summary>
        /// Get a new data loan model from data loan object.
        /// </summary>
        /// <param name="dataLoan">A loan data.</param>
        /// <param name="dataApp">An applicant data.</param>
        /// <param name="inputDataLoan">An input loan data model.</param>
        /// <returns>A loan data model with latest value.</returns>
        private DataLoanModel RetrieveDataModel(CPageData dataLoan, CAppData dataApp, DataLoanModel inputDataLoan)
        {
            DataLoanModel returnDataLoan = new DataLoanModel();
            returnDataLoan.Version = dataLoan.sFileVersion;

            // 1/11/2007 huyn - remove cache in  field sInitialEscrowAcc
            if (dataLoan.sInitialEscrowAcc != null)
            {
                dataLoan.ClearInitialEscrowAccCache();
            }

            if (inputDataLoan.Fields != null)
            {
                foreach (var kvp in inputDataLoan.Fields)
                {
                    string fieldId = kvp.Key;

                    var ignoreFields = new string[] { "BrokerDB_IsUseLayeredFinancing", "isSubFinCreditLine", "UpdateGFEReserves" };

                    if (ignoreFields.Contains(fieldId))
                    {
                        returnDataLoan.AddField(fieldId, kvp.Value);
                        continue;
                    }
                    else if (fieldId == "cocArchivesSelect")
                    {
                        var archiveId = new Guid(inputDataLoan.Fields["cocArchivesSelect"].Value);
                        returnDataLoan.SelectedCocArchive = InputModelProvider.GetInputModel(dataLoan.sClosingCostCoCArchive.FirstOrDefault(x => x.CreatedArchiveId == archiveId));
                        returnDataLoan.AddField("cocArchivesSelect", inputDataLoan.Fields["cocArchivesSelect"]);

                        continue;
                    }
                    else if (fieldId == "MaxPayment")
                    {
                        returnDataLoan.AddField(fieldId, new InputFieldModel(this.GetMaxPayment(dataLoan), readOnly: true));
                        continue;
                    }
                    else if (fieldId == "daySinceOpened")
                    {
                        DateTime closedDate = dataLoan.sClosedD.IsValid
                            ? dataLoan.sClosedD.DateTimeForComputation
                            : DateTime.Now;
                        DateTime openDate = dataLoan.sOpenedD.IsValid
                            ? dataLoan.sOpenedD.DateTimeForComputation
                            : DateTime.Now;

                        int daySinceOpened = closedDate.Subtract(openDate).Days;
                        returnDataLoan.AddField(nameof(daySinceOpened), new InputFieldModel(daySinceOpened, readOnly: true));
                        continue;
                    }
                    else if (fieldId == "sGfeInitialDisclosureDViaHMDA")
                    {
                        returnDataLoan.AddField(fieldId, this.GetsGfeInitialDisclosureDViaHMDAModel(dataLoan));
                        continue;
                    }
                    else if (fieldId == "HasPmlEnabled")
                    {
                        var hasPmlEnabled = BrokerUserPrincipal.CurrentPrincipal.HasFeatures(E_BrokerFeatureT.PriceMyLoan);
                        returnDataLoan.AddField(fieldId, new InputFieldModel(hasPmlEnabled, readOnly: true));
                        continue;
                    }

                    if (!kvp.Value.IsValid())
                    {
                        returnDataLoan.AddField(fieldId, kvp.Value);
                        continue;
                    }

                    InputFieldModel inputModel = kvp.Value;

                    if (inputModel.Type == InputFieldType.DropDownList)
                    {
                        inputModel.Options = EnumUtilities.GetValuesFromField(fieldId, PrincipalFactory.CurrentPrincipal, dataLoan);
                    }

                    inputModel.Value = PageDataUtilities.GetValue(dataLoan, dataApp, fieldId);

                    inputModel.IsReadOnly = this.IsReadOnlyField(dataLoan, dataApp, fieldId);

                    inputModel.IsHidden = this.IsInvisibleField(dataLoan, dataApp, fieldId);

                    returnDataLoan.AddField(fieldId, inputModel);
                }
            }

            if (inputDataLoan.Documents != null)
            {
                returnDataLoan.Documents = EdocService.GetDocuments(dataLoan.sLId);
            }

            if (inputDataLoan.SettlementServiceProviders != null)
            {
                foreach (var keypair in inputDataLoan.SettlementServiceProviders)
                {
                    returnDataLoan.AddSettlementServiceProviders(keypair.Key, this.GetServiceProvidersModel(dataLoan, keypair.Key));
                }
            }

            if (inputDataLoan.AvailableSettlementServiceProviderGroups != null)
            {
                returnDataLoan.AvailableSettlementServiceProviderGroups = this.GetServiceProviderGroups(dataLoan);
            }

            if (inputDataLoan.AssignedContactsFromFees != null && !dataLoan.sIsLegacyClosingCostVersion)
            {
                returnDataLoan.AssignedContactsFromFees = dataLoan.sAssignedContactsFromFees.Select(InputModelProvider.GetInputModel);
            }

            if (inputDataLoan.AlternateLenders != null)
            {
                returnDataLoan.AlternateLenders = this.GetAlternateLenders();

                if (dataLoan.DataState != E_DataState.InitSave)
                {
                    returnDataLoan.AlternateLenders = this.GetAlternateLenders();

                    returnDataLoan.AlternateLenders.Values = inputDataLoan.AlternateLenders.Values.Select(x =>
                    {
                        string strId = x.ContainsKey("id") ? x["id"].ToString() : null;
                        var id = Guid.Empty;
                        if (!string.IsNullOrEmpty(strId))
                        {
                            Guid.TryParse(strId, out id);
                        }

                        var lender = (id == Guid.Empty)
                            ? new DocMagicAlternateLender()
                            : new DocMagicAlternateLender(BrokerUserPrincipal.CurrentPrincipal.BrokerId, id);

                        InputModelProvider.BindObjectFromInputModel(lender, x);
                        var lenderDict = InputModelProvider.GetInputModel(lender);
                        lenderDict["id"] = id.ToString();

                        return lenderDict;
                    }).ToList();
                }
            }

            if (inputDataLoan.TitleBorrowers != null)
            {
                returnDataLoan.TitleBorrowers = this.GetTitleBorrowers(dataLoan);
            }

            if (inputDataLoan.DrawSchedules != null)
            {
                returnDataLoan.DrawSchedules = this.GetDrawSchedules(dataLoan);
            }

            if (inputDataLoan.Sellers != null)
            {
                returnDataLoan.Sellers = this.GetSellerCollectionModel(dataLoan);
            }

            if (inputDataLoan.VisibleBrokerAdjustmentData != null)
            {
                returnDataLoan.VisibleBrokerAdjustmentData = InputModelProvider.GetInputModel(dataLoan.sVisibleBrokerAdjustmentData);
            }

            if (inputDataLoan.HomeownerCounselingOrganization != null)
            {
                returnDataLoan.HomeownerCounselingOrganization = this.GetHomeownerCounselingOrgs(dataLoan);
            }

            if (inputDataLoan.ClosingCostSet != null)
            {
                returnDataLoan.ClosingCostSet = ClosingCostInputHelper.ToInputModel(dataLoan.sClosingCostSet, PrincipalFactory.CurrentPrincipal);
            }

            if (inputDataLoan.EmployeeLoanAssignment != null)
            {
                returnDataLoan.EmployeeLoanAssignment = this.GetEmployeeLoanAssignment(dataLoan);
            }

            if (inputDataLoan.PmlBroker != null)
            {
                returnDataLoan.PmlBroker = this.GetPMLBroker(dataLoan);
            }

            if (inputDataLoan.AgentList != null)
            {
                returnDataLoan.AgentList = this.GetAgentList(dataLoan);
            }

            if (inputDataLoan.AgentRecord != null)
            {
                var recordId = new Guid(SerializationHelper.JsonNetDeserialize<Dictionary<string, object>>(inputDataLoan.AgentRecord["RecordId"].ToString())["v"].ToString());
                returnDataLoan.AgentRecord = this.GetAgentRecord(dataLoan, recordId);
                returnDataLoan.AgentRecord["LicenseInfoList"] = inputDataLoan.AgentRecord["LicenseInfoList"];
                returnDataLoan.AgentRecord["CompanyLicenseInfoList"] = inputDataLoan.AgentRecord["CompanyLicenseInfoList"];
            }

            if (inputDataLoan.BrokerUser != null)
            {
                returnDataLoan.BrokerUser = this.GetBrokerUser();
            }

            if (inputDataLoan.BrokerDB != null)
            {
                returnDataLoan.BrokerDB = this.GetBrokerDB(dataLoan);
            }

            if (inputDataLoan.PreparerFields != null)
            {
                returnDataLoan.PreparerFields = new Dictionary<string, Dictionary<string, object>>();
                foreach (var preparerField in inputDataLoan.PreparerFields)
                {
                    var preparerModel = this.GetPreparerFields(dataLoan, preparerField.Key);
                    if (preparerModel != null)
                    {
                        returnDataLoan.PreparerFields.Add(preparerField.Key, preparerModel);
                    }
                }
            }

            if (inputDataLoan.AgentOfRole != null)
            {
                returnDataLoan.AgentOfRole = new AgentInputType();
                foreach (var agentInput in inputDataLoan.AgentOfRole)
                {
                    var agentModel = this.GetAgentFields(dataLoan, agentInput.Key);
                    if (agentModel != null)
                    {
                        returnDataLoan.AgentOfRole.Add(agentInput.Key, agentModel);
                    }
                }
            }

            if (inputDataLoan.AgentCommission != null)
            {
                returnDataLoan.AgentCommission = this.GetAgentCommission(dataLoan);
            }

            if (inputDataLoan.ApplicantList != null)
            {
                returnDataLoan.ApplicantList = this.GetApplicantList(dataLoan);
            }

            if (inputDataLoan.InitialEscrowAcc != null)
            {
                var table = new InputFieldModel[13, 9];
                for (int i = 0; i < 13; i++)
                {
                    for (int j = 0; j < 9; j++)
                    {
                        table[i, j] = new InputFieldModel(dataLoan.sInitialEscrowAcc[i, j]);
                    }
                }

                returnDataLoan.InitialEscrowAcc = table;
            }

            if (inputDataLoan.LoanEstimateDatesInfo != null)
            {
                returnDataLoan.LoanEstimateDatesInfo = this.GetLoanEstimateDatesInfo(dataLoan);
            }

            if (inputDataLoan.ClosingDisclosureDatesInfo != null)
            {
                returnDataLoan.ClosingDisclosureDatesInfo = this.GetClosingDisclosureDatesInfo(dataLoan);
            }

            if (dataApp != null && inputDataLoan.App != null)
            {
                if (inputDataLoan.App.Fields != null)
                {
                    foreach (var kvp in inputDataLoan.App.Fields)
                    {
                        string fieldId = kvp.Key;

                        if (fieldId == "aAliasTemplate")
                        {
                            returnDataLoan.AddAppField(fieldId, kvp.Value);
                            continue;
                        }

                        InputFieldModel inputModel = kvp.Value;

                        inputModel.Value = PageDataUtilities.GetValue(dataLoan, dataApp, fieldId);

                        inputModel.IsReadOnly = this.IsReadOnlyField(dataLoan, dataApp, fieldId);

                        inputModel.IsHidden = this.IsInvisibleField(dataLoan, dataApp, fieldId);

                        returnDataLoan.AddAppField(fieldId, inputModel);
                    }
                }

                if (inputDataLoan.App.AssetCollection != null)
                {
                    returnDataLoan.App.AssetCollection = dataApp.aAssetCollection.ToInputModel();
                }

                if (inputDataLoan.App.LiaCollection != null)
                {
                    returnDataLoan.App.LiaCollection = dataApp.aLiaCollection.ToInputModel();
                }

                if (inputDataLoan.App.BEmpCollection != null)
                {
                    returnDataLoan.App.BEmpCollection = InputModelProvider.GetInputModel(dataApp.aBEmpCollection);
                }

                if (inputDataLoan.App.CEmpCollection != null)
                {
                    Tools.LogError("TODO: Bind CEmpCollection");
                }

                if (inputDataLoan.App.ReCollection != null)
                {
                    returnDataLoan.App.ReCollection = InputModelProvider.GetInputModel(dataApp.aReCollection);
                }

                if (inputDataLoan.App.OtherIncomeList != null)
                {
                    returnDataLoan.App.OtherIncomeList = this.GetOtherIncome(dataApp);
                }

                if (inputDataLoan.App.BAliases != null)
                {
                    returnDataLoan.App.BAliases = inputDataLoan.App.BAliases;
                }

                if (inputDataLoan.App.CAliases != null)
                {
                    returnDataLoan.App.CAliases = inputDataLoan.App.CAliases;
                }
            }

            this.BindLoanSummary(dataLoan, returnDataLoan);
            return returnDataLoan;
        }

        /// <summary>
        /// Bind data from data loan model to actual data loan object.
        /// </summary>
        /// <param name="dataLoan">Loan data object to set value.</param>
        /// <param name="dataApp">Applicant data object to set value.</param>
        /// <param name="inputDataLoan">Input data loan model.</param>
        private void BindDataModel(CPageData dataLoan, CAppData dataApp, DataLoanModel inputDataLoan)
        {
            if (inputDataLoan.Fields != null)
            {
                var loads1003LineLFromAdjustmentsDependencies = new HashSet<string> { "sOCredit2Amt", "sOCredit2Desc", "sOCredit3Amt", "sOCredit3Desc", "sOCredit4Amt", "sOCredit4Desc" };
                foreach (var kvp in inputDataLoan.Fields)
                {
                    string fieldId = kvp.Key;

                    var ignoreFields = new string[] { "cocArchivesSelect", "BrokerDB_IsUseLayeredFinancing", "isSubFinCreditLine", "MaxPayment", "UpdateGFEReserves", "sApr" };

                    string paymentRepeatKey = nameof(CPageData.sMIPaymentRepeat);

                    if (ignoreFields.Contains(fieldId))
                    {
                        continue;
                    }
                    else if (fieldId == "sGfeInitialDisclosureDViaHMDA")
                    {
                        if (!kvp.Value.IsReadOnly && !dataLoan.BrokerDB.IsProtectDisclosureDates)
                        {
                            dataLoan.Set_sGfeInitialDisclosureD(CDateTime.Create(kvp.Value.Value, dataLoan.m_convertLos), E_GFEArchivedReasonT.InitialDislosureDateSetViaHMDA);
                        }

                        continue;
                    }
                    else if (loads1003LineLFromAdjustmentsDependencies.Contains(fieldId) && dataLoan.sLoads1003LineLFromAdjustments)
                    {
                        continue;
                    }
                    else if (InitialEscrowAccFields.Contains(fieldId) && inputDataLoan.Fields.Keys.Contains(paymentRepeatKey))
                    {
                        E_DisbursementRepIntervalT repeatType;
                        if (E_DisbursementRepIntervalT.TryParse(inputDataLoan.Fields[paymentRepeatKey].Value, out repeatType) && repeatType != E_DisbursementRepIntervalT.Annual)
                        {
                            continue;
                        }
                    }

                    InputFieldModel inputModel = kvp.Value;

                    if (PageDataUtilities.IsBindable(fieldId) && inputModel.IsValid())
                    {
                        PageDataUtilities.SetValue(dataLoan, dataApp, fieldId, inputModel.Value);
                    }
                }

                // 1/11/2007 huyn - if initialEscrowAcc fields is updated. Trigger set method in sInitialEscrowAcc.
                if (inputDataLoan.Fields.Keys.Concat(InitialEscrowAccFields).Count() > 0)
                {
                    dataLoan.sInitialEscrowAcc = dataLoan.sInitialEscrowAcc;
                }
            }

            if (inputDataLoan.AlternateLenders != null && dataLoan.DataState == E_DataState.InitSave)
            {
                foreach (var lenderDict in inputDataLoan.AlternateLenders.Values)
                {
                    var id = lenderDict["id"];
                    if (id != null && id is string)
                    {
                        var lender = new DocMagicAlternateLender(BrokerUserPrincipal.CurrentPrincipal.BrokerId, new Guid(id as string));
                        InputModelProvider.BindObjectFromInputModel(lender, lenderDict);
                        lender.Save(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                    }
                }
            }

            if (inputDataLoan.AgentOfRole != null)
            {
                Tools.LogError("TODO: Bind AgentOfRole");
            }

            if (inputDataLoan.Documents != null)
            {
                if (inputDataLoan.Documents["documents"] != null)
                {
                    var docDicts = SerializationHelper.JsonNetDeserialize<IEnumerable<Dictionary<string, object>>>(inputDataLoan.Documents[@"documents"].ToString());
                    foreach (var docDict in docDicts)
                    {
                        EdocService.SaveDoc(docDict, dataLoan.sLId);
                    }
                }
            }

            if (inputDataLoan.SettlementServiceProviders != null)
            {
                this.BindServiceProvider(dataLoan, inputDataLoan.SettlementServiceProviders);
            }

            if (inputDataLoan.AvailableSettlementServiceProviderGroups != null)
            {
                var providerGroups = new AvailableSettlementServiceProviders();

                foreach (var group in inputDataLoan.AvailableSettlementServiceProviderGroups)
                {
                    if (!group.ContainsKey("FeeTypeId") || !group.ContainsKey("SettlementServiceProviders"))
                    {
                        continue;
                    }

                    var dataModel = SerializationHelper.JsonNetDeserialize<ListModel>(group["SettlementServiceProviders"].ToString());
                    if (dataModel == null || dataModel.Values == null)
                    {
                        continue;
                    }

                    Guid feeTypeId = new Guid(group["FeeTypeId"].ToString());
                    providerGroups.AddFeeType(feeTypeId);

                    var providers = new List<SettlementServiceProvider>();
                    foreach (var itemDict in dataModel.Values)
                    {
                        var provider = new SettlementServiceProvider();
                        InputModelProvider.BindObjectFromInputModel(provider, itemDict);
                        providers.Add(provider);
                    }

                    providerGroups.AddProvidersForFeeType(feeTypeId, providers);
                }

                dataLoan.sAvailableSettlementServiceProviders = providerGroups;
            }

            if (inputDataLoan.DrawSchedules != null)
            {
                if (inputDataLoan.DrawSchedules.ContainsKey("data"))
                {
                    var scheduleJsons = SerializationHelper.JsonNetDeserialize<List<Dictionary<string, object>>>(inputDataLoan.DrawSchedules["data"].ToString());
                    dataLoan.sDrawSchedules = scheduleJsons.Select(x =>
                    {
                        var drawSchedule = new DrawSchedule();
                        InputModelProvider.BindObjectFromInputModel(drawSchedule, x);
                        var convertLos = new LosConvert();
                        decimal basisValue = 0;
                        switch ((E_PercentBaseT)System.Convert.ToInt32(drawSchedule.Basis))
                        {
                            case E_PercentBaseT.LoanAmount:
                                basisValue = dataLoan.sLAmtCalc;
                                break;
                            case E_PercentBaseT.SalesPrice:
                                basisValue = dataLoan.sPurchPrice;
                                break;
                            case E_PercentBaseT.AppraisalValue:
                                basisValue = dataLoan.sApprVal;
                                break;
                            case E_PercentBaseT.OriginalCost:
                                basisValue = dataLoan.sLotOrigC;
                                break;
                            case E_PercentBaseT.TotalLoanAmount:
                                basisValue = dataLoan.sFinalLAmt;
                                break;
                            default:
                                throw new InvalidOperationException();
                        }

                        decimal total = ((convertLos.ToMoney(drawSchedule.Percent) / 100) * basisValue) + convertLos.ToMoney(drawSchedule.FixedAmount);
                        drawSchedule.Total = convertLos.ToMoneyString(total, FormatDirection.ToRep);
                        return drawSchedule;
                    }).ToList();
                }
            }

            if (inputDataLoan.LoanEstimateDatesInfo != null)
            {
                var json = ObsoleteSerializationHelper.JsonSerialize(LoanEstimateDatesInfo.Create());
                string estimateDatesJsonObject = "\"loanEstimateDatesList\":null";
                if (json.Contains(estimateDatesJsonObject))
                {
                    var metadata = new LoanEstimateDatesMetadata(
                        dataLoan.GetClosingCostArchiveById, 
                        dataLoan.GetConsumerDisclosureMetadataByConsumerId(), 
                        dataLoan.sLoanRescindableT);

                    var loanEstimateDates = inputDataLoan.LoanEstimateDatesInfo.Values.Select(x =>
                    {
                        var item = LoanEstimateDates.Create(metadata);
                        InputModelProvider.BindObjectFromInputModel(item, x);
                        return item;
                    });

                    var newEstimateDatesJsonObject = "\"loanEstimateDatesList\":" + ObsoleteSerializationHelper.JsonSerialize(loanEstimateDates);
                    json = json.Replace(estimateDatesJsonObject, newEstimateDatesJsonObject);
                    var updatedInfo = LoanEstimateDatesInfo.FromJson(json, metadata);

                    var updatedInitialRecords = updatedInfo.LoanEstimateDatesList.Where(x => x.IsInitial);
                    var duplicatedInitial = updatedInitialRecords.Count() > 1;

                    if (duplicatedInitial)
                    {
                        var lastInitialIndex = inputDataLoan.LoanEstimateDatesInfo.Values.FindIndex(x =>
                            {
                                var inputModelObject = x["isLastInitial"];
                                if (inputModelObject == null)
                                {
                                    return false;
                                }

                                var inputModel = SerializationHelper.JsonNetDeserialize<InputFieldModel>(inputModelObject.ToString());

                                if (inputModel == null)
                                {
                                    return false;
                                }

                                return inputModel.Value.ToNullableBool() ?? false;
                            });

                        var lastInitialId = lastInitialIndex >= 0 ? updatedInfo.LoanEstimateDatesList[lastInitialIndex].UniqueId : Guid.Empty;
                        var chosenInitialRecord = updatedInitialRecords.FirstOrDefault(x => x.UniqueId != lastInitialId);
                        foreach (var initialRecord in updatedInitialRecords)
                        {
                            if (initialRecord != chosenInitialRecord)
                            {
                                initialRecord.IsInitial = false;
                            }
                        }
                    }

                    dataLoan.sLoanEstimateDatesInfo = updatedInfo;
                }
            }

            if (inputDataLoan.ClosingDisclosureDatesInfo != null)
            {
                var json = ObsoleteSerializationHelper.JsonSerialize(ClosingDisclosureDatesInfo.Create());
                string estimateDatesJsonObject = "\"closingDisclosureDatesList\":null";
                if (json.Contains(estimateDatesJsonObject))
                {
                    var metadata = new ClosingDisclosureDatesMetadata(
                        dataLoan.GetClosingCostArchiveById,
                        dataLoan.GetConsumerDisclosureMetadataByConsumerId(),
                        dataLoan.sLoanRescindableT,
                        dataLoan.sAlwaysRequireAllBorrowersToReceiveClosingDisclosure);

                    var closingDisclosureDates = inputDataLoan.ClosingDisclosureDatesInfo.Values.Select(x =>
                    {
                        var item = ClosingDisclosureDates.Create(metadata);
                        InputModelProvider.BindObjectFromInputModel(item, x);
                        return item;
                    });

                    var newEstimateDatesJsonObject = "\"closingDisclosureDatesList\":" + ObsoleteSerializationHelper.JsonSerialize(closingDisclosureDates);
                    json = json.Replace(estimateDatesJsonObject, newEstimateDatesJsonObject);
                    dataLoan.sClosingDisclosureDatesInfo = ClosingDisclosureDatesInfo.FromJson(json, metadata);
                }
            }

            if (dataApp != null && inputDataLoan.App != null)
            {
                if (inputDataLoan.App.Fields != null)
                {
                    foreach (var kvp in inputDataLoan.App.Fields)
                    {
                        string fieldId = kvp.Key;

                        if (fieldId == "aAliasTemplate")
                        {
                            continue;
                        }

                        InputFieldModel inputModel = kvp.Value;

                        if (PageDataUtilities.IsBindable(fieldId))
                        {
                            PageDataUtilities.SetValue(dataLoan, dataApp, fieldId, inputModel.Value);
                        }
                    }
                }

                if (inputDataLoan.TitleBorrowers != null)
                {
                    var titleBorrowers = new List<TitleBorrower>();
                    foreach (Dictionary<string, object> objInputFieldModel in inputDataLoan.TitleBorrowers.Values)
                    {
                        var titleBorrower = new TitleBorrower();
                        InputModelProvider.BindObjectFromInputModel(titleBorrower, objInputFieldModel);
                        titleBorrowers.Add(titleBorrower);
                    }

                    dataLoan.sTitleBorrowers = titleBorrowers;
                }

                if (inputDataLoan.Sellers != null)
                {
                    var sellers = new List<Seller>();
                    foreach (Dictionary<string, object> objInputFieldModel in inputDataLoan.Sellers.Values)
                    {
                        var seller = new Seller();
                        InputModelProvider.BindObjectFromInputModel(seller, objInputFieldModel);
                        sellers.Add(seller);
                    }

                    var sellerColl = new SellerCollection();
                    sellerColl.ListOfSellers = sellers;

                    dataLoan.sSellerCollection = sellerColl;
                }

                if (inputDataLoan.HomeownerCounselingOrganization != null)
                {
                    var orgList = new DataAccess.HomeownerCounselingOrganizations.HomeownerCounselingOrganizationCollection();

                    orgList.AddRange(inputDataLoan.HomeownerCounselingOrganization.Values.Select(x =>
                         {
                             var counselingOrg = new DataAccess.HomeownerCounselingOrganizations.HomeownerCounselingOrganization();
                             InputModelProvider.BindObjectFromInputModel(counselingOrg, x);
                             return counselingOrg;
                         }));

                    if (dataLoan.DataState != E_DataState.InitSave)
                    {
                        dataLoan.sHomeownerCounselingOrganizationLastModifiedD = CDateTime.Create(DateTime.Today);
                    }

                    dataLoan.sHomeownerCounselingOrganizationCollection = orgList;
                }

                if (inputDataLoan.PreparerFields != null)
                {
                    foreach (var preparerFieldModel in inputDataLoan.PreparerFields)
                    {
                        E_PreparerFormT prepareForm;
                        if (E_PreparerFormT.TryParse(preparerFieldModel.Key, out prepareForm))
                        {
                            IPreparerFields preparer = dataLoan.GetPreparerOfForm(prepareForm, E_ReturnOptionIfNotExist.CreateNew);

                            InputModelProvider.BindObjectFromInputModel(preparer, preparerFieldModel.Value);
                            preparer.Update();
                        }
                    }
                }

                if (inputDataLoan.AgentOfRole != null)
                {
                    foreach (var agentInput in inputDataLoan.AgentOfRole)
                    {
                        E_AgentRoleT agentRole;
                        if (E_AgentRoleT.TryParse(agentInput.Key, out agentRole))
                        {
                            var agentFieldsList = dataLoan.GetAgentsOfRole(agentRole, E_ReturnOptionIfNotExist.CreateNew);
                            if (agentFieldsList != null)
                            {
                                foreach (var agentInputModel in agentInput.Value)
                                {
                                    if (!agentInputModel.ContainsKey(nameof(CAgentFields.RecordId)))
                                    {
                                        continue;
                                    }

                                    var agentFieldInput = SerializationHelper.JsonNetDeserialize<InputFieldModel>(agentInputModel[nameof(CAgentFields.RecordId)].ToString());

                                    if (agentFieldInput == null)
                                    {
                                        continue;
                                    }

                                    var agentFields = agentFieldsList.FirstOrDefault(x => x.RecordId == new Guid(agentFieldInput.Value));
                                    if (agentFields != null)
                                    {
                                        InputModelProvider.BindObjectFromInputModel(agentFields, agentInputModel);
                                        agentFields.Update();
                                    }
                                }
                            }
                        }
                    }
                }

                if (inputDataLoan.AgentCommission != null && inputDataLoan.AgentCommission.Values != null)
                {
                    foreach (var agentCommissionInput in inputDataLoan.AgentCommission.Values)
                    {
                        AgentCommission commission = new AgentCommission();
                        InputModelProvider.BindObjectFromInputModel(commission, agentCommissionInput);
                        var agent = dataLoan.GetAgentFields(commission.RecordId);
                        if (agent != null)
                        {
                            if (commission == null || commission.RecordId != agent.RecordId)
                            {
                                continue;
                            }

                            agent.IsCommissionPaid = commission.IsPaid;
                            agent.PaymentNotes = commission.Notes;
                        }
                    }
                }

                if (inputDataLoan.AgentRecord != null && inputDataLoan.AgentRecord.Values != null)
                {
                    var recordId = new Guid(SerializationHelper.JsonNetDeserialize<Dictionary<string, object>>(inputDataLoan.AgentRecord["RecordId"].ToString())["v"].ToString());
                    CAgentFields agent = dataLoan.GetAgentFields(recordId);
                    InputModelProvider.BindObjectFromInputModel<CAgentFields>(agent, inputDataLoan.AgentRecord);
                    agent.LicenseInfoList = this.BindLicenseList(inputDataLoan.AgentRecord["LicenseInfoList"].ToString());
                    agent.CompanyLicenseInfoList = this.BindLicenseList(inputDataLoan.AgentRecord["CompanyLicenseInfoList"].ToString());
                    if (agent != null)
                    {
                        agent.Update();
                    }
                }

                if (inputDataLoan.ApplicantList != null)
                {
                    foreach (var applicant in inputDataLoan.ApplicantList.Values)
                    {
                        var newApplicant = new RawScore();
                        InputModelProvider.BindObjectFromInputModel(newApplicant, applicant);
                        newApplicant.Update(dataLoan);
                    }
                }

                if (inputDataLoan.ClosingCostSet != null)
                {
                    dataLoan.sClosingCostSet.UpdateWith(ClosingCostInputHelper.BindInputModel(inputDataLoan.ClosingCostSet), null);
                }

                if (inputDataLoan.InitialEscrowAcc != null)
                {
                    var table = new int[13, 9];
                    for (int i = 0; i < 13; i++)
                    {
                        for (int j = 0; j < 9; j++)
                        {
                            int numberValue;
                            if (int.TryParse(inputDataLoan.InitialEscrowAcc[i, j].Value, out numberValue))
                            {
                                table[i, j] = numberValue;
                            }
                        }
                    }

                    dataLoan.sInitialEscrowAcc = table;

                    if (inputDataLoan.Fields.Keys.Contains("UpdateGFEReserves"))
                    {
                        if (inputDataLoan.Fields["UpdateGFEReserves"].Value.ToNullableBool() == true && dataLoan.AreAllVisibleGFE1000RsrvMonthsLocked())
                        {
                            dataLoan.PopulateGfeReserveWithRecommendValue();
                        }
                    }
                }

                if (inputDataLoan.App.AssetCollection != null)
                {
                    dataApp.aAssetCollection.BindFromObjectModel(inputDataLoan.App.AssetCollection);
                }

                if (inputDataLoan.App.LiaCollection != null)
                {
                    dataApp.aLiaCollection.BindFromObjectModel(inputDataLoan.App.LiaCollection);
                }

                if (inputDataLoan.App.BEmpCollection != null)
                {
                    InputModelProvider.BindObjectFromInputModel(dataApp.aBEmpCollection, inputDataLoan.App.BEmpCollection);
                }

                if (inputDataLoan.App.CEmpCollection != null)
                {
                    Tools.LogError("TODO: Bind CEmpCollection");
                }

                if (inputDataLoan.App.ReCollection != null)
                {
                    InputModelProvider.BindObjectFromInputModel(dataApp.aReCollection, inputDataLoan.App.ReCollection);
                }

                if (inputDataLoan.App.BAliases != null)
                {
                    dataApp.aBAliases = inputDataLoan.App.BAliases.Select(x => x.Value);
                }

                if (inputDataLoan.App.CAliases != null)
                {
                    dataApp.aCAliases = inputDataLoan.App.CAliases.Select(x => x.Value);
                }

                if (inputDataLoan.App.OtherIncomeList != null)
                {
                    List<OtherIncome> otherIncomes = new List<OtherIncome>();
                    foreach (var objInputFieldModel in inputDataLoan.App.OtherIncomeList.Values)
                    {
                        OtherIncome otherIncome = new OtherIncome();
                        InputModelProvider.BindObjectFromInputModel(otherIncome, objInputFieldModel);
                        otherIncomes.Add(otherIncome);
                    }

                    // Note: This will be broken for files migrated to use the new income collection.
                    // I am not updating since this code is no longer used.
                    // We should eventually delete this and all code related to it from the project.
                    dataApp.aOtherIncomeList = otherIncomes;
                }
            }
        }

        /// <summary>
        /// Traverse through the data model and generate a list of dependency field ids.
        /// </summary>
        /// <param name="inputDataLoan">The data model.</param>
        /// <returns>A list of dependency field list.</returns>
        private IEnumerable<string> GetFieldListFromDataModel(DataLoanModel inputDataLoan)
        {
            List<string> fieldList = new List<string>();

            if (inputDataLoan.Fields != null)
            {
                foreach (var key in inputDataLoan.Fields.Keys)
                {
                    fieldList.Add(key);
                }
            }

            if (inputDataLoan.AgentOfRole != null)
            {
                fieldList.Add("sfAgentOfRole");
            }

            if (inputDataLoan.InitialEscrowAcc != null)
            {
                fieldList.Add("InitialEscrowAcc");
            }

            if (inputDataLoan.PreparerOfForm != null)
            {
                fieldList.Add("sfPrepareOfForm");
            }

            if (inputDataLoan.ClosingCostSet != null)
            {
                fieldList.Add("sClosingCosts");
            }

            if (inputDataLoan.TitleBorrowers != null)
            {
                fieldList.Add("sTitleBorrowers");
            }

            if (inputDataLoan.DrawSchedules != null)
            {
                fieldList.Add("sDrawSchedules");
            }

            if (inputDataLoan.HomeownerCounselingOrganization != null)
            {
                fieldList.Add("sHomeownerCounselingOrganizationCollection");
            }

            if (inputDataLoan.Sellers != null)
            {
                fieldList.Add("sSellerCollection");
            }

            if (inputDataLoan.SettlementServiceProviders != null)
            {
                fieldList.AddRange(inputDataLoan.SettlementServiceProviders.Keys);
            }

            if (inputDataLoan.AvailableSettlementServiceProviderGroups != null)
            {
                fieldList.Add("AvailableSettlementServiceProviderGroups");
            }

            if (inputDataLoan.LoanEstimateDatesInfo != null)
            {
                fieldList.Add("sLoanEstimateDatesInfo");
            }

            if (inputDataLoan.ClosingDisclosureDatesInfo != null)
            {
                fieldList.Add("sClosingDisclosureDatesInfo");
            }

            if (inputDataLoan.App != null)
            {
                if (inputDataLoan.App.Fields != null)
                {
                    foreach (var key in inputDataLoan.App.Fields.Keys)
                    {
                        fieldList.Add(key);
                    }
                }

                if (inputDataLoan.App.AssetCollection != null)
                {
                    fieldList.Add("aAssetCollection");
                }

                if (inputDataLoan.App.LiaCollection != null)
                {
                    fieldList.Add("aLiaCollection");
                }

                if (inputDataLoan.App.BEmpCollection != null)
                {
                    fieldList.Add("aBEmpCollection");
                }

                if (inputDataLoan.App.CEmpCollection != null)
                {
                    fieldList.Add("aCEmpCollection");
                }

                if (inputDataLoan.App.ReCollection != null)
                {
                    fieldList.Add("aReCollection");
                }

                if (inputDataLoan.App.BAliases != null)
                {
                    fieldList.Add("aBAliases");
                }

                if (inputDataLoan.App.CAliases != null)
                {
                    fieldList.Add("aCAliases");
                }
            }

            return fieldList;
        }

        /// <summary>
        /// Bind the loan summary values to the input data loan model.
        /// </summary>
        /// <param name="dataLoan">The loan object.</param>
        /// <param name="inputDataLoan">The data model.</param>
        private void BindLoanSummary(CPageData dataLoan, DataLoanModel inputDataLoan)
        {
            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData dataApp = dataLoan.GetAppData(i);
                inputDataLoan.AddApplicantInfo(dataApp.aAppId, dataApp.aBNm, dataApp.aCNm, dataApp.aIsPrimary);
            }

            inputDataLoan.AddSummary("sLNm", dataLoan.sLNm);
            inputDataLoan.AddSummary("sStatusT", dataLoan.sStatusT_rep);
            inputDataLoan.AddSummary("sEmployeeLoanRepName", dataLoan.sEmployeeLoanRepName);
            inputDataLoan.AddSummary("sQualTopR", dataLoan.sQualTopR_rep);
            inputDataLoan.AddSummary("sQualBottomR", dataLoan.sQualBottomR_rep);
            inputDataLoan.AddSummary("sCltvR", dataLoan.sCltvR_rep);
            inputDataLoan.AddSummary("sLtvR", dataLoan.sLtvR_rep);
            inputDataLoan.AddSummary("sHcltvR", dataLoan.sHcltvR_rep);
            inputDataLoan.AddSummary("sRateLockStatusT", dataLoan.sRateLockStatusT_rep);
            inputDataLoan.AddSummary("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            inputDataLoan.AddSummary("sLT", dataLoan.sLT_rep);
            inputDataLoan.AddSummary("sNoteIR", dataLoan.sNoteIR_rep);
        }

        /// <summary>
        /// Load loan data with a given field list.
        /// </summary>
        /// <param name="loanId">A loan id of the file.</param>
        /// <param name="applicantId">An applicant id of the file.</param>
        /// <param name="fieldList">A list of fields.</param>
        /// <returns>An input field model.</returns>
        private DataLoanModel Load(Guid loanId, Guid applicantId, List<string> fieldList)
        {
            DataLoanModel dataLoanModel = new DataLoanModel();

            CPageData dataLoan = this.CreatePageData(loanId, fieldList);

            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(applicantId);

            dataLoanModel.Version = dataLoan.sFileVersion;

            foreach (var fieldId in fieldList)
            {
                if (fieldId == "aOtherIncomeList")
                {
                    if (dataLoanModel.App == null)
                    {
                        dataLoanModel.App = new DataAppModel();
                    }

                    dataLoanModel.App.OtherIncomeList = this.GetOtherIncome(dataApp);
                    continue;
                }
                else if (fieldId == nameof(DataLoanModel.AggregateEscrowAccount))
                {
                    dataLoanModel.AggregateEscrowAccount = InputModelProvider.GetInputModel(dataLoan.sAggregateEscrowAccount);
                    continue;
                }
                else if (fieldId == "alternateLenders")
                {
                    dataLoanModel.AlternateLenders = this.GetAlternateLenders();
                    continue;
                }
                else if (fieldId == "BrokerUser")
                {
                    dataLoanModel.BrokerUser = this.GetBrokerUser();
                    continue;
                }
                else if (fieldId == "BrokerDB")
                {
                    dataLoanModel.BrokerDB = this.GetBrokerDB(dataLoan);
                    continue;
                }
                else if (fieldId == "sSellerCollection")
                {
                    dataLoanModel.Sellers = this.GetSellerCollectionModel(dataLoan);
                    continue;
                }
                else if (fieldId == "sVisibleBrokerAdjustmentData")
                {
                    dataLoanModel.VisibleBrokerAdjustmentData = InputModelProvider.GetInputModel(dataLoan.sVisibleBrokerAdjustmentData);
                    continue;
                }
                else if (fieldId == "sDrawSchedules")
                {
                    dataLoanModel.DrawSchedules = this.GetDrawSchedules(dataLoan);
                }
                else if (fieldId == "sHomeownerCounselingOrganizationCollection")
                {
                    dataLoanModel.HomeownerCounselingOrganization = this.GetHomeownerCounselingOrgs(dataLoan);
                }
                else if (fieldId == "sClosingCosts")
                {
                    dataLoanModel.ClosingCostSet = ClosingCostInputHelper.ToInputModel(dataLoan.sClosingCostSet, PrincipalFactory.CurrentPrincipal);
                }
                else if (fieldId == "sTitleBorrowers")
                {
                    dataLoanModel.TitleBorrowers = this.GetTitleBorrowers(dataLoan);
                    continue;
                }
                else if (fieldId == nameof(DataLoanModel.Amortization))
                {
                    dataLoanModel.Amortization = new Dictionary<string, object>();

                    foreach (E_AmortizationScheduleT scheduleType in Enum.GetValues(typeof(E_AmortizationScheduleT)))
                    {
                        dataLoanModel.Amortization[scheduleType.ToString()] = dataLoan.GetAmortTable(scheduleType, false);
                    }

                    continue;
                }
                else if (fieldId.StartsWith("preparerFields."))
                {
                    var substringIndex = "preparerFields.".Length;
                    var preparerRole = fieldId.Substring(substringIndex, fieldId.Length - substringIndex);

                    var prepareModel = this.GetPreparerFields(dataLoan, preparerRole);
                    if (prepareModel != null)
                    {
                        if (dataLoanModel.PreparerFields == null)
                        {
                            dataLoanModel.PreparerFields = new Dictionary<string, Dictionary<string, object>>();
                        }

                        dataLoanModel.PreparerFields.Add(preparerRole, prepareModel);
                    }

                    continue;
                }
                else if (fieldId.StartsWith("sAgentOfRole."))
                {
                    var substringIndex = "sAgentOfRole.".Length;
                    var enumStringValue = fieldId.Substring(substringIndex, fieldId.Length - substringIndex);

                    var agentModel = this.GetAgentFields(dataLoan, enumStringValue);
                    if (agentModel != null)
                    {
                        if (dataLoanModel.AgentOfRole == null)
                        {
                            dataLoanModel.AgentOfRole = new AgentInputType();
                        }

                        dataLoanModel.AgentOfRole.Add(enumStringValue, agentModel);
                    }

                    continue;
                }
                else if (fieldId == nameof(DataLoanModel.FeeChanges))
                {
                    var feeChangeModel = new List<Dictionary<string, object>>();
                    foreach (var feeChange in dataLoan.sClosingCostCoCFeeChanges)
                    {
                        var feeChangeDict = InputModelProvider.GetInputModel(feeChange);
                        feeChangeDict.Add("isSelected", new InputFieldModel(false));
                        feeChangeModel.Add(feeChangeDict);
                    }

                    dataLoanModel.FeeChanges = feeChangeModel;
                }
                else if (fieldId == "cocArchivesSelect")
                {
                    var options = dataLoan.sClosingCostCoCArchive.Select(x => new KeyValuePair<string, string>(x.CreatedArchiveId.ToString(), x.DateArchived)).ToArray();

                    var firstValue = options.Count() > 0 ? options[0].Key : null;
                    var input = new InputFieldModel(firstValue, customInputType: InputFieldType.DropDownList, customOptions: options);

                    if (firstValue != null)
                    {
                        dataLoanModel.SelectedCocArchive = InputModelProvider.GetInputModel(dataLoan.sClosingCostCoCArchive.FirstOrDefault(x => x.CreatedArchiveId.ToString() == firstValue));
                    }

                    dataLoanModel.AddField("cocArchivesSelect", input);
                }
                else if (fieldId.EndsWith(SettlementServiceProvidersSuffix))
                {
                    dataLoanModel.AddSettlementServiceProviders(fieldId, this.GetServiceProvidersModel(dataLoan, fieldId));
                    continue;
                }
                else if (fieldId.StartsWith("AgentCommission"))
                {
                    dataLoanModel.AgentCommission = this.GetAgentCommission(dataLoan);

                    continue;
                }
                else if (fieldId.StartsWith("sDocCollection"))
                {
                    dataLoanModel.Documents = EdocService.GetDocuments(loanId);
                    continue;
                }
                else if (fieldId == "aLiaCollection")
                {
                    if (dataLoanModel.App == null)
                    {
                        dataLoanModel.App = new DataAppModel();
                    }

                    dataLoanModel.App.LiaCollection = dataApp.aLiaCollection.ToInputModel();

                    continue;
                }
                else if (fieldId == "aBAliases")
                {
                    if (dataLoanModel.App == null)
                    {
                        dataLoanModel.App = new DataAppModel();
                    }

                    dataLoanModel.App.BAliases = dataApp.aBAliases.Select(x => new InputFieldModel(x)).ToList();
                    dataLoanModel.AddAppField("aAliasTemplate", new InputFieldModel(string.Empty));
                    continue;
                }
                else if (fieldId == "aCAliases")
                {
                    if (dataLoanModel.App == null)
                    {
                        dataLoanModel.App = new DataAppModel();
                    }

                    dataLoanModel.App.CAliases = dataApp.aCAliases.Select(x => new InputFieldModel(x)).ToList();
                    dataLoanModel.AddAppField("aAliasTemplate", new InputFieldModel(string.Empty));
                    continue;
                }
                else if (fieldId == "HasPmlEnabled")
                {
                    var hasPmlEnabled = BrokerUserPrincipal.CurrentPrincipal.HasFeatures(E_BrokerFeatureT.PriceMyLoan);
                    dataLoanModel.AddField(fieldId, new InputFieldModel(hasPmlEnabled, readOnly: true));
                }
                else if (fieldId == "sApplicantList")
                {
                    dataLoanModel.ApplicantList = this.GetApplicantList(dataLoan);
                }
                else if (fieldId == "HasPmlEnabled")
                {
                    var hasPmlEnabled = BrokerUserPrincipal.CurrentPrincipal.HasFeatures(E_BrokerFeatureT.PriceMyLoan);
                    dataLoanModel.AddField(fieldId, new InputFieldModel(hasPmlEnabled));
                }
                else if (fieldId == "sLinkedLoanInfo")
                {
                    dataLoanModel.LinkedLoanInfo = this.GetLinkedLoanInfo(dataLoan);
                    continue;
                }
                else if (fieldId == "EmployeeLoanAssignment")
                {
                    dataLoanModel.EmployeeLoanAssignment = this.GetEmployeeLoanAssignment(dataLoan);
                }
                else if (fieldId == "PmlBroker")
                {
                    dataLoanModel.PmlBroker = this.GetPMLBroker(dataLoan);
                }
                else if (fieldId == "sAgentList")
                {
                    dataLoanModel.AgentList = this.GetAgentList(dataLoan);
                }
                else if (fieldId.StartsWith("AgentRecord"))
                {
                    // AgentRecord will be retrieved from AgentService/GetAgentRecord API
                    dataLoanModel.AgentRecord = new Dictionary<string, object>();
                }
                else if (fieldId == "aAssetCollection")
                {
                    if (dataLoanModel.App == null)
                    {
                        dataLoanModel.App = new DataAppModel();
                    }

                    dataLoanModel.App.AssetCollection = dataApp.aAssetCollection.ToInputModel();
                    continue;
                }
                else if (fieldId == "aBEmpCollection")
                {
                    if (dataLoanModel.App == null)
                    {
                        dataLoanModel.App = new DataAppModel();
                    }

                    dataLoanModel.App.BEmpCollection = InputModelProvider.GetInputModel(dataApp.aBEmpCollection);

                    continue;
                }
                else if (fieldId == "aReCollection")
                {
                    if (dataLoanModel.App == null)
                    {
                        dataLoanModel.App = new DataAppModel();
                    }

                    dataLoanModel.App.ReCollection = InputModelProvider.GetInputModel(dataApp.aReCollection);

                    continue;
                }
                else if (fieldId == "BrokerDB_IsUseLayeredFinancing")
                {
                    InputFieldModel layeredFinFieldModel = new InputFieldModel();
                    layeredFinFieldModel.IsReadOnly = true;
                    layeredFinFieldModel.Value = dataLoan.BrokerDB.IsUseLayeredFinancing ? "Yes" : "No";
                    dataLoanModel.AddField(fieldId, layeredFinFieldModel);
                    continue;
                }
                else if (fieldId == "isSubFinCreditLine")
                {
                    InputFieldModel isSubFinCreditLineModel = new InputFieldModel();
                    isSubFinCreditLineModel.IsReadOnly = true;
                    isSubFinCreditLineModel.Value = (dataLoan.sSubFinT == E_sSubFinT.Heloc) ? "Yes" : "No";
                    dataLoanModel.AddField(fieldId, isSubFinCreditLineModel);
                    continue;
                }
                else if (fieldId == "sGfeInitialDisclosureDViaHMDA")
                {
                    dataLoanModel.AddField(fieldId, this.GetsGfeInitialDisclosureDViaHMDAModel(dataLoan));
                    continue;
                }
                else if (fieldId == "InitialEscrowAcc")
                {
                    var table = new InputFieldModel[13, 9];
                    for (int i = 0; i < 13; i++)
                    {
                        for (int j = 0; j < 9; j++)
                        {
                            table[i, j] = new InputFieldModel(dataLoan.sInitialEscrowAcc[i, j]);
                        }
                    }

                    dataLoanModel.InitialEscrowAcc = table;
                    dataLoanModel.AddField("UpdateGFEReserves", new InputFieldModel(true, hidden: !dataLoan.AreAllVisibleGFE1000RsrvMonthsLocked()));
                    continue;
                }
                else if (fieldId == "AvailableSettlementServiceProviderGroups")
                {
                    dataLoanModel.AvailableSettlementServiceProviderGroups = this.GetServiceProviderGroups(dataLoan);
                    continue;
                }
                else if (fieldId == "MaxPayment")
                {
                    dataLoanModel.AddField(fieldId, new InputFieldModel(this.GetMaxPayment(dataLoan), readOnly: true));

                    continue;
                }
                else if (fieldId == nameof(dataLoanModel.AssignedContactsFromFees) && !dataLoan.sIsLegacyClosingCostVersion)
                {
                    dataLoanModel.AssignedContactsFromFees = dataLoan.sAssignedContactsFromFees.Select(x => InputModelProvider.GetInputModel(x));
                    continue;
                }
                else if (fieldId == "daySinceOpened")
                {
                    DateTime closedDate = dataLoan.sClosedD.IsValid ? dataLoan.sClosedD.DateTimeForComputation : DateTime.Now;
                    DateTime openDate = dataLoan.sOpenedD.IsValid ? dataLoan.sOpenedD.DateTimeForComputation : DateTime.Now;

                    int daySinceOpened = closedDate.Subtract(openDate).Days;
                    dataLoanModel.AddField(nameof(daySinceOpened), new InputFieldModel(daySinceOpened, readOnly: true));
                }
                else if (fieldId == "sLoanEstimateDatesInfo")
                {
                    dataLoanModel.LoanEstimateDatesInfo = this.GetLoanEstimateDatesInfo(dataLoan);
                }
                else if (fieldId == "sClosingDisclosureDatesInfo")
                {
                    dataLoanModel.ClosingDisclosureDatesInfo = this.GetClosingDisclosureDatesInfo(dataLoan);
                }

                if (PageDataUtilities.ContainsField(fieldId) == false)
                {
                    continue;
                }

                PageDataFieldInfo pageDataFieldInfo = PageDataUtilities.GetFieldInfo(fieldId);
                if (pageDataFieldInfo == null)
                {
                    continue;
                }

                E_PageDataClassType pageDataClassType;
                if (PageDataUtilities.GetPageDataClassType(fieldId, out pageDataClassType) == false)
                {
                    continue;
                }

                InputFieldModel fieldModel = new InputFieldModel();

                fieldModel.Styles = GetStyleFromField(fieldId);
                fieldModel.Value = PageDataUtilities.GetValue(dataLoan, dataApp, fieldId);

                fieldModel.IsReadOnly = this.IsReadOnlyField(dataLoan, dataApp, fieldId);

                fieldModel.IsHidden = this.IsInvisibleField(dataLoan, dataApp, fieldId);

                InputFieldType tempInputFieldType;
                if (GetTypeOfSpecialField(fieldId, out tempInputFieldType))
                {
                    fieldModel.Type = tempInputFieldType;
                }
                else if (pageDataFieldInfo.CustomInputModel != null && pageDataFieldInfo.CustomInputModel.Type != InputFieldType.Unknown)
                {
                    fieldModel.Type = pageDataFieldInfo.CustomInputModel.Type;
                }
                else
                {
                    fieldModel.Type = this.Convert(pageDataFieldInfo.FieldType);
                }

                if (fieldModel.Type == InputFieldType.Checkbox)
                {
                    if (PageDataUtilities.IsLockField(fieldId))
                    {
                        fieldModel.Type = InputFieldType.Lock;
                    }
                }
                else if (fieldModel.Type.HasOptionField())
                {
                    if (pageDataFieldInfo.CustomInputModel?.EnumType != null)
                    {
                        fieldModel.Options = EnumUtilities.GetValuesFromType(pageDataFieldInfo.CustomInputModel.EnumType);
                    }
                    else
                    {
                        fieldModel.Options = EnumUtilities.GetValuesFromField(fieldId, PrincipalFactory.CurrentPrincipal, dataLoan);
                    }
                }

                // Get the max length of field.
                if (pageDataFieldInfo.CustomInputModel != null && pageDataFieldInfo.CustomInputModel.MaxLength > 0)
                {
                    fieldModel.MaxLength = pageDataFieldInfo.CustomInputModel.MaxLength;
                }
                else
                {
                    var fieldInfoTable = CFieldInfoTable.GetInstance();
                    var fieldInfo = fieldInfoTable[fieldId];
                    if (fieldInfo != null)
                    {
                        if (fieldInfo.DbInfo != null && fieldInfo.DbInfo.m_charMaxLen > 0)
                        {
                            fieldModel.MaxLength = fieldInfo.DbInfo.m_charMaxLen;
                        }
                    }
                }

                // check require fields
                if (pageDataFieldInfo.CustomInputModel != null)
                {
                    fieldModel.Required = pageDataFieldInfo.CustomInputModel.Required;
                }

                if (fieldId.EndsWith(InputFieldNameSuffix.Zip))
                {
                    fieldModel.Type = InputFieldType.Zipcode;
                    fieldModel.CalcActionT = E_CalculationType.CalculateZip;
                }
                else if (fieldId.EndsWith(InputFieldNameSuffix.State))
                {
                    fieldModel.Type = InputFieldType.DropDownList;
                    fieldModel.Options = EnumUtilities.GetValuesFromField("state", PrincipalFactory.CurrentPrincipal);
                }

                if (fieldModel.IsReadOnly == false)
                {
                    // TODO: Need to do field protection check to see if need to disable this field.
                }

                if (fieldId == "sHomeownerCounselingOrganizationLastModifiedD")
                {
                    fieldModel.CalcActionT = E_CalculationType.NoCalculate;
                }

                if (pageDataClassType == E_PageDataClassType.CAppBase)
                {
                    dataLoanModel.AddAppField(fieldId, fieldModel);
                }
                else
                {
                    dataLoanModel.AddField(fieldId, fieldModel);
                }
            }

            //// Custom handling for special field should handle here.
            this.BindLoanSummary(dataLoan, dataLoanModel);
            return dataLoanModel;
        }

        /// <summary>
        /// Get info of closing disclosure dates.
        /// </summary>
        /// <param name="dataLoan">Loan data.</param>
        /// <returns>Info of closing disclosure dates.</returns>
        private ListModel GetClosingDisclosureDatesInfo(CPageData dataLoan)
        {
            var closingDisclosureMetadata = new ClosingDisclosureDatesMetadata(
                dataLoan.GetClosingCostArchiveById,
                dataLoan.GetConsumerDisclosureMetadataByConsumerId(),
                dataLoan.sLoanRescindableT,
                dataLoan.sAlwaysRequireAllBorrowersToReceiveClosingDisclosure);

            return new ListModel()
            {
                Template = InputModelProvider.GetInputModel(ClosingDisclosureDates.Create(closingDisclosureMetadata)),
                Values = dataLoan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.Select(x =>
                {
                    var validClosingCostArchives = dataLoan.sClosingCostArchive.Where(y => y.Status == Common.SerializationTypes.ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed);

                    var model = InputModelProvider.GetInputModel(x);

                    var archiveIdFieldName = nameof(ClosingDisclosureDates.ArchiveId);
                    var archiveInput = InputModelProvider.GetInputPropertyModel(x, archiveIdFieldName);

                    var archiveIdInputOptions = new List<KeyValuePair<string, string>>();
                    archiveIdInputOptions.Add(new KeyValuePair<string, string>(Guid.Empty.ToString(), "Empty"));
                    archiveIdInputOptions.AddRange(validClosingCostArchives.Select(y => new KeyValuePair<string, string>(y.Id.ToString(), y.DateArchived)));
                    archiveInput.Options = archiveIdInputOptions;

                    model[archiveIdFieldName] = archiveInput;

                    if (x.ArchiveId != Guid.Empty && !validClosingCostArchives.All(y => y.Id != x.ArchiveId))
                    {
                        var currentClosingCostArchive = dataLoan.sClosingCostArchive.FirstOrDefault(y => y.Id == x.ArchiveId);
                        archiveIdInputOptions.Add(new KeyValuePair<string, string>(currentClosingCostArchive.Id.ToString(), currentClosingCostArchive.DateArchived));
                    }

                    var invalidArchive = false;
                    if (x.ArchiveId != Guid.Empty)
                    {
                        invalidArchive = dataLoan.sClosingCostArchive.FirstOrDefault(y => y.Id == x.ArchiveId && y.Status == Common.SerializationTypes.ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed) != null;
                    }

                    model.Add(nameof(invalidArchive), new InputFieldModel(invalidArchive));

                    return model;
                }).ToList()
            };
        }

        /// <summary>
        /// Get info of loan estimate dates.
        /// </summary>
        /// <param name="dataLoan">Loan data.</param>
        /// <returns>Info of loan estimate dates.</returns>
        private ListModel GetLoanEstimateDatesInfo(CPageData dataLoan)
        {
            var validClosingCostArchives = dataLoan.sClosingCostArchive.Where(y => y.Status == Common.SerializationTypes.ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed);
            var loanEstimateMetadata = new LoanEstimateDatesMetadata(
                dataLoan.GetClosingCostArchiveById,
                dataLoan.GetConsumerDisclosureMetadataByConsumerId(),
                dataLoan.sLoanRescindableT);

            return new ListModel
            {
                Template = this.GetLoanEstimateDatesInfoField(dataLoan, LoanEstimateDates.Create(loanEstimateMetadata), validClosingCostArchives),
                Values = dataLoan.sLoanEstimateDatesInfo.LoanEstimateDatesList.Select(x => this.GetLoanEstimateDatesInfoField(dataLoan, x, validClosingCostArchives)).ToList()
            };
        }

        /// <summary>
        /// Get info of loan estimate dates.
        /// </summary>
        /// <param name="dataLoan">Loan data.</param>
        /// <param name="data">Data to be parsed.</param>
        /// <param name="validClosingCostArchives">Reuse valid closing cost.</param>
        /// <returns>Info of loan estimate dates.</returns>
        private Dictionary<string, object> GetLoanEstimateDatesInfoField(CPageData dataLoan, LoanEstimateDates data, IEnumerable<Common.SerializationTypes.ClosingCostArchive> validClosingCostArchives)
        {
            var model = InputModelProvider.GetInputModel(data);

            var archiveIdFieldName = nameof(ClosingDisclosureDates.ArchiveId);
            var archiveInput = InputModelProvider.GetInputPropertyModel(data, archiveIdFieldName);
            archiveInput.Type = InputFieldType.DropDownList;

            var archiveIdInputOptions = validClosingCostArchives.Select(y => new KeyValuePair<string, string>(y.Id.ToString(), y.DateArchived)).ToList();
            archiveIdInputOptions.Insert(0, new KeyValuePair<string, string>(Guid.Empty.ToString(), "<-- NONE -->"));

            var currentClosingCostArchive = dataLoan.sClosingCostArchive.FirstOrDefault(y => y.Id == data.ArchiveId);
            var invalidArchive = data.ArchiveId != Guid.Empty && validClosingCostArchives.All(y => y.Id != data.ArchiveId);
            model.Add(nameof(invalidArchive), new InputFieldModel(invalidArchive));

            if (invalidArchive)
            {
                archiveIdInputOptions.Add(new KeyValuePair<string, string>(
                    currentClosingCostArchive.Id.ToString(),
                    currentClosingCostArchive.DateArchived));
                var readOnlyFields = new string[] { nameof(ClosingDisclosureDates.IssuedDate), nameof(ClosingDisclosureDates.DeliveryMethod), nameof(ClosingDisclosureDates.ReceivedDate), nameof(ClosingDisclosureDates.IsInitial) };
                foreach (var fields in readOnlyFields)
                {
                    var inputModel = model[fields] as InputFieldModel;
                    if (inputModel != null)
                    {
                        inputModel.IsReadOnly = true;
                    }
                }
            }

            archiveInput.Options = archiveIdInputOptions;
            model[archiveIdFieldName] = archiveInput;

            model["isLastInitial"] = model[nameof(LoanEstimateDates.IsInitial)];

            return model;
        }

        /// <summary>
        /// Convert a page data field type to input field type.
        /// </summary>
        /// <param name="pageDataFieldType">A page data field type.</param>
        /// <returns>A input field type.</returns>
        private InputFieldType Convert(E_PageDataFieldType pageDataFieldType)
        {
            switch (pageDataFieldType)
            {
                case E_PageDataFieldType.Unknown:
                    return InputFieldType.Unknown;
                case E_PageDataFieldType.String:
                    return InputFieldType.String;
                case E_PageDataFieldType.Money:
                    return InputFieldType.Money;
                case E_PageDataFieldType.Percent:
                    return InputFieldType.Percent;
                case E_PageDataFieldType.Ssn:
                    return InputFieldType.Ssn;
                case E_PageDataFieldType.Phone:
                    return InputFieldType.Phone;
                case E_PageDataFieldType.Integer:
                    return InputFieldType.Number;
                case E_PageDataFieldType.Bool:
                    return InputFieldType.Checkbox;
                case E_PageDataFieldType.Enum:
                    return InputFieldType.DropDownList;
                case E_PageDataFieldType.DateTime:
                    return InputFieldType.Date;
                case E_PageDataFieldType.Guid:
                    return InputFieldType.String;
                case E_PageDataFieldType.Decimal:
                    return InputFieldType.Number;
                case E_PageDataFieldType.Array:
                    return InputFieldType.DropDownList;
                case E_PageDataFieldType.SensitiveString:
                    return InputFieldType.String;
                default:
                    throw new UnhandledEnumException(pageDataFieldType);
            }
        }

        /// <summary>
        /// Get input field type from other incomes of an application data.
        /// </summary>
        /// <param name="appData">Application data.</param>
        /// <returns>A list of input field model extract from other income.</returns>
        private ListModel GetOtherIncome(CAppData appData)
        {
            List<OtherIncome> otherIncomes = appData.aOtherIncomeList;

            if (otherIncomes == null)
            {
                otherIncomes = new List<OtherIncome>();
            }

            while (otherIncomes.Count < MinOtherIncomeRowCount)
            {
                otherIncomes.Add(new OtherIncome());
            }

            return new ListModel()
            {
                Template = InputModelProvider.GetInputModel(new OtherIncome()),
                Values = otherIncomes.Select(x => InputModelProvider.GetInputModel(x)).ToList()
            };
        }

        /// <summary>
        /// Get input field type from other incomes of an application data.
        /// </summary>
        /// <param name="loanData">Application data.</param>
        /// <returns>A list of input field model extract from other income.</returns>
        private ListModel GetTitleBorrowers(CPageData loanData)
        {
            var borrowersModel = new ListModel();
            var titleBorowers = loanData.sTitleBorrowers;

            borrowersModel.Values = new List<Dictionary<string, object>>();

            if (titleBorowers != null)
            {
                foreach (TitleBorrower borrower in titleBorowers)
                {
                    borrowersModel.Values.Add(InputModelProvider.GetInputModel(borrower));
                }
            }

            borrowersModel.Template = InputModelProvider.GetInputModel(new TitleBorrower());

            return borrowersModel;
        }

        /// <summary>
        /// Get input fields from draw schedules of an application data.
        /// </summary>
        /// <param name="loanData">Application data.</param>
        /// <returns>A list of input field models extract from  draw schedules.</returns>
        private Dictionary<string, object> GetDrawSchedules(CPageData loanData)
        {
            var scheduleDict = new Dictionary<string, object>();
            scheduleDict["data"] = loanData.sDrawSchedules.Select(x => InputModelProvider.GetInputModel(x));
            scheduleDict["template"] = InputModelProvider.GetInputModel(new DrawSchedule());
            return scheduleDict;
        }

        /// <summary>
        /// Get input field model of a preparer fields.
        /// </summary>
        /// <param name="loanData">Application data.</param>
        /// <param name="roleString">Role of agent.</param>
        /// <returns>A list of input field model extract from preparer fields.</returns>
        private Dictionary<string, object> GetPreparerFields(CPageData loanData, string roleString)
        {
            E_PreparerFormT preparerForm;
            if (E_PreparerFormT.TryParse(roleString, out preparerForm))
            {
                IPreparerFields interviewer = loanData.GetPreparerOfForm(preparerForm, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (interviewer != null)
                {
                    var model = InputModelProvider.GetInputModel(interviewer);

                    if (!interviewer.IsLocked)
                    {
                        foreach (var keyValPair in model)
                        {
                            if (keyValPair.Key != nameof(CPreparerFields.IsLocked) && keyValPair.Key != nameof(CPreparerFields.AgentRoleT) && keyValPair.Value is InputFieldModel)
                            {
                                (keyValPair.Value as InputFieldModel).IsReadOnly = true;
                            }
                        }
                    }

                    return model;
                }
            }

            return null;
        }

        /// <summary>
        /// Get input field model of an agent.
        /// </summary>
        /// <param name="loanData">Application data.</param>
        /// <param name="roleString">Role of agent.</param>
        /// <returns>A list of input field model extract from agents fields.</returns>
        private List<Dictionary<string, object>> GetAgentFields(CPageData loanData, string roleString)
        {
            E_AgentRoleT agentRole;
            if (E_AgentRoleT.TryParse(roleString, out agentRole))
            {
                var agentFieldsList = loanData.GetAgentsOfRole(agentRole, E_ReturnOptionIfNotExist.CreateNew);
                if (agentFieldsList != null)
                {
                    return agentFieldsList.Select(x => InputModelProvider.GetInputModel(x)).ToList();
                }
            }

            return null;
        }

        /// <summary>
        /// Get input field type from sellers of an application data.
        /// </summary>
        /// <param name="loanData">Application data.</param>
        /// <returns>A list of input field model extract from sellers.</returns>
        private ListModel GetSellerCollectionModel(CPageData loanData)
        {
            var sellerssModel = new ListModel();

            sellerssModel.Values = new List<Dictionary<string, object>>();

            foreach (Seller seller in loanData.sSellerCollection.ListOfSellers)
            {
                sellerssModel.Values.Add(InputModelProvider.GetInputModel(seller));
            }

            sellerssModel.Template = InputModelProvider.GetInputModel(new Seller());

            return sellerssModel;
        }

        /// <summary>
        /// Get list of broker user.
        /// </summary>
        /// <returns>An dictionary of broker user.</returns>
        private Dictionary<string, object> GetBrokerUser()
        {
            var brokerUser = BrokerUserPrincipal.CurrentPrincipal;
            var vendor = DocumentVendorBrokerSettings.GetDocMagicSettings(brokerUser.BrokerId);
            return brokerUser == null
                ? null
                : new Dictionary<string, object>
                {
                    { "AllowLoanAssignmentsToAnyBranch", brokerUser.HasPermission(Permission.AllowLoanAssignmentsToAnyBranch) },
                    { "HasPmlEnabled", brokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) },
                    { "documentVendorBrokerSettings", new Dictionary<string, Guid>
                        {
                            { nameof(DocumentVendorBrokerSettings.VendorId), vendor.VendorId },
                            { nameof(DocumentVendorBrokerSettings.BrokerId), vendor.BrokerId },
                        }
                    },
                    { "firstName", brokerUser.FirstName },
                    { "lastName", brokerUser.LastName },
                    { nameof(brokerUser.EmployeeId), brokerUser.EmployeeId },
                };
        }

        /// <summary>
        /// Get sLinkedLoanInfo field.
        /// </summary>
        /// <param name="loanData">The dataLoan.</param>
        /// <returns>An dictionary of LinkedLoanInfo.</returns>
        private Dictionary<string, object> GetLinkedLoanInfo(CPageData loanData)
        {
            Guid m_linkedLoanID = Guid.Empty;
            bool isLinkedLead = false;
            bool isShowLinkedLoanButtons = true;
            if (loanData.sLinkedLoanInfo.IsLoanLinked)
            {
                m_linkedLoanID = loanData.sLinkedLoanInfo.LinkedLId;

                // OPM 217312 - Determin if linked loan is lead.
                CPageData linkedLoan = new CPageData(m_linkedLoanID, new string[] { "sStatusT" });

                try
                {
                    linkedLoan.InitLoad();
                    isLinkedLead = Tools.IsStatusLead(linkedLoan.sStatusT);
                }
                catch (PageDataLoadDenied)
                {
                    // LinkedLoanButtonsPlaceHolder.Visible = false;
                    isShowLinkedLoanButtons = false;
                }
                catch (AccessDenied)
                {
                    // LinkedLoanButtonsPlaceHolder.Visible = false;
                    isShowLinkedLoanButtons = false;
                }
            }

            return new Dictionary<string, object>()
                {
                    { "LinkedLId", m_linkedLoanID },

                    // { "LinkedLNm", loanData.sLinkedLoanInfo.LinkedLNm },
                    // { "IsLoanLinked", loanData.sLinkedLoanInfo.IsLoanLinked },
                    { "IsLinkedLead", isLinkedLead },
                    { "IsShowLinkedLoanButtons", isShowLinkedLoanButtons }
                };
        }

        /// <summary>
        /// Get list of Employee Loan Assignment.
        /// </summary>
        /// <param name="loanData">Loan data.</param>
        /// <returns>An dictionary of Employee Loan Assignment.</returns>
        private ListModel GetEmployeeLoanAssignment(CPageData loanData)
        {
            var brokerUser = BrokerUserPrincipal.CurrentPrincipal;
            IEnumerable<EmployeeLoanAssignment> employeeLoanAssignment = null;

            if (brokerUser != null)
            {
                LoanAssignmentContactTable loanAssignmentContactTable = new LoanAssignmentContactTable(brokerUser.BrokerId, loanData.sLId);
                employeeLoanAssignment = loanAssignmentContactTable.Items;
            }

            return new ListModel()
            {
                Template = null,
                Values = employeeLoanAssignment.Select(x => InputModelProvider.GetInputModel(x)).ToList()
            };
        }

        /// <summary>
        /// Get list of Employee .
        /// </summary>
        /// <param name="loanData">Loan data.</param>
        /// <returns>An dictionary of Employee Loan Assignment.</returns>
        private Dictionary<string, object> GetPMLBroker(CPageData loanData)
        {
            var brokerUser = BrokerUserPrincipal.CurrentPrincipal;
            PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerByLoanId(brokerUser.BrokerId, loanData.sLId);
            if (brokerUser != null && pmlBroker != null)
            {
                return new Dictionary<string, object>()
                {
                    { "NmlsName", pmlBroker.NmlsName },
                    { "CompanyId", pmlBroker.CompanyId },
                    { "PmlBrokerId", pmlBroker.PmlBrokerId },
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get list of Agent.
        /// </summary>
        /// <param name="loanData">Loan data.</param>
        /// <returns>A list of Agents.</returns>
        private ListModel GetAgentList(CPageData loanData)
        {
            DataSet dataset = loanData.sAgentDataSet;
            var table = dataset.Tables["AgentXmlContent"];
            var data = new List<Dictionary<string, object>>();
            foreach (DataRow row in table.Rows)
            {
                var recordID = row["RecordId"].ToString();
                data.Add(new Dictionary<string, object>()
                {
                    { "RecordId", recordID },
                    { "AgentRoleDescription", RolodexDB.GetAgentType(row["AgentRoleT"].ToString(), row["OtherAgentRoleTDesc"].ToString()) },
                    { "AgentName", row["AgentName"] },
                    { "CompanyName", loanData.GetAgentFields(new Guid(recordID)).CompanyName },
                    { "Phone", row["Phone"] },
                    { "EmailAddr", row["EmailAddr"] },
                    { "IsLenderAffiliate", row["IsLenderAffiliate"] },
                    { "IsOriginatorAffiliate", row["IsOriginatorAffiliate"] }
                });
            }

            return new ListModel()
            {
                Template = null,
                Values = data
            };
        }

        /// <summary>
        /// Get specified Agent.
        /// </summary>
        /// <param name="loanData">Loan data.</param>
        /// <param name="recordId">The record ID value.</param>
        /// <returns>An Agents data model.</returns>
        private Dictionary<string, object> GetAgentRecord(CPageData loanData, Guid recordId)
        {
            return InputModelProvider.GetInputModel(loanData.sAgentCollection.GetAgentFields(recordId));
        }

        /// <summary>
        /// Calculate the licenses.
        /// </summary>
        /// <param name="jsonLicenses">The json object.</param>
        /// <returns>The license info list.</returns>
        private LicenseInfoList BindLicenseList(string jsonLicenses)
        {
            LicenseInfoList licenseInfoList = new LicenseInfoList();
            var licenses = SerializationHelper.JsonNetDeserialize<IEnumerable<Dictionary<string, object>>>(SerializationHelper.JsonNetDeserialize<Dictionary<string, object>>(jsonLicenses)["List"].ToString());
            foreach (var licenseModel in licenses)
            {
                var licenseInfo = new LicenseInfo();
                InputModelProvider.BindObjectFromInputModel(licenseInfo, licenseModel);
                if (LicenseInfoList.IsValid(licenseInfo) && !licenseInfoList.IsDuplicateLicense(licenseInfo) && !licenseInfoList.IsFull)
                {
                    licenseInfoList.Add(licenseInfo);
                }
            }

            return licenseInfoList;
        }

        /// <summary>
        /// Get input model of service providers.
        /// </summary>
        /// <param name="loanData">Application data.</param>
        /// <param name="fieldName">Service providers field name.</param>
        /// <returns>A list of input field model of service providers.</returns>
        private ListModel GetServiceProvidersModel(CPageData loanData, string fieldName)
        {
            switch (fieldName)
            {
                case "sMiscSettlementServiceProviders":
                    return this.GetLoanModel(loanData.sMiscSettlementServiceProviders);
                case "sTitleSettlementServiceProviders":
                    return this.GetLoanModel(loanData.sTitleSettlementServiceProviders);
                case "sEscrowSettlementServiceProviders":
                    return this.GetLoanModel(loanData.sEscrowSettlementServiceProviders);
                case "sSurveySettlementServiceProviders":
                    return this.GetLoanModel(loanData.sSurveySettlementServiceProviders);
                case "sPestInspectionSettlementServiceProviders":
                    return this.GetLoanModel(loanData.sPestInspectionSettlementServiceProviders);
                case "sAllSettlementServiceProviders":
                    return this.GetLoanModel(loanData.sAllSettlementServiceProviders);
                case "sAvailableSettlementServiceProviders":
                    return this.GetLoanModel(loanData.sAvailableSettlementServiceProviders.GetAllProviders());
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Bind the value of a field model to object property.
        /// </summary>
        /// <typeparam name="T">The first generic type parameter, for object type.</typeparam>
        /// <typeparam name="S">The second generic type parameter, for object property type.</typeparam>
        /// <param name="dataModel">Data model to be parsed.</param>
        /// <param name="target">The object need to be change property.</param>
        /// <param name="outExpr">The expression to identfy the object property.</param>
        private void BindObjectFromListModel<T, S>(ListModel dataModel, T target, Expression<Func<T, List<S>>> outExpr) where S : class, new()
        {
            MemberExpression expr;
            if (outExpr.Body is MemberExpression)
            {
                expr = (MemberExpression)outExpr.Body;
            }
            else
            {
                var op = ((UnaryExpression)outExpr.Body).Operand;
                expr = (MemberExpression)op;
            }

            var prop = (PropertyInfo)expr.Member;
            if (prop.SetMethod != null)
            {
                var items = new List<S>();
                foreach (var itemDict in dataModel.Values)
                {
                    var item = new S();
                    InputModelProvider.BindObjectFromInputModel(item, itemDict);
                    items.Add(item);
                }

                prop.SetValue(target, items, null);
            }
        }

        /// <summary>
        /// Bind service provider to data loan.
        /// </summary>
        /// <param name="loanData">Loan data to be updated.</param>
        /// <param name="serviceProvider">Service provider model.</param>
        private void BindServiceProvider(CPageData loanData, Dictionary<string, ListModel> serviceProvider)
        {
            foreach (var modelPair in serviceProvider)
            {
                switch (modelPair.Key)
                {
                    case "sMiscSettlementServiceProviders":
                        this.BindObjectFromListModel(modelPair.Value, loanData, x => x.sMiscSettlementServiceProviders);
                        break;
                    case "sTitleSettlementServiceProviders":
                        this.BindObjectFromListModel(modelPair.Value, loanData, x => x.sTitleSettlementServiceProviders);
                        break;
                    case "sEscrowSettlementServiceProviders":
                        this.BindObjectFromListModel(modelPair.Value, loanData, x => x.sEscrowSettlementServiceProviders);
                        break;
                    case "sSurveySettlementServiceProviders":
                        this.BindObjectFromListModel(modelPair.Value, loanData, x => x.sSurveySettlementServiceProviders);
                        break;
                    case "sPestInspectionSettlementServiceProviders":
                        this.BindObjectFromListModel(modelPair.Value, loanData, x => x.sPestInspectionSettlementServiceProviders);
                        break;
                    case "sAllSettlementServiceProviders":
                        break;
                    case "sAvailableSettlementServiceProviders":
                        if (!loanData.sIsLegacyClosingCostVersion)
                        {
                            var providers = new List<SettlementServiceProvider>();
                            foreach (var itemDict in modelPair.Value.Values)
                            {
                                var provider = new SettlementServiceProvider();
                                InputModelProvider.BindObjectFromInputModel(provider, itemDict);
                                providers.Add(provider);
                                //// TODO: update later
                            }
                        }

                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        /// <summary>
        /// Get list model from a list.
        /// </summary>
        /// <typeparam name="T">The first generic type parameter, for object type.</typeparam>
        /// <param name="list">List of data to be extracted list model.</param>
        /// <returns>A list model converted from the input list.</returns>
        private ListModel GetLoanModel<T>(IEnumerable<T> list) where T : class, new()
        {
            if (list == null)
            {
                return null;
            }

            return new ListModel()
            {
                Template = InputModelProvider.GetInputModel(new T()),
                Values = list.Select(x => InputModelProvider.GetInputModel(x)).ToList()
            };
        }

        /// <summary>
        /// Get input field of agent commission.
        /// </summary>
        /// <param name="loanData">Application data.</param>
        /// <returns>A list of input field of agent commission data.</returns>
        private ListModel GetAgentCommission(CPageData loanData)
        {
            IEnumerable<AgentCommission> commissions;
            if (loanData.sAgentCollection == null)
            {
                commissions = new List<AgentCommission>();
            }
            else
            {
                var commissionAgents = loanData.sAgentCollection.GetAllAgents().Where(x => x != null && x.IsValid && x.Commission != 0);
                commissions = commissionAgents.Select(x => new AgentCommission(x));
            }

            return new ListModel()
            {
                Template = InputModelProvider.GetInputModel(new AgentCommission()),
                Values = commissions.Select(InputModelProvider.GetInputModel).ToList()
            };
        }

        /// <summary>
        /// Get list of broker DB.
        /// </summary>
        /// <param name="loanData">Application data.</param>
        /// <returns>An dictionary of broker DB.</returns>
        private Dictionary<string, object> GetBrokerDB(CPageData loanData)
        {
            var brokerUser = BrokerUserPrincipal.CurrentPrincipal;

            if (brokerUser == null)
            {
                return null;
            }

            var brokerDB = loanData.BrokerDB;
            return new Dictionary<string, object>
            {
                { "EnabledStatusesByChannel", new
                    {
                        Retail = brokerDB.GetEnabledStatusesByChannel(E_BranchChannelT.Retail),
                        Wholesale = brokerDB.GetEnabledStatusesByChannel(E_BranchChannelT.Wholesale),
                        Blank = brokerDB.GetEnabledStatusesByChannel(E_BranchChannelT.Blank),
                        Broker = brokerDB.GetEnabledStatusesByChannel(E_BranchChannelT.Broker),
                        Correspondent = new
                        {
                            Blank = brokerDB.GetEnabledStatusesByChannelAndProcessType(E_BranchChannelT.Correspondent, E_sCorrespondentProcessT.Blank),
                            PriorApproved = brokerDB.GetEnabledStatusesByChannelAndProcessType(E_BranchChannelT.Correspondent, E_sCorrespondentProcessT.PriorApproved),
                            MiniCorrespondent = brokerDB.GetEnabledStatusesByChannelAndProcessType(E_BranchChannelT.Correspondent, E_sCorrespondentProcessT.MiniCorrespondent),
                            Bulk = brokerDB.GetEnabledStatusesByChannelAndProcessType(E_BranchChannelT.Correspondent, E_sCorrespondentProcessT.Bulk),
                            MiniBulk = brokerDB.GetEnabledStatusesByChannelAndProcessType(E_BranchChannelT.Correspondent, E_sCorrespondentProcessT.MiniBulk),
                        }
                    }
                },
                { "EnableTeamsUI", brokerDB.EnableTeamsUI },
                { "HasManyUsers", brokerDB.HasManyUsers },
                { "CopyInternalUsersToOfficalContactsByDefault", brokerDB.CopyInternalUsersToOfficalContactsByDefault },
                { "EnableLenderCreditsWhenInLegacyClosingCostMode", brokerDB.EnableLenderCreditsWhenInLegacyClosingCostMode },
                { nameof(brokerDB.IsEnableGfe2015), brokerDB.IsEnableGfe2015 },
                { nameof(brokerDB.IsUseLayeredFinancing), brokerDB.IsUseLayeredFinancing },
                { nameof(brokerDB.IsUseNewNonPurchaseSpouseFeature), brokerDB.IsUseNewNonPurchaseSpouseFeature },
                { nameof(brokerDB.CalculateclosingCostInPML), brokerDB.CalculateclosingCostInPML },
                { nameof(brokerDB.HasLenderDefaultFeatures), brokerDB.HasLenderDefaultFeatures },
                { nameof(brokerDB.IsDisplayChoiceUfmipInLtvCalc), brokerDB.IsDisplayChoiceUfmipInLtvCalc },
                { nameof(brokerDB.ShowCRMID), brokerDB.ShowCRMID },
            };
        }

        /// <summary>
        /// Get list of alternate lender.
        /// </summary>
        /// <returns>An list of alternate lender.</returns>
        private ListModel GetAlternateLenders()
        {
            var alternateLenders = new List<Dictionary<string, object>>();
            var alternateNameAndIds = DocMagicAlternateLender.GetAlternateLenderListByBroker(BrokerUserPrincipal.CurrentPrincipal.BrokerId);

            foreach (var alternateNameAndId in alternateNameAndIds)
            {
                var id = alternateNameAndId.Item2;
                DocMagicAlternateLender altLender = id != Guid.Empty ? new DocMagicAlternateLender(BrokerUserPrincipal.CurrentPrincipal.BrokerId, id) : new DocMagicAlternateLender();
                var lenderDict = InputModelProvider.GetInputModel(altLender);
                lenderDict["id"] = id;
                alternateLenders.Add(lenderDict);
            }

            return new ListModel()
            {
                Template = InputModelProvider.GetInputModel(new DocMagicAlternateLender()),
                Values = alternateLenders
            };
        }

        /// <summary>
        /// Get input field of home owner counseling data.
        /// </summary>
        /// <param name="loanData">Application data.</param>
        /// <returns>A list of input field of home owner counseling data.</returns>
        private ListModel GetHomeownerCounselingOrgs(CPageData loanData)
        {
            var counselingList = loanData.sHomeownerCounselingOrganizationCollection as List<DataAccess.HomeownerCounselingOrganizations.HomeownerCounselingOrganization>;
            var counselingModel = new ListModel
            {
                Values = counselingList.Select(counseling => InputModelProvider.GetInputModel(counseling)).ToList(),
                Template = InputModelProvider.GetInputModel(new DataAccess.HomeownerCounselingOrganizations.HomeownerCounselingOrganization()),
            };

            return counselingModel;
        }

        /// <summary>
        /// Get list of applicants.
        /// </summary>
        /// <param name="loanData">Application data.</param>
        /// <returns>An dictionary of applicants.</returns>
        private ListModel GetApplicantList(CPageData loanData)
        {
            List<RawScore> rawScores = new List<RawScore>();
            for (var i = 0; i < loanData.nApps; i++)
            {
                rawScores.Add(new RawScore(loanData.GetAppData(i)));
            }

            return new ListModel()
            {
                Template = null,
                Values = rawScores.Select(x => InputModelProvider.GetInputModel(x)).ToList()
            };
        }

        /// <summary>
        /// Get the input model of sGfeInitialDisclosureDViaHMDA.
        /// </summary>
        /// <param name="loanData">The loan data.</param>
        /// <returns>The sGfeInitialDisclosureDViaHMDA input model.</returns>
        private InputFieldModel GetsGfeInitialDisclosureDViaHMDAModel(CPageData loanData)
        {
            return new InputFieldModel(loanData.sGfeInitialDisclosureD)
            {
                Type = InputFieldType.Date,
                IsReadOnly = loanData.BrokerDB.IsProtectDisclosureDates,
            };
        }

        /// <summary>
        /// Get list of service provider grouped by fee type.
        /// </summary>
        /// <param name="loanData">Application data.</param>
        /// <returns>A list of service provider grouped by fee type.</returns>
        private IEnumerable<Dictionary<string, object>> GetServiceProviderGroups(CPageData loanData)
        {
            var providerGroups = new List<Dictionary<string, object>>();
            foreach (var feeTypeId in loanData.sAvailableSettlementServiceProviders.GetFeeTypeIds())
            {
                BorrowerClosingCostFee fee = (BorrowerClosingCostFee)loanData.sClosingCostSet.FindFeeByTypeId(feeTypeId);

                var feeSection = new Dictionary<string, object>();

                feeSection["FeeTotalAmount"] = fee.TotalAmount;
                feeSection["FeeTypeId"] = feeTypeId;
                feeSection["FeeDescription"] = fee.Description;
                feeSection["IsTitleFee"] = fee.IsTitleFee;
                feeSection["SettlementServiceProviders"] = this.GetLoanModel(loanData.sAvailableSettlementServiceProviders.GetProvidersForFeeTypeId(feeTypeId));

                providerGroups.Add(feeSection);
            }

            return providerGroups;
        }

        /// <summary>
        /// Get the highest payment could increase to.
        /// </summary>
        /// <param name="loanData">Application data.</param>
        /// <returns>The highest payment could increase to.</returns>
        private decimal GetMaxPayment(CPageData loanData)
        {
            if (!loanData.sHasVarRFeature)
            {
                return 0;
            }

            var worstCaseAmort = loanData.GetAmortTable(E_AmortizationScheduleT.WorstCase, true);

            decimal maxPayment = 0.0M;

            for (int i = 0; i < worstCaseAmort.nRows; i++)
            {
                maxPayment = Math.Max(maxPayment, worstCaseAmort.Items[i].Pmt);
            }

            return maxPayment;
        }

        /// <summary>
        /// Check if a fieldId is a readonly field.
        /// </summary>
        /// <param name="loanData">Loan page Data.</param>
        /// <param name="appData">Application data.</param>
        /// <param name="fieldId">Field name.</param>
        /// <returns>A value detecting whether a fieldId is a readonly field.</returns>
        private bool IsReadOnlyField(CPageData loanData, CAppData appData, string fieldId)
        {
            var brokerDb = PrincipalFactory.CurrentPrincipal.BrokerDB;

            if ((new string[] { nameof(CPageData.sSoftPrepmtPeriodMonths), nameof(CPageData.sPrepmtPeriodMonths) }).Contains(fieldId))
            {
                return loanData.sPrepmtPenaltyT != E_sPrepmtPenaltyT.May;
            }
            else if ((new string[] { nameof(CPageData.sOptionArmMinPayOptionT),  nameof(CPageData.sOptionArmMinPayPeriod), nameof(CPageData.sOptionArmMinPayIsIOOnly), nameof(CPageData.sOptionArmIntroductoryPeriod), nameof(CPageData.sOptionArmInitialFixMinPmtPeriod), nameof(CPageData.sIsFullAmortAfterRecast) }).Contains(fieldId))
            {
                return !loanData.sIsOptionArm;
            }
            else if (fieldId == nameof(CPageData.sOptionArmPmtDiscount))
            {
                return !loanData.sIsOptionArm || (loanData.sIsOptionArm && loanData.sOptionArmMinPayOptionT != E_sOptionArmMinPayOptionT.ByDiscountPmt);
            }
            else if (fieldId == nameof(CPageData.sOptionArmNoteIRDiscount))
            {
                return !loanData.sIsOptionArm || (loanData.sIsOptionArm && loanData.sOptionArmMinPayOptionT != E_sOptionArmMinPayOptionT.ByDiscountNoteIR);
            }
            else if (fieldId == nameof(CPageData.sOptionArmTeaserR))
            {
                return !loanData.sIsOptionArm || loanData.sIsRateLocked;
            }
            else if (fieldId == nameof(CPageData.sFinMethPrintAsOtherDesc))
            {
                return !loanData.sFinMethodPrintAsOther;
            }
            else if (fieldId == nameof(CPageData.sUfCashPdLckd))
            {
                return loanData.sFfUfMipIsBeingFinanced == false || loanData.sLT == E_sLT.FHA;
            }
            else if ((new string[] { nameof(CAppData.aBAddrMail), nameof(CAppData.aBCityMail), nameof(CAppData.aBStateMail), nameof(CAppData.aBZipMail) }).Contains(fieldId))
            {
                return appData.aBAddrMailSourceT != E_aAddrMailSourceT.Other;
            }
            else if ((new string[] { nameof(CAppData.aCAddrMail), nameof(CAppData.aCCityMail), nameof(CAppData.aCStateMail), nameof(CAppData.aCZipMail) }).Contains(fieldId))
            {
                return appData.aCAddrMailSourceT != E_aAddrMailSourceT.Other;
            }
            else if ((new string[] { nameof(CPageData.sRLckdD), nameof(CPageData.sRLckdExpiredD) }).Contains(fieldId))
            {
                return loanData.sIsRateLocked || PrincipalFactory.CurrentPrincipal.BrokerDB.HasFeatures(E_BrokerFeatureT.PricingEngine) || PrincipalFactory.CurrentPrincipal.BrokerDB.HasLenderDefaultFeatures;
            }
            else if (fieldId == nameof(CPageData.sSubmitD))
            {
                return loanData.sIsRateLocked;
            }
            else if (fieldId == nameof(CPageData.sRLckdNumOfDays))
            {
                return brokerDb.HasLenderDefaultFeatures;
            }
            else if (fieldId == nameof(CPageData.sMInsRsrvEscrowedTri))
            {
                return loanData.sIssMInsRsrvEscrowedTriReadOnly;
            }
            else if (fieldId == nameof(CPageData.sRAdjFloorR))
            {
                return loanData.sIsRAdjFloorRReadOnly;
            }
            else
            {
                return PageDataUtilities.IsReadOnly(loanData, appData, fieldId);
            }
        }

        /// <summary>
        /// Check if a fieldId is a invisible field.
        /// </summary>
        /// <param name="loanData">Loan page Data.</param>
        /// <param name="appData">Application data.</param>
        /// <param name="fieldId">Field name.</param>
        /// <returns>A value detecting whether a fieldId is a invisible field.</returns>
        private bool IsInvisibleField(CPageData loanData, CAppData appData, string fieldId)
        {
            var brokerDb = PrincipalFactory.CurrentPrincipal.BrokerDB;
            if (fieldId == nameof(CPageData.sAggEscrowCalcModeT))
            {
                return !loanData.sIsHousingExpenseMigrated;
            }
            else if (fieldId == nameof(CPageData.sCustomaryEscrowImpoundsCalcMinT))
            {
                return !loanData.sIsHousingExpenseMigrated || !brokerDb.EnableCustomaryEscrowImpoundsCalculation;
            }
            else
            {
                return PageDataUtilities.IsInvisible(loanData, appData, fieldId);
            }
        }
    }
}
