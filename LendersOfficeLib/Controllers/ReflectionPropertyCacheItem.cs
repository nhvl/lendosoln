﻿// <copyright file="ReflectionPropertyCacheItem.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Eduardo Michel
//  Date:   6/30/2016
// </summary>

namespace LendersOffice.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using LendersOffice.UI;

    /// <summary>
    /// This object encapsulates all the needed information for constructing an InputFieldModel.  It's meant to be cached to avoid unecessary Reflection and 
    /// Compile Expression calls.
    /// </summary>
    public class ReflectionPropertyCacheItem
    {
        /// <summary>
        /// Gets or sets the Type of the field.
        /// </summary>
        /// <value>The Type of the field.</value>
        public Type ValueType { get; set; }

        /// <summary>
        /// Gets or sets the delegate for the property's Get method.
        /// </summary>
        /// <value>The delegate for the property's Get method.</value>
        public Func<object, object> Getter { get; set; }

        /// <summary>
        /// Gets or sets the delegate for the property's Set method.
        /// </summary>
        /// <value>The delegate for the property's Set method.</value>
        public Action<object, object> Setter { get; set; }

        /// <summary>
        /// Gets or sets the field's Calculation Type.
        /// </summary>
        /// <value>The field's Calculation Type.</value>
        public E_CalculationType CalcType { get; set; }

        /// <summary>
        /// Gets or sets the field's Field Type.
        /// </summary>
        /// <value>The fields InputfieldType.</value>
        public InputFieldType InputFieldType { get; set; }
    }
}
