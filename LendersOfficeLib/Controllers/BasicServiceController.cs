﻿// <copyright file="BasicServiceController.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Huy Nguyen
//  Date:   5/18/2016
// </summary>

namespace LendersOffice.Controllers
{
    using System;
    using System.IO;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LendersOffice.Services;

    /// <summary>
    /// Provide basic operations for loading, saving loan data object.
    /// </summary>
    public class BasicServiceController : AbstractLendingQBController
    {
        /// <summary>
        /// Provide basic operations for loan services.
        /// </summary>
        /// <param name="op">Method string from url.</param>
        /// <returns>Return object for webservice.</returns>
        public override object Main(string op)
        {
            string[] nodes = op.Split('/');
            if (nodes.Length < 2)
            {
                throw new NotImplementedException("BasicServiceController::op=[" + op + "]");
            }

            using (StreamReader streamReader = new StreamReader(this.HttpContext.Request.InputStream))
            {
                var parameterDict = SerializationHelper.JsonSerializeForServiceController(streamReader);
                return LendingQBServiceHelper.InvokeService(nodes[0], nodes[1], parameterDict, PrincipalFactory.CurrentPrincipal);
            }
        }
    }
}
