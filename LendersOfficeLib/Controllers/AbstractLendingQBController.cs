﻿// <copyright file="AbstractLendingQBController.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: David Dao
//  Date:   8/14/2015
// </summary>

namespace LendersOffice.Controllers
{
    using System;
    using System.IO;
    using System.Web;
    using LendersOffice.Common;
    using Newtonsoft.Json;

    /// <summary>
    /// A generic LendingQB Controller class.
    /// </summary>
    public abstract class AbstractLendingQBController
    {
        /// <summary>
        /// Gets the HTTP context of this controller.
        /// </summary>
        /// <value>The HTTP context of this controller.</value>
        protected HttpContext HttpContext { get; private set; }

        /// <summary>
        /// A main entry point to controller. It is a responsible of the actual controller to route to right method.
        /// </summary>
        /// <param name="op">Method to execute.</param>
        /// <returns>A <code>JSON</code> serializable object that return to client.</returns>
        public abstract object Main(string op);

        /// <summary>
        /// Perform initialize. This is call by LendingQBController class.
        /// </summary>
        /// <param name="context">The HTTP context to process.</param>
        internal void Initialize(HttpContext context)
        {
            this.HttpContext = context;
        }

        /// <summary>
        /// Deserialize the request.
        /// </summary>
        /// <typeparam name="T">The type of object to return.</typeparam>
        /// <returns>The input object from request.</returns>
        protected T GetRequestContent<T>()
        {
            using (StreamReader streamReader = new StreamReader(this.HttpContext.Request.InputStream))
            {
                using (JsonReader reader = new JsonTextReader(streamReader))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.DefaultValueHandling = DefaultValueHandling.Ignore;

                    return serializer.Deserialize<T>(reader);
                }
            }
        }

        /// <summary>
        /// Gets the guid value from query string variable.
        /// </summary>
        /// <param name="name">Name of the query string.</param>
        /// <param name="defaultValue">Default value when it is not found.</param>
        /// <returns>A guid value.</returns>
        protected Guid GetGuid(string name, Guid defaultValue)
        {
            return RequestHelper.GetGuid(name, defaultValue);
        }
    }
}