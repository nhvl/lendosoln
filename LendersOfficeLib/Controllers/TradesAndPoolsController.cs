﻿// <copyright file="TradesAndPoolsController.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Eduardo Michel
//  Date:   6/10/2016
// </summary>

namespace LendersOffice.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.MortgagePool;
    using LendersOffice.Security;
    using LendersOffice.UI;

    /// <summary>
    /// This controller class is to faciliate loading and saving the Trades and Pools pages in the new UI.
    /// </summary>
    public class TradesAndPoolsController : ReflectionLendingQBController
    {
        /// <summary>
        /// This is the list of fields that will appear in the Pool Summary page.
        /// </summary>
        private List<string> summaryFields = new List<string>()
        {
            "PoolNumberByAgency",
            "InternalId",
            "AgencyT_rep",
            "AmortizationT_rep",
            "SecurityR_rep",
            "LoanCount",
            "TotalCurrentBalance_rep",
            "OpenBalance_rep"
        };

        /// <summary>
        /// The main method.  It processes the op parameter and determines what method to call.
        /// </summary>
        /// <param name="op">The op should be the action that you're trying to perform.</param>
        /// <returns>Returns the serialized object.</returns>
        public override object Main(string op)
        {
            long poolId = RequestHelper.GetLong("poolId", -1L);

            if (op == "Load")
            {
                List<string> fieldList = this.GetRequestContent<List<string>>();
                return this.LoadPool(poolId, PrincipalFactory.CurrentPrincipal.BrokerId, fieldList);
            }
            else if (op == "Save")
            {
                PoolModel model = this.GetRequestContent<PoolModel>();
                return this.Save(poolId, model);
            }
            else if (op == "Calculate")
            {
                PoolModel model = this.GetRequestContent<PoolModel>();
                return this.Calculate(poolId, model);
            }
            else if (op == "CalculateZipcode")
            {
                PoolModel model = this.GetRequestContent<PoolModel>();

                var returnedString = InputModelProvider.CalculateZipcode(SerializationHelper.JsonNetSerialize(model));
                return SerializationHelper.JsonNetDeserialize<PoolModel>(returnedString);
            }
            else
            {
                throw new NotImplementedException("Unhandled operation. " + op + " does nto have a corresponding method.");
            }
        }

        /// <summary>
        /// Calculates the pool fields.
        /// </summary>
        /// <param name="poolId">The id of the pool to be saved.</param>
        /// <param name="poolModel">The model sent by the client page.</param>
        /// <returns>An updated version of the pool model.</returns>
        public PoolModel Calculate(long poolId, PoolModel poolModel)
        {
            MortgagePool pool = new MortgagePool(poolId);

            this.UpdatePool(pool, poolModel); 

            return this.BindPool(pool, poolModel.Fields.Keys.ToList());
        }

        /// <summary>
        /// Saves the pool fields.
        /// </summary>
        /// <param name="poolId">The id of the pool to be saved.</param>
        /// <param name="poolModel">The model sent by the client page.</param>
        /// <returns>An updated version of the pool model.</returns>
        public PoolModel Save(long poolId, PoolModel poolModel)
        {
            MortgagePool pool = new MortgagePool(poolId);

            this.UpdatePool(pool, poolModel); 

            pool.SaveData();

            return this.BindPool(pool, poolModel.Fields.Keys.ToList());
        }

        /// <summary>
        /// Updates a MortgagePool object with the data in the poolModel.
        /// </summary>
        /// <param name="pool">The MortagePool to update.</param>
        /// <param name="poolModel">The PoolModel to draw the data from.</param>
        public void UpdatePool(MortgagePool pool, PoolModel poolModel)
        {
            if (poolModel.FeatureCodes != null)
            {
                pool.FeatureCodesJSON = SerializationHelper.JsonNetSerialize(poolModel.FeatureCodes);
            }

            if (poolModel.SettlementAccounts != null)
            {
                for (int i = 0; i < poolModel.SettlementAccounts.Count; i++)
                {
                    InputModelProvider.BindObjectFromInputModel(pool.SettlementAccounts[i], poolModel.SettlementAccounts[i]);
                }
            }

            foreach (string key in poolModel.Fields.Keys)
            {
                PropertyInfo property = this.GetRepFieldVersion(key, pool);
                if (property == null || property.GetSetMethod() == null)
                {
                    property = pool.GetType().GetProperty(key);
                }

                this.SetValue(pool, key, poolModel.Fields[key].Value);
            }
        }

        /// <summary>
        /// Loads the Pool object.
        /// </summary>
        /// <param name="poolId">The pool id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="fieldList">The list of fields to be retrived from the pool.</param>
        /// <returns>REturns the newly loaded object.</returns>
        public PoolModel LoadPool(long poolId, Guid brokerId, List<string> fieldList)
        {
            MortgagePool pool = new MortgagePool(poolId);
            PoolModel model = this.BindPool(pool, fieldList);

            return model;
        }

        /// <summary>
        /// Returns a pool model based on the given pool and fieldList.
        /// </summary>
        /// <param name="pool">The pool to draw the data from.</param>
        /// <param name="fieldList">The list of fields to retrieve.</param>
        /// <returns>An updated model of the pool.</returns>
        public PoolModel BindPool(MortgagePool pool, List<string> fieldList)
        {
            PoolModel model = new PoolModel();

            foreach (string fieldName in fieldList)
            {
                InputFieldModel field = new InputFieldModel();

                if (fieldName.Equals("FeatureCodes"))
                {
                    model.FeatureCodes = pool.FeatureCodes;

                    for (int i = model.FeatureCodes.Count; i < 3; i++)
                    {
                        model.FeatureCodes.Add(string.Empty);
                    }

                    continue;
                }

                if (fieldName.Equals("SettlementAccounts"))
                {
                    List<Dictionary<string, object>> accounts = new List<Dictionary<string, object>>();

                    foreach (MortgagePool.SettlementAccount account in pool.SettlementAccounts)
                    {
                        accounts.Add(InputModelProvider.GetInputModel(account));
                    }

                    model.SettlementAccounts = accounts;

                    continue;
                }

                var property = pool.GetType().GetProperty(fieldName);
                field.Value = this.GetValue(property, pool);
                field.Type = TradesAndPoolsController.GetInputType(property.PropertyType, fieldName);

                if (field.Type.Equals(InputFieldType.DropDownList))
                {
                    this.BindOptions(field, property.PropertyType);
                }

                field.IsReadOnly = TradesAndPoolsController.CheckIsReadOnly(pool, fieldName);
                field.IsHidden = false;
                field.CalcActionT = TradesAndPoolsController.GetCalculationType(property.GetType(), fieldName);
                model.Fields.Add(fieldName, field);
            }

            this.LoadSummary(pool, model);
            return model;
        }

        /// <summary>
        /// This loads the values for the Pool Summary into the Pool Model.
        /// </summary>
        /// <param name="pool">The pool to draw the data from.</param>
        /// <param name="model">The model to laod the data into.</param>
        public void LoadSummary(MortgagePool pool, PoolModel model)
        {
            foreach (string fieldName in this.summaryFields)
            {
                var propertyInfo = pool.GetType().GetProperty(fieldName);
                model.AddSummaryField(fieldName, this.GetValue(propertyInfo, pool));
            }
        }

        /// <summary>
        /// Binds a dropdown list's options.
        /// </summary>
        /// <param name="field">The name of the field.</param>
        /// <param name="propertyType">The type of the field.</param>
        public void BindOptions(InputFieldModel field, Type propertyType)
        {
            if (propertyType.IsEnum)
            {
                List<KeyValuePair<string, string>> options = new List<KeyValuePair<string, string>>();
                foreach (Enum enumVal in Enum.GetValues(propertyType))
                {
                    if (propertyType.Name.Equals("E_MortgagePoolAgencyT") && enumVal.Equals(E_MortgagePoolAgencyT.LeaveBlank))
                    {
                        continue;
                    }

                    options.Add(new KeyValuePair<string, string>(Convert.ToInt32(enumVal).ToString(), Enum.GetName(propertyType, enumVal)));
                }

                field.Options = options;
            }
        }
    }
}