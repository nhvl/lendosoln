﻿// <copyright file="TableInfo.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Eduardo Michel
//  Date:   1/19/2016
// </summary>

namespace LendersOffice.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// A container for pipeline table info.
    /// </summary>
    public class TableInfo
    {
        /// <summary>
        /// Gets or sets the header columns.
        /// </summary>
        /// <value>A list of strings that are the displayed header columns.</value>
        public List<string> Headers
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the row values.
        /// </summary>
        /// <value>A List of dictionarys that contain loan values.</value>
        public List<Dictionary<string, string>> Rows
        {
            get;
            set;
        }
    }
}