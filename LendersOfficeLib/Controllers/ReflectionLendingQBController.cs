﻿// <copyright file="ReflectionLendingQBController.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Eduardo Michel
//  Date:   6/10/2016
// </summary>

namespace LendersOffice.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Reflection;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.MortgagePool;
    using LendersOffice.UI;

    /// <summary>
    /// This abstract class ipands on the AbstractLendingQBController by providing methods for the retrieval of data using Reflection.
    /// </summary>
    public abstract class ReflectionLendingQBController : AbstractLendingQBController
    {
        /// <summary>
        /// Used for converting strings to their proper data types.
        /// </summary>
        private LosConvert losConvert = new LosConvert();

        /// <summary>
        /// Initializes static members of the <see cref="ReflectionLendingQBController"/> class.
        /// </summary>
        static ReflectionLendingQBController()
        {
            List<Type> objectTypes = new List<Type>() { typeof(MortgagePool), typeof(BrokerDB), typeof(CommonLib.Address) };
            ReflectionLendingQBController.FieldCache = new Dictionary<Type, Dictionary<string, ReflectionPropertyCacheItem>>();
            ReflectionLendingQBController.GenerateFieldCache(objectTypes);
        }

        /// <summary>
        /// Gets or sets a Dictionary containing the field's name, as well as the ReflectionPropertyCacheItem.  These items
        /// will contain all the data necessary to create InputFieldModels.
        /// </summary>
        /// <value>The cache of field data.</value>
        protected static Dictionary<Type, Dictionary<string, ReflectionPropertyCacheItem>> FieldCache
        {
            get; set;
        }

        /// <summary>
        /// Creates a function that takes in an instance of an object and returns the property value.
        /// </summary>
        /// <param name="propertyInfo">The property info of an object.</param>
        /// <returns>The function for retrieving the property value.</returns>
        public static Func<object, object> GetValueGetter(PropertyInfo propertyInfo)
        {
            var dataObject = Expression.Parameter(typeof(object), "dataObject");

            var objConvert = Expression.Convert(dataObject, propertyInfo.DeclaringType);

            Expression accessExpression = Expression.Property(objConvert, propertyInfo);

            Expression valueConvert = Expression.Convert(accessExpression, typeof(object));

            return Expression.Lambda<Func<object, object>>(valueConvert, dataObject).Compile();
        }

        /// <summary>
        /// Creates an action delegate that sets a value for a given propertyInfo.
        /// </summary>
        /// <param name="propertyInfo">The PropertyInfo for the field whose value will be set.</param>
        /// <returns>An action delegate for setting the value.</returns>
        public static Action<object, object> GetValueSetter(PropertyInfo propertyInfo)
        {
            var dataObject = Expression.Parameter(typeof(object), "dataObject");
            var valueObj = Expression.Parameter(typeof(object), "valueObj");

            var objConvert = Expression.Convert(dataObject, propertyInfo.DeclaringType);
            Expression valueConvert = Expression.Convert(valueObj, propertyInfo.PropertyType);

            if (propertyInfo.GetSetMethod() == null)
            {
                return null;
            }
            else
            {
                Expression assignExpression = Expression.Assign(Expression.Property(objConvert, propertyInfo), valueConvert);
                return Expression.Lambda<Action<object, object>>(assignExpression, dataObject, valueObj).Compile();
            }
        }

        /// <summary>
        /// Returns a boolean that determines if the field should be readonly.
        /// </summary>
        /// <param name="dataObject">The Object to retrieve the field from.</param>
        /// <param name="fieldId">The id of the field.</param>
        /// <returns>A boolean.  Returns true when the field is readonly.</returns>
        public static bool CheckIsReadOnly(object dataObject, string fieldId)
        {
            ReflectionPropertyCacheItem field;
            Dictionary<string, ReflectionPropertyCacheItem> propertyFields;
            if (!PipelineController.FieldCache.TryGetValue(dataObject.GetType(), out propertyFields))
            {
                throw new CBaseException(ErrorMessages.Generic, dataObject.GetType() + " class not found.");
            }
            else
            {
                if (!propertyFields.TryGetValue(fieldId, out field))
                {
                    throw new CBaseException(ErrorMessages.Generic, fieldId + " field not found.");
                }

                if (field.Setter == null)
                {
                    return true;
                }

                // Check for a locked field.
                ReflectionPropertyCacheItem lockField;
                if (propertyFields.TryGetValue(fieldId + "Lckd", out lockField))
                {
                    if (!(bool)lockField.Getter(dataObject))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Returns the InputType of a field, which will determine what control will be used to display it in the new user interface.
        /// </summary>
        /// <param name="propertyType">The property info which will be used to determine the type returned.</param>
        /// <param name="fieldName">The name of the field.</param>
        /// <returns>The InputType of the field.</returns>
        public static InputFieldType GetInputType(Type propertyType, string fieldName)
        {
            if (propertyType.Equals(typeof(CDateTime)) || propertyType.Equals(typeof(DateTime)))
            {
                return InputFieldType.Date;
            }
            else if (propertyType.Equals(typeof(int)) || propertyType.Equals(typeof(int?)))
            {
                return InputFieldType.Number;
            }
            else if (propertyType.Equals(typeof(bool)))
            {
                if (fieldName.EndsWith("Lckd"))
                {
                    return InputFieldType.Lock;
                }

                return InputFieldType.Checkbox;
            }
            else if (propertyType.Equals(typeof(decimal)) || propertyType.Equals(typeof(decimal?)))
            {
                if (fieldName.EndsWith("Pc") || fieldName.EndsWith("R"))
                {
                    return InputFieldType.Percent;
                }
                else
                {
                    return InputFieldType.Money;
                }
            }
            else if (propertyType.IsEnum)
            {
                if (fieldName.Equals("CertAndAgreementT"))
                {
                    return InputFieldType.Radio;
                }

                return InputFieldType.DropDownList;
            }
            else if (fieldName.EndsWith(InputFieldNameSuffix.State))
            {
                return InputFieldType.DropDownList;
            }
            else if (fieldName.EndsWith(InputFieldNameSuffix.Zip) || fieldName.EndsWith("Zipcode"))
            {
                return InputFieldType.Zipcode;
            }
            else if (propertyType.Equals(typeof(string)))
            {
                if (fieldName.EndsWith("T"))
                {
                    return InputFieldType.StringWithDropDown;
                }

                return InputFieldType.String;
            }

            return InputFieldType.String;
        }

        /// <summary>
        /// Gets the type of Calculation to perform on a field in the UI if the field is changed.
        /// </summary>
        /// <param name="propertyType">The type of the given property.</param>
        /// <param name="fieldName">The name of the field.</param>
        /// <returns>Returns the calculation type for the given field.</returns>
        public static E_CalculationType GetCalculationType(Type propertyType, string fieldName)
        {
            if (propertyType.Equals(typeof(string)) && (fieldName.EndsWith(InputFieldNameSuffix.Zip) || fieldName.EndsWith("Zipcode")))
            {
                return E_CalculationType.CalculateZip;
            }

            return E_CalculationType.CalculateNormal;
        }

        /// <summary>
        /// Sets the value of the property as a string for the pipeline model.
        /// </summary>
        /// <param name="dataObject">The object to retrieve the value from.</param>
        /// <param name="fieldName">The name of the field to store the value in.</param>
        /// <param name="value">The value that will be inserted.</param>
        public void SetValue(object dataObject, string fieldName, string value)
        {
            var cacheItem = ReflectionLendingQBController.FieldCache[dataObject.GetType()][fieldName];
            if (cacheItem != null && cacheItem.Setter != null)
            {
                var setter = cacheItem.Setter;

                if (cacheItem.ValueType.Equals(typeof(string)))
                {
                    setter(dataObject, value);
                }
                else if (cacheItem.ValueType.Equals(typeof(bool)))
                {
                    setter(dataObject, bool.Parse(value));
                }
                else if (cacheItem.ValueType.IsEnum)
                {
                    setter(dataObject, Enum.Parse(cacheItem.ValueType, value));
                }
                else if (cacheItem.ValueType.Equals(typeof(Guid)))
                {
                    setter(dataObject, new Guid(value));
                }
                else if (cacheItem.ValueType.Equals(typeof(decimal)))
                {
                    setter(dataObject, decimal.Parse(value));
                }
                else if (cacheItem.ValueType.Equals(typeof(int)) || cacheItem.ValueType.Equals(typeof(int?)))
                {
                    if (string.IsNullOrEmpty(value))
                    {
                        setter(dataObject, 0);
                    }
                    else
                    {
                        setter(dataObject, int.Parse(value));
                    }
                }
                else if (cacheItem.ValueType.Equals(typeof(CDateTime)))
                {
                    setter(dataObject, CDateTime.Create(value, this.losConvert));
                }
            }
        }

        /// <summary>
        /// Generates and Input Field model form the given data object and the field Cache.
        /// </summary>
        /// <param name="dataObject">The dataObject to draw the value from.</param>
        /// <param name="fieldName">The name of the field to generate the field model for.</param>
        /// <returns>Returns an InputFieldModel for the given field.</returns>
        public InputFieldModel GetInputFieldModel(object dataObject, string fieldName)
        {
            InputFieldModel model = new InputFieldModel();
            ReflectionPropertyCacheItem item = ReflectionLendingQBController.FieldCache[dataObject.GetType()][fieldName];

            model.Value = item.Getter(dataObject).ToString();
            model.Type = item.InputFieldType;
            model.CalcActionT = item.CalcType;
            model.IsReadOnly = false;
            model.IsHidden = false;

            if (model.CalcActionT != E_CalculationType.CalculateZip && dataObject.GetType().Equals(typeof(BrokerDB)))
            {
                // BrokerDB models are not to be calculated, unless it is the zipcode model.  This is because
                // the BrokerDB is cached, and any changes to BrokerDB will remain until the user has their
                // HttpContext cleared.  Zipcode calculations don't modify the BrokerDB object, so they're
                // allowed.
                model.CalcActionT = E_CalculationType.NoCalculate;
            }

            return model;
        }

        /// <summary>
        /// Returns the _rep version of thefield property if it exists.
        /// </summary>
        /// <param name="fieldName">The name of the field.</param>
        /// <param name="dataObject">The object to retrieve the data from.</param>
        /// <returns>The rep version of the field.</returns>
        public PropertyInfo GetRepFieldVersion(string fieldName, object dataObject)
        {
            return dataObject.GetType().GetProperty(fieldName + "_rep");
        }

        /// <summary>
        /// Returns the value of the property as a string for the pool model.
        /// </summary>
        /// <param name="property">The propertyInfo of the field whose value is being retrieved.</param>
        /// <param name="dataObject">The object to retrieve the value from.</param>
        /// <returns>Returns the property's value in the form of a string.</returns>
        public string GetValue(PropertyInfo property, object dataObject)
        {
            var valueGetter = ReflectionLendingQBController.GetValueGetter(property);
            var value = valueGetter(dataObject);

            if (value == null)
            {
                value = string.Empty;
            }
            else if (property.PropertyType.IsEnum)
            {
                value = Convert.ToInt32(value);
            }

            return value.ToString();
        }

        /// <summary>
        /// Generates the FieldCache for the Controller.
        /// </summary>
        /// <param name="types">A list of types to retrieve the property from and generate their cache.</param>
        protected static void GenerateFieldCache(List<Type> types)
        {
            // If the cache is empty, go through and generate the field cache for every possible field.
            foreach (Type objectType in types)
            {
                Dictionary<string, ReflectionPropertyCacheItem> fields = new Dictionary<string, ReflectionPropertyCacheItem>();
                foreach (PropertyInfo info in objectType.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public))
                {
                    ReflectionPropertyCacheItem item = new ReflectionPropertyCacheItem();
                    item.Getter = ReflectionLendingQBController.GetValueGetter(info);
                    item.Setter = ReflectionLendingQBController.GetValueSetter(info);
                    item.CalcType = ReflectionLendingQBController.GetCalculationType(info.PropertyType, info.Name);
                    item.InputFieldType = ReflectionLendingQBController.GetInputType(info.PropertyType, info.Name);
                    item.ValueType = info.PropertyType;

                    fields.Add(info.Name, item);
                }

                ReflectionLendingQBController.FieldCache.Add(objectType, fields);
            }
        }
    }
}
