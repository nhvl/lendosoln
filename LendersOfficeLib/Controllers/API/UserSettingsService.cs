﻿namespace LendersOffice.Services
{
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Security;

    /// <summary>
    /// Services for User Settings dialog.
    /// </summary>
    public class UserSettingsService : AbstractLendingQBService
    {
        /// <summary>
        /// Get the user settings value list.
        /// </summary>
        /// <returns>The list of user settings.</returns>
        public static List<KeyValuePair<string, object>> Get()
        {
            var principal = PrincipalFactory.CurrentPrincipal;
            var list = new List<KeyValuePair<string, object>>();
            EmployeeDB employee = EmployeeDB.RetrieveById(principal.BrokerId, principal.EmployeeId);
            list.Add(new KeyValuePair<string, object>("UserTheme", employee.UserTheme));
            if (list.Count == 0)
            {
                list.Add(new KeyValuePair<string, object>("UserTheme", E_UserTheme.Light));
            }

            return list;
        }

        /// <summary>
        /// Update the user settings.
        /// </summary>
        /// <param name="userTheme">The theme value.</param>
        public static void Update(E_UserTheme userTheme)
        {
            var principal = PrincipalFactory.CurrentPrincipal;
            EmployeeDB employee = EmployeeDB.RetrieveById(principal.BrokerId, principal.EmployeeId);
            employee.UserTheme = userTheme;
            employee.Save(principal);
        }
    }
}
