﻿namespace LendersOffice.Services
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Security;
    using LendersOffice.UI;

    /// <summary>
    /// Services for loan programs.
    /// </summary>
    public class LoanProgramService : AbstractLendingQBService
    {
        /// <summary>
        /// Get list of loan template.
        /// </summary>
        /// <returns>List of loan template.</returns>
        public static Dictionary<Guid, string> GetLoanProgram()
        {
            SqlParameter[] pars = new SqlParameter[] { new SqlParameter("@BrokerId", BrokerUserPrincipal.CurrentPrincipal.BrokerId) };

            var templateDict = new Dictionary<Guid, string>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListNonlpeLoanProgramsByBrokerId", pars))
            {
                while (reader.Read())
                {
                    var model = new SimpleObjectInputFieldModel();

                    var id = (Guid)reader["lLpTemplateId"];
                    var name = (string)reader["lLpTemplateNm"];
                    templateDict[id] = name;
                }
            }

            return templateDict;
        }
    }
}
