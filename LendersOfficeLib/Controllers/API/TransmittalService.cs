﻿namespace LendersOffice.Services
{
    using System;

    /// <summary>
    /// Services for Transmittal page.
    /// </summary>
    public class TransmittalService : AbstractLendingQBService
    {
        /// <summary>
        /// Calculate Asset Total.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <returns>The Asset Total.</returns>
        public static decimal CalculateAssetTotal(Guid loanId)
        {
            return DataAccess.Utilities.Transmittal.CalculateAssetTotal(loanId);
        }
    }
}
