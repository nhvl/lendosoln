﻿namespace LendersOffice.Services
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text.RegularExpressions;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.CreditReport;
    using LendersOffice.Security;
    using LendersOffice.UI;
    using LqbGrammar.Drivers.SecurityEventLogging;
    using Newtonsoft.Json;

    /// <summary>
    /// Services for BrokerInfo page.
    /// </summary>
    public class BrokerInfoService : AbstractLendingQBService
    {
        /// <summary>
        /// Get list of CRA options.
        /// </summary>
        /// <param name="brokerId">ID of current broker.</param>
        /// <returns>Return list of CRA options.</returns>
        public IEnumerable<CRA> GetCRAOptions(Guid brokerId)
        {
            return MasterCRAList.RetrieveAvailableCras(brokerId);
        }

        /// <summary>
        /// Get list of CRA options.
        /// </summary>
        /// <param name="brokerId">ID of current broker.</param>
        /// <param name="applicationType">Application type.</param>
        /// <returns>Return list of CRA options.</returns>
        public IEnumerable<CRA> GetHiddenCRAOptions(Guid brokerId, E_ApplicationT applicationType)
        {
            return MasterCRAList.RetrieveHiddenCras(brokerId, applicationType).Select(x => MasterCRAList.FindById(x, false, brokerId));
        }

        /// <summary>
        /// Get the list of CRA.
        /// </summary>
        /// <param name="brokerId">ID of current broker.</param>
        /// <returns>List of CRA.</returns>
        public IEnumerable<SimpleObjectInputFieldModel> GetBrokerCRAs(Guid brokerId)
        {
            SqlParameter[] parameters = { new SqlParameter("@BrokerID", brokerId) };

            var items = new List<SimpleObjectInputFieldModel>();

            var craList = this.GetCRAOptions(PrincipalFactory.CurrentPrincipal.BrokerId);
            var craOptions = craList.Select(x => new KeyValuePair<string, string>(x.ID.ToString(), x.VendorName));

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListBrokerCRA", parameters))
            {
                 while (reader.Read())
                {
                    var model = new SimpleObjectInputFieldModel();

                    model.Add("id", new InputFieldModel((Guid)reader["ServiceComId"], customInputType: InputFieldType.DropDownList, customOptions: craOptions));
                    model.Add("isPdfViewNeeded", new InputFieldModel((bool)reader["IsPdfViewNeeded"]));

                    items.Add(model);
                }
            }

            for (int i = items.Count; i < 10; i++)
            {
                var model = new SimpleObjectInputFieldModel();

                model.Add("id", new InputFieldModel(Guid.Empty, customInputType: InputFieldType.DropDownList, customOptions: craOptions));
                model.Add("isPdfViewNeeded", new InputFieldModel(false));

                items.Add(model);
            }

            return this.CalculateCraModel(items);
        }

        /// <summary>
        /// Set list of CRAs to broker.
        /// </summary>
        /// <param name="creditReportAgencies">The list of CRAs set to current Broker.</param>
        /// <param name="brokerId">ID of current broker.</param>
        /// <param name="brokerDB">Data of current broker.</param>
        public void SetBrokerCRAs(IEnumerable<SimpleObjectInputFieldModel> creditReportAgencies, Guid brokerId, BrokerDB brokerDB)
        {
            SqlParameter[] brokerIdParameters = { new SqlParameter("@BrokerID", brokerId) };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "DeleteAllBrokerCRA", 0, brokerIdParameters);

            foreach (var craModel in creditReportAgencies)
            {
                var craId = new Guid(craModel["id"].Value);
                bool isPDFViewed;
                if (craId != Guid.Empty && bool.TryParse(craModel["isPdfViewNeeded"].Value, out isPDFViewed))
                {
                    DateTime currentDateTime = DateTime.Now;
                    SqlParameter[] parameters =
                        {
                            new SqlParameter("@BrokerID", brokerId),
                            new SqlParameter("@CreatedD", currentDateTime),
                            new SqlParameter("@ModifiedD", currentDateTime),
                            new SqlParameter("@ServiceComId", craId),
                            new SqlParameter("@IsPdfViewNeeded", isPDFViewed)
                        };
                    StoredProcedureHelper.ExecuteNonQuery(brokerId, "CreateBrokerCRA", 0, parameters);
                }
            }
        }

        /// <summary>
        /// Caluclate cra models.
        /// </summary>
        /// <param name="craModels">Cra models input to calculated.</param>
        /// <returns>New cra models.</returns>
        public IEnumerable<SimpleObjectInputFieldModel> CalculateCraModel(IEnumerable<SimpleObjectInputFieldModel> craModels)
        {
            var calculatedCraModels = new List<SimpleObjectInputFieldModel>();

            var usedIds = craModels.Select(x => new Guid(x["id"].Value)).Where(y => y != Guid.Empty);

            var craList = this.GetCRAOptions(PrincipalFactory.CurrentPrincipal.BrokerId);

            foreach (var craModel in craModels)
            {
                var craId = new Guid(craModel["id"].Value);
                craModel["id"].Options = craList.Where(x => !usedIds.Contains(x.ID) || x.ID == craId).Select(x => new KeyValuePair<string, string>(x.ID.ToString(), x.VendorName));
                var craObject = craList.Where(x => x.ID == craId).FirstOrDefault();
                var isNonMclProtocol = false;

                if (craObject != null)
                {
                    isNonMclProtocol = craObject.Protocol != CreditReportProtocol.Mcl;
                }

                var emptyId = craId == Guid.Empty;

                craModel["isPdfViewNeeded"].IsReadOnly = isNonMclProtocol || emptyId;

                if (craModel["isPdfViewNeeded"].IsReadOnly)
                {
                    craModel["isPdfViewNeeded"].Value = false.ToString();
                }

                calculatedCraModels.Add(craModel);
            }

            return calculatedCraModels;
        }

        /// <summary>
        /// Bind ip whitelist to current broker.
        /// </summary>
        /// <param name="ipWhiteListModel">Model list of IP restriction.</param>
        /// <param name="brokerId">ID of current broker client.</param>
        public void EditGlobalWhiteList(IEnumerable<SimpleObjectInputFieldModel> ipWhiteListModel, Guid brokerId)
        {
            var oldWhiteList = BrokerGlobalIpWhiteList.ListByBrokerId(brokerId);

            var deletedWhiteList = oldWhiteList.Where(x => ipWhiteListModel.All(y => Convert.ToInt32(y.ValueFromModel(nameof(BrokerGlobalIpWhiteList.Id))) != x.Id)).ToList();
            deletedWhiteList.ForEach(x => BrokerGlobalIpWhiteList.Delete(brokerId, x.Id));

            foreach (var inputItem in ipWhiteListModel)
            {
                BrokerGlobalIpWhiteList item;
                var id = Convert.ToInt32(inputItem.ValueFromModel(nameof(BrokerGlobalIpWhiteList.Id)));

                var desc = inputItem.ValueFromModel(nameof(BrokerGlobalIpWhiteList.Description));
                var addr = inputItem.ValueFromModel(nameof(BrokerGlobalIpWhiteList.IpAddress));

                if (id != -1)
                {
                    item = BrokerGlobalIpWhiteList.RetrieveById(brokerId, id);

                    if (item.Description == desc && item.IpAddress == addr)
                    {
                        continue;
                    }
                }
                else
                {
                    item = new BrokerGlobalIpWhiteList();
                }

                item.Description = desc;
                item.IpAddress = addr;
                item.Save(BrokerUserPrincipal.CurrentPrincipal);
            }
        }

        /// <summary>
        /// Return the IP Whitelist.
        /// </summary>
        /// <param name="brokerId">ID of current broker client.</param>
        /// <returns>The list of registed global IP addresses.</returns>
        public IEnumerable<SimpleObjectInputFieldModel> GlobalWhiteList(Guid brokerId)
        {
            var modelList = new List<SimpleObjectInputFieldModel>();
            foreach (var brokerIp in BrokerGlobalIpWhiteList.ListByBrokerId(brokerId))
            {
                var model = new SimpleObjectInputFieldModel();
                model.Add(nameof(brokerIp.Id), new InputFieldModel(brokerIp.Id));
                model.Add(nameof(brokerIp.Description), new InputFieldModel(brokerIp.Description));
                model.Add(nameof(brokerIp.IpAddress), new InputFieldModel(brokerIp.IpAddress));
                model.Add(nameof(brokerIp.LastModifiedDateDisplay), new InputFieldModel(brokerIp.LastModifiedDateDisplay));
                model.Add(nameof(brokerIp.LastModifiedLoginName), new InputFieldModel(brokerIp.LastModifiedLoginName));
                modelList.Add(model);
            }

            return modelList;
        }

        /// <summary>
        /// Get loan naming scheme of current broker.
        /// </summary>
        /// <param name="principal">Princial of current broker user.</param>
        /// <returns>Return the naming options of current broker naming scheme.</returns>
        public Dictionary<string, object> GetNamingOptions(AbstractUserPrincipal principal)
        {
            return DataAccess.Utilities.LoanNaming.GetNamingOptions(principal.BrokerDB, principal.BranchId);
        }

        /// <summary>
        /// Caluclate naming option model.
        /// </summary>
        /// <param name="namingOption">Naming option input to calculated.</param>
        /// <returns>New naming option model.</returns>
        public Dictionary<string, object> CalculateNamingOptions(DataAccess.Utilities.LoanNaming.LoanNamingOption namingOption)
        {
            return DataAccess.Utilities.LoanNaming.CalculateNamingOptions(namingOption);
        }

        /// <summary>
        /// Bind naming option to broker loan naming scheme.
        /// </summary>
        /// <param name="namingOptionModel">Naming options.</param>
        /// <param name="broker">Broker need to be updated loan naming scheme.</param>
        public void BindNamingOptions(Dictionary<string, object> namingOptionModel, BrokerDB broker)
        {
            DataAccess.Utilities.LoanNaming.BindNamingOptions(namingOptionModel, broker);
        }

        /// <summary>
        /// Get branch prefix models.
        /// </summary>
        /// <param name="brokerId">ID of current broker.</param>
        /// <returns>Branch prefix models.</returns>
        public IEnumerable<SimpleObjectInputFieldModel> GetBranchNamePrefix(Guid brokerId)
        {
            return DataAccess.Utilities.LoanNaming.GetBranchNamePrefix(brokerId);
        }

        /// <summary>
        /// Calculate branch prefix models.
        /// </summary>
        /// <param name="prefixModels">Branch prefix models to be calculated.</param>
        /// <returns>Calculated branch prefix models.</returns>
        public IEnumerable<SimpleObjectInputFieldModel> CalculateBranchNamePrefix(IEnumerable<SimpleObjectInputFieldModel> prefixModels)
        {
            return DataAccess.Utilities.LoanNaming.CalculateBranchNamePrefix(prefixModels);
        }

        /// <summary>
        /// Save branch prefix models.
        /// </summary>
        /// <param name="brokerId">ID of current broker.</param>
        /// <param name="prefixModels">Branch prefix models to be saved.</param>
        public void SetBranchNamePrefix(Guid brokerId, IEnumerable<SimpleObjectInputFieldModel> prefixModels)
        {
            DataAccess.Utilities.LoanNaming.SetBranchNamePrefix(brokerId, prefixModels);
        }

        /// <summary>
        /// Get all branches.
        /// </summary>
        /// <returns>Services data.</returns>
        public List<KeyValuePair<string, string>> GetBranchList()
        {
            var brancheDBs = BranchDB.GetBranchObjects(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
            var branches = brancheDBs.Select(x => new KeyValuePair<string, string>(x.BranchID.ToString(), x.Name)).ToList();

            if (branches.Count > 0)
            {
                branches.Insert(0, new KeyValuePair<string, string>(Guid.Empty.ToString(), "<-- All branches -->"));
            }

            return branches;
        }

        /// <summary>
        /// Get password option models.
        /// </summary>
        /// <returns>Model of password options.</returns>
        public SimpleObjectInputFieldModel GetPasswordOptionModel()
        {
            var model = new SimpleObjectInputFieldModel();

            var branches = this.GetBranchList();

            var expirePeriodDict = new Dictionary<string, string>
                        {
                            { "15", "15" },
                            { "30", "30" },
                            { "45", "45" },
                            { "60", "60" },
                        };

            model.Add("branch", new InputFieldModel(branches[0].Key, customInputType: InputFieldType.DropDownList, customOptions: branches));
            model.Add("expireDate", new InputFieldModel(DateTime.Now));
            model.Add("option", new InputFieldModel(0));
            model.Add("cycleExpire", new InputFieldModel(false));
            model.Add("expirePeriod", new InputFieldModel(15, customInputType: InputFieldType.DropDownList, customOptions: expirePeriodDict.ToList()));

            return this.CalculatePasswordOption(model);
        }

        /// <summary>
        /// Caluclate password option model.
        /// </summary>
        /// <param name="model">Password option model to be calculated.</param>
        /// <returns>New password option model .</returns>
        public SimpleObjectInputFieldModel CalculatePasswordOption(SimpleObjectInputFieldModel model)
        {
            var isDatedExpire = model["option"].Value == "2";
            var isExpireNext  = model["option"].Value == "0";
            var isCycleExpire = (bool)model["cycleExpire"].ValueFromType(typeof(bool));

            if (!isDatedExpire)
            {
                model["expireDate"].Value = string.Empty;

                if (!isExpireNext)
                {
                    isCycleExpire = false;

                    model["cycleExpire"].Value = isCycleExpire.ToString();
                }
            }

            model["expireDate"].IsReadOnly = !isDatedExpire;
            model["cycleExpire"].IsReadOnly = !isDatedExpire && !isExpireNext;

            model["expirePeriod"].IsReadOnly = !isCycleExpire;

            return model;
        }

        /// <summary>
        /// Apply a password option for a branch.
        /// </summary>
        /// <param name="branch">Branch Id, empty if apply for all branches.</param>
        /// <param name="option">Option want to apply.</param>
        /// <param name="expirationD">Expire day if existed.</param>
        /// <param name="cycleExpire">Cycle of expire if existed.</param>
        /// <param name="expirePeriod">Expire period if existed.</param>
        /// <returns>A error string message, empty if sucess.</returns>
        public string ApplyPasswordOptions(string branch, string option, DateTime? expirationD, bool cycleExpire, string expirePeriod)
        {
            var paramList = new List<SqlParameter>();

            if (string.IsNullOrEmpty(branch))
            {
                return "Please select a valid branch";
            }

            var brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            paramList.Add(new SqlParameter("@BrokerId", brokerId));

            switch (option)
            {
                case "NextL":
                    paramList.Add(new SqlParameter("@PasswordExpirationD", DateTime.Now.Date));
                    break;
                case "Dated":
                    if (expirationD != null)
                    {
                        if (expirationD.Value.CompareTo(DateTime.Now.Date) < 0 || expirationD.Value.Year > 2070)
                        {
                            return "Invalid expiration date.Please specify a date between " + DateTime.Now.Date.ToShortDateString() + " and 12/31/2070.";
                        }

                        paramList.Add(new SqlParameter("@PasswordExpirationD", expirationD.Value));
                    }
                    else
                    {
                        return "Please enter a valid expiration date.";
                    }

                    break;
                case "Never":
                    break;
                default:
                    throw new CBaseException("Invalid option.", "Invalid option.");
            }

            if (cycleExpire)
            {
                paramList.Add(new SqlParameter("@PasswordExpirationPeriod", expirePeriod));
            }

            if (branch != Guid.Empty.ToString())
            {
                paramList.Add(new SqlParameter("@BranchId", branch));
            }

            BrokerEmployeeNameTable employees = new BrokerEmployeeNameTable();
            BrokerUserPermissionsSet permissions = new BrokerUserPermissionsSet();
            string customerCode = PrincipalFactory.CurrentPrincipal.BrokerDB.CustomerCode;

            employees.Retrieve(brokerId);
            permissions.Retrieve(brokerId);

            using (CStoredProcedureExec procedureExec = new CStoredProcedureExec(BrokerUserPrincipal.CurrentPrincipal.BrokerId))
            {
                procedureExec.BeginTransactionForWrite();
                try
                {
                    procedureExec.ExecuteNonQuery("BatchPasswordOptionsUpdate", paramList.ToArray());

                    foreach (var ed in employees)
                    {
                        BrokerUserPermissions bup = permissions[ed.Key];
                        if ((bup != null && bup.IsInternalBrokerUser()) || (customerCode != null && ed.Value.Type == "P" && ed.Value.Login.ToLower() == customerCode))
                        {
                            procedureExec.ExecuteNonQuery("UpdatePasswordOptionForUser", new SqlParameter[] { new SqlParameter("@UserId", ed.Value.UserId) });
                        }
                    }

                    procedureExec.CommitTransaction();
                }
                catch (Exception e)
                {
                    Tools.LogError(e);
                    procedureExec.RollbackTransaction();
                    throw new CBaseException("Could not save password options.", "SQL execute failed.");
                }
            }

            return string.Empty;
        }
    }
}
