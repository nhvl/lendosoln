﻿namespace LendersOffice.Services
{
    using System;
    using DataAccess.Utilities;

    /// <summary>
    /// Services for Edit Team page.
    /// </summary>
    public class EditTeamsService : AbstractLendingQBService
    {
        /// <summary>
        /// Clear Assignment.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="roleString">The role string.</param>
        public void ClearLoanAssignment(Guid loanId, string roleString)
        {
            EditTeams.ClearLoanAssignment(loanId, roleString);
        }

        /// <summary>
        /// Assign Loan.
        /// </summary>
        /// <param name="loanId">The Loan ID.</param>
        /// <param name="teamId">The team ID.</param>
        public void AssignLoan(Guid loanId, Guid teamId)
        {
            EditTeams.AssignLoan(loanId, teamId);
        }
    }
}
