﻿namespace LendersOffice.Services
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Integration.DocumentVendor;

    /// <summary>
    /// Services for Loan Status.
    /// </summary>
    public class DocumentVendorService : AbstractLendingQBService
    {
        /// <summary>
        /// Get list of Loan Status.
        /// </summary>
        /// <param name="brokerId">Broker Id.</param>
        /// <param name="vendorId">Vendor Id.</param>
        /// <param name="code">Plan code.</param>
        /// <returns>A list of Loan Statuses.</returns>
        public static DocumentVendorResult<List<InvestorInfo>> GetTransferToVendor(Guid brokerId, Guid vendorId, string code)
        {
            var vendor = DocumentVendorFactory.Create(brokerId, vendorId);
            if (!vendor.Skin.SupportsTransferToInvestors)
            {
                throw new CBaseException("Unsupported document vendor.", "The vendor must be docmagic.");
            }

            return vendor.GetTransferToInvestors(code);
        }
    }
}
