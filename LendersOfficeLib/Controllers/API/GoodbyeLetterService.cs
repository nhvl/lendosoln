﻿namespace LendersOffice.Services
{
    using System;
    using ObjLib.Rolodex;
    using Security;

    /// <summary>
    /// Services for Goodbye Letter page.
    /// </summary>
    public class GoodbyeLetterService : AbstractLendingQBService
    {
        /// <summary>
        /// Get the next payment date after the acceptingD parameter. Use logic similar to what is used on the servicing page.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="acceptingD">Accepting Date.</param>
        /// <returns>The next payment date.</returns>
        public static string GetSchedDateOfFirstPmt(Guid loanId, string acceptingD)
        {
            return DataAccess.Utilities.GoodbyeLetter.GetSchedDateOfFirstPmt(loanId, acceptingD);
        }

        /// <summary>
        /// Get Investor Infomation.
        /// </summary>
        /// <param name="id">The ID of Investor.</param>
        /// <returns>The Investor Information.</returns>
        public static InvestorRolodexEntry GetInvestorInfo(int id)
        {
            return InvestorRolodexEntry.Get(PrincipalFactory.CurrentPrincipal.BrokerId, id);
        }
    }
}
