﻿namespace LendersOffice.Services
{
    using System;
    using System.Collections.Generic;
    using DataAccess.Utilities;

    /// <summary>
    /// Services for Employee Role page.
    /// </summary>
    public class EmployeeRoleService : AbstractLendingQBService
    {
        /// <summary>
        /// Clear Assignment.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="copyToOfficialAgent">Is copy to Official Agent.</param>
        /// <param name="role">The Agent Role.</param>
        /// <returns>The Error List if exists.</returns>
        public List<string> ClearAssignment(Guid loanId, bool copyToOfficialAgent, string role)
        {
            return EmployeeRole.ClearAssignment(loanId, copyToOfficialAgent, role);       
        }

        /// <summary>
        /// Assign To Employee.
        /// </summary>
        /// <param name="employeeID">Employee ID.</param>
        /// <param name="email">Email Address.</param>
        /// <param name="employeeName">Employee Name.</param>
        /// <param name="pmlUser">PML User Value.</param>
        /// <param name="loanId">Loan ID Value.</param>
        /// <param name="copyToOfficialAgent">Copy To Official Agent.</param>
        /// <param name="role">The Role Value.</param>
        /// <returns>The list of error if exists.</returns>
        public List<string> AssignEmployee(Guid employeeID, string email, string employeeName, bool pmlUser, Guid loanId, bool copyToOfficialAgent, string role)
        {
            return EmployeeRole.AssignEmployee(employeeID, email, employeeName, pmlUser, loanId, copyToOfficialAgent, role);
        }

        /// <summary>
        /// Assign LO And Branch.
        /// </summary>
        /// <param name="loanId">Loan ID Value.</param>
        /// <param name="loanofficerId">Loan Officer ID.</param>
        /// <param name="branchId">Branch ID Value.</param>
        /// <param name="copyToOfficialAgent">Copy To Official Agent.</param>
        /// <param name="loanOfficerName">Loan Officer Name.</param>
        /// <param name="loanOfficerEmail">Loan Officer Email.</param>
        /// <param name="roleTStr">The roleT String.</param>
        /// <returns>The error or status.</returns>
        public string AssignLOAndBranch(Guid loanId, Guid loanofficerId, Guid branchId, bool copyToOfficialAgent, string loanOfficerName, string loanOfficerEmail, string roleTStr)
        {
            return EmployeeRole.AssignLOAndBranch(loanId, loanofficerId, branchId, copyToOfficialAgent, loanOfficerName, loanOfficerEmail, roleTStr);
        }

        /// <summary>
        /// Search Broker Loan Assignment.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="origid">The origid value.</param>
        /// <param name="searchFilter">The search filter.</param>
        /// <param name="status">The status value.</param>
        /// <param name="pmlBrokerId">The pml broker ID.</param>
        /// <param name="role">The role value.</param>
        /// <param name="showMode">The show Mode.</param>
        /// <param name="isOnlyDisplayLqbUsers">Is Display LqbUsers.</param>
        /// <param name="isOnlyDisplayPmlUsers">Is Dislplay Pml Users.</param>
        /// <returns>The list of Broker Loan Assignment.</returns>
        public Dictionary<string, object> Search(Guid loanId, Guid origid, string searchFilter, int status, Guid pmlBrokerId, string role, string showMode, bool isOnlyDisplayLqbUsers, bool isOnlyDisplayPmlUsers)
        {
            return EmployeeRole.Search(loanId, origid, searchFilter, status, pmlBrokerId, role, showMode, isOnlyDisplayLqbUsers, isOnlyDisplayPmlUsers);
        }
    }
}
