﻿namespace LendersOffice.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using LendersOffice.Security;

     /// <summary>
    /// Tool for loading and invoking services.
    /// </summary>
    public class LendingQBServiceHelper
    {
        /// <summary>
        /// Store the service name and method list of that service.
        /// </summary>
        private static Dictionary<string, Dictionary<string, MethodInfo>> serviceMethodDict;

        /// <summary>
        /// Store a constaints for service class name suffix.
        /// </summary>
        private static string serviceSuffix = "Service";

        /// <summary>
        /// Initializes static members of the <see cref="LendingQBServiceHelper" /> class.
        /// </summary>
        static LendingQBServiceHelper()
        {
            if (LendingQBServiceHelper.serviceMethodDict == null)
            {
                var serviceDictionary = new Dictionary<string, Dictionary<string, MethodInfo>>();
                foreach (Type type in
                    Assembly.GetAssembly(typeof(AbstractLendingQBService)).GetTypes()
                    .Where(myType => myType.IsClass && !myType.IsAbstract && myType.IsSubclassOf(typeof(AbstractLendingQBService))))
                {
                    var serviceName = type.Name;

                    if (serviceName.EndsWith(serviceSuffix))
                    {
                        serviceName = serviceName.Substring(0, serviceName.LastIndexOf(serviceSuffix));
                    }

                    var methods = type.GetMethods(BindingFlags.Public | BindingFlags.Static | BindingFlags.Instance | BindingFlags.DeclaredOnly);

                    var methodDict = methods.ToDictionary(method => method.Name);

                    serviceDictionary.Add(serviceName, methodDict);
                }

                LendingQBServiceHelper.serviceMethodDict = serviceDictionary;
            }
        }

        /// <summary>
        /// Invoke a service and return the result.
        /// </summary>
        /// <param name="serviceName">Service to execute.</param>
        /// <param name="methodName">Method to execute.</param>
        /// <param name="paramDict">Parameters in josn dictionary, null if method don't need parameter.</param>
        /// <param name="userPrincipal">Principal info of current user.</param>
        /// <returns>A <code>JSON</code> serializable object that return to client.</returns>
        public static object InvokeService(string serviceName, string methodName, Dictionary<string, object> paramDict, AbstractUserPrincipal userPrincipal)
        {
            if (LendingQBServiceHelper.serviceMethodDict == null || !LendingQBServiceHelper.serviceMethodDict.ContainsKey(serviceName))
            {
                throw new NotImplementedException("LendingQBServiceHelper::op=[" + serviceName + "]");
            }

            var methodDict = LendingQBServiceHelper.serviceMethodDict[serviceName];
            if (methodDict == null || !methodDict.ContainsKey(methodName))
            {
                throw new NotImplementedException(serviceName + "::method=[" + methodName + "]");
            }

            var methodInfo = methodDict[methodName];
            var parameters = MapParameters(methodInfo, paramDict, userPrincipal);

            if (methodInfo.IsStatic)
            {
                return methodInfo.Invoke(null, parameters);
            }
            else
            {
                return methodInfo.Invoke(Activator.CreateInstance(methodInfo.DeclaringType), parameters);
            }
        }

        /// <summary>
        /// Return a list of paramter from a json dictionary input.
        /// </summary>
        /// <param name="method">The method base needed to be invoked.</param>
        /// <param name="namedParameters">Dictionary of input paremeter to invoke the method.</param>
        /// <param name="userPrincipal">Principal info of current user.</param>
        /// <returns>List of ordered paramenter object to invoke the method using MethodBase.Invoke() .</returns>
        private static object[] MapParameters(MethodBase method, IDictionary<string, object> namedParameters, AbstractUserPrincipal userPrincipal)
        {
            var paramNames = method.GetParameters().Select(p => new Tuple<string, Type>(p.Name, p.ParameterType)).ToArray();
            if (paramNames.Length == 0)
            {
                return null;
            }

            object[] parameters = new object[paramNames.Length];

            for (int i = 0; i < parameters.Length; ++i)
            {
                parameters[i] = Type.Missing;
            }

            var brokerIdIndex = Array.FindIndex(paramNames, x => x.Item1 == "brokerId");
            if (brokerIdIndex >= 0)
            {
                parameters[brokerIdIndex] = userPrincipal.BrokerId;
            }

            foreach (var item in namedParameters)
            {
                var paramName = item.Key;
                var paramIndex = Array.FindIndex(paramNames, x => x.Item1 == paramName);

                if (paramIndex >= 0)
                {
                    var paramType = paramNames[paramIndex].Item2;

                    if (paramType == typeof(Guid) && item.Value is string)
                    {
                        parameters[paramIndex] = new Guid(item.Value as string);
                    }
                    else
                    {
                        parameters[paramIndex] = item.Value;
                    }
                }
            }

            for (int i = 0; i < parameters.Length; ++i)
            {
                if (parameters[i] == Type.Missing)
                {
                    var paramType = paramNames[i].Item2;
                    if (paramType.IsValueType || Nullable.GetUnderlyingType(paramType) != null)
                    {
                        parameters[i] = null;
                    }
                    else
                    {
                        throw new Exception("LendingQBServiceHelper::" + method.Name + ": Parameter is not valid");
                    }
                }
            }

            return parameters;
        }
    }
}
