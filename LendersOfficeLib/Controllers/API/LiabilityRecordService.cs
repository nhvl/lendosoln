﻿namespace LendersOffice.Services
{
    using System;
    using DataAccess;

    /// <summary>
    /// Services for Liability page.
    /// </summary>
    public class LiabilityRecordService : AbstractLendingQBService
    {
        /// <summary>
        /// Add To Reo Service.
        /// </summary>
        /// <param name="reoId">The Reo Id.</param>
        /// <param name="balValue">The bal value.</param>
        /// <param name="paymentValue">The payment value.</param>
        /// <param name="loanId">The loan Id.</param>
        /// <param name="appId">The app Id.</param>
        /// <param name="fileVersion">The file version.</param>
        public static void AddToReo(Guid reoId, string balValue, string paymentValue, Guid loanId, Guid appId, int fileVersion)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(LiabilityRecordService));
            dataLoan.InitSave(fileVersion);
            var bal = dataLoan.m_convertLos.ToMoney(balValue);
            var payment = dataLoan.m_convertLos.ToMoney(paymentValue);

            CAppData dataApp = dataLoan.GetAppData(appId);

            var reoFields = dataApp.aReCollection.GetRegRecordOf(reoId);
            reoFields.MAmt += bal;
            reoFields.MPmt += payment;
            reoFields.Update();

            dataLoan.Save();

            // SetResult("sFileVersion", dataLoan.sFileVersion);
        }
    }
}
