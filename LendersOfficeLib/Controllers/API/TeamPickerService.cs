﻿namespace LendersOffice.Services
{
    using System.Collections.Generic;
    using DataAccess.Utilities;
    using Security;

    /// <summary>
    /// Services for Team Picker page.
    /// </summary>
    public class TeamPickerService : AbstractLendingQBService
    {
        /// <summary>
        /// Get list of teams.
        /// </summary>
        /// <param name="isAssignedTeam">Is Assigned Team or Is All Teams.</param>
        /// <param name="roleString">The role string.</param>
        /// <returns>The list of teams.</returns>
        public List<Team> Search(bool isAssignedTeam, string roleString)
        {
            return TeamPicker.Search(isAssignedTeam, roleString);
        }
    }
}
