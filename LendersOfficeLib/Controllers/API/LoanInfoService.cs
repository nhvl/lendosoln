﻿namespace LendersOffice.Services
{
    using System;
    using DataAccess;
    using LendersOffice.Security;

    /// <summary>
    /// Services for Loan Status.
    /// </summary>
    public class LoanInfoService : AbstractLendingQBService
    {
        /// <summary>
        /// Broker User.
        /// </summary>
        private static BrokerUserPrincipal BrokerUser => BrokerUserPrincipal.CurrentPrincipal;

        /// <summary>
        /// Generate New Loan Number.
        /// </summary>
        /// <param name="loanId">Loan Id (sLId).</param>
        /// <returns>A New Loan Number.</returns>
        public static string GenerateNewLoanNumber(Guid loanId)
        {
            // 1/26/2005 kb - Get loan namer and get an auto name.
            // The namer will query the broker's bits to determine
            // if a sequential, counter-based name is used, or a
            // friendly id is used.
            //
            // We also get the loan number passed back, so we should
            // do a security check, to determine if they have permission
            // to get a new name for a loan.  Yes, the loan editor will
            // enforce saving, but this call has the side effect of
            // incrementing the broker's naming counter.  Hmmm...
            //
            // 1/28/2005 kb - Loop until a valid number is served up.
            var loanNamer = new CLoanFileNamer();
            
            string newLoanName;
            do
            {
                string mersMin;
                string refNm;
                newLoanName = loanNamer.GetLoanAutoName(BrokerUser.BrokerId, BrokerUser.BranchId, false, false, false/* enableMersGeneration*/, out mersMin, out refNm);
            }
            while (CLoanFileNamer.CheckIfLoanNameExists(BrokerUser.BrokerId, loanId, newLoanName));

            return newLoanName;
        }
    }
}
