namespace LendersOffice.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess.Utilities;
    using EDocs;
    using LendersOffice.Security;

    /// <summary>
    /// Services for Edoc page.
    /// </summary>
    public class EdocService : AbstractLendingQBService
    {
        /// <summary>
        /// Get all file.
        /// </summary>
        /// <param name="showSystemFolders">Should show system folders or not.</param>
        /// <param name="brokerId">Optional broker id.</param>
        /// <returns>Return all files of the broker.</returns>
        public static IEnumerable<object> GetAllFiles(bool showSystemFolders, Guid brokerId)
        {
            var usingBrokerId = EdocService.GetUsingBrokerId(brokerId);

            return EdocService.GetDocTypeList(usingBrokerId, showSystemFolders).Select(x => new
            {
                docId = x.DocTypeId,
                docName = x.DocTypeName,
                folderId = x.Folder.FolderId,
                folderName = x.Folder.FolderNm,
            });
        }

        /// <summary>
        /// Get ConditionAssociation of EDocument.
        /// </summary>
        /// <param name="loanId">Id of the loan.</param>
        /// <param name="docId">Id of the Edocument.</param>
        /// <returns>List of ConditionAssociation.</returns>
        public static EdocUtilities.ConditionAssociationDTO[] GetConditionAssociation(Guid loanId, Guid? docId)
        {
            return EdocUtilities.GetConditionAssociation(loanId, docId);
        }

        /// <summary>
        /// Get all documents from a loan.
        /// </summary>
        /// <param name="loanId">Id of a loan to get all document from.</param>
        /// <returns>All document from a specific loan.</returns>
        public static Dictionary<string, object> GetDocuments(Guid loanId)
        {
            return EdocUtilities.GetDocuments(loanId);
        }

        /// <summary>
        /// Update documents of a specific loan.
        /// </summary>
        /// <param name="documentDict">Document models.</param>
        /// <param name="loanId">Id of the loan.</param>
        public static void SaveDoc(Dictionary<string, object> documentDict, Guid loanId)
        {
            EdocUtilities.SaveDoc(documentDict, loanId);
        }

        /// <summary>
        /// Get list of docs of a broker user.
        /// </summary>
        /// <param name="brokerId">Id of broker.</param>
        /// <param name="showSystemFolders">Detect if should return all docs of system folders.</param>
        /// <returns>List of docs of a broker user.</returns>
        private static IEnumerable<DocType> GetDocTypeList(Guid brokerId, bool showSystemFolders)
        {
            var docTypeList = EDocumentDocType.GetDocTypesByBroker(brokerId, DataAccess.E_EnforceFolderPermissions.True);
            if (!showSystemFolders)
            {
                docTypeList = docTypeList.Where(x => !x.Folder.IsSystemFolder);
            }

            return docTypeList;
        }

        /// <summary>
        /// Get Id of current broker.
        /// </summary>
        /// <param name="brokerId">Id of broker to request, use this Id in case app can not load broker from principal.</param>
        /// <returns>The broker Id using to request data.</returns>
        private static Guid GetUsingBrokerId(Guid brokerId)
        {
            var broker = BrokerUserPrincipal.CurrentPrincipal?.BrokerId ?? brokerId;
            if (broker == null || broker == Guid.Empty)
            {
                throw new Exception("Broker is not valid.");
            }

            return broker;
        }
    }
}
