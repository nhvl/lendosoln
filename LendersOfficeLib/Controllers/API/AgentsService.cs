﻿namespace LendersOffice.Services
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using Admin;
    using Audit;
    using DataAccess;
    using Security;
    using UI;

    /// <summary>
    /// Services for Agent page.
    /// </summary>
    public class AgentsService : AbstractLendingQBService
    {
        /// <summary>
        /// Check if page is read-only or not.
        /// </summary>
        /// <returns>The page is read-only or not.</returns>
        public bool IsPageReadOnly()
        {
            return false;
        }

        /// <summary>
        /// Delete official contact list.
        /// </summary>
        /// <param name="loanId">The loan Id.</param>
        /// <param name="fileVersion">The file version.</param>
        /// <param name="idItems">The list of contact list need to delete.</param>
        public void DeleteOfficialContact(Guid loanId, int fileVersion, string idItems)
        {
            var idList = idItems.Split(';');
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(AgentsService));
            dataLoan.InitSave(fileVersion);

            DataSet ds = dataLoan.sAgentDataSet;

            DataTable table = ds.Tables[0];
            int count = 0;
            List<DataRow> deletingRows = new List<DataRow>();
            foreach (DataRow row in table.Rows)
            {
                foreach (string id in idList)
                {
                    if (row["recordId"].ToString() == id)
                    {
                        // I could not invoke the Delete() on row object here.
                        // Doing so will modify the collection of table.Rows which cause
                        // the exception to be throw. dd 4/21/2003
                        deletingRows.Add(row);
                        count++;
                        break;
                    }
                }

                if (count > idList.Length)
                {
                    break;
                }
            }

            foreach (var row in deletingRows)
            {
                dataLoan.RecordAuditOnSave(AgentRecordChangeAuditHelper.CreateAuditEvent(row, dataLoan.sSpState, AgentRecordChangeType.DeleteRecord));

                // OPM 209868 - For CFPB migrated loans, clear beneficiary info in any closing cost fee where this agent is the beneficiary.
                if (dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy)
                {
                    dataLoan.sClosingCostSet.ClearBeneficiary(new Guid((string)row["recordId"]));
                }

                row.Delete();

                // 8/13/2016, ML
                // Until we call AcceptChanges, the row will still be present
                // in the DataTable but will not be accessible using DataTable.Rows[index].
                // This presents a problem since DataTable.Rows.Count will return 
                // a value that includes this row, so for methods like GetAgentOfRole
                // that iterate over all of the rows, it is possible that the method
                // will attempt to access a deleted row, resulting in an IndexOutOfRangeException
                // since the deleted row should not be accessed.
                row.AcceptChanges();
            }

            dataLoan.sAgentDataSet = ds;
            dataLoan.Save();
        }

        /// <summary>
        /// Get the team names.
        /// </summary>
        /// <param name="loanId">The loan ID.</param>
        /// <returns>The list of team names.</returns>
        public Dictionary<E_RoleT, string> GetTeamNames(Guid loanId)
        {
            var teamName = new Dictionary<E_RoleT, string>();
            foreach (Team team in Team.ListTeamsOnLoan(loanId))
            {
                Role role = Role.Get(team.RoleId);
                if (role != null)
                {
                    teamName.Add(role.RoleT, team.Name);
                }
            }

            return teamName;
        }

        /// <summary>
        /// Load the Agent Record.
        /// </summary>
        /// <param name="loanId">The loan ID>.</param>
        /// <param name="recordId">The agent record ID.</param>
        /// <returns>The Agent Record.</returns>
        public Dictionary<string, object> GetAgentRecord(Guid loanId, Guid recordId)
        {
            CPageData dataLoan = new CPageData(loanId, new List<string>() { "sAgentCollection" });
            dataLoan.InitLoad();

            return InputModelProvider.GetInputModel(dataLoan.sAgentCollection.GetAgentFields(recordId));
        }

        /// <summary>
        /// Get the permission.
        /// </summary>
        /// <param name="permission">The permision type.</param>
        /// <returns>Has Permision or not.</returns>
        public bool HasPermission(Permission permission)
        {
            return BrokerUserPrincipal.CurrentPrincipal.HasPermission(permission);
        }

        /// <summary>
        /// Get the license templage.
        /// </summary>
        /// <returns>The license template.</returns>
        public Dictionary<string, object> GetLicenseTemplate()
        {
            return InputModelProvider.GetInputModel(new LicenseInfo());
        }
    }
}
