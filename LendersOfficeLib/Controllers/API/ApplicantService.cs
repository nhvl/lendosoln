﻿namespace LendersOffice.Services
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;

    /// <summary>
    /// Services for Applicant updating.
    /// </summary>
    public class ApplicantService : AbstractLendingQBService
    {
        /// <summary>
        /// List of hard code field used when creating data loan.
        /// </summary>
        private static readonly string[] FieldIds = { "sLId", "aCFirstNm", "aCMidNm", "aCLastNm", "aCSuffix" };

        /// <summary>
        /// Set an applicant as primary one.
        /// </summary>
        /// <param name="loanId">Loan Data GUID ID.</param>
        /// <param name="appId">Applicant data GUID ID.</param>
        public static void UpdatePrimaryBorrower(Guid loanId, Guid appId)
        {
            CPageData dataLoan = ApplicantService.GetSaveDataLoan(loanId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAppData dataApp = dataLoan.GetAppData(appId);
            dataApp.aIsPrimary = true;

            dataLoan.Save();
       }

        /// <summary>
        /// Add applicant to a loan.
        /// </summary>
        /// <param name="loanId">Loan Data GUID ID.</param>
        /// <returns>The id of new applicant.</returns>
        public static Guid AddApplication(Guid loanId)
        {
            CPageData dataLoan = ApplicantService.GetSaveDataLoan(loanId);
            
            int appIndex = dataLoan.AddNewApp();
            return dataLoan.GetAppData(appIndex).aAppId;
        }

        /// <summary>
        /// Delete a whole applicant.
        /// </summary>
        /// <param name="loanId">Loan Data GUID ID.</param>
        /// <param name="appId">Applicant data GUID ID.</param>
        public static void DeleteApplicant(Guid loanId, Guid appId)
        {
            CPageData dataLoan = ApplicantService.GetSaveDataLoan(loanId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.DelApp(appId);
        }

        /// <summary>
        /// Swap borrower and coborrwer info of an applicant.
        /// </summary>
        /// <param name="loanId">Loan Data GUID ID.</param>
        /// <param name="appId">Applicant data GUID ID.</param>
        public static void SwapBorrowerCoborrower(Guid loanId, Guid appId)
        {
            CPageData dataLoan = new CSwapCoborrowerData(loanId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAppData dataApp = dataLoan.GetAppData(appId);
            dataApp.SwapMarriedBorAndCobor();
            dataLoan.Save();
        }

        /// <summary>
        /// Delete coborrower of an applicant.
        /// </summary>
        /// <param name="loanId">Loan Data GUID ID.</param>
        /// <param name="appId">Applicant data GUID ID.</param>
        public static void DeleteCoborrower(Guid loanId, Guid appId)
        {
            CPageData dataLoan = ApplicantService.GetSaveDataLoan(loanId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            CAppData dataApp = dataLoan.GetAppData(appId);
            dataApp.DelMarriedCobor();
            dataLoan.Save();
        }

        /// <summary>
        /// Get loan application from a Loan Id.
        /// </summary>
        /// <param name="loanId">Loan Data GUID ID.</param>
        /// <returns>Loan application object.</returns>
        private static CPageData GetSaveDataLoan(Guid loanId)
        {
            return new CPageData(loanId, ApplicantService.FieldIds);
        }
    }
}