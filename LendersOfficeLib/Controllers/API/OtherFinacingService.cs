﻿namespace LendersOffice.Services
{
    using System;
    using DataAccess;
    using LendersOffice.Security;

    /// <summary>
    /// Services for OtherFinancing page.
    /// </summary>
    public class OtherFinancingService : AbstractLendingQBService
    {
        /// <summary>
        /// Unlink the sub financing loan.
        /// </summary>
        /// <param name="loanId">The loan ID sLId.</param>
        public static void UnlinkLoan(Guid loanId)
        {
            // 6/29/2005 kb - We now (case 2218) secure delete requests, so
            // get the calling user's credentials and check.
            Tools.UnlinkLoans(BrokerUserPrincipal.CurrentPrincipal, loanId);
        }

        /// <summary>
        /// Update the sub financing loan.
        /// </summary>
        /// <param name="loanId">The loan ID sLId.</param>
        /// <returns>The feedback to user.</returns>
        public static string UpdateLinkLoan(Guid loanId)
        {
            var brokerDB = BrokerUserPrincipal.CurrentPrincipal.BrokerDB;
            var linkedLoanUpdater = new CPopulate80To20(brokerDB, loanId, Tools.GetLinkedLoanId(brokerDB.BrokerID, loanId));
            try
            {
                linkedLoanUpdater.UpdateLinkedLoans();
                linkedLoanUpdater.AddMsgToUser("The linked loan has been updated. Please verify its data", false);
            }
            catch (CBaseException ex)
            {
                linkedLoanUpdater.AddMsgToUser("Update Error.", false);
                Tools.LogError("UpdateLinkLoan Error: " + ex.Message, ex);
            }

            return linkedLoanUpdater.MsgToUser;
        }
    }
}
