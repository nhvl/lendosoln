﻿namespace LendersOffice.Services
{
    using System.Linq;
    using DataAccess;
    using DataAccess.Utilities;
    using LendersOffice.Security;

    /// <summary>
    /// Services for Loan Status.
    /// </summary>
    public class LoanStatusService : AbstractLendingQBService
    {
        /// <summary>
        /// Get list of Loan Status.
        /// </summary>
        /// <param name="channel">Branch Channel.</param>
        /// <param name="process">Correspondent Process.</param>
        /// <returns>A list of Loan Statuses.</returns>
        public static object GetLoanStatus(E_BranchChannelT channel = E_BranchChannelT.Blank, E_sCorrespondentProcessT process = E_sCorrespondentProcessT.Blank)
        {
            return new
            {
                loan = LoanStatus.GetLoanStatusWithDesc(
                        channel, 
                        process,
                        PrincipalFactory.CurrentPrincipal.BrokerId)
                    .Select(p => new { label = p.Value, value = p.Key })
                    .ToArray(),
                lead = LoanStatus.GetLeadStatusWithDesc()
                    .Select(p => new { label = p.Value, value = p.Key })
                    .ToArray(),
            };
        }
    }
}
