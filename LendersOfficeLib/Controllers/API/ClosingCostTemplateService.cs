﻿namespace LendersOffice.Services
{
    using System;
    using DataAccess.ClosingCostAutomation;
    using DataAccess.Utilities;

    /// <summary>
    /// Services for Closing Cost.
    /// </summary>
    public class ClosingCostTemplateService : AbstractLendingQBService
    {
        /// <summary>
        /// Get Closing Cost List.
        /// </summary>
        /// <param name="loanId">Loan Id (sLId).</param>
        /// <param name="gfeVersion">GFE Version.</param>
        /// <returns>A list of Closing Cost records.</returns>
        public static CCTemplateDetails[] GetClosingCost(Guid loanId, int gfeVersion)
        {
            return ClosingCostTemplate.GetClosingCost(loanId, gfeVersion);
        }

        /// <summary>
        /// Apply Closing Cost template.
        /// </summary>
        /// <param name="loanId">Loan Id (sLId).</param>
        /// <param name="templateId">Closing Cost Template ID.</param>
        public static void ApplyClosingCostTemplate(Guid loanId, Guid templateId)
        {
            ClosingCostTemplate.ApplyClosingCostTemplate(loanId, templateId);
        }
    }
}
