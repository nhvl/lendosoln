﻿namespace LendersOffice.Services
{
    using System.Collections.Generic;

    /// <summary>
    /// An item that will be displayed in the New Ui's navbar.
    /// </summary>
    public class LoanNavigationItem
    {
        /// <summary>
        /// Gets or sets the name of the folder or page.  How it is displayed in the Ui.
        /// </summary>
        /// <value>The display name of the item.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the title of the page.
        /// </summary>
        /// <value>The title of the page.</value>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the id of the item.
        /// </summary>
        /// <value>The id of the item.</value>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the item is visible in the UI.  If it is not visible, it won't be included in the navigation JSON.
        /// </summary>
        /// <value>A bool indicating the item's visibility.</value>
        public bool IsVisible { get; set; } = true;

        /// <summary>
        /// Gets or sets the items parent id.
        /// </summary>
        /// <value>The item's parent's id.</value>
        public string ParentId { get; set; }

        /// <summary>
        /// Gets or sets the items parent path.
        /// </summary>
        /// <value>The item's parent path for the folder.</value>
        public string ParentPath { get; set; }

        /// <summary>
        /// Gets or sets the action performed when clicking the link in the old Ui.
        /// </summary>
        /// <value>The OnClick action.</value>
        public string OnClick { get; set; }

        /// <summary>
        /// Gets or sets the route used to determine what state to go to when clicking it in the new UI.
        /// </summary>
        /// <value>The route to navigate to.</value>
        public string Route { get; set; }

        /// <summary>
        /// Gets or sets the list of items contained by this folder.
        /// </summary>
        /// <value>The folder's children.</value>
        public List<LoanNavigationItem> Children { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the folder is open or not.
        /// </summary>
        /// <value>A boolean indicating whether the folder is open or not.</value>
        public bool IsExpand { get; set; }

        /// <summary>
        /// Gets or sets the full path of the folder.  Used when setting isExpand.
        /// </summary>
        /// <value>The folder's path.  Matches the old UI's.</value>
        public string FullPath { get; set; }

        /// <summary>
        /// Gets or sets the Items type.
        /// </summary>
        /// <value>The type.  Valid values are either 'page' or 'folder'.</value>
        public string ItemType { get; set; }
    }
}
