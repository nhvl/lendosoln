﻿namespace LendersOffice.Services
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.DocMagicLib;
    using LendersOffice.Security;

    /// <summary>
    /// Services for Applicant updating.
    /// </summary>
    public class AlternateLenderService : AbstractLendingQBService
    {
        /// <summary>
        /// Delete lender by lender ID.
        /// </summary>
        /// <param name="lenderId">Alternate lender GUID ID.</param>
        public static void DeleteLender(Guid lenderId)
        {
            if (lenderId == Guid.Empty)
            {
                return;
            }

            DocMagicAlternateLender.DeleteAlternateLenderId(BrokerUserPrincipal.CurrentPrincipal.BrokerId, lenderId);
        }
    }
}