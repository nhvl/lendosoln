﻿namespace LendersOffice.Services
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// Services for Loan Editor's navigation.
    /// </summary>
    public class NavigationService : AbstractLendingQBService
    {
        /// <summary>
        /// Gets the dictionary that contains an Old UI's page ID, and returns the routing path.  Many of the pages do not have a valid route, but are still included
        /// so that the navigation bar renders them.
        /// </summary>
        /// <value>A Dictionary with the page mappings.</value>
        public static Dictionary<string, string> PageMapping { get; } =
            new Dictionary<string, string>()
            {
                { "Page_EmployeeResources", string.Empty },
                { "Page_ImportFromDoDu", string.Empty },
                { "Page_ExportPoint", string.Empty },
                { "Page_FannieAddendum", string.Empty },
                { "Page_ExportLPQ", string.Empty },
                { "Page_JetDocs", string.Empty },
                { "Page_MismoClosing26", string.Empty },
                { "Page_MismoClosing33", string.Empty },
                { "Page_MismoClosing30", string.Empty },
                { "Page_FHLMCLQA", string.Empty },
                { "Page_ExportCustomForm", string.Empty },
                { "Page_Sandbox", string.Empty },
                { "Page_SandboxList", string.Empty },
                { "Page_Duplicate", string.Empty },
                { "Page_CreateTest", string.Empty },
                { "Page_Print", string.Empty },
                { "Page_CreateSecondLoan", string.Empty },
                { "Page_CreateTemplate", string.Empty },
                { "Page_LoanTemplateSettings", string.Empty },
                { "Page_InternalSupportPage", string.Empty },
                { "Page_DataLayerMigration", string.Empty },
                { "Page_ManageNewFeatures", string.Empty },
                { "Page_TestConfig", string.Empty },
                { "Page_Close", string.Empty },
                { "Page_QualifiedLoanProgramSearch", string.Empty },
                { "Page_PMLDefaultValues", string.Empty },
                { "Page_Certificate", string.Empty },
                { "Page_PmlReviewInfo", string.Empty },
                { "Page_ViewBrokerNotes", string.Empty },
                { "Page_CheckEligibility", string.Empty },
                { "Page_RunInternalPricing", string.Empty },
                { "Page_InvestorRateLock", string.Empty },
                { "Page_LockInConfirmation", string.Empty },
                { "QM", string.Empty },
                { "QM2015", string.Empty },
                { "Page_LeadAssignment", string.Empty },
                { "Page_StatusAgents", "statusAgents" },
                { "Page_NewStatusConditions", string.Empty },
                { "Page_StatusConditions", string.Empty },
                { "Page_CreateNewTask", string.Empty },
                { "Page_TaskList", string.Empty },
                { "Page_NewTaskList", string.Empty },
                { "Page_BorrowerEmployments", string.Empty },
                { "Page_CoborrowerEmployments", string.Empty },
                { "Page_LoanTerms", "loanTerms" },
                { "Page_VOD", string.Empty },
                { "Page_VOE", string.Empty },
                { "Page_VerbalVoe", string.Empty },
                { "Page_VOM", string.Empty },
                { "Page_VOL", string.Empty },
                { "Page_VOLand", string.Empty },
                { "Page_LenderCredits", "lenderCredits" },
                { "Page_2010GFE", string.Empty },
                { "Page_2010GFEArchive", string.Empty },
                { "Page_EscrowImpoundsSetup", "escrowImpoundsSetup" },
                { "Page_TIL", "truthInLending" },
                { "Page_ChangeOfCircumstances", string.Empty },
                { "Page_ToleranceCure", string.Empty },
                { "Page_PropHousingExpenses", string.Empty },
                { "Page_LoanEstimate", string.Empty },
                { "Page_LoanEstimateArchive", string.Empty },
                { "Page_SellerClosingDisclosure", string.Empty },
                { "Page_SellerClosingDisclosureArchive", string.Empty },
                { "Page_AdjustmentsAndOtherCredits", string.Empty },
                { "Page_ClosingDisclosure", string.Empty },
                { "Page_ClosingDisclosureArchive", string.Empty },
                { "Page_ChangeOfCircumstancesNew", "changeOfCircumstancesNew" },
                { "Page_Compliance", string.Empty },
                { "Page_FeeAudit", string.Empty },
                { "RActiveNewGfe", string.Empty },
                { "RActiveNewGfeArchive", string.Empty },
                { "Page_SellerClosingCosts", string.Empty },
                { "Page_SellerClosingCostsArchive", string.Empty },
                { "Page_UnderwritingConditions", string.Empty },
                { "Page_UnderwritingConditionsForTask", string.Empty },
                { "Page_ApprovalLetter", string.Empty },
                { "Page_SuspenseNotice", string.Empty },
                { "Page_ConditionSignoff", string.Empty },
                { "Page_CommunityLending", string.Empty },
                { "Page_ExportDu", string.Empty },
                { "Page_ViewDUFindings", string.Empty },
                { "Page_ExportFreddie", string.Empty },
                { "Page_ViewLpFeedback", string.Empty },
                { "FHATotalAudit", string.Empty },
                { "FHATOTALFindings", string.Empty },
                { "Page_FannieMaeEarlyCheck", string.Empty },
                { "Page_FreddieLoanQualityAdvisorExport", string.Empty },
                { "Page_AuditHistory", string.Empty },
                { "Page_ComplianceEagle", string.Empty },
                { "Page_ComplianceEase", string.Empty },
                { "Page_DataVerifyDRIVE", string.Empty },
                { "Page_SettlementCharges", string.Empty },
                { "Page_SettlementChargesMigration", string.Empty },
                { "Page_AdditionalHUD1Data", string.Empty },
                { "Page_TitleAndVesting", string.Empty },
                { "SellerInfo", string.Empty },
                { "Page_SecurityInstrument", string.Empty },
                { "WarehouseLenderRolodexInfo", string.Empty },
                { "Page_FundingCloser", string.Empty },
                { "Page_LoanRecording", string.Empty },
                { "Page_FloodCertification", string.Empty },
                { "MULTI_DOC_VENDORS", string.Empty },
                { "Page_DocMagic", string.Empty },
                { "Page_DocuTech", string.Empty },
                { "Page_DocuTech_1", string.Empty },
                { "Page_HazardInsurancePolicy", string.Empty },
                { "Page_FloodInsurancePolicy", string.Empty },
                { "Page_WindstormInsurancePolicy", string.Empty },
                { "CondoHO6InsurancePolicy", string.Empty },
                { "Page_PurchaseAdvice_Purchasing", string.Empty },
                { "Page_Disbursement_Purchasing", string.Empty },
                { "Page_TrailingDocuments", string.Empty },
                { "Page_SecondaryStatus", string.Empty },
                { "InvestorRolodexInfo", string.Empty },
                { "Page_GSEDelivery", string.Empty },
                { "Page_PoolAssignment", string.Empty },
                { "Page_PurchaseAdvice", string.Empty },
                { "Page_Disbursement", string.Empty },
                { "Page_Transactions", string.Empty },
                { "Page_OriginatorCompensation", string.Empty },
                { "Page_LoanOfficerPricingPolicy", string.Empty },
                { "Page_Accounting", string.Empty },
                { "Page_LoanRepurchase", string.Empty },
                { "Page_Servicing", string.Empty },
                { "Page_SubservicingTransfer", string.Empty },
                { "PayoffStatement", string.Empty },
                { "Page_MortgageInsurancePolicy", string.Empty },
                { "DocVendorPortal", string.Empty },
                { "Documents", string.Empty },
                { "Document_Requests", string.Empty },
                { "Upload_Documents", string.Empty },
                { "Fax_Documents", string.Empty },
                { "Ship_Documents", string.Empty },
                { "Page_FHAConnection", string.Empty },
                { "Page_FHAConnectionResults", string.Empty },
                { "Page_FHAAddendum", string.Empty },
                { "Page_FHATransmittal", string.Empty },
                { "Page_FHATransmittalCombined", string.Empty },
                { "Page_FHAStreamlineRefiNetTangibleBenefit", string.Empty },
                { "Page_FHA_92700", string.Empty },
                { "Page_FHA_54114", string.Empty },
                { "Page_FHA_92800_5B", string.Empty },
                { "Page_FHAPropertyImprovement", string.Empty },
                { "Page_FHAAppraisedValueDisclosure", string.Empty },
                { "Page_VAAddendum", string.Empty },
                { "Page_VA_26_1820", string.Empty },
                { "Page_VA_26_6393", string.Empty },
                { "Page_VALoanComparison", string.Empty },
                { "Page_VA_26_0286", string.Empty },
                { "Page_VA_26_8923", string.Empty },
                { "Page_VA_26_0285", string.Empty },
                { "Page_VA_26_8937", string.Empty },
                { "Page_VA_26_8261a", string.Empty },
                { "Page_VA_26_1880", string.Empty },
                { "Page_VA_26_1805", string.Empty },
                { "Page_1003_1", "loan1003page1" },
                { "Page_1003_2", "loan1003page2" },
                { "Page_1003_3", "loan1003page3" },
                { "Page_1003_4", "loan1003page4" },
                { "Page_TIL32", "truthInLending32" },
                { "Page_CAMLDS_1", string.Empty },
                { "Page_CAMLDS_2", string.Empty },
                { "Page_CARE885_1", string.Empty },
                { "Page_CARE885_2", string.Empty },
                { "Page_CARE885_3", string.Empty },
                { "Page_CARE885_4", string.Empty },
                { "Page_ARMProgramDisclosure", string.Empty },
                { "Page_TaxReturn4506T", "taxReturn" },
                { "Page_LoanSubmissions", "loanSubmission" },
                { "Page_CreditDenial", "creditDenial" },
                { "Page_AggregateEscrow", "aggregateEscrowDisclosure" },
                { "Page_SafeHarborDisclosureAdjustable", string.Empty },
                { "Page_SafeHarborDisclosureFixed", string.Empty },
                { "Page_BorrowerAuthorizationForm", string.Empty },
                { "Page_BorrowerCertificationForm", string.Empty },
                { "Page_CAImpoundStatement", string.Empty },
                { "Page_RealEstateDisclosure", string.Empty },
                { "Page_CreditScoreDisclosure", string.Empty },
                { "Page_DocMagicLoanOptionsSafeHarborDisclosure", string.Empty },
                { "Page_EqualCredit", string.Empty },
                { "Page_FloodHazardNotice", "floodHazardNotice" },
                { "Page_GeorgiaDisclosure", string.Empty },
                { "Page_GoodbyeLetter", "goodbyeLetter" },
                { "Page_MortgageLoanCommitment", "loanCommitment" },
                { "Page_MortgageLoanOriginationAgreement", "mortgageLoanOriginationAgreement" },
                { "Page_PatriotActDisclosure", string.Empty },
                { "Page_PrivacyPolicyDisclosure", string.Empty },
                { "Page_AppraisalDisclosure", string.Empty },
                { "Page_ServicingDisclosure2009", string.Empty },
                { "Page_TXDisclosureMultipleRoles", string.Empty },
                { "Page_TXMortgageBrokerDisclosure", "txMortgageBrokerDisclosure" },
                { "Page_UsdaLoanConditionalCommitment", "usdaLoanConditionalCommitment" },
                { "Page_UsdaLoanGuarantee", "usdaLoanGuarantee" },
                { "Page_WADisclosure", string.Empty },
                { "Page_RequestAppraisal", "requestForAppraisal" },
                { "Page_RequestInsurance", "requestForInsurance" },
                { "Page_RequestTitle", "requestForTitle" },
                { "Page_SurveyRequest", "surveyRequest" },
                { "Page_OrderCredit", string.Empty },
                { "Page_ViewCredit", string.Empty },
                { "Page_DataTracInterface", string.Empty },
                { "Page_BlitzDocsEfolder", string.Empty },
                { "Page_BlitzDocsEfolder2", string.Empty },
                { "Page_DUSubmission", string.Empty },
                { "Page_SubmitToOSI", string.Empty },
                { "Page_SubmitToThirdParty", string.Empty },
                { "Page_SubmitToMiser", string.Empty },
                { "Page_TitleQuoteRequest", string.Empty },
                { "Page_ImportFromSymitar", string.Empty },
                { "Page_Order4056T", string.Empty },
                { "Page_GlobalDMSOrderAppraisal", string.Empty },
                { "Page_OrderMIPolicy", string.Empty },
                { "Generic_Framework_Vendors", string.Empty },
                { "Page_FHA_92900_PUR", string.Empty },
                { "Page_FHA_92900_PUR_Combined", string.Empty },
                { "Page_FHA_92900_WS", string.Empty },
                { "Page_FHA_92900_WS_Combined", string.Empty },
                { "Page_Funding", string.Empty },
                { "Page_CreditScores", "creditScores" },
                { "Page_BorrowerList", "borrowerList" },
                { "Page_BrokerRateLock", "brokerRatelock" },
                { "Page_StatusGeneral",  "statusGeneral" },
                { "Page_StatusProcessing",  "statusProcessing" },
                { "Page_CustomFields",  "customFields" },
                { "Page_CustomFields2",  "customFields2" },
                { "Page_CustomFields3",  "customFields3" },
                { "Page_HomeownerCounselingOrganizationList", "hcoList" },
                { "Page_SettlementServiceProviderList", "settlementSpl" },
                { "Page_StatusHMDA",  "statusHmda" },
                { "Page_StatusTrustAccounts",  "statusTrustAccount" },
                { "Page_NMLSCallReport",  "nmlsCallReport" },
                { "Page_ViewBrokerRateLock",  "viewBrokerRateLock" },
                { "Page_LendingStaffNotes",  "lendingStaffNotes" },
                { "Page_LoanInformation", "loanInfo" },
                { "Page_BigLoanInformation", "loanInfo" },
                { "Page_AdditionalHELOC", "additionalHeloc" },
                { "ConstructionLoanInfo", "constructionLoanInfo" },
                { "Page_PropertyInformation", "propertyInfo" },
                { "Page_SubjectPropertyInvestment", "propertyRentalIncome" },
                { "Page_RefiConstructionLoan", "refiConstructionLoan" },
                { "Page_BorrowerInformation", "borrowerInfo" },
                { "Page_TitleAndEstate", "titleAndEstate" },
                { "Page_BorrowerMonthlyIncome", "monthlyIncome" },
                { "Page_Subfinancing", "otherFinancing" },
                { "Page_HousingExpense", "presHouseExpense" },
                { "Page_BorrowerAssets", "borrowerAsset" },
                { "Page_BorrowerREO", "reo" },
                { "Page_BorrowerLiabilities", "borrowerLiability" },
                { "Page_Declarations", "declarations" },
                { "Page_GovernmentMonitoringData", "governmentMonitoringData" },
                { "Page_LoanOfficerLicensingInfo", "loanOfficerLicensingInfo" },
                { "Page_ContinuationData", "continuationData" },
                { "Page_UpfrontMIP", "upfrontMipff" },
                { "Page_DetailsofTransaction", "detailsOfTransaction" },
                { "Page_PropertyDetails", "propertyDetail" },
                { "Page_1008_04", "transmittal" },
                { "Page_1008_04Combined", "transmittalCombined" },
                { "Page_DisclosureCenter", "disclosureCenter" },
                { "Page_BorrowerClosingCosts", "borrowerClosingCosts" },
                { "Page_StatusCommissions", "statusCommission" },
                { "Batch_Editor", "editEdoc" },
            };

        /// <summary>
        /// Gets the dictionary that contains invisible pages.
        /// </summary>
        /// <value>A Dictionary that contains invisible pages.</value>
        private static Dictionary<string, string> InvisibleNavPages { get; } =
        new Dictionary<string, string>()
        {
                { "mortgageBrokerageBusinessContract", "Mortgage Brokerage Business Contract (FL Only)" }
        };

        /// <summary>
        /// Gets the dictionary that contains custom page title.
        /// </summary>
        /// <value>A Dictionary that contains custom page title.</value>
        private static Dictionary<string, string> TitleMapping { get; } =
        new Dictionary<string, string>()
        {
                { "Page_TIL32", "Truth In Lending Section 32" }
        };

        /// <summary>
        /// Recalculates the Navigation JSON.
        /// </summary>
        /// <param name="loanId">The id of the loan to calculate the navigation JSON for.</param>
        /// <returns>JSON containing the structure of the Navigatio JSON.</returns>
        public static LoanNavigationItem RefreshNavigationFromLoanId(Guid loanId)
        {
            NavigationVisibility visibilityEvaluator = new NavigationVisibility(loanId);
            return RefreshNavigationFromLoadedLoan(visibilityEvaluator);
        }

        /// <summary>
        /// Recalculates the Navigation JSON.
        /// </summary>
        /// <param name="visibilityEvaluator">The the evaluator that will determine an item's visibility logic.</param>
        /// <returns>JSON containing the structure of the Navigatio JSON.</returns>
        public static LoanNavigationItem RefreshNavigationFromLoadedLoan(NavigationVisibility visibilityEvaluator)
        {
            var fileAddress = Tools.GetServerMapPath("~/newlos/LoanNavigationNew.xml.config");
            XmlDocument navigationDoc = new XmlDocument();
            navigationDoc.Load(fileAddress);

            LoanNavigationItem loanNavigation = ConvertXmlNodeToNavigationItem((XmlNode)navigationDoc.DocumentElement);

            EmployeeDB empDB = new EmployeeDB(PrincipalFactory.CurrentPrincipal.EmployeeId, PrincipalFactory.CurrentPrincipal.BrokerId);
            empDB.Retrieve();
            Dictionary<string, string> favorites = ObsoleteSerializationHelper.JsonDeserialize<Dictionary<string, string>>(empDB.FavoritePageIdsJSON);
            string loanFavoritesJson = string.Empty;

            List<string> specificFavorites = null;
            if (favorites.TryGetValue("loan", out loanFavoritesJson))
            {
                specificFavorites = ObsoleteSerializationHelper.JsonDeserialize<List<string>>(loanFavoritesJson);
            }

            ConstructNavigationModel(loanNavigation, visibilityEvaluator, specificFavorites);

            return loanNavigation;
        }

        /// <summary>
        /// This methid takes an XmlNode and converts it to a LoanNavigationItem.
        /// </summary>
        /// <param name="node">The node to convert the LoanNavigationItem from.</param>
        /// <returns>The item resulting from the given node.</returns>
        public static LoanNavigationItem ConvertXmlNodeToNavigationItem(XmlNode node)
        {
            LoanNavigationItem item = new LoanNavigationItem();

            item.Name = node.Attributes["name"]?.Value ?? string.Empty;
            item.Id = node.Attributes["id"]?.Value ?? string.Empty;
            item.ItemType = node.Name;

            if (node.ChildNodes.Count > 0)
            {
                item.Children = new List<LoanNavigationItem>();
                foreach (XmlNode child in node.ChildNodes)
                {
                    if (child.Name.Equals("item") || child.Name.Equals("folder"))
                    {
                        item.Children.Add(ConvertXmlNodeToNavigationItem(child));
                    }
                }
            }

            if (NavigationService.TitleMapping.Keys.Contains(item.Id))
            {
                item.Title = NavigationService.TitleMapping[item.Id];
            }

            return item;
        }

        /// <summary>
        /// Returns the Employee calling this service.
        /// </summary>
        /// <returns>The EmployeeDB object.</returns>
        public static EmployeeDB RetrieveEmployee()
        {
            EmployeeDB employee = new EmployeeDB(PrincipalFactory.CurrentPrincipal.EmployeeId, PrincipalFactory.CurrentPrincipal.BrokerId);
            employee.Retrieve();
            return employee;
        }

        /// <summary>
        /// Retrieves a dictionary containing all of the users favorites..
        /// </summary>
        /// <param name="employee">The Employee to retrieve the favorites from.</param>
        /// <returns>A Dictionary mapping a source, such as lead or loan, to its corresponding favorites JSON.</returns>
        public static Dictionary<string, string> RetrieveFavorites(EmployeeDB employee)
        {
            Dictionary<string, string> favorites = ObsoleteSerializationHelper.JsonDeserialize<Dictionary<string, string>>(employee.FavoritePageIdsJSON);
            if (favorites == null)
            {
                favorites = new Dictionary<string, string>();
            }

            return favorites;
        }

        /// <summary>
        /// Retrives a list of all the id's contained within source's favorits.
        /// </summary>
        /// <param name="favorites">A Dictionary containing all the user's favorites.</param>
        /// <param name="source">The source to retrieve the favorites for, such as "lead" or "loan".</param>
        /// <returns>A list of ids for each page that belongs in the favorites folder.</returns>
        public static List<string> RetrieveSpecificFavorites(Dictionary<string, string> favorites, string source)
        {
            string favoritesJSON;
            favorites.TryGetValue(source, out favoritesJSON);

            favoritesJSON = favoritesJSON ?? string.Empty;

            List<string> specificFavorites = ObsoleteSerializationHelper.JsonDeserialize<List<string>>(favoritesJSON);

            return (specificFavorites == null) ? new List<string>() : specificFavorites.Distinct().ToList();
        }

        /// <summary>
        /// This method saves the provided favorites into the given source for the given employee.
        /// </summary>
        /// <param name="employee">The Employee whose favorites will be modified.</param>
        /// <param name="favorites">The Employee's entire list of favorites.</param>
        /// <param name="source">The source to save the favorites to.</param>
        /// <param name="specificFavorites">The favorites that will be saved.</param>
        public static void SaveFavorites(EmployeeDB employee, Dictionary<string, string> favorites, string source, List<string> specificFavorites)
        {
            string favoritesJSON = ObsoleteSerializationHelper.JsonSerialize(specificFavorites);

            favorites[source] = favoritesJSON;

            employee.FavoritePageIdsJSON = ObsoleteSerializationHelper.JsonSerialize(favorites);
            employee.Save();
        }

        /// <summary>
        /// This method updates the favorites.  It can add, remove, or move the favorites location.
        /// </summary>
        /// <param name="source">The source of favorites to modify.</param>
        /// <param name="pageId">The favorite page that is being modified.</param>
        /// <param name="action">The action to perform.  Can be either add, remove, or move.</param>
        /// <param name="previousPageId">The favorite insert the modified favorite in front of.  Only needed during move operations.</param>
        public static void UpdateFavorites(string source, string pageId, string action, string previousPageId)
        {
            EmployeeDB employee = RetrieveEmployee();
            Dictionary<string, string> favorites = RetrieveFavorites(employee);
            List<string> specificFavorites = RetrieveSpecificFavorites(favorites, source);

            switch (action)
            {
                case "add":
                    if (!specificFavorites.Contains(pageId))
                    {
                        specificFavorites.Add(pageId);
                    }

                    break;
                case "remove":
                    if (specificFavorites.Contains(pageId))
                    {
                        specificFavorites.Remove(pageId);
                    }

                    break;
                case "move":

                    specificFavorites.Remove(pageId);
                    if (string.IsNullOrEmpty(previousPageId))
                    {
                        specificFavorites.Add(pageId);
                    }
                    else
                    {
                        int indexOfLast = specificFavorites.IndexOf(previousPageId);

                        if (indexOfLast < 0)
                        {
                            specificFavorites.Add(pageId);
                        }
                        else
                        {
                            specificFavorites.Insert(indexOfLast, pageId);
                        }
                    }

                    break;
                default:
                    throw new Exception("An unhandled operation was given when updating Favorites.");
            }

            SaveFavorites(employee, favorites, source, specificFavorites);
        }

        /// <summary>
        /// Retrieves the xml containing the Navigation's expand layout.
        /// </summary>
        /// <returns>XML containing the navigation's expand layout.</returns>
        public static string RetrieveMenuLayoutXml()
        {
            string xml = string.Empty;
            SqlParameter[] parameters =
                {
                    new SqlParameter("@UserID", PrincipalFactory.CurrentPrincipal.UserId)
                };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(PrincipalFactory.CurrentPrincipal.ConnectionInfo, "RetrieveLoanEditorMenuTreeState", parameters))
            {
                if (reader.Read())
                {
                    xml = (string)reader["LoanEditorMenuTreeStateXmlContent"];
                }
            }

            return xml;
        }

        /// <summary>
        /// Retrieves a HashTable containing the folder's expand settings.
        /// </summary>
        /// <returns>A HashTable containing the folder's expand settings.</returns>
        public static Dictionary<string, bool> GetFolderSettings()
        {
            Dictionary<string, bool> folderSettings = new Dictionary<string, bool>(StringComparer.OrdinalIgnoreCase);

            string xml = RetrieveMenuLayoutXml();

            if (string.IsNullOrWhiteSpace(xml))
            {
                return null;
            }

            XmlDocument doc = DataAccess.Tools.CreateXmlDoc(xml);

            if (doc == null)
            {
                return null;
            }

            foreach (XmlNode el in doc.SelectNodes("//folder"))
            {
                if (el.NodeType == XmlNodeType.Element)
                {
                    string name = ((XmlElement)el).GetAttribute("path");
                    bool collapse = string.Equals(((XmlElement)el).GetAttribute("collapse"), "true", StringComparison.OrdinalIgnoreCase);
                    if (!folderSettings.ContainsKey(name))
                    {
                        folderSettings.Add(name, collapse);
                    }
                }
            }

            return folderSettings;
        }

        /// <summary>
        /// Saves the given xml into the user's expand settings xml.
        /// </summary>
        /// <param name="newMenuSettingsXml">The menu settings to be saved.</param>
        public static void SaveMenuLayout(string newMenuSettingsXml)
        {
            string oldMenuSettingsXml = RetrieveMenuLayoutXml();

            if (!string.IsNullOrEmpty(newMenuSettingsXml) && !string.IsNullOrEmpty(oldMenuSettingsXml))
            {
                XmlDocument menuSettings = DataAccess.Tools.CreateXmlDoc(newMenuSettingsXml);
                XmlDocument oldMenuSettings = DataAccess.Tools.CreateXmlDoc(oldMenuSettingsXml);

                foreach (XmlNode node in oldMenuSettings.DocumentElement.ChildNodes)
                {
                    string attr = node.Attributes["path"].Value;
                    if (menuSettings.SelectNodes("/root/folder[@path=\"" + node.Attributes["path"].Value + "\"]").Count <= 0)
                    {
                        var importedNode = menuSettings.ImportNode(node, true);
                        menuSettings.DocumentElement.AppendChild(importedNode);
                    }
                }

                newMenuSettingsXml = menuSettings.OuterXml;
            }

            SqlParameter[] parameters =
                {
                    new SqlParameter("@UserID", BrokerUserPrincipal.CurrentPrincipal.UserId),
                    new SqlParameter("@LoanEditorMenuTreeStateXmlContent", newMenuSettingsXml)
                };
            StoredProcedureHelper.ExecuteNonQuery(BrokerUserPrincipal.CurrentPrincipal.BrokerId, "UpdateLoanEditorMenuTreeState", 5, parameters);
        }

        /// <summary>
        /// Takes a LoanNavigation item and constructs the navigatio model.
        /// </summary>
        /// <param name="nav">The LoanNavigation to be constructed.</param>
        /// <param name="visibilityEvaluator">The the evaluator that will determine an item's visibility logic.</param>
        /// <param name="specificFavorites">The list of favorites the user will be using.</param>
        private static void ConstructNavigationModel(LoanNavigationItem nav, NavigationVisibility visibilityEvaluator, List<string> specificFavorites)
        {
            List<LoanNavigationItem> visibleItems = new List<LoanNavigationItem>();
            LoanNavigationItem favoritesFolder = null;
            Dictionary<string, bool> folderSettings = NavigationService.GetFolderSettings();
            Dictionary<string, LoanNavigationItem> favoriteChildren = new Dictionary<string, LoanNavigationItem>();

            foreach (LoanNavigationItem item in nav.Children)
            {
                CheckVisibility(item, visibilityEvaluator, specificFavorites, favoriteChildren, folderSettings);
                if (item.IsVisible)
                {
                    visibleItems.Add(item);
                }

                if (item.Id.Equals("Folder_Favorites"))
                {
                    favoritesFolder = item;
                    favoritesFolder.Children = new List<LoanNavigationItem>();
                }
            }

            nav.Children = visibleItems;

            if (favoritesFolder != null)
            {
                foreach (string key in specificFavorites)
                {
                    LoanNavigationItem item;

                    if (favoriteChildren.TryGetValue(key, out item))
                    {
                        favoritesFolder.Children.Add(item);
                    }
                }
            }

            nav.Children.Add(NavigationService.GetNoRoutingFolder());
        }

        /// <summary>
        /// Computes all the logic for the given LoanNavigationItems.  It adds folders to favorites, checks their visibility, maps the route values, and constructs their path values.
        /// </summary>
        /// <param name="item">The item to be constructed.</param>
        /// <param name="visibilityEvaluator">The the evaluator that will determine an item's visibility logic.</param>
        /// <param name="specificFavorites">The list of favorites ids the user is using.</param>
        /// <param name="favoriteChildren">The current list of favorites objects that will be added to the favorites folder.</param>
        /// <param name="folderCollapseSettings">The Hashtable of a the folder's expand settings.</param>
        private static void CheckVisibility(LoanNavigationItem item, NavigationVisibility visibilityEvaluator, List<string> specificFavorites, Dictionary<string, LoanNavigationItem> favoriteChildren, Dictionary<string, bool> folderCollapseSettings)
        {
            visibilityEvaluator.TreeNode_Create(item);

            if (string.Equals(item.ItemType, "folder"))
            {
                if (!string.IsNullOrEmpty(item.ParentId))
                {
                    item.FullPath = item.ParentPath + "/" + item.Name;
                }
                else
                {
                    item.FullPath = "/" + item.Name;
                }

                bool isCollapsed = false;
                if (folderCollapseSettings.TryGetValue(item.FullPath, out isCollapsed))
                {
                    item.IsExpand = !isCollapsed;
                }

                if (item.Children != null)
                {
                    List<LoanNavigationItem> visibleItems = new List<LoanNavigationItem>();
                    foreach (LoanNavigationItem child in item.Children)
                    {
                        child.ParentId = item.Id;
                        child.ParentPath = item.FullPath;
                        CheckVisibility(child, visibilityEvaluator, specificFavorites, favoriteChildren, folderCollapseSettings);

                        if (child.IsVisible)
                        {
                            visibleItems.Add(child);
                            if (child.ItemType == "item")
                            {
                                string value = string.Empty;

                                if (NavigationService.PageMapping.TryGetValue(child.Id, out value))
                                {
                                    child.Route = value;
                                    if (string.IsNullOrEmpty(child.Route))
                                    {
                                        child.Route = "TODO";
                                    }
                                }
                                else
                                {
                                    Tools.LogError(child.Id + " :The following item ID does not have a corresponding route value.");
                                }
                            }
                        }
                    }

                    item.Children = visibleItems;
                }
            }
            else
            {
                if (specificFavorites != null && specificFavorites.Contains(item.Id) && !favoriteChildren.ContainsKey(item.Id))
                {
                    favoriteChildren.Add(item.Id, item);
                }

                if (string.IsNullOrEmpty(item.Route))
                {
                    item.Route = "TODO";
                }
            }
        }

        /// <summary>
        /// Get list of inivisible navigation items.
        /// </summary>
        /// <returns>List of inivisible navigation items.</returns>
        private static LoanNavigationItem GetNoRoutingFolder()
        {
            var navFolder = new LoanNavigationItem()
            {
                ItemType = "folder",
                Name = "NoRoutingFolder",
                IsVisible = false,
                Children = new List<LoanNavigationItem>()
            };

            foreach (var keyValPair in NavigationService.InvisibleNavPages)
            {
                navFolder.Children.Add(new LoanNavigationItem()
                {
                        ItemType = "file",
                        IsVisible = false,
                        Route = keyValPair.Key,
                        Name = keyValPair.Value,
                });
            }

            return navFolder;
        }
    }
}
