﻿namespace LendersOffice.Services
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// Services for HMDA page.
    /// </summary>
    public class HMDAService : AbstractLendingQBService
    {
        /// <summary>
        /// Get Set Denied by service.
        /// </summary>
        /// <returns>Services data.</returns>
        public static List<KeyValuePair<string, string>> SetDeniedBy()
        {
            return new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("sHmdaLoanDenied", bool.TrueString),
                new KeyValuePair<string, string>("sRejectD", System.DateTime.Today.ToString("d")),
                new KeyValuePair<string, string>("sHmdaLoanDeniedBy", PrincipalFactory.CurrentPrincipal.DisplayName)
            };
        }

        /// <summary>
        /// Retrieve GeoCode.
        /// </summary>
        /// <param name="addr">The sSpAddr Address value.</param>
        /// <param name="city">The sSpCity City value.</param>
        /// <param name="county">The sSpCounty Country value.</param>
        /// <param name="state">The sSpState State value.</param>
        /// <param name="zip">The sSpZip ZipCode value.</param>
        /// <param name="hmdaActionD">The sHmdaActionD year of HMDA Action.</param>
        /// <returns>The GeoCode values.</returns>
        public static List<KeyValuePair<string, string>> ImportGeoCodes(string addr, string city, string county, string state, string zip, string hmdaActionD)
        {
            DateTime dt;
            if (!DateTime.TryParse(hmdaActionD, out dt))
            {
                dt = DateTime.Now;
            }

            var hmdaActionDYear = dt.Year.ToString();

            if (string.IsNullOrWhiteSpace(addr) || string.IsNullOrWhiteSpace(city) || string.IsNullOrWhiteSpace(county) || string.IsNullOrWhiteSpace(zip))
            {
                return new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("Error", ErrorMessages.GeoCode.EmptyAddress)
                };
            }

            var geoCodeContainer = new FFIECGeocode.Geocode(hmdaActionDYear, addr, city, state, zip);
            if (geoCodeContainer.IsError)
            {
                return new List<KeyValuePair<string, string>>()
                {
                    new KeyValuePair<string, string>("Error", geoCodeContainer.ErrorMessage)
                };
            }

            return new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("sHmdaMsaNum", geoCodeContainer.MsaCode.Equals("99999") ? "NA" : geoCodeContainer.MsaCode),
                new KeyValuePair<string, string>("sHmdaCountyCode", geoCodeContainer.CountyCode),
                new KeyValuePair<string, string>("sHmdaStateCode", geoCodeContainer.StateCode),
                new KeyValuePair<string, string>("sHmdaCensusTract", geoCodeContainer.TractCode)
            };
        }
    }
}
