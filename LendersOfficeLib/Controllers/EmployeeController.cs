﻿// <copyright file="EmployeeController.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Huy Nguyen
//  Date:   12/28/2017
// </summary>

namespace LendersOffice.Controllers
{
    using System;
    using System.Collections.Generic;
    using LendersOffice.Admin;
    using LendersOffice.Security;
    using LendersOffice.UI;

    /// <summary>
    /// This controller class is to faciliate loading and saving the employee in the new UI.
    /// </summary>
    public class EmployeeController : ReflectionLendingQBController
    {
        /// <summary>
        /// The main method.  It processes the op parameter and determines what method to call.
        /// </summary>
        /// <param name="op">The op should be the action that you're trying to perform.</param>
        /// <returns>Returns the serialized object.</returns>
        public override object Main(string op)
        {
            switch (op)
            {
                case "Load":
                    List<string> fieldList = this.GetRequestContent<List<string>>();
                    return this.LoadEmployee(fieldList, PrincipalFactory.CurrentPrincipal.BrokerDB.BrokerID, PrincipalFactory.CurrentPrincipal.EmployeeId);
                default:
                    throw new NotImplementedException("Unhandled Operation.");
            }
        }

        /// <summary>
        /// Loads the employee object.
        /// </summary>
        /// <param name="fieldList">The list of fields to be retrived from the pool.</param>
        /// <param name="brokerId">The broker db ID.</param>
        /// /// <param name="employeeId">The employee ID.</param>
        /// <returns>Returns the newly loaded object.</returns>
        public BasicModel LoadEmployee(IEnumerable<string> fieldList, Guid brokerId, Guid employeeId)
        {
            var employee = new EmployeeDB(employeeId, brokerId);
            employee.Retrieve();

            var model = new BasicModel();

            foreach (string fieldName in fieldList)
            {
                var fieldInput = InputModelProvider.GetInputPropertyModel(employee, fieldName);
                if (fieldInput != null)
                {
                    model.Fields[fieldName] = fieldInput;
                }
            }

            return model;
        }
    }
}