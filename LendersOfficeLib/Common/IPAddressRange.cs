﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;



namespace LendersOffice.Common
{
    // Took this class from http://stackoverflow.com/questions/2138706/how-to-check-a-input-ip-fall-in-a-specific-ip-range 
    // Took the idea that we could take classes off of SO from:
    // http://meta.stackoverflow.com/questions/139698/re-using-ideas-or-small-pieces-of-code-from-stackoverflow-com/
    
    /// <summary>
    /// Should work for IPv4 and IPv6 format addresses.
    /// <para> </para>
    /// Throws an ArgumentException if lower and upper are *NOT* in the same family.
    /// <para> </para>
    /// Throws an ArgumentException if lower > upper
    /// </summary>
    public class IPAddressRange
    {
        readonly IPAddress m_lowerIP;
        readonly IPAddress m_upperIP;

        public string LowerIP { get { return m_lowerIP.ToString(); } }
        public string UpperIP { get { return m_upperIP.ToString(); } }

        public override string  ToString()
        {
            return LowerIP + " to " + UpperIP;
        }

        /// <summary>
        /// Should be safe for IPv4 or IPv6 addresses.
        /// <para> </para>
        /// Throws an ArgumentException if lower and upper are *NOT* in the same family.
        /// <para> </para>
        /// Throws an ArgumentException if lower > upper
        /// </summary>
        public IPAddressRange(IPAddress lower, IPAddress upper)
        {
            m_lowerIP = lower;
            m_upperIP = upper;
            
            Initialize();
        }

        /// <summary>
        /// Should be safe for IPv4 or IPv6 addresses.
        /// <para> </para>
        /// Throws an ArgumentException if lower and upper are *NOT* in the same family.
        /// <para> </para>
        /// Throws an ArgumentException if lower > upper
        /// <para> </para>
        /// Throws a FormatException if the strings are bad Format.
        /// </summary>
        public IPAddressRange(string lower, string upper)
        {
            m_lowerIP = IPAddress.Parse(lower);
            m_upperIP = IPAddress.Parse(upper);

            Initialize();
        }
        private void Initialize()
        {
            if (m_lowerIP.AddressFamily != m_upperIP.AddressFamily)
            {
                var msg = "Can't make an ip address range with different types of ip addresses." + Environment.NewLine;
                msg += "lower was " + m_lowerIP.AddressFamily + " and upper was " + m_lowerIP.AddressFamily;
                throw new ArgumentException(msg);
            }
            if (m_lowerIP.CompareTo(m_upperIP) == -1)
            {
                var msg = "Can't make a range in which the first ip address is higher.";
                throw new ArgumentException(msg);
            }
        }
        
        /// <summary>
        /// safe (shouldn't throw any exceptions)
        /// </summary>
        public bool Contains(IPAddress address)
        {
            if (address == null)
            {
                return false;
            }

            if (address.AddressFamily != m_lowerIP.AddressFamily)
            {
                return false;
            }

            if(m_lowerIP.CompareTo(address) == -1 || address.CompareTo(m_upperIP) == -1)
            {
                return false;
            }

            return true;

        }
    }    

}
