﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.Common
{
    public class DependencySet<T>
    {
        private IEqualityComparer<T> m_comparer;

        private Dictionary<T, HashSet<T>> m_dictionary;

        private Dictionary<T, HashSet<T>> m_parentDictionary;

        public DependencySet()
        {
            m_comparer = null;
            m_dictionary = new Dictionary<T, HashSet<T>>(); // Use default comparer
            m_parentDictionary = new Dictionary<T, HashSet<T>>();
        }
        public DependencySet(IEqualityComparer<T> comparer)
        {
            m_comparer = comparer;
            m_dictionary = new Dictionary<T, HashSet<T>>(comparer);
            m_parentDictionary = new Dictionary<T, HashSet<T>>(comparer);
        }

        public void Add(T parent, T child)
        {
            Add(parent, new T[] { child });
        }
        public void Add(T parent, IEnumerable<T> childList)
        {
            HashSet<T> childSet = null;

            if (m_dictionary.TryGetValue(parent, out childSet) == false)
            {
                childSet = CreateSet();

                m_dictionary.Add(parent, childSet);
            }

            foreach (var child in childList)
            {
                childSet.Add(child);

                HashSet<T> parentSet = null;
                if (m_parentDictionary.TryGetValue(child, out parentSet) == false)
                {
                    parentSet = CreateSet();
                    m_parentDictionary.Add(child, parentSet);
                }
                parentSet.Add(parent);
            }
        }

        private HashSet<T> CreateSet()
        {
            if (m_comparer == null)
            {
                return new HashSet<T>();
            }
            else
            {
                return new HashSet<T>(m_comparer);
            }
        }

        public IEnumerable<T> GetDependsOn(IEnumerable<T> list)
        {
            HashSet<T> result = CreateSet();

            HashSet<T> visitedNode = CreateSet();


            Stack<T> stack = new Stack<T>(list);

            while (stack.Count > 0)
            {
                T o = stack.Pop();

                if (visitedNode.Contains(o) == false)
                {
                    HashSet<T> childList;
                    if (m_dictionary.TryGetValue(o, out childList) == true)
                    {
                        foreach (T child in childList)
                        {
                            result.Add(child);

                            if (visitedNode.Contains(child) == false)
                            {
                                stack.Push(child);
                            }
                        }
                    }
                    visitedNode.Add(o);
                }
            }
            return result;

        }

        public IEnumerable<T> GetUsedBy(IEnumerable<T> list)
        {
            HashSet<T> result = CreateSet();

            HashSet<T> visitedNode = CreateSet();


            Stack<T> stack = new Stack<T>(list);

            while (stack.Count > 0)
            {
                T o = stack.Pop();

                if (visitedNode.Contains(o) == false)
                {
                    HashSet<T> parentList;

                    if (m_parentDictionary.TryGetValue(o, out parentList) == true)
                    {
                        foreach (T parent in parentList)
                        {
                            result.Add(parent);
                            if (visitedNode.Contains(parent) == false)
                            {
                                stack.Push(parent);
                            }
                        }
                    }
                    /*
                    foreach (T key in m_dictionary.Keys)
                    {
                        HashSet<T> childList = m_dictionary[key];

                        if (childList.Contains(o) == true) 
                        {
                            result.Add(key);

                            if (visitedNode.Contains(key) == false)
                            {
                                stack.Push(key);
                            }
                        }



                    }
                     */
                    visitedNode.Add(o);
                }
            }
            return result;
        }


    }

}
