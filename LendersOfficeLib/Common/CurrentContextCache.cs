/// Author: David Dao

using System;
using System.Web;

namespace LendersOffice.Common
{
    /// <summary>
    /// Store/Retrieve object through a lifetime of HTTP request.
    /// </summary>
	public class CurrentContextCache
	{
        public static void Set(string key, object value) 
        {
            if (HttpContext.Current != null) 
            {
                HttpContext.Current.Items[key] = value;
            }
        }
        public static object Get(string key) 
        {
            if (HttpContext.Current != null) 
            {
                return HttpContext.Current.Items[key];
            }
            return null;
        }
	}
}
