using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;

using System.Linq;
using System.Text;
using LendersOffice.Constants;
using LendersOffice.HttpModule;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using LendersOffice.AntiXss;
using DataAccess;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using CommonProjectLib.Common.Lib;

namespace LendersOffice.Common
{

    public class BasePage : System.Web.UI.Page
    {
        protected virtual bool DisableViewState
        {
            get { return false; }
        }

        protected static string IncludeVersion = Guid.NewGuid().ToString("N");

        protected static string ReadonlyIncludeVersion
        {
            get
            {
                return IncludeVersion;
            }
        }

        private Dictionary<string, string> m_globalJsObject = new Dictionary<string, string>();
        private bool m_includeVbs = false;
        private List<ClientScriptData> m_initFunctionList = new List<ClientScriptData>();
        private string m_pageTitle;
        // 1/7/2004 dd - This pageid is used in loan edit section. Based on this id, I will be able to mark
        // or highlight current page for the tree view on the left hand side.
        private string m_pageID = "";
        private class ClientScriptData
        {
            public string Name;
            public string Script;
            public string Arguments;

            public ClientScriptData(string n, string s, string a)
            {
                Name = n;
                Script = s;
                Arguments = a;
            }
        }


        public BasePage()
            : base()
        {
            LogPerformanceMonitorItem("Page.Constructor");
        }

        protected virtual bool EnableJQueryInitCode
        {
            get
            {
                return false;
            }
        }
        
        /// <summary>
        /// IE requires the meta tag to be FIRST in head. If this method returns a meta
        /// the base page will add it on pre render. We should make it easier for dev to do this 
        /// </summary>
        /// <returns></returns>
        protected virtual E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return ConstStage.IsForceEdgeEverywhere ? E_XUAComaptibleValue.Edge : E_XUAComaptibleValue.NONE;
        }

        /// <summary>
        /// See ReportResult.aspx.cs and opm 205599
        /// </summary>
        protected virtual bool GoingToPdf
        {
            get
            {
                return false;
            }
        }

        protected virtual bool EnableIEOnlyCheck
        {
            get 
            {
                if (GoingToPdf) // if it's going to become a pdf on this request, don't check the ie version. see opm 205599
                {
                    return false;
                }

                HttpCookie cookie = Request.Cookies["DisabledIECheck"];
                if (cookie != null && cookie.Value == "True")
                {
                    // 6/21/2012 dd - Allow a secret way to by pass IE browser check in IE.
                    return false;
                }

                return true; 
            }
        }

        public string EDocsSite
        {
            get { return ConstSite.EDocsSite; }
        }

        public string VirtualRoot
        {
            get { return DataAccess.Tools.VRoot; }
        }
        public virtual string StyleSheet
        {
            get { return VirtualRoot + "/css/stylesheetnew.css"; }
        }

        public virtual string ImagesFolder
        {
            get { return VirtualRoot + "/images"; }
        }

        public string PageTitle
        {
            get { return m_pageTitle; }
            set { m_pageTitle = value; }
        }

        public string PageID
        {
            get { return m_pageID; }
            set { m_pageID = value; }
        }

        /// <summary>
        /// Check to see if any field had change since load. This
        /// will only work with custom input field.
        /// </summary>
        public bool IsUpdate
        {
            get {
                // I need to test for context == null here because, if open aspx in the designer
                // context will be null. Therefore without checking for null you can't open
                // the aspx page in Design mode. dd 3/26/03
                if (Context != null)
                    return Request.Form["IsUpdate"] == "1";
                else
                    return false;
            }
        }
        /// <summary>
        /// Set the location of the help file when "Help" is clicked or F1 press.
        /// DO NOT call this multiple time in one page life cycle. Only the 
        /// first value will be use.
        /// </summary>
        public string HelpFile
        {
            set { ClientScript.RegisterHiddenField("HelpFileUrl", value); }
        }

        public void SetEnabledPerformancePageSizeWarning(bool enabled)
        {

            PerformanceMonitorItem item = PerformanceMonitorItem.Current;
            if (null == item)
            {
                return;
            }
            item.IsEnabledPageSizeWarning = enabled;
        }
        public void SetEnabledPerformancePageRenderTimeWarning(bool enabled)
        {
            PerformanceMonitorItem item = PerformanceMonitorItem.Current;
            if (null == item)
            {
                return;
            }
            item.IsEnabledPageRenderTimeWarning = enabled;
        }

        /// <summary>
        /// The Goal of this method is to write script tags to the form element before Components get a chance to.  If a Header element exists, the
        /// the script references will be included in the Header, as that occurs before the form element.
        /// </summary>
        /// <param name="script">The name of the script to be added.</param>
        private void RegisterScriptOnPreInit(string script)
        {
            WriteClientScript(script, "~/inc/" + script);
        }

        protected override void OnPreInit(EventArgs e)
        {
            this.RegisterScriptOnPreInit("CrossBrowserPolyfill.js");
            InitializeComponent();
            base.OnPreInit(e);
        }

        protected override void OnInit(EventArgs e)
        {
            RegisterJsScript("CrossBrowserPolyfill.js");

            RegisterJsGlobalVariables("AsyncOverlayTimeBuffer", ConstStage.AsyncOverlayTimeBuffer);
            RegisterJsGlobalVariables("VirtualRoot", VirtualRoot);
            RegisterJsGlobalVariables("EDocsRoot", Tools.GetEDocsLink(Tools.VRoot));
            ClientScript.RegisterHiddenField("VirtualRoot", VirtualRoot);
            base.OnInit(e);
        }
        private void InitializeComponent()
        {

            this.PreInit += new System.EventHandler(this.PagePreInit);
            this.Init += new System.EventHandler(this.PageInit);

            this.InitComplete += new System.EventHandler(this.PageInitComplete);
            this.Load += new System.EventHandler(this.PageLoad);
            this.LoadComplete += new System.EventHandler(this.PageLoadComplete);
            this.PreRender += new System.EventHandler(this.PagePreRender);
            this.PreRenderComplete += new System.EventHandler(this.PagePreRenderComplete);
        }
        protected void LogPerformanceMonitorItem(string eventName)
        {
            PerformanceMonitorItem item = PerformanceMonitorItem.Current;
            if (null != item)
            {
                item.AddTimingDetail(eventName, 0);
            }

        }

        /// <summary>
        /// overrideable.  making these static and using new doesn't work, even though it makes the unit test easier.
        /// </summary>
        public virtual IEnumerable<LendersOffice.Security.Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new List<LendersOffice.Security.Permission>();
            }
        }

        /// <summary>
        /// overrideable.  making these static and using new doesn't work, even though it makes the unit test easier.
        /// </summary>
        public virtual IEnumerable<LendersOffice.Security.E_RoleT> RequiredRolesForLoadingPage
        {
            get
            {
                return new List<LendersOffice.Security.E_RoleT>();
            }
        }

        private void PagePreInit(object sender, EventArgs e)
        {
            LogPerformanceMonitorItem("Page_PreInit");
            Tools.DenyIfUserLacksSufficientPermissionsOrRoles(RequiredPermissionsForLoadingPage, RequiredRolesForLoadingPage);
        }
        private void PageLoad(object sender, EventArgs e)
        {
            LogPerformanceMonitorItem("Page_Load");
        }
        private void PageLoadComplete(object sender, EventArgs e)
        {
            LogPerformanceMonitorItem("Page_LoadComlete");
        }
        private void PagePreRender(object sender, EventArgs e)
        {
            LogPerformanceMonitorItem("Page_PreRender");
        }
        private void PagePreRenderComplete(object sender, EventArgs e)
        {
            LogPerformanceMonitorItem("Page_PreRenderComplete");
        }
        private void PageInitComplete(object sender, EventArgs e)
        {
            LogPerformanceMonitorItem("Page_InitComplete");
        }
        private void PageInit(object sender, EventArgs e)
        {
            LogPerformanceMonitorItem("Page_Init");

            if (m_loadDefaultStylesheet)
            {
                //Unlike stylesheet.css, stylesheets imported in it won't be the newest version,
                //so they should also be included through the IncludeStyleSheet method.
                IncludeStyleSheet(StyleSheet);
            }

            ClientScript.RegisterHiddenField("IsUpdate", "0");
            RegisterJsScript("webmethodServices.js");
            if (m_loadLOdefaultScripts)
            {
                RegisterJsScript("common.js");
                RegisterJsScript("calendar.js");
                RegisterJsScript("calendar-en.js");
            }
            RegisterJsScript("LQBPopup.js");
            Response.ExpiresAbsolute = SmallDateTime.MinValue;

        }
        protected bool m_loadDefaultStylesheet = true;
        protected bool m_loadLOdefaultScripts = true;

        #region CModalDlg javascript functions
        private static string s_cModalDlgJs = string.Format(@"
<script type='text/javascript'>
var VRoot = '{0}';
</script>
<script type=""text/javascript"" src=""{0}/common/ModalDlg/CModalDlg.js""></script>
", Tools.VRoot);
        #endregion

        protected override void OnPreRender(EventArgs e)
        {
            if ( !m_globalJsObject.ContainsKey("server") )
            {
                try
                {
                    RegisterJsGlobalVariables("server", ConstSite.MachineDisplayName);
                }
                catch (InvalidOperationException)
                {
                    //could not get the machine name safe to ignore.
                }
            }
            this.ClientScript.RegisterClientScriptBlock(GetType(), "cModalDlg", s_cModalDlgJs);
            StringBuilder sb = new StringBuilder();
            StringBuilder sb1 = new StringBuilder();
            foreach (ClientScriptData f in m_initFunctionList)
            {
                sb.AppendFormat("if (typeof({0}) == 'function') {0}({1});", f.Name, f.Arguments).Append(Environment.NewLine);
                if (f.Script != null) sb1.Append(f.Script).Append(Environment.NewLine);
            }

            var AuditPostSaveId = RequestHelper.GetSafeQueryString("AuditPostSaveId");
            if (!string.IsNullOrEmpty(AuditPostSaveId))
            {
                sb.AppendFormat(
@"
    var old_postSaveMe = null;
    if(typeof('_postSaveMe') == 'function')
    {{
        old_postSaveMe = _postSaveMe;
    }}

    _postSaveMe = function(results)
    {{
        //Only execute the old post save me if it existed.
        old_postSaveMe && old_postSaveMe(results);
        window.parent.AuditWindow.AuditPostSave({0});
    }}
", AspxTools.JsString(AuditPostSaveId));
            }

            var highlightId = RequestHelper.GetSafeQueryString("highlightId");
            if (!string.IsNullOrEmpty(highlightId))
            {
                sb.AppendFormat(@"    
    var nameTrials = [
        function(name){{ return name; }},
        function(name){{ 
                        if(name.indexOf('_rep') != -1) return name.substring(0, name.length - '_rep'.length);
                        return null;
                      }},
        function(name){{
                        if(name.indexOf('Pe') != -1) return name.substring(0, name.length - 'Pe'.length);
                        return null;
                      }},
        function(name){{ 
                        if(name.indexOf('_rep') == -1) return name + '_rep';
                        return null;
                      }},
        function(name){{ 
                        if(name.indexOf('Calc') != -1) return name.substring(0, name.length - 'Calc'.length);
                        return null;
                      }}

    ]
    var trialIndex = 0;
    var originalName = {0};
    var e = null;
    while(!e)
    {{
        var nameChanger = nameTrials[trialIndex];
        if(!nameChanger) break;
        trialIndex++;
        var name = nameChanger(originalName);
        if(!name) continue;
    
        e = document.getElementById(name);
        if(e) break;

        var inputs = document.getElementsByTagName('*');
        for(var i = 0; i < inputs.length; i++) {{
            if(!inputs[i].id) continue;

            var idx = inputs[i].id.indexOf(name);
            if(idx == -1) continue;
            
            if(idx + name.length == inputs[i].id.length) {{
                e = inputs[i];
                break;
            }}
        }}
    }}

    if(e)
    {{
        e.scrollIntoView();
        window.scrollBy(0, -40);
        if (!e.readOnly && e.tagName.toLowerCase() != 'select') {{
            e.className += ' hiliteBorder';
            if (typeof(e.select) != ""undefined"" && e.type != ""textarea"" && e.type != 'button' && e.type != 'submit') 
            {{
                e.select();
            }}
        }}
        e.focus();
    }}", AspxTools.JsString(highlightId));
            }

            StringBuilder sb2 = new StringBuilder();
            foreach (KeyValuePair<string, string> o in m_globalJsObject)
            {
                sb2.AppendLine(o.Value);
            }
            string checkIeScript = string.Empty;

            if (m_globalJSEnumValueByNameByEnumName.Count > 0)
            {
                m_globalJsVariables.Add("EnumsByName", SerializationHelper.JsonNetSerialize(m_globalJSEnumValueByNameByEnumName));
            }

            if (m_globalJSEnumNameByValueByEnumName.Count > 0)
            {
                m_globalJsVariables.Add("EnumsByValue", SerializationHelper.JsonNetSerialize(m_globalJSEnumNameByValueByEnumName));
            }

            if (m_globalJsVariables.Count > 0)
            {
                sb2.AppendLine();
                sb2.Append("var ML = {");
                bool isFirst = true;
                foreach (var o in m_globalJsVariables)
                {
                    if (isFirst == false)
                    {
                        sb2.Append(",");
                    }
                    sb2.Append(o.Key + ":" + o.Value);
                    isFirst = false;
                }
                sb2.Append("};");
            }
            if (EnableJQueryInitCode)
            {                
                string script = string.Format(@"
<script type=""text/javascript"">
{2}
            
$j(function(){{
    {1}
     if( window.onhelp ){{
        displayHelp();
    }}
    else{{
       $j(document).bind('keydown',  function(e){{
            var c = (e.keyCode ? e.keyCode : e.which);
            if( c === 112 ) {{
                displayHelp(); 
                return false;
            }}
        }});
    }}

    var virtualRoot =  $j('#VirtualRoot'); 
    if( virtualRoot.length > 0 ){{
         gVirtualRoot  = virtualRoot.val();
    }}

    if (typeof(_initInput) == 'function') _initInput();
    if (typeof(__initCombobox) == 'function') __initCombobox();
    {0}
    if (typeof(_init) == 'function') _init();
}});
</script>
", sb, sb1, sb2);

                ClientScript.RegisterStartupScript(this.GetType(), "__init", script);

            }
            else
            {
                string script = string.Format(@"
<script type=""text/javascript"">
<!--
{3}
addOnLoadHandler(document, 'load', _main, true);
if(typeof(event_onkeyup) == 'function')
    addEventHandler(document, 'keyup', event_onkeyup);
document.onhelp = function event_help() {{ displayHelp(); event.returnValue = false; }}

{2}

function _main() {{
    gVirtualRoot = document.getElementById('VirtualRoot') == null ? '' : document.getElementById('VirtualRoot').value;
    var body = document.getElementsByTagName('body')[0];
    if(body != null && getBrowserVersion() >= 11) 
        body.className = body.className + ' is-modern-browser';

    if (typeof(_initInput) == 'function') _initInput();
    if (typeof(__initCombobox) == 'function') __initCombobox();
    {0}
    if (typeof(_init) == 'function') _init();
}}
{1}
//-->
</script>", sb, sb1, sb2, checkIeScript);



                if (m_loadLOdefaultScripts)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "__init", script);
                }
                else
                {
                    string script2 = string.Format(@"<script type=""text/javascript"">{0}</script>", sb2);
                    ClientScript.RegisterStartupScript(this.GetType(), "__loadObjects", script2);
                }
            }

            IncludeJqueryScript();
            if (Header != null)
            {
                if (Header.Controls.Count > 0)
                {
                    // 6/15/2015 dd - Add to the second position. This allow to leave X-UA-Compatible meta tag as first one. See loadmin.Master as an example.
                    Header.Controls.AddAt(1, CreateJsVirtualRootVariable()); // Note that if head element has no runat, error handling won't work.
                }
                else
                {
                    Header.Controls.AddAt(0, CreateJsVirtualRootVariable()); // Note that if head element has no runat, error handling won't work.
                }
            }
           
            RegisterAllIncludeScript();

            if (this.Header != null)
            {
                HtmlLink link = new HtmlLink();
                link.Attributes.Add("type", "image/x-icon");
                link.Attributes.Add("rel", "icon");
                link.Attributes.Add("href", Page.ResolveUrl("~/favicon.ico?v=2"));
                this.Header.Controls.Add(link);
            }



            //The following call has to modify the header last. compat meta tag must come first.
            IncludeCompatMetaTag();
            base.OnPreRender(e);
        }

        private void IncludeCompatMetaTag()
        {
            
            if (Header == null)
            {
                return;
            }

            string version = "IE=";

            switch (GetForcedCompatibilityMode())
            {
                case E_XUAComaptibleValue.NONE:
                    return; 
                case E_XUAComaptibleValue.IE9:
                    version += "9";
                    break;
                case E_XUAComaptibleValue.IE8:
                    version += "8";
                    break;
                case E_XUAComaptibleValue.IE7:
                    version += "7";
                    break;
                case E_XUAComaptibleValue.IE5:
                    version += "5";
                    break;
                case E_XUAComaptibleValue.EmulateIE9:
                    version += "EmulateIE9";
                    break;
                case E_XUAComaptibleValue.EmulateIE8:
                    version += "EmulateIE8";
                    break;
                case E_XUAComaptibleValue.EmulateIE7:
                    version += "EmulateIE7";
                    break;
                case E_XUAComaptibleValue.Edge:
                    version += "edge";
                    break;
                case E_XUAComaptibleValue.IE10:
                    version += "10";
                    break;
                case E_XUAComaptibleValue.IE11:
                    version += "11";
                    break;
                case E_XUAComaptibleValue.EmulateIE10:
                    version += "EmulateIE10";
                    break;
            }

            HtmlMeta meta = new HtmlMeta();
            meta.Attributes.Add("http-equiv", "X-UA-Compatible");
            meta.Attributes.Add("content", version);
            Header.Controls.AddAt(0, meta);
        }

        /// <summary>
        /// Add the client script that will get execute when page is load.
        /// 
        /// Usage: AddInitScriptFunction("foo", "function foo() { // COde here }");
        /// </summary>
        /// <param name="name"></param>
        /// <param name="script"></param>
        public void AddInitScriptFunction(string name, string script)
        {
            AddInitScriptFunctionWithArgs(name, script, string.Empty);
        }
        public void AddInitScriptFunction(string name)
        {
            AddInitScriptFunction(name, null);
        }

        public void AddInitScriptFunctionWithArgs(string name, string script, string args)
        {
            m_initFunctionList.Add(new ClientScriptData(name, script, args));
        }
        /// <summary>
        /// Call client script with arguments when page finish loading.
        /// Args must be valid javascript arguments, ie "'foo1', 'foo2'"
        /// Ex:
        ///    name(args)
        ///    
        /// </summary>
        /// <param name="name"></param>
        /// <param name="args"></param>
        public void AddInitScriptFunctionWithArgs(string name, string args)
        {
            AddInitScriptFunctionWithArgs(name, null, args);
        }
        /// <summary>
        /// Register a javascript object in JSON format to the page. This method will throw exception if 'name' already exist.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="obj"></param>
        public void RegisterJsObject<T>(string name, T obj) where T : class, new()
        {
            EnsureJSVariableName(name);
            string script = "var " + name + " = " + ObsoleteSerializationHelper.JsonSerialize(obj) + ";";
            m_globalJsObject.Add(name, script);

        }

        /// <summary>
        /// NOTE: I have not verified the usage of JavascriptJsonSerialize
        /// Register a javascript object in JSON format using SerializationHelper.JavascriptJsonSerialize
        /// </summary>
        public void RegisterJsObjectWithJavascriptSerializer<T>(string name, T obj) where T : class
        {
            EnsureJSVariableName(name);
            string script = "var " + name + " = " + ObsoleteSerializationHelper.JavascriptJsonSerialize(obj) + ";";
            m_globalJsObject.Add(name, script);

        }

        public void RegisterJsObjectWithJsonNetSerializer<T>(string name, T obj) where T : class, new()
        {
            this.EnsureJSVariableName(name);
            string script = "var " + name + " = " + SerializationHelper.JsonNetSerialize(obj) + ";";
            m_globalJsObject.Add(name, script);
        }

        public void RegisterJsObjectWithJsonNetAnonymousSerializer<T>(string name, T obj)
        {
            this.EnsureJSVariableName(name);
            string script = "var " + name + " = " + SerializationHelper.JsonNetAnonymousSerialize(obj) + ";";
            m_globalJsObject.Add(name, script);
        }

        /// <summary>
        /// Register a list as javascript array in JSON format. This method will throw exception if 'name' already exist.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <param name="list"></param>
        public void RegisterJsObjectArray<T>(string name, IEnumerable<T> list) where T : class, new()
        {
            EnsureJSVariableName(name);

            StringBuilder sb = new StringBuilder();

            sb.Append("var " + name + " = [");

            if (list != null)
            {
                bool isFirst = true;

                foreach (var o in list)
                {
                    if (isFirst == false)
                    {
                        sb.Append(",");
                    }
                    sb.Append(ObsoleteSerializationHelper.JsonSerialize(o));
                    isFirst = false;
                }
            }

            sb.Append("];");

            m_globalJsObject.Add(name, sb.ToString());

        }

        public void RegisterJsStruct<T>(string name, T obj)
        {
            EnsureJSVariableName(name);
            JavaScriptSerializer jsr = new JavaScriptSerializer();
            jsr.MaxJsonLength = ConstApp.MaxJsonLength;
            string script = "var " + name + " = " + jsr.Serialize(obj) + ";";
            m_globalJsObject.Add(name, script);

        }

        /// <summary>
        /// NOTE: In JS may want to use == not === since the values here are strings. <para/>
        /// Registers an enum so in javascript you can get the (numeric (see UnderlyingType)) value by name. <para/>
        /// e.g. <para/>
        /// Code behind: <para/>
        /// RegisterJsEnumByName(typeof(E_sLT)); <para/>
        /// <para/>
        /// Javascript: <para/>
        /// sLTInput.value == ML.EnumsByName.E_sLT.Conventional  // do Conventional stuff. <para/>
        /// </summary>
        public void RegisterJsEnumByName(Type tEnum)
        {
            if (tEnum == null)
            {
                throw new ArgumentNullException("Cannot register null type as a js enum.");
            }

            if (!tEnum.IsEnum)
            {
                throw new ArgumentException(string.Format("Type {0} is not an enumerated type", tEnum.FullName));
            }

            EnsureJSVariableName(tEnum.Name);

            m_globalJSEnumValueByNameByEnumName.Add(tEnum.Name, Tools.GetEnumAsDictionaryByName(tEnum));
        }

        /// <summary>
        /// NOTE: In JS may want to use == not === since the values here are strings. <para/>
        /// ALSO NOTE: This won't work with Enums with multiple names for the same value, or enums with flags. <para/>
        /// Registers an enum so in javascript you can get the name by its numeric value. <para/>
        /// e.g. <para/>
        /// Code behind: <para/>
        /// RegisterJsEnumByValue(typeof(E_sLT)); <para/>
        /// <para/>
        /// Javascript: <para/>
        /// sLT.innerHTML = ML.EnumsByValue.E_sLT[sLTObject.value] <para/>
        /// </summary>
        public void RegisterJsEnumByValue(Type tEnum)
        {
            if (tEnum == null)
            {
                throw new ArgumentNullException("Cannot register null type as a js enum.");
            }

            if (!tEnum.IsEnum)
            {
                throw new ArgumentException(string.Format("Type {0} is not an enumerated type", tEnum.FullName));
            }

            if (Tools.HasNonInheritedFlagsAttribute(tEnum))
            {
                throw new ArgumentException(string.Format("Type {0} has flags attribute so name by value doesn't make sense.", tEnum.FullName));
            }

            EnsureJSVariableName(tEnum.Name);

            m_globalJSEnumNameByValueByEnumName.Add(tEnum.Name, Tools.GetEnumAsDictionaryByValue(tEnum));
        }

        private List<string> m_includeOrderedKeys = new List<string>();
        private IDictionary<string, string> m_includeDictionary = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        private Dictionary<string, string> m_globalJsVariables = new Dictionary<string, string>();
        private Dictionary<string, ReadOnlyDictionary<string, string>> m_globalJSEnumValueByNameByEnumName = new Dictionary<string, ReadOnlyDictionary<string, string>>();
        private Dictionary<string, ReadOnlyDictionary<string, string>> m_globalJSEnumNameByValueByEnumName = new Dictionary<string, ReadOnlyDictionary<string, string>>();

        /// <summary>
        /// Register javascript global variable so you can retrieve value from client script with this syntax: ML.{name}
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void RegisterJsGlobalVariables(string name, string value)
        {
            EnsureJSVariableName(name);
            m_globalJsVariables.Add(name, AspxTools.JsString(value));
        }
        public void RegisterJsGlobalVariables(string name, Guid value)
        {
            EnsureJSVariableName(name);
            m_globalJsVariables.Add(name, AspxTools.JsString(value));
        }
        public void RegisterJsGlobalVariables(string name, int value)
        {
            EnsureJSVariableName(name);
            m_globalJsVariables.Add(name, value.ToString());
        }
        public void RegisterJsGlobalVariables(string name, decimal value)
        {
            EnsureJSVariableName(name);
            m_globalJsVariables.Add(name, value.ToString());
        }
        public void RegisterJsGlobalVariables(string name, bool value)
        {
            EnsureJSVariableName(name);
            m_globalJsVariables.Add(name, value ? "true" : "false");
        }
        private void EnsureJSVariableName(string name)
        {
            if (Regex.IsMatch(name, "^[a-zA-Z0-9_]+$") == false)
            {
                throw new CBaseException(ErrorMessages.Generic, name + " is not a valid JS identifier.");
            }
        }
        /// <summary>
        /// Register to include js file on this page. js file must be in {VirtualRoot}/inc/ folder
        /// </summary>
        /// <param name="js"></param>
        public void RegisterJsScript(string js)
        {
            this.RegisterJsScript("~/inc/", js);
        }

        public void RegisterJsScript(string baseDirectory, string js)
        {
            if (string.IsNullOrEmpty(js))
            {
                return;
            }

            if (m_includeDictionary.ContainsKey(js) == false)
            {
                m_includeDictionary.Add(js, baseDirectory + js);
                m_includeOrderedKeys.Add(js);
            }
        }

        public void IncludeStyleSheet(string cssFilePath)
        {
            if (null != Page.Header)
            {
                HtmlLink link = new HtmlLink();
                link.Href = this.ResolveUrl(cssFilePath) + "?v=" + IncludeVersion;
                link.Attributes["type"] = "text/css";
                link.Attributes["rel"] = "stylesheet";
                Page.Header.Controls.Add(link);
            }
            else
            {
                var startIndex = cssFilePath.LastIndexOf("/") + 1;
                this.RegisterCSS(cssFilePath.Substring(startIndex));
            }
        }

        /// <summary>
        /// Register to include a css file on this page. CSS file must be in the {VirtualRoot}/css/ folder.
        /// </summary>
        /// <param name="css"></param>
        public void RegisterCSS(string css)
        {
            if (string.IsNullOrEmpty(css)) return;
            if (m_includeDictionary.ContainsKey(css)) return; 

            m_includeDictionary.Add(css, "~/css/" + css);
            m_includeOrderedKeys.Add(css);
        }

        public void RegisterCssFromInc(string css)
        {
            if (string.IsNullOrEmpty(css)) return;
            if (m_includeDictionary.ContainsKey(css)) return;

            m_includeDictionary.Add(css, "~/inc/" + css);
            m_includeOrderedKeys.Add(css);
        }

        /// <summary>
        /// Register to include vbs file on this page. vbsfile must be in {VirtualRoot}/inc/ folder
        /// </summary>
        /// <param name="vbs"></param>
        public virtual void RegisterVbsScript(string vbs)
        {
            if (vbs.Equals("common.vbs", StringComparison.OrdinalIgnoreCase))
            {
                m_includeVbs = true; 
            }
            else
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), vbs, string.Format("<script src=\"{0}/inc/{1}\" language=\"vbscript\" type=\"text/vbscript\"></script>", VirtualRoot, vbs));
            }
        }

        /// <summary>
        /// This method will immediately write the link or script tags to the form element for the specified script.
        /// </summary>
        /// <param name="scriptKey">The name of the script to add.</param>
        /// <param name="scriptLocation">The script's location.</param>
        private void WriteClientScript(string scriptKey, string scriptLocation)
        {
            string elementFormat;

            if (scriptKey.Contains("css"))
            {
                elementFormat = "<link rel='stylesheet' type='text/css' href='{0}' />";
            }
            else
            {
                elementFormat = "<script src=\"{0}\" type=\"text/javascript\"></script>";
            }

            ClientScript.RegisterClientScriptBlock(this.GetType(), scriptKey,
                string.Format(elementFormat, AppendVersionToInclude(scriptLocation)));
        }

        private void RegisterAllIncludeScript()
        {
            if (m_includeOrderedKeys.Contains("LQBPopupV2.js"))
            {
                m_includeOrderedKeys.Remove("LQBPopup.js");
            }

            m_includeOrderedKeys = m_includeOrderedKeys.OrderBy(p => Regex.IsMatch(p, "jquery-\\d") ? -2 : 
            p.Equals("CrossBrowserPolyfill.js") ? -1 : 
            0).ToList();

            if (this.Header == null)
            {
                Type type = this.GetType();

                if (m_includeVbs)
                {
                    string vbs = String.Format(@"<script src=""{0}"" language=""vbscript"" type=""text/vbscript""></script>", AppendVersionToInclude(Page.ResolveClientUrl("~/inc/common.vbs")));
                    ClientScript.RegisterClientScriptBlock(type, "commonvbs", vbs);
                    string backup = String.Format(@"<script src=""{0}"" type=""text/javascript""></script>", AppendVersionToInclude(Page.ResolveClientUrl("~/inc/vbsfix.js")));
                    ClientScript.RegisterClientScriptBlock(type, "backup", backup);
                }

                foreach (var scriptKey in m_includeOrderedKeys)
                {
                    WriteClientScript(scriptKey, m_includeDictionary[scriptKey]);
                }
            }
            else
            {
                if (m_includeVbs)
                {
                    var element = new HtmlGenericControl("script");
                    element.Attributes.Add("src", AppendVersionToInclude(Page.ResolveClientUrl("~/inc/common.vbs")));
                    element.Attributes.Add("type", "text/vbscript");
                    element.Attributes.Add("language", "vbscript");
                    this.Header.Controls.Add(element);
                    this.Header.Controls.Add(CreateIncludeReference("~/inc/vbsfix.js"));
                }
                // 6/2/2011 dd - Add to <HEAD> tag.
                foreach (var scriptKey in m_includeOrderedKeys)
                {
                    this.Header.Controls.Add(
                        CreateIncludeReference(m_includeDictionary[scriptKey])
                    );
                }
            }
        }
        private HtmlGenericControl CreateIncludeReference(string path)
        {
            HtmlGenericControl element;
            if (path.Contains("css"))
            {
                element = new HtmlGenericControl("link");
                element.Attributes.Add("href", AppendVersionToInclude(path));
                element.Attributes.Add("type", "text/css");
                element.Attributes.Add("rel", "stylesheet");

            }
            else
            {
                element = new HtmlGenericControl("script");
                element.Attributes.Add("src", AppendVersionToInclude(path));
                element.Attributes.Add("type", "text/javascript");
            }
            return element;
        }

        private string AppendVersionToInclude(string path)
        {
            string url = ResolveUrl(path);

            if (url.IndexOf("?") >= 0)
            {
                Tools.LogError("WHY is js [" + path + "] contains ?");
                return url; // 4/17/2012 dd - Return as is.
            }
            else
            {

                return url + "?v=" + IncludeVersion;
            }
        }
        private HtmlGenericControl CreateJqueryNoConflict()
        {
            HtmlGenericControl script = new HtmlGenericControl("script");
            script.Attributes.Add("type", "text/javascript");
            script.InnerHtml = "var $j = jQuery.noConflict(); var $ = $j;";
            return script;

        }
        private HtmlGenericControl CreateJsVirtualRootVariable()
        {
            HtmlGenericControl script = new HtmlGenericControl("script");
            script.Attributes.Add("type", "text/javascript");
            script.InnerHtml = "var gVirtualRoot = '" + VirtualRoot + "';";
            return script;
        }

        private void IncludeJqueryScript()
        {
            string jquerySrc = string.Empty;
            var version = GetJQueryVersion();
            switch (version)
            {
                case E_JqueryVersion._1_4_1:
                    jquerySrc = "jquery-1.4.1.min.js";
                    this.EnableJqueryMigrate = false;
                    break;
                case E_JqueryVersion._1_4_2:
                    jquerySrc = "jquery-1.4.2.min.js";
                    this.EnableJqueryMigrate = false;
                    break;
                case E_JqueryVersion._1_6_1:
                    jquerySrc = "jquery-1.6.1.min.js";
                    break;
                case E_JqueryVersion._1_7_1:
                    jquerySrc = "jquery-1.7.1.min.js";
                    break;
                case E_JqueryVersion._1_11_2:
                    jquerySrc = "jquery-1.11.2.min.js";
                    break;
                case E_JqueryVersion._1_12_4:
                    jquerySrc = "jquery-1.12.4.min.js";
                    break;
                default:
                    throw new UnhandledEnumException(version);
            }

            if (this.Header == null)
            {
                RegisterJsScript(jquerySrc);
                if (this.EnableJqueryMigrate && version == E_JqueryVersion._1_12_4)
                {
                    RegisterJsScript("jquery-migrate-1.4.1.min.js");
                }

                if(this.EnableJqueryUiLatest)
                {
                    RegisterJsScript("jquery-ui-1.11.4.min.js");
                    RegisterCSS("jquery-ui-1.11.custom.css");
                }

                RegisterJsScript("NoConflict.js");
                RegisterJsScript("json.js");
            }
            else
            {
                jquerySrc = "~/inc/" + jquerySrc;
                HtmlGenericControl jqueryScript = CreateIncludeReference(jquerySrc);
                HtmlGenericControl noConflictJqueryScript = CreateJqueryNoConflict();
                HtmlGenericControl jsonScript = CreateIncludeReference("~/inc/json.js");
                

                int startingIndex = Header.Controls[0].GetType() == typeof(HtmlMeta) ? 1 : 0;
                // We can access the Header even if its contents are not literal.  We simply cannot access the innerText and innerHtml properties.
                Header.Controls.AddAt(startingIndex, jqueryScript); // 6/2/2011 dd - Making sure this is the first script tag in the <head> tag.
                
                Header.Controls.AddAt(startingIndex + 1, noConflictJqueryScript);
                Header.Controls.AddAt(startingIndex + 1, jsonScript); // 6/2/2011 dd - Always include json library when jquery is include.
                
                if(this.EnableJqueryUiLatest)
                {
                    HtmlGenericControl jqueryUiJsScript = CreateIncludeReference("~/inc/jquery-ui-1.11.4.min.js");
                    HtmlGenericControl jqueryUiCssScript = CreateIncludeReference("~/css/jquery-ui-1.11.custom.css");
                    Header.Controls.AddAt(startingIndex + 1, jqueryUiJsScript);
                    Header.Controls.AddAt(startingIndex + 1, jqueryUiCssScript);
                }

                if (this.EnableJqueryMigrate && version == E_JqueryVersion._1_12_4)
                {
                    HtmlGenericControl jqueryMigrateScript = CreateIncludeReference("~/inc/jquery-migrate-1.4.1.min.js");
                    Header.Controls.AddAt(startingIndex + 1, jqueryMigrateScript);
                }
            }
        }

        /// <summary>
        /// EnableJQuery is now obsolete.  Do not set this to true, as jQuery will already be added everywhere.  There
        /// are plans to repurpose this field into something like "AddedJQueryViaLegacyCode".
        /// </summary>
        public bool EnableJquery { get; set; }
        public bool EnableJqueryUiLatest { get; set; } = true;
        public bool EnableJqueryMigrate { get; set; } = true;
        protected virtual E_JqueryVersion GetJQueryVersion()
        {
            return EnableJquery && EnableJqueryMigrate ? E_JqueryVersion._1_6_1 : E_JqueryVersion._1_12_4;
        }

        protected string JQuery(params Control[] ctrlList)
        {
            if (EnableJquery == false)
            {
                throw new CBaseException(ErrorMessages.Generic, "The page must have JQuery in order to use this method.");
            }
            return AspxTools.JQuery(ctrlList);
        }
        protected string JsString(string input)
        {
            return AspxTools.JsString(input);
        }
        protected string JsString(Enum input)
        {
            return AspxTools.JsString(input);
        }
        protected string JsString(Guid input)
        {
            return AspxTools.JsString(input);
        }
        protected string JsString(int input)
        {
            return AspxTools.JsString(input);
        }
        protected string JsArray(params Enum[] list)
        {
            return AspxTools.JsArray(list);
        }

        protected override void SavePageStateToPersistenceMedium(object state)
        {
            if (!this.DisableViewState)
            {
                base.SavePageStateToPersistenceMedium(state);
            }
        }

        protected override object LoadPageStateFromPersistenceMedium()
        {
            if (this.DisableViewState)
            {
                return null; 
            }
            else {
                return base.LoadPageStateFromPersistenceMedium();
            }
        }
    }
    public enum E_JqueryVersion
    {
        _1_4_1,
        _1_4_2,
        _1_6_1,
        _1_7_1,
        _1_11_2, // 3/16/2015 dd - Added.
        _1_12_4,
        _2_2_4
    }

    /// <summary>
    /// Represents the available values for the XUA-Comapatible http-equiv meta tag.
    /// None, means do not use compatible value.
    /// </summary>
    public enum E_XUAComaptibleValue
    {
        /// <summary>
        /// Dont Use XUA compatible value
        /// </summary>
        NONE,
        /// <summary>
        /// The webpage is displayed in IE9 Standards mode.
        /// </summary>
        IE9, 
        /// <summary>
        /// The webpage is displayed in IE8 Standards mode.
        /// </summary>
        IE8, 
        /// <summary>
        /// The webpage is displayed in IE7 Standards mode.
        /// </summary>
        IE7, 
        /// <summary>
        /// The webpage is displayed in IE5 (Quirks) mode.
        /// </summary>
        IE5,
        /// <summary>
        /// If the webpage specifies a standards-based doctype directive, 
        /// the page is displayed in IE9 mode; otherwise, it is displayed in IE5 mode.
        /// </summary>
        EmulateIE9, 
        /// <summary>
        /// If the webpage specifies a standards-based doctype directive, the page is displayed
        /// in IE8 mode; otherwise, it is displayed in IE5 mode.
        /// </summary>
        EmulateIE8,
        /// <summary>
        /// If the webpage specifies a standards-based doctype directive,
        /// e page is displayed in IE7 mode; otherwise, it is displayed in IE5 mode.
        /// </summary>
        EmulateIE7,
        //EDGE

        /// <summary>
        /// Force IE to use standards latest mode
        /// </summary>
        Edge,

        // OPM 233831 - Add IE10 modes

        /// <summary>
        /// The webpage is displayed in IE10 Standards mode.
        /// </summary>
        IE10,

        /// <summary>
        /// The webpage is displayed in IE11 Standards mode.
        /// </summary>
        IE11,

        /// <summary>
        /// If the webpage specifies a standards-based doctype directive,
        /// e page is displayed in IE10 mode; otherwise, it is displayed in IE5 mode.
        /// </summary>
        EmulateIE10,
    }
}
