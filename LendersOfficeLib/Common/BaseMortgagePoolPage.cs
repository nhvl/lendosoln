﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.ObjLib.MortgagePool;

namespace LendersOffice.Common
{
    public class BaseMortgagePoolPage : BasePage
    {
        protected List<Permission> RequiredPermissions = new List<Permission>() { 
            Permission.AllowCapitalMarketsAccess
        };

        //Note that including jQuery happens in the master pages, which is why you don't see it in this file.
        [Obsolete("Mortgage Pool pages always enable jQuery", true)]
        public new bool EnableJquery
        {
            get
            {
                return true;
            }
            set 
            {
                if (value == false)
                {
                    throw new Exception("Mortgage Pool pages always enable jQuery, and it cannot be disabled");
                }
            }
        }

        public int PoolId
        {
            get
            {
                return RequestHelper.GetInt("PoolId", -1);
            }
        }

        MortgagePool m_Pool;
        public MortgagePool CurrentPool
        {
            get
            {
                return m_Pool;
            }
        }

        public void AccessDenied(string why)
        {
            throw new CBaseException("You do not have access to this pool",
                "User [" + BrokerUserPrincipal.CurrentPrincipal.LoginNm + "] at broker id [" + 
                BrokerUserPrincipal.CurrentPrincipal.BrokerId + "] tried to access pool " +
                PoolId + ", stopped because " + why);
        }

        protected override E_XUAComaptibleValue GetForcedCompatibilityMode()
        {
            return E_XUAComaptibleValue.IE8;
        }

        protected override void OnPreRender(EventArgs e)
        {
            //Don't want the base page somehow including another copy of jquery 
            base.EnableJquery = false;
            base.OnPreRender(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            m_loadDefaultStylesheet = false;
            //If we weren't given a pool id, we're probably in a stand alone editor; that means we should skip loading the pool.
            if (PoolId != -1)
            {
                try
                {
                    //Trying to load the mortgage pool will throw an exception if the broker is wrong.
                    m_Pool = new MortgagePool(PoolId);
                }
                catch (CBaseException)
                {
                    AccessDenied("the pool does not belong to their broker or the pool does not exist");
                    return;
                }
            
                RegisterJsGlobalVariables("PoolId", PoolId);
            }

            foreach (var perm in RequiredPermissions)
            {
                if (!BrokerUserPrincipal.CurrentPrincipal.HasPermission(perm))
                {
                    AccessDenied(" the user does not have required permission " + perm);
                }
            }



            base.OnLoad(e);
        }
    }
}
