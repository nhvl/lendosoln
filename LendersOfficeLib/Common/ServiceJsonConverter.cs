﻿// <copyright file="ServiceJsonConverter.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Huy Nguyen
//  Date:   6/12/2016
// </summary>

namespace LendersOffice.Common
{
    using System;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// The json converter using for deserilize service parameters.
    /// </summary>
    public class ServiceJsonConverter : JsonConverter
    {
        /// <summary>
        ///  Gets a value indicating whether this Newtonsoft.Json.JsonConverter can read JSON.
        /// </summary>
        /// <value>Value indicating whether this Newtonsoft.Json.JsonConverter can read JSON.</value>
        public override bool CanRead
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this Newtonsoft.Json.JsonConverter can write JSON.
        /// </summary>
        /// /// <value>Value indicating whether this Newtonsoft.Json.JsonConverter can write JSON.</value>
        public override bool CanWrite
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Determines whether this instance can convert the specified object type.
        /// </summary>
        /// <param name="objectType">Type of the object.</param>
        /// <returns>True if this instance can convert the specified object type; otherwise, false.</returns>
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(object);
        }

        /// <summary>
        ///  Reads the JSON representation of the object.
        /// </summary>
        /// <param name="reader">The Newtonsoft.Json.JsonReader to read from.</param>
        /// <param name="objectType">Type of the object.</param>
        /// <param name="existingValue">The existing value of object being read.</param>
        /// <param name="serializer">The calling serializer.</param>
        /// <returns>The object value.</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JValue jsonValue = serializer.Deserialize<JValue>(reader);

            if (jsonValue.Type == JTokenType.Float)
            {
                return jsonValue.Value<float>();
            }
            else if (jsonValue.Type == JTokenType.Integer)
            {
                return jsonValue.Value<int>();
            }
            else if (jsonValue.Type == JTokenType.Date)
            {
                return jsonValue.Value<DateTime>();
            }
            else if (jsonValue.Type == JTokenType.Boolean)
            {
                return jsonValue.Value<bool>();
            }
            else if (jsonValue.Type == JTokenType.String)
            {
                return jsonValue.Value<string>();
            }

            throw new FormatException();
        }

        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The Newtonsoft.Json.JsonWriter to write to.</param>
        /// <param name="value">The value.</param>
        /// <param name="serializer">The calling serializer.</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}