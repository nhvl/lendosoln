﻿namespace LendersOffice.Common
{
    using System.Text;
    using System.Web.UI.WebControls;

    /// <summary>
    /// Represents a data file.
    /// </summary>
    public class FileData
    {
        /// <summary>
        /// The name of the file.
        /// </summary>
        private string name;
        
        /// <summary>
        /// The byte content of the file.
        /// </summary>
        private byte[] bytes;

        /// <summary>
        /// The character encoding for the file.
        /// </summary>
        private Encoding encoding;

        /// <summary>
        /// The string content of the file.
        /// </summary>
        private string contents;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileData"/> class.
        /// </summary>
        /// <param name="fileUpload">A FileUpload UI control.</param>
        /// <param name="encoding">The file encoding.</param>
        public FileData(FileUpload fileUpload, Encoding encoding = null)
        {
            this.encoding = encoding ?? Encoding.UTF8;
            this.name = fileUpload.FileName;
            this.bytes = fileUpload.FileBytes;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileData"/> class.
        /// </summary>
        /// <param name="fileName">The file name.</param>
        /// <param name="contents">The file contents.</param>
        /// <param name="encoding">The file encoding.</param>
        public FileData(string fileName, string contents, Encoding encoding = null)
        {
            this.encoding = encoding ?? Encoding.UTF8;
            this.name = fileName;
            this.bytes = this.encoding.GetBytes(contents);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileData"/> class.
        /// </summary>
        /// <param name="fileName">The file name.</param>
        /// <param name="contents">The file contents.</param>
        public FileData(string fileName, byte[] contents)
        {
            this.encoding = Encoding.UTF8;
            this.name = fileName;
            this.bytes = contents;
        }

        /// <summary>
        /// Gets the length of the content in bytes.
        /// </summary>
        /// <value>The length of the content in bytes.</value>
        public int FileLength
        {
            get
            {
                return this.bytes.Length;
            }
        }

        /// <summary>
        /// Gets the file name.
        /// </summary>
        /// <value>The file name.</value>
        public string FileName
        {
            get
            {
                return this.name;
            }
        }

        /// <summary>
        /// Gets the byte content of the file.
        /// </summary>
        /// <value>The byte content of the file.</value>
        public byte[] FileBytes
        {
            get
            {
                return this.bytes;
            }
        }

        /// <summary>
        /// Gets the contents of the file in UTF-8.
        /// </summary>
        /// <value>The contents of the file in UTF-8.</value>
        public string Contents
        {
            get
            {
                if (string.IsNullOrEmpty(this.contents))
                {
                    this.contents = this.encoding.GetString(this.bytes);
                }

                return this.contents;
            }
        }
    }
}
