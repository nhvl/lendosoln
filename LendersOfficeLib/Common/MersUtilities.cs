using System;
using System.Text.RegularExpressions;

namespace LendersOffice.Common
{
	public class MersUtilities
	{
        public static string GenerateMersNumber(DataAccess.CLoanFileNamer.LoanNamingData info)
        {
            //if we actually have a mers ID for the broker, generate a mers MIN
            if (!string.IsNullOrEmpty(info.mersOrganizationId))
            {
                return GenerateMersNumber(info.mersOrganizationId, info.mersCounter);
            }
            else
            {
                //Otherwise, just leave it blank
                return "";
            }
        }

        /// <summary>
        /// Warning: what should go in the counter part of this method varies by broker. 
        /// Sometimes it'll be the MERS counter, but sometimes it'll be the loan number.
        /// Generate 18 characters MERS number. OrganizationId need to be 7 digits long.
        /// </summary>
        /// <param name="organizationId"></param>
        /// <param name="counter"></param>
        /// <returns></returns>
        public static string GenerateMersNumber(string organizationId, long counter) 
        {
            // Mers Number has following format.
            // 7 organization id
            // 10 sequential counter
            // 1 check digit using MOD 10 weight 2 algorithm. 

            if (organizationId.Length != 7)
                throw new ArgumentException("OrganizationId need to be 7 characters long");

            string ret = organizationId + counter.ToString("D10");

            int checkDigit = CalcCheckDigit(ret);

            return ret + checkDigit;
        }

        private static int CalcCheckDigit(string ret)
        {
            int sum = 0;
            int i = 2;
            foreach (char ch in ret.ToCharArray())
            {
                int b = int.Parse(ch.ToString()) * i;

                sum += ((b / 10) + (b % 10));

                i = i == 2 ? 1 : 2;
            }
            int checkDigit = 0;

            if ((sum % 10) != 0)
                checkDigit = 10 - (sum % 10);
            return checkDigit;
        }
	}
}
