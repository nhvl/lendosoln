﻿//-----------------------------------------------------------------------
// <copyright file="CompressionHelper.cs" company="Meridianlink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <author>David Dao, Scott Kibler</author>
// <summary>Class to faciilitate compression and decompression.</summary>
//-----------------------------------------------------------------------
namespace LendersOffice.Common
{
    using System.Collections.Generic;
    using System.IO;
    using System.IO.Compression;
    using System.Text;
    using DataAccess;
    using ICSharpCode.SharpZipLib.Core;
    using ICSharpCode.SharpZipLib.Zip;

    /// <summary>
    /// Class to facilitate compression and decompression.
    /// </summary>
    public static class CompressionHelper
    {
        /// <summary>
        /// The conventional extension for GZip files.
        /// </summary>
        private const string GZipExtension = ".gz";

        /// <summary>
        /// Pass empty string for fileFilter to include all file. <para></para>
        /// Uses SharpZipLib.
        /// </summary>
        /// <param name="pathName">The directory where the source file is.</param>
        /// <param name="outputZipFile">The name of the zip file to be created.</param>
        /// <param name="fileFilter">Empty for all, or specific file name, or search pattern.</param>
        public static void CompressDirectory(string pathName, string outputZipFile, string fileFilter)
        {
            FastZip fastZip = new FastZip();
            fastZip.CreateZip(outputZipFile, pathName, true, fileFilter);
        }

        /// <summary>
        /// Compresses to a GZip file using <see cref="GZipStream"/>.
        /// </summary>
        /// <param name="sourceFullFileName">The full file name (including path) to the source file.</param>
        /// <param name="targetFullFileNameWithoutExtension">The full file name of the file to be written, except for the extension since that will be GZipExtension.</param>
        /// <returns>The full target file name (with the GZip standard extension).</returns>
        public static string CompressToGZip(string sourceFullFileName, string targetFullFileNameWithoutExtension)
        {
            // adapted from http://stackoverflow.com/a/11153588/420667, http://stackoverflow.com/a/1581706/420667
            var fi = new FileInfo(sourceFullFileName);
            var targetFullFileName = targetFullFileNameWithoutExtension + CompressionHelper.GZipExtension;
            
            using (FileStream sourceFS = fi.OpenRead())
            using (FileStream outputFS = File.Create(targetFullFileName))
            using (GZipStream compressedStream = new GZipStream(outputFS, CompressionMode.Compress))
            {
                sourceFS.CopyTo(compressedStream);
            }

            return targetFullFileName;
        }

        /// <summary>
        /// Decompresses a GZip file using <see cref="GZipStream"/>.
        /// </summary>
        /// <param name="sourceFullFileName">The full file path and name of the file to be decompressed.</param>
        /// <param name="targetFullFileName">The full file path and name of the resulting decompressed file.</param>
        public static void DecompressGZip(string sourceFullFileName, string targetFullFileName)
        {
            // adapted from http://stackoverflow.com/a/11153588/420667, http://stackoverflow.com/a/1581706/420667
            var fi = new FileInfo(sourceFullFileName);

            using (FileStream sourceFS = fi.OpenRead())
            using (FileStream outputFS = File.Create(targetFullFileName))
            using (GZipStream compressedStream = new GZipStream(outputFS, CompressionMode.Decompress))
            {
                sourceFS.CopyTo(compressedStream);
            }
        }

        /// <summary>
        /// Compresses a file to a zip archive using System.IO.Compression.
        /// </summary>
        /// <param name="sourceFileInfo">The file info for the source file.</param>
        /// <param name="targetFileInfo">The file info for the target file.</param>
        /// <param name="deleteFileIfExists">If true, deletes the target file if it already exists.</param>
        public static void CompressWithIOCompression(FileInfo sourceFileInfo, FileInfo targetFileInfo, bool deleteFileIfExists)
        {
            CompressWithIOCompression(null, new FileInfo[] { sourceFileInfo }, targetFileInfo, deleteFileIfExists);   
        }

        /// <summary>
        /// Compresses a file to a zip archive using System.IO.Compression.
        /// </summary>
        /// <param name="sourceFileDBEntryInfos">The FileDB entries to be zipped.</param>
        /// <param name="sourceFileInfos">The info of the files to be zipped.</param>
        /// <param name="targetFileInfo">The file info for the target file.</param>
        /// <param name="deleteFileIfExists">If true, deletes the target file if it already exists.</param>
        public static void CompressWithIOCompression(
            IEnumerable<FileDBEntryInfo> sourceFileDBEntryInfos, 
            IEnumerable<FileInfo> sourceFileInfos, 
            FileInfo targetFileInfo,
            bool deleteFileIfExists)
        {
            if (sourceFileDBEntryInfos == null)
            {
                sourceFileDBEntryInfos = new List<FileDBEntryInfo>();
            }

            if (sourceFileInfos == null)
            {
                sourceFileInfos = new List<FileInfo>();
            }

            if (deleteFileIfExists)
            {
                if (targetFileInfo.Exists)
                {
                    targetFileInfo.Delete();
                }
            }

            using (ZipArchive zip = System.IO.Compression.ZipFile.Open(targetFileInfo.FullName, ZipArchiveMode.Create))
            {
                foreach (var sourceFileInfo in sourceFileInfos)
                {
                    zip.CreateEntryFromFile(sourceFileInfo.FullName, sourceFileInfo.Name);
                }

                foreach (var sourceFileDBEntryInfo in sourceFileDBEntryInfos)
                {
                    FileDBTools.UseFile(
                        sourceFileDBEntryInfo.FileDBType,
                        sourceFileDBEntryInfo.Key,
                        (fi) => zip.CreateEntryFromFile(fi.FullName, sourceFileDBEntryInfo.Key));
                }
            }
        }

        /// <summary>
        /// Decompresses a zip file using System.IO.Compression.
        /// </summary>
        /// <param name="sourceArchiveFileInfo">The full file path and name of the source zip archive.</param>
        /// <param name="targetDirectoryInfo">The directory to extract the files to.</param>
        /// <param name="deleteExisting">If true, deletes the existing directory (recursively) if it's already there.</param>
        public static void DecompressIOCompressedFile(FileInfo sourceArchiveFileInfo, DirectoryInfo targetDirectoryInfo, bool deleteExisting)
        {
            if (deleteExisting)
            {
                if (targetDirectoryInfo.Exists)
                {
                    targetDirectoryInfo.Delete(true);
                }
            }

            System.IO.Compression.ZipFile.ExtractToDirectory(sourceArchiveFileInfo.FullName, targetDirectoryInfo.FullName);            
        }

        /// <summary>
        /// Be careful with this as it uses a lot of contiguous memory. <para></para>
        /// Uses <see cref="GZipStream"/>.
        /// </summary>
        /// <param name="value">The string to compress.</param>
        /// <returns>A byte array storing the compressed version of the original string.</returns>
        public static byte[] Compress(string value)
        {
            byte[] bytes = null;
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            using (MemoryStream stream = new MemoryStream())
            {
                using (GZipStream gzipStream = new GZipStream(stream, CompressionMode.Compress))
                {
                    byte[] inputBytes = UTF8Encoding.ASCII.GetBytes(value);
                    gzipStream.Write(inputBytes, 0, inputBytes.Length);
                }

                bytes = stream.ToArray();
            }

            return bytes;
        }

        /// <summary>
        /// Decompresses the byte array provided to the full original string using <see cref="GZipStream"/>.
        /// </summary>
        /// <param name="bytes">They compressed data as an array of bytes.</param>
        /// <returns>The decompressed string.</returns>
        public static string Decompress(byte[] bytes)
        {
            if (bytes == null || bytes.Length == 0)
            {
                return string.Empty;
            }

            using (MemoryStream stream = new MemoryStream())
            {
                using (GZipStream gzipStream = new GZipStream(new MemoryStream(bytes), CompressionMode.Decompress))
                {
                    byte[] buffer = new byte[4096];
                    int bytesRead = gzipStream.Read(buffer, 0, buffer.Length);
                    while (bytesRead > 0)
                    {
                        stream.Write(buffer, 0, bytesRead);
                        bytesRead = gzipStream.Read(buffer, 0, buffer.Length);
                    }
                }

                return UTF8Encoding.ASCII.GetString(stream.ToArray());
            }
        }

        /// <summary>
        /// Creates an archive given a list of memory streams.
        /// </summary>
        /// <param name="files">A dictionary using the fileName as the key, and the bytes array for the file contents.</param>
        /// <returns>An archive string content.</returns>
        public static string CreateZipArchiveFromFileDict(Dictionary<string, byte[]> files)
        {
            using (MemoryStream memStream = new MemoryStream())
            {
                using (ZipOutputStream zipStream = new ZipOutputStream(memStream))
                {
                    zipStream.SetLevel(3);

                    foreach (KeyValuePair<string, byte[]> pair in files)
                    {
                        using (MemoryStream fileStream = new MemoryStream(pair.Value))
                        {
                            ZipEntry newEntry = new ZipEntry(pair.Key);
                            newEntry.DateTime = System.DateTime.Now;
                            zipStream.PutNextEntry(newEntry);

                            StreamUtils.Copy(fileStream, zipStream, new byte[4096]);
                        }
                    }

                    zipStream.CloseEntry();
                    zipStream.IsStreamOwner = false;
                    zipStream.Close();
                    return System.Text.Encoding.Default.GetString(memStream.GetBuffer(), 0, (int)memStream.Position);
                }
            }
        }
    }
}
