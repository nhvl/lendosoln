using System;

namespace LendersOffice.Common
{
	public class LoanNotFoundException : NotFoundException
	{
		public LoanNotFoundException() : base(ErrorMessages.LoanNotFound, ErrorMessages.LoanNotFound)
		{
            this.IsEmailDeveloper = false;
		}

        public LoanNotFoundException(Guid sLId) : base(ErrorMessages.LoanNotFound, "sLId = " + sLId + " is not found.") 
        {
            this.IsEmailDeveloper = false;
        }
	}
}
