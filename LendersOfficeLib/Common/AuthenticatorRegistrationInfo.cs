﻿namespace LendersOffice.Common
{
    /// <summary>
    /// The response for the banana tfa barcode call.
    /// </summary>
    public class AuthenticatorRegistrationInfo
    {
        /// <summary>
        /// Gets or sets the seed used for registration.
        /// </summary>
        public string Seed { get; set; }
        
        /// <summary>
        /// Gets or sets the manual code the user can enter.
        /// </summary>
        public string ManualCode { get; set; }

        /// <summary>
        /// Gets or sets the qr code url. 
        /// </summary>
        public string QrCodeUrl { get; set; }
    }
}
