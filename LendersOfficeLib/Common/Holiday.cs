namespace LendersOffice.Common
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Provides some holiday calculations used for Lock Policy closure calculations.
    /// </summary>
    public class Holiday
    {
        /// <summary>
        /// The holidays available for lock policy closures.
        /// </summary>
        public static readonly IReadOnlyList<Holiday> AvailableLockPolicyHolidays = new ReadOnlyCollection<Holiday>(new[]
        {
            new Holiday("New Year's Day", "NewYearsDay", year => new DateTime(year, 1, 1)),
            new Holiday("Martin Luther King, Jr. Day", "MartinLutherKingJrDay", year => NthDayOfWeekInMonth(3, DayOfWeek.Monday, year, 1)),
            new Holiday("President's Day", "PresidentsDay", year => NthDayOfWeekInMonth(3, DayOfWeek.Monday, year, 2)),
            new Holiday("Good Friday", "GoodFriday", year => CalculateEaster(year).AddDays(-2)),
            new Holiday("Easter", "Easter", CalculateEaster),
            new Holiday("Memorial Day", "MemorialDay", CalculateMemorialDay),
            new Holiday("Independence Day", "IndependenceDay", year => new DateTime(year, 7, 4)),
            new Holiday("Labor Day", "LaborDay", year => NthDayOfWeekInMonth(1, DayOfWeek.Monday, year, 9)),
            new Holiday("Columbus Day", "ColumbusDay", year => NthDayOfWeekInMonth(2, DayOfWeek.Monday, year, 10)),
            new Holiday("Veteran's Day", "VeteransDay", CalculateVeteransDay),
            new Holiday("Thanksgiving Day", "ThanksgivingDay", CalculateThanksgivingDay),
            new Holiday("Day After Thanksgiving", "DayAfterThanksgiving", year => CalculateThanksgivingDay(year).AddDays(1)),
            new Holiday("Christmas Eve", "ChristmasEve", year => new DateTime(year, 12, 24)),
            new Holiday("Christmas Day", "ChristmasDay", year => new DateTime(year, 12, 25)),
            new Holiday("New Years Eve", "NewYearsEve", year => new DateTime(year, 12, 31)),
        });

        /// <summary>
        /// The calculation of the holiday's date in a given year.
        /// </summary>
        private readonly Func<int, DateTime> calculationForYear;

        /// <summary>
        /// Initializes a new instance of the <see cref="Holiday"/> class.
        /// </summary>
        /// <param name="name">The name of the holiday, as presented to humans.</param>
        /// <param name="enumName">The unchanging name of the holiday, that is safe for storage.</param>
        /// <param name="calculationForYear">The calculation used to determine when the holiday falls in a given year.</param>
        private Holiday(string name, string enumName, Func<int, DateTime> calculationForYear)
        {
            this.Name = name;
            this.EnumName = enumName;
            this.calculationForYear = calculationForYear;
        }

        /// <summary>
        /// Gets the name of the holiday, as displayed to a user.
        /// </summary>
        /// <value>The name of the holiday.</value>
        public string Name { get; }

        /// <summary>
        /// Gets the unchanging name of the holiday as it would appear in an enumeration.  This is the value stored in the DB.
        /// </summary>
        /// <value>The name of the holiday as it would appear in an enumeration.</value>
        public string EnumName { get; }

        /// <summary>
        /// Calculates the date of the holiday in a specified year.
        /// </summary>
        /// <param name="year">The year of the holiday.</param>
        /// <returns>The date of the holiday.</returns>
        public DateTime CalculateDate(int year)
        {
            return this.calculationForYear(year);
        }

        /// <summary>
        /// Calculates the next occurence of the holiday on or after a specified date.
        /// </summary>
        /// <param name="date">The date of the start for the holiday search.</param>
        /// <returns>The date of the holiday.</returns>
        public DateTime NextOccurrenceOnOrAfter(DateTime date)
        {
            DateTime nextDate = this.CalculateDate(date.Year);
            if (nextDate < date.Date)
            {
                nextDate = this.CalculateDate(date.Year + 1);
            }

            return nextDate;
        }

        /// <summary>
        /// Given a positive n, finds the nth occurrence of the specified day of the week in the given month.
        /// </summary>
        /// <param name="n">The occurrence count of the day of the week.</param>
        /// <param name="dayOfWeek">The day of the week to track down.</param>
        /// <param name="year">The year of the month to search through.</param>
        /// <param name="month">The month to search through.</param>
        /// <returns>A date matching the specifications.</returns>
        private static DateTime NthDayOfWeekInMonth(int n, DayOfWeek dayOfWeek, int year, int month)
        {
            if (n < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(n));
            }

            var minimumDate = new DateTime(year, month, (7 * n) - 6);
            if (minimumDate.Year != year || minimumDate.Month != month)
            {
                throw new ArgumentOutOfRangeException("Invalid number of weeks in month");
            }

            if (minimumDate.DayOfWeek == dayOfWeek)
            {
                return minimumDate;
            }

            int day = minimumDate.Day + dayOfWeek - minimumDate.DayOfWeek + (dayOfWeek > minimumDate.DayOfWeek ? 0 : 7);
            return new DateTime(year, month, day);
        }

        /// <summary>
        /// Calculates Easter for a given year.
        /// </summary>
        /// <param name="year">The year to determine the date of Easter for.</param>
        /// <returns>The date of Easter that year.</returns>
        private static DateTime CalculateEaster(int year)
        {
            // Used formula to calculate Easter found from http://aa.usno.navy.mil/faq/docs/easter.php
            int c = year / 100;
            int n = year - (19 * (year / 19));
            int k = (c - 17) / 25;
            int i = c - (c / 4) - ((c - k) / 3) + (19 * n) + 15;
            i = i - (30 * (i / 30));
            i = i - ((i / 28) * (1 - ((i / 28) * (29 / (i + 1))
                * ((21 - n) / 11))));
            int j = year + (year / 4) + i + 2 - c + (c / 4);
            j = j - (7 * (j / 7));
            int l = i - j;
            int month = 3 + ((l + 40) / 44);
            int day = l + 28 - (31 * (month / 4));

            return new DateTime(year, month, day);
        }

        /// <summary>
        /// Calculates Memorial Day for a given year.
        /// </summary>
        /// <param name="year">The year to determine the date of Memorial Day for.</param>
        /// <returns>The date of Memorial Day that year.</returns>
        /// <remarks>
        /// I'd add a function to calculate the last occurrence of a day of the week in a month,
        /// but there isn't a place to reuse it.
        /// </remarks>
        private static DateTime CalculateMemorialDay(int year)
        {
            // Calculated by the last Monday in May
            DateTime minimumDate = new DateTime(year, 5, 25);
            DayOfWeek desiredDayOfWeek = DayOfWeek.Monday;
            if (minimumDate.DayOfWeek == desiredDayOfWeek)
            {
                return minimumDate;
            }

            int day = minimumDate.Day + desiredDayOfWeek - minimumDate.DayOfWeek + (desiredDayOfWeek > minimumDate.DayOfWeek ? 0 : 7);
            return new DateTime(year, 5, day);
        }

        /// <summary>
        /// Calculates Veterans' Day for a given year.
        /// </summary>
        /// <param name="year">The year to determine the date of Veterans' Day for.</param>
        /// <returns>The date of Veterans' Day that year.</returns>
        private static DateTime CalculateVeteransDay(int year)
        {
            // Veteran's Day is November 11, or the nearest weekday
            DateTime vd = new DateTime(year, 11, 11);

            if (vd.DayOfWeek == DayOfWeek.Saturday)
            {
                return new DateTime(year, 11, 10);
            }
            else if (vd.DayOfWeek == DayOfWeek.Sunday)
            {
                return new DateTime(year, 11, 12);
            }

            return vd;
        }

        /// <summary>
        /// Calculates Thanksgiving Day for a given year.
        /// </summary>
        /// <param name="year">The year to determine the date of Thanksgiving Day for.</param>
        /// <returns>The date of Thanksgiving Day that year.</returns>
        /// <remarks>Needed for the day after calculations.</remarks>
        private static DateTime CalculateThanksgivingDay(int year)
        {
            return NthDayOfWeekInMonth(4, DayOfWeek.Thursday, year, 11);
        }
    }
}
