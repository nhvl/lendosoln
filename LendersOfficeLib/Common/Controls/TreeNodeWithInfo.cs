﻿// <copyright file="TreeNodeWithInfo.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   4/3/2015
// </summary>

namespace LendersOffice.Common.Controls
{
    using System.Collections.Generic;

    /// <summary>
    /// Has an additional info property that doesn't get rendered to the page. <para></para>
    /// At some point may want to fix up ChildNodes to be of the same type.
    /// http://weblogs.asp.net/dannychen/436454 .
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1650:ElementDocumentationMustBeSpelledCorrectly", Justification = "Reviewed.")]
    public class TreeNodeWithInfo : System.Web.UI.WebControls.TreeNode
    {
        /// <summary>
        /// Dictionary of key value pairs with additional info, doesn't get rendered to page.
        /// </summary>
        private Dictionary<string, string> info = new Dictionary<string, string>();

        /// <summary>
        /// Gets a dictionary of key value pairs with additional info, doesn't get rendered to page.
        /// </summary>
        /// <value>The dictionary of additional info.</value>
        public Dictionary<string, string> Info
        {
            get { return this.info; }
        }
    }
}
