﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace LendersOffice.Common.Controls
{
    /// <summary>
    /// Allows user to specify how to render
    /// </summary>
    public class CustomHtmlControl : HtmlControl
    {
        public Action<HtmlTextWriter> RenderAction { get; set; }

        protected override void Render(HtmlTextWriter writer)
        {
            if (RenderAction != null)
            {
                RenderAction(writer);
            }
        }
    }
}
