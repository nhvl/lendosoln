﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.Common
{
    public enum E_LpeErrorAreaT
    {
        Schema,
        MapProcessor,
        WebBotDownload,
        EmailBotDownload,
        RateUpload,
        LpeUpdateGeneral,
    }
}
