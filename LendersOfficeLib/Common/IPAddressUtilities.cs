﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace LendersOffice.Common
{

    public static class IPAddressUtilities
    {
        private static string[] s_specialInternalIpList = {
                                                              // 4/18/2014 dd - Pull from CheckIpForUser stored procedure
                                                              // Not sure if this IP still valid. The comment from stored procedure
                                                              // is back in 2011.
                                                              "172.16.99.18"
                                                              };

        public static IEnumerable<string> SpecialInternalIpList
        {
            get { return s_specialInternalIpList; }
        }
        /// <summary>
        /// Check to see if IP address is within expected range.
        /// 
        /// // 4/18/2014 dd - As of today only handle IPv4 right now.
        /// 
        /// Use * as a wildcard for range.
        /// 
        /// Example:
        ///     12.10.1.*
        ///         is equivalent to
        ///             12.10.1.0 with subnet mask 255.255.255.0
        ///     12.10.*.*
        ///         is equivalent to 
        ///             12.10.0.0 with subnet mask 255.255.0.0
        ///             
        /// </summary>
        /// <param name="expectedIPAddr">Single IP address or IP range using wildcard (12.10.1.*). Empty string always return true</param>
        /// <param name="actualIpAddr">Single IP address to verify</param>
        /// <returns></returns>
        public static bool IsMatch(string expectedIPAddress, string actualIpAddress)
        {
            if (string.IsNullOrEmpty(actualIpAddress))
            {
                throw new ArgumentException("Actual IP cannot be empty.");
            }

            if (string.IsNullOrEmpty(expectedIPAddress))
            {
                return true;
            }
            // 4/18/2014 dd - To keep code simple, I will utilize the string comparison instead of go
            // through IPAddress class and convert to bytes array and loop through each byte.

            // NOTE: This method DOES NOT validate the string is valid IP address.
            //    So if you pass in  foo.bar.john.doe it will be okay.

            string[] expectedIpAddressParts = expectedIPAddress.Split('.');
            string[] actualIpAddressParts = actualIpAddress.Split('.');

            if (expectedIpAddressParts.Length != actualIpAddressParts.Length)
            {
                return false;
            }

            for (int i = 0; i < expectedIpAddressParts.Length; i++)
            {
                string expected = expectedIpAddressParts[i];
                string actual = actualIpAddressParts[i];
                if (expected == "*")
                {
                    continue; // Skip wildcard
                }
                if (expected != actual)
                {
                    return false;
                }
            }

            return true;

        }

        /// <summary>
        /// Check to see if IP address is within one of the expected range.
        /// 
        /// NOTE: This method will return true if the IP match one of the special internal IP address.
        /// </summary>
        /// <param name="expectedIPAddressList"></param>
        /// <param name="actualIpAddress"></param>
        /// <returns></returns>
        public static bool IsMatch(IEnumerable<string> expectedIPAddressList, string actualIpAddress)
        {
            if (expectedIPAddressList == null || expectedIPAddressList.Count() == 0)
            {
                return true;
            }

            foreach (var expectedIPAddress in expectedIPAddressList)
            {
                if (IsMatch(expectedIPAddress, actualIpAddress))
                {
                    return true;
                }
            }
            foreach (var expectedIPAddress in SpecialInternalIpList)
            {
                if (IsMatch(expectedIPAddress, actualIpAddress))
                {
                    return true;
                }

            }
            return false;
        }


    }
}
