﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using DataAccess;
using LendersOffice.Common.SerializationTypes;

namespace LendersOffice.Common
{
    /// <summary>
    /// The purpose of this class is to store very general purpose methods that may not otherwise have a place.
    /// </summary>
    public static class StaticMethodsAndExtensions
    {
        /// <summary>
        /// DO NOT USE THIS FOR NEW CODE.
        /// This method handles a common .NET 2.0 use case of <see cref="string.Trim()"/> to remove byte order marks.
        /// </summary>
        /// <param name="s">The string to trim.</param>
        /// <returns>The string, less whitespace and a BOM if one exists at the start.</returns>
        public static string TrimWhitespaceAndBOM(this string s)
        {
            // dd 7/17/2015
            // Starting .NET 4.0 string.TrimWhitespaceAndBOM() is no longer remove BOM marker.
            // We have some code that rely on the old Trim() method to remove BOM marker. 
            // Even I rewrite some code so BOM marker is not include, however I cannot guarantee I fix all the place. Therefore
            // this method is a fail safe.
            // Ideally we should not use this method to remove the BOM marker.

            if (string.IsNullOrEmpty(s))
            {
                return s;
            }

            string tmp = s.Trim();

            if (tmp.Length > 0 && tmp[0] == 0xFEFF)
            {
                // 7/17/2015 - dd - Ideally we need to have BOM marker remove before calling this method.
                Tools.LogError("Someone is using TrimAndTrimBOM to remove the BOM marker. Please investigate and update the caller.");
                return tmp.Substring(1);
            }
            else
            {
                return tmp;
            }

        }

        /// <summary>
        /// Normalizes new lines in a string with \r\n.
        /// </summary>
        /// <param name="input">The string normalize the new lines.</param>
        /// <returns>The normalized string.</returns>
        public static string NormalizeNewLines(this string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }

            return Regex.Replace(input, @"\r\n|\n\r|\n|\r", "\r\n");
        }

        /// <summary>
        /// Safe cvs string.
        /// </summary>
        /// <param name="s">Input string.</param>
        /// <returns>Return safe string for csv.</returns>
        public static string SafeCsvString(this string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }

            //// OPM 464778 Isaac Ribakoff wrote "RFC 4180 states CRLF, so \r and \n separate might be overkill. 
            //// It's still valid CSV though, so I'm fine with your suggestion (Timothy's suggestion)."
            if (s.IndexOfAny(new char[] { ',', '"', '\r', '\n' }) >= 0)
            {
                return "\"" + s.Replace("\"", "\"\"") + "\"";
            }

            return s;
        }

        public static T Max<T>(T a, T b) where T : IComparable { return (a.CompareTo(b) > 0) ? a : b; } 
        public static T Min<T>(T a, T b) where T : IComparable { return (a.CompareTo(b) > 0) ? b : a; }

        // extension method example, not great though:
        //public static T MaxBetween<T>(this T a, T b) where T : IComparable { return (a.CompareTo(b) > 0) ? b : a; } 

        /// <summary>
        /// Includes the start and finish.
        /// Unfortunately TryParse is not a member of some interface,
        /// so this method would need to be implemented for each numeric type (afaik)
        /// </summary>
        /// <param name="candidate"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static bool IsInRange(string candidate, int start, int end)
        {
            if (string.IsNullOrEmpty(candidate))
            {
                return false;
            }
            int candidateAsNumber;
            if (!Int32.TryParse(candidate, out candidateAsNumber))
            {
                return false;
            }

            return IsInRange(candidateAsNumber, start, end);
        }

        /// <summary>
        /// Determines whether a candidate number is in a given range (inclusive).
        /// </summary>
        /// <param name="candidate">The number to test.</param>
        /// <param name="start">The beginning of the range to test against.</param>
        /// <param name="end">The end of the range to test against.</param>
        /// <returns>A boolean indicating whether the candidate number falls in the given range.</returns>
        public static bool IsInRange(int candidate, int start, int end)
        {
            return start <= candidate && candidate <= end;
        }

        /// <summary>
        /// Returns a value indicating whether the specified System.String object occurs
        /// within this string. A parameter specifies the type of search to
        /// use for the specified string.
        /// </summary>
        /// <param name="str">The System.String object to seek within.</param>
        /// <param name="value">The System.String object to seek.</param>
        /// <param name="comparisonType">One of the System.StringComparison values.</param>
        /// <returns>true if the value parameter occurs within this string, or if value is the empty string (""); otherwise, false.</returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="value"/> is null.</exception>
        /// <exception cref="System.ArgumentException"><paramref name="comparisonType"/> is not a valid System.StringComparison value.</exception>
        public static bool Contains(this string str, string value, StringComparison comparisonType)
        {
            return str.IndexOf(value, comparisonType) >= 0;
        }

        /// <summary>
        /// Returns a value indicating whether the specified item equals another.
        /// </summary>
        /// <typeparam name="T">The type of item.</typeparam>
        /// <param name="item">The item to search for.</param>
        /// <param name="other">The item to check against.</param>
        /// <returns>true if the <paramref name="item"/> is equal to <paramref name="other"/>; otherwise, false.</returns>
        public static bool EqualsOneOf<T>(this T item, T other)
        {
            return EqualityComparer<T>.Default.Equals(item, other);
        }

        /// <summary>
        /// Returns a value indicating whether the specified item equals an item in a set of items.
        /// </summary>
        /// <typeparam name="T">The type of item.</typeparam>
        /// <param name="item">The item to search for.</param>
        /// <param name="firstChoice">The first item to check against.</param>
        /// <param name="secondChoice">The second item to check against.</param>
        /// <returns>true if the <paramref name="item"/> is equal to either choice; otherwise, false.</returns>
        public static bool EqualsOneOf<T>(this T item, T firstChoice, T secondChoice)
        {
            return EqualityComparer<T>.Default.Equals(item, firstChoice) 
                || EqualityComparer<T>.Default.Equals(item, secondChoice);
        }

        /// <summary>
        /// Returns a value indicating whether the specified item equals an item in a set of items.
        /// </summary>
        /// <typeparam name="T">The type of item.</typeparam>
        /// <param name="item">The item to search for.</param>
        /// <param name="firstChoice">The first item to check against.</param>
        /// <param name="secondChoice">The second item to check against.</param>
        /// <param name="thirdChoice">The third item to check against.</param>
        /// <returns>true if the <paramref name="item"/> is equal to any choice; otherwise, false.</returns>
        public static bool EqualsOneOf<T>(this T item, T firstChoice, T secondChoice, T thirdChoice)
        {
            return EqualityComparer<T>.Default.Equals(item, firstChoice)
                || EqualityComparer<T>.Default.Equals(item, secondChoice)
                || EqualityComparer<T>.Default.Equals(item, thirdChoice);
        }

        /// <summary>
        /// Returns a value indicating whether the specified item equals an item in the set of items.
        /// </summary>
        /// <typeparam name="T">The type of item.</typeparam>
        /// <param name="item">The item to search for.</param>
        /// <param name="others">The set of values to check item against.</param>
        /// <returns>true if the <paramref name="item"/> is in <paramref name="others"/>; otherwise, false.</returns>
        public static bool EqualsOneOf<T>(this T item, params T[] others)
        {
            return others.Contains(item);
        }

        /// <summary>
        /// Adds an item to <paramref name="collection"/>, provided it is not null.
        /// </summary>
        /// <typeparam name="T">The type of the elements in <paramref name="collection"/>.</typeparam>
        /// <param name="collection">The collection that will receive <paramref name="item"/>, provided it is not null.</param>
        /// <param name="item">The object to add to <paramref name="collection"/>.</param>
        public static void AddIfNotNull<T>(this ICollection<T> collection, T item)
        {
            if (item != null)
            {
                collection.Add(item);
            }
        }

        /// <summary>
        /// Adds a collection to <paramref name="collection"/>, provided it is not null.
        /// </summary>
        /// <typeparam name="T">The type of the elements in <paramref name="collection"/>.</typeparam>
        /// <param name="collection">The collection that will receive <paramref name="items"/>, provided it is not null.</param>
        /// <param name="items">The collection to add to <paramref name="collection"/>.</param>
        public static void AddRangeIfNotNull<T>(this List<T> collection, IEnumerable<T> items)
        {
            if (items != null)
            {
                collection.AddRange(items);
            }
        }

        /// <summary>
        /// Adds a string item to <paramref name="collection"/>, provided it is not null or the empty string.
        /// </summary>
        /// <param name="collection">The collection that will receive <paramref name="item"/>, provided it is not null or the empty string.</param>
        /// <param name="item">The string to add to <paramref name="collection"/>.</param>
        public static void AddIfNotNullOrEmpty(this ICollection<string> collection, string item)
        {
            if (item != string.Empty)
            {
                collection.AddIfNotNull(item);
            }
        }

        /// <summary>
        /// Converts <paramref name="element"/> into an instance of <see cref="System.Xml.XmlElement"/>.
        /// </summary>
        /// <param name="element">The element to convert.</param>
        /// <param name="ownerDocument">The document that will contain the created element.</param>
        /// <returns>An element that can be added to <paramref name="ownerDocument"/>.</returns>
        public static XmlElement ToXmlElement(this System.Xml.Linq.XElement element, XmlDocument ownerDocument)
        {
            return (XmlElement)ownerDocument.ReadNode(element.CreateReader());
        }

        /// <summary>
        /// Determines whether a given xml path exists in an xml element.
        /// </summary>
        /// <param name="xml">Xml element to be examined.</param>
        /// <param name="xPath">Xml path to be evaluated.</param>
        /// <returns>true if xml path exists in the given xml element, otherwise false.</returns>
        public static bool DoesXPathExist(XmlElement xml, string xPath)
        {
            return xml.SelectSingleNode(xPath) != null;
        }

        /// <summary>
        /// Shuffles the values of <paramref name="list"/> according to the
        /// values of <paramref name="rnd"/>.
        /// </summary>
        /// <typeparam name="T">The type of the items in <paramref name="list"/>.</typeparam>
        /// <param name="list">The list to shuffle.</param>
        /// <param name="rnd">A random number generator.</param>
        /// <remarks>Taken from http://stackoverflow.com/a/22668974/2946652</remarks>
        public static void Shuffle<T>(this IList<T> list, Random rnd)
        {
            for (var i = 0; i < list.Count; i++)
            {
                list.Swap(i, rnd.Next(i, list.Count));
            }
        }

        /// <summary>
        /// Given a list and two indices, swaps the values at those indices.
        /// </summary>
        /// <typeparam name="T">The type of the items in <paramref name="list"/>.</typeparam>
        /// <param name="list">The list to swap items within.</param>
        /// <param name="i">The index of the first item in the swap.</param>
        /// <param name="j">The index of the second item in the swap.</param>
        /// <remarks>Taken from http://stackoverflow.com/a/22668974/2946652</remarks>
        public static void Swap<T>(this IList<T> list, int i, int j)
        {
            var temp = list[i];
            list[i] = list[j];
            list[j] = temp;
        }

        /// <summary>
        /// Projects each element of a sequence into its index and filters the sequence based
        /// on a predicate applied to the corresponding element.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of <paramref name="source"/>.</typeparam>
        /// <param name="source">An <seealso cref="IEnumerable{T}"/> to filter into indices.</param>
        /// <param name="predicate">A function to test each element for a condition.</param>
        /// <returns>An <seealso cref="IEnumerable{T}"/> of the indices of elements in <paramref name="source"/> that satisfy the condition.</returns>
        public static IEnumerable<int> SelectIndicesWhere<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            int index = 0;
            foreach (var item in source)
            {
                if (predicate(item))
                {
                    yield return index;
                }

                index += 1;
            }
        }

        /// <summary>
        /// Coalesces the current nullable <seealso cref="IEnumerable{T}"/> with the empty enumerable, to allow
        /// for further method chaining and extension.
        /// </summary>
        /// <typeparam name="T">The type of <paramref name="source"/>.</typeparam>
        /// <param name="source">A nullable <seealso cref="IEnumerable{T}"/>.</param>
        /// <returns>An <seealso cref="IEnumerable{T}"/> that is not null.</returns>
        /// <remarks>
        /// This is to simplify <c>?? Enumerable.Empty&lt;T&gt;()</c>, which requires the full type name to be written
        /// and requires parenthesis in a more awkward way.  Do note, however, that <c>my?.prop.CoalesceWithEmpty()</c>
        /// will short circuit and return null if <c>my</c> is null, so instead we must write <c>(my?.prop).CoalesceWithEmpty()</c>.
        /// </remarks>
        public static IEnumerable<T> CoalesceWithEmpty<T>(this IEnumerable<T> source)
        {
            return source ?? Enumerable.Empty<T>();
        }

        /// <summary>
        /// Filters <paramref name="source"/> to exclude null items.
        /// </summary>
        /// <typeparam name="T">The type of the sequence.</typeparam>
        /// <param name="source">The sequence to filter.</param>
        /// <returns><paramref name="source"/> without any null entries.</returns>
        public static IEnumerable<T> WhereNotNull<T>(this IEnumerable<T> source)
        {
            return source.Where(t => t != null);
        }

        /// <summary>
        /// Filters <paramref name="source"/> to exclude null items and map to the value of nullables.
        /// </summary>
        /// <typeparam name="T">The type of the sequence.</typeparam>
        /// <param name="source">The sequence to filter.</param>
        /// <returns><paramref name="source"/> without any null entries.</returns>
        public static IEnumerable<T> WhereNotNull<T>(this IEnumerable<T?> source) where T : struct
        {
            return source.Where(t => t != null).Select(t => t.Value);
        }

        /// <summary>
        /// Returns the first element of the sequence that satisfies a condition or a null value if no such element is found.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of <paramref name="source"/>.</typeparam>
        /// <param name="source">An <see cref="IEnumerable{TSource}"/> to return an element from.</param>
        /// <param name="predicate">A function to test each element for a condition.</param>
        /// <returns><see langword="null"/> if <paramref name="source"/> is empty or if no element passes the test specified by<para/>
        /// <paramref name="predicate"/>; otherwise, the first element in <paramref name="source"/> that passes the test<para/>
        /// specified by <paramref name="predicate"/>.</returns>
        public static TSource? FirstOrNull<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate) where TSource : struct
        {
            return source.Cast<TSource?>().FirstOrDefault(o => predicate(o.Value));
        }

        /// <summary>
        /// Calculates the minimum element in a sequence by value. Ties
        /// are broken by selecting the first element.
        /// </summary>
        /// <typeparam name="TSource">
        /// The type of the elements of source.
        /// </typeparam>
        /// <typeparam name="TValue">
        /// The type of the value to compare.
        /// </typeparam>
        /// <param name="source">
        /// A sequence of values to determine the minimum value of.
        /// </param>
        /// <param name="selector">
        /// A transform function to apply to each element.
        /// </param>
        /// <returns>
        /// The element with the minimum value or default for 
        /// <typeparamref name="TSource"/> if no minimum could be found.
        /// </returns>
        public static TSource MinBy<TSource, TValue>(this IEnumerable<TSource> source, Func<TSource, TValue> selector)
            where TSource : class, IEquatable<TSource>
            where TValue: IComparable<TValue>
        {
            return SelectMinMaxBy(source, selector, compareResult => compareResult > 0);
        }

        /// <summary>
        /// Calculates the maximum element in a sequence by value. Ties
        /// are broken by selecting the first element.
        /// </summary>
        /// <typeparam name="TSource">
        /// The type of the elements of source.
        /// </typeparam>
        /// <typeparam name="TValue">
        /// The type of the value to compare.
        /// </typeparam>
        /// <param name="source">
        /// A sequence of values to determine the minimum value of.
        /// </param>
        /// <param name="selector">
        /// A transform function to apply to each element.
        /// </param>
        /// <returns>
        /// The element with the maximum value or default for 
        /// <typeparamref name="TSource"/> if no maximum could be found.
        /// </returns>
        public static TSource MaxBy<TSource, TValue>(this IEnumerable<TSource> source, Func<TSource, TValue> selector)
            where TSource : class, IEquatable<TSource>
            where TValue : IComparable<TValue>
        {
            return SelectMinMaxBy(source, selector, compareResult => compareResult < 0);
        }

        /// <summary>
        /// Splits a list of elements into chunks of at most the specified size.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the elements in the list.
        /// </typeparam>
        /// <param name="source">
        /// The source list.
        /// </param>
        /// <param name="chunkSize">
        /// The size of the chunks to divide the list into.
        /// </param>
        /// <returns>
        /// A list of source list chunks.
        /// </returns>
        public static IEnumerable<IEnumerable<T>> Chunk<T>(this List<T> source, int chunkSize)
        {
            if (source == null || chunkSize < 1)
            {
                throw new ArgumentException();
            }

            var maxChunks = (source.Count / chunkSize) + 1;
            var chunksByIndex = new Dictionary<int, LinkedList<T>>(maxChunks);

            var counter = 0;
            foreach (var element in source)
            {
                var index = counter / chunkSize;

                LinkedList<T> chunk;
                if (!chunksByIndex.TryGetValue(index, out chunk))
                {
                    chunk = new LinkedList<T>();
                    chunksByIndex[index] = chunk;
                }

                chunk.AddLast(element);
                ++counter;
            }

            foreach (var key in chunksByIndex.Keys)
            {
                yield return chunksByIndex[key];
            }
        }

        /// <summary>
        /// Calculates the maximum element in a sequence by value. Ties
        /// are broken by selecting the first element.
        /// </summary>
        /// <typeparam name="TSource">
        /// The type of the elements of source.
        /// </typeparam>
        /// <typeparam name="TValue">
        /// The type of the value to compare.
        /// </typeparam>
        /// <param name="source">
        /// A sequence of values to determine the minimum value of.
        /// </param>
        /// <param name="selector">
        /// A transform function to apply to each element.
        /// </param>
        /// <param name="isCurrentCompareToResultBetter">
        /// The function that processes the result of 
        /// the compare to call between the current value
        /// in the sequence and the previously selected
        /// best value to determine if the current value
        /// is the new best value in the sequence.
        /// </param>
        /// <returns>
        /// The element in the sequence with the minimum or maximum value.
        /// </returns>
        private static TSource SelectMinMaxBy<TSource, TValue>(
            IEnumerable<TSource> source, 
            Func<TSource, TValue> selector, 
            Func<int, bool> isCurrentCompareToResultBetter)
            where TSource : class, IEquatable<TSource>
            where TValue : IComparable<TValue>
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (selector == null)
            {
                throw new ArgumentNullException(nameof(selector));
            }

            if (isCurrentCompareToResultBetter == null)
            {
                throw new ArgumentNullException(nameof(isCurrentCompareToResultBetter));
            }

            var ret = default(TSource);
            foreach (var element in source)
            {
                if (ret == default(TSource))
                {
                    ret = element;
                    continue;
                }

                var retProperty = selector(ret);
                var elementProperty = selector(element);
                var compareToResult = retProperty.CompareTo(elementProperty);

                if (isCurrentCompareToResultBetter(compareToResult))
                {
                    ret = element;
                }
            }

            return ret;
        }

        /// <summary>
        /// Parses a string to one of the defined values in an enum.
        /// </summary>
        /// <typeparam name="TEnum">The enum to parse to.</typeparam>
        /// <param name="value">The string to parse.</param>
        /// <param name="result">The parsed enum.</param>
        /// <param name="ignoreCase">Whether or not we ignore the case.</param>
        /// <returns>True if parsed, false otherwise.</returns>
        public static bool TryParseDefine<TEnum>(this string value, out TEnum result, bool ignoreCase = false) where TEnum : struct, IConvertible
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("TEnum must be an enumerated type");
            }

            return Enum.TryParse(value, ignoreCase, out result) && Enum.IsDefined(typeof(TEnum), result);
        }

        public static bool IsReserveOrPrepaid(this FeeChange change)
        {
            var id = Guid.Parse(change.FeeId);
            return ClosingCostSetUtils.ConstantInitialEscrowLegacyFeeList.Contains(id)
                && id != DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId
                || change.IsPrepaid();
        }

        public static bool IsPrepaid(this FeeChange change)
        {
            var id = Guid.Parse(change.FeeId);
            return ClosingCostSetUtils.ConstantPrepaidEscrowFeeList.Contains(id)
                && id != DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId
                && id != DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId
                && id != DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId;
        }

        /// <summary>
        /// Converts a TriState value to a nullable boolean.
        /// </summary>
        /// <param name="value">The TriState value to convert.</param>
        /// <returns>A nullable boolean value.</returns>
        public static bool? ToNullableBool(this E_TriState value)
        {
            switch (value)
            {
                case E_TriState.Blank:
                    return null;
                case E_TriState.Yes:
                    return true;
                case E_TriState.No:
                    return false;
                default:
                    throw new UnhandledEnumException(value);
            }
        }

        /// <summary>
        /// Converts a TriState value to "Y", "N", or blank.
        /// </summary>
        /// <param name="value">The TriState value to convert.</param>
        /// <returns>The string value.</returns>
        public static string ToYN(this E_TriState value)
        {
            switch (value)
            {
                case E_TriState.Blank:
                    return string.Empty;
                case E_TriState.Yes:
                    return "Y";
                case E_TriState.No:
                    return "N";
                default:
                    throw new UnhandledEnumException(value);
            }
        }

        /// <summary>
        /// Converts a database value (which may be an instance of either <typeparamref name="T"/> or <see cref="DBNull"/>)
        /// to the logically equivalent nullable instance.
        /// </summary>
        /// <typeparam name="T">The type that <paramref name="databaseValue"/> actually may be.</typeparam>
        /// <param name="databaseValue">The value read from the database.</param>
        /// <returns>The instance as the nullable type.</returns>
        public static T? AsDbNullable<T>(this object databaseValue) where T : struct
        {
            return databaseValue is DBNull ? (T?)null : (T)databaseValue;
        }

        /// <summary>
        /// Converts a string to a nullable bool.
        /// </summary>
        /// <param name="value">The string value to convert.</param>
        /// <returns>A boolean value if the string can be successfully parsed, or null if it cannot.</returns>
        public static bool? ToNullableBool(this string value)
        {
            bool result;
            return bool.TryParse(value, out result) ? result : (bool?)null;
        }

        /// <summary>
        /// Casts an integer to a defined enum value.  Will throw if the value is not defined.
        /// </summary>
        /// <typeparam name="TEnum">The type of enum.</typeparam>
        /// <param name="value">The value to convert to the enum.</param>
        /// <returns>The value as an enum.</returns>
        public static TEnum CastDefined<TEnum>(this int value) where TEnum : struct
        {
            if (Enum.IsDefined(typeof(TEnum), value))
            {
                return (TEnum)(object)value;
            }

            throw new LqbGrammar.Exceptions.DeveloperException(
                LqbGrammar.DataTypes.ErrorMessage.SystemError,
                new LqbGrammar.Exceptions.SimpleContext("Unable to convert " + value + " to a defined value of " + typeof(TEnum)));
        }

        /// <summary>
        /// Converts a string to a nullable enum.
        /// </summary>
        /// <param name="value">The string value to convert.</param>
        /// <param name="ignoreCase">Whether to ignore case for enum name matching.</param>
        /// <returns>An enum value if the string can be successfully parsed, or null if it cannot.</returns>
        public static TEnum? ToNullableEnum<TEnum>(this string value, bool ignoreCase = false) where TEnum : struct
        {
            TEnum result;
            return Enum.TryParse(value, ignoreCase, out result) ? result : (TEnum?)null;
        }

        /// <summary>
        /// Converts a string to a nullable enum, returning null if the resulting value isn't parsed or isn't defined.
        /// </summary>
        /// <param name="value">The string value to convert.</param>
        /// <param name="ignoreCase">Whether to ignore case for enum name matching.</param>
        /// <returns>An enum value if the string is successfully parsed to a defined value, or null if it is not.</returns>
        public static TEnum? ToNullableEnumDefined<TEnum>(this string value, bool ignoreCase = false) where TEnum : struct
        {
            TEnum result;
            return Enum.TryParse(value, ignoreCase, out result) && Enum.IsDefined(typeof(TEnum), result) ? result : (TEnum?)null;
        }

        public static T? ToNullable<T>(this string s, TryParse<T> tryParseMethod) where T : struct
        {
            T result;
            return tryParseMethod(s, out result) ? result : (T?)null;
        }

        public static TValue GetValueOrNull<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> dictionary, TKey key) where TValue : class
        {
            TValue value;
            return dictionary.TryGetValue(key, out value) ? value : null;
        }

        public static TValue? GetValueOrNull<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue?> dictionary, TKey key) where TValue : struct
        {
            TValue? value;
            return dictionary.TryGetValue(key, out value) ? value : null;
        }

        public static TValue? GetNullableValue<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> dictionary, TKey key) where TValue : struct
        {
            TValue value;
            return dictionary.TryGetValue(key, out value) ? value : default(TValue?);
        }

        public static LqbGrammar.DataTypes.DataObjectIdentifier<TDataObjectKind, Guid> ToIdentifier<TDataObjectKind>(this Guid g)
            where TDataObjectKind : LqbGrammar.DataTypes.DataObjectKind
        {
            return LqbGrammar.DataTypes.DataObjectIdentifier<TDataObjectKind, Guid>.Create(g);
        }

        /// <summary>
        /// Computes the sum of a sequence of <see cref="LqbGrammar.DataTypes.Money"/> values.
        /// </summary>
        /// <param name="source">A sequence of <see cref="LqbGrammar.DataTypes.Money"/> values to calculate the sum of.</param>
        /// <returns>The sum of the values in the sequence.</returns>
        public static LqbGrammar.DataTypes.Money Sum(this IEnumerable<LqbGrammar.DataTypes.Money> source)
        {
            var sum = LqbGrammar.DataTypes.Money.Zero;
            foreach (var v in source)
            {
                sum += v;
            }

            return sum;
        }

        /// <summary>
        /// Computes the sum of a sequence of nullable <see cref="LqbGrammar.DataTypes.Money"/> values.
        /// </summary>
        /// <param name="source">A sequence of nullable <see cref="LqbGrammar.DataTypes.Money"/> values to calculate the sum of.</param>
        /// <returns>The sum of the values in the sequence.</returns>
        public static LqbGrammar.DataTypes.Money Sum(this IEnumerable<LqbGrammar.DataTypes.Money?> source)
        {
            var sum = LqbGrammar.DataTypes.Money.Zero;
            foreach (var v in source)
            {
                if (v.HasValue)
                {
                    sum += v.Value;
                }
            }

            return sum;
        }

        /// <summary>
        /// Computes the sum of the sequence of <see cref="LqbGrammar.DataTypes.Money"/> values that are obtained by
        /// invoking a transform function on each element of the input sequence.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <param name="source">A sequence of values that are used to calculate a sum.</param>
        /// <param name="selector">A transform function to apply to each element.</param>
        /// <returns>The sum of the projected values.</returns>
        public static LqbGrammar.DataTypes.Money Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, LqbGrammar.DataTypes.Money> selector)
        {
            return Sum(Enumerable.Select(source, selector));
        }

        /// <summary>
        /// Computes the sum of the sequence of nullable <see cref="LqbGrammar.DataTypes.Money"/> values that are obtained
        /// by invoking a transform function on each element of the input sequence.
        /// </summary>
        /// <typeparam name="TSource">The type of the elements of source.</typeparam>
        /// <param name="source">A sequence of values that are used to calculate a sum.</param>
        /// <param name="selector">A transform function to apply to each element.</param>
        /// <returns>The sum of the projected values.</returns>
        public static LqbGrammar.DataTypes.Money Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, LqbGrammar.DataTypes.Money?> selector)
        {
            return Sum(Enumerable.Select(source, selector));
        }

        /// <summary>
        /// Forces a nullable instance to the actual value, or throws an exception.
        /// </summary>
        /// <typeparam name="T">The type of nullable we're operating on.</typeparam>
        /// <param name="nullable">The nullable struct to solidify.</param>
        /// <returns>The actual instance contained in <paramref name="nullable"/>.</returns>
        /// <exception cref="LqbGrammar.Exceptions.DeveloperException"><paramref name="nullable"/> is null.</exception>
        /// <remarks>
        /// This is intended primarily for use with the semantic datatypes in the project.  In our code, I
        /// frequently see custom function or inline throw calls for handling every case where the string
        /// value did not parse into a correct value.  This function is intended to be an easily referenced
        /// utility, and will hopefully make writing and debugging with the semantic types much easier.
        /// </remarks>
        public static T ForceValue<T>(this T? nullable) where T : struct
        {
            if (nullable.HasValue)
            {
                return nullable.Value;
            }

            throw new LqbGrammar.Exceptions.DeveloperException(
                LqbGrammar.DataTypes.ErrorMessage.SystemError,
                new LqbGrammar.Exceptions.SimpleContext("Unable to get value of " + typeof(T)));
        }

        /// <summary>
        /// Opens the connection and returns itself; used to avoid nesting using statements.
        /// </summary>
        /// <typeparam name="TDbConnection">The connection type.</typeparam>
        /// <param name="connection">The connection to open.</param>
        /// <returns><paramref name="connection"/>.</returns>
        public static TDbConnection OpenDbConnection<TDbConnection>(this TDbConnection connection) where TDbConnection : System.Data.IDbConnection
        {
            connection.Open();
            return connection;
        }
    }

    public delegate bool TryParse<T>(string s, out T result);
}
