﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace LendersOffice.Common.SerializationTypes
{
    // NOTE: BE VERY CAREFUL IF YOU ARE MAKING ANY CHANGES TO THE INTERFACE OR
    // GFEARCHIVE CLASS WHICH COULD INTERFERE WITH SERIALIZATION OR DESERIALIZATION.
    // If deserialization fails and the user reverts the closing cost migration from opm 
    // 150695, we do NOT maintain the data that we were unable to deserialize and data
    // loss could result. 2/14/14 gf
    public interface IGFEArchive
    {
        string DateArchived { get; }
        bool IsClosingCostMigrationArchive { get; }

        Dictionary<string, object> Fields { get; }

        #region fields that need to be gotten
        Boolean sConsummationDLckd { get; }
        String sConsummationD_rep { get; }
        Boolean sEstCloseDLckd { get; }
        String sEstCloseD_rep { get; }
        String sOpenedD_rep { get; }
        String sDue_rep { get; }
        Boolean sIsRateLocked { get; }
        String sTerm_rep { get; }
        Boolean sSchedDueD1Lckd { get; }
        String sSchedDueD1_rep { get; }
        String sNoteIR_rep { get; }
        String sFinalLAmt_rep { get; }
        E_sFinMethT sFinMethT { get; }
        String sDaysInYr_rep { get; }
        String sRAdj1stCapR_rep { get; }
        String sRAdjCapR_rep { get; }
        String sRAdjLifeCapR_rep { get; }
        String sRAdjCapMon_rep { get; }
        String sRAdj1stCapMon_rep { get; }
        String sPmtAdjCapR_rep { get; }
        String sPmtAdjMaxBalPc_rep { get; }
        String sPmtAdjRecastStop_rep { get; }
        String sPmtAdjRecastPeriodMon_rep { get; }
        String sPmtAdjCapMon_rep { get; }
        String sIOnlyMon_rep { get; }
        String sGfeNoteIRAvailTillD_Date { get; }
        bool sGfeNoteIRAvailTillDLckd { get; }
        String sGfeNoteIRAvailTillD_Time { get; }
        String sGfeEstScAvailTillD_Date { get; }
        bool sGfeEstScAvailTillDLckd { get; }
        String sGfeEstScAvailTillD_Time { get; }
        String sGfeRateLockPeriod_rep { get; }
        String sGfeLockPeriodBeforeSettlement_rep { get; }
        E_sTimeZoneT sGfeNoteIRAvailTillDTimeZoneT { get; }
        E_sTimeZoneT sGfeEstScAvailTillDTimeZoneT { get; }
        Boolean sIsPrintTimeForGfeNoteIRAvailTillD { get; }
        Boolean sIsPrintTimeForsGfeEstScAvailTillD { get; }
        Boolean sGfeCanRateIncrease { get; }
        String sRLifeCapR_rep { get; }
        String sGfeFirstInterestChangeIn { get; }
        Boolean sGfeCanLoanBalanceIncrease { get; }
        String sGfeMaxLoanBalance_rep { get; }
        Boolean sGfeCanPaymentIncrease { get; }
        String sGfeFirstPaymentChangeIn { get; }
        String sGfeFirstAdjProThisMPmtAndMIns_rep { get; }
        String sGfeMaxProThisMPmtAndMIns_rep { get; }
        Boolean sGfeHavePpmtPenalty { get; }
        String sGfeMaxPpmtPenaltyAmt_rep { get; }
        Boolean sGfeIsBalloon { get; }
        String sGfeBalloonPmt_rep { get; }
        String sGfeBalloonDueInYrs { get; }
        Boolean sMldsHasImpound { get; }
        Boolean sIsRequireFeesFromDropDown { get; }
        Boolean sGfeUsePaidToFromOfficialContact { get; }
        Boolean sGfeRequirePaidToFromContacts { get; }
        String s800U5F_rep { get; }
        String s800U5FDesc { get; }
        String s800U4F_rep { get; }
        String s800U4FDesc { get; }
        String s800U3F_rep { get; }
        String s800U3FDesc { get; }
        String s800U2F_rep { get; }
        String s800U2FDesc { get; }
        String s800U1F_rep { get; }
        String s800U1FDesc { get; }
        String sWireF_rep { get; }
        String sUwF_rep { get; }
        String sProcF_rep { get; }
        Boolean sProcFPaid { get; }
        String sTxServF_rep { get; }
        String sFloodCertificationF_rep { get; }
        E_FloodCertificationDeterminationT sFloodCertificationDeterminationT { get; }
        String sMBrokF_rep { get; }
        String sMBrokFMb_rep { get; }
        String sMBrokFPc_rep { get; }
        E_PercentBaseT sMBrokFBaseT { get; }
        String sInspectF_rep { get; }
        String sCrF_rep { get; }
        Boolean sCrFPaid { get; }
        String sApprF_rep { get; }
        Boolean sApprFPaid { get; }
        String sLOrigF_rep { get; }
        String sLOrigFMb_rep { get; }
        String sLOrigFPc_rep { get; }
        String s900U1Pia_rep { get; }
        String s900U1PiaDesc { get; }
        String sVaFf_rep { get; }
        String s904Pia_rep { get; }
        String s904PiaDesc { get; }
        String sHazInsPia_rep { get; }
        String sHazInsPiaMon_rep { get; }
        E_PercentBaseT sProHazInsT { get; }
        String sProHazInsR_rep { get; }
        String sProHazInsMb_rep { get; }
        String sMipPia_rep { get; }
        String sIPia_rep { get; }
        String sIPiaDy_rep { get; }
        Boolean sIPiaDyLckd { get; }
        Boolean sIPerDayLckd { get; }
        String sIPerDay_rep { get; }
        String sAggregateAdjRsrv_rep { get; }
        Boolean sAggregateAdjRsrvLckd { get; }
        String s1007Rsrv_rep { get; }
        String s1007ProHExp_rep { get; }
        String s1007RsrvMon_rep { get; }
        String s1007ProHExpDesc { get; }
        String s1006Rsrv_rep { get; }
        String s1006ProHExp_rep { get; }
        String s1006RsrvMon_rep { get; }
        String s1006ProHExpDesc { get; }
        String sU3Rsrv_rep { get; }
        String sProU3Rsrv_rep { get; }
        String sU3RsrvMon_rep { get; }
        String sU3RsrvDesc { get; }
        String sU4Rsrv_rep { get; }
        String sProU4Rsrv_rep { get; }
        String sU4RsrvMon_rep { get; }
        String sU4RsrvDesc { get; }
        String sFloodInsRsrv_rep { get; }
        String sProFloodIns_rep { get; }
        String sFloodInsRsrvMon_rep { get; }
        String sRealETxRsrv_rep { get; }
        E_PercentBaseT sProRealETxT { get; }
        String sProRealETxMb_rep { get; }
        String sProRealETxR_rep { get; }
        String sRealETxRsrvMon_rep { get; }
        String sSchoolTxRsrv_rep { get; }
        String sProSchoolTx_rep { get; }
        String sSchoolTxRsrvMon_rep { get; }
        String sMInsRsrv_rep { get; }
        String sProMIns_rep { get; }
        String sMInsRsrvMon_rep { get; }
        String sHazInsRsrv_rep { get; }
        String sProHazIns_rep { get; }
        String sHazInsRsrvMon_rep { get; }
        String sU4Tc_rep { get; }
        String sU4TcDesc { get; }
        String sU3Tc_rep { get; }
        String sU3TcDesc { get; }
        String sU2Tc_rep { get; }
        String sU2TcDesc { get; }
        String sU1Tc_rep { get; }
        String sU1TcDesc { get; }
        String sTitleInsF_rep { get; }
        String sTitleInsFTable { get; }
        String sAttorneyF_rep { get; }
        String sNotaryF_rep { get; }
        String sDocPrepF_rep { get; }
        String sEscrowF_rep { get; }
        String sOwnerTitleInsF_rep { get; }
        String sEscrowFTable { get; }
        String sU3GovRtc_rep { get; }
        String sU3GovRtcMb_rep { get; }
        E_PercentBaseT sU3GovRtcBaseT { get; }
        String sU3GovRtcPc_rep { get; }
        String sU3GovRtcDesc { get; }
        String sU2GovRtc_rep { get; }
        String sU2GovRtcMb_rep { get; }
        E_PercentBaseT sU2GovRtcBaseT { get; }
        String sU2GovRtcPc_rep { get; }
        String sU2GovRtcDesc { get; }
        String sU1GovRtc_rep { get; }
        String sU1GovRtcMb_rep { get; }
        E_PercentBaseT sU1GovRtcBaseT { get; }
        String sU1GovRtcPc_rep { get; }
        String sU1GovRtcDesc { get; }
        String sStateRtcDesc { get; }
        String sStateRtc_rep { get; }
        String sStateRtcMb_rep { get; }
        E_PercentBaseT sStateRtcBaseT { get; }
        String sStateRtcPc_rep { get; }
        String sCountyRtcDesc { get; }
        String sCountyRtc_rep { get; }
        String sCountyRtcMb_rep { get; }
        E_PercentBaseT sCountyRtcBaseT { get; }
        String sCountyRtcPc_rep { get; }
        String sRecFDesc { get; }
        String sRecF_rep { get; }
        String sRecFMb_rep { get; }
        E_PercentBaseT sRecBaseT { get; }
        String sRecFPc_rep { get; }
        Boolean sRecFLckd { get; }
        String sRecDeed_rep { get; }
        String sRecMortgage_rep { get; }
        String sRecRelease_rep { get; }
        String sU5Sc_rep { get; }
        String sU5ScDesc { get; }
        String sU4Sc_rep { get; }
        String sU4ScDesc { get; }
        String sU3Sc_rep { get; }
        String sU3ScDesc { get; }
        String sU2Sc_rep { get; }
        String sU2ScDesc { get; }
        String sU1Sc_rep { get; }
        String sU1ScDesc { get; }
        String sPestInspectF_rep { get; }
        String sGfeTotalEstimateSettlementCharge_rep { get; }
        String sGfeOriginationF_rep { get; }
        String sLDiscnt_rep { get; }
        String sGfeAdjOriginationCharge_rep { get; }
        String sGfeRequiredServicesTotalFee_rep { get; }
        String sGfeLenderTitleTotalFee_rep { get; }
        String sGfeOwnerTitleTotalFee_rep { get; }
        String sGfeServicesYouShopTotalFee_rep { get; }
        String sGfeGovtRecTotalFee_rep { get; }
        String sGfeTransferTaxTotalFee_rep { get; }
        String sGfeInitialImpoundDeposit_rep { get; }
        String sGfeDailyInterestTotalFee_rep { get; }
        String sGfeHomeOwnerInsuranceTotalFee_rep { get; }
        String sGfeTotalOtherSettlementServiceFee_rep { get; }
        String sGfeNotApplicableF_rep { get; }
        String sGfeTradeOffLowerCCLoanAmt_rep { get; }
        String sGfeTradeOffLowerCCNoteIR_rep { get; }
        String sGfeTradeOffLowerCCMPmtAndMInsDiff_rep { get; }
        String sGfeTradeOffLowerCCClosingCostDiff_rep { get; }
        String sGfeTradeOffLowerCCClosingCost_rep { get; }
        String sGfeTradeOffLowerRateLoanAmt_rep { get; }
        String sGfeTradeOffLowerRateNoteIR_rep { get; }
        String sGfeTradeOffLowerRateMPmtAndMInsDiff_rep { get; }
        String sGfeTradeOffLowerRateClosingCostDiff_rep { get; }
        String sGfeTradeOffLowerRateClosingCost_rep { get; }
        String sGfeProThisMPmtAndMIns_rep { get; }
        String sGfeTradeOffLowerCCMPmtAndMIns_rep { get; }
        String sGfeTradeOffLowerRateMPmtAndMIns_rep { get; }
        String sGfeShoppingCartLoan1OriginatorName { get; }
        String sGfeShoppingCartLoan1LoanAmt_rep { get; }
        String sGfeShoppingCartLoan1LoanTerm_rep { get; }
        String sGfeShoppingCartLoan1NoteIR_rep { get; }
        String sGfeShoppingCartLoan1InitialPmt_rep { get; }
        String sGfeShoppingCartLoan1RateLockPeriod_rep { get; }
        E_TriState sGfeShoppingCartLoan1CanRateIncreaseTri { get; }
        E_TriState sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri { get; }
        E_TriState sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri { get; }
        E_TriState sGfeShoppingCartLoan1HavePpmtPenaltyTri { get; }
        E_TriState sGfeShoppingCartLoan1IsBalloonTri { get; }
        String sGfeShoppingCartLoan1TotalClosingCost_rep { get; }
        String sGfeShoppingCartLoan2OriginatorName { get; }
        String sGfeShoppingCartLoan2LoanAmt_rep { get; }
        String sGfeShoppingCartLoan2LoanTerm_rep { get; }
        String sGfeShoppingCartLoan2NoteIR_rep { get; }
        String sGfeShoppingCartLoan2InitialPmt_rep { get; }
        String sGfeShoppingCartLoan2RateLockPeriod_rep { get; }
        E_TriState sGfeShoppingCartLoan2CanRateIncreaseTri { get; }
        E_TriState sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri { get; }
        E_TriState sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri { get; }
        E_TriState sGfeShoppingCartLoan2HavePpmtPenaltyTri { get; }
        E_TriState sGfeShoppingCartLoan2IsBalloonTri { get; }
        String sGfeShoppingCartLoan2TotalClosingCost_rep { get; }
        String sGfeShoppingCartLoan3OriginatorName { get; }
        String sGfeShoppingCartLoan3LoanAmt_rep { get; }
        String sGfeShoppingCartLoan3LoanTerm_rep { get; }
        String sGfeShoppingCartLoan3NoteIR_rep { get; }
        String sGfeShoppingCartLoan3InitialPmt_rep { get; }
        String sGfeShoppingCartLoan3RateLockPeriod_rep { get; }
        E_TriState sGfeShoppingCartLoan3CanRateIncreaseTri { get; }
        E_TriState sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri { get; }
        E_TriState sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri { get; }
        E_TriState sGfeShoppingCartLoan3HavePpmtPenaltyTri { get; }
        E_TriState sGfeShoppingCartLoan3IsBalloonTri { get; }
        String sGfeShoppingCartLoan3TotalClosingCost_rep { get; }
        String sLpTemplateNm { get; }
        String sCcTemplateNm { get; }
        Int32 sLOrigFProps { get; }
        Int32 sGfeOriginatorCompFProps { get; }
        Int32 sApprFProps { get; }
        Int32 sCrFProps { get; }
        Int32 sInspectFProps { get; }
        Int32 sMBrokFProps { get; }
        Int32 sTxServFProps { get; }
        Int32 sFloodCertificationFProps { get; }
        Int32 sProcFProps { get; }
        Int32 sUwFProps { get; }
        Int32 sWireFProps { get; }
        Int32 s800U1FProps { get; }
        Int32 s800U2FProps { get; }
        Int32 s800U3FProps { get; }
        Int32 s800U4FProps { get; }
        Int32 s800U5FProps { get; }
        Int32 sIPiaProps { get; }
        Int32 sMipPiaProps { get; }
        Int32 sHazInsPiaProps { get; }
        Int32 s904PiaProps { get; }
        Int32 sVaFfProps { get; }
        Int32 s900U1PiaProps { get; }
        Int32 sHazInsRsrvProps { get; }
        Int32 sMInsRsrvProps { get; }
        Int32 sSchoolTxRsrvProps { get; }
        Int32 sRealETxRsrvProps { get; }
        Int32 sFloodInsRsrvProps { get; }
        Int32 s1006RsrvProps { get; }
        Int32 s1007RsrvProps { get; }
        Int32 sU3RsrvProps { get; }
        Int32 sU4RsrvProps { get; }
        Int32 sAggregateAdjRsrvProps { get; }
        Int32 sEscrowFProps { get; }
        Int32 sOwnerTitleInsProps { get; }
        Int32 sDocPrepFProps { get; }
        Int32 sNotaryFProps { get; }
        Int32 sAttorneyFProps { get; }
        Int32 sTitleInsFProps { get; }
        Int32 sU1TcProps { get; }
        Int32 sU2TcProps { get; }
        Int32 sU3TcProps { get; }
        Int32 sU4TcProps { get; }
        Int32 sRecFProps { get; }
        Int32 sCountyRtcProps { get; }
        Int32 sStateRtcProps { get; }
        Int32 sU1GovRtcProps { get; }
        Int32 sU2GovRtcProps { get; }
        Int32 sU3GovRtcProps { get; }
        Int32 sPestInspectFProps { get; }
        Int32 sU1ScProps { get; }
        Int32 sU2ScProps { get; }
        Int32 sU3ScProps { get; }
        Int32 sU4ScProps { get; }
        Int32 sU5ScProps { get; }
        String sApprFPaidTo { get; }
        String sCrFPaidTo { get; }
        String sTxServFPaidTo { get; }
        String sFloodCertificationFPaidTo { get; }
        String sInspectFPaidTo { get; }
        String sProcFPaidTo { get; }
        String sUwFPaidTo { get; }
        String sWireFPaidTo { get; }
        String s800U1FPaidTo { get; }
        String s800U2FPaidTo { get; }
        String s800U3FPaidTo { get; }
        String s800U4FPaidTo { get; }
        String s800U5FPaidTo { get; }
        String sOwnerTitleInsPaidTo { get; }
        String sDocPrepFPaidTo { get; }
        String sNotaryFPaidTo { get; }
        String sAttorneyFPaidTo { get; }
        String sU1TcPaidTo { get; }
        String sU2TcPaidTo { get; }
        String sU3TcPaidTo { get; }
        String sU4TcPaidTo { get; }
        String sU1GovRtcPaidTo { get; }
        String sU2GovRtcPaidTo { get; }
        String sU3GovRtcPaidTo { get; }
        String sPestInspectPaidTo { get; }
        String sU1ScPaidTo { get; }
        String sU2ScPaidTo { get; }
        String sU3ScPaidTo { get; }
        String sU4ScPaidTo { get; }
        String sU5ScPaidTo { get; }
        String sHazInsPiaPaidTo { get; }
        String sMipPiaPaidTo { get; }
        String sVaFfPaidTo { get; }
        E_GfeSectionT s800U1FGfeSection { get; }
        E_GfeSectionT s800U2FGfeSection { get; }
        E_GfeSectionT s800U3FGfeSection { get; }
        E_GfeSectionT s800U4FGfeSection { get; }
        E_GfeSectionT s800U5FGfeSection { get; }
        E_GfeSectionT sEscrowFGfeSection { get; }
        E_GfeSectionT sDocPrepFGfeSection { get; }
        E_GfeSectionT sNotaryFGfeSection { get; }
        E_GfeSectionT sAttorneyFGfeSection { get; }
        E_GfeSectionT sU1TcGfeSection { get; }
        E_GfeSectionT sU2TcGfeSection { get; }
        E_GfeSectionT sU3TcGfeSection { get; }
        E_GfeSectionT sU4TcGfeSection { get; }
        E_GfeSectionT sU1GovRtcGfeSection { get; }
        E_GfeSectionT sU2GovRtcGfeSection { get; }
        E_GfeSectionT sU3GovRtcGfeSection { get; }
        E_GfeSectionT sU1ScGfeSection { get; }
        E_GfeSectionT sU2ScGfeSection { get; }
        E_GfeSectionT sU3ScGfeSection { get; }
        E_GfeSectionT sU4ScGfeSection { get; }
        E_GfeSectionT sU5ScGfeSection { get; }
        E_GfeSectionT s904PiaGfeSection { get; }
        E_GfeSectionT s900U1PiaGfeSection { get; }
        Boolean sGfeIsTPOTransaction { get; }
        Boolean sIsItemizeBrokerCommissionOnIFW { get; }
        E_sOriginatorCompensationPaymentSourceT sOriginatorCompensationPaymentSourceT { get; }
        String sGfeOriginatorCompF_rep { get; }
        String sGfeOriginatorCompFPc_rep { get; }
        String sGfeOriginatorCompFMb_rep { get; }
        E_PercentBaseT sGfeOriginatorCompFBaseT { get; }
        Boolean sIsOriginationCompensationSourceRequired { get; }
        E_BranchChannelT sBranchChannelT { get; }
        String sLDiscntFMb_rep { get; }
        String sLDiscntPc_rep { get; }
        E_PercentBaseT sLDiscntBaseT { get; }
        E_CreditLenderPaidItemT sGfeCreditLenderPaidItemT { get; }
        String sGfeCreditLenderPaidItemF_rep { get; }
        String sGfeLenderCreditFPc_rep { get; }
        String sGfeLenderCreditF_rep { get; }
        Int32 sGfeLenderCreditFProps { get; }
        String sGfeDiscountPointFPc_rep { get; }
        String sGfeDiscountPointF_rep { get; }
        Int32 sGfeDiscountPointFProps { get; }
        String sGfeInitialDisclosureD_rep { get; }
        String sGfeRedisclosureD_rep { get; }
        String sLastDisclosedGFEArchiveD_rep { get; }
        E_sLPurposeT sLPurposeT { get; }
        Nullable<DateTime> sGfeInitialDisclosureD { get; }
        String sSpAddr { get; }
        String sSpCity { get; }
        String sSpState { get; }
        String sSpZip { get; }
        String sGfeNoteIRAvailTillD_DateTime { get; }
        String sGfeEstScAvailTillD_DateTime { get; }
        String sDueInYr_rep { get; }
        E_sGfeCreditChargeT sGfeCreditChargeT { get; }
        String sGfeNoCreditChargeNoteIR { get; }
        String sGfeCreditNoteIR { get; }
        String sGfeCreditLDiscnt { get; }
        String sGfeChargeNoteIR { get; }
        String sGfeChargeLDiscnt { get; }
        Boolean sGfeHasImpoundDeposit { get; }
        String sGfeCanRateIncrease_rep { get; }
        String sGfeCanLoanBalanceIncrease_rep { get; }
        String sGfeHavePpmtPenalty_rep { get; }
        String sGfeIsBalloon_rep { get; }
        String sGfeShoppingCartLoan1CanRateIncreaseTri_rep { get; }
        String sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri_rep { get; }
        String sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri_rep { get; }
        String sGfeShoppingCartLoan1HavePpmtPenaltyTri_rep { get; }
        String sGfeShoppingCartLoan1IsBalloonTri_rep { get; }
        String sGfeShoppingCartLoan2CanRateIncreaseTri_rep { get; }
        String sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri_rep { get; }
        String sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri_rep { get; }
        String sGfeShoppingCartLoan2HavePpmtPenaltyTri_rep { get; }
        String sGfeShoppingCartLoan2IsBalloonTri_rep { get; }
        String sGfeShoppingCartLoan3CanRateIncreaseTri_rep { get; }
        String sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri_rep { get; }
        String sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri_rep { get; }
        String sGfeShoppingCartLoan3HavePpmtPenaltyTri_rep { get; }
        String sGfeShoppingCartLoan3IsBalloonTri_rep { get; }
        String sGfeService1WeSelectName { get; }
        String sGfeService2WeSelectName { get; }
        String sGfeService3WeSelectName { get; }
        String sGfeService4WeSelectName { get; }
        String sGfeService5WeSelectName { get; }
        String sGfeService6WeSelectName { get; }
        String sGfeService7WeSelectName { get; }
        String sGfeService8WeSelectName { get; }
        String sGfeService9WeSelectName { get; }
        String sGfeService10WeSelectName { get; }
        String sGfeService11WeSelectName { get; }
        String sGfeService12WeSelectName { get; }
        String sGfeService13WeSelectName { get; }
        String sGfeService14WeSelectName { get; }
        String sGfeService15WeSelectName { get; }
        String sGfeService16WeSelectName { get; }
        String sGfeService17WeSelectName { get; }
        String sGfeService18WeSelectName { get; }
        String sGfeService1WeSelectFee { get; }
        String sGfeService2WeSelectFee { get; }
        String sGfeService3WeSelectFee { get; }
        String sGfeService4WeSelectFee { get; }
        String sGfeService5WeSelectFee { get; }
        String sGfeService6WeSelectFee { get; }
        String sGfeService7WeSelectFee { get; }
        String sGfeService8WeSelectFee { get; }
        String sGfeService9WeSelectFee { get; }
        String sGfeService10WeSelectFee { get; }
        String sGfeService11WeSelectFee { get; }
        String sGfeService12WeSelectFee { get; }
        String sGfeService13WeSelectFee { get; }
        String sGfeService14WeSelectFee { get; }
        String sGfeService15WeSelectFee { get; }
        String sGfeService16WeSelectFee { get; }
        String sGfeService17WeSelectFee { get; }
        String sGfeService18WeSelectFee { get; }
        String sGfeService1YouCanShopName { get; }
        String sGfeService2YouCanShopName { get; }
        String sGfeService3YouCanShopName { get; }
        String sGfeService4YouCanShopName { get; }
        String sGfeService5YouCanShopName { get; }
        String sGfeService6YouCanShopName { get; }
        String sGfeService7YouCanShopName { get; }
        String sGfeService8YouCanShopName { get; }
        String sGfeService9YouCanShopName { get; }
        String sGfeService10YouCanShopName { get; }
        String sGfeService11YouCanShopName { get; }
        String sGfeService12YouCanShopName { get; }
        String sGfeService13YouCanShopName { get; }
        String sGfeService14YouCanShopName { get; }
        String sGfeService15YouCanShopName { get; }
        String sGfeService16YouCanShopName { get; }
        String sGfeService1YouCanShopFee { get; }
        String sGfeService2YouCanShopFee { get; }
        String sGfeService3YouCanShopFee { get; }
        String sGfeService4YouCanShopFee { get; }
        String sGfeService5YouCanShopFee { get; }
        String sGfeService6YouCanShopFee { get; }
        String sGfeService7YouCanShopFee { get; }
        String sGfeService8YouCanShopFee { get; }
        String sGfeService9YouCanShopFee { get; }
        String sGfeService10YouCanShopFee { get; }
        String sGfeService11YouCanShopFee { get; }
        String sGfeService12YouCanShopFee { get; }
        String sGfeService13YouCanShopFee { get; }
        String sGfeService14YouCanShopFee { get; }
        String sGfeService15YouCanShopFee { get; }
        String sGfeService16YouCanShopFee { get; }
        String sHazInsPiaPaidToPrint { get; }
        String sGfeHomeOwnerInsurance1Name { get; }
        String sGfeHomeOwnerInsurance1Fee { get; }
        String sGfeHomeOwnerInsurance2Name { get; }
        String sGfeHomeOwnerInsurance2Fee { get; }
        String sGfeHomeOwnerInsurance3Name { get; }
        String sGfeHomeOwnerInsurance3Fee { get; }
        Boolean sGfeHasImpoundDepositTax { get; }
        Boolean sGfeHasImpoundDepositInsurance { get; }
        Boolean sGfeHasImpoundDepositOther { get; }
        String sGfeHasImpoundDepositDescription { get; }
        Boolean sGfeRateLockPeriodLckd { get; }
        Boolean sHazInsRsrvMonLckd { get; }
        Boolean sMInsRsrvMonLckd { get; }
        Boolean sRealETxRsrvMonLckd { get; }
        Boolean sSchoolTxRsrvMonLckd { get; }
        Boolean sFloodInsRsrvMonLckd { get; }
        Boolean s1006RsrvMonLckd { get; }
        Boolean s1007RsrvMonLckd { get; }
        Boolean sU3RsrvMonLckd { get; }
        Boolean sU4RsrvMonLckd { get; }
        E_TriState sHazInsRsrvEscrowedTri { get; }
        E_TriState sMInsRsrvEscrowedTri { get; }
        E_TriState sRealETxRsrvEscrowedTri { get; }
        E_TriState sSchoolTxRsrvEscrowedTri { get; }
        E_TriState sFloodInsRsrvEscrowedTri { get; }
        E_TriState s1006RsrvEscrowedTri { get; }
        E_TriState s1007RsrvEscrowedTri { get; }
        E_TriState sU3RsrvEscrowedTri { get; }
        E_TriState sU4RsrvEscrowedTri { get; }
        E_sLT sLT { get; }
        bool sFfUfMipIsBeingFinanced { get; }
        E_sLenderCreditCalculationMethodT sLenderCreditCalculationMethodT { get; }
        E_MipFrequency sMipFrequency { get; }
        #endregion

        decimal? sLAmtCalc { get; }
        decimal? sPurchPrice { get; }
        decimal? sApprVal { get; }
        decimal? sSpOrigC { get; }
        decimal? sLotOrigC { get; }
        decimal? sFinalLAmt { get; }

        decimal? sLenderTargetInitialCreditAmt { get; } // OPM 143721.
        #region manually entered fields.
        string gfeTil_CompanyName { get; }
        string gfeTil_StreetAddr { get; }
        string gfeTil_City { get; }
        string gfeTil_State { get; }
        string gfeTil_Zip { get; }
        string gfeTil_PhoneOfCompany { get; }
        string gfeTil_EmailAddr { get; }
        string gfeTil_PrepareDate { get; }
        bool gfeTil_IsLocked { get; }
        E_AgentRoleT gfeTil_AgentRoleT { get; }

        string dataApp_aBNm_aCNm { get; }
        #endregion


    }

    public class GFEArchive : IGFEArchive
    {
        #region ( unusual defaults )
        private bool m_sEstCloseDLckd = true;
        private bool m_sGfeEstScAvailTillDLckd = true;
        private bool m_sGfeNoteIRAvailTillDLckd = true;
        private bool m_sSchedDueD1Lckd = true;
        private bool m_sGfeRateLockPeriodLckd = true;
        private bool m_sHazInsRsrvMonLckd = true;
        private bool m_sMInsRsrvMonLckd = true;
        private bool m_sRealETxRsrvMonLckd = true;
        private bool m_sSchoolTxRsrvMonLckd = true;
        private bool m_sFloodInsRsrvMonLckd = true;
        private bool m_s1006RsrvMonLckd = true;
        private bool m_s1007RsrvMonLckd = true;
        private bool m_sU3RsrvMonLckd = true;
        private bool m_sU4RsrvMonLckd = true;
        private E_TriState m_sHazInsRsrvEscrowedTri = E_TriState.Yes;
        private E_TriState m_sMInsRsrvEscrowedTri = E_TriState.Yes;
        private E_TriState m_sRealETxRsrvEscrowedTri = E_TriState.Yes;
        private E_TriState m_sSchoolTxRsrvEscrowedTri = E_TriState.Yes;
        private E_TriState m_sFloodInsRsrvEscrowedTri = E_TriState.Yes;
        private E_TriState m_s1006RsrvEscrowedTri = E_TriState.Yes;
        private E_TriState m_s1007RsrvEscrowedTri = E_TriState.Yes;
        private E_TriState m_sU3RsrvEscrowedTri = E_TriState.Yes;
        private E_TriState m_sU4RsrvEscrowedTri = E_TriState.Yes;

        decimal? m_sLAmtCalc = null;
        decimal? m_sPurchPrice = null;
        decimal? m_sApprVal = null;
        decimal? m_sSpOrigC = null;
        decimal? m_sLotOrigC = null;
        decimal? m_sFinalLAmt = null;
        #endregion


        public GFEArchive() { Fields = new Dictionary<string, object>(); } // need empty constructor for serialization. (and initialization)
        public string DateArchived { get; set; }
        public bool IsClosingCostMigrationArchive { get; set; }

        public Dictionary<string, object> Fields { get; set; }

        #region fields that need to be gotten
        public Boolean sConsummationDLckd { get; set; }
        public String sConsummationD_rep { get; set; }
        public Boolean sEstCloseDLckd { get { return m_sEstCloseDLckd; } set { m_sEstCloseDLckd = value; } }
        public String sEstCloseD_rep { get; set; }
        public String sOpenedD_rep { get; set; }
        public String sDue_rep { get; set; }
        public Boolean sIsRateLocked { get; set; }
        public String sTerm_rep { get; set; }
        public Boolean sSchedDueD1Lckd { get { return m_sSchedDueD1Lckd; } set { m_sSchedDueD1Lckd = value; } }
        public String sSchedDueD1_rep { get; set; }
        public String sNoteIR_rep { get; set; }
        public String sFinalLAmt_rep { get; set; }
        public E_sFinMethT sFinMethT { get; set; }
        public String sDaysInYr_rep { get; set; }
        public String sRAdj1stCapR_rep { get; set; }
        public String sRAdjCapR_rep { get; set; }
        public String sRAdjLifeCapR_rep { get; set; }
        public String sRAdjCapMon_rep { get; set; }
        public String sRAdj1stCapMon_rep { get; set; }
        public String sPmtAdjCapR_rep { get; set; }
        public String sPmtAdjMaxBalPc_rep { get; set; }
        public String sPmtAdjRecastStop_rep { get; set; }
        public String sPmtAdjRecastPeriodMon_rep { get; set; }
        public String sPmtAdjCapMon_rep { get; set; }
        public String sIOnlyMon_rep { get; set; }
        public String sGfeNoteIRAvailTillD_Date { get; set; }
        public bool sGfeNoteIRAvailTillDLckd { get { return m_sGfeNoteIRAvailTillDLckd; } set { m_sGfeNoteIRAvailTillDLckd = value; } }
        public String sGfeNoteIRAvailTillD_Time { get; set; }
        public String sGfeEstScAvailTillD_Date { get; set; }
        public bool sGfeEstScAvailTillDLckd { get { return m_sGfeEstScAvailTillDLckd; } set { m_sGfeEstScAvailTillDLckd = value; } }
        public String sGfeEstScAvailTillD_Time { get; set; }
        public String sGfeRateLockPeriod_rep { get; set; }
        public String sGfeLockPeriodBeforeSettlement_rep { get; set; }
        public E_sTimeZoneT sGfeNoteIRAvailTillDTimeZoneT { get; set; }
        public E_sTimeZoneT sGfeEstScAvailTillDTimeZoneT { get; set; }
        public Boolean sIsPrintTimeForGfeNoteIRAvailTillD { get; set; }
        public Boolean sIsPrintTimeForsGfeEstScAvailTillD { get; set; }
        public Boolean sGfeCanRateIncrease { get; set; }
        public String sRLifeCapR_rep { get; set; }
        public String sGfeFirstInterestChangeIn { get; set; }
        public Boolean sGfeCanLoanBalanceIncrease { get; set; }
        public String sGfeMaxLoanBalance_rep { get; set; }
        public Boolean sGfeCanPaymentIncrease { get; set; }
        public String sGfeFirstPaymentChangeIn { get; set; }
        public String sGfeFirstAdjProThisMPmtAndMIns_rep { get; set; }
        public String sGfeMaxProThisMPmtAndMIns_rep { get; set; }
        public Boolean sGfeHavePpmtPenalty { get; set; }
        public String sGfeMaxPpmtPenaltyAmt_rep { get; set; }
        public Boolean sGfeIsBalloon { get; set; }
        public String sGfeBalloonPmt_rep { get; set; }
        public String sGfeBalloonDueInYrs { get; set; }
        public Boolean sMldsHasImpound { get; set; }
        public Boolean sIsRequireFeesFromDropDown { get; set; }
        public Boolean sGfeUsePaidToFromOfficialContact { get; set; }
        public Boolean sGfeRequirePaidToFromContacts { get; set; }
        public String s800U5F_rep { get; set; }
        public String s800U5FDesc { get; set; }
        public String s800U4F_rep { get; set; }
        public String s800U4FDesc { get; set; }
        public String s800U3F_rep { get; set; }
        public String s800U3FDesc { get; set; }
        public String s800U2F_rep { get; set; }
        public String s800U2FDesc { get; set; }
        public String s800U1F_rep { get; set; }
        public String s800U1FDesc { get; set; }
        public String sWireF_rep { get; set; }
        public String sUwF_rep { get; set; }
        public String sProcF_rep { get; set; }
        public Boolean sProcFPaid { get; set; }
        public String sTxServF_rep { get; set; }
        public String sFloodCertificationF_rep { get; set; }
        public E_FloodCertificationDeterminationT sFloodCertificationDeterminationT { get; set; }
        public String sMBrokF_rep { get; set; }
        public String sMBrokFMb_rep { get; set; }
        public String sMBrokFPc_rep { get; set; }
        public E_PercentBaseT sMBrokFBaseT { get; set; }
        public String sInspectF_rep { get; set; }
        public String sCrF_rep { get; set; }
        public Boolean sCrFPaid { get; set; }
        public String sApprF_rep { get; set; }
        public Boolean sApprFPaid { get; set; }
        public String sLOrigF_rep { get; set; }
        public String sLOrigFMb_rep { get; set; }
        public String sLOrigFPc_rep { get; set; }
        public String s900U1Pia_rep { get; set; }
        public String s900U1PiaDesc { get; set; }
        public String sVaFf_rep { get; set; }
        public String s904Pia_rep { get; set; }
        public String s904PiaDesc { get; set; }
        public String sHazInsPia_rep { get; set; }
        public String sHazInsPiaMon_rep { get; set; }
        public E_PercentBaseT sProHazInsT { get; set; }
        public String sProHazInsR_rep { get; set; }
        public String sProHazInsMb_rep { get; set; }
        public String sMipPia_rep { get; set; }
        public String sIPia_rep { get; set; }
        public String sIPiaDy_rep { get; set; }
        public Boolean sIPiaDyLckd { get; set; }
        public Boolean sIPerDayLckd { get; set; }
        public String sIPerDay_rep { get; set; }
        public String sAggregateAdjRsrv_rep { get; set; }
        public Boolean sAggregateAdjRsrvLckd { get; set; }
        public String s1007Rsrv_rep { get; set; }
        public String s1007ProHExp_rep { get; set; }
        public String s1007RsrvMon_rep { get; set; }
        public String s1007ProHExpDesc { get; set; }
        public String s1006Rsrv_rep { get; set; }
        public String s1006ProHExp_rep { get; set; }
        public String s1006RsrvMon_rep { get; set; }
        public String s1006ProHExpDesc { get; set; }
        public String sU3Rsrv_rep { get; set; }
        public String sProU3Rsrv_rep { get; set; }
        public String sU3RsrvMon_rep { get; set; }
        public String sU3RsrvDesc { get; set; }
        public String sU4Rsrv_rep { get; set; }
        public String sProU4Rsrv_rep { get; set; }
        public String sU4RsrvMon_rep { get; set; }
        public String sU4RsrvDesc { get; set; }
        public String sFloodInsRsrv_rep { get; set; }
        public String sProFloodIns_rep { get; set; }
        public String sFloodInsRsrvMon_rep { get; set; }
        public String sRealETxRsrv_rep { get; set; }
        public E_PercentBaseT sProRealETxT { get; set; }
        public String sProRealETxMb_rep { get; set; }
        public String sProRealETxR_rep { get; set; }
        public String sRealETxRsrvMon_rep { get; set; }
        public String sSchoolTxRsrv_rep { get; set; }
        public String sProSchoolTx_rep { get; set; }
        public String sSchoolTxRsrvMon_rep { get; set; }
        public String sMInsRsrv_rep { get; set; }
        public String sProMIns_rep { get; set; }
        public String sMInsRsrvMon_rep { get; set; }
        public String sHazInsRsrv_rep { get; set; }
        public String sProHazIns_rep { get; set; }
        public String sHazInsRsrvMon_rep { get; set; }
        public String sU4Tc_rep { get; set; }
        public String sU4TcDesc { get; set; }
        public String sU3Tc_rep { get; set; }
        public String sU3TcDesc { get; set; }
        public String sU2Tc_rep { get; set; }
        public String sU2TcDesc { get; set; }
        public String sU1Tc_rep { get; set; }
        public String sU1TcDesc { get; set; }
        public String sTitleInsF_rep { get; set; }
        public String sTitleInsFTable { get; set; }
        public String sAttorneyF_rep { get; set; }
        public String sNotaryF_rep { get; set; }
        public String sDocPrepF_rep { get; set; }
        public String sEscrowF_rep { get; set; }
        public String sOwnerTitleInsF_rep { get; set; }
        public String sEscrowFTable { get; set; }
        public String sU3GovRtc_rep { get; set; }
        public String sU3GovRtcMb_rep { get; set; }
        public E_PercentBaseT sU3GovRtcBaseT { get; set; }
        public String sU3GovRtcPc_rep { get; set; }
        public String sU3GovRtcDesc { get; set; }
        public String sU2GovRtc_rep { get; set; }
        public String sU2GovRtcMb_rep { get; set; }
        public E_PercentBaseT sU2GovRtcBaseT { get; set; }
        public String sU2GovRtcPc_rep { get; set; }
        public String sU2GovRtcDesc { get; set; }
        public String sU1GovRtc_rep { get; set; }
        public String sU1GovRtcMb_rep { get; set; }
        public E_PercentBaseT sU1GovRtcBaseT { get; set; }
        public String sU1GovRtcPc_rep { get; set; }
        public String sU1GovRtcDesc { get; set; }
        public String sStateRtcDesc { get; set; }
        public String sStateRtc_rep { get; set; }
        public String sStateRtcMb_rep { get; set; }
        public E_PercentBaseT sStateRtcBaseT { get; set; }
        public String sStateRtcPc_rep { get; set; }
        public String sCountyRtcDesc { get; set; }
        public String sCountyRtc_rep { get; set; }
        public String sCountyRtcMb_rep { get; set; }
        public E_PercentBaseT sCountyRtcBaseT { get; set; }
        public String sCountyRtcPc_rep { get; set; }
        public String sRecFDesc { get; set; }
        public String sRecF_rep { get; set; }
        public String sRecFMb_rep { get; set; }
        public E_PercentBaseT sRecBaseT { get; set; }
        public String sRecFPc_rep { get; set; }
        public Boolean sRecFLckd { get; set; }
        public String sRecDeed_rep { get; set; }
        public String sRecMortgage_rep { get; set; }
        public String sRecRelease_rep { get; set; }
        public String sU5Sc_rep { get; set; }
        public String sU5ScDesc { get; set; }
        public String sU4Sc_rep { get; set; }
        public String sU4ScDesc { get; set; }
        public String sU3Sc_rep { get; set; }
        public String sU3ScDesc { get; set; }
        public String sU2Sc_rep { get; set; }
        public String sU2ScDesc { get; set; }
        public String sU1Sc_rep { get; set; }
        public String sU1ScDesc { get; set; }
        public String sPestInspectF_rep { get; set; }
        public String sGfeTotalEstimateSettlementCharge_rep { get; set; }
        public String sGfeOriginationF_rep { get; set; }
        public String sLDiscnt_rep { get; set; }
        public String sGfeAdjOriginationCharge_rep { get; set; }
        public String sGfeRequiredServicesTotalFee_rep { get; set; }
        public String sGfeLenderTitleTotalFee_rep { get; set; }
        public String sGfeOwnerTitleTotalFee_rep { get; set; }
        public String sGfeServicesYouShopTotalFee_rep { get; set; }
        public String sGfeGovtRecTotalFee_rep { get; set; }
        public String sGfeTransferTaxTotalFee_rep { get; set; }
        public String sGfeInitialImpoundDeposit_rep { get; set; }
        public String sGfeDailyInterestTotalFee_rep { get; set; }
        public String sGfeHomeOwnerInsuranceTotalFee_rep { get; set; }
        public String sGfeTotalOtherSettlementServiceFee_rep { get; set; }
        public String sGfeNotApplicableF_rep { get; set; }
        public String sGfeTradeOffLowerCCLoanAmt_rep { get; set; }
        public String sGfeTradeOffLowerCCNoteIR_rep { get; set; }
        public String sGfeTradeOffLowerCCMPmtAndMInsDiff_rep { get; set; }
        public String sGfeTradeOffLowerCCClosingCostDiff_rep { get; set; }
        public String sGfeTradeOffLowerCCClosingCost_rep { get; set; }
        public String sGfeTradeOffLowerRateLoanAmt_rep { get; set; }
        public String sGfeTradeOffLowerRateNoteIR_rep { get; set; }
        public String sGfeTradeOffLowerRateMPmtAndMInsDiff_rep { get; set; }
        public String sGfeTradeOffLowerRateClosingCostDiff_rep { get; set; }
        public String sGfeTradeOffLowerRateClosingCost_rep { get; set; }
        public String sGfeProThisMPmtAndMIns_rep { get; set; }
        public String sGfeTradeOffLowerCCMPmtAndMIns_rep { get; set; }
        public String sGfeTradeOffLowerRateMPmtAndMIns_rep { get; set; }
        public String sGfeShoppingCartLoan1OriginatorName { get; set; }
        public String sGfeShoppingCartLoan1LoanAmt_rep { get; set; }
        public String sGfeShoppingCartLoan1LoanTerm_rep { get; set; }
        public String sGfeShoppingCartLoan1NoteIR_rep { get; set; }
        public String sGfeShoppingCartLoan1InitialPmt_rep { get; set; }
        public String sGfeShoppingCartLoan1RateLockPeriod_rep { get; set; }
        public E_TriState sGfeShoppingCartLoan1CanRateIncreaseTri { get; set; }
        public E_TriState sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri { get; set; }
        public E_TriState sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri { get; set; }
        public E_TriState sGfeShoppingCartLoan1HavePpmtPenaltyTri { get; set; }
        public E_TriState sGfeShoppingCartLoan1IsBalloonTri { get; set; }
        public String sGfeShoppingCartLoan1TotalClosingCost_rep { get; set; }
        public String sGfeShoppingCartLoan2OriginatorName { get; set; }
        public String sGfeShoppingCartLoan2LoanAmt_rep { get; set; }
        public String sGfeShoppingCartLoan2LoanTerm_rep { get; set; }
        public String sGfeShoppingCartLoan2NoteIR_rep { get; set; }
        public String sGfeShoppingCartLoan2InitialPmt_rep { get; set; }
        public String sGfeShoppingCartLoan2RateLockPeriod_rep { get; set; }
        public E_TriState sGfeShoppingCartLoan2CanRateIncreaseTri { get; set; }
        public E_TriState sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri { get; set; }
        public E_TriState sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri { get; set; }
        public E_TriState sGfeShoppingCartLoan2HavePpmtPenaltyTri { get; set; }
        public E_TriState sGfeShoppingCartLoan2IsBalloonTri { get; set; }
        public String sGfeShoppingCartLoan2TotalClosingCost_rep { get; set; }
        public String sGfeShoppingCartLoan3OriginatorName { get; set; }
        public String sGfeShoppingCartLoan3LoanAmt_rep { get; set; }
        public String sGfeShoppingCartLoan3LoanTerm_rep { get; set; }
        public String sGfeShoppingCartLoan3NoteIR_rep { get; set; }
        public String sGfeShoppingCartLoan3InitialPmt_rep { get; set; }
        public String sGfeShoppingCartLoan3RateLockPeriod_rep { get; set; }
        public E_TriState sGfeShoppingCartLoan3CanRateIncreaseTri { get; set; }
        public E_TriState sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri { get; set; }
        public E_TriState sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri { get; set; }
        public E_TriState sGfeShoppingCartLoan3HavePpmtPenaltyTri { get; set; }
        public E_TriState sGfeShoppingCartLoan3IsBalloonTri { get; set; }
        public String sGfeShoppingCartLoan3TotalClosingCost_rep { get; set; }
        public String sLpTemplateNm { get; set; }
        public String sCcTemplateNm { get; set; }
        public Int32 sLOrigFProps { get; set; }
        public Int32 sGfeOriginatorCompFProps { get; set; }
        public Int32 sApprFProps { get; set; }
        public Int32 sCrFProps { get; set; }
        public Int32 sInspectFProps { get; set; }
        public Int32 sMBrokFProps { get; set; }
        public Int32 sTxServFProps { get; set; }
        public Int32 sFloodCertificationFProps { get; set; }
        public Int32 sProcFProps { get; set; }
        public Int32 sUwFProps { get; set; }
        public Int32 sWireFProps { get; set; }
        public Int32 s800U1FProps { get; set; }
        public Int32 s800U2FProps { get; set; }
        public Int32 s800U3FProps { get; set; }
        public Int32 s800U4FProps { get; set; }
        public Int32 s800U5FProps { get; set; }
        public Int32 sIPiaProps { get; set; }
        public Int32 sMipPiaProps { get; set; }
        public Int32 sHazInsPiaProps { get; set; }
        public Int32 s904PiaProps { get; set; }
        public Int32 sVaFfProps { get; set; }
        public Int32 s900U1PiaProps { get; set; }
        public Int32 sHazInsRsrvProps { get; set; }
        public Int32 sMInsRsrvProps { get; set; }
        public Int32 sSchoolTxRsrvProps { get; set; }
        public Int32 sRealETxRsrvProps { get; set; }
        public Int32 sFloodInsRsrvProps { get; set; }
        public Int32 s1006RsrvProps { get; set; }
        public Int32 s1007RsrvProps { get; set; }
        public Int32 sU3RsrvProps { get; set; }
        public Int32 sU4RsrvProps { get; set; }
        public Int32 sAggregateAdjRsrvProps { get; set; }
        public Int32 sEscrowFProps { get; set; }
        public Int32 sOwnerTitleInsProps { get; set; }
        public Int32 sDocPrepFProps { get; set; }
        public Int32 sNotaryFProps { get; set; }
        public Int32 sAttorneyFProps { get; set; }
        public Int32 sTitleInsFProps { get; set; }
        public Int32 sU1TcProps { get; set; }
        public Int32 sU2TcProps { get; set; }
        public Int32 sU3TcProps { get; set; }
        public Int32 sU4TcProps { get; set; }
        public Int32 sRecFProps { get; set; }
        public Int32 sCountyRtcProps { get; set; }
        public Int32 sStateRtcProps { get; set; }
        public Int32 sU1GovRtcProps { get; set; }
        public Int32 sU2GovRtcProps { get; set; }
        public Int32 sU3GovRtcProps { get; set; }
        public Int32 sPestInspectFProps { get; set; }
        public Int32 sU1ScProps { get; set; }
        public Int32 sU2ScProps { get; set; }
        public Int32 sU3ScProps { get; set; }
        public Int32 sU4ScProps { get; set; }
        public Int32 sU5ScProps { get; set; }
        public String sApprFPaidTo { get; set; }
        public String sCrFPaidTo { get; set; }
        public String sTxServFPaidTo { get; set; }
        public String sFloodCertificationFPaidTo { get; set; }
        public String sInspectFPaidTo { get; set; }
        public String sProcFPaidTo { get; set; }
        public String sUwFPaidTo { get; set; }
        public String sWireFPaidTo { get; set; }
        public String s800U1FPaidTo { get; set; }
        public String s800U2FPaidTo { get; set; }
        public String s800U3FPaidTo { get; set; }
        public String s800U4FPaidTo { get; set; }
        public String s800U5FPaidTo { get; set; }
        public String sOwnerTitleInsPaidTo { get; set; }
        public String sDocPrepFPaidTo { get; set; }
        public String sNotaryFPaidTo { get; set; }
        public String sAttorneyFPaidTo { get; set; }
        public String sU1TcPaidTo { get; set; }
        public String sU2TcPaidTo { get; set; }
        public String sU3TcPaidTo { get; set; }
        public String sU4TcPaidTo { get; set; }
        public String sU1GovRtcPaidTo { get; set; }
        public String sU2GovRtcPaidTo { get; set; }
        public String sU3GovRtcPaidTo { get; set; }
        public String sPestInspectPaidTo { get; set; }
        public String sU1ScPaidTo { get; set; }
        public String sU2ScPaidTo { get; set; }
        public String sU3ScPaidTo { get; set; }
        public String sU4ScPaidTo { get; set; }
        public String sU5ScPaidTo { get; set; }
        public String sHazInsPiaPaidTo { get; set; }
        public String sMipPiaPaidTo { get; set; }
        public String sVaFfPaidTo { get; set; }
        public E_GfeSectionT s800U1FGfeSection { get; set; }
        public E_GfeSectionT s800U2FGfeSection { get; set; }
        public E_GfeSectionT s800U3FGfeSection { get; set; }
        public E_GfeSectionT s800U4FGfeSection { get; set; }
        public E_GfeSectionT s800U5FGfeSection { get; set; }
        public E_GfeSectionT sEscrowFGfeSection { get; set; }
        public E_GfeSectionT sDocPrepFGfeSection { get; set; }
        public E_GfeSectionT sNotaryFGfeSection { get; set; }
        public E_GfeSectionT sAttorneyFGfeSection { get; set; }
        public E_GfeSectionT sU1TcGfeSection { get; set; }
        public E_GfeSectionT sU2TcGfeSection { get; set; }
        public E_GfeSectionT sU3TcGfeSection { get; set; }
        public E_GfeSectionT sU4TcGfeSection { get; set; }
        public E_GfeSectionT sU1GovRtcGfeSection { get; set; }
        public E_GfeSectionT sU2GovRtcGfeSection { get; set; }
        public E_GfeSectionT sU3GovRtcGfeSection { get; set; }
        public E_GfeSectionT sU1ScGfeSection { get; set; }
        public E_GfeSectionT sU2ScGfeSection { get; set; }
        public E_GfeSectionT sU3ScGfeSection { get; set; }
        public E_GfeSectionT sU4ScGfeSection { get; set; }
        public E_GfeSectionT sU5ScGfeSection { get; set; }
        public E_GfeSectionT s904PiaGfeSection { get; set; }
        public E_GfeSectionT s900U1PiaGfeSection { get; set; }
        public Boolean sGfeIsTPOTransaction { get; set; }
        public Boolean sIsItemizeBrokerCommissionOnIFW { get; set; }
        public E_sOriginatorCompensationPaymentSourceT sOriginatorCompensationPaymentSourceT { get; set; }
        public String sGfeOriginatorCompF_rep { get; set; }
        public String sGfeOriginatorCompFPc_rep { get; set; }
        public String sGfeOriginatorCompFMb_rep { get; set; }
        public E_PercentBaseT sGfeOriginatorCompFBaseT { get; set; }
        public Boolean sIsOriginationCompensationSourceRequired { get; set; }
        public E_BranchChannelT sBranchChannelT { get; set; }
        public String sLDiscntFMb_rep { get; set; }
        public String sLDiscntPc_rep { get; set; }
        public E_PercentBaseT sLDiscntBaseT { get; set; }
        public E_CreditLenderPaidItemT sGfeCreditLenderPaidItemT { get; set; }
        public String sGfeCreditLenderPaidItemF_rep { get; set; }
        public String sGfeLenderCreditFPc_rep { get; set; }
        public String sGfeLenderCreditF_rep { get; set; }
        public Int32 sGfeLenderCreditFProps { get; set; }
        public String sGfeDiscountPointFPc_rep { get; set; }
        public String sGfeDiscountPointF_rep { get; set; }
        public Int32 sGfeDiscountPointFProps { get; set; }
        public String sGfeInitialDisclosureD_rep { get; set; }
        public String sGfeRedisclosureD_rep { get; set; }
        public String sLastDisclosedGFEArchiveD_rep { get; set; }
        public E_sLPurposeT sLPurposeT { get; set; }
        public Nullable<DateTime> sGfeInitialDisclosureD { get; set; }
        public String sSpAddr { get; set; }
        public String sSpCity { get; set; }
        public String sSpState { get; set; }
        public String sSpZip { get; set; }
        public String sGfeNoteIRAvailTillD_DateTime { get; set; }
        public String sGfeEstScAvailTillD_DateTime { get; set; }
        public String sDueInYr_rep { get; set; }
        public E_sGfeCreditChargeT sGfeCreditChargeT { get; set; }
        public String sGfeNoCreditChargeNoteIR { get; set; }
        public String sGfeCreditNoteIR { get; set; }
        public String sGfeCreditLDiscnt { get; set; }
        public String sGfeChargeNoteIR { get; set; }
        public String sGfeChargeLDiscnt { get; set; }
        public Boolean sGfeHasImpoundDeposit { get; set; }
        public String sGfeCanRateIncrease_rep { get; set; }
        public String sGfeCanLoanBalanceIncrease_rep { get; set; }
        public String sGfeHavePpmtPenalty_rep { get; set; }
        public String sGfeIsBalloon_rep { get; set; }
        public String sGfeShoppingCartLoan1CanRateIncreaseTri_rep { get; set; }
        public String sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri_rep { get; set; }
        public String sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri_rep { get; set; }
        public String sGfeShoppingCartLoan1HavePpmtPenaltyTri_rep { get; set; }
        public String sGfeShoppingCartLoan1IsBalloonTri_rep { get; set; }
        public String sGfeShoppingCartLoan2CanRateIncreaseTri_rep { get; set; }
        public String sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri_rep { get; set; }
        public String sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri_rep { get; set; }
        public String sGfeShoppingCartLoan2HavePpmtPenaltyTri_rep { get; set; }
        public String sGfeShoppingCartLoan2IsBalloonTri_rep { get; set; }
        public String sGfeShoppingCartLoan3CanRateIncreaseTri_rep { get; set; }
        public String sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri_rep { get; set; }
        public String sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri_rep { get; set; }
        public String sGfeShoppingCartLoan3HavePpmtPenaltyTri_rep { get; set; }
        public String sGfeShoppingCartLoan3IsBalloonTri_rep { get; set; }
        public String sGfeService1WeSelectName { get; set; }
        public String sGfeService2WeSelectName { get; set; }
        public String sGfeService3WeSelectName { get; set; }
        public String sGfeService4WeSelectName { get; set; }
        public String sGfeService5WeSelectName { get; set; }
        public String sGfeService6WeSelectName { get; set; }
        public String sGfeService7WeSelectName { get; set; }
        public String sGfeService8WeSelectName { get; set; }
        public String sGfeService9WeSelectName { get; set; }
        public String sGfeService10WeSelectName { get; set; }
        public String sGfeService11WeSelectName { get; set; }
        public String sGfeService12WeSelectName { get; set; }
        public String sGfeService13WeSelectName { get; set; }
        public String sGfeService14WeSelectName { get; set; }
        public String sGfeService15WeSelectName { get; set; }
        public String sGfeService16WeSelectName { get; set; }
        public String sGfeService17WeSelectName { get; set; }
        public String sGfeService18WeSelectName { get; set; }
        public String sGfeService1WeSelectFee { get; set; }
        public String sGfeService2WeSelectFee { get; set; }
        public String sGfeService3WeSelectFee { get; set; }
        public String sGfeService4WeSelectFee { get; set; }
        public String sGfeService5WeSelectFee { get; set; }
        public String sGfeService6WeSelectFee { get; set; }
        public String sGfeService7WeSelectFee { get; set; }
        public String sGfeService8WeSelectFee { get; set; }
        public String sGfeService9WeSelectFee { get; set; }
        public String sGfeService10WeSelectFee { get; set; }
        public String sGfeService11WeSelectFee { get; set; }
        public String sGfeService12WeSelectFee { get; set; }
        public String sGfeService13WeSelectFee { get; set; }
        public String sGfeService14WeSelectFee { get; set; }
        public String sGfeService15WeSelectFee { get; set; }
        public String sGfeService16WeSelectFee { get; set; }
        public String sGfeService17WeSelectFee { get; set; }
        public String sGfeService18WeSelectFee { get; set; }
        public String sGfeService1YouCanShopName { get; set; }
        public String sGfeService2YouCanShopName { get; set; }
        public String sGfeService3YouCanShopName { get; set; }
        public String sGfeService4YouCanShopName { get; set; }
        public String sGfeService5YouCanShopName { get; set; }
        public String sGfeService6YouCanShopName { get; set; }
        public String sGfeService7YouCanShopName { get; set; }
        public String sGfeService8YouCanShopName { get; set; }
        public String sGfeService9YouCanShopName { get; set; }
        public String sGfeService10YouCanShopName { get; set; }
        public String sGfeService11YouCanShopName { get; set; }
        public String sGfeService12YouCanShopName { get; set; }
        public String sGfeService13YouCanShopName { get; set; }
        public String sGfeService14YouCanShopName { get; set; }
        public String sGfeService15YouCanShopName { get; set; }
        public String sGfeService16YouCanShopName { get; set; }
        public String sGfeService1YouCanShopFee { get; set; }
        public String sGfeService2YouCanShopFee { get; set; }
        public String sGfeService3YouCanShopFee { get; set; }
        public String sGfeService4YouCanShopFee { get; set; }
        public String sGfeService5YouCanShopFee { get; set; }
        public String sGfeService6YouCanShopFee { get; set; }
        public String sGfeService7YouCanShopFee { get; set; }
        public String sGfeService8YouCanShopFee { get; set; }
        public String sGfeService9YouCanShopFee { get; set; }
        public String sGfeService10YouCanShopFee { get; set; }
        public String sGfeService11YouCanShopFee { get; set; }
        public String sGfeService12YouCanShopFee { get; set; }
        public String sGfeService13YouCanShopFee { get; set; }
        public String sGfeService14YouCanShopFee { get; set; }
        public String sGfeService15YouCanShopFee { get; set; }
        public String sGfeService16YouCanShopFee { get; set; }
        public String sHazInsPiaPaidToPrint { get; set; }
        public String sGfeHomeOwnerInsurance1Name { get; set; }
        public String sGfeHomeOwnerInsurance1Fee { get; set; }
        public String sGfeHomeOwnerInsurance2Name { get; set; }
        public String sGfeHomeOwnerInsurance2Fee { get; set; }
        public String sGfeHomeOwnerInsurance3Name { get; set; }
        public String sGfeHomeOwnerInsurance3Fee { get; set; }
        public Boolean sGfeHasImpoundDepositTax { get; set; }
        public Boolean sGfeHasImpoundDepositInsurance { get; set; }
        public Boolean sGfeHasImpoundDepositOther { get; set; }
        public String sGfeHasImpoundDepositDescription { get; set; }
        public Boolean sGfeRateLockPeriodLckd { get { return m_sGfeRateLockPeriodLckd; } set { m_sGfeRateLockPeriodLckd = value; } }
        public Boolean sHazInsRsrvMonLckd { get { return m_sHazInsRsrvMonLckd; } set { m_sHazInsRsrvMonLckd = value; } }
        public Boolean sMInsRsrvMonLckd { get { return m_sMInsRsrvMonLckd; } set { m_sMInsRsrvMonLckd = value; } }
        public Boolean sRealETxRsrvMonLckd { get { return m_sRealETxRsrvMonLckd; } set { m_sRealETxRsrvMonLckd = value; } }
        public Boolean sSchoolTxRsrvMonLckd { get { return m_sSchoolTxRsrvMonLckd; } set { m_sSchoolTxRsrvMonLckd = value; } }
        public Boolean sFloodInsRsrvMonLckd { get { return m_sFloodInsRsrvMonLckd; } set { m_sFloodInsRsrvMonLckd = value; } }
        public Boolean s1006RsrvMonLckd { get { return m_s1006RsrvMonLckd; } set { m_s1006RsrvMonLckd = value; } }
        public Boolean s1007RsrvMonLckd { get { return m_s1007RsrvMonLckd; } set { m_s1007RsrvMonLckd = value; } }
        public Boolean sU3RsrvMonLckd { get { return m_sU3RsrvMonLckd; } set { m_sU3RsrvMonLckd = value; } }
        public Boolean sU4RsrvMonLckd { get { return m_sU4RsrvMonLckd; } set { m_sU4RsrvMonLckd = value; } }
        public E_TriState sHazInsRsrvEscrowedTri { get { return m_sHazInsRsrvEscrowedTri; } set { m_sHazInsRsrvEscrowedTri = value; } }
        public E_TriState sMInsRsrvEscrowedTri { get { return m_sMInsRsrvEscrowedTri; } set { m_sMInsRsrvEscrowedTri = value; } }
        public E_TriState sRealETxRsrvEscrowedTri { get { return m_sRealETxRsrvEscrowedTri; } set { m_sRealETxRsrvEscrowedTri = value; } }
        public E_TriState sSchoolTxRsrvEscrowedTri { get { return m_sSchoolTxRsrvEscrowedTri; } set { m_sSchoolTxRsrvEscrowedTri = value; } }
        public E_TriState sFloodInsRsrvEscrowedTri { get { return m_sFloodInsRsrvEscrowedTri; } set { m_sFloodInsRsrvEscrowedTri = value; } }
        public E_TriState s1006RsrvEscrowedTri { get { return m_s1006RsrvEscrowedTri; } set { m_s1006RsrvEscrowedTri = value; } }
        public E_TriState s1007RsrvEscrowedTri { get { return m_s1007RsrvEscrowedTri; } set { m_s1007RsrvEscrowedTri = value; } }
        public E_TriState sU3RsrvEscrowedTri { get { return m_sU3RsrvEscrowedTri; } set { m_sU3RsrvEscrowedTri = value; } }
        public E_TriState sU4RsrvEscrowedTri { get { return m_sU4RsrvEscrowedTri; } set { m_sU4RsrvEscrowedTri = value; } }
        public E_sLT sLT { get; set; }
        public bool sFfUfMipIsBeingFinanced { get; set; }
        public E_sLenderCreditCalculationMethodT sLenderCreditCalculationMethodT { get; set; }
        public E_MipFrequency sMipFrequency { get; set; }
        #endregion

        public decimal? sLAmtCalc { get { return m_sLAmtCalc; } set { m_sLAmtCalc = value; } }
        public decimal? sPurchPrice { get { return m_sPurchPrice; } set { m_sPurchPrice = value; } }
        public decimal? sApprVal { get { return m_sApprVal; } set { m_sApprVal = value; } }
        public decimal? sSpOrigC { get { return m_sSpOrigC; } set { m_sSpOrigC = value; } }
        public decimal? sLotOrigC { get { return m_sLotOrigC; } set { m_sLotOrigC = value; } }
        public decimal? sFinalLAmt { get { return m_sFinalLAmt; } set { m_sFinalLAmt = value; } }

        public decimal? sLenderTargetInitialCreditAmt { get; set; }
        #region manually entered fields.
        public string gfeTil_CompanyName { get; set; }
        public string gfeTil_StreetAddr { get; set; }
        public string gfeTil_City { get; set; }
        public string gfeTil_State { get; set; }
        public string gfeTil_Zip { get; set; }
        public string gfeTil_PhoneOfCompany { get; set; }
        public string gfeTil_EmailAddr { get; set; }
        public string gfeTil_PrepareDate { get; set; }
        public bool gfeTil_IsLocked { get; set; }
        public E_AgentRoleT gfeTil_AgentRoleT { get; set; }

        public string dataApp_aBNm_aCNm { get; set; }
        #endregion

    }

    /// <summary>
    /// For loading formatted data out of the GFE Archive without needing to apply it to the loan itself.
    /// </summary>
    public class GFEFormattedArchive : IGFEArchive
    {
        public IGFEArchive GfeArchive { get; private set; }
        public LosConvert ConvertFromLos { get; private set; }
        public LosConvert ConvertToLos { get; private set; }

        public GFEFormattedArchive(IGFEArchive gfeArchive, LosConvert convertLos)
        {
            this.GfeArchive = gfeArchive;
            this.ConvertFromLos = new LosConvert();
            this.ConvertToLos = convertLos;
        }

        private string ToMoneyString(string str)
        {
            decimal money = this.ConvertFromLos.ToMoney(str);
            return this.ConvertToLos.ToMoneyString(money, FormatDirection.ToRep);
        }

        private string ToCountString(string str)
        {
            int count = this.ConvertFromLos.ToCount(str);
            return this.ConvertToLos.ToCountString(count);
        }

        private string ToRateString(string str)
        {
            decimal rate = this.ConvertFromLos.ToRate(str);
            return this.ConvertToLos.ToRateString(rate);
        }

        private string ToDateString(string str)
        {
            CDateTime date = CDateTime.Create(str, this.ConvertFromLos);
            return date.ToString(this.ConvertToLos);
        }

        private string ToTriString(string str)
        {
            E_TriState tri = this.ConvertFromLos.ToTri(str);
            return this.ConvertToLos.ToTriString(tri);
        }

        private string ToBitString(string str)
        {
            bool bit = this.ConvertFromLos.ToBit(str);
            return this.ConvertToLos.ToBitString(bit);
        }

        public string DateArchived { get { return this.GfeArchive.DateArchived; } }
        public bool IsClosingCostMigrationArchive { get { return this.GfeArchive.IsClosingCostMigrationArchive; } }

        public Dictionary<string, object> Fields { get { return this.GfeArchive.Fields; } }

        #region fields that need to be gotten
        public Boolean sConsummationDLckd { get { return this.GfeArchive.sConsummationDLckd; } }
        public String sConsummationD_rep { get { return this.ToDateString(this.GfeArchive.sConsummationD_rep); } }
        public Boolean sEstCloseDLckd { get { return this.GfeArchive.sEstCloseDLckd; } }
        public String sEstCloseD_rep { get { return this.ToDateString(this.GfeArchive.sEstCloseD_rep); } }
        public String sOpenedD_rep { get { return this.ToDateString(this.GfeArchive.sOpenedD_rep); } }
        public String sDue_rep { get { return this.ToCountString(this.GfeArchive.sDue_rep); } }
        public Boolean sIsRateLocked { get { return this.GfeArchive.sIsRateLocked; } }
        public String sTerm_rep { get { return this.ToCountString(this.GfeArchive.sTerm_rep); } }
        public Boolean sSchedDueD1Lckd { get { return this.GfeArchive.sSchedDueD1Lckd; } }
        public String sSchedDueD1_rep { get { return this.ToDateString(this.GfeArchive.sSchedDueD1_rep); } }
        public String sNoteIR_rep { get { return this.ToRateString(this.GfeArchive.sNoteIR_rep); } }
        public String sFinalLAmt_rep { get { return this.ToMoneyString(this.GfeArchive.sFinalLAmt_rep); } }
        public E_sFinMethT sFinMethT { get { return this.GfeArchive.sFinMethT; } }
        public String sDaysInYr_rep { get { return this.ToCountString(this.GfeArchive.sDaysInYr_rep); } }
        public String sRAdj1stCapR_rep { get { return this.ToRateString(this.GfeArchive.sRAdj1stCapR_rep); } }
        public String sRAdjCapR_rep { get { return this.ToRateString(this.GfeArchive.sRAdjCapR_rep); } }
        public String sRAdjLifeCapR_rep { get { return this.ToRateString(this.GfeArchive.sRAdjLifeCapR_rep); } }
        public String sRAdjCapMon_rep { get { return this.ToCountString(this.GfeArchive.sRAdjCapMon_rep); } }
        public String sRAdj1stCapMon_rep { get { return this.ToCountString(this.GfeArchive.sRAdj1stCapMon_rep); } }
        public String sPmtAdjCapR_rep { get { return this.ToRateString(this.GfeArchive.sPmtAdjCapR_rep); } }
        public String sPmtAdjMaxBalPc_rep { get { return this.ToRateString(this.GfeArchive.sPmtAdjMaxBalPc_rep); } }
        public String sPmtAdjRecastStop_rep { get { return this.ToCountString(this.GfeArchive.sPmtAdjRecastStop_rep); } }
        public String sPmtAdjRecastPeriodMon_rep { get { return this.ToCountString(this.GfeArchive.sPmtAdjRecastPeriodMon_rep); } }
        public String sPmtAdjCapMon_rep { get { return this.ToCountString(this.GfeArchive.sPmtAdjCapMon_rep); } }
        public String sIOnlyMon_rep { get { return this.ToCountString(this.GfeArchive.sIOnlyMon_rep); } }
        public String sGfeNoteIRAvailTillD_Date { get { return this.GfeArchive.sGfeNoteIRAvailTillD_Date; } }
        public bool sGfeNoteIRAvailTillDLckd { get { return this.GfeArchive.sGfeNoteIRAvailTillDLckd; } }
        public String sGfeNoteIRAvailTillD_Time { get { return this.GfeArchive.sGfeNoteIRAvailTillD_Time; } }
        public String sGfeEstScAvailTillD_Date { get { return this.GfeArchive.sGfeEstScAvailTillD_Date; } }
        public bool sGfeEstScAvailTillDLckd { get { return this.GfeArchive.sGfeEstScAvailTillDLckd; } }
        public String sGfeEstScAvailTillD_Time { get { return this.GfeArchive.sGfeEstScAvailTillD_Time; } }
        public String sGfeRateLockPeriod_rep { get { return this.ToCountString(this.GfeArchive.sGfeRateLockPeriod_rep); } }
        public String sGfeLockPeriodBeforeSettlement_rep { get { return this.ToCountString(this.GfeArchive.sGfeLockPeriodBeforeSettlement_rep); } }
        public E_sTimeZoneT sGfeNoteIRAvailTillDTimeZoneT { get { return this.GfeArchive.sGfeNoteIRAvailTillDTimeZoneT; } }
        public E_sTimeZoneT sGfeEstScAvailTillDTimeZoneT { get { return this.GfeArchive.sGfeEstScAvailTillDTimeZoneT; } }
        public Boolean sIsPrintTimeForGfeNoteIRAvailTillD { get { return this.GfeArchive.sIsPrintTimeForGfeNoteIRAvailTillD; } }
        public Boolean sIsPrintTimeForsGfeEstScAvailTillD { get { return this.GfeArchive.sIsPrintTimeForsGfeEstScAvailTillD; } }
        public Boolean sGfeCanRateIncrease { get { return this.GfeArchive.sGfeCanRateIncrease; } }
        public String sRLifeCapR_rep { get { return this.ToRateString(this.GfeArchive.sRLifeCapR_rep); } }
        public String sGfeFirstInterestChangeIn { get { return this.GfeArchive.sGfeFirstInterestChangeIn; } }
        public Boolean sGfeCanLoanBalanceIncrease { get { return this.GfeArchive.sGfeCanLoanBalanceIncrease; } }
        public String sGfeMaxLoanBalance_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeMaxLoanBalance_rep); } }
        public Boolean sGfeCanPaymentIncrease { get { return this.GfeArchive.sGfeCanPaymentIncrease; } }
        public String sGfeFirstPaymentChangeIn { get { return this.GfeArchive.sGfeFirstPaymentChangeIn; } }
        public String sGfeFirstAdjProThisMPmtAndMIns_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeFirstAdjProThisMPmtAndMIns_rep); } }
        public String sGfeMaxProThisMPmtAndMIns_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeMaxProThisMPmtAndMIns_rep); } }
        public Boolean sGfeHavePpmtPenalty { get { return this.GfeArchive.sGfeHavePpmtPenalty; } }
        public String sGfeMaxPpmtPenaltyAmt_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeMaxPpmtPenaltyAmt_rep); } }
        public Boolean sGfeIsBalloon { get { return this.GfeArchive.sGfeIsBalloon; } }
        public String sGfeBalloonPmt_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeBalloonPmt_rep); } }
        public String sGfeBalloonDueInYrs { get { return this.GfeArchive.sGfeBalloonDueInYrs; } }
        public Boolean sMldsHasImpound { get { return this.GfeArchive.sMldsHasImpound; } }
        public Boolean sIsRequireFeesFromDropDown { get { return this.GfeArchive.sIsRequireFeesFromDropDown; } }
        public Boolean sGfeUsePaidToFromOfficialContact { get { return this.GfeArchive.sGfeUsePaidToFromOfficialContact; } }
        public Boolean sGfeRequirePaidToFromContacts { get { return this.GfeArchive.sGfeRequirePaidToFromContacts; } }
        public String s800U5F_rep { get { return this.ToMoneyString(this.GfeArchive.s800U5F_rep); } }
        public String s800U5FDesc { get { return this.GfeArchive.s800U5FDesc; } }
        public String s800U4F_rep { get { return this.ToMoneyString(this.GfeArchive.s800U4F_rep); } }
        public String s800U4FDesc { get { return this.GfeArchive.s800U4FDesc; } }
        public String s800U3F_rep { get { return this.ToMoneyString(this.GfeArchive.s800U3F_rep); } }
        public String s800U3FDesc { get { return this.GfeArchive.s800U3FDesc; } }
        public String s800U2F_rep { get { return this.ToMoneyString(this.GfeArchive.s800U2F_rep); } }
        public String s800U2FDesc { get { return this.GfeArchive.s800U2FDesc; } }
        public String s800U1F_rep { get { return this.ToMoneyString(this.GfeArchive.s800U1F_rep); } }
        public String s800U1FDesc { get { return this.GfeArchive.s800U1FDesc; } }
        public String sWireF_rep { get { return this.ToMoneyString(this.GfeArchive.sWireF_rep); } }
        public String sUwF_rep { get { return this.ToMoneyString(this.GfeArchive.sUwF_rep); } }
        public String sProcF_rep { get { return this.ToMoneyString(this.GfeArchive.sProcF_rep); } }
        public Boolean sProcFPaid { get { return this.GfeArchive.sProcFPaid; } }
        public String sTxServF_rep { get { return this.ToMoneyString(this.GfeArchive.sTxServF_rep); } }
        public String sFloodCertificationF_rep { get { return this.ToMoneyString(this.GfeArchive.sFloodCertificationF_rep); } }
        public E_FloodCertificationDeterminationT sFloodCertificationDeterminationT { get { return this.GfeArchive.sFloodCertificationDeterminationT; } }
        public String sMBrokF_rep { get { return this.ToMoneyString(this.GfeArchive.sMBrokF_rep); } }
        public String sMBrokFMb_rep { get { return this.ToMoneyString(this.GfeArchive.sMBrokFMb_rep); } }
        public String sMBrokFPc_rep { get { return this.ToRateString(this.GfeArchive.sMBrokFPc_rep); } }
        public E_PercentBaseT sMBrokFBaseT { get { return this.GfeArchive.sMBrokFBaseT; } }
        public String sInspectF_rep { get { return this.ToMoneyString(this.GfeArchive.sInspectF_rep); } }
        public String sCrF_rep { get { return this.ToMoneyString(this.GfeArchive.sCrF_rep); } }
        public Boolean sCrFPaid { get { return this.GfeArchive.sCrFPaid; } }
        public String sApprF_rep { get { return this.ToMoneyString(this.GfeArchive.sApprF_rep); } }
        public Boolean sApprFPaid { get { return this.GfeArchive.sApprFPaid; } }
        public String sLOrigF_rep { get { return this.ToMoneyString(this.GfeArchive.sLOrigF_rep); } }
        public String sLOrigFMb_rep { get { return this.ToMoneyString(this.GfeArchive.sLOrigFMb_rep); } }
        public String sLOrigFPc_rep { get { return this.ToRateString(this.GfeArchive.sLOrigFPc_rep); } }
        public String s900U1Pia_rep { get { return this.ToMoneyString(this.GfeArchive.s900U1Pia_rep); } }
        public String s900U1PiaDesc { get { return this.GfeArchive.s900U1PiaDesc; } }
        public String sVaFf_rep { get { return this.ToMoneyString(this.GfeArchive.sVaFf_rep); } }
        public String s904Pia_rep { get { return this.ToMoneyString(this.GfeArchive.s904Pia_rep); } }
        public String s904PiaDesc { get { return this.GfeArchive.s904PiaDesc; } }
        public String sHazInsPia_rep { get { return this.ToMoneyString(this.GfeArchive.sHazInsPia_rep); } }
        public String sHazInsPiaMon_rep { get { return this.ToCountString(this.GfeArchive.sHazInsPiaMon_rep); } }
        public E_PercentBaseT sProHazInsT { get { return this.GfeArchive.sProHazInsT; } }
        public String sProHazInsR_rep { get { return this.ToRateString(this.GfeArchive.sProHazInsR_rep); } }
        public String sProHazInsMb_rep { get { return this.ToMoneyString(this.GfeArchive.sProHazInsMb_rep); } }
        public String sMipPia_rep { get { return this.ToMoneyString(this.GfeArchive.sMipPia_rep); } }
        public String sIPia_rep { get { return this.ToMoneyString(this.GfeArchive.sIPia_rep); } }
        public String sIPiaDy_rep { get { return this.ToCountString(this.GfeArchive.sIPiaDy_rep); } }
        public Boolean sIPiaDyLckd { get { return this.GfeArchive.sIPiaDyLckd; } }
        public Boolean sIPerDayLckd { get { return this.GfeArchive.sIPerDayLckd; } }
        public String sIPerDay_rep { get { return this.ToMoneyString(this.GfeArchive.sIPerDay_rep); } }
        public String sAggregateAdjRsrv_rep { get { return this.ToMoneyString(this.GfeArchive.sAggregateAdjRsrv_rep); } }
        public Boolean sAggregateAdjRsrvLckd { get { return this.GfeArchive.sAggregateAdjRsrvLckd; } }
        public String s1007Rsrv_rep { get { return this.ToMoneyString(this.GfeArchive.s1007Rsrv_rep); } }
        public String s1007ProHExp_rep { get { return this.ToMoneyString(this.GfeArchive.s1007ProHExp_rep); } }
        public String s1007RsrvMon_rep { get { return this.ToCountString(this.GfeArchive.s1007RsrvMon_rep); } }
        public String s1007ProHExpDesc { get { return this.GfeArchive.s1007ProHExpDesc; } }
        public String s1006Rsrv_rep { get { return this.ToMoneyString(this.GfeArchive.s1006Rsrv_rep); } }
        public String s1006ProHExp_rep { get { return this.ToMoneyString(this.GfeArchive.s1006ProHExp_rep); } }
        public String s1006RsrvMon_rep { get { return this.ToCountString(this.GfeArchive.s1006RsrvMon_rep); } }
        public String s1006ProHExpDesc { get { return this.GfeArchive.s1006ProHExpDesc; } }
        public String sU3Rsrv_rep { get { return this.ToMoneyString(this.GfeArchive.sU3Rsrv_rep); } }
        public String sProU3Rsrv_rep { get { return this.ToMoneyString(this.GfeArchive.sProU3Rsrv_rep); } }
        public String sU3RsrvMon_rep { get { return this.ToCountString(this.GfeArchive.sU3RsrvMon_rep); } }
        public String sU3RsrvDesc { get { return this.GfeArchive.sU3RsrvDesc; } }
        public String sU4Rsrv_rep { get { return this.ToMoneyString(this.GfeArchive.sU4Rsrv_rep); } }
        public String sProU4Rsrv_rep { get { return this.ToMoneyString(this.GfeArchive.sProU4Rsrv_rep); } }
        public String sU4RsrvMon_rep { get { return this.ToCountString(this.GfeArchive.sU4RsrvMon_rep); } }
        public String sU4RsrvDesc { get { return this.GfeArchive.sU4RsrvDesc; } }
        public String sFloodInsRsrv_rep { get { return this.ToMoneyString(this.GfeArchive.sFloodInsRsrv_rep); } }
        public String sProFloodIns_rep { get { return this.ToMoneyString(this.GfeArchive.sProFloodIns_rep); } }
        public String sFloodInsRsrvMon_rep { get { return this.ToCountString(this.GfeArchive.sFloodInsRsrvMon_rep); } }
        public String sRealETxRsrv_rep { get { return this.ToMoneyString(this.GfeArchive.sRealETxRsrv_rep); } }
        public E_PercentBaseT sProRealETxT { get { return this.GfeArchive.sProRealETxT; } }
        public String sProRealETxMb_rep { get { return this.ToMoneyString(this.GfeArchive.sProRealETxMb_rep); } }
        public String sProRealETxR_rep { get { return this.ToRateString(this.GfeArchive.sProRealETxR_rep); } }
        public String sRealETxRsrvMon_rep { get { return this.ToCountString(this.GfeArchive.sRealETxRsrvMon_rep); } }
        public String sSchoolTxRsrv_rep { get { return this.ToMoneyString(this.GfeArchive.sSchoolTxRsrv_rep); } }
        public String sProSchoolTx_rep { get { return this.ToMoneyString(this.GfeArchive.sProSchoolTx_rep); } }
        public String sSchoolTxRsrvMon_rep { get { return this.ToCountString(this.GfeArchive.sSchoolTxRsrvMon_rep); } }
        public String sMInsRsrv_rep { get { return this.ToMoneyString(this.GfeArchive.sMInsRsrv_rep); } }
        public String sProMIns_rep { get { return this.ToMoneyString(this.GfeArchive.sProMIns_rep); } }
        public String sMInsRsrvMon_rep { get { return this.ToCountString(this.GfeArchive.sMInsRsrvMon_rep); } }
        public String sHazInsRsrv_rep { get { return this.ToMoneyString(this.GfeArchive.sHazInsRsrv_rep); } }
        public String sProHazIns_rep { get { return this.ToMoneyString(this.GfeArchive.sProHazIns_rep); } }
        public String sHazInsRsrvMon_rep { get { return this.ToCountString(this.GfeArchive.sHazInsRsrvMon_rep); } }
        public String sU4Tc_rep { get { return this.ToMoneyString(this.GfeArchive.sU4Tc_rep); } }
        public String sU4TcDesc { get { return this.GfeArchive.sU4TcDesc; } }
        public String sU3Tc_rep { get { return this.ToMoneyString(this.GfeArchive.sU3Tc_rep); } }
        public String sU3TcDesc { get { return this.GfeArchive.sU3TcDesc; } }
        public String sU2Tc_rep { get { return this.ToMoneyString(this.GfeArchive.sU2Tc_rep); } }
        public String sU2TcDesc { get { return this.GfeArchive.sU2TcDesc; } }
        public String sU1Tc_rep { get { return this.ToMoneyString(this.GfeArchive.sU1Tc_rep); } }
        public String sU1TcDesc { get { return this.GfeArchive.sU1TcDesc; } }
        public String sTitleInsF_rep { get { return this.ToMoneyString(this.GfeArchive.sTitleInsF_rep); } }
        public String sTitleInsFTable { get { return this.GfeArchive.sTitleInsFTable; } }
        public String sAttorneyF_rep { get { return this.ToMoneyString(this.GfeArchive.sAttorneyF_rep); } }
        public String sNotaryF_rep { get { return this.ToMoneyString(this.GfeArchive.sNotaryF_rep); } }
        public String sDocPrepF_rep { get { return this.ToMoneyString(this.GfeArchive.sDocPrepF_rep); } }
        public String sEscrowF_rep { get { return this.ToMoneyString(this.GfeArchive.sEscrowF_rep); } }
        public String sOwnerTitleInsF_rep { get { return this.ToMoneyString(this.GfeArchive.sOwnerTitleInsF_rep); } }
        public String sEscrowFTable { get { return this.GfeArchive.sEscrowFTable; } }
        public String sU3GovRtc_rep { get { return this.ToMoneyString(this.GfeArchive.sU3GovRtc_rep); } }
        public String sU3GovRtcMb_rep { get { return this.ToMoneyString(this.GfeArchive.sU3GovRtcMb_rep); } }
        public E_PercentBaseT sU3GovRtcBaseT { get { return this.GfeArchive.sU3GovRtcBaseT; } }
        public String sU3GovRtcPc_rep { get { return this.ToRateString(this.GfeArchive.sU3GovRtcPc_rep); } }
        public String sU3GovRtcDesc { get { return this.GfeArchive.sU3GovRtcDesc; } }
        public String sU2GovRtc_rep { get { return this.ToMoneyString(this.GfeArchive.sU2GovRtc_rep); } }
        public String sU2GovRtcMb_rep { get { return this.ToMoneyString(this.GfeArchive.sU2GovRtcMb_rep); } }
        public E_PercentBaseT sU2GovRtcBaseT { get { return this.GfeArchive.sU2GovRtcBaseT; } }
        public String sU2GovRtcPc_rep { get { return this.ToRateString(this.GfeArchive.sU2GovRtcPc_rep); } }
        public String sU2GovRtcDesc { get { return this.GfeArchive.sU2GovRtcDesc; } }
        public String sU1GovRtc_rep { get { return this.ToMoneyString(this.GfeArchive.sU1GovRtc_rep); } }
        public String sU1GovRtcMb_rep { get { return this.ToMoneyString(this.GfeArchive.sU1GovRtcMb_rep); } }
        public E_PercentBaseT sU1GovRtcBaseT { get { return this.GfeArchive.sU1GovRtcBaseT; } }
        public String sU1GovRtcPc_rep { get { return this.ToRateString(this.GfeArchive.sU1GovRtcPc_rep); } }
        public String sU1GovRtcDesc { get { return this.GfeArchive.sU1GovRtcDesc; } }
        public String sStateRtcDesc { get { return this.GfeArchive.sStateRtcDesc; } }
        public String sStateRtc_rep { get { return this.ToMoneyString(this.GfeArchive.sStateRtc_rep); } }
        public String sStateRtcMb_rep { get { return this.ToMoneyString(this.GfeArchive.sStateRtcMb_rep); } }
        public E_PercentBaseT sStateRtcBaseT { get { return this.GfeArchive.sStateRtcBaseT; } }
        public String sStateRtcPc_rep { get { return this.ToRateString(this.GfeArchive.sStateRtcPc_rep); } }
        public String sCountyRtcDesc { get { return this.GfeArchive.sCountyRtcDesc; } }
        public String sCountyRtc_rep { get { return this.ToMoneyString(this.GfeArchive.sCountyRtc_rep); } }
        public String sCountyRtcMb_rep { get { return this.ToMoneyString(this.GfeArchive.sCountyRtcMb_rep); } }
        public E_PercentBaseT sCountyRtcBaseT { get { return this.GfeArchive.sCountyRtcBaseT; } }
        public String sCountyRtcPc_rep { get { return this.ToRateString(this.GfeArchive.sCountyRtcPc_rep); } }
        public String sRecFDesc { get { return this.GfeArchive.sRecFDesc; } }
        public String sRecF_rep { get { return this.ToMoneyString(this.GfeArchive.sRecF_rep); } }
        public String sRecFMb_rep { get { return this.ToMoneyString(this.GfeArchive.sRecFMb_rep); } }
        public E_PercentBaseT sRecBaseT { get { return this.GfeArchive.sRecBaseT; } }
        public String sRecFPc_rep { get { return this.ToRateString(this.GfeArchive.sRecFPc_rep); } }
        public Boolean sRecFLckd { get { return this.GfeArchive.sRecFLckd; } }
        public String sRecDeed_rep { get { return this.ToMoneyString(this.GfeArchive.sRecDeed_rep); } }
        public String sRecMortgage_rep { get { return this.ToMoneyString(this.GfeArchive.sRecMortgage_rep); } }
        public String sRecRelease_rep { get { return this.ToMoneyString(this.GfeArchive.sRecRelease_rep); } }
        public String sU5Sc_rep { get { return this.ToMoneyString(this.GfeArchive.sU5Sc_rep); } }
        public String sU5ScDesc { get { return this.GfeArchive.sU5ScDesc; } }
        public String sU4Sc_rep { get { return this.ToMoneyString(this.GfeArchive.sU4Sc_rep); } }
        public String sU4ScDesc { get { return this.GfeArchive.sU4ScDesc; } }
        public String sU3Sc_rep { get { return this.ToMoneyString(this.GfeArchive.sU3Sc_rep); } }
        public String sU3ScDesc { get { return this.GfeArchive.sU3ScDesc; } }
        public String sU2Sc_rep { get { return this.ToMoneyString(this.GfeArchive.sU2Sc_rep); } }
        public String sU2ScDesc { get { return this.GfeArchive.sU2ScDesc; } }
        public String sU1Sc_rep { get { return this.ToMoneyString(this.GfeArchive.sU1Sc_rep); } }
        public String sU1ScDesc { get { return this.GfeArchive.sU1ScDesc; } }
        public String sPestInspectF_rep { get { return this.ToMoneyString(this.GfeArchive.sPestInspectF_rep); } }
        public String sGfeTotalEstimateSettlementCharge_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeTotalEstimateSettlementCharge_rep); } }
        public String sGfeOriginationF_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeOriginationF_rep); } }
        public String sLDiscnt_rep { get { return this.ToMoneyString(this.GfeArchive.sLDiscnt_rep); } }
        public String sGfeAdjOriginationCharge_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeAdjOriginationCharge_rep); } }
        public String sGfeRequiredServicesTotalFee_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeRequiredServicesTotalFee_rep); } }
        public String sGfeLenderTitleTotalFee_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeLenderTitleTotalFee_rep); } }
        public String sGfeOwnerTitleTotalFee_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeOwnerTitleTotalFee_rep); } }
        public String sGfeServicesYouShopTotalFee_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeServicesYouShopTotalFee_rep); } }
        public String sGfeGovtRecTotalFee_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeGovtRecTotalFee_rep); } }
        public String sGfeTransferTaxTotalFee_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeTransferTaxTotalFee_rep); } }
        public String sGfeInitialImpoundDeposit_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeInitialImpoundDeposit_rep); } }
        public String sGfeDailyInterestTotalFee_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeDailyInterestTotalFee_rep); } }
        public String sGfeHomeOwnerInsuranceTotalFee_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeHomeOwnerInsuranceTotalFee_rep); } }
        public String sGfeTotalOtherSettlementServiceFee_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeTotalOtherSettlementServiceFee_rep); } }
        public String sGfeNotApplicableF_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeNotApplicableF_rep); } }
        public String sGfeTradeOffLowerCCLoanAmt_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeTradeOffLowerCCLoanAmt_rep); } }
        public String sGfeTradeOffLowerCCNoteIR_rep { get { return this.ToRateString(this.GfeArchive.sGfeTradeOffLowerCCNoteIR_rep); } }
        public String sGfeTradeOffLowerCCMPmtAndMInsDiff_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeTradeOffLowerCCMPmtAndMInsDiff_rep); } }
        public String sGfeTradeOffLowerCCClosingCostDiff_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeTradeOffLowerCCClosingCostDiff_rep); } }
        public String sGfeTradeOffLowerCCClosingCost_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeTradeOffLowerCCClosingCost_rep); } }
        public String sGfeTradeOffLowerRateLoanAmt_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeTradeOffLowerRateLoanAmt_rep); } }
        public String sGfeTradeOffLowerRateNoteIR_rep { get { return this.ToRateString(this.GfeArchive.sGfeTradeOffLowerRateNoteIR_rep); } }
        public String sGfeTradeOffLowerRateMPmtAndMInsDiff_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeTradeOffLowerRateMPmtAndMInsDiff_rep); } }
        public String sGfeTradeOffLowerRateClosingCostDiff_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeTradeOffLowerRateClosingCostDiff_rep); } }
        public String sGfeTradeOffLowerRateClosingCost_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeTradeOffLowerRateClosingCost_rep); } }
        public String sGfeProThisMPmtAndMIns_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeProThisMPmtAndMIns_rep); } }
        public String sGfeTradeOffLowerCCMPmtAndMIns_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeTradeOffLowerCCMPmtAndMIns_rep); } }
        public String sGfeTradeOffLowerRateMPmtAndMIns_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeTradeOffLowerRateMPmtAndMIns_rep); } }
        public String sGfeShoppingCartLoan1OriginatorName { get { return this.GfeArchive.sGfeShoppingCartLoan1OriginatorName; } }
        public String sGfeShoppingCartLoan1LoanAmt_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeShoppingCartLoan1LoanAmt_rep); } }
        public String sGfeShoppingCartLoan1LoanTerm_rep { get { return this.ToCountString(this.GfeArchive.sGfeShoppingCartLoan1LoanTerm_rep); } }
        public String sGfeShoppingCartLoan1NoteIR_rep { get { return this.ToRateString(this.GfeArchive.sGfeShoppingCartLoan1NoteIR_rep); } }
        public String sGfeShoppingCartLoan1InitialPmt_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeShoppingCartLoan1InitialPmt_rep); } }
        public String sGfeShoppingCartLoan1RateLockPeriod_rep { get { return this.ToCountString(this.GfeArchive.sGfeShoppingCartLoan1RateLockPeriod_rep); } }
        public E_TriState sGfeShoppingCartLoan1CanRateIncreaseTri { get { return this.GfeArchive.sGfeShoppingCartLoan1CanRateIncreaseTri; } }
        public E_TriState sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri { get { return this.GfeArchive.sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri; } }
        public E_TriState sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri { get { return this.GfeArchive.sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri; } }
        public E_TriState sGfeShoppingCartLoan1HavePpmtPenaltyTri { get { return this.GfeArchive.sGfeShoppingCartLoan1HavePpmtPenaltyTri; } }
        public E_TriState sGfeShoppingCartLoan1IsBalloonTri { get { return this.GfeArchive.sGfeShoppingCartLoan1IsBalloonTri; } }
        public String sGfeShoppingCartLoan1TotalClosingCost_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeShoppingCartLoan1TotalClosingCost_rep); } }
        public String sGfeShoppingCartLoan2OriginatorName { get { return this.GfeArchive.sGfeShoppingCartLoan2OriginatorName; } }
        public String sGfeShoppingCartLoan2LoanAmt_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeShoppingCartLoan2LoanAmt_rep); } }
        public String sGfeShoppingCartLoan2LoanTerm_rep { get { return this.ToCountString(this.GfeArchive.sGfeShoppingCartLoan2LoanTerm_rep); } }
        public String sGfeShoppingCartLoan2NoteIR_rep { get { return this.ToRateString(this.GfeArchive.sGfeShoppingCartLoan2NoteIR_rep); } }
        public String sGfeShoppingCartLoan2InitialPmt_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeShoppingCartLoan2InitialPmt_rep); } }
        public String sGfeShoppingCartLoan2RateLockPeriod_rep { get { return this.ToCountString(this.GfeArchive.sGfeShoppingCartLoan2RateLockPeriod_rep); } }
        public E_TriState sGfeShoppingCartLoan2CanRateIncreaseTri { get { return this.GfeArchive.sGfeShoppingCartLoan2CanRateIncreaseTri; } }
        public E_TriState sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri { get { return this.GfeArchive.sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri; } }
        public E_TriState sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri { get { return this.GfeArchive.sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri; } }
        public E_TriState sGfeShoppingCartLoan2HavePpmtPenaltyTri { get { return this.GfeArchive.sGfeShoppingCartLoan2HavePpmtPenaltyTri; } }
        public E_TriState sGfeShoppingCartLoan2IsBalloonTri { get { return this.GfeArchive.sGfeShoppingCartLoan2IsBalloonTri; } }
        public String sGfeShoppingCartLoan2TotalClosingCost_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeShoppingCartLoan2TotalClosingCost_rep); } }
        public String sGfeShoppingCartLoan3OriginatorName { get { return this.GfeArchive.sGfeShoppingCartLoan3OriginatorName; } }
        public String sGfeShoppingCartLoan3LoanAmt_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeShoppingCartLoan3LoanAmt_rep); } }
        public String sGfeShoppingCartLoan3LoanTerm_rep { get { return this.ToCountString(this.GfeArchive.sGfeShoppingCartLoan3LoanTerm_rep); } }
        public String sGfeShoppingCartLoan3NoteIR_rep { get { return this.ToRateString(this.GfeArchive.sGfeShoppingCartLoan3NoteIR_rep); } }
        public String sGfeShoppingCartLoan3InitialPmt_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeShoppingCartLoan3InitialPmt_rep); } }
        public String sGfeShoppingCartLoan3RateLockPeriod_rep { get { return this.ToCountString(this.GfeArchive.sGfeShoppingCartLoan3RateLockPeriod_rep); } }
        public E_TriState sGfeShoppingCartLoan3CanRateIncreaseTri { get { return this.GfeArchive.sGfeShoppingCartLoan3CanRateIncreaseTri; } }
        public E_TriState sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri { get { return this.GfeArchive.sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri; } }
        public E_TriState sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri { get { return this.GfeArchive.sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri; } }
        public E_TriState sGfeShoppingCartLoan3HavePpmtPenaltyTri { get { return this.GfeArchive.sGfeShoppingCartLoan3HavePpmtPenaltyTri; } }
        public E_TriState sGfeShoppingCartLoan3IsBalloonTri { get { return this.GfeArchive.sGfeShoppingCartLoan3IsBalloonTri; } }
        public String sGfeShoppingCartLoan3TotalClosingCost_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeShoppingCartLoan3TotalClosingCost_rep); } }
        public String sLpTemplateNm { get { return this.GfeArchive.sLpTemplateNm; } }
        public String sCcTemplateNm { get { return this.GfeArchive.sCcTemplateNm; } }
        public Int32 sLOrigFProps { get { return this.GfeArchive.sLOrigFProps; } }
        public Int32 sGfeOriginatorCompFProps { get { return this.GfeArchive.sGfeOriginatorCompFProps; } }
        public Int32 sApprFProps { get { return this.GfeArchive.sApprFProps; } }
        public Int32 sCrFProps { get { return this.GfeArchive.sCrFProps; } }
        public Int32 sInspectFProps { get { return this.GfeArchive.sInspectFProps; } }
        public Int32 sMBrokFProps { get { return this.GfeArchive.sMBrokFProps; } }
        public Int32 sTxServFProps { get { return this.GfeArchive.sTxServFProps; } }
        public Int32 sFloodCertificationFProps { get { return this.GfeArchive.sFloodCertificationFProps; } }
        public Int32 sProcFProps { get { return this.GfeArchive.sProcFProps; } }
        public Int32 sUwFProps { get { return this.GfeArchive.sUwFProps; } }
        public Int32 sWireFProps { get { return this.GfeArchive.sWireFProps; } }
        public Int32 s800U1FProps { get { return this.GfeArchive.s800U1FProps; } }
        public Int32 s800U2FProps { get { return this.GfeArchive.s800U2FProps; } }
        public Int32 s800U3FProps { get { return this.GfeArchive.s800U3FProps; } }
        public Int32 s800U4FProps { get { return this.GfeArchive.s800U4FProps; } }
        public Int32 s800U5FProps { get { return this.GfeArchive.s800U5FProps; } }
        public Int32 sIPiaProps { get { return this.GfeArchive.sIPiaProps; } }
        public Int32 sMipPiaProps { get { return this.GfeArchive.sMipPiaProps; } }
        public Int32 sHazInsPiaProps { get { return this.GfeArchive.sHazInsPiaProps; } }
        public Int32 s904PiaProps { get { return this.GfeArchive.s904PiaProps; } }
        public Int32 sVaFfProps { get { return this.GfeArchive.sVaFfProps; } }
        public Int32 s900U1PiaProps { get { return this.GfeArchive.s900U1PiaProps; } }
        public Int32 sHazInsRsrvProps { get { return this.GfeArchive.sHazInsRsrvProps; } }
        public Int32 sMInsRsrvProps { get { return this.GfeArchive.sMInsRsrvProps; } }
        public Int32 sSchoolTxRsrvProps { get { return this.GfeArchive.sSchoolTxRsrvProps; } }
        public Int32 sRealETxRsrvProps { get { return this.GfeArchive.sRealETxRsrvProps; } }
        public Int32 sFloodInsRsrvProps { get { return this.GfeArchive.sFloodInsRsrvProps; } }
        public Int32 s1006RsrvProps { get { return this.GfeArchive.s1006RsrvProps; } }
        public Int32 s1007RsrvProps { get { return this.GfeArchive.s1007RsrvProps; } }
        public Int32 sU3RsrvProps { get { return this.GfeArchive.sU3RsrvProps; } }
        public Int32 sU4RsrvProps { get { return this.GfeArchive.sU4RsrvProps; } }
        public Int32 sAggregateAdjRsrvProps { get { return this.GfeArchive.sAggregateAdjRsrvProps; } }
        public Int32 sEscrowFProps { get { return this.GfeArchive.sEscrowFProps; } }
        public Int32 sOwnerTitleInsProps { get { return this.GfeArchive.sOwnerTitleInsProps; } }
        public Int32 sDocPrepFProps { get { return this.GfeArchive.sDocPrepFProps; } }
        public Int32 sNotaryFProps { get { return this.GfeArchive.sNotaryFProps; } }
        public Int32 sAttorneyFProps { get { return this.GfeArchive.sAttorneyFProps; } }
        public Int32 sTitleInsFProps { get { return this.GfeArchive.sTitleInsFProps; } }
        public Int32 sU1TcProps { get { return this.GfeArchive.sU1TcProps; } }
        public Int32 sU2TcProps { get { return this.GfeArchive.sU2TcProps; } }
        public Int32 sU3TcProps { get { return this.GfeArchive.sU3TcProps; } }
        public Int32 sU4TcProps { get { return this.GfeArchive.sU4TcProps; } }
        public Int32 sRecFProps { get { return this.GfeArchive.sRecFProps; } }
        public Int32 sCountyRtcProps { get { return this.GfeArchive.sCountyRtcProps; } }
        public Int32 sStateRtcProps { get { return this.GfeArchive.sStateRtcProps; } }
        public Int32 sU1GovRtcProps { get { return this.GfeArchive.sU1GovRtcProps; } }
        public Int32 sU2GovRtcProps { get { return this.GfeArchive.sU2GovRtcProps; } }
        public Int32 sU3GovRtcProps { get { return this.GfeArchive.sU3GovRtcProps; } }
        public Int32 sPestInspectFProps { get { return this.GfeArchive.sPestInspectFProps; } }
        public Int32 sU1ScProps { get { return this.GfeArchive.sU1ScProps; } }
        public Int32 sU2ScProps { get { return this.GfeArchive.sU2ScProps; } }
        public Int32 sU3ScProps { get { return this.GfeArchive.sU3ScProps; } }
        public Int32 sU4ScProps { get { return this.GfeArchive.sU4ScProps; } }
        public Int32 sU5ScProps { get { return this.GfeArchive.sU5ScProps; } }
        public String sApprFPaidTo { get { return this.GfeArchive.sApprFPaidTo; } }
        public String sCrFPaidTo { get { return this.GfeArchive.sCrFPaidTo; } }
        public String sTxServFPaidTo { get { return this.GfeArchive.sTxServFPaidTo; } }
        public String sFloodCertificationFPaidTo { get { return this.GfeArchive.sFloodCertificationFPaidTo; } }
        public String sInspectFPaidTo { get { return this.GfeArchive.sInspectFPaidTo; } }
        public String sProcFPaidTo { get { return this.GfeArchive.sProcFPaidTo; } }
        public String sUwFPaidTo { get { return this.GfeArchive.sUwFPaidTo; } }
        public String sWireFPaidTo { get { return this.GfeArchive.sWireFPaidTo; } }
        public String s800U1FPaidTo { get { return this.GfeArchive.s800U1FPaidTo; } }
        public String s800U2FPaidTo { get { return this.GfeArchive.s800U2FPaidTo; } }
        public String s800U3FPaidTo { get { return this.GfeArchive.s800U3FPaidTo; } }
        public String s800U4FPaidTo { get { return this.GfeArchive.s800U4FPaidTo; } }
        public String s800U5FPaidTo { get { return this.GfeArchive.s800U5FPaidTo; } }
        public String sOwnerTitleInsPaidTo { get { return this.GfeArchive.sOwnerTitleInsPaidTo; } }
        public String sDocPrepFPaidTo { get { return this.GfeArchive.sDocPrepFPaidTo; } }
        public String sNotaryFPaidTo { get { return this.GfeArchive.sNotaryFPaidTo; } }
        public String sAttorneyFPaidTo { get { return this.GfeArchive.sAttorneyFPaidTo; } }
        public String sU1TcPaidTo { get { return this.GfeArchive.sU1TcPaidTo; } }
        public String sU2TcPaidTo { get { return this.GfeArchive.sU2TcPaidTo; } }
        public String sU3TcPaidTo { get { return this.GfeArchive.sU3TcPaidTo; } }
        public String sU4TcPaidTo { get { return this.GfeArchive.sU4TcPaidTo; } }
        public String sU1GovRtcPaidTo { get { return this.GfeArchive.sU1GovRtcPaidTo; } }
        public String sU2GovRtcPaidTo { get { return this.GfeArchive.sU2GovRtcPaidTo; } }
        public String sU3GovRtcPaidTo { get { return this.GfeArchive.sU3GovRtcPaidTo; } }
        public String sPestInspectPaidTo { get { return this.GfeArchive.sPestInspectPaidTo; } }
        public String sU1ScPaidTo { get { return this.GfeArchive.sU1ScPaidTo; } }
        public String sU2ScPaidTo { get { return this.GfeArchive.sU2ScPaidTo; } }
        public String sU3ScPaidTo { get { return this.GfeArchive.sU3ScPaidTo; } }
        public String sU4ScPaidTo { get { return this.GfeArchive.sU4ScPaidTo; } }
        public String sU5ScPaidTo { get { return this.GfeArchive.sU5ScPaidTo; } }
        public String sHazInsPiaPaidTo { get { return this.GfeArchive.sHazInsPiaPaidTo; } }
        public String sMipPiaPaidTo { get { return this.GfeArchive.sMipPiaPaidTo; } }
        public String sVaFfPaidTo { get { return this.GfeArchive.sVaFfPaidTo; } }
        public E_GfeSectionT s800U1FGfeSection { get { return this.GfeArchive.s800U1FGfeSection; } }
        public E_GfeSectionT s800U2FGfeSection { get { return this.GfeArchive.s800U2FGfeSection; } }
        public E_GfeSectionT s800U3FGfeSection { get { return this.GfeArchive.s800U3FGfeSection; } }
        public E_GfeSectionT s800U4FGfeSection { get { return this.GfeArchive.s800U4FGfeSection; } }
        public E_GfeSectionT s800U5FGfeSection { get { return this.GfeArchive.s800U5FGfeSection; } }
        public E_GfeSectionT sEscrowFGfeSection { get { return this.GfeArchive.sEscrowFGfeSection; } }
        public E_GfeSectionT sDocPrepFGfeSection { get { return this.GfeArchive.sDocPrepFGfeSection; } }
        public E_GfeSectionT sNotaryFGfeSection { get { return this.GfeArchive.sNotaryFGfeSection; } }
        public E_GfeSectionT sAttorneyFGfeSection { get { return this.GfeArchive.sAttorneyFGfeSection; } }
        public E_GfeSectionT sU1TcGfeSection { get { return this.GfeArchive.sU1TcGfeSection; } }
        public E_GfeSectionT sU2TcGfeSection { get { return this.GfeArchive.sU2TcGfeSection; } }
        public E_GfeSectionT sU3TcGfeSection { get { return this.GfeArchive.sU3TcGfeSection; } }
        public E_GfeSectionT sU4TcGfeSection { get { return this.GfeArchive.sU4TcGfeSection; } }
        public E_GfeSectionT sU1GovRtcGfeSection { get { return this.GfeArchive.sU1GovRtcGfeSection; } }
        public E_GfeSectionT sU2GovRtcGfeSection { get { return this.GfeArchive.sU2GovRtcGfeSection; } }
        public E_GfeSectionT sU3GovRtcGfeSection { get { return this.GfeArchive.sU3GovRtcGfeSection; } }
        public E_GfeSectionT sU1ScGfeSection { get { return this.GfeArchive.sU1ScGfeSection; } }
        public E_GfeSectionT sU2ScGfeSection { get { return this.GfeArchive.sU2ScGfeSection; } }
        public E_GfeSectionT sU3ScGfeSection { get { return this.GfeArchive.sU3ScGfeSection; } }
        public E_GfeSectionT sU4ScGfeSection { get { return this.GfeArchive.sU4ScGfeSection; } }
        public E_GfeSectionT sU5ScGfeSection { get { return this.GfeArchive.sU5ScGfeSection; } }
        public E_GfeSectionT s904PiaGfeSection { get { return this.GfeArchive.s904PiaGfeSection; } }
        public E_GfeSectionT s900U1PiaGfeSection { get { return this.GfeArchive.s900U1PiaGfeSection; } }
        public Boolean sGfeIsTPOTransaction { get { return this.GfeArchive.sGfeIsTPOTransaction; } }
        public Boolean sIsItemizeBrokerCommissionOnIFW { get { return this.GfeArchive.sIsItemizeBrokerCommissionOnIFW; } }
        public E_sOriginatorCompensationPaymentSourceT sOriginatorCompensationPaymentSourceT { get { return this.GfeArchive.sOriginatorCompensationPaymentSourceT; } }
        public String sGfeOriginatorCompF_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeOriginatorCompF_rep); } }
        public String sGfeOriginatorCompFPc_rep { get { return this.ToRateString(this.GfeArchive.sGfeOriginatorCompFPc_rep); } }
        public String sGfeOriginatorCompFMb_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeOriginatorCompFMb_rep); } }
        public E_PercentBaseT sGfeOriginatorCompFBaseT { get { return this.GfeArchive.sGfeOriginatorCompFBaseT; } }
        public Boolean sIsOriginationCompensationSourceRequired { get { return this.GfeArchive.sIsOriginationCompensationSourceRequired; } }
        public E_BranchChannelT sBranchChannelT { get { return this.GfeArchive.sBranchChannelT; } }
        public String sLDiscntFMb_rep { get { return this.ToMoneyString(this.GfeArchive.sLDiscntFMb_rep); } }
        public String sLDiscntPc_rep { get { return this.ToRateString(this.GfeArchive.sLDiscntPc_rep); } }
        public E_PercentBaseT sLDiscntBaseT { get { return this.GfeArchive.sLDiscntBaseT; } }
        public E_CreditLenderPaidItemT sGfeCreditLenderPaidItemT { get { return this.GfeArchive.sGfeCreditLenderPaidItemT; } }
        public String sGfeCreditLenderPaidItemF_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeCreditLenderPaidItemF_rep); } }
        public String sGfeLenderCreditFPc_rep { get { return this.ToRateString(this.GfeArchive.sGfeLenderCreditFPc_rep); } }
        public String sGfeLenderCreditF_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeLenderCreditF_rep); } }
        public Int32 sGfeLenderCreditFProps { get { return this.GfeArchive.sGfeLenderCreditFProps; } }
        public String sGfeDiscountPointFPc_rep { get { return this.ToRateString(this.GfeArchive.sGfeDiscountPointFPc_rep); } }
        public String sGfeDiscountPointF_rep { get { return this.ToMoneyString(this.GfeArchive.sGfeDiscountPointF_rep); } }
        public Int32 sGfeDiscountPointFProps { get { return this.GfeArchive.sGfeDiscountPointFProps; } }
        public String sGfeInitialDisclosureD_rep { get { return this.ToDateString(this.GfeArchive.sGfeInitialDisclosureD_rep); } }
        public String sGfeRedisclosureD_rep { get { return this.ToDateString(this.GfeArchive.sGfeRedisclosureD_rep); } }
        public String sLastDisclosedGFEArchiveD_rep { get { return this.ToDateString(this.GfeArchive.sLastDisclosedGFEArchiveD_rep); } }
        public E_sLPurposeT sLPurposeT { get { return this.GfeArchive.sLPurposeT; } }
        public Nullable<DateTime> sGfeInitialDisclosureD { get { return this.GfeArchive.sGfeInitialDisclosureD; } }
        public String sSpAddr { get { return this.GfeArchive.sSpAddr; } }
        public String sSpCity { get { return this.GfeArchive.sSpCity; } }
        public String sSpState { get { return this.GfeArchive.sSpState; } }
        public String sSpZip { get { return this.GfeArchive.sSpZip; } }
        public String sGfeNoteIRAvailTillD_DateTime { get { return this.GfeArchive.sGfeNoteIRAvailTillD_DateTime; } }
        public String sGfeEstScAvailTillD_DateTime { get { return this.GfeArchive.sGfeEstScAvailTillD_DateTime; } }
        public String sDueInYr_rep { get { return this.ToCountString(this.GfeArchive.sDueInYr_rep); } }
        public E_sGfeCreditChargeT sGfeCreditChargeT { get { return this.GfeArchive.sGfeCreditChargeT; } }
        public String sGfeNoCreditChargeNoteIR { get { return this.GfeArchive.sGfeNoCreditChargeNoteIR; } }
        public String sGfeCreditNoteIR { get { return this.GfeArchive.sGfeCreditNoteIR; } }
        public String sGfeCreditLDiscnt { get { return this.GfeArchive.sGfeCreditLDiscnt; } }
        public String sGfeChargeNoteIR { get { return this.GfeArchive.sGfeChargeNoteIR; } }
        public String sGfeChargeLDiscnt { get { return this.GfeArchive.sGfeChargeLDiscnt; } }
        public Boolean sGfeHasImpoundDeposit { get { return this.GfeArchive.sGfeHasImpoundDeposit; } }
        public String sGfeCanRateIncrease_rep { get { return this.ToBitString(this.GfeArchive.sGfeCanRateIncrease_rep); } }
        public String sGfeCanLoanBalanceIncrease_rep { get { return this.ToBitString(this.GfeArchive.sGfeCanLoanBalanceIncrease_rep); } }
        public String sGfeHavePpmtPenalty_rep { get { return this.ToBitString(this.GfeArchive.sGfeHavePpmtPenalty_rep); } }
        public String sGfeIsBalloon_rep { get { return this.ToBitString(this.GfeArchive.sGfeIsBalloon_rep); } }
        public String sGfeShoppingCartLoan1CanRateIncreaseTri_rep { get { return this.ToTriString(this.GfeArchive.sGfeShoppingCartLoan1CanRateIncreaseTri_rep); } }
        public String sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri_rep { get { return this.ToTriString(this.GfeArchive.sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri_rep); } }
        public String sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri_rep { get { return this.ToTriString(this.GfeArchive.sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri_rep); } }
        public String sGfeShoppingCartLoan1HavePpmtPenaltyTri_rep { get { return this.ToTriString(this.GfeArchive.sGfeShoppingCartLoan1HavePpmtPenaltyTri_rep); } }
        public String sGfeShoppingCartLoan1IsBalloonTri_rep { get { return this.ToTriString(this.GfeArchive.sGfeShoppingCartLoan1IsBalloonTri_rep); } }
        public String sGfeShoppingCartLoan2CanRateIncreaseTri_rep { get { return this.ToTriString(this.GfeArchive.sGfeShoppingCartLoan2CanRateIncreaseTri_rep); } }
        public String sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri_rep { get { return this.ToTriString(this.GfeArchive.sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri_rep); } }
        public String sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri_rep { get { return this.ToTriString(this.GfeArchive.sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri_rep); } }
        public String sGfeShoppingCartLoan2HavePpmtPenaltyTri_rep { get { return this.ToTriString(this.GfeArchive.sGfeShoppingCartLoan2HavePpmtPenaltyTri_rep); } }
        public String sGfeShoppingCartLoan2IsBalloonTri_rep { get { return this.ToTriString(this.GfeArchive.sGfeShoppingCartLoan2IsBalloonTri_rep); } }
        public String sGfeShoppingCartLoan3CanRateIncreaseTri_rep { get { return this.ToTriString(this.GfeArchive.sGfeShoppingCartLoan3CanRateIncreaseTri_rep); } }
        public String sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri_rep { get { return this.ToTriString(this.GfeArchive.sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri_rep); } }
        public String sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri_rep { get { return this.ToTriString(this.GfeArchive.sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri_rep); } }
        public String sGfeShoppingCartLoan3HavePpmtPenaltyTri_rep { get { return this.ToTriString(this.GfeArchive.sGfeShoppingCartLoan3HavePpmtPenaltyTri_rep); } }
        public String sGfeShoppingCartLoan3IsBalloonTri_rep { get { return this.ToTriString(this.GfeArchive.sGfeShoppingCartLoan3IsBalloonTri_rep); } }
        public String sGfeService1WeSelectName { get { return this.GfeArchive.sGfeService1WeSelectName; } }
        public String sGfeService2WeSelectName { get { return this.GfeArchive.sGfeService2WeSelectName; } }
        public String sGfeService3WeSelectName { get { return this.GfeArchive.sGfeService3WeSelectName; } }
        public String sGfeService4WeSelectName { get { return this.GfeArchive.sGfeService4WeSelectName; } }
        public String sGfeService5WeSelectName { get { return this.GfeArchive.sGfeService5WeSelectName; } }
        public String sGfeService6WeSelectName { get { return this.GfeArchive.sGfeService6WeSelectName; } }
        public String sGfeService7WeSelectName { get { return this.GfeArchive.sGfeService7WeSelectName; } }
        public String sGfeService8WeSelectName { get { return this.GfeArchive.sGfeService8WeSelectName; } }
        public String sGfeService9WeSelectName { get { return this.GfeArchive.sGfeService9WeSelectName; } }
        public String sGfeService10WeSelectName { get { return this.GfeArchive.sGfeService10WeSelectName; } }
        public String sGfeService11WeSelectName { get { return this.GfeArchive.sGfeService11WeSelectName; } }
        public String sGfeService12WeSelectName { get { return this.GfeArchive.sGfeService12WeSelectName; } }
        public String sGfeService13WeSelectName { get { return this.GfeArchive.sGfeService13WeSelectName; } }
        public String sGfeService14WeSelectName { get { return this.GfeArchive.sGfeService14WeSelectName; } }
        public String sGfeService15WeSelectName { get { return this.GfeArchive.sGfeService15WeSelectName; } }
        public String sGfeService16WeSelectName { get { return this.GfeArchive.sGfeService16WeSelectName; } }
        public String sGfeService17WeSelectName { get { return this.GfeArchive.sGfeService17WeSelectName; } }
        public String sGfeService18WeSelectName { get { return this.GfeArchive.sGfeService18WeSelectName; } }
        public String sGfeService1WeSelectFee { get { return this.GfeArchive.sGfeService1WeSelectFee; } }
        public String sGfeService2WeSelectFee { get { return this.GfeArchive.sGfeService2WeSelectFee; } }
        public String sGfeService3WeSelectFee { get { return this.GfeArchive.sGfeService3WeSelectFee; } }
        public String sGfeService4WeSelectFee { get { return this.GfeArchive.sGfeService4WeSelectFee; } }
        public String sGfeService5WeSelectFee { get { return this.GfeArchive.sGfeService5WeSelectFee; } }
        public String sGfeService6WeSelectFee { get { return this.GfeArchive.sGfeService6WeSelectFee; } }
        public String sGfeService7WeSelectFee { get { return this.GfeArchive.sGfeService7WeSelectFee; } }
        public String sGfeService8WeSelectFee { get { return this.GfeArchive.sGfeService8WeSelectFee; } }
        public String sGfeService9WeSelectFee { get { return this.GfeArchive.sGfeService9WeSelectFee; } }
        public String sGfeService10WeSelectFee { get { return this.GfeArchive.sGfeService10WeSelectFee; } }
        public String sGfeService11WeSelectFee { get { return this.GfeArchive.sGfeService11WeSelectFee; } }
        public String sGfeService12WeSelectFee { get { return this.GfeArchive.sGfeService12WeSelectFee; } }
        public String sGfeService13WeSelectFee { get { return this.GfeArchive.sGfeService13WeSelectFee; } }
        public String sGfeService14WeSelectFee { get { return this.GfeArchive.sGfeService14WeSelectFee; } }
        public String sGfeService15WeSelectFee { get { return this.GfeArchive.sGfeService15WeSelectFee; } }
        public String sGfeService16WeSelectFee { get { return this.GfeArchive.sGfeService16WeSelectFee; } }
        public String sGfeService17WeSelectFee { get { return this.GfeArchive.sGfeService17WeSelectFee; } }
        public String sGfeService18WeSelectFee { get { return this.GfeArchive.sGfeService18WeSelectFee; } }
        public String sGfeService1YouCanShopName { get { return this.GfeArchive.sGfeService1YouCanShopName; } }
        public String sGfeService2YouCanShopName { get { return this.GfeArchive.sGfeService2YouCanShopName; } }
        public String sGfeService3YouCanShopName { get { return this.GfeArchive.sGfeService3YouCanShopName; } }
        public String sGfeService4YouCanShopName { get { return this.GfeArchive.sGfeService4YouCanShopName; } }
        public String sGfeService5YouCanShopName { get { return this.GfeArchive.sGfeService5YouCanShopName; } }
        public String sGfeService6YouCanShopName { get { return this.GfeArchive.sGfeService6YouCanShopName; } }
        public String sGfeService7YouCanShopName { get { return this.GfeArchive.sGfeService7YouCanShopName; } }
        public String sGfeService8YouCanShopName { get { return this.GfeArchive.sGfeService8YouCanShopName; } }
        public String sGfeService9YouCanShopName { get { return this.GfeArchive.sGfeService9YouCanShopName; } }
        public String sGfeService10YouCanShopName { get { return this.GfeArchive.sGfeService10YouCanShopName; } }
        public String sGfeService11YouCanShopName { get { return this.GfeArchive.sGfeService11YouCanShopName; } }
        public String sGfeService12YouCanShopName { get { return this.GfeArchive.sGfeService12YouCanShopName; } }
        public String sGfeService13YouCanShopName { get { return this.GfeArchive.sGfeService13YouCanShopName; } }
        public String sGfeService14YouCanShopName { get { return this.GfeArchive.sGfeService14YouCanShopName; } }
        public String sGfeService15YouCanShopName { get { return this.GfeArchive.sGfeService15YouCanShopName; } }
        public String sGfeService16YouCanShopName { get { return this.GfeArchive.sGfeService16YouCanShopName; } }
        public String sGfeService1YouCanShopFee { get { return this.GfeArchive.sGfeService1YouCanShopFee; } }
        public String sGfeService2YouCanShopFee { get { return this.GfeArchive.sGfeService2YouCanShopFee; } }
        public String sGfeService3YouCanShopFee { get { return this.GfeArchive.sGfeService3YouCanShopFee; } }
        public String sGfeService4YouCanShopFee { get { return this.GfeArchive.sGfeService4YouCanShopFee; } }
        public String sGfeService5YouCanShopFee { get { return this.GfeArchive.sGfeService5YouCanShopFee; } }
        public String sGfeService6YouCanShopFee { get { return this.GfeArchive.sGfeService6YouCanShopFee; } }
        public String sGfeService7YouCanShopFee { get { return this.GfeArchive.sGfeService7YouCanShopFee; } }
        public String sGfeService8YouCanShopFee { get { return this.GfeArchive.sGfeService8YouCanShopFee; } }
        public String sGfeService9YouCanShopFee { get { return this.GfeArchive.sGfeService9YouCanShopFee; } }
        public String sGfeService10YouCanShopFee { get { return this.GfeArchive.sGfeService10YouCanShopFee; } }
        public String sGfeService11YouCanShopFee { get { return this.GfeArchive.sGfeService11YouCanShopFee; } }
        public String sGfeService12YouCanShopFee { get { return this.GfeArchive.sGfeService12YouCanShopFee; } }
        public String sGfeService13YouCanShopFee { get { return this.GfeArchive.sGfeService13YouCanShopFee; } }
        public String sGfeService14YouCanShopFee { get { return this.GfeArchive.sGfeService14YouCanShopFee; } }
        public String sGfeService15YouCanShopFee { get { return this.GfeArchive.sGfeService15YouCanShopFee; } }
        public String sGfeService16YouCanShopFee { get { return this.GfeArchive.sGfeService16YouCanShopFee; } }
        public String sHazInsPiaPaidToPrint { get { return this.GfeArchive.sHazInsPiaPaidToPrint; } }
        public String sGfeHomeOwnerInsurance1Name { get { return this.GfeArchive.sGfeHomeOwnerInsurance1Name; } }
        public String sGfeHomeOwnerInsurance1Fee { get { return this.GfeArchive.sGfeHomeOwnerInsurance1Fee; } }
        public String sGfeHomeOwnerInsurance2Name { get { return this.GfeArchive.sGfeHomeOwnerInsurance2Name; } }
        public String sGfeHomeOwnerInsurance2Fee { get { return this.GfeArchive.sGfeHomeOwnerInsurance2Fee; } }
        public String sGfeHomeOwnerInsurance3Name { get { return this.GfeArchive.sGfeHomeOwnerInsurance3Name; } }
        public String sGfeHomeOwnerInsurance3Fee { get { return this.GfeArchive.sGfeHomeOwnerInsurance3Fee; } }
        public Boolean sGfeHasImpoundDepositTax { get { return this.GfeArchive.sGfeHasImpoundDepositTax; } }
        public Boolean sGfeHasImpoundDepositInsurance { get { return this.GfeArchive.sGfeHasImpoundDepositInsurance; } }
        public Boolean sGfeHasImpoundDepositOther { get { return this.GfeArchive.sGfeHasImpoundDepositOther; } }
        public String sGfeHasImpoundDepositDescription { get { return this.GfeArchive.sGfeHasImpoundDepositDescription; } }
        public Boolean sGfeRateLockPeriodLckd { get { return this.GfeArchive.sGfeRateLockPeriodLckd; } }
        public Boolean sHazInsRsrvMonLckd { get { return this.GfeArchive.sHazInsRsrvMonLckd; } }
        public Boolean sMInsRsrvMonLckd { get { return this.GfeArchive.sMInsRsrvMonLckd; } }
        public Boolean sRealETxRsrvMonLckd { get { return this.GfeArchive.sRealETxRsrvMonLckd; } }
        public Boolean sSchoolTxRsrvMonLckd { get { return this.GfeArchive.sSchoolTxRsrvMonLckd; } }
        public Boolean sFloodInsRsrvMonLckd { get { return this.GfeArchive.sFloodInsRsrvMonLckd; } }
        public Boolean s1006RsrvMonLckd { get { return this.GfeArchive.s1006RsrvMonLckd; } }
        public Boolean s1007RsrvMonLckd { get { return this.GfeArchive.s1007RsrvMonLckd; } }
        public Boolean sU3RsrvMonLckd { get { return this.GfeArchive.sU3RsrvMonLckd; } }
        public Boolean sU4RsrvMonLckd { get { return this.GfeArchive.sU4RsrvMonLckd; } }
        public E_TriState sHazInsRsrvEscrowedTri { get { return this.GfeArchive.sHazInsRsrvEscrowedTri; } }
        public E_TriState sMInsRsrvEscrowedTri { get { return this.GfeArchive.sMInsRsrvEscrowedTri; } }
        public E_TriState sRealETxRsrvEscrowedTri { get { return this.GfeArchive.sRealETxRsrvEscrowedTri; } }
        public E_TriState sSchoolTxRsrvEscrowedTri { get { return this.GfeArchive.sSchoolTxRsrvEscrowedTri; } }
        public E_TriState sFloodInsRsrvEscrowedTri { get { return this.GfeArchive.sFloodInsRsrvEscrowedTri; } }
        public E_TriState s1006RsrvEscrowedTri { get { return this.GfeArchive.s1006RsrvEscrowedTri; } }
        public E_TriState s1007RsrvEscrowedTri { get { return this.GfeArchive.s1007RsrvEscrowedTri; } }
        public E_TriState sU3RsrvEscrowedTri { get { return this.GfeArchive.sU3RsrvEscrowedTri; } }
        public E_TriState sU4RsrvEscrowedTri { get { return this.GfeArchive.sU4RsrvEscrowedTri; } }
        public E_sLT sLT { get { return this.GfeArchive.sLT; } }
        public bool sFfUfMipIsBeingFinanced { get { return this.GfeArchive.sFfUfMipIsBeingFinanced; } }
        public E_sLenderCreditCalculationMethodT sLenderCreditCalculationMethodT { get { return this.GfeArchive.sLenderCreditCalculationMethodT; } }
        public E_MipFrequency sMipFrequency { get { return this.GfeArchive.sMipFrequency; } }
        #endregion

        public decimal? sLAmtCalc { get { return this.GfeArchive.sLAmtCalc; } }
        public decimal? sPurchPrice { get { return this.GfeArchive.sPurchPrice; } }
        public decimal? sApprVal { get { return this.GfeArchive.sApprVal; } }
        public decimal? sSpOrigC { get { return this.GfeArchive.sSpOrigC; } }
        public decimal? sLotOrigC { get { return this.GfeArchive.sLotOrigC; } }
        public decimal? sFinalLAmt { get { return this.GfeArchive.sFinalLAmt; } }

        public decimal? sLenderTargetInitialCreditAmt { get { return this.GfeArchive.sLenderTargetInitialCreditAmt; } }

        #region manually entered fields.
        public string gfeTil_CompanyName { get { return this.GfeArchive.gfeTil_CompanyName; } }
        public string gfeTil_StreetAddr { get { return this.GfeArchive.gfeTil_StreetAddr; } }
        public string gfeTil_City { get { return this.GfeArchive.gfeTil_City; } }
        public string gfeTil_State { get { return this.GfeArchive.gfeTil_State; } }
        public string gfeTil_Zip { get { return this.GfeArchive.gfeTil_Zip; } }
        public string gfeTil_PhoneOfCompany { get { return this.GfeArchive.gfeTil_PhoneOfCompany; } }
        public string gfeTil_EmailAddr { get { return this.GfeArchive.gfeTil_EmailAddr; } }
        public string gfeTil_PrepareDate { get { return this.GfeArchive.gfeTil_PrepareDate; } }
        public bool gfeTil_IsLocked { get { return this.GfeArchive.gfeTil_IsLocked; } }
        public E_AgentRoleT gfeTil_AgentRoleT { get { return this.GfeArchive.gfeTil_AgentRoleT; } }

        public string dataApp_aBNm_aCNm { get { return this.GfeArchive.dataApp_aBNm_aCNm; } }
        #endregion
    }
}
