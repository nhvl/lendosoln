﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.Common.SerializationTypes
{
    public interface ICoCArchive
    {
        string DateArchived { get; }
        string GfePrepareDate { get; }
        string sCircumstanceChangeD { get; }
        string sGfeRedisclosureD { get; }
        string sCircumstanceChangeExplanation { get; }
        string sCircumstanceChange1Amount { get; }
        string sCircumstanceChange1Reason { get; }
        string sCircumstanceChange1FeeId { get; }
        string sCircumstanceChange2Amount { get; }
        string sCircumstanceChange2Reason { get; }
        string sCircumstanceChange2FeeId { get; }
        string sCircumstanceChange3Amount { get; }
        string sCircumstanceChange3Reason { get; }
        string sCircumstanceChange3FeeId { get; }
        string sCircumstanceChange4Amount { get; }
        string sCircumstanceChange4Reason { get; }
        string sCircumstanceChange4FeeId { get; }
        string sCircumstanceChange5Amount { get; }
        string sCircumstanceChange5Reason { get; }
        string sCircumstanceChange5FeeId { get; }
        string sCircumstanceChange6Amount { get; }
        string sCircumstanceChange6Reason { get; }
        string sCircumstanceChange6FeeId { get; }
        string sCircumstanceChange7Amount { get; }
        string sCircumstanceChange7Reason { get; }
        string sCircumstanceChange7FeeId { get; }
        string sCircumstanceChange8Amount { get; }
        string sCircumstanceChange8Reason { get; }
        string sCircumstanceChange8FeeId { get; }

    }

    public class CoCArchive : ICoCArchive
    {

        public CoCArchive() { }
        public string DateArchived { get; set; }
        public string GfePrepareDate { get; set; }
        public string sCircumstanceChangeD { get; set; }
        public string sGfeRedisclosureD { get; set; }
        public string sCircumstanceChangeExplanation { get; set; }
        public string sCircumstanceChange1Amount { get; set; }
        public string sCircumstanceChange1Reason { get; set; }
        public string sCircumstanceChange1FeeId { get; set; }
        public string sCircumstanceChange2Amount { get; set; }
        public string sCircumstanceChange2Reason { get; set; }
        public string sCircumstanceChange2FeeId { get; set; }
        public string sCircumstanceChange3Amount { get; set; }
        public string sCircumstanceChange3Reason { get; set; }
        public string sCircumstanceChange3FeeId { get; set; }
        public string sCircumstanceChange4Amount { get; set; }
        public string sCircumstanceChange4Reason { get; set; }
        public string sCircumstanceChange4FeeId { get; set; }
        public string sCircumstanceChange5Amount { get; set; }
        public string sCircumstanceChange5Reason { get; set; }
        public string sCircumstanceChange5FeeId { get; set; }
        public string sCircumstanceChange6Amount { get; set; }
        public string sCircumstanceChange6Reason { get; set; }
        public string sCircumstanceChange6FeeId { get; set; }
        public string sCircumstanceChange7Amount { get; set; }
        public string sCircumstanceChange7Reason { get; set; }
        public string sCircumstanceChange7FeeId { get; set; }
        public string sCircumstanceChange8Amount { get; set; }
        public string sCircumstanceChange8Reason { get; set; }
        public string sCircumstanceChange8FeeId { get; set; }
    }

    public class FeeChange
    {
        public string ArchiveDescription { get; set; }
        public string RawDescription { get; set; }
        public string Description { get; set; }
        public bool IsPercentFee { get; set; }
        public DataAccess.E_PercentBaseT PercentBase { get; set; }
        public string ArchiveValue { get; set; }
        public string CurrentValue { get; set; }
        public string GfeSection { get; set; }
        public string FeeId { get; set; }
        public string ArchiveGfeSection { get; set; }
        public string ArchivePercent { get; set; }
        public string Percent { get; set; }
        public string ArchiveFixedFee { get; set; }
        public string FixedFee { get; set; }

        // OPM 227743. mf.  Some CoC fees can now be based
        // directly on loan values. This one indicates that.
        public bool IsLoanField { get; set; }

        public bool DoNotShow { get; set; }
    }

}
