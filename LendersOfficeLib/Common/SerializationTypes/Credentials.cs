﻿//-----------------------------------------------------------------------
// <summary>
//      A user credential used in automated requests from a database message queue.
// </summary>
// <copyright file="Credentials.cs" company="MeridianLink">
//      Copyright (c) MeridianLink. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LendersOffice.Common.SerializationTypes
{
    /// <summary>
    /// Contains a set of user credentials for serialization.
    /// </summary>
    public class Credentials
    {
        /// <summary>
        /// Gets or sets the user's username.
        /// </summary>
        /// <value>The user's username.</value>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the user's password.
        /// </summary>
        /// <value>The user's password.</value>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the number of attempts, as in transmission attempts from a database message queue.
        /// </summary>
        /// <value>The number of attempts.</value>
        public int AttemptCount { get; set; }
    }
}
