﻿//-----------------------------------------------------------------------
// <summary>
//      Closing Cost Archives.
// </summary>
// <copyright file="ClosingCostCoCArchive.cs" company="MeridianLink">
//      Copyright (c) MeridianLink. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LendersOffice.Common.SerializationTypes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using ComplianceEagle;
    using DataAccess;

    /// <summary>
    /// Archive entry of a 2015 Change of Circumstance.
    /// </summary>
    public class ClosingCostCoCArchive
    {
        /// <summary>
        /// Initializes a new instance of the ClosingCostCoCArchive class.
        /// </summary>
        public ClosingCostCoCArchive() 
        { 
        }

        /// <summary>
        /// Gets or sets the date archived.
        /// </summary>
        /// <value>Value of the date archived.</value>
        public string DateArchived { get; set; }

        /// <summary>
        /// Gets or sets the value of the date that circumstances changed.
        /// </summary>
        /// <value>Value of the date that circumstances changed.</value>
        public string CircumstanceChangeD { get; set; }

        /// <summary>
        /// Gets or sets the date re-disclosed.
        /// </summary>
        /// <value>Value of date re-disclosed.</value>
        public string RedisclosureD { get; set; }

        /// <summary>
        /// Gets or sets the changed circumstance category.
        /// </summary>
        /// <value>Value of the changed circumstance category.</value>
        public CEagleChangedCircumstanceCategory ComplianceCOCCategory { get; set; }

        /// <summary>
        /// Gets or sets the value of the fee.
        /// </summary>
        /// <value>Value of the fee.</value>
        public string CircumstanceChangeExplanation { get; set; }

        /// <summary>
        /// Gets or sets the id of the archive that was used to check for fee 
        /// differences against live data.
        /// </summary>
        /// <value>
        /// The id of the archive that was used to check for fee differences 
        /// against live data.
        /// </value>
        public Guid BaseArchiveId { get; set; }

        /// <summary>
        /// Gets or sets the id of the archive that was created as a result of the CoC.
        /// </summary>
        /// <value>
        /// The id of the archive that was created as a result of the CoC.
        /// </value>
        public Guid CreatedArchiveId { get; set; }

        /// <summary>
        /// Gets or sets the list of fees.
        /// </summary>
        /// <value>List of fees.</value>
        public List<Fee> Fees { get; set; }

        /// <summary>
        /// Gets or sets the list of loan values.
        /// </summary>
        /// <value>List of loan values.</value>
        public List<LoanValue> LoanValues { get; set; }

        /// <summary>
        /// A plain old data class representing a stored fee.
        /// </summary>
        public class Fee
        {
            /// <summary>
            /// Gets or sets the FeeId.
            /// </summary>
            /// <value>The Id of this fee.</value>
            public Guid FeeId { get; set; }

            /// <summary>
            /// Gets or sets the value of the fee.
            /// </summary>
            /// <value>Value of the fee.</value>
            public decimal Value { get; set; }

            /// <summary>
            /// Gets or sets the description of the fee.
            /// </summary>
            /// <value>Value of description of the fee.</value>
            public string Description { get; set; }
        }

        /// <summary>
        /// A plain old data class representing a stored loan value.
        /// </summary>
        public class LoanValue
        {
            /// <summary>
            /// Gets or sets the FieldId.
            /// </summary>
            /// <value>The FieldId of this loan value.</value>
            public string FieldId { get; set; }

            /// <summary>
            /// Gets or sets the value of the loan value.
            /// </summary>
            /// <value>Value of the loan value.</value>
            public decimal Value { get; set; }

            /// <summary>
            /// Gets or sets the description of the loan value.
            /// </summary>
            /// <value>Value of description of the loan value.</value>
            public string Description { get; set; }
        }
    }
}
