﻿namespace LendersOffice.Common.SerializationTypes
{
    using System;
    using System.Data;
    using System.Xml;
    using System.Xml.Schema;
    using DataAccess;

    /// <summary>
    /// Archive of 2015 version closing cost fees.
    /// </summary>
    public interface IClosingCostArchive
    {
        /// <summary>
        /// Gets or sets the type of the archive.
        /// </summary>
        /// <value>The type of the archive.</value>
        ClosingCostArchive.E_ClosingCostArchiveType ClosingCostArchiveType { get; set; }

        /// <summary>
        /// Gets or sets the date this archive was archived.
        /// </summary>
        /// <value>Date of this archive.</value>
        string DateArchived { get; set; }

        /// <summary>
        /// Gets a value indicating the file version of the FileDB content.
        /// </summary>
        /// <value>A value indicating the file version of the FileDB content.</value>
        int FileDBContentVersion { get; }

        /// <summary>
        /// Gets the FileDB key for this archive's field/value content.<para/>
        /// Note that after deserialization this may give a value for the intended FileDB key
        /// even if there's no entry in the FileDB.
        /// </summary>
        /// <value>The value of the key associated with the archive, or what it would be when written to the fileDB.</value>
        string FileDBKey { get; }

        /// <summary>
        /// Gets or sets the ID of a closing cost archive linked to this archive.
        /// </summary>
        /// <value>
        /// The linked archive ID or null if no link exists.
        /// </value>
        /// <remarks>
        /// This ID is intended primarily to link TRID 2.0 live data
        /// archives with their associated tolerance archives.
        /// </remarks>
        Guid? LinkedArchiveId { get; set; }

        /// <summary>
        /// Gets the archive ID.
        /// </summary>
        /// <value>The archive ID.</value>
        Guid Id { get; }

        /// <summary>
        /// Gets a value indicating whether this archive should be considered disclosed.
        /// </summary>
        /// <value>True if the archive is disclosed. Otherwise, false.</value>
        bool IsDisclosed { get; }

        /// <summary>
        /// Gets or sets a value indicating whether the archive is being used temporarily by the CoC process.
        /// </summary>
        /// <value>True if the archive will be discarded.</value>
        bool IsForCoCProcess { get; set; }

        /// <summary>
        /// Gets or sets the source of the archive. 
        /// </summary>
        /// <value>The source of the archive.</value>
        ClosingCostArchive.E_ClosingCostArchiveSource Source { get; set; }

        /// <summary>
        /// Gets or sets the status of the archive.
        /// </summary>
        /// <value>
        /// The status of the archive. All non-Loan Estimate archives default to
        /// Disclosed.
        /// </value>
        ClosingCostArchive.E_ClosingCostArchiveStatus Status { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the field/value content should be serialized inline.
        /// </summary>
        /// <value>
        /// True if the field/value content should be stored inline and NOT in FileDB.
        ///  False if the field/value content should be stored in FileDB.
        /// </value>
        bool StoreFieldsInline { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this archive was a tolerance basis archive at one point.
        /// </summary>
        /// <value>Value indicating whether this archive was a tolerance basis archive.</value>
        bool WasBasisArchive { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this archive uses live data
        /// at the time of creation and should not be refreshed during events such
        /// as document generation or change of circumstance submissions.
        /// </summary>
        /// <value>
        /// True if the archive uses live data, false otherwise.
        /// </value>
        bool UsesLiveData { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the archive was created
        /// from an archive in the Pending CoC archive as part of document
        /// generation.
        /// </summary>
        /// <value>True if the archive was sourced from a pending CoC archive. Otherwise, false.</value>
        bool WasSourcedFromPendingCoC { get; set; }

        /// <summary>
        /// Gets the APR associated with the closing cost archive.
        /// </summary>
        /// <value>The APR associated with the closing cost archive. Null if it does not exist.</value>
        decimal? Apr { get; }

        /// <summary>
        /// Duplicate this closing cost archive, and give it a new ID.
        /// </summary>
        /// <returns>A duplicate of this closing cost archive.</returns>
        ClosingCostArchive Duplicate();

        /// <summary>
        /// Populate this archive from this loan file.
        /// </summary>
        /// <param name="loanId">The loan to use.</param>
        void ExtractFromLoan(Guid loanId);

        /// <summary>
        /// Populate this archive from this loan file.
        /// </summary>
        /// <param name="dataLoan">The loan to use.</param>
        void ExtractFromLoan(CPageData dataLoan);

        /// <summary>
        /// Flush's the archive's field/value content to FileDB.
        /// </summary>
        void FlushToFileDBIfDirty();

        /// <summary>
        /// Get boolean value from archive.
        /// </summary>
        /// <param name="fieldId">Field to retrieve.</param>
        /// <returns>Field value from this archive.</returns>
        bool GetBitValue(string fieldId);

        /// <summary>
        /// Get boolean value from archive.
        /// </summary>
        /// <param name="fieldId">Field to retrieve.</param>
        /// <param name="defaultValue">
        /// Value to return if archive does not contain field.
        /// </param>
        /// <returns>
        /// If the archive contains the field, returns the value from the
        /// archive. Otherwise, returns value specified by defaultValue.
        /// </returns>
        bool GetBitValue(string fieldId, bool defaultValue);

        /// <summary>
        /// Gets the agent data set and caches it.
        /// </summary>
        /// <returns>A data set representing the agents.</returns>
        DataSet GetCachedAgentDataSet();

        /// <summary>
        /// Get CDateTime value from archive.
        /// </summary>
        /// <param name="fieldId">Field to retrieve.</param>
        /// <returns>Field value from this archive.</returns>
        CDateTime GetCDateTimeValue(string fieldId);

        /// <summary>
        /// Returns a closing cost set from the current archive.  Each one is a new copy.
        /// </summary>
        /// <param name="converter">The converter to use.</param>
        /// <returns>A closing cost set for the current archive.</returns>
        BorrowerClosingCostSet GetClosingCostSet(LosConvert converter = null);

        /// <summary>
        /// Get integer value from archive.
        /// </summary>
        /// <param name="fieldId">Field to retrieve.</param>
        /// <returns>Field value from this archive.</returns>
        int GetCountValue(string fieldId);

        /// <summary>
        /// Get DateTime value from archive.
        /// </summary>
        /// <param name="fieldId">Field to retrieve.</param>
        /// <returns>Field value from this archive.</returns>
        DateTime GetDateTimeValue(string fieldId);

        /// <summary>
        /// Get guid value from archive.
        /// </summary>
        /// <param name="fieldId">Field to retrieve.</param>
        /// <returns>Field value from this archive.</returns>
        Guid GetGuidValue(string fieldId);

        /// <summary>
        /// Get money value from archive.
        /// </summary>
        /// <param name="fieldId">Field to retrieve.</param>
        /// <returns>Field value from this archive.</returns>
        decimal GetMoneyValue(string fieldId);

        /// <summary>
        /// Get rate value from archive.
        /// </summary>
        /// <param name="fieldId">Field to retrieve.</param>
        /// <returns>Field value from this archive.</returns>
        decimal GetRateValue(string fieldId);

        /// <summary>
        /// Per MSDN: This method is reserved and should not be used.  When implementing the IXmlSerializable interface, you should return null.
        /// </summary>
        /// <returns>Null.  Always.</returns>
        XmlSchema GetSchema();

        /// <summary>
        /// Get string value from archive.
        /// </summary>
        /// <param name="fieldId">Field to retrieve.</param>
        /// <returns>Field value from this archive.</returns>
        string GetValue(string fieldId);

        /// <summary>
        /// Check if this archive has a value.
        /// </summary>
        /// <param name="fieldId">Field to overwrite.</param>
        /// <returns>True if value is known, otherwise false.</returns>
        bool HasValue(string fieldId);

        /// <summary>
        /// Write an existing value with new one.
        /// </summary>
        /// <param name="fieldId">Field to overwrite.</param>
        /// <param name="newValue">Value to store.</param>
        void OverrideValue(string fieldId, string newValue);

        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="reader">Xml reader to read content from.</param>
        void ReadXml(XmlReader reader);

        /// <summary>
        /// Remove the given field from the archive to allow it to update again.
        /// </summary>
        /// <param name="fieldId">The field id to remove.</param>
        void RemoveValue(string fieldId);

        /// <summary>
        /// Allows us to overwrite the ID for the current archive. 
        /// </summary>
        /// <param name="id">The new ID to use in the closing archive.</param>
        void SetId(Guid id);

        /// <summary>
        /// Copies the latest values from the loan data that are marked as always update in CoC mode.
        /// </summary>
        /// <param name="dataLoan">The loan data object containing the latest.</param>
        void UpdateArchiveWithLatestLoanDataForCoC(CPageData dataLoan);

        /// <summary>
        /// Converts an object into its XML representation.
        /// </summary>
        /// <param name="writer">Xml writer to receive content.</param>
        void WriteXml(XmlWriter writer);
    }
}