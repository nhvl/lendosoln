﻿// <copyright file="ClosingCostFeeWithSEction.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   6/30/2015
// </summary>

namespace LendersOffice.Common.SerializationTypes
{
    using DataAccess;

    /// <summary>
    /// Simple class used for sending fee and it's section since the fee's section may not change with view.
    /// </summary>
    public class ClosingCostFeeWithSection
    {
        /// <summary>
        /// Gets or sets the section that this fee would belong to in it's appropriate view.
        /// </summary>
        /// <value>The integrated disclosure section of the fee given the view.</value>
        public E_IntegratedDisclosureSectionT Section { get; set; }

        /// <summary>
        /// Gets or sets the fee itself.
        /// </summary>
        /// <value>The fee that's in the view.</value>
        public LoanClosingCostFee Fee { get; set; }
    }
}
