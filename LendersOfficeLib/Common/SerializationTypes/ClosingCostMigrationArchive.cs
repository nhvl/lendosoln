﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.Common.SerializationTypes
{
    public class ClosingCostMigrationArchive
    {
        public ClosingCostMigrationArchive()
        {
        }
        public GFEArchive GFEArchive { get; set; }
        public Guid SourceEmployeeId { get; set; }
        public DataAccess.E_CurrentClosingCostSourceT CurrentClosingCostSource { get; set; }
        public DataAccess.E_LastDisclosedClosingCostSourceT LastDisclosedClosingCostSource { get; set; }
    }
}
