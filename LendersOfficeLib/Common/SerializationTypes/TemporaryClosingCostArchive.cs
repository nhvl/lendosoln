﻿namespace LendersOffice.Common.SerializationTypes
{
    using System;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    /// Represents a temporary archive for use in seamless document generation.
    /// </summary>
    public class TemporaryArchive : IXmlSerializable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TemporaryArchive"/> class.
        /// </summary>
        /// <param name="toleranceArchive">The closing cost archive containing tolerance data.</param>
        /// <param name="action">
        /// The action to take after documents are generated from this archive.
        /// </param>
        public TemporaryArchive(
            ClosingCostArchive toleranceArchive,
            PostDocumentGenerationAction action)
        {
            this.ToleranceDataClosingCostArchive = toleranceArchive;
            this.Action = action;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TemporaryArchive"/> class.
        /// </summary>
        /// <param name="toleranceArchive">The closing cost archive containing tolerance data.</param>
        /// <param name="liveDataArchive">The closing cost archive containing live data, if used.</param>
        /// <param name="action">
        /// The action to take after documents are generated from this archive.
        /// </param>
        public TemporaryArchive(
            ClosingCostArchive toleranceArchive,
            ClosingCostArchive liveDataArchive,
            PostDocumentGenerationAction action) : this(toleranceArchive, action)
        {
            this.LiveDataClosingCostArchive = liveDataArchive;
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="TemporaryArchive"/> class from being created.
        /// Intended for use only with the serializer.
        /// </summary>
        private TemporaryArchive()
        {
        }

        /// <summary>
        /// Represents an action to be taken after documents are generated
        /// from a temporary archive.
        /// </summary>
        public enum PostDocumentGenerationAction
        {
            /// <summary>
            /// Do nothing.
            /// </summary>
            None = 0,

            /// <summary>
            /// Mark the pending loan estimate archive as invalid.
            /// </summary>
            MarkPendingLoanEstimateInvalid = 1,

            /// <summary>
            /// Mark the pending loan estimate archive as disclosed.
            /// </summary>
            MarkPendingLoanEstimateIncludedInClosingDisclosure = 2
        }

        /// <summary>
        /// Gets the archive containing tolerance data.
        /// </summary>
        /// <value>The archive containing tolerance data.</value>
        /// <remarks>
        /// For TRID loans not using TRID 2.0 regulations, this archive
        /// should be used for the document generation process. For loans
        /// post TRID 2.0, this archive is used only for tolerance calculations.
        /// </remarks>
        public ClosingCostArchive ToleranceDataClosingCostArchive { get; private set; }

        /// <summary>
        /// Gets the archive containing live loan data.
        /// </summary>
        /// <value>The archive containing live loan data.</value>
        /// <remarks>
        /// This archive will exist only for post TRID 2.0 loans.
        /// </remarks>
        public ClosingCostArchive LiveDataClosingCostArchive { get; private set; }

        /// <summary>
        /// Gets the action to take after documents are generated with this archive.
        /// </summary>
        /// <value>The action to take after documents are generated with this archive.</value>
        public PostDocumentGenerationAction Action { get; private set; }

        /// <summary>
        /// Gets or sets the user id creating the temporary archive.
        /// </summary>
        public Guid? UserId { get; set; }

        /// <summary>
        /// Returns null.
        /// </summary>
        /// <returns>Always returns null.</returns>
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        /// <summary>
        /// Deserializes the instance from the given reader.
        /// </summary>
        /// <param name="reader">The xml reader.</param>
        public void ReadXml(XmlReader reader)
        {
            this.Action = (PostDocumentGenerationAction)Enum.Parse(
                typeof(PostDocumentGenerationAction),
                reader.GetAttribute("Action"));

            var userId = reader.GetAttribute("UserId");

            if (!string.IsNullOrEmpty(userId))
            {
                this.UserId = Guid.Parse(userId);
            }

            reader.Read();

            var archiveSerializer = new XmlSerializer(typeof(ClosingCostArchive));
            this.ToleranceDataClosingCostArchive = (ClosingCostArchive)archiveSerializer.Deserialize(reader);

            if (string.Equals(nameof(ClosingCostArchive), reader.LocalName, StringComparison.OrdinalIgnoreCase))
            {
                this.LiveDataClosingCostArchive = (ClosingCostArchive)archiveSerializer.Deserialize(reader);
            }

            reader.Read();
        }

        /// <summary>
        /// Serializes the instance to the given writer.
        /// </summary>
        /// <param name="writer">The xml writer.</param>
        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("Action", this.Action.ToString());
            writer.WriteAttributeString("UserId", this.UserId.ToString());

            var archiveSerializer = new XmlSerializer(typeof(ClosingCostArchive));

            this.ToleranceDataClosingCostArchive.StoreFieldsInline = true;
            archiveSerializer.Serialize(writer, this.ToleranceDataClosingCostArchive);

            if (this.LiveDataClosingCostArchive != null)
            {
                this.LiveDataClosingCostArchive.StoreFieldsInline = true;
                archiveSerializer.Serialize(writer, this.LiveDataClosingCostArchive);
            }
        }
    }
}
