﻿//-----------------------------------------------------------------------
// <summary>
//      Closing Cost Archives.
// </summary>
// <copyright file="ClosingCostArchive.cs" company="MeridianLink">
//      Copyright (c) MeridianLink. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LendersOffice.Common.SerializationTypes
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Serialization;
    using DataAccess;
    using HttpModule;

    /// <summary>
    /// Archive of 2015 version closing cost fees.
    /// </summary>
    public class ClosingCostArchive : IXmlSerializable, IClosingCostArchive
    {
        /// <summary>
        /// Dictionary of cache invalidating methods by field name.  DO NOT UPDATE THIS AT RUNTIME.  <para></para>
        /// When we finish the migration to .net45 we can use ReadOnlyDictionary from System.Object.Model.
        /// </summary>
        private static readonly Dictionary<string, Action<ClosingCostArchive>> CacheInvalidatorByFieldName = new Dictionary<string, Action<ClosingCostArchive>>
        {
            { "sAgentXmlContent", (cca) => cca.cachedAgentDataSet = null },
            { nameof(CPageBase.sApr), (cca) => cca.apr = null }
        };

        /// <summary>
        /// A los convert for type conversion.
        /// </summary>
        private LosConvert converter = new LosConvert();

        /// <summary>
        /// Indicates whether the content stored in FileDB has been updated.
        /// </summary>
        private bool fileDBContentDirty;

        /// <summary>
        /// Dictionary of all fields needed by this archive type.
        /// </summary>
        private Dictionary<string, string> internalFieldList = new Dictionary<string, string>();

        /// <summary>
        /// A cache of the agent data set.
        /// </summary>
        private DataSet cachedAgentDataSet = null;

        /// <summary>
        /// The APR associated with this archive.
        /// </summary>
        private decimal? apr;

        /// <summary>
        /// Initializes a new instance of the ClosingCostArchive class.
        /// </summary>
        /// <param name="dateArchived">Date of the archive.</param>
        /// <param name="archiveType">Type of archive.</param>
        /// <param name="status">The status of the archive.</param>
        /// <param name="source">The source of the archive.</param>
        public ClosingCostArchive(
            string dateArchived,
            E_ClosingCostArchiveType archiveType,
            E_ClosingCostArchiveStatus status,
            E_ClosingCostArchiveSource source) : this()
        {
            this.DateArchived = dateArchived;
            this.ClosingCostArchiveType = archiveType;
            this.Status = status;
            this.Source = source;
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="ClosingCostArchive" /> class from being created.
        /// </summary>
        private ClosingCostArchive()
        {
            this.Id = Guid.NewGuid();
            this.IsForCoCProcess = false;
            this.FileDBKey = this.GenerateFileDBKey();
        }

        /// <summary>
        /// Represents a type of archive.
        /// </summary>
        public enum E_ClosingCostArchiveType
        {
            /// <summary>
            /// Represents a GFE2010.
            /// </summary>
            Gfe2010 = 0,

            /// <summary>
            /// Represents a GFE2015.
            /// </summary>
            Gfe2015 = 1,

            /// <summary>
            /// Represents a LoanEstimate.
            /// </summary>
            LoanEstimate = 2,

            /// <summary>
            /// Represents a ClosingDisclosure.
            /// </summary>
            ClosingDisclosure = 3
        }

        /// <summary>
        /// Represents the disclosure type of the archive.
        /// </summary>
        public enum E_ClosingCostArchiveStatus
        {
            /// <summary>
            /// We do not know what the user did with the archive.
            /// </summary>
            Unknown = 0,

            /// <summary>
            /// The archive will not be disclosed.
            /// </summary>
            Invalid = 1,

            /// <summary>
            /// The archive is intended to be disclosed or was disclosed.
            /// </summary>
            Disclosed = 2,

            /// <summary>
            /// Documents have yet to be generated from this archive.
            /// </summary>
            PendingDocumentGeneration = 3,

            /// <summary>
            /// Archive is associated with a valid CoC that was not directly disclosed.
            /// </summary>
            SupersededCoC = 4,

            /// <summary>
            /// The archive was disclosed as part of a Closing Disclosure. This status
            /// only applies to Loan Estimate Archives.
            /// </summary>
            IncludedInClosingDisclosure = 5
        }

        /// <summary>
        /// The source of the archive.
        /// </summary>
        public enum E_ClosingCostArchiveSource
        {
            /// <summary>
            /// We don't know where this archive came from.
            /// </summary>
            Unknown = 0,

            /// <summary>
            /// The archive was created manually by a user.
            /// </summary>
            Manual = 1,

            /// <summary>
            /// The archive was created as part of the document generation process.
            /// </summary>
            DocumentGeneration = 2,

            /// <summary>
            /// The archive was created for a change of circumstance.
            /// </summary>
            ChangeOfCircumstance = 3,

            /// <summary>
            /// The archive was created because the GFE initial disclosure date was set.
            /// </summary>
            GfeInitialDisclosureDateSet = 4,

            /// <summary>
            /// The archive was created as part of the migration that moved files onto the TRID data layer.
            /// </summary>
            Migration = 5,

            /// <summary>
            /// The archive was created as part of some external process.
            /// </summary>
            External  = 6
        }

        /// <summary>
        /// Gets or sets the date this archive was archived.
        /// </summary>
        /// <value>Date of this archive.</value>
        public string DateArchived { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the archive is being used temporarily by the CoC process.
        /// </summary>
        /// <value>True if the archive will be discarded.</value>
        public bool IsForCoCProcess { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the field/value content should be serialized inline.
        /// </summary>
        /// <value>
        /// True if the field/value content should be stored inline and NOT in FileDB.
       ///  False if the field/value content should be stored in FileDB.
        /// </value>
        public bool StoreFieldsInline { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this archive was a tolerance basis archive at one point.
        /// </summary>
        /// <value>Value indicating whether this archive was a tolerance basis archive.</value>
        public bool WasBasisArchive { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this archive uses live data
        /// at the time of creation and should not be refreshed during events such
        /// as document generation or change of circumstance submissions.
        /// </summary>
        /// <value>
        /// True if the archive uses live data, false otherwise.
        /// </value>
        public bool UsesLiveData { get; set; }

        /// <summary>
        /// Gets or sets the type of the archive.
        /// </summary>
        /// <value>The type of the archive.</value>
        public E_ClosingCostArchiveType ClosingCostArchiveType { get; set; }

        /// <summary>
        /// Gets or sets the status of the archive.
        /// </summary>
        /// <value>
        /// The status of the archive. All non-Loan Estimate archives default to
        /// Disclosed.
        /// </value>
        public E_ClosingCostArchiveStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the source of the archive. 
        /// </summary>
        /// <value>The source of the archive.</value>
        public E_ClosingCostArchiveSource Source { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the archive was created
        /// from an archive in the Pending CoC archive as part of document
        /// generation.
        /// </summary>
        /// <value>True if the archive was sourced from a pending CoC archive. Otherwise, false.</value>
        public bool WasSourcedFromPendingCoC { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the APR should be retrieved from
        /// FileDB if it is not already available when the object is serialized.
        /// Does not apply to GFE archives, which will never have an APR.
        /// </summary>
        /// <value>True if the APR should be retrieved on serialize. Otherwise, false. Default is false.</value>
        public bool ForceAprRetrievalOnSerialize { get; set; }

        /// <summary>
        /// Gets the archive ID.
        /// </summary>
        /// <value>The archive ID.</value>
        public Guid Id { get; private set; }

        /// <summary>
        /// Gets a value indicating the file version of the FileDB content.
        /// </summary>
        /// <value>A value indicating the file version of the FileDB content.</value>
        public int FileDBContentVersion { get; private set; }

        /// <summary>
        /// Gets the FileDB key for this archive's field/value content.<para/>
        /// Note that after deserialization this may give a value for the intended FileDB key
        /// even if there's no entry in the FileDB.
        /// </summary>
        /// <value>The value of the key associated with the archive, or what it would be when written to the fileDB.</value>
        public string FileDBKey { get; private set; }

        /// <summary>
        /// Gets or sets the ID of a closing cost archive linked to this archive.
        /// </summary>
        /// <value>
        /// The linked archive ID or null if no link exists.
        /// </value>
        /// <remarks>
        /// This ID is intended primarily for use with TRID 2.0 archives. 
        /// For loans using TRID 2.0, this ID will link the live data 
        /// archive to the tolerance archive and vice versa.
        /// </remarks>
        public Guid? LinkedArchiveId { get; set; }

        /// <summary>
        /// Gets a value indicating whether this archive should be considered disclosed.
        /// </summary>
        /// <value>True if the archive is disclosed. Otherwise, false.</value>
        public bool IsDisclosed
        {
            get
            {
                return IsDisclosedStatus(this.ClosingCostArchiveType, this.Status);
            }
        }

        /// <summary>
        /// Gets the APR associated with the closing cost archive.
        /// </summary>
        /// <value>The APR associated with the closing cost archive. Null if it does not exist.</value>
        public decimal? Apr
        {
            get
            {
                if (!this.IsTridArchive)
                {
                    return null;
                }

                // Note: If we expect the archive to have an APR but it does not,
                // then this will keep checking. May want to look into using a 
                // sentinel value.
                if (this.apr == null && this.HasValue(nameof(CPageBase.sApr)))
                {
                    this.apr = this.GetRateValue(nameof(CPageBase.sApr));
                }

                return this.apr;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the archive is of a TRID type (LE/CD).
        /// </summary>
        /// <value>True if the archive is for a LE/CD. Otherwise, false.</value>
        public bool IsTridArchive
        {
            get
            {
                return this.ClosingCostArchiveType == E_ClosingCostArchiveType.LoanEstimate
                    || this.ClosingCostArchiveType == E_ClosingCostArchiveType.ClosingDisclosure;
            }
        }

        /// <summary>
        /// Gets a dictionary of all fields needed by this archive type.
        /// </summary>
        /// <remarks>
        /// If the field/value data is stored in FileDB, this will provide lazy loading.
        /// </remarks>
        /// <value>A dictionary of all fields needed by this archive type.</value>
        private Dictionary<string, string> FieldList
        {
            get
            {
                if (this.internalFieldList == null)
                {
                    this.LoadFieldValueContentFromFileDB();
                }

                return this.internalFieldList;
            }
        }

        /// <summary>
        /// Gets or sets the date the closing cost list.
        /// </summary>
        /// <value>Closing cost list.</value>
        private string ClosingCostJson_legacy { get; set; }

        /// <summary>
        /// Gets or sets the seller responsible closing cost list.
        /// </summary>
        /// <value>Closing cost list.</value>
        private string SellerResponsibleClosingCostJson_legacy { get; set; }

        /// <summary>
        /// Gets or sets Total loan amount at time of archive.
        /// </summary>
        /// <value>Total loan amount at time of archive.</value>
        private decimal LoanAmount_legacy { get; set; }

        /// <summary>
        /// Gets or sets Purchase Price at time of archive.
        /// </summary>
        /// <value>Purchase Price at time of archive.</value>
        private decimal PurchasePrice_legacy { get; set; }

        /// <summary>
        /// Gets or sets Appraisal Value at time of archive.
        /// </summary>
        /// <value>Appraisal value at time of archive.</value>
        private decimal AppraisalValue_legacy { get; set; }

        /// <summary>
        /// Gets or sets Total Loan Amount at time of archive.
        /// </summary>
        /// <value>Total Loan Amount at time of archive.</value>
        private decimal TotalLoanAmount_legacy { get; set; }

        /// <summary>
        /// Gets a value indicating whether the status is considered disclosed for the archive type.
        /// </summary>
        /// <param name="type">The type of the archive.</param>
        /// <param name="status">The status of the archive.</param>
        /// <returns>True if the archive should be considered disclosed. Otherwise, false.</returns>
        public static bool IsDisclosedStatus(E_ClosingCostArchiveType type, E_ClosingCostArchiveStatus status)
        {
            return status == E_ClosingCostArchiveStatus.Disclosed
                || (type == E_ClosingCostArchiveType.LoanEstimate 
                    && status == E_ClosingCostArchiveStatus.IncludedInClosingDisclosure);
        }

        /// <summary>
        /// Gets a value indicating whether the specified archive is considered an archive
        /// with disclosed data.
        /// </summary>
        /// <param name="regulationVersion">
        /// The regulation version for the loan file associated with the archive.
        /// </param>
        /// <param name="archive">
        /// The archive to check.
        /// </param>
        /// <returns>
        /// True if the archive is a disclosed archive, false otherwise.
        /// </returns>
        public static bool IsDisclosedDataArchive(LendersOffice.ObjLib.TRID2.TridTargetRegulationVersionT regulationVersion, ClosingCostArchive archive)
        {
            if (Is2015TridArchive(regulationVersion, archive))
            {
                return true;
            }

            // For TRID 2017 loan files, the archive using live data
            // will be disclosed to the document vendor.
            return archive.UsesLiveData;
        }

        /// <summary>
        /// Gets a value indicating whether the specified archive is considered an archive
        /// with tolerance data.
        /// </summary>
        /// <param name="regulationVersion">
        /// The regulation version for the loan file associated with the archive.
        /// </param>
        /// <param name="archive">
        /// The archive to check.
        /// </param>
        /// <returns>
        /// True if the archive is a tolerance archive, false otherwise.
        /// </returns>
        public static bool IsToleranceDataArchive(LendersOffice.ObjLib.TRID2.TridTargetRegulationVersionT regulationVersion, ClosingCostArchive archive)
        {
            if (Is2015TridArchive(regulationVersion, archive))
            {
                return true;
            }

            // For TRID 2017 loan files, the archive not using
            // live data is the tolerance archive.
            return !archive.UsesLiveData;
        }

        /// <summary>
        /// Get list of properties for this archive type.
        /// </summary>
        /// <param name="archiveType">Type of archive.</param>
        /// <returns>Field names for all properties accessed by this archive type.</returns>
        public static IEnumerable<string> GetLoanPropertyList(E_ClosingCostArchiveType archiveType)
        {
            switch (archiveType)
            {
                case E_ClosingCostArchiveType.Gfe2015:
                case E_ClosingCostArchiveType.LoanEstimate:
                case E_ClosingCostArchiveType.ClosingDisclosure:
                    return GetLoanPropertyData(null, archiveType).Select(p => p.FieldId);
                case E_ClosingCostArchiveType.Gfe2010: // Not support.
                default:
                    throw new UnhandledEnumException(archiveType);
            }
        }

        /// <summary>
        /// Generates a string of fields.
        /// </summary>
        public static void PrintPropUsedFields()
        {
            HashSet<string> closingDisclosureFields;
            HashSet<string> gfe2015Fields;
            HashSet<string> loanEstimate;

            HashSet<LoanFileFieldData> temp = new HashSet<LoanFileFieldData>();
            GetCommonFieldDataForArchive(temp, null);

            closingDisclosureFields = new HashSet<string>(temp.Select(p => p.FieldId));
            gfe2015Fields = new HashSet<string>(temp.Select(p => p.FieldId));
            loanEstimate = new HashSet<string>(temp.Select(p => p.FieldId));

            temp.Clear();
            GetFieldDataForOnlyClosingDisclosure(temp, null);

            foreach (var item in temp)
            {
                closingDisclosureFields.Add(item.FieldId);
            }

            temp.Clear();
            GetFieldDataForGfe2015(temp, null);

            foreach (var item in temp)
            {
                gfe2015Fields.Add(item.FieldId);
            }

            temp.Clear();

            GetFieldDataForLoanEstimateAndClosingDisclosure(temp, null);

            foreach (var item in temp)
            {
                loanEstimate.Add(item.FieldId);
                closingDisclosureFields.Add(item.FieldId);
            }

            HashSet<string> allFields = new HashSet<string>(closingDisclosureFields);

            foreach (string field in gfe2015Fields)
            {
                allFields.Add(field);
            }

            foreach (string field in loanEstimate)
            {
                allFields.Add(field);
            }

            CFieldInfoTable fieldInfoTable = CFieldInfoTable.GetInstance();
            
            StringBuilder sb = new StringBuilder();
            using (PerformanceMonitorItem.Time("ClosingCostArchive PrintPropUsedFields call to GatherRequiredIndepFields with loop count = " + allFields.Count))
            {
                foreach (string field in allFields)
                {
                    DependResult res = fieldInfoTable.GatherRequiredIndepFields(new string[] { field });
                    bool shouldRecalc = res.DbFields.Has("sClosingCostSetJsonContent");

                    sb.AppendFormat("{0}\t{1}\t{2}\t{3}\t{4}\n", field, closingDisclosureFields.Contains(field) ? "yes" : "no", gfe2015Fields.Contains(field) ? "yes" : "no", loanEstimate.Contains(field) ? "yes" : "no", shouldRecalc ? "yes" : "no");
                }
            }

            Tools.LogInfo(sb.ToString());
        }

        /// <summary>
        /// Get list of fields used by the archive.
        /// </summary>
        /// <param name="archiveType">Type of archive.</param>
        /// <returns>Fields used by the archive.</returns>
        public static IEnumerable<string> GetCalculatedLoanPropertyList(E_ClosingCostArchiveType archiveType)
        {
            return GetLoanPropertyData(null, archiveType).Select(p => p.FieldId);
        }

        /// <summary>
        /// Per MSDN: This method is reserved and should not be used.  When implementing the IXmlSerializable interface, you should return null.
        /// </summary>
        /// <returns>Null.  Always.</returns>
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        /// <summary>
        /// Returns a closing cost set from the current archive.  Each one is a new copy.
        /// </summary>
        /// <param name="converter">The converter to use.</param>
        /// <returns>A closing cost set for the current archive.</returns>
        public BorrowerClosingCostSet GetClosingCostSet(LosConvert converter = null)
        {
            if (converter != null)
            {
                return new BorrowerClosingCostSet(this, converter);
            }

            return new BorrowerClosingCostSet(this);
        }

        /// <summary>
        /// Gets the agent data set and caches it.
        /// </summary>
        /// <returns>A data set representing the agents.</returns>
        public DataSet GetCachedAgentDataSet()
        {
            if (this.cachedAgentDataSet == null)
            {
                this.cachedAgentDataSet = CPageBase.GetDataSet(E_DataSetT.AgentXml, this.GetValue("sAgentXmlContent"));
            }

            return this.cachedAgentDataSet;
        }

        /// <summary>
        /// Generates an object from its XML representation.
        /// </summary>
        /// <param name="reader">Xml reader to read content from.</param>
        public void ReadXml(XmlReader reader)
        {
            reader.Read();
            if (reader.MoveToContent() == XmlNodeType.Element && reader.LocalName == "CCarchive")
            {
                // TODO: After migration, remove if and just allow to fail on blank id.
                if (!string.IsNullOrEmpty(reader["Id"]))
                {
                    this.Id = new Guid(reader["Id"]);
                }
                
                this.DateArchived = reader["DateArchived"];
                this.WasBasisArchive = reader["WasBasisArchive"] == "True" ? true : false;
                this.ClosingCostArchiveType = (E_ClosingCostArchiveType)this.converter.ToCount(reader["ClosingCostArchiveType"]);
                this.Status = this.TryGetStatusFromAttribute(reader);
                this.Source = this.TryGetSourceFromAttribute(reader);
                this.FileDBKey = reader["FileDBKey"] == null ? this.GenerateFileDBKey() : reader["FileDBKey"];
                this.FileDBContentVersion = reader["FileDBContentVersion"] == null ? 0 : int.Parse(reader["FileDBContentVersion"]);
                this.WasSourcedFromPendingCoC = reader["WasSourcedFromPendingCoC"] == "True" ? true : false;
                this.StoreFieldsInline = reader["StoreFieldsInline"] == bool.TrueString;
                this.apr = string.IsNullOrEmpty(reader["Apr"]) ? null : (decimal?)decimal.Parse(reader["Apr"]);
                this.LinkedArchiveId = reader["LinkedArchiveId"] == null ? default(Guid?) : new Guid(reader["LinkedArchiveId"]);
                this.UsesLiveData = reader["UsesLiveData"] == bool.TrueString;

                // For legacy support.  Had the big 4 as attributes, 
                // but that cause issue.  Only old archives have these.
                this.ReadLegacyFields(reader);

                reader.Read();
                this.ReadLegacyFieldValueContentIfPresent(reader);
                this.ReadThroughEndOfClosingCostArchiveElement(reader);
            }

            if (!this.FieldList.Any())
            {
                this.LazyLoadFieldListFromFileDB();
            }
            else
            {
                this.ProcessLegacyFieldsForRead();

                // If we read the field values from XML, we need to mark the
                // content as dirty so it will be flushed to FileDB on save.
                this.fileDBContentDirty = true;
            }
        }

        /// <summary>
        /// Converts an object into its XML representation.
        /// </summary>
        /// <param name="writer">Xml writer to receive content.</param>
        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("CCarchive");
            writer.WriteAttributeString("Id", this.Id.ToString());
            writer.WriteAttributeString("DateArchived", this.DateArchived);
            writer.WriteAttributeString("ClosingCostArchiveType", this.converter.ToCountString((int)this.ClosingCostArchiveType));
            writer.WriteAttributeString("WasBasisArchive", this.WasBasisArchive ? "True" : "False");
            writer.WriteAttributeString("FileDBKey", this.FileDBKey);
            writer.WriteAttributeString("Source", this.Source.ToString("D"));
            writer.WriteAttributeString("WasSourcedFromPendingCoC", this.WasSourcedFromPendingCoC.ToString());
            writer.WriteAttributeString("UsesLiveData", this.UsesLiveData.ToString());

            if (this.LinkedArchiveId.HasValue)
            {
                writer.WriteAttributeString("LinkedArchiveId", this.LinkedArchiveId.ToString());
            }

            // We only want to add the status to loan estimate and closing disclosure
            // archives for now. We currently do not provide any status-handling
            // functionality for the GFE.
            if (this.ClosingCostArchiveType == E_ClosingCostArchiveType.LoanEstimate ||
                this.ClosingCostArchiveType == E_ClosingCostArchiveType.ClosingDisclosure)
            {
                writer.WriteAttributeString("Status", this.Status.ToString("D"));
            }

            // The apr member will be cleared any time the field/value content is
            // modified. Modifying archive content will require that the field/value
            // content is loaded, so we can pull the APR from the loaded field/value
            // pairs.
            // If we haven't modified the archive content but our apr member still
            // has a value, then we can serialize that -- it will not be stale due
            // to the cache invalidation.
            // Note: We could update this to force a migration on file save. This may
            // be worth looking into.
            if (this.IsTridArchive
                && (this.internalFieldList != null || this.apr.HasValue || this.ForceAprRetrievalOnSerialize))
            {
                writer.WriteAttributeString("Apr", this.Apr?.ToString());
            }

            if (this.fileDBContentDirty)
            {
                ++this.FileDBContentVersion;
            }

            // Don't write out the FileDB content version for temporary archives so it will be the default
            // on deserialization.
            if (!this.StoreFieldsInline)
            {
                writer.WriteAttributeString("FileDBContentVersion", this.FileDBContentVersion.ToString());
            }
            else
            {
                writer.WriteAttributeString("StoreFieldsInline", bool.TrueString);
                this.WriteFieldValueContent(writer);
            }

            writer.WriteEndElement(); // CcArchive
        }

        /// <summary>
        /// Flush's the archive's field/value content to FileDB.
        /// </summary>
        public void FlushToFileDBIfDirty()
        {
            if (this.StoreFieldsInline)
            {
                throw new InvalidOperationException("Should not flush temp archive to FileDB.");
            }

            if (this.fileDBContentDirty && this.internalFieldList != null)
            {
                FileDBTools.WriteJsonNetObject(
                    E_FileDB.Normal,
                    this.FileDBKey,
                    this.internalFieldList);
            }

            this.fileDBContentDirty = false;
        }

        /// <summary>
        /// Populate this archive from this loan file.
        /// </summary>
        /// <param name="dataLoan">The loan to use.</param>
        public void ExtractFromLoan(CPageData dataLoan)
        {
            var props = GetLoanPropertyData(dataLoan, this.ClosingCostArchiveType);

            foreach (var prop in props)
            {
                this.AddValue(prop.FieldId, prop.Getter.Invoke());
            }

            this.InvalidateCachedFields();
        }

        /// <summary>
        /// Populate this archive from this loan file.
        /// </summary>
        /// <param name="loanId">The loan to use.</param>
        public void ExtractFromLoan(Guid loanId)
        {
            var fields = GetLoanPropertyList(this.ClosingCostArchiveType);
            CPageData dataLoan = new CPageData(loanId, fields);
            dataLoan.InitLoad();

            this.ExtractFromLoan(dataLoan);
        }

        /// <summary>
        /// Write an existing value with new one.
        /// </summary>
        /// <param name="fieldId">Field to overwrite.</param>
        /// <param name="newValue">Value to store.</param>
        public void OverrideValue(string fieldId, string newValue)
        {
            this.FieldList[fieldId] = newValue;

            this.InvalidateCachedField(fieldId);
            this.fileDBContentDirty = true;
        }

        /// <summary>
        /// Remove the given field from the archive to allow it to update again.
        /// </summary>
        /// <param name="fieldId">The field id to remove.</param>
        public void RemoveValue(string fieldId)
        {
            if (this.FieldList.ContainsKey(fieldId))
            {
                this.FieldList.Remove(fieldId);
                this.InvalidateCachedField(fieldId);
                this.fileDBContentDirty = true;
            }
        }

        /// <summary>
        /// Allows us to overwrite the ID for the current archive. 
        /// </summary>
        /// <param name="id">The new ID to use in the closing archive.</param>
        public void SetId(Guid id)
        {
            this.Id = id;
        }

        /// <summary>
        /// Check if this archive has a value.
        /// </summary>
        /// <param name="fieldId">Field to overwrite.</param>
        /// <returns>True if value is known, otherwise false.</returns>
        public bool HasValue(string fieldId)
        {
            if (this.FieldList.ContainsKey(fieldId))
            {
                return true;
            }
            else
            {
                // Datalayer is still being added to, but
                // we will want to be loud here.  It means
                // something we think we should have is not here.
                return false;
            }
        }

        /// <summary>
        /// Get string value from archive.
        /// </summary>
        /// <param name="fieldId">Field to retrieve.</param>
        /// <returns>Field value from this archive.</returns>
        public string GetValue(string fieldId)
        {
            return this.GetRawValue(fieldId);
        }

        /// <summary>
        /// Get guid value from archive.
        /// </summary>
        /// <param name="fieldId">Field to retrieve.</param>
        /// <returns>Field value from this archive.</returns>
        public Guid GetGuidValue(string fieldId)
        {
            return new Guid(this.GetRawValue(fieldId));
        }

        /// <summary>
        /// Get boolean value from archive.
        /// </summary>
        /// <param name="fieldId">Field to retrieve.</param>
        /// <returns>Field value from this archive.</returns>
        public bool GetBitValue(string fieldId)
        {
            return this.converter.ToBit(this.GetRawValue(fieldId));
        }

        /// <summary>
        /// Get boolean value from archive.
        /// </summary>
        /// <param name="fieldId">Field to retrieve.</param>
        /// <param name="defaultValue">
        /// Value to return if archive does not contain field.
        /// </param>
        /// <returns>
        /// If the archive contains the field, returns the value from the
        /// archive. Otherwise, returns value specified by defaultValue.
        /// </returns>
        public bool GetBitValue(string fieldId, bool defaultValue)
        {
            if (this.HasValue(fieldId))
            {
                return this.GetBitValue(fieldId);
            }
            else
            {
                return defaultValue;
            }
        }

        /// <summary>
        /// Get integer value from archive.
        /// </summary>
        /// <param name="fieldId">Field to retrieve.</param>
        /// <returns>Field value from this archive.</returns>
        public int GetCountValue(string fieldId)
        {
            return this.converter.ToCount(this.GetRawValue(fieldId));
        }

        /// <summary>
        /// Get money value from archive.
        /// </summary>
        /// <param name="fieldId">Field to retrieve.</param>
        /// <returns>Field value from this archive.</returns>
        public decimal GetMoneyValue(string fieldId)
        {
            return this.converter.ToMoney(this.GetRawValue(fieldId));
        }

        /// <summary>
        /// Get rate value from archive.
        /// </summary>
        /// <param name="fieldId">Field to retrieve.</param>
        /// <returns>Field value from this archive.</returns>
        public decimal GetRateValue(string fieldId)
        {
            return this.converter.ToRate(this.GetRawValue(fieldId));
        }

        /// <summary>
        /// Get CDateTime value from archive.
        /// </summary>
        /// <param name="fieldId">Field to retrieve.</param>
        /// <returns>Field value from this archive.</returns>
        public CDateTime GetCDateTimeValue(string fieldId)
        {
            try
            {
                return CDateTime.Create(Convert.ToDateTime(this.GetRawValue(fieldId)));
            }
            catch (FormatException)
            {
                return CDateTime.InvalidWrapValue;
            }
        }

        /// <summary>
        /// Get DateTime value from archive.
        /// </summary>
        /// <param name="fieldId">Field to retrieve.</param>
        /// <returns>Field value from this archive.</returns>
        public DateTime GetDateTimeValue(string fieldId)
        {
            try
            {
                return Convert.ToDateTime(this.GetRawValue(fieldId));
            }
            catch (FormatException)
            {
                return DateTime.MinValue;
            }
        }

        /// <summary>
        /// Duplicate this closing cost archive, and give it a new ID.
        /// </summary>
        /// <returns>A duplicate of this closing cost archive.</returns>
        public ClosingCostArchive Duplicate()
        {
            ClosingCostArchive dupe = new ClosingCostArchive()
            {
                DateArchived = this.DateArchived,
                ClosingCostArchiveType = this.ClosingCostArchiveType,
                Status = this.Status,
                WasBasisArchive = this.WasBasisArchive,
                internalFieldList = new Dictionary<string, string>(this.FieldList),
                fileDBContentDirty = true,
                Source = this.Source,
                UsesLiveData = this.UsesLiveData
            };

            return dupe;
        }

        /// <summary>
        /// Copies the latest values from the loan data that are marked as always update in CoC mode.
        /// </summary>
        /// <param name="dataLoan">The loan data object containing the latest.</param>
        public void UpdateArchiveWithLatestLoanDataForCoC(CPageData dataLoan)
        {
            HashSet<LoanFileFieldData> props = ClosingCostArchive.GetLoanPropertyData(dataLoan, this.ClosingCostArchiveType);

            foreach (LoanFileFieldData prop in props)
            {
                if (prop.UpdateValueOnCoC)
                {
                    this.OverrideValue(prop.FieldId, prop.Getter.Invoke());
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the regulation version of the loan
        /// associated with the archive is TRID 2015 or the archive was added 
        /// to a TRID 2015 loan that is now targeting TRID 2017.
        /// </summary>
        /// <param name="regulationVersion">
        /// The regulation version for the loan file associated with the archive.
        /// </param>
        /// <param name="archive">
        /// The archive to check.
        /// </param>
        /// <returns>
        /// True if the loan is using TRID 2015 disclosures or the archive was added
        /// to a TRID 2015 loan, false otherwise.
        /// </returns>
        private static bool Is2015TridArchive(LendersOffice.ObjLib.TRID2.TridTargetRegulationVersionT regulationVersion, ClosingCostArchive archive)
        {
            if (!archive.IsTridArchive)
            {
                return false;
            }

            if (archive.ClosingCostArchiveType == E_ClosingCostArchiveType.ClosingDisclosure)
            {
                // Closing Disclosure archives were not updated in the 2017 regulations.
                return true;
            }

            if (regulationVersion != LendersOffice.ObjLib.TRID2.TridTargetRegulationVersionT.TRID2017)
            {
                // The loan is not targeting the new TRID 2017 regulations or was rolled
                // back from TRID 2017 regulations. In this case, we want the archive without
                // a linked archive ID (created using 2015 TRID) that is not using live data, 
                // as this archive contains the estimated value dataset required for TRID 2015
                // disclosures.
                return !archive.LinkedArchiveId.HasValue && !archive.UsesLiveData;
            }

            if (!archive.LinkedArchiveId.HasValue)
            {
                // Archive was created prior to the loan being migrated to TRID 2017.
                return true;
            }

            return false;
        }

        /// <summary>
        /// Uses the DateTime's ToString to output the string version. Will also output the time as well.
        /// </summary>
        /// <param name="dateTime">The DateTime to convert.</param>
        /// <returns>The date in the specified string format.</returns>
        private static string ToDateAndTimeString(DateTime dateTime)
        {
            return dateTime.ToString("MM/dd/yyyy hh:mm:ss tt");
        }

        /// <summary>
        /// Gets the loan property data for this type.
        /// </summary>
        /// <param name="dataLoan">Loan to get property from.</param>
        /// <param name="type">Type of archive.</param>
        /// <returns>The requested loan properties.</returns>
        private static HashSet<LoanFileFieldData> GetLoanPropertyData(CPageData dataLoan, E_ClosingCostArchiveType type)
        {
            if (dataLoan == null)
            {
                dataLoan = new CPageData(Guid.Empty);
            }

            HashSet<LoanFileFieldData> props = new HashSet<LoanFileFieldData>();

            GetCommonFieldDataForArchive(props, dataLoan);

            if (type == E_ClosingCostArchiveType.LoanEstimate)
            {
                GetFieldDataForLoanEstimateAndClosingDisclosure(props, dataLoan);
            }
            else if (type == E_ClosingCostArchiveType.ClosingDisclosure)
            {
                GetFieldDataForLoanEstimateAndClosingDisclosure(props, dataLoan);
                GetFieldDataForOnlyClosingDisclosure(props, dataLoan);
            }
            else if (type == E_ClosingCostArchiveType.Gfe2015)
            {
                GetFieldDataForGfe2015(props, dataLoan);
            }
            else
            {
                throw new UnhandledEnumException(type);
            }

            return props;
        }

        /// <summary>
        /// Get the data fields considered common to all archives.
        /// </summary>
        /// <param name="props">Property list to add to.</param>
        /// <param name="dataLoan">Loan file to be used.</param>
        private static void GetCommonFieldDataForArchive(HashSet<LoanFileFieldData> props, CPageData dataLoan)
        {
            props.Add(new LoanFileFieldData(
                "sLoanVersionT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sLoanVersionT),
                true));

            props.Add(new LoanFileFieldData(
                "sClosingCostSetJsonContent",
                () => dataLoan.sClosingCostSet.ToJson()));

            props.Add(new LoanFileFieldData(
                "sSellerResponsibleClosingCostSetJsonContent",
                () => dataLoan.sSellerResponsibleClosingCostSet.ToJson(),
                true));

            props.Add(new LoanFileFieldData(
                "sProrationListJson",
                () => SerializationHelper.JsonNetSerialize(dataLoan.sProrationList),
                true));

            props.Add(new LoanFileFieldData(
                "sAdjustmentListJson",
                () => SerializationHelper.JsonNetSerialize(dataLoan.sAdjustmentList),
                true));

            props.Add(new LoanFileFieldData(
                "sAssetsAsAdjustmentsJson",
                () => SerializationHelper.JsonNetSerialize(dataLoan.sAdjustmentList.AssetAdjustments)));

            props.Add(new LoanFileFieldData(
                "sDocMagicClosingD",
                () => dataLoan.sDocMagicClosingD_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sDocMagicClosingDLckd",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sDocMagicClosingDLckd),
                true));

            props.Add(new LoanFileFieldData(
                "sPropertyTransferD",
                () => dataLoan.sPropertyTransferD_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sPropertyTransferDLckd",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sPropertyTransferDLckd),
                true));

            props.Add(new LoanFileFieldData(
                "sBranchChannelT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sBranchChannelT)));

            props.Add(new LoanFileFieldData(
                "sTridNoteIR",
                () => dataLoan.sTridNoteIR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sNoteIR",
                () => dataLoan.sNoteIR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sIPiaDy",
                () => dataLoan.sIPiaDy_rep));

            props.Add(new LoanFileFieldData(
                "sIPerDay",
                () => dataLoan.sIPerDay_rep));

            props.Add(new LoanFileFieldData(
                "sConsummationD",
                () => dataLoan.sConsummationD_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sConsummationDLckd",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sConsummationDLckd),
                true));

            props.Add(new LoanFileFieldData(
                "sPurchPrice",
                () => dataLoan.sPurchPrice_rep));

            props.Add(new LoanFileFieldData(
                "sApprVal",
                () => dataLoan.sApprVal_rep));

            props.Add(new LoanFileFieldData(
                "sAltCost",
                () => dataLoan.sAltCost_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sLandCost",
                () => dataLoan.sLandCost_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sRefPdOffAmt1003",
                () => dataLoan.sRefPdOffAmt1003_rep));
            
            props.Add(new LoanFileFieldData(
                "sLotImprovC",
                () => dataLoan.sLotImprovC_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sTerm",
                () => dataLoan.sTerm_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sDue",
                () => dataLoan.sDue_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sFinMethT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sFinMethT),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeEstScAvailTillDLckd",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sGfeEstScAvailTillDLckd),
                true));

            props.Add(new LoanFileFieldData(
                "sIsPrintTimeForsGfeEstScAvailTillD",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sIsPrintTimeForsGfeEstScAvailTillD),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeEstScAvailTillDTimeZoneT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sGfeEstScAvailTillDTimeZoneT),
                true));

            props.Add(new LoanFileFieldData(
                "sPmtAdjRecastStop",
                () => dataLoan.sPmtAdjRecastStop_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sRAdjCapMon",
                () => dataLoan.sRAdjCapMon_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sRAdj1stCapMon",
                () => dataLoan.sRAdj1stCapMon_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeHavePpmtPenalty",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sGfeHavePpmtPenalty)));

            props.Add(new LoanFileFieldData(
                "sGfeMaxPpmtPenaltyAmt",
                () => dataLoan.sGfeMaxPpmtPenaltyAmt_rep));

            props.Add(new LoanFileFieldData(
                "sSchedDueD1",
                () => dataLoan.sSchedDueD1_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sSchedDueD1Lckd",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sSchedDueD1Lckd),
                true));

            props.Add(new LoanFileFieldData(
                "sEstCloseD",
                () => dataLoan.sEstCloseD_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sIOnlyMon",
                () => dataLoan.sIOnlyMon_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sOriginatorCompensationPaymentSourceT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sOriginatorCompensationPaymentSourceT),
                true));

            props.Add(new LoanFileFieldData(
                "sHasOriginatorCompensationPlan",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sHasOriginatorCompensationPlan),
                updateValueOnCoc: true));

            props.Add(new LoanFileFieldData(
                "sGfeIsTPOTransaction",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sGfeIsTPOTransaction),
                true));

            props.Add(new LoanFileFieldData(
                "sFinalLAmt",
             () => dataLoan.sFinalLAmt_rep));

            props.Add(new LoanFileFieldData(
                "sLAmtCalc",
            () => dataLoan.sLAmtCalc_rep));

            props.Add(new LoanFileFieldData(
                "sMipPia",
                () => dataLoan.sMipPia_rep));

            props.Add(new LoanFileFieldData(
                "sVaFf",
                () => dataLoan.sVaFf_rep));

            props.Add(new LoanFileFieldData(
                "sProHazInsR",
                () => dataLoan.sProHazInsR_rep, 
                true));

            props.Add(new LoanFileFieldData(
                "sProHazInsT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sProHazInsT),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeEstScAvailTillD",
                () => dataLoan.m_convertLos.ToDateTimeString(dataLoan.sGfeEstScAvailTillD),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeNoteIRAvailTillD",
                () => dataLoan.m_convertLos.ToDateTimeString(dataLoan.sGfeNoteIRAvailTillD),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeOriginatorCompF",
                () => dataLoan.sGfeOriginatorCompF_rep));

            props.Add(new LoanFileFieldData(
                "sProHazInsMb",
                () => dataLoan.sProHazInsMb_rep));

            props.Add(new LoanFileFieldData(
                "s1007RsrvMon",
                () => dataLoan.s1007RsrvMon_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sHazInsRsrv",
                () => dataLoan.sHazInsRsrv_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sAgentXmlContent",
                () => dataLoan.sAgentDataSet.GetXml(), // Might want to compress this one down.
                true));

            props.Add(new LoanFileFieldData(
                "sLT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sLT),
                true));

            props.Add(new LoanFileFieldData(
                "sProHazIns",
                () => dataLoan.sProHazIns_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sProMIns",
                () => dataLoan.sProMIns_rep));

            props.Add(new LoanFileFieldData(
                "sHazInsPiaMon",
                () => dataLoan.sHazInsPiaMon_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sProRealETxMb",
                () => dataLoan.sProRealETxMb_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sProSchoolTx",
                () => dataLoan.sProSchoolTx_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sProFloodIns",
                () => dataLoan.sProFloodIns_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sAggregateAdjRsrv",
                () => dataLoan.sAggregateAdjRsrv_rep));

            props.Add(new LoanFileFieldData(
                "s1006ProHExp",
                () => dataLoan.s1006ProHExp_rep,
                true));

            props.Add(new LoanFileFieldData(
                "s1007ProHExp",
                () => dataLoan.s1007ProHExp_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sHazInsRsrvMon",
                () => dataLoan.sHazInsRsrvMon_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sMInsRsrvMon",
                () => dataLoan.sMInsRsrvMon_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sRealETxRsrvMon",
                () => dataLoan.sRealETxRsrvMon_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sSchoolTxRsrvMon",
                () => dataLoan.sSchoolTxRsrvMon_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sFloodInsRsrvMon",
                () => dataLoan.sFloodInsRsrvMon_rep,
                true));

            props.Add(new LoanFileFieldData(
                "s1006RsrvMon",
                () => dataLoan.s1006RsrvMon_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sProRealETxR",
                () => dataLoan.sProRealETxR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sProRealETxT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sProRealETxT),
                true));

            props.Add(new LoanFileFieldData(
                "sLDiscnt",
                () => dataLoan.sLDiscnt_rep));

            props.Add(new LoanFileFieldData(
                "sHousingExpenseJsonContent",
                () => dataLoan.sHousingExpenses.ToJson()));

            props.Add(new LoanFileFieldData(
                "sLastDisclosedGFEArchiveId",
                () => dataLoan.sLastDisclosedGFEArchiveId_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sOpenedD",
                () => dataLoan.sOpenedD_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sIsManuallySetThirdPartyAffiliateProps",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sIsManuallySetThirdPartyAffiliateProps),
                true));

            props.Add(new LoanFileFieldData(
                "sEscrowImpoundsCalcMethodT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sEscrowImpoundsCalcMethodT),
                true));
            
            props.Add(new LoanFileFieldData(
                "sCustomaryEscrowImpoundsCalcMinT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sCustomaryEscrowImpoundsCalcMinT),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeIsBalloon",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sGfeIsBalloon)));

            props.Add(new LoanFileFieldData(
                "sGfeIsBalloonLckd",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sGfeIsBalloonLckd)));

            props.Add(new LoanFileFieldData(
                "sGfeBalloonPmt",
                () => dataLoan.sGfeBalloonPmt_rep));

            props.Add(new LoanFileFieldData(
                "sGfeCanPaymentIncrease",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sGfeCanPaymentIncrease)));

            props.Add(new LoanFileFieldData(
                "sGfeInitialImpoundDeposit",
                () => dataLoan.sGfeInitialImpoundDeposit_rep));

            props.Add(new LoanFileFieldData(
                "sAggEscrowCalcModeT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sAggEscrowCalcModeT),
                true));

            props.Add(new LoanFileFieldData(
                "sIsRefinancing",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sIsRefinancing),
                true));

            props.Add(new LoanFileFieldData(
                "sSpOrigC",
                () => dataLoan.sSpOrigC_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sIsConstructionLoan",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sIsConstructionLoan),
                true));

            props.Add(new LoanFileFieldData(
                "sLotOrigC",
                () => dataLoan.sLotOrigC_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sLPurposeT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sLPurposeT),
                true));

            props.Add(new LoanFileFieldData(
                "sDisclosureRegulationT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sDisclosureRegulationT),
                true));

            props.Add(new LoanFileFieldData(
                "sEstCloseDLckd",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sEstCloseDLckd),
                true));

            props.Add(new LoanFileFieldData(
                "sClosingDateMonth",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sClosingDateMonth),
                true));

            props.Add(new LoanFileFieldData(
                "sOCredit5Amt",
                () => dataLoan.sOCredit5Amt_rep));

            props.Add(new LoanFileFieldData(
                nameof(CPageData.sTridTargetRegulationVersionT),
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sTridTargetRegulationVersionT),
                updateValueOnCoc: true));

            props.Add(new LoanFileFieldData(
                nameof(CPageData.sGseTargetApplicationT),
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sGseTargetApplicationT),
                updateValueOnCoc: true));
        }

        /// <summary>
        /// Get the data fields considered to be used exclusively by 2015 GFE.
        /// These are not referenced by other Closing Cost types.
        /// </summary>
        /// <param name="props">Property list to add to.</param>
        /// <param name="dataLoan">Loan file to be used.</param>
        private static void GetFieldDataForGfe2015(HashSet<LoanFileFieldData> props, CPageData dataLoan)
        {
            props.Add(new LoanFileFieldData(
                "sGfeEstScAvailTillD_Time",
                () => dataLoan.sGfeEstScAvailTillD_Time,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeTotalFundByLender",
                () => dataLoan.sGfeTotalFundByLender_rep));

            props.Add(new LoanFileFieldData(
                "sGfeTotalFundBySeller",
                () => dataLoan.sGfeTotalFundBySeller_rep));

            props.Add(new LoanFileFieldData(
                "sTotEstPp1003",
                () => dataLoan.sTotEstPp1003_rep));

            props.Add(new LoanFileFieldData(
                "sTotEstCcNoDiscnt1003",
                () => dataLoan.sTotEstCcNoDiscnt1003_rep));

            props.Add(new LoanFileFieldData(
                "sFfUfmip1003",
                () => dataLoan.sFfUfmip1003_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sLDiscnt1003",
                () => dataLoan.sLDiscnt1003_rep));

            props.Add(new LoanFileFieldData(
                "sTotTransC",
                () => dataLoan.sTotTransC_rep));

            props.Add(new LoanFileFieldData(
                "sOCredit1Desc",
                () => dataLoan.sOCredit1Desc));

            props.Add(new LoanFileFieldData(
                "sOCredit2Desc",
                () => dataLoan.sOCredit2Desc));

            props.Add(new LoanFileFieldData(
                "sONewFinBal",
                () => dataLoan.sONewFinBal_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sTotCcPbs",
                () => dataLoan.sTotCcPbs_rep));

            props.Add(new LoanFileFieldData(
                "sOCredit1Amt",
                () => dataLoan.sOCredit1Amt_rep));

            props.Add(new LoanFileFieldData(
                "sOCredit2Amt",
                () => dataLoan.sOCredit2Amt_rep));

            props.Add(new LoanFileFieldData(
                "sONewFinCc",
                () => dataLoan.sONewFinCc_rep));

            props.Add(new LoanFileFieldData(
                "sFfUfmipFinanced",
                () => dataLoan.sFfUfmipFinanced_rep, 
                true));

            props.Add(new LoanFileFieldData(
                "sTransNetCash",
                () => dataLoan.sTransNetCash_rep));

            props.Add(new LoanFileFieldData(
                "sGfeRateLockPeriod",
                () => dataLoan.sGfeRateLockPeriod_rep, 
                true));

            props.Add(new LoanFileFieldData(
                "sIsRateLocked",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sIsRateLocked)));

            props.Add(new LoanFileFieldData(
                "sGfeLockPeriodBeforeSettlement",
                () => dataLoan.sGfeLockPeriodBeforeSettlement_rep));

            props.Add(new LoanFileFieldData(
                "sRLifeCapR",
                () => dataLoan.sRLifeCapR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sMldsHasImpound",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sMldsHasImpound)));

            props.Add(new LoanFileFieldData(
                "sRefPdOffAmt1003Lckd",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sRefPdOffAmt1003Lckd),
                true));

            props.Add(new LoanFileFieldData(
                "sTotEstPp1003Lckd",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sTotEstPp1003Lckd),
                true));

            props.Add(new LoanFileFieldData(
                "sTotEstCc1003Lckd",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sTotEstCc1003Lckd),
                true));

            props.Add(new LoanFileFieldData(
                "sLDiscnt1003Lckd",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sLDiscnt1003Lckd),
                true));

            props.Add(new LoanFileFieldData(
                "sOCredit3Desc",
                () => dataLoan.sOCredit3Desc));

            props.Add(new LoanFileFieldData(
                "sOCredit4Desc",
                () => dataLoan.sOCredit4Desc));

            props.Add(new LoanFileFieldData(
                "sTotCcPbsLocked",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sTotCcPbsLocked),
                true));

            props.Add(new LoanFileFieldData(
                "sOCredit1Lckd",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sOCredit1Lckd),
                true));

            props.Add(new LoanFileFieldData(
                "sOCredit3Amt",
                () => dataLoan.sOCredit3Amt_rep));

            props.Add(new LoanFileFieldData(
                "sOCredit4Amt",
                () => dataLoan.sOCredit4Amt_rep));

            props.Add(new LoanFileFieldData(
                "sTransNetCashLckd",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sTransNetCashLckd),
                true));

            props.Add(new LoanFileFieldData(
                "sLpTemplateNm",
                () => dataLoan.sLpTemplateNm,
                true));

            props.Add(new LoanFileFieldData(
                "sCcTemplateNm",
                () => dataLoan.sCcTemplateNm,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeNoteIRAvailTillDLckd",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sGfeNoteIRAvailTillDLckd),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeRateLockPeriodLckd",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sGfeRateLockPeriodLckd),
                true));

            props.Add(new LoanFileFieldData(
                "sDaysInYr",
                () => dataLoan.sDaysInYr_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sRAdj1stCapR",
                () => dataLoan.sRAdj1stCapR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sRAdjCapR",
                () => dataLoan.sRAdjCapR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sRAdjLifeCapR",
                () => dataLoan.sRAdjLifeCapR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sPmtAdjCapR",
                () => dataLoan.sPmtAdjCapR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sPmtAdjRecastPeriodMon",
                () => dataLoan.sPmtAdjRecastPeriodMon_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sPmtAdjCapMon",
                () => dataLoan.sPmtAdjCapMon_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sPmtAdjMaxBalPc",
                () => dataLoan.sPmtAdjMaxBalPc_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sIsPrintTimeForGfeNoteIRAvailTillD",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sIsPrintTimeForGfeNoteIRAvailTillD),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeNoteIRAvailTillDTimeZoneT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sGfeNoteIRAvailTillDTimeZoneT),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeTradeOffLowerCCLoanAmt",
                () => dataLoan.sGfeTradeOffLowerCCLoanAmt_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeTradeOffLowerRateLoanAmt",
                () => dataLoan.sGfeTradeOffLowerRateLoanAmt_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeTradeOffLowerCCNoteIR",
                () => dataLoan.sGfeTradeOffLowerCCNoteIR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeTradeOffLowerRateNoteIR",
                () => dataLoan.sGfeTradeOffLowerRateNoteIR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeTradeOffLowerCCClosingCost",
                () => dataLoan.sGfeTradeOffLowerCCClosingCost_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeTradeOffLowerRateClosingCost",
                () => dataLoan.sGfeTradeOffLowerRateClosingCost_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan1OriginatorName",
                () => dataLoan.sGfeShoppingCartLoan1OriginatorName,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan2OriginatorName",
                () => dataLoan.sGfeShoppingCartLoan2OriginatorName,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan3OriginatorName",
                () => dataLoan.sGfeShoppingCartLoan3OriginatorName,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan1CanRateIncreaseTri",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sGfeShoppingCartLoan1CanRateIncreaseTri),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sGfeShoppingCartLoan1CanLoanBalanceIncreaseTri),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sGfeShoppingCartLoan1CanMonthlyPmtIncreaseTri),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan1HavePpmtPenaltyTri",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sGfeShoppingCartLoan1HavePpmtPenaltyTri),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan1IsBalloonTri",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sGfeShoppingCartLoan1IsBalloonTri),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan1LoanAmt",
                () => dataLoan.sGfeShoppingCartLoan1LoanAmt_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan1LoanTerm",
                () => dataLoan.sGfeShoppingCartLoan1LoanTerm_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan1NoteIR",
                () => dataLoan.sGfeShoppingCartLoan1NoteIR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan1InitialPmt",
                () => dataLoan.sGfeShoppingCartLoan1InitialPmt_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan1RateLockPeriod",
                () => dataLoan.sGfeShoppingCartLoan1RateLockPeriod_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan1TotalClosingCost",
                () => dataLoan.sGfeShoppingCartLoan1TotalClosingCost_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan2CanRateIncreaseTri",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sGfeShoppingCartLoan2CanRateIncreaseTri),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sGfeShoppingCartLoan2CanLoanBalanceIncreaseTri),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sGfeShoppingCartLoan2CanMonthlyPmtIncreaseTri),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan2HavePpmtPenaltyTri",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sGfeShoppingCartLoan2HavePpmtPenaltyTri),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan2IsBalloonTri",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sGfeShoppingCartLoan2IsBalloonTri),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan2LoanAmt",
                () => dataLoan.sGfeShoppingCartLoan2LoanAmt_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan2LoanTerm",
                () => dataLoan.sGfeShoppingCartLoan2LoanTerm_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan2NoteIR",
                () => dataLoan.sGfeShoppingCartLoan2NoteIR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan2InitialPmt",
                () => dataLoan.sGfeShoppingCartLoan2InitialPmt_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan2RateLockPeriod",
                () => dataLoan.sGfeShoppingCartLoan2RateLockPeriod_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan2TotalClosingCost",
                () => dataLoan.sGfeShoppingCartLoan2TotalClosingCost_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan3CanRateIncreaseTri",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sGfeShoppingCartLoan3CanRateIncreaseTri),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sGfeShoppingCartLoan3CanLoanBalanceIncreaseTri),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sGfeShoppingCartLoan3CanMonthlyPmtIncreaseTri),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan3HavePpmtPenaltyTri",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sGfeShoppingCartLoan3HavePpmtPenaltyTri),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan3IsBalloonTri",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sGfeShoppingCartLoan3IsBalloonTri),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan3LoanAmt",
                () => dataLoan.sGfeShoppingCartLoan3LoanAmt_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan3LoanTerm",
                () => dataLoan.sGfeShoppingCartLoan3LoanTerm_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan3NoteIR",
                () => dataLoan.sGfeShoppingCartLoan3NoteIR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan3InitialPmt",
                () => dataLoan.sGfeShoppingCartLoan3InitialPmt_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan3RateLockPeriod",
                () => dataLoan.sGfeShoppingCartLoan3RateLockPeriod_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeShoppingCartLoan3TotalClosingCost",
                () => dataLoan.sGfeShoppingCartLoan3TotalClosingCost_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGfeInitialDisclosureD",
                () => dataLoan.sGfeInitialDisclosureD_rep));

            props.Add(new LoanFileFieldData(
                "sGfeRedisclosureD",
                () => dataLoan.sGfeRedisclosureD_rep,
                true));

            props.Add(new LoanFileFieldData(
               "sGfeMaxLoanBalance",
               () => dataLoan.sGfeMaxLoanBalance_rep));

            props.Add(new LoanFileFieldData(
               "sGfeFirstInterestChangeIn",
               () => dataLoan.sGfeFirstInterestChangeIn));

            props.Add(new LoanFileFieldData(
               "sGfeFirstPaymentChangeIn",
               () => dataLoan.sGfeFirstPaymentChangeIn));

            props.Add(new LoanFileFieldData(
               "sGfeFirstAdjProThisMPmtAndMIns",
               () => dataLoan.sGfeFirstAdjProThisMPmtAndMIns_rep));

            props.Add(new LoanFileFieldData(
               "sGfeMaxProThisMPmtAndMIns",
               () => dataLoan.sGfeMaxProThisMPmtAndMIns_rep));

            props.Add(new LoanFileFieldData(
               "sGfeBalloonDueInYrs",
               () => dataLoan.sGfeBalloonDueInYrs));

            props.Add(new LoanFileFieldData(
               "sGfeOriginationF",
               () => dataLoan.sGfeOriginationF_rep));

            props.Add(new LoanFileFieldData(
               "sGfeRequiredServicesTotalFee",
               () => dataLoan.sGfeRequiredServicesTotalFee_rep));

            props.Add(new LoanFileFieldData(
               "sGfeServicesYouShopTotalFee",
               () => dataLoan.sGfeServicesYouShopTotalFee_rep));

            props.Add(new LoanFileFieldData(
               "sGfeTotalOtherSettlementServiceFee",
               () => dataLoan.sGfeTotalOtherSettlementServiceFee_rep));

            props.Add(new LoanFileFieldData(
               "sGfeLenderTitleTotalFee",
               () => dataLoan.sGfeLenderTitleTotalFee_rep));

            props.Add(new LoanFileFieldData(
               "sGfeGovtRecTotalFee",
               () => dataLoan.sGfeGovtRecTotalFee_rep));

            props.Add(new LoanFileFieldData(
               "sLoads1003LineLFromAdjustments",
               () => dataLoan.m_convertLos.ToBitString(dataLoan.sLoads1003LineLFromAdjustments),
               true));

            props.Add(new LoanFileFieldData(
               "sGfeDailyInterestTotalFee",
               () => dataLoan.sGfeDailyInterestTotalFee_rep));

            props.Add(new LoanFileFieldData(
               "sGfeAdjOriginationCharge",
               () => dataLoan.sGfeAdjOriginationCharge_rep));

            props.Add(new LoanFileFieldData(
               "sGfeOwnerTitleTotalFee",
               () => dataLoan.sGfeOwnerTitleTotalFee_rep));

            props.Add(new LoanFileFieldData(
               "sGfeTransferTaxTotalFee",
               () => dataLoan.sGfeTransferTaxTotalFee_rep));

            props.Add(new LoanFileFieldData(
               "sGfeHomeOwnerInsuranceTotalFee",
               () => dataLoan.sGfeHomeOwnerInsuranceTotalFee_rep));

            props.Add(new LoanFileFieldData(
               "sGfeNotApplicableF",
               () => dataLoan.sGfeNotApplicableF_rep));

            props.Add(new LoanFileFieldData(
               "sGfeProThisMPmtAndMIns",
               () => dataLoan.sGfeProThisMPmtAndMIns_rep));

            props.Add(new LoanFileFieldData(
               "sGfeTradeOffLowerCCMPmtAndMIns",
               () => dataLoan.sGfeTradeOffLowerCCMPmtAndMIns_rep));

            props.Add(new LoanFileFieldData(
               "sGfeTradeOffLowerRateMPmtAndMIns",
               () => dataLoan.sGfeTradeOffLowerRateMPmtAndMIns_rep));

            props.Add(new LoanFileFieldData(
               "sGfeTradeOffLowerCCMPmtAndMInsDiff",
               () => dataLoan.sGfeTradeOffLowerCCMPmtAndMInsDiff_rep));

            props.Add(new LoanFileFieldData(
               "sGfeTradeOffLowerRateMPmtAndMInsDiff",
               () => dataLoan.sGfeTradeOffLowerRateMPmtAndMInsDiff_rep));

            props.Add(new LoanFileFieldData(
               "sGfeTradeOffLowerCCClosingCostDiff",
               () => dataLoan.sGfeTradeOffLowerCCClosingCostDiff_rep));

            props.Add(new LoanFileFieldData(
               "sGfeTradeOffLowerRateClosingCostDiff",
               () => dataLoan.sGfeTradeOffLowerRateClosingCostDiff_rep));

            props.Add(new LoanFileFieldData(
               "sGfeTotalEstimateSettlementCharge",
               () => dataLoan.sGfeTotalEstimateSettlementCharge_rep));

            props.Add(new LoanFileFieldData(
                "sGfeIsTPOTransactionIsCalculated",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sGfeIsTPOTransactionIsCalculated),
                true));
        }

        /// <summary>
        /// Get the data fields considered to be used exclusively by Loan Estimate and Disclosure.
        /// These are not referenced by other Closing Cost types.
        /// </summary>
        /// <param name="props">Property list to add to.</param>
        /// <param name="dataLoan">Loan file to be used.</param>
        private static void GetFieldDataForLoanEstimateAndClosingDisclosure(HashSet<LoanFileFieldData> props, CPageData dataLoan)
        {
            props.Add(new LoanFileFieldData(
                "sSpState",
                () => dataLoan.sSpState,
                true));

            props.Add(new LoanFileFieldData(
                "sLenderCaseNum",
                () => dataLoan.sLenderCaseNum,
                true));

            props.Add(new LoanFileFieldData(
                "sLenderCaseNumLckd",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sLenderCaseNumLckd),
                true));

            props.Add(new LoanFileFieldData(
                "sLoanEstScAvailTillD",
                () => dataLoan.m_convertLos.ToDateTimeString(dataLoan.sLoanEstScAvailTillD),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeNoteIRAvailTillD_Time",
                () => dataLoan.sGfeNoteIRAvailTillD_Time,
                true));

            props.Add(new LoanFileFieldData(
                "sIsPrintTimeForsLoanEstScAvailTillD",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sIsPrintTimeForsLoanEstScAvailTillD),
                true));

            props.Add(new LoanFileFieldData(
                "sLoanEstScAvailTillDLckd",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sLoanEstScAvailTillDLckd),
                true));

            props.Add(new LoanFileFieldData(
                "sLoanEstScAvailTillD_Time",
                () => dataLoan.sLoanEstScAvailTillD_Time,
                true));

            props.Add(new LoanFileFieldData(
                "sTridProThisMPmt",
                () => dataLoan.sTridProThisMPmt_rep));

            props.Add(new LoanFileFieldData(
                "sProThisMPmt",
                () => dataLoan.sProThisMPmt_rep));

            props.Add(new LoanFileFieldData(
                "sGfeCanLoanBalanceIncrease",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sGfeCanLoanBalanceIncrease)));

            props.Add(new LoanFileFieldData(
                "sGfeCanRateIncrease",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sGfeCanRateIncrease)));

            props.Add(new LoanFileFieldData(
                "sEscrowPmtNonMI",
                () => dataLoan.sEscrowPmtNonMI_rep));

            props.Add(new LoanFileFieldData(
                "sFHACondCommMonExpenseEstimate",
                () => dataLoan.sFHACondCommMonExpenseEstimate_rep));

            props.Add(new LoanFileFieldData(
                "sMonthlyNonPINonMIExpenseTotalPITI",
                () => dataLoan.sMonthlyNonPINonMIExpenseTotalPITI_rep));

            props.Add(new LoanFileFieldData(
                "sRAdjMarginR",
                () => dataLoan.sRAdjMarginR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sMaxBuydwnR",
                () => dataLoan.sMaxBuydwnR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDLoanEstimateTotalPIMIPLoanCostsIn5Yrs",
                () => dataLoan.sTRIDLoanEstimateTotalPIMIPLoanCostsIn5Yrs_rep));

            props.Add(new LoanFileFieldData(
                "sApr",
                () => dataLoan.sApr_rep));

            props.Add(new LoanFileFieldData(
                "sCFPBTotalInterestPercent",
                () => dataLoan.sCFPBTotalInterestPercent_rep));

            props.Add(new LoanFileFieldData(
                "sCFPBTotalInterestPercentLE",
                () => dataLoan.sCFPBTotalInterestPercentLE_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDLoanEstimateTotalLoanCosts",
                () => dataLoan.sTRIDLoanEstimateTotalLoanCosts_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDLoanEstimateTotalOtherCosts",
                () => dataLoan.sTRIDLoanEstimateTotalOtherCosts_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDLoanEstimateLenderCredits",
                () => dataLoan.sTRIDLoanEstimateLenderCredits_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDLoanEstimateTotalClosingCosts",
                () => dataLoan.sTRIDLoanEstimateTotalClosingCosts_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDLoanEstimateBorrowerFinancedClosingCosts",
                () => dataLoan.sTRIDLoanEstimateBorrowerFinancedClosingCosts_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDLoanEstimateDownPaymentOrFundsFromBorrower",
                () => dataLoan.sTRIDLoanEstimateDownPaymentOrFundsFromBorrower_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDCashDeposit",
                () => dataLoan.sTRIDCashDeposit_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDLoanEstimateFundsForBorrower",
                () => dataLoan.sTRIDLoanEstimateFundsForBorrower_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDSellerCredits",
                () => dataLoan.sTRIDSellerCredits_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDLoanEstimateSellerCredits",
                 () => dataLoan.sTRIDLoanEstimateSellerCredits_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDLoanEstimateCashToClose",
                () => dataLoan.sTRIDLoanEstimateCashToClose_rep));

            props.Add(new LoanFileFieldData(
                "sRLckdExpiredD",
                () => dataLoan.sRLckdExpiredD_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sPmtAdjMaxBalPc",
                () => dataLoan.sPmtAdjMaxBalPc_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sBuydwnMon1",
                () => dataLoan.sBuydwnMon1_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sBuydwnMon2",
                () => dataLoan.sBuydwnMon2_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sBuydwnMon3",
                () => dataLoan.sBuydwnMon3_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sBuydwnMon4",
                () => dataLoan.sBuydwnMon4_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sBuydwnMon5",
                () => dataLoan.sBuydwnMon5_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sBuydwnR1",
                () => dataLoan.sBuydwnR1_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sBuydwnR2",
                () => dataLoan.sBuydwnR2_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sBuydwnR3",
                () => dataLoan.sBuydwnR3_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sBuydwnR4",
                () => dataLoan.sBuydwnR4_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sBuydwnR5",
                () => dataLoan.sBuydwnR5_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGradPmtR",
                () => dataLoan.sGradPmtR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGradPmtYrs",
                () => dataLoan.sGradPmtYrs_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sHouseVal",
                () => dataLoan.sHouseVal_rep));

            props.Add(new LoanFileFieldData(
                "sPmtAdjCapMon",
                () => dataLoan.sPmtAdjCapMon_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sPmtAdjCapR",
                () => dataLoan.sPmtAdjCapR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sPmtAdjRecastPeriodMon",
                () => dataLoan.sPmtAdjRecastPeriodMon_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sPpmtAmt",
                () => dataLoan.sPpmtAmt_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sPpmtMon",
                () => dataLoan.sPpmtMon_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sPpmtOneAmt",
                () => dataLoan.sPpmtOneAmt_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sPpmtOneMon",
                () => dataLoan.sPpmtOneMon_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sPpmtStartMon",
                () => dataLoan.sPpmtStartMon_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sProMIns2",
                () => dataLoan.sProMIns2_rep));

            props.Add(new LoanFileFieldData(
                "sProMIns2Mon",
                () => dataLoan.sProMIns2Mon_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sProMInsCancelLtv",
                () => dataLoan.sProMInsCancelLtv_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sProMInsMidptCancel",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sProMInsMidptCancel),
                true));

            props.Add(new LoanFileFieldData(
                "sProMInsMon",
                () => dataLoan.sProMInsMon_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sRAdj1stCapR",
                () => dataLoan.sRAdj1stCapR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sRAdjCapR",
                () => dataLoan.sRAdjCapR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sRAdjFloorR",
                () => dataLoan.sRAdjFloorR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sRAdjIndexR",
                () => dataLoan.sRAdjIndexR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sRAdjLifeCapR",
                () => dataLoan.sRAdjLifeCapR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sRAdjRoundT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sRAdjRoundT),
                true));

            props.Add(new LoanFileFieldData(
                "sRAdjRoundToR",
                () => dataLoan.sRAdjRoundToR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sRAdjWorstIndex",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sRAdjWorstIndex),
                true));

            props.Add(new LoanFileFieldData(
                "sIsOptionArm",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sIsOptionArm),
                true));

            props.Add(new LoanFileFieldData(
                "sOptionArmMinPayPeriod",
                () => dataLoan.sOptionArmMinPayPeriod_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sOptionArmMinPayIsIOOnly",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sOptionArmMinPayIsIOOnly),
                true));

            props.Add(new LoanFileFieldData(
                "sOptionArmTeaserR",
                () => dataLoan.sOptionArmTeaserR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sOptionArmIntroductoryPeriod",
                () => dataLoan.sOptionArmIntroductoryPeriod_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sOptionArmInitialFixMinPmtPeriod",
                () => dataLoan.sOptionArmInitialFixMinPmtPeriod_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sOptionArmMinPayOptionT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sOptionArmMinPayOptionT),
                true));

            props.Add(new LoanFileFieldData(
                "sOptionArmPmtDiscount",
                () => dataLoan.sOptionArmPmtDiscount_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sOptionArmNoteIRDiscount",
                () => dataLoan.sOptionArmNoteIRDiscount_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sIsFullAmortAfterRecast",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sIsFullAmortAfterRecast),
                true));

            props.Add(new LoanFileFieldData(
                "sProMInsCancelMinPmts",
                () => dataLoan.sProMInsCancelMinPmts_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sFfUfmipR",
                () => dataLoan.sFfUfmipR_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sFfUfMipIsBeingFinanced",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sFfUfMipIsBeingFinanced),
                true));

            props.Add(new LoanFileFieldData(
                "sIsConstructionLoanWithConstructionRefiData",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sIsConstructionLoanWithConstructionRefiData),
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDLoanEstimateTotalAllCosts",
                () => dataLoan.sTRIDLoanEstimateTotalAllCosts_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDLoanEstimateNoteIRAvailTillDTime",
                () => ToDateAndTimeString(dataLoan.sTRIDLoanEstimateNoteIRAvailTillDTime),
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDLoanEstimateNoteIRAvailTillD",
                () => dataLoan.sTRIDLoanEstimateNoteIRAvailTillD_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDIsLoanEstimateRateLocked",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sTRIDIsLoanEstimateRateLocked),
                true));

            props.Add(new LoanFileFieldData(
                "sInterestRateMaxEverStartAfterMon",
                () => dataLoan.sInterestRateMaxEverStartAfterMon_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDPIPmtSubsequentAdjustFreqMon",
                () => dataLoan.sTRIDPIPmtSubsequentAdjustFreqMon_rep));

            props.Add(new LoanFileFieldData(
                "sPIPmtFirstChangeAfterMon",
                () => dataLoan.sPIPmtFirstChangeAfterMon_rep));

            props.Add(new LoanFileFieldData(
                "sHardPlusSoftPrepmtPeriodMonths",
                () => dataLoan.sHardPlusSoftPrepmtPeriodMonths_rep));

            props.Add(new LoanFileFieldData(
                "sPIPmtMaxEverStartAfterMon",
                () => dataLoan.sPIPmtMaxEverStartAfterMon_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDInterestRateMaxEver",
                () => dataLoan.sTRIDInterestRateMaxEver_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDPIPmtMaxEver",
                () => dataLoan.sTRIDPIPmtMaxEver_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDLoanEstimateSetLockStatusMethodT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sTRIDLoanEstimateSetLockStatusMethodT),
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDLoanEstimateNoteIRAvailTillDTimeZoneT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sTRIDLoanEstimateNoteIRAvailTillDTimeZoneT),
                true));

            props.Add(new LoanFileFieldData(
                "sLoanEstScAvailTillDTimeZoneT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sLoanEstScAvailTillDTimeZoneT),
                true));

            props.Add(new LoanFileFieldData(
                "sSpAddr",
                () => dataLoan.sSpAddr,
                true));

            props.Add(new LoanFileFieldData(
                "sSpCity",
                () => dataLoan.sSpCity,
                true));

            props.Add(new LoanFileFieldData(
                "sSpZip",
                () => dataLoan.sSpZip,
                true));

            props.Add(new LoanFileFieldData(
                "sArmIndexNameVstr",
                () => dataLoan.sArmIndexNameVstr,
                true));

            props.Add(new LoanFileFieldData(
                "sLateDays",
                () => dataLoan.sLateDays,
                true));

            props.Add(new LoanFileFieldData(
                "sLateChargePc",
                () => dataLoan.sLateChargePc,
                true));

            props.Add(new LoanFileFieldData(
                "sLateChargeBaseDesc",
                () => dataLoan.sLateChargeBaseDesc,
                true));

            props.Add(new LoanFileFieldData(
                "sAssumeLT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sAssumeLT),
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDAdjustmentsAndOtherCredits",
                () => dataLoan.sTRIDAdjustmentsAndOtherCredits_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDLoanEstimateAdjustmentsAndOtherCredits",
                () => dataLoan.sTRIDLoanEstimateAdjustmentsAndOtherCredits_rep));

            props.Add(new LoanFileFieldData(
                "sReqPropIns",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sReqPropIns),
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDLoanEstimateCashToCloseCalcMethodT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sTRIDLoanEstimateCashToCloseCalcMethodT),
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDTotalPayoffsAndPayments",
                () => dataLoan.sTRIDTotalPayoffsAndPayments_rep));

            props.Add(new LoanFileFieldData(
                "sTolerance10BasisLEArchiveD",
                () => dataLoan.sTolerance10BasisLEArchiveD_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sTolerance10BasisLEArchiveId",
                () => dataLoan.sTolerance10BasisLEArchiveId_rep,
                true));

            // OPM 222055
            props.Add(new LoanFileFieldData(
                "sTridProjectedPayments",
                () => SerializationHelper.JsonNetSerialize(dataLoan.sTridProjectedPayments)));

            props.Add(new LoanFileFieldData(
                "sONewFinNetProceeds",
                () => dataLoan.sONewFinNetProceeds_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sONewFinBal",
                () => dataLoan.sONewFinBal_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDLoanEstimateLenderIntendsToServiceLoan",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sTRIDLoanEstimateLenderIntendsToServiceLoan),
                true));

            props.Add(new LoanFileFieldData(
                "sGfeDiscountPointF",
                () => dataLoan.sGfeDiscountPointF_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureAltCost",
                () => dataLoan.sTRIDClosingDisclosureAltCost_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureAltCostDescription",
                () => dataLoan.sTRIDClosingDisclosureAltCostDescription,
                updateValueOnCoc: true));

            props.Add(new LoanFileFieldData(
                "sPurchasePrice1003",
                () => dataLoan.sPurchasePrice1003_rep));

            props.Add(new LoanFileFieldData(
                "sLandIfAcquiredSeparately1003",
                () => dataLoan.sLandIfAcquiredSeparately1003_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sTridLPurposeT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sTridLPurposeT),
                true));

            props.Add(new LoanFileFieldData(
                "sIsIntReserveRequired",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sIsIntReserveRequired),
                true));

            props.Add(new LoanFileFieldData(
                "sConstructionIntCalcT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sConstructionIntCalcT),
                true));

            props.Add(new LoanFileFieldData(
                "sConstructionPhaseIntAccrualT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sConstructionPhaseIntAccrualT),
                true));

            props.Add(new LoanFileFieldData(
                "sConstructionInitialOddDays",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sConstructionInitialOddDays),
                true));

            props.Add(new LoanFileFieldData(
                "sProMInsCancelAppraisalLtv",
                () => dataLoan.sProMInsCancelAppraisalLtv_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sApprValFor_LTV_CLTV_HCLTV",
                () => dataLoan.sApprValFor_LTV_CLTV_HCLTV_rep,
                true));
        }

        /// <summary>
        /// Get the data fields considered to be used exclusively by Closing Disclosure.
        /// These are not referenced by other Closing Cost types.
        /// </summary>
        /// <param name="props">Property list to add to.</param>
        /// <param name="dataLoan">Loan file to be used.</param>
        private static void GetFieldDataForOnlyClosingDisclosure(HashSet<LoanFileFieldData> props, CPageData dataLoan)
        {
            props.Add(new LoanFileFieldData(
                "sSellerXmlContent",
                () => dataLoan.sSellerCollection.Serialize(),
                true));

            props.Add(new LoanFileFieldData(
                "sDocMagicDisbursementDLckd",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sDocMagicDisbursementDLckd),
                true));

            props.Add(new LoanFileFieldData(
                "sDocMagicDisbursementD",
                () => dataLoan.sDocMagicDisbursementD_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sLastDisclosedClosingDisclosureArchiveD",
                () => dataLoan.sLastDisclosedClosingDisclosureArchiveD_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sLastDisclosedClosingDisclosureArchiveId",
                () => dataLoan.sLastDisclosedClosingDisclosureArchiveId_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sNonMIHousingExpensesEscrowedReasonT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sNonMIHousingExpensesEscrowedReasonT),
                true));

            props.Add(new LoanFileFieldData(
                "sNonMIHousingExpensesNotEscrowedReasonT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sNonMIHousingExpensesNotEscrowedReasonT),
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDEscrowWaiverFee",
                () => dataLoan.sTRIDEscrowWaiverFee_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureLastLoanEstimateTotalClosingCostsExcludingLenderCredit",
                () => dataLoan.sTRIDClosingDisclosureLastLoanEstimateTotalClosingCostsExcludingLenderCredit_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureTotalClosingCostsExcludingLenderCredit",
                () => dataLoan.sTRIDClosingDisclosureTotalClosingCostsExcludingLenderCredit_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureLastLoanEstimateLenderCredit",
                () => dataLoan.sTRIDClosingDisclosureLastLoanEstimateLenderCredit_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sGrossDueFromBorrPersonalProperty",
                () => dataLoan.sGrossDueFromBorrPersonalProperty_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureBorrowerCostsPaidAtClosing",
                () => dataLoan.sTRIDClosingDisclosureBorrowerCostsPaidAtClosing_rep));

            props.Add(new LoanFileFieldData(
                "sTotalSellerPaidClosingCostsPaidAtClosing",
                () => dataLoan.sTotalSellerPaidClosingCostsPaidAtClosing_rep));

            props.Add(new LoanFileFieldData(
                "sTotCashDeposit",
                () => dataLoan.sTotCashDeposit_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sReductionsDueToSellerExcessDeposit",
                () => dataLoan.sReductionsDueToSellerExcessDeposit_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sReductionsDueToSellerPayoffOf1stMtgLoan",
                () => dataLoan.sReductionsDueToSellerPayoffOf1stMtgLoan_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sReductionsDueToSellerPayoffOf2ndMtgLoan",
                () => dataLoan.sReductionsDueToSellerPayoffOf2ndMtgLoan_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDTotalDueFromBorrowerAtClosing",
                () => dataLoan.sTRIDTotalDueFromBorrowerAtClosing_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDTotalPaidAlreadyByOrOnBehalfOfBorrowerAtClosing",
                () => dataLoan.sTRIDTotalPaidAlreadyByOrOnBehalfOfBorrowerAtClosing_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDTotalDueToSellerAtClosing",
                () => dataLoan.sTRIDTotalDueToSellerAtClosing_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDTotalDueFromSellerAtClosing",
                () => dataLoan.sTRIDTotalDueFromSellerAtClosing_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDSummariesOfTransactionCashFromBorrower",
                () => dataLoan.sTRIDSummariesOfTransactionCashFromBorrower_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDSummariesOfTransactionCashToSeller",
                () => dataLoan.sTRIDSummariesOfTransactionCashToSeller_rep));

            props.Add(new LoanFileFieldData(
                "sToleranceCure",
                () => dataLoan.sToleranceCure_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureTotalLoanCosts",
                () => dataLoan.sTRIDClosingDisclosureTotalLoanCosts_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureTotalOtherCosts",
                () => dataLoan.sTRIDClosingDisclosureTotalOtherCosts_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureGeneralLenderCredits",
                () => dataLoan.sTRIDClosingDisclosureGeneralLenderCredits_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureTotalClosingCosts",
                () => dataLoan.sTRIDClosingDisclosureTotalClosingCosts_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureLastLoanEstimateTotalClosingCosts",
                () => dataLoan.sTRIDClosingDisclosureLastLoanEstimateTotalClosingCosts_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureClosingCostsPaidBeforeClosing",
                () => dataLoan.sTRIDClosingDisclosureClosingCostsPaidBeforeClosing_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureBorrowerFinancedClosingCosts",
                () => dataLoan.sTRIDClosingDisclosureBorrowerFinancedClosingCosts_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureLastLoanEstimateBorrowerFinancedClosingCosts",
                () => dataLoan.sTRIDClosingDisclosureLastLoanEstimateBorrowerFinancedClosingCosts_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureDownPaymentOrFundsFromBorrower",
                () => dataLoan.sTRIDClosingDisclosureDownPaymentOrFundsFromBorrower_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureLastLoanEstimateDownPaymentOrFundsFromBorrower",
                () => dataLoan.sTRIDClosingDisclosureLastLoanEstimateDownPaymentOrFundsFromBorrower_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureLastLoanEstimateCashDeposit",
                () => dataLoan.sTRIDClosingDisclosureLastLoanEstimateCashDeposit_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureFundsForBorrower",
                () => dataLoan.sTRIDClosingDisclosureFundsForBorrower_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureLastLoanEstimateFundsForBorrower",
                () => dataLoan.sTRIDClosingDisclosureLastLoanEstimateFundsForBorrower_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureLastLoanEstimateSellerCredits",
                () => dataLoan.sTRIDClosingDisclosureLastLoanEstimateSellerCredits_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureLastLoanEstimateAdjustmentsAndOtherCredits",
                () => dataLoan.sTRIDClosingDisclosureLastLoanEstimateAdjustmentsAndOtherCredits_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureCashToClose",
                () => dataLoan.sTRIDClosingDisclosureCashToClose_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureLastLoanEstimateCashToClose",
                () => dataLoan.sTRIDClosingDisclosureLastLoanEstimateCashToClose_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sMiCertId",
                () => dataLoan.sMiCertId,
                true));

            props.Add(new LoanFileFieldData(
                "sFinCharge",
                () => dataLoan.sFinCharge_rep));

            props.Add(new LoanFileFieldData(
                "sFinancedAmt",
                () => dataLoan.sFinancedAmt_rep));

            props.Add(new LoanFileFieldData(
                "sTRIDTotalOfPayments",
                () => dataLoan.sTRIDTotalOfPayments_rep));

            props.Add(new LoanFileFieldData(
                "sHasDemandFeature",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sHasDemandFeature),
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDNegativeAmortizationT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sTRIDNegativeAmortizationT),
                true));

            props.Add(new LoanFileFieldData(
                "sSpFullAddr",
                () => dataLoan.sSpFullAddr,
                true));

            props.Add(new LoanFileFieldData(
                "sEscrowAccountExistsForNonMIHousingExpenses",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sEscrowAccountExistsForNonMIHousingExpenses)));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureLastLoanEstimateFinalLAmt",
                () => dataLoan.sTRIDClosingDisclosureLastLoanEstimateFinalLAmt_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT),
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDClosingDisclosureLastLoanEstimateTotalPayoffsAndPayments",
                () => dataLoan.sTRIDClosingDisclosureLastLoanEstimateTotalPayoffsAndPayments_rep,
                true));

            props.Add(new LoanFileFieldData(
                "sOriginatorCompensationTotalAmount",
                () => dataLoan.sOriginatorCompensationTotalAmount_rep));

            props.Add(new LoanFileFieldData(
                "sSettlementAgentFileNum",
                () => dataLoan.sSettlementAgentFileNum,
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDPartialPaymentAcceptanceT",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sTRIDPartialPaymentAcceptanceT),
                true));

            props.Add(new LoanFileFieldData(
                "sTRIDPropertyIsInNonRecourseLoanState",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sTRIDPropertyIsInNonRecourseLoanState),
                true));

            props.Add(new LoanFileFieldData(
                "sToleranceZeroPercentCure",
                () => dataLoan.sToleranceZeroPercentCure_rep));

            props.Add(new LoanFileFieldData(
                "sToleranceTenPercentCure",
                () => dataLoan.sToleranceTenPercentCure_rep));

            props.Add(new LoanFileFieldData(
                "sToleranceZeroPercentCureLckd",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sToleranceZeroPercentCureLckd)));

            props.Add(new LoanFileFieldData(
                "sToleranceTenPercentCureLckd",
                () => dataLoan.m_convertLos.ToBitString(dataLoan.sToleranceTenPercentCureLckd)));

            props.Add(new LoanFileFieldData(
                "sRenoFeesInClosingCosts",
                () => dataLoan.sRenoFeesInClosingCosts_rep,
                updateValueOnCoc: true));

            props.Add(new LoanFileFieldData(
                "sRemain1stMBal",
                () => dataLoan.sRemain1stMBal_rep));

            props.Add(new LoanFileFieldData(
                "sLienToPayoffTotDebt",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sLienToPayoffTotDebt)));

            props.Add(new LoanFileFieldData(
                "sClosingDisclosureEscrowedPropertyCostsFirstYear",
                () => dataLoan.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep,
                updateValueOnCoc: true));

            props.Add(new LoanFileFieldData(
                "sClosingDisclosureNonEscrowedPropertyCostsFirstYear",
                () => dataLoan.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep,
                updateValueOnCoc: true));

            props.Add(new LoanFileFieldData(
                "sClosingDisclosureMonthlyEscrowPayment",
                () => dataLoan.sClosingDisclosureMonthlyEscrowPayment_rep,
                updateValueOnCoc: true));

            props.Add(new LoanFileFieldData(
                "sClosingDisclosurePropertyCostsFirstYear",
                () => dataLoan.sClosingDisclosurePropertyCostsFirstYear_rep,
                updateValueOnCoc: true));

            props.Add(new LoanFileFieldData(
                "sLienToIncludeCashDepositInTridDisclosures",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sLienToIncludeCashDepositInTridDisclosures)));

            props.Add(new LoanFileFieldData(
                "sLoanTransactionInvolvesSeller",
                () => dataLoan.m_convertLos.ToCountString((int)dataLoan.sLoanTransactionInvolvesSeller)));
        }

        /// <summary>
        /// Pull out the legacy-style fields if they exist.
        /// </summary>
        /// <param name="reader">Reader on main element.</param>
        private void ReadLegacyFields(XmlReader reader)
        {
            if (reader["LoanAmount"] != null)
            {
                this.LoanAmount_legacy = this.converter.ToDecimal(reader["LoanAmount"]);
            }

            if (reader["PurchasePrice"] != null)
            {
                this.PurchasePrice_legacy = this.converter.ToDecimal(reader["PurchasePrice"]);
            }

            if (reader["AppraisalValue"] != null)
            {
                this.AppraisalValue_legacy = this.converter.ToDecimal(reader["AppraisalValue"]);
            }

            if (reader["TotalLoanAmount"] != null)
            {
                this.TotalLoanAmount_legacy = this.converter.ToDecimal(reader["TotalLoanAmount"]);
            }
        }

        /// <summary>
        /// Gets the status during deserialization process.
        /// </summary>
        /// <param name="reader">The xml reader.</param>
        /// <returns>The archive status.</returns>
        private E_ClosingCostArchiveStatus TryGetStatusFromAttribute(XmlReader reader)
        {
            var status = reader["Status"];

            // Loan estimate archives that existed prior to the status being
            // added will default to Disclosed. We also want to consider all
            // other archive types as disclosed because this will simplify
            // the filtering logic.
            if (string.IsNullOrEmpty(status))
            {
                return E_ClosingCostArchiveStatus.Disclosed;
            }
            else
            {
                return (E_ClosingCostArchiveStatus)Enum.Parse(
                    typeof(E_ClosingCostArchiveStatus),
                    status);
            }
        }

        /// <summary>
        /// Gets the archive source during deserialization.
        /// </summary>
        /// <param name="reader">The xml reader.</param>
        /// <returns>The archive source. Unknown if the xml didn't contain the source.</returns>
        private E_ClosingCostArchiveSource TryGetSourceFromAttribute(XmlReader reader)
        {
            var source = reader["Source"];

            if (string.IsNullOrEmpty(source))
            {
                return E_ClosingCostArchiveSource.Unknown;
            }
            else
            {
                return (E_ClosingCostArchiveSource)Enum.Parse(
                    typeof(E_ClosingCostArchiveSource),
                    source);
            }
        }

        /// <summary>
        /// Writes the field value content to the xml writer.
        /// </summary>
        /// <param name="writer">The xml writer.</param>
        private void WriteFieldValueContent(XmlWriter writer)
        {
            if (!this.StoreFieldsInline)
            {
                throw new InvalidOperationException(
                    "Writing field value content only supported for temporary archives.");
            }

            writer.WriteStartElement("Fields");

            foreach (var field in this.FieldList)
            {
                writer.WriteStartElement("fi");
                writer.WriteAttributeString("id", field.Key);
                writer.WriteAttributeString("val", field.Value);
                writer.WriteEndElement();
            }

            writer.WriteEndElement(); // Fields
        }

        /// <summary>
        /// Reads the Fields element and its contents if available.
        /// </summary>
        /// <remarks>
        /// This will not do anything if the archive has been migrated to store
        /// its field/value content in FileDB.
        /// </remarks>
        /// <param name="reader">The xml reader.</param>
        private void ReadLegacyFieldValueContentIfPresent(XmlReader reader)
        {
            if (reader.MoveToContent() == XmlNodeType.Element && reader.LocalName == "Fields")
            {
                reader.Read();
                while (reader.MoveToContent() == XmlNodeType.Element && reader.LocalName == "fi")
                {
                    this.AddValue(reader["id"], reader["val"]);
                    reader.Read();
                }
            }
        }

        /// <summary>
        /// Handles reading the rest of the xml content after the Fields element.
        /// </summary>
        /// <param name="reader">The xml reader.</param>
        private void ReadThroughEndOfClosingCostArchiveElement(XmlReader reader)
        {
            if (reader.NodeType == XmlNodeType.EndElement &&
                reader.LocalName == "ClosingCostArchive")
            {
                reader.ReadEndElement();
            }
            else
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.EndElement &&
                        reader.LocalName == "ClosingCostArchive")
                    {
                        reader.ReadEndElement();
                        break;
                    }

                    if (reader.NodeType == XmlNodeType.Element &&
                        reader.LocalName == "ccset")
                    {
                        this.ClosingCostJson_legacy = reader.ReadElementContentAsString();
                    }
                }
            }
        }

        /// <summary>
        /// Tells the instance that it will need to lazy load the field/value
        /// content from FileDB.
        /// </summary>
        private void LazyLoadFieldListFromFileDB()
        {
            this.internalFieldList = null;
        }

        /// <summary>
        /// Loads the field/value content from FileDB.
        /// </summary>
        private void LoadFieldValueContentFromFileDB()
        {
            try
            {
                this.internalFieldList = FileDBTools.ReadJsonNetObject<Dictionary<string, string>>(
                    E_FileDB.Normal,
                    this.FileDBKey);
            }
            catch (System.IO.FileNotFoundException e)
            {
                Tools.LogError("ClosingCostArchive expected to have file " + this.FileDBKey + " but it wasn't found.", e);
                throw;
            }
        }

        /// <summary>
        /// Get legacy fields in correct format if they exist.
        /// </summary>
        private void ProcessLegacyFieldsForRead()
        {
            if (this.FieldList.Keys.Contains("sLAmtCalc") == false)
            {
                this.AddValue("sLAmtCalc", this.converter.ToDecimalString(this.LoanAmount_legacy, FormatDirection.ToDb));
            }

            if (this.FieldList.Keys.Contains("sFinalLAmt") == false)
            {
                this.AddValue("sFinalLAmt", this.converter.ToDecimalString(this.TotalLoanAmount_legacy, FormatDirection.ToDb));
            }

            if (this.FieldList.Keys.Contains("sPurchPrice") == false)
            {
                this.AddValue("sPurchPrice", this.converter.ToDecimalString(this.PurchasePrice_legacy, FormatDirection.ToDb));
            }

            if (this.FieldList.Keys.Contains("sApprVal") == false)
            {
                this.AddValue("sApprVal", this.converter.ToDecimalString(this.TotalLoanAmount_legacy, FormatDirection.ToDb));
            }

            if (this.FieldList.Keys.Contains("sClosingCostSetJsonContent") == false)
            {
                this.AddValue("sClosingCostSetJsonContent", this.ClosingCostJson_legacy);
            }
        }

        /// <summary>
        /// Gets a string to use as a FileDB key.
        /// </summary>
        /// <returns>A string to use as a FileDB key.</returns>
        private string GenerateFileDBKey()
        {
            return Guid.NewGuid().ToString("N") + "_cc_archive";
        }

        /// <summary>
        /// Adds a field value pair to the archive.
        /// </summary>
        /// <param name="fieldId">The field id.</param>
        /// <param name="value">The value for the field.</param>
        private void AddValue(string fieldId, string value)
        {
            this.FieldList.Add(fieldId, value);
            this.fileDBContentDirty = true;
        }

        /// <summary>
        /// Some field update operations require cached fields not to be cached anymore.
        /// </summary>
        private void InvalidateCachedFields()
        {
            foreach (var cachedFieldName in CacheInvalidatorByFieldName.Keys)
            {
                CacheInvalidatorByFieldName[cachedFieldName](this);
            }
        }

        /// <summary>
        /// Invalidate a particular cached field.
        /// </summary>
        /// <param name="fieldName">The name of the field to invalidate.</param>
        private void InvalidateCachedField(string fieldName)
        {
            if (CacheInvalidatorByFieldName.ContainsKey(fieldName))
            {
                CacheInvalidatorByFieldName[fieldName](this);
            }
        }

        /// <summary>
        /// Get value from field dictionary.
        /// </summary>
        /// <param name="fieldId">Field identifier.</param>
        /// <returns>Field's value.</returns>
        private string GetRawValue(string fieldId)
        {
            string ret;
            if (this.FieldList.TryGetValue(fieldId, out ret))
            {
                return ret;
            }

            // Legacy...
            if (this.FieldList.TryGetValue(fieldId + "_rep", out ret))
            {
                return ret;
            }

            // Once we have defined what version needs what fields, 
            // can possibly error if this was expected to be here.
            return string.Empty;
        }

        /// <summary>
        /// Indicates error when updating LE archive status.
        /// </summary>
        public class LoanEstimateArchiveStatusUpdateException : CBaseException
        {
            /// <summary>
            /// The user message for the exception.
            /// </summary>
            private const string UserMsg = "There is a Loan Estimate "
                + "associated with this archive marked as Initial. Please go to the " 
                + "Status/General page and select the correct Initial Loan Estimate.";

            /// <summary>
            /// The developer message for the exception.
            /// </summary>
            private const string DevMsg = "User attempted invalid archive status update."; 

            /// <summary>
            /// Initializes a new instance of the <see cref="LoanEstimateArchiveStatusUpdateException"/> class.
            /// </summary>
            public LoanEstimateArchiveStatusUpdateException()
                : base(UserMsg, DevMsg)
            {
                this.IsEmailDeveloper = false;
            }
        }

        /// <summary>
        /// Indicates error when updating CD archive status.
        /// </summary>
        public class ClosingDisclosureArchiveStatusUpdateException : CBaseException
        {
            /// <summary>
            /// The user message for the exception.
            /// </summary>
            private const string UserMsg = "There is a Closing Disclosure "
                + "associated with this archive marked as Initial, Preview, or Final. " 
                + "Please go to the Status/General page and select the correct Initial, "
                + "Preview, or Final Closing Disclosure.";

            /// <summary>
            /// The developer message for the exception.
            /// </summary>
            private const string DevMsg = "User attempted invalid archive status update."; 

            /// <summary>
            /// Initializes a new instance of the <see cref="ClosingDisclosureArchiveStatusUpdateException"/> class.
            /// </summary>
            public ClosingDisclosureArchiveStatusUpdateException()
                : base(UserMsg, DevMsg)
            {
                this.IsEmailDeveloper = false;
            }
        }

        /// <summary>
        /// Data representing a loan file field.
        /// </summary>
        private class LoanFileFieldData
        {
            /// <summary>
            /// Initializes a new instance of the LoanFileFieldData class.
            /// </summary>
            /// <param name="fieldId">The field identifier.</param>
            /// <param name="getter">The getting method for pulling value into archive.</param>
            /// <param name="updateValueOnCoc">True if the field should update from loan data when 
            /// creating a new archive via the change of circumstance process.</param>
            public LoanFileFieldData(string fieldId, Func<string> getter, bool updateValueOnCoc)
            {
                this.FieldId = fieldId;
                this.Getter = getter;
                this.UpdateValueOnCoC = updateValueOnCoc;
            }

            /// <summary>
            /// Initializes a new instance of the LoanFileFieldData class.
            /// </summary>
            /// <param name="fieldId">The field identifier.</param>
            /// <param name="getter">The getting method for pulling value into archive.</param>
            public LoanFileFieldData(string fieldId, Func<string> getter)
                : this(fieldId, getter, false)
            {
            }

            /// <summary>
            /// Gets the field identifier.
            /// </summary>
            public string FieldId { get; private set; }

            /// <summary>
            /// Gets the getting method for pulling value into archive.
            /// </summary>
            public Func<string> Getter { get; private set; }

            /// <summary>
            /// Gets a value indicating whether the loan field data should be updated when generating a new archive
            /// as a result from the change of circumstance process.
            /// </summary>
            /// <value>True if the value should update from loan data when creating a new archive.</value>
            public bool UpdateValueOnCoC { get; private set; }
        }
    }
}
