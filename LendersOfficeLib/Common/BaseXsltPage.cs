///
/// Author: David Dao
/// 
using System;
using System.IO;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;

using LendersOffice.Constants;
using LendersOffice.HttpModule;
using System.Text;
using DataAccess;

namespace LendersOffice.Common
{
    public abstract class BaseXsltPage : System.Web.UI.Page
    {
        private bool m_hasContent = true;
        

        protected bool HasContent 
        {
            get { return m_hasContent; }
            set { m_hasContent = value; }
        }
        protected virtual bool IsXsltFromEmbeddedResource
        {
            get { return false; }
        }
        protected abstract string XsltFileLocation { get; }
        protected abstract void GenerateXmlData(XmlWriter writer);

        protected virtual string NoContentMessage 
        {
            get { return ""; }
        }
        protected virtual XsltArgumentList XsltParams
        {
            get { return null; }
        }
        protected override void OnInit(EventArgs e)
        {
            this.Load += new System.EventHandler(this.PageLoad);
            base.OnInit(e);
        }

        protected virtual void InitXslt() 
        {
            // Any initialize of Xslt can take place here.

        }

        private void PageLoad(object sender, System.EventArgs e) 
        {
            Response.Clear();
            Response.ClearContent();
            try 
            {
                // 3/24/2008 dd - Disable the render of timing output code.
                PerformanceMonitorItem item = PerformanceMonitorItem.Current;
                if (null != item)
                    item.IsEnabledDebugOutput = false;
            } 
            catch {}

            InitXslt();
            using (MemoryStream stream = new MemoryStream(10000)) 
            {
                XmlWriterSettings writerSettings = new XmlWriterSettings();
                writerSettings.Encoding = new UTF8Encoding(false); // Skip BOM
                writerSettings.OmitXmlDeclaration = true;

                XmlWriter writer = XmlWriter.Create(stream, writerSettings);
                GenerateXmlData(writer);
                writer.Flush();

                if (m_hasContent) 
                {
                    stream.Seek(0, SeekOrigin.Begin);
                    if (IsXsltFromEmbeddedResource)
                    {
                        XslTransformHelper.TransformFromEmbeddedResource(XsltFileLocation, stream, Response.OutputStream, XsltParams);
                    }
                    else
                    {
                        XslTransformHelper.Transform(XsltFileLocation, stream, Response.OutputStream, XsltParams);
                    }
                } 
                else 
                {
                    Response.Write(string.Format("<html><body><table width='100%' height='100%'><tr><td valign='center' align='center' style='font-weight:bold;font-size:18px'>{0}</td></tr></table></body></html>", NoContentMessage));
                }

                //Response.Flush();
                Response.End();
            }
        }
    }
}
