﻿namespace LendersOffice.Common
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;
    using System.Xml;
    using System.Xml.Linq;
    using Constants;
    using DataAccess;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Class that contains various methods to call the Banana Security web services.
    /// </summary>
    public class BananaApiService
    {
        /// <summary>
        /// Gets a value indicating whether the token matches the given seed.
        /// </summary>
        /// <param name="seed">The seed to match against.</param>
        /// <param name="token">The token to verify.</param>
        /// <returns>NUll, if we ran into an errror, true if it matches and false otherwise.</returns>
        public static bool? ValidateAuthenticatorToken(string seed, string token)
        {
            if (string.IsNullOrEmpty(seed) || string.IsNullOrEmpty(token))
            {
                return false;
            }

            var doc = GenerateAuthenticatorRequest(seed);
            string url = $"{ConstStage.BananaSecurityTfaUrl}/verify/{token}";
            var response = SubmitRequest(url, doc);

            if (response == null)
            {
                return null;
            }

            var message = response.Element("MESSAGE");
            if (message == null)
            {
                Tools.LogError("Unexpected response.");
                return null;
            }

            return string.Equals(message.Value, "true", StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Gets the qr code and manual code for a seed.
        /// </summary>
        /// <param name="seed">The seed used to generate values.</param>
        /// <param name="issuer">Who should the issuer be listed as on the authenticator app.</param>
        /// <param name="name">What name should appear on the authenticator app.</param>
        /// <returns>An object with the response details.</returns>
        public static AuthenticatorRegistrationInfo GetAuthenticatorRegistration(string seed, string issuer, string name)
        {
            if (string.IsNullOrEmpty(seed))
            {
                throw new ArgumentException("Seed is invalid.");
            }

            string url = $"{ConstStage.BananaSecurityTfaUrl}/qrcode";

            var doc = GenerateAuthenticatorRequest(seed);
            var response = SubmitRequest(url, doc);

            if (response == null)
            {
                return null;
            }

            // There's a typo in the response. In case they fix it we dont have to change the code.
            var barcodeUrl = response.Element("ORCODE") ?? response.Element("QRCODE");
            var message = response.Element("MESSAGE");
            var manualCode = response.Element("MANUALCODE");

            if (manualCode == null || message.Value.IndexOf("Success") == -1)
            {
                return null;
            }

            return new AuthenticatorRegistrationInfo()
            {
                Seed = seed,
                ManualCode = manualCode.Value,
                QrCodeUrl = GenerateFriendlyAuthenticatorGoogleUrlFrom(manualCode.Value, issuer, name)
            };
        }

        /// <summary>
        /// Sends an SMS message.
        /// </summary>
        /// <param name="phoneNumber">The phone number to send to.</param>
        /// <param name="message">The message to send.</param>
        /// <param name="attemptCount">The number of attempts we've tried to send this message.</param>
        /// <returns>True if OK response recieved. False otherwise.</returns>
        public static bool SendSmsMessage(string phoneNumber, string message, int attemptCount)
        {
            XDocument requestDoc = GenerateSmsRequestXml(phoneNumber, message, attemptCount);
            string requestXml = requestDoc.ToString();

            string loggingXml = requestXml;
            loggingXml = Regex.Replace(loggingXml, " password=\"[^ ]+\"", " password=\"******\"");
            loggingXml = Regex.Replace(loggingXml, "LendingQB Authentication Code: [^ <]+([ <])", "LendingQB Authentication Code: *****$1");
            loggingXml = Regex.Replace(loggingXml, "phone_number=\"[^ ]+\"", "phone_number=\"******\"");
            loggingXml = Regex.Replace(loggingXml, "source_number=\"([^ ]{1,4})[^ ]+\"", "source_number=\"$1-******\"");
            Tools.LogInfo("BananaSecuritySmsRequest", loggingXml);

            byte[] data = Encoding.UTF8.GetBytes(requestXml);
            string smsUrl = ConstStage.BananaSecuritySmsUrl;

            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(smsUrl);
                webRequest.Method = "POST";
                webRequest.ContentLength = data.Length;

                using (Stream stream = webRequest.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                HttpStatusCode statusCode;
                XDocument responseDocument;
                using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse())
                {
                    using (Stream responseStream = webResponse.GetResponseStream())
                    {
                        responseDocument = XDocument.Load(responseStream);
                    }

                    statusCode = webResponse.StatusCode;
                }

                Tools.LogInfo("BananaSecuritySmsResponse", responseDocument.ToString());
                if (statusCode != HttpStatusCode.OK)
                {
                    Tools.LogError($"Error code: {statusCode}.");
                    return false;
                }

                return true;
            }
            catch (SystemException exc)
            {
                Tools.LogError($"URL hit: <{smsUrl}>{Environment.NewLine}", exc);
                return false;
            }
        }

        /// <summary>
        /// Generates a friendly URL from the given manual code, issuer and description.
        /// </summary>
        /// <param name="manualCode">The manual registration code.</param>
        /// <param name="issuer">The issuer of the manual code.</param>
        /// <param name="description">The description of the user.</param>
        /// <returns>A url to the barcode.</returns>
        private static string GenerateFriendlyAuthenticatorGoogleUrlFrom(string manualCode, string issuer, string description)
        {
            issuer = issuer.Replace(' ', '-');
            var url = HttpUtility.UrlEncode($"otpauth://totp/{issuer}:{description}?secret={manualCode}&issuer={issuer}").Replace("+", "%2520");
            var barcodeUrl = $"http://chart.googleapis.com/chart?cht=qr&chs=300x300&chl={url}";
            return barcodeUrl;
        }

        /// <summary>
        /// Gets the OTP request for the given seed.  Logs the request without the password.
        /// </summary>
        /// <param name="seed">The seed to create the request for.</param>
        /// <returns>The xml document for the request.</returns>
        private static XDocument GenerateAuthenticatorRequest(string seed)
        {
            var token = new XAttribute("token", ConstStage.BananaSecurityToken);

            string decryptedToken = EncryptionHelper.Decrypt(ConstStage.BananaSecurityPassword);
            var password = new XAttribute("password", "********");
            var seedAttr = new XAttribute("seed", "*****");

            var doc = new XDocument(new XElement("REQUEST", new XAttribute("type", "TFA"), seedAttr, new XElement("USER", token, password)));
            Tools.LogInfo($"BananaSecurityQRRequest:\n{doc}");

            password.Value = decryptedToken;
            seedAttr.Value = seed;

            return doc;
        }

        /// <summary>
        /// Submits a request to the given URL.
        /// </summary>
        /// <param name="url">The url to make the request to.</param>
        /// <param name="request">The document to send to the url.</param>
        /// <returns>The xml to return.</returns>
        private static XElement SubmitRequest(string url, XDocument request)
        {
            string responseDocument = null;

            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.Method = "POST";
                using (Stream stream = webRequest.GetRequestStream())
                using (var xmlStream = XmlTextWriter.Create(stream))
                {
                    request.WriteTo(xmlStream);
                }

                using (HttpWebResponse wr = (HttpWebResponse)webRequest.GetResponse())
                {
                    using (var responseStream = wr.GetResponseStream())
                    using (var sr = new StreamReader(responseStream))
                    {
                        responseDocument = sr.ReadToEnd();
                    }

                    Tools.LogInfo($"BananaSecurityQRResponse:\n{responseDocument}");

                    if (wr.StatusCode != HttpStatusCode.OK || string.IsNullOrEmpty(responseDocument))
                    {
                        return null;
                    }
                }
            }
            catch (WebException e)
            {
                Tools.LogError(e);
                return null;
            }

            XElement response;
            try
            {
                response = XElement.Parse(responseDocument);
            }
            catch (FormatException e)
            {
                Tools.LogError("Could not parse banana xml", e);
                return null;
            }

            if (response == null)
            {
                Tools.LogError("Got back null doc.");
                return null;
            }

            return response;
        }

        /// <summary>
        /// Generates the request XML for SMS.
        /// </summary>
        /// <param name="phoneNumber">The phone number.</param>
        /// <param name="message">The message to send.</param>
        /// <param name="retryCount">The number of attempts we've tried to send this message.</param>
        /// <returns>Xml document for the request.</returns>
        private static XDocument GenerateSmsRequestXml(string phoneNumber, string message, int retryCount)
        {
            if (string.IsNullOrEmpty(phoneNumber) || string.IsNullOrEmpty(message))
            {
                throw new ArgumentNullException();
            }

            var sourceNumbers = ConstStage.BananaSecuritySmsSourceNumber.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            if (sourceNumbers.Length == 0)
            {
                throw new DeveloperException(LqbGrammar.DataTypes.ErrorMessage.BadConfiguration, new SimpleContext("BananaSecuritySmsSourceNumber not configured."));
            }

            var numberIndex = retryCount % sourceNumbers.Length;
            var sourceNumber = sourceNumbers[numberIndex];

            var phoneNumberToUse = ConstructPhoneNumberToSend(phoneNumber);

            XDocument request = new XDocument();
            XElement rootElement = new XElement(
                                    "REQUEST",
                                    new XAttribute("type", "SMS"),
                                    new XAttribute("phone_number", phoneNumberToUse),
                                    new XAttribute("source_number", sourceNumber));

            var token = ConstStage.BananaSecurityToken;
            var password = EncryptionHelper.Decrypt(ConstStage.BananaSecurityPassword);
            XElement userElement = new XElement(
                                    "USER",
                                    new XAttribute("token", token),
                                    new XAttribute("password", password));
            rootElement.Add(userElement);

            XElement templateElement = new XElement("TEMPLATE", message);
            rootElement.Add(templateElement);

            request.Add(rootElement);

            return request;
        }

        /// <summary>
        /// Constructs the phone number. The API requires the country code in order for it to send.
        /// </summary>
        /// <param name="phoneNumber">The phone number to send to.</param>
        /// <returns>The modified phone number.</returns>
        private static string ConstructPhoneNumberToSend(string phoneNumber)
        {
            var phoneNumberModified = Regex.Replace(phoneNumber, "[^0-9]", string.Empty, RegexOptions.Compiled);

            // We need to prepend the international calling code to the phone number. For US numbers, this is 1.
            // We want to prepend it to phone numbers that don't start with 1 or for those that have 10 characters or less. (111)111-1111.
            if (!phoneNumberModified.StartsWith("1") || phoneNumberModified.Length <= 10)
            {
                var countryCode = "1";
                phoneNumberModified = $"{countryCode}{phoneNumberModified}";
            }

            return phoneNumberModified;
        }
    }
}
