using System;

namespace LendersOffice.Common
{
    /// <summary>
    /// We frequently need to return a list of mappings between keys
    /// from one domain that relate to values from another.  A hash
    /// table does this nicely, but the order of the entries is also
    /// important.  So, we provide a shared base class for objects
    /// that contain string-based mappings.
    /// </summary>

    public interface IPair
    {
        /// <summary>
        /// Retrieve embedded mapping from implementation.
        /// </summary>

        string Key
        {
            get;
        }

        string Val
        {
            get;
        }

    }

}
