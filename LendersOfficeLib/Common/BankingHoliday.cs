﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Admin;

namespace LendersOffice.Common
{
    public enum HolidayType
    {
        NewYearDay,
        MartinLutherKingDay,
        PresidentsDay,
        MemorialDay,
        IndependenceDay,
        LaborDay,
        ColumbusDay,
        VeteransDay,
        ThanksgivingDay,
        ChristmasDay,
    }

    /// <summary>
    /// Banking holiday is the same as the Federal holiday. According to Wikipedia http://en.wikipedia.org/wiki/Federal_holidays_in_the_United_States
    /// here are the list of holidays: New Year, Martin Luther King, Presidents, Memorial, Independence Day, Labor Day, Columbus Day,
    /// Veterans Day, Thanksgiving Day
    /// </summary>
    public class BankingHoliday
    {
        private BankingHoliday()
        {

        }

        public HolidayType Type { get; private set; }

        public DateTime Date { get; private set; }


        private static BankingHoliday CreateHoliday(HolidayType type, int year)
        {
            DateTime dt;
            switch (type)
            {
                case HolidayType.NewYearDay: // 3/11/2010 dd - Always 1/1
                    dt = new DateTime(year, 1, 1);
                    break;
                case HolidayType.MartinLutherKingDay: // 3/11/2010 dd - Third Monday of January each year.
                    dt = GetDate(year, 1, DayOfWeek.Monday, 3);
                    break;
                case HolidayType.PresidentsDay: // 3/11/2010 dd - Third Monday of February each year
                    dt = GetDate(year, 2, DayOfWeek.Monday, 3);
                    break;
                case HolidayType.MemorialDay:// 3/11/2010 dd - Last Monday of May
                    dt = new DateTime(year, 5, 31);
                    while (dt.DayOfWeek != DayOfWeek.Monday)
                    {
                        dt = dt.AddDays(-1);
                    }
                    break;
                case HolidayType.IndependenceDay: // July 4th
                    dt = new DateTime(year, 7, 4);
                    break;
                case HolidayType.LaborDay: // 3/11/2010 dd - First Monday of September
                    dt = GetDate(year, 9, DayOfWeek.Monday, 1);
                    break;
                case HolidayType.ColumbusDay: // 3/11/2010 dd - Second Monday of October
                    dt = GetDate(year, 10, DayOfWeek.Monday, 2);
                    break;
                case HolidayType.VeteransDay: // 3/11/2010 dd - November 11
                    dt = new DateTime(year, 11, 11);
                    break;
                case HolidayType.ThanksgivingDay: // 3/11/2010 dd - 4th Thursday in November
                    dt = GetDate(year, 11, DayOfWeek.Thursday, 4);
                    break;
                case HolidayType.ChristmasDay: // 3/11/2010 dd - December 25
                    dt = new DateTime(year, 12, 25);
                    break;
                default:
                    throw new UnhandledEnumException(type);
            }
            return new BankingHoliday() { Date = dt, Type = type };
        }

        private static DateTime GetDate(int year, int month, DayOfWeek dayOfWeek, int position)
        {
            DateTime dt = new DateTime(year, month, 1);
            
            // 3/11/2010 dd - Find the first dayOfWeek in this month.
            while (dt.DayOfWeek != dayOfWeek)
            {
                dt = dt.AddDays(1);
            }
            return dt.AddDays((position - 1) * 7);
        }

        private static Dictionary<int, IEnumerable<BankingHoliday>> s_holidayHash = new Dictionary<int,IEnumerable<BankingHoliday>>();
        private static object s_lock = new object();
        public static IEnumerable<BankingHoliday> GetHolidayDaysInYear(int year)
        {
            lock (s_lock)
            {
                IEnumerable<BankingHoliday> result = null;
                if (!s_holidayHash.TryGetValue(year, out result))
                {
                    List<BankingHoliday> list = new List<BankingHoliday>();
                    foreach (HolidayType d in Enum.GetValues(typeof(HolidayType)))
                    {
                        list.Add(CreateHoliday(d, year));
                    }

                    result = list.OrderBy(o => o.Date);
                    s_holidayHash.Add(year, result);
                }
                return result;
            }
        }

        public static bool IsBankHoliday(DateTime dt, bool pushSundayHolidaysToMonday)
        {
            if (dt.DayOfWeek == DayOfWeek.Sunday)
            {
                return true;
            }
            else
            {
                foreach (BankingHoliday holiday in GetHolidayDaysInYear(dt.Year))
                {
                    if (holiday.Date == dt.Date)
                    {
                        return true;
                    }
                    else if (pushSundayHolidaysToMonday && holiday.Date.DayOfWeek == DayOfWeek.Sunday && holiday.Date.AddDays(1) == dt.Date)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Only count Monday - Saturday. Also skip over any Federal Banking Holiday.
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="days"></param>
        /// <param name="pushSundayHolidaysToMonday"></param>
        /// <returns></returns>
        public static DateTime AddWorkingBankDays(DateTime dt, int days, bool pushSundayHolidaysToMonday)
        {
            int currentDays = 0;

            if (days >= 0)
            {
                while (currentDays < days)
                {
                    dt = dt.AddDays(1);

                    if (IsBankHoliday(dt, pushSundayHolidaysToMonday))
                    {
                        continue;
                    }

                    currentDays++;
                }
            }
            else
            {
                while (currentDays > days)
                {
                    dt = dt.AddDays(-1);

                    if (IsBankHoliday(dt, pushSundayHolidaysToMonday))
                    {
                        continue;
                    }
                    currentDays--;
                }
            }

            return dt;

        }
        
        public static DateTime GetDateByPreviousBusinessDays(DateTime startingDate, int businessDays, bool pushSundayHolidaysToMonday, Guid brokerId)
        {
            if (businessDays < 0)
            {
                throw new CBaseException(ErrorMessages.Generic, "days cannot be negative in BankingHoliday.GetPreviousBusinessDays");
            }

            // 11/17/2010 dd - Return the date of the last business days. 
            // Business day is consider to be Mon - Friday and is not in the Banking Holiday
            // and is not in lender closure list.
            DateTime dt = startingDate;
            BrokerDB broker;
            Guid lockPolicyID = Guid.Empty;
            if (brokerId != Guid.Empty)
            {
                broker = BrokerDB.RetrieveById(brokerId);
                lockPolicyID = broker.DefaultLockPolicyID.Value;
            }
            var keyForHolidayList = GeneratedKeyForRequestOrThreadCache.LockPolicyClosureListKey(lockPolicyID);

            var lenderHolidayList = (ObjLib.LockPolicies.LockDeskClosures)CurrentRequestOrThreadCache.GetRequestOrThreadObjectByName(keyForHolidayList);
            if (lenderHolidayList == null)
            {
                lenderHolidayList = new ObjLib.LockPolicies.LockDeskClosures(brokerId, lockPolicyID);
                CurrentRequestOrThreadCache.SetRequestOrThreadObjectByName(keyForHolidayList, lenderHolidayList);
            }

            int numDays = 0;
            while (numDays != businessDays)
            {
                dt = dt.AddDays(-1);

                if (dt.DayOfWeek != DayOfWeek.Saturday && dt.DayOfWeek != DayOfWeek.Sunday
                    && BankingHoliday.IsBankHoliday(dt, pushSundayHolidaysToMonday) == false && !(lenderHolidayList.FindClosureOrDefault(dt)?.IsClosedForBusiness ?? false))
                {
                    numDays++;
                }
            }
            return dt;
        }
    }
}
