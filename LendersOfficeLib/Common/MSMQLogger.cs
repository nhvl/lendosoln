﻿namespace LendersOffice.Common
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Security.Principal;
    using System.Text;
    using System.Web;
    using System.Xml.Linq;

    using CommonProjectLib.Logging;
    using DataAccess;
    using LendersOffice.ObjLib.Logging;

    /// <summary>
    /// Copied from CommonProjectLib.Logging.MSMQLogger, send log messages to a message queue.
    /// </summary>
    public class MSMQLogger : ILogger
    {
        /// <summary>
        /// The name of the current process.
        /// </summary>
        private static string currentProcessName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;

        /// <summary>
        /// Dictionary of components to ignore.
        /// </summary>
        private static Dictionary<string, bool> componentIgnoreDictionary = new Dictionary<string, bool>()
        {
            { "DataAccess.Tools::LogMadness", true },
            { "DataAccess.Tools::LogRegTest", true },
            { "DataAccess.Tools::LogError", true },
            { "DataAccess.Tools::LogErrorImpl", true },
            { "DataAccess.Tools::LogWarning", true },
            { "DataAccess.Tools::LogWarningImpl", true },
            { "DataAccess.Tools::LogErrorAndSendMail", true },
            { "DataAccess.Tools::LogBug", true },
            { "DataAccess.Tools::LogInfo", true },
            { "DataAccess.Tools::LogTimingInfo", true },
            { "LendersOffice.HttpModule.AbstractHttpModule::Log", true },
            { "LendersOfficeApp.MessageLog.LogHelper::Log", true },
            { "LendersOfficeApp.MessageLog.LogHelper::LogContext", true },
            { "LendersOfficeApp.MessageLog.LogHelper::LogContextFormat", true },
            { "LendersOfficeApp.MessageLog.LogHelper::Logf", true },
            { "LendersOfficeApp.MessageLog.LogHelper::Bugf", true },
            { "LendersOfficeApp.MessageLog.LogHelper::Errorf", true },
            { "LendersOfficeApp.MessageLog.LogHelper::Infof", true },
            { "LendersOfficeApp.MessageLog.LogHelper::Verbosef", true },
            { "LendersOfficeApp.MessageLog.LogHelper::TimingLog", true },
            { "LendersOffice.Common.PaulBunyanHelper::Log", true },
            { "LendersOffice.Common.PaulBunyanHelper::LogImpl", true },
            { "LendersOffice.Common.PaulBunyanHelper::LogWithoutDriver", true },
            { "LendersOffice.Common.PaulBunyanHelper::Error", true },
            { "LendersOffice.Common.PaulBunyanHelper::RegTest", true },
            { "LendersOffice.Common.PaulBunyanHelper::Verbose", true },
            { "LendersOffice.Common.PaulBunyanHelper::Bug", true },
            { "LendersOffice.Common.PaulBunyanHelper::Warning", true },
            { "LendersOffice.Common.ErrorUtilities::LogPBAndSendEmail", true },
            { "LendersOffice.Common.ErrorUtilities::DisplayErrorPage", true },
            { "CommonProjectLib.Logging.DbLogger::Log", true },
            { "CommonProjectLib.Logging.MSMQLogger::Log", true },
            { "LendersOffice.Common.MSMQLogger::Log", true },
            { "LendersOffice.Logging.MSMQLogger::Log", true },
            { "LendersOffice.Logging.DbLogger::Log", true },
            { "LPEUpdate.ReportingTools::Log", true },
            { "DataAccess.CBase::LogError", true },
            { "DataAccess.CBase::LogErrorException", true },
            { "LendersOffice.HttpModule.LendingQBSoapExtension::LogException", true },
            { "DataAccess.CBase::LogErrorAndSendMail", true },
            { "CommonProjectLib.Logging.LogManager+<>c__DisplayClass2::<Log>b__0", true },
            { "DataAccess.LargeFieldStorage::LogError", true },
            { "DataAccess.CBase::LogWarning", true },
            { "System.Collections.Generic.List`1::ForEach", true },
            { "CommonProjectLib.Logging.LogManager::Log", true },
            { "CommonProjectLib.Logging.LogManager::Info", true },
            { "CommonProjectLib.Logging.LogManager::Error", true },
            { "CommonProjectLib.Logging.LogManager::Warn", true },
            { "CommonProjectLib.Logging.LogManager::Trace", true },
            { "CommonProjectLib.Logging.LogManager::Bug", true }
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="MSMQLogger"/> class.
        /// </summary>
        /// <param name="connectionStrings">The message queue.</param>
        /// <param name="environmentName">Environment name descriptor.</param>
        public MSMQLogger(string connectionStrings, string environmentName) : this(new string[] { connectionStrings }, environmentName)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MSMQLogger"/> class.
        /// </summary>
        /// <param name="connectionStrings">The list of message queues.</param>
        /// <param name="environmentName">Environment name descriptor.</param>
        public MSMQLogger(string[] connectionStrings, string environmentName)
        {
            if (connectionStrings.All(p => string.IsNullOrEmpty(p)))
            {
                throw new ArgumentNullException("connectionString is required.");
            }

            this.MessageQueueConnections = connectionStrings.Where(p => !string.IsNullOrEmpty(p)).ToArray();
            this.EnvironmentName = environmentName;
        }

        /// <summary>
        /// Gets or sets the environment name.
        /// </summary>
        private string EnvironmentName { get; set; }

        /// <summary>
        /// Gets or sets the list of message queue names.
        /// </summary>
        private string[] MessageQueueConnections { get; set; }

        /// <summary>
        /// Formulate a log message and send it to the queues.
        /// </summary>
        /// <param name="level">The severity level of the message.</param>
        /// <param name="properties">The content of the message.</param>
        public void Log(E_LoggingLevel level, LogProperties properties)
        {
            string category = string.Empty;
            Guid requestUniqueId = Guid.Empty;
            string loanId = string.Empty;
            string correlationId = string.Empty;
            string clientIp = string.Empty;
            string uniqueClientIdCookie = string.Empty;
            string errorUniqueId = string.Empty;
            string timingProcessing = string.Empty;

            if (properties.CalleeProperties.ContainsKey("RequestUniqueId"))
            {
                requestUniqueId = new Guid(properties.CalleeProperties["RequestUniqueId"]);
            }

            if (properties.CalleeProperties.TryGetValue("Category", out category) == false)
            {
                category = string.Empty;
            }

            if (LoggingContext.Current != null && LoggingContext.Current.LoanId.HasValue)
            {
                loanId = LoggingContext.Current.LoanId.Value.ToString();
            }
            else if (properties.CalleeProperties.TryGetValue("sLId", out loanId) == false)
            {
                loanId = string.Empty;
            }

            if (LoggingContext.Current != null && LoggingContext.Current.CorrelationId.HasValue)
            {
                correlationId = Tools.ConvertGuidToFriendlyBase64(LoggingContext.Current.CorrelationId.Value);
            }
            else if (properties.CalleeProperties.TryGetValue("CorrelationId", out correlationId) == false)
            {
                correlationId = string.Empty;
            }

            if (properties.CalleeProperties.TryGetValue("ClientIp", out clientIp) == false)
            {
                clientIp = string.Empty;
            }

            if (properties.CalleeProperties.TryGetValue("UniqueClientIdCookie", out uniqueClientIdCookie) == false)
            {
                uniqueClientIdCookie = string.Empty;
            }

            if (properties.CalleeProperties.TryGetValue("error_unique_id", out errorUniqueId) == false)
            {
                errorUniqueId = string.Empty;
            }

            if (!properties.CalleeProperties.TryGetValue("timing_processing", out timingProcessing))
            {
                timingProcessing = string.Empty;
            }

            this.LogImpl(level, category, properties.Message, requestUniqueId, loanId, correlationId, clientIp, uniqueClientIdCookie, errorUniqueId, timingProcessing);
        }

        /// <summary>
        /// Remove illegal characters from the xml.
        /// </summary>
        /// <param name="xml">The xml as a string.</param>
        /// <returns>The same xml with illegal characters stripped out.</returns>
        private static string SantizeXmlString(string xml)
        {
            // NOTE: This can be replaced with an LqbGrammar.Utilities method 
            //       which was added in an independent pull request.
            StringBuilder sb = new StringBuilder(xml.Length);
            foreach (char ch in xml.ToCharArray())
            {
                if (IsLegalXmlChar(ch))
                {
                    sb.Append(ch);
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Test whether or not the input character is legal for xml.
        /// </summary>
        /// <param name="ch">The character being tested.</param>
        /// <returns>True if the input character is legal for xml, false otherwise.</returns>
        private static bool IsLegalXmlChar(int ch)
        {
            // The list of allowable characters http://www.w3.org/TR/REC-xml/#charsets
            return ch == 0x9 || ch == 0xA || ch == 0xD || (ch >= 0x20 && ch <= 0xD7FF) || (ch >= 0xE000 && ch <= 0xFFFD) || (ch >= 0x10000 && ch <= 0x10FFFF);
        }

        /// <summary>
        /// Send the message to the message queue.
        /// </summary>
        /// <param name="level">The severity level of the message.</param>
        /// <param name="category">The message category.</param>
        /// <param name="message">The log message.</param>
        /// <param name="requestUniqueId">A unique identifer.</param>
        /// <param name="loanId">A loan identifier.</param>
        /// <param name="correlationId">A correlation identifier.</param>
        /// <param name="clientIp">The IP address of the client.</param>
        /// <param name="uniqueClientIdCookie">A cookie used to track the client.</param>
        /// <param name="errorUniqueId">In case of error, an identifier associated for that error.</param>
        /// <param name="timingProcessing">The amount of time processing for the event in the log, if supplied.</param>
        private void LogImpl(
            E_LoggingLevel level, 
            string category, 
            string message, 
            Guid requestUniqueId, 
            string loanId,
            string correlationId,
            string clientIp,
            string uniqueClientIdCookie,
            string errorUniqueId,
            string timingProcessing)
        {
            try
            {
                Stopwatch sw = Stopwatch.StartNew();
                string machine = string.Empty;
                try
                {
                    machine = Environment.MachineName;
                }
                catch (InvalidOperationException)
                {
                    machine = "Environment.MachineName is undefined.";
                }

                string threadName = System.Threading.Thread.CurrentThread.Name;
                if (threadName == null)
                {
                    threadName = string.Empty;
                }

                string context = string.Empty;
                if (LoggingContext.Current != null && !string.IsNullOrWhiteSpace(LoggingContext.Current.UserName))
                {
                    context = LoggingContext.Current.UserName;
                }
                else
                {
                    IPrincipal principal = System.Threading.Thread.CurrentPrincipal;
                    if (principal != null)
                    {
                        context = principal.Identity.Name;
                    }
                }

                string customerCode = string.Empty;

                // 8/21/2017 - dd - DO NOT UNCOMMENT OUT THESE LINES BELOW THAT LOG CustomerCode.
                //
                //                  Under certain scenario where BrokerDB is reload from cache, the userPrincipal.BrokerDB will cause stackoverflow.
                //                  Basic flows:
                //                       1. Refresh BrokerDB because cache expire.
                //                       2. Call BrokerDB.BindDataReader
                //                       3.    Set IsRateLockedAtSubmission
                //                       4.         If CustomerCode="PML0278" and value change then log to PB
                //                       5. PB log try to retrieve BrokerDB base from userPrincipal.
                //                       6. Go to step 1
                //
                //     Note I leave the comment code here so I can investigate and come up a better solution to prevent this from happenning.
                //
                // AbstractUserPrincipal userPrincipal = principal as AbstractUserPrincipal;
                // if (userPrincipal != null && userPrincipal.BrokerDB != null)
                // {
                //    customerCode = userPrincipal.BrokerDB.CustomerCode;
                // }
                string module = AppDomain.CurrentDomain.FriendlyName;
                string file = string.Empty;
                if (HttpContext.Current != null)
                {
                    try
                    {
                        file = HttpContext.Current.Request.Path;
                        module += " (" + HttpContext.Current.Request.Url.Host + ")";
                    }
                    catch (HttpException)
                    {
                        // 9/23/2011 dd - HttpException can be throw if the Request object is not available or ready.
                    }
                }
                else if (ModuleNameContext.Current != null && !string.IsNullOrEmpty(ModuleNameContext.Current.Name))
                {
                    module = ModuleNameContext.Current.Name;
                }

                string component = this.GetComponentName();

                XDocument msg = this.CreateMessage(
                    level, 
                    component, 
                    module, 
                    context, 
                    customerCode,
                    category, 
                    file, 
                    machine, 
                    this.EnvironmentName,
                    threadName, 
                    message, 
                    requestUniqueId, 
                    loanId,
                    correlationId, 
                    clientIp, 
                    uniqueClientIdCookie, 
                    errorUniqueId,
                    timingProcessing);

                foreach (string connectionString in this.MessageQueueConnections)
                {
                    Drivers.Gateways.MessageQueueHelper.SendXML(connectionString, null, msg);
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Formulate an XDocument version of the log message.
        /// </summary>
        /// <param name="level">The severity level of the message.</param>
        /// <param name="component">A component value.</param>
        /// <param name="module">A module value.</param>
        /// <param name="context">A context value.</param>
        /// <param name="customerCode">PML client code.</param>
        /// <param name="category">A category value.</param>
        /// <param name="file">A file path.</param>
        /// <param name="machine">The machine name.</param>
        /// <param name="environmentName">Environment name.</param>
        /// <param name="threadName">The thread name.</param>
        /// <param name="message">The message contents.</param>
        /// <param name="requestUniqueId">A unique identifier for the request.</param>
        /// <param name="loanId">A loan identifier.</param>
        /// <param name="correlationId">A correlation identifier.</param>
        /// <param name="clientIp">The IP address of the client.</param>
        /// <param name="uniqueClientIdCookie">A cookie used to track the client.</param>
        /// <param name="errorUniqueId">In case of error, an identifier associated for that error.</param>
        /// <param name="timingProcessing">The amount of time processing for the event in the log, if supplied.</param>
        /// <returns>The message formulated as an XDocument.</returns>
        private XDocument CreateMessage(
            E_LoggingLevel level,
            string component, 
            string module, 
            string context, 
            string customerCode,
            string category, 
            string file, 
            string machine,
            string environmentName,
            string threadName, 
            string message, 
            Guid requestUniqueId, 
            string loanId,
            string correlationId, 
            string clientIp, 
            string uniqueClientIdCookie, 
            string errorUniqueId,
            string timingProcessing)
        {
            XDocument doc = new XDocument();

            XElement el = new XElement(
                "log",
                new XAttribute("Level", level.ToString()),
                new XAttribute("Component", component),
                new XAttribute("Module", module),
                new XAttribute("Context", context),
                new XAttribute("CustomerCode", customerCode),
                new XAttribute("Category", category),
                new XAttribute("File", file),
                new XAttribute("Machine", machine),
                new XAttribute("Environment", environmentName),
                new XAttribute("ProcessName", currentProcessName),
                new XAttribute("ThreadName", threadName),
                new XAttribute("sLId", loanId),
                new XAttribute("RequestUniqueId", requestUniqueId.ToString()),
                new XAttribute("CorrelationId", correlationId),
                new XAttribute("ClientIp", clientIp),
                new XAttribute("UniqueClientIdCookie", uniqueClientIdCookie),
                new XAttribute("ErrorUniqueId", errorUniqueId),
                new XAttribute("TimingProcessing", timingProcessing));

            el.Value = SantizeXmlString(message);
            doc.Add(el);
            return doc;
        }

        /// <summary>
        /// Navigate the call stack and retrieve the calling component's name, unless it is in the ignore list.
        /// </summary>
        /// <returns>The calling component's name.</returns>
        private string GetComponentName()
        {
            string component = string.Empty;
            StackTrace stackTrace = new StackTrace();
            for (int i = 2; i < stackTrace.FrameCount; i++)
            {
                MethodBase method = stackTrace.GetFrame(i).GetMethod();
                component = method.DeclaringType.FullName + "::" + method.Name;
                if (!componentIgnoreDictionary.ContainsKey(component))
                {
                    break;
                }
            }

            return component;
        }
    }
}
