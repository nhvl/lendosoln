﻿namespace LendersOffice.Common
{
    using System.Collections.Generic;

    /// <summary>
    /// Represents a single part of a MIME multipart content message.
    /// </summary>
    public class MultipartPart
    {
        /// <summary>
        /// Gets or sets the header collection of the multipart MIME part. 
        /// Each content header has different rules about its use. Instead of enforcing them, for now we just have a key-value dictionary of header values.
        /// </summary>
        /// <value>Header dictionary.</value>
        public Dictionary<string, string> Headers { get; set; } = new Dictionary<string, string>();

        /// <summary>
        /// Gets or sets the actual body content of the multipart communication.
        /// </summary>
        /// <value>MIME content.</value>
        public string Content
        {
            get;
            set;
        }
    }
}
