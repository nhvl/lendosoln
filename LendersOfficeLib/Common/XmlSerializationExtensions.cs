﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace LendersOffice.Common
{
    public static class XmlSerializationExtensions
    {
        public static string ToTrueFalse(this bool value)
        {
            return value.ToString().ToLower();
        }

        public static string ToYN(this bool value)
        {
            return value ? "Y" : "N";
        }

        public static string GetSafeAttribute(this XmlReader reader, string attr)
        {
            return reader.GetAttribute(attr) ?? string.Empty;
        }

        public static T ReadAsEnum<T>(this XmlReader reader) where T : struct, IConvertible
        {
            string value = reader.ReadElementSafe();
            return ToEnum<T>(value);
        }

        public static T ToEnum<T>(this string value) where T : struct, IConvertible
        {
            Type t = typeof(T);

            if (!t.IsEnum)
            {
                throw new ArgumentException(t.Name + " must be an enumerated type ");
            }

            return (T)Enum.Parse(t, value);
        }

        public static Nullable<T> TryParseEnum<T>(this string value) where T : struct, IConvertible
        {
            if (string.IsNullOrEmpty(value))
            {
                return new Nullable<T>();
            }

            return value.ToEnum<T>();
        }


        public static Nullable<T> GetNullableEnumAttribute<T>(this XmlReader reader, string attr) where T : struct, IConvertible
        {
            return GetSafeAttribute(reader, attr).TryParseEnum<T>();
        }

        public static Nullable<T> ReadAsNullableEnum<T>(this XmlReader reader) where T : struct, IConvertible
        {
            string value = reader.ReadElementSafe();
            return value.TryParseEnum<T>();
        }

        /// <summary>
        /// Reads a node <node>with some value</node> 
        /// it also handles <node/> 
        /// it also consumes the start and the end
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static string ReadElementSafe(this XmlReader reader)
        {
            string value = string.Empty;

            bool isEmptyElement = reader.IsEmptyElement;
            reader.ReadStartElement();

            if (!isEmptyElement)
            {
                value = reader.ReadContentAsString();
                reader.ReadEndElement();
            }

            return value;
        }

        public static Nullable<DateTime> ReadElementAsTicks(this XmlReader reader)
        {
            Nullable<DateTime> dateTime = new Nullable<DateTime>();

            bool isEmptyElement = reader.IsEmptyElement;
            reader.ReadStartElement();
            if (!isEmptyElement)
            {
                dateTime = new DateTime(reader.ReadContentAsLong());
                reader.ReadEndElement();
            }

            return dateTime;
        }

        public static decimal ReadElementAsDecimalSafe(this XmlReader reader)
        {
            decimal val;
            if (!Decimal.TryParse(reader.ReadElementSafe(), out val))
            {
                val = 0;
            }
            return val;
        }

        /// <summary>
        /// consumes the containing tag 
        /// verifies all children element are of tag = name
        /// handles empty elements as well 
        /// consumes end tag if needed.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static List<T> ReadAsList<T>(this XmlReader reader, string name) where T : XmlSerializableCommon.AbstractXmlSerializable, new()
        {
            List<T> elements = new List<T>();

            bool consumeEnd = !reader.IsEmptyElement;
            reader.ReadStartElement(); //consume parent

            while (reader.NodeType == XmlNodeType.Element && reader.Name == name)
            {
                T t = new T();
                t.ReadXml(reader);
                elements.Add(t);
            }

            if (consumeEnd)
            {
                reader.ReadEndElement();
            }

            return elements;
        }

        /// <summary>
        /// Writes <containerName><element></element></containerName>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="writer"></param>
        /// <param name="containerName"></param>
        /// <param name="elementName"></param>
        /// <param name="items"></param>
        public static void WriteList<T>(this XmlWriter writer, string containerName, string elementName, IEnumerable<T> items) where T : XmlSerializableCommon.AbstractXmlSerializable, new()
        {
            writer.WriteStartElement(containerName);
            foreach (T t in items)
            {
                writer.WriteStartElement(elementName);
                t.WriteXml(writer);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }
    }
}
