﻿namespace LendersOffice.Common
{
    /// <summary>
    /// Email Disclaimer Type.
    /// </summary>
    public enum E_DisclaimerType
    {
        /// <summary>
        /// Normal disclaimer.
        /// </summary>
        NORMAL,

        /// <summary>
        /// Consumer disclaimer.
        /// </summary>
        CONSUMER,

        /// <summary>
        /// No disclaimer.
        /// </summary>
        /// <remarks>
        /// 4/2/2014 dd - Provide a way to turn off disclaimer text. This mean the code generate email is responsible for inserting disclaimer.
        /// </remarks>
        NONE
    }
}
