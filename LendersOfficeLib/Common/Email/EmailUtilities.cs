namespace LendersOffice.Common
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.IO;
    using System.Text;
    using System.Web;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Email;
    using LendersOffice.HttpModule;
    using LendersOffice.ObjLib.Email;

    /// <summary>
    /// Class used to facilitate the sending of emails.
    /// </summary>
    public static class EmailUtilities
    {
        /// <summary>
        /// BE VERY CAREFUL WHEN MODIFYING THIS LIST.
        /// <para></para>
        /// *THE LAST THING WE WANT IS TO BE IGNORING CRITICAL EMAILS*.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "This is an old field.")]
        private static readonly IEnumerable<string> X_IP_ADDRESSES_TO_SKIP_EMAIL = new string[] { "208.116.56.43" };

        /// <summary>
        /// BE VERY CAREFUL WHEN MODIFYING THIS LIST.
        /// <para></para>
        /// *THE LAST THING WE WANT IS TO BE IGNORING CRITICAL EMAILS*.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "This is an old field.")]
        private static readonly IEnumerable<string> X_IP_RANGES_TO_SKIP_EMAIL = new string[] { "91.209.196.", "199.66.200." };

        /// <summary>
        /// Gets the default error email "from" address.
        /// </summary>
        /// <value>The default error email "from" address.</value>
        private static string DefaultErrorEmailFromAddress
        {
            get { return ConstStage.DefaultErrorEmailFromAddress; }
        }

        /// <summary>
        /// 12/16/2010 dd - OPM 20386
        /// Check to see if error message had been send in last x minutes. The database only check for maximum of 150 characters in error message.
        /// </summary>
        /// <param name="errorMessage">Subject in the error email. Only the first 150 characters are compare.</param>
        /// <param name="numberOfMinutes">Interval in minutes.</param>
        /// <param name="countOfTheDay">Current count.</param>
        /// <param name="isNewError">Is new error message.</param>
        /// <param name="lastEmailedToOpmDate">Last emailed to OPM date.</param>
        /// <param name="lastEmailedToCriticalDate">Last emailed to critical date.</param>
        /// <returns>True if error message has exceeded the send time interval.</returns>
        public static bool IsErrorMessageLastSendExceed(string errorMessage, int numberOfMinutes, out int countOfTheDay, out bool isNewError, out DateTime lastEmailedToOpmDate, out DateTime lastEmailedToCriticalDate)
        {
            countOfTheDay = 0;
            isNewError = false;
            lastEmailedToOpmDate = DateTime.MinValue;
            lastEmailedToCriticalDate = DateTime.MinValue;
            if (string.IsNullOrEmpty(errorMessage)) 
            {
                return true;
            }

            bool ret = true;

            SqlParameter[] parameters =
                {
                    new SqlParameter("@ErrorMessage", errorMessage),
                    new SqlParameter("@IntervalInMinutes", numberOfMinutes)
                };

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOTransient, "ERROR_IsNeedToEmailErrorMessage", parameters))
            {
                if (reader.Read())
                {
                    ret = (bool)reader["IsExceedTimeInterval"];
                    countOfTheDay = (int)reader["CurrentCount"];
                    isNewError = (bool)reader["IsNewErrorMessage"];

                    lastEmailedToOpmDate = reader.GetSafeValue("LastEmailedToOpmDate", DateTime.MinValue);
                    lastEmailedToCriticalDate = reader.GetSafeValue("LastEmailedToCriticalDate", DateTime.MinValue);                    
                }
            }

            return ret;
        }

        /// <summary>
        /// Sends DavidD an XSS warning email.
        /// </summary>
        /// <param name="url">Page where error occured.</param>
        /// <param name="txt">Error text.</param>
        public static void SendDavidCrossScriptingWarning(string url, string txt)
        {
            // 10/3/2014 dd - If ConstStage.SmtpServer is not defined then do not email out.
            if (string.IsNullOrEmpty(ConstStage.SmtpServer))
            {
                return;
            }

            Tools.LogWarning("XSS: Url:" + url + Environment.NewLine + txt);

            if (ConstStage.UseNewEmailer)
            {
                try
                {
                    SendCrossScriptingWarningNEW(ConstStage.SmtpServer, ConstStage.SmtpServerPortNumber, LqbGrammar.DataTypes.EmailAddress.DavidDao, url, txt);
                }
                catch (LqbGrammar.Exceptions.LqbException)
                {
                    SendCrossScriptingWarningOLD(ConstStage.SmtpServer, ConstStage.SmtpServerPortNumber, "david@meridianlink.com", "David Dao", url, txt);
                    SendNewCodeWarningWithMessage("SendDavidCrossScriptingWarning");
                }
            }
            else
            {
                SendCrossScriptingWarningOLD(ConstStage.SmtpServer, ConstStage.SmtpServerPortNumber, "david@meridianlink.com", "David Dao", url, txt);
            }
        }

        /// <summary>
        /// As of 2015-03-05 - This is the prefer way to send email. It use the newer mail namespace without if/else check.
        /// We can remove other SmtpSend method.
        /// </summary>
        /// <param name="email">CBaseEmail to send.</param>
        public static void SendEmail(CBaseEmail email)
        {
            if (email == null || string.IsNullOrEmpty(email.To))
            {
                // It is normal for one or more of our internal destination email addresses to be configured as empty
                return;
            }

            if (string.IsNullOrEmpty(ConstStage.SmtpServer))
            {
                return;
            }

            string from = email.From;
            string to = email.To;
            string subject = email.Subject;

            bool done = false;
            if (ConstStage.UseNewEmailer)
            {
                try
                {
                    LqbGrammar.DataTypes.EmailServerName? serverName = LqbGrammar.DataTypes.EmailServerName.Create(ConstStage.SmtpServer);
                    if (serverName == null)
                    {
                        throw new LqbGrammar.Exceptions.DeveloperException(LqbGrammar.DataTypes.ErrorMessage.BadConfiguration);
                    }

                    LqbGrammar.DataTypes.PortNumber? port = LqbGrammar.DataTypes.PortNumber.Create(ConstStage.SmtpServerPortNumber);
                    if (port == null)
                    {
                        throw new LqbGrammar.Exceptions.DeveloperException(LqbGrammar.DataTypes.ErrorMessage.BadConfiguration);
                    }

                    var lqbEmail = ConvertToMailMessageNEW(email);

                    LendersOffice.Drivers.Emailer.EmailHelper.SendEmail(serverName.Value, port.Value, lqbEmail);
                    done = true;
                }
                catch (LqbGrammar.Exceptions.LqbException)
                {
                    done = false;
                    string message = string.Format("From:{0}|To:{1}|Subject:{2}", from, to, subject);
                    SendNewCodeWarningWithMessage(message);
                }
            }

            if (!done)
            {
                System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(ConstStage.SmtpServer, ConstStage.SmtpServerPortNumber);
                smtpClient.EnableSsl = false;
                smtpClient.UseDefaultCredentials = false; // Per Rajya (CMG) in Exchange 2013, this is require.
                smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network; // OPM 461108 - Delivery Method MUST be Network for sender address to be used as SMTP FROM (return-path) address.

                using (System.Net.Mail.MailMessage mailMessage = ConvertToMailMessage(email))
                {
                    smtpClient.Send(mailMessage);
                }
            }
        }

        /// <summary>
        /// Sends critical email.
        /// </summary>
        /// <param name="subject">Email subject.</param>
        /// <param name="body">Email body.</param>
        public static void SendToCritical(string subject, string body)
        {
            if (ConstAppDavid.CurrentServerLocation == ServerLocation.Production)
            {
                Send(E_DisclaimerType.NORMAL, DefaultErrorEmailFromAddress, ConstStage.EmailAddressOfCriticalOPM, subject, body, false);
            }
            else
            {
                Tools.LogError($"SendToCritical - Subject: {subject} Body: {body}");
            }
        }

        /// <summary>
        /// Updates exception counters and sends a critical email
        /// if the counter for a particular exception surpasses
        /// the value of ConstApp.ExceptionCountForCriticalCase.
        /// </summary>
        /// <param name="errMsg">Error Message.</param>
        /// <param name="exc">Exception object.</param>
        public static void UpdateExceptionTracking(string errMsg, Exception exc)
        {
            UpdateExceptionTracking(DefaultErrorEmailFromAddress, errMsg, exc, null /* uniqueKey */);
        }

        /// <summary>
        /// Updates exception counters and sends a critical email
        /// if the counter for a particular exception surpasses
        /// the value of ConstApp.ExceptionCountForCriticalCase.
        /// </summary>
        /// <param name="errmsg">Error message.</param>
        public static void UpdateExceptionTracking(string errmsg)
        {
            System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace();

            string callStack = st.ToString();
            string s = string.Format("{0}\r\n****SENT FROM THIS CALL STACK: {1}", errmsg, callStack);

            // 10/6/2014 dd - Provide a unique key base on the call stack
            string uniqueKey = EncryptionHelper.ComputeSHA256Hash(callStack);

            UpdateExceptionTracking(DefaultErrorEmailFromAddress, null, new Exception(s), uniqueKey);
        }

        /// <summary>
        /// Sends an email to the integration pipeline in kayako, unlike <seealso cref="SendToIntegrationTeamWithoutCreatingTicket(string, string)"/>.
        /// </summary>
        /// <param name="subject">The email subject.</param>
        /// <param name="emailBody">The email body.</param>
        public static void SendToIntegrationSpecialist(string subject, string emailBody)
        {
            var cbe = new CBaseEmail(ConstAppDavid.SystemBrokerGuid)
            {
                DisclaimerType = E_DisclaimerType.NORMAL,
                From = ConstStage.DefaultDoNotReplyAddress,
                To = ConstStage.LQBIntegrationEmail,
                Subject = subject,
                Message = emailBody,
                IsHtmlEmail = true,
                BccToSupport = false
            };

            cbe.Send();
        }

        /// <summary>
        /// Sends an email to the integration team, without creating a kayako ticket unlike <see cref="SendToIntegrationSpecialist(string, string)"/>.
        /// </summary>
        /// <param name="subject">The email subject.</param>
        /// <param name="emailBody">The email body.</param>
        public static void SendToIntegrationTeamWithoutCreatingTicket(string subject, string emailBody)
        {
            var cbe = new CBaseEmail(ConstAppDavid.SystemBrokerGuid)
            {
                DisclaimerType = E_DisclaimerType.NORMAL,
                From = ConstStage.DefaultDoNotReplyAddress,
                To = ConstStage.LQBIntegrationTeamEmail,
                Subject = subject,
                Message = emailBody,
                IsHtmlEmail = true,
                BccToSupport = false
            };

            cbe.Send();
        }

        /// <summary>
        /// Send PML timing email.
        /// </summary>
        /// <param name="subject">The email subject.</param>
        /// <param name="emailBody">The email body.</param>
        public static void SendPmlTimingEmail(string subject, string emailBody)
        {
            string from = ConstStage.DefaultDoNotReplyAddress;
            string to = ConstStage.PmlTimingSupport;

            var cbe = new LendersOffice.Email.CBaseEmail(ConstAppDavid.SystemBrokerGuid)
            {
                DisclaimerType = E_DisclaimerType.NORMAL,
                From = from,
                To = to,
                Subject = subject,
                Message = emailBody,
                IsHtmlEmail = true,
                BccToSupport = false
            };

            // OPM 233895 - Queue Email.
            cbe.Send();
        }

        /// <summary>
        /// Send error to eOPM for SAEs. Be careful using this--it will create a case.
        /// This method dedupes to avoid spamming eOPM cases.
        /// </summary>
        /// <param name="subject">The email subject.</param>
        /// <param name="emailBody">The email body.</param>
        public static void SendSaeError(string subject, string emailBody)
        {
            DateTime lastEmailedToOpmDate = DateTime.MinValue;
            DateTime lastEmailedToCriticalDate = DateTime.MinValue;
            int currentCount = 0;
            bool isNewErrorMessage = false;

            string errorKey = "SAE_ERROR_" + subject;
            if (EmailUtilities.IsErrorMessageLastSendExceed(errorKey, ConstStage.MinutesToDedupException, out currentCount, out isNewErrorMessage, out lastEmailedToOpmDate, out lastEmailedToCriticalDate) == false)
            {
                return;
            }

            var cbe = new LendersOffice.Email.CBaseEmail(ConstAppDavid.SystemBrokerGuid)
            {
                DisclaimerType = E_DisclaimerType.NORMAL,
                From = ConstStage.DefaultDoNotReplyAddress,
                To = ConstStage.PmlFeedbackSupport,
                Subject = string.Format("[{0}] {1}", currentCount, isNewErrorMessage ? "NEW " : string.Empty) + subject,
                Message = emailBody,
                IsHtmlEmail = false,
                BccToSupport = false
            };
            SendEmail(cbe);
        }

        /// <summary>
        /// Used to send Alan an email if the FOOL email code has an error.
        /// </summary>
        public static void SendNewCodeWarning()
        {
            SendNewCodeWarningWithMessage(null);
        }

        /// <summary>
        /// Used to send Alan an email if the FOOL email code has an error.
        /// </summary>
        /// <param name="message">Message to send in email body.</param>
        public static void SendNewCodeWarningWithMessage(string message)
        {
            string to = "aland@lendingqb.com";
            string from = DefaultErrorEmailFromAddress;
            string subject = "ERROR FROM NEW EMAIL CODE";

            var stack = new System.Diagnostics.StackTrace();
            string body = stack.ToString();

            System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();
            System.Net.Mail.MailAddress fromAddress = new System.Net.Mail.MailAddress(from);

            System.Net.Mail.MailAddress sendToAddress = new System.Net.Mail.MailAddress(to);

            mailMessage.From = fromAddress;
            mailMessage.To.Add(sendToAddress);
            mailMessage.Subject = subject;

            mailMessage.Body = string.IsNullOrEmpty(message) ? body : string.Concat(message, Environment.NewLine, body);
            System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(ConstStage.SmtpServer, ConstStage.SmtpServerPortNumber);
            smtpClient.Send(mailMessage);
        }

        /// <summary>
        /// Add disclaimer to email body.
        /// </summary>
        /// <param name="body">Email body as string.</param>
        /// <param name="isHtmlFormat">Indicates if the email is HTML formatted.</param>
        /// <param name="disclaimerType">Disclaimer Type.</param>
        /// <returns>Body with added disclaimer.</returns>
        private static string AddDisclaimer(string body, bool isHtmlFormat, E_DisclaimerType disclaimerType)
        {
            string disclaimer;
            
            switch (disclaimerType)
            {
                case E_DisclaimerType.NORMAL:
                    disclaimer = "This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to which they are addressed. If you have received this email in error, please note that any use, copying or distribution of this e-mail, in whole or in part, is strictly prohibited. Please notify the sender by return e-mail of the error and delete this e-mail from your system. Finally, the recipient should check this email and any attachments for the presence of viruses. The company accepts no liability for any damage caused by any virus transmitted by this email.";
                    break;

                case E_DisclaimerType.CONSUMER:
                    disclaimer = "This email and any files transmitted with it are confidential and intended solely for the use of the individual or entity to which they are addressed. If you have received this email in error, please notify the sender of the error and delete this e-mail from your system.";
                    break;
                case E_DisclaimerType.NONE:
                    disclaimer = string.Empty;
                    break;
                default:
                    throw new UnhandledEnumException(disclaimerType);
            }
        
            string openTags = "<br /><br /><br /><hr><p style=\"font:Verdana; font-style:italic; font-size:12; color:Gray\">";
            string closeTags = "</p>";

            if (isHtmlFormat)
            {
                var disclaimerHtml = openTags + disclaimer + closeTags;
                var lastIndexOfBodyTag = body.ToLower().LastIndexOf("</body>");
                if (lastIndexOfBodyTag == -1)
                {
                    return body + disclaimerHtml;
                }

                return body.Substring(0, lastIndexOfBodyTag) + disclaimerHtml + body.Substring(lastIndexOfBodyTag);
            }
            else
            {
                if (string.IsNullOrEmpty(disclaimer))
                {
                    return body;
                }

                return body + "\r\n\r\n\r\n\r\n" + disclaimer;
            }
        }

        /// <summary>
        /// Converts <see cref="CBaseEmail"/> to .Net <see cref="System.Net.Mail.MailMessage"/>.
        /// </summary>
        /// <param name="cbe">CBaseEmail object.</param>
        /// <returns>MailMessage object.</returns>
        private static System.Net.Mail.MailMessage ConvertToMailMessage(LendersOffice.Email.CBaseEmail cbe)
        {
            var email = new System.Net.Mail.MailMessage(cbe.From, cbe.To);
            email.Headers.Add("X-Auto-Response-Suppress", "DR, NDR, RN, NRN, OOF, AutoReply");
            email.Subject = SanitizeSubject(cbe.Subject);
            email.Body = AddDisclaimer(cbe.Message, cbe.IsHtmlEmail, cbe.DisclaimerType); // cbe.Message;
            email.IsBodyHtml = cbe.IsHtmlEmail;

            // OPM 461108 - Check if from address has been validated for spoofing.
            // If authorized, set the sender address to the from address.
            // If not authorized, use "On behalf of" and set the from and sender to the LQB default address.
            if (ConstStage.EnableEmailDomainAuthorization)
            {
                if (AuthorizedEmailDomainUtilities.IsAddressFromAuthorizedDomain(email.From, cbe.BrokerId))
                {
                    email.Sender = new System.Net.Mail.MailAddress(email.From.Address);
                }
                else
                {
                    var fromLabel = "On behalf of " + email.From.Address;
                    email.From = new System.Net.Mail.MailAddress(ConstStage.DefaultSenderAddress, fromLabel);
                    email.Sender = new System.Net.Mail.MailAddress(ConstStage.DefaultSenderAddress);
                }
            }

            System.Net.Mail.AlternateView av = null;
            if (cbe.IsHtmlEmail)
            {
                av = System.Net.Mail.AlternateView.CreateAlternateViewFromString(email.Body, System.Text.UTF8Encoding.Default, "text/html");
                email.AlternateViews.Add(av);
                email.Body = string.Empty;
            }

            if (!string.IsNullOrEmpty(cbe.CCRecipient))
            {
                email.CC.Add(cbe.CCRecipient);
            }

            if (cbe.BccToSupport && ConstStage.IsEnableBCCToSystemSentBox)
            {
                email.Bcc.Add(ConstApp.BCCEMAIL);
            }

            foreach (var cattachment in cbe.Attachments)
            {
                if (cattachment.IsLinkedResource && av == null)
                {
                    continue;
                }

                byte[] data;
                if (cattachment.WasBinary)
                {
                    data = Convert.FromBase64String(cattachment.Data);
                }
                else
                {
                    data = UTF8Encoding.UTF8.GetBytes(cattachment.Data);
                }

                MemoryStream ms = new MemoryStream(data);
                ms.Position = 0;
                if (cattachment.IsLinkedResource)
                {
                    var resource = new System.Net.Mail.LinkedResource(ms);
                    resource.ContentId = cattachment.Name;
                    av.LinkedResources.Add(resource);
                }
                else
                {
                    var attachment = new System.Net.Mail.Attachment(ms, cattachment.Name);
                    email.Attachments.Add(attachment);
                }
            }

            return email;
        }

        /// <summary>
        /// Converts <see cref="CBaseEmail"/> to .Net <see cref="LqbGrammar.Drivers.Emailer.EmailPackage"/>.
        /// </summary>
        /// <param name="cbe">CBaseEmail to convert.</param>
        /// <returns>EmailPackage object.</returns>
        private static LqbGrammar.Drivers.Emailer.EmailPackage ConvertToMailMessageNEW(LendersOffice.Email.CBaseEmail cbe)
        {
            LqbGrammar.DataTypes.EmailAddress? fromAddress = LqbGrammar.DataTypes.EmailAddress.Create(cbe.From);
            if (fromAddress == null)
            {
                throw new LqbGrammar.Exceptions.DeveloperException(LqbGrammar.DataTypes.ErrorMessage.SystemError);
            }

            var listTo = LqbGrammar.DataTypes.EmailAddress.ParsePotentialList(cbe.To);
            if (listTo.Count == 0)
            {
                throw new LqbGrammar.Exceptions.DeveloperException(LqbGrammar.DataTypes.ErrorMessage.SystemError);
            }

            LqbGrammar.DataTypes.EmailSubject? subject = LqbGrammar.DataTypes.EmailSubject.Create(SanitizeSubject(cbe.Subject));
            if (subject == null)
            {
                throw new LqbGrammar.Exceptions.DeveloperException(LqbGrammar.DataTypes.ErrorMessage.SystemError);
            }

            LqbGrammar.Drivers.Emailer.EmailBody body;
            if (cbe.IsHtmlEmail)
            {
                body = new LqbGrammar.Drivers.Emailer.EmailHtmlBody();
            }
            else
            {
                body = new LqbGrammar.Drivers.Emailer.EmailTextBody();
            }

            body.AppendText(AddDisclaimer(cbe.Message, cbe.IsHtmlEmail, cbe.DisclaimerType));

            var email = new LqbGrammar.Drivers.Emailer.EmailPackage(cbe.BrokerId, fromAddress.Value, subject.Value, body, listTo.ToArray());

            if (!string.IsNullOrEmpty(cbe.CCRecipient))
            {
                var listCC = LqbGrammar.DataTypes.EmailAddress.ParsePotentialList(cbe.CCRecipient);
                foreach (var cc in listCC)
                {
                    email.AddCc(cc);
                }
            }

            if (cbe.BccToSupport && ConstStage.IsEnableBCCToSystemSentBox)
            {
                LqbGrammar.DataTypes.EmailAddress? bccAddress = LqbGrammar.DataTypes.EmailAddress.Create(ConstApp.BCCEMAIL);
                email.AddBcc(bccAddress.Value);
            }

            foreach (var cattachment in cbe.Attachments)
            {
                if (cattachment.IsLinkedResource && !cbe.IsHtmlEmail)
                {
                    continue;
                }

                if (cattachment.WasBinary)
                {
                    byte[] data = Convert.FromBase64String(cattachment.Data);
                    var binaryData = LqbGrammar.DataTypes.TypedResource<byte[]>.CreateBinary(data, MimeType(data), cattachment.Name);
                    email.AddAttachment(binaryData);
                }
                else
                {
                    var mimeType = cattachment.IsLinkedResource ? System.Net.Mime.MediaTypeNames.Text.Html : System.Net.Mime.MediaTypeNames.Text.Plain;
                    var textData = LqbGrammar.DataTypes.TypedResource<string>.CreateText(cattachment.Data, mimeType, cattachment.Name);
                    email.AddAttachment(textData);
                }
            }

            return email;
        }

        /// <summary>
        /// Parses MIME type from byte array.
        /// </summary>
        /// <param name="data">Data to parse.</param>
        /// <returns>MIME type.</returns>
        private static string MimeType(byte[] data)
        {
            return IsPDF(data) ? System.Net.Mime.MediaTypeNames.Application.Pdf : System.Net.Mime.MediaTypeNames.Image.Gif;
        }

        /// <summary>
        /// Checks if MIME type is PDF.
        /// </summary>
        /// <param name="data">Data to parse.</param>
        /// <returns>True, if it's a PDF.</returns>
        private static bool IsPDF(byte[] data)
        {
            if (data[0] != Convert.ToByte('%'))
            {
                return false;
            }

            if (data[1] != Convert.ToByte('P'))
            {
                return false;
            }

            if (data[2] != Convert.ToByte('D'))
            {
                return false;
            }

            if (data[3] != Convert.ToByte('F'))
            {
                return false;
            }

            if (data[4] != Convert.ToByte('-'))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Send exception to a list of recipients.
        /// </summary>
        /// <param name="discType">Disclaimer type.</param>
        /// <param name="from">Email "From" address.</param>
        /// <param name="recipientList">List of email recipients.</param>
        /// <param name="subject">Email subject.</param>
        /// <param name="body">Email body.</param>
        /// <param name="requiresBCC">True, if email should be BCCed to support.</param>
        private static void Send(E_DisclaimerType discType, string from, string recipientList, string subject, string body, bool requiresBCC) 
        {
            if (ConstStage.BanIpsFromEmail != string.Empty)
            {
                string[] ipList = ConstStage.BanIpsFromEmail.Split(';');
                string clientIp = RequestHelper.ClientIP;
                foreach (var ip in ipList)
                {
                    if (ip.Equals(clientIp))
                    {
                        return; // 11/3/2010 dd - No not send out email if client IP is in a ban ip list.
                    }
                }
            }

            if (!string.IsNullOrEmpty(recipientList)) 
            {
                if (string.IsNullOrEmpty(from))
                {
                    from = DefaultErrorEmailFromAddress; // 8/24/2006 dd - Default from email address.
                }

                if (body.Length > 150000)
                {
                    body = body.Substring(0, 150000) + "-TRUNCATED-"; // av opm 16492 limit to 150 kb 
                }
                
                var cbe = new LendersOffice.Email.CBaseEmail(ConstAppDavid.SystemBrokerGuid)
                {
                    DisclaimerType = discType,
                    From = from,
                    To = recipientList,
                    Subject = subject,
                    Message = body,
                    IsHtmlEmail = false,
                    BccToSupport = requiresBCC
                };

                SendEmail(cbe);
            }
        }

        /// <summary>
        /// Updates exception counters and sends a critical email
        /// if the counter for a particular exception surpasses
        /// the value of ConstApp.ExceptionCountForCriticalCase.
        /// </summary>
        /// <param name="from">Email "From" address.</param>
        /// <param name="message">Message to send.</param>
        /// <param name="exc">Exception to send.</param>
        /// <param name="uniqueKey">Unique key for exception being sent.</param>
        private static void UpdateExceptionTracking(string from, string message, Exception exc, string uniqueKey) 
        {
            if (ShouldSkipOurEmailQueue())
            {
                // 8/2/2013 dd - OPM 125817 - If the error is cause by our IT network
                // vulnerability then skip it.
                return;
            }

            if (string.IsNullOrEmpty(uniqueKey)) 
            {
                // 10/6/2014 dd - If unique key is not specify then generate from exception object.
                uniqueKey = Tools.GenerateUniqueString(exc);
            }

            DateTime lastEmailedToOpmDate = DateTime.MinValue;
            DateTime lastEmailedToCriticalDate = DateTime.MinValue;
            int currentCount = 0;
            bool isNewErrorMessage = false;

            string errorKey = "ERR_UNIQUE_" + uniqueKey;
            if (string.IsNullOrEmpty(uniqueKey) == false)
            {
                // Only send similar exception every 3 hours.
                if (EmailUtilities.IsErrorMessageLastSendExceed(errorKey, ConstStage.MinutesToDedupException, out currentCount, out isNewErrorMessage, out lastEmailedToOpmDate, out lastEmailedToCriticalDate) == false)
                {
                    return;
                }
            }

            if (currentCount > ConstApp.ExceptionCountForCriticalCase
                && (lastEmailedToCriticalDate == DateTime.MinValue || lastEmailedToCriticalDate.DayOfYear != DateTime.Today.DayOfYear)
                && !string.IsNullOrEmpty(ConstStage.EmailAddressOfCriticalOPM))
            {
                string body = GenerateExceptionEmailBody(message, exc);
                string subject = GenerateExceptionEmailSubject(exc);

                // This actually sends the email.
                body = $"This exception has occurred TOO MANY TIMES ({currentCount} times). Please investigate. {Environment.NewLine}{body}";
                Send(E_DisclaimerType.NORMAL, from, ConstStage.EmailAddressOfCriticalOPM, subject, body, false);
                UpdateLastEmailErrorTimestamp(errorKey, DateTime.Now, DateTime.Now);
            }
        }

        /// <summary>
        /// Generates the body for an exception email.
        /// </summary>
        /// <param name="message">Message from developer.</param>
        /// <param name="exc">The exception this email is about.</param>
        /// <returns>Email body.</returns>
        private static string GenerateExceptionEmailBody(string message, Exception exc)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine($"Server: {ConstAppDavid.ServerName}");

            PerformanceMonitorItem item = PerformanceMonitorItem.Current;

            sb.AppendLine();

            sb.AppendLine($"Application: {ConstAppDavid.CurrentDomainFriendlyName}");

            sb.AppendLine("==============================");

            sb.Append("Exception Message: ");
            if (!string.IsNullOrEmpty(message))
            {
                sb.AppendLine(message);
            }

            sb.AppendLine(exc.Message);

            if (exc.InnerException != null)
            {
                // 10/13/2004 kb - We are getting more nested with our throwing,
                // so we need to dump all the errors.  It would also be nice to
                // pull out all the embedded error strings of each sql exception
                // we come across.
                Exception innerException = exc.InnerException;
                int iLevel = 0;

                while (innerException != null)
                {
                    sb.AppendLine($"[{iLevel}] {innerException.Message}");

                    innerException = innerException.InnerException;

                    ++iLevel;
                }
            }

            sb.AppendLine("------------------------------");

            sb.AppendLine("Exception Stack Trace:");
            sb.AppendLine(exc.StackTrace);

            sb.AppendLine("------------------------------");

            sb.AppendLine("Inner Exception:");

            sb.AppendLine(exc.InnerException?.ToString());

            sb.AppendLine("------------------------------");

            GenerateRequestInformation(sb);
            return sb.ToString();
        }

        /// <summary>
        /// Generates the subject of an exception email.
        /// </summary>
        /// <param name="exc">The exception this email is about.</param>
        /// <returns>Email subject.</returns>
        private static string GenerateExceptionEmailSubject(Exception exc)
        {
            // 4/3/2006 dd - Prevent subject in email too exceed length limit.
            // 05/15/2018 je - OPM 464897 - Subject should not contain exception message.
            string subject = "[LoException] ";
            if (exc is DataAccess.CBaseException)
            {
                string code = ((DataAccess.CBaseException)exc).EmailSubjectCode;
                if (!string.IsNullOrEmpty(code))
                {
                    subject += code + " ";
                }
            }
            else if (exc is System.OutOfMemoryException)
            {
                subject += "OUT_OF_MEMORY-";
            }
            
            string typeName = exc.GetType().FullName;
            subject += Tools.TruncateString(typeName, 40);
            if (typeName.Length > 40)
            {
                subject += "...";
            }

            return subject;
        }

        /// <summary>
        /// Per OPM 125817, we can filter out users from certain IPs. 
        /// <para></para>
        /// This method should not throw any exceptions.
        /// </summary>
        /// <returns>True if should skip email queue.</returns>
        private static bool ShouldSkipOurEmailQueue()
        {
            var clientIPAddress = RequestHelper.ClientIP;
            if (clientIPAddress == string.Empty)
            {
                return false;
            }

            System.Net.IPAddress clientIP;
            if (!System.Net.IPAddress.TryParse(clientIPAddress, out clientIP))
            {
                return false;
            }

            foreach (var address in ConstStage.VulnerabilityScanWhitelistedIps)
            {
                if (clientIPAddress == address)
                {
                    return true;
                }
            }

            foreach (var address in X_IP_ADDRESSES_TO_SKIP_EMAIL)
            {
                if (clientIPAddress == address)
                {
                    return true;
                }
            }

            foreach (var addressRange in X_IP_RANGES_TO_SKIP_EMAIL)
            {
                if (clientIPAddress.StartsWith(addressRange))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Update last email error timestamp.
        /// </summary>
        /// <param name="id">Error message ID.</param>
        /// <param name="lastEmailedToOpmDate">Last emailed to OPM date.</param>
        /// <param name="lastEmailedToCriticalDate">Last emailed to critical date.</param>
        private static void UpdateLastEmailErrorTimestamp(string id, DateTime lastEmailedToOpmDate, DateTime lastEmailedToCriticalDate)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@ErrorMessage", id));

            if (lastEmailedToCriticalDate != DateTime.MinValue)
            {
                parameters.Add(new SqlParameter("@LastEmailedToCriticalDate", lastEmailedToCriticalDate));
            }

            if (lastEmailedToOpmDate != DateTime.MinValue)
            {
                parameters.Add(new SqlParameter("@LastEmailedToOpmDate", lastEmailedToOpmDate));
            }

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "ERROR_UpdateEmailDate", 3, parameters.ToArray());
        }

        /// <summary>
        /// Generate request information.
        /// </summary>
        /// <param name="sb"><see cref="StringBuilder"/> information is appended to.</param>
        private static void GenerateRequestInformation(StringBuilder sb) 
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            HttpRequest request = null;

            if (context != null)
            {
                try
                {
                    request = context.Request;
                }
                catch (HttpException)
                {
                }
            }

            if (request != null) 
            {
                // Display user request information.
                sb.AppendLine();
                sb.AppendLine();
                sb.AppendLine();
                sb.AppendLine("<-- Request Information -->");
                sb.AppendLine($"TIME OF EXCEPTION = {DateTime.Now:G}");
                sb.AppendLine($"HTTP HOST         = {request.Url}");
                sb.AppendLine($"URL REFERRER      = {request.UrlReferrer}");
                sb.AppendLine($"HTTP METHOD       = {request.HttpMethod}");
                sb.AppendLine($"APPLICATION PATH  = {request.ApplicationPath}");
                sb.AppendLine($"HEADERS           = {request.Headers.ToString()}");
                sb.AppendLine($"USER AGENT        = {request.UserAgent}");
                sb.AppendLine($"USER PRINCIPAL    = {context.User}");
                sb.AppendLine($"USER IP           = {RequestHelper.ClientIP}");

                sb.AppendLine("<-- Start request string values -->");

                string[] keys = request.QueryString.AllKeys;
                foreach (string k in keys) 
                {
                    sb.AppendLine($"{k} = {request.QueryString[k]}");
                }

                sb.AppendLine("<-- End request string values -->");

                sb.AppendLine("<-- Start postback values -->");

                keys = new string[]
                {
                    "__EVENTTARGET", "__EVENTARGUMENT"
                };

                foreach (string k in keys) 
                {
                    sb.AppendLine($"{k} = {request.Form[k]}");
                }

                sb.AppendLine("<-- End postback values -->");
            }
        }

        /// <summary>
        /// Sanatizes email subject line.
        /// </summary>
        /// <remarks>
        /// OPM 221811, 8/4/2015, ML.
        /// The problem we were having stems from the subject check in the MailMessage class.
        /// See http://stackoverflow.com/a/7239760.
        /// </remarks>
        /// <param name="subject">Subject line to sanatize.</param>
        /// <returns>Sanitized subject line.</returns>
        private static string SanitizeSubject(string subject)
        {
            if (string.IsNullOrEmpty(subject))
            {
                return string.Empty;
            }

            subject = subject.TrimWhitespaceAndBOM();

            // Remove these characters from the subject, which cause an ArgumentException
            // to be raised from the MailMessage.Subject setter.
            subject = subject.Replace("\r\n", " ").Replace('\r', ' ').Replace('\n', ' ');

            // Per RFC 2822 section 2.1.1, http://www.faqs.org/rfcs/rfc2822.html. 
            // We truncate to the more conservative 78 characters for display purposes.
            // OPM 244822, 8/25/2016, ML - Expanding to max length of 255 characters.
            subject = subject.Truncate(ConstAppDavid.EmailSubjectMaxLength);

            return subject;
        }

        /// <summary>
        /// Old Method of ending an XSS warning email.
        /// </summary>
        /// <param name="emailServer">SMTP server URL.</param>
        /// <param name="portNumber">SMTP port number.</param>
        /// <param name="notifyEmail">Email send to address.</param>
        /// <param name="notifyName">Email send to name.</param>
        /// <param name="url">Page where error occured.</param>
        /// <param name="txt">Error text.</param>
        private static void SendCrossScriptingWarningOLD(string emailServer, int portNumber, string notifyEmail, string notifyName, string url, string txt)
        {
            System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();
            System.Net.Mail.MailAddress fromAddress = new System.Net.Mail.MailAddress(DefaultErrorEmailFromAddress);
            System.Net.Mail.MailAddress sendToAddress = new System.Net.Mail.MailAddress(notifyEmail, notifyName);

            mailMessage.From = fromAddress;
            mailMessage.To.Add(sendToAddress);
            mailMessage.Subject = SanitizeSubject("[XSS] - " + url);

            mailMessage.Body = txt;
            System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(emailServer, portNumber);
            smtpClient.Send(mailMessage);
        }

        /// <summary>
        /// Uses FOOL email classes to send XSS warning email.
        /// </summary>
        /// <param name="emailServer">SMTP server URL.</param>
        /// <param name="portNumber">SMTP port number.</param>
        /// <param name="notifyEmail">Email send to address.</param>
        /// <param name="url">Page where error occured.</param>
        /// <param name="txt">Error text.</param>
        private static void SendCrossScriptingWarningNEW(string emailServer, int portNumber, LqbGrammar.DataTypes.EmailAddress notifyEmail, string url, string txt)
        {
            LqbGrammar.DataTypes.EmailServerName? serverName = LqbGrammar.DataTypes.EmailServerName.Create(emailServer);
            if (serverName == null)
            {
                throw new LqbGrammar.Exceptions.DeveloperException(LqbGrammar.DataTypes.ErrorMessage.BadConfiguration);
            }

            LqbGrammar.DataTypes.PortNumber? port = LqbGrammar.DataTypes.PortNumber.Create(portNumber);
            if (port == null)
            {
                throw new LqbGrammar.Exceptions.DeveloperException(LqbGrammar.DataTypes.ErrorMessage.BadConfiguration);
            }

            LqbGrammar.DataTypes.EmailAddress? fromAddress = LqbGrammar.DataTypes.EmailAddress.Create(DefaultErrorEmailFromAddress);
            if (fromAddress == null)
            {
                throw new LqbGrammar.Exceptions.DeveloperException(LqbGrammar.DataTypes.ErrorMessage.BadConfiguration);
            }

            LqbGrammar.DataTypes.EmailAddress sendToAddress = notifyEmail;

            LqbGrammar.DataTypes.EmailSubject? subject = LqbGrammar.DataTypes.EmailSubject.Create("[XSS] - " + url);
            if (subject == null)
            {
                throw new LqbGrammar.Exceptions.DeveloperException(LqbGrammar.DataTypes.ErrorMessage.SystemError);
            }

            var body = new LqbGrammar.Drivers.Emailer.EmailTextBody();
            body.AppendText(txt);

            var email = new LqbGrammar.Drivers.Emailer.EmailPackage(ConstAppDavid.SystemBrokerGuid, fromAddress.Value, subject.Value, body, sendToAddress);

            LendersOffice.Drivers.Emailer.EmailHelper.SendEmail(serverName.Value, port.Value, email);
        }
    }
}