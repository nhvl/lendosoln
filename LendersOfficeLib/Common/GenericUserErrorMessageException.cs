/// Author: David Dao

using System;
using DataAccess;

namespace LendersOffice.Common
{

	public class GenericUserErrorMessageException : CBaseException
	{
		public GenericUserErrorMessageException(string devMessage) : base(ErrorMessages.Generic, devMessage)
		{
		}

        public GenericUserErrorMessageException(Exception innerException) : base(ErrorMessages.Generic, innerException) 
        {
        }
	}
}
