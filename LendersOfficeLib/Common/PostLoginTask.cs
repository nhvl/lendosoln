﻿// <copyright file="PostLoginTask.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Jhairo Erazo
//    Date:   8/22/2016
// </summary>
namespace LendersOffice.Common
{
    /// <summary>
    /// A set of tasks that need to be performed after the user logs in.
    /// </summary>
    /// <remarks>
    /// When adding new tasks to this list, make sure to edit the switch statement in RequestHelper::DoPostLoginTasks.
    /// Keep tasks in the order in which they must be performed. These enum values should not be stored anywhere,
    /// so it should be fine to renumber them. Just make sure to use the actual enums and not their int values
    /// in any code that references them.
    /// </remarks>
    public enum PostLoginTask
    {
        /// <summary>
        /// Just logged in, No post login stuff done yet.
        /// </summary>
        Login = 0,

        /// <summary>
        /// Run the check that warns and then blocks users
        /// from authenticating into LQB if BrowserXT is
        /// detected on the user's work station.
        /// </summary>
        RunBrowserXtVerificationCheck = 1,

        /// <summary>
        /// Check if password needs to be changed.
        /// </summary>
        ChangePassword = 2,

        /// <summary>
        /// Check if license needs to be renewed or has expired.
        /// </summary>
        CheckLicensing = 3,

        /// <summary>
        /// Check if user must accept our terms of agreement.
        /// </summary>
        AcceptTerms = 4,

        /// <summary>
        /// Check if user needs to setup security questions.
        /// </summary>
        SetSecurityQuestions = 5,

        /// <summary>
        /// At this point all checks are done, and user should be redirected to desired destination page.
        /// </summary>
        Redirect = 6
    }
}
