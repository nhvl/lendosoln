/// Author: David Dao

using System;

namespace LendersOffice.Common
{
	public class SystemStartupException : DataAccess.CBaseException
	{
		public SystemStartupException(Exception exc) : base("SystemStartupCheck FAILED!!", exc.ToString())
		{
		}
        public override string EmailSubjectCode 
        {
            get { return "SYSTEM_STARTUP_FAILED"; }
        }
	}
}
