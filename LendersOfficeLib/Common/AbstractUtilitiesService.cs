﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;
using DataAccess;
using EDocs;
using ExpertPdf.HtmlToPdf;
using LendersOffice.Admin;
using LendersOffice.Common.SerializationTypes;
using LendersOffice.Constants;
using LendersOffice.Conversions;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.DistributeUnderwriting.Model;
using LendersOffice.Drivers.Gateways;
using LendersOffice.Integration.DocumentVendor;
using LendersOffice.ObjLib.DocumentGeneration;
using LendersOffice.ObjLib.Task;
using LendersOffice.Rolodex;
using LendersOffice.Security;
using PdfRasterizerLib;

namespace LendersOffice.Common
{
    /// <summary>
    /// Make this class abstract because it will be share between multiple projects. LO & PML.
    /// </summary>
    public abstract class AbstractUtilitiesService : BaseSimpleServiceXmlPage
    {
        private static DateTime EpochDateTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        protected abstract Guid BrokerID
        {
            get;
        }
        protected BrokerDB Broker
        {
            get
            {
                return PrincipalFactory.CurrentPrincipal.BrokerDB;
            }
        }

        protected abstract bool HasPermission(Permission p);

        protected override void Process(string methodName)
        {
            switch (methodName)
            {
                case "smartzipcode":
                    ProcessSmartZipcode();
                    break;
                case "AddToRolodex":
                    AddToRolodex();
                    break;
                case "PopulateFromOfficialContactList":
                    PopulateFromOfficialContactList();
                    break;
                case "GetCountiesForState": //1-10-28 av 18913 
                    this.GetCounties();
                    break;
                case "CheckDODULoginFields":
                    CheckDODULoginFields();
                    break;
                case "ClearDODULoginName":
                    ClearDODULoginName();
                    break;
                case "GetDoDuDuplicates":
                    GetDoDuDuplicates();
                    break;
                case "ValidateFieldId":
                    ValidateFieldId();
                    break;
                case "CacheLoans":
                    CacheLoans();
                    break;
                case "CheckFindingsData":
                    CheckFindingsData();
                    break;
                case "GetCurrentTicks":
                    GetCurrentTicks();
                    break;
                case "CreateFhaConEdoc":
                    SaveFHAConnectionEDoc();
                    break;
                case "UniqueId":
                    SetResult("Id", Guid.NewGuid().ToString());
                    break;
                case "GetFeeTypeProperties":
                    GetFeeTypeProperties();
                    break;
                case "ProcessDateOffset":
                    ProcessDateOffset();
                    break;
                case "MarkSlowness":
                    MarkSlowness();
                    break;
                case "CreateTestLoan":
                    CreateNewLoanFromCurrent(CreateTestLoan);
                    break;
                case "CreateDuplicateLoan":
                    CreateNewLoanFromCurrent(CreateDuplicateLoan);
                    break;
                case "CreateSandboxLoan":
                    CreateNewLoanFromCurrent(CreateSandboxLoan);
                    break;
                case "CreateTemplateFromLoan":
                    CreateNewLoanFromCurrent(CreateTemplateFromLoan);
                    break;
                case "GetBranchCustomPricingPolicyData":
                    GetBranchCustomPricingPolicyData();
                    break;
                case "UpdateStatusOfArchiveInUnknownStatus":
                    UpdateStatusOfArchiveInUnknownStatus();
                    break;
                case "UpdateStatusesOfBothLeAndCdArchivesInUnknownStatus":
                    UpdateStatusesOfBothLeAndCdArchivesInUnknownStatus();
                    break;
                case "UpdatePendingLoanEstimateArchiveStatus":
                    UpdatePendingLoanEstimateArchiveStatus();
                    break;
                case "GetDocumentPackageInfo":
                    GetDocumentPackageInfo();
                    break;
                case "PrepareUnknownLoanEstimateArchiveForResubmission":
                    PrepareUnknownLoanEstimateArchiveForResubmission();
                    break;
                case "CheckForDocumentGenerationResult":
                    CheckForDocumentGenerationResult();
                    break;
                case "RecordClientTiming":
                    RecordClientTiming();
                    break;
                case nameof(ClearRespaEnteredDate):
                    ClearRespaEnteredDate();
                    break;
                case nameof(GetIdsLoanFileLink):
                    GetIdsLoanFileLink();
                    break;
                case "StoreIdListToCache":
                    this.StoreIdListToCache();
                    break;
                case "RetrieveIdListFromCache":
                    this.RetrieveIdListFromCache();
                    break;
                case "AssociateDocsWithTask":
                    this.AssociateDocsWithTask();
                    break;
            }
        }

        private void AssociateDocsWithTask()
        {
            var taskId = this.GetString("TaskId");
            var documentIdJson = this.GetString("DocumentIdJson");

            var task = Task.Retrieve(this.BrokerID, taskId);
            var docIds = task.AssociatedDocs.Select(association => association.DocumentId).ToList();

            var newDocIds = SerializationHelper.JsonNetDeserialize<List<Guid>>(documentIdJson);
            docIds.AddRange(newDocIds);
            task.UpdateDocumentAssociations(docIds);

            task.Save(false);
        }

        private void StoreIdListToCache()
        {
            var idListJson = this.GetString("IdList");
            var cacheKey = Guid.NewGuid();

            AutoExpiredTextCache.AddToCacheByUser(PrincipalFactory.CurrentPrincipal, idListJson, TimeSpan.FromMinutes(5), cacheKey);
            this.SetResult("CacheKey", cacheKey);
        }

        private void RetrieveIdListFromCache()
        {
            var cacheKey = this.GetString("CacheKey");
            var cacheResult = AutoExpiredTextCache.GetFromCacheByUser(PrincipalFactory.CurrentPrincipal, cacheKey);

            if (cacheResult == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "ID list cache expired for key " + cacheKey);
            }

            this.SetResult("IdList", cacheResult);
        }

        private void GetIdsLoanFileLink()
        {
            var brokerUser = BrokerUserPrincipal.CurrentPrincipal;
            var loanId = this.GetGuid("LoanId");
            var vendorId = this.GetGuid("VendorId");

            IDocumentVendor vendor = DocumentVendorFactory.Create(PrincipalFactory.CurrentPrincipal.BrokerId, vendorId);
            DocumentVendorResult<LqbGrammar.DataTypes.LqbAbsoluteUri> loanFileLinkResult = vendor.GetIdsLoanFileLink(loanId, brokerUser);
            this.SetResult("success", !loanFileLinkResult.HasError);
            this.SetResult("url", loanFileLinkResult.Result);
            this.SetResult("error", loanFileLinkResult.ErrorMessage);
        }

        private void ClearRespaEnteredDate()
        {
            var loanId = this.GetGuid("LoanId");
            var fieldIdToClear = this.GetString("FieldIdToClear");
            var applicationId = this.GetGuid("ApplicationId", Guid.Empty);

            // We will check for a valid field id at the lower level, too.
            // We perform a check here since we can prevent an unnecessary
            // InitSave and workflow check. 
            if (!CPageBase.IsValidRespaEnteredDFieldId(fieldIdToClear))
            {
                throw new CBaseException(
                    ErrorMessages.Generic,
                    $"Invalid RESPA entered date field id: {fieldIdToClear}.");
            }

            var loan = CPageData.CreateUsingSmartDependency(
                loanId,
                typeof(AbstractUtilitiesService));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);
            loan.ClearRespaFieldEnteredD(fieldIdToClear, applicationId);
            loan.Save();

            this.SetResult(nameof(loan.sRespa6D), loan.sRespa6D_rep);
            this.SetResult(nameof(loan.sRespa6DLckd), loan.sRespa6DLckd);
            this.SetResult(nameof(loan.sRespaBorrowerNameCollectedD), loan.sRespaBorrowerNameCollectedD_rep);
            this.SetResult(nameof(loan.sRespaBorrowerNameCollectedDLckd), loan.sRespaBorrowerNameCollectedDLckd);
            this.SetResult(nameof(loan.sRespaBorrowerNameFirstEnteredD), loan.sRespaBorrowerNameFirstEnteredD_rep);
            this.SetResult(nameof(loan.sRespaBorrowerSsnCollectedD), loan.sRespaBorrowerSsnCollectedD_rep);
            this.SetResult(nameof(loan.sRespaBorrowerSsnCollectedDLckd), loan.sRespaBorrowerSsnCollectedDLckd);
            this.SetResult(nameof(loan.sRespaBorrowerSsnFirstEnteredD), loan.sRespaBorrowerSsnFirstEnteredD_rep);
            this.SetResult(nameof(loan.sRespaIncomeCollectedD), loan.sRespaIncomeCollectedD_rep);
            this.SetResult(nameof(loan.sRespaIncomeCollectedDLckd), loan.sRespaIncomeCollectedDLckd);
            this.SetResult(nameof(loan.sRespaIncomeFirstEnteredD), loan.sRespaIncomeFirstEnteredD_rep);
            this.SetResult(nameof(loan.sRespaPropAddressCollectedD), loan.sRespaPropAddressCollectedD_rep);
            this.SetResult(nameof(loan.sRespaPropAddressCollectedDLckd), loan.sRespaPropAddressCollectedDLckd);
            this.SetResult(nameof(loan.sRespaPropAddressFirstEnteredD), loan.sRespaPropAddressFirstEnteredD_rep);
            this.SetResult(nameof(loan.sRespaPropValueCollectedD), loan.sRespaPropValueCollectedD_rep);
            this.SetResult(nameof(loan.sRespaPropValueCollectedDLckd), loan.sRespaPropValueCollectedDLckd);
            this.SetResult(nameof(loan.sRespaPropValueFirstEnteredD), loan.sRespaPropValueFirstEnteredD_rep);
            this.SetResult(nameof(loan.sRespaLoanAmountCollectedD), loan.sRespaLoanAmountCollectedD_rep);
            this.SetResult(nameof(loan.sRespaLoanAmountCollectedDLckd), loan.sRespaLoanAmountCollectedDLckd);
            this.SetResult(nameof(loan.sRespaLoanAmountFirstEnteredD), loan.sRespaLoanAmountFirstEnteredD_rep);
        }

        private DateTime GetLocalDateTime(string name)
        {
            long ticks = GetLong(name, 0);

            if (ticks == 0)
            {
                return DateTime.MinValue;
            }

            return EpochDateTime.AddMilliseconds(ticks).ToLocalTime();
        }

        private Guid GetShortenGuidOrGuid(string name)
        {
            string v = GetString(name, string.Empty);
            if (string.IsNullOrEmpty(v))
            {
                return Guid.Empty;
            }

            Guid guid = Guid.Empty;

            if (Guid.TryParse(v, out guid) == false)
            {
                // The value is in Guid shorten form.
                guid = Tools.GetGuidFrom(v);
            }

            return guid;

        }
        private void RecordClientTiming()
        {
            DateTime start = GetLocalDateTime("ClientPriceTiming_Start");
            DateTime beforeSubmit = GetLocalDateTime("ClientPriceTiming_BeforeSubmit");
            DateTime renderResultStart = GetLocalDateTime("ClientPriceTiming_RenderResultStart");
            DateTime renderResultEnd = GetLocalDateTime("ClientPriceTiming_RenderResultEnd");
            DateTime end = GetLocalDateTime("ClientPriceTiming_End");
            TimeSpan ts = end - start;
            TimeSpan timeClickToResult = renderResultStart - start;
            TimeSpan timeRenderResult = renderResultEnd - renderResultStart;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("ClientPriceTiming_Start=" + start);
            sb.AppendLine("ClientPriceTiming_BeforeSubmit=" + beforeSubmit);
            sb.AppendLine("ClientPriceTiming_RenderResultStart=" + renderResultStart);
            sb.AppendLine("ClientPriceTiming_RenderResultEnd=" + renderResultEnd);
            sb.AppendLine("ClientPriceTiming_End=" + end);
            sb.AppendLine("TotalMs=" + ts.TotalMilliseconds);
            sb.AppendLine("timeClickToResult=" + timeClickToResult.TotalMilliseconds);
            sb.AppendLine("timeRenderResult=" + timeRenderResult.TotalMilliseconds);
            sb.AppendLine("ClientPriceTiming_ServerPolling=" + GetString("ClientPriceTiming_ServerPolling", string.Empty));

            var principal = PrincipalFactory.CurrentPrincipal;

            PricingTimingEventLog eventLog = PricingTimingUtils.CreateEventLog();
            eventLog.User = principal.LoginNm;
            eventLog.CustomerCode = principal.BrokerDB.CustomerCode;
            eventLog.LoanId = GetGuid("LoanID", Guid.Empty);
            eventLog.TimingType = "client";
            eventLog.RequestBatchId = GetShortenGuidOrGuid("ClientPriceTiming_RequestId");
            eventLog.Duration = SafeIntConvert(ts.TotalMilliseconds);
            eventLog.ClientWaitForResult = SafeIntConvert(timeClickToResult.TotalMilliseconds);
            eventLog.ClientRenderResult = SafeIntConvert(timeRenderResult.TotalMilliseconds);

            // 2/14/2017 - dd - The cache key is base from MainService.aspx in PML project.
            string cacheKey = $"MergeKeys_{eventLog.RequestBatchId}_{principal.EmployeeId}";
            string jsonServerRequestList = AutoExpiredTextCache.GetFromCache(cacheKey);

            if (!string.IsNullOrEmpty(jsonServerRequestList))
            {
                sb.AppendLine();
                sb.AppendLine("ServerIds=[" + jsonServerRequestList + "]");
            }

            sb.AppendLine();
            sb.AppendLine("ClientPriceTiming_NavTiming=[" + GetString("ClientPriceTiming_NavTiming", string.Empty) + "]");
            eventLog.Details = sb.ToString();
            PricingTimingUtils.Record(eventLog);
        }

        private int SafeIntConvert(double value)
        {
            int result;
            try
            {
                result = Convert.ToInt32(value);
            }
            catch (System.OverflowException exc)
            {
                Tools.LogWarning($"Could not convert : {value}.", exc);
                result = 0;
            }

            return result;
        }

        private void CheckForDocumentGenerationResult()
        {
            var pollingId = this.GetGuid("pollingId");
            var key = Tools.ShortenGuid(pollingId) + Tools.ShortenGuid(PrincipalFactory.CurrentPrincipal.UserId);

            var result = AutoExpiredTextCache.GetFromCache(key);

            // The request status is inserted into the cache prior to the request
            // being sent and it is updated when we receive the result. If we do
            // not find the item then we know that we already retrieved the result.
            // We can stop polling.
            if (result == null)
            {
                SetResult("stopPolling", true);
            }
            else
            {
                var status = SerializationHelper.JsonNetDeserialize<DocumentGenerationRequestStatus>(result);

                if (status.RequestCompleted)
                {
                    var serializedDetails = SerializationHelper.JsonNetSerialize(status.ArchiveDetails);
                    SetResult("serializedDetails", serializedDetails);
                    AutoExpiredTextCache.RemoveFromCacheImmediately(key);
                }
            }
        }

        private void UpdateStatusOfArchiveInUnknownStatus()
        {
            var loanId = this.GetGuid("sLId");
            var status = (ClosingCostArchive.E_ClosingCostArchiveStatus)this.GetInt("status");
            var loan = CPageData.CreateUsingSmartDependency(
                loanId,
                typeof(AbstractUtilitiesService));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);
            loan.UpdateStatusOfArchiveInUnknownStatus(status);
            loan.Save();
        }

        private void UpdateStatusesOfBothLeAndCdArchivesInUnknownStatus()
        {
            var loanId = this.GetGuid("sLId");
            var leStatus = (ClosingCostArchive.E_ClosingCostArchiveStatus)this.GetInt("LEStatus");
            var cdStatus = (ClosingCostArchive.E_ClosingCostArchiveStatus)this.GetInt("CDStatus");
            var loan = CPageData.CreateUsingSmartDependency(
                loanId,
                typeof(AbstractUtilitiesService));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);
            loan.UpdateStatusesOfBothLeCdArchiveInUnknownStatus(leStatus, cdStatus);
            loan.Save();
        }

        private void PrepareUnknownLoanEstimateArchiveForResubmission()
        {
            var loanId = this.GetGuid("sLId");
            var loan = CPageData.CreateUsingSmartDependency(
                loanId,
                typeof(AbstractUtilitiesService));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);
            loan.PrepareUnknownLoanEstimateArchiveForResubmission();
            loan.Save();
        }

        private void UpdatePendingLoanEstimateArchiveStatus()
        {
            var loanId = this.GetGuid("sLId");
            var status = (ClosingCostArchive.E_ClosingCostArchiveStatus)this.GetInt("status");
            var loan = CPageData.CreateUsingSmartDependency(
                loanId,
                typeof(AbstractUtilitiesService));
            loan.InitSave(ConstAppDavid.SkipVersionCheck);
            loan.UpdateLoanEstimateArchiveWithPendingStatus(status);
            loan.Save();
        }

        private void GetDocumentPackageInfo()
        {
            var loanId = this.GetGuid("sLId");
            var vendorId = this.GetGuid("VendorId");
            var packageType = this.GetString("PackageType");
            var vendor = DocumentVendorFactory.Create(PrincipalFactory.CurrentPrincipal.BrokerId, vendorId);
            var package = vendor.Config.RetrievePackage(packageType);
            var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(
                loanId,
                typeof(AbstractUtilitiesService));
            loan.InitLoad();
            var warning = "";
            var pendingArchiveDate = loan.sHasLoanEstimateArchiveInPendingStatus ?
                loan.sLoanEstimateArchiveInPendingStatus.DateArchived :
                "";

            if (package.HasClosingDisclosure &&
                loan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                loan.sHasLoanEstimateArchiveInPendingStatus)
            {
                warning = ErrorMessages.ArchiveError.GeneratingClosingPackageWithPendingArchiveWarning(
                    pendingArchiveDate);
            }

            bool promptForTemporaryArchiveSource = DocumentAuditor.MustSpecifyTemporaryArchiveSource(
                package,
                loan.sDisclosureRegulationT,
                loan.sHasLastDisclosedLoanEstimateArchive,
                loan.sHasLoanEstimateArchiveInPendingStatus);

            string promptMessage = package.HasClosingDisclosure ?
                ErrorMessages.ArchiveError.ShouldPendingLoanEstimateArchiveBeIncludedInClosingDisclosure :
                ErrorMessages.ArchiveError.PendingArchiveWithoutLastDisclosedArchive(pendingArchiveDate);

            bool forbidGeneratingPackage = package.HasLoanEstimate &&
                loan.sHasLoanEstimateArchiveInIncludedInClosingDisclosureStatus;

            string packageGenerationForbiddenMessage = forbidGeneratingPackage ?
                ErrorMessages.ArchiveError.CannotGenerateLEAfterIncludingLEInCD :
                "";

            SetResult("Warning", warning);
            SetResult("PromptForTemporaryArchiveSource", promptForTemporaryArchiveSource);
            SetResult("TemporaryArchiveSourcePrompt", promptMessage);
            SetResult("PackageHasClosingDisclosure", package.HasClosingDisclosure);
            SetResult("ForbidGeneratingPackage", forbidGeneratingPackage);
            SetResult("PackageGenerationForbiddenMessage", packageGenerationForbiddenMessage);
        }

        /// <summary>
        /// This method is used to trigger the loan creation and sets the results.  It takes in a function that handles the loan creation and 
        /// returns a Tuple of type Guid and string that corresponds to the newly created loan.
        /// </summary>
        /// <param name="createNewLoanFromCurrent"></param>
        private void CreateNewLoanFromCurrent(Func<Guid, AbstractUserPrincipal, Tuple<Guid, string>> createNewLoanFromCurrent)
        {
            Guid currentLoanID = GetGuid("LoanID");
            var principal = BrokerUserPrincipal.CurrentPrincipal;
            bool hasError = false;

            try
            {
                var loanIDAndName = createNewLoanFromCurrent(currentLoanID, principal);
                SetResult("NewLoanID", loanIDAndName.Item1);
                SetResult("LoanName", loanIDAndName.Item2);
            }
            catch (CBaseException exc)
            {
                hasError = true;
                Tools.LogErrorWithCriticalTracking(exc);
                SetResult("UserMessage", exc.UserMessage);
            }

            SetResult("Error", hasError);
        }

        /// <summary>
        /// This method creates a duplicate loan.
        /// </summary>
        /// <param name="existingLoanId">The existing loan from which to duplicate from.</param>
        /// <param name="principal">The user's principal.</param>
        /// <returns>Returns a tuple containing the new loan's id and name.</returns>
        private static Tuple<Guid, string> CreateDuplicateLoan(Guid existingLoanId, AbstractUserPrincipal principal)
        {
            if (!principal.HasPermission(Permission.CanDuplicateLoans))
            {
                throw new AccessDenied("Cannot duplicate loan because CanDuplicateLoans is false for user");
            }

            CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.FileDuplication);

            SqlParameter[] parameters =
            {
                new SqlParameter("@LoanID", existingLoanId)
            };
            bool isTemplate = Convert.ToBoolean(StoredProcedureHelper.ExecuteScalar(principal.BrokerId, "IsLoanTemplate", parameters));

            Guid newLoanId;

            if (isTemplate)
            {
                newLoanId = creator.DuplicateLoanTemplate(existingLoanId);
            }
            else
            {
                newLoanId = creator.DuplicateLoanFile(existingLoanId);
            }

            return Tuple.Create(newLoanId, creator.LoanName);
        }

        /// <summary>
        /// This method creates a sandbox loan from the existing loan.
        /// </summary>
        /// <param name="existingLoanId">The id of the loan to create the sandbox from.</param>
        /// <param name="principal">The user's principal.</param>
        /// <returns>Returns a tuple containing the new loan's id and name.</returns>
        private static Tuple<Guid, string> CreateSandboxLoan(Guid existingLoanId, AbstractUserPrincipal principal)
        {
            CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.Sandbox);

            Guid newLoanId = creator.CreateSandboxFile(existingLoanId);

            if (principal.BrokerDB.SandboxScratchLOXml.Length > 0)
            {
                //We should probably do something about the errors.
                LOFormatImporter.Import(newLoanId, PrincipalFactory.CurrentPrincipal, principal.BrokerDB.SandboxScratchLOXml, true);
            }

            return Tuple.Create(newLoanId, creator.LoanName);
        }

        /// <summary>
        /// This method creates a Template from an existing loan.
        /// </summary>
        /// <param name="existingLoanId">The loan to create the template from.</param>
        /// <param name="principal">The user's principal.</param>
        /// <returns>Returns a tuple containing the new loan's id and name.</returns>
        private static Tuple<Guid, string> CreateTemplateFromLoan(Guid existingLoanId, AbstractUserPrincipal principal)
        {
            CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.FromTemplate);
            Guid newLoanId = creator.CreateTemplateFromLoan(existingLoanId);

            return Tuple.Create(newLoanId, creator.LoanName);
        }

        /// <summary>
        /// This method creates a Test loan from an existing loan.
        /// </summary>
        /// <param name="existingLoanID">The id of the loan to create the test loan from.</param>
        /// <param name="principal">The user's principal.</param>
        /// <returns>Returns a tuple containing the new loan's id and name.</returns>
        private static Tuple<Guid, string> CreateTestLoan(Guid existingLoanID, AbstractUserPrincipal principal)
        {
            if (!principal.HasPermission(Permission.AllowCreatingTestLoans))
            {
                throw new AccessDenied("Cannot create test loan because AllowCreatingTestLoans is false for user");
            }

            CLoanFileCreator creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.CreateTest);
            Guid newLoanID = creator.DuplicateLoanIntoTestFile(existingLoanID);
            return Tuple.Create(newLoanID, creator.LoanName);
        }

        private void GetBranchCustomPricingPolicyData()
        {
            Guid branchId = this.GetGuid("branchId");
            Guid brokerId;

            // If called from LOAdmin, take the value from the call.
            // Otherwise, we'll want to grab the principal of the current user.
            if (PrincipalFactory.CurrentPrincipal is InternalUserPrincipal)
            {
                brokerId = this.GetGuid("brokerId");
            }
            else
            {
                brokerId = this.BrokerID;
            }

            var branch = new BranchDB(branchId, brokerId);
            branch.Retrieve();

            this.SetResult("CustomPricingPolicyField1", branch.CustomPricingPolicyField1);
            this.SetResult("CustomPricingPolicyField2", branch.CustomPricingPolicyField2);
            this.SetResult("CustomPricingPolicyField3", branch.CustomPricingPolicyField3);
            this.SetResult("CustomPricingPolicyField4", branch.CustomPricingPolicyField4);
            this.SetResult("CustomPricingPolicyField5", branch.CustomPricingPolicyField5);
        }

        private void MarkSlowness()
        {
            // 1/28/2015 dd - Just a create a log to PB entry.
            string speedTestInMs = GetString("speedTestInMs");
            string mbps = GetString("Mbps");
            string speedTestSizeInBytes = GetString("speedTestSizeInBytes");
            string comment = GetString("comment");

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Speed Test in Ms:  " + speedTestInMs + "ms. Speed Test Total Bytes: " + speedTestSizeInBytes + ". Speed: " + mbps + "Mbps");
            sb.AppendLine();
            sb.AppendLine(comment);
            Tools.LogInfo("MarkSlowness", sb.ToString());
        }

        private void ProcessDateOffset()
        {
            string dateStr = GetString("DateString");
            int offsetPeriod;
            int.TryParse(GetString("OffsetPeriod"), out offsetPeriod);
            char offsetType = GetString("OffsetType")[0];

            CDateTime baseDate = CDateTime.Create(dateStr, null);
            CDateTime newDate;
            if (baseDate.IsValid == false) { SetResult("IsValid", "0"); return; }

            switch (offsetType)
            {
                case 'd':
                    newDate = CDateTime.Create(baseDate.DateTimeForComputation.AddDays(offsetPeriod));
                    break;
                case 'b':
                    if (offsetPeriod < 0 || offsetPeriod > 255) { SetResult("IsValid", "0"); return; } // not sure why we use a byte here...
                    newDate = baseDate.AddBusinessDays((byte)offsetPeriod, Broker);
                    break;
                case 'w':
                    newDate = CDateTime.Create(baseDate.DateTimeForComputation.AddDays(offsetPeriod * 7));
                    break;
                case 'm':
                    newDate = CDateTime.Create(baseDate.DateTimeForComputation.AddMonths(offsetPeriod));
                    break;
                case 'y':
                    newDate = CDateTime.Create(baseDate.DateTimeForComputation.AddYears(offsetPeriod));
                    break;
                default:
                    SetResult("IsValid", "0");
                    return;
            }

            SetResult("IsValid", "1");
            SetResult("NewDateString", newDate.ToStringWithDefaultFormatting());
        }

        private void GetFeeTypeProperties()
        {
            string description = GetString("Description");
            string lineNumber = GetString("LineNumber");

            // Check for blank option
            if (description == ((char)8204).ToString()) // description == &zwnj;
            {
                SetResult("blank", true);
                return;
            }

            FeeType feeType = FeeType.RetrieveByDescription(Broker.BrokerID, lineNumber, description);

            if (feeType == null)
            {
                SetResult("notFound", true);
                return;
            }

            SetResult("Apr", feeType.Apr);
            SetResult("Fha", feeType.FhaAllow);
            SetResult("Page2", feeType.GfeSection);
        }

        private void SaveFHAConnectionEDoc()
        {
            CPageData data = CPageData.CreateUsingSmartDependency(GetGuid("sLId"), typeof(AbstractUtilitiesService));

            string url = string.Empty;
            HttpContext httpContext = HttpContext.Current;
            if (httpContext != null)
            {
                url = httpContext.Request.IsSecureConnection ? "https://" : "http://";
                url += httpContext.Request.Url.Host + Tools.VRoot + "/";

            }

            //which type 
            data.InitLoad();
            string xml = "";
            string xlst = "";
            int num = GetInt("Num");
            XDocument xmlDoc = null;

            if (num == 1)
            {
                xml = data.sFHACaseNumberResultXmlContent;
                if (string.IsNullOrEmpty(xml))
                {
                    return;
                }
                xmlDoc = XDocument.Parse(xml);
                // Check if it's a case number assignment or a holds tracking request
                if (xmlDoc.Root.Name.LocalName == "MORTGAGEDATA")
                {
                    xlst = "LendersOffice.ObjLib.FHAConnection.xslt.caseNumberAssignment.xslt";
                }
                else if (xmlDoc.Root.Name.LocalName == "MULTICASEDATA")
                {
                    xlst = "LendersOffice.ObjLib.FHAConnection.xslt.holdsTracking.xslt";
                }
                else
                {
                    Tools.LogErrorWithCriticalTracking("INvalid root element for SaveFHAConnectionEDoc " + xmlDoc.Root.Name.LocalName);
                    return;
                }
            }

            else if (num == 2)
            {
                xml = data.sFHACaseQueryResultXmlContent;
                if (string.IsNullOrEmpty(xml))
                {
                    return;
                }
                xmlDoc = XDocument.Parse(xml);
                xlst = "LendersOffice.ObjLib.FHAConnection.xslt.caseQuery.xslt";
            }
            else if (num == 3)
            {
                xml = data.sFHACAVIRSResultXmlContent;
                if (string.IsNullOrEmpty(xml))
                {
                    return;
                }
                xmlDoc = XDocument.Parse(xml);
                xlst = "LendersOffice.ObjLib.FHAConnection.xslt.caivrsAuthorization.xslt";
            }


            var lqbElement = new XElement("LQB_DATA");
            lqbElement.Add(new XElement("ImageFileName", url + "images/logo_fha.gif"));
            lqbElement.Add(new XElement("PropertyType", data.sGseSpTFriendlyDisplay));
            xmlDoc.Root.Add(lqbElement);

            xml = xmlDoc.ToString();

            string path = TempFileUtils.NewTempFilePath() + ".html"; // opm 205359 - need extension or get "Conversion error: WebKit Navigation timeout."

            Action<BinaryFileHelper.LqbBinaryStream> writeHandler = delegate (BinaryFileHelper.LqbBinaryStream fs)
            {
                XslTransformHelper.TransformFromEmbeddedResource(xlst, xml, fs.Stream, null, Encoding.UTF8);
            };

            BinaryFileHelper.OpenNew(path, writeHandler);

            string pdfPath = path;

            if (ConstStage.UsePDFRasterizerServiceForPDFConversion)
            {
                PDFConversionOptions options = new PDFConversionOptions();
                options.ShowFooter = true;
                options.ShowPageNumber = true;
                options.PageSize = (int)PdfPageSize.Letter;
                pdfPath = Tools.ConvertHtmlFileToPdfFile(url, path, options);
            }
            else
            {
                MakePdf(path, url);
            }

            var repo = EDocumentRepository.GetUserRepository();
            EDocument doc = repo.CreateDocument(E_EDocumentSource.UserUpload);
            int docType = GetInt("DocTypeId");


            doc.DocumentTypeId = docType;
            doc.LoanId = data.sLId;
            doc.AppId = data.GetAppData(0).aAppId;
            doc.IsUploadedByPmlUser = false;
            doc.UpdatePDFContentOnSave(pdfPath);
            doc.EDocOrigin = E_EDocOrigin.LO;
            repo.Save(doc);
        }


        private void MakePdf(string htmlDocPath, string baseUrl)
        {
            try
            {
                PdfConverter pdfConverter = new PdfConverter();
                if (string.IsNullOrEmpty(ConstStage.PdfConverterLicenseKey) == false)
                {
                    pdfConverter.LicenseKey = ConstStage.PdfConverterLicenseKey;
                }

                // 11/15/2010 dd - Set .5" margin around the page.
                pdfConverter.PdfDocumentOptions.BottomMargin = 36;
                pdfConverter.PdfDocumentOptions.LeftMargin = 36;
                pdfConverter.PdfDocumentOptions.RightMargin = 36;
                pdfConverter.PdfDocumentOptions.TopMargin = 36;

                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfFooterOptions.ShowPageNumber = true;
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.Letter;


                byte[] bytes = pdfConverter.GetPdfBytesFromHtmlFile(htmlDocPath, baseUrl);
                BinaryFileHelper.WriteAllBytes(htmlDocPath, bytes);
            }
            catch (Exception ex)
            {
                // Restart PDF Conversion Process
                Tools.RestartPdfConversionProcess();
                throw ex;
            }
        }
        private void GetCurrentTicks()
        {
            SetResult("Ticks", DateTime.Now.Ticks);
        }
        private void CheckFindingsData()
        {
            Guid loanId = GetGuid("loanid");
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanId, typeof(AbstractUtilitiesService));
            dataLoan.InitLoad();
            SetResult("DUFindingsExist", !string.IsNullOrEmpty(dataLoan.sDuFindingsHtml));
            SetResult("LPFindingsExist", !string.IsNullOrEmpty(dataLoan.sFreddieFeedbackResponseXml.Value));
            SetResult("FHAFindingsExist", !string.IsNullOrEmpty(dataLoan.sTotalScoreCertificateXmlContent.Value));
        }

        private void CacheLoans()
        {
            string jsonData = GetString("Loans");

            //make sure its a valid list
            List<Guid> data = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<Guid>>(jsonData);
            string key = AutoExpiredTextCache.AddToCache(jsonData, TimeSpan.FromMinutes(10));
            SetResult("Key", key);
        }

        private void ValidateFieldId()
        {
            string name = GetString("Id", string.Empty);
            SetResult("ActualId", PageDataUtilities.GetActualFieldId(name));
        }
        private void CheckDODULoginFields()
        {
            try
            {
                Guid userid = this.GetGuid("id");
                Guid pmlSiteId = this.GetGuid("siteid", Guid.Empty);
                Guid brokerId = this.GetGuid("brokerId", Guid.Empty);
                bool checkDo = this.GetBool("checkDo");
                bool checkDu = this.GetBool("checkDu");
                string dousername = checkDo ? this.GetString("dologin") : null;
                string duusername = checkDu ? this.GetString("dulogin") : null;

                SqlParameter[] parameters = new SqlParameter[7];
                parameters[0] = new SqlParameter("@PmlSiteId", pmlSiteId);

                parameters[1] = new SqlParameter("@DoLogin", dousername);
                parameters[2] = new SqlParameter("@DuLogin", duusername);
                parameters[3] = new SqlParameter("@DuplicateDULogin", 0);
                parameters[3].Direction = ParameterDirection.Output;
                parameters[4] = new SqlParameter("@DuplicateDOLogin", 0);
                parameters[4].Direction = ParameterDirection.Output;
                parameters[5] = new SqlParameter("@UserId", userid == Guid.Empty ? (object)DBNull.Value : (object)userid);
                parameters[6] = new SqlParameter("@BrokerId", brokerId);

                StoredProcedureHelper.ExecuteScalar(brokerId, "CountDODUsersWithGivenLogin", parameters);

                SetResult("IsDuUnique", parameters[3].Value.ToString() == "0");
                SetResult("IsDoUnique", parameters[4].Value.ToString() == "0");
            }
            catch (CBaseException e)
            {
                SetResult("error", e.UserMessage);
                Tools.LogError("Unexpected exception", e);
            }

            catch (Exception e)
            {
                Tools.LogError("Unexpected exception", e);
                SetResult("error", LendersOffice.Common.ErrorMessages.Generic);
            }
        }

        private void ClearDODULoginName()
        {
            if (!BrokerUserPrincipal.CurrentPrincipal.HasPermission(LendersOffice.Security.Permission.CanAdministrateExternalUsers))
            {
                SetResult("error", LendersOffice.Common.JsMessages.AccessDenied);
                return;
            }

            try
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerID", BrokerUserPrincipal.CurrentPrincipal.BrokerId),
                                                new SqlParameter("@EmployeeID", GetGuid("EmployeeID") ),
                                                new SqlParameter("@Field", GetString("Field"))
                                            };
                StoredProcedureHelper.ExecuteNonQuery(BrokerUserPrincipal.CurrentPrincipal.BrokerId, "ClearDODUAutoLoginFor", 1, parameters);
                SetResult("OK", "OK");
            }
            catch (Exception e)
            {
                Tools.LogError("Error running ClearDODUAutoLoginFor SP", e);
                SetResult("error", LendersOffice.Common.JsMessages.GenericError);
            }

        }

        private void GetDoDuDuplicates()
        {
            if (!BrokerUserPrincipal.CurrentPrincipal.HasPermission(LendersOffice.Security.Permission.CanAdministrateExternalUsers))
            {
                SetResult("error", LendersOffice.Common.JsMessages.AccessDenied);
                return;
            }

            Guid brokerId = BrokerUserPrincipal.CurrentPrincipal.BrokerId;
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerID", brokerId ),
                                            new SqlParameter("@DOLogin", GetString("doLoginName") ),
                                            new SqlParameter("@DULogin", GetString("duLoginName") )
                                        };
            int resultCount = 0;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetDUDOWithNames", parameters))
            {
                while (reader.Read())
                {
                    for (int x = 0; x < reader.FieldCount; x++)
                    {
                        SetResult("User" + resultCount + reader.GetName(x), reader[x].ToString());
                    }
                    resultCount++;
                }

                SetResult("UserCount", resultCount);
            }
        }

        private void ProcessSmartZipcode()
        {
            string county = string.Empty;
            string city = string.Empty;
            string state = string.Empty;

            bool isValid = Tools.GetCountyCityStateByZip(GetString("zipcode"), out county, out city, out state);

            if (isValid)
            {
                SetResult("City", city);
                SetResult("County", county);
                SetResult("State", state);
            }


            SetResult("IsValid", isValid ? "1" : "0");

            bool counties = false;
            try
            {
                counties = this.GetBool("GetCounties"); //1-10-28 av 
            }
            catch (GenericUserErrorMessageException)
            {
                //its ok we are not expecting this value all the the time 
            }
            if (counties)
            {
                GetCounties(state);
            }

        }

        //1-10-28 av 18913 
        private void GetCounties()
        {
            if (GetString("State") != null)
            {
                GetCounties(GetString("State"));
            }
        }

        //1-10-28 av 18913 
        private void GetCounties(string State)
        {
            int count = 0;
            foreach (string county in StateInfo.Instance.GetCountiesFor(State))
            {
                SetResult("County" + count.ToString(), county);
                count++;
            }
            SetResult("CountyCount", count);


        }

        private void PopulateFromOfficialContactList()
        {
            Guid sLId = GetGuid("LoanID");
            E_AgentRoleT agentRoleT = (E_AgentRoleT)GetInt("AgentRoleT");
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(AbstractUtilitiesService));
            dataLoan.InitLoad();

            CAgentFields agentField = dataLoan.GetAgentOfRole(agentRoleT, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            SetResult("AgentName", agentField.AgentName);
            SetResult("AgentStreetAddr", agentField.StreetAddr);
            SetResult("AgentCity", agentField.City);
            SetResult("AgentState", agentField.State);
            SetResult("AgentZip", agentField.Zip);
            SetResult("AgentCounty", agentField.County);
            SetResult("AgentCompanyName", agentField.CompanyName);
            SetResult("AgentEmail", agentField.EmailAddr);
            SetResult("AgentFax", agentField.FaxNum);
            SetResult("AgentPhone", agentField.Phone);
            SetResult("AgentTitle", agentField.AgentRoleDescription);
            SetResult("AgentDepartmentName", agentField.DepartmentName);
            SetResult("AgentPager", agentField.PagerNum);
            SetResult("AgentLicenseNumber", agentField.LicenseNumOfAgent);
            SetResult("PhoneOfCompany", agentField.PhoneOfCompany);
            SetResult("FaxOfCompany", agentField.FaxOfCompany);
            SetResult("CompanyLicenseNumber", agentField.LicenseNumOfCompany);
            SetResult("CompanyLoanOriginatorIdentifier", agentField.CompanyLoanOriginatorIdentifier);
            SetResult("LoanOriginatorIdentifier", agentField.LoanOriginatorIdentifier);
            SetResult("TaxID", agentField.TaxId);
            SetResult("AgentSourceT", ((int)agentField.AgentSourceT).ToString());
            SetResult("BrokerLevelAgentID", agentField.BrokerLevelAgentID.ToString());

            // OPM 150537 - Added fields for funding page
            SetResult("BranchName", agentField.BranchName);
            SetResult("PayToBankName", agentField.PayToBankName);
            SetResult("PayToBankCityState", agentField.PayToBankCityState);
            SetResult("PayToABANumber", agentField.PayToABANumber.Value);
            SetResult("PayToAccountName", agentField.PayToAccountName);
            SetResult("PayToAccountNumber", agentField.PayToAccountNumber.Value);
            SetResult("FurtherCreditToAccountNumber", agentField.FurtherCreditToAccountNumber.Value);
            SetResult("FurtherCreditToAccountName", agentField.FurtherCreditToAccountName);
            SetResult("CompanyId", agentField.CompanyId);
        }
        private void BindRolodex(RolodexDB rolodex)
        {
            rolodex.Name = GetString("AgentName", rolodex.Name);
            rolodex.Type = (E_AgentRoleT)GetInt("AgentType", (int)rolodex.Type);
            rolodex.Title = GetString("AgentTitle", rolodex.Title);
            rolodex.CompanyName = GetString("AgentCompanyName", rolodex.CompanyName);
            rolodex.DepartmentName = GetString("AgentDepartmentName", rolodex.DepartmentName);
            rolodex.Phone = GetString("AgentPhone", rolodex.Phone);
            rolodex.AlternatePhone = GetString("AgentAltPhone", rolodex.AlternatePhone);
            rolodex.Pager = GetString("AgentPager", rolodex.Pager);
            rolodex.Email = GetString("AgentEmail", rolodex.Email);
            rolodex.Fax = GetString("AgentFax", rolodex.Fax);
            rolodex.AgentLicenseNumber = GetString("AgentLicenseNumber", rolodex.AgentLicenseNumber);
            rolodex.CompanyLicenseNumber = GetString("CompanyLicenseNumber", rolodex.CompanyLicenseNumber);
            rolodex.Address.StreetAddress = GetString("AgentStreetAddr", rolodex.Address.StreetAddress);
            rolodex.Address.City = GetString("AgentCity", rolodex.Address.City);
            rolodex.Address.State = GetString("AgentState", rolodex.Address.State);
            rolodex.Address.Zipcode = GetString("AgentZip", rolodex.Address.Zipcode);
            rolodex.Address.County = GetString("AgentCounty", rolodex.Address.County);
            rolodex.Note = GetString("AgentNote", rolodex.Note);
            rolodex.PhoneOfCompany = GetString("PhoneOfCompany", rolodex.PhoneOfCompany);
            rolodex.FaxOfCompany = GetString("FaxOfCompany", rolodex.FaxOfCompany);
            rolodex.IsNotifyWhenLoanStatusChange = GetString("IsNotifyWhenLoanStatusChange", rolodex.IsNotifyWhenLoanStatusChange ? "1" : "0") == "1";
            rolodex.CommissionPointOfLoanAmount = GetRate("CommissionPointOfLoanAmount", rolodex.CommissionPointOfLoanAmount);
            rolodex.CommissionPointOfGrossProfit = GetRate("CommissionPointOfGrossProfit", rolodex.CommissionPointOfGrossProfit);
            rolodex.CommissionMinBase = GetMoney("CommissionMinBase", rolodex.CommissionMinBase);
            rolodex.CompanyID = GetString("CompanyId", rolodex.CompanyID);
        }

        /// <summary>
        /// We want to track duplicates so we can build nice collision
        /// explanation.
        /// </summary>

        private class RolodexDesc
        {
            /// <summary>
            /// Keep track of duplicates.
            /// </summary>

            public String FullName = String.Empty;
            public String Company = String.Empty;
            public E_AgentRoleT Type = E_AgentRoleT.Other;
            public Guid Id = Guid.Empty;

            public String Kind
            {
                // Access member.

                get
                {
                    return Type.ToString();
                }
            }

            #region ( Descriptor properties )

            public IDataReader Contents
            {
                // Access member

                set
                {
                    if (value["AgentNm"] is String)
                    {
                        FullName = (String)value["AgentNm"];
                    }
                    else
                    {
                        FullName = String.Empty;
                    }

                    if (value["AgentComNm"] is String)
                    {
                        Company = (String)value["AgentComNm"];
                    }
                    else
                    {
                        Company = String.Empty;
                    }

                    if (value["AgentType"] is Int32)
                    {
                        Type = (E_AgentRoleT)(Int32)value["AgentType"];
                    }
                    else
                    {
                        Type = E_AgentRoleT.Other;
                    }

                    if (value["AgentId"] is Guid)
                    {
                        Id = (Guid)value["AgentId"];
                    }
                    else
                    {
                        Id = Guid.Empty;
                    }
                }
            }

            #endregion

        }

        private void AddToRolodex()
        {
            // We will add the rolodex object to the database according
            // to the specified command.  If the caller wants us to add
            // if unique or overwrite, then we need to check the tables
            // for an entry that already matches.  If we just add, then
            // we will append this new record without regard for priors.
            // We initialize the add command to a default if it's not
            // specified.
            //
            // NOTE:  This function relies on field names that are
            // specific to the invoking page/webform.  Becareful when
            // cutting and pasting into service code for another form.

            ArrayList duplicates = new ArrayList();
            String cmd;

            cmd = GetString("AddCommand", "Add");

            if (HasPermission(Permission.AllowWritingToRolodex) == false)
            {
                SetResult("AddResult", "Denied");

                return;
            }

            // 1/23/2008 dd - OPM 19931 - Temporary disable update existing contact feature. Always add new contact.
            cmd = "Add";
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerID)
                                            , new SqlParameter( "@FullName"    , GetString( "AgentName"        , ""  ) )
                                            , new SqlParameter( "@CompanyName" , GetString( "AgentCompanyName" , ""  ) )
                                            , new SqlParameter( "@AgentType"   , GetString( "AgentType"        , "0" ) )
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerID, "ListRolodexEntries", parameters))
            {
                while (reader.Read() == true)
                {
                    // 5/19/2005 kb - We previously matched against type,
                    // name, and company.  I don't think this helps.  Most
                    // collisions I saw involved mismatched type, or company
                    // spelling, or both.  I think if the name is already
                    // in the rolodex, then we should at least tell them
                    // that the person exists, per case 1885.

                    RolodexDesc rD = new RolodexDesc();

                    rD.Contents = reader;

                    duplicates.Add(rD);
                }
            }

            // Handle adding the records based on the current add
            // command specified by the caller.  If no collisions,
            // then we process this request as a straight add.  If
            // there are duplicates, then we return early if we are
            // told to add if unique, and we overwrite them if told
            // to do so.  Note that overwrite duplicates all prior
            // entries that match the specified name and broker.

            Guid brokerid = BrokerID;

            if (cmd == "AddIfUnique")
            {
                // If no duplicates, then simulate an add operation.

                if (duplicates.Count != 0)
                {
                    String sWhy = String.Format
                        ("<table cellspacing=\"0\" cellpadding=\"4\" width=\"100%\" style=\"COLOR: black; FONT: 12px arial; DISPLAY: none;\">"
                        + "<tr style=\"BACKGROUND-COLOR: lightgrey;\">"
                        + "<td colspan=\"3\">"
                        + "Current matches"
                        + "</td>"
                        + "</tr>"
                        );

                    foreach (RolodexDesc rD in duplicates)
                    {
                        sWhy += String.Format
                            ("<tr style=\"BACKGROUND-COLOR: whitesmoke;\">"
                            + "<td nowrap width=\"0%\">"
                            + "{0}"
                            + "</td>"
                            + "<td width=\"100%\">"
                            + "{1}"
                            + "</td>"
                            + "<td width=\"0%\">"
                            + "{2}"
                            + "</td>"
                            + "</tr>"
                            , Utilities.SafeHtmlString(rD.FullName)
                            , Utilities.SafeHtmlString(rD.Company)
                            , Utilities.SafeHtmlString(rD.Kind)
                            );
                    }

                    sWhy += String.Format
                        ("</table>"
                        + "<a href=\"#\" onclick=\"return f_viewDetail(this);\">"
                        + "(show details)"
                        + "</a>"
                        );

                    SetResult("AddResult", "Collided");
                    SetResult("ReasonWhy", sWhy);

                    return;
                }
                else
                {
                    cmd = "Add";
                }
            }

            if (cmd == "Overwrite")
            {
                // If no duplicates, then simulate an add operation.
                // Otherwise, overwrite each entry.

                if (duplicates.Count != 0)
                {
                    foreach (RolodexDesc rD in duplicates)
                    {
                        // Overwrite the specified agent using its
                        // agent id.

                        RolodexDB rolodex = new RolodexDB(rD.Id, brokerid);

                        BindRolodex(rolodex);

                        if (!rolodex.Save())
                        {
                            SetResult("AddResult", "Failed");

                            return;
                        }
                    }
                }
                else
                {
                    cmd = "Add";
                }
            }

            if (cmd == "Add")
            {
                // Simply add the specified record to the rolodex.

                RolodexDB rolodex = new RolodexDB();

                rolodex.BrokerID = brokerid;

                BindRolodex(rolodex);

                if (!rolodex.Save())
                {
                    SetResult("AddResult", "Failed");
                    return;
                }
                else
                {
                    SetResult("BrokerLevelAgentID", rolodex.ID);
                }
            }

            SetResult("AddResult", "Added");
        }
    }
}
