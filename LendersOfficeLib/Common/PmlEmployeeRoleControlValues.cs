﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using DataAccess;
using System.Xml;
using System.IO;
using LendersOffice.Admin;

namespace LendersOffice.Common
{
    [DataContract]
    public sealed class PmlEmployeeRoleControlValues
    {

        [DataMember]
        public string RoleDesc { get; set; }

        [DataMember]
        public Guid LoanId { get; set; }

        [DataMember] 

        public string Role { get; set; }

        [DataMember]
        public Guid EmployeeId { get; set; }

        [DataMember]
        public Guid OwnerId { get; set; }

        [DataMember]
        public string ShowMode { get; set; }

        [DataMember]
        public Guid RoleId { get; set; }

        [DataMember]
        public string SearchFilter { get; set; }

        [DataMember]
        public E_EmployeeStatusFilterT EmployeeStatus { get; set; }

        [DataMember]
        public bool IsDesc { get; set; }

        public PmlEmployeeRoleControlValues() 
        {
            RoleDesc = "";
            LoanId = Guid.Empty;
            Role = "";
            EmployeeId = Guid.Empty;
            OwnerId = Guid.Empty;
            ShowMode = "";
            RoleId = Guid.Empty;
            SearchFilter = "";
            EmployeeStatus = E_EmployeeStatusFilterT.ActiveOnly;
            IsDesc = false;
        }

        public static PmlEmployeeRoleControlValues RetrieveFromCache(string key)
        {
            byte[] data = AutoExpiredTextCache.GetBytesFromCache(key);

            if (data == null)
            {
                return null;
            }

            PmlEmployeeRoleControlValues ob;

            DataContractSerializer ds = new DataContractSerializer(typeof(PmlEmployeeRoleControlValues));


            using (MemoryStream ms = new MemoryStream(data))
            {
                using (XmlReader r = XmlDictionaryReader.Create(ms))
                {
                    ob = (PmlEmployeeRoleControlValues)ds.ReadObject(r);
                }
            }

            return ob;
        }

        /// <summary>
        /// Returns the key to retrive it by
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string Save(PmlEmployeeRoleControlValues data, int cacheLengthInMinutes)
        {
            DataContractSerializer ds = new DataContractSerializer(typeof(PmlEmployeeRoleControlValues));

            using (MemoryStream ms = new MemoryStream())
            {
                using (XmlWriter xdw = XmlDictionaryWriter.Create(ms))
                {
                    ds.WriteObject(xdw, data);
                }

                byte[] serializeData = ms.ToArray();
                return AutoExpiredTextCache.AddToCache(serializeData, TimeSpan.FromMinutes(cacheLengthInMinutes));
            }
            
        }

  
    }
}
