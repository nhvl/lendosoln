﻿namespace LendersOffice.Common
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Class that holds app name and app id.
    /// </summary>
    public class AppNameAndId
    {
        /// <summary>
        /// Initializes a new instance of the<see cref="AppNameAndId" /> class.
        /// </summary>
        /// <param name="name">Application name.</param>
        /// <param name="appId">Application Id.</param>
        public AppNameAndId(string name, Guid appId)
        {
            this.Name = name;
            this.AppId = appId;
        }

        /// <summary>
        /// Gets or sets the application name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the application id.
        /// </summary>
        public Guid AppId { get; set; }
    }    
}
