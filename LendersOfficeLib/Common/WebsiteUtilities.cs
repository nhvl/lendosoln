using System;
using DataAccess;
using System.Data.Common;
using LendersOffice.HttpModule;

namespace LendersOffice.Common
{
    public class WebsiteUtilities
    {

        private const int NUMBER_OF_SECONDS_TO_CACHE = 300; // 5 minutes

        private static bool x_IsDatabaseOffline = true;
        private static DateTime x_IsDatabaseOfflineTimestamp = DateTime.MinValue;
        public static bool IsDatabaseOffline
        {
            get
            {
                int secondsToCache = NUMBER_OF_SECONDS_TO_CACHE;
                if (x_IsDatabaseOffline)
                {
                    // 2/29/2012 dd - Check every 30 seconds;
                    secondsToCache = 30;
                }
                if (x_IsDatabaseOfflineTimestamp.AddSeconds(secondsToCache).CompareTo(DateTime.Now) < 0)
                {
                    x_IsDatabaseOffline = GetIsDatabaseOffline();
                    x_IsDatabaseOfflineTimestamp = DateTime.Now;
                }
                return x_IsDatabaseOffline;
            }
        }
        private static bool GetIsDatabaseOffline()
        {
            var item = PerformanceMonitorItem.GetOrCreate();
            using (PerformanceMonitorItem.Time("WebsiteUtiliities.GetIsDatabaseOffline"))
            {
                try
                {
                    using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShareROnly))
                    {
                        connection.Open();
                        using (var command = connection.CreateCommand())
                        {
                            command.CommandText = "SELECT GETDATE()";
                            command.CommandType = System.Data.CommandType.Text;

                            using (var reader = command.ExecuteReader())
                            {
                                return !reader.Read();
                            }
                        }
                    }
                }
                catch (DbException)
                {
                    return true;
                }
            }
        }
	}
}
