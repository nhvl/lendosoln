﻿// <copyright file="XmlUtils.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: ajay
//  Date:   7/20/2011 5:54:44 PM
// </summary>
namespace LendersOffice.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;

    /// <summary>
    /// This class contains XML utility methods.
    /// </summary>
    public class XmlUtils
    {
        /// <summary>
        /// Removes control characters and other non-UTF-8 characters.
        /// </summary>
        /// <param name="xmlStr">The string to process.</param>
        /// <returns>A string with no control characters or entities above 0x00FD.</returns>
        public static string RemoveInvalidXmlChars(string xmlStr)
        {
            if (xmlStr == null)
            {
                return null;
            }

            StringBuilder newString = new StringBuilder();
            char ch;
            for (int i = 0; i < xmlStr.Length; i++)
            {
                ch = xmlStr[i];

                // remove any characters outside the valid UTF-8 range as well as all control characters
                // except tabs and new lines
                if ((ch < 0x00FD && ch > 0x001F) || ch == '\t' || ch == '\n' || ch == '\r')
                {
                    newString.Append(ch);
                }
            }

            return newString.ToString();
        }

        /// <summary>
        /// Strips the namespaces from the XML string.
        /// </summary>
        /// <param name="xml">The XML to be stripped.</param>
        /// <returns>The XML with the namespaces removed.</returns>
        public static string StripNS(string xml)
        {
            XElement root = XElement.Parse(xml);
            XElement element = StripNS(root);

            XmlDocument doc = new XmlDocument();
            return (doc.ReadNode(element.CreateReader()) as XmlElement).OuterXml;
        }

        /// <summary>
        /// Recursively strips the namespaces from an XML element.
        /// </summary>
        /// <param name="root">The XML element to be stripped.</param>
        /// <returns>The XML element without namespaces.</returns>
        public static XElement StripNS(XElement root)
        {
            XElement res = new XElement(
                root.Name.LocalName,
                root.HasElements ? root.Elements().Select(el => StripNS(el)) : (object)root.Value);

            res.ReplaceAttributes(
                root.Attributes().Where(attr => (!attr.IsNamespaceDeclaration)));

            return res;
        }

        /// <summary>
        /// Gets the value of the specified attribute from the element, returning null on failure.
        /// </summary>
        /// <param name="element">The element that may contain the attribute.</param>
        /// <param name="name">The name of the attribute.</param>
        /// <returns>The value of the specified attribute, or null if <paramref name="element"/> is null or the attribute is not found.</returns>
        public static string GetXAttributeValue(XElement element, XName name)
        {
            XAttribute attribute;
            if (element == null || (attribute = element.Attribute(name)) == null)
            {
                return null;
            }

            return attribute.Value;
        }
    }
}
