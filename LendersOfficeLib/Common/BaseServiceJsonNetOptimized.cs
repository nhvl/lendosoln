﻿namespace LendersOffice.Common
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Optimizes the service page by serializing the result with JsonNet.  It also does not require
    /// serializing objects beforehand.
    /// </summary>
    public class BaseServiceJsonNetOptimized : BaseSimpleServiceXmlPage
    {
        /// <summary>
        /// Gets or sets the result to be serialized when handling json.  Does not require
        /// serializing objects before hand.
        /// </summary>
        /// <value>A dictionary of keys and their values.</value>
        protected Dictionary<string, object> OptimizedResult { get; set; } = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// Serializes the result and writes the json to the response.
        /// </summary>
        public override void HandleJson()
        {
            // 2/2/2012 dd - ASP.NET AJAX framework (i.e PageMethod) always include their json result
            // in { d: { result } } in order to prevent json security issue. I decided to follow
            // the ASP.net format.
            if (this.UseJsonOptimization)
            {
                Dictionary<string, object> jsonResult = new Dictionary<string, object>();
                foreach (var pair in this.m_result)
                {
                    if (this.OptimizedResult.ContainsKey(pair.Key))
                    {
                        Tools.LogBug("BaseServiceJsonNetOptimized::SetResult key=[" + pair.Key + "] already existed. Override previous value.");
                        this.OptimizedResult[pair.Key] = pair.Value;
                    }
                    else
                    {
                        this.OptimizedResult.Add(pair.Key, pair.Value);
                    }
                }

                jsonResult.Add("d", this.OptimizedResult);
                Response.Write(SerializationHelper.JsonNetSerialize(jsonResult));
            }
            else
            {
                base.HandleJson();
            }
        }

        /// <summary>
        /// Sets the key and value into the results dictionary.
        /// </summary>
        /// <param name="key">The key to send the value back as.</param>
        /// <param name="value">The object to be sent back.</param>
        public override void SetResult(string key, object value)
        {
            if (this.UseJsonOptimization)
            {
                if (key == null)
                {
                    throw new ArgumentNullException("key");
                }

                if (value != null)
                {
                    if (this.OptimizedResult.ContainsKey(key))
                    {
                        Tools.LogBug("BaseServiceJsonNetOptimized::SetResult key=[" + key + "] already existed. Override previous value.");
                        this.OptimizedResult[key] = value;
                    }
                    else
                    {
                        this.OptimizedResult.Add(key, value);
                    }
                }
            }
            else
            {
                base.SetResult(key, value);
            }
        }
    }
}
