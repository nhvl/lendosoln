﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using DataAccess;
using LendersOffice.Constants;
using CommonLib;

namespace LendersOffice.Common
{
    /// <summary>
    /// Use to send message relate to LpeUpdate to SAE
    /// </summary>
    public static class LpeAdminMessaging
    {
        private static LqbGrammar.DataTypes.EmailServerName CreateEmailServer(string server)
        {
            var emailServer = LqbGrammar.DataTypes.EmailServerName.Create(server);
            if (emailServer != null)
            {
                return emailServer.Value;
            }
            else
            {
                throw new LqbGrammar.Exceptions.DeveloperException(LqbGrammar.DataTypes.ErrorMessage.BadConfiguration);
            }
        }

        private static LqbGrammar.DataTypes.PortNumber CreatePortNumber(int port)
        {
            var p = LqbGrammar.DataTypes.PortNumber.Create(port);
            if (p == null)
            {
                throw new LqbGrammar.Exceptions.DeveloperException(LqbGrammar.DataTypes.ErrorMessage.BadConfiguration);
            }

            return p.Value;
        }

        private static LqbGrammar.DataTypes.EmailAddress CreateEmailAddress(string address)
        {
            var email = LqbGrammar.DataTypes.EmailAddress.Create(address);
            if (email != null)
            {
                return email.Value;
            }
            else
            {
                throw new LqbGrammar.Exceptions.DeveloperException(LqbGrammar.DataTypes.ErrorMessage.SystemError);
            }
        }

        private static LqbGrammar.DataTypes.EmailSubject CreateEmailSubject(string subject)
        {
            var emailSubject = LqbGrammar.DataTypes.EmailSubject.Create(subject);
            if (emailSubject != null)
            {
                return emailSubject.Value;
            }
            else
            {
                throw new LqbGrammar.Exceptions.DeveloperException(LqbGrammar.DataTypes.ErrorMessage.SystemError);
            }
        }

        public static void SendEmail(E_LpeErrorAreaT lpeErrorAreaT, string subject, string errorMessage)
        {
            string recipient = ConstSite.ErrorEmailAddressRecipient;

            // 7/26/2010 dd - In additional to the main error email, we are sending additional
            // email to different alias depend on error area.
            string extraRecipient = null;
            switch (lpeErrorAreaT)
            {
                case E_LpeErrorAreaT.Schema:
                case E_LpeErrorAreaT.MapProcessor:
                    if (ConstSite.SchemaMapErrorRecipient != "")
                    {
                        extraRecipient = ConstSite.SchemaMapErrorRecipient;
                    }
                    break;
                case E_LpeErrorAreaT.WebBotDownload:
                    if (ConstSite.WebBotDownloadErrorRecipient != "")
                    {
                        extraRecipient = ConstSite.WebBotDownloadErrorRecipient;
                    }
                    break;
                case E_LpeErrorAreaT.EmailBotDownload:
                case E_LpeErrorAreaT.RateUpload:
                case E_LpeErrorAreaT.LpeUpdateGeneral:
                    break;
                default:
                    throw new UnhandledEnumException(lpeErrorAreaT);
            }

            if (ConstStage.UseNewEmailer)
            {
                try
                {
                    SendEmailNEW(recipient, extraRecipient, false, lpeErrorAreaT, subject, errorMessage);
                }
                catch (LqbGrammar.Exceptions.LqbException)
                {
                    SendEmailOLD(recipient, extraRecipient, false, lpeErrorAreaT, subject, errorMessage);
                    EmailUtilities.SendNewCodeWarning();
                }
            }
            else
            {
                SendEmailOLD(recipient, extraRecipient, false, lpeErrorAreaT, subject, errorMessage);
            }
        }

        public static void SendEmailOLD(string recipient, string extraRecipient, bool suppressDuplicationCheck, E_LpeErrorAreaT lpeErrorAreaT, string subject, string errorMessage)
        {
            string email = recipient;

            if (string.IsNullOrEmpty(email) == false)
            {
                System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
                string[] recipientList = email.Split(';');
                foreach (string o in recipientList)
                {
                    msg.To.Add(o);
                }

                if (string.IsNullOrEmpty(extraRecipient) == false)
                {
                    msg.To.Add(extraRecipient);
                }

                msg.From = new MailAddress(ConstSite.PmlRatesEmailAddress);


                string subj = SafeString.Left(subject, 100).Replace('\r', ' ').Replace('\n', ' ');
                msg.Subject = string.Format("LPE.EXE: [{0}] {1}", lpeErrorAreaT, subj);
                msg.Body = errorMessage;

                // 12/16/2010 dd - OPM 20386 - Only send out exception if the error did not get send in last X minutes.
                int currentCount = 0;
                bool isNewErrorMessage = false;
                DateTime lastEmailedToOpmDate = DateTime.MinValue;
                DateTime lastEmailedToCriticalDate = DateTime.MinValue;
                bool sendEmail = (suppressDuplicationCheck) ? 
                    true : 
                    EmailUtilities.IsErrorMessageLastSendExceed(msg.Subject, ConstSite.ErrorMessageDuplicationIntervalInMinutes, out currentCount, out isNewErrorMessage, out lastEmailedToOpmDate, out lastEmailedToCriticalDate);

                if (sendEmail)
                {
                    try
                    {
                        System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(ConstStage.SmtpServer, ConstStage.SmtpServerPortNumber);
                        smtpClient.Send(msg);
                    }
                    catch (SmtpException exc)
                    {
                        // 10/12/2011 dd - A timeout could occur when try to send email. Log to PB instead.
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine("Failed to send email:: " + exc.Message);
                        sb.AppendLine(msg.Subject);
                        sb.AppendLine();
                        sb.AppendLine(errorMessage);
                        Tools.LogError(sb.ToString());
                    }

                }

            }

        }

        public static void SendEmailNEW(string recipient, string extraRecipient, bool suppressDuplicationCheck, E_LpeErrorAreaT lpeErrorAreaT, string subject, string errorMessage)
        {
            string email = recipient;

            if (!string.IsNullOrEmpty(email))
            {
                var server = CreateEmailServer(ConstStage.SmtpServer);
                var port = CreatePortNumber(ConstStage.SmtpServerPortNumber);

                var listTo = new List<LqbGrammar.DataTypes.EmailAddress>();

                string[] recipientList = email.Split(';');
                foreach (string o in recipientList)
                {
                    if (!string.IsNullOrEmpty(o.Trim()))
                    {
                        listTo.Add(CreateEmailAddress(o.Trim()));
                    }
                }

                extraRecipient = extraRecipient?.Trim();
                if (!string.IsNullOrEmpty(extraRecipient))
                {
                    listTo.Add(CreateEmailAddress(extraRecipient));
                }

                var from = CreateEmailAddress(ConstSite.PmlRatesEmailAddress);

                string subj = SafeString.Left(subject, 100).Replace('\r', ' ').Replace('\n', ' ');
                subj = string.Format("LPE.EXE: [{0}] {1}", lpeErrorAreaT, subj);
                var emailSubject = CreateEmailSubject(subj);

                var body = new LqbGrammar.Drivers.Emailer.EmailTextBody();
                body.AppendText(errorMessage);

                var emailPackage = new LqbGrammar.Drivers.Emailer.EmailPackage(ConstAppDavid.SystemBrokerGuid, from, emailSubject, body, listTo.ToArray());

                // 12/16/2010 dd - OPM 20386 - Only send out exception if the error did not get sent in last X minutes.
                int currentCount = 0;
                bool isNewErrorMessage = false;
                DateTime lastEmailedToOpmDate = DateTime.MinValue;
                DateTime lastEmailedToCriticalDate = DateTime.MinValue;
                bool sendEmail = (suppressDuplicationCheck) ?
                    true :
                    EmailUtilities.IsErrorMessageLastSendExceed(subj, ConstSite.ErrorMessageDuplicationIntervalInMinutes, out currentCount, out isNewErrorMessage, out lastEmailedToOpmDate, out lastEmailedToCriticalDate);

                if (sendEmail)
                {
                    try
                    {
                        LendersOffice.Drivers.Emailer.EmailHelper.SendEmail(server, port, emailPackage);
                    }
                    catch (LqbGrammar.Exceptions.LqbException exc)
                    {
                        // 10/12/2011 dd - A timeout could occur when try to send email. Log to PB instead.
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine("Failed to send email:: " + exc.Message);
                        sb.AppendLine(emailSubject.ToString());
                        sb.AppendLine();
                        sb.AppendLine(errorMessage);
                        Tools.LogError(sb.ToString());
                    }

                }

            }

        }

    }
}
