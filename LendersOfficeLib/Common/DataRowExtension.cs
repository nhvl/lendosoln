﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccess;

namespace LendersOffice.Common
{
    public static class DataRowExtension
    {
        public static T GetSafeValue<T>(this DataRow dataRow, string columnName, T defaultValue)
        {
            try
            {
                object o = dataRow[columnName];
                if (o != null && !Convert.IsDBNull(o))
                {
                    return (T)o;
                }
                else
                {
                    return defaultValue;
                }
            }
            catch (Exception e)
            {
                Tools.LogErrorWithCriticalTracking(e);
                throw;
            }
        }

    }
}
