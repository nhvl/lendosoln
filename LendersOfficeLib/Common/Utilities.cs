namespace LendersOffice.Common
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Security.Cryptography;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml.Serialization;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.Events;
    using LendersOffice.Security;
    using LqbGrammar.Drivers.SecurityEventLogging;

    public class Utilities
	{
        /// <summary>
        /// Decodes a Base64 string into a file. Returns the filename of the decoded file. Use this when dealing with potentially large files;
        /// it is slower, but uses little memory.
        /// </summary>
        /// <param name="base64String">The encoded base64 string</param>
        /// <returns>The path to a file containing a decoded version of base64String</returns>
        public static string Base64StringToFile(string base64String, string suffix)
        {
            suffix = suffix ?? "";
            var fileIn = TempFileUtils.NewTempFilePath() + suffix + "_base64";
            File.WriteAllText(fileIn, base64String);

            return TransformFile(fileIn, suffix, new FromBase64Transform());
        }

        /// <summary>
        /// Copies the contents of the given input file to the given output file, less any UTF8 byte order mark. Is a no-op if the inputFile has no BOM.
        /// </summary>
        /// <param name="inputFile">The full filepath of the input file.</param>
        /// <param name="outputFile">The full filepath of the target output file.</param>
        /// <returns>True if the input file has a BOM and the output file was written (w/o BOM). False if the input file has no BOM, and therefore no output file was written.</returns>
        public static bool CopyFileWithoutUTF8BOM(string inputFile, string outputFile)
        {
            using (FileStream inputStream = new FileStream(inputFile, FileMode.Open, FileAccess.Read, FileShare.None))
            {
                byte[] bom = Encoding.UTF8.GetPreamble();
                byte[] buffer = new byte[bom.Length];
                inputStream.Read(buffer, 0, bom.Length);

                if (!bom.SequenceEqual(buffer))
                {
                    return false;
                }

                buffer = new byte[60000];
                int read;

                using (FileStream outputStream = new FileStream(outputFile, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    while ((read = inputStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        outputStream.Write(buffer, 0, read);
                    }
                }
            }

            return true;
        }

        public static void LockRate(Guid loanId, E_ApplicationT fromWhere)
        {
            //This stuff largely mimics LockRate in BrokerRateLockService.aspx.cs in LO.
            var currentUser = LendersOffice.Security.PrincipalFactory.CurrentPrincipal;

            //The loan needs to be locked even if the user normally isn't allowed to touch these fields
            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(Utilities));
            dataLoan.ByPassFieldSecurityCheck = true; // ejm opm 477588 - Some users won't have access to fields required for locking due to permissions. So we'll bypass permission checks while locking.
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            Tools.LogInfo("Loan [" + dataLoan.sLNm + "], id: " + loanId + " automatically locked by workflow config due to request from [" + currentUser.LoginNm + "], userid: " + currentUser.UserId);

            //Automatically use the equivalent of the bottom row in the rate lock table
            var noteRate = dataLoan.sBrokerLockOriginatorPriceNoteIR_rep;
            var brokerCompFee = dataLoan.sBrokerLockOriginatorPriceBrokComp1PcFee_rep;
            var margin = dataLoan.sBrokerLockOriginatorPriceRAdjMarginR_rep;
            var teaserRate = dataLoan.sBrokerLockOriginatorPriceOptionArmTeaserR_rep;

            var originalsBrokerLockPriceRowLockedT = dataLoan.sBrokerLockPriceRowLockedT;

            dataLoan.sBrokerLockPriceRowLockedT = E_sBrokerLockPriceRowLockedT.OriginatorPrice;
            dataLoan.LoadBrokerLockData(noteRate
                , brokerCompFee
                , margin
                , teaserRate
            );

            DateTime rateLockDate = DateTime.Now;
            dataLoan.sRLckdD = CDateTime.Create(rateLockDate);
            dataLoan.sRLckdN = "Automatically locked by workflow";
            string reason = "Automatically locked on " + DateTime.Now.ToShortDateString() +
                " at " + DateTime.Now.ToShortTimeString() +
                " after a lock request by user: " + currentUser.LoginNm;

            //dataLoan.sRLckdN = "Automatically locked by workflow";
            dataLoan.LockRate("AUTO LOCK", reason, false);

            dataLoan.sBrokerLockPriceRowLockedT = originalsBrokerLockPriceRowLockedT;

            // This loan must be saved before PDF Generation so that all of the info is up to date when generating the PDF.
            dataLoan.Save();

            // OPM 184181 - Auto Save a Copy of Lock Certificate when auto lock is enabled
            if (dataLoan.BrokerDB.SaveLockConfOnAutoLock)
            {
               // A seperate instance of the dataLoan is loaded for the PDF generation to modify.  Since the previous instance of the loan was locked,
               // the loan is prevented from saving twice.  Saving it again would result in a rate lock error.
               var dataLoanPdfGeneration = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(Utilities));
               dataLoanPdfGeneration.InitSave(ConstAppDavid.SkipVersionCheck);

               dataLoanPdfGeneration.GenerateAndSaveRateLockConfirmationPdf();
               dataLoanPdfGeneration.Save();
            }

            var rateLockExpirationDate = dataLoan.CalculateRateLockExpirationDate_rep(dataLoan.sRLckdD, dataLoan.sRLckdDays);

            string rate;
            string price;
            if (dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees)
            {
                // When "in addition to lender fees", use these values. OPM 75717
                rate = dataLoan.sBrokerLockOriginatorPriceNoteIR_rep;
                price = dataLoan.sBrokerLockOriginatorPriceBrokComp1PcFee_rep;
            }
            else
            {
                rate = dataLoan.sNoteIR_rep;
                price = dataLoan.sBrokComp1Pc_rep;
            }

            //OPM 111418 - Do not notify lock desk for auto-locks if SendAutoLockToLockDesk == false
            var excludeRoles = new List<E_RoleT>();
            excludeRoles.Add(E_RoleT.Administrator);
            if (!dataLoan.BrokerDB.SendAutoLockToLockDesk)
            {
                excludeRoles.Add(E_RoleT.LockDesk);
            }

            LoanRateLocked.Send(
                currentUser.BrokerId,
                loanId,
                rate,
                price,
                rateLockExpirationDate,
                fromWhere,
                excludedRoles: excludeRoles,
                continueOnException: false);
        }

        public static IList<Guid> GetLoanAssignments(LoanAssignmentContactTable loanAssignmentTable, E_RoleT fileOutRole)
        {
            return GetLoanAssignments(loanAssignmentTable, new List<E_RoleT>() { fileOutRole });
        }

        public static IList<Guid> GetLoanAssignments(LoanAssignmentContactTable loanAssignmentTable, List<E_RoleT> fileOutRoles)
        {
            List<Guid> empList = new List<Guid>();

            foreach (var assignedEmployee in loanAssignmentTable.Items)
            {
                if (!fileOutRoles.Contains(assignedEmployee.RoleT))
                {
                    if (empList.Contains(assignedEmployee.EmployeeId) == false)
                    {
                        empList.Add(assignedEmployee.EmployeeId);
                    }
                }
            }

            return empList;
        }

        private static string TransformFile(string fileIn, string outFileSuffix, ICryptoTransform transform)
        {
            var fileOut = TempFileUtils.NewTempFilePath() + outFileSuffix;

            using (FileStream inStream = File.OpenRead(fileIn),
                             outStream = File.Create(fileOut))
            {
                using (CryptoStream cryptStream = new CryptoStream(outStream, transform, CryptoStreamMode.Write))
                {
                    byte[] buffer = new byte[4096];
                    int bytesRead;

                    while ((bytesRead = inStream.Read(buffer, 0, buffer.Length)) > 0)
                        cryptStream.Write(buffer, 0, bytesRead);

                    cryptStream.FlushFinalBlock();
                }
            }
            return fileOut;
        }

        public static void DetectTimeDifferentWithDB() 
        {
            DateTime serverTime = DateTime.Now;
            DateTime dbTime = Tools.GetDBCurrentDateTime();

            if( Math.Abs( (DateTime.Now - serverTime).TotalSeconds) > 3 ) 
            {
                // network or db is busy. 
                return;
            }

            TimeSpan ts = serverTime.Subtract(dbTime);

            if (Math.Abs(ts.TotalSeconds) > 5) 
            {
                Tools.LogWarning("Time different between server and DB is greater than 5 seconds. Actual different is " + 
                                  ts.TotalSeconds.ToString("N2") + " secs, ServerTime=" + serverTime + ", DB Time=" + dbTime);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="loanId"></param>
        /// <returns></returns>
        public static CPageData GetPDFPrintData(Guid loanId) 
        {
            return new CPDFPrintData(loanId);
        }

        /// <summary>
        /// This method will ensure the string display on HTML escape correctly.
        /// Convert all following characters <!--<, >, ", &, '-->&lt;, &gt;, &quot;, &amp;, &#39; to <!--&lt;, &gt;, &quot;, &amp;, &#39;,--> &amp;lt;, &amp;gt;, &amp;quot;, &amp;amp;, &amp;#39; respectively.
        /// 
        /// <remarks>
        /// To investigate: Do I need to escape ' to &amp;apos;??
        /// Answer: found a case where it was a problem, so we should escape apostrophe. 
        /// Note that &amp;apos; may not be recognized by all HTML user agents. <see cref="https://www.w3.org/TR/xhtml1/#C_16"/>. 
        /// </remarks>
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string SafeHtmlString(string str) 
        {
            if (str != null) 
                return str.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("\'", "&#39;");
            else 
                return null;

        }

        public static string SafeHtmlString(object obj) 
        {
            if (null != obj)
                return SafeHtmlString(obj.ToString());
            else
                return null;
        }

        /// <summary>
        /// Convert single quote to \'. When you should use this method? Anytime in your code you need to generate
        /// a string literal for javascript.
        /// NOTE: This only work if you enclose javascript string with single quote.
        /// i.e:  
        /// If you want to generate the following script.
        /// 
        /// var list = new Array('1', '2', 'O\'Hara');
        /// 
        /// Then use this method to escape single quote correctly in O'Hara case.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string SafeJsString(string str) 
        {
            if (null != str) 
                return str.Replace("'", @"\'");
            else 
                return null;
        }

        public static string SafeJsLiteralString(string str) 
        {
            if (null != str) 
            {
                StringBuilder sb = new StringBuilder(str);
                sb.Replace("'", @"\'");
                sb.Replace("\n", @"\n");

                return sb.ToString();
            }
            else 
                return null;
        }

		//OPM 17384 -jM
		//Needed to escape certain characters
		//Deals with {%,*,'}
		public static string SafeSQLString(string SearchString)
		{
			string cleanString = SearchString.TrimWhitespaceAndBOM().Replace("%","[%]").Replace("*", "[*]").Replace("'","''");
			return (cleanString.Length > 0 && SearchString.TrimWhitespaceAndBOM().EndsWith("*")) ? cleanString + "*" : cleanString;
		}


		/// <summary>
		/// OPM 26066 av Escapes wildcards and escape characters for LIKE in sql server
		/// </summary>
		/// <param name="searchString"></param>
		/// <returns> Left bracket, Percent sign and underscore in [] </returns>
		public static string EscapeForSqlLike(string searchString ) 
		{
			return Regex.Replace(searchString, @"([\[_%*])" , "[$1]" );
		}

        /// <summary>
        /// Allows declaration of enumerated types that specify both an in-code name and an XML string name, for use with XML serialization.
        /// Example:
        /// [XmlEnum("")]
        /// None,
        /// [XmlEnum("Y")]
        /// Yes,
        /// [XmlEnum("N")]
        /// No,
        /// </summary>
        /// <param name="e"></param>
        /// <returns>A string representing the enum value</returns>
        public static string ConvertEnumToString(Enum e)
        {
            Type t = e.GetType();

            FieldInfo info = t.GetField(Enum.GetName(t,e));

            if (!info.IsDefined(typeof(XmlEnumAttribute), false))
            {
                return e.ToString("G");
            }

            object[] o = info.GetCustomAttributes(typeof(XmlEnumAttribute), false);
            var att = (XmlEnumAttribute)o[0];
            return att.Name;
        }
	}

	/// <summary>
	/// Take a given collection and data type and produce a
	/// corresponding data view using the public type info
	/// and exposed get properties.  We provide this api for
	/// generating grid-ready views on the fly.  We pull the
	/// view's schema from the first object in the list.
	/// </summary>

	public class ViewGenerator
	{
		/// <summary>
		/// Generate a single-dimension data view using the
		/// passed-in collection.  First element's type is
		/// used to build the schema.
		/// </summary>

		public static DataView Generate( ICollection oArray , String rowFilter )
		{
			// Delegate a the generation request and apply a
			// row filter only if we have at least one entry.
			// The row filter will fail on zero entries because
			// the data view is not initialized (there was noone
			// to get type info from).

			DataView dView = Generate( oArray );

			if( oArray.Count > 0 )
			{
				dView.RowFilter = rowFilter;
			}

			return dView;
		}

		/// <summary>
		/// Generate a single-dimension data view using the
		/// passed-in collection.  First element's type is
		/// used to build the schema.
		/// </summary>

		public static DataView Generate( ICollection oArray )
		{
			// Take a given collection and data type and
			// produce a corresponding data view using the
			// public type info and exposed get properties.
			// We provide this api for generating grid-ready
			// views on the fly.  We pull the view's schema
			// from the first object in the list.

			try
			{
				// Create a table and get the type from the
				// first entry in the collection.  This type
				// will tell us the layout.

				DataTable      dT = new DataTable();
				ArrayList aSchema = new ArrayList();

				int i = 0;

				foreach( object oItem in oArray )
				{
					if( i == 0 )
					{
						// Capture the table's schema before we
						// go any further.  Only known value types
						// are allowed.  We throw on invalid ones.

						Type baseType = oItem.GetType();

						foreach( PropertyInfo propInfo in baseType.GetProperties() )
						{
							MethodInfo mInfo = propInfo.GetGetMethod();

							if( propInfo.IsSpecialName == true || mInfo == null )
							{
								continue;
							}

							if( mInfo.DeclaringType != baseType )
							{
								continue;
							}

							if( mInfo.IsPublic == false )
							{
								continue;
							}

							try
							{
                                // For nullable data types, we want to use the underlying
                                // type for the column in the table.
                                var columnType = Nullable.GetUnderlyingType(mInfo.ReturnType) ?? mInfo.ReturnType;
                                dT.Columns.Add(propInfo.Name, columnType);
							}
							catch( Exception e )
							{
								// Oops!

								throw new GenericUserErrorMessageException("Can't add column for " + propInfo.Name + " because type isn't supported.  " + e.ToString() );
							}

							aSchema.Add( mInfo );
						}
					}

					// We have a schema for this instance, so let's
					// get the objects for these values in an array
					// and add the row.

					object[] oValues = new object[ aSchema.Count ];

					int j = 0;

					foreach( MethodInfo mInfo in aSchema )
					{
						oValues[ j ] = mInfo.Invoke( oItem , null );

						++j;
					}

					dT.Rows.Add( oValues );

					++i;
				}

				return dT.DefaultView;
			}
			catch( Exception e )
			{
				// Oops!

				throw new GenericUserErrorMessageException( "Failed to generate view.  " + e.ToString() );
			}
		}


        public static DataView GenerateGeneric<T>(IEnumerable<T> oArray)
        {
            // Take a given collection and data type and
            // produce a corresponding data view using the
            // public type info and exposed get properties.
            // We provide this api for generating grid-ready
            // views on the fly.  We pull the view's schema
            // from the first object in the list.

            try
            {
                // Create a table and get the type from the
                // first entry in the collection.  This type
                // will tell us the layout.

                DataTable dT = new DataTable();
                ArrayList aSchema = new ArrayList();

                int i = 0;

                foreach (T oItem in oArray)
                {
                    if (i == 0)
                    {
                        // Capture the table's schema before we
                        // go any further.  Only known value types
                        // are allowed.  We throw on invalid ones.

                        Type baseType = oItem.GetType();

                        foreach (PropertyInfo propInfo in baseType.GetProperties())
                        {
                            MethodInfo mInfo = propInfo.GetGetMethod();

                            if (propInfo.IsSpecialName == true || mInfo == null)
                            {
                                continue;
                            }

                            if (mInfo.DeclaringType != baseType)
                            {
                                continue;
                            }

                            if (mInfo.IsPublic == false)
                            {
                                continue;
                            }

                            try
                            {
                                dT.Columns.Add(propInfo.Name, mInfo.ReturnType);
                            }
                            catch (Exception e)
                            {
                                // Oops!

                                throw new GenericUserErrorMessageException("Can't add column for " + propInfo.Name + " because type isn't supported.  " + e.ToString());
                            }

                            aSchema.Add(mInfo);
                        }
                    }

                    // We have a schema for this instance, so let's
                    // get the objects for these values in an array
                    // and add the row.

                    object[] oValues = new object[aSchema.Count];

                    int j = 0;

                    foreach (MethodInfo mInfo in aSchema)
                    {
                        oValues[j] = mInfo.Invoke(oItem, null);

                        ++j;
                    }

                    dT.Rows.Add(oValues);

                    ++i;
                }

                return dT.DefaultView;
            }
            catch (Exception e)
            {
                // Oops!

                throw new GenericUserErrorMessageException("Failed to generate view.  " + e.ToString());
            }
        }

	}

	/// <summary>
	/// Wrap a real reader and throw when done.
	/// </summary>

	public class ImportStreamReader
	{
		/// <summary>
		/// Wrap a real reader and throw when done.
		/// </summary>

		private StreamReader m_Reader;
		private String[]     m_Parsed;
		private Int32        m_LineNo;

		public String[] Parsed
		{
			// Access member.

			get
			{
				return m_Parsed;
			}
		}

		public Boolean IsValid
		{
			// Access member.

			get
			{
				if( m_Reader == null || m_Reader.BaseStream.Length <= 0 )
				{
					return false;
				}

				return true;
			}
		}

		public Int32 LineNo
		{
			// Access member.

			get
			{
				return m_LineNo;
			}
		}

		/// <summary>
		/// Read next line and throw when done.
		/// </summary>
		/// <returns>
		/// Read in line.
		/// </returns>

		public String ReadLine()
		{
			// Delegate to embedded reader.

			String s = m_Reader.ReadLine();

			if( s == null )
			{
				throw new EndOfStreamException( "Done." );
			}

            m_Parsed = smartSplit(s, ',');
			++m_LineNo;

			return s;
		}

		/// <summary>
		/// Close any attached stream.
		/// </summary>

		public void Close()
		{
			// Delegate to embedded reader.

			m_Reader.Close();
		}

		/// <summary>
		/// Construct reader.
		/// </summary>
		/// <param name="sStream">
		/// Stream to walk.
		/// </param>

		public ImportStreamReader( Stream sStream )
		{
			// Initialize members.

			m_Reader = new System.IO.StreamReader( sStream );

			m_Parsed = null;

			m_LineNo = 0;
		}

        /// <summary>
        /// We now do a smart split right here.
        /// If the commas are within a double quote string, then we ignore them and don't split.
        /// If an opening double quote lacks the closing one, then we just eat till the end of the line.
        /// We assume that all whitespace is ignorable.
        /// </summary>
        /// <param name="textToParse">
        /// String to walk.
        /// </param>
        /// <param name="separator">
        /// Separator character used to split the string.
        /// </param>
        
        
        
        public static string[] smartSplit(string stringToSplit, char separator)
        {
            if (stringToSplit == null)
                throw new ArgumentNullException("textToParse");
            if (separator == '"')
                throw new ArgumentException("The string separator cannot be the double quotes (\") carachter.", "separator");

            // Load up all the strings we find into a list
            // and then convert after we're done.
            ArrayList pList = new ArrayList();
            string p = "";

            for (int i = 0; i < stringToSplit.Length; ++i)
            {
                if (stringToSplit[i] == separator)
                {
                    pList.Add(p);

                    p = "";

                    continue;
                }

                if (stringToSplit[i] == '\"')
                {
                    ++i;

                    while (i < stringToSplit.Length)
                    {
                        if (stringToSplit[i] == '\"')
                        {
                            break;
                        }

                        p += stringToSplit[i];

                        ++i;
                    }

                    continue;
                }

                p += stringToSplit[i];
            }

            pList.Add(p);

            return pList.ToArray(typeof(string)) as string[];
        }

	}
	


	/// <summary>
    /// Provide class to force serialization to use utf-8
    /// encoding of all content.  This class is a string
    /// writer that uses utf-8 encoding.
    /// </summary>

	public class StringWriter8 : StringWriter
    {
        /// <summary>
        /// Override encoding specification for
        /// serialization.
        /// </summary>

		public override Encoding Encoding
        {
            // Access member.

            get
            {
                return Encoding.UTF8;
            }
        }

    }

	/// <summary>
	/// Provide readonly constants for initializers.
	/// </summary>

	public class SmallDateTime
	{
		/// <summary>
		/// Provide readonly constants for initializers.
		/// </summary>

		public static readonly DateTime MinValue;
		public static readonly DateTime MaxValue;

		/// <summary>
		/// Construct readonly.
		/// </summary>

		static SmallDateTime()
		{
			// Initialize constants on the first use.

			MinValue = DateTime.Parse( "1/1/1901" );
			MaxValue = DateTime.Parse( "1/1/2078" );
		}

	}

}
