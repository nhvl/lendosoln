/// Author: David Dao

using System;
using System.IO;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Text;
using DataAccess;
using System.Reflection;

namespace LendersOffice.Common
{
	public class XslTransformHelper
	{
        public static string TransformFromEmbeddedResource(string resourceName, string rawXml, XsltArgumentList xsltArguments)
        {
            using (MemoryStream inputStream = new MemoryStream(10000))
            {
                XmlTextWriter writer = new XmlTextWriter(inputStream, System.Text.Encoding.ASCII);
                writer.WriteRaw(rawXml);
                writer.Flush();
                inputStream.Seek(0, SeekOrigin.Begin);

                StringBuilder sb = new StringBuilder();
                StringWriter stringWriter = new StringWriter(sb);

                XslCompiledTransform transform = CreateTransformFromEmbeddedResources(resourceName);
                XPathDocument doc = new XPathDocument(inputStream);
                transform.Transform(doc, xsltArguments, stringWriter);

                return sb.ToString();
            }
        }
        public static void TransformFromEmbeddedResource(string resourceName, TextReader inputReader, TextWriter outputWriter, XsltArgumentList xsltArguments)
        {
            XslCompiledTransform transform = CreateTransformFromEmbeddedResources(resourceName);
            XPathDocument doc = new XPathDocument(inputReader);
            transform.Transform(doc, xsltArguments, outputWriter);
        }
        public static void TransformFromEmbeddedResource(string resourceName, Stream inputStream, Stream outputStream, XsltArgumentList xsltArguments)
        {
            XslCompiledTransform transform = CreateTransformFromEmbeddedResources(resourceName);
            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
            xmlReaderSettings.XmlResolver = null;
#if LQB_NET45
            xmlReaderSettings.DtdProcessing = DtdProcessing.Parse;
#else
            xmlReaderSettings.ProhibitDtd = false;
#endif
            using (var reader = XmlReader.Create(inputStream, xmlReaderSettings))
            {
                XPathDocument doc = new XPathDocument(reader);
                transform.Transform(doc, xsltArguments, outputStream);
            }
        }

        /// <summary>
        /// Takes a XLST embedded resource location and applies the given parameter xml data. 
        /// </summary>
        /// <param name="location">THE namespace of the Embedded resource</param>
        /// <param name="parameterXml">XML data to apply</param>
        /// <returns></returns>
        internal static string ApplyDataToStylesheet(string location, string parameterXml)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                XslTransformHelper.TransformFromEmbeddedResource(location, parameterXml, ms, null);
                ms.Position = 0;
                using (StreamReader reader = new StreamReader(ms))
                {
                    return reader.ReadToEnd();

                }
            }
        }

        public static void TransformFromEmbeddedResource(string resourceName, string rawXml, Stream outputStream, XsltArgumentList xsltArguments, Encoding encoding)
        {
            using (MemoryStream inputStream = new MemoryStream(10000))
            {
                XmlTextWriter writer = new XmlTextWriter(inputStream, encoding);
                writer.WriteRaw(rawXml);
                writer.Flush();
                inputStream.Seek(0, SeekOrigin.Begin);

                TransformFromEmbeddedResource(resourceName, inputStream, outputStream, xsltArguments);
            }

        }

        public static void TransformFromEmbeddedResource(string resourceName, string rawXml, Stream outputStream, XsltArgumentList xsltArguments)
        {
            TransformFromEmbeddedResource(resourceName, rawXml, outputStream, xsltArguments, System.Text.Encoding.ASCII);
        }

        public static void Transform(string xsltFileLocation, string rawXml, Stream outputStream, XsltArgumentList xsltArguments) 
        {
            using (MemoryStream inputStream = new MemoryStream(10000)) 
            {
                XmlTextWriter writer = new XmlTextWriter(inputStream, System.Text.Encoding.ASCII);
                writer.WriteRaw(rawXml);
                writer.Flush();
                inputStream.Seek(0, SeekOrigin.Begin);

                Transform(xsltFileLocation, inputStream, outputStream, xsltArguments);
            }
        }

        public static void Transform(string xsltFileLocation, Stream inputStream, Stream outputStream, XsltArgumentList xsltArguments) 
        {
            XslCompiledTransform transform = GetTransform(xsltFileLocation, XsltSettings.Default);
            XPathDocument doc = new XPathDocument(inputStream);
            transform.Transform(doc, xsltArguments, outputStream);
        }

        public static string Transform(string xsltFileLocation, string rawXml, XsltArgumentList xsltArguments) 
        {
            using (MemoryStream inputStream = new MemoryStream(10000)) 
            {
                XmlTextWriter writer = new XmlTextWriter(inputStream, System.Text.Encoding.ASCII);
                writer.WriteRaw(rawXml);
                writer.Flush();
                inputStream.Seek(0, SeekOrigin.Begin);

                return Transform(xsltFileLocation, inputStream, xsltArguments);
            }
        }
        public static string Transform(string xsltFileLocation, Stream inputStream, XsltArgumentList xsltArguments) 
        {
            StringBuilder sb = new StringBuilder();
            StringWriter writer = new StringWriter(sb);

            XPathDocument doc = new XPathDocument(inputStream);

            XslCompiledTransform transform = GetTransform(xsltFileLocation, XsltSettings.Default);
            transform.Transform(doc, xsltArguments, writer);

            return sb.ToString();

        }

        public static void TransformToFile(string xsltFileLocation, string inputFile, string outputFile, XsltArgumentList xsltArguments, XsltSettings xsltSettings)
        {
            XslCompiledTransform transform = GetTransform(xsltFileLocation, xsltSettings);

            XPathDocument xpathDocument = new XPathDocument(inputFile);

            using (FileStream stream = new FileStream(outputFile, FileMode.Create, FileAccess.Write))
            {
                transform.Transform(xpathDocument, xsltArguments, stream);
            }
        }
        private static XslCompiledTransform CreateTransformFromEmbeddedResources(string resourceName)
        {
            string key = "XSLT_EMBEDDED_" + resourceName;

            XslCompiledTransform transform = (XslCompiledTransform)LOCache.Get(key);
            if (null == transform)
            {
                transform = new XslCompiledTransform();
                Assembly assembly = typeof(XslTransformHelper).Assembly;
                using (Stream stream = assembly.GetManifestResourceStream(resourceName))
                {
                    transform.Load(XmlReader.Create(stream), null, new OtherEmbeddedResourceXmlUrlResolver(assembly, resourceName)); // https://stackoverflow.com/questions/39704036/load-xslt-with-import-from-assembly-in-c-sharp
                }

                LOCache.Set(key, transform, DateTime.Now.AddDays(1));
            }

            return transform;
        }

        /// <summary>
        /// Gets the output encoding of the compiled transform. <para></para>
        /// Adding this to continue to hide the compiled transform outside the class.<para></para>
        /// NOTE: If you need to use this for a compiled transform with a non-default xslt settings, please update the method.
        /// </summary>
        /// <param name="xsltFilePath">The full path where the transform is stored.</param>
        /// <returns>The output encoding of the compiled transform.</returns>
        public static Encoding GetOutputEncoding(string xsltFilePath)
        {
            var transform = GetTransform(xsltFilePath, XsltSettings.Default);
            return transform.OutputSettings.Encoding;
        }

        /// <summary>
        /// Resolves resources referenced from one xsl document via xsl:import.  This currently
        /// only supports items that are siblings in the file system with the same full file
        /// extension, e.g. both files must be .xsl.config or .0.xsl.
        /// </summary>
        private class OtherEmbeddedResourceXmlUrlResolver : XmlUrlResolver
        {
            public OtherEmbeddedResourceXmlUrlResolver(Assembly assembly, string rootResourceName)
            {
                this.Assembly = assembly;
                this.RootResourceName = rootResourceName;
            }

            public Assembly Assembly { get; }

            public string RootResourceName { get; }

            public override object GetEntity(Uri absoluteUri, string role, Type ofObjectToReturn)
            {
                if (!absoluteUri.IsFile)
                {
                    return base.GetEntity(absoluteUri, role, ofObjectToReturn);
                }

                // We're only supporting files that are siblings to the current document, so the directory in absoluteUri is the current directory.
                FileInfo localFile = new FileInfo(absoluteUri.LocalPath);
                if (localFile.DirectoryName != Directory.GetCurrentDirectory())
                {
                    return base.GetEntity(absoluteUri, role, ofObjectToReturn);
                }

                // To determine the appropriate namespace, we have to assume any periods in the file name are pseudo extensions, e.g. .xsl.config or .0.xsl
                int firstPeriod = localFile.Name.IndexOf('.');
                string extensionToTrim = firstPeriod < 0 ? string.Empty : localFile.Name.Substring(firstPeriod);
                if (!this.RootResourceName.EndsWith(extensionToTrim))
                {
                    return base.GetEntity(absoluteUri, role, ofObjectToReturn);
                }

                int indexOfEndOfResourceNamespace = this.RootResourceName.LastIndexOf('.', this.RootResourceName.Length - 1 - extensionToTrim.Length);
                string resourceNamespace = this.RootResourceName.Remove(indexOfEndOfResourceNamespace);
                string referencedResourceName = resourceNamespace + '.' + localFile.Name;
                return this.Assembly.GetManifestResourceStream(referencedResourceName);
            }
        }

        private static XslCompiledTransform GetTransform(string xsltFileLocation, XsltSettings xsltSettings) 
        {
            if (!File.Exists(xsltFileLocation))
            {
                throw new CBaseException(ErrorMessages.Generic, xsltFileLocation + " is not found.");
            }
            FileInfo fi = new FileInfo(xsltFileLocation);
            string key = "XSLT_" + xsltFileLocation + "::" + fi.LastWriteTime.Ticks;
            XslCompiledTransform transform = (XslCompiledTransform) LOCache.Get(key);

            if (null == transform)
            {
                transform = new XslCompiledTransform();
                transform.Load(xsltFileLocation, xsltSettings, new XmlUrlResolver());
                LOCache.Set(key, transform, DateTime.Now.AddDays(1));
            }

            return transform;

        }
	}
}
