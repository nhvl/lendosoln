﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using LendersOffice.Constants;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;

namespace LendersOffice.Common
{
    public static class HardcodeIdentitySequence
    {

        private static object s_lock = new object();
        /// <summary>
        /// Get the next available string in RESERVE_STRING_LIST.
        /// The stored procedure is handle row lock, so no two processes
        /// will be able to pull the same string.
        /// However as a precaution, I will just add additional lock in the code.
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetNextAvailableIdentity(Guid brokerId, string type)
        {
            lock (s_lock)
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", brokerId),
                                                new SqlParameter("@Type", type)
                                            };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RESERVE_STRING_LIST_GetNext",
                    parameters))
                {
                    if (reader.Read())
                    {
                        return reader["StringValue"].ToString();
                    }
                }
                throw new CBaseException("No available key in reserve list.", "No available key in reserve list.");
            }
        }

    }

}
