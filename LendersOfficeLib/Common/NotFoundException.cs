﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace LendersOffice.Common
{
    public class NotFoundException : CBaseException
    {
        public NotFoundException(string userMessage, string devMessage) :
            base(userMessage, devMessage)
        {
        }

        public NotFoundException(string userMessage) :
            base(userMessage, userMessage)
        {

        }
    }
}
