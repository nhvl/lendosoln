using System;
using System.Text;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using LendersOffice.Common;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.HttpModule;
using LqbGrammar.DataTypes;
using System.Linq;

namespace LendersOffice.Common
{

    public class BaseSimpleServiceXmlPage : System.Web.UI.Page
    {
        private string sLogData = "defaultempty";

        /// <summary>
        /// Determines whether JsonNet is used for the final serialization
        /// call.
        /// </summary>
        /// <value>False by default.  Only possibly true in inherited classes.</value>
        protected bool UseJsonOptimization {get; set; } = false;

        protected virtual bool PerformResultOptimization
        {
            get
            {
                // 7/26/2010 dd - By default, if the result we return is the same as the arguments pass in then 
                // we do not want to return to client for performance reason. The example is GFE page. Since each
                // calculation only affect few fields, we do not want to return all fields to client if it is 
                // remain unchange.
                //
                // However there are instance for certain page expects result regardless if the value remain unchange.
                // Example is the Borrower Liabilities, Assets, REO page. In this instance we need to override
                // this property and return false.
                //
                // 8/14/2010 dd - There are undetermine amount of codes that expect the arguments to return result
                // even if the values are the same. Therefore I have to change this default to false, and turn on
                // explicitly for page need performance boost.
                return false;
            }
        }
        private Dictionary<string, string> m_arguments = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        protected Dictionary<string, string> m_result = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        protected LosConvert m_losConvert = new LosConvert();

        private string m_requestXML = "";

        private bool m_isHandleJson = false;

        #region Get Argument methods
        public bool ContainsField(string fieldId)
        {
            return m_arguments.ContainsKey(fieldId);
        }

		public bool HasValue(string valToFind)
		{
			if(null == valToFind)
				return false;
			foreach(string val in m_arguments.Keys)
			{
				if(m_arguments[val].Equals(valToFind))
					return true;
			}
			return false;
		}
		
		public string GetString(string key) 
        {
            if (null == key) throw new ArgumentNullException("key");

            string ret = "";

            if (m_arguments.TryGetValue(key, out ret) == false)
            {
                throw new GenericUserErrorMessageException("Arguments do not contain " + key  + " av123:" + sLogData);
            }
            return ret;
        }

		public decimal GetRate(string key, decimal defaultValue)
		{
			decimal ret = defaultValue;
			string str = GetString(key, null);			
		
			try 
			{
				if (null != str)
					ret = m_losConvert.ToRate(str);
			} 
			catch {}
			
			return ret;
		}

		public decimal GetRate(string key) 
		{
			string str = GetString(key);

			try 
			{
				return m_losConvert.ToRate(str);
			} 
			catch 
			{
				throw new GenericUserErrorMessageException("Input string is not decimal. Key='" + key + "', Value='" + str + "', Xml=" + m_requestXML);
			}
		}

		public decimal GetMoney(string key, decimal defaultValue)
		{
			decimal ret = defaultValue;
			string str = GetString(key, null);			
		
			try 
			{
				if (null != str)
					ret = m_losConvert.ToMoney(str);
			} 
			catch {}
			
			return ret;
		}

		public decimal GetMoney(string key) 
		{
			string str = GetString(key);

			try 
			{
				return m_losConvert.ToMoney(str);
			} 
			catch 
			{
				throw new GenericUserErrorMessageException("Input string is not decimal. Key='" + key + "', Value='" + str + "', Xml=" + m_requestXML);
			}
		}
		
		public string GetString(string key, string defaultValue) 
        {
            if (null == key) throw new ArgumentNullException("key");

            string ret = defaultValue;

            if (m_arguments.TryGetValue(key, out ret) == false)
            {
                ret = defaultValue;
            }

            return ret;
        }

        public bool GetBool(string key) 
        {
            string value = GetString(key);

            try 
            {
                return bool.Parse(value);
            } 
            catch {
				throw new GenericUserErrorMessageException("Input string is not boolean.  Key = '" + key + "', Value = '" + value + "'");
            }

        }

        public bool? GetNullableBool(string key) => this.GetString(key, null).ToNullableBool();

        public bool GetBool(string key, bool defaultValue) 
        {
            string str = GetString(key, null);

            bool ret = defaultValue;

            try 
            {
                if (null != str)
                    ret = bool.Parse(str);
            } 
            catch {}
            return ret;
        }

        public long GetLong(string key) 
        {
            string value = GetString(key);

            try 
            {
                return long.Parse(value);
            } 
            catch 
            {
                throw new GenericUserErrorMessageException("Input string is not long. Key='" + key + "', Value='" + value + "', Xml=" + m_requestXML);
            }

        }

        public long GetLong(string key, long defaultValue) 
        {
            long ret = defaultValue;

            string str = GetString(key, null);

            try 
            {
                if (null != str)
                    ret = long.Parse(str);
            } 
            catch {}
            return ret;
        }

        public int GetInt(string key) 
        {
            string value = GetString(key);

            try 
            {
                return int.Parse(value);
            } 
            catch 
            {
				throw new GenericUserErrorMessageException("Input string is not integer.  Key = '" + key + "', Value = '" + value + "', Xml = " + m_requestXML);
            }
        }
        public int GetInt(string key, int defaultValue) 
        {
            int ret = defaultValue;

            string str = GetString(key, null);

            try 
            {
                if (null != str)
                    ret = int.Parse(str);
            } 
            catch {}
            return ret;
        }

        /// <summary>
        /// Gets the value for the specified key, returning null if not found as a valid integer.
        /// </summary>
        /// <param name="key">The key name.</param>
        /// <returns>The integer under the key, or null if not found or not a valid integer.</returns>
        public int? GetNullableInt(string key)
        {
            return this.GetString(key, null).ToNullable<int>(int.TryParse);
        }

        public Guid? GetNullableGuid(string argumentKey)
        {
            string guidString = this.GetString(argumentKey);
            return string.IsNullOrEmpty(guidString) ? default(Guid?) : Guid.Parse(guidString);
        }

        public Guid? GetNullableGuid(string argumentKey, Guid? defaultValue)
        {
            string guidString = this.GetString(argumentKey, null);
            return guidString.ToNullable<Guid>(Guid.TryParse) ?? defaultValue;
        }

        public Guid GetGuid(string argumentKey) 
        {
            var guidStringVal = GetString(argumentKey);

            return GetGuidImpl(argumentKey, guidStringVal, null);
        }

        public Guid GetGuid(string argumentKey, Guid defaultValue)
        {
            var guidStringVal = GetString(argumentKey, null);

            return GetGuidImpl(argumentKey, guidStringVal, defaultValue);
        }

        public Guid GetGuidImpl(string argumentKeyForLogging, string guidStringVal, Guid? defaultValue)
        {
            var ret = Guid.Empty;

            if (!Guid.TryParse(guidStringVal, out ret))
            {
                if (defaultValue.HasValue)
                {
                    ret = defaultValue.Value;
                }
                else
                {
                    var msg = string.Format(
                        "An attempt was made to obtain the invalid GUID value \"{0}\" for key \"{1}\" and no default was supplied." +
                        "{2}Request XML: {2}{3}" +
                        "{2}Log data: {2}{4}",
                        guidStringVal,
                        argumentKeyForLogging,
                        Environment.NewLine,
                        this.m_requestXML,
                        this.sLogData);

                    throw CBaseException.GenericException(msg);
                }
            }

            return ret;
        }

        public E_TriState GetTriState(string key) 
        {
            bool v1 = GetBool(key + "_0");
            bool v2 = GetBool(key + "_1");
            E_TriState ret = E_TriState.Blank;
            if (v1)
                ret = E_TriState.Yes;
            else if (v2)
                ret = E_TriState.No;

            return ret;
        }
        public T GetEnum<T>(string key) where T : struct, IConvertible
        {
            return (T)Enum.ToObject(typeof(T), this.GetInt(key));
        }

        public E_TriState GetTriState(string key, E_TriState defaultValue) 
        {
            E_TriState ret = defaultValue;
            
            try 
            {
                ret = GetTriState(key);
            } 
            catch {}
            return ret;
        }
        public System.IO.Stream GetBase64File(string key) 
        {
            string value = GetString(key);

            try 
            {
                byte[] bytes = Convert.FromBase64String(value);
                System.IO.MemoryStream stream = new System.IO.MemoryStream(bytes);
                return stream;
            } 
            catch 
            {
				throw new GenericUserErrorMessageException("Error in GetBase64File. Key = '" + key + "', Value = '" + value + "'");
            }
        }
        #endregion
        #region Set result methods
        public void SetResult(string key, string value) 
        {
            if (null == key) throw new ArgumentNullException("key");

            if (null != value)
            {
                string argumentValue;
                if (m_arguments.TryGetValue(key, out argumentValue) == false)
                {
                    argumentValue = null;
                }
                if (PerformResultOptimization == false || argumentValue != value)
                {
                    if (m_result.ContainsKey(key))
                    {
                        Tools.LogBug("BaseSimpleServiceXmlPage::SetResult key=[" + key + "] already existed. Override previous value.");
                        m_result[key] = value;
                    }
                    else
                    {
                        m_result.Add(key, value);
                    }
                }
            }
        }
        public void SetResult(string key, bool value) 
        {
            SetResult(key, value.ToString());
        }
        public void SetResult(string key, int value) 
        {
            SetResult(key, value.ToString());
        }
        public void SetResult(string key, long value) 
        {
            SetResult(key, value.ToString());
        }
        public void SetResult(string key, E_TriState value, bool forDropdown = false) 
        {
            if (forDropdown)
            {
                this.SetResult(key, value.ToString("D"));
            }
            else
            {
                SetResult(key + "_0", value == E_TriState.Yes ? "True" : "False");
                SetResult(key + "_1", value == E_TriState.No ? "True" : "False");
            }
        }
        public void SetResult(string key, Enum value) 
        {
            SetResult(key, value.ToString("D"));
        }
        public virtual void SetResult(string key, object value) 
        {
            if (null != value)
                SetResult(key, value.ToString());
        }
        public void SetResult(string key, Uri value)
        {
            if (null != value)
                SetResult(key, value.AbsoluteUri);
        }
        #endregion

        private void ParseXmlRequest(XmlDocument doc) 
        {
            XmlElement e = (XmlElement) doc.SelectSingleNode("/request/data");

            if (null != e) 
            {
                foreach (XmlAttribute a in e.Attributes) 
                {
                    if (m_arguments.ContainsKey(a.Name))
                    {
                        Tools.LogBug("BaseSimpleServiceXmlPage::ParseXmlRequest [" + a.Name + "] already existed in the request. Override previous value.");
                        m_arguments[a.Name] = a.Value;
                    }
                    else
                    {
                        m_arguments.Add(a.Name, a.Value);
                    }
                }
            }
        }

        private XmlDocument BuildXmlResponse() 
        {
            XmlDocument doc = new XmlDocument();
            XmlElement root = doc.CreateElement("response");
            doc.AppendChild(root);
            XmlElement element = doc.CreateElement("data");
            root.AppendChild(element);
            foreach (string key in m_result.Keys) 
            {
                string str = m_result[key];
                if (str.Length > 100000)
                {
                    XmlElement el = doc.CreateElement(key);
                    el.InnerText = str;
                    element.AppendChild(el);
                    //element.Value = str;
                }
                else
                {
                    element.SetAttribute(key, m_result[key]);
                }
            }
            return doc;
        }
        private void PageLoad(object sender, EventArgs e) 
        {
            try
            {
                if (Request.ContentType.StartsWith("application/json", StringComparison.OrdinalIgnoreCase))
                {
                    // 2/1/2012 dd - For security reason we always force a POST when handle JSON.
                    m_isHandleJson = Request.HttpMethod == "POST";
                }
                this.Context.Items[ConstAppDavid.HttpContextItem_IsServicePageKey] = true;

                string xml = string.Empty;
                using (StreamReader reader = new StreamReader(Request.InputStream))
                {
                    xml = reader.ReadToEnd();
                }

                m_requestXML = xml; // 3/31/2005 dd - For debugging purpose.
                sLogData = xml; //for logging

                if (string.IsNullOrEmpty(sLogData))
                {
                    Tools.LogWarning("Received Empty Request Data.");
                }


                string method = RequestHelper.GetSafeQueryString("method");
                if (m_isHandleJson)
                {
                    Dictionary<string, string> jsonInput = ObsoleteSerializationHelper.JavascriptJsonDeserializer<Dictionary<string, string>>(xml);

                    // 2/1/2012 dd - Since jsonInput is a dictionary with case sensitive, we need to 
                    // make them case insensitive

                    if (jsonInput == null)
                    {
                        m_arguments = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
                    }
                    else
                    {
                        m_arguments = new Dictionary<string, string>(jsonInput, StringComparer.OrdinalIgnoreCase);
                    }
                }
                else if (Request.ContentType.StartsWith("application/x-www-form-urlencoded", StringComparison.OrdinalIgnoreCase))
                {
                    m_arguments = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
                    foreach(var k in Request.Form.AllKeys)
                    {
                        m_arguments.Add(k, Request.Form[k]);
                    }
                }
                else
                {
                    // 6/8/2011 dd - Use Tools.CreateXmlDoc because it will handle illegal character in xml.
                    XmlDocument doc = Tools.CreateXmlDoc(xml);


                    ParseXmlRequest(doc);
                }
                Initialize();

                PerformanceMonitorItem monitorItem = PerformanceMonitorItem.Current;
                if (monitorItem != null)
                {
                    // 1/16/2012 dd - Record loanid
                    Guid loanId = GetGuid("loanid", Guid.Empty);
                    if (loanId != Guid.Empty)
                    {
                        monitorItem.SetCustomKeyValue("sLId", loanId.ToString());
                    }
                    monitorItem.SetCustomKeyValue("ssMethod", method);
                }
                try
                {
                    if (m_backgroundItemHash.Count == 0)
                    {
                        // 12/8/2004 dd - Use old method.
                        Process(method);
                    }
                    else
                    {
                        string clientID = GetString("_ClientID", "");

                        try
                        {
                            AbstractBackgroundServiceItem item = m_backgroundItemHash[clientID];

                            // 1/3/2005 dd - With ClientID nonempty, the method will have the following format {ClientID}_{MethodName}.
                            // Therefore need to remove {ClientID}_
                            if (clientID != "")
                                method = method.Replace(clientID + "_", "");

                            item.MainProcess(method);
                        }
                        catch (KeyNotFoundException)
                        {
                            throw new CBaseException(ErrorMessages.Generic, "There is no BackgroundServiceItem for ClientId=[" + clientID + "]. Did you forget to pass _ClientId to webservice.");
                        }
                    }
                }
                catch (GenericUserErrorMessageException y)
                {
                    if (sLogData.Length > 0)
                    {
                        Tools.LogError("GenericUserErrorMessageException: " + sLogData, y);
                    }
                    throw;
                }


                Response.Clear();
                Response.ClearContent();
                if (m_isHandleJson || Request.AcceptTypes.Any(type => type.StartsWith("application/json")))
                {
                    Response.ContentType = "application/json";
                    HandleJson();
                }
                else
                {
                    Response.ContentType = "text/xml";

                    XmlDocument doc = BuildXmlResponse();
                    doc.Save(Response.Output);
                }
            }
            catch (PageDataAccessDenied exc)
            {
                CreateErrorMessage(exc);
            }
            catch (VersionMismatchException exc)
            {
                CreateErrorMessage(exc);
            }
            catch (LoanFieldWritePermissionDenied exc)
            {
                CreateErrorMessage(exc);
            }
            catch (InvalidTaxIdValueException exc)
            {
                CreateErrorMessage(exc);
            }
            catch (CBaseException exc)
            {
                // 12/27/06 db OPM 8935 - if the error message indicates that the sBrokComp1Pc field cannot
                // be changed because the rate has been locked, don't log an error.  Instead, notify the user
                // that the field cannot be changed and disable it.
                if (exc.UserMessage.Equals(ErrorMessages.RebateCannotBeChangedDuringRateLock))
                {
                    CreateUserMessage(exc);
                }
                else
                {
                    //sometimes the client sends an  empty request, in this case just log it but dont email.
                    if (sLogData.Length == 0)
                    {
                        Tools.LogError(exc);
                    }
                    else
                    {
                        Tools.LogErrorWithCriticalTracking(exc);
                    }

                    CreateErrorMessage(exc);
                }
            }
            catch (OverflowException exc)
            {
                CreateErrorMessage(new CBaseException("Error. The entered value resulted in a calculation that was either too large or too small.", exc.Message));
            }
            catch (Exception exc)
            {
                CBaseException bExc = new GenericUserErrorMessageException(exc);
                Tools.LogErrorWithCriticalTracking(bExc);
                CreateErrorMessage(bExc);
            }

            //Response.Flush();
            Response.End();
        }

        public virtual void HandleJson()
        {
            // 2/2/2012 dd - ASP.NET AJAX framework (i.e PageMethod) always include their json result
            // in { d: { result } } in order to prevent json security issue. I decided to follow
            // the ASP.net format.

            Dictionary<string, object> jsonResult = new Dictionary<string, object>();
            jsonResult.Add("d", m_result);
            Response.Write(ObsoleteSerializationHelper.JavascriptJsonSerialize(jsonResult));
        }

        private void PageInit(object sender, System.EventArgs e) 
        {
            Tools.DenyIfUserLacksSufficientPermissionsOrRoles(RequiredPermissionsForLoadingPage, RequiredRolesForLoadingPage);
        }

        /// <summary>
        /// overrideable.  making these static and using new doesn't work, even though it makes the unit test easier.
        /// </summary>
        public virtual IEnumerable<LendersOffice.Security.Permission> RequiredPermissionsForLoadingPage
        {
            get
            {
                return new List<LendersOffice.Security.Permission>();
            }
        }

        /// <summary>
        /// overrideable.  making these static and using new doesn't work, even though it makes the unit test easier.
        /// </summary>
        public virtual IEnumerable<LendersOffice.Security.E_RoleT> RequiredRolesForLoadingPage
        {
            get
            {
                return new List<LendersOffice.Security.E_RoleT>();
            }
        }


		// 12/272006 db OPM 8935 - A situation can arise in which 2 users are editing a loan at the same time, and one
		// locks the rate while the other tries to change the sBrokComp1Pc field on the GFE.  Instead of logging
		// this error, we will alert the user that the field cannot be edited because the rate is locked and we'll
		// disable the sBrokComp1Pc textbox on the GFE.
		private void CreateUserMessage(CBaseException exc)
		{
			Response.Clear();
			Response.ClearContent();

            if (m_isHandleJson)
            {
                Response.ContentType = "application/json";

                // 2/2/2012 dd - ASP.NET AJAX framework (i.e PageMethod) always include their json result
                // in { d: { result } } in order to prevent json security issue. I decided to follow
                // the ASP.net format.

                Dictionary<string, object> jsonResult = new Dictionary<string, object>();
                m_result.Add("UserMessage", exc.UserMessage);

                jsonResult.Add("d", m_result);
                Response.Write(ObsoleteSerializationHelper.JavascriptJsonSerialize(jsonResult));
            } 
            else 
            {


                Response.ContentType = "text/xml";

                XmlDocument doc = new XmlDocument();
                XmlElement root = doc.CreateElement("response");
                doc.AppendChild(root);
                XmlElement el = doc.CreateElement("data");
                root.AppendChild(el);

                el.SetAttribute("UserMessage", exc.UserMessage);

                foreach (string key in m_result.Keys)
                {
                    el.SetAttribute(key, m_result[key]);
                }

                doc.Save(Response.Output);
            }
            //Response.Flush();
            //Response.End();
		}

        private void CreateErrorMessage(CBaseException exc) 
        {
			ErrorUtilities.StoreExceptionToCache(exc);

            if (m_isHandleJson)
            {
                Response.Clear();
                Response.ClearContent();
                Response.ContentType = "application/json";
                Response.StatusCode = 500;

                // IIS can intercept this one and send the generic 500 page. Do not want.
                Response.TrySkipIisCustomErrors = true;

                Dictionary<string, string> dictionary = new Dictionary<string, string>();

                dictionary.Add("type", exc.GetType().Name);
                dictionary.Add("UserMessage", exc.UserMessage);
                dictionary.Add("ErrorReferenceNumber", exc.ErrorReferenceNumber);
                dictionary.Add("IsWorkflowError", exc.IsWorkflowError.ToString());
                Response.Write(ObsoleteSerializationHelper.JavascriptJsonSerialize(dictionary));
            }
            else
            {
                Response.Clear();
                Response.ClearContent();
                Response.ContentType = "text/xml";


                XmlDocument doc = new XmlDocument();
                XmlElement root = doc.CreateElement("response");
                doc.AppendChild(root);
                XmlElement el = doc.CreateElement("error");
                root.AppendChild(el);

                el.SetAttribute("type", exc.GetType().Name);
                el.SetAttribute("UserMessage", exc.UserMessage);
                el.SetAttribute("IsWorkflowError", exc.IsWorkflowError.ToString());
                el.SetAttribute("ErrorReferenceNumber", exc.ErrorReferenceNumber);

                doc.Save(Response.Output);
            }

            //Response.Flush();
            //Response.End();
        }
        protected virtual void Initialize() 
        {
        }
        private Dictionary<string, AbstractBackgroundServiceItem> m_backgroundItemHash = new Dictionary<string, AbstractBackgroundServiceItem>(4);
        /// <summary>
        /// Put clientID = "" when this is the only BackgroundServiceItem
        /// </summary>
        /// <param name="clientID"></param>
        /// <param name="item"></param>
        protected void AddBackgroundItem(string clientID, AbstractBackgroundServiceItem item) 
        {
            if (null == item) return;

            item.BaseXmlPage = this;

            m_backgroundItemHash.Add(clientID, item);
        }
        protected virtual void Process(string methodName) 
        {
            throw new NotImplementedException("Need to override Process(string methodName, NameValueCollection args).");
        }

    protected override void OnInit(EventArgs e) 
        {

            this.Load += new System.EventHandler(this.PageLoad);
            this.Init += new System.EventHandler(this.PageInit);

            base.OnInit(e);
        }

        public static Dictionary<string, Object> GetLeftTreeFrameWatchedValues(CPageData dataLoan)
        {
            if ((dataLoan.DataState == E_DataState.InitSave))
            {
                var values = new Dictionary<string, Object>()
                {
                    { "sLPurposeT", dataLoan.sLPurposeT},
                    { "sLT", dataLoan.sLT}
                };

                return values;
            }

            return null;
        }

        public static bool ShouldRefreshLeftNav(Dictionary<string, Object> initialValues, Dictionary<string, Object> finalValues)
        {
            if (initialValues != null)
            {
                foreach (var key in initialValues.Keys)
                {
                    if (!initialValues.GetValueOrNull(key).Equals(finalValues.GetValueOrNull(key)))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
