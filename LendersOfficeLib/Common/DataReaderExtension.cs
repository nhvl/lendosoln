﻿namespace LendersOffice.Common
{
    using System;
    using System.Data;
    using System.Linq;
    using DataAccess;

    public static class DataReaderExtension
    {
        public static T GetSafeValue<T>(this IDataReader dataReader, string columnName, T defaultValue)
        {
            try
            {
                if (!dataReader.HasColumn(columnName)) return defaultValue;

                object o = dataReader[columnName];
                if (o != null && !Convert.IsDBNull(o))
                {
                    return (T)o;
                }
                else
                {
                    return defaultValue;
                }
            }
            catch (Exception e)
            {
                Tools.LogErrorWithCriticalTracking(e);
                throw;
            }
        }

        public static bool HasColumn(this IDataReader reader, string columnName)
        {
            return reader.GetSchemaTable()
                         .Rows
                         .OfType<DataRow>()
                         .Any(row => (string)row["ColumnName"] == columnName);
        }
    }
}
