﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Constants;
using LqbGrammar.DataTypes;

namespace LendersOffice.Common
{
    public abstract class AbstractBackgroundServiceItem
    {
        private string m_currentMethodName = "";
        protected string CurrentMethodName
        {
            get { return m_currentMethodName; }
        }

        protected int sFileVersion
        {
            get { return GetInt("sFileVersion", ConstAppDavid.SkipVersionCheck); }
        }

        protected Guid sLId
        {
            get { return GetGuid("loanid"); }
        }
        protected Guid aAppId
        {
            get { return GetGuid("applicationID", Guid.Empty); }
        }
        protected abstract CPageData ConstructPageDataClass(Guid sLId);
        protected abstract void BindData(CPageData dataLoan, CAppData dataApp);
        protected abstract void LoadData(CPageData dataLoan, CAppData dataApp);

        private BaseSimpleServiceXmlPage m_baseXmlPage = null;
        public BaseSimpleServiceXmlPage BaseXmlPage
        {
            set { m_baseXmlPage = value; }
        }

        public void MainProcess(string methodName)
        {
            m_currentMethodName = methodName;
            switch (methodName)
            {
                case "CalculateData":
                    CalculateData();
                    break;
                case "SaveData":
                    SaveData();
                    break;
                default:
                    Process(methodName);
                    break;
            }
        }
        protected virtual void Process(string methodName)
        {
            // 12/8/2004 dd - Override this method to handle method other than CalculateData, SaveData
        }

        //av opm 27411
        protected virtual void AfterSaveAndLoadPageDataCallback(CPageData data)
        {

        }

        public void SaveData()
        {
            CPageData dataLoan = ConstructPageDataClass(sLId);
            dataLoan.InitSave(sFileVersion);


            CAppData dataApp = null;

            if (dataLoan.nApps != 0)
            {
                if (aAppId == Guid.Empty)
                    dataApp = dataLoan.GetAppData(0);
                else
                    dataApp = dataLoan.GetAppData(aAppId);
            }

            BindData(dataLoan, dataApp);

            dataLoan.Save();

            SetResult("sFileVersion", dataLoan.sFileVersion);
            LoadData(dataLoan, dataApp);

            AfterSaveAndLoadPageDataCallback(dataLoan);
        }

        public void CalculateData()
        {
            CPageData dataLoan = ConstructPageDataClass(sLId);
            dataLoan.InitLoad();

            Guid appID = GetGuid("applicationID", Guid.Empty);
            CAppData dataApp = null;

            if (dataLoan.nApps != 0)
            {
                if (aAppId == Guid.Empty)
                    dataApp = dataLoan.GetAppData(0);
                else
                    dataApp = dataLoan.GetAppData(aAppId);
            }

            BindData(dataLoan, dataApp);

            LoadData(dataLoan, dataApp);
        }


        #region Get Argument methods

        public T GetEnum<T>(string key) where T : struct, IConvertible
        {
            return (T)(object)GetInt(key);
        }

        public string GetString(string key)
        {
            return m_baseXmlPage.GetString(key);
        }

        public string GetString(string key, string defaultValue)
        {
            return m_baseXmlPage.GetString(key, defaultValue);
        }
        public bool GetBool(string key)
        {
            return m_baseXmlPage.GetBool(key);
        }
        public bool GetBool(string key, bool defaultValue)
        {
            return m_baseXmlPage.GetBool(key, defaultValue);
        }

        public int GetInt(string key)
        {
            return m_baseXmlPage.GetInt(key);
        }
        public int GetInt(string key, int defaultValue)
        {
            return m_baseXmlPage.GetInt(key, defaultValue);
        }

        public Guid GetGuid(string key)
        {
            return m_baseXmlPage.GetGuid(key);
        }
        public Guid GetGuid(string key, Guid defaultValue)
        {
            return m_baseXmlPage.GetGuid(key, defaultValue);
        }


        public E_TriState GetTriState(string key)
        {
            return m_baseXmlPage.GetTriState(key);
        }

        public E_TriState GetTriState(string key, E_TriState defaultValue)
        {
            return m_baseXmlPage.GetTriState(key, defaultValue);
        }
        #endregion
        #region Set result methods
        public void SetResult(string key, string value)
        {
            m_baseXmlPage.SetResult(key, value);
        }
        public void SetResult(string key, bool value)
        {
            m_baseXmlPage.SetResult(key, value);
        }
        public void SetResult(string key, int value)
        {
            m_baseXmlPage.SetResult(key, value);
        }
        public void SetResult(string key, E_TriState value, bool forDropdown = false)
        {
            m_baseXmlPage.SetResult(key, value, forDropdown);
        }
        public void SetResult(string key, Enum value)
        {
            m_baseXmlPage.SetResult(key, value);
        }
        public void SetResult(string key, object value)
        {
            m_baseXmlPage.SetResult(key, value);
        }
        #endregion
    }

}
