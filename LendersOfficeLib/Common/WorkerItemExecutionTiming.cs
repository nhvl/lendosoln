﻿// <copyright file="WorkerItemExecutionTiming.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is WorkerItemExecutionTiming class.
// Author: David
// Date: 5/31/2015
// </summary>
namespace LendersOffice.Common
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using Newtonsoft.Json;

    /// <summary>
    /// A timing info of each item process in single task run.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class WorkerItemExecutionTiming : IDisposable
    {
        /// <summary>
        /// Constant string.
        /// </summary>
        public const string OKStatus = "OK";

        /// <summary>
        /// Constant string.
        /// </summary>
        public const string ErrorStatus = "Error";

        /// <summary>
        /// Stop watch.
        /// </summary>
        private Stopwatch globalStopwatch;

        /// <summary>
        /// When the item is process.
        /// </summary>
        [JsonProperty("timestamp")]
        private DateTime startDate;

        /// <summary>
        /// The description of the item.
        /// </summary>
        [JsonProperty("desc")]
        private string description;

        /// <summary>
        /// When this item insert in queue.
        /// </summary>
        [JsonProperty("inserted_time")]
        private DateTime insertedInQueueTimeUtc;

        /// <summary>
        /// Error message.
        /// </summary>
        [JsonProperty("error", NullValueHandling = NullValueHandling.Ignore)]
        private string errorMessage;

        /// <summary>
        /// Execution time in milliseconds.
        /// </summary>
        [JsonProperty("ms")]
        private long executionTimeInMs;

        /// <summary>
        /// The wait time that item insert in queue and when it pick up to be process.
        /// </summary>
        [JsonProperty("wait_ms")]
        private int waitTimeInMs;

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkerItemExecutionTiming" /> class.
        /// </summary>
        /// <param name="desc">Description of the item.</param>
        /// <param name="insertedInQueueTime">Time that item inserted in queue.</param>
        internal WorkerItemExecutionTiming(string desc, DateTime insertedInQueueTime)
        {
            this.startDate = DateTime.UtcNow;
            this.description = desc;
            this.globalStopwatch = Stopwatch.StartNew();
            this.Status = OKStatus;
            this.insertedInQueueTimeUtc = insertedInQueueTime.ToUniversalTime();

            TimeSpan ts = DateTime.UtcNow - this.insertedInQueueTimeUtc;
            if (ts.TotalMilliseconds > 0)
            {
                this.waitTimeInMs = (int)ts.TotalMilliseconds;
            }
        }

        /// <summary>
        /// Gets the status of the item.
        /// </summary>
        /// <value>Status of the item.</value>
        [JsonProperty("status")]
        public string Status { get; private set; }

        /// <summary>
        /// If exception occur during processing then record this to log.
        /// </summary>
        /// <param name="exc">Exception object.</param>
        public void RecordError(Exception exc)
        {
            if (exc != null)
            {
                this.errorMessage = exc.ToString();
            }

            this.Status = ErrorStatus;
        }

        /// <summary>
        /// Stop timer.
        /// </summary>
        public void Dispose()
        {
            this.globalStopwatch.Stop();
            this.executionTimeInMs = this.globalStopwatch.ElapsedMilliseconds;
        }
    }
}