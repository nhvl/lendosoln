﻿namespace LendersOffice.Common
{
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using LendersOffice.ObjLib.Profiling.PerformanceMonitor;

    /// <summary>
    /// Provides a set of extensions for data adapters.
    /// </summary>
    public static class DataAdapterExtension
    {
        /// <summary>
        /// Updates the adapter without profiling.
        /// </summary>
        /// <param name="adapter">
        /// The adapter to update the data set.
        /// </param>
        /// <param name="updateCommand">
        /// The command used for the update.
        /// </param>
        /// <param name="dataset">
        /// The data set to update.
        /// </param>
        /// <param name="tableName">
        /// The name of the table for the data set.
        /// </param>
        /// <param name="attachRowUpdatingEvent">
        /// Action to attach a row updating event to the adapter.
        /// </param>
        /// <remarks>
        /// This method is primarily intended for use with the loan
        /// save mechanism to preserve the row updating event, which
        /// caps the values of certain parameters to within database
        /// limits. See <see cref="DataAccess.CPageData.Save"/> for
        /// additional details.
        /// </remarks>
        public static void UpdateWithoutProfiling(
            this DbDataAdapter adapter, 
            DbCommand updateCommand,
            DataSet dataset, 
            string tableName,
            Action<SqlDataAdapter> attachRowUpdatingEvent)
        {
            if (adapter is PerformanceMonitorDbDataAdapter)
            {
                adapter = ((PerformanceMonitorDbDataAdapter)adapter).InternalAdapter;
            }

            if (updateCommand is PerformanceMonitorDbCommand)
            {
                updateCommand = ((PerformanceMonitorDbCommand)updateCommand).InternalCommand;
            }

            var sqlAdapter = adapter as SqlDataAdapter;
            var sqlCommand = updateCommand as SqlCommand;

            if (sqlAdapter == null || sqlCommand == null)
            {
                throw new InvalidOperationException(nameof(UpdateWithoutProfiling) + " called in an unsupported context.");
            }

            sqlAdapter.UpdateCommand = sqlCommand;
            attachRowUpdatingEvent(sqlAdapter);

            sqlAdapter.Update(dataset, tableName);
        }
    }
}
