﻿namespace LendersOffice.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.TitleProvider;
    using LendersOffice.Security;
    using LendersOffice.Services;

    /// <summary>
    /// This class is used to contain the logic for determining a LoanNavigationItem's visibility.
    /// </summary>
    public class NavigationVisibility
    {
        /// <summary>
        /// A dictionary mapping a page id to it's corresponding function for determining its display name.
        /// </summary>
        private static readonly Dictionary<string, Action<LoanNavigationItem, CPageData, BrokerDB>> NameChangeFunctions = new Dictionary<string, Action<LoanNavigationItem, CPageData, BrokerDB>>()
        {
            {
                "QM2015",
                new Action<LoanNavigationItem, CPageData, BrokerDB>((item, dataLoan, broker) => item.Name = dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015 ? "QM" : item.Name)
            },
            {
                "Folder_DisclosuresGFETILlegacy",
                new Action<LoanNavigationItem, CPageData, BrokerDB>((item, dataLoan, broker) => item.Name = dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy ? "Disclosures" : item.Name)
            },
            {
                "Folder_DisclosuresTRIDnew",
                new Action<LoanNavigationItem, CPageData, BrokerDB>((item, dataLoan, broker) => item.Name = dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015 ? "Disclosures TRID" : item.Name)
            },
            {
                "Folder_DisclosuresGFETILnew",
                new Action<LoanNavigationItem, CPageData, BrokerDB>((item, dataLoan, broker) => item.Name = dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015 ? "Disclosures GFE/TIL" : item.Name)
            },
            {
                "Page_ChangeOfCircumstancesNew",
                new Action<LoanNavigationItem, CPageData, BrokerDB>((item, dataLoan, broker) => item.Name = dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015 ? "Change of Circumstances" : item.Name)
            },
            {
                "RActiveNewGfe",
                new Action<LoanNavigationItem, CPageData, BrokerDB>((item, dataLoan, broker) => item.Name = dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015 ? "GFE" : item.Name)
            },
            {
                "RActiveNewGfeArchive",
                new Action<LoanNavigationItem, CPageData, BrokerDB>((item, dataLoan, broker) => item.Name = dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015 ? "GFE Archive" : item.Name)
            },
        };

        /// <summary>
        /// A Dictionary mapping a page id to it's corresponding visibility function.  The function can be used to determine if the page is visible in the page navigation.
        /// </summary>
        private static readonly Dictionary<string, Func<LoanNavigationItem, CPageData, BrokerDB, bool>> VisbilityFunctions = new Dictionary<string, Func<LoanNavigationItem, CPageData, BrokerDB, bool>>()
        {
            {
                "Page_CreateTemplate",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sLpqLoanNumber == 0)
            },
            {
                "QM",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => item.ParentId.Equals("Folder_ObsoleteDisclosuresGFETILlegacy") 
                || item.ParentId.Equals("Folder_Favorites") 
                || dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.ClosingCostFee2015)
            },
            {
                "QM2015",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy)
            },
            {
                "Folder_DisclosuresGFETILlegacy",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.ClosingCostFee2015)
            },
            {
                "Folder_DisclosuresTRIDnew",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy && dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            },
            {
                "Folder_DisclosuresGFETILnew",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy && dataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
            },
            {
                "Folder_ObsoleteDisclosuresGFETILlegacy",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015)
            },
            {
                "FHATotalAudit",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.IsTotalScorecardEnabled && BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) 
                && BrokerUser.HasPermission(Permission.CanAccessTotalScorecard))
            },
            {
                "FHATOTALFindings",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.IsTotalScorecardEnabled && BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan) 
                && BrokerUser.HasPermission(Permission.CanAccessTotalScorecard))
            },
            {
                "Page_ExportLPQ",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.HasLPQExportEnabled)
            },
            {
                "Page_PMLDefaultValues",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.IsTemplate && BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
            },
            {
                "Page_Funding",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.HasLenderDefaultFeatures && BrokerUser.HasPermission(Permission.AllowCloserRead))
            },
            {
                "Folder_Underwriting",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.HasLenderDefaultFeatures && BrokerUser.HasPermission(Permission.AllowUnderwritingAccess))
            },
            {
                "Folder_PriceMyLoan",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
            },
            {
                "Page_Certificate",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
            },
            {
                "Page_PmlReviewInfo",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
            },
            {
                "Page_ViewBrokerNotes",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
            },
            {
                "Page_QualifiedLoanProgramSearch",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasFeatures(E_BrokerFeatureT.PriceMyLoan))
            },
            {
                "Electronic_Docs",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.IsEDocsEnabled
                && BrokerUser.HasPermission(Permission.CanViewEDocs) && (dataLoan.sLoanFileT == E_sLoanFileT.Loan || dataLoan.sLoanFileT == E_sLoanFileT.Test))
            },
            {
                "InvestorRolodexInfo",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasPermission(Permission.AllowViewingInvestorInformation))
            },
            {
                "Page_TrailingDocuments",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasPermission(Permission.AllowAccountantRead) && BrokerUser.HasPermission(Permission.AllowViewingInvestorInformation))
            },
            {
                "Folder_Closer",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasPermission(Permission.AllowCloserRead))
            },
            {
                "Folder_Finance",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasPermission(Permission.AllowAccountantRead))
            },
            {
                "Folder_Shipping",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasPermission(Permission.AllowAccountantRead))
            },
            {
                "Folder_Servicing",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasPermission(Permission.AllowAccountantRead))
            },
            {
                "Folder_Purchasing",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent 
                && BrokerUser.HasPermission(Permission.AllowAccountantRead))
            },
            {
                "Folder_LockDesk",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasPermission(Permission.AllowLockDeskRead))
            },
            {
                "Page_Disbursement",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasPermission(Permission.AllowCloserRead) && BrokerUser.HasPermission(Permission.AllowAccountantRead)) },
            {
                "Page_Disbursement_Purchasing",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasPermission(Permission.AllowCloserRead) && BrokerUser.HasPermission(Permission.AllowAccountantRead))
            },
            {
                "Page_Accounting",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasPermission(Permission.AllowLockDeskRead) && BrokerUser.HasPermission(Permission.AllowCloserRead)
                && BrokerUser.HasPermission(Permission.AllowAccountantRead))
            },
            {
                "Page_Servicing",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasPermission(Permission.AllowCloserRead) && BrokerUser.HasPermission(Permission.AllowAccountantRead))
            },
            {
                "Page_SecondaryStatus",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) =>
                BrokerUser.HasPermission(Permission.AllowCloserRead)
                && BrokerUser.HasPermission(Permission.AllowAccountantRead)
                && BrokerUser.HasPermission(Permission.AllowLockDeskRead))
            },
            {
                "Page_StatusCommissions",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasPermission(Permission.AllowCloserRead) && BrokerUser.HasPermission(Permission.AllowAccountantRead))
            },
            {
                "Page_LendingStaffNotes",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasPermission(Permission.CanAccessLendingStaffNotes))
            },
            {
                "Page_DocuTech",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => (broker.IsDocuTechEnabled || broker.IsDocuTechStageEnabled) && broker.DocuTechInfo == null)
            },
            {
                "Page_DocuTech_1",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => (broker.IsDocuTechEnabled || broker.IsDocuTechStageEnabled) && broker.DocuTechInfo != null)
            },
            {
                "Page_CheckEligibility",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasRole(E_RoleT.Underwriter) || BrokerUser.HasRole(E_RoleT.JuniorUnderwriter)
                || BrokerUser.IsInEmployeeGroup(ConstAppDavid.CheckEligibilityEmployeeGroup))
            },
            {
                "Page_NewTaskList",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.IsUseNewTaskSystem)
            },
            {
                "Page_CreateNewTask",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => !broker.IsUseNewTaskSystem)
            },
            {
                "Page_TaskList",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => !broker.IsUseNewTaskSystem)
            },
            {
                "Page_StatusConditions",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasLenderDefaultFeatures)
            },
            {
                "Page_NewStatusConditions",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasLenderDefaultFeatures)
            },
            {
                "Page_UnderwritingConditions",
               new Func<LoanNavigationItem, CPageData, BrokerDB, bool>(
                   (item, dataLoan, broker) => BrokerUser.HasLenderDefaultFeatures == true
                   && broker.IsUseNewTaskSystem == false
                   && (BrokerUser.HasPermission(Permission.AllowUnderwritingAccess) || BrokerUser.HasPermission(Permission.AllowCloserRead)))
            },
            {
                "Page_UnderwritingConditionsForTask",
               new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasLenderDefaultFeatures == true && broker.IsUseNewTaskSystem == true
                && (BrokerUser.HasPermission(Permission.AllowUnderwritingAccess) || BrokerUser.HasPermission(Permission.AllowCloserRead)))
            },
            {
                "Page_ConditionSignoff",
               new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasLenderDefaultFeatures == true && broker.IsUseNewTaskSystem == true
                && (BrokerUser.HasPermission(Permission.AllowUnderwritingAccess) || BrokerUser.HasPermission(Permission.AllowCloserRead)))
            },
            {
                "Folder_Funding",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasPermission(Permission.AllowCloserRead))
            },
            {
                "Page_Duplicate",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sLpqLoanNumber == 0
                && BrokerUser.HasPermission(Permission.CanDuplicateLoans)
                && (dataLoan.sLoanFileT == E_sLoanFileT.Loan || dataLoan.sLoanFileT == E_sLoanFileT.Test))
            },
            {
                "Page_CreateTest",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasPermission(Permission.AllowCreatingTestLoans) && dataLoan.sLoanFileT == E_sLoanFileT.Loan)
            },
            {
                "Page_TestConfig",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sLoanFileT == E_sLoanFileT.Test)
            },
            {
                "Page_Sandbox",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => false)
            },
            {
                "Page_SandboxList",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.IsSandboxEnabled && !dataLoan.IsTemplate && dataLoan.sLoanFileT == E_sLoanFileT.Loan)
            },
            {
                "Page_CreateSecondLoan",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sLpqLoanNumber == 0 && (dataLoan.sLoanFileT == E_sLoanFileT.Loan || dataLoan.sLoanFileT == E_sLoanFileT.Test))
            },
            {
                "Page_DocMagic",
               new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => (broker.BillingVersion == E_BrokerBillingVersion.PerTransaction && broker.IsEnablePTMDocMagicOnlineInterface)
                   || broker.BillingVersion != E_BrokerBillingVersion.PerTransaction)
            },
            {
                "Page_FHAConnection",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.IsEnableFhaConnection)
            },
            {
                "Page_FHAConnectionResults",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.IsEnableFhaConnection)
            },
            {
                "Page_DataVerifyDRIVE",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.IsEnableDriveIntegration)
            },
            {
                "Page_2010GFE",
               new Func<LoanNavigationItem, CPageData, BrokerDB, bool>(
                   (item, dataLoan, broker) => (!string.IsNullOrEmpty(item.ParentId)
                   && (item.ParentId.Equals("Folder_ObsoleteDisclosuresGFETILlegacy") || item.ParentId.Equals("Folder_Favorites") || item.ParentId.Equals("Folder_DisclosuresGFETILlegacy")))
                   || (dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.ClosingCostFee2015
                   && dataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID))
            },
            {
                "Page_FeeAudit",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            },
            {
                "Page_2010GFEArchive",
               new Func<LoanNavigationItem, CPageData, BrokerDB, bool>(
                   (item, dataLoan, broker) => broker.IsGFEandCoCVersioningEnabled &&
                   (item.ParentId.Equals("Folder_ObsoleteDisclosuresGFETILlegacy") || item.ParentId.Equals("Folder_Favorites") || dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy))
            },
            {
                "Page_TitleQuoteRequest",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasPermission(Permission.AllowOrderingTitleServices)
                && TitleProvider.GetAssociations(BrokerUser.BrokerId).Count > 0)
            },
            {
                "Page_ChangeOfCircumstances",
               new Func<LoanNavigationItem, CPageData, BrokerDB, bool>(
                   (item, dataLoan, broker) => item.ParentId.Equals("Folder_ObsoleteDisclosuresGFETILlegacy")
                   || item.ParentId.Equals("Folder_Favorites")
                   || dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
            },
            {
                "Page_ChangeOfCircumstancesNew",
               new Func<LoanNavigationItem, CPageData, BrokerDB, bool>(
                   (item, dataLoan, broker) => dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy
                   && broker.IsGFEandCoCVersioningEnabled)
            },
            {
                "Page_LoanRepurchase",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.EnableLoanRepurchasePage)
            },
            {
                "DocVendorPortal",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>(
                    (item, dataLoan, broker) => 
                    broker.IsEnablePTMDocMagicSeamlessInterface && 
                    broker.GetDocumentVendors(dataLoan.sLoanFileT == E_sLoanFileT.Test, true)
                        .Any(vendor => vendor.VendorId != Guid.Empty && Integration.DocumentVendor.VendorConfig.Retrieve(vendor.VendorId).PlatformType == E_DocumentVendor.IDS))
            },
            {
                "Page_Order4056T",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.Has4506TIntegration && BrokerUser.HasPermission(Permission.AllowOrder4506T))
            },
            {
                "Page_OrderMIPolicy",
               new Func<LoanNavigationItem, CPageData, BrokerDB, bool>(
                   (item, dataLoan, broker) => broker.HasMIVendorIntegration && BrokerUser.HasPermission(Permission.AllowOrderingPMIPolicies)
                   && (dataLoan.sLoanFileT == E_sLoanFileT.Loan || dataLoan.sLoanFileT == E_sLoanFileT.Test) && !dataLoan.IsTemplate)
            },
            {
                "Page_FreddieLoanQualityAdvisorExport",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.IsEnableFreddieMacLoanQualityAdvisor)
            },
            {
                "Page_FHLMCLQA",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.IsEnableFreddieMacLoanQualityAdvisor)
            },
            {
                "Page_FannieMaeEarlyCheck",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.IsEnableFannieMaeEarlyCheck)
            },
            {
                "Page_AdditionalHELOC",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.IsEnableHELOC && (dataLoan.IsTemplate || dataLoan.sIsLineOfCredit))
            },
            {
                "Page_DUSubmission",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.IsEnableSeamlessDu)
            },
            {
                "Page_LoanOfficerPricingPolicy",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.IsCustomPricingPolicyFieldEnabled)
            },
            {
                "Page_SettlementCharges",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.ClosingCostFee2015)
            },
            {
                "Page_AdditionalHUD1Data",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
            },
            {
                "Page_SettlementChargesMigration",
               new Func<LoanNavigationItem, CPageData, BrokerDB, bool>(
                   (item, dataLoan, broker) => (!dataLoan.IsTemplate && broker.IsGFEandCoCVersioningEnabled && (!dataLoan.sUseGFEDataForSCFields || dataLoan.sHasClosingCostMigrationArchive))
                   && (string.IsNullOrEmpty(item.ParentId)
                   || ((dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.ClosingCostFee2015) ? item.ParentId.Equals("Folder_Obsolete") : item.ParentId.Equals("Folder_Funding"))))
            },
            {
                "RActiveNewGfe",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy
                && dataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
            },
            {
                "RActiveNewGfeArchive",
               new Func<LoanNavigationItem, CPageData, BrokerDB, bool>(
                   (item, dataLoan, broker) => broker.IsGFEandCoCVersioningEnabled && dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy
                   && dataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
            },
            {
                "Page_SellerClosingDisclosure",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy 
                && dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            },
            {
                "Page_SellerClosingDisclosureArchive",
               new Func<LoanNavigationItem, CPageData, BrokerDB, bool>(
                   (item, dataLoan, broker) => broker.IsGFEandCoCVersioningEnabled
                   && IsInternalUser
                   && dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy
                   && dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            },
            {
                "Page_SellerClosingCosts",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy 
                && dataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
            },
            {
                "Page_SellerClosingCostsArchive",
               new Func<LoanNavigationItem, CPageData, BrokerDB, bool>(
                   (item, dataLoan, broker) => broker.IsGFEandCoCVersioningEnabled && IsInternalUser
                   && dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy
                   && dataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
            },
            {
                "Page_EmployeeResources",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => !broker.EmployeeResourcesUrl.Equals(string.Empty))
            },
            {
                "Page_LoanEstimate",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            },
            {
                "Page_ClosingDisclosure",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            },
            {
                "Page_LoanTerms",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            },
            {
                "Page_BorrowerClosingCosts",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            },
            {
                "Page_AdjustmentsAndOtherCredits",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            },
            {
                "Page_ToleranceCure",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            },
            {
                "Page_LenderCredits",
               new Func<LoanNavigationItem, CPageData, BrokerDB, bool>(
                   (item, dataLoan, broker) => dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy
                   || broker.EnableLenderCreditsWhenInLegacyClosingCostMode)
            },
            {
                "Page_PropHousingExpenses",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy)
            },
            {
                "Page_LoanEstimateArchive",
               new Func<LoanNavigationItem, CPageData, BrokerDB, bool>(
                   (item, dataLoan, broker) => dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID
                    && broker.IsGFEandCoCVersioningEnabled
                    && dataLoan.sHasLoanEstimateArchive)
            },
            {
                "Page_ClosingDisclosureArchive",
               new Func<LoanNavigationItem, CPageData, BrokerDB, bool>(
                   (item, dataLoan, broker) => dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID
                   && broker.IsGFEandCoCVersioningEnabled && dataLoan.sHasClosingDisclosureArchive)
            },
            {
                "Page_InternalSupportPage",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => IsInternalUser)
            },
            {
                "Page_ManageNewFeatures",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasPermission(Permission.AllowManageNewFeatures))
            },
            {
                "Page_DataLayerMigration",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => BrokerUser.HasPermission(Permission.AllowManageNewFeatures))
            },
            {
                "Page_BigLoanInformation",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.IsEnableBigLoanPage)
            },
            {
                "Page_LoanInformation",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => !broker.IsEnableBigLoanPage)
            },
            {
                "Page_TIL",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => !string.Equals(item.ParentId, "Folder_Disclosure") || dataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
            },
            {
                "Page_StatusGeneral",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => !broker.IsEditLeadsInFullLoanEditor || !Tools.IsStatusLead(dataLoan.sStatusT))
            },
            {
                "Page_StatusAgents",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => !broker.IsEditLeadsInFullLoanEditor || !Tools.IsStatusLead(dataLoan.sStatusT))
            },
            {
                "Page_LeadAssignment",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.IsEditLeadsInFullLoanEditor && Tools.IsStatusLead(dataLoan.sStatusT))
            },
            {
                "Page_MismoClosing33",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sIsInTRID2015Mode && !dataLoan.sIsLegacyClosingCostVersion)
            },
            {
                "Page_ImportFromSymitar",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.GetEnabledSymitarLenderConfig(dataLoan.sLoanFileT)?.AllowImportFromSymitar ?? false)
            },
            {
                "Page_ExportToSymitar",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => broker.GetEnabledSymitarLenderConfig(dataLoan.sLoanFileT)?.AllowExportToSymitar ?? false)
            },
            {
                "Folder_FHA",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sLT == E_sLT.FHA)
            },
            {
                "Folder_VA",
                new Func<LoanNavigationItem, CPageData, BrokerDB, bool>((item, dataLoan, broker) => dataLoan.sLT == E_sLT.VA)
            }
        };

        // I do not believe this is the way to add code to the system upon Nav Click.  Besides that, I don't believe the equivalent of alert is available in the new UI yet.
        // The logic has been kept here for future reference.
        /*
        private static readonly Dictionary<string, Action<LoanNavigationItem, CPageData, BrokerDB>> onClickChangeList = new Dictionary<string, Action<LoanNavigationItem, CPageData, BrokerDB>>()
        {
            {
                 "Page_2010GFEArchive",
                new Action<LoanNavigationItem, CPageData, BrokerDB>(
                    (item, dataLoan, broker) => item.OnClick = (string.IsNullOrEmpty(dataLoan.sLastDisclosedGFEArchiveD_rep) || !dataLoan.sDoesGfeArchivesDataExist) ?
                    "alert('No GFE Archive on file.  "
                    + "Please create one before attempting to access the archives.');"
                    + "return false;" : item.OnClick)
            },
            {
                 "Page_ChangeOfCircumstances",
                new Action<LoanNavigationItem, CPageData, BrokerDB>(
                    (item, dataLoan, broker) => item.OnClick = broker.IsGFEandCoCVersioningEnabled && (string.IsNullOrEmpty(dataLoan.sLastDisclosedGFEArchiveD_rep) || !dataLoan.sDoesGfeArchivesDataExist) ?
                    item.OnClick = "alert('No GFE Archive on file.  "
                    + "Please create one before attempting to apply a Change of Circumstance.');"
                    + "return false;"
                    : "return f_viewChangeOfCircumstancesForArchives(item, dataLoan, broker);")
            },
            {
                 "Page_ChangeOfCircumstancesNew",
                new Action<LoanNavigationItem, CPageData, BrokerDB>(
                    (item, dataLoan, broker) => item.OnClick = !dataLoan.sHasLoanEstimateArchive && !dataLoan.sHasGfe2015Archive ?
                    item.OnClick = "alert('No Archive on file.  "
                    + "Please create one before attempting to apply a Change of Circumstance.');"
                    + "return false;"
                    : "return f_viewChangeOfCircumstancesForCcArchive(item, dataLoan, broker);")
            },
             { "VendorPortal", new Action<LoanNavigationItem, CPageData, BrokerDB>((item, dataLoan, broker) => item.OnClick = "return f_openVendorPortal('" + dataLoan.sDocVendorPortalUrl + "');") },
            {
                 "RActiveNewGfeArchive", new Action<LoanNavigationItem, CPageData, BrokerDB>(
                    (item, dataLoan, broker) => item.OnClick = item.OnClick = !dataLoan.sHasGfe2015Archive ? "alert('No GFE Archive on file.  "
                    + "Please create one before attempting to access the archives.');"
                    + "return false;" : item.OnClick)
            },
            {
                 "Page_SellerClosingDisclosureArchive", new Action<LoanNavigationItem, CPageData, BrokerDB>(
                    (item, dataLoan, broker) => item.OnClick = dataLoan.sClosingCostArchive.Count() == 0 ?
                    "alert('No Closing Cost Archive on file.  "
                    + "Please create one before attempting to access the archives.');"
                    + "return false;" : item.OnClick)
            },
            {
                 "Page_SellerClosingCostsArchive", new Action<LoanNavigationItem, CPageData, BrokerDB>(
                    (item, dataLoan, broker) => item.OnClick = dataLoan.sClosingCostArchive.Count() == 0 ?
                    "alert('No Closing Cost Archive on file.  "
                    + "Please create one before attempting to access the archives.');"
                    + "return false;" : item.OnClick)
            },
            {
                 "Page_ClosingDisclosureArchive", new Action<LoanNavigationItem, CPageData, BrokerDB>(
                    (item, dataLoan, broker) => item.OnClick = !dataLoan.sHasClosingDisclosureArchive ?
                    "alert('No Closing Disclosure Archive on file.  "
                    + "Please create one before attempting to access the archives.');"
                    + "return false;" : item.OnClick)
            },
        };
        */

        /// <summary>
        /// Initializes a new instance of the <see cref="NavigationVisibility"/> class.
        /// </summary>
        /// <param name="loanId">The id of the loan that will be used to determine a page's visible status.</param>
        public NavigationVisibility(Guid loanId)
        {
            this.DataLoan = new CPageData(
                loanId,
                "PageNavigation",
                new List<string>()
                {
                    "sClosingCostFeeVersionT",
                    "sLpqLoanNumber",
                    "sDisclosureRegulationT",
                    "IsTemplate",
                    "sLoanFileT",
                    "sBranchChannelT",
                    "sDocVendorPortalUrl",
                    "sIsLineOfCredit",
                    "sUseGFEDataForSCFields",
                    "sHasClosingCostMigrationArchive",
                    "sHasLoanEstimateArchive",
                    "sHasClosingDisclosureArchive",
                    "sStatusT",
                    "sIsInTRID2015Mode",
                    "sIsLegacyClosingCostVersion",
                    "sLT"
                });

            this.DataLoan.InitLoad();
        }

        /// <summary>
        /// Gets the user principal for the current user.
        /// </summary>
        /// <value>An AbstractUserPrincipal for the current user.</value>
        public static AbstractUserPrincipal BrokerUser
        {
            get
            {
                return PrincipalFactory.CurrentPrincipal;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the Broker user is internal.
        /// </summary>
        /// <value>A boolean determining whether the Broker user is internal.</value>
        public static bool IsInternalUser
        {
            get
            {
                return BrokerUser.HasPermission(Permission.CanModifyLoanPrograms);
            }
        }

        /// <summary>
        /// Gets or sets the loan object for this instance.
        /// </summary>
        private CPageData DataLoan
        {
            get; set;
        }

        /// <summary>
        /// Gets the user's broker object.
        /// </summary>
        private BrokerDB Broker
        {
            get
            {
                return PrincipalFactory.CurrentPrincipal.BrokerDB;
            }
        }

        /// <summary>
        /// This is a handler containing the same logic as LeftTreeFrame.  It takes a LoanNavigationItem and determines if it's visible.
        /// </summary>
        /// <param name="item">The item to determine whether it's visible.</param>
        public void TreeNode_Create(LoanNavigationItem item)
        {
            if (item.Id == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "Node " + item.Name + " does not have ID");
            }

            Func<LoanNavigationItem, CPageData, BrokerDB, bool> visibilityFunction;
            if (VisbilityFunctions.TryGetValue(item.Id, out visibilityFunction))
            {
                item.IsVisible = visibilityFunction(item, this.DataLoan, this.Broker);
            }

            // Check if the name should be changed.
            Action<LoanNavigationItem, CPageData, BrokerDB> nameChangeFunction;
            if (NameChangeFunctions.TryGetValue(item.Id, out nameChangeFunction))
            {
                nameChangeFunction(item, this.DataLoan, this.Broker);
            }
        }
    }
}
