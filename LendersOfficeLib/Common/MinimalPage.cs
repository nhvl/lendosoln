﻿namespace LendersOffice.Common
{
    using System;

    /// <summary>
    /// A class that inherits from Base Page.  It intends to minimize the number of scripts inserted to a page, but still provides
    /// the functionality of BasePage if needed, such as VRoot, and the registration of scripts.
    /// </summary>
    public class MinimalPage : BasePage
    {
        /// <summary>
        /// This bool determines whether a script will be run for checking whether the browser is IE.  Defaults to false to avoid
        /// breaking any pages.
        /// </summary>
        /// <value>Always false.</value>
        protected override bool EnableIEOnlyCheck
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// This method occurs before the init method.
        /// </summary>
        /// <param name="e">The asp event arguments.</param>
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
            this.m_loadDefaultStylesheet = false;
        }
    }
}
