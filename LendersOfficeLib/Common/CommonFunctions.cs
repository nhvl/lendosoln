﻿namespace LendersOffice.Common
{
    using System;
    using System.Collections.Generic;
    using Constants;
    using DataAccess;
    using LendersOffice.Admin;

    public class CommonFunctions
    {
        public static void CopyEmployeeInfoToAgent(Guid brokerId, CAgentFields agent, Guid employeeId)
        {
            CopyEmployeeInfoToAgent(brokerId, agent, employeeId, false);
        }

        public static void CopyEmployeeInfoToAgent(Guid brokerId, CAgentFields agent, Guid employeeId, bool bCopyCommissions)
        {
            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerId);
            EmployeeDB employee = EmployeeDB.RetrieveById(brokerId, employeeId);
            CopyEmployeeInfoToAgent(brokerDB, agent, employee, bCopyCommissions);
        }        

        public static void CopyEmployeeInfoToAgent(BrokerDB brokerDB, CAgentFields agent, EmployeeDB employee, bool bCopyCommissions)
        {
            if (employee.UserType == 'P')
            {
                // This is a "P" User.
                // TODO: Retrieve from PML_BROKER
                PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerById(employee.PmlBrokerId, brokerDB.BrokerID);
                agent.CompanyName = pmlBroker.NmlsName;
                agent.PhoneOfCompany = pmlBroker.Phone;
                agent.FaxOfCompany = pmlBroker.Fax;
                agent.SetRawCompanyLicenseXmlContent(pmlBroker.LicenseInfoList.ToString());

                // We will now only copy the tax id from the OC if it is valid. opm 452912 gf
                if (pmlBroker.TaxIdValid)
                {
                    agent.TaxId = pmlBroker.TaxId;
                }

                if (ConstStage.EnableTaxIdLogging)
                {
                    string msg = null;

                    if (pmlBroker.TaxIdValid)
                    {
                        msg = "[OPM 456490] Setting agent (type '" + agent.AgentRoleTDesc +
                            "') tax ID (original '" + agent.TaxId +
                            "') to PML broker value '" + pmlBroker.TaxId +
                            "' when copying source employee " + employee.FullName;
                    }
                    else
                    {
                        msg = "[OPM 456490] PML broker tax ID is not valid, " +
                            "will not set agent (type '" + agent.AgentRoleTDesc +
                            "') tax ID (original '" + agent.TaxId +
                            "') to PML broker value.";
                    }

                    Tools.LogInfoWithStackTrace(msg);
                }

                agent.StreetAddr = pmlBroker.Addr;
                agent.City = pmlBroker.City;
                agent.State = pmlBroker.State;
                agent.Zip = pmlBroker.Zip;

                agent.CompanyLoanOriginatorIdentifier = pmlBroker.NmLsIdentifier;
            }
            else
            {
                BranchDB branch = BranchDB.RetrieveById(brokerDB.BrokerID, employee.BranchID);

                agent.CompanyName = branch.DisplayNm;// OPM 50281. Populate the Display Name to the agent/official contact record as the Company Name in all cases where we currently populate either the lender's Company Name or the Branch Name.

                // This is a "B" user                                     
                if (branch.IsUseBranchInfoForLoans)
                {   
                    agent.PhoneOfCompany = branch.Phone;
                    agent.FaxOfCompany = branch.Fax;
                    agent.SetRawCompanyLicenseXmlContent(branch.LicenseXmlContent);

                    agent.StreetAddr = branch.Address.StreetAddress;
                    agent.City = branch.Address.City;
                    agent.State = branch.Address.State;
                    agent.Zip = branch.Address.Zipcode;

                    agent.CompanyLoanOriginatorIdentifier = branch.LosIdentifier;

                    foreach (LicenseInfo licenseInfo in agent.CompanyLicenseInfoList)
                    {
                            licenseInfo.DefaultDisplayName = branch.DisplayNm;
                            licenseInfo.DefaultStreet = branch.Address.StreetAddress;
                            licenseInfo.DefaultCity = branch.Address.City;
                            licenseInfo.DefaultAddrState = branch.Address.State;
                            licenseInfo.DefaultZip = branch.Address.Zipcode;
                            licenseInfo.DefaultPhone = branch.Phone;
                            licenseInfo.DefaultFax = branch.Fax;
                    }   
                }
                else
                {                 
                    agent.PhoneOfCompany = brokerDB.Phone;
                    agent.FaxOfCompany = brokerDB.Fax;
                    agent.SetRawCompanyLicenseXmlContent(brokerDB.LicenseInformationList.ToString());

                    agent.StreetAddr = brokerDB.Address.StreetAddress;
                    agent.City = brokerDB.Address.City;
                    agent.State = brokerDB.Address.State;
                    agent.Zip = brokerDB.Address.Zipcode;

                    agent.CompanyLoanOriginatorIdentifier = brokerDB.NmLsIdentifier;

                    foreach (LicenseInfo licenseInfo in agent.CompanyLicenseInfoList)
                    {
                            licenseInfo.DefaultDisplayName = branch.DisplayNm;
                            licenseInfo.DefaultStreet = brokerDB.Address.StreetAddress;
                            licenseInfo.DefaultCity = brokerDB.Address.City;
                            licenseInfo.DefaultAddrState = brokerDB.Address.State;
                            licenseInfo.DefaultZip = brokerDB.Address.Zipcode;
                            licenseInfo.DefaultPhone = brokerDB.Phone;
                            licenseInfo.DefaultFax = brokerDB.Fax;
                    }   
                }

                if (bCopyCommissions)
                {
                    agent.CommissionPointOfLoanAmount = employee.CommissionPointOfLoanAmount;
                    agent.CommissionPointOfGrossProfit = employee.CommissionPointOfGrossProfit;
                    agent.CommissionMinBase = employee.CommissionMinBase;
                }
                else
                {
                    agent.CommissionMinBase = 0;
                    agent.CommissionPointOfLoanAmount = 0;
                    agent.CommissionPointOfGrossProfit = 0;
                }
            }

            agent.EmployeeId = employee.ID;
            if (string.IsNullOrEmpty(employee.NmOnLoanDocs))
                agent.AgentName = employee.FullNmWithMNmAndSuffix;
            else
                agent.AgentName = employee.NmOnLoanDocs;
            agent.Phone = employee.Phone;
            agent.FaxNum = employee.Fax;
            agent.PagerNum = employee.Pager;
            agent.CellPhone = employee.CellPhone;
            agent.EmailAddr = employee.Email;
            agent.LoanOriginatorIdentifier = employee.LosIdentifier;
            agent.SetRawLicenseXmlContent(employee.LicenseInformationList.ToString());
            agent.EmployeeIDInCompany = employee.EmployeeIDInCompany;
            agent.ChumsId = employee.ChumsId;

            ///add the license data
            LicenseInfoList licenses = agent.CompanyLicenseInfoList;
            foreach (LicenseInfo licenseInfo in licenses)
            {
                if (licenseInfo.License == agent.LicenseNumOfCompany)
                {
                    if (licenseInfo.DisplayName != "")
                    {
                        agent.CompanyName = licenseInfo.DisplayName;
                        agent.StreetAddr = licenseInfo.Street;
                        agent.City = licenseInfo.City;
                        agent.State = licenseInfo.AddrState;
                        agent.Zip = licenseInfo.Zip;
                        agent.PhoneOfCompany = licenseInfo.Phone;
                        agent.FaxOfCompany = licenseInfo.Fax;
                    }
                }
            }
        }

        public static bool RoleHasPrecedenceForCommission(E_AgentRoleT role, E_AgentRoleT roleToCompareAgainst)
        {          
            List<E_AgentRoleT> commissionPrecedenceList = new List<E_AgentRoleT>(new E_AgentRoleT[]{E_AgentRoleT.LoanOfficer, E_AgentRoleT.Lender, E_AgentRoleT.CallCenterAgent, E_AgentRoleT.LoanOpener, E_AgentRoleT.Processor});
            if (!commissionPrecedenceList.Contains(role)) return false;
            if (!commissionPrecedenceList.Contains(roleToCompareAgainst)) return true;
            return (commissionPrecedenceList.IndexOf(role) < commissionPrecedenceList.IndexOf(roleToCompareAgainst));
        }

        public static E_AgentRoleT GetEmployeeRoleWithCommissionInLoanFile(Guid employeeId, Guid loanId)
        {
            COfficialAgentData loanData = new COfficialAgentData(loanId);
            loanData.AllowLoadWhileQP2Sandboxed = true;
            loanData.InitLoad();

            List<E_AgentRoleT> rolesToCheck = new List<E_AgentRoleT> { E_AgentRoleT.LoanOfficer, E_AgentRoleT.Processor, E_AgentRoleT.LoanOpener, E_AgentRoleT.Manager, E_AgentRoleT.CallCenterAgent, E_AgentRoleT.Realtor, E_AgentRoleT.Lender};
            foreach(E_AgentRoleT role in rolesToCheck)
            {
                CAgentFields agent = loanData.GetAgentOfRole(role, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if(agent!= CAgentFields.Empty)
                {
                    //if the agent contact was created with the employee and a comission exist for the agent, return the role.
                    if(agent.EmployeeId == employeeId && agent.CommissionPointOfLoanAmount != 0 && agent.CommissionPointOfGrossProfit != 0 && agent.CommissionMinBase !=0)
                    {
                        return role;
                    }
                }                
            }
            return E_AgentRoleT.Other;     
        }

        public static Guid ToGuid_Safe(string s)
        {
            Guid guidOut = Guid.Empty;
            if (string.IsNullOrEmpty(s)) return guidOut;

            try
            {
                guidOut = new Guid(s);
            }
            catch (FormatException)
            {
                return Guid.Empty;
            }
            catch (InvalidCastException)
            {
                return Guid.Empty;
            }            
            return guidOut;
        }
    }
}
