/// Author: David Dao

using System;

namespace LendersOffice.Common
{
	public interface IYieldCpu
	{
        /// <summary>
        /// Return number of seconds that it is used by this function.
        /// </summary>
        /// <param name="maxSeconds"></param>
        /// <returns></returns>
        int Yield(int maxSeconds);
	}
}
