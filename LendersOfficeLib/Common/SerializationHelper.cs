namespace LendersOffice.Common
{
    using Constants;
    using DataAccess;
    using LqbGrammar.DataTypes;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml;
    using System.Xml.Serialization;
    using Newtonsoft.Json.Serialization;
    using System.Reflection;
    using System.Collections;

    public class SerializationHelper 
    {
        static SerializationHelper()
        {
            JsonConvert.DefaultSettings = () => GetJsonNetSerializerSettings();
        }

        public static string XmlSerialize(object o) 
        {
            if (null == o)
            {
                return string.Empty;
            }

            XmlSerializer serializer = new XmlSerializer(o.GetType());
            using (MemoryStream stream = new MemoryStream()) 
            {
                XmlTextWriter writer = new XmlTextWriter(stream, null);

                serializer.Serialize(writer, o);
                
                return System.Text.Encoding.UTF8.GetString(stream.GetBuffer(), 0, (int)stream.Position);
            }
        }

        /// <summary>
        /// Serializes the input object as an XML string.
        /// </summary>
        /// <param name="o">The object to be serialized.</param>
        /// <param name="includeXMLDeclaration">True if an XML declaration should be included. False to omit, such as when the object will be plugged into another XML string with its own root.</param>
        /// <param name="namespaces">A collection of namespaces and associated prefixes referenced by the object.</param>
        /// <param name="overrides">A set of overrides for XML attributes in the serialized object.</param>
        /// <returns>The given object serialized as an XML string.</returns>
        public static string XmlSerialize(object o, bool includeXMLDeclaration, XmlSerializerNamespaces namespaces, XmlAttributeOverrides overrides = null)
        {
            return XmlSerialize(o, includeXMLDeclaration, namespaces, null, overrides);
        }

        /// <summary>
        /// Serializes the input object as an XML string and omits the w3.org namespaces that .Net will otherwise add by default.
        /// </summary>
        /// <param name="o">The object to be serialized.</param>
        /// <param name="includeXMLDeclaration">True if an XML declaration should be included. False to omit, such as when the object will be plugged into another XML string with its own root.</param>
        /// <returns>The given object serialized as an XML string.</returns>
        public static string XmlSerializeStripDefaultNamespace(object o, bool includeXMLDeclaration)
        {
            XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, string.Empty);

            return XmlSerialize(o, includeXMLDeclaration, namespaces);
        }

        /// <summary>
        /// Serializes the input object as an XML string.
        /// </summary>
        /// <param name="o">The object to be serialized.</param>
        /// <param name="includeXMLDeclaration">True if an XML declaration should be included. False to omit, such as when the object will be plugged into another XML string with its own root.</param>
        /// <param name="defaultNamespace">The default namespace to use for all the XML elements.</param>
        /// <returns>The given object serialized as an XML string.</returns>
        public static string XmlSerialize(object o, bool includeXMLDeclaration, string defaultNamespace)
        {
            XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
            namespaces.Add(string.Empty, defaultNamespace);
            return XmlSerialize(o, includeXMLDeclaration, namespaces, defaultNamespace);
        }

        /// <summary>
        /// Serializes the input object as an XML string.
        /// </summary>
        /// <param name="o">The object to be serialized.</param>
        /// <param name="includeXMLDeclaration">True if an XML declaration should be included. False to omit, such as when the object will be plugged into another XML string with its own root.</param>
        /// <param name="namespaces">A collection of namespaces and associated prefixes referenced by the object.</param>
        /// <param name="defaultNamespace">The default namespace to use for all the XML elements.</param>
        /// <param name="overrides">A set of overrides for XML attributes in the serialized object.</param>
        /// <returns>The given object serialized as an XML string.</returns>
        public static string XmlSerialize(object o, bool includeXMLDeclaration, XmlSerializerNamespaces namespaces, string defaultNamespace, XmlAttributeOverrides overrides = null)
        {
            if (null == o)
            {
                return string.Empty;
            }

            XmlSerializer serializer;

            if (overrides == null)
            {
                serializer = new XmlSerializer(o.GetType(), defaultNamespace);
            }
            else
            {
                serializer = new XmlSerializer(o.GetType(), overrides);
            }

            using (MemoryStream stream = new MemoryStream())
            {
                //// XmlWriter is used rather than XmlTextWriter so that the writer setting can be used to specify whether to include/omit the XML declaration.
                //// .Net suggests XmlWriter above XmlTextWriter with v2.0+. This has been implemented as a separate method here to avoid migrating the list of things that reference the overloaded method above (for now).
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.OmitXmlDeclaration = !includeXMLDeclaration;
                settings.Encoding = new System.Text.UTF8Encoding(false); // 2/17/2015 BB - omit BOM;
                XmlWriter writer = XmlWriter.Create(stream, settings);
                
                serializer.Serialize(writer, o, namespaces);

                return System.Text.Encoding.UTF8.GetString(stream.GetBuffer(), 0, (int)stream.Position);
            }
        }

        public static void XmlSerializeToFile(object o, string fileName) 
        {
            if (null == o)
            {
                return;
            }
            XmlSerializer serializer = new XmlSerializer(o.GetType());

            using (XmlWriter writer = XmlTextWriter.Create(fileName))
            {
                serializer.Serialize(writer, o);
            }
        }
        public static T XmlDeserializeFromFile<T>(string fileName) where T : class
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            XmlReaderSettings readerSettings = new XmlReaderSettings();
            readerSettings.XmlResolver = null;
            readerSettings.IgnoreWhitespace = false;
            using (XmlReader reader = XmlReader.Create(fileName, readerSettings))
            {
                return serializer.Deserialize(reader) as T;
            }

        }

        public static T XmlDeserialize<T>(string xml, XmlAttributeOverrides overrides = null) where T : class
        {
            return XmlDeserialize(xml, typeof(T), overrides) as T;
        }
        public static object XmlDeserialize(string xml, Type type, XmlAttributeOverrides overrides = null) 
        {
            try 
            {
                XmlSerializer serializer = overrides == null ? new XmlSerializer(type) : new XmlSerializer(type, overrides);
                using (MemoryStream stream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(xml))) 
                {
                    return serializer.Deserialize(stream);
                }
            } 
            catch (Exception exc) 
            {
                string devErrMsg = string.Format("SerializationHelper::XmlDeserialize{0}---{0}{0}---{0}Exception: {1}", Environment.NewLine, exc.ToString());

                throw new GenericUserErrorMessageException(devErrMsg);
            }
        }

        public static object XmlDeserialize(Stream stream, Type type) 
        {
            XmlSerializer serializer = new XmlSerializer(type);
            XmlTextReader xmlReader = new XmlTextReader(stream);
            // 6/23/2006 dd - The following settings are need to preserve the newline character and whitespace.
            xmlReader.WhitespaceHandling = WhitespaceHandling.All;
            xmlReader.Normalization = false;
            xmlReader.XmlResolver = null;

            return serializer.Deserialize( xmlReader);
        }



        public static LocalFilePath JsonSerializeToFile<T>(T obj) where T : class
        {
            LocalFilePath tempFile = TempFileUtils.NewTempFile();

            using (StreamWriter writer = File.CreateText(tempFile.Value))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.DefaultValueHandling = DefaultValueHandling.Ignore; // dd - Skip serialization property that is null to save space.
                serializer.Serialize(writer, obj);
            }

            return tempFile;
        }

        public static void JsonSerializeToStream<T>(TextWriter writer, T obj) where T : class
        {
            JsonSerializer serializer = new JsonSerializer();
            serializer.DefaultValueHandling = DefaultValueHandling.Ignore; // dd - Skip serialization property that is null to save space.
            serializer.Serialize(writer, obj);
            writer.Flush();
        }
        public static T JsonDeserializeFromFile<T>(LocalFilePath file) where T : class, new()
        {
            if (file == LocalFilePath.Invalid)
            {
                throw new ArgumentException("Invalid file path.");
            }

            // File.OpenText(...) is thread-safe. Multiple threads can use File.OpenText(...) to read same file.
            // In fact, it use "shared read" to read data file.
            // By the way from MSDN, any public static members of the type System.IO.File are thread safe. 
            using (StreamReader reader = File.OpenText(file.Value))
            {
                using (JsonReader jsonReader = new JsonTextReader(reader))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    return serializer.Deserialize<T>(jsonReader);
                }
            }
        }

        /// <summary>
        /// Serializes the the request data to parameters. This method is mainly used in new loan editor service.
        /// </summary>
        /// <param name="streamReader">The stream reader to read data from http request.</param>
        /// <returns>Return the dictionary for parameters.</returns>
        public static Dictionary<string, object> JsonSerializeForServiceController(StreamReader streamReader)
        {
            using (JsonReader reader = new JsonTextReader(streamReader))
            {

                JsonSerializerSettings settings = GetJsonNetSerializerSettings();
                settings.Converters = new List<JsonConverter> { new ServiceJsonConverter() };
                JsonSerializer serializer = JsonSerializer.Create(settings);
                return serializer.Deserialize<Dictionary<string, object>>(reader);
            }
        }




        public static string JsonNetSerialize<T>(T obj) where T : class, new()
        {
            if (obj == null)
            {
                return string.Empty;
            }

            return JsonConvert.SerializeObject(obj);
        }

        /// <summary>
        /// Serializes an instance into JSON, preserving type information.
        /// </summary>
        /// <param name="import">The instance to serialize.</param>
        /// <returns>The serialized string value of <paramref name="item"/>.</returns>
        public static string JsonNetSerializeWithTypeNameHandling<T>(T item)
        {
            var settings = GetJsonNetSerializerSettings();
            settings.TypeNameHandling = Newtonsoft.Json.TypeNameHandling.All;
            return Newtonsoft.Json.JsonConvert.SerializeObject(item, settings);
        }

        public static Newtonsoft.Json.JsonSerializerSettings GetJsonNetSerializerSettings()
        {
            var settings = new Newtonsoft.Json.JsonSerializerSettings();

            if (ConstStage.UseJsonNetHtmlEscaping)
            {
                settings.StringEscapeHandling = StringEscapeHandling.EscapeHtml;
            }

            return settings;
        }

        /// <summary>
        /// Deserializes an instance from JSON.
        /// </summary>
        /// <param name="value">The string to construct our object from.</param>
        /// <returns>The initialized instance.</returns>
        public static T JsonNetDeserializeWithTypeNameHandling<T>(string value)
        {
            var settings = SerializationHelper.GetJsonNetSerializerSettings();
            settings.TypeNameHandling = Newtonsoft.Json.TypeNameHandling.All;
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(value, settings);
        }

        /// <summary>
        /// Will serialize an object, masking out any properties that are defined in <see cref="propertiesToMask"/>.
        /// </summary>
        /// <typeparam name="T">The type of the object to serialize.</typeparam>
        /// <param name="obj">The object to serialize.</param>
        /// <param name="propertiesToMask">The properties to mask. See remarks for more details.</param>
        /// <param name="dictKeysToMask">The dictionary keys to mask, in case you're making JSON objects that way.</param>
        /// <returns>The JSON string with the masked properties.</returns>
        /// <remarks>
        /// propertiesToMask is keyed off of the property type (Item1) and property name(Item2). The value is the string to use as the mask. Null mask will default to string.Empty.
        /// If you pass in null for the property type, it will mask using just property name. So if you want a blanket mask of a property for all objects, do it like this.
        /// dictKeysToMask only keys off of the dictionary key. I haven't found a good way to tie it to a particular type yet. So this will target all dictionaries with strings as keys.
        /// </remarks>
        public static string JsonNetSerializeWithMask<T>(T obj, IReadOnlyDictionary<Tuple<Type, string>, string> propertiesToMask, IReadOnlyDictionary<string, string> dictKeysToMask)
        {
            var contractResolver = new MaskContractSerializer(propertiesToMask, dictKeysToMask);
            var settings = new JsonSerializerSettings() { ContractResolver = contractResolver };

            return JsonConvert.SerializeObject(obj, settings);
        }

        public static string JsonNetSerialize<T>(T obj, JsonSerializerSettings settings) where T : class, new()
        {
            if (obj == null)
            {
                return string.Empty;
            }

            if (ConstStage.UseJsonNetHtmlEscaping)
            {
                settings.StringEscapeHandling = StringEscapeHandling.EscapeHtml;
            }
            
            return JsonConvert.SerializeObject(obj, settings);
        }

        public static T JsonNetDeserialize<T>(string json) where T : class
        {
            if (string.IsNullOrEmpty(json))
            {
                return null;
            }

            return JsonConvert.DeserializeObject<T>(json);
        }

        public static T JsonNetDeserializeWithTypeHandling<T>(string json) where T : class {
            if(string.IsNullOrEmpty(json))
            {
                return null;
            }

            var settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All, MetadataPropertyHandling = MetadataPropertyHandling.ReadAhead };
            return JsonConvert.DeserializeObject<T>(json, settings);
        }

        public static string JsonNetAnonymousSerialize(object o)
        {
            if (o == null)
            {
                return string.Empty;
            }

            return JsonConvert.SerializeObject(o);
        }

        public static string JsonBeautify(string json)
        {
            object o = JsonConvert.DeserializeObject(json);
            return JsonConvert.SerializeObject(o, Newtonsoft.Json.Formatting.Indented);
        }

        public static string JsonNetSerializeBeautifully<T>(T obj, string dateFormat = null) where T : class, new()
        {
            if (obj == null)
            {
                return string.Empty;
            }

            var convertersList = new List<JsonConverter>() { new Newtonsoft.Json.Converters.StringEnumConverter() };
            if (dateFormat != null)
            {
                convertersList.Add(new FormattedDateTimeConverter(dateFormat));
            }
            return JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented, System.Linq.Enumerable.ToArray(convertersList));
        }

        public static string JsonNetSafeSerializeBeautifully<T>(T obj, string dateFormat = null) where T : class, new()
        {
            try
            {
                return SerializationHelper.JsonNetSerializeBeautifully(obj, dateFormat);
            }
            catch (JsonSerializationException)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Sanitizes the json blob directly before it gets passed on to whatever is going to use it.
        /// Will go through each token in the json blob and sanitize each string token. 
        /// </summary>
        /// <param name="json">The json to sanitize.</param>
        /// <returns>The sanitized json blob.</returns>
        public static string SanitizeJsonString(string json)
        {
            if (string.IsNullOrEmpty(json))
            {
                return json;
            }

            JToken parsedJson = JToken.Parse(json);
            JsonSerializer serializer = new JsonSerializer();

            using (var reader = new StringReader(json))
            {
                var jsonReader = new JsonTextReader(reader);
                while (jsonReader.Read())
                {
                    if (jsonReader.TokenType == JsonToken.String && jsonReader.ValueType == typeof(string))
                    {
                        string newValue = Tools.SantizeXmlString(serializer.Deserialize<string>(jsonReader));
                        var value = parsedJson.SelectToken(jsonReader.Path) as JValue;
                        value.Replace(JToken.FromObject(newValue));
                    }
                }
            }

            return parsedJson.ToString(Newtonsoft.Json.Formatting.None);
        }

        public static byte[] ProtobufSerialize<T>(T obj)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                ProtoBuf.Serializer.Serialize<T>(stream, obj);
                return stream.ToArray();
            }
        }
        public static T ProtobufDeserialize<T>(byte[] buf)
        {
            using (var stream = new MemoryStream(buf))
            {
                return ProtoBuf.Serializer.Deserialize<T>(stream); ;
            }
        }

        private class DictionaryMaskConverter : JsonConverter
        {
            private IReadOnlyDictionary<string, string> keysToMask;

            public DictionaryMaskConverter(IReadOnlyDictionary<string, string> keysToMask)
            {
                this.keysToMask = keysToMask;
            }

            public override bool CanConvert(Type objectType)
            {
                return this.keysToMask != null;
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                var dict = (IDictionary)value;
                JObject obj = new JObject();
                foreach (string key in dict.Keys)
                {
                    if (this.keysToMask.ContainsKey(key))
                    {
                        obj.Add(key, this.keysToMask[key]);
                    }
                    else
                    {
                        obj.Add(key, dict[key] == null ? JValue.CreateNull() : JToken.FromObject(dict[key]));
                    }
                }

                obj.WriteTo(writer);
            }

            public override bool CanRead
            {
                get
                {
                    return false;
                }
            }

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                throw new NotImplementedException();
            }
        }

        private class MaskContractSerializer : DefaultContractResolver
        {
            private IReadOnlyDictionary<Tuple<Type, string>, string> propertiesToMask = null;
            private IReadOnlyDictionary<string, string> keysToMask = null;

            public MaskContractSerializer(IReadOnlyDictionary<Tuple<Type, string>, string> propertiesToMask, IReadOnlyDictionary<string, string> keysToMask)
            {
                this.propertiesToMask = propertiesToMask;
                this.keysToMask = keysToMask;
            }

            protected override JsonDictionaryContract CreateDictionaryContract(Type objectType)
            {
                var contract = base.CreateDictionaryContract(objectType);

                if (this.keysToMask != null && contract.DictionaryKeyType == typeof(string))
                {
                    Type valueType = contract.DictionaryValueType;
                    contract.Converter = new DictionaryMaskConverter(this.keysToMask);
                }

                return contract;
            }

            protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
            {
                JsonProperty property = base.CreateProperty(member, memberSerialization);

                if (this.propertiesToMask != null)
                {
                    string mask = null;
                    var typedTuple = new Tuple<Type, string>(property.DeclaringType, property.PropertyName);
                    var nullTuple = new Tuple<Type, string>(null, property.PropertyName);
                    if (this.propertiesToMask.TryGetValue(typedTuple, out mask) || this.propertiesToMask.TryGetValue(nullTuple, out mask))
                    {
                        property.ValueProvider = new StringValueProvider(mask ?? string.Empty);
                    }
                }

                return property;
            }
        }

        private class StringValueProvider : IValueProvider
        {
            private readonly string _value;

            public StringValueProvider(string value)
            {
                _value = value;
            }

            public void SetValue(object target, object value)
            {
                throw new NotSupportedException();
            }

            public object GetValue(object target)
            {
                return _value;
            }
        }
    }
}
