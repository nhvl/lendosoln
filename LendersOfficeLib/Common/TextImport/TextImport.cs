﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using CommonProjectLib.Common.Lib;
using System.Text;
using System.Linq;
using System.Data.OleDb;
using System.Text.RegularExpressions;
using System.ComponentModel;
using DataAccess;

namespace LendersOffice.Common.TextImport
{
    public static class DataParsing
    {
        /// <summary>
        /// Checks if the string value can be represented with ASCII encoding.
        /// </summary>
        /// <param name="value">The string to check.</param>
        /// <returns>True if valid, false otherwise.</returns>
        public static bool IsASCII(string value)
        {
            // ASCII encoding replaces non-ascii with question marks, so we use UTF8 to see if multi-byte sequences are there
            return Encoding.UTF8.GetByteCount(value) == value.Length;
        }

        /// <summary>
        /// Turns a file it understands into a CSV string. Currently understands .[cpt]sv and .xls(x)
        /// </summary>
        /// <param name="FilePath">The path to the file</param>
        /// <param name="SheetName">If the file turns out to be an Excel spreadsheet, we need a sheet name. </param>
        /// <param name="KeepFirstRow">Should the first row be kept? (useful for getting rid of a header)</param>
        /// <returns></returns>
        public static string FileToCSV(string FilePath, bool KeepFirstRow, string SheetName)
        {
            string ext = Path.GetExtension(FilePath);
            char delim;
            switch (ext)
            {
                //"Pipe separated values"
                case(".psv"):
                    delim = '|';
                    break;
                //"Tab separated values"
                case(".tsv"):
                    delim = '\t';
                    break;
                //"Comma separated values"
                case (".csv"):
                    delim = ',';
                    break;
                case (".xls"):
                case (".xlsx"):
                    return ParseExcel(FilePath, SheetName).ToCSV(KeepFirstRow);
                default:
                    throw new ArgumentException("Could not understand file extension " + ext, "FilePath");
            }

            string ret;
            using (StreamReader sr = new StreamReader(FilePath))
            {
                if (!KeepFirstRow)
                {
                    sr.ReadLine();
                }
                ret = DelimitedListParser.OtherDelimToCSV(sr.ReadToEnd(), delim);
            }

            return ret;
        }


        /// <summary>
        /// Turns a file it understands into a DataTable. Currently understands .[cpt]sv and .xls(x)
        /// </summary>
        /// <param name="FilePath">The path to the file</param>
        /// <param name="SheetName">If the file turns out to be an Excel spreadsheet, we need a sheet name. If null, the first sheet name alphabetically will be used</param>
        /// <param name="HasHeaderRow">Is the file expected to have a header row? If it exists, it will be turned into the DataTable's columns.</param>
        /// <param name="parseErrors">A list of parse errors that occurred while parsing this file, if it is some sort of delimited text file</param>
        /// <returns></returns>
        public static DataTable FileToDataTable(string FilePath, bool HasHeaderRow, string SheetName, out ILookup<int, string> parseErrors)
        {
            string ext = Path.GetExtension(FilePath);
            char delim;
            switch (ext.ToLowerInvariant())
            {
                //"Pipe separated values"
                case (".psv"):
                    delim = '|';
                    break;
                //"Tab separated values"
                case (".tsv"):
                    delim = '\t';
                    break;
                //"Comma separated values"
                case (".config"):
                    // note to allow reading files that we've hidden by adding .config extension.  see opm 468303
                case (".csv"):
                    delim = ',';
                    break;
                case (".xls"):
                case (".xlsx"):
                    //I have to go through all this just to make an empty Lookup :(
                    var empty = new List<Tuple<int, string>>();
                    parseErrors = empty.ToLookup(p => p.Item1, p => p.Item2);
                    return ParseExcel(FilePath, SheetName, HasHeaderRow);
                default:
                    throw new ArgumentException("Could not understand file extension " + ext, "FilePath");
            }

            DataTable ret;
            using (StreamReader sr = new StreamReader(FilePath))
            {
                string file = sr.ReadToEnd();
                ret = DelimitedListParser.DsvToDataTable(file, delim, HasHeaderRow, out parseErrors);
            }

            return ret;
        }

        /// <summary>
        /// Gets something from a list by Id or by name. If any elements are found by Id, only elements by id are returned. Otherwise, elements are searched for by name.
        /// </summary>
        /// <typeparam name="T">The element type you're looking for</typeparam>
        /// <param name="LookIn">The thing to look in</param>
        /// <param name="LookFor">The thing to look for, may be an ID or a name</param>
        /// <param name="GetName">Gets the name of a T</param>
        /// <param name="GetId">Gets the ID of a T</param>
        /// <returns>A list of matching IDs if any, or a list of matching names if any.</returns>
        public static IEnumerable<T> GetByIdOrName<T>(IEnumerable<T> LookIn, string LookFor, Func<T, string> GetName, Func<T, int> GetId)
        {
            int id;

            if (int.TryParse(LookFor, out id))
            {
                return from a in LookIn where id == GetId(a) select a;
            }
            else
            {
                return from a in LookIn where LookFor.Equals(GetName(a), StringComparison.InvariantCultureIgnoreCase) select a;
            }
        }


        
        //This regex checks if a string looks like a guid, so we can avoid throwing an exception
        //if it's not.
        //It's not significantly faster than guid.parse, but I tend to debug with
        //break on all exceptions and that gets annoying
        private static Regex GuidMatcher = new Regex(@"^[A-Fa-f0-9]{32}$|^({|\()?[A-Fa-f0-9]{8}-([A-Fa-f0-9]{4}-){3}[A-Fa-f0-9]{12}(}|\))?$", RegexOptions.Compiled);

        public static bool TryParseGuid(string input, out Guid id)
        {
            id = Guid.Empty;
            if (string.IsNullOrEmpty(input)) return false;

            if (GuidMatcher.IsMatch(input))
            {
                //the regex has false positives, but shouldn't have false negatives.
                try
                {
                    id = new Guid(input);
                    return true;
                }
                catch (FormatException)
                {

                }
            }
            return false;
        }

        /// <summary>
        /// Gets something from a list by Id or by name. If any elements are found by Id, only elements by id are returned. Otherwise, elements are searched for by name.
        /// </summary>
        /// <typeparam name="T">The element type you're looking for</typeparam>
        /// <param name="LookIn">The thing to look in</param>
        /// <param name="LookFor">The thing to look for, may be an ID or a name</param>
        /// <param name="GetName">Gets the name of a T</param>
        /// <param name="GetId">Gets the ID of a T</param>
        /// <returns>A list of matching IDs if any, or a list of matching names if any.</returns>
        public static IEnumerable<T> GetByIdOrName<T>(IEnumerable<T> LookIn, string LookFor, Func<T, string> GetName, Func<T, Guid> GetId)
        {
            Guid id;
            if (TryParseGuid(LookFor, out id))
            {
                return from a in LookIn where id == GetId(a) select a;
            }
            else
            {
                return from a in LookIn where LookFor.Equals(GetName(a), StringComparison.InvariantCultureIgnoreCase) select a;
            }
        }


        /// <summary>
        /// Gets something from a list by Id or by name. If any elements are found by Id, only elements by id are returned. Otherwise, elements are searched for by name.
        /// </summary>
        /// <typeparam name="T">The element type you're looking for</typeparam>
        /// <param name="LookIn">The thing to look in</param>
        /// <param name="LookFor">The thing to look for, may be an ID or a name</param>
        /// <param name="GetName">Gets the name of a T</param>
        /// <param name="GetId">Gets the ID of a T</param>
        /// <returns>A list of matching IDs if any, or a list of matching names if any.</returns>
        public static IEnumerable<T> GetByIdOrName<T>(IEnumerable<T> LookIn, string LookFor, Func<T, string> GetName, Func<T, string> GetId)
        {            
            var byId = from a in LookIn where LookFor.Equals(GetId(a), StringComparison.InvariantCultureIgnoreCase) select a;
            if (byId.Any())
            {
                return byId;
            }
            else
            {
                return from a in LookIn where LookFor.Equals(GetName(a), StringComparison.InvariantCultureIgnoreCase) select a;
            }
        }

        /// <summary>
        /// Validates the columns of a data table, to ensure that the right columns exist and that the required columns are not empty.
        /// </summary>
        /// <param name="toValidate">The DataTable to be validated</param>
        /// <param name="allColumnNames">The names of all columns that should be present</param>
        /// <param name="requiredColumnNames">The names of all columns that are required to be non-empty</param>
        /// <param name="offset">The offset to the line number of the header row, for error reporting</param>
        /// <returns>A list of errors based on line number</returns>
        public static ILookup<int, string> ValidateColumns(DataTable toValidate, IEnumerable<string> allColumnNames, IEnumerable<string> requiredColumnNames, int offset)
        {
            LinkedList<Tuple<int, string>> tempErrors = new LinkedList<Tuple<int, string>>();

            foreach (string name in allColumnNames)
            {
                if (!toValidate.Columns.Contains(name))
                {
                    tempErrors.AddLast(new Tuple<int,string>(offset, "Expected column " + name + " was not found"));
                }
            }

            //offset = line # of the header row, so after we've done the header row we increment it.
            offset++;

            for(int i = 0; i < toValidate.Rows.Count; i++)
            {
                foreach(string reqColumn in requiredColumnNames)
                {
                    if (toValidate.Columns.Contains(reqColumn) && string.IsNullOrEmpty(toValidate.Rows[i][reqColumn].ToString()))
                    {
                        tempErrors.AddLast(new Tuple<int,string>(offset + i, "Required column " + reqColumn + " was empty"));
                    }
                }
            }

            return tempErrors.ToLookup((a) => a.Item1, (a) => a.Item2);

        }
        
        /// <summary>
        /// Takes an Excel file and turns it into a DataTable. Assumes that the Excel file has a header row.
        /// </summary>
        /// <param name="filename">The name of the file to parse</param>
        /// <param name="sheetname">The name of the sheet that should be parsed. If this is null, the first sheet alphabetically will be used.</param>
        /// <returns>A DataTable representation of the file</returns>
        public static DataTable ParseExcel(string filename, string sheetname)
        {
            return ParseExcel(filename, sheetname, true);
        }

        /// <summary>
        /// Takes an Excel file and turns it into a DataTable.
        /// </summary>
        /// <param name="filename">The name of the file to parse</param>
        /// <param name="sheetname">The name of the sheet that should be parsed. If this is null, the first sheet alphabetically will be used.</param>
        /// <param name="hasHeader">True if the Excel file is expected to have a header. This will end up as the DataRow's columns.</param>
        /// <returns>A DataTable representation of the file</returns>
        public static DataTable ParseExcel(string filename, string sheetname, bool hasHeader)
        {
            string connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filename + 
                                      @";Extended Properties='Excel 12.0;HDR=" + (hasHeader?"YES":"NO") + "'";
            DataTable toImport = new DataTable();

            using (OleDbConnection con = new OleDbConnection(connectionString))
            {
                con.Open();
                if (string.IsNullOrEmpty(sheetname))
                {
                    DataTable InfoSchema = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    // This only gives us the first sheet name alphabetically. 
                    // There seems to be no way to get the sheet Excel displays first :(                    
                    sheetname = InfoSchema.Rows[0]["TABLE_NAME"].ToString();

                    //This comes out with a $ at the end, we don't want that.
                    //(We can't just use TrimEnd('$') because you just know someone is going to 
                    //name their sheet with dollars at the end)
                    sheetname = sheetname.Substring(0, sheetname.Length - 1);
                }


                string query = "select * from [" + sheetname + "$]";
                using (OleDbDataAdapter da = new OleDbDataAdapter(query, con))
                {
                    da.Fill(toImport);
                }
                con.Close();
            }

            //Excel will randomly decide that it needs to have empty rows at the end of a sheet,
            //so we'll remove them here; we'll leave internal empty rows alone.
            for (int i = (toImport.Rows.Count - 1); i >= 0; i--)
            {
                if (toImport.Rows[i].ItemArray.All((x) => string.IsNullOrEmpty(x.ToString().TrimWhitespaceAndBOM())))
                {
                    toImport.Rows[i].Delete();
                }
                else
                {
                    break;
                }
            }
            toImport.AcceptChanges();

            //Make sure all column names are well-trimmed, because people will accidentally put 
            //spaces in sometimes.
            for (int i = 0; i < toImport.Columns.Count; i++)
            {
                toImport.Columns[i].ColumnName = toImport.Columns[i].ColumnName.TrimWhitespaceAndBOM();
            }

            //And then make sure all columns are strings, because Jet will be "smart" 
            //and try to type things based on the first 8 columns.
            bool tableContainsNonStringColumns = false;
            foreach (DataColumn dc in toImport.Columns)
            {
                if (dc.DataType != typeof(string))
                {
                    tableContainsNonStringColumns = true;
                    break;
                }
            }

            //This is a little expensive, so we don't want to do it every time.
            if (tableContainsNonStringColumns)
            {
                //We can't change the data type if there's already data in the column, 
                //so we have to make a clean data table

                //Get a copy of the schema
                DataTable newToImport = toImport.Clone();

                //And make sure every column's type is string
                foreach (DataColumn c in newToImport.Columns)
                {
                    c.DataType = typeof(string);
                }

                //And then import all the rows
                foreach (DataRow dr in toImport.Rows)
                {
                    newToImport.ImportRow(dr);
                }

                toImport = newToImport;
            }

            //And now replace all DBNulls with empty strings, because we're bad bad people 
            //who don't care about data integrity
            foreach (DataRow r in toImport.Rows)
            {
                foreach (DataColumn c in toImport.Columns)
                {
                    if (r[c] == System.DBNull.Value)
                    {
                        r[c] = "";
                    }
                }
            }

            return toImport;
        }

        /// <summary>
        /// Sanitizes an input string so it will play nicely as an entry in a CSV file. Removes enclosing quotes, doubles inner quotes, and wraps with quotes if it contains the delimiter or internal quotes.
        /// </summary>
        /// <param name="input">String to sanitize</param>
        /// <returns>Sanitized string</returns>
        public static string sanitizeForCSV(string input)
        {
            //Get rid of any extra whitespace
            string ret = input.TrimWhitespaceAndBOM();

            //If it's already got quotes at the beginning or end, strip them out    
            ret = ret.Trim('"');

            //Double up the double quotes if there are any internally, now that we removed the outside ones
            if (ret.Contains('"'))
            {
                ret = ret.Replace("\"", "\"\"");
            }

            //Wrap quotes around it if it contains a comma or double quote
            if (ret.Contains(',') || ret.Contains('"') || ret.Contains(Environment.NewLine))
            {
                ret = "\"" + ret + "\"";
            }
            return ret;
        }


        /// <summary>
        /// Converts a list to a data table, containing the requested columns.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="ReqColumns">The columns you want in the DataTable. Must come from typeof(T), otherwise an ArgumentException will be thrown.</param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(this IList<T> data, IList<PropertyDescriptor> ReqColumns)
        {
            DataTable table = new DataTable();
            foreach (var col in ReqColumns)
            {
                if (col.ComponentType != typeof(T))
                {
                    throw new ArgumentException("Column " + col.Name + " is not from " + typeof(T).Name);
                }
                table.Columns.Add(col.Name, col.PropertyType);
            }

            object[] values = new object[ReqColumns.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = ReqColumns[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        /// <summary>
        /// Converts a list to a data table, containing properties named in ReqColumnNames as the columns.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="ReqColumnNames">Required columns.</param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(this IList<T> data, IList<string> ReqColumnNames)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            List<PropertyDescriptor> ReqColumns = new List<PropertyDescriptor>();

            foreach (string Name in ReqColumnNames)
            {
                foreach (PropertyDescriptor p in props)
                {
                    if (p.Name.Equals(Name))
                    {
                        ReqColumns.Add(p);
                    }
                }
            }

            return data.ToDataTable(ReqColumns);
        }

        /// <summary>
        /// Converts a list to a data table, containing all properties as columns
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            List<PropertyDescriptor> ReqColumns = new List<PropertyDescriptor>();
            foreach (PropertyDescriptor p in props)
            {
                ReqColumns.Add(p);
            }

            return data.ToDataTable(ReqColumns);
        }

        /// <summary>
        /// Maps a data table from one representation to another
        /// </summary>
        /// <param name="data"></param>
        /// <param name="Map">A dictionary of ColName=>Map(data.Rows[i][ColName]). The mapping functions will not be called for null values.</param>
        /// <returns>A new DataTable, with columns mapped according to the Map parameter</returns>
        public static DataTable MapColumns(this DataTable data,  Dictionary<string, Func<object, object>> Map)
        {
            DataTable ret = data.Clone();

            foreach (var mapKey in Map)
            {
                //We can't know, a-priori, what this thing's "real" type will be
                //So we make an example, and ask.
                
                //Of course, we have to make sure we can get something that isn't null first
                object source = null;
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    if (data.Rows[i][mapKey.Key] != System.DBNull.Value)
                    {
                        source = data.Rows[i][mapKey.Key];
                    }
                }

                //If we found something that wasn't null, set the type
                //If we didn't find something that's not null, well, we don't really care about
                //this column, now do we?
                if (source != null)
                {
                    object example = mapKey.Value(source);
                    ret.Columns[mapKey.Key].DataType = example.GetType();
                }
            }

            foreach (DataRow dr in data.Rows)
            {
                DataRow translated = ret.NewRow();
                foreach (DataColumn dc in data.Columns)
                {
                    //It's easier to just refer to everything by name
                    string name = dc.ColumnName;
                    if (Map.ContainsKey(name))
                    {
                        //We only care about doing the mapping if the value is not null
                        if (dr[name] != System.DBNull.Value)
                        {
                            translated[name] = Map[name](dr[name]);
                        }
                    }
                    else
                    {
                        translated[name] = dr[name];
                    }
                }
                ret.Rows.Add(translated);
            }

            data = ret;
            return ret;
        }

        /// <summary>
        /// Renames the columns in a data table
        /// </summary>
        /// <param name="data"></param>
        /// <param name="Rename">A dictionary of Name=>Rename pairs</param>
        /// <returns></returns>
        public static DataTable RenameColumns(this DataTable data, Dictionary<string, string> Rename)
        {
            foreach (var pair in Rename)
            {
                data.Columns[pair.Key].ColumnName = pair.Value;
            }
            return data;
        }
        /// <summary>
        /// Takes a data table and makes it into a CSV string with a header row
        /// </summary>
        /// <param name="toCSV">The data table to convert</param>
        /// <returns>A csv string of values</returns>
        public static string ToCSVWithHeader(this DataTable toCSV)
        {
            return toCSV.ToCSV(true);
        }

        /// <summary>
        /// Takes a data table and makes it into a CSV string with no header row
        /// </summary>
        /// <param name="toCSV">The data table to convert</param>
        /// <returns>A csv string of values</returns>
        public static string ToCSV(this DataTable toCSV)
        {
            return toCSV.ToCSV(false);
        }

        /// <summary>
        /// Writes the given datatable into a CSV format and returns the file use. Since some of the CSV files can be gigantic 
        /// it will save us memory to write it to a file. 
        /// </summary>
        /// <param name="datatable"></param>
        /// <param name="includeHeader"></param>
        /// <param name="encoding"></param>
        /// <returns>The path to the csv file.</returns>
        public static string ToCSVFile(this DataTable datatable, bool includeHeader, Encoding encoding)
        {
            string tempFile = TempFileUtils.NewTempFilePath();

            using (StreamWriter sw = new StreamWriter(tempFile, false, encoding))
            {
                if (includeHeader)
                {
                    for (int i = 0; i < datatable.Columns.Count; i++)
                    {
                        DataColumn column = datatable.Columns[i];
                        sw.Write(sanitizeForCSV(column.ColumnName));

                        if (i + 1  < datatable.Columns.Count )
                        {
                            sw.Write(",");
                        }
                    }
                    sw.WriteLine();
                }

                for (int i = 0; i < datatable.Rows.Count; i++ )
                {
                    DataRow row = datatable.Rows[i];
                    for (int y = 0; y < row.ItemArray.Length; y++)
                    {
                        object item = row.ItemArray[y];
                        string text = string.Empty;
                        if (item != null)
                        {
                            text = item.ToString();
                        }

                        sw.Write(sanitizeForCSV(text));

                        if (y + 1 < row.ItemArray.Length)
                        {
                            sw.Write(",");
                        }
                    }
                    sw.WriteLine();
                }
            }

            return tempFile;
        }

        /// <summary>
        /// Takes a data table and makes it a CSV string. DO NOT USE THIS. Switch over to ToCSVFile instead. 
        /// </summary>
        /// <param name="toCSV">The data table to turn into a CSV string</param>
        /// <param name="GenerateHeader">True if the CSV string should be generated with headers based on the data column names</param>
        /// <returns>A CSV string of values, with columns separated by commas and rows by newlines</returns>
        public static string ToCSV(this DataTable toCSV, bool GenerateHeader)
        {
            StringBuilder result = new StringBuilder();
            if (GenerateHeader)
            {
                StringBuilder colBuilder = new StringBuilder();
                foreach (DataColumn col in toCSV.Columns)
                {
                    //Note that this will add a trailing comma to the last element, which we don't want
                    colBuilder.Append(sanitizeForCSV(col.ColumnName) + ",");
                }

                string currRow = colBuilder.ToString();
                //And here's where we remove that offensive trailing commma
                currRow = currRow.Remove(currRow.Length - 1);
                result.AppendLine(currRow);
            }
            foreach (DataRow row in toCSV.Rows)
            {
                //Same as before, but with the row values
                StringBuilder rowBuilder = new StringBuilder();
                foreach (string col in from a in row.ItemArray select a.ToString())
                {
                    rowBuilder.Append(sanitizeForCSV(col) + ",");
                }

                string currRow = rowBuilder.ToString();
                currRow = currRow.Remove(currRow.Length - 1);
                result.AppendLine(currRow);
            }

            return result.ToString();
        }
    }

    //I know, you're thinking "I'll just use string.split(',')", but you're wrong!
    //What happens when there's a comma inside the field you're splitting? (like a full address: "123 address way, city, state, zip")
    //Oh, you'll use a regex? Well, good luck with that - it's theoretically possible, but I've never seen a fully working regex that
    //actually parses real-world CSVs.
    public class DelimitedListParser : IEnumerable
    {
        /// <summary>
        /// Turns a CSV string into a data table, with some requirements.
        /// </summary>
        /// <param name="csv">The CSV string to be parsed. Note that this is not a filename, but an actual CSV string.</param>
        /// <param name="hasHeader">Does the CSV string have a header?</param>
        /// <param name="reqColumns">The number of required columns. Required columns come first, going left to right.</param>
        /// <param name="numColumns">The total number of columns</param>
        /// <param name="errors">A lookup table of line numbers and errors</param>
        /// <returns>The data table that was parsed</returns>
        public static DataTable CsvToDataTable(string csv, bool hasHeader, int reqColumns, int numColumns, out ILookup<int, string> errors)
        {
            ILookup<int, string> parseErrors;
            ILookup<int, string> validateErrors;
            DataTable result = DsvToDataTable(csv, ',', hasHeader, out parseErrors);

            List<string> reqColumnNames = new List<string>();
            List<string> allColumnNames = new List<string>();

            for (int i = 0; i < numColumns; i++)
            {
                //The first reqColumns are required to be non-empty
                if (i < reqColumns)
                {
                    reqColumnNames.Add(result.Columns[i].ColumnName);
                }
                allColumnNames.Add(result.Columns[i].ColumnName);
            }

            validateErrors = DataParsing.ValidateColumns(result, allColumnNames, reqColumnNames, 1);

            //Merging ILookups is a giant pain :(
            errors = (from a in parseErrors.Union(validateErrors)
                      select (from b in a select new Tuple<int, string>(a.Key, b))) //This gives me a list of tuples orderd by key, with each entry in the list being a list of errors
                      .Aggregate(new List<Tuple<int, string>>(), //This flattens that list of lists into a single list of tuples
                      (accumulator, current) =>
                      {
                          accumulator.AddRange(current);
                          return accumulator;
                      })
                      .ToLookup((a) => a.Item1, (a) => a.Item2); //And then finally make it a lookup again.
                       


            return result;
        }

        /// <summary>
        /// Converts a delimiter separated values string to a DataTable. If the DSV string has a header row, the DataTable's column names will reflect the header row.
        /// </summary>
        /// <param name="csv">The string to convert</param>
        /// <param name="hasHeader">Does the csv have a header row?</param>
        /// <param name="errors">A list of errors that occurred while converting</param>
        /// <returns>A data table that represents the csv string that was passed in</returns>
        public static DataTable DsvToDataTable(string csv, char delimiter, bool hasHeader, out ILookup<int, string> errors)
        {
            DataTable dt = new DataTable();
            //Hold the errors here temporarily so we can turn it into a Lookup later.
            List<Tuple<int, string>> tempErrors = new List<Tuple<int, string>>();
            DelimitedListParser csvParser = new DelimitedListParser(delimiter);
            int lineNum = 0;
            //Note that that's *String*Reader, not *Stream*Reader.
            using (StringReader sr = new StringReader(csv))
            {
                if (hasHeader)
                {
                    csvParser.ParseMultilineDSV(sr);
                    lineNum++;

                    try
                    {
                        for (int i = 0; i < csvParser.Length; i++)
                        {
                            string columnName = csvParser[i].TrimWhitespaceAndBOM();
                            if (!string.IsNullOrEmpty(columnName))
                            {
                                dt.Columns.Add(columnName);
                            }
                            else
                            {
                                tempErrors.Add(new Tuple<int, string>(lineNum, "Column " + (i + 1) + " has an empty name"));
                            }
                        }
                    }
                    catch (DuplicateNameException e)
                    {
                        tempErrors.Add(new Tuple<int, string>(lineNum, "Could not parse header; there may be duplicate column names. Error text is: " + e.Message));
                    }
                }

                while (sr.Peek() != -1)
                {
                    csvParser.ParseMultilineDSV(sr);
                    lineNum++;

                    IEnumerable<string> nonAsciiStrings = csvParser.m_Set.Where((value) => !DataParsing.IsASCII(value));
                    if (nonAsciiStrings.Any())
                    {
                        tempErrors.Add(new Tuple<int, string>(lineNum, $"This line contains values with non-ASCII characters. Invalid strings: {string.Join(delimiter.ToString(), nonAsciiStrings)}"));
                        dt.Rows.Add(dt.NewRow());
                    }

                    try
                    {
                        //This is guaranteed to be in the correct order because 
                        //the backing store to csvParser's m_Set is a List
                        dt.Rows.Add((from s in csvParser.m_Set select s.TrimWhitespaceAndBOM()).ToArray<string>());
                    }
                    catch (ArgumentException)
                    {
                        tempErrors.Add(new Tuple<int,string>(lineNum, "This line contains the wrong number of columns! Expecting " + dt.Columns.Count + ", found " + csvParser.m_Set.Count));
                        dt.Rows.Add(dt.NewRow());
                    }
                    catch (ConstraintException e)
                    {
                        tempErrors.Add(new Tuple<int, string>(lineNum, "Somehow you managed to generate a constraint exception; the number of columns in this row may be wrong. Error text is: " + e.Message));
                        dt.Rows.Add(dt.NewRow());
                        //This error may not be recoverable, so rethrow it.
                        throw;
                    }
                    
                }
            }
            errors = tempErrors.ToLookup<Tuple<int, string>, int, string>((a) => a.Item1, (b) => (b).Item2);
            return dt;
        }

        public static string OtherDelimToCSV(string input, char delim)
        {
            DelimitedListParser otherParser = new DelimitedListParser(delim);
            StringBuilder result = new StringBuilder();
            using (StringReader sr = new StringReader(input))
            {
                while (sr.Peek() != -1)
                {
                    otherParser.ParseMultilineDSV(sr);
                    StringBuilder row = new StringBuilder();
                    foreach (string col in otherParser)
                    {
                        //Can't assign to foreach variables :(
                        string temp = col;

                        if (temp.Contains(","))
                        {
                            temp = "\"" + temp + "\"";
                        }

                        row.Append(temp + ", ");
                    }

                    //Strip out the last comma and space
                    row.Length -= 2;
                    string currRow = row.ToString();
                    result.AppendLine(currRow);
                }
            }

            return result.ToString();
        }

        private char delimiter;

        /// <summary>
        /// Create a delimited list parser using commas as the delimiter.
        /// </summary>
        public DelimitedListParser() : this(',') { }

        /// <summary>
        /// Create a delimited list parser with the specified delimiter.
        /// </summary>
        /// <param name="delimiter">The delimiter to use</param>
        public DelimitedListParser(char delimiter)
        {
            this.delimiter = delimiter;
        }

        private List<string> m_Set = new List<string>();

        public String this[int iItem]
        {
            // Access member.

            get
            {
                if (iItem >= 0 && iItem < m_Set.Count)
                {
                    return m_Set[iItem];
                }

                return "";
            }
        }

        public int Length
        {
            get { return m_Set.Count; }
        }

        public void ParseMultilineDSV(StringReader sr)
        {
            m_Set.Clear();
            StringBuilder intermediate = new StringBuilder();
            bool wasInQuotes = parseInternal(sr.ReadLine(), false, ref intermediate);
            while (wasInQuotes)
            {
                wasInQuotes = parseInternal(sr.ReadLine(), wasInQuotes, ref intermediate);
            }
        }

        private bool parseInternal(string sLine, bool startInQuotes, ref StringBuilder partialStatus)
        {
            StringBuilder sP = partialStatus;
            bool inQuotes = startInQuotes;
            for (int i = 0; i < sLine.Length; ++i)
            {
                if (sLine[i] == '\"' && !inQuotes)
                {
                    inQuotes = true;
                    ++i;
                }

                if(inQuotes)
                {
                    while (i < sLine.Length)
                    {
                        if (sLine[i] == '\"')
                        {
                            //Double quotes ("") escape quotes within quotes.
                            //e.g, "He said ""Hello, how are you?"" to me"
                            if (i + 1 < sLine.Length && sLine[i + 1] == '\"')
                            {
                                //We don't actually want to emit two double quotes, we just want a single double quote
                                //sP.Append(sLine[i]);

                                ++i;
                                //The next quote in the double quotes is added down there, which is what we want.
                            }
                            else
                            {
                                inQuotes = false;
                                break;
                            }
                        }
                        sP.Append(sLine[i]);
                        ++i;
                    }

                    continue;
                }

                if (sLine[i] == delimiter)
                {
                    m_Set.Add(sP.ToString());

                    sP = new StringBuilder();

                    continue;
                }
                sP.Append(sLine[i]);
            }
            partialStatus = sP;
            if (!inQuotes)
            {
                m_Set.Add(sP.ToString());
            }
            return inQuotes;
        }

        /// <summary>
        /// Process the line and separate it into the comma-separated
        /// entries.  We handle quoted strings as opaque.
        /// </summary>
        public DelimitedListParser Eat(String sLine)
        {
            // Check the string.  If valid, we commence with eating.
            if (sLine == null)
            {
                throw new ArgumentException("Invalid line to eat.");
            }

            m_Set.Clear();

            // 7/7/2005 kb - We now do a smart split right
            // here.  If the commas are within a double quote
            // string, then we ignore them and don't split.
            var start = new StringBuilder();
            parseInternal(sLine, false, ref start);

            return this;
        }

        public IEnumerator GetEnumerator()
        {
            // Access looping interface for our list.

            return m_Set.GetEnumerator();
        }

    }
}
