﻿namespace LendersOffice.Common
{
    using System;
    using Newtonsoft.Json;

    /// <summary>
    /// Writes formatted DateTime values to JSON.
    /// </summary>
    public class FormattedDateTimeConverter : JsonConverter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FormattedDateTimeConverter"/> class.
        /// </summary>
        /// <param name="formatString">The format string to use for conversion.</param>
        public FormattedDateTimeConverter(string formatString)
        {
            this.FormatString = formatString;
        }

        /// <summary>
        /// Gets or sets the DateTime format string to pass.
        /// </summary>
        public string FormatString { get; set; }

        /// <summary>
        /// Gets a value indicating whether the converter can read JSON.
        /// </summary>
        public override bool CanRead => true;

        /// <summary>
        /// Gets a value indicating whether the converter can write JSON.
        /// </summary>
        public override bool CanWrite => true;

        /// <summary>
        /// Determines whether the converter can convert the given type.
        /// </summary>
        /// <param name="objectType">The type to determine convertibility for.</param>
        /// <returns>Whether the type can be converted.</returns>
        public override bool CanConvert(Type objectType)
        {
            return objectType.IsAssignableFrom(typeof(DateTime));
        }

        /// <summary>
        /// Reads a JSON value to a DateTime.
        /// </summary>
        /// <param name="reader">JSON reader.</param>
        /// <param name="objectType">The object type.</param>
        /// <param name="existingValue">The existing value.</param>
        /// <param name="serializer">The JSON serializer making the call.</param>
        /// <returns>A new DateTime from parsing the reader.</returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var stringValue = (string)reader.Value;

            return DateTime.Parse(stringValue);
        }

        /// <summary>
        /// Writes the given value to a JSON string representation.
        /// </summary>
        /// <param name="writer">JSON writer.</param>
        /// <param name="value">The value to write.</param>
        /// <param name="serializer">The JSON serializer making the call.</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var date = value as DateTime?;
            if (date == null)
            {
                return;
            }

            writer.WriteValue(date.Value.ToString(this.FormatString));
        }
    }
}
