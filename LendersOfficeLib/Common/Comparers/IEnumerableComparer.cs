﻿// <copyright file="IEnumerableComparer.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   1/14/2015
// </summary>

namespace LendersOffice.Common.Comparers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Provides a way of comparing enumerations for equality.  
    /// </summary>
    /// <typeparam name="T">The type of item that the enumerable enumerates.</typeparam>
    public class IEnumerableComparer<T> : IEqualityComparer<IEnumerable<T>>
    {
        // Adapted from http://stackoverflow.com/a/14675741/420667.

        /// <summary>
        /// Returns true iff their sequences are equal.
        /// </summary>
        /// <param name="x">The first enumerable.</param>
        /// <param name="y">The second enumerable.</param>
        /// <returns>True iff their sequences equal.</returns>
        public bool Equals(IEnumerable<T> x, IEnumerable<T> y)
        {
            return object.ReferenceEquals(x, y) || (x != null && y != null && x.SequenceEqual(y));
        }

        /// <summary>
        /// Returns a hash code for the enumeration.
        /// </summary>
        /// <param name="enumerable">The enumeration.</param>
        /// <returns>An integer hash for the enumeration.</returns>
        public int GetHashCode(IEnumerable<T> enumerable)
        {
            if (enumerable == null)
            {
                return 0;
            }

            return unchecked(enumerable.Select(e => e.GetHashCode()).Aggregate(17, (a, b) => (23 * a) + b));
        }
    }
}
