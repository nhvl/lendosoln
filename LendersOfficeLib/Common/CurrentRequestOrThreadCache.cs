﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;

namespace LendersOffice.Common
{
    /*
     * Some things to (possibly) add:
     *     - Keys should be obtained through a class (GetLoanKey(loanid), GetBrokerKey(brokerId), GetEmployeeKey(), etc.)
     *      and that class should internally use a Keys enum (Keys.Loan, Keys.Broker, Keys.Employee)
     *     - A Refresher() (if it expires, call the Refresher() and give it more time to expire.)
     *     - A collision checker.  (Don't want to clobber old data... so should throw an error, but also let us force a no-throw)
     * 
     */

    public class GeneratedKeyForRequestOrThreadCache
    {
        public string Key { get; protected set; }
        protected GeneratedKeyForRequestOrThreadCache(string key)
        {
            Key = key;
        }
        struct KeyPrefixes
        {
            public static string ListLockListLockDeskClosureByPolicyId { get { return "ListLockDeskClosureByPolicyId_"; } }
            public static string RetrievePricingGroupById { get { return "RetrievePricingGroupById_"; } }
            public static string LockPolicy_Retrieve { get { return "LockPolicy_Retrieve_";}}
            public static string CEDocTypeId { get { return "CEDocTypeIdByBroker"; } }
            public static string IsForQP2LoanCreationProcess { get { return "IsForQP2LoanCreationProcess"; } }
        }       
        public static GeneratedKeyForRequestOrThreadCache LockPolicyClosureListKey(Guid brokerId)
        {
            return new GeneratedKeyForRequestOrThreadCache(KeyPrefixes.ListLockListLockDeskClosureByPolicyId + brokerId.ToString());
        }

        public static GeneratedKeyForRequestOrThreadCache PriceGroupKey(Guid priceGroupID)
        {
            return new GeneratedKeyForRequestOrThreadCache(KeyPrefixes.RetrievePricingGroupById + priceGroupID.ToString());
        }
        public static GeneratedKeyForRequestOrThreadCache LockPolicyKey(Guid lockPolicyID)
        {
            return new GeneratedKeyForRequestOrThreadCache(KeyPrefixes.LockPolicy_Retrieve + lockPolicyID.ToString());
        }
        public static GeneratedKeyForRequestOrThreadCache CEDocTypeKey(Guid brokerId)
        {
            return new GeneratedKeyForRequestOrThreadCache(String.Concat(KeyPrefixes.CEDocTypeId, brokerId.ToString("N")));
        }
        public static GeneratedKeyForRequestOrThreadCache IsForQP2LoanCreationProcess()
        {
            return new GeneratedKeyForRequestOrThreadCache(KeyPrefixes.IsForQP2LoanCreationProcess);
        }
    }

    public class CurrentRequestOrThreadCache
    {   
        /// <summary>
        /// returns null if the object isn't there.
        /// </summary>
        public static object GetRequestOrThreadObjectByName(GeneratedKeyForRequestOrThreadCache key)
        {
            HttpContext context = HttpContext.Current;

            if (null != context)
            {
                return context.Items[key.Key];
            }
            else
            {
                return Thread.GetData(Thread.GetNamedDataSlot(key.Key));
            }
        }
        public static void SetRequestOrThreadObjectByName(GeneratedKeyForRequestOrThreadCache key, object value)
        {
            HttpContext context = HttpContext.Current;

            if (null != context)
            {
                context.Items[key.Key] = value;
            }
            else
            {
                Thread.SetData(Thread.GetNamedDataSlot(key.Key), value);
            }
        }
    }

    public class ExpiringCurrentRequestOrThreadCache
    {
        private class Cookie
        {
            public object Value;
            public DateTime Expiration;
            public Cookie()
            {
            }
            public Cookie(object value, TimeSpan expiresFromNow)
            {
                Initialize(
                      value
                    , DateTime.Now + expiresFromNow
                );
            }
            public Cookie(object value, DateTime expiration)
            {
                Initialize(
                      value
                    , expiration
                );
            }
            private void Initialize(object value, DateTime expiration)
            {
                Value = value;
                Expiration = expiration;
            }
            public bool IsExpired
            {
                get
                {
                    if (Expiration < DateTime.Now)
                    {
                        return true;
                    }
                    return false;
                }
            }
        }

        public static object GetRequestOrThreadObjectByName(GeneratedKeyForRequestOrThreadCache key)
        {
            var cookie = (Cookie)CurrentRequestOrThreadCache.GetRequestOrThreadObjectByName(key);
            if (cookie.IsExpired)
            {
                return null;
            }
            else
            {
                return cookie.Value;
            }
        }
        public static void SetRequestOrThreadObjectByName(GeneratedKeyForRequestOrThreadCache key, object value, TimeSpan expiresFromNow)
        {
            var cookie = new Cookie(value, expiresFromNow);
            CurrentRequestOrThreadCache.SetRequestOrThreadObjectByName(key, cookie);
        }
        public static void SetRequestOrThreadObjectByName(GeneratedKeyForRequestOrThreadCache key, object value, DateTime expiration)
        {
            var cookie = new Cookie(value, expiration);
            CurrentRequestOrThreadCache.SetRequestOrThreadObjectByName(key, cookie);
        }
    }
}
