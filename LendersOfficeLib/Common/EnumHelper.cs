﻿namespace LendersOffice.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Defines a set of utilities on enum types for functionality not provided
    /// through the core library's <see cref="Enum"/> class.
    /// </summary>
    public static class EnumHelper
    {
        /// <summary>
        /// Gets the maximum value of the enum as defined in the underlying type.
        /// </summary>
        /// <typeparam name="TEnum">The enum type.</typeparam>
        /// <returns>The maximum value of the enum.</returns>
        public static TEnum MaxValue<TEnum>() where TEnum : struct, IConvertible => ValuesHelper<TEnum>.SortedValues.Last();

        /// <summary>
        /// Returns an indication whether a constant with the specified value exists in <typeparamref name="TEnum"/>.
        /// </summary>
        /// <typeparam name="TEnum">The enum type.</typeparam>
        /// <param name="value">The value of a constant in <typeparamref name="TEnum"/>.</param>
        /// <returns><see langword="true"/> if a constant in <typeparamref name="TEnum"/> has a value equal to <paramref name="value"/>; otherwise, <see langword="false"/>.</returns>
        public static bool IsDefined<TEnum>(TEnum value) where TEnum : struct, IConvertible
        {
            return Array.BinarySearch(ValuesHelper<TEnum>.SortedValues, value) >= 0;
        }

        /// <summary>
        /// Returns a list of the values for <typeparamref name="TEnum"/>.
        /// </summary>
        /// <typeparam name="TEnum">The enum type.</typeparam>
        /// <returns>A list of the values.</returns>
        public static IReadOnlyList<TEnum> GetValues<TEnum>() where TEnum : struct, IConvertible
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<TEnum>(ValuesHelper<TEnum>.Values);
        }

        /// <summary>
        /// Provides the values of a particular enum type.
        /// </summary>
        /// <typeparam name="TEnum">The enum type.</typeparam>
        private static class ValuesHelper<TEnum> where TEnum : struct, IConvertible
        {
            /// <summary>
            /// The raw list of values of the constants for <typeparamref name="TEnum"/>.
            /// </summary>
            internal static readonly TEnum[] Values;

            /// <summary>
            /// A sorted list of values of the constants for <typeparamref name="TEnum"/>.
            /// </summary>
            internal static readonly TEnum[] SortedValues;

            /// <summary>
            /// Initializes static members of the <see cref="ValuesHelper{TEnum}"/> class.
            /// </summary>
            static ValuesHelper()
            {
                TEnum[] values = (TEnum[])Enum.GetValues(typeof(TEnum)); // GetValues will throw if TEnum is not an enum type
                TEnum[] sortedValues = new TEnum[values.Length];
                Array.Copy(values, sortedValues, values.Length);
                Array.Sort(sortedValues);
                Values = values;
                SortedValues = sortedValues;
            }
        }
    }
}
