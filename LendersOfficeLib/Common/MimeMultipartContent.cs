﻿namespace LendersOffice.Common
{
    using System.Collections.Generic;
    using System.Text;
    using System.Text.RegularExpressions;
    using LendersOffice.Drivers.NetFramework;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Represents MIME Multipart content. Serializes and deserializes the content for internet transmission. 
    /// See https://www.rfc-editor.org/info/rfc2046 for the specification of the MIME multipart content type.
    /// </summary>
    /// <remarks>
    /// The <see cref="System.Net.Http.MultipartContent"/> class could be an alternative here. 
    /// It already handles the serialization of multipart content and does content header validation.
    /// However: 
    /// 1. The parsing of MultipartContent from strings is done through the System.Net.Http.HttpContentMultipartExtensions class in the 
    /// System.Net.Http.Formatting assembly, and would increase our reliance on external dependencies. 
    /// 2. Different paradigms: The HttpContentMultipartExtensions implementation uses async tasks and streams,
    /// which would be difficult to adapt to the current system. Also, HttpContent isn't consumed by our <see cref="System.Net.HttpWebRequest"/>-based model.
    /// We'd need to shift to <see cref="System.Net.Http.HttpClient"/> to get full use out of it.
    /// 3. Some of our integrations may not comply exactly with the multipart spec (though DU appears to) 
    /// and a custom implementation can be adapted to those inconsistencies.
    /// <para/>
    /// There also exists <see cref="CommonLib.MultiPartFormData"/>, but that class has problems with readability and header support.
    /// </remarks>
    public class MimeMultipartContent
    {
        /// <summary>
        /// Gets a list of parts contained by the multipart content.
        /// </summary>
        /// <value>Multipart content parts.</value>
        public List<MultipartPart> Parts { get; private set; } = new List<MultipartPart>();

        /// <summary>
        /// Gets or sets the boundary used to create the multipart content, which will also be used by default for serializing the content.
        /// </summary>
        /// <value>Multipart boundary.</value>
        public string Boundary { get; set; }

        /// <summary>
        /// Creates a multipart content instance from the specified content string (e.g. an HTTP request body).
        /// </summary>
        /// <param name="multiPartContentString">The content string to parse.</param>
        /// <param name="boundary">The multipart content boundary to use. This is specified in the "boundary" parameter of the "Content-Type: multipart/xxx; boundary=***" MIME header.</param>
        /// <returns>The parsed MimeMultipartContent instance, or null if one could not be parsed.</returns>
        public static MimeMultipartContent Parse(string multiPartContentString, string boundary)
        {
            if (!BoundaryIsValid(boundary))
            {
                return null;
            }

            boundary = Regex.Escape(boundary);

            // These patterns are based on the MIME multipart type spec in RFC 2046 (https://www.rfc-editor.org/info/rfc2046), 
            // modified slightly to fit the 2 integrations (DU and Equifax) that use this format.
            string partContentRegex = @"(?<partheader>(?<headername>(Content-|X-)[^:]+)\s*:\s*(?<headervalue>[^\r]+)\r\n)*(?(partheader)|\r\n)(?<partcontent>.*?)";
            string mimePartRegex = $@"(?<transportpadding>[^\r]*)\r\n(?<part>{partContentRegex})(?=\r\n--{boundary})";
            string validationRegex = $@"^(--{boundary}|(?<preamble>.*?)\r\n--{boundary}){mimePartRegex}(\r\n--{boundary}{mimePartRegex})*\r\n--{boundary}(--)?(?<epilogue>.*)$";

            // Validate the full content. If it doesn't match this regex, it should be considered invalid.
            var validationMatch = Regex.Match(multiPartContentString, validationRegex, RegexOptions.Singleline);
            if (!validationMatch.Success)
            {
                return null;
            }
            else
            {
                MimeMultipartContent parsedContent = new MimeMultipartContent();
                parsedContent.Boundary = boundary;

                // Anything dealing with preamble, transport-padding, or epilogue content can go here.
                // There's no use for them right now though, and the spec indicates they should be accepted but ignored.
                foreach (Capture part in validationMatch.Groups["part"].Captures)
                {
                    MultipartPart createdPart = new MultipartPart();

                    // Match again to get the captures within this part's substring (so each part can know its own headers rather than having a big collection of all headers)
                    var parsingMatch = Regex.Match(part.Value, $"^{partContentRegex}$", RegexOptions.Singleline);
                    createdPart.Content = parsingMatch.Groups["partcontent"].Value.Trim();
                    for (int i = 0; i < parsingMatch.Groups["partheader"].Captures.Count; i++)
                    {
                        createdPart.Headers.Add(parsingMatch.Groups["headername"].Captures[i].Value.Trim(), parsingMatch.Groups["headervalue"].Captures[i].Value.Trim());
                    }

                    parsedContent.Parts.Add(createdPart);
                }

                return parsedContent;
            }
        }

        /// <summary>
        /// Returns a value indicating whether the boundary value provided conforms to the RFC specification.
        /// </summary>
        /// <param name="boundary">The boundary string to check.</param>
        /// <returns>Whether the boundary string matches the expected length and character set.</returns>
        public static bool BoundaryIsValid(string boundary)
        {
            return RegularExpressionHelper.IsMatch(RegularExpressionString.MimeMultipartBoundary, boundary);
        }

        /// <summary>
        /// Attempts to parse a boundary from some string.
        /// </summary>
        /// <param name="contentType">The string to pull the boundary value from.</param>
        /// <returns>The string if successful, null otherwise.</returns>
        public static string ExtractBoundary(string contentType)
        {
            var match = Regex.Match(contentType, ".*boundary=\"?(?<boundary>[^\";]*)\"?.*");
            if (match.Success && match.Groups["boundary"].Success)
            {
                return match.Groups["boundary"].Value;
            }

            return null;
        }

        /// <summary>
        /// Overrides the default ToString implementation.
        /// </summary>
        /// <returns>The multipart content, serialized for internet transmission.</returns>
        public override string ToString()
        {
            return this.ToString(this.Boundary);
        }

        /// <summary>
        /// Writes the content to a serialized form for internet transmission, with the given boundary.
        /// </summary>
        /// <param name="boundary">A boundary to use instead of the default boundary for this content.</param>
        /// <returns>The multipart content, serialized for internet transmission.</returns>
        /// <remarks>
        /// The RFC spec specifically uses CRLF line endings, which is why those are used here instead of <see cref="System.Environment.NewLine"/>.
        /// We wouldn't want this to break if run on a Linux server.
        /// </remarks>
        public string ToString(string boundary)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"--{boundary}");
            foreach (MultipartPart part in this.Parts)
            {
                foreach (KeyValuePair<string, string> header in part.Headers)
                {
                    sb.Append($"\r\n{header.Key}: {header.Value}");
                }

                sb.Append($"\r\n\r\n{part.Content}\r\n--{boundary}");
            }

            if (this.Parts.Count == 0)
            {
                sb.Append($"\r\n\r\n--{boundary}");
            }

            sb.Append("--\r\n");

            return sb.ToString();
        }
    }
}
