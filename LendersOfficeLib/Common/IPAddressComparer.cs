﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace LendersOffice.Common
{
    public static class IPAddressComparer
    {
        /// <summary>
        /// Returns -1 if first is bigger, 0 if equal, and 1 if second is bigger.
        /// <para> </para>
        /// Throws ArgumentException if the ipaddresses are different families.
        /// <para> </para>
        /// Throws NullReferenceException if either ip is null.
        /// </summary>
        public static int Compare(IPAddress one, IPAddress two)
        {
            if (one.AddressFamily != two.AddressFamily)
            {
                throw new ArgumentException("Can't compare ip addresses of different families.");
            }
            var oneBytes = one.GetAddressBytes();
            var twoBytes = two.GetAddressBytes();

            for (var i = 0; i < oneBytes.Length; i++)
            {
                if (oneBytes[i] < twoBytes[i])
                {
                    return 1;
                }
                else if (twoBytes[i] < oneBytes[i])
                {
                    return -1;
                }
                // else they are equal and we check the next bits.
            }
            return 0;
        }

        /// <summary>
        /// Returns -1 if first is bigger, 0 if equal, and 1 if second is bigger.
        /// <para> </para>
        /// Throws ArgumentException if the ipaddresses are different families.
        /// <para> </para>
        /// Throws NullReferenceException if either ip is null.
        /// </summary>
        public static int CompareTo(this IPAddress thisOne, IPAddress theOtherOne)
        {
            return Compare(thisOne, theOtherOne);
        }
    }
}
