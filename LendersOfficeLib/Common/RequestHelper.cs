namespace LendersOffice.Common
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Security.Principal;
    using System.Threading;
    using System.Web;
    using System.Web.Security;

    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Kayako;
    using LendersOffice.ObjLib.Security;
    using LendersOffice.Security;
    using CBaseException = DataAccess.CBaseException;
    using LqbGrammar;
    using LqbGrammar.Drivers.SecurityEventLogging;
    using LqbGrammar.Drivers;
    using LqbGrammar.DataTypes;
    using Drivers.SqlServerDB;
    using System.Net;
    using LqbGrammar.Exceptions;
    using System.Security.Cryptography;

    public enum E_CookieModeT
    {
        Persistent,
        NonPersistent
    }
    public class RequestHelper
    {

        public static void SendFileToClient(HttpContext context, string path, string contentType, string fileName, bool inlineFile = false)
        {
            if (!context.Response.IsClientConnected)
            {
                return;
            }

            var headersToSend = new List<Tuple<string, string>>();
            foreach (var key in context.Response.Headers.AllKeys)
            {
                if (key.StartsWith("Access-Control-"))
                {
                    headersToSend.Add(Tuple.Create(key, context.Response.Headers[key]));
                }
            }

            context.Response.ClearHeaders();
            context.Response.Clear();

            foreach (var header in headersToSend)
            {
                context.Response.AddHeader(header.Item1, header.Item2);
            }

            if (context.Items.Contains("COMPRESSED") && (bool)context.Items.Contains("COMPRESSED"))
            {
                throw CBaseException.GenericException(contentType + " does not support send filetoclient with gzip on.");
            }

            context.Response.ContentType = contentType;
            string mode = "attachment";

            if (inlineFile)
            {
                mode = "inline";
            }

            string headerValue = string.Concat(mode, "; filename=", fileName);
            context.Response.AddHeader("Content-Disposition", headerValue);

            FileInfo fileInfo = new FileInfo(path);

            //set cachebility to private to allow IE to download it via HTTPS. Otherwise it might refuse it
            //see reason for HttpCacheability.Private at http://support.microsoft.com/kb/812935
            context.Response.Cache.SetCacheability(HttpCacheability.Private);
            context.Response.Buffer = false;
            context.Response.BufferOutput = false;
            context.Response.AddHeader("Content-Length", fileInfo.Length.ToString());

            //Do not exceed 80k as this will be allocated to the LOH.
            int blockSize = 32768;

            if (fileInfo.Length < blockSize)
            {
                blockSize = (int)fileInfo.Length;
            }

            byte[] buffer = new byte[blockSize];
            int bytesRead;

            using (FileStream fs = fileInfo.OpenRead())
            {
                while ((bytesRead = fs.Read(buffer, 0, blockSize)) > 0)
                {
                    context.Response.OutputStream.Write(buffer, 0, bytesRead);
                }
                context.Response.OutputStream.Flush();
            }
        }

        private static void DisplayErrorPage(CBaseException exception, bool isSendEmail)
        {
            HttpContext context = HttpContext.Current;

            Guid brokerId = Guid.Empty;
            Guid employeeId = Guid.Empty;

            if (null != context)
            {
                AbstractUserPrincipal principal = context.User as AbstractUserPrincipal;
                if (null != principal)
                {
                    brokerId = principal.BrokerId;
                    employeeId = principal.EmployeeId;
                }
            }
            ErrorUtilities.DisplayErrorPage(exception, isSendEmail, brokerId, employeeId);
        }

        public static string GetSafeQueryString(string key)
        {
            return Utilities.SafeHtmlString(HttpContext.Current.Request.QueryString[key]);
        }

        /// <summary>
        /// Gets the query string value for the specified key.  Unlike <seealso cref="GetSafeQueryString(string)"/>, this does not HTML escape the value.
        /// </summary>
        /// <param name="key">The key to retrieve.</param>
        /// <returns>The value at the specified key, if found; otherwise, null.</returns>
        public static string GetQueryString(string key)
        {
            return HttpContext.Current.Request.QueryString[key];
        }

        /// <summary>
        /// Return a guid from Request.QueryString. Redirect to error page
        /// if value does not exist or value is malformat.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static Guid GetGuid(string key)
        {
            Guid value = Guid.Empty;
            try
            {
                if (HttpContext.Current.Request.QueryString[key] != "")
                {
                    value = new Guid(HttpContext.Current.Request.QueryString[key]);
                }

            }
            catch {}
            if (value == Guid.Empty)
            {
                DisplayErrorPage(new CBaseException(ErrorMessages.Generic, key + " is not a valid Guid format."), false);
            }
            return value;
        }
        /// <summary>
        /// Return Guid from Request.QueryString. Return default value if
        /// QueryString value does not exist.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static Guid GetGuid(string key, Guid defaultValue)
        {
            Guid value = defaultValue;
            try
            {
                string val = HttpContext.Current.Request.QueryString[key];
                if( val != "" && val != null )
                {
                    value = new Guid(val);
                }

            }
            catch {}

            return value;
        }

        /// <summary>
        /// Returns an enum value from the request's URL query string.
        /// </summary>
        /// <typeparam name="TEnum">The type of enum to return.</typeparam>
        /// <param name="key">The key of the value to look for in the query string.</param>
        /// <param name="defaultValue">The default value to return if the key is not found or parsing fails.</param>
        /// <returns>A parsed enum from the specified query string parameter. If not found, returns the provided default value.</returns>
        public static TEnum GetEnum<TEnum>(string key, TEnum defaultValue, bool ignoreCase = false) where TEnum : struct => GetNullableEnum<TEnum>(key, ignoreCase) ?? defaultValue;

        /// <summary>
        /// Returns a <see cref="Nullable{T}"/> enum value from the request's URL query string.
        /// </summary>
        /// <typeparam name="TEnum">The type of enum to return.</typeparam>
        /// <param name="key">The key of the value to look for in the query string.</param>
        /// <returns>A parsed enum from the specified query string parameter. If not found, returns null.</returns>
        public static TEnum? GetNullableEnum<TEnum>(string key, bool ignoreCase = false) where TEnum : struct
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new CBaseException(ErrorMessages.Generic, $"RequestHelper.GetEnum(): {typeof(TEnum)} is not an Enum type.");
            }

            return HttpContext.Current.Request.QueryString[key].ToNullableEnum<TEnum>(ignoreCase);
        }

        /// <summary>
        /// Return loanid value in Request.QueryString.
        /// </summary>
        public static Guid LoanID
        {
            get
            {
                string key = "rLoanID";
                if (HttpContext.Current.Items[key] == null || HttpContext.Current.Items[key].GetType() != typeof(Guid))
                {
                    Guid value = GetGuid("loanid");
                    HttpContext.Current.Items[key] = value;
                }
                return (Guid) HttpContext.Current.Items[key];

            }
        }

        private static string DecryptAndExtractIp(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }

            try
            {
                byte[] encryptedBytes = Convert.FromBase64String(str);
                string decryptedText = EncryptionHelper.DecryptString(EncryptionKeyIdentifier.WebserviceProxy, encryptedBytes);

                // dd 1/31/2019 - Decrypted text has the following format: {RandomString}|{OriginalClientIp}|{ExpirationDateInTicks}
                string[] parts = decryptedText.Split('|');

                if (parts.Length != 3)
                {
                    return string.Empty;
                }

                long ticks = 0;

                if (!long.TryParse(parts[2], out ticks))
                {
                    return string.Empty;
                }

                DateTime expirationDate = new DateTime(ticks);

                if (expirationDate < DateTime.Now)
                {
                    return string.Empty;
                }

                return parts[1];
            }
            catch (FormatException)
            {
                // NO-OP. If string is not valid base64 encode then do nothing.
            }
            catch (DeveloperException)
            {

            }
            catch (CryptographicException)
            {
                // NO-OP
            }

            return string.Empty;
        }

        /// <summary>
        /// Returns the Client's IP address
        /// </summary>
        public static String ClientIP
        {
            get
            {
                if (HttpContext.Current != null)
                {
                    if (HttpContext.Current.Request != null)
                    {
                        HttpRequest CurrRequest = HttpContext.Current.Request;

                        string ip = CurrRequest.ServerVariables["HTTP_X_FORWARDED_FOR_ML"];
                        if (!string.IsNullOrEmpty(ip))
                        {
                            return ip;
                        }

                        ip = DecryptAndExtractIp(CurrRequest.Headers["LQB_FORWARDED_FOR_WS_PROXY"]); // dd 1/31/2019 - Our webservice proxy pass the original client ip in this header.
                        if (!string.IsNullOrEmpty(ip))
                        {
                            return ip;
                        }

                        ip = CurrRequest.ServerVariables["REMOTE_ADDR"];
                        if (!string.IsNullOrEmpty(ip))
                        {
                            return ip;
                        }

                        return CurrRequest.UserHostAddress;
                    }
                }
                return "";
            }
        }

        /// <summary>
        /// Return bool value for query string variable. Return true if value is 'T', 't', '1', 'true', else return false.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool GetBool(string key)
        {
            bool value = false;
            string str = HttpContext.Current.Request.QueryString[key];

            if (str != null && (str == "T" || str == "t" || str == "1" || str.ToLower() == "true"))
            {
                value = true;
            }
            return value;
        }


        /// <summary>
        /// Return int from Request.QueryString. Redirect user to error page
        /// if key is not found or invalid number.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static int GetInt(string key)
        {
            if (HttpContext.Current.Request.QueryString[key] != "")
            {
                try
                {
                    return int.Parse(HttpContext.Current.Request.QueryString[key]);
                }
                catch {}
            }
            CBaseException e = new CBaseException(ErrorMessages.Generic, key + " is not a valid number format.");
            DisplayErrorPage(e, false);

            return -1;
        }

        /// <summary>
        /// Return int from Request.QueryString. Return default value if key is not found or invalid number.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static int GetInt(string key, int defaultValue)
        {
            int value;
            if (int.TryParse(HttpContext.Current.Request.QueryString[key], out value))
                return value;

            return defaultValue;
        }


        /// <summary>
        /// Return long from Request.QueryString. Return default value if key is not found or invalid number.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static long GetLong(string key, long defaultValue)
        {
            long value;
            if (long.TryParse(HttpContext.Current.Request.QueryString[key], out value))
                return value;

            return defaultValue;
        }

        /// <summary>
        /// Called post login to redirect the user through a series of pages that perform post login checks and tasks.
        /// </summary>
        /// <remarks>
        /// Certain tasks redirect the user to another page. When that happens, DoNextPostLoginTask should be called again
        /// from that page with the task that page performs passed in as the lastTask parameter.
        /// </remarks>
        /// <param name="lastTask">The last task performed in the post login process.</param>
        public static void DoNextPostLoginTask(PostLoginTask lastTask)
        {
            if (HttpContext.Current == null)
            {
                return;
            }

            HttpContext context = HttpContext.Current;

            PostLoginTask nextTask = lastTask + 1;
            switch (nextTask)
            {
                case PostLoginTask.RunBrowserXtVerificationCheck:
                    RunBrowserXtVerificationCheck(context);
                    goto case PostLoginTask.ChangePassword; // Fall through if we did not redirect.
                case PostLoginTask.ChangePassword:
                    ChangePassword(context);
                    goto case PostLoginTask.CheckLicensing;    // Fall through if we did not redirect.
                case PostLoginTask.CheckLicensing:
                    CheckLicensing(context);
                    goto case PostLoginTask.AcceptTerms;    // Fall through if we did not redirect.
                case PostLoginTask.AcceptTerms:
                    ForceAcceptLatestAgreement(context);
                    goto case PostLoginTask.SetSecurityQuestions;    // Fall through if we did not redirect.
                case PostLoginTask.SetSecurityQuestions:
                    SetSecurityQuestions(context);
                    goto case PostLoginTask.Redirect;    // Fall through if we did not redirect.
                case PostLoginTask.Redirect:
                    RedirectToDestination(context);
                    break;
                default:
                    throw new UnhandledEnumException(nextTask);
            }
        }

        /// <summary>
        /// Used to maintin info needed to determine final redirect destination across 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="url"></param>
        public static void RedirectToNextTaskPage(HttpContext context, string url)
        {
            string selectedSiteId = RequestHelper.GetSafeQueryString("tp");
            string returnTo = GetSafeQueryString("ReturnTo");

            if (!string.IsNullOrWhiteSpace(selectedSiteId))
            {
                url += (url.Contains("?") ? "&" : "?") + $"tp={selectedSiteId}";
            }

            if (!string.IsNullOrEmpty(returnTo))
            {
                url += (url.Contains("?") ? "&" : "?") + $"ReturnTo={returnTo}";
            }                        

            context.Response.Redirect(url);
        }

        /// <summary>
        /// Forward the request to another server.
        /// </summary>
        /// <param name="host">Host name to forward.</param>
        /// <param name="context">The original HttpContext object..</param>
        public static void Forwarding(string host, HttpContext context)
        {
            UriBuilder builder = new UriBuilder(context.Request.Url);
            builder.Host = host; // Only replace the host of the original request.
            string url = builder.ToString();

            var newHttpRequest = (HttpWebRequest)WebRequest.Create(url);

            var currentHttpRequest = context.Request;

            foreach (string headerName in currentHttpRequest.Headers.AllKeys)
            {
                if (!WebHeaderCollection.IsRestricted(headerName))
                {
                    newHttpRequest.Headers[headerName] = currentHttpRequest.Headers[headerName];
                }
            }

            if (currentHttpRequest.ContentLength > 0 && currentHttpRequest.InputStream.Length == 0)
            {
                // dd 10-21-2018 - This can happen when the InputStream of original request was disposed. This sanity check
                // help prevent hard to detect error when forward an empty body.
                throw CBaseException.GenericException("HttpRequest.InputStream.Length is zero. HttpRequest.InputStream is already disposed.");
            }

            newHttpRequest.ContentType = currentHttpRequest.ContentType;
            newHttpRequest.Method = currentHttpRequest.HttpMethod;
            newHttpRequest.UserAgent = currentHttpRequest.UserAgent;

            currentHttpRequest.InputStream.Seek(0, SeekOrigin.Begin);

            using (Stream newRequestStream = newHttpRequest.GetRequestStream())
            {
                currentHttpRequest.InputStream.CopyTo(newRequestStream);
            }

            using (var newHttpResponse = newHttpRequest.GetResponse())
            {
                foreach (string headerName in newHttpResponse.Headers.AllKeys)
                {
                    if (!WebHeaderCollection.IsRestricted(headerName, true))
                    {
                        context.Response.Headers[headerName] = newHttpResponse.Headers[headerName];
                    }
                }
                context.Response.ContentType = newHttpResponse.ContentType;

                newHttpResponse.GetResponseStream().CopyTo(context.Response.OutputStream);
            }

            context.Response.Flush();
            context.Response.End();
        }

        private static void ChangePassword(HttpContext context)
        {
            BrokerUserPrincipal brokerUser = BrokerUserPrincipal.CurrentPrincipal;

            // OPM 24342 - Ethan
            //
            // added for LOAdmin, to see if an internal user's account password requires a change or not
            var principal = Thread.CurrentPrincipal as AbstractUserPrincipal;

            if (principal is InternalUserPrincipal || (principal != null && principal.InitialPrincipalType == InitialPrincipalTypeT.Internal))
            {
                Guid m_userID = Guid.Empty;
                m_userID = principal is InternalUserPrincipal ? principal.UserId : principal.InitialUserId;

                LendersOffice.LOAdmin.Internal.InternalUserDB user = new LendersOffice.LOAdmin.Internal.InternalUserDB(m_userID);

                if (user.Retrieve())
                {
                    if (user.hasStrongPassword != true)
                    {
                        if (principal is InternalUserPrincipal)
                        {
                            context.Response.Redirect("~/LOAdmin/Internal/ChangePassword.aspx?requirepwchange=true");
                        }
                        else
                        {
                            context.Response.Redirect("~/loadmin.aspx?requireinternalpwchange=true");
                        }
                    }
                }
            }

            /// 12/17/2003 dd - If account is broker user and password already expired, redirect them to the change password page.
            //OPM 19602
            if (null != brokerUser && brokerUser.IsPasswordExpired && brokerUser.InitialPrincipalType == InitialPrincipalTypeT.NonInternal)
            {
                if (ConstStage.EnableEnhancedPostLoginChecks)
                {
                    if (brokerUser.PostLoginTask != PostLoginTask.Redirect)
                    {
                        brokerUser.PostLoginTask += 1;
                    }

                    StoreToCookie(brokerUser, ConstApp.LOCookieExpiresInMinutes, E_CookieModeT.Persistent);
                }

                RedirectToNextTaskPage(context, "~/los/ChangePassword.aspx");
            }
        }

        /// <summary>
        /// Redirects the user to the post-log in page verifying that
        /// BrowserXT is not installed on the client's work station.
        /// </summary>
        /// <param name="context">
        /// The context for the request.
        /// </param>
        private static void RunBrowserXtVerificationCheck(HttpContext context)
        {
            if (ConstStage.DisableBrowserXtVerificationPostLogInStep)
            {
                return;
            }

            if (BrokerUserPrincipal.CurrentPrincipal == null)
            {
                return;
            }

            if (BrokerUserPrincipal.CurrentPrincipal.BrokerDB != null && BrokerUserPrincipal.CurrentPrincipal.BrokerDB.DisableBrowserXtVerificationPostLogInStep)
            {
                return;
            }

            if (ConstStage.EnableEnhancedPostLoginChecks)
            {
                if (BrokerUserPrincipal.CurrentPrincipal.PostLoginTask != PostLoginTask.Redirect)
                {
                    BrokerUserPrincipal.CurrentPrincipal.PostLoginTask += 1;
                }

                StoreToCookie(BrokerUserPrincipal.CurrentPrincipal, ConstApp.LOCookieExpiresInMinutes, E_CookieModeT.Persistent);
            }

            RedirectToNextTaskPage(context, "~/newlos/RunBrowserXtVerificationCheck.aspx");
        }

        private static void CheckLicensing(HttpContext context)
        {
            BrokerUserPrincipal brokerUser = BrokerUserPrincipal.CurrentPrincipal;
            if (brokerUser == null)
            {
                return;
            }

            if (brokerUser.BillingVersion == E_BrokerBillingVersion.New)
            {
                // 4/29/2015 dd - This billing version is no longer valid.
                throw new NotSupportedException();
            }
            else if (brokerUser.BillingVersion == E_BrokerBillingVersion.NoBilling
                || brokerUser.BillingVersion == E_BrokerBillingVersion.PerTransaction)
            {
                //do no checking.
                return;
            }

            #region old license version
            LendersOfficeApp.ObjLib.Licensing.BrokerUser brokerUserLic = new LendersOfficeApp.ObjLib.Licensing.BrokerUser(brokerUser.UserId, brokerUser.BrokerId, brokerUser.EmployeeId, brokerUser.DisplayName, brokerUser.Permissions, brokerUser.Type);
            if (null != brokerUserLic && brokerUserLic.EnforceLicensing())
            {
                if (ConstStage.EnableEnhancedPostLoginChecks)
                {
                    if (brokerUser.PostLoginTask != PostLoginTask.Redirect)
                    {
                        brokerUser.PostLoginTask += 1;
                    }

                    StoreToCookie(brokerUser, ConstApp.LOCookieExpiresInMinutes, E_CookieModeT.Persistent);
                }

                // OPM 19602
                if (!brokerUserLic.HasValidLicense())
                {
                    RedirectToNextTaskPage(context, "~/los/Licensing/LicenseExpired.aspx");
                }
                else if (brokerUserLic.DisplayRenewalReminder())
                {
                    RedirectToNextTaskPage(context, "~/los/Licensing/RenewReminder.aspx");
                }
            }
            #endregion
        }

        /// <summary>
        /// Check to see if user need to accept latest TOS agreement. Currently only require for BrokerUserPrincipal object.
        /// </summary>
        private static void ForceAcceptLatestAgreement(HttpContext context)
        {
            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
            if (null != principal && principal.NeedToAcceptLatestAgreement)
            {
                if (ConstStage.EnableEnhancedPostLoginChecks)
                {
                    if (principal.PostLoginTask != PostLoginTask.Redirect)
                    {
                        principal.PostLoginTask += 1;
                    }

                    StoreToCookie(principal, ConstApp.LOCookieExpiresInMinutes, E_CookieModeT.Persistent);
                }

                RedirectToNextTaskPage(context, "~/los/term.aspx");
            }
        }

        /// <summary>
        /// Check to see if user needs to set security questions. Currently only require for BrokerUserPrincipal object.
        /// </summary>
        private static void SetSecurityQuestions(HttpContext context)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["secquest"];    // NOTE: DON'T USE GET. GET CREATES COOKIE IF NOT FOUND.
            BrokerUserPrincipal principal = BrokerUserPrincipal.CurrentPrincipal;
            if (null != principal
                && !BrokerUserDB.AreSecurityAnswersSetForTheUser(principal.BrokerId, principal.UserId)
                && (cookie == null || cookie.Value != "f"))
            {
                if (ConstStage.EnableEnhancedPostLoginChecks)
                {
                    if (principal.PostLoginTask != PostLoginTask.Redirect)
                    {
                        principal.PostLoginTask += 1;
                    }

                    StoreToCookie(principal, ConstApp.LOCookieExpiresInMinutes, E_CookieModeT.Persistent);
                }

                RedirectToNextTaskPage(context, "~/website/passwordreset/SetSecurityQuestions.aspx");
            }
        }

        private static void RedirectToDestination(HttpContext context)
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

            if (ConstStage.EnableEnhancedPostLoginChecks)
            {
                principal.PostLoginTask = PostLoginTask.Redirect;
                StoreToCookie(principal, ConstApp.LOCookieExpiresInMinutes, E_CookieModeT.Persistent);
            }

            switch (principal.Type)
            {
                case "B":
                    string url = ConstApp.LendingQBPipelinePage;
                    var referrer = GetReferrer();
                    if (referrer != null)
                    {
                        string returnTo = GetSafeQueryString("ReturnTo");
                        if (referrer.Item1.StartsWith("https://support.lendingqb.com"))
                        {
                            AuthManager manager = new AuthManager();
                            try
                            {
                                url = manager.GetAuthUrlFor(principal);
                                if (!string.IsNullOrEmpty(returnTo))
                                {
                                    url += (url.Contains("?") ? "&" : "?") + $"ReturnTo={HttpContext.Current.Server.UrlEncode(returnTo)}";
                                }
                            }
                            catch (CBaseException)
                            {
                                RequestHelper.StoreToCookie("DisplaySupportConfirm", "EmailFailure", DateTime.Now.AddMinutes(1));
                            }
                        }
                        else
                        {
                            url = referrer.Item1 + ThirdPartyAuthHandler.GetSafeBase64AuthTicket(referrer.Item3);
                        }
                    }           

                    context.Response.Redirect(url);
                    break;
                case "I":
                    context.Response.Redirect("~/LOAdmin/main.aspx");
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unexpected principal type: " + principal.Type);
            }
        }

        public static Tuple<string, string, string> GetReferrer()
        {
            string selectedSiteId = RequestHelper.GetSafeQueryString("tp");

            if (string.IsNullOrEmpty(selectedSiteId))
            {
                return null;
            }

            var referrers = ConstStage.ThirdPartyReferrers;
            var referrer = referrers.FirstOrDefault(p => p.Item2.Equals(selectedSiteId));
            return referrer;
        }

        /// <summary>
        /// Add key/value to cookie. If persist is false then cookie will lost when browser is close. Otherwise
        /// cookie will expire in 1 day.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="isPersist"></param>
        public static void StoreToCookie(string key, string value, bool isPersist)
        {
            HttpCookie cookie = new HttpCookie(key, value);
            // 2/12/2008 dd - If the code run on production then require cookie to be "secure". "Secure" cookie will only transmit through SSL.
            if (ConstSite.IsSecureCookieRequired)
            {
                cookie.Secure = true;
            }
            cookie.HttpOnly = true; // 3/5/2009 dd - OPM 27896 - Protected all cookies with HttpOnly

            // 1/5/2004 dd - If Expires does not set then this cookie only available until browser is closed.
            if (isPersist)
            {
                cookie.Expires = DateTime.Now.AddDays(1);
            }
            if (null != HttpContext.Current)
            {
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }

        /// <summary>
        /// Add key/value to cookie with expiration.
        /// </summary>
        public static void StoreToCookie(string key, string value, DateTime expiration, bool setDomain = false)
        {
            if (null != HttpContext.Current)
            {
                HttpCookie cookie = new HttpCookie(key, value);
                // 2/12/2008 dd - If the code run on production then require cookie to be "secure". "Secure" cookie will only transmit through SSL.
                if (ConstSite.IsSecureCookieRequired)
                {
                    cookie.Secure = true;
                }
                if (setDomain && !string.IsNullOrEmpty(ConstSite.CookieDomain))
                {
                    cookie.Domain = ConstSite.CookieDomain;
                }
                cookie.HttpOnly = true; // 3/5/2009 dd - OPM 27896 - Protected all cookies with HttpOnly
                cookie.Expires = expiration;

                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }

        /// <summary>
        /// Add key/value to cookie with expiration. This cookie DOES NOT force transmit over HTTPS. Do not call this to store sensitive cookie.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expiration"></param>
        /// <param name="setDomain"></param>
        public static void StoreToCookieInsecure(string key, string value, DateTime expiration, bool setDomain = false)
        {
            if (null != HttpContext.Current)
            {
                HttpCookie cookie = new HttpCookie(key, value);

                if (setDomain && !string.IsNullOrEmpty(ConstSite.CookieDomain))
                {
                    cookie.Domain = ConstSite.CookieDomain;
                }
                cookie.HttpOnly = true; // 3/5/2009 dd - OPM 27896 - Protected all cookies with HttpOnly
                cookie.Expires = expiration;

                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }

        /// <summary>
        /// Allow LOAdmin to become another user. Only support Broker User type.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>
        /// False if failed.  Otherwise, redirect kicks in.
        /// </returns>
        public static bool BecomeUser(Guid brokerId, Guid userID, Guid initialUserId)
        {
            return BecomeUser(brokerId, userID, true, false, initialUserId);
        }
        public static bool BecomeUser(Guid brokerId, Guid userID, bool willRedirect, bool use2016Ui, Guid initialUserId, bool isRetailBUser = false)
        {
            const bool allowDuplicateLogin = true;
            const bool isStoreToCookie = true;

            var principal = PrincipalFactory.Create(brokerId, userID, "B", allowDuplicateLogin, isStoreToCookie, InitialPrincipalTypeT.Internal, initialUserId);

            if (principal != null)
            {
                SecurityEventLogHelper.CreateBecomeUserLog(principal);

                if (!isRetailBUser || principal.BrokerDB.EnableRetailTpoPortalMode)
                {
                    // 1/5/2004 dd - Allow LOAdmin to become broker user without accepting user agreement.
                    StoreToCookie("tos", "f", false);

                    // OPM 68408 - Allow LOAdmin to become broker user without being prompted to set security questions.
                    StoreToCookie("secquest", "f", false);
                    if (null != HttpContext.Current)
                    {
                        if (willRedirect)
                        {
                            if (isRetailBUser)
                            {
                                HttpContext.Current.Response.Redirect(BecomePmlUserUrl(userID, pmlLenderSiteId: Guid.Empty, initialUserId: initialUserId, isRetail: true));
                            }
                            else if (use2016Ui)
                            {
                                HttpContext.Current.Response.Redirect("~/Pipeline/Pipeline.aspx");
                            }
                            else
                            {
                                HttpContext.Current.Response.Redirect("~/los/main.aspx");
                            }
                        }

                        return true;
                    }
                }                
            }

            return false;
        }        

        public static void BecomePmlUser(Guid userId, Guid pmlLenderSiteId, Guid initialUserId)
        {
            string redirectSite = BecomePmlUserUrl(userId, pmlLenderSiteId, initialUserId);
            HttpContext.Current.Response.Redirect(redirectSite);
        }
        public static string BecomePmlUserUrl(Guid userId, Guid pmlLenderSiteId, Guid initialUserId, bool isRetail = false)
        {
            string userData = $"{initialUserId.ToString()}|{ClientIP}";

            string secret = DataAccess.Tools.SecretHashForPml(userId);
            secret = HttpContext.Current.Server.UrlEncode(secret);

            var cacheResult = AutoExpiredTextCache.GetFromCache(secret);

            if (cacheResult == null)
            {
                AutoExpiredTextCache.AddToCache(userData, TimeSpan.FromHours(1), secret);
                StoreToCookie(ConstApp.InternalTpoCookieName, "T", isPersist: false);
            }
            else
            {
                AutoExpiredTextCache.UpdateCache(userData, secret);
                AutoExpiredTextCache.RefreshCacheDuration(secret, TimeSpan.FromHours(1));
            }

            Guid brokerId;

            DbConnectionInfo.GetConnectionInfoByUserId(userId, out brokerId);

            StoredProcedureName spName = StoredProcedureName.Create("GetSecurityEventLogInfoForPMLByUserId").Value;
            
            SqlParameter[] parameters =
            {
                new SqlParameter("@UserId", userId)
            };

            string firstNm = string.Empty;
            string lastNm = string.Empty;
            string loginNm = string.Empty;

            using (var conn = DbConnectionInfo.GetConnection(brokerId))
            {
                using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, spName, parameters, TimeoutInSeconds.Default))
                {
                    if (reader.Read())
                    {
                        firstNm = (string)reader["UserFirstNm"];
                        lastNm = (string)reader["UserLastNm"];
                        loginNm = (string)reader["LoginNm"];
                    }
                }
            }            

            if (!isRetail)
            {
                SecurityEventLogHelper.CreateBecomeUserLog(SystemUserPrincipal.SecurityLogPrincipal(brokerId, loginNm, firstNm, lastNm, InitialPrincipalTypeT.Internal, "P", userId));
            }

            return string.Format(
                "{0}/main/baseframe.aspx?lenderpmlsiteid={1}&userid={2}&secret={3}&islogin={4}&isRetail={5}",
                PmlBaseUrl, pmlLenderSiteId, userId, secret, ConstAppDavid.PmlStrangeKey, isRetail);
        }
        public static string PmlBaseUrl
        {
            get
            {
                // 01/06/11 m.p PML40751 Check if we are on dev or local
                if (ConstAppDavid.CurrentServerLocation == LendersOffice.Constants.ServerLocation.LocalHost)
                {
                    return ConstSite.PmlBaseUrl;
                }
                else if (ConstAppDavid.CurrentServerLocation == LendersOffice.Constants.ServerLocation.Development)
                {
                    return ConstSite.PmlBaseUrl;
                }
                else
                {
                    return ConstStage.PmlBaseUrl;
                }
            }
        }
        public static string PmlLoginUrl(Guid pmlLenderSiteId)
        {
            return PmlBaseUrl + "/simple_login.aspx?lenderpmlsiteid=" + pmlLenderSiteId;
        }

        /// <summary>
        /// Checks if the passed in ip is in the allowed loadmin IPs.
        /// </summary>
        /// <param name="ipToTest">The ip to check.</param>
        /// <returns>True if the parameter IP begins with any one of the allowed LOAdmin IPs.</returns>
        public static bool IsLOAdminIP(string ipToTest)
        {
            string[] ipList = ConstStage.AllowedIpForLOAdmin.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            // If the parameter IP does not start with any of the LOAdmin IPs, then return false.
            if (ipList.Length != 0 && !ipList.Any(ip => ipToTest.IndexOf(ip) == 0))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static void AuthenticateRequest()
        {
            HttpContext context = HttpContext.Current;

            //OPM 115545
            if (context.Request.Url.AbsolutePath.EndsWith("Website/index.aspx", StringComparison.OrdinalIgnoreCase))
            {
                return;
            }

            if (context.Request.Url.AbsolutePath.Contains("loadmin", StringComparison.OrdinalIgnoreCase))
            {
                string[] ips = ConstStage.AllowedIpForLOAdmin.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (ips.Length != 0 && !ips.Any(p=>context.Request.UserHostAddress.IndexOf(p) == 0))
                {
                    Signout();
                    return;
                }
            }

            string[] ignorePages = {
                                        "userprofileimage.aspx",
                                        ////OPM 237895: ignore authentication state for these publicly available web services.
                                        "/los/webservice/AuthService.asmx",
                                        "/los/webservice/Loan.asmx",
                                        "/los/webservice/AppView.asmx",
                                        "/los/webservice/PmlUser.asmx",
                                        "/los/webservice/OriginatingCompany.asmx",
                                        "/los/webservice/TradesAndPools.asmx",
                                        "/los/webservice/Reporting.asmx",
                                        "/los/webservice/Contacts.asmx",
                                        "/los/webservice/EDocsService.asmx",
                                        "sitecheck.aspx",
                                        "sitecheckedocs.aspx",
                                        "selfcheck.aspx",
                                        "uploadlpe.aspx",
                                        "sitecheckwebservice.aspx",
                                        "sitecheckedocs.aspx",
                                        "checkinternalresource.aspx"
                                   };

            string currentPath = context.Request.Url.AbsolutePath.ToLower();
            foreach (string page in ignorePages)
            {
                // 6/13/2013 dd - These pages don't need authorization.

                if (currentPath.EndsWith(page.ToLower()))
                {
                    context.User = new GenericPrincipal(new GenericIdentity("FOO"), new string[0]);
                    return;

                }
            }

            // Retrieve third party authenticate id.
            Guid auth_id = RequestHelper.GetGuid("auth_id", Guid.Empty);
            // Note: Only validate using auth_id on GET request only. Since
            // auth_id will get delete after first use, therefore any sequential POST request
            // will redirect to login page. dd 5/22/2003.

            if (context.Request.HttpMethod == "GET" && auth_id != Guid.Empty)
            {
                // If authenticate id is available then
                // construct MeridianLink Principal based on
                // this id.
                context.User = PrincipalFactory.Create(auth_id);
                return;
            }

            if (context.SkipAuthorization || context.User == null ||
                !context.User.Identity.IsAuthenticated ||
                context.User.Identity.AuthenticationType != "Forms")
            {
                // 12/18/2003 dd - Don't proceed with authorization if any of the conditions are meet.
                // Generally, these conditions happen when authentication did not take place.

                //OPM 64849: We sometimes want to allow anonymous users through at this point
                if (AllowAnonymousUser(context.Request.Url.AbsolutePath.ToLower()))
                {
                    context.User = new GenericPrincipal(new GenericIdentity("Foo"), new string[0]);
                }
                return;
            }

            FormsIdentity identity = (FormsIdentity) context.User.Identity;
            FormsAuthenticationTicket ticket = identity.Ticket;

            if (ticket.Version != ConstAppDavid.CookieVersion)
            {
                Signout();
            }
            context.User = PrincipalFactory.Create(identity);

            if (ConstStage.EnableEnhancedPostLoginChecks)
            {
                var isIgnorepage = false;

                foreach (var page in IgnoreAuthPages.Union(DoNotRedirectPages))
                {
                    if (currentPath.Contains(page, StringComparison.OrdinalIgnoreCase))
                    {
                        isIgnorepage = true;
                        break;
                    }
                }
                
                if (!isIgnorepage)
                {
                    var principal = ((AbstractUserPrincipal)context.User);

                    if (principal.PostLoginTask != PostLoginTask.Redirect)
                    {
                        if (principal.PostLoginTask != PostLoginTask.Login)
                        {
                            principal.PostLoginTask -= 1;
                        }

                        DoNextPostLoginTask(principal.PostLoginTask);
                    }
                }
            }

            // this code will only run if we don't redirect. 
            Thread.CurrentPrincipal = context.User;
        }

        private static readonly string[] IgnoreAuthPages =
        {
            "ExporterClfVersion.aspx",
            "ImporterClfVersion.aspx",
            "clfversion.aspx",
            "logout.aspx",
            "apperror.aspx",
            "loadmin.aspx",
            "devlogin.aspx",
            "appview.asmx",
            "authservice.asmx",
            "broker.asmx",
            "loan.asmx",
            "pmluser.asmx",
            "user.asmx",
            "EncompassLoginFailure.aspx",
            "GlobalDMS.aspx",
            "ESignUpdate.aspx",
            "logopl.aspx",
            "los/webservice/TitleVendorRquest.aspx",
            "AppraisalStatusUpdate.aspx",
            "ConsumerPDF.aspx",
            "4506TPost.aspx", // 8/8/2013 dd - OPM 88442
            "AuthenticationCodePage.aspx", // 4/27/2014 dd - OPM 125242
            "VerifyClientCertificate.aspx", // 5/10/2014 dd - OPM 125242s
            "MIFrameworkPost.aspx", // 10/28/2014 BB - OPM 187049
            "VendorCommunicationPost.aspx",
            "DocFrameworkPost.aspx",
            "DocuSignEnvelopeInformationHandler.aspx",
            "Auth.aspx",
            "ka.aspx",
            "barcodeupload.aspx",
            "loan.ashx",
            "ocrupdate.aspx",
        };

        private static readonly string[] DoNotRedirectPages =
        {
            "RunBrowserXtVerificationCheck.aspx",
            "ChangePassword.aspx",
            "LendingQBAuthenticationCodePage.aspx",
            "LicenseExpired.aspx",
            "RenewReminder.aspx",
            "term.aspx",
            "SetSecurityQuestions.aspx",
            "LicenseList.aspx",
            "BuyNewLicense.aspx",
            "AppDownload.aspx"
        };

        /// <summary>
        /// Returns true if the provided path ignores authentication and allows anonymous users in.
        /// </summary>
        /// <param name="currentPath">The path being accessed</param>
        /// <returns>True if the path should skip auth, false otherwise</returns>
        private static bool AllowAnonymousUser(string currentPath)
        {
            foreach (string page in IgnoreAuthPages)
            {
                if (currentPath.EndsWith(page.ToLower()))
                {
                    return true;
                }
            }
            return false;
        }


        public static void StoreToCookie(AbstractUserPrincipal principal, int cookieExpiresMinutes, E_CookieModeT cookieModeT, bool isAnonymousQP = false)
        {
            if (null != principal)
            {
                if (null == HttpContext.Current )
                    return;

                string userData = principal.ToUserDataString(isAnonymousQP);

                //OPM 19222 -jM
                //Adds Host IP address to user data
                //userData = userData + "|" + RequestHelper.ClientIP;
                // OPM 24152 - IP address was removed from the cookie 

                // NOTE: UserData CANNOT exceed 1000 characters or it will not stored in cookies.
                if (userData.Length > 1000)
                {
                    CBaseException exc = new CBaseException(ErrorMessages.Generic,"Cookies UserData is exceed 1000 characters. UserData=[" + userData + "]");
                    DisplayErrorPage(exc, true);
                }

                DateTime expirationTime = DateTime.Now.AddMinutes(cookieExpiresMinutes);

                if (HttpContext.Current.User != null && HttpContext.Current.User.Identity is FormsIdentity)
                {
                    FormsIdentity oldIdentity = (FormsIdentity)HttpContext.Current.User.Identity;
                    expirationTime = oldIdentity.Ticket.Expiration;

                    if (string.IsNullOrEmpty(HttpContext.Current.Request.Headers["X-Requested-With"])) //we dont want to update cookie expiration for javascript request.
                    {
                        expirationTime = DateTime.Now.AddMinutes(cookieExpiresMinutes);
                    }
                }

                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(ConstAppDavid.CookieVersion,
                    principal.Identity.Name,
                    DateTime.Now,
                    expirationTime,
                    true,
                    userData);

                string str = FormsAuthentication.Encrypt(ticket);
                string cookieName = isAnonymousQP ? ConstAppDavid.AnonymousQpCookieName : FormsAuthentication.FormsCookieName;
                HttpCookie cookie = new HttpCookie(cookieName, str);
                // 2/12/2008 dd - If the code run on production then require cookie to be "secure". "Secure" cookie will only transmit through SSL.
                if (ConstSite.IsSecureCookieRequired)
                {
                    cookie.Secure = true;
                }

                //OPM 19222 -jM
                //If browser supported, doesn't show cookie data when under XSS attack. Bypassed with XMLHttpOnly XSS attack :(
                cookie.Path = FormsAuthentication.FormsCookiePath;
                if (false == string.IsNullOrEmpty(ConstSite.CookieDomain))
                {
                    cookie.Domain = ConstSite.CookieDomain;
                }
                cookie.HttpOnly = true;

                if (cookieModeT == E_CookieModeT.Persistent)
                {
                    cookie.Expires = DateTime.MaxValue; // OPM 1760 - Reduce dependency on user system clock.
                }
                //if (null != HttpContext.Current)
                    HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }

        /// <summary>
        /// Clear authentication cookie and redirect to login page.
        /// </summary>
        public static void Signout(AbstractUserPrincipal principal = null)
        {
            if (principal != null)
            {
                SecurityEventLogHelper.CreateLogoutLog(principal);
            }

            if (HttpContext.Current != null)
            {
                ClearAuthenticationCookies();

                if (ConstStage.UseEnhancedSessionManagement)
                {
                    EndUserSession();
                }

                HttpContext.Current.Response.Redirect(ConstSite.LoginUrl);
            }            
        }

        public static void EndUserSession()
        {
            // OPM 247103. Mark the cookie session as being logged out.
            var principal = PrincipalFactory.CurrentPrincipal; 
            if (principal != null && principal.Type == "B" && principal.CookieSessionId.Length != 0)
            {
                SqlParameter[] parameters = new SqlParameter[]
                {
                    new SqlParameter("@SessionId", principal.CookieSessionId),
                    new SqlParameter("@LogoutDate", DateTime.Now)
                };

                StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "AddUserSessionLogout", 1, parameters);
            }
        }

        public static void ClearAuthenticationCookies()
        {
            HttpResponse response = null;

            if (HttpContext.Current != null)
            {
                response = HttpContext.Current.Response;
            }

            if (response == null)
            {
                return;
            }

            // 10/19/2011 dd - A list of cookies to always clear on logout.
            string[] cookieNameList = {
                                          "AVB",
                                          ".MERIDIANUSER",  // 10/19/2011 dd - Old LO Cookie
                                          ".PMLUSER",  // 10/19/2011 dd - Old PML Cookie
                                          "_LENDINGQBUSER", // 10/19/2011 dd - Current LO Cookie
                                          "_PMLUSER", // 10/19/2011 dd - Current PML Cookie
                                          FormsAuthentication.FormsCookieName,
                                          ConstApp.InternalTpoCookieName,
                                          ConstSite.PmlCookieName
                                      };
            string[] domainPathList = {
                                          "",
                                          ".lendersoffice.com",
                                          ".lendingqb.com",
                                          ConstSite.CookieDomain
                                      };

            foreach (var cookieName in cookieNameList)
            {
                if (cookieName != string.Empty)
                {
                    foreach (var domainPath in domainPathList)
                    {
                        HttpCookie cookie = new HttpCookie(cookieName, "F");
                        cookie.Expires = new DateTime(1999, 10, 12);
                        cookie.HttpOnly = true; // 8/19/2013 dd - Mark these cookies as HttpOnly so vulnerability scan will not complain.
                        if (string.IsNullOrEmpty(domainPath) == false)
                        {
                            cookie.Domain = domainPath;
                        }
                        response.Cookies.Add(cookie);
                    }
                }                
            }


        }

        /// <summary>
        /// Enforces IP Restrictions for web services. Currently, users with MFA are not allowed.
        /// </summary>
        /// <param name="principal">The user principal to be evaluated.</param>
        public static void EnforceForWebServices(AbstractUserPrincipal principal)
        {
            string currentIpAddress = ClientIP;
            bool? ipVerificationResult = VerifyIPRestriction(principal, currentIpAddress, true);
            if (ipVerificationResult.HasValue)
            {
                if (ipVerificationResult == false)
                {
                    string devMessage = "Access failed due to IP restriction. Current IP :" + currentIpAddress + " UserId:" + principal.UserId + " Username :[" + principal.LoginNm + "]";

                    if (HttpContext.Current != null)
                    {
                        var clientCertificate = HttpContext.Current.Request.ClientCertificate;

                        if (ClientCertificateUtilities.IsValidClientCertificate(clientCertificate, principal))
                        {
                            return;
                        }
                        else
                        {
                            devMessage += " clientCertificate.IsPresent=" + clientCertificate.IsPresent + ", clientCertificate.IsValid=" + clientCertificate.IsValid;
                        }
                    }

                    Tools.LogInfo("EnforceForWebServices", devMessage);
                    throw new CBaseException(ErrorMessages.IPRestrictionAccessDenied, ErrorMessages.IPRestrictionAccessDenied);
                }
            }
            else
            {
                bool isRequiredMultiFactor = false;
                BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);

                if (brokerDB.IsEnableMultiFactorAuthentication && principal.Type == "B")
                {
                    isRequiredMultiFactor = EmployeeDB.RetrieveEnabledMultiFactorAuthenticationByEmployeeId(principal.BrokerId, principal.EmployeeId);
                }
                else if (brokerDB.IsEnableMultiFactorAuthenticationTPO && principal.Type == "P")
                {
                    isRequiredMultiFactor = EmployeeDB.RetrieveEnabledMultiFactorAuthenticationByEmployeeId(principal.BrokerId, principal.EmployeeId);
                }

                if (isRequiredMultiFactor)
                {
                    string devMessage = "UserName: " + principal.LoginNm + " has Multi Factor Authentication enabled and cannot be authenticated.";

                    if (HttpContext.Current != null)
                    {
                        var clientCertificate = HttpContext.Current.Request.ClientCertificate;

                        if (ClientCertificateUtilities.IsValidClientCertificate(clientCertificate, principal))
                        {
                            return;
                        }
                        else
                        {
                            devMessage += " clientCertificate.IsPresent=" + clientCertificate.IsPresent + ", clientCertificate.IsValid=" + clientCertificate.IsValid;
                        }
                    }

                    Tools.LogInfo("EnforceForWebServices", devMessage);

                    throw new CBaseException("User has Multi Factor Authentication enabled and cannot be authenticated.", "User has Multi Factor Authentication enabled and cannot be authenticated.");
                }
            }
        }

        /// <summary>
        /// Enforces multifactor authentication for document capture vendors.
        /// </summary>
        public static void EnforceForDocumentCapture()
        {
            // ML 8/30 - For now, we are only validating KTA.
            var vendorId = ConstStage.KtaVendorId;
            if (vendorId == null)
            {
                throw new CBaseException(ErrorMessages.GenericAccessDenied, "KTA vendor ID is not defined in ConstStage.");
            }

            var context = HttpContext.Current;
            if (context != null)
            {
                var clientCertificate = HttpContext.Current.Request.ClientCertificate;
                if (ClientCertificateUtilities.IsValidVendorClientCertificate(clientCertificate, vendorId.Value))
                {
                    return;
                }

                string errorMessage = $"Unable to validate client certificate.{Environment.NewLine}";
                errorMessage += clientCertificate == null 
                    ? $"Certificate is null."
                    : $"IsPresent = {clientCertificate.IsPresent}, IsValid = {clientCertificate.IsValid}.";

                Tools.LogError(errorMessage);
            }

            throw new CBaseException(ErrorMessages.GenericAccessDenied, "Vendor failed client certificate check.");
        }

        /// <summary>
        /// OPM 125242.
        /// This method will check if the current user require to redirect to the second page for perform multi-factor authentication.
        /// Only call this method AFTER user is authenticate with username/password. This method expect the principal is set.
        /// If the method determine requires second authentication steps then it will clear out the principal.
        /// </summary>
        /// <returns>true if user required to perform second authentication.</returns>
        public static bool EnforceMultiFactorAuthentication()
        {
            AbstractUserPrincipal principal = System.Threading.Thread.CurrentPrincipal as AbstractUserPrincipal;

            if (principal == null)
            {
                throw CBaseException.GenericException("Principal cannot be null when calling IsRequireMultiFactorAuthentication.");
            }

            bool result = VerifyIpRestrictionAndMultiFactorAuthentication(principal);

            if (result)
            {
                Guid m_lenderPmlSiteID = Guid.Empty;
                HttpCookie siteIdCookie = HttpContext.Current.Request.Cookies["lenderPmlSiteID"];
                if (siteIdCookie != null)
                {
                    m_lenderPmlSiteID = new Guid(siteIdCookie.Value);
                }
                InsertAuditRecord(principal, m_lenderPmlSiteID);
            }
            else
            {
                GenerateMultiFactorAuthenticationCode(principal.BrokerId, principal.UserId);
                RedirectToNextTaskPage(HttpContext.Current, ConstApp.LendingQBAuthenticationCodePage);
            }

            return result;
        }

        public static void GenerateMultiFactorAuthenticationCode(Guid brokerId, Guid userId)
        {
            MultiFactorAuthCodeUtilities.GenerateAndSendNewCode(brokerId, userId, false);

            ClearAuthenticationCookies();

            string brokerIdUserId = brokerId + ":" + userId + ":" + Guid.NewGuid();

            string encryptBrokerIdUserId = EncryptionHelper.Encrypt(brokerIdUserId);
            StoreToCookie(ConstApp.CookieMultiFactorAuthentication, encryptBrokerIdUserId, false);
        }
        /// <summary>
        /// Insert successful login in audit record. Only call this after successful login.
        /// </summary>
        /// <param name="principal">The current principal.</param>
        public static void InsertAuditRecord(AbstractUserPrincipal principal, Guid brokerPmlSiteId)
        {
            // Add a tracking event to the database for each successful
            // login.  The event contains all the user's information
            // for quick lookup and sorting in the admin view.
            
            Guid brokerId = Guid.Empty;

            if (principal.BrokerId == Guid.Empty && principal.Type == "I")
            {
                // 8/21/2014 dd - For internal user, we will use the system broker id.
                brokerId = ConstAppDavid.SystemBrokerGuid;
            }
            else
            {
                brokerId = principal.BrokerId;
            }

            SecurityEventLogHelper.CreateLoginLog(principal);

            // Reset login failure.
            string userAgent = string.Empty;

            if (HttpContext.Current != null)
            {
                userAgent = HttpContext.Current.Request.ServerVariables.Get("HTTP_USER_AGENT");
            }
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@LoginNm", principal.LoginNm),
                new SqlParameter("@LoginBlockExpirationD", SmallDateTime.MinValue),
                new SqlParameter("@Type", principal.Type),
                new SqlParameter("@BrokerPmlSiteId", brokerPmlSiteId),
                new SqlParameter("@LastUserAgentString", userAgent)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "ResetLoginFailureCount", 1, parameters);

        }

        /// <summary>
        /// Perform IP Restriction check and check if user require to be redirect to multi-factor authentication page.
        /// </summary>
        /// <param name="principal"></param>
        /// <returns></returns>
        private static bool VerifyIpRestrictionAndMultiFactorAuthentication(AbstractUserPrincipal principal)
        {
            string currentIpAddress = ClientIP;
            bool? ipVerificationResult = VerifyIPRestriction(principal, currentIpAddress, false);
            if (ipVerificationResult.HasValue)
            {
                return ipVerificationResult.Value;
            }

            bool isRequiredMultiFactor = false;

            BrokerDB brokerDB = principal.BrokerDB;

            if (brokerDB.IsEnableMultiFactorAuthentication && principal.Type =="B")
            {                
                isRequiredMultiFactor = EmployeeDB.RetrieveEnabledMultiFactorAuthenticationByEmployeeId(principal.BrokerId, principal.EmployeeId);
            }
            else if(brokerDB.IsEnableMultiFactorAuthenticationTPO && principal.Type =="P")
            {
                isRequiredMultiFactor = EmployeeDB.RetrieveEnabledMultiFactorAuthenticationByEmployeeId(principal.BrokerId, principal.EmployeeId);
            }

            if (isRequiredMultiFactor == false)
            {
                return true;
            }

            VerifyClientCertificate(principal.BrokerId, principal.UserId, true);
            return false;
        }

        /// <summary>
        /// Verifies IP Restriction for user principal.
        /// </summary>
        /// <param name="principal">The user principal to be evaluated.</param>
        /// <param name="currentIpAddress">The current IP address of requesting machine.</param>
        /// <param name="isFromWebServices">If true, prevents redirection to Verify Client Certificate page.</param>
        /// <returns>Returns true if IP verification is successful, false if unsuccessful, null if ip restriction does not apply.</returns>
        private static bool? VerifyIPRestriction(AbstractUserPrincipal principal, string currentIpAddress, bool isFromWebServices)
        {
            if (principal.Type == "I")
            {
                return true; // Internal user does not have IP restriction nor multi-factor authentication.
            }

            IEnumerable<string> globalWhiteList = BrokerGlobalIpWhiteList.ListByBrokerId(principal.BrokerId).Select((entry) => entry.IpAddress);

            // For webservices, we want to also check against any ips we've whitelisted.
            // IPAddressUtilities.IsMatch checks against a range so this should be fine.
            if (isFromWebServices)
            {
                globalWhiteList = globalWhiteList.Concat(ConstStage.WebserviceWhitelistedIps);
            }

            foreach (var ipAddress in globalWhiteList)
            {
                if (IPAddressUtilities.IsMatch(ipAddress, currentIpAddress))
                {
                    // 5/8/2014 dd - Client IP is in one of lender white list. There is no need to perform further check.
                    return true;
                }
            }

            // Step 1 - Check to see if user has IP restriction turn on.
            bool enabledIpRestriction = false;
            List<string> userLevelIpAllowList = new List<string>();
            SqlParameter[] parameters =
            {
                new SqlParameter("@UserId", principal.UserId)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "RetrieveUserIpRestrictionMultiFactorAuthenticationSettings", parameters))
            {
                if (reader.Read())
                {
                    enabledIpRestriction = (bool)reader["EnabledIpRestriction"];
                    string mustLoginFromTheseIpAddresses = (string)reader["MustLoginFromTheseIpAddresses"];

                    if (string.IsNullOrEmpty(mustLoginFromTheseIpAddresses) == false)
                    {
                        userLevelIpAllowList = new List<string>(mustLoginFromTheseIpAddresses.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
                    }
                }
                else
                {
                    throw CBaseException.GenericException("UserId=[" + principal.UserId + "] is not valid");
                }
            }

            if (enabledIpRestriction)
            {
                if (userLevelIpAllowList.Count > 0 && IPAddressUtilities.IsMatch(userLevelIpAllowList, currentIpAddress) == true)
                {
                    // 5/8/2014 dd - Client IP is on the allowable list.
                    return true;
                }
                else if (isFromWebServices) //Web Services to execute this code path
                {
                    return false;
                }
                else
                {
                    VerifyClientCertificate(principal.BrokerId, principal.UserId, false);

                    string devMessage = "Access failed due to IP restriction. Current IP :" + currentIpAddress + " UserId:" + principal.UserId + " Username :[" + principal.LoginNm + "]"; //doing this to make sure its working

                    ClearAuthenticationCookies();
                    DisplayErrorPage(new CBaseException(ErrorMessages.IPRestrictionAccessDenied, devMessage), false);
                    return false;
                }
            }
            else
            {
                // IP addresses aren't restricted but we still want to check if the IP address was registered in the past 30 days.
                // If not registered, then let this go for further checking.
                var knownIpList = UserRegisteredIp.ListByUserId(principal.BrokerId, principal.UserId);

                foreach (var ip in knownIpList)
                {
                    if (ip.IsRegistered && ip.CreatedDate.AddDays(30) > DateTime.Today)
                    {
                        if (ip.IpAddress == currentIpAddress)
                        {
                            // 4/28/2014 dd - This ip address already registered.
                            // Don't need to enforce multi-factor authentication.
                            return true;
                        }
                    }
                }
            }

            if (principal.Type != "B" && principal.Type != "P")
            {
                // 4/27/2014 dd - Only perform multi-factor authentication on "B" and "P" user.
                return true;
            }

            return null;
        }

        private static void VerifyClientCertificate(Guid brokerId, Guid userId, bool redirectToAuthenticationCodePageOnInvalid)
        {
            // If there are registered user level certificates for this user or if there are any broker level certificates, then we have to verify the certificate in the request.
            if (ClientCertificate.HasClientCertificate(brokerId, userId) ||
                LenderClientCertificate.GetLenderClientCertificatesByBrokerId(brokerId, false).Any())
            {
                string str = string.Format("{0}|{1}|{2}", brokerId, userId, redirectToAuthenticationCodePageOnInvalid ? "1" : "0");
                Guid g = Guid.NewGuid();
                AutoExpiredTextCache.AddToCache(str, TimeSpan.FromMinutes(5), RequestHelper.GetMFACacheKey(g));
                RequestHelper.RedirectToNextTaskPage(HttpContext.Current, "~/VerifyClientCertificate.aspx?id=" + Tools.ShortenGuid(g));
            }
        }

        private static string GetMFACacheKey(Guid g)
        {
            return "mfa_" + g;

        }
        public static string GetMFACacheKey(string shortenGuid)
        {
            Guid g = Tools.GetGuidFrom(shortenGuid);
            return RequestHelper.GetMFACacheKey(g);
        }
    }


    public interface IQueryArgs
    {
        NameValueCollection QueryString {get; }
        Guid GetGuid(string key);
        Guid GetGuid(string key, Guid defaultValue);
        bool GetBool(string key);
        int GetInt(string key);
        int GetInt(string key, int defaultValue);
        Guid LoanID { get;}

        // TODO : need refactor because not make sense to put DisplayErrorPage(...) at here,
        void DisplayErrorPage(Exception exc, bool isSendEmail, Guid brokerID, Guid employeeID);
        void DisplayErrorPage(CBaseException exc, bool isSendEmail, Guid brokerID, Guid employeeID);
    }

    public class RequestHelperForWeb : IQueryArgs
    {
        public static RequestHelperForWeb Instance = new RequestHelperForWeb();

        #region Implementation of IRequestHelper
        public NameValueCollection QueryString
        {
            get { return HttpContext.Current.Request.QueryString; }
        }

        public System.Guid GetGuid(string key)
        {
            return RequestHelper.GetGuid(key);
        }
        public System.Guid GetGuid(string key, System.Guid defaultValue)
        {
            return RequestHelper.GetGuid(key, defaultValue);
        }
        public bool GetBool(string key)
        {
            return RequestHelper.GetBool(key);
        }
        public int GetInt(string key)
        {
            return RequestHelper.GetInt(key);
        }
        public int GetInt(string key, int defaultValue)
        {
            return RequestHelper.GetInt(key, defaultValue);
        }
        public System.Guid LoanID
        {
            get
            {
                return RequestHelper.LoanID;
            }
        }

        public void DisplayErrorPage(Exception exc, bool isSendEmail, Guid brokerID, Guid employeeID)
        {
            ErrorUtilities.DisplayErrorPage(exc, isSendEmail, brokerID, employeeID);
        }

        public  void DisplayErrorPage(CBaseException exc, bool isSendEmail, Guid brokerID, Guid employeeID)
        {
            ErrorUtilities.DisplayErrorPage(exc, isSendEmail, brokerID, employeeID);
        }

        #endregion
    }



}
