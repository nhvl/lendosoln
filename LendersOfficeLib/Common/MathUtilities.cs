﻿//-----------------------------------------------------------------------
// <summary>
//      Utilities class for mathematical static methods.
// </summary>
// <copyright file="MathUtilities.cs" company="MeridianLink">
//      Copyright (c) MeridianLink. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
namespace LendersOffice.Common
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Utilities class for mathematical static methods.
    /// </summary>
    public static class MathUtilities
    {
        /// <summary>
        /// If x is less than or equal to 0, returns false. If 1, 2, 4, 8, ..., 2048, ... return true.
        /// Uses bit operations.
        /// </summary>
        /// <param name="x">The number to be checked.</param>
        /// <returns>True or false depending on if x is a power of 2.</returns>
        public static bool IsPowerOfTwo(int x)
        {
            // see http://stackoverflow.com/questions/600293/how-to-check-if-a-number-is-a-power-of-2
            if (x <= 0)
            {
                return false;
            }
            else
            {
                return (x & (x - 1)) == 0;
            }
        }

        /// <summary>
        /// Tells if the value is between min and max, including both min and max.
        /// May throw a null exception.
        /// </summary>
        /// <typeparam name="T">T must have a CompareTo method.</typeparam>
        /// <param name="value">The value to test.</param>
        /// <param name="min">The minimum of the range.</param>
        /// <param name="max">The maximum of the range.</param>
        /// <returns>True if and only if the value is inclusively between the min and the max.</returns>
        public static bool IsInclusivelyBetween<T>(T value, T min, T max) where T : IComparable<T>
        {
            // http://stackoverflow.com/a/13470099/420667 , as well as Mark Sowul's comment.
            return (min.CompareTo(value) <= 0) && (value.CompareTo(max) <= 0);
        }
    }
}
