﻿namespace LendersOffice.Common
{
    using System;
    using System.Collections.Concurrent;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Admin;
    using Security;

    /// <summary>
    /// Manage "all" lender's call volume. By design, only the Loan/EDocsService web methods use this class.
    /// The CallVolume's main purpose: detect what lender abusing our system. Therefore we don't need to get absolutely accurate count. 
    /// For example, we will not update #pending calls to db at application shutdown time ...
    ///     Put this class into LenderOfficeLib project for running UnitTests in future (Otherwise we will put it in LendersOfficeApp project).
    /// </summary>
    public static class CallVolume
    {
        /// <summary>
        /// LenderCallVolume collection.
        /// </summary>
        private static ConcurrentDictionary<Guid, LenderCallVolume> lenderVolumeDict = new ConcurrentDictionary<Guid, LenderCallVolume>();

        /// <summary>
        /// Increase call volume for particular lender.
        /// </summary>
        /// <param name="brokerId">Broker Id.</param>
        /// <returns>Call volume after increasing.</returns>
        public static int Increase(Guid brokerId)
        {
            LenderCallVolume lenderVolume = lenderVolumeDict.GetOrAdd(brokerId, keyStr => new LenderCallVolume(brokerId));
            return lenderVolume.Increment();
        }

        /// <summary>
        /// Increase call volume and compare it with lender call volume threshold.
        /// </summary>
        /// <param name="principal">The principal of the requesting lender.</param>
        public static void IncreaseAndCheck(AbstractUserPrincipal principal)
        {
            int numCall = CallVolume.Increase(principal.BrokerId);
            BrokerDB brokerDb = principal.BrokerDB;
            if (numCall > brokerDb.MaxWebServiceDailyCallVolume)
            {
                string errorMessage = string.Format($"Today the broker {brokerDb.Name} ({brokerDb.BrokerID}) made {numCall.ToString("N0")} web service calls (> {brokerDb.MaxWebServiceDailyCallVolume.ToString("N0")}). This broker is now blocked from using web services for the day.");

                if ((numCall - brokerDb.MaxWebServiceDailyCallVolume) % (50 * 1000) == 1)
                {
                    if (brokerDb.SuiteType == BrokerSuiteType.VendorTest)
                    {
                        string subject = "[BrokerBlock] The broker " + brokerDb.Name + " is blocked from using web services for the day.";
                        EmailUtilities.SendToIntegrationSpecialist(subject, errorMessage);
                    }
                }

                throw new CBaseException("WebService invoked too many times.", errorMessage);
            }
        }

        /// <summary>
        /// This class contains today call volume for a lender.
        /// </summary>
        private class LenderCallVolume
        {
            /// <summary>
            /// The pendingCount will write to database if pendingCount age > this number.
            /// </summary>
            private const int AllowableCachePeriodInMinutes = 5;

            /// <summary>
            /// The pendingCount will write to database if pendingCount > this number.
            /// </summary>
            private const int MaxPendingCount = 100;

            /// <summary>
            ///  Num call that retrieved from database.
            /// </summary>
            private int lastCountFromDatabase;

            /// <summary>
            /// Number of pending call.
            /// </summary>
            private int pendingCount; // number of call that not record to database.

            /// <summary>
            /// The time stamp for last read from database.
            /// </summary>
            private DateTime lastReadFromDatabase = DateTime.MinValue;

            /// <summary>
            /// Lock object to make thread-safe.
            /// </summary>
            private object lockObject = new object();

            /// <summary>
            /// Initializes a new instance of the <see cref="LenderCallVolume" /> class.
            /// </summary>
            /// <param name="brokerId">Broker Id.</param>
            public LenderCallVolume(Guid brokerId)
            {
                this.BrokerId = brokerId;
            }

            /// <summary>
            /// Gets/sets Broker Id.
            /// </summary>
            public Guid BrokerId { get; private set; }

            /// <summary>
            /// Increase call volume.
            /// </summary>
            /// <param name="numCall">Number of call will be added to call volume.</param>
            /// <returns>Call Volume after increasing.</returns>
            public int Increment(int numCall = 1)
            {
                // dd - This will block per broker. And database hit is only every 5 minutes.
                lock (this.lockObject)
                {
                    this.pendingCount += numCall;

                    if (this.lastReadFromDatabase.AddMinutes(AllowableCachePeriodInMinutes) <= DateTime.Now || this.pendingCount > MaxPendingCount)
                    {
                        // Cache expired. Refresh number from database.
                        SqlParameter[] parameters =
                        {
                            new SqlParameter("@BrokerId", this.BrokerId),
                            new SqlParameter("@PendingCount", this.pendingCount)
                        };

                        using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, "BROKER_DAILY_WEBSERVICE_CALL_VOLUME_UpdateAndGetTodayCount", parameters))
                        {
                            if (reader.Read())
                            {
                                this.lastCountFromDatabase = (int)reader["NumberOfCalls"];
                                this.pendingCount = 0; // Reset
                            }
                        }                        

                        this.lastReadFromDatabase = DateTime.Now;
                    }

                    return this.lastCountFromDatabase + this.pendingCount;
                }
            }
        }
    }
}
