using System;
using System.Collections;
using System.Web;
using DataAccess;
using System.Xml;
using System.Xml.Serialization;

namespace LendersOffice.Common
{
	public class ErrorUtilities
	{
		private const string ErrorPageUrl = "~/common/AppError.aspx";


        public static void DisplayErrorPage(string errorMsg, bool isSendEmail, Guid brokerID, Guid employeeID)
        {
            CBaseException exc = new CBaseException(ErrorMessages.Generic, errorMsg);
            DisplayErrorPage(exc, isSendEmail, brokerID, employeeID);
        }
		public static void DisplayErrorPage(Exception exc, bool isSendEmail, Guid brokerID, Guid employeeID) 
		{
            if (exc is HttpRequestValidationException)
            {
                HttpContext context = HttpContext.Current;

                if (null != context)
                {
                    Tools.LogError(exc);
                    context.Server.ClearError(); // 12/17/2008 dd - Clear out exception so it will not propagate up to worker process on Server 2003 IIS6.
                    context.Response.Redirect("~/common/RequestError.aspx?url=" + context.Server.UrlEncode(context.Request.Url.AbsoluteUri), true);
                    return;
                }
            }

			if (exc is System.Web.HttpUnhandledException) 
			{
				Exception innerException = exc.InnerException;
				if (innerException is CBaseException)
					DisplayErrorPage((CBaseException) innerException, isSendEmail, brokerID, employeeID);
				else
					DisplayErrorPage(innerException, isSendEmail, brokerID, employeeID);
			} 
			else if (exc is CBaseException)
			{
				DisplayErrorPage((CBaseException) exc, isSendEmail, brokerID, employeeID);
			}
            else if (exc is HttpException)
            {
                HttpException httpException = exc as HttpException;
                bool isEmailDeveloper = true;
                if (httpException.Message.StartsWith("The remote host closed the connection.") ||
                    httpException.Message.StartsWith("The client disconnected.") ||
                    httpException.Message.StartsWith("Maximum request length exceeded.")) {
                    isEmailDeveloper = false;
                }
                CBaseException baseException = new GenericUserErrorMessageException(exc);
                baseException.IsEmailDeveloper = isEmailDeveloper;
                DisplayErrorPage(baseException, isEmailDeveloper, brokerID, employeeID);
            }
            else
            {
                CBaseException baseException = new GenericUserErrorMessageException(exc);

                DisplayErrorPage(baseException, isSendEmail, brokerID, employeeID);
            }
		}
		public static void DisplayErrorPage(CBaseException exc, bool isSendEmail, Guid brokerID, Guid employeeID) 
		{
            string id = string.Empty;

            if (exc is LendersOffice.Security.DuplicationLoginException)
            {
                id = "DUP_LOGIN";
            }
            else
            {
                id = StoreExceptionToCache(exc);
                LogPBAndSendEmail(exc, isSendEmail);
            }

			HttpContext context = HttpContext.Current;
            
			if (null != context)
			{
                context.Server.ClearError(); // 12/17/2008 dd - Clear out exception so it will not propagate up to worker process on Server 2003 IIS6.
				context.Response.Redirect(ErrorPageUrl + "?id=" + id + "&brokerid=" + brokerID + "&employeeid=" + employeeID);
			}

		}

		private static void LogPBAndSendEmail(CBaseException exc, bool isSendEmail) 
		{
			if (exc is LendersOffice.Security.DuplicationLoginException) 
			{
				Tools.LogRegTest(exc.ToString());
				return;
			}

			if (exc is PageDataAccessDenied) 
			{
				Tools.LogError("Error Reference Number: " + exc.ErrorReferenceNumber, exc);

				return; // 6/17/2005 dd - OPM #2069 - If exception is PageDataAccessDenied then don't send email to us.
			}

            if (isSendEmail)
            {
                Tools.LogErrorWithCriticalTracking(exc);
            }
            else
            {
                Tools.LogError(exc);
            }
		}

		/// <summary>
		/// Store exception to server cache so AppError.aspx can retrieve by id.
		/// Returns id pass to AppError.aspx
		/// </summary>
		/// <param name="exc"></param>
		/// <returns></returns>
		public static string StoreExceptionToCache(CBaseException exc) 
		{
			if (null == exc) 
			{
				Tools.LogBug("exc is null in ErrorUtilities.StoreExceptionToCache.");
				return "";
			}

			// 05/30/06 mf - Per OPM 2582, we no longer use server cache.
			// We can no longer cache the entire exception object because 
			// some properties are decorated with declarative security
			// permission attributes.  
			// We only cache the data AppError needs.  For this we use an
			// ExceptionInfo object that AppError can deserialize.

			ExceptionInfo eI = new ExceptionInfo(exc);

			string xml = SerializationHelper.XmlSerialize(eI);
			//av 1-30-08 opm 19618 Changed 5 to 60 in timeout 
			AutoExpiredTextCache.AddToCache(xml, TimeSpan.FromMinutes(60), "ERR_" + exc.ErrorReferenceNumber);

			return exc.ErrorReferenceNumber;
		}

		/// <summary>
		/// Expires the exception in the cache so its deleted next time the cleaner runs. 
		/// av 1-30-08 
		/// </summary>
		public static void ExpireException(string id)
		{
			try 
			{	
				AutoExpiredTextCache.ExpireCache( "ERR_"+ id );
			}
			catch (Exception e)
			{
				Tools.LogWarning(String.Format("Could not expire exception {0} in cache. It may have been removed already.", "ERR_"+id), e);
			}	
		}

		public static ExceptionInfo RetrieveExceptionFromCache(string id) 
		{

			string xml = AutoExpiredTextCache.GetFromCache("ERR_" + id);
			if (xml != null)
			{
                try 
                {
                    return (ExceptionInfo) SerializationHelper.XmlDeserialize(xml, typeof(ExceptionInfo));
                } 
                catch (Exception exc) 
                {
                    Tools.LogError("Unable to RetrieveExceptionFromCache. Exception=" + exc);
                }
			}

			return null;
		}
	}
	
	// A light class that we can serialize and cache
	// for AppError.aspx to deserialize.

	[XmlRoot("ExceptionInfo")]
	public class ExceptionInfo
	{
		private string m_ExceptionType;
		private string m_UserMessage;
		private string m_DeveloperMessage;
		private string m_ErrorReferenceNumber;
		
		[XmlAttribute]
		public string ExceptionType
		{
			get { return m_ExceptionType; }
			set { m_ExceptionType = value; }
		}

		[XmlAttribute]
		public string UserMessage
		{
			get { return m_UserMessage; }
			set { m_UserMessage = value; }
		}

		[XmlAttribute]
		public string DeveloperMessage
		{
			get { return m_DeveloperMessage; }
			set { m_DeveloperMessage = value; }
		}
		
		[XmlAttribute]
		public string ErrorReferenceNumber
		{
			get { return m_ErrorReferenceNumber; }
			set { m_ErrorReferenceNumber = value; }
		}

        [XmlAttribute]
        public string OtherExceptionDetails
        {
            get;
            set; 
        }

		// Default Public constructor needed for deserialization
		public ExceptionInfo() : this(new GenericUserErrorMessageException("Generic Exception"))
		{
		}

		public ExceptionInfo(CBaseException exc)
		{
			m_UserMessage = exc.UserMessage;
			m_DeveloperMessage = exc.DeveloperMessage;
			m_ErrorReferenceNumber = exc.ErrorReferenceNumber;
			m_ExceptionType = exc.GetType().ToString();
            OtherExceptionDetails = "[EXCEPTION_CALL_STACK] :" + exc.StackTrace;
		}
		
	}

}
