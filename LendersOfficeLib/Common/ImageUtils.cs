﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using DataAccess;
using LqbGrammar.DataTypes;

namespace LendersOffice.Common
{
    public class ImageUtils
    {

        public class ImageDetails 
        {
            public int Width { get; set; }
            public int Height { get;  set; }

            public double AspectRatio
            {
                get
                {
                        return (double)Width / (double)Height;
                }
            }
        }

        public static bool IsValidImageType(string mimeType)
        {
            if (string.IsNullOrEmpty(mimeType))
                return false;

            switch (mimeType)
            {
                case "image/pjpeg":
                case "image/jpeg":
                case "image/png":
                case "image/x-png":
                case "image/gif":
                case "image/bmp":
                    return true;
                default:
                    return false;
            }
        }
        public static System.Drawing.Image ResizeImage(System.IO.Stream upFileStream, int Width, int Height)
        {
            System.Drawing.Image imgPhoto = new Bitmap(upFileStream);

            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width / (float)sourceWidth);
            nPercentH = ((float)Height / (float)sourceHeight);
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = System.Convert.ToInt16((Width - (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = System.Convert.ToInt16((Height - (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = null;
            Graphics grPhoto = null;

            try
            {
                bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
                bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);
                grPhoto = Graphics.FromImage(bmPhoto);

                grPhoto.Clear(Color.White);
                grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

                grPhoto.DrawImage(imgPhoto
                                    , new Rectangle(destX, destY, destWidth, destHeight)
                                    , new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight)
                                    , GraphicsUnit.Pixel);
            }
            catch { }
            finally
            {
                imgPhoto.Dispose();
                grPhoto.Dispose();
            }

            return bmPhoto;
        }

        public static bool SaveImageToFileDB(string key, System.Drawing.Image img)
        {
            return SaveImageToFileDB(key, img, ImageFormat.Png); //have Png as the default format 
        }

        public static bool SaveImageToFileDB(string key, System.Drawing.Image img, ImageFormat format)
        {
            LocalFilePath localFilePath = TempFileUtils.NewTempFile();

            ImageCodecInfo encoder = GetImageEncoder(format);
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
            EncoderParameters parameters = new EncoderParameters(1);
            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 95L);
            parameters.Param[0] = myEncoderParameter;

            img.Save(localFilePath.Value, encoder, parameters);
            FileDBTools.WriteFile(E_FileDB.Normal, key, localFilePath.Value);

            return true;
        }

        public static ImageCodecInfo GetImageEncoder(ImageFormat format)
        {

            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        public static ImageDetails GetImageDetails(string key)
        {
            ImageDetails details = null;
           Action<Stream> getImageInfo = delegate(Stream stream)
            {
                using( Bitmap image = new Bitmap(stream))
                {
                    details = new ImageDetails() {
                        Height = image.Height,
                        Width = image.Width

                    };
                }
            };

           FileDBTools.ReadData(E_FileDB.Normal,key, getImageInfo);

           if (details == null)
           {
               throw CBaseException.GenericException("Could not load image details.");
           }
           return details;
        }
    }
}
