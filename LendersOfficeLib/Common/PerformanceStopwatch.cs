﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Web;
using DataAccess;
using LendersOffice.HttpModule;
using LendersOffice.Constants;

namespace LendersOffice.Common
{
    public class PerformanceStopwatch : IDisposable
    {
        private PerformanceStopwatch m_parent = null;
        private List<PerformanceStopwatch> m_childrenList = new List<PerformanceStopwatch>();
        private Stopwatch m_stopwatch = null;
        private int m_count = 0;
        private string m_description;

        private long m_total
        {
            get
            {
                if (m_stopwatch == null)
                    return 0;
                else
                    return m_stopwatch.ElapsedMilliseconds;
            }
        }
        private PerformanceStopwatch(string description)
        {
            m_description = description;
            m_stopwatch = new Stopwatch();
        }
        public void Dispose()
        {
            StopImpl();
        }
        private void StartImpl()
        {
            m_stopwatch.Start();
            CurrentNode = this;
            m_count++;
        }
        private void StopImpl()
        {
            m_stopwatch.Stop();
            CurrentNode = this.m_parent;

            if (ConstStage.EnablePerformanceStopwatchLogging)
            {
                PerformanceMonitorItem monitorItem = PerformanceMonitorItem.Current;
                if (null != monitorItem)
                {
                    monitorItem.AddTimingDetail(this.m_description, m_stopwatch.ElapsedMilliseconds);
                }
            }
        }
        private void AddChild(PerformanceStopwatch o)
        {
            o.m_parent = this;
            m_childrenList.Add(o);
        }
        private void PrintItem(StringBuilder sb, int level)
        {
            
            sb.AppendLine(FormatPrint(level, m_total, m_parent == null ? 0 : m_parent.m_total, m_description, m_count));

            long childrenTotal = 0L;
            foreach (var o in m_childrenList) 
            {
                o.PrintItem(sb, level + 1);
                childrenTotal += o.m_total;
            }
            if (childrenTotal > 0 && m_total > 0) 
            {
                sb.AppendLine(FormatPrint(level + 1, (m_total - childrenTotal), m_total, "[OTHER]", 0));
            }
        }
        private string FormatPrint(int level, long current, long total, string description, int count)
        {
            double percent = 0;
            if (total > 0)
            {
                percent = ((double)current / (double)total) * 100.0;
            }
            return string.Format("{0} - {1} ({2} ms. {3}%). Hit Count: {4}", "".PadLeft(level * 4), description, current, percent.ToString("0.##"), count);
        }
        private static PerformanceStopwatch RootNode
        {
            get
            {
                if (x_rootNode == null)
                {
                    x_rootNode = new PerformanceStopwatch("<ROOT>");
                }
                return x_rootNode;
            }
        }

        private static PerformanceStopwatch CurrentNode
        {
            get
            {
                if (x_currentNode == null)
                {
                    x_currentNode = RootNode;
                }
                return x_currentNode;
            }
            set
            {
                x_currentNode = value;
            }
        }

        [ThreadStatic]
        private static PerformanceStopwatch x_rootNode = null;

        [ThreadStatic]
        private static PerformanceStopwatch x_currentNode = null;

        public static void ResetAll()
        {
            x_rootNode = null;
            x_currentNode = null;
        }

        public static void TimeAndRun(string name, Action action)
        {
            using (PerformanceStopwatch.Start(name))
            {
                action(); 
            }
        }

        public static IDisposable Start(string description)
        {
            PerformanceStopwatch currentNode = CurrentNode;
            PerformanceStopwatch node = null;

            foreach (var o in currentNode.m_childrenList)
            {
                if (o.m_description == description)
                {
                    node = o;
                    break;
                }
            }
            if (null == node)
            {
                node = new PerformanceStopwatch(description);
                currentNode.AddChild(node);
            }
            node.StartImpl();
            return node;
        }
        public static string ReportOutput
        {
            get
            {
                string result = string.Empty;
                if (RootNode.m_childrenList.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    RootNode.PrintItem(sb, 0);
                    result = sb.ToString();
                }
                return result;
            }
        }

        public static string FlatListReport
        {
            get
            {
                Dictionary<string, long> dictionary = new Dictionary<string, long>(StringComparer.OrdinalIgnoreCase);
                FlatListReportImpl(dictionary, RootNode);

                StringBuilder sb = new StringBuilder();
                foreach (var item in dictionary.OrderBy(o => o.Value))
                {
                    sb.AppendLine(item.Key + " - " + item.Value);
                }
                return sb.ToString();
            }
        }
        private static void FlatListReportImpl(Dictionary<string, long> dictionary, PerformanceStopwatch parent)
        {
            foreach (var child in parent.m_childrenList)
            {
                long total;
                if (dictionary.TryGetValue(child.m_description, out total) == false)
                {
                    total = 0L;
                }
                total += child.m_total;
                dictionary[child.m_description] = total;

                if (child.m_childrenList.Count > 0)
                {
                    FlatListReportImpl(dictionary, child);
                }
            }
        }
    }
}