/// Author: David Dao

using System;
using System.Collections;
using System.Collections.Generic;

namespace LendersOffice.Common
{
    public class CacheExpirableItem 
    {
        private DateTime m_expiredDate = DateTime.MaxValue;
        private object m_item = null;

        public CacheExpirableItem(object item, DateTime expiredDate) 
        {
            m_expiredDate = expiredDate;
            m_item = item;
        }

        public bool IsExpired 
        {
            get 
            { 
                bool isExpired = m_expiredDate < DateTime.Now;
                if (isExpired)
                    m_item = null;

                return isExpired;
            }
        }

        public object Item 
        {
            get { return m_item; }
        }
    }

    /// <summary>
    /// Class is thread safe
    /// </summary>
	public class LOCache
	{
        private static int s_currentAccessCount = 0;
        private static object s_lock = new object();
        private static Dictionary<string, CacheExpirableItem> s_hash = new Dictionary<string, CacheExpirableItem>(20);
        /// <summary>
        /// Insert or update item in cache with expire time.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expiredDate"></param>
        public static void Set(string key, object value, DateTime expiredDate) 
        {
            lock (s_lock) 
            {
                s_hash[key] = new CacheExpirableItem(value, expiredDate);
            }
        }

        public static object Get(string key) 
        {
            lock (s_lock) 
            {
                s_currentAccessCount++;

                if (s_currentAccessCount % 1000 == 0)
                {
                    // 10/28/2009 dd - Purge expire item every 1000 count.
                    PurgeExpired();
                    s_currentAccessCount = 0;
                }
                CacheExpirableItem ret;

                if (s_hash.TryGetValue(key, out ret))
                {
                    if (!ret.IsExpired)
                    {
                        return ret.Item;
                    }
                    else
                    {
                        s_hash.Remove(key);
                    }
                }
                return null;
            }
        }
        private static void PurgeExpired()
        {
            List<string> expireList = new List<string>();
            foreach (var o in s_hash)
            {
                if (o.Value.IsExpired)
                {
                    expireList.Add(o.Key);
                }
            }
            foreach (var o in expireList)
            {
                s_hash.Remove(o);
            }
        }
	}
}
