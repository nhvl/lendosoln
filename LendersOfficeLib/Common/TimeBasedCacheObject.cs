﻿// <copyright file="TimeBasedCacheObject.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   7/8/2014 11:47:13 AM 
// </summary>
namespace LendersOffice.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// An object that will cache itself for certain time interval. When access value after time is expired then it will reload from source.
    /// </summary>
    /// <typeparam name="T">The underlying value type of TimeBasedCacheObject generic type.</typeparam>
    public class TimeBasedCacheObject<T>
    {
        /// <summary>
        /// Function pointer for loading object.
        /// </summary>
        private Func<T> getValue;

        /// <summary>
        /// Time interval to cache the object.
        /// </summary>
        private TimeSpan timeoutInterval;

        /// <summary>
        /// Lock object to make thread-safe.
        /// </summary>
        private object lockObject = new object();

        /// <summary>
        /// Last loaded time.
        /// </summary>
        private DateTime lastLoaded = DateTime.MinValue;

        /// <summary>
        /// Cache value.
        /// </summary>
        private T cacheValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeBasedCacheObject{T}" /> class.
        /// </summary>
        /// <param name="timeoutInterval">Time interval to cache the object.</param>
        /// <param name="getValue">Function to load the value.</param>
        public TimeBasedCacheObject(TimeSpan timeoutInterval, Func<T> getValue)
        {
            this.getValue = getValue;
            this.timeoutInterval = timeoutInterval;
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>Value of the object.</value>
        public T Value
        {
            get
            {
                lock (this.lockObject)
                {
                    if (this.lastLoaded.Add(this.timeoutInterval) < DateTime.Now)
                    {
                        this.cacheValue = this.getValue();
                        this.lastLoaded = DateTime.Now;
                    }

                    return this.cacheValue;
                }
            }
        }
    }
}