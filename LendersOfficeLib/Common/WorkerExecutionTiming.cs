﻿// <copyright file="WorkerExecutionTiming.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is WorkerExecutionTiming class.
// Author: David
// Date: 5/31/2015
// </summary>
namespace LendersOffice.Common
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Messaging;
    using System.Text;
    using DataAccess;
    using LendersOffice.Constants;
    using Newtonsoft.Json;

    /// <summary>
    /// Log execution time for executing a task.
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class WorkerExecutionTiming : IDisposable
    {
        /// <summary>
        /// Constant string.
        /// </summary>
        public const string OKStatus = "OK";

        /// <summary>
        /// Constant string.
        /// </summary>
        public const string ErrorStatus = "Error";

        /// <summary>
        /// Constant string.
        /// </summary>
        public const string NoWorkStatus = "NoWork";

        /// <summary>
        /// Unique task name.
        /// </summary>
        [JsonProperty("name")]
        private string name;

        /// <summary>
        /// Time task start.
        /// </summary>
        [JsonProperty("timestamp")]
        private DateTime startTimeUtc;

        /// <summary>
        /// Time task end.
        /// </summary>
        [JsonProperty("end")]
        private DateTime endTimeUtc;

        /// <summary>
        /// Total execution time in milliseconds.
        /// </summary>
        [JsonProperty("ms")]
        private long executionTimeInMs;

        /// <summary>
        /// Server name.
        /// </summary>
        [JsonProperty("server")]
        private string serverName;

        /// <summary>
        /// Overall status of the task.
        /// </summary>
        [JsonProperty("status")]
        private string status;

        /// <summary>
        /// Number of successful items process in this run.
        /// </summary>
        [JsonProperty("num_success")]
        private int numberOfSuccessfulItems;

        /// <summary>
        /// Number of failed items process in this run.
        /// </summary>
        [JsonProperty("num_failed")]
        private int numberOfFailedItems;

        /// <summary>
        /// Total number of items process in this run.
        /// </summary>
        [JsonProperty("total_items")]
        private int totalItems;

        /// <summary>
        /// Detail list of items process in this run.
        /// </summary>
        [JsonProperty("items")]
        private List<WorkerItemExecutionTiming> itemList = new List<WorkerItemExecutionTiming>();

        /// <summary>
        /// Stop watch.
        /// </summary>
        private Stopwatch globalStopwatch;

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkerExecutionTiming" /> class.
        /// </summary>
        /// <param name="name">Name of the task.</param>
        public WorkerExecutionTiming(string name)
        {
            this.name = name;
            this.startTimeUtc = DateTime.UtcNow;
            this.serverName = Environment.MachineName;

            this.globalStopwatch = Stopwatch.StartNew();
        }

        /// <summary>
        /// Create and add a new timer to record when process a new item.
        /// </summary>
        /// <param name="desc">Description of the item.</param>
        /// <param name="insertedInQueueTime">The time that the item insert in queue.</param>
        /// <returns>A timer for the item.</returns>
        public WorkerItemExecutionTiming RecordItem(string desc, DateTime insertedInQueueTime)
        {
            WorkerItemExecutionTiming item = new WorkerItemExecutionTiming(desc, insertedInQueueTime);
            this.itemList.Add(item);
            return item;
        }

        /// <summary>
        /// Stop the timer and then send to logging service.
        /// </summary>
        public void Dispose()
        {
            this.endTimeUtc = DateTime.UtcNow;
            this.globalStopwatch.Stop();
            this.executionTimeInMs = this.globalStopwatch.ElapsedMilliseconds;

            if (this.itemList.Count == 0)
            {
                this.status = NoWorkStatus;
            }
            else
            {
                foreach (var o in this.itemList)
                {
                    if (o.Status == WorkerItemExecutionTiming.OKStatus)
                    {
                        this.numberOfSuccessfulItems++;
                    }
                    else if (o.Status == WorkerItemExecutionTiming.ErrorStatus)
                    {
                        this.numberOfFailedItems++;
                    }
                }

                this.status = this.numberOfFailedItems > 0 ? ErrorStatus : OKStatus;
            }

            this.totalItems = this.itemList.Count;

            // 5/31/2015 dd - Do not modify the code bellow unless you know the architecture of ElasticSearch.
            // 7/30/2015 dd - We are not interest in run that result in NoWork
            if (this.totalItems > 0)
            {
                if (string.IsNullOrEmpty(ConstStage.MSMQ_ElasticSearch) == false)
                {
                    string id = Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Replace("/", "-").Replace("+", "_").Replace("==", string.Empty);

                    string elasticSearchIndexLabel = string.Format("lqb_worker_log-{0}/worker/{1}", this.startTimeUtc.Year + "." + (this.startTimeUtc.DayOfYear / 7), id);

                    try
                    {
                        Drivers.Gateways.MessageQueueHelper.SendJSON(ConstStage.MSMQ_ElasticSearch, elasticSearchIndexLabel, this);
                    }
                    catch (MessageQueueException exc)
                    {
                        Tools.LogErrorWithCriticalTracking(exc);

                        // 5/31/2015 dd - Continue to process.
                    }
                }
            }
        }
    }
}
