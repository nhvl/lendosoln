﻿// <copyright file="MessageQueueUtilities.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>This is MessageQueueUtilities class.
//    Author: David Dao.
//    Date: 6/19/2015
// </summary>
namespace LendersOffice.Common
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using DataAccess;

    /// <summary>A set of helper method for sending to message queue.</summary>
    [Obsolete("Use LendersOffice.Drivers.Gateways.MessageQueueHelper instead.")]
    public static class MessageQueueUtilities
    {
    }
}
