﻿namespace LendersOffice.Common
{
    using Constants;
    using Newtonsoft.Json.Serialization;
    using System.IO;
    using System.Runtime.Serialization.Json;
    using System.Web.Script.Serialization;

    /// <summary>
    /// Contains sets of methods that should no longer be used as these methods are using obsolete technologies.
    /// </summary>
    public static class ObsoleteSerializationHelper
    {
        /// <summary>
        /// Do not use this!
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string JsonSerialize<T>(T obj) where T : class
        {
            if (obj == null)
                return string.Empty;

            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            string ret = string.Empty;
            using (MemoryStream stream = new MemoryStream())
            {
                serializer.WriteObject(stream, obj);
                ret = System.Text.Encoding.UTF8.GetString(stream.GetBuffer(), 0, (int)stream.Position);

            }
            return ret;
        }

        /// <summary>
        /// Do not use this!
        /// Serializes the object using the DataContractJsonSerializer. Will also sanitize the output json string.
        /// </summary>
        /// <typeparam name="T">The type of the object to serialize.</typeparam>
        /// <param name="obj">The object to serialize.</param>
        /// <returns>The sanitized json string.</returns>
        public static string JsonSerializeAndSanitize<T>(T obj) where T : class
        {
            string json = JsonSerialize<T>(obj);
            return SerializationHelper.SanitizeJsonString(json);
        }

        public static T JsonDeserialize<T>(string json) where T : class
        {
            if (string.IsNullOrEmpty(json))
            {
                return null;
            }
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            using (MemoryStream stream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(json)))
            {
                return (T)serializer.ReadObject(stream);
            }
        }

        /// <summary>
        /// Do not use this!
        /// Deserializes a json string into the specified object. Will also sanitize the input json before deserializing.
        /// </summary>
        /// <typeparam name="T">The object type to deserialize to.</typeparam>
        /// <param name="json">The json to deserialize.</param>
        /// <returns>The object that was made.</returns>
        public static T JsonDeserializeAndSanitize<T>(string json) where T : class, new()
        {
            using (HttpModule.PerformanceMonitorItem.Time("JsonDeserializeAndSanitize_" + typeof(T).FullName))
            {
                string sanitizedJson = SerializationHelper.SanitizeJsonString(json);
                return JsonDeserialize<T>(sanitizedJson.ToString());
            }
        }

        /// <summary>
        /// Do not use this!
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string JavascriptJsonSerialize<T>(T obj) where T : class
        {
            if (obj == null)
            {
                return string.Empty;
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = ConstApp.MaxJsonLength;
            return serializer.Serialize(obj);
        }



        /// <summary>
        /// Do not use this!
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string JavascriptJsonSerializeStruct<T>(T obj) where T : struct
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = ConstApp.MaxJsonLength;
            return serializer.Serialize(obj);
        }


        /// <summary>
        /// Do not use this!
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T JavascriptJsonDeserializer<T>(string json) where T : class
        {
            if (string.IsNullOrEmpty(json))
            {
                return null;
            }
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = ConstApp.MaxJsonLength;

            return serializer.Deserialize<T>(json);
        }

        /// <summary>
        /// Do not use this!
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T JavascriptJsonDeserializerStruct<T>(string json) where T : struct
        {
            if (string.IsNullOrEmpty(json))
            {
                return new T();
            }

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Deserialize<T>(json);
        }
    }
}
