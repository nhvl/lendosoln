/// Author: David Dao

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace LendersOffice.Common
{
	public class MultiPartParser
	{
        public static IEnumerable<MultiPartItem> Parse(string content) 
        {
            List<MultiPartItem> list = new List<MultiPartItem>();

            if (string.IsNullOrEmpty(content))
                return list;

            string boundary = null;
            // I expected the boundary is the very first line of content.
            Regex detectBoundaryRegex = new Regex(@"\A--(?<boundary>\S+)", RegexOptions.Multiline| RegexOptions.Compiled);
            
            Match mc = detectBoundaryRegex.Match(content);
            if (mc.Success) 
            {
                boundary = mc.Groups["boundary"].Value;
            } 
            else 
            {
                return list; // Could not detect boundary string. Return immediately
            }
            // Characters other than . $ ^ { [ ( | ) * + ? \ match themselves
            string[] charactersNeedEscape = { @"\", ".", "$", "^", "{", "[", "(", "|", ")", "*", "+", "?",};
            foreach (string ch in charactersNeedEscape)
                boundary = boundary.Replace(ch, @"\" + ch);

            string boundaryPattern = "--" + boundary + @"(.*?)(?=--" + boundary + ")";
            Regex boundaryRegex = new Regex(boundaryPattern, RegexOptions.Singleline | RegexOptions.Compiled);

            string contentPattern = @"Content-Disposition: form-data; name=""(?<name>[a-zA-Z0-9_-]+)""(; filename=""(?<filename>[^""]+)"")?\s+(?<content>.+)";
            Regex contentRegex = new Regex(contentPattern, RegexOptions.Singleline | RegexOptions.Compiled);


            
            MatchCollection matchCollection = boundaryRegex.Matches(content);
            for (int i = 0; i < matchCollection.Count; i++) 
            {
                string body = matchCollection[i].Groups[1].Value.TrimWhitespaceAndBOM();
                MatchCollection bodyCollection = contentRegex.Matches(body);
                for (int j = 0; j < bodyCollection.Count; j++) 
                {
                    Match m = bodyCollection[j];
                    MultiPartItem item = new MultiPartItem(m.Groups["name"].Value, m.Groups["filename"].Value, m.Groups["content"].Value);
                    list.Add(item);
                }
            }
            return list;

            
        }
	}

    public class MultiPartItem 
    {
        
        public string Name { get; private set; }
        public string FileName { get; private set; }
        public string Content { get; private set; }

        public MultiPartItem(string name, string fileName, string content) 
        {
            Name = name;
            FileName = fileName;
            Content = content;
        }
    }
}
