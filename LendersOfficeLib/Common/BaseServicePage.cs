using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace LendersOffice.Common
{
	public class BaseServicePage : BasePage
	{
        private class ServiceScriptData 
        {
            public ServiceScriptData(string name, string url) 
            {
                this.Url = url;
                this.Name = name;
            }

            public string Url;
            public string Name;
        }

        private List<ServiceScriptData> m_list = new List<ServiceScriptData>();
        protected override void OnInit(EventArgs e) 
        {
            InitializeComponent();
            base.OnInit(e);
        }

        protected override void OnPreRender(EventArgs e) 
        {
            StringBuilder sb = new StringBuilder();
            if (m_loadLOdefaultScripts)
            {
                foreach (ServiceScriptData f in m_list)
                {
                    sb.AppendFormat(@"gService.register('{0}{1}', '{2}');", VirtualRoot, f.Url, f.Name);
                    sb.Append(Environment.NewLine);
                }
                AddInitScriptFunction("_init_utilsService", string.Format(@"function _init_utilsService() {{
{0}
}}", sb));
            }
            
            base.OnPreRender(e);
        }
        private void InitializeComponent() 
        {
            this.Init += new System.EventHandler(this.PageInit);
        }


        /// <summary>
        /// Call this method to register a name associate with service page. Make sure service name is unique
        /// per page. Url uses absolute location.
        /// Ex:
        /// RegisterService("foo", "/services/foo.aspx");
        /// 
        /// On the client script you invoke this service as following:
        /// var arg = new Object();
        /// ...
        /// ... // set your arguments
        /// gService.foo.call("methodname", args);
        /// </summary>
        /// <param name="serviceName"></param>
        /// <param name="url"></param>
        public void RegisterService(string serviceName, string url) 
        {
            m_list.Add(new ServiceScriptData(serviceName, url));
        }

        private const string DefaultUtilitiesServiceString = "/common/UtilitiesService.aspx";
        protected virtual string UtilitiesServiceUrl
        {
            get
            {
                return DefaultUtilitiesServiceString;
            }
        }

        private void PageInit(object sender, EventArgs e) 
        {
            if (m_loadLOdefaultScripts)
            {
                // By default register zipcode service.
                RegisterService("utils", this.UtilitiesServiceUrl);
                RegisterJsScript("simpleservices.js");
            }
        }
	}
}
