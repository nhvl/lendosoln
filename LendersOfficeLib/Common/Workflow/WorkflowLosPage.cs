﻿namespace LendersOffice.Common.Workflow
{
    using System;

    using DataAccess;
    using LendersOffice.Security;

    /// <summary>
    /// Class for LOS Workflow Pages.
    /// </summary>
    public class WorkflowLosPage : BaseServicePage
    {
        /// <summary>
        /// The style sheet used by the page.
        /// </summary>
        public override string StyleSheet
        {
            get { return Tools.VRoot + "/css/stylesheet.css"; }
        }

        /// <summary>
        /// Verifies that the user attempting to access the page is the Workflow Controller.
        /// </summary>
        /// <param name="e">Event Args.</param>
        /// <exception cref="CBaseException">Throws if principal is null or if UserID does not match Workflow Controller ID.</exception>
        protected override void OnInit(EventArgs e)
        {
            AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

            if (principal == null || !principal.BrokerDB.ExposeClientFacingWorkflowConfiguration || principal.UserId != principal.BrokerDB.WorkflowRulesControllerId)
            {
                throw new CBaseException(ErrorMessages.GenericAccessDenied, $"Attempt to access LOS Workflow page by user who is not the Workflow Controller. UserId=[{principal?.UserId.ToString() ?? "null"}]");
            }

            base.OnInit(e);
        }
    }
}
