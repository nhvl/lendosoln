/// Author: David Dao
namespace LendersOffice.Common
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using DataAccess;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    public class EncryptionHelper
    {
        /// <summary>
        /// The salt is appended the number of iterations to use to generate the password hash. 
        /// This is only used by the ComputerPBKDF2Hash  
        /// </summary>
        private const int DEFAULT_HASH_ITERATIONS = 1000;
        private const int SALT_SIZE = 16;
        /// <summary>
        /// Only access through Random(bytes) 
        /// </summary>
        private static RandomNumberGenerator RandomSource = RandomNumberGenerator.Create();

        public static Guid GetBrokerIdFromBarcodeUploaderToken(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                throw new ArgumentNullException("token");
            }

            string data = LendersOffice.Drivers.Encryption.EncryptionHelper.DecodeAndDecrypt(EncryptionKeyIdentifier.BarcodeUploader, token);
            string[] parts = data.Split('|');
            return new Guid(parts[1]);
        }

        public static string GetBarcodeUploadToken(Guid brokerId, string customerCode)
        {
            string data = string.Format("Barcode|{0}|{1}", brokerId, customerCode);
            return LendersOffice.Drivers.Encryption.EncryptionHelper.EncryptAndEncode(EncryptionKeyIdentifier.BarcodeUploader, data);
        }

        /// <summary>
        /// Encrypts the data for the Document Framework methods in web services.
        /// </summary>
        /// <param name="data">The data to encrypt.</param>
        /// <returns>The encrypted data.</returns>
        public static byte[] EncryptDocumentFramework(string data)
        {
            return LendersOffice.Drivers.Encryption.EncryptionHelper.EncryptString(EncryptionKeyIdentifier.DocFramework, data);
        }

        /// <summary>
        /// Decrypts the data for the Document Framework methods in web services.
        /// </summary>
        /// <param name="buffer">The data to decrypt.</param>
        /// <returns>The plain text data.</returns>
        public static string DecryptDocumentFramework(byte[] buffer)
        {
            return LendersOffice.Drivers.Encryption.EncryptionHelper.DecryptString(EncryptionKeyIdentifier.DocFramework, buffer);
        }

        public static byte[] EncryptThirdParty(string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                return null;
            }

            return LendersOffice.Drivers.Encryption.EncryptionHelper.EncryptString(EncryptionKeyIdentifier.ThirdParties, data);
        }

        public static string DecryptThirdParty(byte[] encrypted)
        {
            // Adding this so we can unit test the process since third party encryption is unusual (custom BlockSize)
            if (encrypted == null)
            {
                return null;
            }

            return LendersOffice.Drivers.Encryption.EncryptionHelper.DecryptString(EncryptionKeyIdentifier.ThirdParties, encrypted);
        }

        public static string EncryptMeritMatrix(string data)
        {
            if (null == data || data.TrimWhitespaceAndBOM() == string.Empty)
            {
                return string.Empty;
            }

            return LendersOffice.Drivers.Encryption.EncryptionHelper.EncryptAndEncode(EncryptionKeyIdentifier.MeritMatrix, data);
        }

        public static string DecryptMeritMatrix(string data)
        {
            if (null == data || data.TrimWhitespaceAndBOM() == string.Empty)
            {
                return string.Empty;
            }

            return LendersOffice.Drivers.Encryption.EncryptionHelper.DecodeAndDecrypt(EncryptionKeyIdentifier.MeritMatrix, data);
        }

        /// <summary>
        /// Encryptes the LPA user password.
        /// </summary>
        /// <param name="data">The data to encrypt.</param>
        /// <returns>The encrypted byte array.</returns>
        public static byte[] EncryptLpaUserPassword(string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                return null;
            }

            return LendersOffice.Drivers.Encryption.EncryptionHelper.EncryptString(EncryptionKeyIdentifier.LpaUserPassword, data);
        }

        /// <summary>
        /// Decryptes the byte array into an LPA user password.
        /// </summary>
        /// <param name="buffer">The byte array.</param>
        /// <returns>The string LPA user password.</returns>
        public static string DecryptLpaUserPassword(byte[] buffer)
        {
            return LendersOffice.Drivers.Encryption.EncryptionHelper.DecryptString(EncryptionKeyIdentifier.LpaUserPassword, buffer);
        }

        public static byte[] EncryptString(EncryptionKeyIdentifier keyId, string inputData)
        {
            return Drivers.Encryption.EncryptionHelper.EncryptString(keyId, inputData);
        }

        public static string DecryptString(EncryptionKeyIdentifier keyId, byte[] encrypted)
        {
            return Drivers.Encryption.EncryptionHelper.DecryptString(keyId, encrypted);
        }

        public static EncryptionKeyIdentifier GenerateNewKey()
        {
            return Drivers.Encryption.EncryptionHelper.GenerateNewKey();
        }

        /// <summary>
        /// Encrypt data using default algorithm (Rijndael) and then return bit stream in base64 encode.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string Encrypt(string data) 
        {
            if (null == data || data.TrimWhitespaceAndBOM() == string.Empty)
            {
                return string.Empty;
            }

            return LendersOffice.Drivers.Encryption.EncryptionHelper.EncryptAndEncode(EncryptionKeyIdentifier.Default, data);
        }

        public static string Decrypt(string data)
        {
            if (null == data || data.TrimWhitespaceAndBOM() == string.Empty)
            {
                return string.Empty;
            }

            return LendersOffice.Drivers.Encryption.EncryptionHelper.DecodeAndDecrypt(EncryptionKeyIdentifier.Default, data);
        }

        public static string EncryptConnectionString(string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                return string.Empty;
            }

            return LendersOffice.Drivers.Encryption.EncryptionHelper.EncryptAndEncode(EncryptionKeyIdentifier.DbConnectionString, data);
        }

        public static string DecryptConnectionString(string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                return string.Empty;
            }

            return LendersOffice.Drivers.Encryption.EncryptionHelper.DecodeAndDecrypt(EncryptionKeyIdentifier.DbConnectionString, data);
        }

        public static string EncryptKta(string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                return string.Empty;
            }

            return LendersOffice.Drivers.Encryption.EncryptionHelper.EncryptAndEncode(EncryptionKeyIdentifier.Kta, data);
        }

        public static string DecryptKta(string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                return string.Empty;
            }

            return LendersOffice.Drivers.Encryption.EncryptionHelper.DecodeAndDecrypt(EncryptionKeyIdentifier.Kta, data);
        }

        /// <summary>
        /// Gets the MD5 hash of all the given strings as if they were appended together. 
        /// However this method does not append nor create any more large objects. 
        /// </summary>
        /// <param name="datas">The list of strings to compute the has for.</param>
        /// <returns>String.Empty if no strings or the md5 hash</returns>
        public static string ComputeMD5Hash(IEnumerable<string> datas, bool toLower)
        {
            if (!datas.Any())
            {
                return string.Empty;
            }


            int characterBufferSize = 20000;
            int largestStringSize = datas.Max(p=>p.Length);

            if (characterBufferSize > largestStringSize)
            {
                characterBufferSize = largestStringSize;
            }

            char[] charBuffer = new char[characterBufferSize];
            byte[] byteBuffer = new byte[ASCIIEncoding.ASCII.GetMaxByteCount(characterBufferSize)];

            int cCount, bCount = 0;

            using (var md5 = MD5.Create())
            {
                md5.Initialize();
                foreach (string data in datas)
                {
                    using (StringReader sr = new StringReader(data))
                    {
                        while ((cCount = sr.Read(charBuffer, 0, charBuffer.Length)) != 0)
                        {
                            if (toLower)
                            {
                                for (int j = 0; j < cCount; j++)
                                {
                                    charBuffer[j] = char.ToLower(charBuffer[j]);
                                }
                            }

                            bCount = ASCIIEncoding.ASCII.GetBytes(charBuffer, 0, cCount, byteBuffer, 0);
                            md5.TransformBlock(byteBuffer, 0, bCount, null, 0);
                        }
                    }
                }

                md5.TransformFinalBlock(new byte[0], 0, 0);

                StringBuilder sb = new StringBuilder(32, 32);

                foreach (var i in md5.Hash)
                {
                    sb.Append(i.ToString("X2"));
                }

                return sb.ToString();
            }
        }

        public static string ComputeSHA256Hash(string data)
        {
            using (SHA256 sha256 = new SHA256Managed())
            {
                return Convert.ToBase64String(sha256.ComputeHash(System.Text.Encoding.UTF8.GetBytes(data)));
            }
        }

        /// <summary>
        /// Compute ShA-256 checksum for a given file. If the file does not exists then exception will be throw.
        /// </summary>
        /// <param name="path">The path of the file to perform SHA-256 checksum.</param>
        /// <returns>The SHA-256 checksum of the file.</returns>
        public static SHA256Checksum ComputeSHA256Hash(LocalFilePath path)
        {
            if (path == LocalFilePath.Invalid)
            {
                throw new ArgumentException("Invalid path.");
            }

            string checksum = string.Empty;
            using (var sha = SHA256.Create())
            {
                using (Stream stream = File.OpenRead(path.Value))
                {
                    byte[] bytes = sha.ComputeHash(stream);
                    checksum = BitConverter.ToString(bytes).Replace("-", "").ToLower();
                }
            }

            SHA256Checksum? result = SHA256Checksum.Create(checksum);
            if (result == null)
            {
                throw CBaseException.GenericException("Unexpected exception. Unable to generate the SHA-256 checksum.");
            }

            return result.Value;
        }

        public static SHA256Checksum ComputeSHA256Hash(Stream stream)
        {
            string checksum = string.Empty;
            using (var sha = SHA256.Create())
            {
                byte[] bytes = sha.ComputeHash(stream);
                checksum = BitConverter.ToString(bytes).Replace("-", "").ToLower();
            }

            SHA256Checksum? result = SHA256Checksum.Create(checksum);
            if (result == null)
            {
                throw CBaseException.GenericException("Unexpected exception. Unable to generate the SHA-256 checksum.");
            }

            return result.Value;
        }

        /// <summary>
        /// Computes the SHA-256 checksum of the specified byte array.
        /// </summary>
        /// <param name="bytes">The byte array.</param>
        /// <returns>The checksum.</returns>
        public static SHA256Checksum ComputeSHA256Hash(byte[] bytes)
        {
            using (Stream stream = new MemoryStream(bytes))
            {
                return ComputeSHA256Hash(stream);
            }
        }

        /// <summary>
        /// Generate the MD5 checksum hash. Since MD5 is not a secure algorithm, therefore DO NOT USE this to generate
        /// a hash for secure item such as password. However just to verify that data not modify then it is okay to use it.
        /// </summary>
        /// <param name="data"></param>
        /// <returns>The dashless md5 hash of the data.</returns>
        public static string ComputeMD5Hash(string data)
        {
            using (var md5 = MD5.Create())
            {
                byte[] bytes = md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(data));

                return BitConverter.ToString(bytes).Replace("-","").ToLower();
            }
        }

        /// <summary>
        /// Generate the MD5 checksum hash. Since MD5 is not a secure algorithm, therefore DO NOT USE this to generate
        /// a hash for secure item such as password. However just to verify that data not modify then it is okay to use it.
        /// </summary>
        /// <param name="dataStream">The stream where the data lies.  Since Stream is Disposable, it assumes you are using using.</param>
        /// <returns>The dashless md5 hash of the stream.</returns>
        public static string ComputeMD5Hash(Stream dataStream)
        {
            using (var md5 = MD5.Create())
            {
                byte[] bytes = md5.ComputeHash(dataStream);

                return BitConverter.ToString(bytes).Replace("-", "").ToLower();
            }
        }

        /// <summary>
        /// Uses a static 
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static byte[] Random(int bytes)
        {
            var ret = new byte[bytes];

            lock (RandomSource)
                RandomSource.GetBytes(ret);

            return ret;
        }

        /// <summary>
        /// Returns a string composed of random data. 
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string GenerateRandomString(int bytesLength)
        {
            byte[] random = Random(bytesLength);
            string data = Convert.ToBase64String(random);
            return data;
        }

        private static string GeneratePBKDF2Salt()
        {
            return GeneratePBKDF2Salt(DEFAULT_HASH_ITERATIONS);   
        }

        private static string GeneratePBKDF2Salt(int iterations)
        {
            if (DEFAULT_HASH_ITERATIONS > iterations)
            {
                throw new ArgumentException("Cannot be less than " + DEFAULT_HASH_ITERATIONS, "iterations");
            }

            var bytes = Random(SALT_SIZE);
            return iterations.ToString("X") + "." + Convert.ToBase64String(bytes); 
        }
        /// <summary>
        /// Adopted from https://code.google.com/p/stackid/source/browse/OpenIdProvider/Current.cs 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="salt">Must be initially generated from EncryptionHelper.GeneratePBKDF2HSalt(int) </param>
        /// <returns></returns>
        private static string GeneratePBKDF2HashImpl(string value, string salt)
        {
            string result = ""; 
      
                int i = salt.IndexOf(".");
                int iterations = int.Parse(salt.Substring(0, i), System.Globalization.NumberStyles.HexNumber);
                salt = salt.Substring(i + 1);

                var pbkdf2 = new Rfc2898DeriveBytes(Encoding.UTF8.GetBytes(value), Convert.FromBase64String(salt), iterations);
                var key = pbkdf2.GetBytes(24);
                result = Convert.ToBase64String(key);
         
            return result;
        }

        public static string GeneratePBKDF2Hash(string password, out string salt)
        {
            salt = GeneratePBKDF2Salt();
            return GeneratePBKDF2HashImpl(password, salt);
        }

        public static bool ValidatePBKDF2Hash(string password, string passwordHash, string passwordSalt)
        {
            return passwordHash.Equals(GeneratePBKDF2HashImpl(password, passwordSalt));
        }

        public static string GetPasswordAndPBKDF2HashSalt(out string passwordHash, out string passwordSalt)
        {
            string password = GeneratePassword(10);

            passwordHash = GeneratePBKDF2Hash(password, out passwordSalt);
            return password;
        }

        /// <summary>
        /// Generate a random password with a specific length. The random password only include alphanumeric.
        /// It is also avoid ambiguous letter such as O (letter O), l (letter L), 0 (numeric), 1 (numeric)
        /// </summary>
        /// <param name="length">Length of the password. Minimum length is 5.</param>
        /// <returns>A random password.</returns>
        public static string GeneratePassword(int length)
        {
            if (length < 5)
            {
                throw new ArgumentException("length is too short. Length must be greater than or equal to 5.");
            }

            string upperCase = "ABCDEFGHJKLMNPQRTUVWXYZ";
            string lowerCase = "abcdefghjkmnpqrstuvwxyz";
            string digit = "23456789";

            RandomNumberGenerator rng = RandomNumberGenerator.Create();

            byte[] bytes = new byte[length * 2];

            rng.GetBytes(bytes);

            int numberOfUpperCase = 0;
            int numberOfLowerCase = 0;
            int numberOfDigit = 0;

            char[] array = new char[length];

            for (int i = 0; i < length; i++)
            {
                int d = (int)bytes[i];

                int k = (int)bytes[i + length];
                if (d % 3 == 0)
                {
                    array[i] = upperCase[k % upperCase.Length];
                    numberOfUpperCase++;
                }
                else if (d % 3 == 1)
                {
                    array[i] = lowerCase[k % lowerCase.Length];
                    numberOfLowerCase++;
                }
                else
                {
                    array[i] = digit[k % digit.Length];
                    numberOfDigit++;
                }

            }

            return new string(array);
        }
	}
}
