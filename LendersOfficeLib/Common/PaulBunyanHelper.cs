using System;
using System.Collections.Generic;
using System.Web;
using DataAccess;
using LendersOffice.HttpModule;
using LendersOffice.Logging;
using LqbGrammar.DataTypes;

namespace LendersOffice.Common
{
    /// <summary>
    /// This class provides static methods that make PaulBunyan logging called easier.
    /// </summary>
    public class PaulBunyanHelper
    {
        public static void Log(LoggingLevel logLevel, string category, string message, Dictionary<string, string> extraProperties, bool useDriver = false)
        {
            // 1/23/2015 dd - Microsoft MessageQueue can only accept message with max size of 4MB. Anything above that message will not get
            // log and throw exception.
            // We should not be logging message this large anyway. So I will replace any message that exceed 2MB in length with the first 50000 characters.

            if (message != null && message.Length > 2000000)
            {
                message = "MESSAGE TOO LARGE. Truncate to first 50,000 characters. " + message.Substring(0, 50000);
            }

            if (Tools.GetLogCorrelationId() == Guid.Empty)
            {
                Tools.ResetLogCorrelationId();
            }

            string fileName = string.Empty;
            string context = string.Empty;

            // 5/16/2014 dd - Add three properties to log that will help finding related log message.
            string correlationId = Tools.ConvertGuidToFriendlyBase64(Tools.GetLogCorrelationId());
            string clientIp = string.Empty;
            string uniqueClientIdCookie = string.Empty;

            HttpContext httpContext = HttpContext.Current;

            if (httpContext != null)
            {
                try
                {
                    fileName = httpContext.Request.Path;
                    context = httpContext.Request.Url.Host;
                    clientIp = RequestHelper.ClientIP;

                    HttpCookie cookie = httpContext.Request.Cookies[PerformanceMonitorHttpModule.UniqueClientIdCookie];
                    if (null != cookie)
                    {
                        uniqueClientIdCookie = cookie.Value;
                    }
                }
                catch (HttpException)
                {
                }
            }

            if (useDriver)
            {
                LogWithDriver(logLevel, category, message, extraProperties, correlationId, clientIp, uniqueClientIdCookie);
            }
            else
            {
                LogWithoutDriver(logLevel, category, message, extraProperties, correlationId, clientIp, uniqueClientIdCookie);
            }
        }

        /// <summary>
        /// Logs the PB message outside of the FOOL framework.
        /// </summary>
        /// <param name="logLevel">The logging level/category of the log message.</param>
        /// <param name="category">The category field of the log message.</param>
        /// <param name="message">The text content of the log message.</param>
        /// <param name="extraProperties">Any extra properties to add to the log.</param>
        /// <param name="correlationId">Correlation identifier for the message.</param>
        /// <param name="clientIp">THe Client IP.</param>
        /// <param name="uniqueClientIdCookie">A unique Client ID cookie value for performance tracking.</param>
        private static void LogWithoutDriver(LoggingLevel logLevel, string category, string message, Dictionary<string, string> extraProperties, string correlationId, string clientIp, string uniqueClientIdCookie)
        {
            Dictionary<string, string> properties = new Dictionary<string, string>();

            PerformanceMonitorItem monitorItem = PerformanceMonitorItem.Current;

            if (monitorItem != null)
            {
                properties.Add("RequestUniqueId", monitorItem.UniqueId.ToString());

                if (monitorItem.sLId != Guid.Empty)
                {
                    properties.Add("sLId", monitorItem.sLId.ToString());
                }
            }

            properties.Add("Category", category);
            properties.Add("CorrelationId", correlationId);
            properties.Add("ClientIp", clientIp);
            properties.Add("UniqueClientIdCookie", uniqueClientIdCookie);

            if (extraProperties != null)
            {
                foreach (var o in extraProperties)
                {
                    properties.Add(o.Key, o.Value);
                }
            }

            ILogger _logger = LoggerManager.GetDefaultLogger();
            _logger.Log(logLevel, message, properties);
        }

        /// <summary>
        /// Logs the PB message using the FOOL <see cref="Drivers.Logger.LoggingHelper"/> class to support dependency injection through <see cref="LqbGrammar.GenericLocator{T}"/>.
        /// </summary>
        /// <param name="logLevel">The logging level/category of the log message.</param>
        /// <param name="category">The category field of the log message.</param>
        /// <param name="message">The text content of the log message.</param>
        /// <param name="extraProperties">Any extra properties to add to the log.</param>
        /// <param name="correlationId">Correlation identifier for the message.</param>
        /// <param name="clientIp">THe Client IP.</param>
        /// <param name="uniqueClientIdCookie">A unique Client ID cookie value for performance tracking.</param>
        private static void LogWithDriver(LoggingLevel logLevel, string category, string message, Dictionary<string, string> extraProperties, string correlationId, string clientIp, string uniqueClientIdCookie)
        {
            Dictionary<LogPropertyName, LogPropertyValue> properties = new Dictionary<LogPropertyName, LogPropertyValue>();

            PerformanceMonitorItem monitorItem = PerformanceMonitorItem.Current;

            if (monitorItem != null)
            {
                properties.Add(LogPropertyName.Create("RequestUniqueId").ForceValue(), LogPropertyValue.Create(monitorItem.UniqueId.ToString()).ForceValue());

                if (monitorItem.sLId != Guid.Empty)
                {
                    properties.Add(LogPropertyName.Create("sLId").ForceValue(), LogPropertyValue.Create(monitorItem.sLId.ToString()).ForceValue());
                }
            }

            properties.Add(LogPropertyName.Create("Category").ForceValue(), LogPropertyValue.Create(category).ForceValue());
            properties.Add(LogPropertyName.Create("CorrelationId").ForceValue(), LogPropertyValue.Create(correlationId).ForceValue());
            properties.Add(LogPropertyName.Create("ClientIp").ForceValue(), LogPropertyValue.Create(clientIp).ForceValue());
            properties.Add(LogPropertyName.Create("UniqueClientIdCookie").ForceValue(), LogPropertyValue.Create(uniqueClientIdCookie).ForceValue());

            if (extraProperties != null)
            {
                foreach (var o in extraProperties)
                {
                    properties.Add(LogPropertyName.Create(o.Key).ForceValue(), LogPropertyValue.Create(o.Value).ForceValue());
                }
            }

            var targets = new List<LqbGrammar.Drivers.LoggingTargetInfo>();
            var msmqLogConnectStr = MessageQueuePath.Create(Constants.ConstStage.MsMqLogConnectStr);
            var msmqpbLogConnectStr = MessageQueuePath.Create(Constants.ConstSite.MsMqPbLogConnectStr);
            if (msmqLogConnectStr != null)
            {
                targets.Add(Drivers.Logger.LoggingHelper.CreateMsmqInfo(msmqLogConnectStr.Value));
            }

            if (msmqpbLogConnectStr != null)
            {
                targets.Add(Drivers.Logger.LoggingHelper.CreateMsmqInfo(msmqpbLogConnectStr.Value));
            }

            Drivers.Logger.LoggingHelper.Log(logLevel, LogMessage.Create(message).ForceValue(), properties, targets.ToArray());
        }
    }
}