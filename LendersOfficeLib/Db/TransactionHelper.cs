﻿namespace LendersOffice.Db
{
    using System.Data.Common;
    using Adapter;

    /// <summary>
    /// Help with executing stored procedures transactionally. <para></para>
    /// </summary>
    /// <remarks>
    /// NOTE: Transactions are not to be taken lightly, especially loan-level transactions.  Use with caution.
    /// TODO: This probably belongs elsewhere.
    /// </remarks>
    public static class TransactionHelper
    {
        /// <summary>
        /// Note the transactional nature only applies for action members that use the provided connection and transaction.
        /// </summary>
        /// <param name="connectionGetter">A function that gets the connection to be used for the transaction.</param>
        /// <param name="actionToCommitOrNot">An action to be run after the connection and transaction has been created, that returns true if it should commit,  <para></para>
        /// or false if it should roll back.  It is probably best not to commit/rollback the transaction within the action code.</param>
        /// <param name="isolationLevel">The isolation level to create the transaction with.</param>
        public static void DoTransactionally(
            System.Func<DbConnection> connectionGetter,
            System.Func<DbConnection, DbTransaction, bool> actionToCommitOrNot,
            System.Data.IsolationLevel isolationLevel = System.Data.IsolationLevel.ReadCommitted)
        {
            // extracted from CategoryBridge.SetCategoryOrder
            using (var conn = connectionGetter())
            {
                DbTransaction tran = null;
                try
                {
                    if (conn != null)
                    {
                        if (conn.State != System.Data.ConnectionState.Open)
                        {
                            conn.OpenWithRetry();
                        }

                        tran = conn.BeginTransaction(isolationLevel);
                    }

                    var shouldCommit = actionToCommitOrNot(conn, tran);

                    if (tran != null)
                    {
                        if (shouldCommit)
                        {
                            tran.Commit();
                        }
                        else
                        {
                            tran.Rollback();
                        }
                    }
                }
                catch
                {
                    if (tran != null)
                    {
                        tran.Rollback();
                    }

                    throw;
                }
            }
        }
    }
}
