﻿// <copyright file="SharedTableInfo.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   11/4/2016
// </summary>

namespace LendersOffice.Db
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;

    /// <summary>
    /// Keeps info about shared tables.
    /// </summary>
    public class SharedTableInfo
    {
        /// <summary>
        /// A list of shared table names as of 10/16/2014. From devfaq question 136.
        /// </summary>
        public static readonly ReadOnlyCollection<string> Names
            = new List<string>(new string[]
            {
                "ALL_USER_SHARED_DB",
                "APPRAISAL_VENDOR_CONFIGURATION",
                "CityCountyStateZip",
                "CONFORMING_LOAN_LIMITS",
                "CUSTOM_FORM_FIELD_CATEGORY",
                "CUSTOM_FORM_FIELD_LIST",
                "DATA_RETRIEVAL_PARTNER",
                "DB_CONNECTION",
                "DB_CONNECTION_x_BROKER",
                "DOCMAGIC_XPATH_MAPPINGS",
                "DOCUMENT_VENDOR_CONFIGURATION",
                "EDOCS_DOCMAGIC_DOCTYPE",
                "EDOCS_DOCUTECH_DOCTYPE",
                "EDOCS_IDS_DOCTYPE",
                "FEATURE",
                "FFIEC_YIELD_TABLE",
                "FHA_LOAN_LIMITS",
                "FIPS_COUNTY",
                "FIRST_AMERICAN_ENDORSEMENT",
                "FIRST_AMERICAN_QUOTE_POLICY",
                "INSTANT_SUPPORT_UNAVAILABLE_MSG",
                "IRS_4506T_VENDOR_CONFIGURATION",
                "LOAN_EXTERNAL_INVOLVEMENT",
                "LOAN_FIELD_SECURITY",
                "LOAN_FIELD_TRACKED",
                "Loan_Status",
                "LPE_ERRMSG_MAP",
                "LPE_NEW_KEYWORD",
                "LPE_RELEASE",
                "MBS_DESCRIPTION",
                "MSG_LOGGING_CONFIG",
                "OCR_VENDOR_CONFIGURATION",
                "Q_ARCHIVE",
                "Q_MESSAGE",
                "Q_TYPE",
                "ROLE",
                "SECURITY_QUESTION",
                "SERVICE_COMPANY",
                "SINGLETON_TRACKER",
                "STAGE_CONFIG",
                "TITLE_VENDOR",
                "TITLE_VENDOR_POLICY",
                "WORKFLOW_GHOST_ENUM"
            }).AsReadOnly();
    }
}
