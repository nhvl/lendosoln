﻿//-----------------------------------------------------------------------
// <summary>
//     TeamUserAssignments Querier
// </summary>
// <copyright file="Querier.cs" company="MeridianLink">
//      Copyright (c) MeridianLink. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LendersOffice.Db.TeamUserAssignments
{
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Security;

    /// <summary>
    /// This class queries the database to find bad team user assignments, provided an 'I' user is asking.
    /// </summary>
    public class Querier
    {
        /// <summary>
        /// The person who is said to be running the queries.
        /// </summary>
        private readonly AbstractUserPrincipal personToRunThisQuery;

        /// <summary>
        /// Initializes a new instance of the <see cref="Querier"/> class. <para></para>
        /// Makes sure that an 'I' type person is asking before returning an instance.
        /// </summary>
        /// <param name="personToRunThisQuery">The principal of the user who wants to run this query.</param>
        public Querier(AbstractUserPrincipal personToRunThisQuery)
        {
            if (personToRunThisQuery.Type != "I")
            {
                throw CBaseException.GenericException("User does not have the ability to run team queries.");
            }

            this.personToRunThisQuery = personToRunThisQuery;
        }

        /// <summary>
        /// Finds the team user assignments where there's more than one primary team assigned to a user and role.
        /// </summary>
        /// <returns>An enumerable of results.</returns>
        public IEnumerable<QueryResult> FindEmployeesWithTooManyPrimaryTeamsForARole()
        {
            return this.RunQueryAcrossDatabases(QueryNames.GetAViewOfEmployeesWithTooManyPrimaryTeamsForARole);
        }

        /// <summary>
        /// Finds the team user assignments where there's teams assigned for a user and role, but no primaries.
        /// </summary>
        /// <returns>An enumerable of results.</returns>
        public IEnumerable<QueryResult> FindEmployeesWithTeamsButNoPrimaryTeamsForARole()
        {
            return this.RunQueryAcrossDatabases(QueryNames.GetAViewOfEmployeesWithTeamsButNoPrimaryTeamsForARole);
        }

        /// <summary>
        /// Aggregates the results of queries across all the databases.
        /// </summary>
        /// <param name="queryName">The name of the query to run per database.</param>
        /// <returns>An aggregation of results.</returns>
        private IEnumerable<QueryResult> RunQueryAcrossDatabases(string queryName)
        {
            var resultSet = new List<QueryResult>();

            foreach (var connInfo in DbConnectionInfo.ListAll())
            {
                resultSet.AddRange(
                    StoredProcedureHelper.ExecuteReaderAndGetResults(
                        (r) => QueryResult.GetFromReader(r),
                        connInfo,
                        queryName,
                        null));
            }

            return resultSet;
        }
    }
}
