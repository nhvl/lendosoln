﻿//-----------------------------------------------------------------------
// <summary>
//     TeamUserAssignments QueryResultColumnNames
// </summary>
// <copyright file="QueryResultColumnNames.cs" company="MeridianLink">
//      Copyright (c) MeridianLink. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LendersOffice.Db.TeamUserAssignments
{
    /// <summary>
    /// A class intended to house the column names that could be returned when running a query for the bad team user assignments.
    /// </summary>
    internal static class QueryResultColumnNames
    {
        /// <summary>
        /// EmployeeId is a column name in the result set.
        /// </summary>
        internal static readonly string EmployeeId = nameof(EmployeeId);

        /// <summary>
        /// RoleId is a column name in the result set.
        /// </summary>
        internal static readonly string RoleId = nameof(RoleId);

        /// <summary>
        /// BrokerId is a column name in the result set.
        /// </summary>
        internal static readonly string BrokerId = nameof(BrokerId);

        /// <summary>
        /// UserFirstNm is a column name in the result set.
        /// </summary>
        internal static readonly string UserFirstNm = nameof(UserFirstNm);

        /// <summary>
        /// UserLastNm is a column name in the result set.
        /// </summary>
        internal static readonly string UserLastNm = nameof(UserLastNm);
    }
}
