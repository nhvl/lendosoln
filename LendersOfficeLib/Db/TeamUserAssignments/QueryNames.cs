﻿//-----------------------------------------------------------------------
// <summary>
//     TeamUserAssignments QueryNames
// </summary>
// <copyright file="QueryNames.cs" company="MeridianLink">
//      Copyright (c) MeridianLink. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LendersOffice.Db.TeamUserAssignments
{
    /// <summary>
    /// Houses the names of the queries that can be run that return the columns defined by QueryResultColumnNames.
    /// </summary>
    internal static class QueryNames
    {
        /// <summary>
        /// Finds employee with too many teams assigned as primary teams for a particular role.
        /// </summary>
        internal static readonly string GetAViewOfEmployeesWithTooManyPrimaryTeamsForARole = nameof(GetAViewOfEmployeesWithTooManyPrimaryTeamsForARole);

        /// <summary>
        /// Finds employee with too many teams assigned for a role but no primary team.
        /// </summary>
        internal static readonly string GetAViewOfEmployeesWithTeamsButNoPrimaryTeamsForARole = nameof(GetAViewOfEmployeesWithTeamsButNoPrimaryTeamsForARole);
    }
}
