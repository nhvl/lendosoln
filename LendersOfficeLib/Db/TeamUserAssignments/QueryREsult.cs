﻿//-----------------------------------------------------------------------
// <summary>
//     TeamUserAssignments QueryResult
// </summary>
// <copyright file="QueryResult.cs" company="MeridianLink">
//      Copyright (c) MeridianLink. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace LendersOffice.Db.TeamUserAssignments
{
    using System;
    using System.Data;

    /// <summary>
    /// A class to house information about TeamUserAssignment queries that would be helpful.
    /// </summary>
    public class QueryResult
    {
        /// <summary>
        /// The id of the employee in the team assignment.
        /// </summary>
        public readonly Guid EmployeeId;

        /// <summary>
        /// The id of the role in the team assignment.
        /// </summary>
        public readonly Guid RoleId;

        /// <summary>
        /// The id of the broker the employee belongs to.
        /// </summary>
        public readonly Guid BrokerId;

        /// <summary>
        /// The employee's first name from the Employee table.
        /// </summary>
        public readonly string UserFirstNm;

        /// <summary>
        /// The employee's last name from the Employee table.
        /// </summary>
        public readonly string UserLastNm;

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryResult"/> class.
        /// </summary>
        /// <param name="employeeId">The id of the employee.</param>
        /// <param name="roleId">The id of the role.</param>
        /// <param name="brokerId">The id of the broker of the employee.</param>
        /// <param name="userFirstNm">The first name of the employee from the employee table.</param>
        /// <param name="userLastNm">The last name of the employee from the employee table.</param>
        private QueryResult(Guid employeeId, Guid roleId, Guid brokerId, string userFirstNm, string userLastNm)
        {
            this.EmployeeId = employeeId;
            this.RoleId = roleId;
            this.BrokerId = brokerId;
            this.UserFirstNm = userFirstNm;
            this.UserLastNm = userLastNm;
        }

        /// <summary>
        /// Gets a query result from an IDataReader, assuming that <paramref name="record"/> has all the necessary fields.
        /// </summary>
        /// <param name="record">An IDataReader that contains the necessary columns.</param>
        /// <returns>The query result at the current position of the record.</returns>
        public static QueryResult GetFromReader(IDataRecord record)
        {
            return new QueryResult(
                            (Guid)record[QueryResultColumnNames.EmployeeId],
                            (Guid)record[QueryResultColumnNames.RoleId],
                            (Guid)record[QueryResultColumnNames.BrokerId],
                            (string)record[QueryResultColumnNames.UserFirstNm],
                            (string)record[QueryResultColumnNames.UserLastNm]);
        }

        /// <summary>
        /// Get a string representation for a tsv header for a QueryResult. <para></para>
        /// Keep this in sync with AsTsvString().
        /// </summary>
        /// <returns>A tsv string form of the QueryResult header.</returns>
        public static string GetTsvHeaderString()
        {
            return "EmployeeId\tRoleId\tBrokerId\tUserFirstNm\tUserLastNm";
        }

        /// <summary>
        /// Get a string representation for a QueryResult. <para></para>
        /// Keep this in sync with GetTsvHeaderString().
        /// </summary>
        /// <returns>A tsv string form of the QueryResult.</returns>
        public string AsTsvString()
        {
            return $"{EmployeeId}\t{RoleId}\t{BrokerId}\t{UserFirstNm}\t{UserLastNm}";
        }
    }
}
