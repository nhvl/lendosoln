﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Pdf
{
    public class CCreditScoreDisclosureH3_1PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "CreditScoreDisclosure_H3_1.pdf"; }
        }
        public override string Description
        {
            get { return "Page 1"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
            if (BorrType == "C")
            {
                dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
            }
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.CreditDisclosureLender, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            AddFormFieldData("CreditScorePreparerInfo", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));
            AddFormFieldData("aDecisionCreditSourceName", dataApp.aDecisionCreditSourceName);

            AddFormFieldData("aNm", dataApp.aNm);
            AddFormFieldData("aDecisionCreditFactors", dataApp.aDecisionCreditFactors);
            AddFormFieldData("aDecisionCreditScoreFrom", dataApp.aDecisionCreditScoreFrom_rep);
            AddFormFieldData("aDecisionCreditScoreTo", dataApp.aDecisionCreditScoreTo_rep);
            AddFormFieldData("aDecisionCreditCreatedD", dataApp.aDecisionCreditCreatedD_rep);
            AddFormFieldData("aDecisionCreditScore", dataApp.aDecisionCreditScore_rep);
            AddFormFieldData("aDecisionCreditPercentile", dataApp.aDecisionCreditPercentile_rep);

        }
    }
    public class CCreditScoreDisclosureH3_2PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "CreditScoreDisclosure_H3_2.pdf"; }
        }
        public override string Description
        {
            get { return "Page 2"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {

        }
    }
    public class CCreditScoreDisclosureH3_3PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "CreditScoreDisclosure_H3_3.pdf"; }
        }
        public override string Description
        {
            get { return "Page 3"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {

        }
    }

    public class CCreditScoreDisclosureH3PDF : AbstractBatchPDF
    {
        protected override E_AppPrintModeT AppPrintModeT
        {
            get { return E_AppPrintModeT.BorrowerAndSpouseSeparately; }
        }

        public override string EditLink
        {
            get { return "/newlos/Disclosure/CreditScoreDisclosure.aspx"; }
        }
        public override string Description
        {
            get
            {
                return "Credit Score Disclosure - H-3 - Disclosure Exception";
            }
        }
        protected override AbstractPDFFile[] ChildPages
        {
            get 
            {
                return new AbstractPDFFile[] {
                    new CCreditScoreDisclosureH3_1PDF(),
                    new CCreditScoreDisclosureH3_2PDF(),
                    new CCreditScoreDisclosureH3_3PDF()
                };
            }
        }
    }
}
