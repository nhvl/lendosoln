﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CVALoanComparison : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "VALoanComparison.pdf"; }
        }
        public override string Description
        {
            get { return "VA Loan Comparison (OBSOLETE)"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            CAgentFields preparer = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("CompanyName", preparer.CompanyName);

            AddFormFieldData("sSpAddr_Multiline", dataLoan.sSpAddr_MultiLine);
            AddFormFieldData("Applicants", dataApp.aBNm_aCNm);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);

            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sRateAmortT", dataLoan.sRateAmortT);
            AddFormFieldData("sTerm", dataLoan.sTerm_rep);
            AddFormFieldData("sMonthlyPmt", dataLoan.sMonthlyPmt_rep);
            AddFormFieldData("aVaTotalClosingCost", dataLoan.aVaTotalClosingCost_rep);
            AddFormFieldData("sVaLoanNumCurrentLoan", dataLoan.sVaLoanNumCurrentLoan);
            AddFormFieldData("sVaLoanAmtCurrentLoan", dataLoan.sVaLoanAmtCurrentLoan_rep);
            AddFormFieldData("sVaNoteIrCurrentLoan", dataLoan.sVaNoteIrCurrentLoan_rep);
            AddFormFieldData("sVaTermCurrentLoan", dataLoan.sVaTermCurrentLoan_rep);
            AddFormFieldData("sVaMonthlyPmtCurrentLoan", dataLoan.sVaMonthlyPmtCurrentLoan_rep);
            AddFormFieldData("sVaNewPmtQualified", dataLoan.sVaNewPmtQualified);
            AddFormFieldData("sVaLoanCompMonthlyPmtDecreaseAmt", dataLoan.sVaLoanCompMonthlyPmtDecreaseAmt_rep);
            AddFormFieldData("sVaLoanCompRecoupCostsTime", dataLoan.sVaLoanCompRecoupCostsTime_rep);
            AddFormFieldData("sProThisMPmt", dataLoan.sProThisMPmt_rep);
            AddFormFieldData("sVaPICurrentLoan", dataLoan.sVaPICurrentLoan_rep);
        }

        public override string EditLink
        {
            get { return "/newlos/VA/VALoanComparison.aspx"; }
        }
    }
}
