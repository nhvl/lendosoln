﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Pdf
{
    public class CIRS_W9PDF : AbstractBatchPDF
    {
        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                                                 new CIRS_W9_1PDF(),
                                                 new CIRS_W9_2PDF(),
                                                 new CIRS_W9_3PDF(),
                                                 new CIRS_W9_4PDF()
                                             };
            }

        }

        public override string Description
        {
            get { return "Request for Taxpayer Identification and Certification"; }
        }
    }

    public class CIRS_W9_1PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "IRS-W-9_1.pdf"; }
        }
        public override string Description
        {
            get { return "Page 1"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public class CIRS_W9_2PDF : AbstractLetterPDF
    {

        public override string PdfFile
        {
            get { return "IRS-W-9_2.pdf"; }
        }
        public override string Description
        {
            get { return "Page 2"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public class CIRS_W9_3PDF : AbstractLetterPDF
    {

        public override string PdfFile
        {
            get { return "IRS-W-9_3.pdf"; }
        }
        public override string Description
        {
            get { return "Page 3"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }

    public class CIRS_W9_4PDF : AbstractLetterPDF
    {

        public override string PdfFile
        {
            get { return "IRS-W-9_4.pdf"; }
        }
        public override string Description
        {
            get { return "Page 4"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }
}
