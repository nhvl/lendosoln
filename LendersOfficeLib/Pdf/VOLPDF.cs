using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Admin;

namespace LendersOffice.Pdf
{
    public class CVOLListPDF : AbstractVerificationListPDF
    {
        public override string Description 
        {
            get { return "Verification of Loan"; }
        }

        protected override IEnumerable<PdfVerificationItem> GetItems(CAppData dataApp)
        {
            List<PdfVerificationItem> list = new List<PdfVerificationItem>();

            var subCollection = dataApp.aLiaCollection.GetSubcollection(true, E_DebtGroupT.Installment | E_DebtGroupT.Revolving);

            string aBNm = Utilities.SafeHtmlString(dataApp.aBNm);
            string aCNm = Utilities.SafeHtmlString(dataApp.aCNm);
            foreach (ILiabilityRegular o in subCollection)
            {
                string displayName = "";

                switch (o.OwnerT)
                {
                    case E_LiaOwnerT.Borrower: displayName = aBNm; break;
                    case E_LiaOwnerT.CoBorrower: displayName = aCNm; break;
                    case E_LiaOwnerT.Joint: displayName = aBNm + " & " + aCNm; break;
                    default:
                        throw new UnhandledEnumException(o.OwnerT);
                }


                displayName = displayName + ", " + Utilities.SafeHtmlString(o.ComNm);
                list.Add(new PdfVerificationItem() { RecordId = o.RecordId, DisplayName = displayName });
            }

            return list;
        }

        protected override AbstractPDFFile ContructPdfItem()
        {
            return new CVOLPDF();
        }
    }

	public class CVOLPDF : AbstractLetterPDF
	{

        public override string PdfFile 
        {
            get { return "VOL.pdf"; }
        } 

        public override string Description 
        {
            get { return "VOL: " + Arguments["displayname"]; }
        }

        public override Guid RecordID
        {
            get
            {
                Guid id;

                if (Guid.TryParse(this.Arguments["recordid"], out id))
                {
                    return id;
                }
                else
                {
                    return Guid.Empty;
                }
            }
        }

        public override string EditLink 
        {
            get { return "/newlos/Verifications/VOLRecord.aspx"; }
        }


        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            string title = "";
            string preparerName = "";

            string brokerPhone = "";
            string creditorAddress = "";
            string sLenderNumVerif = "";
            string loanNumber = "";
            string propertyAddress = "";
            string accountName = "";
            string accountNumber = "";
            string applicantAddress = "";
            string accountBalance = "";
            string name = "";
            string brokerAddress = "";

            sLenderNumVerif = dataLoan.sLenderNumVerif;
            loanNumber = dataLoan.sLNm;

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfLoan, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (preparer.IsValid) 
            {

                preparerName = preparer.PreparerName;
                title = preparer.Title;
                name = string.Format("{0}{2}{1}", preparerName, preparer.CompanyName, Environment.NewLine);
                
                brokerAddress = Tools.FormatAddress(name, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip);
                brokerPhone = preparer.Phone;

            }

            string borrowerName = dataApp.aBNmAndSsnForVerifications.TrimWhitespaceAndBOM() + Environment.NewLine;
            string coborrowerName = dataApp.aCNmAndSsnForVerifications.TrimWhitespaceAndBOM();
            if (!string.IsNullOrEmpty(coborrowerName)) coborrowerName += Environment.NewLine;

            applicantAddress = Tools.FormatAddress(borrowerName + coborrowerName, dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip);


            ILiabilityRegular field = dataApp.aLiaCollection.GetRegRecordOf(RecordID);

            if (dataLoan.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp && field.VerifHasSignature)
            {
                EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(dataLoan.sBrokerId, field.VerifSigningEmployeeId, dataLoan.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);
                AddFormFieldData("LenderSignature", signInfo.SignatureKey.ToString());
            }

            accountName = field.AccNm;
            accountNumber = field.AccNum.Value;
            accountBalance = field.Bal_rep;
            creditorAddress = Tools.FormatAddress(field.Attention + Environment.NewLine + field.ComNm, field.ComAddr, field.ComCity, field.ComState, field.ComZip);


            AddFormFieldData("sLNm", loanNumber);
            AddFormFieldData("sLenderNumVerif", sLenderNumVerif);
            AddFormFieldData("CreditorAddress", creditorAddress);
            AddFormFieldData("BrokerAddress", brokerAddress);
            AddFormFieldData("BrokerPhone", brokerPhone);
            AddFormFieldData("Title", title);
            AddFormFieldData("Date", preparer.PrepareDate_rep);
            AddFormFieldData("PropertyAddress", propertyAddress);
            AddFormFieldData("AccountName", accountName);
            AddFormFieldData("AccountNumber", accountNumber);
            AddFormFieldData("AccountBalance", accountBalance);
            AddFormFieldData("ApplicantAddress", applicantAddress);
            if (field.IsSeeAttachment) 
            {
                AddFormFieldData("ApplicantSignature", "SEE ATTACHMENT");
                if (dataApp.aCSsn != string.Empty)
                {
                    AddFormFieldData("CoApplicantSignature", "SEE ATTACHMENT");
                }

            }
        }
	}
}
