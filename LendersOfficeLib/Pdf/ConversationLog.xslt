﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="html" indent="yes"/>
    
    <xsl:variable name="EnableConversationLogPermissions" select="/ConversationLog/@EnableConversationLogPermissions"></xsl:variable>
  
    <xsl:template match="/BrokerLacksFeature">
      <html>
        <head></head>
        
        <body>
         <h2 style="color:red">Sorry, your company has not enabled the conversation log feature.</h2>
        </body>
      
      </html>
    
    </xsl:template>

    <xsl:template match="/ConversationLog">
      <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
          <title>Conversation Log</title>
          <style type="text/css">
            body
            {
              font-size:18px;
              padding-bottom:10px;
            }
            .removedBySection
            {
              font-weight:bold;
            }
            .hiddenComment
            {
              color:darkgray;
              font-style:italic;
            }
            #viewControlsSection{
              padding-top:3px;
              padding-bottom:3px;
            }
            #orderByFilter
            {
              padding:2px;
              border-color:black;
              border-width:1px;
              border-style:solid;
              margin-right:0;
            }
            .categoryConversationSection
            {
              background-color:gainsboro;
              border-bottom-style:solid;
              border-bottom-width:1px;
            }
            #categoriesFilterDisplayer
            {
              padding:3px;
              padding-right:5px;
              border-color:black;
              border-width:1px;
              border-style:solid;
              margin-left:0;
              background-color:white;
            }
            #filterDefaultCategoriesHeader{
              background-color:gainsboro;
              color:black;
              padding:2px;
              padding-left:7px;
              width:97%;
              border-style:solid;
              border-width:1px;
              border-color:black;
            }
            #categoriesFilterSection
            {
              position:fixed;
              width:220px;
              padding-left:1%;
              padding-right:1%;
              padding-top:6px;
              padding-bottom:6px;
              background-color:white;
              border-style:solid;
              border-width:1px;
              border-color:black;
              z-index:1;
              margin-left:70px;
            }
            #categoriesFilters
            {
            padding-top:5px;
            }
            #container{
              width:950px;
              margin-left:10px;
              margin-bottom:10px;
              border-style:solid;
              border-width:1px;
            }
            .categoryVisibilityCheckboxContainer{
            width:50%;
            display:inline-block;
            margin-top:2px;
            }
            .hidden
            {
            display:none
            }
            .categoryNameSection
            {
              background-color:gainsboro;
              font-weight:bold;
              text-decoration:underline;
              padding:5px;
              padding-left:15px;
            }
            .commentSection
            {
              padding:10px;
              background-color:white;
              margin-top:3px;
              margin-bottom:3px;
              padding-bottom:5px;
              padding-right:0;
              margin-right:0;
              white-space:nowrap;
              min-width:550px;
              display:table;
            }
            .commentInfoSection
            {
              float:left;
              width:200px;
              text-align:left;
              padding-right:10px;
              padding-left:10px;
              display:inline-block;
              vertical-align:top;
              white-space:normal;
              font-style:italic;
            }
            .commentTextAndReplyLinkSection
            {
              background-color:white;
              display:inline-block;
              padding-right:22px;
            }
            .commentTextSection
            {
              margin-bottom:11px;
              white-space:normal;
              word-wrap:break-word;
              padding-right:11px;
            }
            .commentsSection
            {
            position:relative;
            }
            .conversationsSection
            {
              min-height:300px;
              min-width:400px;
            }
            #newCommentSection
            {
            padding-top:10px;
            background-color:gainsboro;
            height:100px;
            }
            .categorySelectorLabelSection
            {
            display: inline-block;
            width: 70px;
            }
            .categorySelectorSection
            {
            display: inline-block;
            }
            .newCommentBtnAndLinkSection
            {
            margin: 2px;
            }
            .newCommentCategorySelectorSection
            {
            display: inline-block;
            vertical-align: top;
            width: 28%;
            }
            .newCommentTextAndAddSection
            {
            display: inline-block;
            width: 70%;
            }
            .newCommentTextSection
            {
            margin: 2px;
            margin-bottom: 5px;
            width: 97%;
            }
            select#categorySelector
            {
            width: 150px;
            padding:1px;
            }
            textarea#newCommentTextArea
            {
            width: 97%;
            }
            .newReplyTextArea{
            width:100%;
            }
            select::-ms-expand
            {
            background-color:white;
            border-style:none;
            /*display:none*/
            }
          </style>
        </head>
          <body>
              <div id="container">
                <div id="conversationsSection" class="conversationsSection">
                  <xsl:apply-templates select="Conversation"/>
                </div>
              </div>
          </body>
        </html>
    </xsl:template>
  
    <xsl:template match="Conversation">
      <div class="categoryConversationSection">
        <div class="categoryNameSection">
          <xsl:value-of select="@CategoryName"/>
        </div>
        <div class="commentsSection">
          <xsl:apply-templates select="Comment"/>
        </div>
      </div>
    </xsl:template>
  
  <xsl:template match="CommentLine">
    <xsl:copy-of select="Text"></xsl:copy-of><br/>
  </xsl:template>

  <xsl:template match="Comment">
    <div class="commentSection" style="margin-left:{@LeftMargin}">
      <div class="commentInfoSection">
        <xsl:value-of select="@Name"></xsl:value-of><br />
        <xsl:value-of select="@CreatedDate"></xsl:value-of><br />
        <xsl:if test="$EnableConversationLogPermissions = 'True'">
          Permission:       <xsl:value-of select="@PermissionLevelName"></xsl:value-of><br />
        </xsl:if>
      </div>
      <div class="commentTextAndReplyLinkSection" style="width:{@Width};">
        <div class="commentTextSection" style="width:{@Width};">
          <xsl:if test="@IsHidden = 'True'">
            <span class="removedBySection">
              Removed by <xsl:value-of select="@HiderName"></xsl:value-of>
            </span><br /><br/>
          </xsl:if>
          <span>
            <xsl:if test="@IsHidden = 'True'">
              <xsl:attribute name="class">hiddenComment</xsl:attribute>
            </xsl:if>
            <xsl:apply-templates select="CommentLine"></xsl:apply-templates>
          </span>
        </div>
      </div>
    </div>
  </xsl:template>

</xsl:stylesheet>
