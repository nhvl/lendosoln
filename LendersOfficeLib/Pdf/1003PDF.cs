namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;

    /// <summary>
    /// An abstract class that adds the method "AddLoanNumberData", for determining whether the sLNm should include the ULI data.
    /// </summary>
    public abstract class AbstractUliPdf : AbstractLegalPDF
    {
        /// <summary>
        /// This method appends the ULI info to the Loan Number description if the broker's IsAddUliTo1003Pages is true.
        /// </summary>
        /// <param name="dataLoan">The loan to retrieve the sLNm and Uli from.</param>
        /// <returns>A string description of the loan number and uli.</returns>
        protected string AddLoanNumberData(CPageData dataLoan)
        {
            var isAddUliNumber = this.Arguments != null && this.Arguments.Get("IsAddUli") == true.ToString();
            var value = "Loan Number: " + dataLoan.sLNm + ((isAddUliNumber && dataLoan.BrokerDB.IsAddUliTo1003Pages) ? (" ULI: " + dataLoan.sUniversalLoanIdentifier) : "");
            return value;
        }
    }

    /// <summary>
    ///  An abstract class that overrides AbstractBatchPDF's value by adding "IsAddUli" as true to the arguments.
    /// </summary>
    public abstract class AbstractBatchUliPdf : AbstractBatchPDF
    {
        /// <summary>
        /// The collection of arguments that will be passed on to the child pages.  Sets "IsAddUli" as true.
        /// </summary>
        protected override NameValueCollection ChildArguments
        {
            get
            {
                return new NameValueCollection() { { "IsAddUli", true.ToString() } };
            }
        }
    }


    /// <summary>
    /// This class should only be inherit by 1003p1, 1003p2, 1003p3.
    /// The reason I used this class for first 3 pages is that I don't
    /// want to separate out which fields locate on which 1003 page due to time constraint.
    /// </summary>
    public abstract class Abstract1003PDF : AbstractUliPdf
    {
        /// <summary>
        /// Only do 1 application at a time.
        /// </summary>
        /// <param name="dataLoan"></param>
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            LosConvert losConvert = dataLoan.m_convertLos;
            bool areConstructionLoanCalcsMigrated = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V32_ConstructionLoans_CalcChanges);

            string address = "";

            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sTerm", dataLoan.sTerm_rep + "/" + dataLoan.sDue_rep);
            AddFormFieldData("sUnitsNum", dataLoan.sUnitsNum_rep);
            AddFormFieldData("sYrBuilt", dataLoan.sYrBuilt);
            AddFormFieldData("sSpLegalDesc", dataLoan.sSpLegalDesc);
            AddFormFieldData("sSpCounty", dataLoan.sSpCounty);

            if (dataLoan.sLPurposeT == E_sLPurposeT.HomeEquity)
            {
                // 9/9/2013 dd - OPM 136658. When Equity is select then check "Other" and fill in either "Equity" or "HELOC" for other description.
                AddFormFieldData("sLPurposeT", E_sLPurposeT.Other);
                if (dataLoan.sIsLineOfCredit)
                {
                    AddFormFieldData("sOLPurposeDesc", "HELOC");
                }
                else
                {
                    AddFormFieldData("sOLPurposeDesc", "Equity");
                }
            }
            else
            {
                // Refinance cashout should also check as refinance.
                if (dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout || dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl)
                {
                    AddFormFieldData("sLPurposeT", E_sLPurposeT.Refin);
                }
                else
                {
                    AddFormFieldData("sLPurposeT", dataLoan.sLPurposeT);
                }
                AddFormFieldData("sOLPurposeDesc", dataLoan.sOLPurposeDesc);
            }
            AddFormFieldData("sLT", dataLoan.sLT);
            AddFormFieldData("sLTODesc", dataLoan.sLTODesc);
            AddFormFieldData("sMultiApps", dataLoan.sMultiApps);
            AddFormFieldData("aSpouseIExcl", dataApp.aSpouseIExcl);
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
            AddFormFieldData("sLNm", AddLoanNumberData(dataLoan));
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sTitleNm1", dataApp.aTitleNm1);
            AddFormFieldData("sTitleNm2", dataApp.aTitleNm2);
            AddFormFieldData("sManner", dataApp.aManner);
            AddFormFieldData("sSpImprovC", dataLoan.sSpImprovC_rep);
            AddFormFieldData("sSpImprovDesc", dataLoan.sSpImprovDesc);
            AddFormFieldData("sSpImprovTimeFrameT", dataLoan.sSpImprovTimeFrameT);
            AddFormFieldData("sEstateHeldT", dataLoan.sEstateHeldT);
            AddFormFieldData("sLeaseHoldExpireD", dataLoan.sLeaseHoldExpireD_rep);
            AddFormFieldData("aOccT", dataApp.aOccT);
            AddFormFieldData("sLotAcqYr", areConstructionLoanCalcsMigrated ? dataLoan.sLotAcquiredD_rep : dataLoan.sLotAcqYr);
            AddFormFieldData("sLotOrigC", dataLoan.sLotOrigC_rep);
            AddFormFieldData("sLotLien", dataLoan.sLotLien_rep);
            AddFormFieldData("sLotVal", areConstructionLoanCalcsMigrated ? dataLoan.sPresentValOfLot_rep : dataLoan.sLotVal_rep);
            AddFormFieldData("sLotImprovC", dataLoan.sLotImprovC_rep);
            AddFormFieldData("sLotWImprovTot", dataLoan.sLotWImprovTot_rep);
            if (dataLoan.sIsRefinancing)
            {
                // 6/13/2011 dd - OPM 67547 - Original Cost field always display $0.00 for 1003.
                dataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
                AddFormFieldData("sSpAcqYr", dataLoan.sSpAcqYr);
                AddFormFieldData("sSpOrigC", dataLoan.sSpOrigC_rep);
                AddFormFieldData("sSpLien", dataLoan.sSpLien_rep);
                dataLoan.SetFormatTarget(FormatTarget.PrintoutNormalFields);
            }
            AddFormFieldData("sRefPurpose", dataLoan.sRefPurpose);
            AddFormFieldData("sDwnPmtSrc", dataLoan.sDwnPmtSrc);
            AddFormFieldData("sDwnPmtSrcExplain", dataLoan.sDwnPmtSrcExplain);
            // 12/14/2004 dd - Just add new option for other amortization type.
            // If sFinMethodPrintAsOther is checked then don't check other box.
            if (dataLoan.sFinMethodPrintAsOther) 
            {
                AddFormFieldData("sFinMethodPrintAsOther", dataLoan.sFinMethodPrintAsOther);
                AddFormFieldData("sFinMethPrintAsOtherDesc", dataLoan.sFinMethPrintAsOtherDesc);
            } 
            else 
            {
                AddFormFieldData("sFinMethT", dataLoan.sFinMethT);
                AddFormFieldData("sFinMethDesc", dataLoan.sFinMethDesc);
            }

            address = dataLoan.sSpAddr + ", " + dataLoan.sSpCity + ", " + dataLoan.sSpState + " " + dataLoan.sSpZip;
            AddFormFieldData("PropertyAddress", address);

            // In the new 2004 1003 form, the property address has the following format {Addr}, {City}, {State} {Zip} County: {County}
            string address2004 = address + "    County: " + dataLoan.sSpCounty;
            AddFormFieldData("PropertyAddress2004", address2004);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aBSsn", dataApp.aBSsn);
            AddFormFieldData("aBDob", dataApp.aBDob_rep);
            AddFormFieldData("aBAge", dataApp.aBAge_rep);
            AddFormFieldData("aBSchoolYrs", dataApp.aBSchoolYrs_rep);
            AddFormFieldData("aBMaritalStatT", dataApp.aBMaritalStatT);
            AddFormFieldData("aBHPhone", dataApp.aBHPhone);
            
            // 2/18/2005 dd - aBDependNum need to display "0" not blank.
            dataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
            AddFormFieldData("aBDependNum", dataApp.aBDependNum_rep);
            dataLoan.SetFormatTarget(FormatTarget.PrintoutNormalFields);

            AddFormFieldData("aBDependAges", dataApp.aBDependAges);

            if (dataApp.aCNm.TrimWhitespaceAndBOM() != "") 
            {
                AddFormFieldData("aCNm", dataApp.aCNm);
                AddFormFieldData("aCSsn", dataApp.aCSsn);
                AddFormFieldData("aCAge", dataApp.aCAge_rep);
                AddFormFieldData("aCDob", dataApp.aCDob_rep);
                AddFormFieldData("aCSchoolYrs", dataApp.aCSchoolYrs_rep);
                AddFormFieldData("aCMaritalStatT", dataApp.aCMaritalStatT);
                AddFormFieldData("aCHPhone", dataApp.aCHPhone);
                // 2/18/2005 dd - aBDependNum need to display "0" not blank.
                dataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
                AddFormFieldData("aCDependNum", dataApp.aCDependNum_rep);
                dataLoan.SetFormatTarget(FormatTarget.PrintoutNormalFields);

                AddFormFieldData("aCDependAges", dataApp.aCDependAges);
            }

            address = Tools.FormatSingleLineAddress(dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip);
            if (address.TrimWhitespaceAndBOM() != "") 
            {
                AddFormFieldData("aBPresentAddr", address);
                if (dataApp.aBAddrT == E_aBAddrT.LivingRentFree) 
                {
                    AddFormFieldData("aBAddrT", E_aBAddrT.Rent);
                } 
                else 
                {
                    AddFormFieldData("aBAddrT", dataApp.aBAddrT);
                }
                AddFormFieldData("aBAddrYrs", dataApp.aBAddrYrs);
            }

            // The way I handle mailing address is a little different than POINT. In POINT it always
            // print out mailing address even mailing address same as present address.
            // I only printout mailing address if it is different. dd 10/21/2003
            //if (!dataApp.aBAddrMailUsePresentAddr) 
            if (dataApp.aBAddrMailSourceT != E_aAddrMailSourceT.PresentAddress)
            {
                AddFormFieldData("aBMailingAddr", Tools.FormatSingleLineAddress(dataApp.aBAddrMail, dataApp.aBCityMail, dataApp.aBStateMail, dataApp.aBZipMail));
            }
            address = Tools.FormatSingleLineAddress(dataApp.aCAddr, dataApp.aCCity, dataApp.aCState, dataApp.aCZip);
            if (address.TrimWhitespaceAndBOM() != "") 
            {
                AddFormFieldData("aCPresentAddr", address);
                if (dataApp.aCAddrT == E_aCAddrT.LivingRentFree) 
                {
                    AddFormFieldData("aCAddrT", E_aCAddrT.Rent);
                } 
                else 
                {
                    AddFormFieldData("aCAddrT", dataApp.aCAddrT);
                }
                AddFormFieldData("aCAddrYrs", dataApp.aCAddrYrs);
            }
            //if (!dataApp.aCAddrMailUsePresentAddr) 
            if (dataApp.aCAddrMailSourceT != E_aAddrMailSourceT.PresentAddress)
            {
                AddFormFieldData("aCMailingAddr", Tools.FormatSingleLineAddress(dataApp.aCAddrMail, dataApp.aCCityMail, dataApp.aCStateMail, dataApp.aCZipMail));
            }
            address = Tools.FormatSingleLineAddress(dataApp.aBPrev1Addr, dataApp.aBPrev1City, dataApp.aBPrev1State, dataApp.aBPrev1Zip);
            if (address.TrimWhitespaceAndBOM() != "") 
            {
                AddFormFieldData("aBPrev1Addr", address);
                if (dataApp.aBPrev1AddrT == E_aBPrev1AddrT.LivingRentFree) 
                {
                    AddFormFieldData("aBPrev1AddrT", E_aBPrev1AddrT.Rent);
                } 
                else 
                {
                    AddFormFieldData("aBPrev1AddrT", dataApp.aBPrev1AddrT);
                }
                AddFormFieldData("aBPrev1AddrYrs", dataApp.aBPrev1AddrYrs);
            }

            address = Tools.FormatSingleLineAddress(dataApp.aCPrev1Addr, dataApp.aCPrev1City, dataApp.aCPrev1State, dataApp.aCPrev1Zip);
            if (address.TrimWhitespaceAndBOM() != "") 
            {
                AddFormFieldData("aCPrev1Addr", address);
                if (dataApp.aCPrev1AddrT == E_aCPrev1AddrT.LivingRentFree) 
                {
                    AddFormFieldData("aCPrev1AddrT", E_aCPrev1AddrT.Rent);
                } 
                else 
                {
                    AddFormFieldData("aCPrev1AddrT", dataApp.aCPrev1AddrT);
                }
                AddFormFieldData("aCPrev1AddrYrs", dataApp.aCPrev1AddrYrs);
            }

            address = Tools.FormatSingleLineAddress(dataApp.aBPrev2Addr, dataApp.aBPrev2City, dataApp.aBPrev2State, dataApp.aBPrev2Zip);
            if (address.TrimWhitespaceAndBOM() != "") 
            {
                AddFormFieldData("aBPrev2Addr", address);

                if (dataApp.aBPrev2AddrT == E_aBPrev2AddrT.LivingRentFree) 
                {
                    AddFormFieldData("aBPrev2AddrT", E_aBPrev2AddrT.Rent);
                } 
                else 
                {
                    AddFormFieldData("aBPrev2AddrT", dataApp.aBPrev2AddrT);
                }

                AddFormFieldData("aBPrev2AddrYrs", dataApp.aBPrev2AddrYrs);
            }
            address = Tools.FormatSingleLineAddress(dataApp.aCPrev2Addr, dataApp.aCPrev2City, dataApp.aCPrev2State, dataApp.aCPrev2Zip);
            if (address.TrimWhitespaceAndBOM() != "") 
            {
                AddFormFieldData("aCPrev2Addr", address);

                if (dataApp.aCPrev2AddrT == E_aCPrev2AddrT.LivingRentFree) 
                {
                    AddFormFieldData("aCPrev2AddrT", E_aCPrev2AddrT.Rent);
                } 
                else 
                {
                    AddFormFieldData("aCPrev2AddrT", dataApp.aCPrev2AddrT);
                }

                AddFormFieldData("aCPrev2AddrYrs", dataApp.aCPrev2AddrYrs);
            }

            for (int i = 0; i < 2; i++) 
            {
                char ch = (i == 0) ? 'B' : 'C';

                IEmpCollection employmentList = i == 0 ? dataApp.aBEmpCollection : dataApp.aCEmpCollection;

                IPrimaryEmploymentRecord primaryEmploymentField = employmentList.GetPrimaryEmp(false);
                if (null != primaryEmploymentField) 
                {
                    AddFormFieldData("a" + ch + "EmplmtLen0", primaryEmploymentField.EmplmtLen_rep);
                    AddFormFieldData("a" + ch + "ProfLen0", primaryEmploymentField.ProfLen_rep);
                    AddFormFieldData("a" + ch + "IsSelfEmplmt0", primaryEmploymentField.IsSelfEmplmt);
                    AddFormFieldData("a" + ch + "EmplrBusPhone0", primaryEmploymentField.EmplrBusPhone);
                    AddFormFieldData("a" + ch + "JobTitle0", primaryEmploymentField.JobTitle);
                    string addr = Tools.FormatAddress(primaryEmploymentField.EmplrNm, primaryEmploymentField.EmplrAddr, primaryEmploymentField.EmplrCity, primaryEmploymentField.EmplrState, primaryEmploymentField.EmplrZip);
                    AddFormFieldData("a" + ch + "EmplrAddr0", addr);
                }
                int count = employmentList.CountRegular > 2 ? 2 : employmentList.CountRegular;
                // 5/24/2004 dd - There are only 2 slots for previous employment on 1003 pg1.
                for (int index = 0; index < count; index++) 
                {
                    IRegularEmploymentRecord field = (IRegularEmploymentRecord) employmentList.GetRegularRecordAt(index);

                    string duration = field.EmplmtStartD_rep + "-" + field.EmplmtEndD_rep;

                    AddFormFieldData("a" + ch + "EmplrDuration" + (index + 1), duration);
                    AddFormFieldData("a" + ch + "MonI" + (index + 1), field.MonI_rep);
                    AddFormFieldData("a" + ch + "IsSelfEmplmt" + (index + 1), field.IsSelfEmplmt);
                    AddFormFieldData("a" + ch + "EmplrBusPhone" + (index + 1), field.EmplrBusPhone);
                    AddFormFieldData("a" + ch + "JobTitle" + (index + 1), field.JobTitle);
                    string addr = Tools.FormatAddress(field.EmplrNm, field.EmplrAddr, field.EmplrCity, field.EmplrState, field.EmplrZip);
                    AddFormFieldData("a" + ch + "EmplrAddr" + (index + 1), addr);
                }
            }


            // 1003 Page 2
            AddFormFieldData("aAsstLiaCompletedNotJointly", dataApp.aAsstLiaCompletedNotJointly);
            AddFormFieldData("sPro1stM", dataLoan.sProFirstMPmt_rep);
            AddFormFieldData("sProOFinPmt", dataLoan.sProSecondMPmt_rep);
            AddFormFieldData("sProHazIns", dataLoan.sProHazIns_rep);
            AddFormFieldData("sProRealETx", dataLoan.sProRealETx_rep);
            AddFormFieldData("sProMIns", dataLoan.sProMIns_rep);
            AddFormFieldData("sProHoAssocDues", dataLoan.sProHoAssocDues_rep);
            AddFormFieldData("sProOHExp", dataLoan.sProOHExp_rep);
            AddFormFieldData("sMonthlyPmt", dataLoan.sMonthlyPmt_rep);
            
            AddFormFieldData("aBBaseI", dataApp.aBBaseI_rep);
            AddFormFieldData("aBOvertimeI", dataApp.aBOvertimeI_rep);
            AddFormFieldData("aBCommisionI", dataApp.aBCommisionI_rep);
            AddFormFieldData("aBBonusesI", dataApp.aBBonusesI_rep);
            AddFormFieldData("aBDividendI", dataApp.aBDividendI_rep);
            AddFormFieldData("aBNetRentI", dataApp.aBNetRentI1003_rep);
            AddFormFieldData("aBTotOI", dataApp.aBTotOI_print_rep);
            AddFormFieldData("aBTotI", dataApp.aBTotI_rep);
            
            AddFormFieldData("aCBaseI", dataApp.aCBaseI_rep);
            AddFormFieldData("aCOvertimeI", dataApp.aCOvertimeI_rep);
            AddFormFieldData("aCBonusesI", dataApp.aCBonusesI_rep);
            AddFormFieldData("aCCommisionI", dataApp.aCCommisionI_rep);
            AddFormFieldData("aCDividendI", dataApp.aCDividendI_rep);
            AddFormFieldData("aCNetRentI", dataApp.aCNetRentI1003_rep);
            AddFormFieldData("aCTotOI", dataApp.aCTotOI_print_rep);
            AddFormFieldData("aCTotI", dataApp.aCTotI_rep);

            AddFormFieldData("aTotBaseI", dataApp.aTotBaseI_rep);
            AddFormFieldData("aTotOvertimeI", dataApp.aTotOvertimeI_rep);
            AddFormFieldData("aTotBonusesI", dataApp.aTotBonusesI_rep);
            AddFormFieldData("aTotCommisionI", dataApp.aTotCommisionI_rep);
            AddFormFieldData("aTotDividendI", dataApp.aTotDividendI_rep);
            AddFormFieldData("aTotNetRentI", dataApp.aTotNetRentI1003_rep);
            AddFormFieldData("aTotOI", dataApp.aTotOI_print_rep);
            AddFormFieldData("aTotI", dataApp.aTotI_rep);

            AddFormFieldData("aPresRent", dataApp.aPresRent_rep);
            AddFormFieldData("aPres1stM", dataApp.aPres1stM_rep);
            AddFormFieldData("aPresOFin", dataApp.aPresOFin_rep);
            AddFormFieldData("aPresHazIns", dataApp.aPresHazIns_rep);
            AddFormFieldData("aPresRealETx", dataApp.aPresRealETx_rep);
            AddFormFieldData("aPresMIns", dataApp.aPresMIns_rep);
            AddFormFieldData("aPresHoAssocDues", dataApp.aPresHoAssocDues_rep);
            AddFormFieldData("aPresOHExp", dataApp.aPresOHExp_rep);
            AddFormFieldData("aPresTotHExp", dataApp.aPresTotHExp_rep);
            AddFormFieldData("aAsstLiqTot", dataApp.aAsstLiqTot_rep);
            AddFormFieldData("aAsstValTot", dataApp.aAsstValTot_rep);
            AddFormFieldData("aLiaBalTot", dataApp.aLiaBalTot_rep);
            AddFormFieldData("aNetWorth", dataApp.aNetWorth_rep);

            #region Other Income Lines
            var allIncomes = dataApp.aOtherIncomeList;

            // Separate the "Subject Net Rental Income" from the others
            var otherIncomes = CreateOtherIncomeData(dataApp.aBSpPosCf, dataApp.aCSpPosCf, allIncomes);
            int numOtherIncomes = otherIncomes.Count();
            decimal totalIncomes = 0;

            for (int i = 0; i < numOtherIncomes; i++)
            {
                switch (i)
                {
                    case 0:
                        AddFormFieldData("aO1IForC", otherIncomes[i].IsForCoBorrower ? "C" : "B");
                        AddFormFieldData("aO1IDesc", otherIncomes[i].Desc);
                        AddFormFieldData("aO1I", dataApp.m_convertLos.ToMoneyString(otherIncomes[i].Amount, FormatDirection.ToRep)); // This doesn't seem kosher
                        break;
                    case 1:
                        AddFormFieldData("aO2IForC", otherIncomes[i].IsForCoBorrower ? "C" : "B");
                        AddFormFieldData("aO2IDesc", otherIncomes[i].Desc);
                        AddFormFieldData("aO2I", dataApp.m_convertLos.ToMoneyString(otherIncomes[i].Amount, FormatDirection.ToRep));
                        break;
                    case 2:
                        if (numOtherIncomes > 3)
                        {
                             // OPM 60794.
                            AddFormFieldData("aO3IDesc", "SEE CONTINUATION SHEET FOR ADDITIONAL DETAILS");
                        }
                        else
                        {
                            AddFormFieldData("aO3IForC", otherIncomes[i].IsForCoBorrower ? "C" : "B");
                            AddFormFieldData("aO3IDesc", otherIncomes[i].Desc);
                            AddFormFieldData("aO3I", dataApp.m_convertLos.ToMoneyString(otherIncomes[i].Amount, FormatDirection.ToRep));
                        }
                        break;
                    default: // No Other Income to report
                        break;
                }

                if (i >= 2)
                    totalIncomes += otherIncomes[i].Amount;
            }

            if (numOtherIncomes > 3)
            {
                AddFormFieldData("aO3I", dataApp.m_convertLos.ToMoneyString(totalIncomes, FormatDirection.ToRep));
            }
            #endregion

            ILiaCollection liabilityCollection = dataApp.aLiaCollection;

            #region Liablity special record type
            var alimony = liabilityCollection.GetAlimony(false);
            var childSupport = liabilityCollection.GetChildSupport(false);
            if (alimony != null || childSupport != null) 
            {
                var totalAmount = string.Empty;
                var owedTo = string.Empty;

                if (alimony != null)
                {
                    if (alimony.Pmt != 0M)
                    {
                        if (alimony.NotUsedInRatio)
                        {
                            totalAmount = "(" + alimony.Pmt_rep + ") (A)";
                        }
                        else
                        {
                            totalAmount = alimony.Pmt_rep + " (A)";
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(alimony.OwedTo))
                    {
                        owedTo = alimony.OwedTo + " (A)";
                    }
                }

                if (alimony != null && childSupport != null)
                {
                    totalAmount += Environment.NewLine;

                    if (!string.IsNullOrWhiteSpace(alimony.OwedTo) && !string.IsNullOrWhiteSpace(childSupport.OwedTo))
                    {
                        owedTo += " / ";
                    }
                }

                if (childSupport != null)
                {
                    if (childSupport.Pmt != 0M)
                    {
                        if (childSupport.NotUsedInRatio)
                        {
                            totalAmount += "(" + childSupport.Pmt_rep + ") (CS)";
                        }
                        else
                        {
                            totalAmount += childSupport.Pmt_rep + " (CS)";
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(childSupport.OwedTo))
                    {
                        owedTo += childSupport.OwedTo + " (CS)";
                    }
                }

                AddFormFieldData("LiabilityAlimonyTotal", totalAmount);
                AddFormFieldData("LiabilityAlimonyOwedTo", owedTo);
            }

            for (int i = 0; i < 2; i++) 
            {
                ILiabilityJobExpense jobExpense = null;
                if (i == 0) 
                {
                    jobExpense = liabilityCollection.GetJobRelated1(false);
                } 
                else 
                {
                    jobExpense = liabilityCollection.GetJobRelated2(false);
                }
                if (null != jobExpense) 
                {
                    if (jobExpense.NotUsedInRatio) 
                    {
                        if (jobExpense.Pmt_rep != "")
                            AddFormFieldData("LiabilityJobExpenseMonthly" + i, "(" + jobExpense.Pmt_rep + ")");

                    } 
                    else 
                    {
                        AddFormFieldData("LiabilityJobExpenseMonthly" + i, jobExpense.Pmt_rep);
                    }
                    AddFormFieldData("LiabilityJobExpenseName" + i, jobExpense.ExpenseDesc);
                }
            }
            #endregion
            // Print normal liabilities
            var liabilityList = liabilityCollection.First7NormalLiabilities;
            for (int i = 0; i < liabilityList.Count; i++) 
            {
                ILiabilityRegular field = (ILiabilityRegular) liabilityList.GetRecordAt(i);

                string prefix = "Liability" + i;

                // If company name is blank then use description as first line, and blank out
                // description on last line.
                string name = field.ComNm.TrimWhitespaceAndBOM() == "" ? field.Desc : field.ComNm;
                string description = field.ComNm.TrimWhitespaceAndBOM() == "" ? "" : field.Desc;

                AddFormFieldData(prefix + "Name", Tools.FormatAddress(name, field.ComAddr, field.ComCity, field.ComState, field.ComZip, description));
                AddFormFieldData(prefix + "AcctNumber", field.AccNum.Value);
                AddFormFieldData(prefix + "Monthly", field.PmtRemainMons_rep1003);
                AddFormFieldData(prefix + "Balance", field.Bal_rep);
            }

            IAssetCollection assetCollection = dataApp.aAssetCollection;
            // Print normal asset liabilities bank/checking/saving
            var assetList = assetCollection.First4NormalAssets;
            for (int i = 0; i < assetList.Count; i++) 
            {
                var field = (IAssetRegular) assetList.GetRecordAt(i);
                string prefix = "Asset" + i;
                //OPM 60497: Net Equity / Net Sales Proceed's "bank" is always "Net Equity"
                if (field.AssetT == E_AssetRegularT.PendingNetSaleProceedsFromRealEstateAssets)
                {
                    AddFormFieldData(prefix + "Bank", "Net Equity");
                }
                else
                {
                    AddFormFieldData(prefix + "Bank", Tools.FormatAddress(field.ComNm.TrimWhitespaceAndBOM() == "" ? field.Desc : field.ComNm, field.StAddr, field.City, field.State, field.Zip));
                }

                AddFormFieldData(prefix + "AcctNumber", field.AccNum.Value);
                AddFormFieldData(prefix + "Value", field.Val_rep);
            }

            // 5/24/2004 dd - TODO: Push this assetCollection variable up.
            #region Fill-in Cash Deposits
            var cashDeposit = assetCollection.GetCashDeposit1(false);
            if (null != cashDeposit) 
            {
                AddFormFieldData("cashDeposit0", cashDeposit.Val_rep);
                AddFormFieldData("cashDepositDesc0", cashDeposit.Desc );
            }

            cashDeposit = assetCollection.GetCashDeposit2(false);
            if (null != cashDeposit) 
            {
                AddFormFieldData("cashDeposit1", cashDeposit.Val_rep);
                AddFormFieldData("cashDepositDesc1", cashDeposit.Desc );
            }
            #endregion

            #region Stock/Bond list
            if (assetCollection.HasExtraStocks) 
            {
                AddFormFieldData("stock1", "See Page 4 of 1003 for stocks");
                AddFormFieldData("stockAssetTotal", assetCollection.StockTotal_rep);
            } 
            else 
            {
                var stockList = assetCollection.StockCollection;
                int count = stockList.Count;
                for (int i = 0; i < count; i++) 
                {
                    var field = (IAsset) stockList.GetRecordAt(i);
                    AddFormFieldData("stock" + i, field.Desc);
                    AddFormFieldData("stock" + i + "Value", field.Val_rep);
                }
            }
            #endregion

            #region Asset special record type
            var lifeInsurance = assetCollection.GetLifeInsurance(false);
            if (null != lifeInsurance) 
            {
                AddFormFieldData("lifeInsuranceTotal", lifeInsurance.Val_rep);
                AddFormFieldData("lifeInsuranceFaceValue", lifeInsurance.FaceVal_rep);
            }

            var retirement = assetCollection.GetRetirement(false);
            if (null != retirement) 
            {
                AddFormFieldData("retirementTotal", retirement.Val_rep);
            }

            var business = assetCollection.GetBusinessWorth(false);
            if (null != business) 
            {
                AddFormFieldData("businessTotal", business.Val_rep);
            }
            #endregion

            #region Automobiles
            if (assetCollection.HasExtraAutomobiles) 
            {
                AddFormFieldData("auto1", "See Page 4 of 1003 for automobiles");
                AddFormFieldData("autoAssetTotal", assetCollection.AutomobileWorthTotal_rep); 
            } 
            else 
            {
                var autoList = assetCollection.AutomobileCollection;

                for (int i = 0; i < autoList.Count; i++) 
                {
                    var field = (IAsset) autoList.GetRecordAt(i);
                    AddFormFieldData("auto" + i, field.Desc);
                    AddFormFieldData("auto" + i + "Value", field.Val_rep);
                }

            }
            #endregion

            #region Other Asset List
            if (assetCollection.HasExtraOtherAssets) 
            {
                AddFormFieldData("otherAsset1", "See Page 4 of 1003 for other assets");
                AddFormFieldData("otherAssetTotal", assetCollection.OtherAssetTotal_rep);
            } 
            else 
            {
                var otherAssetList = assetCollection.OtherAssetCollection;
                for (int i = 0; i < otherAssetList.Count; i++) 
                {
                    var field = (IAsset) otherAssetList.GetRecordAt(i);
                    AddFormFieldData("otherAsset" + i, field.Desc);
                    AddFormFieldData("otherAsset" + i + "Value", field.Val_rep);

                }
            }
            #endregion

            AddFormFieldData("LiabilityMonthlyTotal", dataApp.aLiaMonTot_rep);
            AddFormFieldData("aBDecJudgment", dataApp.aBDecJudgment);
            AddFormFieldData("aCDecJudgment", dataApp.aCDecJudgment);
            AddFormFieldData("aBDecBankrupt", dataApp.aBDecBankrupt);
            AddFormFieldData("aCDecBankrupt", dataApp.aCDecBankrupt);
            AddFormFieldData("aBDecForeclosure", dataApp.aBDecForeclosure);
            AddFormFieldData("aCDecForeclosure", dataApp.aCDecForeclosure);
            AddFormFieldData("aBDecLawsuit", dataApp.aBDecLawsuit);
            AddFormFieldData("aCDecLawsuit", dataApp.aCDecLawsuit);
            AddFormFieldData("aBDecDelinquent", dataApp.aBDecDelinquent);
            AddFormFieldData("aCDecDelinquent", dataApp.aCDecDelinquent);
            AddFormFieldData("aBDecAlimony", dataApp.aBDecAlimony);
            AddFormFieldData("aCDecAlimony", dataApp.aCDecAlimony);
            AddFormFieldData("aBDecBorrowing", dataApp.aBDecBorrowing);
            AddFormFieldData("aCDecBorrowing", dataApp.aCDecBorrowing);
            AddFormFieldData("aBDecObligated", dataApp.aBDecObligated);
            AddFormFieldData("aCDecObligated", dataApp.aCDecObligated);
            AddFormFieldData("aBDecPastOwnership", dataApp.aBDecPastOwnership);
            AddFormFieldData("aCDecPastOwnership", dataApp.aCDecPastOwnership);
            AddFormFieldData("aBDecEndorser", dataApp.aBDecEndorser);
            AddFormFieldData("aCDecEndorser", dataApp.aCDecEndorser);
            AddFormFieldData("aBDecCitizen", dataApp.aBDecCitizen);
            AddFormFieldData("aCDecCitizen", dataApp.aCDecCitizen);
            AddFormFieldData("aBDecResidency", dataApp.aBDecResidency);
            AddFormFieldData("aCDecResidency", dataApp.aCDecResidency);
            AddFormFieldData("aBDecOcc", dataApp.aBDecOcc);
            AddFormFieldData("aCDecOcc", dataApp.aCDecOcc);
            IPreparerFields prep = dataLoan.GetPreparerOfForm( E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject );
			if (prep.IsValid) 
			{
				// 11/15/06 mf OPM 7970. Adding the license number
				AddFormFieldData("aIntrvwrFirstNm", ( prep.LicenseNumOfAgent.TrimWhitespaceAndBOM() == string.Empty ) ? prep.PreparerName : prep.PreparerName + " / Lic. " + prep.LicenseNumOfAgent );
				AddFormFieldData("aIntrvwrPhone", prep.Phone);
				string brokerInfo = Tools.FormatAddress( 
					( prep.LicenseNumOfCompany.TrimWhitespaceAndBOM() == string.Empty ) ? prep.CompanyName : prep.CompanyName + Environment.NewLine + "Lic. " + prep.LicenseNumOfCompany
					, prep.StreetAddr, prep.City, prep.State, prep.Zip);
				if (prep.PhoneOfCompany != "")
				{
					brokerInfo += Environment.NewLine + "Ph: " + prep.PhoneOfCompany;
					if (prep.FaxOfCompany != "")
						brokerInfo += " Fax: " + prep.FaxOfCompany;
				}
				else if (prep.FaxOfCompany != "")
                    brokerInfo += Environment.NewLine + "Fax: " + prep.FaxOfCompany;
				AddFormFieldData("InterviewerEmployer", brokerInfo);

                AddFormFieldData("App1003InterviewerLoanOriginatorIdentifier", prep.LoanOriginatorIdentifier);
                AddFormFieldData("App1003InterviewerCompanyLoanOriginatorIdentifier", prep.CompanyLoanOriginatorIdentifier);
                AddFormFieldData("App1003InterviewerPreparerName", prep.PreparerName);
                AddFormFieldData("App1003InterviewerPhone", prep.Phone);
                AddFormFieldData("App1003InterviewerCompanyName", prep.CompanyName);
                

                string newBrokerInfo = prep.StreetAddrMultiLines;
                if (prep.PhoneOfCompany != "")
                {
                    newBrokerInfo += Environment.NewLine + "Ph: " + prep.PhoneOfCompany;
                    if (prep.FaxOfCompany != "")
                    {
                        newBrokerInfo += " Fax: " + prep.FaxOfCompany;
                    }
                }
                else if (prep.FaxOfCompany != "")
                {
                    newBrokerInfo += Environment.NewLine + "Fax: " + prep.FaxOfCompany;
                }

                AddFormFieldData("App1003InterviewerCompanyAddr", newBrokerInfo);
			}

            AddFormFieldData("a1003ContEditSheet", dataApp.a1003ContEditSheet);
            AddFormFieldData("aIntrvwrMethodT", dataApp.aIntrvwrMethodT);
            AddFormFieldData("App1003InterviewerPrepareDate", dataLoan.sApp1003InterviewerPrepareDate_rep);

            AddFormFieldData("aBGender", dataApp.aBGenderFallback);
            AddFormFieldData("aBRaceT", dataApp.aBRaceT);
            if (dataApp.aBRaceT == E_aBRaceT.Other) 
            {
                AddFormFieldData("aBORaceDesc", dataApp.aBORaceDesc);
            }
            AddFormFieldData("aBNoFurnish", dataApp.aBNoFurnish);
            if (dataApp.aBDecPastOwnedPropT != E_aBDecPastOwnedPropT.Empty) 
            {
                AddFormFieldData("aBDecPastOwnedPropT", dataApp.aBDecPastOwnedPropT.ToString());
            }
            if (dataApp.aBDecPastOwnedPropTitleT != E_aBDecPastOwnedPropTitleT.Empty) 
            {
                AddFormFieldData("aBDecPastOwnedPropTitleT", dataApp.aBDecPastOwnedPropTitleT.ToString());
            }

            AddFormFieldData("aCGender", dataApp.aCGenderFallback);
            AddFormFieldData("aCRaceT", dataApp.aCRaceT);
            if (dataApp.aCRaceT == E_aCRaceT.Other) 
            {
                AddFormFieldData("aBORaceDesc", dataApp.aCORaceDesc);
            }
            AddFormFieldData("aCNoFurnish", dataApp.aCNoFurnish);
            if (dataApp.aCDecPastOwnedPropT != E_aCDecPastOwnedPropT.Empty) 
            {
                AddFormFieldData("aCDecPastOwnedPropT", dataApp.aCDecPastOwnedPropT.ToString());
            }
            if (dataApp.aCDecPastOwnedPropTitleT != E_aCDecPastOwnedPropTitleT.Empty) 
            {
                AddFormFieldData("aCDecPastOwnedPropTitleT", dataApp.aCDecPastOwnedPropTitleT.ToString());
            }

            // New race information for 2004's 1003.
            AddFormFieldData("aBHispanicT", dataApp.aBHispanicTFallback);
            AddFormFieldData("aBIsAmericanIndian", dataApp.aBIsAmericanIndian);
            AddFormFieldData("aBIsPacificIslander", dataApp.aBIsPacificIslander);
            AddFormFieldData("aBIsAsian", dataApp.aBIsAsian);
            AddFormFieldData("aBIsWhite", dataApp.aBIsWhite);
            AddFormFieldData("aBIsBlack", dataApp.aBIsBlack);
            AddFormFieldData("aCHispanicT", dataApp.aCHispanicTFallback);
            AddFormFieldData("aCIsAmericanIndian", dataApp.aCIsAmericanIndian);
            AddFormFieldData("aCIsPacificIslander", dataApp.aCIsPacificIslander);
            AddFormFieldData("aCIsAsian", dataApp.aCIsAsian);
            AddFormFieldData("aCIsWhite", dataApp.aCIsWhite);
            AddFormFieldData("aCIsBlack", dataApp.aCIsBlack);


            AddFormFieldData("aAltNm1", dataApp.aAltNm1);
            AddFormFieldData("aAltNm1CreditorNm", dataApp.aAltNm1CreditorNm);
            AddFormFieldData("aAltNm1AccNum", dataApp.aAltNm1AccNum.Value);
            AddFormFieldData("aAltNm2", dataApp.aAltNm2);
            AddFormFieldData("aAltNm2CreditorNm", dataApp.aAltNm2CreditorNm);
            AddFormFieldData("aAltNm2AccNum", dataApp.aAltNm2AccNum.Value);
            AddFormFieldData("aReTotVal", dataApp.aReTotVal_rep);
            AddFormFieldData("aReTotMAmt", dataApp.aReTotMAmt_rep);
            AddFormFieldData("aReTotGrossRentI", dataApp.aReTotGrossRentI_rep);
            AddFormFieldData("aReTotMPmt", dataApp.aReTotMPmt_rep);
            AddFormFieldData("aReTotHExp", dataApp.aReTotHExp_rep);
            AddFormFieldData("aReTotNetRentI", dataApp.aReTotNetRentI_rep);
            
			var reSubcoll = dataApp.aReCollection.GetSubcollection( true, E_ReoGroupT.All );
            int realEstateCount = reSubcoll.Count;
			int ind = 0;
			foreach( var item in reSubcoll )
			{
				if( ind >= 3 )
					break;

                var field = (IRealEstateOwned)item;
                string prefix = "RE" + ind;
                string name = Tools.FormatAddress(field.Addr, field.City, field.State, field.Zip);
                AddFormFieldData(prefix, name);
                AddFormFieldData(prefix + "Val", field.Val_rep);
                AddFormFieldData(prefix + "MAmt", field.MAmt_rep);
                AddFormFieldData(prefix + "GrossRentI", field.GrossRentI_rep);
                AddFormFieldData(prefix + "MPmt", field.MPmt_rep);
                AddFormFieldData(prefix + "HExp", field.HExp_rep);
                AddFormFieldData(prefix + "NetRentI", field.NetRentI_rep);   
                AddFormFieldData(prefix + "Type", field.Type);
                AddFormFieldData(prefix + "Stat", field.Stat);
				++ind;
            }

            AddFormFieldData("sPurchPrice", dataLoan.sPurchasePrice1003_rep);
            AddFormFieldData("sAltCost", dataLoan.sAltCost_rep);
            AddFormFieldData("sLandCost", dataLoan.sLandIfAcquiredSeparately1003_rep);
            AddFormFieldData("sRefPdOffAmt1003", dataLoan.sRefPdOffAmt1003_rep);
            AddFormFieldData("sTotEstPp1003", dataLoan.sTotEstPp1003_rep);
            AddFormFieldData("sTotEstCc1003", dataLoan.sTotEstCcNoDiscnt1003_rep);
            AddFormFieldData("sFfUfmip1003", dataLoan.sFfUfmip1003_rep);
            AddFormFieldData("sLDiscnt1003", dataLoan.sLDiscnt1003_rep);
            AddFormFieldData("sTotTransC", dataLoan.sTotTransC_rep);
            AddFormFieldData("sONewFinBal", dataLoan.sONewFinBal_rep); 
            AddFormFieldData("sTotCcPbs", dataLoan.sTotCcPbs_rep);
            AddFormFieldData("sOCredit1Desc", dataLoan.sOCredit1Desc);
            AddFormFieldData("sOCredit2Desc", dataLoan.sOCredit2Desc);
            AddFormFieldData("sOCredit3Desc", dataLoan.sOCredit3Desc);
            AddFormFieldData("sOCredit4Desc", dataLoan.sOCredit4Desc);
            AddFormFieldData("sOCredit1Amt", dataLoan.sOCredit1Amt_rep);
            AddFormFieldData("sOCredit2Amt", dataLoan.sOCredit2Amt_rep);
            AddFormFieldData("sOCredit3Amt", dataLoan.sOCredit3Amt_rep);
            AddFormFieldData("sOCredit4Amt", dataLoan.sOCredit4Amt_rep);
            if (dataLoan.sOCredit5Amt_rep != "")
            {
                AddFormFieldData("sOCredit5Desc", dataLoan.sOCredit5Desc);
                AddFormFieldData("sOCredit5Amt", dataLoan.sOCredit5Amt_rep);
            }
            AddFormFieldData("sLAmt1003", dataLoan.sLAmt1003_rep);
            AddFormFieldData("sFfUfmipFinanced", dataLoan.sFfUfmipFinanced_rep);
            AddFormFieldData("sTransNetCashAbsVal", dataLoan.sTransNetCashAbsVal_rep);
            AddFormFieldData("sTransNetCashFromToDesc", dataLoan.sTransNetCashFromToDesc);
            AddFormFieldData("a1003ContEditSheet", dataApp.a1003ContEditSheet);

			AddFormFieldData("sONewFinCcAsOCreditAmtDesc", dataLoan.sONewFinCcAsOCreditAmtDesc);
			AddFormFieldData("sONewFinCcAsOCreditAmt", dataLoan.sONewFinCcAsOCreditAmt_rep);

            AddFormFieldData("sProOHExpDesc", dataLoan.sProOHExpDesc);

        }

        public static List<OtherIncome> CreateOtherIncomeData(decimal aBSpPosCf, decimal aCSpPosCf, IEnumerable<OtherIncome> incomes)
        {
            if (incomes.Count() <= 3 && aBSpPosCf <= 0 && aCSpPosCf <= 0) // If we don't have to add more lines and then merge them
            {
                // This takes care of the case where there are 3 line items and no subject property income
                return incomes.ToList();
            }

            // Separate the Subject Property Net Cash Flow incomes from the other incomes
            var borrowerSubjectNetCashIncomes = new List<OtherIncome>();
            var coborrowerSubjectNetCashIncomes = new List<OtherIncome>();
            var nonBorrowerSubjectNetCashIncomes = new List<OtherIncome>();
            foreach (var income in incomes)
            {
                if (OtherIncome.Get_aOIDescT(income.Desc) == E_aOIDescT.SubjPropNetCashFlow)
                {
                    if (income.IsForCoBorrower)
                    {
                        coborrowerSubjectNetCashIncomes.Add(income);
                    }
                    else
                    {
                        borrowerSubjectNetCashIncomes.Add(income);
                    }
                }
                else
                {
                    nonBorrowerSubjectNetCashIncomes.Add(income);
                }
            }

            var totalIncomes = new List<OtherIncome>();
            if (aBSpPosCf > 0) // Add the aBSpPosCf to Subject Property Net Cash Flow
            {
                borrowerSubjectNetCashIncomes.Add(new OtherIncome()
                {
                    Amount = aBSpPosCf,
                    Desc = "Subject Property Net Cash Flow",
                    IsForCoBorrower = false
                });
            }
            var borrowerTotalSubjectNetCash = new OtherIncome()
            {
                Amount = borrowerSubjectNetCashIncomes.Sum(item => item.Amount),
                Desc = "Subject Property Net Cash Flow",
                IsForCoBorrower = false
            };
            if (borrowerTotalSubjectNetCash.Amount != 0)
            {
                totalIncomes.Add(borrowerTotalSubjectNetCash);
            }

            if (aCSpPosCf > 0) // Add the aCSpPosCf to Subject Property Net Cash Flow
            {
                coborrowerSubjectNetCashIncomes.Add(new OtherIncome()
                {
                    Amount = aCSpPosCf,
                    Desc = "Subject Property Net Cash Flow",
                    IsForCoBorrower = true
                });
            }
            var coborrowerTotalSubjectNetCash = new OtherIncome()
            {
                Amount = coborrowerSubjectNetCashIncomes.Sum(item => item.Amount),
                Desc = "Subject Property Net Cash Flow",
                IsForCoBorrower = true
            };
            if (coborrowerTotalSubjectNetCash.Amount != 0)
            {
                // The coborrower subject net cash doesn't always get its own line
                // It gets lumped in with the nonBorrowerSubjectNetCashIncomes
                nonBorrowerSubjectNetCashIncomes.Add(coborrowerTotalSubjectNetCash);
            }

            // Combine the non Subject Property Net Cash Flow incomes if necessary
            // Use 2 as the limit, since we only have 3 lines, and we're using one for the borrower subject net cash
            //var combinedNonSubjectNetCashIncomes = Tools.ReduceOtherIncomes(nonBorrowerSubjectNetCashIncomes, 2);
            //totalIncomes.AddRange(combinedNonSubjectNetCashIncomes);

            totalIncomes.AddRange(nonBorrowerSubjectNetCashIncomes);

            // TotalIncomes should have at most 3 lines
            return totalIncomes;
        }

    }

    /// <summary>
    /// Assets &amp; Liabilities continuation sheet.
    /// </summary>
    public abstract class Abstract1003_4aPDF : AbstractUliPdf
    {
        public override string Description 
        {
            get { return "Page 4 : Liabilities / Assets - "; }
        }

        public override bool IsUlad
        {
            get
            {
                return true;
            }
        }

        public override string EditLink 
        {
            get { return "Liabilities"; }
        }

        public override bool IsVisible 
        {
            get 
            {
                if (null == m_dataApp) 
                {
                    return false;
                }
                var liabilityList = m_dataApp.aLiaCollection.ExtraNormalLiabilities;
                var assetList = m_dataApp.aAssetCollection.ExtraNormalAssets;

                return liabilityList.Count > 0 || assetList.Count > 0;
            }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            var liabilityList = dataApp.aLiaCollection.ExtraNormalLiabilities;
            var assetList = dataApp.aAssetCollection.ExtraNormalAssets;

            int extraRecordCount = liabilityList.Count < assetList.Count ? assetList.Count : liabilityList.Count;

            for (int i = 0; i < extraRecordCount; i++) 
            {
                if (i % 10 == 0) 
                {
                    if (i != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("sLNm", this.AddLoanNumberData(dataLoan));
                    AddFormFieldData("aBNm", dataApp.aBNm); 
                    AddFormFieldData("aCNm", dataApp.aCNm); 

                }
                if (i < liabilityList.Count) 
                {
                    ILiabilityRegular field = (ILiabilityRegular) liabilityList.GetRecordAt(i);

                    string prefix = "Liability" + (i % 10);

                    // If company name is blank then use description as first line, and blank out
                    // description on last line.
                    string name = field.ComNm.TrimWhitespaceAndBOM() == "" ? field.Desc : field.ComNm;
                    string description = field.ComNm.TrimWhitespaceAndBOM() == "" ? "" : field.Desc;

                    AddFormFieldData(prefix + "Name", Tools.FormatAddress(name, field.ComAddr, field.ComCity, field.ComState, field.ComZip, description));
                    AddFormFieldData(prefix + "AcctNumber", field.AccNum.Value);
                    AddFormFieldData(prefix + "Monthly", field.PmtRemainMons_rep1003);
                    AddFormFieldData(prefix + "Balance", field.Bal_rep);
                }
                if (i < assetList.Count) 
                {
                    var field = (IAssetRegular) assetList.GetRecordAt(i);
                    string prefix = "Asset" + (i % 10);
                    //OPM 60497: Net Equity / Net Sales Proceed's "bank" is always "Net Equity"
                    if (field.AssetT == E_AssetRegularT.PendingNetSaleProceedsFromRealEstateAssets)
                    {
                        AddFormFieldData(prefix + "Bank", "Net Equity");
                    }
                    else
                    {
                        AddFormFieldData(prefix + "Bank", Tools.FormatAddress(field.ComNm.TrimWhitespaceAndBOM() == "" ? field.Desc : field.ComNm, field.StAddr, field.City, field.State, field.Zip));
                    }
                    AddFormFieldData(prefix + "AcctNumber", field.AccNum.Value);
                    AddFormFieldData(prefix + "Value", field.Val_rep);
                }
            }
        }
    }

    /// <summary>
    /// REO continuation sheet.
    /// </summary>
    public abstract class Abstract1003_4bPDF : AbstractUliPdf
    {

        public override string Description 
        {
            get { return "Page 4 : Real Estate Owned - "; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the PDF has its values edited on a page that has Ulad variants.
        /// </summary>
        public override bool IsUlad
        {
            get
            {
                return true;
            }
        }

        public override string EditLink 
        {
            get { return "REO"; }
        }

        public override bool IsVisible 
        {
            get 
            {
                if (null == m_dataApp) 
                {
                    return false;
                }
                return m_dataApp.aReCollection.HasExtraREO;
            }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            var subcoll = dataApp.aReCollection.GetSubcollection( true, E_ReoGroupT.All );
            int extraRealEstateCount = subcoll.Count - 3;

            for (int index = 0; index < extraRealEstateCount; index++) 
            {
                if (index % 26 == 0) 
                {
                    if (index != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("aBNm", dataApp.aBNm);
                    AddFormFieldData("aCNm", dataApp.aCNm);
                    AddFormFieldData("sLNm", this.AddLoanNumberData(dataLoan));
                }
                var field = (IRealEstateOwned) subcoll.GetRecordAt( index + 3 );

                string prefix = "RE" + (index % 26);
                string name = Tools.FormatAddress(field.Addr, field.City, field.State, field.Zip);

                AddFormFieldData(prefix, name);
                AddFormFieldData(prefix + "Val", field.Val_rep);
                AddFormFieldData(prefix + "MAmt", field.MAmt_rep);
                AddFormFieldData(prefix + "GrossRentI", field.GrossRentI_rep);
                AddFormFieldData(prefix + "MPmt", field.MPmt_rep);
                AddFormFieldData(prefix + "HExp", field.HExp_rep);
                AddFormFieldData(prefix + "NetRentI", field.NetRentI_rep);
                AddFormFieldData(prefix + "Type", field.Type);
                AddFormFieldData(prefix + "Stat", field.Stat);
            }
        }

    }

    /// <summary>
    /// Stock & bond continuation sheet
    /// </summary>
    public abstract class Abstract1003_4cPDF : AbstractUliPdf
    {

        public override string Description 
        {
            get { return "Page 4 : Stocks &amp; Bonds - "; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the PDF has its values edited on a page that has Ulad variants.
        /// </summary>
        public override bool IsUlad
        {
            get
            {
                return true;
            }
        }

        public override string EditLink 
        {
            get { return "Assets"; }
        }

        public override bool IsVisible 
        {
            get 
            {
                if (null == m_dataApp) 
                {
                    return false;
                }

                return m_dataApp.aAssetCollection.HasExtraStocks;
            }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            IAssetCollection assetCollection = dataApp.aAssetCollection;
            if (!assetCollection.HasExtraStocks) 
            {
                /// Only print stocks on additional page if there are more than 3 stocks.
                return;
            }

            var stockList = assetCollection.StockCollection;

            int count = stockList.Count;

            for (int index = 0; index < count; index++) 
            {
                if (index % 24 == 0) 
                {
                    if (index != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("sLNm", this.AddLoanNumberData(dataLoan));
                    AddFormFieldData("aBNm", dataApp.aBNm);
                    AddFormFieldData("aCNm", dataApp.aCNm);
                }
                var field = (IAsset) stockList.GetRecordAt(index);

                int prefixIndex = index % 24;
                switch (field.OwnerT) 
                {
                    case E_AssetOwnerT.Borrower: 
                        AddFormFieldData("owner" + prefixIndex, "B"); 
                        break;
                    case E_AssetOwnerT.CoBorrower: 
                        AddFormFieldData("owner" + prefixIndex, "C"); 
                        break;
                    case E_AssetOwnerT.Joint: 
                        AddFormFieldData("owner" + prefixIndex, "J"); 
                        break;
                }
                AddFormFieldData("stock" + prefixIndex, field.Desc);
                AddFormFieldData("stock" + prefixIndex + "Value", field.Val_rep);
            }
        }


    }

    /// <summary>
    /// Automobiles continuation sheet
    /// </summary>
    public abstract class Abstract1003_4dPDF : AbstractUliPdf
    {
        public override string Description 
        {
            get { return "Page 4 : Automobiles - "; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the PDF has its values edited on a page that has Ulad variants.
        /// </summary>
        public override bool IsUlad
        {
            get
            {
                return true;
            }
        }

        public override string EditLink 
        {
            get { return "Assets"; }
        }

        public override bool IsVisible 
        {
            get 
            {
                if (null == m_dataApp) 
                {
                    return false;
                }

                return m_dataApp.aAssetCollection.HasExtraAutomobiles;
            }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            if (!dataApp.aAssetCollection.HasExtraAutomobiles) 
            {
                /// Only print auto on additional page if there are more than 3 autos.
                return;
            }
            var autoList = dataApp.aAssetCollection.GetSubcollection(true, E_AssetGroupT.Auto);
            int count = autoList.Count;
            for (int index = 0; index < count; index++) 
            {
                if (index % 24 == 0) 
                {
                    if (index != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("sLNm", this.AddLoanNumberData(dataLoan));
                    AddFormFieldData("aBNm", dataApp.aBNm);
                    AddFormFieldData("aCNm", dataApp.aCNm);
                }
                var field = (IAsset) autoList.GetRecordAt(index);

                int prefixIndex = index % 24;
                switch (field.OwnerT) 
                {
                    case E_AssetOwnerT.Borrower: 
                        AddFormFieldData("owner" + prefixIndex, "B"); 
                        break;
                    case E_AssetOwnerT.CoBorrower: 
                        AddFormFieldData("owner" + prefixIndex, "C"); 
                        break;
                    case E_AssetOwnerT.Joint: 
                        AddFormFieldData("owner" + prefixIndex, "J"); 
                        break;
                }
                AddFormFieldData("auto" + prefixIndex, field.Desc);
                AddFormFieldData("auto" + prefixIndex + "Value", field.Val_rep);
            }

        }
    }
    /// <summary>
    /// Other asset continuation sheet
    /// </summary>
    public abstract class Abstract1003_4ePDF : AbstractUliPdf
    {
        public override string Description 
        {
            get { return "Page 4 : Other Assets - "; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the PDF has its values edited on a page that has Ulad variants.
        /// </summary>
        public override bool IsUlad
        {
            get
            {
                return true;
            }
        }

        public override string EditLink 
        {
            get { return "Assets"; }
        }

        public override bool IsVisible 
        {
            get 
            {
                if (null == m_dataApp) 
                {
                    return false;
                }

                return m_dataApp.aAssetCollection.HasExtraOtherAssets;
            }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            IAssetCollection assetCollection = dataApp.aAssetCollection;
            if (!assetCollection.HasExtraOtherAssets) 
            {
                /// Only print itemized assets on additional page if there are more than 3 itemized assets.
                return;
            }

            var otherAssetList = assetCollection.OtherAssetCollection;
            int count = otherAssetList.Count;
            for (int index = 0; index < count; index++) 
            {
                if (index % 24 == 0) 
                {
                    if (index != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("sLNm", this.AddLoanNumberData(dataLoan));
                    AddFormFieldData("aBNm", dataApp.aBNm);
                    AddFormFieldData("aCNm", dataApp.aCNm);
                }
                var field = (IAsset) otherAssetList.GetRecordAt(index);

                int prefixIndex = index % 24;
                switch (field.OwnerT) 
                {
                    case E_AssetOwnerT.Borrower: 
                        AddFormFieldData("owner" + prefixIndex, "B"); 
                        break;
                    case E_AssetOwnerT.CoBorrower: 
                        AddFormFieldData("owner" + prefixIndex, "C"); 
                        break;
                    case E_AssetOwnerT.Joint: 
                        AddFormFieldData("owner" + prefixIndex, "J"); 
                        break;
                }
                AddFormFieldData("otherAsset" + prefixIndex, field.Desc);
                AddFormFieldData("otherAsset" + prefixIndex + "Value", field.Val_rep);
            }

        }
    }

        /// <summary>
    /// Employment continuation sheet
    /// </summary>
    public abstract class Abstract1003_4fPDF : AbstractUliPdf
    {
        public override string Description 
        {
            get { return "Page 4 : Employments - "; }
        }

        public override string EditLink 
        {
            get { return "/newlos/BorrowerInfo.aspx?pg=1"; }
        }

        public override bool IsVisible 
        {
            get 
            {
                if (null == m_dataApp) 
                {
                    return false;
                }
                var extraBorrowerEmployments = m_dataApp.aBEmpCollection.ExtraPreviousEmployments;
                var extraCoborrowerEmployments = m_dataApp.aCEmpCollection.ExtraPreviousEmployments;

                return extraBorrowerEmployments.Count > 0 || extraCoborrowerEmployments.Count > 0;
            }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            var extraBorrowerEmployments = dataApp.aBEmpCollection.ExtraPreviousEmployments;
            var extraCoborrowerEmployments = dataApp.aCEmpCollection.ExtraPreviousEmployments;

            int extraEmploymentRecordCount = extraBorrowerEmployments.Count > extraCoborrowerEmployments.Count ? extraBorrowerEmployments.Count : extraCoborrowerEmployments.Count;

            for (int extraIndex = 0; extraIndex < extraEmploymentRecordCount; extraIndex++) 
            {
                if (extraIndex % 8 == 0) 
                {
                    if (extraIndex != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("sLNm", this.AddLoanNumberData(dataLoan));
                    AddFormFieldData("aBNm", dataApp.aBNm);
                    AddFormFieldData("aCNm", dataApp.aCNm);
                }

                for (int i = 0; i < 2; i++) 
                {
                    var list = (i == 0) ? extraBorrowerEmployments : extraCoborrowerEmployments;
                    if (extraIndex < list.Count) 
                    {
                        string ch = (i == 0) ? "B" : "C";
                        int j = extraIndex % 8;
                        IRegularEmploymentRecord f = (IRegularEmploymentRecord) list.GetRecordAt(extraIndex);
                        AddFormFieldData("a" + ch + "IsSelfEmplmt" + j, f.IsSelfEmplmt);
                        AddFormFieldData("a" + ch + "EmplrBusPhone" + j, f.EmplrBusPhone);
                        AddFormFieldData("a" + ch + "JobTitle" + j, f.JobTitle);
                        AddFormFieldData("a" + ch + "EmplrAddr" + j, Tools.FormatAddress(f.EmplrNm, f.EmplrAddr, f.EmplrCity, f.EmplrState, f.EmplrZip));
                        AddFormFieldData("a" + ch + "EmplrDuration" + j, f.EmplmtStartD_rep + Environment.NewLine + f.EmplmtEndD_rep);
                        AddFormFieldData("a" + ch + "MonI" + j, f.MonI_rep);
                    }
                }
            }

        }

    }

    /// <summary>
    /// Former Address Continuation sheet.
    /// </summary>
    public abstract class Abstract1003_4gPDF : AbstractUliPdf
    {

        public override string Description 
        {
            get { return "Page 4 : Former address - "; }
        }
        public override string EditLink 
        {
            get { return "/newlos/BorrowerInfo.aspx?pg=0"; }
        }
        public override bool IsVisible 
        {
            get 
            {
                return m_dataApp.aHasExtraFormerAddress;
            }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
            AddFormFieldData("sLNm", this.AddLoanNumberData(dataLoan));
            AddFormFieldData("aBNm", dataApp.aBNm); 
            AddFormFieldData("aCNm", dataApp.aCNm); 
            string address = Tools.FormatAddress(dataApp.aBPrev2Addr, dataApp.aBPrev2City, dataApp.aBPrev2State, dataApp.aBPrev2Zip);

            if (address.TrimWhitespaceAndBOM() != "") 
            {
                AddFormFieldData("aBPrev2Addr", address);

                if (dataApp.aBPrev2AddrT == E_aBPrev2AddrT.LivingRentFree)
                {
                    AddFormFieldData("aBPrev2AddrT", E_aBPrev2AddrT.Rent);
                }
                else
                {
                    AddFormFieldData("aBPrev2AddrT", dataApp.aBPrev2AddrT);
                }

                AddFormFieldData("aBPrev2AddrYrs", dataApp.aBPrev2AddrYrs);
            }
            address = Tools.FormatAddress(dataApp.aCPrev2Addr, dataApp.aCPrev2City, dataApp.aCPrev2State, dataApp.aCPrev2Zip);

            if (address.TrimWhitespaceAndBOM() != "") 
            {
                AddFormFieldData("aCPrev2Addr", address);

                if (dataApp.aCPrev2AddrT == E_aCPrev2AddrT.LivingRentFree)
                {
                    AddFormFieldData("aCPrev2AddrT", E_aCPrev2AddrT.Rent);
                }
                else
                {
                    AddFormFieldData("aCPrev2AddrT", dataApp.aCPrev2AddrT);
                }

                AddFormFieldData("aCPrev2AddrYrs", dataApp.aCPrev2AddrYrs);
            }
        }
    }

    /// <summary>
    /// Other Income continuation sheet
    /// </summary>
    public abstract class Abstract1003_4hPDF : AbstractUliPdf
    {
        public override string Description
        {
            get { return "Page 4 : Other Income - "; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the PDF has its values edited on a page that has Ulad variants.
        /// </summary>
        public override bool IsUlad
        {
            get
            {
                return true;
            }
        }

        public override string EditLink
        {
            get { return "MonthlyIncome"; }
        }

        public override bool IsVisible
        {
            get
            {
                if (null == m_dataApp)
                {
                    return false;
                }
                return Abstract1003PDF.CreateOtherIncomeData(m_dataApp.aBSpPosCf, m_dataApp.aCSpPosCf, m_dataApp.aOtherIncomeList).Count > 3;
            }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            var otherIncomeData = Abstract1003PDF.CreateOtherIncomeData(dataApp.aBSpPosCf, dataApp.aCSpPosCf, dataApp.aOtherIncomeList);
            if (otherIncomeData.Count <= 3)
            {
                /// Only print auto on additional page if there are more than 3 autos.
                return;
            }
            int count = otherIncomeData.Count;
            for (int index = 0; index < count; index++)
            {
                if (index % 24 == 0)
                {
                    if (index != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("sLNm", this.AddLoanNumberData(dataLoan));
                    AddFormFieldData("aBNm", dataApp.aBNm);
                    AddFormFieldData("aCNm", dataApp.aCNm);
                }
                OtherIncome income = otherIncomeData[index];

                int prefixIndex = index % 24;
                AddFormFieldData("iowner" + prefixIndex, income.IsForCoBorrower ?  "C" : "B");
                
                AddFormFieldData("income" + prefixIndex, income.Desc);
                AddFormFieldData("income" + prefixIndex + "Value", dataApp.m_convertLos.ToMoneyString(income.Amount, FormatDirection.ToRep));
            }

        }
    }


    #region "1003 after 01/2004"
    public class C1003_04_1PDF : Abstract1003PDF 
    {

        public override string PdfFile 
        {
            get { return "1003_04_1.pdf"; }
        }

        public override string Description 
        {
            get { return "Page 1"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/Loan1003.aspx"; }
        }

    }

    public class C1003_04_2PDF : Abstract1003PDF 
    {

        public override string PdfFile 
        {
            get { return "1003_04_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=1"; }
        }
    }

    public class C1003_04_3PDF : Abstract1003PDF 
    {

        public override string PdfFile 
        {
            get { return "1003_04_3.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 3"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=2"; }
        }

    }

    public class C1003_04_4PDF : Abstract1003PDF
    {

        public override string PdfFile 
        {
            get { return "1003_04_4.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 4"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=3"; }
        }

    }

    /// <summary>
    /// Assets & Liabilities continuation sheet.
    /// </summary>
    public class C1003_04_4aPDF : Abstract1003_4aPDF 
    {

        public override string PdfFile 
        {
            get { return "1003_04_4a.pdf"; }
        }
    }

    /// <summary>
    /// REO continuation sheet.
    /// </summary>
    public class C1003_04_4bPDF : Abstract1003_4bPDF  
    {

        public override string PdfFile 
        {
            get { return "1003_04_4b.pdf"; }
        }

    }

    /// <summary>
    /// Stock & bond continuation sheet
    /// </summary>
    public class C1003_04_4cPDF : Abstract1003_4cPDF
    {

        public override string PdfFile 
        {
            get { return "1003_04_4c.pdf"; }
        }

    }

    /// <summary>
    /// Automobiles continuation sheet
    /// </summary>
    public class C1003_04_4dPDF : Abstract1003_4dPDF 
    {

        public override string PdfFile 
        {
            get { return "1003_04_4d.pdf"; }
        }
    }

    /// <summary>
    /// Other asset continuation sheet
    /// </summary>
    public class C1003_04_4ePDF : Abstract1003_4ePDF 
    {

        public override string PdfFile 
        {
            get { return "1003_04_4e.pdf"; }
        }

    }

    /// <summary>
    /// Employment continuation sheet
    /// </summary>
    public class C1003_04_4fPDF : Abstract1003_4fPDF
    {

        public override string PdfFile 
        {
            get { return "1003_04_4f.pdf"; }
        }
    }
    /// <summary>
    /// The second former address info for the new 1003.
    /// </summary>
    public class C1003_04_4gPDF : Abstract1003_4gPDF 
    {

        public override string PdfFile 
        {
            get { return "1003_04_4g.pdf"; }
        }


    }
    #endregion

    #region "1003 after 01/2006"
    public class C1003_06_1PDF : Abstract1003PDF 
    {

        public override string PdfFile 
        {
            get { return "1003_06_1.pdf"; }
        }

        public override string Description 
        {
            get { return "Page 1"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/Loan1003.aspx"; }
        }

    }

    public class C1003_06_2PDF : Abstract1003PDF 
    {

        public override string PdfFile 
        {
            get { return "1003_06_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=1"; }
        }
    }

    public class C1003_06_3PDF : Abstract1003PDF 
    {

        public override string PdfFile 
        {
            get { return "1003_06_3.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 3"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=2"; }
        }

    }

    public class C1003_06_4PDF : Abstract1003PDF
    {

        public override string PdfFile 
        {
            get { return "1003_06_4.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 4"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=3"; }
        }

    }

    /// <summary>
    /// Assets & Liabilities continuation sheet.
    /// </summary>
    public class C1003_06_4aPDF : Abstract1003_4aPDF 
    {
        public override string Description 
        {
            get { return "Page 4 : Liabilities / Assets - "; }
        }
        public override string PdfFile 
        {
            get { return "1003_06_4a.pdf"; }
        }
    }

    /// <summary>
    /// REO continuation sheet.
    /// </summary>
    public class C1003_06_4bPDF : Abstract1003_4bPDF  
    {
        public override string Description 
        {
            get { return "Page 4 : Real Estate Owned - "; }
        }
        public override string PdfFile 
        {
            get { return "1003_06_4b.pdf"; }
        }

    }

    /// <summary>
    /// Stock & bond continuation sheet
    /// </summary>
    public class C1003_06_4cPDF : Abstract1003_4cPDF
    {
        public override string Description 
        {
            get { return "Page 4 : Stocks &amp; Bonds - "; }
        }
        public override string PdfFile 
        {
            get { return "1003_06_4c.pdf"; }
        }

    }

    /// <summary>
    /// Automobiles continuation sheet
    /// </summary>
    public class C1003_06_4dPDF : Abstract1003_4dPDF 
    {
        public override string Description 
        {
            get { return "Page 4 : Automobiles - "; }
        }
        public override string PdfFile 
        {
            get { return "1003_06_4d.pdf"; }
        }
    }

    /// <summary>
    /// Other asset continuation sheet
    /// </summary>
    public class C1003_06_4ePDF : Abstract1003_4ePDF 
    {
        public override string Description 
        {
            get { return "Page 4 : Other Assets - "; }
        }
        public override string PdfFile 
        {
            get { return "1003_06_4e.pdf"; }
        }

    }

    /// <summary>
    /// Employment continuation sheet
    /// </summary>
    public class C1003_06_4fPDF : Abstract1003_4fPDF
    {
        public override string Description 
        {
            get { return "Page 4 : Employments - "; }
        }

        public override string PdfFile 
        {
            get { return "1003_06_4f.pdf"; }
        }
    }
    /// <summary>
    /// The second former address info for the new 1003.
    /// </summary>
    public class C1003_06_4gPDF : Abstract1003_4gPDF 
    {
        public override string Description 
        {
            get { return "Page 4 : Former address - "; }
        }
        public override string PdfFile 
        {
            get { return "1003_06_4g.pdf"; }
        }


    }
    #endregion

    #region "1003 after 07/2010 (Legal Size)"
    public class C1003_10_1PDF : Abstract1003PDF
    {
        public override string PdfFile
        {
            get { return "1003_10_1.pdf"; }
        }

        public override string Description
        {
            get { return "Page 1"; }
        }

        public override string EditLink
        {
            get { return "/newlos/Forms/Loan1003.aspx"; }
        }

    }

    public class C1003_10_2PDF : Abstract1003PDF
    {

        public override string PdfFile
        {
            get { return "1003_10_2.pdf"; }
        }
        public override string Description
        {
            get { return "Page 2"; }
        }

        public override string EditLink
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=1"; }
        }
    }

    public class C1003_10_3PDF : Abstract1003PDF
    {

        public override string PdfFile
        {
            get { return "1003_10_3.pdf"; }
        }
        public override string Description
        {
            get { return "Page 3"; }
        }

        public override string EditLink
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=2"; }
        }

    }

    public class CFinal1003_10_3PDF : C1003_10_3PDF
    {
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            // Apply all of the form data.
            base.ApplyData(dataLoan, dataApp);
            // Wipe out the interview date.
            AddFormFieldData("App1003InterviewerPrepareDate", "");
        }
    }
    
    public class CFinal1003_17_3PDF : C1003_17_3PDF
    {
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            // Apply all of the form data.
            base.ApplyData(dataLoan, dataApp);
            // Wipe out the interview date.
            AddFormFieldData("App1003InterviewerPrepareDate", "");
        }
    }

    public class C1003_10_4PDF : Abstract1003PDF
    {

        public override string PdfFile
        {
            get { return "1003_10_4.pdf"; }
        }
        public override string Description
        {
            get { return "Page 4"; }
        }

        public override string EditLink
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=3"; }
        }

    }

    /// <summary>
    /// Assets & Liabilities continuation sheet.
    /// </summary>
    public class C1003_10_4aPDF : Abstract1003_4aPDF
    {
        public override string Description
        {
            get { return "Page 4 : Liabilities / Assets - "; }
        }
        public override string PdfFile
        {
            get { return "1003_10_4a.pdf"; }
        }
    }

    /// <summary>
    /// REO continuation sheet.
    /// </summary>
    public class C1003_10_4bPDF : Abstract1003_4bPDF
    {
        public override string Description
        {
            get { return "Page 4 : Real Estate Owned - "; }
        }
        public override string PdfFile
        {
            get { return "1003_10_4b.pdf"; }
        }

    }

    /// <summary>
    /// Stock & bond continuation sheet
    /// </summary>
    public class C1003_10_4cPDF : Abstract1003_4cPDF
    {
        public override string Description
        {
            get { return "Page 4 : Stocks &amp; Bonds - "; }
        }
        public override string PdfFile
        {
            get { return "1003_10_4c.pdf"; }
        }

    }

    /// <summary>
    /// Automobiles continuation sheet
    /// </summary>
    public class C1003_10_4dPDF : Abstract1003_4dPDF
    {
        public override string Description
        {
            get { return "Page 4 : Automobiles - "; }
        }
        public override string PdfFile
        {
            get { return "1003_10_4d.pdf"; }
        }
    }

    /// <summary>
    /// Other asset continuation sheet
    /// </summary>
    public class C1003_10_4ePDF : Abstract1003_4ePDF
    {
        public override string Description
        {
            get { return "Page 4 : Other Assets - "; }
        }
        public override string PdfFile
        {
            get { return "1003_10_4e.pdf"; }
        }

    }

    /// <summary>
    /// Employment continuation sheet
    /// </summary>
    public class C1003_10_4fPDF : Abstract1003_4fPDF
    {
        public override string Description
        {
            get { return "Page 4 : Employments - "; }
        }

        public override string PdfFile
        {
            get { return "1003_10_4f.pdf"; }
        }
    }
    /// <summary>
    /// The second former address info for the new 1003.
    /// </summary>
    public class C1003_10_4gPDF : Abstract1003_4gPDF
    {
        public override string Description
        {
            get { return "Page 4 : Former address - "; }
        }
        public override string PdfFile
        {
            get { return "1003_10_4g.pdf"; }
        }


    }

    /// <summary>
    /// OtherIncome continuation sheet
    /// </summary>
    public class C1003_10_4hPDF : Abstract1003_4hPDF
    {
        public override string Description
        {
            get { return "Page 4 : Other Income - "; }
        }
        public override string PdfFile
        {
            get { return "1003_10_4h.pdf"; }
        }
    }

    #endregion


    #region 1003 after 01/2006 (Letter size) 
    public class C1003_06Letter_1PDF : Abstract1003PDF 
    {
        public override string PdfFile 
        {
            get { return "1003_06Letter_1.pdf"; }
        }
        public override LendersOffice.PdfGenerator.PDFPageSize PageSize 
        {
            get { return LendersOffice.PdfGenerator.PDFPageSize.Letter; }
        }
        public override string Description 
        {
            get { return "Page 1"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/Loan1003.aspx"; }
        }
    }

    public class C1003_06Letter_2PDF : Abstract1003PDF 
    {
        public override string PdfFile 
        {
            get { return "1003_06Letter_2.pdf"; }
        }
        public override LendersOffice.PdfGenerator.PDFPageSize PageSize 
        {
            get { return LendersOffice.PdfGenerator.PDFPageSize.Letter; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=1"; }
        }
    }

    public class C1003_06Letter_3PDF : Abstract1003PDF 
    {
        public override string PdfFile 
        {
            get { return "1003_06Letter_3.pdf"; }
        }
        public override LendersOffice.PdfGenerator.PDFPageSize PageSize 
        {
            get { return LendersOffice.PdfGenerator.PDFPageSize.Letter; }
        }
        public override string Description 
        {
            get { return "Page 3"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=2"; }
        }

    }

    public class C1003_06Letter_4PDF : Abstract1003PDF
    {
        public override string PdfFile 
        {
            get { return "1003_06Letter_4.pdf"; }
        }
        public override LendersOffice.PdfGenerator.PDFPageSize PageSize 
        {
            get { return LendersOffice.PdfGenerator.PDFPageSize.Letter; }
        }
        public override string Description 
        {
            get { return "Page 4"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=2"; }
        }

    }
    public class C1003_06Letter_5PDF : Abstract1003PDF 
    {
        public override string PdfFile 
        {
            get { return "1003_06Letter_5.pdf"; }
        }
        public override LendersOffice.PdfGenerator.PDFPageSize PageSize 
        {
            get { return LendersOffice.PdfGenerator.PDFPageSize.Letter; }
        }
        public override string Description 
        {
            get { return "Page 5"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=3"; }
        }
    
    }

    public class C1003_06Letter_5aPDF : Abstract1003_4aPDF 
    {
        public override string Description 
        {
            get { return "Page 5 : Liabilities / Assets - "; }
        }
        public override string PdfFile 
        {
            get { return "1003_06Letter_5a.pdf"; }
        }
        public override bool IsVisible 
        {
            get 
            {
                if (null == m_dataApp) 
                {
                    return false;
                }
                var liabilityList = m_dataApp.aLiaCollection.ExtraNormalLiabilitiesInNew1003;
                var assetList = m_dataApp.aAssetCollection.ExtraNormalAssets;
        
                return liabilityList.Count > 0 || assetList.Count > 0;
            }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            var liabilityList = dataApp.aLiaCollection.ExtraNormalLiabilitiesInNew1003;
            var assetList = dataApp.aAssetCollection.ExtraNormalAssets;
        
            int extraRecordCount = liabilityList.Count < assetList.Count ? assetList.Count : liabilityList.Count;
        
            for (int i = 0; i < extraRecordCount; i++) 
            {
                if (i % 7 == 0) 
                {
                    if (i != 0) AddNewPage();
        
                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("sLNm", "Loan Number" + dataLoan.sLNm);
                    AddFormFieldData("aBNm", dataApp.aBNm); 
                    AddFormFieldData("aCNm", dataApp.aCNm); 
        
                }
                if (i < liabilityList.Count) 
                {
                    ILiabilityRegular field = (ILiabilityRegular) liabilityList.GetRecordAt(i);
        
                    string prefix = "Liability" + (i % 7);
        
                    // If company name is blank then use description as first line, and blank out
                    // description on last line.
                    string name = field.ComNm.TrimWhitespaceAndBOM() == "" ? field.Desc : field.ComNm;
                    string description = field.ComNm.TrimWhitespaceAndBOM() == "" ? "" : field.Desc;
        
                    AddFormFieldData(prefix + "Name", Tools.FormatAddress(name, field.ComAddr, field.ComCity, field.ComState, field.ComZip, description));
                    AddFormFieldData(prefix + "AcctNumber", field.AccNum.Value);
                    AddFormFieldData(prefix + "Monthly", field.PmtRemainMons_rep1003);
                    AddFormFieldData(prefix + "Balance", field.Bal_rep);
                }
                if (i < assetList.Count) 
                {
                    var field = (IAssetRegular)assetList.GetRecordAt(i);
                    string prefix = "Asset" + (i % 7);
                    //OPM 60497: Net Equity / Net Sales Proceed's "bank" is always "Net Equity"
                    if (field.AssetT == E_AssetRegularT.PendingNetSaleProceedsFromRealEstateAssets)
                    {
                        AddFormFieldData(prefix + "Bank", "Net Equity");
                    }
                    else
                    {
                        AddFormFieldData(prefix + "Bank", Tools.FormatAddress(field.ComNm.TrimWhitespaceAndBOM() == "" ? field.Desc : field.ComNm, field.StAddr, field.City, field.State, field.Zip));
                    }
                    AddFormFieldData(prefix + "AcctNumber", field.AccNum.Value);
                    AddFormFieldData(prefix + "Value", field.Val_rep);
                }
            }
        }
    }

    /// <summary>
    /// REO continuation sheet.
    /// </summary>
    public class C1003_06Letter_5bPDF : Abstract1003_4bPDF  
    {
        public override string Description 
        {
            get { return "Page 5 : Real Estate Owned - "; }
        }
        public override string PdfFile 
        {
            get { return "1003_06Letter_5b.pdf"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            var subcoll = dataApp.aReCollection.GetSubcollection(true, E_ReoGroupT.All);
            int extraRealEstateCount = subcoll.Count - 3;

            for (int index = 0; index < extraRealEstateCount; index++)
            {
                if (index % 18 == 0)
                {
                    if (index != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("aBNm", dataApp.aBNm);
                    AddFormFieldData("aCNm", dataApp.aCNm);
                    AddFormFieldData("sLNm", "Loan Number " + dataLoan.sLNm);
                }
                var field = (IRealEstateOwned)subcoll.GetRecordAt(index + 3);

                string prefix = "RE" + (index % 18);
                string name = Tools.FormatAddress(field.Addr, field.City, field.State, field.Zip);

                AddFormFieldData(prefix, name);
                AddFormFieldData(prefix + "Val", field.Val_rep);
                AddFormFieldData(prefix + "MAmt", field.MAmt_rep);
                AddFormFieldData(prefix + "GrossRentI", field.GrossRentI_rep);
                AddFormFieldData(prefix + "MPmt", field.MPmt_rep);
                AddFormFieldData(prefix + "HExp", field.HExp_rep);
                AddFormFieldData(prefix + "NetRentI", field.NetRentI_rep);
                AddFormFieldData(prefix + "Type", field.Type);
                AddFormFieldData(prefix + "Stat", field.Stat);
            }
        }
    }

    /// <summary>
    /// Stock & bond continuation sheet
    /// </summary>
    public class C1003_06Letter_5cPDF : Abstract1003_4cPDF
    {
        public override string Description 
        {
            get { return "Page 5 : Stocks &amp; Bonds - "; }
        }
        public override string PdfFile 
        {
            get { return "1003_06Letter_5c.pdf"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            IAssetCollection assetCollection = dataApp.aAssetCollection;
            if (!assetCollection.HasExtraStocks)
            {
                /// Only print stocks on additional page if there are more than 3 stocks.
                return;
            }

            var stockList = assetCollection.StockCollection;

            int count = stockList.Count;

            for (int index = 0; index < count; index++)
            {
                if (index % 17 == 0)
                {
                    if (index != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("sLNm", "Loan Number " + dataLoan.sLNm);
                    AddFormFieldData("aBNm", dataApp.aBNm);
                    AddFormFieldData("aCNm", dataApp.aCNm);
                }
                var field = (IAsset)stockList.GetRecordAt(index);

                int prefixIndex = index % 17;
                switch (field.OwnerT)
                {
                    case E_AssetOwnerT.Borrower:
                        AddFormFieldData("owner" + prefixIndex, "B");
                        break;
                    case E_AssetOwnerT.CoBorrower:
                        AddFormFieldData("owner" + prefixIndex, "C");
                        break;
                    case E_AssetOwnerT.Joint:
                        AddFormFieldData("owner" + prefixIndex, "J");
                        break;
                }
                AddFormFieldData("stock" + prefixIndex, field.Desc);
                AddFormFieldData("stock" + prefixIndex + "Value", field.Val_rep);
            }
        }
    }

    /// <summary>
    /// Automobiles continuation sheet
    /// </summary>
    public class C1003_06Letter_5dPDF : Abstract1003_4dPDF 
    {
        public override string Description 
        {
            get { return "Page 5 : Automobiles - "; }
        }
        public override string PdfFile 
        {
            get { return "1003_06Letter_5d.pdf"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            if (!dataApp.aAssetCollection.HasExtraAutomobiles)
            {
                /// Only print auto on additional page if there are more than 3 autos.
                return;
            }
            var autoList = dataApp.aAssetCollection.GetSubcollection(true, E_AssetGroupT.Auto);
            int count = autoList.Count;
            for (int index = 0; index < count; index++)
            {
                if (index % 16 == 0)
                {
                    if (index != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("sLNm", "Loan Number " + dataLoan.sLNm);
                    AddFormFieldData("aBNm", dataApp.aBNm);
                    AddFormFieldData("aCNm", dataApp.aCNm);
                }
                var field = (IAsset)autoList.GetRecordAt(index);

                int prefixIndex = index % 16;
                switch (field.OwnerT)
                {
                    case E_AssetOwnerT.Borrower:
                        AddFormFieldData("owner" + prefixIndex, "B");
                        break;
                    case E_AssetOwnerT.CoBorrower:
                        AddFormFieldData("owner" + prefixIndex, "C");
                        break;
                    case E_AssetOwnerT.Joint:
                        AddFormFieldData("owner" + prefixIndex, "J");
                        break;
                }
                AddFormFieldData("auto" + prefixIndex, field.Desc);
                AddFormFieldData("auto" + prefixIndex + "Value", field.Val_rep);
            }
        }
        
    }

    /// <summary>
    /// Other asset continuation sheet
    /// </summary>
    public class C1003_06Letter_5ePDF : Abstract1003_4ePDF 
    {
        public override string Description 
        {
            get { return "Page 5 : Other Assets - "; }
        }
        public override string PdfFile 
        {
            get { return "1003_06Letter_5e.pdf"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            IAssetCollection assetCollection = dataApp.aAssetCollection;
            if (!assetCollection.HasExtraOtherAssets)
            {
                /// Only print itemized assets on additional page if there are more than 3 itemized assets.
                return;
            }

            var otherAssetList = assetCollection.OtherAssetCollection;
            int count = otherAssetList.Count;
            for (int index = 0; index < count; index++)
            {
                if (index % 17 == 0)
                {
                    if (index != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("sLNm", "Loan Number " + dataLoan.sLNm);
                    AddFormFieldData("aBNm", dataApp.aBNm);
                    AddFormFieldData("aCNm", dataApp.aCNm);
                }
                var field = (IAsset)otherAssetList.GetRecordAt(index);

                int prefixIndex = index % 17;
                switch (field.OwnerT)
                {
                    case E_AssetOwnerT.Borrower:
                        AddFormFieldData("owner" + prefixIndex, "B");
                        break;
                    case E_AssetOwnerT.CoBorrower:
                        AddFormFieldData("owner" + prefixIndex, "C");
                        break;
                    case E_AssetOwnerT.Joint:
                        AddFormFieldData("owner" + prefixIndex, "J");
                        break;
                }
                AddFormFieldData("otherAsset" + prefixIndex, field.Desc);
                AddFormFieldData("otherAsset" + prefixIndex + "Value", field.Val_rep);
            }
        }
        
    }

    /// <summary>
    /// Employment continuation sheet
    /// </summary>
    public class C1003_06Letter_5fPDF : Abstract1003_4fPDF
    {
        public override string Description 
        {
            get { return "Page 5 : Employments - "; }
        }

        public override string PdfFile 
        {
            get { return "1003_06Letter_5f.pdf"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            var extraBorrowerEmployments = dataApp.aBEmpCollection.ExtraPreviousEmployments;
            var extraCoborrowerEmployments = dataApp.aCEmpCollection.ExtraPreviousEmployments;

            int extraEmploymentRecordCount = extraBorrowerEmployments.Count > extraCoborrowerEmployments.Count ? extraBorrowerEmployments.Count : extraCoborrowerEmployments.Count;

            for (int extraIndex = 0; extraIndex < extraEmploymentRecordCount; extraIndex++)
            {
                if (extraIndex % 6 == 0)
                {
                    if (extraIndex != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("sLNm", "Loan Number " + dataLoan.sLNm);
                    AddFormFieldData("aBNm", dataApp.aBNm);
                    AddFormFieldData("aCNm", dataApp.aCNm);
                }

                for (int i = 0; i < 2; i++)
                {
                    var list = (i == 0) ? extraBorrowerEmployments : extraCoborrowerEmployments;
                    if (extraIndex < list.Count)
                    {
                        string ch = (i == 0) ? "B" : "C";
                        int j = extraIndex % 6;
                        IRegularEmploymentRecord f = (IRegularEmploymentRecord)list.GetRecordAt(extraIndex);
                        AddFormFieldData("a" + ch + "IsSelfEmplmt" + j, f.IsSelfEmplmt);
                        AddFormFieldData("a" + ch + "EmplrBusPhone" + j, f.EmplrBusPhone);
                        AddFormFieldData("a" + ch + "JobTitle" + j, f.JobTitle);
                        AddFormFieldData("a" + ch + "EmplrAddr" + j, Tools.FormatAddress(f.EmplrNm, f.EmplrAddr, f.EmplrCity, f.EmplrState, f.EmplrZip));
                        AddFormFieldData("a" + ch + "EmplrDuration" + j, f.EmplmtStartD_rep + Environment.NewLine + f.EmplmtEndD_rep);
                        AddFormFieldData("a" + ch + "MonI" + j, f.MonI_rep);
                    }
                }
            }
        }

        
    }
    /// <summary>
    /// The second former address info for the new 1003.
    /// </summary>
    public class C1003_06Letter_5gPDF : Abstract1003_4gPDF 
    {
        public override string Description 
        {
            get { return "Page 5 : Former address - "; }
        }
        public override string PdfFile 
        {
            get { return "1003_06Letter_5g.pdf"; }
        }
    }

    #endregion

    #region 1003 after 07/2010 (Letter size)
    public class C1003_10Letter_1PDF : Abstract1003PDF
    {
        public override string PdfFile
        {
            get { return "1003_10Letter_1.pdf"; }
        }
        public override LendersOffice.PdfGenerator.PDFPageSize PageSize
        {
            get { return LendersOffice.PdfGenerator.PDFPageSize.Letter; }
        }
        public override string Description
        {
            get { return "Page 1"; }
        }
        public override string EditLink
        {
            get { return "/newlos/Forms/Loan1003.aspx"; }
        }
    }

    public class C1003_10Letter_2PDF : Abstract1003PDF
    {
        public override string PdfFile
        {
            get { return "1003_10Letter_2.pdf"; }
        }
        public override LendersOffice.PdfGenerator.PDFPageSize PageSize
        {
            get { return LendersOffice.PdfGenerator.PDFPageSize.Letter; }
        }
        public override string Description
        {
            get { return "Page 2"; }
        }
        public override string EditLink
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=1"; }
        }
    }

    public class C1003_10Letter_3PDF : Abstract1003PDF
    {
        public override string PdfFile
        {
            get { return "1003_10Letter_3.pdf"; }
        }
        public override LendersOffice.PdfGenerator.PDFPageSize PageSize
        {
            get { return LendersOffice.PdfGenerator.PDFPageSize.Letter; }
        }
        public override string Description
        {
            get { return "Page 3"; }
        }
        public override string EditLink
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=2"; }
        }

    }

    public class CFinal1003_10Letter_4PDF : C1003_10Letter_4PDF
    {
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            // Apply all of the form data.
            base.ApplyData(dataLoan, dataApp);
            // Wipe out the interview date.
            AddFormFieldData("App1003InterviewerPrepareDate", "");
        }
    }

    public class CFinal1003_17Letter_4PDF : C1003_17Letter_4PDF
    {
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            // Apply all of the form data.
            base.ApplyData(dataLoan, dataApp);
            // Wipe out the interview date.
            AddFormFieldData("App1003InterviewerPrepareDate", "");
        }
    }

    public class C1003_10Letter_4PDF : Abstract1003PDF
    {
        public override string PdfFile
        {
            get { return "1003_10Letter_4.pdf"; }
        }
        public override LendersOffice.PdfGenerator.PDFPageSize PageSize
        {
            get { return LendersOffice.PdfGenerator.PDFPageSize.Letter; }
        }
        public override string Description
        {
            get { return "Page 4"; }
        }
        public override string EditLink
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=2"; }
        }

    }
    public class C1003_10Letter_5PDF : Abstract1003PDF
    {
        public override string PdfFile
        {
            get { return "1003_10Letter_5.pdf"; }
        }
        public override LendersOffice.PdfGenerator.PDFPageSize PageSize
        {
            get { return LendersOffice.PdfGenerator.PDFPageSize.Letter; }
        }
        public override string Description
        {
            get { return "Page 5"; }
        }
        public override string EditLink
        {
            get { return "/newlos/Forms/Loan1003.aspx?pg=3"; }
        }

    }

    public class C1003_10Letter_5aPDF : Abstract1003_4aPDF
    {
        public override string Description
        {
            get { return "Page 5 : Liabilities / Assets - "; }
        }
        public override string PdfFile
        {
            get { return "1003_10Letter_5a.pdf"; }
        }
        public override bool IsVisible
        {
            get
            {
                if (null == m_dataApp)
                {
                    return false;
                }
                var liabilityList = m_dataApp.aLiaCollection.ExtraNormalLiabilitiesInNew1003;
                var assetList = m_dataApp.aAssetCollection.ExtraNormalAssets;

                return liabilityList.Count > 0 || assetList.Count > 0;
            }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            var liabilityList = dataApp.aLiaCollection.ExtraNormalLiabilitiesInNew1003;
            var assetList = dataApp.aAssetCollection.ExtraNormalAssets;

            int extraRecordCount = liabilityList.Count < assetList.Count ? assetList.Count : liabilityList.Count;

            for (int i = 0; i < extraRecordCount; i++)
            {
                if (i % 7 == 0)
                {
                    if (i != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("sLNm", this.AddLoanNumberData(dataLoan));
                    AddFormFieldData("aBNm", dataApp.aBNm);
                    AddFormFieldData("aCNm", dataApp.aCNm);

                }
                if (i < liabilityList.Count)
                {
                    ILiabilityRegular field = (ILiabilityRegular)liabilityList.GetRecordAt(i);

                    string prefix = "Liability" + (i % 7);

                    // If company name is blank then use description as first line, and blank out
                    // description on last line.
                    string name = field.ComNm.TrimWhitespaceAndBOM() == "" ? field.Desc : field.ComNm;
                    string description = field.ComNm.TrimWhitespaceAndBOM() == "" ? "" : field.Desc;

                    AddFormFieldData(prefix + "Name", Tools.FormatAddress(name, field.ComAddr, field.ComCity, field.ComState, field.ComZip, description));
                    AddFormFieldData(prefix + "AcctNumber", field.AccNum.Value);
                    AddFormFieldData(prefix + "Monthly", field.PmtRemainMons_rep1003);
                    AddFormFieldData(prefix + "Balance", field.Bal_rep);
                }
                if (i < assetList.Count)
                {
                    var field = (IAssetRegular)assetList.GetRecordAt(i);
                    string prefix = "Asset" + (i % 7);
                    //OPM 60497: Net Equity / Net Sales Proceed's "bank" is always "Net Equity"
                    if (field.AssetT == E_AssetRegularT.PendingNetSaleProceedsFromRealEstateAssets)
                    {
                        AddFormFieldData(prefix + "Bank", "Net Equity");
                    }
                    else
                    {
                        AddFormFieldData(prefix + "Bank", Tools.FormatAddress(field.ComNm.TrimWhitespaceAndBOM() == "" ? field.Desc : field.ComNm, field.StAddr, field.City, field.State, field.Zip));
                    }
                    AddFormFieldData(prefix + "AcctNumber", field.AccNum.Value);
                    AddFormFieldData(prefix + "Value", field.Val_rep);
                }
            }
        }
    }

    /// <summary>
    /// REO continuation sheet.
    /// </summary>
    public class C1003_10Letter_5bPDF : Abstract1003_4bPDF
    {
        public override string Description
        {
            get { return "Page 5 : Real Estate Owned - "; }
        }
        public override string PdfFile
        {
            get { return "1003_10Letter_5b.pdf"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            var subcoll = dataApp.aReCollection.GetSubcollection(true, E_ReoGroupT.All);
            int extraRealEstateCount = subcoll.Count - 3;

            for (int index = 0; index < extraRealEstateCount; index++)
            {
                if (index % 18 == 0)
                {
                    if (index != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("aBNm", dataApp.aBNm);
                    AddFormFieldData("aCNm", dataApp.aCNm);
                    AddFormFieldData("sLNm", this.AddLoanNumberData(dataLoan));
                }
                var field = (IRealEstateOwned)subcoll.GetRecordAt(index + 3);

                string prefix = "RE" + (index % 18);
                string name = Tools.FormatAddress(field.Addr, field.City, field.State, field.Zip);

                AddFormFieldData(prefix, name);
                AddFormFieldData(prefix + "Val", field.Val_rep);
                AddFormFieldData(prefix + "MAmt", field.MAmt_rep);
                AddFormFieldData(prefix + "GrossRentI", field.GrossRentI_rep);
                AddFormFieldData(prefix + "MPmt", field.MPmt_rep);
                AddFormFieldData(prefix + "HExp", field.HExp_rep);
                AddFormFieldData(prefix + "NetRentI", field.NetRentI_rep);
                AddFormFieldData(prefix + "Type", field.Type);
                AddFormFieldData(prefix + "Stat", field.Stat);
            }
        }
    }

    /// <summary>
    /// Stock & bond continuation sheet
    /// </summary>
    public class C1003_10Letter_5cPDF : Abstract1003_4cPDF
    {
        public override string Description
        {
            get { return "Page 5 : Stocks &amp; Bonds - "; }
        }
        public override string PdfFile
        {
            get { return "1003_10Letter_5c.pdf"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            IAssetCollection assetCollection = dataApp.aAssetCollection;
            if (!assetCollection.HasExtraStocks)
            {
                /// Only print stocks on additional page if there are more than 3 stocks.
                return;
            }

            var stockList = assetCollection.StockCollection;

            int count = stockList.Count;

            for (int index = 0; index < count; index++)
            {
                if (index % 17 == 0)
                {
                    if (index != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("sLNm", this.AddLoanNumberData(dataLoan));
                    AddFormFieldData("aBNm", dataApp.aBNm);
                    AddFormFieldData("aCNm", dataApp.aCNm);
                }
                var field = (IAsset)stockList.GetRecordAt(index);

                int prefixIndex = index % 17;
                switch (field.OwnerT)
                {
                    case E_AssetOwnerT.Borrower:
                        AddFormFieldData("owner" + prefixIndex, "B");
                        break;
                    case E_AssetOwnerT.CoBorrower:
                        AddFormFieldData("owner" + prefixIndex, "C");
                        break;
                    case E_AssetOwnerT.Joint:
                        AddFormFieldData("owner" + prefixIndex, "J");
                        break;
                }
                AddFormFieldData("stock" + prefixIndex, field.Desc);
                AddFormFieldData("stock" + prefixIndex + "Value", field.Val_rep);
            }
        }
    }

    /// <summary>
    /// Automobiles continuation sheet
    /// </summary>
    public class C1003_10Letter_5dPDF : Abstract1003_4dPDF
    {
        public override string Description
        {
            get { return "Page 5 : Automobiles - "; }
        }
        public override string PdfFile
        {
            get { return "1003_10Letter_5d.pdf"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            if (!dataApp.aAssetCollection.HasExtraAutomobiles)
            {
                /// Only print auto on additional page if there are more than 3 autos.
                return;
            }
            var autoList = dataApp.aAssetCollection.GetSubcollection(true, E_AssetGroupT.Auto);
            int count = autoList.Count;
            for (int index = 0; index < count; index++)
            {
                if (index % 16 == 0)
                {
                    if (index != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("sLNm", this.AddLoanNumberData(dataLoan));
                    AddFormFieldData("aBNm", dataApp.aBNm);
                    AddFormFieldData("aCNm", dataApp.aCNm);
                }
                var field = (IAsset)autoList.GetRecordAt(index);

                int prefixIndex = index % 16;
                switch (field.OwnerT)
                {
                    case E_AssetOwnerT.Borrower:
                        AddFormFieldData("owner" + prefixIndex, "B");
                        break;
                    case E_AssetOwnerT.CoBorrower:
                        AddFormFieldData("owner" + prefixIndex, "C");
                        break;
                    case E_AssetOwnerT.Joint:
                        AddFormFieldData("owner" + prefixIndex, "J");
                        break;
                }
                AddFormFieldData("auto" + prefixIndex, field.Desc);
                AddFormFieldData("auto" + prefixIndex + "Value", field.Val_rep);
            }
        }

    }

    /// <summary>
    /// Other asset continuation sheet
    /// </summary>
    public class C1003_10Letter_5ePDF : Abstract1003_4ePDF
    {
        public override string Description
        {
            get { return "Page 5 : Other Assets - "; }
        }
        public override string PdfFile
        {
            get { return "1003_10Letter_5e.pdf"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            IAssetCollection assetCollection = dataApp.aAssetCollection;
            if (!assetCollection.HasExtraOtherAssets)
            {
                /// Only print itemized assets on additional page if there are more than 3 itemized assets.
                return;
            }

            var otherAssetList = assetCollection.OtherAssetCollection;
            int count = otherAssetList.Count;
            for (int index = 0; index < count; index++)
            {
                if (index % 17 == 0)
                {
                    if (index != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("sLNm", this.AddLoanNumberData(dataLoan));
                    AddFormFieldData("aBNm", dataApp.aBNm);
                    AddFormFieldData("aCNm", dataApp.aCNm);
                }
                var field = (IAsset)otherAssetList.GetRecordAt(index);

                int prefixIndex = index % 17;
                switch (field.OwnerT)
                {
                    case E_AssetOwnerT.Borrower:
                        AddFormFieldData("owner" + prefixIndex, "B");
                        break;
                    case E_AssetOwnerT.CoBorrower:
                        AddFormFieldData("owner" + prefixIndex, "C");
                        break;
                    case E_AssetOwnerT.Joint:
                        AddFormFieldData("owner" + prefixIndex, "J");
                        break;
                }
                AddFormFieldData("otherAsset" + prefixIndex, field.Desc);
                AddFormFieldData("otherAsset" + prefixIndex + "Value", field.Val_rep);
            }
        }

    }

    /// <summary>
    /// Employment continuation sheet
    /// </summary>
    public class C1003_10Letter_5fPDF : Abstract1003_4fPDF
    {
        public override string Description
        {
            get { return "Page 5 : Employments - "; }
        }

        public override string PdfFile
        {
            get { return "1003_10Letter_5f.pdf"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            var extraBorrowerEmployments = dataApp.aBEmpCollection.ExtraPreviousEmployments;
            var extraCoborrowerEmployments = dataApp.aCEmpCollection.ExtraPreviousEmployments;

            int extraEmploymentRecordCount = extraBorrowerEmployments.Count > extraCoborrowerEmployments.Count ? extraBorrowerEmployments.Count : extraCoborrowerEmployments.Count;

            for (int extraIndex = 0; extraIndex < extraEmploymentRecordCount; extraIndex++)
            {
                if (extraIndex % 6 == 0)
                {
                    if (extraIndex != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("sLNm", this.AddLoanNumberData(dataLoan));
                    AddFormFieldData("aBNm", dataApp.aBNm);
                    AddFormFieldData("aCNm", dataApp.aCNm);
                }

                for (int i = 0; i < 2; i++)
                {
                    var list = (i == 0) ? extraBorrowerEmployments : extraCoborrowerEmployments;
                    if (extraIndex < list.Count)
                    {
                        string ch = (i == 0) ? "B" : "C";
                        int j = extraIndex % 6;
                        IRegularEmploymentRecord f = (IRegularEmploymentRecord)list.GetRecordAt(extraIndex);
                        AddFormFieldData("a" + ch + "IsSelfEmplmt" + j, f.IsSelfEmplmt);
                        AddFormFieldData("a" + ch + "EmplrBusPhone" + j, f.EmplrBusPhone);
                        AddFormFieldData("a" + ch + "JobTitle" + j, f.JobTitle);
                        AddFormFieldData("a" + ch + "EmplrAddr" + j, Tools.FormatAddress(f.EmplrNm, f.EmplrAddr, f.EmplrCity, f.EmplrState, f.EmplrZip));
                        AddFormFieldData("a" + ch + "EmplrDuration" + j, f.EmplmtStartD_rep + Environment.NewLine + f.EmplmtEndD_rep);
                        AddFormFieldData("a" + ch + "MonI" + j, f.MonI_rep);
                    }
                }
            }
        }


    }
    /// <summary>
    /// Other Income continuation sheet
    /// </summary>
    public class C1003_10Letter_5hPDF : Abstract1003_4hPDF
    {
        public override string Description
        {
            get { return "Page 5 : Other Income - "; }
        }
        public override string PdfFile
        {
            get { return "1003_10Letter_5h.pdf"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            var otherIncomeData = Abstract1003PDF.CreateOtherIncomeData(dataApp.aBSpPosCf, dataApp.aCSpPosCf, dataApp.aOtherIncomeList);
            if (otherIncomeData.Count <= 3)
            {
                /// Only print auto on additional page if there are more than 3 autos.
                return;
            }

            int count = otherIncomeData.Count;

            for (int index = 0; index < count; index++)
            {
                if (index % 16 == 0)
                {
                    if (index != 0) AddNewPage();

                    AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
                    AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
                    AddFormFieldData("sLNm", this.AddLoanNumberData(dataLoan));
                    AddFormFieldData("aBNm", dataApp.aBNm);
                    AddFormFieldData("aCNm", dataApp.aCNm);
                }
                OtherIncome income = otherIncomeData[index];

                int prefixIndex = index % 16;
                AddFormFieldData("iowner" + prefixIndex, income.IsForCoBorrower ? "C" : "B");

                AddFormFieldData("income" + prefixIndex, income.Desc);
                AddFormFieldData("income" + prefixIndex + "Value", dataApp.m_convertLos.ToMoneyString(income.Amount, FormatDirection.ToRep));
            }
        }

    }


    /// <summary>
    /// The second former address info for the new 1003.
    /// </summary>
    public class C1003_10Letter_5gPDF : Abstract1003_4gPDF
    {
        public override string Description
        {
            get { return "Page 5 : Former address - "; }
        }
        public override string PdfFile
        {
            get { return "1003_10Letter_5g.pdf"; }
        }
    }

    /// <summary>
    /// Employment continuation sheet
    /// </summary>
    public class C1003_17GmiAddendum : AbstractUliPdf
    {

        public override E_AppPrintModeT AppPrintModeT
        {
            get { return E_AppPrintModeT.BorrowerAndSpouseSeparately; }
        }

        public override string Description
        {
            get { return "GMI Addendum"; }
        }

        public override string PdfFile
        {
            get { return "1003_GMI_Addendum.pdf"; }
        }

        public override bool ShouldRenderCoBorrower
        {
            get
            {
                return !string.IsNullOrEmpty(m_dataApp.aCNm) || m_dataApp.aCIsDefined;
            }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            AddFormFieldData("sLNm", this.AddLoanNumberData(dataLoan));

            var applicantGender = IsBorrower ? dataApp.aBGender : dataApp.aCGender;
            var maleGenders = new List<E_GenderT>() { E_GenderT.Male, E_GenderT.MaleAndFemale, E_GenderT.MaleAndNotFurnished, E_GenderT.MaleFemaleNotFurnished };
            var femaleGenders = new List<E_GenderT>() { E_GenderT.Female, E_GenderT.FemaleAndNotFurnished, E_GenderT.MaleAndFemale, E_GenderT.MaleFemaleNotFurnished };
            var unfurnishedGenders = new List<E_GenderT>() { E_GenderT.MaleAndNotFurnished, E_GenderT.MaleFemaleNotFurnished, E_GenderT.FemaleAndNotFurnished, E_GenderT.Unfurnished };


            AddFormFieldData("aGender:1", maleGenders.Contains(applicantGender) ? "1" : "");
            AddFormFieldData("aGender:2", femaleGenders.Contains(applicantGender) ? "2" : "");
            AddFormFieldData("aGender:4", unfurnishedGenders.Contains(applicantGender) ? "4" : "");

            AddFormFieldData("aDoesNotWishToProvideRace", IsBorrower ? dataApp.aBDoesNotWishToProvideRace : dataApp.aCDoesNotWishToProvideRace);
            AddFormFieldData("aIsGuamanianOrChamorro", IsBorrower ? dataApp.aBIsGuamanianOrChamorro : dataApp.aCIsGuamanianOrChamorro);
            AddFormFieldData("aIsOtherAsian", IsBorrower ? dataApp.aBIsOtherAsian : dataApp.aCIsOtherAsian);
            AddFormFieldData("aIsPuertoRican", IsBorrower ? dataApp.aBIsPuertoRican : dataApp.aCIsPuertoRican);
            AddFormFieldData("aIsBlack", IsBorrower ? dataApp.aBIsBlack : dataApp.aCIsBlack);
            AddFormFieldData("aIsAsian", IsBorrower ? dataApp.aBIsAsian : dataApp.aCIsAsian);
            AddFormFieldData("aInterviewMethodT", IsBorrower ? dataApp.aBInterviewMethodT : dataApp.aCInterviewMethodT);
            AddFormFieldData("aDoesNotWishToProvideEthnicity", IsBorrower ? dataApp.aBDoesNotWishToProvideEthnicity : dataApp.aCDoesNotWishToProvideEthnicity);
            AddFormFieldData("aOtherHispanicOrLatinoDescription", IsBorrower ? dataApp.aBOtherHispanicOrLatinoDescription : dataApp.aCOtherHispanicOrLatinoDescription);
            
            AddFormFieldData("aIsNativeHawaiian", IsBorrower ? dataApp.aBIsNativeHawaiian : dataApp.aCIsNativeHawaiian);
            AddFormFieldData("aIsAmericanIndian", IsBorrower ? dataApp.aBIsAmericanIndian : dataApp.aCIsAmericanIndian);
            AddFormFieldData("aIsChinese", IsBorrower ? dataApp.aBIsChinese : dataApp.aCIsChinese);
            AddFormFieldData("aBIsCuban", IsBorrower ? dataApp.aBIsCuban : dataApp.aCIsCuban);
            AddFormFieldData("aOtherPacificIslanderDescription", IsBorrower ? dataApp.aBOtherPacificIslanderDescription : dataApp.aCOtherPacificIslanderDescription);
            AddFormFieldData("aOtherAsianDescription", IsBorrower ? dataApp.aBOtherAsianDescription : dataApp.aCOtherAsianDescription);
            AddFormFieldData("aNm", IsBorrower ? dataApp.aBNm : dataApp.aCNm);
            AddFormFieldData("aEthnicityCollectedByObservationOrSurname", IsBorrower ? dataApp.aBEthnicityCollectedByObservationOrSurname : dataApp.aCEthnicityCollectedByObservationOrSurname);
            AddFormFieldData("aSexCollectedByObservationOrSurname", IsBorrower ? dataApp.aBSexCollectedByObservationOrSurname : dataApp.aCSexCollectedByObservationOrSurname);
            AddFormFieldData("aRaceCollectedByObservationOrSurname", IsBorrower ? dataApp.aBRaceCollectedByObservationOrSurname : dataApp.aCRaceCollectedByObservationOrSurname);
            AddFormFieldData("aIsOtherPacificIslander", IsBorrower ? dataApp.aBIsOtherPacificIslander : dataApp.aCIsOtherPacificIslander);
            AddFormFieldData("aIsKorean", IsBorrower ? dataApp.aBIsKorean : dataApp.aCIsKorean);
            AddFormFieldData("aIsOtherHispanicOrLatino", IsBorrower ? dataApp.aBIsOtherHispanicOrLatino : dataApp.aCIsOtherHispanicOrLatino);
            AddFormFieldData("aIsPacificIslander", IsBorrower ? dataApp.aBIsPacificIslander : dataApp.aCIsPacificIslander);
            AddFormFieldData("aOtherAmericanIndianDescription", IsBorrower ? dataApp.aBOtherAmericanIndianDescription : dataApp.aCOtherAmericanIndianDescription);
            AddFormFieldData("aIsJapanese", IsBorrower ? dataApp.aBIsJapanese : dataApp.aCIsJapanese);
            AddFormFieldData("aIsWhite", IsBorrower ? dataApp.aBIsWhite : dataApp.aCIsWhite);
            AddFormFieldData("aIsFilipino", IsBorrower ? dataApp.aBIsFilipino : dataApp.aCIsFilipino);
            AddFormFieldData("aIsVietnamese", IsBorrower ? dataApp.aBIsVietnamese : dataApp.aCIsVietnamese);
            AddFormFieldData("aIsAsianIndian", IsBorrower ? dataApp.aBIsAsianIndian : dataApp.aCIsAsianIndian);
            AddFormFieldData("aIsMexican", IsBorrower ? dataApp.aBIsMexican : dataApp.aCIsMexican);

            var isHispanicValues = new List<E_aHispanicT> { E_aHispanicT.Hispanic, E_aHispanicT.BothHispanicAndNotHispanic };
            var notHispanicValues = new List<E_aHispanicT> { E_aHispanicT.NotHispanic, E_aHispanicT.BothHispanicAndNotHispanic };
            var applicantHispanicVal = IsBorrower ? dataApp.aBHispanicT : dataApp.aCHispanicT;

            AddFormFieldData("aHispanicT:1", isHispanicValues.Contains(applicantHispanicVal) ? "1" : "");
            AddFormFieldData("aHispanicT:2", notHispanicValues.Contains(applicantHispanicVal) ? "2" : "");

            AddFormFieldData("aIsSamoan", IsBorrower ? dataApp.aBIsSamoan : dataApp.aCIsSamoan);
        }

        private string ConvertBoolToYN(E_TriState isTrue)
        {
            switch (isTrue)
            {
                case E_TriState.Blank:
                    return "";
                case E_TriState.No:
                    return "N";
                case E_TriState.Yes:
                    return "Y";
                default:
                    return "";
            }
        }

        private bool IsBorrower
        {
            get
            {
                return this.BorrType == "B";
            }
        }
    }
    #endregion

    public class C1003_17_3PDF : C1003_10_3PDF
    {
        public override string PdfFile
        {
            get { return "1003_17_3.pdf"; }
        }
    }

    public class C1003_17Letter_4PDF : C1003_10Letter_4PDF
    {
        public override string PdfFile
        {
            get { return "1003_17Letter_4.pdf"; }
        }
    }

    public class C1003_04PDF : AbstractBatchPDF 
    {

        public override string Description 
        {
            get { return "1003 Loan Application (<b>Effective after 01/2004</b>)"; }
        }

        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            {
                return new AbstractPDFFile[] {
                                                 new C1003_04_1PDF(),
                                                 new C1003_04_2PDF(),
                                                 new C1003_04_3PDF(),
                                                 new C1003_04_4aPDF(),
                                                 new C1003_04_4bPDF(),
                                                 new C1003_04_4cPDF(),
                                                 new C1003_04_4dPDF(),
                                                 new C1003_04_4ePDF(),
                                                 new C1003_04_4fPDF(),
                                                 new C1003_04_4gPDF(),
                                                 new C1003_04_4PDF()
                                             };
            }
        }
    }

    public class C1003_06PDF : AbstractBatchPDF 
    {

        public override string Description 
        {
            get { return "1003 Loan Application (<b>Effective after 07/2005</b>, legal size printout)"; }
        }

        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            {
                return new AbstractPDFFile[] {
                                                 new C1003_06_1PDF(),
                                                 new C1003_06_2PDF(),
                                                 new C1003_06_3PDF(),
                                                 new C1003_06_4aPDF(),
                                                 new C1003_06_4bPDF(),
                                                 new C1003_06_4cPDF(),
                                                 new C1003_06_4dPDF(),
                                                 new C1003_06_4ePDF(),
                                                 new C1003_06_4fPDF(),
                                                 new C1003_06_4gPDF(),
                                                 new C1003_06_4PDF()

                                             };
            }
        }
    }

    public class C1003_10PDF : AbstractBatchPDF
    {

        public override string Description
        {
            get { return "1003 Loan Application (<b>Effective after 07/2010</b>, legal size printout)"; }
        }

        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                                                 new C1003_10_1PDF(),
                                                 new C1003_10_2PDF(),
                                                 new C1003_10_3PDF(),
                                                 new C1003_10_4aPDF(),
                                                 new C1003_10_4bPDF(),
                                                 new C1003_10_4cPDF(),
                                                 new C1003_10_4dPDF(),
                                                 new C1003_10_4ePDF(),
                                                 new C1003_10_4fPDF(),
                                                 new C1003_10_4gPDF(),
                                                 new C1003_10_4hPDF(),
                                                 new C1003_10_4PDF(),
                                             };
            }
        }
    }

    public class C1003_17PDF : AbstractBatchUliPdf
    {

        public override string Description
        {
            get { return "1003 Loan Application with Demographic Addendum (<b>Effective after 1/1/2017</b> legal size printout)"; }
        }

        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return this.ApplyBatchArguments( new AbstractPDFFile[] {
                                                 new C1003_10_1PDF(),
                                                 new C1003_10_2PDF(),
                                                 new C1003_17_3PDF(),
                                                 new C1003_10_4aPDF(),
                                                 new C1003_10_4bPDF(),
                                                 new C1003_10_4cPDF(),
                                                 new C1003_10_4dPDF(),
                                                 new C1003_10_4ePDF(),
                                                 new C1003_10_4fPDF(),
                                                 new C1003_10_4gPDF(),
                                                 new C1003_10_4hPDF(),
                                                 new C1003_10_4PDF(),
                                                 new C1003_17GmiAddendum()
                                             }).ToArray();
            }
        }
    }

    public class CFinal1003_10PDF : AbstractBatchPDF
    {
        public override string Description
        {
            get { return "Final 1003 Loan Application (<b>Effective after 07/2010</b>, legal size printout)"; }
        }

        public override bool IsVisible
        {
            get { return DataLoan != null && DataLoan.sUseGFEDataForSCFields; }
        }

        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                                                 new C1003_10_1PDF(),
                                                 new C1003_10_2PDF(),
                                                 new CFinal1003_10_3PDF(),
                                                 new C1003_10_4aPDF(),
                                                 new C1003_10_4bPDF(),
                                                 new C1003_10_4cPDF(),
                                                 new C1003_10_4dPDF(),
                                                 new C1003_10_4ePDF(),
                                                 new C1003_10_4fPDF(),
                                                 new C1003_10_4gPDF(),
                                                 new C1003_10_4hPDF(),
                                                 new C1003_10_4PDF()

                                             };
            }
        }
    }

    public class CFinal1003_17PDF : AbstractBatchUliPdf
    {
        public override string Description
        {
            get { return "Final 1003 Loan Application with Demographic Addendum (<b>Effective after 1/1/2017</b> legal size printout)"; }
        }

        public override bool IsVisible
        {
            get { return DataLoan != null && DataLoan.sUseGFEDataForSCFields; }
        }

        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return this.ApplyBatchArguments( new AbstractPDFFile[] {
                                                 new C1003_10_1PDF(),
                                                 new C1003_10_2PDF(),
                                                 new CFinal1003_17_3PDF(),
                                                 new C1003_10_4aPDF(),
                                                 new C1003_10_4bPDF(),
                                                 new C1003_10_4cPDF(),
                                                 new C1003_10_4dPDF(),
                                                 new C1003_10_4ePDF(),
                                                 new C1003_10_4fPDF(),
                                                 new C1003_10_4gPDF(),
                                                 new C1003_10_4hPDF(),
                                                 new C1003_10_4PDF(),
                                                 new C1003_17GmiAddendum()
                                             }).ToArray();
            }
        }
    }

    public class C1003_06LetterPDF : AbstractBatchPDF 
    {

        public override string Description 
        {
            get { return "1003 Loan Application (<b>Effective after 07/2005</b>, letter size printout)"; }
        }

        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            {
                return new AbstractPDFFile[] {
                                                 new C1003_06Letter_1PDF(),
                                                 new C1003_06Letter_2PDF(),
                                                 new C1003_06Letter_3PDF(),
                                                 new C1003_06Letter_4PDF(),
                                                 new C1003_06Letter_5aPDF(),
                                                 new C1003_06Letter_5bPDF(),
                                                 new C1003_06Letter_5cPDF(),
                                                 new C1003_06Letter_5dPDF(),
                                                 new C1003_06Letter_5ePDF(),
                                                 new C1003_06Letter_5fPDF(),
                                                 new C1003_06Letter_5gPDF(),
                                                 new C1003_06Letter_5PDF()

                                             };
            }
        }
    }

    public class C1003_10LetterPDF : AbstractBatchPDF
    {

        public override string Description
        {
            get { return "1003 Loan Application (<b>Effective after 07/2010</b>, letter size printout)"; }
        }

        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                                                 new C1003_10Letter_1PDF(),
                                                 new C1003_10Letter_2PDF(),
                                                 new C1003_10Letter_3PDF(),
                                                 new C1003_10Letter_4PDF(),
                                                 new C1003_10Letter_5aPDF(),
                                                 new C1003_10Letter_5bPDF(),
                                                 new C1003_10Letter_5cPDF(),
                                                 new C1003_10Letter_5dPDF(),
                                                 new C1003_10Letter_5ePDF(),
                                                 new C1003_10Letter_5fPDF(),
                                                 new C1003_10Letter_5gPDF(),
                                                 new C1003_10Letter_5hPDF(),
                                                 new C1003_10Letter_5PDF()

                                             };
            }
        }
    }

    public class C1003_17LetterPDF : AbstractBatchUliPdf
    {

        public override string Description
        {
            get { return "1003 Loan Application with Demographic Addendum (<b>Effective after 1/1/2017</b> letter size printout)"; }
        }

        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return this.ApplyBatchArguments( new AbstractPDFFile[] {
                                                 new C1003_10Letter_1PDF(),
                                                 new C1003_10Letter_2PDF(),
                                                 new C1003_10Letter_3PDF(),
                                                 new C1003_17Letter_4PDF(),
                                                 new C1003_10Letter_5aPDF(),
                                                 new C1003_10Letter_5bPDF(),
                                                 new C1003_10Letter_5cPDF(),
                                                 new C1003_10Letter_5dPDF(),
                                                 new C1003_10Letter_5ePDF(),
                                                 new C1003_10Letter_5fPDF(),
                                                 new C1003_10Letter_5gPDF(),
                                                 new C1003_10Letter_5hPDF(),
                                                 new C1003_10Letter_5PDF(),
                                                 new C1003_17GmiAddendum()
                                             }).ToArray();
            }
        }
    }

    public class CFinal1003_10LetterPDF : AbstractBatchPDF
    {

        public override string Description
        {
            get { return "Final 1003 Loan Application (<b>Effective after 07/2010</b>, letter size printout)"; }
        }

        public override bool IsVisible
        {
            get { return DataLoan != null && DataLoan.sUseGFEDataForSCFields; }
        }

        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                                                 new C1003_10Letter_1PDF(),
                                                 new C1003_10Letter_2PDF(),
                                                 new C1003_10Letter_3PDF(),
                                                 new CFinal1003_10Letter_4PDF(),
                                                 new C1003_10Letter_5aPDF(),
                                                 new C1003_10Letter_5bPDF(),
                                                 new C1003_10Letter_5cPDF(),
                                                 new C1003_10Letter_5dPDF(),
                                                 new C1003_10Letter_5ePDF(),
                                                 new C1003_10Letter_5fPDF(),
                                                 new C1003_10Letter_5gPDF(),
                                                 new C1003_10Letter_5hPDF(),
                                                 new C1003_10Letter_5PDF()

                                             };
            }
        }
    }

    public class CFinal1003_17LetterPDF : AbstractBatchUliPdf
    {

        public override string Description
        {
            get { return "Final 1003 Loan Application with Demographic Addendum (<b>Effective after 1/1/2017</b> letter size printout)"; }
        }

        public override bool IsVisible
        {
            get { return DataLoan != null && DataLoan.sUseGFEDataForSCFields; }
        }

        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return this.ApplyBatchArguments( new AbstractPDFFile[] {
                                                 new C1003_10Letter_1PDF(),
                                                 new C1003_10Letter_2PDF(),
                                                 new C1003_10Letter_3PDF(),
                                                 new CFinal1003_17Letter_4PDF(),
                                                 new C1003_10Letter_5aPDF(),
                                                 new C1003_10Letter_5bPDF(),
                                                 new C1003_10Letter_5cPDF(),
                                                 new C1003_10Letter_5dPDF(),
                                                 new C1003_10Letter_5ePDF(),
                                                 new C1003_10Letter_5fPDF(),
                                                 new C1003_10Letter_5gPDF(),
                                                 new C1003_10Letter_5hPDF(),
                                                 new C1003_10Letter_5PDF(),
                                                 new C1003_17GmiAddendum()
                                             }).ToArray();
            }
        }
    }

}
