﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Common;
using DataAccess;

namespace LendersOffice.Pdf
{
    public class CSuspenseNotice_1PDF : AbstractLetterPDF
    {
        public override string Description
        {
            get
            {
                return "Page 1";
            }
        }
        public override string PdfFile
        {
            get { return "SuspenseNotice_1.pdf"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            //            SuspendedDate
            //PaymentType
            //BrokerName

            CAgentFields lender = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("BrokerName", lender.CompanyName);
            AddFormFieldData("BrokerAddress", lender.StreetAddr_MultiLine);

            AddFormFieldData("PaymentType", dataLoan.sIsIOnly ? "Interest Only" : "Principal & Interest");

            //AddFormFieldData("BrokerAddress", dataLoan.BrokerAddress_rep);
            AddFormFieldData("sSuspendedD", dataLoan.sSuspendedD_rep);
            CAgentFields agent = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            AddFormFieldData("GetAgentOfRole[LoanOfficer].AgentName", agent.AgentName);
            AddFormFieldData("GetAgentOfRole[LoanOfficer].FaxNum", agent.FaxNum);
            AddFormFieldData("GetAgentOfRole[LoanOfficer].Phone", agent.Phone);
            AddFormFieldData("GetAgentOfRole[LoanOfficer].EmailAddr", agent.EmailAddr);


            AddFormFieldData("sSpAddr_MultiLine", dataLoan.sSpAddr_MultiLine);
            AddFormFieldData("sProd3rdPartyUwResultT", dataLoan.sProd3rdPartyUwResultT_rep);
            AddFormFieldData("sEmployeeLenderAccExecfax", dataLoan.sEmployeeLenderAccExecFax);
            AddFormFieldData("aBNm_acNm", dataApp.aBNm_aCNm);
            AddFormFieldData("sEmployeeLenderAccExecPhone", dataLoan.sEmployeeLenderAccExecPhone);
            AddFormFieldData("sProdCashoutAmt", dataLoan.sProdCashoutAmt_rep);
            AddFormFieldData("sQualTopR", dataLoan.sQualTopR_rep);
            AddFormFieldData("sLpTemplateNm", dataLoan.sLpTemplateNm);
            AddFormFieldData("sLPurposeT", dataLoan.sLPurposeT_rep);
            AddFormFieldData("sEmployeeProcessorEmail", dataLoan.sEmployeeProcessorEmail);
            AddFormFieldData("sEmployeeLenderAccExecName", dataLoan.sEmployeeLenderAccExecName);
            AddFormFieldData("sCltvR", dataLoan.sCltvR_rep);
            AddFormFieldData("sEmployeeProcessorName", dataLoan.sEmployeeProcessorName);
            AddFormFieldData("sProd3rdPartyUwResultT", dataLoan.sProd3rdPartyUwResultT);
            AddFormFieldData("sQualBottomR", dataLoan.sQualBottomR_rep);
            AddFormFieldData("sProThisMPmt", dataLoan.sProThisMPmt_rep);
            AddFormFieldData("sEmployeeProcessorPhone", dataLoan.sEmployeeProcessorPhone);
            AddFormFieldData("sMaxDti", dataLoan.sMaxDti_rep);
            AddFormFieldData("sEmployeeProcessorFax", dataLoan.sEmployeeProcessorFax);
            AddFormFieldData("sIncomeDocExpD", dataLoan.sIncomeDocExpD_rep);
            AddFormFieldData("sEmployeeUnderwriterName", dataLoan.sEmployeeUnderwriterName);
            AddFormFieldData("sProdDocT", dataLoan.sProdDocT_rep);
            AddFormFieldData("sCrExpD", dataLoan.sCrExpD_rep);
            AddFormFieldData("sRLckdExpiredD", dataLoan.sRLckdExpiredD_rep);
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("sProdImpound", dataLoan.sProdImpound);
            AddFormFieldData("aTotI", dataApp.aTotI_rep);
            AddFormFieldData("sLAmtCalc", dataLoan.sLAmtCalc_rep);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sEmployeeUnderwriterPhone", dataLoan.sEmployeeUnderwriterPhone);
            AddFormFieldData("sDue", dataLoan.sDue_rep);
            AddFormFieldData("sMaxR", dataLoan.sMaxR_rep);
            AddFormFieldData("sApprVal", dataLoan.sApprVal_rep);
            AddFormFieldData("sEmployeeUnderwriterfax", dataLoan.sEmployeeUnderwriterFax);
            AddFormFieldData("aOccT", dataApp.aOccT);
            AddFormFieldData("sGseSpT", dataLoan.sGseSpT_rep);
            AddFormFieldData("sEmployeeUnderwriterEmail", dataLoan.sEmployeeUnderwriterEmail);
            AddFormFieldData("sProdMIOptionT", dataLoan.sProdMIOptionT);
            AddFormFieldData("sTerm", dataLoan.sTerm_rep);
            AddFormFieldData("sEmployeeLenderAccExecEmail", dataLoan.sEmployeeLenderAccExecEmail);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sApprRprtExpD", dataLoan.sApprRprtExpD_rep);
            AddFormFieldData("sCreditScoreLpeQual", dataLoan.sCreditScoreLpeQual_rep);
            AddFormFieldData("sFfUfmipFinanced", dataLoan.sFfUfmipFinanced_rep);
            AddFormFieldData("sLtvR", dataLoan.sLtvR_rep);
        }

    }
    public class CSuspenseNotice_2PDF : AbstractLetterPDF
    {

        public override string Description
        {
            get
            {
                return "Page 2";
            }
        }
        public override string PdfFile
        {
            get { return "SuspenseNotice_2.pdf"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            AddFormFieldData("sOutstandingConditionHtml", dataLoan.sOutstandingConditionHtml.Value, false);
        }

    }


    public class CSuspenseNoticeObsoletePDF : AbstractBatchPDF
    {
        public override string Description
        {
            get
            {
                return "Suspense Notice (OBSOLETE)";
            }
        }


        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                    new CSuspenseNotice_1PDF(),
                    new CSuspenseNotice_2PDF()
                };
            }
        }
    }
}
