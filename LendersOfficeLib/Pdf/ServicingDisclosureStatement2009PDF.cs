﻿namespace LendersOffice.Pdf
{
    using DataAccess;

    public class CServicingDisclosureStatement2009PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "ServicingDisclosureStatement2009.pdf"; }
        }
        public override string Description
        {
            get { return "Lender Servicing Disclosure Statement"; }
        }
        public override string EditLink
        {
            get { return "/newlos/Disclosure/ServicingDisclosure.aspx"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.ServicingDisclosure, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("ServicingDisclosureCompanyName", preparer.CompanyName);
            AddFormFieldData("ServicingDisclosureStreetAddrMultiLines", preparer.StreetAddrMultiLines);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("GfeTilPrepareDate", preparer.PrepareDate_rep);

            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("sMayAssignSellTransfer", dataLoan.sMayAssignSellTransfer);
            AddFormFieldData("sDontService", dataLoan.sDontService);
            AddFormFieldData("sServiceDontIntendToTransfer", dataLoan.sServiceDontIntendToTransfer);
        }
    }
}