﻿<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html"
              doctype-public="-//W3C//DTD HTML 4.01//EN"
              doctype-system="http://www.w3.org/TR/html4/loose.dtd"
              encoding="utf-8"
              indent="yes"
              media-type="text/html"
              omit-xml-declaration="yes"
              version="4.01"/>

  <xsl:template match="sections">
    <xsl:for-each select="section">
      <table width="1200" border="0" class="fees dontbreak SectionsTable">
        <colgroup>
          <col width="300"></col>
          <col width="300"></col>
          <col width="200"></col>
          <col width="200" style="border-left: solid 1px black; border-right: solid 1px black;"></col>
          <col width="200" style="border-left: solid 1px black; border-right: solid 1px black;"></col>
        </colgroup>
        <thead>
          <tr>
            <td colspan="2" class="sectionheader" align="left">
              <xsl:value-of select="description"/>
            </td>
            <td class="sectionheader alignright">
              <xsl:value-of select="total"/>
            </td>
            <td class="sectionheader alignright" style="white-space:nowrap;">
              <xsl:value-of select="totalPaidToLender"/>
            </td>
            <td class="sectionheader alignright" style="white-space:nowrap;">
              <xsl:value-of select="totalPaidByLender"/>
            </td>
          </tr>
        </thead>
        <tbody>
          <xsl:for-each select="fees/fee">
            <tr>
              <td align="left" class="feedesc">
                <xsl:value-of select="description"/>
              </td>
              <td align="left">
                <xsl:value-of select="calc"/>
              </td>
              <td class="alignright">
                <xsl:value-of select="total"/>
              </td>
              <td class="alignright">
                <xsl:value-of select="paidToLenderAmt"/>
              </td>
              <td class="alignright">
                <xsl:value-of select="paidByLenderAmt"/>
              </td>
            </tr>
          </xsl:for-each>
          <tr>
            <td>&#160;</td>
            <td>&#160;</td>
            <td>&#160;</td>
            <td>&#160;</td>
            <td>&#160;</td>
          </tr>
        </tbody>
      </table>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="FundingWorksheet">
    <html>
      <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title>Initial Fees Worksheet</title>
        <style type="text/css" media="all">
          html, body { font-family: Verdana, Helvetica, sans-serif; font-size: 12pt;  }
          .header { color: rgb(8,86,147); font-size: 20pt;  }
          .center { text-align: center; }
          .italic { font-style: italic; font-size:16pt }
          .right { float: right; }
          .left { float: left; }
          .clear { clear:both; }
          .alignright { text-align: right; }
          .margintop { margin-top: 10px; }
          .marginbottom { margin-bottom: 10px; }
          .sectionheader { background-color: rgb(8,86,147); color: #FFF; font-weight: bold; padding: 3px;}
          .sectionheaderblack { background-color: rgb(75,75,75); color: #FFF; font-weight: bold; padding: 3px;}
          .feeheader { background-color: #ffeb8e; }
          table { border-collapse: collapse;  }
          .feedesc {   overflow:hidden; text-overflow: ellipsis; white-space: nowrap; }
          table.fees { table-layout: fixed; }
          .borderless td { border: none; }
          .dontbreak { page-break-inside: avoid; }
          .disclaimer2010 { padding-top: 20px; padding-bottom: 10px; }

          .ColumnDisplayTable td { padding: 0px 10px;}
          .ColumnDisplayTable .label { text-align: left; }
          .ColumnDisplayTable .value { text-align:right; font-weight: bold;}

          .SectionsTable {border: 1px solid black;}
          .SectionsTable td {padding-left: 3px; padding-right: 3px;}
          .SectionsTable td.feedesc { padding-left: 20px;}

          .EstGross { color: rgb(64, 64, 64); font-weight: bold;}
          .EstNet { color: white; background-color: rgb(64, 64, 64); font-weight: bold;}

        </style>
      </head>
      <body class="center">
        <div class="header marginbottom" >Funding Worksheet</div>
        <div class="marginbottom">
          <xsl:value-of select="BrokerName"/>
        </div>
        <div class="center">
          <table width="1200" border="0" class="ColumnDisplayTable marginbottom" align="center">
            <tbody>
              <tr>
                <td class="label">Borrower name</td>
                <td class="value">
                  <xsl:value-of select="aBNm"/>
                </td>
                <td class="label">Total loan amount</td>
                <td class="value">
                  <xsl:value-of select="sFinalLAmt"/>
                </td>
              </tr>
              <tr>
                <td class="label">Loan number</td>
                <td class="value">
                  <xsl:value-of select="sLNm"/>
                </td>
                <xsl:choose>
                  <xsl:when test="sDisclosureRegulationT='2'">
                    <td class="label">Closing costs deducted from loan proceeds</td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td class="label">Paid to</td>
                  </xsl:otherwise>
                </xsl:choose>
                <td class="value">
                  <xsl:value-of select="sSettlementChargesDedFromLoanProc"/>
                </td>
              </tr>
              <tr>
                <td class="label"></td>
                <td class="value"></td>
                <xsl:choose>
                  <xsl:when test="sDisclosureRegulationT='2'">
                    <td class="label">
                      Closing costs funded by <xsl:value-of select="BrokerName"/> at closing
                    </td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td class="label">Paid by</td>
                  </xsl:otherwise>
                </xsl:choose>
                <td class="value">
                  <xsl:if test="sDisclosureRegulationT!='2'">
                    <xsl:value-of select="BrokerName"/> &#160;
                  </xsl:if>
                  <xsl:value-of select="sSettlementTotalFundByLenderAtClosing"/>
                </td>
              </tr>
              <xsl:if test="sDisclosureRegulationT='2'">
                <tr>
                  <td class="label"></td>
                  <td class="value"></td>
                  <td class="label">
                    Other costs deducted from loan proceeds
                  </td>
                  <td class="value">
                    <xsl:value-of select="sAdjustmentsOtherCreditsDeductedFromLoanProceeds"/>
                  </td>
                </tr>
                <tr>
                  <td class="label"></td>
                  <td class="value"></td>
                  <td class="label">
                    Other costs funded by <xsl:value-of select="BrokerName"/> at closing
                  </td>
                  <td class="value">
                    <xsl:value-of select="sAdjustmentsOtherCreditsFundedByLenderAtClosing"/>
                  </td>
                </tr>
              </xsl:if>
              <tr>
                <td class="label">Warehouse lender</td>
                <td class="value">
                  <xsl:value-of select="sWarehouseFunderDesc"/>
                </td>
                <td class="label">Other funding adj</td>
                <td class="value">
                  <xsl:value-of select="sOtherFundAdj"/>
                </td>
              </tr>
              <tr>
                <td class="label">Warehouse line max funding %</td>
                <td class="value">
                  <xsl:value-of select="sWarehouseMaxFundPc"/>
                </td>
                <td class="label"></td>
                <td class="value"></td>
              </tr>
              <tr>
                <td class="label">Warehouse line max funding amt</td>
                <td class="value">
                  <xsl:value-of select="sWarehouseMaxFundAmt"/>
                </td>
                <td class="label">Amount req to fund</td>
                <td class="value">
                  <xsl:value-of select="sAmtReqToFund"/>
                </td>
              </tr>
              <tr>
                <td class="label"></td>
                <td class="value"></td>
                <td class="label">Total amount funded</td>
                <td class="value">
                  <xsl:value-of select="sTotalAmtFund"/>
                </td>
              </tr>
              <tr>
                <td class="label">Prepaid Interest Start Date</td>
                <td class="value">
                  <xsl:value-of select="sConsummationD"/>
                </td>
                <td class="label"></td>
                <td class="value"></td>
              </tr>
              <tr>
                <td class="label">Tracking number</td>
                <td class="value">
                  <xsl:value-of select="sTrackingN"/>
                </td>
                <td class="label">Funded from warehouse</td>
                <td class="value">
                  <xsl:value-of select="sAmtFundFromWarehouseLine"/>
                </td>
              </tr>
              <tr>
                <td class="label"></td>
                <td class="value"></td>
                <td class="label"></td>
                <td class="value"></td>
              </tr>
              <tr>
                <td class="label"></td>
                <td class="value"></td>
                <td class="label">Haircut</td>
                <td class="value">
                  <xsl:value-of select="sFundReqForShortfall"/>
                </td>
              </tr>
              <tr>
                <td class="label"></td>
                <td class="value"></td>
                <td class="label">Excess</td>
                <td class="value">
                  <xsl:value-of select="sChkDueFromClosing"/></td>
              </tr>
            </tbody>
          </table>

          <table width="1200" border="0" class="fees dontbreak">
            <colgroup>
              <col width="300"></col>
              <col width="300"></col>
              <col width="200"></col>
              <col width="200"></col>
              <col width="200"></col>
            </colgroup>
            <tbody>
              <tr>
                <td align="left">
                  Fee description
                </td>
                <td align="left">
                  Calculation
                </td>
                <td>
                  Total
                </td>
                <td>
                  DFLP
                </td>
                <td>
                  Paid by lender
                </td>
              </tr>
            </tbody>
          </table>
          
          <xsl:apply-templates select="sections"></xsl:apply-templates>

          <table width="1200" border="0" class="fees dontbreak SectionsTable">
            <colgroup>
              <col width="300"></col>
              <col width="300"></col>
              <col width="200"></col>
              <col width="200" style="border-left: solid 1px black; border-right: solid 1px black;"></col>
              <col width="200" style="border-left: solid 1px black; border-right: solid 1px black;"></col>
            </colgroup>
            <tbody>
              <tr class="EstGross" style="border: solid 1px black; border-right: solid 1px black;">
                <td align="left">Gross Closing Costs</td>
                <td>&#160;</td>
                <td class="alignright">
                  <xsl:value-of select="grossTotal"/>
                </td>
                <td class="alignright">
                  <xsl:value-of select="grossPaidToLender"/>
                </td>
                <td class="alignright">
                  <xsl:value-of select="grossPaidByLender"/>
                </td>
              </tr>
              <tr style="border: solid 1px black; border-right: solid 1px black;">
                <td align="left" class="feedesc">General Lender Credit</td>
                <td>&#160;</td>
                <td class="alignright">
                  <xsl:value-of select="generalLenderCreditTotal"/>
                </td>
                <td class="alignright"></td>
                <td class="alignright">
                  <xsl:value-of select="generalLenderCreditPaidByLender"/>
                </td>
              </tr>
              <tr class="EstNet" style="border: solid 1px black; border-right: solid 1px black;">
                <td align="left">Net Closing Costs</td>
                <td>&#160;</td>
                <td class="alignright">
                  <xsl:value-of select="netTotal"/>
                </td>
                <td class="alignright">
                  <xsl:value-of select="grossPaidToLender"/>
                </td>
                <td class="alignright">
                  <xsl:value-of select="netPaidByLender"/>
                </td>
              </tr>
            </tbody>
          </table>

          <xsl:if test="sDisclosureRegulationT='2'">
            <table width="1200" border="0" class="fees dontbreak SectionsTable">
              <colgroup>
                <col width="300"></col>
                <col width="300"></col>
                <col width="200"></col>
                <col width="200" style="border-left: solid 1px black; border-right: solid 1px black;"></col>
                <col width="200" style="border-left: solid 1px black; border-right: solid 1px black;"></col>
              </colgroup>
              <thead>
                <tr>
                  <td>&#160;</td>
                  <td>&#160;</td>
                  <td>&#160;</td>
                  <td>&#160;</td>
                  <td>&#160;</td>
                </tr>
                <tr>
                  <td colspan="2" class="sectionheader" align="left">
                    Other Costs
                  </td>
                  <td class="sectionheader alignright">
                    <xsl:value-of select="adjustmentsList/adjTotal"/>
                  </td>
                  <td class="sectionheader alignright" style="white-space:nowrap;">
                    <xsl:value-of select="adjustmentsList/adjDFLPTotal"/>
                  </td>
                  <td class="sectionheader alignright" style="white-space:nowrap;">
                    <xsl:value-of select="adjustmentsList/adjpaidByLenderTotal"/>
                  </td>
                </tr>
              </thead>
              <tbody>
                <xsl:for-each select="adjustmentsList/adjustment">
                  <tr>
                    <td align="left" class="feedesc">
                      <xsl:value-of select="adjDesc"/>
                    </td>
                    <td align="left">
                    </td>
                    <td class="alignright">
                      <xsl:value-of select="adjTotalAmt"/>
                    </td>
                    <td class="alignright">
                      <xsl:value-of select="adjDFLPAmt"/>
                    </td>
                    <td class="alignright">
                      <xsl:value-of select="adjPaidByLenderAmt"/>
                    </td>
                  </tr>
                </xsl:for-each>
                <tr class="EstNet" style="border: solid 1px black; border-right: solid 1px black;">
                  <td align="left">Total Costs</td>
                  <td>&#160;</td>
                  <td class="alignright">
                    <xsl:value-of select="allTotalAmt"/>
                  </td>
                  <td class="alignright">
                    <xsl:value-of select="allDFLPAmt"/>
                  </td>
                  <td class="alignright">
                    <xsl:value-of select="allPaidByLenderAmt"/>
                  </td>
                </tr>
              </tbody>
            </table>
          </xsl:if>
          
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>