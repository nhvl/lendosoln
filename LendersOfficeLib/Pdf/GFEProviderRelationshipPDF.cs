using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Security;
using System.Text;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CGFEProviderRelationshipPDF : AbstractLetterPDF
	{

        public override string Description 
        {
            get { return "Good Faith Estimate Provider Relationship"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/Status/Agents.aspx"; }
        }
        public override string PdfFile 
        {
            get { return "GFEProviderRelationship.pdf"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {

            string brokerInfo = "";

            IPreparerFields gfeTil = dataLoan.GetPreparerOfForm( E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("GfeTil", Tools.FormatAddress(gfeTil.CompanyName, gfeTil.StreetAddr, gfeTil.City, gfeTil.State, gfeTil.Zip, gfeTil.PhoneOfCompany));
            AddFormFieldData("GfeTilPrepareDate", gfeTil.PrepareDate_rep);

            string propertyAddress = Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip);

            int i = 0;

            // Borrower information also printout even if no agent record found.
            AddFormFieldData("BrokerInfo", brokerInfo);
            AddFormFieldData("PropertyAddress", propertyAddress);
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("Applicants", dataApp.aBNm + Environment.NewLine + dataApp.aCNm);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);

            int count = dataLoan.GetAgentRecordCount();
            for (int index = 0; index < count; index++) 
            {
                CAgentFields field = dataLoan.GetAgentFields(index);
                if (!field.IsListedInGFEProviderForm) 
                {
                    continue;
                } // end if

                if (i % 2 == 0) 
                {
                    if (i != 0) 
                    {
                        AddNewPage();
                        AddFormFieldData("GfeTil", Tools.FormatAddress(gfeTil.CompanyName, gfeTil.StreetAddr, gfeTil.City, gfeTil.State, gfeTil.Zip, gfeTil.PhoneOfCompany));
                        AddFormFieldData("PropertyAddress", propertyAddress);
                        AddFormFieldData("sLNm", dataLoan.sLNm);
                        AddFormFieldData("GfeTilPrepareDate", gfeTil.PrepareDate_rep);
                        AddFormFieldData("Applicants", dataApp.aBNm + Environment.NewLine + dataApp.aCNm);
                        AddFormFieldData("aBNm", dataApp.aBNm);
                        AddFormFieldData("aCNm", dataApp.aCNm);
                    } // end if (i != 0)


                } // end if (i % 2 == 0)

                AddFormFieldData("Provider" + (i % 2), field.CompanyName);
                AddFormFieldData("ProviderItemNumber" + (i % 2), field.ProviderItemNumber);
                AddFormFieldData("Address" + (i % 2), Tools.FormatAddress(field.StreetAddr, field.City, field.State, field.Zip));
                AddFormFieldData("Phone" + (i % 2), field.Phone);
                AddFormFieldData("IsLenderAffiliate" + (i % 2), field.IsLenderAffiliate);
                AddFormFieldData("IsLenderAssociation" + (i % 2), field.IsLenderAssociation);
                AddFormFieldData("IsLenderRelative" + (i % 2), field.IsLenderRelative);
                AddFormFieldData("IsUsedRepeatlyByLenderLast12Months" + (i % 2), field.IsUsedRepeatlyByLenderLast12Months);
                AddFormFieldData("HasLenderRelationship" + (i % 2), field.HasLenderRelationship);
                AddFormFieldData("HasLenderAccountLast12Months" + (i % 2), field.HasLenderAccountLast12Months);
                i++;


            } // end for (int index = 0; index < count; index++) 
        }
	}
}
