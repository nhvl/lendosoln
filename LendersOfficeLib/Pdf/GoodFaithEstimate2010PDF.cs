using System;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using DataAccess;
using LendersOffice.Common;
using System.Linq;

namespace LendersOffice.Pdf
{
    public class CGoodFaithEstimate2010_1PDF : AbstractGoodFaithEstimate2010PDF
    {
        public override string PdfFile
        {
            get { return "GoodFaithEstimate2010_1.pdf"; }
        }
        public override string Description
        {
            get { return "Page 1"; }
        }
        public CGoodFaithEstimate2010_1PDF() { }
        public CGoodFaithEstimate2010_1PDF(NameValueCollection arguments)
        {
            if (arguments != null)
            {
                Arguments = arguments;
            }
        }
    }

    public class CGoodFaithEstimate2010_2PDF : AbstractGoodFaithEstimate2010PDF
    {
        public override string PdfFile
        {
            get { return "GoodFaithEstimate2010_2.pdf"; }
        }
        public override string Description
        {
            get { return "Page 2"; }
        }
        public CGoodFaithEstimate2010_2PDF(){}
        public CGoodFaithEstimate2010_2PDF(NameValueCollection arguments)
        {
            if (arguments != null)
            {
                Arguments = arguments;
            }
        }
    }

    public class CGoodFaithEstimate2010_3PDF : AbstractGoodFaithEstimate2010PDF
    {
        public override string PdfFile
        {
            get { return "GoodFaithEstimate2010_3.pdf"; }
        }
        public override string Description
        {
            get { return "Page 3"; }
        }
        public CGoodFaithEstimate2010_3PDF(){}
        public CGoodFaithEstimate2010_3PDF(NameValueCollection arguments)
        {
            if (arguments != null)
            {
                Arguments = arguments;
            }
        }
    }

    public class CGoodFaithEstimate2010PDF : AbstractBatchPDF
    {
        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                                                 new CGoodFaithEstimate2010_1PDF()
                                                ,new CGoodFaithEstimate2010_2PDF()
                                                ,new CGoodFaithEstimate2010_3PDF()
                                             };
            }
        }

        public override string Description
        {
            get { return "2010 Good Faith Estimate"; }
        }

        public override string EditLink
        {
            get { return "/newlos/Forms/GoodFaithEstimate2010.aspx"; }
        }

        public override bool IsVisible
        {
            get
            {
                return !DataLoan.sIsConstructionLoanWithConstructionRefiData;
            }
        }
    }

    public abstract class AbstractGoodFaithEstimate2010PDF : AbstractGoodFaithEstimate2010PDFFromGFEArchive
    {
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {  
            var gfe = dataLoan.ExtractStaticGFEArchive();
            ((LendersOffice.Common.SerializationTypes.GFEArchive)gfe).dataApp_aBNm_aCNm = dataApp.aBNm_aCNm;
            ApplyData(gfe);
        }
    }
}
