using System;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    /// <summary>
    /// Summary description for HUD_56001PDF.
    /// </summary>
    public abstract class AbstractHUD_56001PDF : AbstractLetterPDF
    {
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            // Page 1
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sTerm", dataLoan.sTerm_rep);
            AddFormFieldData("sFHAPropImprovHasFedPastDueTri", dataLoan.sFHAPropImprovHasFedPastDueTri);
            AddFormFieldData("sFHAPropImprovHasFHAPendingAppTri", dataLoan.sFHAPropImprovHasFHAPendingAppTri);
            AddFormFieldData("sFHAPropImprovHasFHAPendingAppWithWhom", dataLoan.sFHAPropImprovHasFHAPendingAppWithWhom);
            AddFormFieldData("sFHAPropImprovRefinTitle1LoanTri", dataLoan.sFHAPropImprovRefinTitle1LoanTri);
            AddFormFieldData("sFHAPropImprovRefinTitle1LoanNum", dataLoan.sFHAPropImprovRefinTitle1LoanNum);
            AddFormFieldData("sFHAPropImprovRefinTitle1LoanBal", dataLoan.sFHAPropImprovRefinTitle1LoanBal_rep);
            
            if (dataLoan.sIsTargeting2019Ulad)
            {
                AddFormFieldData("aBDecJudgment", dataApp.aBDecHasOutstandingJudgmentsUlad.ToYN());
                AddFormFieldData("aBDecBankrupt", dataApp.aBDecHasBankruptcyLast7YearsUlad.ToYN());
                AddFormFieldData("aBDecLawsuit", dataApp.aBDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad.ToYN());

                // This form field name is incorrect -- the form is asking for
                // foreclosure, not debt obligations.
                AddFormFieldData("aBDecObligated", dataApp.aBDecHasForeclosedLast7YearsUlad.ToYN());
            }
            else
            {
                AddFormFieldData("aBDecJudgment", dataApp.aBDecJudgment);
                AddFormFieldData("aBDecBankrupt", dataApp.aBDecBankrupt);
                AddFormFieldData("aBDecLawsuit", dataApp.aBDecLawsuit);

                // This form field name is incorrect -- the form is asking for
                // foreclosure, not debt obligations.
                AddFormFieldData("aBDecObligated", dataApp.aBDecForeclosure);
            }

            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aBSsn", dataApp.aBSsn);
            AddFormFieldData("aBHPhone", dataApp.aBHPhone);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("aCSsn", dataApp.aCSsn);
            AddFormFieldData("aCHPhone", dataApp.aCHPhone);
            AddFormFieldData("aBPresentAddr", Tools.FormatAddress(dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip));
            AddFormFieldData("aCPresentAddr", Tools.FormatAddress(dataApp.aCAddr, dataApp.aCCity, dataApp.aCState, dataApp.aCZip));
            AddFormFieldData("aBAddrYrs", dataApp.aBAddrYrs);
            if (dataApp.aBAddrT != E_aBAddrT.LeaveBlank)
                AddFormFieldData("aBAddrT", dataApp.aBAddrT.ToString());
            AddFormFieldData("aCAddrYrs", dataApp.aCAddrYrs);
            if (dataApp.aCAddrT != E_aCAddrT.LeaveBlank)
                AddFormFieldData("aCAddrT", dataApp.aCAddrT.ToString());
            AddFormFieldData("aBPrev1Addr", Tools.FormatAddress(dataApp.aBPrev1Addr, dataApp.aBPrev1City, dataApp.aBPrev1State, dataApp.aBPrev1Zip));
            AddFormFieldData("aCPrev1Addr", Tools.FormatAddress(dataApp.aCPrev1Addr, dataApp.aCPrev1City, dataApp.aCPrev1State, dataApp.aCPrev1Zip));
            AddFormFieldData("aBPrev1AddrYrs", dataApp.aBPrev1AddrYrs);
            if (dataApp.aBPrev1AddrT != E_aBPrev1AddrT.LeaveBlank)
                AddFormFieldData("aBPrev1AddrT", dataApp.aBPrev1AddrT.ToString());
            AddFormFieldData("aCPrev1AddrYrs", dataApp.aCPrev1AddrYrs);
            if (dataApp.aCPrev1AddrT != E_aCPrev1AddrT.LeaveBlank)
                AddFormFieldData("aCPrev1AddrT", dataApp.aCPrev1AddrT.ToString());
            AddFormFieldData("aBMaritalStatT", dataApp.aBMaritalStatT);
            AddFormFieldData("aCMaritalStatT", dataApp.aCMaritalStatT);
            AddFormFieldData("aBGenderT", dataApp.aBGenderFallback);

            AddFormFieldData("aBDob", dataApp.aBDob_rep);
            AddFormFieldData("aBDependNum", dataApp.aBDependNum_rep);
            AddFormFieldData("aCGenderT", dataApp.aCGenderFallback);
            AddFormFieldData("aCDob", dataApp.aCDob_rep);
            AddFormFieldData("aCDependNum", dataApp.aCDependNum_rep);
            AddFormFieldData("aBHispanicT", dataApp.aBHispanicTFallback);
            AddFormFieldData("aCHispanicT", dataApp.aCHispanicTFallback);
            AddFormFieldData("aBIsAmericanIndian", dataApp.aBIsAmericanIndian);
            AddFormFieldData("aBIsAsian", dataApp.aBIsAsian);
            AddFormFieldData("aBIsBlack", dataApp.aBIsBlack);
            AddFormFieldData("aBIsWhite", dataApp.aBIsWhite);
            AddFormFieldData("aBIsPacificIslander", dataApp.aBIsPacificIslander);

            AddFormFieldData("aCIsAmericanIndian", dataApp.aCIsAmericanIndian);
            AddFormFieldData("aCIsAsian", dataApp.aCIsAsian);
            AddFormFieldData("aCIsBlack", dataApp.aCIsBlack);
            AddFormFieldData("aCIsWhite", dataApp.aCIsWhite);
            AddFormFieldData("aCIsPacificIslander", dataApp.aCIsPacificIslander);
            AddFormFieldData("sFHAPropImprovBorrRelative", dataLoan.sFHAPropImprovBorrRelativeNm + Environment.NewLine + dataLoan.sFHAPropImprovBorrRelativeAddr);
            AddFormFieldData("sFHAPropImprovBorrRelativeRelationship", dataLoan.sFHAPropImprovBorrRelativeRelationship);
            AddFormFieldData("sFHAPropImprovBorrRelativePhone", dataLoan.sFHAPropImprovBorrRelativePhone);
            AddFormFieldData("sFHAPropImprovCoborRelative", dataLoan.sFHAPropImprovCoborRelativeNm + Environment.NewLine + dataLoan.sFHAPropImprovCoborRelativeAddr);
            AddFormFieldData("sFHAPropImprovCoborRelativeRelationship", dataLoan.sFHAPropImprovCoborRelativeRelationship);
            AddFormFieldData("sFHAPropImprovCoborRelativePhone", dataLoan.sFHAPropImprovCoborRelativePhone);

            // Page 2
            IEmpCollection recordList = dataApp.aBEmpCollection;

            IPrimaryEmploymentRecord primaryEmployment = recordList.GetPrimaryEmp(false);
            if (null != primaryEmployment) 
            {
                AddFormFieldData("aBEmpEmplrAddr", Tools.FormatAddress(primaryEmployment.EmplrNm, primaryEmployment.EmplrAddr, primaryEmployment.EmplrCity, primaryEmployment.EmplrState, primaryEmployment.EmplrZip));
                AddFormFieldData("aBEmpEmplrBusPhone", primaryEmployment.EmplrBusPhone);
                AddFormFieldData("aBEmpJobTitle", primaryEmployment.JobTitle);
                AddFormFieldData("aBEmpEmplmtLen", primaryEmployment.EmplmtLen_rep);
            }
            if (recordList.CountRegular > 0) 
            {
                IRegularEmploymentRecord previousEmployment = recordList.GetRegularRecordAt(0);
                AddFormFieldData("aBPrevEmpEmplrAddr", Tools.FormatAddress(previousEmployment.EmplrNm, previousEmployment.EmplrAddr, previousEmployment.EmplrCity, previousEmployment.EmplrState, previousEmployment.EmplrZip));
                AddFormFieldData("aBPrevEmpEmplrBusPhone", previousEmployment.EmplrBusPhone);
                AddFormFieldData("aBPrevEmpJobTitle", previousEmployment.JobTitle);
                AddFormFieldData("aBPrevEmpMonI", previousEmployment.MonI_rep);
            }

            recordList = dataApp.aCEmpCollection;

            primaryEmployment = recordList.GetPrimaryEmp(false);
            if (null != primaryEmployment) 
            {
                AddFormFieldData("aCEmpEmplrAddr", Tools.FormatAddress(primaryEmployment.EmplrNm, primaryEmployment.EmplrAddr, primaryEmployment.EmplrCity, primaryEmployment.EmplrState, primaryEmployment.EmplrZip));
                AddFormFieldData("aCEmpEmplrBusPhone", primaryEmployment.EmplrBusPhone);
                AddFormFieldData("aCEmpJobTitle", primaryEmployment.JobTitle);
                AddFormFieldData("aCEmpEmplmtLen", primaryEmployment.EmplmtLen_rep);
            }
            if (recordList.CountRegular > 0) 
            {
                IRegularEmploymentRecord previousEmployment = recordList.GetRegularRecordAt(0);
                AddFormFieldData("aCPrevEmpEmplrAddr", Tools.FormatAddress(previousEmployment.EmplrNm, previousEmployment.EmplrAddr, previousEmployment.EmplrCity, previousEmployment.EmplrState, previousEmployment.EmplrZip));
                AddFormFieldData("aCPrevEmpEmplrBusPhone", previousEmployment.EmplrBusPhone);
                AddFormFieldData("aCPrevEmpJobTitle", previousEmployment.JobTitle);
                AddFormFieldData("aCPrevEmpMonI", previousEmployment.MonI_rep);
            }
            AddFormFieldData("sFHAPropImprovBorrHasChecking", dataLoan.sFHAPropImprovBorrHasChecking);
            AddFormFieldData("sFHAPropImprovBorrHasSaving", dataLoan.sFHAPropImprovBorrHasSaving);
            AddFormFieldData("sFHAPropImprovBorrHasNoBankAcount", dataLoan.sFHAPropImprovBorrHasNoBankAcount);
            AddFormFieldData("sFHAPropImprovCoborHasChecking", dataLoan.sFHAPropImprovCoborHasChecking);
            AddFormFieldData("sFHAPropImprovCoborHasSaving", dataLoan.sFHAPropImprovCoborHasSaving);
            AddFormFieldData("sFHAPropImprovCoborHasNoBankAcount", dataLoan.sFHAPropImprovCoborHasNoBankAcount);
            AddFormFieldData("sFHAPropImprovBorrBankInfo", dataLoan.sFHAPropImprovBorrBankInfo.Value);
            AddFormFieldData("sFHAPropImprovCoborBankInfo", dataLoan.sFHAPropImprovCoborBankInfo.Value);

            // TODO: Implement the Debt section
            // TODO: Support additional debts.
            // Currently only print first 2 auto loan, 2 mortgage account, and 11 normal accounts
            int currentAutoIndex = 0;
            int currentMortgageIndex = 0;
            int currentDebtIndex = 0;
            ILiaCollection debtList = dataApp.aLiaCollection;
            for (int index = 0; index < debtList.CountRegular; index++) 
            {
                ILiabilityRegular debt = debtList.GetRegularRecordAt(index);
                if (debt.DebtT.EqualsOneOf(E_DebtRegularT.Mortgage, E_DebtRegularT.Heloc)) 
                {
                    if (currentMortgageIndex < 2) 
                    {
                        AddFormFieldData("ComNm" + (currentMortgageIndex + 2), debt.ComNm);
                        AddFormFieldData("OrigDebtAmt" + (currentMortgageIndex + 2), debt.OrigDebtAmt_rep);
                        AddFormFieldData("IsMortFHAInsured" + (currentMortgageIndex + 2), debt.IsMortFHAInsured ? "Yes" : "No");
                    AddFormFieldData("Bal" + (currentMortgageIndex + 2), debt.Bal_rep);
                        AddFormFieldData("Pmt" + (currentMortgageIndex + 2), debt.Pmt_rep);
                        currentMortgageIndex++;
                    } 
                    else 
                    {
                        continue;
                    }
                } 
                else if (debt.IsForAuto) 
                {
                    // Auto loan
                    if (currentAutoIndex < 2) 
                    {
                        AddFormFieldData("ComNm" + currentAutoIndex, debt.ComNm);
                        AddFormFieldData("AutoYearMake" + currentAutoIndex, debt.AutoYearMake);
                        AddFormFieldData("OrigDebtAmt" + currentAutoIndex, debt.OrigDebtAmt_rep);
                        AddFormFieldData("Bal" + currentAutoIndex, debt.Bal_rep);
                        AddFormFieldData("Pmt" + currentAutoIndex, debt.Pmt_rep);
                        currentAutoIndex++;
                    } 
                    else 
                    {
                        continue;
                    }
                } 
                else 
                {

                    // Revolving/Installment.
                    if (currentDebtIndex < 11) 
                    {
                        AddFormFieldData("ComNm" + (currentDebtIndex + 4), debt.ComNm);
                        AddFormFieldData("AccNum" + (currentDebtIndex + 4), debt.AccNum.Value);
                        AddFormFieldData("OrigDebtAmt" + (currentDebtIndex + 4), debt.OrigDebtAmt_rep);
                        AddFormFieldData("Bal" + (currentDebtIndex + 4), debt.Bal_rep);
                        AddFormFieldData("Pmt" + (currentDebtIndex + 4), debt.Pmt_rep);
                        currentDebtIndex++;
                    } 
                    else 
                    {
                        continue;
                    }

                }
            }

            // Page 3
            AddFormFieldData("sFHAPropImprovIsPropSingleFamily", dataLoan.sFHAPropImprovIsPropSingleFamily);
            AddFormFieldData("sFHAPropImprovIsPropMultifamily", dataLoan.sFHAPropImprovIsPropMultifamily);
            AddFormFieldData("sUnitsNum", dataLoan.sUnitsNum_rep);
            AddFormFieldData("sFHAPropImprovIsPropNonresidential", dataLoan.sFHAPropImprovIsPropNonresidential);
            AddFormFieldData("sFHAPropImprovNonresidentialUsageDesc", dataLoan.sFHAPropImprovNonresidentialUsageDesc);
            AddFormFieldData("sFHAPropImprovIsPropManufacturedHome", dataLoan.sFHAPropImprovIsPropManufacturedHome);
            AddFormFieldData("sFHAPropImprovIsPropHistoricResidential", dataLoan.sFHAPropImprovIsPropHistoricResidential);
            AddFormFieldData("sFHAPropImprovIsPropHistoricResidentialUnitsNum", dataLoan.sFHAPropImprovIsPropHistoricResidentialUnitsNum);
            AddFormFieldData("sFHAPropImprovIsPropHealthCareFacility", dataLoan.sFHAPropImprovIsPropHealthCareFacility);
            AddFormFieldData("sFHAPropImprovIsPropOwnedByBorr", dataLoan.sFHAPropImprovIsPropOwnedByBorr);
            AddFormFieldData("sFHAPropImprovIsPropLeasedFromSomeone", dataLoan.sFHAPropImprovIsPropLeasedFromSomeone);
            AddFormFieldData("sFHAPropImprovIsPropBeingPurchasedOnContract", dataLoan.sFHAPropImprovIsPropBeingPurchasedOnContract);
            AddFormFieldData("sFHAPropImprovIsThereMortOnProp", dataLoan.sFHAPropImprovIsThereMortOnProp);
            AddFormFieldData("PropertyAddress", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            AddFormFieldData("sFHAPropImprovLeaseOwnerInfo", dataLoan.sFHAPropImprovLeaseOwnerInfo);
            AddFormFieldData("sYearBuilt", dataLoan.sYrBuilt);
            AddFormFieldData("sPurchPrice", dataLoan.sPurchPrice_rep);
            AddFormFieldData("sApprVal", dataLoan.sApprVal_rep);
            AddFormFieldData("sFHAPropImprovLeaseMonPmt", dataLoan.sFHAPropImprovLeaseMonPmt_rep);
            AddFormFieldData("sFHAPropImprovLeaseExpireD", dataLoan.sFHAPropImprovLeaseExpireD_rep);
            AddFormFieldData("sFHAPropImprovIsPropNewOccMoreThan90Days", dataLoan.sFHAPropImprovIsPropNewOccMoreThan90Days);
            AddFormFieldData("sSpImprovDesc", dataLoan.sSpImprovDesc);
            AddFormFieldData("sSpImprovC", dataLoan.sSpImprovC_rep);
            AddFormFieldData("sFHAPropImprovDealerContractorContactInfo", dataLoan.sFHAPropImprovDealerContractorContactInfo);

        }


    }
    public class CHUD_56001_1PDF : AbstractHUD_56001PDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-56001_1.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 1"; }
        }
    }

    public class CHUD_56001_2PDF : AbstractHUD_56001PDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-56001_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }
    }

    public class CHUD_56001_3PDF : AbstractHUD_56001PDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-56001_3.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 3"; }
        }
    }

    public class CHUD_56001_4PDF : AbstractHUD_56001PDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-56001_4.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 4"; }
        }

    }
    public class CHUD_56001PDF : AbstractBatchPDF
    {
        public override string Description 
        {
            get { return "FHA Credit Application for Property Improvement (HUD-56001)"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/FHA/FHAPropertyImprovement.aspx"; }
        }

        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            { 
                return new AbstractPDFFile[] {
                                                 new CHUD_56001_1PDF(),
                                                 new CHUD_56001_2PDF(),
                                                 new CHUD_56001_3PDF(),
                                                 new CHUD_56001_4PDF()
                                             };
            }
        }
    }
}
