using System;
using System.Collections.Specialized;
using System.Globalization;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CMortgageBrokerageBusinessContract_1PDF : AbstractLegalPDF 
    {
        public override string PdfFile 
        {
            get { return "MortgageBrokerageBusinessContract_1.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 1"; }
        }
        

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            string borrowerName = dataApp.aCNm.TrimWhitespaceAndBOM() == "" ? dataApp.aBNm : dataApp.aBNm + " and " + dataApp.aCNm;
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("BorrowerName", borrowerName);
            AddFormFieldData("PropertyAddress", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sTerm", dataLoan.sTerm_rep);
            AddFormFieldData("sDue", dataLoan.sDue_rep);
            AddFormFieldData("sLienPosT", dataLoan.sLienPosT);
            AddFormFieldData("sProThisMPmt", dataLoan.sProThisMPmt_rep);

            // TODO: Still missing fields. Waiting for CPageData to add it.
            AddFormFieldData("sFloridaContractDays", dataLoan.sFloridaContractDays_rep);
            AddFormFieldData("sFloridaBorrEstimateSpVal", dataLoan.sFloridaBorrEstimateSpVal_rep);
            AddFormFieldData("sFloridaBorrEstimateExistingMBal", dataLoan.sFloridaBorrEstimateExistingMBal_rep);

            string sFloridaBorrDeposit = dataLoan.sFloridaBorrDeposit_rep; // OPM 51438
            AddFormFieldData("sFloridaBorrDeposit", sFloridaBorrDeposit == ""? "0.00": sFloridaBorrDeposit);

            dataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
            // 1/19/2007 dd - OPM 9598 - These two fields require to be print regardless if its value is zero.
            AddFormFieldData("sFloridaAdditionalCompMinR", dataLoan.sFloridaAdditionalCompMinR_rep);
            AddFormFieldData("sFloridaAdditionalCompMaxR", dataLoan.sFloridaAdditionalCompMaxR_rep);

            AddFormFieldData("sFloridaAdditionalCompMax", dataLoan.sFloridaAdditionalCompMax_rep);
            AddFormFieldData("sFloridaAdditionalCompMin", dataLoan.sFloridaAdditionalCompMin_rep);
            dataLoan.SetFormatTarget(FormatTarget.PrintoutNormalFields);
            
            
            string sFloridaAppFee = dataLoan.sFloridaAppFee_rep; // OPM 51438
            AddFormFieldData("sFloridaAppFee", sFloridaAppFee == "" ? "0.00": sFloridaAppFee);
            string sFloridaBrokerFee = dataLoan.sFloridaBrokerFee_rep; // OPM 51438
            AddFormFieldData("sFloridaBrokerFee", sFloridaBrokerFee == "" ? "0.00" : sFloridaBrokerFee);
            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.FloridaMortBrokerBusinessContract, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (preparer.IsValid) 
            {
                AddFormFieldData("CompanyName", preparer.CompanyName);
                AddFormFieldData("LicenseNumOfCompany", preparer.LicenseNumOfCompany);
                AddFormFieldData("PreparerName", preparer.PreparerName);
            }
            AddFormFieldData("sFloridaIsAppFeeApplicableToCc", dataLoan.sFloridaIsAppFeeApplicableToCc);
            AddFormFieldData("sFloridaIsAppFeeRefundable", dataLoan.sFloridaIsAppFeeRefundable);
        }
    }

    public class CMortgageBrokerageBusinessContract_2PDF : AbstractLegalPDF 
    {
        public override string PdfFile 
        {
            get { return "MortgageBrokerageBusinessContract_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);

        }
    }

    public class CMortgageBrokerageBusinessContractPDF : AbstractBatchPDF
    {

        public override string Description 
        {
            get { return "Florida Mortgage Brokerage Business Contract"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/MortgageBrokerageBusinessContract.aspx"; }
        }


        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            { 
                return new AbstractPDFFile[] {
                                                 new CMortgageBrokerageBusinessContract_1PDF(),
                                                 new CMortgageBrokerageBusinessContract_2PDF()
                                             };
            }
        }
    }

}
