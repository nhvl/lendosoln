using System;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CPMILPMIPDF : AbstractLetterPDF
    {
        public override string PdfFile 
        {
            get { return "PrivateMortgageInsuranceLPMI.pdf"; }
        }

        public override string Description 
        {
            get { return "PMI Disclosure - LPMI"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("GfeTilPrepareDate",  dataLoan.GetPreparerOfForm( E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject ).PrepareDate_rep );
            AddFormFieldData("sSpAddr", dataLoan.sSpAddr);
            AddFormFieldData("PropertyAddress2", Tools.CombineCityStateZip(dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));

        }


    }
}
