using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CVA_26_8923PDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "VA-26-8923.pdf";        	 }
        }
        public override string Description 
        {
            get { return "VA Refinancing Worksheet (VA 26-8923)"; }
        }
		public override bool OnlyPrintPrimary
		{
			get { return true; }
		}
        public override string EditLink 
        {
            get { return "/newlos/VA/VARefiWorksheet.aspx"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sVaRefiWsAllowableCcAndPp",            dataLoan.sVaRefiWsAllowableCcAndPp_rep);
            AddFormFieldData("sVaRefiWsCashPmtFromVet",              dataLoan.sVaRefiWsCashPmtFromVet_rep);
            AddFormFieldData("sVaRefiWsDiscnt",                      dataLoan.sVaRefiWsDiscnt_rep);
            AddFormFieldData("sVaRefiWsDiscntPc",                    dataLoan.sVaRefiWsDiscntPc_rep);
            AddFormFieldData("sVaRefiWsExistingVaLBal",              dataLoan.sVaRefiWsExistingVaLBal_rep);
            AddFormFieldData("sVaRefiWsExistingVaLBalAfterCashPmt",  dataLoan.sVaRefiWsExistingVaLBalAfterCashPmt_rep);
            AddFormFieldData("sVaRefiWsExistingVaLBalAfterCashPmt2", dataLoan.sVaRefiWsExistingVaLBalAfterCashPmt_rep);
            AddFormFieldData("sVaRefiWsFf",                          dataLoan.sVaRefiWsFf_rep);
            AddFormFieldData("sVaRefiWsFfPc",                        dataLoan.sVaRefiWsFfPc_rep);
            AddFormFieldData("sVaRefiWsFinalDiscnt",                 dataLoan.sVaRefiWsFinalDiscnt_rep);
            AddFormFieldData("sVaRefiWsFinalDiscntPc",               dataLoan.sVaRefiWsFinalDiscntPc_rep);
            AddFormFieldData("sVaRefiWsFinalFf",                     dataLoan.sVaRefiWsFinalFf_rep);
            AddFormFieldData("sVaRefiWsFinalSubtotalItem12",         dataLoan.sVaRefiWsFinalSubtotalItem12_rep);
            AddFormFieldData("sVaRefiWsFinalSubtotalItem14",         dataLoan.sVaRefiWsFinalSubtotalItem14_rep);
            AddFormFieldData("sVaRefiWsFinalSubtotalItem16",         dataLoan.sVaRefiWsFinalSubtotalItem16_rep);
            AddFormFieldData("sVaRefiWsMaxLAmt",                     dataLoan.sVaRefiWsMaxLAmt_rep);
            AddFormFieldData("sVaRefiWsOrigFee",                     dataLoan.sVaRefiWsOrigFee_rep);
            AddFormFieldData("sVaRefiWsOrigFeePc",                   dataLoan.sVaRefiWsOrigFeePc_rep);
            AddFormFieldData("sVaRefiWsPreliminaryTot",              dataLoan.sVaRefiWsPreliminaryTot_rep);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_8928Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("VA26_8928LenderPrepareDate", preparer.PrepareDate_rep);
            AddFormFieldData("VA26_8928LenderTitle", preparer.Title);
            AddFormFieldData("VA26_8928LenderCompanyName", preparer.CompanyName);

        }
	}
}
