using System;
using System.Collections.Specialized;
using System.Data;

using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{

    public class CHUD_92900_WS_1PDF : AbstractLetterPDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-92900-WS_1.pdf"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/FHA/FHACreditAnalysisRefinance.aspx"; }
        }
        public override string Description 
        {
            get { return "Page 1"; }
        }
 
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("aFHAOtherDebtPmtMCAWPrinting", dataApp.aFHAOtherDebtPmtMCAWPrinting_rep);
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sFHAHousingActSection", dataLoan.sFHAHousingActSection);
            AddFormFieldData("sFHAConstructionT", dataLoan.sFHAConstructionT);

            IPreparerFields underwriter = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHACreditAnalysisRefinanceUnderwriter, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("FHACreditAnalysisRefinanceLicenseNumOfAgent", underwriter.LicenseNumOfAgent);

            AddFormFieldData("sLAmt", dataLoan.sLAmtCalc_rep);
            AddFormFieldData("sFfUfmip1003", dataLoan.sFfUfmip1003_rep);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sApprVal", dataLoan.sApprVal_rep);
            AddFormFieldData("sFHACcTot", dataLoan.sFHACcTot_rep);
            AddFormFieldData("sFHACcPbs", dataLoan.sFHACcPbs_rep);
            AddFormFieldData("sFHACcPbb", dataLoan.sFHACcPbb_rep);
            AddFormFieldData("sPresLTotPersistentHExp", dataApp.aPresTotHExp_rep);
            AddFormFieldData("sTermInYr", dataLoan.sTermInYr_rep);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sBuydownResultIR", dataLoan.sBuydownResultIR_rep);
            AddFormFieldData("sFHAExistingMLien", dataLoan.sFHAExistingMLien_rep);
            AddFormFieldData("sFHAImprovementsDesc", dataLoan.sFHAImprovementsDesc);
            AddFormFieldData("sFHAImprovements", dataLoan.sFHAImprovements_rep);
            AddFormFieldData("sFHASalesConcessions", dataLoan.sFHASalesConcessions_rep);
            AddFormFieldData("sFHAMBasisRefin", dataLoan.sFHAMBasisRefin_rep);
            AddFormFieldData("sFHAMBasisRefinMultiply", dataLoan.sFHAMBasisRefinMultiply_rep);
            AddFormFieldData("sFHAApprValMultiply", dataLoan.sFHAApprValMultiply_rep);
            AddFormFieldData("sFHAReqInvestment", dataLoan.sFHAReqInvestment_rep);
            AddFormFieldData("sFHADiscountPoints", dataLoan.sFHADiscountPoints_rep);
            AddFormFieldData("sFHAPrepaidExp", dataLoan.sFHAPrepaidExp_rep);
            AddFormFieldData("sUfCashPd", dataLoan.sUfCashPd_rep);
            AddFormFieldData("sFHANonrealty", dataLoan.sFHANonrealty_rep);
            AddFormFieldData("sFHAReqTot", dataLoan.sFHAReqTot_rep);
            AddFormFieldData("sFHAAmtPaid", dataLoan.sFHAAmtPaid_rep);
            AddFormFieldData("sFHAAmtToBePd", dataLoan.sFHAAmtToBePd_rep);
            AddFormFieldData("sFHAAssetAvail", dataApp.aFHAAssetAvail_rep);
            AddFormFieldData("sFHA2ndMAmt", dataLoan.sFHA2ndMAmt_rep);
            AddFormFieldData("sFHABBaseI", dataApp.aFHABBaseI_rep);
            AddFormFieldData("sFHABOI", dataApp.aFHABOI_rep);
            AddFormFieldData("sFHACBaseI", dataApp.aFHACBaseI_rep);
            AddFormFieldData("sFHACOI", dataApp.aFHACOI_rep);
            AddFormFieldData("sFHANetRentalI", dataApp.aFHANetRentalI_rep);
            AddFormFieldData("sFHAGrossMonI", dataApp.aFHAGrossMonI_rep);
            AddFormFieldData("sFHADebtInstallPmt", dataApp.aFHADebtInstallPmt_rep);
            AddFormFieldData("sFHADebtInstallBal", dataApp.aFHADebtInstallBal_rep);
            AddFormFieldData("sFHAChildSupportPmt", dataApp.aFHAChildSupportPmt_rep);
            AddFormFieldData("sFHAOtherDebtPmt", dataApp.aFHAOtherDebtPmt_rep);
            AddFormFieldData("sFHAOtherDebtBal", dataApp.aFHAOtherDebtBal_rep);
            AddFormFieldData("sFHADebtPmtTot", dataApp.aFHADebtPmtTot_rep);
            AddFormFieldData("sFHAPro1stMPmt", dataLoan.sFHAPro1stMPmt_rep);
            AddFormFieldData("sFHAProMIns", dataLoan.sFHAProMIns_rep);
            AddFormFieldData("sFHAProHoAssocDues", dataLoan.sFHAProHoAssocDues_rep);
            AddFormFieldData("sFHAProGroundRent", dataLoan.sFHAProGroundRent_rep);
            AddFormFieldData("sFHAPro2ndFinPmt", dataLoan.sFHAPro2ndFinPmt_rep);
            AddFormFieldData("sFHAProHazIns", dataLoan.sFHAProHazIns_rep);
            AddFormFieldData("sFHAProRealETx", dataLoan.sFHAProRealETx_rep);
            AddFormFieldData("sFHAMonthlyPmt", dataLoan.sFHAMonthlyPmt_rep);
            AddFormFieldData("sFHAPmtFixedTot", dataApp.aFHAPmtFixedTot_rep);
            AddFormFieldData("sFHAMPmtToIRatio", dataApp.aFHAMPmtToIRatio_rep);
            AddFormFieldData("sFHAFixedPmtToIRatio", dataApp.aFHAFixedPmtToIRatio_rep);
            AddFormFieldData("sFHACreditRating", dataApp.aFHACreditRating);
            AddFormFieldData("sFHARatingIAdequacy", dataApp.aFHARatingIAdequacy);
            AddFormFieldData("sFHARatingIStability", dataApp.aFHARatingIStability);
            AddFormFieldData("sFHARatingAssetAdequacy", dataApp.aFHARatingAssetAdequacy);
            AddFormFieldData("sFHABCaivrsNum", dataApp.aFHABCaivrsNum);
            AddFormFieldData("sFHACCaivrsNum", dataApp.aFHACCaivrsNum);
            AddFormFieldData("sFHABLpdGsa", dataApp.aFHABLpdGsa);
            AddFormFieldData("sFHACLpdGsa", dataApp.aFHACLpdGsa);
            AddFormFieldData("sPurchPrice6pc", dataLoan.sPurchPrice6pc_rep);
            AddFormFieldData("sFHASellerContribution", dataLoan.sFHASellerContribution_rep);
            AddFormFieldData("sFHAExcessContribution", dataLoan.sFHAExcessContribution_rep);
            AddFormFieldData("sFHACreditAnalysisRemarks", dataLoan.sFHACreditAnalysisRemarks);
            AddFormFieldData("sFHAGiftAmtTot", dataLoan.sFHAGiftAmtTot_rep);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("aBSsn", dataApp.aBSsn);
            AddFormFieldData("aCSsn", dataApp.aCSsn);
			AddFormFieldData("sFHALtv2", dataLoan.sFHALtvRefi_rep);
            AddFormFieldData("sFHAIsAmtPdInCash", dataLoan.sFHAIsAmtPdInCash);
            AddFormFieldData("sFHAIsAmtPdInOther", dataLoan.sFHAIsAmtPdInOther);
            AddFormFieldData("sFHAIsAmtToBePdInCash", dataLoan.sFHAIsAmtToBePdInCash);
            AddFormFieldData("sFHAIsAmtToBePdInOther", dataLoan.sFHAIsAmtToBePdInOther);
        }
    }

    public class CHUD_92900_WS_2PDF : AbstractLetterPDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-92900-WS_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
        }


    }

    public class CHUD_92900_WSPDF : AbstractBatchPDF
    {
 
        public override string Description 
        {
            get { return "FHA MCAW - Refinance (HUD-92900-WS)"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/FHA/FHACreditAnalysisRefinance.aspx"; }
        }
        
        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            { 
                return new AbstractPDFFile[] {
                                                 new CHUD_92900_WS_1PDF(),
                                                 new CHUD_92900_WS_2PDF()
                                             };
            }
        }

    }

    public class CHUD_92900_WS_Combined_1PDF : AbstractLetterPDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-92900-WS_1.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 1"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/FHA/FHACreditAnalysisRefinanceCombine.aspx"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("aFHAOtherDebtPmtMCAWPrinting", dataLoan.sFHAOtherDebtPmtMCAWPrinting_rep);
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sFHAHousingActSection", dataLoan.sFHAHousingActSection);
            AddFormFieldData("sFHAConstructionT", dataLoan.sFHAConstructionT);

            IPreparerFields underwriter = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHACreditAnalysisRefinanceUnderwriter, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("FHACreditAnalysisRefinanceLicenseNumOfAgent", underwriter.LicenseNumOfAgent);

            AddFormFieldData("sLAmt", dataLoan.sLAmtCalc_rep);
            AddFormFieldData("sFfUfmip1003", dataLoan.sFfUfmip1003_rep);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sApprVal", dataLoan.sApprVal_rep);
            AddFormFieldData("sFHACcTot", dataLoan.sFHACcTot_rep);
            AddFormFieldData("sFHACcPbs", dataLoan.sFHACcPbs_rep);
            AddFormFieldData("sFHACcPbb", dataLoan.sFHACcPbb_rep);
            AddFormFieldData("sPresLTotPersistentHExp", dataLoan.sPresLTotHExp_rep );
            AddFormFieldData("sTermInYr", dataLoan.sTermInYr_rep);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sBuydownResultIR", dataLoan.sBuydownResultIR_rep);
            AddFormFieldData("sFHAExistingMLien", dataLoan.sFHAExistingMLien_rep);
            AddFormFieldData("sFHAImprovementsDesc", dataLoan.sFHAImprovementsDesc);
            AddFormFieldData("sFHAImprovements", dataLoan.sFHAImprovements_rep);
            AddFormFieldData("sFHASalesConcessions", dataLoan.sFHASalesConcessions_rep);
            AddFormFieldData("sFHAMBasisRefin", dataLoan.sFHAMBasisRefin_rep);
            AddFormFieldData("sFHAMBasisRefinMultiply", dataLoan.sFHAMBasisRefinMultiply_rep);
            AddFormFieldData("sFHAApprValMultiply", dataLoan.sFHAApprValMultiply_rep);
            AddFormFieldData("sFHAReqInvestment", dataLoan.sFHAReqInvestment_rep);
            AddFormFieldData("sFHADiscountPoints", dataLoan.sFHADiscountPoints_rep);
            AddFormFieldData("sFHAPrepaidExp", dataLoan.sFHAPrepaidExp_rep);
            AddFormFieldData("sUfCashPd", dataLoan.sUfCashPd_rep);
            AddFormFieldData("sFHANonrealty", dataLoan.sFHANonrealty_rep);
            AddFormFieldData("sFHAReqTot", dataLoan.sFHAReqTot_rep);
            AddFormFieldData("sFHAAmtPaid", dataLoan.sFHAAmtPaid_rep);
            AddFormFieldData("sFHAAmtToBePd", dataLoan.sFHAAmtToBePd_rep);
            AddFormFieldData("sFHAAssetAvail", dataLoan.sFHAAssetAvail_rep);
            AddFormFieldData("sFHA2ndMAmt", dataLoan.sFHA2ndMAmt_rep);
            AddFormFieldData("sFHABBaseI", dataLoan.sFHABBaseI_rep);
            AddFormFieldData("sFHABOI", dataLoan.sFHABOI_rep);
            AddFormFieldData("sFHACBaseI", dataLoan.sFHACBaseI_rep);
            AddFormFieldData("sFHACOI", dataLoan.sFHACOI_rep);
            AddFormFieldData("sFHANetRentalI", dataLoan.sFHANetRentalI_rep);
            AddFormFieldData("sFHAGrossMonI", dataLoan.sFHAGrossMonI_rep);
            AddFormFieldData("sFHADebtInstallPmt", dataLoan.sFHADebtInstallPmt_rep);
            AddFormFieldData("sFHADebtInstallBal", dataLoan.sFHADebtInstallBal_rep);
            AddFormFieldData("sFHAChildSupportPmt", dataLoan.sFHAChildSupportPmt_rep);
            AddFormFieldData("aFHAOtherDebtPmt", dataApp.aFHAOtherDebtPmt_rep);
            AddFormFieldData("sFHAOtherDebtBal", dataLoan.sFHAOtherDebtBal_rep);
            AddFormFieldData("sFHADebtPmtTot", dataLoan.sFHADebtPmtTot_rep);
            AddFormFieldData("sFHAPro1stMPmt", dataLoan.sFHAPro1stMPmt_rep);
            AddFormFieldData("sFHAProMIns", dataLoan.sFHAProMIns_rep);
            AddFormFieldData("sFHAProHoAssocDues", dataLoan.sFHAProHoAssocDues_rep);
            AddFormFieldData("sFHAProGroundRent", dataLoan.sFHAProGroundRent_rep);
            AddFormFieldData("sFHAPro2ndFinPmt", dataLoan.sFHAPro2ndFinPmt_rep);
            AddFormFieldData("sFHAProHazIns", dataLoan.sFHAProHazIns_rep);
            AddFormFieldData("sFHAProRealETx", dataLoan.sFHAProRealETx_rep);
            AddFormFieldData("sFHAMonthlyPmt", dataLoan.sFHAMonthlyPmt_rep);
            AddFormFieldData("sFHAPmtFixedTot", dataLoan.sFHAPmtFixedTot_rep);
            AddFormFieldData("sFHAMPmtToIRatio", dataLoan.sFHAMPmtToIRatio_rep);
            AddFormFieldData("sFHAFixedPmtToIRatio", dataLoan.sFHAFixedPmtToIRatio_rep);
            AddFormFieldData("sFHACreditRating", dataLoan.sFHACreditRating);
            AddFormFieldData("sFHARatingIAdequacy", dataLoan.sFHARatingIAdequacy);
            AddFormFieldData("sFHARatingIStability", dataLoan.sFHARatingIStability);
            AddFormFieldData("sFHARatingAssetAdequacy", dataLoan.sFHARatingAssetAdequacy);
            AddFormFieldData("sFHABCaivrsNum", dataLoan.sFHABCaivrsNum);
            AddFormFieldData("sFHACCaivrsNum", dataLoan.sFHACCaivrsNum);
            AddFormFieldData("sFHABLpdGsa", dataLoan.sFHABLpdGsa);
            AddFormFieldData("sFHACLpdGsa", dataLoan.sFHACLpdGsa);
            AddFormFieldData("sPurchPrice6pc", dataLoan.sPurchPrice6pc_rep);
            AddFormFieldData("sFHASellerContribution", dataLoan.sFHASellerContribution_rep);
            AddFormFieldData("sFHAExcessContribution", dataLoan.sFHAExcessContribution_rep);
            AddFormFieldData("sFHACreditAnalysisRemarks", dataLoan.sFHACreditAnalysisRemarks);
            AddFormFieldData("sFHAGiftAmtTot", dataLoan.sFHAGiftAmtTot_rep);
            AddFormFieldData("aBNm", dataLoan.sCombinedBorNm);
            AddFormFieldData("aCNm", dataLoan.sCombinedCoborNm);
            AddFormFieldData("aBSsn", dataLoan.sCombinedBorSsn);
            AddFormFieldData("aCSsn", dataLoan.sCombinedCoborSsn);
			AddFormFieldData("sFHALtv2", dataLoan.sFHALtvRefi_rep);
        }

    }

    public class CHUD_92900_WS_CombinedPDF : AbstractBatchPDF 
    {
 
        public override string Description 
        {
            get { return "FHA MCAW - Combined Refinance (HUD-92900-WS)"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/FHA/FHACreditAnalysisRefinanceCombine.aspx"; }
        }
		public override bool OnlyPrintPrimary
		{
			get { return true; }
		}
        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            { 
                return new AbstractPDFFile[] {
                                                 new CHUD_92900_WS_Combined_1PDF(),
                                                 new CHUD_92900_WS_2PDF()
                                             };
            }
        }

    }
}
