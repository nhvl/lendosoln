namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Specialized;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using DataAccess;
    using LendersOffice.AntiXss;
    using LendersOffice.Common;
    using LendersOffice.PdfGenerator;
    using LOPDFGenLib;

    public abstract class AbstractFreeLayoutPDFFile : IPDFGenerator, IPDFPrintItem
	{
        private Guid m_loanID = Guid.Empty;
        private Guid m_applicationID = Guid.Empty;
        private CPageData m_dataLoan = null;
        private CAppData m_dataApp = null;
        private NameValueCollection m_arguments;
        private E_PdfPrintOptions m_printOptions = E_PdfPrintOptions.FormWithData;
        private bool m_isDebug = false;
        private List<AbstractItem> m_list = new List<AbstractItem>();
        private string m_id;

        public virtual IEnumerable<string> DependencyFields
        {
            get
            {
                return CPageData.GetCPageBaseAndCAppDataDependencyList(this.GetType());
            }
        }

        public virtual Guid RecordID 
        {
            get { return Guid.Empty; }
        }

        public virtual string Name 
        {
            get 
            {
                // 5/24/2004 dd - If class name has following format C{name}PDF then return unique code as {name}
                string name = GetType().Name;
                if (name.StartsWith("C") && name.EndsWith("PDF")) 
                {
                    name = name.Substring(1, name.Length - 4);
                }
                return name;  
            }
        }

        public virtual string Description 
        {
            get { return GetType().FullName; }
        }

        public virtual string EditLink 
        {
            get { return ""; }
        }

        public virtual string PreviewLink 
        {
            get 
            {
                return string.Format(
                    @"(<a href=""#"" onclick=""return _preview('{0}.aspx', {1});"">preview</a>)", 
                    AspxTools.JsStringUnquoted(Name), 
                    AspxTools.JsString(ID)); 
            }
        }

        public string ID 
        {
            get { return m_id; }
            set { m_id = value; }
        }

        public virtual bool IsVisible 
        {
            get { return true; }
        }

        public virtual bool HasPdf
        {
            get { return true; }
        }

        public bool OnlyPrintPrimary 
        {
			// 1/29/2007 nw - OPM 9805 - Remove method public override bool OnlyPrintPrimary
			// This isn't an override and we can't delete this, so we should set the value to false
			// 3/2/2007 nw - OPM 9805 - This needs to return true for the child classes
            get { return true; }
        }

        public NameValueCollection Arguments 
        {
            get { return this.m_arguments; }
            set 
            { 
                this.m_arguments = value;

                if (this.m_arguments == null)
                {
                    return;
                }

                Guid argLoanId;

                if (Guid.TryParse(this.m_arguments["loanid"], out argLoanId))
                {
                    this.m_loanID = argLoanId;
                }

                // FIXME: Old code doesn't actually set this.m_applicationID, but it seems like
                //        it probably should. Determine correct behavior.
                ////Guid argAppId;

                ////if (Guid.TryParse(this.m_arguments["applicationid"], out argAppId))
                ////{
                ////    this.m_applicationID = argAppId;
                ////}

                int argPrintOptions;

                if (int.TryParse(this.m_arguments["printoptions"], out argPrintOptions))
                {
                    this.m_printOptions = (E_PdfPrintOptions)argPrintOptions;
                }
            }
        }

        public void SetCurrentDataApp(CAppData dataApp) 
        {
            m_dataApp = dataApp;
            m_applicationID = dataApp.aAppId;
        }

        public void SetPdfPrintOptions(E_PdfPrintOptions options) 
        {
            m_printOptions = options;
        }

        public void RenderPrintLink(StringBuilder sb, bool isIndent) 
        {
            if (!IsVisible) return;

            if (null == m_dataLoan)
                InitializeDataLoan();

            bool isAppSpecific = Guid.Empty != m_applicationID;
            int nApps = (OnlyPrintPrimary || isAppSpecific) ? 1 : m_dataLoan.nApps;


            for (int i = 0; i < nApps; i++) 
            {
                if (isAppSpecific)
                    SetCurrentDataApp(m_dataLoan.GetAppData(m_applicationID));
                else
                    SetCurrentDataApp(m_dataLoan.GetAppData(i));

                string edit = EditLink;
                if (edit != "") 
                {
                    edit = string.Format(@"(<a href=""javascript:_link('{0}{1}', '{2}');"">edit</a>)", Tools.VRoot, edit, this.ID);
                }
                string desc = Description;
                if (nApps > 1) 
                {
                    desc += " - " + m_dataApp.aBNm;
                    if (m_dataApp.aCNm != "")
                        desc += " & " + m_dataApp.aCNm;
                }
                RenderPrintLink(sb, ID, desc, "", edit, PreviewLink, isIndent);
            }

        }

        public void RenderPrintLink(StringBuilder sb) 
        {
            RenderPrintLink(sb, false);
        }

        protected void RenderPrintLink(StringBuilder sb, string id, string desc, string script, string editLink, string previewLink, bool isIndent) 
        {
            string extra = "appid='" + m_applicationID + "'";
            if (RecordID != Guid.Empty)
                extra += " recordid='" + RecordID + "'";
            extra += " pageSize='" + (this.PageSize == LendersOffice.PdfGenerator.PDFPageSize.Letter ? "0" : "1") + "'";
            
            string disabledCheckbox = string.Empty;
            string accessDeniedMsg = string.Empty;

            if (!HasPrintPermission)
            {
                disabledCheckbox = "disabled='disabled'";
                accessDeniedMsg = "<span style='font-weight:bold;color:red'>" + ErrorMessages.PdfPrintAccessDenied + "</span>";
                previewLink = string.Empty;

            }
            string checkboxHtml = string.Format("<input name='{0}' type=checkbox {1} pdf='{2}' {3} {4} {5}>"
                , id // 0
                , script // 1
                , GetType().Name // 2
                , extra // 3
                , OnlyPrintPrimary ? " primaryOnly=t " : "" //4
                , disabledCheckbox // 5
                );
            if (isIndent)
            {
                sb.AppendFormat("<tr><td>&nbsp;</td><td>{0}&nbsp;{1} {2} {3} {4}</td></tr>"
                    , checkboxHtml // 0
                    , desc // 1
                    , editLink // 2
                    , previewLink // 3
                    , accessDeniedMsg // 4
                    );
            }
            else
            {
                sb.AppendFormat("<tr><td>{0}</td><td>{1} {2} {3} {4}</td></tr>"
                    , checkboxHtml // 0
                    , desc // 1
                    , editLink // 2
                    , previewLink // 3
                    , accessDeniedMsg // 4
                    );
            }
        }

        protected bool IsDebug 
        {
            get { return m_isDebug; }
            set { m_isDebug = value; }
        }

        public Guid LoanID
        {
            get { return m_loanID; }
        }

        public Guid ApplicationID
        {
            get { return m_applicationID; }
        }

        public CPageData DataLoan
        {
            get { return m_dataLoan; }
            set { 
                m_dataLoan = value; 
                m_loanID = m_dataLoan.sLId;
            }
        }

        protected abstract void ApplyData(CPageData dataLoan, CAppData dataApp);

        private void InitializeDataLoan() 
        {
            m_dataLoan = Utilities.GetPDFPrintData(m_loanID);
            m_dataLoan.InitLoad();
        }

        public void GeneratePDF(Stream outputStream, string ownerPassword, string userPassword)
        {
            GeneratePDFImpl(outputStream, ownerPassword, userPassword);
        }
        private void GeneratePDFImpl(Stream outputStream, string ownerPassword, string userPassword)
        {
            if (m_dataLoan == null)
                InitializeDataLoan();

            AddTitle();

            if (m_printOptions == E_PdfPrintOptions.FormWithData)
            {
                m_dataLoan.SetFormatTarget(FormatTarget.PrintoutNormalFields);
                ApplyData(m_dataLoan, m_dataLoan.GetAppData(0)); // Only do primary borrower.
            }

            PDFGenerator pdf = new PDFGenerator(m_list);
            if (m_isDebug) pdf.IsPrintPoint = true;

            byte[] buffer = pdf.GeneratePDF(ownerPassword, userPassword);
            outputStream.Write(buffer, 0, buffer.Length);

        }

        /// <summary>
        /// Render title of freelayout form.
        /// </summary>
        protected abstract void AddTitle();

        public void AddItem(AbstractItem item) 
        {
            m_list.Add(item);
        }
        public PDFPageSize PageSize 
        {
            get { return PDFPageSize.Legal; }
        }

        public bool HasPdfFormLayout => false;

        public LendersOffice.PdfLayout.PdfFormLayout PdfFormLayout
        {
            get { throw new NotImplementedException(); }
        }

        public string PdfName
        {
            get;
            set;
        }

        #region IPDFPrintItem Members


        public virtual bool HasPrintPermission
        {
            get { return true; }
        }

        #endregion
    }
}
