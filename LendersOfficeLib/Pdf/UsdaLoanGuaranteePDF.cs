﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.Globalization;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CUsdaLoanGuarantee_1PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "UsdaLoanGuarantee_1.pdf"; }
        }
        public override string Description
        {
            get { return "Page 1"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            // ** Approved Lender Table
            IPreparerFields approvedLender = dataLoan.GetPreparerOfForm(E_PreparerFormT.UsdaGuaranteeApprovedLender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("UsdaGuaranteeApprovedLenderCompanyName", approvedLender.CompanyName);
            AddFormFieldData("UsdaGuaranteeApprovedLenderTaxId", approvedLender.TaxId);

            AddFormFieldData("UsdaGuaranteeApprovedLenderPreparerName", approvedLender.PreparerName);
            AddFormFieldData("UsdaGuaranteeApprovedLenderEmailAddr", approvedLender.EmailAddr);
            AddFormFieldData("UsdaGuaranteeApprovedLenderPhoneOfCompany", approvedLender.PhoneOfCompany);
            AddFormFieldData("UsdaGuaranteeApprovedLenderFaxOfCompany", approvedLender.FaxOfCompany);
            AddFormFieldData("UsdaGuaranteeApprovedLenderTitle", approvedLender.Title);

            IPreparerFields thirdPartyTPO = dataLoan.GetPreparerOfForm(E_PreparerFormT.UsdaGuaranteeThirdPartyOriginator, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("UsdaGuaranteeThirdPartyOriginatorCompanyName", thirdPartyTPO.CompanyName);
            AddFormFieldData("UsdaGuaranteeThirdPartyOriginatorTaxId", thirdPartyTPO.TaxId);

            AddFormFieldData("sUsdaGuaranteeNum", dataLoan.sUsdaGuaranteeNum);
            AddFormFieldData("sLNm", dataLoan.sLNm);

            // ** Applicant/Co-Applicant Info Table
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aBSsn", dataApp.aBSsn);
            AddFormFieldData("aBDob", dataApp.aBDob_rep);
            AddFormFieldData("aBDecCitizen", dataApp.aBDecCitizen);
            AddFormFieldData("aBDecResidency", dataApp.aBDecResidency);

            AddFormFieldData("aBUsdaGuaranteeVeteranTri", dataApp.aBUsdaGuaranteeVeteranTri);
            AddFormFieldData("aBUsdaGuaranteeDisabledTri", dataApp.aBUsdaGuaranteeDisabledTri);
            AddFormFieldData("aBGender", dataApp.aBGenderFallback);
            AddFormFieldData("aBUsdaGuaranteeFirstTimeHomeBuyerTri", dataApp.aBUsdaGuaranteeFirstTimeHomeBuyerTri);

            AddFormFieldData("aBHispanicT", dataApp.aBHispanicTFallback);

            AddFormFieldData("aBIsAmericanIndian", dataApp.aBIsAmericanIndian);
            AddFormFieldData("aBIsPacificIslander", dataApp.aBIsPacificIslander);
            AddFormFieldData("aBIsAsian", dataApp.aBIsAsian);
            AddFormFieldData("aBIsWhite", dataApp.aBIsWhite);
            AddFormFieldData("aBIsBlack", dataApp.aBIsBlack);

            AddFormFieldData("aBMaritalStatT", dataApp.aBMaritalStatT);

            AddFormFieldData("aBUsdaGuaranteeEmployeeRelationshipT", dataApp.aBUsdaGuaranteeEmployeeRelationshipT);
            AddFormFieldData("aBUsdaGuaranteeEmployeeRelationshipDesc", dataApp.aBUsdaGuaranteeEmployeeRelationshipDesc);

            AddFormFieldData("aBDecisionCreditScore", dataApp.aBDecisionCreditScore_rep);
            AddFormFieldData("aBHasHighestScore", !dataApp.aBHasHighestScore);

            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("aCSsn", dataApp.aCSsn);
            AddFormFieldData("aCDob", dataApp.aCDob_rep);
            AddFormFieldData("aCDecCitizen", dataApp.aCDecCitizen);
            AddFormFieldData("aCDecResidency", dataApp.aCDecResidency);

            AddFormFieldData("aCUsdaGuaranteeVeteranTri", dataApp.aCUsdaGuaranteeVeteranTri);
            AddFormFieldData("aCUsdaGuaranteeDisabledTri", dataApp.aCUsdaGuaranteeDisabledTri);
            AddFormFieldData("aCGender", dataApp.aCGenderFallback);
            AddFormFieldData("aCUsdaGuaranteeFirstTimeHomeBuyerTri", dataApp.aCUsdaGuaranteeFirstTimeHomeBuyerTri);

            AddFormFieldData("aCHispanicT", dataApp.aCHispanicTFallback);

            AddFormFieldData("aCIsAmericanIndian", dataApp.aCIsAmericanIndian);
            AddFormFieldData("aCIsPacificIslander", dataApp.aCIsPacificIslander);
            AddFormFieldData("aCIsAsian", dataApp.aCIsAsian);
            AddFormFieldData("aCIsWhite", dataApp.aCIsWhite);
            AddFormFieldData("aCIsBlack", dataApp.aCIsBlack);

            AddFormFieldData("aCMaritalStatT", dataApp.aCMaritalStatT);

            AddFormFieldData("aCUsdaGuaranteeEmployeeRelationshipT", dataApp.aCUsdaGuaranteeEmployeeRelationshipT);
            AddFormFieldData("aCUsdaGuaranteeEmployeeRelationshipDesc", dataApp.aCUsdaGuaranteeEmployeeRelationshipDesc);

            AddFormFieldData("aCDecisionCreditScore", dataApp.aCDecisionCreditScore_rep);
            AddFormFieldData("aCHasHighestScore", !dataApp.aCHasHighestScore);

            // ** Extra information
            AddFormFieldData("sSpAddr", dataLoan.sSpAddr);
            AddFormFieldData("sSpCity", dataLoan.sSpCity);
            AddFormFieldData("sSpState", dataLoan.sSpState);

            AddFormFieldData("sSpZip", dataLoan.sSpZip);

            AddFormFieldData("sSpCounty", dataLoan.sSpCounty);

            AddFormFieldData("sUsdaGuaranteeIsRefinanceLoan", dataLoan.sUsdaGuaranteeIsRefinanceLoan);
            AddFormFieldData("sUsdaGuaranteeRefinancedLoanT", dataLoan.sUsdaGuaranteeRefinancedLoanT);

            AddFormFieldData("sUsdaGuaranteeNumOfPersonInHousehold", dataLoan.sUsdaGuaranteeNumOfPersonInHousehold_rep);
            AddFormFieldData("sUsdaGuaranteeNumOfDependents", dataLoan.sUsdaGuaranteeNumOfDependents_rep);

            AddFormFieldData("sLTotI", dataLoan.sLTotI_rep);
            AddFormFieldData("sUsdaGuaranteeAdjustedAnnualIncome", dataLoan.sUsdaGuaranteeAdjustedAnnualIncome_rep);

            AddFormFieldData("sQualTopR", dataLoan.sQualTopR_rep);
            AddFormFieldData("sQualBottomR", dataLoan.sQualBottomR_rep);

            AddFormFieldData("sLAmtCalc", dataLoan.sLAmtCalc_rep);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sMonthlyPmt", dataLoan.sMonthlyPmt_rep);

            AddFormFieldData("sUsdaGuaranteeIsNoteRateBasedOnFnma", dataLoan.sUsdaGuaranteeIsNoteRateBasedOnFnma);
            AddFormFieldData("sUsdaGuaranteeNoteRateBasedOnFnmaD", dataLoan.sUsdaGuaranteeNoteRateBasedOnFnmaD_rep);

            AddFormFieldData("sUsdaGuaranteeIsNoteRateLocked", dataLoan.sUsdaGuaranteeIsNoteRateLocked);
            AddFormFieldData("sUsdaGuaranteeNoteRateLockedD", dataLoan.sUsdaGuaranteeNoteRateLockedD_rep);

            AddFormFieldData("sUsdaGuaranteeIsNoteRateFloat", dataLoan.sUsdaGuaranteeIsNoteRateFloat);

            // ** Loan funds purpose
            AddFormFieldData("sUsdaGuaranteePurchaseAmtDesc", dataLoan.sUsdaGuaranteePurchaseAmtDesc);
            AddFormFieldData("sUsdaGuaranteePurchaseAmt", dataLoan.sUsdaGuaranteePurchaseAmt_rep);

            AddFormFieldData("sUsdaGuaranteeClosingCostsDesc", dataLoan.sUsdaGuaranteeClosingCostsDesc);
            AddFormFieldData("sUsdaGuaranteeClosingCosts", dataLoan.sUsdaGuaranteeClosingCosts_rep);

            AddFormFieldData("sUsdaGuaranteeRepairFeeDesc", dataLoan.sUsdaGuaranteeRepairFeeDesc);
            AddFormFieldData("sUsdaGuaranteeRepairFee", dataLoan.sUsdaGuaranteeRepairFee_rep);

            AddFormFieldData("sUsdaGuaranteeGuaranteeFeeDesc", dataLoan.sUsdaGuaranteeGuaranteeFeeDesc);
            AddFormFieldData("sUsdaGuaranteeGuaranteeFee", dataLoan.sUsdaGuaranteeGuaranteeFee_rep);

            AddFormFieldData("sUsdaGuaranteeTotalRequestFee", dataLoan.sUsdaGuaranteeTotalRequestFee_rep);
            AddFormFieldData("sLTotAnnualI_rep", dataLoan.sLTotAnnualI_rep);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sProFirstMPmt", dataLoan.sProThisMPmt_rep);
        }

        public override string EditLink
        {
            get { return "/newlos/Forms/UsdaLoanGuarantee.aspx?pg=0"; }
        }
    }

    public class CUsdaLoanGuarantee_2PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "UsdaLoanGuarantee_2.pdf"; }
        }
        public override string Description
        {
            get { return "Page 2"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            IPreparerFields approvedLender = dataLoan.GetPreparerOfForm(E_PreparerFormT.UsdaGuaranteeApprovedLender, E_ReturnOptionIfNotExist.CreateNew);
            AddFormFieldData("UsdaGuaranteeApprovedLenderPreparerName", approvedLender.PreparerName);
            AddFormFieldData("UsdaGuaranteeApprovedLenderTitle", approvedLender.Title);

            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
        }
    }

    public class CUsdaLoanGuarantee_3PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "UsdaLoanGuarantee_3.pdf"; }
        }
        public override string Description
        {
            get { return "Page 3"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
        }
    }

    public class CUsdaLoanGuarantee_4PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "UsdaLoanGuarantee_4.pdf"; }
        }
        public override string Description
        {
            get { return "Page 4"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
        }
    }

    public class CUsdaLoanGuarantee_5PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "UsdaLoanGuarantee_5.pdf"; }
        }
        public override string Description
        {
            get { return "Page 5"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
        }
    }


    public class CUsdaLoanGuarantee_6PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "UsdaLoanGuarantee_6.pdf"; }
        }
        public override string Description
        {
            get { return "Page 6"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
        }
    }
    public class CUsdaLoanGuarantee_7PDF : AbstractLetterPDF
    {
        public override string PdfFile
        {
            get { return "UsdaLoanGuarantee_7.pdf"; }
        }
        public override string Description
        {
            get { return "Page 7"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
        }
    }

    public class CUsdaLoanGuaranteePDF : AbstractBatchPDF
    {
        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[] {
                                                 new CUsdaLoanGuarantee_1PDF()
                                                ,new CUsdaLoanGuarantee_2PDF()
                                                ,new CUsdaLoanGuarantee_3PDF()
                                                ,new CUsdaLoanGuarantee_4PDF()
                                                ,new CUsdaLoanGuarantee_5PDF()
                                                ,new CUsdaLoanGuarantee_6PDF()
                                                ,new CUsdaLoanGuarantee_7PDF()
                                             };
            }
        }

        public override string Description
        {
            get { return "USDA RD 3555-21: Request for Single Family Housing Loan Guarantee"; }
        }

        public override string EditLink
        {
            get { return "/newlos/Forms/UsdaLoanGuarantee.aspx"; }
        }
    }
}
