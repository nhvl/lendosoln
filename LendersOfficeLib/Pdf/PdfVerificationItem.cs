﻿namespace LendersOffice.Pdf
{
    using System;

    public class PdfVerificationItem
    {
        public Guid RecordId { get; set; }

        public string DisplayName { get; set; }
    }
}
