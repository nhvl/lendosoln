using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CFHAPotentialHomeEnergyBenefitsPDF : AbstractLetterPDF
	{
        public override string Description 
        {
            get { return "FHA Potential Home Energy Benefits"; }
        }
        public override string PdfFile 
        {
            get { return "FHAPotentialHomeEnergyBenefits.pdf";        	 }
        }
        

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("sLNm", dataLoan.sLNm);
        }
	}
}
