namespace LendersOffice.Pdf
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Reminders;
    using LendersOffice.Security;
    //using LendersOffice.Features;
    using LOPDFGenLib;

    /// <summary>
    /// Summary description for ConditionPDF.
    /// </summary>
    public class CConditionPDF : AbstractFreeLayoutPDFFile
	{
        private BrokerDB m_brokerDB = null;

        private BrokerDB BrokerDb
        {
            get
            {
                BrokerUserPrincipal brokerUser = System.Threading.Thread.CurrentPrincipal as BrokerUserPrincipal;

                // 12/2/2004 dd - This class can be call from NHC GetLoanForm webservice.
                // Therefore BrokerUserPrincipal can be null. As a result, just assume
                // this user doesn't have PriceMyLoan.
                if (null == brokerUser)
                {
                    return null;
                }

                // 2/8/2005 kb - PML is not the key.  We use lender default features now.

                if (m_brokerDB == null)
                {
                    try
                    {
                        m_brokerDB = brokerUser.BrokerDB;
                        return m_brokerDB;
                    }
                    catch (BrokerDBNotFoundException e)
                    {
                        Tools.LogError("Failed to load broker access info.", e);
                    }
                }
                return m_brokerDB;
            }
        }

        private Boolean UseUnderwritingConditions
        {
            get
            {
                if (BrokerDb == null)
                {
                    return false;
                }
                return BrokerDb.HasLenderDefaultFeatures;
            }
        }

        private Boolean IsUseNewTaskSystem
        {
            get
            {
                if (BrokerDb == null)
                {
                    return false;
                }
                return BrokerDb.IsUseNewTaskSystem;
            }
        }

        // 5/26/2004 dd - NHC Webservices GetLoanForm expected this value.

		public override string Name 
        {
            get { return "Conditions"; }
        }
        public override string EditLink 
        {
            get
			{
				// 10/7/2004 kb - We now use 2 condition pages, depending on
				// whether pml is enabled or not.  We may consolidate soon,
				// but don't bet on it.

                if (IsUseNewTaskSystem)
                {
                    return "/newlos/Underwriting/Conditions/TaskConditions.aspx";
                }
				else if ( UseUnderwritingConditions == true )
				{
					return "/newlos/Underwriting/TaskConditions.aspx";
				}
				else
				{
					return "/newlos/Status/Conditions.aspx";
				}
			}
        }
        public override string Description 
        {
            get { return "Loan Tracking Report - Full Conditions List"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
			// 9/30/2004 kb - We need to lookup the broker's options to see if
			// we're using the old conditions (embedded in the data loan) or a
			// new method using the associated tasks.  If the broker's options
			// are cached somewhere, then we should pull from there.
			//
			// 2/11/2005 kb - Expect the result of the list-disc-logs-by-Z
			// method to return a set ordered by an arbitrary rank number,
			// and then by creation date.  We need to clean up this call to
			// centralize the access to a loan's task-conditions.
			//
			// 2/24/2005 kb - This implementation is way out of hand.  It's
			// time to bifurcate within this handler.  If any more changes
			// come in, then we'll do this right(er).

            AddHeader( dataLoan.sLNm , dataApp.aBNm , dataApp.aCNm );

			AddConditionHeader();

            if (dataLoan.BrokerDB.IsUseNewTaskSystem)
            {
                List<Task> conditions = Task.GetActiveConditionsByLoanId(dataLoan.BrokerDB.BrokerID, dataLoan.sLId, false);
                foreach (Task condition in conditions)
                {
                    string done = condition.TaskStatus == E_TaskStatus.Closed ? "Yes" : "No";
                    string datedone = condition.TaskStatus == E_TaskStatus.Closed && condition.TaskClosedDate.HasValue ? condition.TaskClosedDate.Value.ToShortDateString() : "";
                    string required = "";
                    string priorToEventDesc = condition.CondCategoryId_rep;
                    string desc = condition.TaskSubject;
                    AddCondition(done, datedone, required, priorToEventDesc, desc);
                }
            }
            else
            {
                DataView dv = dataLoan.sCondDataSet.Value.Tables[0].DefaultView;

                if (UseUnderwritingConditions == true)
                {
                    // 9/30/2004 kb - Fetch the data set that contains all the
                    // current loan's conditions as tasks.  We use a simple list
                    // sproc that returns the task items in the correct format.

                    // 11/6/2006 mf. Modified to support new condition system.				
                    DataTable dT = BuildUnderwritingConditionList(dataLoan.sLId);

                    dv = dT.DefaultView;
                }

                dv.RowFilter = "IsRequired='True'";

                foreach (DataRow row in dv.Table.Rows)
                {
                    if (UseUnderwritingConditions == false || row["PriorToEventDesc"].ToString().ToLower().TrimWhitespaceAndBOM() != "notes")
                    {
                        AddCondition
                            (row["IsDone"].ToString() == "True" ? "Yes" : ""
                            , row["DoneDate"] is DateTime ? ((DateTime)row["DoneDate"]).ToShortDateString() : row["DoneDate"].ToString()
                            , ""
                            , row["PriorToEventDesc"].ToString()
                            , row["CondDesc"].ToString()
                            );
                    }
                    else
                    {
                        AddCondition
                            (""
                            , ""
                            , ""
                            , row["PriorToEventDesc"].ToString()
                            , row["CondDesc"].ToString()
                            );
                    }
                }
            }
        }

		private DataTable BuildUnderwritingConditionList( Guid loanId )
		{
			// We create a datatable that we can bind to the list
			// of conditions to be compatible with both condition modes.
			DataTable conditionTable = new DataTable();

            CConditionSetObsolete conditions = new CConditionSetObsolete(loanId);
			conditionTable.Columns.Add("IsDone");
			conditionTable.Columns.Add("IsRequired");
			conditionTable.Columns.Add("DoneDate", typeof(DateTime));
			conditionTable.Columns.Add("PriorToEventDesc");
			conditionTable.Columns.Add("CondDesc");

			foreach( CConditionObsolete condition in conditions )
			{
				DataRow row = conditionTable.NewRow();
				row["IsRequired"] = "True";
				if ( condition.IsDone )
				{
					row["IsDone"] = "True";
					row["DoneDate"] = condition.DateDone;
				}
				else
				{
					row["IsDone"] = "False";
				}
				row["PriorToEventDesc"] = condition.CondCategoryDesc;
				row["CondDesc"] = condition.CondDesc;

				conditionTable.Rows.Add( row );
			}

			return conditionTable;
		}

		#region "Helper functions to layout "

        protected override void AddTitle() 
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 12;

            RowItem row = new RowItem(0, 40);
            row.Add(new TextItem("Tracking - Full Conditions List", 240, 0, font, fontSize));
            AddItem(row);

        }
        private void AddHeader(string sLNm, string aBNm, string aCNm) 
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Loan Number: ", 40, 0, font, fontSize));
            row.Add(new TextItem(sLNm, 100, 0, font, fontSize));
            row.Add(new TextItem("Date/Time: ", 450, 0, font, fontSize));
            row.Add(new TextItem(DateTime.Now.ToString("MM/dd/yyyy hh:mm tt"), 500, 0, font, fontSize));

            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Borrower: " , 40, 0, font, fontSize));

            string name = aBNm;
            if (aCNm != "")
                name += " / " + aCNm;
            row.Add(new TextItem(name, 80, 0, font, fontSize));

            AddItem(row);
        }

        private void AddConditionHeader() 
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Done?", 40, 0, font, fontSize));

			if( UseUnderwritingConditions == true )
			{
				row.Add(new TextItem("Date Done", 80, 0, font, fontSize));
                row.Add(new TextItem("Category", 140, 0, 100, font, fontSize));
				row.Add(new TextItem("Condition", 240, 0, 330, font, fontSize));
			}
			else
			{
				row.Add(new TextItem("Date Done", 80, 0, font, fontSize));
				row.Add(new TextItem("Prior To", 140, 0, 100, font, fontSize));
				row.Add(new TextItem("Description", 240, 0, 330, font, fontSize));
			}

            AddItem(row);
            AddItem(new HorizontalLineItem());
        }

        private void AddCondition(string done, string dateDone, string required, string priorTo, string description) 
        {
            FontName font = FontName.Helvetica;
            short fontSize = 10;

            RowItem row = new RowItem();
            row.Add(new TextItem(done, 40, 0, font, fontSize));

			if( UseUnderwritingConditions == true )
			{
                string dateDoneFormatted;
                DateTime dt;

                if (DateTime.TryParse(dateDone, out dt))
                {
                    dateDoneFormatted = dt.ToShortDateString();
                }
                else
                {
                    dateDoneFormatted = dateDone;
                }

				row.Add(new TextItem(dateDoneFormatted, 80, 0, font, fontSize));
                row.Add(new TextItem(priorTo, 140, 0, 100, font, fontSize));
				row.Add(new TextItem(description, 240, 0, 330, font, fontSize));
			}
			else
			{
				row.Add(new TextItem(dateDone, 80, 0, font, fontSize));
				row.Add(new TextItem(priorTo, 140, 0, 100, font, fontSize));
				row.Add(new TextItem(description, 240, 0, 330, font, fontSize));
			}

            AddItem(row);
        }

		#endregion 

	}

}
