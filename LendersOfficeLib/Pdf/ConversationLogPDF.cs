﻿namespace LendersOffice.Pdf
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using DataAccess;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.ConversationLog;
    using Security;
    using Constants;
    using System;
    using PdfGenerator;
    using ExpertPdf.HtmlToPdf;
    using PdfRasterizerLib;
    using ObjLib.Views;
    using Drivers.ConversationLog;
    using ObjLib.Security.Authorization.ConversationLog;

    /// <summary>
    /// Generates a pdf that shows the conversations as the user would see them in the UI, except for links.
    /// </summary>
    public class CConversationLogPDF : AbstractXsltPdf
    {
        public override PDFPageSize PageSize
        {
            get
            {
                return PDFPageSize.Letter;
            }
        }

        /// <summary>
        /// Gets the filename for this pdf.
        /// </summary>
        /// <value>The filename for this pdf.</value>
        public override string PdfName
        {
            get
            {
                return "Conversation Log";
            }

            set
            {
            }
        }

        /// <summary>
        /// Gets the url for the edit link in the print list.
        /// </summary>
        /// <value>The url for the edit link in the print list.</value>
        public override string EditUrl
        {
            get
            {
                return Tools.VRoot + "/newlos/ConversationLog";
            }
        }

        /// <summary>
        /// Gets the pdf description.
        /// </summary>
        /// <value>The pdf description.</value>
        public override string Description
        {
            get
            {
                return "Conversation Log";
            }
        }

        /// <summary>
        /// Gets the xslt location for this pdf.
        /// </summary>
        /// <value>The xslt location for this pdf.</value>
        protected override string XsltEmbeddedResourceLocation
        {
            get
            {
                return "LendersOffice.Pdf.ConversationLog.xslt";
            }
        }

        private Dictionary<string, Category> CategoryByName;
        private Dictionary<long, ConversationLogPermissionLevel> PermissionLevelByName;

        /// <summary>
        /// Generates the data in xml form to be combined with the xslt to produce html.
        /// </summary>
        /// <param name="writer">The writer to use.</param>
        /// <param name="dataLoan">The data loan.</param>
        /// <param name="dataApp">The app for this pdf.</param>
        protected override void GenerateXmlData(XmlWriter writer, CPageData dataLoan, CAppData dataApp)
        {
            var principal = (AbstractUserPrincipal)PrincipalFactory.CurrentPrincipal;
            var featureIsEnabledForUser = principal.HasConversationLogFeatureEnabled;
            if (!featureIsEnabledForUser)
            {
                writer.WriteStartElement("BrokerLacksFeature");
                writer.WriteEndElement();
                return;
            }

            var securityToken = SecurityService.CreateToken();
            var identifier = ResourceIdentifier.CreateForLoanFile(securityToken.BrokerId, dataLoan.sLRefNm);
            if (identifier == null)
            {
                throw new LqbGrammar.Exceptions.ServerException(ErrorMessage.SystemError);
            }

            CategoryByName = ConversationLogHelper.GetAllCategories(securityToken, ConstStage.ConversationLogIP, ConstStage.ConversationLogPort).ToDictionary(c => c.Identity.Name.ToString());
            PermissionLevelByName = ConversationLogPermissionLevel.GetOrderedPermissionLevelsForBroker(securityToken.BrokerId).ToDictionary(l => l.Id.Value);

            var resourceId = new ResourceId()
            {
                Type = ResourceType.LoanFile,
                Identifier = identifier.Value
            };

            var conversations = ConversationLogHelper.GetAllConversations(securityToken, resourceId, ConstStage.ConversationLogIP, ConstStage.ConversationLogPort);

            ////! note these are going to have to be:
            // sorted: by conv creation date depending on user's saved creation date favorite.
            // filtered by:
            //  - user's saved filter, 
            //  - what's hidden to the user (if anything.)
            writer.WriteStartElement("ConversationLog");
            writer.WriteAttributeString("EnableConversationLogPermissions", ConstStage.EnableConversationLogPermissions.ToString());


            foreach (var conversation in conversations)
            {
                this.WriteConversation(writer, conversation);
            }

            writer.WriteEndElement(); // end ConversationLog;
        }

        protected override void UpdatePdfConversionOptions(CPageData dataLoan, CAppData dataApp, PDFConversionOptions options)
        {
            options.ShowHeader = true;
            options.DrawHeaderLine = false;
            options.HeaderText = this.GenerateHeaderText(dataLoan);
            options.HeaderTextFontSize = 11;
            options.HeaderTextAlign = (int)HorizontalTextAlign.Left;
            options.HeaderHeight = 62;

            options.ShowHeaderOnFirstPage = true;
            options.ShowHeaderOnEvenPages = false;
            options.ShowHeaderOnOddPages = false;

            options.BottomMargin = 0;
            options.ShowFooter = true;
            options.FooterText = this.GenerateFooterText(dataLoan);
            options.FooterTextFontSize = 11;
            options.DrawFooterLine = false;
        }

        private string GenerateHeaderText(CPageData dataLoan)
        {
            var rolesOfInterest = new E_AgentRoleT[] { E_AgentRoleT.LoanOfficer, E_AgentRoleT.Processor, E_AgentRoleT.Manager };
            var agentsForRoles = rolesOfInterest.Select(r => dataLoan.GetAgentOfRole(r, E_ReturnOptionIfNotExist.ReturnEmptyObject));
            var agentsOfInterest = agentsForRoles.Where(a => a.IsValid);
            var agentDescriptionsOfInterest = agentsOfInterest.Select(a => a.AgentRoleDescription + " - " + a.AgentName);
            return string.Join(Environment.NewLine, new string[] { GetLoanAndPrimaryBorrowerText(dataLoan) }.Union(agentDescriptionsOfInterest));
        }

        private string GetLoanAndPrimaryBorrowerText(CPageData dataLoan)
        {
            var primaryApp = dataLoan.GetAppData(0);
            var primaryBorrowerName = primaryApp.aBLastNm + ", " + primaryApp.aBFirstNm;

            return dataLoan.sLNm + " - " + primaryBorrowerName;
        }

        private string GenerateFooterText(CPageData dataLoan)
        {
            return GetLoanAndPrimaryBorrowerText(dataLoan);
        }

        /// <summary>
        /// Writes the xml for the conversation to the xml writer.
        /// </summary>
        /// <param name="writer">The stream where the xml is going.</param>
        /// <param name="conversation">The conversation being serialized.</param>
        private void WriteConversation(XmlWriter writer, Conversation conversation)
        {
            writer.WriteStartElement("Conversation");
            writer.WriteAttributeString("CategoryName", CategoryByName[conversation.Category.Name.ToString()].DisplayName.ToString());

            foreach (var comment in conversation.Comments)
            {
                this.WriteComment(writer, comment, conversation.PermissionLevelId, conversation.Category.Id);
            }

            writer.WriteEndElement(); // end Conversation
        }

        /// <summary>
        /// Writes the xml for the comment to the xml writer. <para/>
        /// NOTE: If the comment depth exceeds the max comment depth, the xml structure no longer represents the comment structure.
        /// </summary>
        /// <param name="writer">The stream where the xml is going.</param>
        /// <param name="comment">The comment being serialized.</param>
        /// <param name="permissionLevelId">The id of the permission level for the comment, inherited from the conversation.</param>
        /// <param name="categoryId">The id of the category from the conversation.</param>
        private void WriteComment(XmlWriter writer, Comment comment, long? permissionLevelId, long categoryId)
        {
            var viewOfComment = new ViewOfCommentForLoUser(comment, Tools.PacificStandardVariantOffset, permissionLevelId, categoryId);
            var commentDepth = comment.Depth.Value;
            var writeRepliesBesidesComment = commentDepth > ConstStage.ConversationLogMaxVisibleReplyDepth;

            writer.WriteStartElement("Comment");
            writer.WriteAttributeString("Name", viewOfComment.CommenterName);
            writer.WriteAttributeString("CreatedDate", viewOfComment.CreatedDateStringForDisplay);
            writer.WriteAttributeString("PermissionLevelName", 
                permissionLevelId.HasValue ? PermissionLevelByName[permissionLevelId.Value].Name : ConversationLogPermissionLevel.DefaultLevelName);

            writer.WriteAttributeString("IsHidden", viewOfComment.IsHidden.ToString());
            if(viewOfComment.IsHidden)
            {
                writer.WriteAttributeString("HiderName", viewOfComment.HiderFullName);
            }

            var visualDepth = Math.Min(commentDepth, ConstStage.ConversationLogMaxVisibleReplyDepth + 1);
            writer.WriteAttributeString("Width", (693 - visualDepth * 11 * 2).ToString() +"px");
            writer.WriteAttributeString("LeftMargin", (visualDepth * 2 * 11).ToString() + "px");
            
            {
                var lines = comment.Value.ToString()
                        .Split(new string[] { Environment.NewLine }, StringSplitOptions.None)
                        .SelectMany(l => l.Split(new char[] { '\r' }, StringSplitOptions.None))
                        .SelectMany(l => l.Split(new char[] { '\n' }, StringSplitOptions.None)).ToList();

                foreach (var line in lines)
                {
                    writer.WriteStartElement("CommentLine");
                    writer.WriteStartElement("Text");
                    writer.WriteCData(line); // CData needed so PDF can be generated.
                    writer.WriteEndElement(); // end Text;
                    writer.WriteEndElement(); // end CommentLine;
                }
            }

            writer.WriteEndElement();
        }
    }
}