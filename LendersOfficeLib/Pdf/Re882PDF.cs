using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CRe882_1ObsoletePDF : AbstractMldsPDF 
    {
        public override string PdfFile 
        {
            get { return "RE_882_1.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 1"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/CAMLDS.aspx"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            ApplyData(dataLoan, dataApp, true);
        }
    }
    public class CRe882_2ObsoletePDF : AbstractMldsPDF 
    {
        public override string PdfFile 
        {
            get { return "RE_882_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/CAMLDS.aspx"; }
        }
    }
	public class CRe882ObsoletePDF : AbstractBatchPDF
	{
        public override string Description 
        {
            get { return "CA Mortgage Loan Disclosure Statement (RE 882) <b>Pre 2010</b>"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/CAMLDS.aspx"; }
        }
 
        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            {
                return new AbstractPDFFile[] {
                                                 new CRe882_1ObsoletePDF(),
                                                 new CRe882_2ObsoletePDF()
                                             };
            }
        }
	}
}
