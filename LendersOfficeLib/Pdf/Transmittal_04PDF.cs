namespace LendersOffice.Pdf
{
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using DataAccess;
    using LendersOffice.Common;

    public class CTransmittal_04PDF : AbstractLegalPDF
    {

        public override string PdfFile 
        {
            get { return "1008_04.pdf"; }
        }
    
        public override string Description 
        {
            get { return "(1008) Uniform Underwriting and Transmittal Summary <b>for 2004</b>"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Forms/Transmittal_04.aspx"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            
            string address = Tools.FormatSingleLineAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip);
            AddFormFieldData("PropertyAddress", address);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aBSsn", dataApp.aBSsn);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("aCSsn", dataApp.aCSsn);
            AddFormFieldData("aOccT", dataApp.aOccT);
            AddFormFieldData("sTerm", dataLoan.sTerm_rep + "/" + dataLoan.sDue_rep);
            AddFormFieldData("sUnitsNum", dataLoan.sUnitsNum_rep);
            AddFormFieldData("sApprVal", dataLoan.sApprVal_rep);
            AddFormFieldData("sEstateHeldT", dataLoan.sEstateHeldT);
            AddFormFieldData("sLT", dataLoan.sLT);
            AddFormFieldData("sProjNm", dataLoan.sProjNm);
            AddFormFieldData("sPurchPrice", dataLoan.sPurchPrice_rep);
            AddFormFieldData("sBuydown", dataLoan.sBuydown);
            AddFormFieldData("s1stMOwnerT", dataLoan.s1stMOwnerT);
            AddFormFieldData("s1stMtgOrigLAmt", dataLoan.s1stMtgOrigLAmt_rep);
            
            AddFormFieldData("sConcurSubFin", dataLoan.sConcurSubFin_rep);
            if (dataLoan.sIsOFinCreditLineInDrawPeriod)
            {
                AddFormFieldData("sSubFin", string.IsNullOrEmpty(dataLoan.sSubFin_rep) ? "" : "$" + dataLoan.sSubFin_rep);
            }

            AddFormFieldData("sSpProjectClassFannieT", dataLoan.sSpProjectClassFannieT);
            
            // 12/14/2004 dd - Just add new option for other amortization type.
            // If sFinMethodPrintAsOther is checked then don't check other box.
            if (dataLoan.sFinMethodPrintAsOther) 
            {
                AddFormFieldData("sFinMethT", "2");
                AddFormFieldData("sFinMethDesc", dataLoan.sFinMethPrintAsOtherDesc);
            } 
            else 
            {

                if (dataLoan.sFinMethT == E_sFinMethT.Fixed) 
                {
                    if (dataLoan.sBiweeklyPmt)
                        AddFormFieldData("sFinMethT", "10"); // Biweekly Payments
                    else
                        AddFormFieldData("sFinMethT", dataLoan.sFinMethT);

                }
                else if (dataLoan.sFinMethT == E_sFinMethT.ARM) 
                {
                    AddFormFieldData("sFinMethT", dataLoan.sFinMethT);

                    AddFormFieldData("sArmFinMethDesc", dataLoan.sFinMethDesc);
                } 
                else 
                {
                    AddFormFieldData("sFinMethT", dataLoan.sFinMethT);

                    AddFormFieldData("sFinMethDesc", dataLoan.sFinMethDesc);
                }
            }

            AddFormFieldData("sBalloonPmt", dataLoan.sBalloonPmt);

            AddFormFieldData("sUwerNm", dataLoan.GetAgentOfRole( E_AgentRoleT.Underwriter, E_ReturnOptionIfNotExist.ReturnEmptyObject ).AgentName );
            CAgentFields appraiser = dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser,E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("sApprer", string.Format("{0} / {1}", appraiser.AgentName, appraiser.LicenseNumOfAgent));
            AddFormFieldData("sApprComNm", appraiser.CompanyName);

            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sLenAddr", dataLoan.sLenAddr);
            AddFormFieldData("sLenAddr2",  Tools.CombineCityStateZip(dataLoan.sLenCity, dataLoan.sLenState, dataLoan.sLenZip));
            AddFormFieldData("sLenContactNm", dataLoan.sLenContactNm);
            AddFormFieldData("sLenContactPhone", dataLoan.sLenContactPhone);
            AddFormFieldData("sLenContactTitle", dataLoan.sLenContactTitle);
            AddFormFieldData("sLenLNum", dataLoan.sLenLNum);
            AddFormFieldData("sLenNm", dataLoan.sLenNm);
            AddFormFieldData("sLenNum", dataLoan.sLenNum);
            AddFormFieldData("sCommitNum", dataLoan.sCommitNum);
            AddFormFieldData("sLenContractD", dataLoan.sLenContractD_rep);
            AddFormFieldData("sContractNum", dataLoan.sContractNum);
            AddFormFieldData("sInvestLNum", dataLoan.sInvestLNum);               
            AddFormFieldData("sProThisMPmt", dataLoan.sProThisMPmt_rep);

            AddFormFieldData("sPrimAppTotBaseI", dataApp.aBBaseI_rep);
            AddFormFieldData("sNprimAppsTotBaseI", dataApp.aCBaseI_rep);
            AddFormFieldData("sLTotBaseI", dataApp.aTotBaseI_rep);

            AddFormFieldData("sPrimAppTotNonbaseI", dataApp.aBNonbaseI_rep);
            AddFormFieldData("sNprimAppsTotNonbaseI", dataApp.aCNonbaseI_rep);
            AddFormFieldData("sLTotNonbaseI", dataApp.aTotNonbaseI_rep);

            AddFormFieldData("sPrimAppOIFrom1008", dataApp.aBOIFrom1008_rep);
            AddFormFieldData("sNprimAppsTotOIFrom1008", dataApp.aCOIFrom1008_rep);
            AddFormFieldData("sLTotOIFrom1008", dataApp.aTotOIFrom1008_rep);
            AddFormFieldData("sOIFrom1008Desc", dataApp.aOIFrom1008Desc);

            AddFormFieldData("sPrimAppTotSpPosCf", dataApp.aBSpPosCf_rep);
            AddFormFieldData("sNprimAppsTotSpPosCf", dataApp.aCSpPosCf_rep);
            AddFormFieldData("sLTotSpPosCf", dataApp.aTotSpPosCf_rep);

            AddFormFieldData("sPrimAppTotI", dataApp.aTransmBTotI_rep);
            AddFormFieldData("sNprimAppsTotI", dataApp.aTransmCTotI_rep);
            AddFormFieldData("sLTotI", dataApp.aTransmTotI_rep);

            AddFormFieldData("sLienPosT", dataLoan.sLienPosT);

            AddFormFieldData("sQualTopR", dataApp.aQualTopR_rep);
            AddFormFieldData("sQualBottomR", dataApp.aQualBottomR_rep);

            AddFormFieldData("sCltvR", dataLoan.sCltvR_rep);
            AddFormFieldData("sLtvR", dataLoan.sLtvR_rep);

            AddFormFieldData("sTransmUwerComments", dataLoan.sTransmUwerComments);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            
            AddFormFieldData("sPresLTotPersistentHExp", dataApp.aPresTotHExp_rep);
            AddFormFieldData("sTransmPro1stMPmt", dataApp.aTransmPro1stMPmt_rep);
            AddFormFieldData("sTransmPro2ndMPmt", dataApp.aTransmPro2ndMPmt_rep);
            AddFormFieldData("sTransmProHazIns", dataApp.aTransmProHazIns_rep);
            AddFormFieldData("sTransmProRealETx", dataApp.aTransmProRealETx_rep);
            AddFormFieldData("sTransmProMIns", dataApp.aTransmProMIns_rep);
            AddFormFieldData("sTransmOtherProHExp", dataApp.aTransmOtherProHExp_rep);

            AddFormFieldData("sTransmProHoAssocDues", dataApp.aTransmProHoAssocDues_rep);
            AddFormFieldData("sTransmProTotHExp", dataApp.aTransmProTotHExp_rep);
            AddFormFieldData("sSpNegCf", dataApp.aSpNegCf_rep);
            AddFormFieldData("sTransmOMonPmt", dataApp.aTransmOMonPmt_rep);


            if (dataApp.aOpNegCf_rep != "")
                AddFormFieldData("OtherPropertyLabel", "Other Properties");
            AddFormFieldData("aOpNegCf", dataApp.aOpNegCf_rep);
            AddFormFieldData("sTransmTotMonPmt", dataApp.aTransmTotMonPmt_rep);

            AddFormFieldData("sApprDriveBy", dataLoan.sApprDriveBy);
            AddFormFieldData("sApprFull", dataLoan.sApprFull);
            AddFormFieldData("sMOrigT", dataLoan.sMOrigT);

            AddFormFieldData("sSpProjClassT", dataLoan.sSpProjClassT);

            E_sQualIRDeriveT sQualIRDeriveT = dataLoan.sQualIRDeriveT;
            decimal noteRate = dataLoan.sNoteIR;
            decimal qualifyRate = dataLoan.sQualIR;

            
            if (sQualIRDeriveT == E_sQualIRDeriveT.NoteRateDependent) 
            {
                // 2/18/2005 dd - OPM #1158 - If qualify rate is 0 then assume qualify rate = note rate.
                if (qualifyRate == 0 || noteRate == qualifyRate) 
                {
                    AddFormFieldData("sQualIRDeriveT", "0");
                    AddFormFieldData("sNoteRate0", dataLoan.sNoteIR_rep);
                } 
                else if (noteRate < qualifyRate) 
                {
                    AddFormFieldData("sQualIRDeriveT", "3");
                    AddFormFieldData("sNoteRate3", dataLoan.sQualIR_rep);
                    AddFormFieldData("sNoteRateDifferent3", dataLoan.m_convertLos.ToRateString(qualifyRate - noteRate));
                } 
                else 
                {
                    AddFormFieldData("sQualIRDeriveT", "4");
                    AddFormFieldData("sNoteRate4", dataLoan.sQualIR_rep);
                    AddFormFieldData("sNoteRateDifferent4", dataLoan.m_convertLos.ToRateString(noteRate - qualifyRate));

                }
            } 
            else if (sQualIRDeriveT == E_sQualIRDeriveT.BoughtDownRate) 
            {
                AddFormFieldData("sQualIRDeriveT", "1");
                AddFormFieldData("sNoteRate1", dataLoan.m_convertLos.ToRateString(qualifyRate));
            } 
            else 
            {
                AddFormFieldData("sQualIRDeriveT", "2");
                AddFormFieldData("sNoteRate2", dataLoan.sQualIR_rep);

            }

            // 4/1/2004 dd - Map sGseSpT to Property Type on printout.
            if (dataLoan.sUnitsNum > 1 ) 
            {
                AddFormFieldData("sGseSpT", "1"); // 2-4 units
            } 
            else 
            {
                E_sGseSpT sGseSpT = dataLoan.sGseSpT;
                if (sGseSpT == E_sGseSpT.Attached || sGseSpT == E_sGseSpT.Detached) 
                {
                    AddFormFieldData("sGseSpT", "0"); // 1 unit. SFR
                } 
                else if (sGseSpT == E_sGseSpT.Condominium || sGseSpT == E_sGseSpT.DetachedCondominium || sGseSpT == E_sGseSpT.HighRiseCondominium
                    || sGseSpT == E_sGseSpT.ManufacturedHomeCondominium) 
                {
                    AddFormFieldData("sGseSpT", "2"); // Condominium.
                } 
                else if (sGseSpT == E_sGseSpT.PUD) 
                {
                    AddFormFieldData("sGseSpT", "3");
                } 
                else if (sGseSpT == E_sGseSpT.Cooperative) 
                {
                    AddFormFieldData("sGseSpT", "4");
                } 
                else if (sGseSpT == E_sGseSpT.ManufacturedHomeCondominium || sGseSpT == E_sGseSpT.ManufacturedHomeMultiwide ||
                    sGseSpT == E_sGseSpT.ManufacturedHousing || sGseSpT == E_sGseSpT.ManufacturedHousingSingleWide) 
                {
                    AddFormFieldData("sGseSpT_Manufactured", true);
                    if (sGseSpT == E_sGseSpT.ManufacturedHousingSingleWide)
                        AddFormFieldData("sGseSpT", "5");
                    else if (sGseSpT == E_sGseSpT.ManufacturedHomeMultiwide)
                        AddFormFieldData("sGseSpT", "6");
                }

            }

            // 4/1/2004 dd - Map sLPurposeT to Printout
            E_sLPurposeT sLPurposeT = dataLoan.sLPurposeT;

            if (sLPurposeT == E_sLPurposeT.Purchase || sLPurposeT == E_sLPurposeT.ConstructPerm)
                AddFormFieldData("sLPurposeT", sLPurposeT);
            else if (sLPurposeT == E_sLPurposeT.Refin || sLPurposeT == E_sLPurposeT.RefinCashout || sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || sLPurposeT == E_sLPurposeT.VaIrrrl) 
            {
                E_sGseRefPurposeT sGseRefPurposeT = dataLoan.sGseRefPurposeT;
                switch (sGseRefPurposeT) 
                {
                    case E_sGseRefPurposeT.NoCashOutFHAStreamlinedRefinance:
                    case E_sGseRefPurposeT.NoCashOutFREOwnedRefinance:
                    case E_sGseRefPurposeT.NoCashOutOther:
                    case E_sGseRefPurposeT.NoCashOutStreamlinedRefinance:
                        AddFormFieldData("sLPurposeT", "12"); // No Cash-Out Refinance (Freddie)
                        break;
                    case E_sGseRefPurposeT.CashOutLimited:
                        AddFormFieldData("sLPurposeT", "11"); // Limited Cash-Out Refinance (Fannie)
                        break;
                    case E_sGseRefPurposeT.CashOutHomeImprovement:
                        AddFormFieldData("sLPurposeT", "13"); // Home Improvment.
                        break;
                    default:
                        if (sLPurposeT == E_sLPurposeT.Refin || sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance)
                            AddFormFieldData("sLPurposeT", "12"); // No Cash-Out Refin.
                        else
                            AddFormFieldData("sLPurposeT", "10"); // Cashout Refinance
                        break;
                }
            }
            else if (sLPurposeT == E_sLPurposeT.HomeEquity)
            {
                AddFormFieldData("sLPurposeT", "10");
            }

            AddFormFieldData("sHcltvR", dataLoan.sHcltvR_rep);
            AddFormFieldData("sDebtToHousingGapR", dataLoan.sDebtToHousingGapR_rep);
            AddFormFieldData("sIsSpReviewNoAppr", dataLoan.sIsSpReviewNoAppr);
            AddFormFieldData("sSpReviewFormNum", dataLoan.sSpReviewFormNum);
            AddFormFieldData("sTransmFntc", dataLoan.sTransmFntc_rep);
            AddFormFieldData("sVerifAssetAmt", dataLoan.sVerifAssetAmt_rep);
            AddFormFieldData("sFntcSrc", dataLoan.sFntcSrc);
            AddFormFieldData("sRsrvMonNumDesc", dataLoan.sRsrvMonNumDesc);
            AddFormFieldData("sInterestedPartyContribR", dataLoan.sInterestedPartyContribR_rep);
            AddFormFieldData("sIsManualUw", dataLoan.sIsManualUw);
            if (dataLoan.sIsDuUw || dataLoan.sIsLpUw || dataLoan.sIsOtherUw)
                AddFormFieldData("sIsAUS", true);
            AddFormFieldData("sIsDuUw", dataLoan.sIsDuUw);
            AddFormFieldData("sIsLpUw", dataLoan.sIsLpUw);
            AddFormFieldData("sIsOtherUw", dataLoan.sIsOtherUw);
            AddFormFieldData("sOtherUwDesc", dataLoan.sOtherUwDesc);
            AddFormFieldData("sAusRecommendation", dataLoan.sAusRecommendation);

            if (dataLoan.sLpAusKey != "" || dataLoan.sDuCaseId != "")
                AddFormFieldData("sDuCaseId_sLpAusKey", dataLoan.sDuCaseId + " / " + dataLoan.sLpAusKey);

            AddFormFieldData("sLpDocClass", dataLoan.sLpDocClass);
            AddFormFieldData("sRepCrScore", dataLoan.sRepCrScore);
            AddFormFieldData("sIsCommLen", dataLoan.sIsCommLen);
            AddFormFieldData("sIsHOwnershipEdCertInFile", dataLoan.sIsHOwnershipEdCertInFile);
            AddFormFieldData("sIsMOrigBroker", dataLoan.sIsMOrigBroker);
            AddFormFieldData("sIsMOrigCorrespondent", dataLoan.sIsMOrigCorrespondent);
            AddFormFieldData("sWillEscrowBeWaived", dataLoan.sWillEscrowBeWaived);
            AddFormFieldData("sTransmBuydwnTermDesc", dataLoan.sTransmBuydwnTermDesc);
            AddFormFieldData("TransmMOriginatorCompanyName", dataLoan.GetPreparerOfForm(E_PreparerFormT.TransmMOriginator, E_ReturnOptionIfNotExist.ReturnEmptyObject).CompanyName);
            AddFormFieldData("sSpProjectClassFreddieT", dataLoan.sSpProjectClassFreddieT); // 4/9/2009 dd - OPM 27432
            AddFormFieldData("sCpmProjectId", dataLoan.sCpmProjectId); // 4/9/2009 dd - OPM 27432

        }
    }

    public class CTransmittal_05PDF : CTransmittal_04PDF
    {
        public override string PdfFile 
        {
            get { return "1008_05.pdf"; }
        }
    
        public override string Description 
        {
            get { return "(1008) Uniform Underwriting and Transmittal Summary <b>for 2005</b>"; }
        }
    }
    public class CTransmittal_08PDF : CTransmittal_04PDF
    {
        public override LendersOffice.PdfGenerator.PDFPageSize PageSize
        {
            get { return LendersOffice.PdfGenerator.PDFPageSize.Letter; }
        }
        public override string PdfFile
        {
            get { return "1008_08.pdf"; }
        }

        public override string Description
        {
            get { return "(1008) Uniform Underwriting and Transmittal Summary <b>for 2008</b>"; }
        }
    }
    public class CTransmittal_09PDF : CTransmittal_04PDF
    {
        public override LendersOffice.PdfGenerator.PDFPageSize PageSize
        {
            get { return LendersOffice.PdfGenerator.PDFPageSize.Letter; }
        }
        public override string PdfFile
        {
            get { return "1008_09.pdf"; }
        }

        public override string Description
        {
            get { return "(1008) Uniform Underwriting and Transmittal Summary <b>for 2009</b>"; }
        }
    }
}
