﻿namespace LendersOffice.Pdf
{
    using LendersOffice.Common;

    /// <summary>
    /// Page 3 of the 26-1805/26-1843 combined form.
    /// </summary>
    public class CVA_26_1843_1PDF : AbstractVA_26_1805PDF
    {
        /// <summary>
        /// PDF file name.
        /// </summary>
        /// <value>Name of the pdf file for this page.</value>
        public override string PdfFile
        {
            get { return "VA-26-1843_1.pdf"; }
        }

        /// <summary>
        /// Form page description.
        /// </summary>
        /// <value>The description for showing in the print list.</value>
        public override string Description
        {
            get { return "Certificate of Value - Requestor's Copy 3 (26-1843) (Jun 2001)"; }
        }
    }

    /// <summary>
    /// Page 4 of the 26-1805/26-1843 combined form.
    /// </summary>
    public class CVA_26_1843_2PDF : AbstractVA_26_1805PDF
    {
        /// <summary>
        /// PDF file name.
        /// </summary>
        /// <value>Name of the pdf file for this page.</value>
        public override string PdfFile
        {
            get { return "VA-26-1843_2.pdf"; }
        }

        /// <summary>
        /// Form page description.
        /// </summary>
        /// <value>The description for showing in the print list.</value>
        public override string Description
        {
            get { return "Certificate of Value - VA File Copy 4 (26-1843) (Jun 2001)"; }
        }
    }

    /// <summary>
    /// Page 5 of the 26-1805/26-1843 combined form.
    /// </summary>
    public class CVA_26_1843_3PDF : AbstractVA_26_1805PDF
    {
        /// <summary>
        /// PDF file name.
        /// </summary>
        /// <value>Name of the pdf file for this page.</value>
        public override string PdfFile
        {
            get { return "VA-26-1843_3.pdf"; }
        }

        /// <summary>
        /// Form page description.
        /// </summary>
        /// <value>The description for showing in the print list.</value>
        public override string Description
        {
            get { return "Certificate of Value - Purchaser Copy 5 (26-1843) (Jun 2001)"; }
        }
    }

    /// <summary>
    /// The multi-page form containing instances of VA form 26-1843 from the VA 26-1805/26-1843 combined form.
    /// </summary>
    public class CVA_26_1843PDF : AbstractBatchPDF
    {
        /// <summary>
        /// Form description.
        /// </summary>
        /// <value>The description for showing in the print list.</value>
        public override string Description
        {
            get { return "VA Certificate of Reasonable Value (VA 26-1843)"; }
        }

        /// <summary>
        /// Page to edit the form details.
        /// </summary>
        /// <value>The page to show when clicking "edit" in the print list.</value>
        public override string EditLink
        {
            get { return "/newlos/VA/VAReasonableValue.aspx"; }
        }

        /// <summary>
        /// Child pages of the form.
        /// </summary>
        /// <value>A list of child pages.</value>
        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[]
                {
                    new CVA_26_1843_1PDF(),
                    new CVA_26_1843_2PDF(),
                    new CVA_26_1843_3PDF()
                };
            }
        }
    }
}
