using System;
using System.Collections.Specialized;
using System.Data;

using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CFHAAmendatoryClausePDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "FHAAmendatoryClause.pdf"; }
        }
        public override string Description 
        {
            get { return "FHA Amendatory Clause / Real Estate Certification"; }
        }


        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {

            string name = dataApp.aBNm;
            if (dataApp.aCNm != "") 
            {
                name += " & " + dataApp.aCNm;
            }
            AddFormFieldData("Borrowers", name);
            AddFormFieldData("sPurchPrice", dataLoan.sPurchPrice_rep);
			IPreparerFields gfeTil = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
			AddFormFieldData("GfeTilPrepareDate", gfeTil.PrepareDate_rep); // OPM 23041

            string propertyAddress = string.Format("{0}, {1}", dataLoan.sSpAddr, Tools.CombineCityStateZip(dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            AddFormFieldData("PropertyAddress", propertyAddress);

            AddFormFieldData("sLNm", dataLoan.sLNm);
        }
    }
}
