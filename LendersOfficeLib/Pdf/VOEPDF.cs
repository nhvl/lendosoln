using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CVOEListPDF : AbstractVerificationListPDF 
    {
        public override string Description 
        {
            get { return "Verification of Employment"; }
        }

        protected override IEnumerable<PdfVerificationItem> GetItems(CAppData dataApp)
        {
            List<PdfVerificationItem> list = new List<PdfVerificationItem>();

            var appList = new[] {
                new { empCollection = dataApp.aBEmpCollection, displayName = dataApp.aBNm}
                , new { empCollection = dataApp.aCEmpCollection, displayName = dataApp.aCNm}
            };

            foreach (var app in appList)
            {
                var subCollection = app.empCollection.GetSubcollection(true, E_EmpGroupT.All);
                foreach (IEmploymentRecord o in subCollection)
                {
                    if (o.EmplrNm.TrimWhitespaceAndBOM() == "")
                        continue; // 5/25/2004 dd - If employer name is empty do not add to list.


                    string displayName = Utilities.SafeHtmlString(app.displayName) + ", " + Utilities.SafeHtmlString(o.EmplrNm);

                    list.Add(new PdfVerificationItem() { RecordId = o.RecordId, DisplayName = displayName });
                }
            }
            return list;
        }

        protected override AbstractPDFFile ContructPdfItem()
        {
            return new CVOEPDF();
        }
    }

	public class CVOEPDF : AbstractLetterPDF
	{

        private bool m_isBorrower = false;

        public override string PdfFile 
        {
            get { return "VOE.pdf"; }
        }

        public override string Description 
        {
            get { return "VOE: " + Arguments["displayname"]; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Verifications/VOERecord.aspx"; }
        }

        public override Guid RecordID
        {
            get
            {
                Guid id;

                if (Guid.TryParse(this.Arguments["recordid"], out id))
                {
                    return id;
                }
                else
                {
                    return Guid.Empty;
                }
            }
        }

        private IEmploymentRecord FindEmployment(CAppData dataApp, Guid recordID) 
        {
            for (int i = 0; i < 2; i++) 
            {
                m_isBorrower = i == 0;
                IEmpCollection empCollection = m_isBorrower ? dataApp.aBEmpCollection : dataApp.aCEmpCollection;
                var subCollection = empCollection.GetSubcollection(true, E_EmpGroupT.All);
                foreach (IEmploymentRecord o in subCollection) 
                {
                    if (o.RecordId == recordID)
                        return o; // 5/25/2004 dd - Return immediately
                }
            }
            return null; // 5/25/2004 dd - NOT FOUND return null
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            IEmploymentRecord employmentRecord = FindEmployment(dataApp, RecordID);
            if (null == employmentRecord)
                return; // 5/25/2004 dd - Unable to find employment record. Do not fill in printout.

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfEmployment, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            string preparerTitle = "";
            string preparerAddress = "";

            if (dataLoan.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp && employmentRecord.VerifHasSignature)
            {
                EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(dataLoan.sBrokerId, employmentRecord.VerifSigningEmployeeId, dataLoan.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);
                AddFormFieldData("LenderSignature", signInfo.SignatureKey.ToString());
            }

            if (preparer.IsValid) 
            {
                string name = string.Format("{0}{2}{1}", preparer.PreparerName, preparer.CompanyName, Environment.NewLine);
                string phone = "";
                if (preparer.Phone != "") 
                {
                    phone = "Phone: " + preparer.Phone + "  ";
                }
                if (preparer.FaxNum != "") 
                {
                    phone += "Fax: " + preparer.FaxNum;
                }
                preparerTitle = preparer.Title;
                preparerAddress = Tools.FormatAddress(name, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip, phone);
            }


            object[] args = null;
            if (m_isBorrower) 
            {
                args = new object[] { dataApp.aBNmAndSsnForVerifications, dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip, Environment.NewLine };
            } 
            else 
            {
                args = new object[] { dataApp.aCNmAndSsnForVerifications, dataApp.aCAddr, dataApp.aCCity, dataApp.aCState, dataApp.aCZip, Environment.NewLine };
            }

            string applicantAddress = string.Format("{0}{5}{1} {2}, {3} {4}", args);
            string employerPhone = "";
            if (employmentRecord.EmplrBusPhone != "")
                employerPhone = "Phone: " + employmentRecord.EmplrBusPhone + "   ";
            if (employmentRecord.EmplrFax != "")
                employerPhone += "Fax: " + employmentRecord.EmplrFax;

            string employerAddress = Tools.FormatAddress(employmentRecord.Attention + Environment.NewLine + employmentRecord.EmplrNm, employmentRecord.EmplrAddr, employmentRecord.EmplrCity, employmentRecord.EmplrState, employmentRecord.EmplrZip, employerPhone);

            if (employmentRecord.IsSeeAttachment) 
            {
                AddFormFieldData("ApplicantSignature", "SEE ATTACHMENT");
            }

            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("EmployerAddress", employerAddress);
            AddFormFieldData("PreparerAddress", preparerAddress);
            AddFormFieldData("PreparerTitle", preparerTitle);
            // 10/27/2005 dd - OPM 3195. If VOE prepare date is blank then use order date.
            if (preparer.PrepareDate_rep == "") 
            {
                AddFormFieldData("Date", employmentRecord.VerifSentD_rep);
            } 
            else 
            {
                AddFormFieldData("Date", preparer.PrepareDate_rep);
            }
            AddFormFieldData("sLenderNumVerif", dataLoan.sLenderNumVerif);
            AddFormFieldData("ApplicantAddress", applicantAddress);


        }
	}
}
