using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CFloodInsuranceDisclosurePDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "FloodInsuranceDisclosure.pdf"; }
        }
        public override string Description 
        {
            get { return "Flood Insurance Disclosure"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            string brokerName = "";
            IPreparerFields agent = dataLoan.GetPreparerOfForm( E_PreparerFormT.BorrCertification, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (agent.IsValid) 
            {
                brokerName = agent.CompanyName;
            }

            AddFormFieldData("BrokerName", brokerName);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("PropertyAddress", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
			IPreparerFields gfeTil = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
			AddFormFieldData("GfeTilPrepareDate", gfeTil.PrepareDate_rep);
		}
	}
}
