namespace LendersOffice.Pdf
{
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using System.Data;
    using DataAccess;
    using LOPDFGenLib;

    /// <summary>
    /// Summary description for AgentListPDF.
    /// </summary>
    public class CAgentListPDF : AbstractFreeLayoutPDFFile
	{

        public override string Description 
        {
            get { return "Loan Tracking Report - Agent List"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Status/Agents.aspx"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddHeader(dataLoan.sLNm, dataApp.aBNm, dataApp.aCNm);
            CAgentFields field = null;

            field = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer,E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (field.IsValid) 
            {
                AddAgent(field.AgentRoleDescription, field.AgentName, field.Phone, field.FaxNum, field.CellPhone, field.EmailAddr);
            }

            field = dataLoan.GetAgentOfRole(E_AgentRoleT.Processor,E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (field.IsValid) 
            {
                AddAgent(field.AgentRoleDescription, field.AgentName, field.Phone, field.FaxNum, field.CellPhone, field.EmailAddr);
            }

            int count = dataLoan.sAgentCollection.GetAgentRecordCount();
            for (int i = 0; i < count; i++) 
            {
                field = dataLoan.GetAgentFields(i);
                if (field.AgentRoleT != E_AgentRoleT.LoanOfficer &&
                    field.AgentRoleT != E_AgentRoleT.Processor) 
                {
                    AddAgent(field.AgentRoleDescription, field.CaseNum, field.AgentName, field.Phone, field.FaxNum, field.CompanyName, field.StreetAddr, field.CityStateZip, field.CellPhone, field.EmailAddr);

                }
            }
        }

        #region "Helper layout functions"
        protected override void AddTitle() 
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 12;

            RowItem row = new RowItem(0, 40);
            row.Add(new TextItem("Tracking - Agents List", 240, 0, font, fontSize));
            AddItem(row);

        }
        private void AddHeader(string sLNm, string aBNm, string aCNm) 
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Loan Number: ", 40, 0, font, fontSize));
            row.Add(new TextItem(sLNm, 100, 0, font, fontSize));
            row.Add(new TextItem("Date/Time: ", 450, 0, font, fontSize));
            row.Add(new TextItem(DateTime.Now.ToString("MM/dd/yyyy hh:mm tt"), 500, 0, font, fontSize));

            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Borrower: " , 40, 0, font, fontSize));

            string name = aBNm;
            if (aCNm != "")
                name += " / " + aCNm;
            row.Add(new TextItem(name, 80, 0, font, fontSize));

           AddItem(row);
        }

        private void AddAgent(string agentDescription, string number, string agentName, string phone, string fax, string companyName, string streetAddr, string cityStateZip, string cellphone, string emailAddr) 
        {
            FontName font = FontName.Helvetica;
            short fontSize = 8;
            short x = 150;
            short x1 = 200;

            RowItem row = new RowItem();
            row.Add(new TextItem(agentDescription + ":", 40, 0, font, fontSize));
            row.Add(new TextItem("Number", x, 0, font, fontSize));
            row.Add(new TextItem("- " + number, x1, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Name", x, 0, font, fontSize));
            row.Add(new TextItem("- " + agentName, x1, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Phone", x, 0, font, fontSize));
            row.Add(new TextItem("- " + phone, x1, 0, font, fontSize));
            row.Add(new TextItem("Cell", 444, 0, font, fontSize));
            row.Add(new TextItem("- " + cellphone, 468, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Email", x, 0, font, fontSize));
            row.Add(new TextItem("- " + emailAddr, x1, 0, font, fontSize));
            row.Add(new TextItem("Fax", 444, 0, font, fontSize));
            row.Add(new TextItem("- " + fax, 468, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Company", x, 0, font, fontSize));
            row.Add(new TextItem("- " + companyName, x1, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Address", x, 0, font, fontSize));
            row.Add(new TextItem("- " + streetAddr,x1,  0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("- " + cityStateZip, x1, 0, font, fontSize));
            AddItem(row);
            AddItem(new HorizontalLineItem());


        }
        private void AddAgent(string agentDescription, string agentName, string phone, string fax, string cellphone, string email) 
        {
            FontName font = FontName.Helvetica;
            short fontSize = 8;
            short x = 150;
            short x1 = 200;

            RowItem row = new RowItem();
            row.Add(new TextItem(agentDescription + ":", 40, 0, font, fontSize));
            row.Add(new TextItem("Name", x, 0, font, fontSize));
            row.Add(new TextItem("- " + agentName, x1, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Phone", x, 0, font, fontSize));
            row.Add(new TextItem("- " + phone, x1, 0, font, fontSize));
            row.Add(new TextItem("Cell", 444, 0, font, fontSize));
            row.Add(new TextItem("- " + cellphone, 468, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Email", x, 0, font, fontSize));
            row.Add(new TextItem("- " + email, x1, 0, font, fontSize));
            row.Add(new TextItem("Fax", 444, 0, font, fontSize));
            row.Add(new TextItem("- " + fax, 468, 0, font, fontSize));
            AddItem(row);

            AddItem(new HorizontalLineItem());
        }

#endregion 
	}
}
