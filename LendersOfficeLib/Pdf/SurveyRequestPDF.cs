using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;


namespace LendersOffice.Pdf
{
	public class CSurveyRequestPDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "SurveyRequest.pdf"; }
        }
        
        public override string Description 
        {
            get { return "Survey Request"; }
        }
	    
        public override string EditLink 
        {
            get { return "/newlos/RequestForms/SurveyRequest.aspx"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("date", DateTime.Now.ToShortDateString()); // 1/31/2005 dd - POINT always use current date.
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aBHPhone", dataApp.aBHPhone);
            AddFormFieldData("sSpT", dataLoan.sSpT);

            E_sLPurposeT sLPurposeT = dataLoan.sLPurposeT;
            if (sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || sLPurposeT == E_sLPurposeT.VaIrrrl)
                sLPurposeT = E_sLPurposeT.Refin;
            if (sLPurposeT == E_sLPurposeT.HomeEquity)
            {
                sLPurposeT = E_sLPurposeT.RefinCashout;
            }

            AddFormFieldData("sLPurposeT", sLPurposeT);
            AddFormFieldData("sSpLegalDesc", dataLoan.sSpLegalDesc);
            AddFormFieldData("PropertyAddress", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            AddFormFieldData("sAttachedContractBit", dataLoan.sAttachedContractBit);
            AddFormFieldData("sAttachedSurveyBit", dataLoan.sAttachedSurveyBit);
            AddFormFieldData("sAttachedPlansBit", dataLoan.sAttachedPlansBit);
            AddFormFieldData("sAttachedCostsBit", dataLoan.sAttachedCostsBit);
            AddFormFieldData("sAttachedSpecsBit", dataLoan.sAttachedSpecsBit);
            AddFormFieldData("sSurveyRequestN", dataLoan.sSurveyRequestN);

            CAgentFields seller = dataLoan.GetAgentOfRole(E_AgentRoleT.Seller, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("SellerName", seller.AgentName);
            AddFormFieldData("SellerPhone", seller.Phone);

            var titleAgent = dataLoan.GetAgentOfRole(E_AgentRoleT.Title, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("TitleAgent_CompanyName", titleAgent.CompanyName);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.SurveyRequest_From, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            string desc = "";
            if (preparer.PhoneOfCompany != "")
                desc += "Phone: " + preparer.PhoneOfCompany + Environment.NewLine;
            if (preparer.FaxOfCompany != "")
                desc += "Fax: " + preparer.FaxOfCompany;

            AddFormFieldData("SurveyRequest_From", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip, desc));
            AddFormFieldData("SurveyRequest_FromPrepareDate", preparer.PrepareDate_rep);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.SurveyRequest_To, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("SurveyRequest_To", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));
        }
	}
}
