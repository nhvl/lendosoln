using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CHUD_54114PDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "HUD-54114.pdf"; }
        }
        public override string Description 
        {
            get { return "FHA D.E Analysis of Appraisal Report (HUD-54114)"; }
        }
        
        public override string EditLink 
        {
            get { return "/newlos/FHA/FHAAnalysisAppraisedValue.aspx"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {

            string name = dataApp.aBNm;
            if (dataApp.aCNm.TrimWhitespaceAndBOM() != "") 
            {
                name = name + " & " + dataApp.aCNm;
            }
            AddFormFieldData("BorrowerName", name);
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sLenderCaseNum", dataLoan.sLenderCaseNum);
            AddFormFieldData("PropertyAddress", string.Format("{0}{1}{2}{1}County: {3}", dataLoan.sSpAddr, Environment.NewLine, Tools.CombineCityStateZip(dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip), dataLoan.sSpCounty));

            IPreparerFields appraiser = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAnalysisAppraisalAppraiser, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("AppraiserAgentName", appraiser.PreparerName);

            AddFormFieldData("AnalysisAppraisalUnderwriterPreparerName", dataLoan.sFHAApprUnderwriterName);
            AddFormFieldData("AnalysisAppraisalUnderwriterLicenseNumOfAgent", dataLoan.sFHAApprUnderwriterChumsId);

            AddFormFieldData("sApprVal", dataLoan.sApprVal_rep);
            AddFormFieldData("sFHAApprIsFairTri", dataLoan.sFHAApprIsFairTri);
            AddFormFieldData("sFHAApprNotFairExplanation", dataLoan.sFHAApprNotFairExplanation);
            AddFormFieldData("sFHAApprQualityComment", dataLoan.sFHAApprQualityComment);
            AddFormFieldData("sFHAApprAreComparablesAcceptableTri", dataLoan.sFHAApprAreComparablesAcceptableTri);
            AddFormFieldData("sFHAApprComparablesComment", dataLoan.sFHAApprComparablesComment);
            AddFormFieldData("sFHAApprAdjAcceptableTri", dataLoan.sFHAApprAdjAcceptableTri);
            AddFormFieldData("sFHAApprAdjNotAcceptableExplanation", dataLoan.sFHAApprAdjNotAcceptableExplanation);
            AddFormFieldData("sFHAApprIsValAcceptableTri", dataLoan.sFHAApprIsValAcceptableTri);
            AddFormFieldData("sFHAApprValNeedCorrectionTri", dataLoan.sFHAApprValNeedCorrectionTri);
            AddFormFieldData("sFHAApprCorrectedVal", dataLoan.sFHAApprCorrectedVal_rep);
            AddFormFieldData("sFHAApprValCorrectionJustification", dataLoan.sFHAApprValCorrectionJustification);
            AddFormFieldData("sFHAApprRepairConditions", dataLoan.sFHAApprRepairConditions);
            AddFormFieldData("sFHAApprOtherComments", dataLoan.sFHAApprOtherComments);

        }

	}
}
