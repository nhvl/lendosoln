﻿namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Reflection;
    using DataAccess;
    using LendersOffice.ObjLib.Extensions;
    using LendersOffice.PdfGenerator;

    /// <summary>
    /// Provides a repository for <see cref="IPDFGenerator"/>
    /// instances.
    /// </summary>
    public class PdfGeneratorRepository
    {
        /// <summary>
        /// Provides a mapping between PDF print generator names and their types.
        /// </summary>
        private static readonly LqbSingleThreadInitializeLazy<Dictionary<string, Type>> PdfGeneratorTypesByName =
            new LqbSingleThreadInitializeLazy<Dictionary<string, Type>>(CreatePdfGeneratorTypesByName);

        /// <summary>
        /// Determines whether the repository contains a generator
        /// for the given PDF name.
        /// </summary>
        /// <param name="pdfName">
        /// The name of the PDF.
        /// </param>
        /// <returns>
        /// True if the repository has a generator, false otherwise.
        /// </returns>
        public bool HasPdfGenerator(string pdfName) => PdfGeneratorTypesByName.Value.ContainsKey(pdfName);

        /// <summary>
        /// Retrieves the names of all standard forms.
        /// </summary>
        /// <returns>
        /// The list of all standard form names.
        /// </returns>
        public IEnumerable<string> GetAllStandardFormNames()
        {
            return PdfGeneratorTypesByName.Value.Keys.Where(name => !string.Equals("custom", name, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// Creates a PDF generator instance.
        /// </summary>
        /// <param name="pdfName">
        /// The name of the PDF.
        /// </param>
        /// <returns>
        /// The PDF generator or null if the generator
        /// does not exist.
        /// </returns>
        public IPDFGenerator CreatePdfInstance(string pdfName)
        {
            IPDFGenerator pdf = null;

            Type t = null;
            PdfGeneratorTypesByName.Value.TryGetValue(pdfName, out t);
            if (t != null)
            {
                ConstructorInfo c = t.GetConstructor(new Type[0]);
                if (c != null)
                {
                    IPDFPrintItem obj = (IPDFPrintItem)c.Invoke(new object[0]);
                    pdf = (IPDFGenerator)obj;
                }
            }

            return pdf;
        }

        /// <summary>
        /// Creates a PDF generator instance.
        /// </summary>
        /// <param name="pdfName">
        /// The name of the PDF.
        /// </param>
        /// <param name="pdfArguments">
        /// The arguments for the PDF.
        /// </param>
        /// <returns>
        /// The PDF generator or null if the generator
        /// does not exist.
        /// </returns>
        public IPDFGenerator CreatePdfInstance(string pdfName, NameValueCollection pdfArguments)
        {
            IPDFGenerator pdf = null;

            Type t = PdfGeneratorTypesByName.Value[pdfName];
            if (t != null)
            {
                ConstructorInfo c = t.GetConstructor(new Type[0]);
                if (c != null)
                {
                    IPDFPrintItem obj = (IPDFPrintItem)c.Invoke(new object[0]);
                    if (obj != null)
                    {
                        obj.Arguments = pdfArguments;
                    }

                    pdf = (IPDFGenerator)obj;
                }
                else
                {
                    c = t.GetConstructor(new Type[] { typeof(NameValueCollection) });
                    if (c != null)
                    {
                        pdf = (IPDFGenerator)c.Invoke(new object[] { pdfArguments });
                    }
                }
            }

            return pdf;
        }

        /// <summary>
        /// Creates the mapping of PDF generator types by name.
        /// </summary>
        /// <returns>
        /// The PDF generator types by name.
        /// </returns>
        private static Dictionary<string, Type> CreatePdfGeneratorTypesByName()
        {
            var pdfTypesByName = new Dictionary<string, Type>(StringComparer.OrdinalIgnoreCase);

            // Get the assembly PDFHandler reside.
            Assembly assembly = typeof(PDFHandler).Assembly;

            Type[] list = assembly.GetTypes();
            Type ipdfType = typeof(IPDFGenerator);
            foreach (Type type in list)
            {
                // 9/5/2006 dd - Skip these two special classes.
                // 10/26/2017 gf - Also skip the payment statement. It is not in
                // the print list and it requires user input to generate.
                if (type.Name == "BatchPDFControl" || type.Name == "GenericBatchPDF" || type == typeof(PaymentStatement))
                {
                    continue;
                }

                // For some reason type.IsSubclassOf(typeof(LOPrnLib.IPDFGenerator)) always return false.
                if (type.IsClass && !type.IsAbstract && type.GetInterface(ipdfType.Name) != null)
                {
                    ConstructorInfo constructor = type.GetConstructor(new Type[0]);
                    if (constructor != null)
                    {
                        object o = constructor.Invoke(new object[0]);
                        IPDFPrintItem printItem = o as IPDFPrintItem;

                        if (printItem != null)
                        {
                            pdfTypesByName.Add(printItem.Name, type);
                        }
                        else
                        {
                            Tools.LogWarning(type.Name + " does not implements LendersOffice.Common.IPDFPrintItem");
                        }
                    }
                    else
                    {
                        Tools.LogWarning(type.Name + " has no default constructor");
                    }
                }
            }

            return pdfTypesByName;
        }
    }
}
