namespace LendersOffice.Pdf
{
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Reminders;
    using LendersOffice.Security;
    using LOPDFGenLib;

    /// <summary>
    /// Summary description for LoanTaskPDF.
    /// </summary>
    public class CLoanTaskPDF : AbstractFreeLayoutPDFFile
    {
        public override bool IsVisible
        {
            get
            {
                BrokerDB broker = BrokerDB.RetrieveById(BrokerUserPrincipal.CurrentPrincipal.BrokerId);
                return broker.IsUseNewTaskSystem == false;
            }
        }
        public override string Description 
        {
            get { return "Loan Tracking Report - Tasks List"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/TaskList.aspx"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
//            IsDebug = true;
            AddHeader(dataLoan.sLNm, dataApp.aBNm, dataApp.aCNm);

			try
			{
				CDiscussionSet ds = new CDiscussionSet(dataLoan.sLId);

				foreach (CDiscussion o in ds)
					AddTask(o);
			}
			catch (Exception exc)
			{
                Tools.LogErrorWithCriticalTracking("CLoanTaskPDF", exc);

                RowItem r = new RowItem();
                r.Add(new TextItem("Error had occur while generating this printout. Please contact technical support for assistance.", 20, 0, 600, FontName.Helvetica, 14, HorizontalAlign.Center));
                AddItem(r);
			}

            // 3/17/2004 dd - Add ending indicator to the end of print out.
            RowItem row = new RowItem();
            row.Add(new TextItem(" ", 0, 0));
            AddItem(row);
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("---- End Tasks List ----", 20, 0, 600, FontName.Helvetica, 14, HorizontalAlign.Center));
            AddItem(row);
        }

        #region Helper functions to do layout.
        protected override void AddTitle() 
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 12;

            RowItem row = new RowItem(0, 40);
            row.Add(new TextItem("Tracking - Tasks List", 240, 0, font, fontSize));
            AddItem(row);

        }
        private void AddHeader(string sLNm, string aBNm, string aCNm) 
        {
            FontName font = FontName.HelveticaBold;
            short fontSize = 8;

            RowItem row = new RowItem();
            row.Add(new TextItem("Loan Number: ", 40, 0, font, fontSize));
            row.Add(new TextItem(sLNm, 100, 0, font, fontSize));
            row.Add(new TextItem("Date/Time: ", 450, 0, font, fontSize));
            row.Add(new TextItem(DateTime.Now.ToString("MM/dd/yyyy hh:mm tt"), 500, 0, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("Borrower: ", 40, 0, font, fontSize));

            string name = aBNm;
            if (aCNm != "")
                name += " / " + aCNm;
            row.Add(new TextItem(name, 80, 0, font, fontSize));
            AddItem(row);
            AddItem(new HorizontalLineItem());

        }

        private void AddTask(CDiscussion task) 
        {
            FontName font = FontName.Helvetica;
            FontName boldFont = FontName.HelveticaBold;
            short fontSize = 8;

            // 3/17/2004 dd - Display:  Subject: {0}
            RowItem row = new RowItem();
            row.Add(new TextItem("Subject: ", 40, 0, boldFont, fontSize));
            row.Add(new TextItem(task.Subject, 80, 0, 500, font, fontSize));
            AddItem(row);

            // 3/17/2004 dd - Display: Status: {0}     Priority: {1}     Due Date: {2}
            row = new RowItem();
            row.Add(new TextItem("Status: " , 40, 0, boldFont, fontSize));
            row.Add(new TextItem(task.Status, 80, 0, font, fontSize));
            row.Add(new TextItem("Priority: ", 200, 0, boldFont, fontSize));
            row.Add(new TextItem(task.Priority, 250, 0, font, fontSize));
            row.Add(new TextItem("Due Date: ", 400, 0, boldFont, fontSize));
            row.Add(new TextItem(task.DueDate, 450, 0, font, fontSize));
            AddItem(row);

            // 3/17/2004 dd - Display: Active Participants: {0}
            row = new RowItem();
            row.Add(new TextItem("Active Participants: ", 40, 0, boldFont, fontSize));
            row.Add(new TextItem(task.ActiveParticipants, 120, 0, 400, font, fontSize));
            AddItem(row);

            // 3/17/2004 dd - Display: Past Participants: {0}
            row = new RowItem();
            row.Add(new TextItem("Past Participants: ", 40, 0, boldFont, fontSize));
            row.Add(new TextItem(task.PastParticipants, 120, 0, 400, font, fontSize));
            AddItem(row);

            row = new RowItem();
            row.Add(new TextItem("History: ", 40, 0, boldFont, fontSize));
            AddItem(row);

            // 3/17/2004 dd - Due to the limitation of LOPDFGenLib, if Log is longer than 1 page then when
            // printout Log will get chops off at the end.
            // Work around is split Log by '\n' instead of create TextItem of Log.
            string[] lines = task.Log.Split('\n');
            foreach (string s in lines) 
            {
                row = new RowItem();
            
                row.Add(new TextItem(s, 60, 0, 500, font, fontSize));
                AddItem(row);
            }

            AddItem(new TextItem("", 40, 0, boldFont, fontSize)); // Just Empty LIne.
            AddItem(new HorizontalLineItem());
        }
        #endregion
    }
}
