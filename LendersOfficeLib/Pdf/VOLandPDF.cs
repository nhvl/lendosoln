using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Admin;

namespace LendersOffice.Pdf
{
    public class CVOLandListPDF : AbstractVerificationListPDF 
    {
        public override string Description 
        {
            get { return "Verification of Land Contract"; }
        }

        protected override IEnumerable<PdfVerificationItem> GetItems(CAppData dataApp)
        {
            List<PdfVerificationItem> list = new List<PdfVerificationItem>();

            int count = dataApp.GetVorRecordCount();
            for (int i = 0; i < count; i++)
            {
                IVerificationOfRent o = dataApp.GetVorFields(i);
                if (o.VerifT == E_VorT.LandContract)
                {
                    string displayName = Utilities.SafeHtmlString(dataApp.aBNm) + ", " + Utilities.SafeHtmlString(o.AddressFor_SingleLine);

                    list.Add(new PdfVerificationItem() { RecordId = o.RecordId, DisplayName = displayName });
                }
            }

            return list;
        }

        protected override AbstractPDFFile ContructPdfItem()
        {
            return new CVOLandPDF();
        }
    }
	public class CVOLandPDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "VOM.pdf"; }
        }

        public override string Description 
        {
            get { return "Verif of Land: " + Arguments["displayname"]; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Verifications/VOLandRecord.aspx"; }
        }

        public override Guid RecordID
        {
            get
            {
                Guid id;

                if (Guid.TryParse(this.Arguments["recordid"], out id))
                {
                    return id;
                }
                else
                {
                    return Guid.Empty;
                }
            }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            IVerificationOfRent field = dataApp.GetVorFields(RecordID);

            if (dataLoan.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp && field.VerifHasSignature)
            {
                EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(dataLoan.sBrokerId, field.VerifSigningEmployeeId, dataLoan.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);                
                AddFormFieldData("LenderSignature", signInfo.SignatureKey.ToString());
            }

            string creditorAddress = Tools.FormatAddress(field.Attention + Environment.NewLine + field.LandlordCreditorName, field.AddressTo, field.CityTo, field.StateTo, field.ZipTo);
            string propertyAddress = Tools.FormatAddress(field.AddressFor, field.CityFor, field.StateFor, field.ZipFor);
            bool isSeeAttachment = field.IsSeeAttachment;

            string borrowerName = dataApp.aBNmAndSsnForVerifications.TrimWhitespaceAndBOM() + Environment.NewLine;
            string coborrowerName = dataApp.aCNmAndSsnForVerifications.TrimWhitespaceAndBOM();
            if (!string.IsNullOrEmpty(coborrowerName)) coborrowerName += Environment.NewLine;

            string applicantAddress = string.Format("{0}{1}{2}", borrowerName, 
                coborrowerName, Tools.FormatAddress(dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip));


            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfLandContract, E_ReturnOptionIfNotExist.ReturnEmptyObject);

                string name = string.Format("{0}{2}{1}", preparer.PreparerName, preparer.CompanyName, Environment.NewLine);
            
                string brokerAddress = Tools.FormatAddress(name, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip);


            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("sLenderNumVerif", dataLoan.sLenderNumVerif);

            AddFormFieldData("CreditorAddress", creditorAddress);
            AddFormFieldData("BrokerAddress", brokerAddress);
            AddFormFieldData("Title", preparer.Title);
            AddFormFieldData("Date", preparer.PrepareDate_rep);
            AddFormFieldData("PropertyAddress", propertyAddress);
            AddFormFieldData("AccountName", field.AccountName);
            AddFormFieldData("ApplicantAddress", applicantAddress);
            AddFormFieldData("AccountType", "L");

            if (isSeeAttachment) 
            {
                AddFormFieldData("ApplicantSignature", "SEE ATTACHMENT");
                if (dataApp.aCSsn != string.Empty)
                {
                    AddFormFieldData("CoApplicantSignature", "SEE ATTACHMENT");
                }

            }

        }
	}
}
