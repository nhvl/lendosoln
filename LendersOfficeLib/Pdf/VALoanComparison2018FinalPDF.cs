﻿namespace LendersOffice.Pdf
{
    using DataAccess;

    /// <summary>
    /// Data mapping class for the VA Loan Comparison 2018 Final PDF.
    /// </summary>
    public class VALoanComparison2018FinalPDF : AbstractLetterPDF
    {
        /// <summary>
        /// PDF File Name.
        /// </summary>
        public override string PdfFile
        {
            get { return "VALoanComparison2018Final.pdf"; }
        }

        /// <summary>
        /// PDF File Description.
        /// </summary>
        public override string Description
        {
            get { return "VA Loan Comparison (Final Statement)"; }
        }

        /// <summary>
        /// PDF File Edit Link.
        /// </summary>
        public override string EditLink
        {
            get { return "/newlos/VA/VALoanComparison2018.aspx"; }
        }

        /// <summary>
        /// Applies data to the form.
        /// </summary>
        /// <param name="dataLoan">Data loan object.</param>
        /// <param name="dataApp">Data app object.</param>
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            // existing fields from original VA Loan Comparison PDF
            CAgentFields preparer = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            this.AddFormFieldData("CompanyName", preparer.CompanyName);

            this.AddFormFieldData("sSpAddr_Multiline", dataLoan.sSpAddr_MultiLine);
            this.AddFormFieldData("Applicants", dataApp.aBNm_aCNm);
            this.AddFormFieldData("aBNm", dataApp.aBNm);
            this.AddFormFieldData("aCNm", dataApp.aCNm);

            this.AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            this.AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            this.AddFormFieldData("sRateAmortT", dataLoan.sRateAmortT);
            this.AddFormFieldData("sTerm", dataLoan.sTerm_rep);
            this.AddFormFieldData("sMonthlyPmt", dataLoan.sMonthlyPmt_rep);
            this.AddFormFieldData("aVaTotalClosingCost", dataLoan.aVaTotalClosingCost_rep);
            this.AddFormFieldData("sVaLoanNumCurrentLoan", dataLoan.sVaLoanNumCurrentLoan);
            this.AddFormFieldData("sVaLoanAmtCurrentLoan", dataLoan.sVaLoanAmtCurrentLoan_rep);
            this.AddFormFieldData("sVaNoteIrCurrentLoan", dataLoan.sVaNoteIrCurrentLoan_rep);
            this.AddFormFieldData("sVaTermCurrentLoan", dataLoan.sVaTermCurrentLoan_rep);
            this.AddFormFieldData("sVaMonthlyPmtCurrentLoan", dataLoan.sVaMonthlyPmtCurrentLoan_rep);
            this.AddFormFieldData("sVaNewPmtQualified", dataLoan.sVaNewPmtQualified);
            this.AddFormFieldData("sVaLoanCompMonthlyPmtDecreaseAmt", dataLoan.sVaLoanCompMonthlyPmtDecreaseAmt_rep);
            this.AddFormFieldData("sVaLoanCompRecoupCostsTime", dataLoan.sVaLoanCompRecoupCostsTime_rep);
            this.AddFormFieldData("sProThisMPmt", dataLoan.sProThisMPmt_rep);
            this.AddFormFieldData("sVaPICurrentLoan", dataLoan.sVaPICurrentLoan_rep);

            // new fields for the VA Loan Comparison 2018 Final PDF
            DataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
            this.AddFormFieldData("sVaFinalServYouDidShopFor", dataLoan.sVaFinalServYouDidShopFor_rep);
            this.AddFormFieldData("sVaFinalServYouDidNotShopFor", dataLoan.sVaFinalServYouDidNotShopFor_rep);
            this.AddFormFieldData("sTRIDClosingDisclosureGeneralLenderCredits", dataLoan.sTRIDClosingDisclosureGeneralLenderCredits_rep);
            this.AddFormFieldData("sVaFinalLoanCompRecoupCostsTime", Tools.IsStringEmptyOrNegativeInt(dataLoan.sVaFinalLoanCompRecoupCostsTime_rep) ? "N/A" : dataLoan.sVaFinalLoanCompRecoupCostsTime_rep);
            this.AddFormFieldData("sVaFinalFundingFee", dataLoan.sVaFinalFundingFee_rep);
            this.AddFormFieldData("sVaFinalOriginationCharge", dataLoan.sVaFinalOriginationCharge_rep);
            this.AddFormFieldData("sVaFinalTotalClosingCost", dataLoan.sVaFinalTotalClosingCost_rep);
            this.AddFormFieldData("sVaFinalTaxesOtherGovFee", dataLoan.sVaFinalTaxesOtherGovFee_rep);
            this.AddFormFieldData("sVaFinalOtherFee", dataLoan.sVaFinalOtherFee_rep);
        }
    }
}
