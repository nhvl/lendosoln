﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" version="1.0" encoding="utf-16" indent="yes"/>

  <xsl:template match="/">
    <xsl:apply-templates select="TASKLIST"/>
  </xsl:template>

  <xsl:template match="TASKLIST">
    <html>
      <head>
        <title>Task List</title>
        <style>
          #TaskListTable {
          border-spacing: 0px;
          border-collapse: collapse;
          width:100%;
          }
          * {
          font-family: "Calibri", sans-serif;
          }
          h1 {
          font-weight: normal;
          }
          th {
          background: #BBBBFF;
          text-align: left;
          }
          td, th {
          border: 1px solid black;
          <!-- possible soln to page break problem: get rid of this border -->
          }
          .AlternateRow {
          background: #DDDDFF;
          }

          .CheckboxColumn {
          width: 1.5em;
          }
          .TriggerColumn {
          width: 6em;
          }
          .TaskIDColumn {
          width: 8em;
          }
          .SubjectColumn {
          width: 42em;
          }
          .DateColumn {
          width: 8em;
          }
          .AssignedToColumn {
          width:  8em;
          }
          .OwnerColumn {
          width:  8em;
          }
          .WarningDate {
          width: 8em;
          color: red;
          }

        </style>
      </head>
      <body>
        <h1>
          Task List
        </h1>
        <table id="TaskListTable">
          <tr>
            <th class="CheckboxColumn">
              <xsl:comment>Checkbox goes here</xsl:comment>
            </th>
            <th class="TriggerColumn">Trigger</th>
            <th class="TaskIDColumn">Task</th>
            <th class="SubjectColumn">Subject</th>
            <th class="DateColumn">Due Date</th>
            <th class="AssignedToColumn">To Be Assigned</th>
            <th class="OwnerColumn">To Own</th>
          </tr>

          <xsl:apply-templates select="Task" />
        </table>

      </body>
    </html>
  </xsl:template>

  <xsl:template match="Task">
    <tr style="page-break-inside: avoid;">
      <xsl:if test="position() mod 2 != 0">
        <xsl:attribute name="class">AlternateRow</xsl:attribute>
      </xsl:if>
      <td class="CheckboxColumn">
        <input type="checkbox"></input>
      </td>
      <td class="TriggerColumn">
        <xsl:value-of select="Trigger"/>
      </td>
      <td class="TaskIDColumn">
        <xsl:value-of select="TaskID"/>
      </td>
      <td class="SubjectColumn">
        <xsl:value-of select="Subject"/>
      </td>
      <td class="DateColumn">
        <xsl:value-of select="DueDate"/>
      </td>
      <td class="AssignedToColumn">
        <xsl:value-of select="Assigned"/>
      </td>
      <td class="OwnerColumn">
        <xsl:value-of select="Owner"/>
      </td>
    </tr>

  </xsl:template>

</xsl:stylesheet>
