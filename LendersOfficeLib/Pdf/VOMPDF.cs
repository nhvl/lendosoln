using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Admin;

namespace LendersOffice.Pdf
{
    public class CVOMListPDF : AbstractVerificationListPDF 
    {
        public override string Description 
        {
            get { return "Verification of Mortgage"; }
        }

        protected override IEnumerable<PdfVerificationItem> GetItems(CAppData dataApp)
        {
            List<PdfVerificationItem> list = new List<PdfVerificationItem>();

            ILiaCollection recordList = dataApp.aLiaCollection;
            int count = recordList.CountRegular;

            string aBNm = Utilities.SafeHtmlString(dataApp.aBNm);
            string aCNm = Utilities.SafeHtmlString(dataApp.aCNm);

            for (int i = 0; i < count; i++)
            {
                ILiabilityRegular liability = recordList.GetRegularRecordAt(i);
                if (liability.DebtT.EqualsOneOf(E_DebtRegularT.Mortgage, E_DebtRegularT.Heloc))
                {
                    string displayName = "";
                    switch (liability.OwnerT)
                    {
                        case E_LiaOwnerT.Borrower: displayName = aBNm; break;
                        case E_LiaOwnerT.CoBorrower: displayName = aCNm; break;
                        case E_LiaOwnerT.Joint: displayName = aBNm + " & " + aCNm; break;
                        default:
                            throw new UnhandledEnumException(liability.OwnerT);
                    }

                    displayName = displayName + ", " + Utilities.SafeHtmlString(liability.ComNm);

                    list.Add(new PdfVerificationItem() { RecordId = liability.RecordId, DisplayName = displayName });
                }
            }

            return list;
        }

        protected override AbstractPDFFile ContructPdfItem()
        {
            return new CVOMPDF();
        }
    }

	public class CVOMPDF : AbstractLetterPDF
	{
 
        public override string PdfFile 
        {
            get { return "VOM.pdf"; }
        }
        public override string Description 
        {
            get { return "VOM: " + Arguments["displayname"]; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Verifications/VOMRecord.aspx"; }
        }

        public override Guid RecordID
        {
            get
            {
                Guid id;

                if (Guid.TryParse(this.Arguments["recordid"], out id))
                {
                    return id;
                }
                else
                {
                    return Guid.Empty;
                }
            }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {

            string propertyAddress = "";

            string brokerAddress = "";
            bool isSeeAttachment = false;


            ILiabilityRegular field = dataApp.aLiaCollection.GetRegRecordOf(RecordID);

            if (dataLoan.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp && field.VerifHasSignature)
            {
                EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(dataLoan.sBrokerId, field.VerifSigningEmployeeId, dataLoan.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);
                AddFormFieldData("LenderSignature", signInfo.SignatureKey.ToString());
            }

            string creditorPhone = "";
            if (field.ComPhone != "")
                creditorPhone = "Phone: " + field.ComPhone + "   ";
            if (field.ComFax != "")
                creditorPhone += "Fax: " + field.ComFax;

            string creditorAddress = Tools.FormatAddress(field.Attention + Environment.NewLine + field.ComNm, field.ComAddr, field.ComCity, field.ComState, field.ComZip, creditorPhone);
            Guid reRecordID = field.MatchedReRecordId;

            var reSubcoll = dataApp.aReCollection.GetSubcollection( true, E_ReoGroupT.All );
            foreach( var item in reSubcoll )
            {
                var f = (IRealEstateOwned)item;
                if (f.RecordId == reRecordID) 
                {
                    propertyAddress = Tools.FormatAddress(null, f.Addr, f.City, f.State, f.Zip);

                    break;
                }
            }
            isSeeAttachment = field.IsSeeAttachment;


            string borrowerName = dataApp.aBNmAndSsnForVerifications.TrimWhitespaceAndBOM() + Environment.NewLine;
            string coborrowerName = dataApp.aCNmAndSsnForVerifications.TrimWhitespaceAndBOM();
            if (!string.IsNullOrEmpty(coborrowerName)) coborrowerName += Environment.NewLine;

            string applicantAddress = string.Format("{0}{1}{2}", borrowerName, 
                coborrowerName, Tools.FormatAddress(dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip));


            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VerificationOfMortgage, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            string name = string.Format("{0}{2}{1}", preparer.PreparerName, preparer.CompanyName, Environment.NewLine);
            string phone = "";

            if (preparer.Phone != "")
                phone = "Phone: " + preparer.Phone + "   ";

            if (preparer.FaxNum != "")
                phone += "Fax: " + preparer.FaxNum;
            
            brokerAddress = Tools.FormatAddress(name, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip, phone);

            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("sLenderNumVerif", dataLoan.sLenderNumVerif);

            AddFormFieldData("CreditorAddress", creditorAddress);
            AddFormFieldData("BrokerAddress", brokerAddress);
            AddFormFieldData("Title", preparer.Title);
            AddFormFieldData("Date", preparer.PrepareDate_rep);
            AddFormFieldData("PropertyAddress", propertyAddress);
            AddFormFieldData("AccountName", field.AccNm);
            AddFormFieldData("AccountNumber", field.AccNum.Value);
            AddFormFieldData("ApplicantAddress", applicantAddress);
            AddFormFieldData("AccountType", "M");

            if (isSeeAttachment) 
            {
                AddFormFieldData("ApplicantSignature", "SEE ATTACHMENT");
                if (dataApp.aCSsn != string.Empty)
                {
                    AddFormFieldData("CoApplicantSignature", "SEE ATTACHMENT");
                }

            }
            


        }
	}
}
