﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:param name="LogoSource" />
  
  <xsl:output method="html" version="1.0" encoding="utf-16" indent="yes"/>

  <xsl:template match="/">
    <xsl:apply-templates select="TASKLIST"/>
  </xsl:template>

  <xsl:template match="TASKLIST">
    <html>
      <head>
        <title>
          <xsl:value-of select="@Heading"></xsl:value-of> List
        </title>
        <style>
          #TaskListTable {
          border-spacing: 0px;
          border-collapse: collapse;
          width:100%;
          }
          * {
          font-family: "Calibri", sans-serif;
          }
          h1 {
          font-weight: normal;
          }
          th {
          background: #035387;
          text-align: left;
          color: white;
          }
          td, th {
          border: 1px solid black;
          <!-- possible soln to page break problem: get rid of this border -->
          }
          .AlternateRow {
          background: #DDEBF7;
          }

          .CheckboxColumn {
          width: 1.5em;
          }
          .TaskIDColumn {
          width: 8em;
          }
          .SubjectColumn {
          width: 50em;
          }
          .LoanNumberColumn {
          width: 8em;
          }
          .DateColumn {
          width: 8em;
          }
          .WarningDate {
          width: 8em;
          color: red;
          }

        </style>
      </head>
      <body>
        <tr>
          <td>
            <div align="left">
              <xsl:choose>
                <xsl:when test="$LogoSource != ''">
                  <img>
                    <xsl:attribute name="id">pllogo</xsl:attribute>
                    <xsl:attribute name="src">
                      <xsl:value-of select="$LogoSource"/>
                    </xsl:attribute>
                  </img>
                </xsl:when>
                <xsl:otherwise>
                  <h1>
                    <xsl:value-of select="@BrokerName" />
                  </h1>
                </xsl:otherwise>
              </xsl:choose>
            </div>
          </td>
        </tr>
        <h1>
          <xsl:value-of select="@Heading"></xsl:value-of> List
        </h1>
        <table id="TaskListTable">
          <tr>
            <th class="CheckboxColumn">
              <xsl:comment>Checkbox goes here</xsl:comment>
            </th>
            <th class="TaskIDColumn">
              <xsl:value-of select="@Heading"/>
            </th>
            <xsl:if test="@Heading = 'Condition'">
              <th>  Category</th>
            </xsl:if>
            <th class="SubjectColumn">Subject</th>
            <th class="LoanNumberColumn">Loan Number</th>
            <th class="DateColumn">Due Date</th>
            <th class="DateColumn">Follow-up Date</th>
          </tr>

          <xsl:apply-templates select="Task" />
        </table>

      </body>
    </html>
  </xsl:template>

  <xsl:template match="Task">
    <tr style="page-break-inside: avoid;">
      <xsl:if test="position() mod 2 = 0">
        <xsl:attribute name="class">AlternateRow</xsl:attribute>
      </xsl:if>
      <td class="CheckboxColumn">
        <input type="checkbox"></input>
      </td>
      <td class="TaskIDColumn">
        <xsl:value-of select="TaskID"/>
      </td>
      <xsl:if test="Category">
        <td>
          <xsl:value-of select="Category"/>
        </td>
      </xsl:if>
      <td class="SubjectColumn">
        <xsl:value-of select="Subject"/>
      </td>
      <td class="LoanNumberColumn">
        <xsl:value-of select="LoanNumber"/>
      </td>
      <td class="DateColumn">
        <xsl:if test="isStyleDueDate = 1">
          <xsl:attribute name="class">WarningDate</xsl:attribute>
        </xsl:if>
        <xsl:value-of select="DueDate"/>
      </td>
      <td class="DateColumn">
        <xsl:if test="isStyleFollowupDate = 1">
          <xsl:attribute name="class">WarningDate</xsl:attribute>
        </xsl:if>
        <xsl:value-of select="FollowupDate"/>
      </td>
    </tr>

  </xsl:template>

</xsl:stylesheet>
