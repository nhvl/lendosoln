using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CVA_26_1820_1PDF : AbstractLegalPDF
    {
        public override string PdfFile 
        {
            get { return "VA-26-1820_1.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 1"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            string str = null;

            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aBAddrFull", Tools.FormatAddress(dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip));
            AddFormFieldData("aBSsn",                                         dataApp.aBSsn);
            AddFormFieldData("aVaVestTitleODesc",                             dataApp.aVaVestTitleODesc);
            AddFormFieldData("aVaVestTitleT",                                 dataApp.aVaVestTitleT);
            AddFormFieldData("sAgencyCaseNum",                                dataLoan.sAgencyCaseNum);
            AddFormFieldData("sClosedD",                                      dataLoan.sClosedD_rep);
            AddFormFieldData("sVALenderIdCode",                              dataLoan.sVALenderIdCode);

            // 4/22/2004 dd - Format for Address of Relative
            // Line 1: {Name}
            // Line 2: {Addr}, {City} {State} {Zip} / Ph: {Phone}
            str = dataLoan.sFHAPropImprovBorrRelativeNm;
            if (str.TrimWhitespaceAndBOM() != "") 
            {
                str += Environment.NewLine;
            }
            string[] lines = dataLoan.sFHAPropImprovBorrRelativeAddr.Split(new char[] {'\n','\r'});
            // 4/22/2004 dd - Convert new line to comma.
            // Assume user type address in 2 lines.
            for (int i = 0; i < lines.Length; i++) 
            {
                if (lines[i].TrimWhitespaceAndBOM() != "") 
                {
                    str += lines[i];
                    if (i == 0) str += ", ";
                }
            }

            if (dataLoan.sFHAPropImprovBorrRelativePhone.TrimWhitespaceAndBOM() != "")
                str += " / Ph: " + dataLoan.sFHAPropImprovBorrRelativePhone;

            AddFormFieldData("sFHAPropImprovBorrRelativeAddr",                str);
            AddFormFieldData("sFHAPurposeIsConstructHome",                    dataLoan.sFHAPurposeIsConstructHome);
            AddFormFieldData("sFHAPurposeIsFinanceCoopPurchase",              dataLoan.sFHAPurposeIsFinanceCoopPurchase);
            AddFormFieldData("sFHAPurposeIsFinanceImprovement",               dataLoan.sFHAPurposeIsFinanceImprovement);
            AddFormFieldData("sFHAPurposeIsManufacturedHomeAndLot",           dataLoan.sFHAPurposeIsManufacturedHomeAndLot);
            AddFormFieldData("sFHAPurposeIsPurchaseExistCondo",               dataLoan.sFHAPurposeIsPurchaseExistCondo);
            AddFormFieldData("sFHAPurposeIsPurchaseExistHome",                dataLoan.sFHAPurposeIsPurchaseExistHome);
            AddFormFieldData("sFHAPurposeIsPurchaseManufacturedHome",         dataLoan.sFHAPurposeIsPurchaseManufacturedHome);
            AddFormFieldData("sFHAPurposeIsPurchaseNewCondo",                 dataLoan.sFHAPurposeIsPurchaseNewCondo);
            AddFormFieldData("sFHAPurposeIsPurchaseNewHome",                  dataLoan.sFHAPurposeIsPurchaseNewHome);
            AddFormFieldData("sFHAPurposeIsRefiManufacturedHomeOrLotLoan",    dataLoan.sFHAPurposeIsRefiManufacturedHomeOrLotLoan);
            AddFormFieldData("sFHAPurposeIsRefiManufacturedHomeToBuyLot",     dataLoan.sFHAPurposeIsRefiManufacturedHomeToBuyLot);
            AddFormFieldData("sFHAPurposeIsRefinance",                        dataLoan.sFHAPurposeIsRefinance);
            AddFormFieldData("sFinalLAmt",                                    dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sIsAlterationCompleted",                        dataLoan.sIsAlterationCompleted);
            AddFormFieldData("sLNoteD",                                       dataLoan.sLNoteD_rep);
            AddFormFieldData("sLProceedPaidOutD",                             dataLoan.sLProceedPaidOutD_rep);
            AddFormFieldData("sLeaseHoldExpireD",                             dataLoan.sLeaseHoldExpireD_rep);
            AddFormFieldData("sLotAcquiredD",                                 dataLoan.sLotAcquiredD_rep);
            AddFormFieldData("sMaturityD",                                    dataLoan.sMaturityD_rep);
            AddFormFieldData("sMaturityDLckd",                                dataLoan.sMaturityDLckd);
            AddFormFieldData("sNoteIR",                                       dataLoan.sNoteIR_rep);
            AddFormFieldData("sProFloodInsFaceAmt",                           dataLoan.sProFloodInsFaceAmt_rep);
            AddFormFieldData("sProFloodInsPerYr",                             dataLoan.sProFloodInsPerYr_rep);
            AddFormFieldData("sProFloodInsPerYrLckd",                         dataLoan.sProFloodInsPerYrLckd);
            AddFormFieldData("sProHazInsFaceAmt",                             dataLoan.sProHazInsFaceAmt_rep);
            AddFormFieldData("sProHazInsPerYr",                               dataLoan.sProHazInsPerYr_rep);
            AddFormFieldData("sProHazInsPerYrLckd",                           dataLoan.sProHazInsPerYrLckd);
            AddFormFieldData("sProRealETxPerYr",                              dataLoan.sProRealETxPerYr_rep);
            AddFormFieldData("sProRealETxPerYrLckd",                          dataLoan.sProRealETxPerYrLckd);
            AddFormFieldData("sProThisMPmt",                                  dataLoan.sProThisMPmt_rep);
            AddFormFieldData("sSchedDueD1",                                   dataLoan.sSchedDueD1_rep);
            str = OneLineAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip);
            if (str.TrimWhitespaceAndBOM() != "")
                str += Environment.NewLine;
            str += dataLoan.sSpLegalDesc;

            AddFormFieldData("sSpAddrFull", str);
            AddFormFieldData("sTerm",                                         dataLoan.sTerm_rep);
            if (!dataLoan.sVaAdditionalSecurityTakenDescPrintSeparately) 
            {
                AddFormFieldData("sVaAdditionalSecurityTakenDesc",                dataLoan.sVaAdditionalSecurityTakenDesc);
            } 
            else 
            {
                AddFormFieldData("sVaAdditionalSecurityTakenDesc", "See Attachment");
            }

            AddFormFieldData("sVaAgent1RoleDesc",                             dataLoan.sVaAgent1RoleDesc);
            AddFormFieldData("sVaAgent2RoleDesc",                             dataLoan.sVaAgent2RoleDesc);
            AddFormFieldData("sVaAgent3RoleDesc",                             dataLoan.sVaAgent3RoleDesc);
            AddFormFieldData("sVaAgent4RoleDesc",                             dataLoan.sVaAgent4RoleDesc);
            AddFormFieldData("sVaAgent5RoleDesc",                             dataLoan.sVaAgent5RoleDesc);
            AddFormFieldData("sVaEstateHeldOtherDesc",                        dataLoan.sVaEstateHeldOtherDesc);
            AddFormFieldData("sVaEstateHeldT",                                dataLoan.sVaEstateHeldT);
            AddFormFieldData("sVaEstateHeldTLckd",                            dataLoan.sVaEstateHeldTLckd);
            AddFormFieldData("sVaIsAutoProc",                                 dataLoan.sVaIsAutoProc); 
            AddFormFieldData("sVaIsGuarantyEvidenceRequested",                dataLoan.sVaIsGuarantyEvidenceRequested); 
            AddFormFieldData("sVaIsInsuranceEvidenceRequested",               dataLoan.sVaIsInsuranceEvidenceRequested); 
            AddFormFieldData("sVaIsPriorApprovalProc",                        dataLoan.sVaIsPriorApprovalProc); 
            AddFormFieldData("sVaLProceedDepositDesc",                        dataLoan.sVaLProceedDepositDesc);
            AddFormFieldData("sVaLProceedDepositT",                           dataLoan.sVaLProceedDepositT);
            AddFormFieldData("sVaLenderCaseNum",                              dataLoan.sVaLenderCaseNum);
            AddFormFieldData("sVaLenderCaseNumLckd",                          dataLoan.sVaLenderCaseNumLckd);
            AddFormFieldData("sVaLienPosOtherDesc",                           dataLoan.sVaLienPosOtherDesc);
            AddFormFieldData("sVaLienPosT",                                   dataLoan.sVaLienPosT);
            AddFormFieldData("sVaLienPosTLckd",                               dataLoan.sVaLienPosTLckd);
            AddFormFieldData("sVaLotPurchPrice",                              dataLoan.sVaLotPurchPrice_rep);
            AddFormFieldData("sVaMaintainAssessPmtPerYear",                   dataLoan.sVaMaintainAssessPmtPerYear_rep);
            AddFormFieldData("sVaMaintainAssessPmtPerYearLckd",               dataLoan.sVaMaintainAssessPmtPerYearLckd);
            if (!dataLoan.sVaNonrealtyAcquiredWithLDescPrintSeparately) 
            {
                AddFormFieldData("sVaNonrealtyAcquiredWithLDesc",                 dataLoan.sVaNonrealtyAcquiredWithLDesc);
            } 
            else 
            {
                AddFormFieldData("sVaNonrealtyAcquiredWithLDesc", "See Attachment");
            }

            AddFormFieldData("sVaSpecialAssessPmtPerYear",                    dataLoan.sVaSpecialAssessPmtPerYear_rep);
            AddFormFieldData("sVaSpecialAssessUnpaid",                        dataLoan.sVaSpecialAssessUnpaid_rep);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("VA26_1820PrepareDate", preparer.PrepareDate_rep);

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Agent1, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("VA26_1820Agent1PreparerName", preparer.PreparerName);
            AddFormFieldData("VA26_1820Agent1Address", OneLineAddress(preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Agent2, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("VA26_1820Agent2PreparerName", preparer.PreparerName);
            AddFormFieldData("VA26_1820Agent2Address", OneLineAddress(preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Agent3, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("VA26_1820Agent3PreparerName", preparer.PreparerName);
            AddFormFieldData("VA26_1820Agent3Address", OneLineAddress(preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Agent4, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("VA26_1820Agent4PreparerName", preparer.PreparerName);
            AddFormFieldData("VA26_1820Agent4Address", OneLineAddress(preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));

            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Agent5, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("VA26_1820Agent5PreparerName", preparer.PreparerName);
            AddFormFieldData("VA26_1820Agent5Address", OneLineAddress(preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));

            AddFormFieldData("aBHispanicT", dataApp.aBHispanicTFallback);
            AddFormFieldData("aBIsAmericanIndian", dataApp.aBIsAmericanIndian);
            AddFormFieldData("aBIsPacificIslander", dataApp.aBIsPacificIslander);
            AddFormFieldData("aBIsAsian", dataApp.aBIsAsian);
            AddFormFieldData("aBIsWhite", dataApp.aBIsWhite);
            AddFormFieldData("aBIsBlack", dataApp.aBIsBlack);
            AddFormFieldData("aCHispanicT", dataApp.aCHispanicTFallback);
            AddFormFieldData("aCIsAmericanIndian", dataApp.aCIsAmericanIndian);
            AddFormFieldData("aCIsPacificIslander", dataApp.aCIsPacificIslander);
            AddFormFieldData("aCIsAsian", dataApp.aCIsAsian);
            AddFormFieldData("aCIsWhite", dataApp.aCIsWhite);
            AddFormFieldData("aCIsBlack", dataApp.aCIsBlack);
            AddFormFieldData("aBGender", dataApp.aBGenderFallback);
            AddFormFieldData("aCRaceT", dataApp.aCRaceT);
        }

        private string OneLineAddress(string addr, string city, string state, string zip) 
        {
            string ret = null;
            string csz = Tools.CombineCityStateZip(city, state, zip);
            if (addr == null || addr.TrimWhitespaceAndBOM() == "")
                ret = csz;
            else
                ret = addr + ", " + csz;

            return ret;
        }
    }

    public class CVA_26_1820_2PDF : AbstractLegalPDF
    {
        public override string PdfFile 
        {
            get { return "VA-26-1820_2.pdf"; }
        }
        public override string Description 
        {
            get { return "Page 2"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("aFHABorrCertInformedPropValNotAwareAtContractSigning", dataApp.aFHABorrCertInformedPropValNotAwareAtContractSigning);
            AddFormFieldData("aFHABorrCertInformedPropValAwareAtContractSigning", dataApp.aFHABorrCertInformedPropValAwareAtContractSigning);
            AddFormFieldData("aFHABorrCertInformedPropVal", dataApp.aFHABorrCertInformedPropVal_rep);
            AddFormFieldData("aVaNotDischargedOrReleasedSinceCertOfEligibility", dataApp.aVaNotDischargedOrReleasedSinceCertOfEligibility);
            
            AddFormFieldData("aFHABorrCertOccIsAsHome", dataApp.aFHABorrCertOccIsAsHome);
            AddFormFieldData("aFHABorrCertOccIsAsHomeForActiveSpouse", dataApp.aFHABorrCertOccIsAsHomeForActiveSpouse);
            AddFormFieldData("aFHABorrCertOccIsChildAsHome", dataApp.aFHABorrCertOccIsChildAsHome);
            AddFormFieldData("aFHABorrCertOccIsAsHomePrev", dataApp.aFHABorrCertOccIsAsHomePrev);
            AddFormFieldData("aFHABorrCertOccIsAsHomePrevForActiveSpouse", dataApp.aFHABorrCertOccIsAsHomePrevForActiveSpouse);
            AddFormFieldData("aFHABorrCertOccIsChildAsHomePrev", dataApp.aFHABorrCertOccIsChildAsHomePrev);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_1820Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("VA26_1820LenderAddrFull", Tools.FormatAddress(preparer.CompanyName, preparer.StreetAddr, preparer.City, preparer.State, preparer.Zip));
            AddFormFieldData("VA26_1820LenderPhoneOfCompany", preparer.PhoneOfCompany);
            AddFormFieldData("aBHispanicT", dataApp.aBHispanicTFallback);
            AddFormFieldData("aBIsAmericanIndian", dataApp.aBIsAmericanIndian);
            AddFormFieldData("aBIsPacificIslander", dataApp.aBIsPacificIslander);
            AddFormFieldData("aBIsAsian", dataApp.aBIsAsian);
            AddFormFieldData("aBIsWhite", dataApp.aBIsWhite);
            AddFormFieldData("aBIsBlack", dataApp.aBIsBlack);
            AddFormFieldData("aCHispanicT", dataApp.aCHispanicTFallback);
            AddFormFieldData("aCIsAmericanIndian", dataApp.aCIsAmericanIndian);
            AddFormFieldData("aCIsPacificIslander", dataApp.aCIsPacificIslander);
            AddFormFieldData("aCIsAsian", dataApp.aCIsAsian);
            AddFormFieldData("aCIsWhite", dataApp.aCIsWhite);
            AddFormFieldData("aCIsBlack", dataApp.aCIsBlack);
            AddFormFieldData("aBGender", dataApp.aBGenderFallback);
            AddFormFieldData("aCGender", dataApp.aCGenderFallback);
        }
    }

    public class CVA_26_1820_ContPDF : AbstractLegalPDF
    {
        public override string PdfFile 
        {
            get { return "VA-26-1820_Cont.pdf";        	 }
        }
        public override string Description 
        {
            get { return "Attachment"; }
        }
        public override bool IsVisible 
        {
            get 
            {
                return DataLoan.sVaNonrealtyAcquiredWithLDescPrintSeparately || DataLoan.sVaAdditionalSecurityTakenDescPrintSeparately;
            }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            if (dataLoan.sVaNonrealtyAcquiredWithLDescPrintSeparately)
                AddFormFieldData("sVaNonrealtyAcquiredWithLDesc", dataLoan.sVaNonrealtyAcquiredWithLDesc);

            if (dataLoan.sVaAdditionalSecurityTakenDescPrintSeparately)
                AddFormFieldData("sVaAdditionalSecurityTakenDesc", dataLoan.sVaAdditionalSecurityTakenDesc);
        }
    }

    public class CVA_26_1820PDF : AbstractBatchPDF 
    {
        public override string Description 
        {
            get { return "VA Report and Certification of Loan Disbursement (VA 26-1820)"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/VA/VACertificateLoanDisbursement.aspx"; }
        }

        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            { 
                return new AbstractPDFFile[] {
                                                 new CVA_26_1820_1PDF(),
                                                 new CVA_26_1820_2PDF(),
                                                 new CVA_26_1820_ContPDF()
                                             };
            }
        }
    }
}
