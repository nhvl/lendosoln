using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CVA_26_0503PDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "VA-26-0503.pdf";        	 }
        }
        public override string Description 
        {
            get { return "VA Federal Collection Policy Notice (VA 26-0503)"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("aBNm", dataApp.aBNm);
        }
	}
}
