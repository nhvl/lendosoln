namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Specialized;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Common;

    public class CDisclosureNoticesPDF : AbstractLetterPDF
	{

        public override string PdfFile 
        {
            get { return "DisclosureNotices.pdf"; }
        }
        public override string Description 
        {
            get { return "Disclosure Notices"; }
        }

        protected override  void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("PropertyAddress", Tools.FormatAddress(null, dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));

            AddFormFieldData("aOccT", dataApp.aOccT);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("Applicants", dataApp.aBNm + Environment.NewLine + dataApp.aCNm);

        }


	}
}
