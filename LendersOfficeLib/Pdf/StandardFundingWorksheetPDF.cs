﻿using System;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using System.Threading;
using LendersOffice.ObjLib.Rolodex;

namespace LendersOffice.Pdf
{
    public class CStandardFundingWorksheetPDF : AbstractLegalPDF
    {
        public override string PdfFile
        {
            get { return "StandardFundingWorksheet.pdf"; }
        }
        public override bool HasPrintPermission
        {
            get
            {
                AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;

                return base.HasPrintPermission && principal.HasPermission(Permission.AllowCloserRead);
            }
        }
        public override string EditLink
        {
            get { return "/newlos/Closer/Funding.aspx"; }
        }

        public override string Description
        {
            get { return "Funding Worksheet"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("BrokerName", BrokerUserPrincipal.CurrentPrincipal.BrokerName);
            AddFormFieldData("sSettlementChargesDedFromLoanProc", dataLoan.sSettlementChargesDedFromLoanProc_rep);
            AddFormFieldData("sSettlementTotalFundByLenderAtClosing", dataLoan.sSettlementTotalFundByLenderAtClosing_rep);
            AddFormFieldData("sSettlementFloodInsRsrv", dataLoan.sSettlementFloodInsRsrv_rep);
            AddFormFieldData("s1007ProHExpDesc", dataLoan.s1007ProHExpDesc);
            AddFormFieldData("sSettlementHazInsRsrv", dataLoan.sSettlementHazInsRsrv_rep);
            AddFormFieldData("sSettlementU3ScDesc", dataLoan.sSettlementU3ScDesc);
            AddFormFieldData("sSettlement1009RsrvMon", dataLoan.sSettlement1009RsrvMon_rep);
            AddFormFieldData("sSettlementU4Tc", dataLoan.sSettlementU4Tc_rep);
            AddFormFieldData("sSettlement800U3F", dataLoan.sSettlement800U3F_rep);
            AddFormFieldData("sSettlementStateRtc", dataLoan.sSettlementStateRtc_rep);
            AddFormFieldData("sSettlementPestInspectF", dataLoan.sSettlementPestInspectF_rep);
            AddFormFieldData("sSettlementCrF", dataLoan.sSettlementCrF_rep);
            AddFormFieldData("sSettlementLOrigF", dataLoan.sSettlementLOrigF_rep);
            AddFormFieldData("sSettlementFloodCertificationF", dataLoan.sSettlementFloodCertificationF_rep);
            AddFormFieldData("sSettlementU5Sc", dataLoan.sSettlementU5Sc_rep);
            AddFormFieldData("sSettlementU1Sc", dataLoan.sSettlementU1Sc_rep);
            AddFormFieldData("sSettlementUwF", dataLoan.sSettlementUwF_rep);
            AddFormFieldData("s1006ProHExpDesc", dataLoan.s1006ProHExpDesc);
            AddFormFieldData("sProRealETxMb", dataLoan.sProRealETxMb_rep);
            AddFormFieldData("sSettlement904Pia", dataLoan.sSettlement904Pia_rep);
            AddFormFieldData("sTrackingN", dataLoan.sTrackingN);
            AddFormFieldData("sSettlement800U1FDesc", dataLoan.sSettlement800U1FDesc);
            AddFormFieldData("sSettlementU2ScDesc", dataLoan.sSettlementU2ScDesc);
            AddFormFieldData("sSettlementProcF", dataLoan.sSettlementProcF_rep);
            AddFormFieldData("sTotalAmtFund", dataLoan.sTotalAmtFund_rep);
            AddFormFieldData("sSettlementHazInsRsrvMon", dataLoan.sSettlementHazInsRsrvMon_rep);
            AddFormFieldData("sSettlementSchoolTxRsrv", dataLoan.sSettlementSchoolTxRsrv_rep);
            AddFormFieldData("sSettlementRealETxRsrv", dataLoan.sSettlementRealETxRsrv_rep);
            AddFormFieldData("sSettlementMInsRsrvMon", dataLoan.sSettlementMInsRsrvMon_rep);
            AddFormFieldData("sSettlementU3GovRtc", dataLoan.sSettlementU3GovRtc_rep);
            AddFormFieldData("sSettlement904PiaDesc", dataLoan.sSettlement904PiaDesc);
            AddFormFieldData("sSettlementU1ScDesc", dataLoan.sSettlementU1ScDesc);
            AddFormFieldData("sSettlementEscrowF", dataLoan.sSettlementEscrowF_rep);
            AddFormFieldData("sAmtReqToFund", dataLoan.sAmtReqToFund_rep);
            AddFormFieldData("sSettlement800U4F", dataLoan.sSettlement800U4F_rep);
            AddFormFieldData("sSettlementDocPrepF", dataLoan.sSettlementDocPrepF_rep);
            AddFormFieldData("sSettlementMBrokF", dataLoan.sSettlementMBrokF_rep);
            AddFormFieldData("sSettlementU3TcDesc", dataLoan.sSettlementU3TcDesc);
            AddFormFieldData("sSettlementU2GovRtcDesc", dataLoan.sSettlementU2GovRtcDesc);
            AddFormFieldData("sSettlementU1GovRtc", dataLoan.sSettlementU1GovRtc_rep);
            AddFormFieldData("sSettlementU2GovRtc", dataLoan.sSettlementU2GovRtc_rep);
            AddFormFieldData("sSettlementApprF", dataLoan.sSettlementApprF_rep);
            AddFormFieldData("sSettlementIPerDay", dataLoan.sSettlementIPerDay_rep);
            AddFormFieldData("sSettlement800U3FDesc", dataLoan.sSettlement800U3FDesc);
            AddFormFieldData("sChkDueFromClosing", dataLoan.sChkDueFromClosing_rep);
            AddFormFieldData("sSettlement800U4FDesc", dataLoan.sSettlement800U4FDesc);
            AddFormFieldData("sSettlementIPiaDy", dataLoan.sSettlementIPiaDy_rep);
            AddFormFieldData("sSettlementIPia", dataLoan.sSettlementIPia_rep);
            AddFormFieldData("sSettlementU1Tc", dataLoan.sSettlementU1Tc_rep);
            AddFormFieldData("sSettlementTxServF", dataLoan.sSettlementTxServF_rep);
            AddFormFieldData("sSettlementU2Tc", dataLoan.sSettlementU2Tc_rep);
            AddFormFieldData("sSettlementAttorneyF", dataLoan.sSettlementAttorneyF_rep);
            AddFormFieldData("sSettlementCountyRtc", dataLoan.sSettlementCountyRtc_rep);
            AddFormFieldData("sAmtFundFromWarehouseLine", dataLoan.sAmtFundFromWarehouseLine_rep);
            AddFormFieldData("sProSchoolTx", dataLoan.sProSchoolTx_rep);
            AddFormFieldData("s1007ProHExp", dataLoan.s1007ProHExp_rep);
            AddFormFieldData("sSettlementU3Sc", dataLoan.sSettlementU3Sc_rep);
            AddFormFieldData("sProMIns", dataLoan.sProMIns_rep);
            AddFormFieldData("sSettlementU1TcDesc", dataLoan.sSettlementU1TcDesc);
            AddFormFieldData("sProFloodIns", dataLoan.sProFloodIns_rep);
            AddFormFieldData("sSettlementInspectF", dataLoan.sSettlementInspectF_rep);
            AddFormFieldData("sSettlement800U1F", dataLoan.sSettlement800U1F_rep);
            AddFormFieldData("sSettlementU1GovRtcDesc", dataLoan.sSettlementU1GovRtcDesc);
            AddFormFieldData("sWarehouseMaxFundAmt", dataLoan.sWarehouseMaxFundAmt_rep);
            AddFormFieldData("sSettlementU2TcDesc", dataLoan.sSettlementU2TcDesc);
            AddFormFieldData("sSettlementNotaryF", dataLoan.sSettlementNotaryF_rep);
            AddFormFieldData("sMipPia", dataLoan.sMipPia_rep);
            AddFormFieldData("sFundD", dataLoan.sFundD_rep);
            AddFormFieldData("sSettlementOwnerTitleInsF", dataLoan.sSettlementOwnerTitleInsF_rep);
            AddFormFieldData("sSettlementMInsRsrv", dataLoan.sSettlementMInsRsrv_rep);
            AddFormFieldData("sSettlementBrokerComp", dataLoan.sGfeOriginatorCompF_rep); // case 72384
            AddFormFieldData("sSettlement1008Rsrv", dataLoan.sSettlement1008Rsrv_rep);
            AddFormFieldData("sSettlementWireF", dataLoan.sSettlementWireF_rep);
            AddFormFieldData("sSettlementU5ScDesc", dataLoan.sSettlementU5ScDesc);
            AddFormFieldData("sSettlementHazInsPia", dataLoan.sSettlementHazInsPia_rep);
            AddFormFieldData("sProHazIns", dataLoan.sProHazIns_rep);
            AddFormFieldData("sSettlementFloodInsRsrvMon", dataLoan.sSettlementFloodInsRsrvMon_rep);
            AddFormFieldData("sSettlementU4ScDesc", dataLoan.sSettlementU4ScDesc);
            AddFormFieldData("sSettlementLDiscnt", dataLoan.sSettlementLDiscnt_rep);
            AddFormFieldData("sSettlement800U2FDesc", dataLoan.sSettlement800U2FDesc);
            AddFormFieldData("sSettlementSchoolTxRsrvMon", dataLoan.sSettlementSchoolTxRsrvMon_rep);
            AddFormFieldData("sSettlement800U5FDesc", dataLoan.sSettlement800U5FDesc);
            AddFormFieldData("sSettlementTitleInsF", dataLoan.sSettlementTitleInsF_rep);
            AddFormFieldData("sSettlement800U2F", dataLoan.sSettlement800U2F_rep);
            AddFormFieldData("sWarehouseMaxFundPc", dataLoan.sWarehouseMaxFundPc_rep);
            AddFormFieldData("sSettlement1008RsrvMon", dataLoan.sSettlement1008RsrvMon_rep);
            AddFormFieldData("sSettlementU4Sc", dataLoan.sSettlementU4Sc_rep);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sSettlementU2Sc", dataLoan.sSettlementU2Sc_rep);
            AddFormFieldData("sOtherFundAdj", dataLoan.sOtherFundAdj_rep);
            string warehouseLenderLabel = "";
            int lenderId = dataLoan.sWarehouseLenderRolodexId;
            if (lenderId >= 0)
            {
                var warehouseLender = WarehouseLenderRolodexEntry.Get(BrokerUserPrincipal.CurrentPrincipal.BrokerId, lenderId);
                warehouseLenderLabel = warehouseLender.WarehouseLenderName;
            }
            else if (lenderId == -2) // "Other" selected
            {
                warehouseLenderLabel = dataLoan.sWarehouseFunderDesc;
            }
            AddFormFieldData("sWarehouseFunderDesc", warehouseLenderLabel);
            AddFormFieldData("sSettlementRecF", dataLoan.sSettlementRecF_rep);
            AddFormFieldData("sSettlementTotalEstimateSettlementCharge", dataLoan.sSettlementTotalEstimateSettlementCharge_rep);
            AddFormFieldData("s1006ProHExp", dataLoan.s1006ProHExp_rep);
            AddFormFieldData("sSettlement800U5F", dataLoan.sSettlement800U5F_rep);
            AddFormFieldData("sSettlement1009Rsrv", dataLoan.sSettlement1009Rsrv_rep);
            AddFormFieldData("sSettlementRealETxRsrvMon", dataLoan.sSettlementRealETxRsrvMon_rep);
            AddFormFieldData("sVaFf", dataLoan.sVaFf_rep);
            AddFormFieldData("sFundReqForShortfall", dataLoan.sFundReqForShortfall_rep);


            AddFormFieldData("sSettlementLOrigFPaidToLender", dataLoan.sSettlementLOrigFPaidToLender_rep);
            AddFormFieldData("sSettlementLDiscntPaidToLender", dataLoan.sSettlementLDiscntPaidToLender_rep);
            AddFormFieldData("sSettlementApprFPaidToLender", dataLoan.sSettlementApprFPaidToLender_rep);
            AddFormFieldData("sSettlementCrFPaidToLender", dataLoan.sSettlementCrFPaidToLender_rep);
            AddFormFieldData("sSettlementTxServFPaidToLender", dataLoan.sSettlementTxServFPaidToLender_rep);
            AddFormFieldData("sSettlementFloodCertificationFPaidToLender", dataLoan.sSettlementFloodCertificationFPaidToLender_rep);
            AddFormFieldData("sSettlementMBrokFPaidToLender", dataLoan.sSettlementMBrokFPaidToLender_rep);
            AddFormFieldData("sSettlementInspectFPaidToLender", dataLoan.sSettlementInspectFPaidToLender_rep);
            AddFormFieldData("sSettlementProcFPaidToLender", dataLoan.sSettlementProcFPaidToLender_rep);
            AddFormFieldData("sSettlementUwFPaidToLender", dataLoan.sSettlementUwFPaidToLender_rep);
            AddFormFieldData("sSettlementWireFPaidToLender", dataLoan.sSettlementWireFPaidToLender_rep);
            AddFormFieldData("sSettlement800U1FPaidToLender", dataLoan.sSettlement800U1FPaidToLender_rep);
            AddFormFieldData("sSettlement800U2FPaidToLender", dataLoan.sSettlement800U2FPaidToLender_rep);
            AddFormFieldData("sSettlement800U3FPaidToLender", dataLoan.sSettlement800U3FPaidToLender_rep);
            AddFormFieldData("sSettlement800U4FPaidToLender", dataLoan.sSettlement800U4FPaidToLender_rep);
            AddFormFieldData("sSettlement800U5FPaidToLender", dataLoan.sSettlement800U5FPaidToLender_rep);
            AddFormFieldData("sSettlementIPiaPaidToLender", dataLoan.sSettlementIPiaPaidToLender_rep);
            AddFormFieldData("sMipPiaPaidToLender", dataLoan.sMipPiaPaidToLender_rep);
            AddFormFieldData("sSettlementHazInsPiaPaidToLender", dataLoan.sSettlementHazInsPiaPaidToLender_rep);
            AddFormFieldData("sSettlement904PiaPaidToLender", dataLoan.sSettlement904PiaPaidToLender_rep);
            AddFormFieldData("sVaFfPaidToLender", dataLoan.sVaFfPaidToLender_rep);
            AddFormFieldData("sSettlement900U1PiaPaidToLender", dataLoan.sSettlement900U1PiaPaidToLender_rep);
            AddFormFieldData("sSettlementHazInsRsrvPaidToLender", dataLoan.sSettlementHazInsRsrvPaidToLender_rep);
            AddFormFieldData("sSettlementMInsRsrvPaidToLender", dataLoan.sSettlementMInsRsrvPaidToLender_rep);
            AddFormFieldData("sSettlementRealETxRsrvPaidToLender", dataLoan.sSettlementRealETxRsrvPaidToLender_rep);
            AddFormFieldData("sSettlementSchoolTxRsrvPaidToLender", dataLoan.sSettlementSchoolTxRsrvPaidToLender_rep);
            AddFormFieldData("sSettlementFloodInsRsrvPaidToLender", dataLoan.sSettlementFloodInsRsrvPaidToLender_rep);
            AddFormFieldData("sSettlementAggregateAdjRsrv", dataLoan.sSettlementAggregateAdjRsrv_rep);
            AddFormFieldData("sSettlementAggregateAdjRsrvPaidToLender", dataLoan.sSettlementAggregateAdjRsrvPaidToLender_rep);
            AddFormFieldData("sSettlement1008RsrvPaidToLender", dataLoan.sSettlement1008RsrvPaidToLender_rep);
            AddFormFieldData("sSettlement1009RsrvPaidToLender", dataLoan.sSettlement1009RsrvPaidToLender_rep);
            AddFormFieldData("sSettlementEscrowFPaidToLender", dataLoan.sSettlementEscrowFPaidToLender_rep);
            AddFormFieldData("sSettlementOwnerTitleInsFPaidToLender", dataLoan.sSettlementOwnerTitleInsFPaidToLender_rep);
            AddFormFieldData("sSettlementTitleInsFPaidToLender", dataLoan.sSettlementTitleInsFPaidToLender_rep);
            AddFormFieldData("sSettlementDocPrepFPaidToLender", dataLoan.sSettlementDocPrepFPaidToLender_rep);
            AddFormFieldData("sSettlementNotaryFPaidToLender", dataLoan.sSettlementNotaryFPaidToLender_rep);
            AddFormFieldData("sSettlementAttorneyFPaidToLender", dataLoan.sSettlementAttorneyFPaidToLender_rep);
            AddFormFieldData("sSettlementU1TcPaidToLender", dataLoan.sSettlementU1TcPaidToLender_rep);
            AddFormFieldData("sSettlementU2TcPaidToLender", dataLoan.sSettlementU2TcPaidToLender_rep);
            AddFormFieldData("sSettlementU3TcPaidToLender", dataLoan.sSettlementU3TcPaidToLender_rep);
            AddFormFieldData("sSettlementU4TcPaidToLender", dataLoan.sSettlementU4TcPaidToLender_rep);
            AddFormFieldData("sSettlementRecFPaidToLender", dataLoan.sSettlementRecFPaidToLender_rep);
            AddFormFieldData("sSettlementCountyRtcPaidToLender", dataLoan.sSettlementCountyRtcPaidToLender_rep);
            AddFormFieldData("sSettlementStateRtcPaidToLender", dataLoan.sSettlementStateRtcPaidToLender_rep);
            AddFormFieldData("sSettlementU1GovRtcPaidToLender", dataLoan.sSettlementU1GovRtcPaidToLender_rep);
            AddFormFieldData("sSettlementU2GovRtcPaidToLender", dataLoan.sSettlementU2GovRtcPaidToLender_rep);
            AddFormFieldData("sSettlementU3GovRtcPaidToLender", dataLoan.sSettlementU3GovRtcPaidToLender_rep);
            AddFormFieldData("sSettlementPestInspectFPaidToLender", dataLoan.sSettlementPestInspectFPaidToLender_rep);
            AddFormFieldData("sSettlementU1ScPaidToLender", dataLoan.sSettlementU1ScPaidToLender_rep);
            AddFormFieldData("sSettlementU2ScPaidToLender", dataLoan.sSettlementU2ScPaidToLender_rep);
            AddFormFieldData("sSettlementU3ScPaidToLender", dataLoan.sSettlementU3ScPaidToLender_rep);
            AddFormFieldData("sSettlementU4ScPaidToLender", dataLoan.sSettlementU4ScPaidToLender_rep);
            AddFormFieldData("sSettlementU5ScPaidToLender", dataLoan.sSettlementU5ScPaidToLender_rep);
            AddFormFieldData("sSettlementLenderCreditFPaidToLender", dataLoan.sSettlementLenderCreditFPaidToLender_rep);
            AddFormFieldData("sSettlementDiscountPointFPaidToLender", dataLoan.sSettlementDiscountPointFPaidToLender_rep);
            AddFormFieldData("sSettlementCreditLenderPaidItemFPaidToLender", dataLoan.sSettlementCreditLenderPaidItemFPaidToLender_rep);

            AddFormFieldData("sSettlementLOrigFPaidByLender", dataLoan.sSettlementLOrigFPaidByLender_rep);
            AddFormFieldData("sSettlementLDiscntPaidByLender", dataLoan.sSettlementLDiscntPaidByLender_rep);
            AddFormFieldData("sSettlementBrokerCompPaidByLender", dataLoan.sSettlementBrokerCompPaidByLender_rep);
            AddFormFieldData("sSettlementApprFPaidByLender", dataLoan.sSettlementApprFPaidByLender_rep);
            AddFormFieldData("sSettlementCrFPaidByLender", dataLoan.sSettlementCrFPaidByLender_rep);
            AddFormFieldData("sSettlementTxServFPaidByLender", dataLoan.sSettlementTxServFPaidByLender_rep);
            AddFormFieldData("sSettlementFloodCertificationFPaidByLender", dataLoan.sSettlementFloodCertificationFPaidByLender_rep);
            AddFormFieldData("sSettlementMBrokFPaidByLender", dataLoan.sSettlementMBrokFPaidByLender_rep);
            AddFormFieldData("sSettlementInspectFPaidByLender", dataLoan.sSettlementInspectFPaidByLender_rep);
            AddFormFieldData("sSettlementProcFPaidByLender", dataLoan.sSettlementProcFPaidByLender_rep);
            AddFormFieldData("sSettlementUwFPaidByLender", dataLoan.sSettlementUwFPaidByLender_rep);
            AddFormFieldData("sSettlementWireFPaidByLender", dataLoan.sSettlementWireFPaidByLender_rep);
            AddFormFieldData("sSettlement800U1FPaidByLender", dataLoan.sSettlement800U1FPaidByLender_rep);
            AddFormFieldData("sSettlement800U2FPaidByLender", dataLoan.sSettlement800U2FPaidByLender_rep);
            AddFormFieldData("sSettlement800U3FPaidByLender", dataLoan.sSettlement800U3FPaidByLender_rep);
            AddFormFieldData("sSettlement800U4FPaidByLender", dataLoan.sSettlement800U4FPaidByLender_rep);
            AddFormFieldData("sSettlement800U5FPaidByLender", dataLoan.sSettlement800U5FPaidByLender_rep);
            AddFormFieldData("sSettlementIPiaPaidByLender", dataLoan.sSettlementIPiaPaidByLender_rep);
            AddFormFieldData("sMipPiaPaidByLender", dataLoan.sMipPiaPaidByLender_rep);
            AddFormFieldData("sSettlementHazInsPiaPaidByLender", dataLoan.sSettlementHazInsPiaPaidByLender_rep);
            AddFormFieldData("sSettlement904PiaPaidByLender", dataLoan.sSettlement904PiaPaidByLender_rep);
            AddFormFieldData("sVaFfPaidByLender", dataLoan.sVaFfPaidByLender_rep);
            AddFormFieldData("sSettlement900U1PiaPaidByLender", dataLoan.sSettlement900U1PiaPaidByLender_rep);
            AddFormFieldData("sSettlementHazInsRsrvPaidByLender", dataLoan.sSettlementHazInsRsrvPaidByLender_rep);
            AddFormFieldData("sSettlementMInsRsrvPaidByLender", dataLoan.sSettlementMInsRsrvPaidByLender_rep);
            AddFormFieldData("sSettlementRealETxRsrvPaidByLender", dataLoan.sSettlementRealETxRsrvPaidByLender_rep);
            AddFormFieldData("sSettlementSchoolTxRsrvPaidByLender", dataLoan.sSettlementSchoolTxRsrvPaidByLender_rep);
            AddFormFieldData("sSettlementFloodInsRsrvPaidByLender", dataLoan.sSettlementFloodInsRsrvPaidByLender_rep);
            AddFormFieldData("sSettlementAggregateAdjRsrvPaidByLender", dataLoan.sSettlementAggregateAdjRsrvPaidByLender_rep);
            AddFormFieldData("sSettlement1008RsrvPaidByLender", dataLoan.sSettlement1008RsrvPaidByLender_rep);
            AddFormFieldData("sSettlement1009RsrvPaidByLender", dataLoan.sSettlement1009RsrvPaidByLender_rep);
            AddFormFieldData("sSettlementEscrowFPaidByLender", dataLoan.sSettlementEscrowFPaidByLender_rep);
            AddFormFieldData("sSettlementOwnerTitleInsFPaidByLender", dataLoan.sSettlementOwnerTitleInsFPaidByLender_rep);
            AddFormFieldData("sSettlementTitleInsFPaidByLender", dataLoan.sSettlementTitleInsFPaidByLender_rep);
            AddFormFieldData("sSettlementDocPrepFPaidByLender", dataLoan.sSettlementDocPrepFPaidByLender_rep);
            AddFormFieldData("sSettlementNotaryFPaidByLender", dataLoan.sSettlementNotaryFPaidByLender_rep);
            AddFormFieldData("sSettlementAttorneyFPaidByLender", dataLoan.sSettlementAttorneyFPaidByLender_rep);
            AddFormFieldData("sSettlementU1TcPaidByLender", dataLoan.sSettlementU1TcPaidByLender_rep);
            AddFormFieldData("sSettlementU2TcPaidByLender", dataLoan.sSettlementU2TcPaidByLender_rep);
            AddFormFieldData("sSettlementU3TcPaidByLender", dataLoan.sSettlementU3TcPaidByLender_rep);
            AddFormFieldData("sSettlementU4TcPaidByLender", dataLoan.sSettlementU4TcPaidByLender_rep);
            AddFormFieldData("sSettlementRecFPaidByLender", dataLoan.sSettlementRecFPaidByLender_rep);
            AddFormFieldData("sSettlementCountyRtcPaidByLender", dataLoan.sSettlementCountyRtcPaidByLender_rep);
            AddFormFieldData("sSettlementStateRtcPaidByLender", dataLoan.sSettlementStateRtcPaidByLender_rep);
            AddFormFieldData("sSettlementU1GovRtcPaidByLender", dataLoan.sSettlementU1GovRtcPaidByLender_rep);
            AddFormFieldData("sSettlementU2GovRtcPaidByLender", dataLoan.sSettlementU2GovRtcPaidByLender_rep);
            AddFormFieldData("sSettlementU3GovRtcPaidByLender", dataLoan.sSettlementU3GovRtcPaidByLender_rep);
            AddFormFieldData("sSettlementPestInspectFPaidByLender", dataLoan.sSettlementPestInspectFPaidByLender_rep);
            AddFormFieldData("sSettlementU1ScPaidByLender", dataLoan.sSettlementU1ScPaidByLender_rep);
            AddFormFieldData("sSettlementU2ScPaidByLender", dataLoan.sSettlementU2ScPaidByLender_rep);
            AddFormFieldData("sSettlementU3ScPaidByLender", dataLoan.sSettlementU3ScPaidByLender_rep);
            AddFormFieldData("sSettlementU4ScPaidByLender", dataLoan.sSettlementU4ScPaidByLender_rep);
            AddFormFieldData("sSettlementU5ScPaidByLender", dataLoan.sSettlementU5ScPaidByLender_rep);
            AddFormFieldData("sSettlementLenderCreditFPaidByLender", dataLoan.sSettlementLenderCreditFPaidByLender_rep);
            AddFormFieldData("sSettlementDiscountPointFPaidByLender", dataLoan.sSettlementDiscountPointFPaidByLender_rep);
            AddFormFieldData("sSettlementCreditLenderPaidItemFPaidByLender", dataLoan.sSettlementCreditLenderPaidItemFPaidByLender_rep);

            AddFormFieldData("sSettlementTotalEstimateSettlementChargePaidToLender", dataLoan.sSettlementTotalDedFromLoanProc_rep);
            AddFormFieldData("sSettlementTotalEstimateSettlementChargePaidByLender", dataLoan.sSettlementTotalFundByLenderAtClosing_rep);
            AddFormFieldData("sSettlement900U1PiaDesc", dataLoan.sSettlement900U1PiaDesc);
            AddFormFieldData("sSettlement900U1Pia", dataLoan.sSettlement900U1Pia_rep);
            AddFormFieldData("sSettlementU4TcDesc", dataLoan.sSettlementU4TcDesc);
            AddFormFieldData("sSettlementU3Tc", dataLoan.sSettlementU3Tc_rep);
            AddFormFieldData("sSettlementU3GovRtcDesc", dataLoan.sSettlementU3GovRtcDesc);
            AddFormFieldData("sSettlementU3GovRtc", dataLoan.sSettlementU3GovRtc_rep);
            
            AddFormFieldData("sSettlement800PaidToLenderTotal", dataLoan.sSettlement800PaidToLenderTotal_rep);
            AddFormFieldData("sSettlement800PaidByLenderTotal", dataLoan.sSettlement800PaidByLenderTotal_rep);

            AddFormFieldData("sSettlement900PaidToLenderTotal", dataLoan.sSettlement900PaidToLenderTotal_rep);
            AddFormFieldData("sSettlement900PaidByLenderTotal", dataLoan.sSettlement900PaidByLenderTotal_rep);

            AddFormFieldData("sSettlement1000PaidToLenderTotal", dataLoan.sSettlement1000PaidToLenderTotal_rep);
            AddFormFieldData("sSettlement1000PaidByLenderTotal", dataLoan.sSettlement1000PaidByLenderTotal_rep);

            AddFormFieldData("sSettlement1100PaidToLenderTotal", dataLoan.sSettlement1100PaidToLenderTotal_rep);
            AddFormFieldData("sSettlement1100PaidByLenderTotal", dataLoan.sSettlement1100PaidByLenderTotal_rep);

            AddFormFieldData("sSettlement1200PaidToLenderTotal", dataLoan.sSettlement1200PaidToLenderTotal_rep);
            AddFormFieldData("sSettlement1200PaidByLenderTotal", dataLoan.sSettlement1200PaidByLenderTotal_rep);

            AddFormFieldData("sSettlement1300PaidToLenderTotal", dataLoan.sSettlement1300PaidToLenderTotal_rep);
            AddFormFieldData("sSettlement1300PaidByLenderTotal", dataLoan.sSettlement1300PaidByLenderTotal_rep);
            
            AddFormFieldData("sSettlementLenderCreditF", dataLoan.sSettlementLenderCreditF_rep);
            AddFormFieldData("sSettlementDiscountPointF", dataLoan.sSettlementDiscountPointF_rep);
        }
    }
}