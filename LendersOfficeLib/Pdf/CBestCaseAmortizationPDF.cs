﻿// <copyright file="CBestCaseAmortizationPDF.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   4/16/2015
// </summary>
namespace LendersOffice.Pdf
{
    /// <summary>
    /// Best case amortization schedule pdf.
    /// </summary>
    public class CBestCaseAmortizationPDF : CAmortizationPDF
    {
        /// <summary>
        /// The description of the pdf.
        /// </summary>
        /// <value>The description.</value>
        public override string Description
        {
            get { return "Amortization Schedule (Best Case)"; }
        }

        /// <summary>
        /// Gets a value indicating whether the form should be visible in the print list.
        /// </summary>
        /// <value>True if the associated loan is an ARM.</value>
        public override bool IsVisible
        {
            get
            {
                return this.DataLoan != null &&
                    this.DataLoan.sFinMethT == DataAccess.E_sFinMethT.ARM &&
                    !this.DataLoan.sRAdjWorstIndex;
            }
        }

        /// <summary>
        /// Gets the type of the amortization, best case here.
        /// </summary>
        /// <value>Best case.</value>
        protected override DataAccess.E_AmortizationScheduleT ScheduleType
        {
            get { return DataAccess.E_AmortizationScheduleT.BestCase; }
        }
    }
}
