namespace LendersOffice.Pdf
{
    using DataAccess;

    /// <summary>
    /// 4506-T-EZ Tax form page 1.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Consistent with other PDFs.")]
    public class CIRS_4506_T_EZ_1PDF : CIRS_4506_T_1PDF
    {
        /// <summary>
        /// Gets the PDF File Name.
        /// </summary>
        public override string PdfFile
        {
            get { return "IRS-4506-T-EZ_1.pdf"; }
        }

        /// <summary>
        /// Maps form data.
        /// </summary>
        /// <param name="dataLoan">The loan data.</param>
        /// <param name="dataApp">The app data.</param>
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            base.ApplyData(dataLoan, dataApp);

            this.AddFormFieldData("a4506TThirdPartyName", dataApp.a4506TThirdPartyName);
            this.AddFormFieldData("a4506TThirdPartyPhone", dataApp.a4506TThirdPartyPhone);
            this.AddFormFieldData("a4506TThirdPartyAddress", dataApp.a4506TThirdPartyAddress);
        }
    }

    /// <summary>
    /// 4506-T-EZ Tax form page 2.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Consistent with other PDFs.")]
    public class CIRS_4506_T_EZ_2PDF : CIRS_4506_T_2PDF
    {
        /// <summary>
        /// Gets the PDF File Name.
        /// </summary>
        public override string PdfFile
        {
            get { return "IRS-4506-T-EZ_2.pdf"; }
        }
    }

    /// <summary>
    /// 4506-T-EZ Tax form.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1402:FileMayOnlyContainASingleClass", Justification = "Consistent with other PDFs.")]
    public class CIRS_4506_T_EZPDF : CIRS_4506_TPDF
    {
        /// <summary>
        /// Gets the form description.
        /// </summary>
        public override string Description
        {
            get { return "Request for Transcript of Tax Return (4506-T-EZ)"; }
        }

        /// <summary>
        /// Gets this forms child pages.
        /// </summary>
        protected override AbstractPDFFile[] ChildPages
        {
            get
            {
                return new AbstractPDFFile[]
                {
                    new CIRS_4506_T_EZ_1PDF(),
                    new CIRS_4506_T_EZ_2PDF()
                };
            }
        }
    }
}
