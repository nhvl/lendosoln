using System;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CGeorgiaDisclosurePDF : AbstractLetterPDF
	{
		
        public override string PdfFile 
        {
            get { return "GeorgiaDisclosure.pdf"; }
        }
        public override string Description 
        {
            get { return "GA Disclosures"; }
        }
		public override string EditLink
		{
			get { return "/newlos/Disclosure/GeorgiaDisclosure.aspx"; }
		}


        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            IPreparerFields broker = dataLoan.GetPreparerOfForm( E_PreparerFormT.GeorgiaDisclosure, E_ReturnOptionIfNotExist.ReturnEmptyObject ); 
            AddFormFieldData("BrokerName", broker.CompanyName);


            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("PropertyAddress0", dataLoan.sSpAddr);
            AddFormFieldData("PropertyAddress1", Tools.CombineCityStateZip(dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            AddFormFieldData("sCrF", dataLoan.sCrF_rep);
            AddFormFieldData("sApprF", dataLoan.sApprF_rep);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
                

        }

	}
}
