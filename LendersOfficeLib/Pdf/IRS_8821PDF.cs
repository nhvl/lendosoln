using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CIRS_8821PDF : AbstractLetterPDF
	{

        public override string PdfFile 
        {
            get { return "IRS-8821.pdf";        	 }
        }
	    
        public override string Description 
        {
            get { return "Tax Information Authorization (8821)"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            string name = dataApp.aBNm;
            
            if (dataApp.aCNm.TrimWhitespaceAndBOM() != "")
                name += Environment.NewLine + dataApp.aCNm;

            AddFormFieldData("Name", Tools.FormatAddress(name, dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip));
            AddFormFieldData("aBSsn", dataApp.aBSsn);
            AddFormFieldData("aCSsn", dataApp.aCSsn);
            AddFormFieldData("aBHPhone", dataApp.aBHPhone);

        }
	}
}
