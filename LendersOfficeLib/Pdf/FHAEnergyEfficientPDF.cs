using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CFHAEnergyEfficientPDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "FHAEnergyEfficient.pdf";        	 }
        }
        public override string Description 
        {
            get { return "FHA Energy Efficient Mortgage Pilot Program"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("sLNm", dataLoan.sLNm);
        }
	}
}
