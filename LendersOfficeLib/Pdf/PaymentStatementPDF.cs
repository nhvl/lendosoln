﻿// <copyright file="PaymentStatementPDF.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   11/27/2014 3:26:22 PM 
// </summary>
namespace LendersOffice.Pdf
{
    using System;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Payment Statement PDF.
    /// </summary>
    public class PaymentStatementPDF : AbstractLetterPDF
    {
        /// <summary>
        /// The pdf name in the PDFForms project.
        /// </summary>
        /// <value>The name of the file that will be used to generate the form. </value>
        public override string PdfFile
        {
            get { return "PaymentStatement.pdf"; }
        }

        /// <summary>
        /// The friendly description of the form shown on the print list.
        /// </summary>
        /// <value>The friendly description of the form.</value>
        public override string Description
        {
            get
            {
                return "Payment Statement";
            }
        }

        /// <summary>
        /// Extracts the needed data from the loan and app and saves it so it can be used to generate the PDF.
        /// </summary>
        /// <param name="dataLoan">The loan object.</param>
        /// <param name="dataApp">The selected application.</param>
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            CAgentFields lender = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            string contact = string.Empty;
            if (lender.IsValid)
            {
                contact = string.Format("{1}{0}{2}", Environment.NewLine, lender.CompanyName, lender.StreetAddr_MultiLine);
            }

            string borrowerNames = string.IsNullOrEmpty(dataApp.aCNm.Trim()) ? dataApp.aBNm : dataApp.aBNm + " & " + dataApp.aCNm;

            this.AddFormFieldData("sLNm", dataLoan.sLNm);
            this.AddFormFieldData("BorrowerNames", borrowerNames);
            this.AddFormFieldData("aBAddrPost_MultiLine", dataApp.aBAddrPost_MultiLine);
            this.AddFormFieldData("sSpAddr_Multiline", dataLoan.sSpAddr_MultiLine);
            this.AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            this.AddFormFieldData("sServicingCurrentPmtDueD", dataLoan.sServicingCurrentPmtDueD_rep);
            this.AddFormFieldData("sServicingDueFunds_LateFees", dataLoan.sServicingDueFunds_LateFees_rep);
            this.AddFormFieldData("sServicingCurrentTotalFunds_Escrow", dataLoan.sServicingCurrentTotalFunds_Escrow_rep);
            this.AddFormFieldData("LenderOfficialContact", contact);
            this.AddFormFieldData("sProMIns", dataLoan.sProMIns_rep);
            this.AddFormFieldData("sServicingCurrentPmtDueAmt", dataLoan.sServicingCurrentPmtDueAmt_rep);
            this.AddFormFieldData("sServicingCurrentPmtDueEscrowHyphensProMIns", dataLoan.sServicingTaxInsuranceImpounds_rep);
            this.AddFormFieldData("sServicingPrincipalBasis", dataLoan.sServicingPrincipalBasis_rep);
            this.AddFormFieldData("sServicingUnpaidPrincipalBalance", dataLoan.sServicingUnpaidPrincipalBalance_rep);
            this.AddFormFieldData("sTodayD", dataLoan.sTodayD_rep);
            this.AddFormFieldData("sServicingCurrentPmtPrincipal", dataLoan.sServicingCurrentPmtPrincipal_rep);
            this.AddFormFieldData("sServicingCurrentPmtDueInterest", dataLoan.sServicingCurrentPmtDueInterest_rep);
            this.AddFormFieldData("sServicingNextPmtAmt", dataLoan.sServicingNextPmtAmt_rep);
        }
    }
}
