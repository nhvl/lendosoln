﻿namespace LendersOffice.Pdf
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;

    public class PrintListGroup
    {
        public string Name { get; set; }
        public Guid Id { get; set; }
        
        public static IEnumerable<PrintListGroup> ListAllGroups(Guid brokerID)
        {
            return ListSpecialGroups(brokerID).Concat(ListBrokerGroups(brokerID));
        }

        public static List<PrintListGroup> ListBrokerGroups(Guid brokerID)
        {
            SqlParameter[] parameters = { new SqlParameter("@BrokerID", brokerID) };
            var ret = new List<PrintListGroup>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerID, "ListPrintGroupByBrokerID", parameters))
            {
                while (reader.Read())
                {
                    ret.Add(new PrintListGroup()
                    {
                        Id = new Guid(reader["GroupID"].ToString()),
                        Name = reader["GroupName"].ToString().TrimWhitespaceAndBOM()
                    });
                }
            }

            return ret;
        }

        public static List<PrintListGroup> ListSpecialGroups(Guid brokerId)
        {
            var brokerDb = Admin.BrokerDB.RetrieveById(brokerId);
            var printGroups = new List<PrintListGroup>(){
                new PrintListGroup() {
                    Name = "<-- All forms -->",
                    Id = Guid.Empty
                }};

            if(brokerDb.IsCustomWordFormsEnabled)
            {
                printGroups.Add(new PrintListGroup()
                {
                    Name = "<-- Custom Word Forms -->",
                    Id = new Guid("11111111-1111-1111-1111-111111111111")
                });
            }

            printGroups.Add(new PrintListGroup()
            {
                Name = "<-- Custom PDF Forms -->",
                Id = new Guid("22222222-2222-2222-2222-222222222222")
            });

            return printGroups;
        }
    }

    public class PdfPrintListItem
    {
        public string Type { get; set; }
        public string Description { get; set; }
        public bool IsExcludePrintGroup { get; set; }
        public bool IsExcludeEsign { get; set; }
    }
    public class PdfPrintList
    {
        private static List<PdfPrintListItem> ImpRetrieveAll(string resourceName)
        {
            List<PdfPrintListItem> list = new List<PdfPrintListItem>();

            Assembly assembly = Assembly.GetAssembly(typeof(PdfPrintListItem));

            XmlDocument document = new XmlDocument();
            document.Load(assembly.GetManifestResourceStream(resourceName));

            foreach (XmlElement el in document.SelectNodes("//printlist/*"))
            {
                string type = "";
                string description = "";
                bool isExcludePrintGroup = false;
                bool isExcludeEsign = false;
                if (el.Name == "header")
                {
                    type = "Header";
                    description = el.GetAttribute("name");
                    isExcludePrintGroup = true;
                }
                else if (el.Name == "print")
                {
                    type = el.GetAttribute("type");

                    ConstructorInfo constructor = PDFClassHashTable.GetConstructor(type);
                    IPDFPrintItem obj = (IPDFPrintItem)constructor.Invoke(new object[0]);
                    description = obj.Description;

                    isExcludePrintGroup = el.GetAttribute("exclude_print_group") == "true";
                    isExcludeEsign = el.GetAttribute("exclude_esign") == "true";
                }
                list.Add(new PdfPrintListItem() { Type = type, Description = description, IsExcludePrintGroup = isExcludePrintGroup, IsExcludeEsign = isExcludeEsign });
            }
            return list;
        }
        public static List<PdfPrintListItem> RetrieveAll()
        {
            return ImpRetrieveAll("LendersOffice.Pdf.printlist.xml.config");   
        }
        public static List<PdfPrintListItem> RetrieveAllForLead()
        {
            return ImpRetrieveAll("LendersOffice.Pdf.leadprintlist.xml.config");
        }

        public static List<PdfPrintListItem> RetrieveOnlyPrintItem()
        {
            List<PdfPrintListItem> list = new List<PdfPrintListItem>();
            foreach (PdfPrintListItem o in RetrieveAll())
            {
                if (o.Type == "Header")
                {
                    continue;
                }
                list.Add(o);
            }
            return list;

        }
    }
}
