﻿namespace LendersOffice.Pdf
{
    using System;
    using DataAccess;
    using LendersOffice.Common;

    public class CVA_26_8978PDF : AbstractLetterPDF
	{
        public override string PdfFile 
        {
            get { return "VA-26-8978.pdf";}
        }
        public override string Description 
        {
            get { return "VA Rights of Loan Borrowers (VA 26-8978)"; }
        }
        public override bool OnlyPrintPrimary
        {
            get { return true; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
        }
	}
}