﻿using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CVA_26_0551PDF : AbstractLetterPDF
    {
        public override string Description
        {
            get { return "VA Debt Questionnaire (VA 26-0551)"; }
        }
        public override string PdfFile
        {
            get { return "VA-26-0551-1.pdf"; }
        }

        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
        }
    }
}