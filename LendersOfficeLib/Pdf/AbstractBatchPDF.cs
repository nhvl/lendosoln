﻿namespace LendersOffice.Pdf
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Text;
    using DataAccess;
    using System.Linq;
    using LendersOffice.Common;
    using LendersOffice.PdfLayout;
    using AntiXss;

    public abstract class AbstractBatchPDF : LendersOffice.PdfGenerator.BatchPDFControl, IPDFPrintItem
    {
        private CPageData m_dataLoan;
        private CAppData m_dataApp;
        private NameValueCollection m_arguments;
        private Guid m_loanID;
        private Guid m_applicationID;
        private string m_id;
        private E_PdfPrintOptions m_printOptions = E_PdfPrintOptions.FormWithData;
        private int m_currentIndex = 0;

        public CPageData DataLoan
        {
            get { return m_dataLoan; }
            set
            {
                m_dataLoan = value;
                m_loanID = m_dataLoan.sLId;
            }
        }

        protected CAppData CurrentAppData
        {
            get { return this.m_dataApp; }
        }

        protected virtual E_AppPrintModeT AppPrintModeT
        {
            get
            {
                return E_AppPrintModeT.Joint;
            }
        }

        public NameValueCollection Arguments
        {
            get { return this.m_arguments; }
            set
            {
                this.m_arguments = value;

                if (this.m_arguments == null)
                {
                    return;
                }

                Guid argLoanId;

                if (Guid.TryParse(this.m_arguments["loanid"], out argLoanId))
                {
                    this.m_loanID = argLoanId;
                }

                Guid argAppId;

                if (Guid.TryParse(this.m_arguments["applicationid"], out argAppId))
                {
                    this.m_applicationID = argAppId;
                }

                if (this.m_arguments["printoptions"] == "2")
                {
                    IsTestMode = true;
                    this.m_printOptions = E_PdfPrintOptions.BlankForms;
                }
                else
                {
                    int argPrintOptions;

                    if (int.TryParse(this.m_arguments["printoptions"], out argPrintOptions))
                    {
                        this.m_printOptions = (E_PdfPrintOptions)argPrintOptions;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether each borrower on the form should generate a full, separate form.
        /// </summary>
        public virtual bool FormPerBorrower { get; } = false;

        private void InitializeDataLoan()
        {
            m_dataLoan = new CPageData(m_loanID, "AbstractBatchPdf", DependencyFields);
            //m_dataLoan = Utilities.GetPDFPrintData(m_loanID);
            m_dataLoan.InitLoad();
            m_dataLoan.SetFormatTarget(FormatTarget.PrintoutNormalFields);
        }

        protected abstract AbstractPDFFile[] ChildPages
        {
            get;
        }

        public bool IsContainSignature
        {
            get
            {
                foreach (AbstractPDFFile f in ChildPages)
                {
                    if (f.IsContainSignature)
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public bool IsContainBorrowerSignature
        {
            get
            {
                foreach (AbstractPDFFile f in ChildPages)
                {
                    if (f.IsContainBorrowerSignature)
                    {
                        return true;
                    }
                }
                return false;
            }

        }

        public bool IsContainCoborrowerSignature
        {
            get
            {
                foreach (AbstractPDFFile f in ChildPages)
                {
                    if (f.IsContainCoborrowerSignature)
                    {
                        return true;
                    }
                }
                return false;
            }

        }

        public override void Initialize()
        {
            if (null == m_dataLoan)
            {
                InitializeDataLoan();
            }

            bool isAppSpecific = Guid.Empty != m_applicationID;

            int nApps = (OnlyPrintPrimary || isAppSpecific) ? 1 : m_dataLoan.nApps;

            for (int i = 0; i < nApps; i++)
            {
                if (isAppSpecific)
                {
                    SetCurrentDataApp(m_dataLoan.GetAppData(m_applicationID));
                }
                else
                {
                    SetCurrentDataApp(m_dataLoan.GetAppData(i));
                }

                var providedBorrType = m_arguments["borrtype"];
                List<string> borrTypes = new List<string>();

                if (String.IsNullOrEmpty(providedBorrType))
                {
                    borrTypes.Add("B");
                    
                    if(this.FormPerBorrower && m_dataApp.aCIsDefined)
                    {
                        borrTypes.Add("C");
                    }
                }
                else
                {
                    borrTypes.Add(providedBorrType);
                }

                foreach (string borrType in borrTypes)
                {
                    foreach (AbstractPDFFile f in ChildPages)
                    {
                        f.DataLoan = m_dataLoan;
                        f.SetCurrentDataApp(m_dataApp);
                        f.SetPdfPrintOptions(m_printOptions);

                        if (!string.IsNullOrEmpty(borrType))
                        {
                            f.BorrType = borrType;
                        }

                        if (f.IsVisible)
                            Add(f);

                        if(f.AppPrintModeT == E_AppPrintModeT.BorrowerAndSpouseSeparately && !this.FormPerBorrower && f.ShouldRenderCoBorrower)
                        {
                            var coBorrowerFile = (AbstractPDFFile)(Activator.CreateInstance(f.GetType()));

                            coBorrowerFile.DataLoan = m_dataLoan;
                            coBorrowerFile.SetCurrentDataApp(m_dataApp);
                            coBorrowerFile.SetPdfPrintOptions(m_printOptions);
                            coBorrowerFile.BorrType = "C";
                            this.ApplyBatchArguments(coBorrowerFile);

                            if (coBorrowerFile.IsVisible)
                            {
                                Add(coBorrowerFile);
                            }
                        }
                    }
                }
            }
        }

        public virtual string Name
        {
            get
            {
                // 5/24/2004 dd - If class name has following format C{name}PDF then return unique code as {name}
                string name = GetType().Name;
                if (name.StartsWith("C") && name.EndsWith("PDF"))
                {
                    name = name.Substring(1, name.Length - 4);
                }
                return name;
            }
        }

        public virtual Guid RecordID
        {
            get { return Guid.Empty; }
        }
        #region Implementation of IPDFPrintItem

        public string ID
        {
            get { return m_id; }
            set { m_id = value; }
        }

        public virtual string Description
        {
            get { return GetType().Name; }
        }

        public virtual string EditLink
        {
            get { return ""; }
        }

        public string PreviewLink
        {
            get
            {
                return GeneratePreviewLink(ID + "_" + m_currentIndex);

            }
        }

        private string GeneratePreviewLink(string id)
        {
            return string.Format(
                @"(<a href=""#"" onclick=""return _preview('{0}.aspx', {1});"">preview</a>)", 
                AspxTools.JsStringUnquoted(Name), 
                AspxTools.JsString(id));
        }

        public virtual bool IsVisible
        {
            get { return true; }
        }

        public bool HasPdf
        {
            get { return true; }
        }
        #endregion

        public void SetCurrentDataApp(CAppData dataApp)
        {
            m_dataApp = dataApp;
            m_applicationID = dataApp.aAppId;
        }

        public virtual bool OnlyPrintPrimary
        {
            get { return false; }
        }

        /// <summary>
        /// The arguments that the Batch PDF intends to pass on to its Child Pages.
        /// </summary>
        protected virtual NameValueCollection ChildArguments
        {
            get { return null; }
        }
        
        /// <summary>
        /// This method takes the given list of PDF Files and adds the ChildArguments to each page's arguments object.
        /// </summary>
        /// <param name="children">The list of children that will have their arguments updated with ChildArguments.</param>
        /// <returns>An enumerable of the updated children.</returns>
        protected virtual IEnumerable<AbstractPDFFile> ApplyBatchArguments(IEnumerable<AbstractPDFFile> children)
        {
            return this.ChildArguments == null ?
                children :
                children.Select(page =>
                {
                    this.ApplyBatchArguments(page);
                    return page;
                });
        }

        /// <summary>
        /// This method takes an AbstractPdfFile and applies the Batch File's ChildArguments to the pdf's arguments.
        /// </summary>
        /// <param name="child">The child pdf that will have it's argument's updated.</param>
        protected virtual void ApplyBatchArguments(AbstractPDFFile child)
        {
            var arguments = child.Arguments == null ? new NameValueCollection() : child.Arguments;
            arguments.Add(this.ChildArguments);
            child.Arguments = arguments;
        }

        public void RenderPrintLink(StringBuilder sb)
        {
            RenderPrintLink(sb, false);
        }

        private void AddChildIds(ArrayList idsList, AbstractPDFFile file)
        {
            if (file.AppPrintModeT == E_AppPrintModeT.Joint)
            {
                idsList.Add(AspxTools.JsString(file.ID));
            }
            else if (file.AppPrintModeT == E_AppPrintModeT.BorrowerAndSpouseSeparately)
            {
                idsList.Add($"'{AspxTools.JsStringUnquoted(file.ID) + "_B" }'");
                idsList.Add($"'{AspxTools.JsStringUnquoted(file.ID) + "_C" }'");
            }
        }

        public void RenderPrintLink(StringBuilder sb, bool isIndent)
        {
            // Don't render hidden pdfs
            if (!IsVisible)
            {
                return;
            }

            int nApps = OnlyPrintPrimary ? 1 : m_dataLoan.nApps;

            for (int i = 0; i < nApps; i++)
            {
                m_currentIndex = i;
                SetCurrentDataApp(m_dataLoan.GetAppData(i));
                if (AppPrintModeT == E_AppPrintModeT.Joint)
                {
                    string printId = this.ID + "_" + i;
                    string edit = EditLink;
                    if (edit != "")
                    {
                        edit = string.Format(@"(<a href=""javascript:_link('{0}{1}', '{2}');"">edit</a>)", Tools.VRoot, edit, printId);
                    }
                    string desc = Description;
                    if (nApps > 1)
                    {
                        desc += " - " + Utilities.SafeHtmlString(m_dataApp.aBNm);
                        if (m_dataApp.aCNm != "")
                            desc += " & " + Utilities.SafeHtmlString(m_dataApp.aCNm);
                    }

                    ArrayList idsList = new ArrayList();
                    StringBuilder sbChild = new StringBuilder();
                    int index = 0;

                    this.ApplyBatchArguments(ChildPages);
                    foreach (AbstractPDFFile child in ChildPages)
                    {
                        child.DataLoan = m_dataLoan;
                        child.SetCurrentDataApp(m_dataApp);
                        child.ID = m_id + "_" + i + "_" + index;
                        if (child.IsVisible)
                        {
                            child.Script = @"onclick=""clearMainForm(this, " + AspxTools.JsString(printId) + @");""";
                            this.AddChildIds(idsList, child);
                            child.RenderPrintLink(sbChild, true);
                        }
                        index++;
                    }

                    string script = string.Format(@"onclick=""selectWhole(this, {0});""", string.Join(",", ((string[])idsList.ToArray(typeof(string)))));
                    RenderPrintLink(sb, printId, desc, script, edit, PreviewLink, string.Empty);
                    sb.Append(sbChild);
                }
                else if (AppPrintModeT == E_AppPrintModeT.BorrowerAndSpouseSeparately)
                {
                    // 3/30/2010 dd - This option created to handle case 42887, where we need to display a print form for borrower and coborrower separately.


                    // Borrower
                    string printId = this.ID + "_" + i + "_B";

                    string edit = EditLink;
                    if (edit != "")
                    {
                        edit = string.Format(@"(<a href=""javascript:_link('{0}{1}', '{2}');"">edit</a>)", Tools.VRoot, edit, printId);
                    }
                    string desc = Description;
                    desc += " - " + Utilities.SafeHtmlString(m_dataApp.aBNm);

                    ArrayList idsList = new ArrayList();
                    StringBuilder sbChild = new StringBuilder();
                    int index = 0;
                    this.ApplyBatchArguments(this.ChildPages);
                    foreach (AbstractPDFFile child in ChildPages)
                    {

                        // 9/3/2013 EM - Case 136862 Load args before you call setCurrentDataAPP to prevent the info
                        //from resetting
                        var arguments = child.Arguments == null ? new NameValueCollection() : child.Arguments;
                        arguments.Add("borrtype", "B");
                        child.Arguments = arguments;

                        child.DataLoan = m_dataLoan;
                        child.SetCurrentDataApp(m_dataApp);
                        child.ID = m_id + "_" + i + "_" + index + "_B";
                        if (child.IsVisible)
                        {
                            child.Script = @"onclick=""clearMainForm(this, " + AspxTools.JsString(printId) + @");""";
                            this.AddChildIds(idsList, child);
                            child.RenderPrintLink(sbChild, true);
                        }
                        index++;
                    }

                    string script = string.Format(@"onclick=""selectWhole(this, {0});""", string.Join(",", ((string[])idsList.ToArray(typeof(string)))));
                    RenderPrintLink(sb, printId, desc, script, edit, GeneratePreviewLink(printId), "B");
                    sb.Append(sbChild);


                    // Coborrower
                    //Do we actually have one?
                    if (string.IsNullOrEmpty(m_dataApp.aCNm) == false)
                    {
                        //Yes we do, so set up their data
                        desc = Description;
                        desc += " - " + Utilities.SafeHtmlString(m_dataApp.aCNm);

                        idsList = new ArrayList();
                        sbChild = new StringBuilder();
                        index = 0;
                        printId = this.ID + "_" + i + "_C";

                        edit = EditLink;
                        if (edit != "")
                        {
                            edit = string.Format(@"(<a href=""javascript:_link('{0}{1}', '{2}');"">edit</a>)", Tools.VRoot, edit, printId);
                        }
                        foreach (AbstractPDFFile child in ChildPages)
                        {

                            // 9/3/2013 EM - Case 136862 Load args before you call setCurrentDataAPP to prevent the info
                            //from resetting
                            var arguments = child.Arguments == null ? new NameValueCollection() : child.Arguments;
                            arguments.Add("borrtype", "C");
                            child.Arguments = arguments;

                            child.DataLoan = m_dataLoan;
                            child.SetCurrentDataApp(m_dataApp);
                            child.ID = m_id + "_" + i + "_" + index + "_C";
                            if (child.IsVisible)
                            {
                                child.Script = @"onclick=""clearMainForm(this, " + AspxTools.JsString(printId) + @");""";
                                this.AddChildIds(idsList, child);
                                child.RenderPrintLink(sbChild, true);
                            }
                            index++;
                        }

                        script = string.Format(@"onclick=""selectWhole(this, {0});""", string.Join(",", ((string[])idsList.ToArray(typeof(string)))));
                        RenderPrintLink(sb, printId, desc, script, edit, GeneratePreviewLink(printId), "C");
                        sb.Append(sbChild);
                    }
                }
            }
        }

        protected void RenderPrintLink(StringBuilder sb, string id, string desc, string script, string editLink, string previewLink, string borrType)
        {
            string extra = "appid='" + m_applicationID + "'";
            if (RecordID != Guid.Empty)
                extra += " recordid='" + RecordID + "'";

            if (!string.IsNullOrEmpty(borrType))
            {
                extra += " borrtype='" + borrType + "'";
            }

            string disabledCheckbox = string.Empty;
            string accessDeniedMsg = string.Empty;

            if (!HasPrintPermission)
            {
                disabledCheckbox = "disabled='disabled'";
                accessDeniedMsg = "<span style='font-weight:bold;color:red'>" + ErrorMessages.PdfPrintAccessDenied + "</span>";
                previewLink = string.Empty;

            }

            string checkboxHtml = string.Format("<input name='{0}' type='checkbox' parentLink='t' {1} {2} pdf='{3}' excludeFromBatch='t' {4} {5}>"
                , id // 0
                , script // 1
                , extra // 2
                , GetType().Name // 3
                , OnlyPrintPrimary ? " primaryOnly=t " : "" // 4
                , disabledCheckbox // 5
                );

            sb.AppendFormat("<tr><td>{0}</td><td>{1} {2} {3} {4}</td></tr>"
                , checkboxHtml // 0
                , desc // 1
                , editLink // 2
                , previewLink // 3
                , accessDeniedMsg // 4
                );
        }

        #region IPDFPrintItem Members

        public virtual bool HasPrintPermission
        {
            get
            {
                foreach (AbstractPDFFile child in ChildPages)
                {
                    if (!child.HasPrintPermission)
                    {
                        return false; // 3/10/2010 dd - If any child page does not have permission to print then do not allow to print whole document.
                    }
                }
                return true;
            }
        }

        #endregion

        #region IPDFPrintItem Members

        public IEnumerable<string> DependencyFields
        {
            get
            {
                List<string> dependencyFields = new List<string>();

                foreach (AbstractPDFFile child in ChildPages)
                {
                    foreach (string field in child.DependencyFields)
                    {
                        if (!dependencyFields.Contains(field))
                        {
                            dependencyFields.Add(field);
                        }
                    }
                }
                return dependencyFields;
            }
        }

        #endregion
    }
}
