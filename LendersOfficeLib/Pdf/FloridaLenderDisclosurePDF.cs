using System;
using System.Collections.Specialized;
using System.Data;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CFloridaLenderDisclosurePDF : AbstractLetterPDF
	{

        public override string Description 
        {
            get { return "Florida-Lender Disclosure"; }
        }
        public override string PdfFile 
        {
            get { return "FloridaLenderDisclosure.pdf"; }
        } 
        public override string EditLink 
        {
            get { return "/newlos/Forms/FloridaDisclosureEdit.aspx"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            string borrowerName = dataApp.aBNm;
            string coborrowerName = dataApp.aCNm;

            if (coborrowerName.TrimWhitespaceAndBOM() != "") 
            {
                borrowerName = borrowerName + " & " + coborrowerName;
            }

            IPreparerFields lender = dataLoan.GetPreparerOfForm( E_PreparerFormT.FloridaLenderDisclosure, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            if( lender.IsValid ) 
            {
                AddFormFieldData("LenderName", lender.CompanyName);
            }
            switch (dataLoan.sRefundabilityOfFeeToLenderT) 
            {
                case E_sRefundabilityOfFeeToLenderT.Nonrefundable: 
                    AddFormFieldData("sRefundabilityOfFeeToLender", "nonrefundable");
                    break;
                case E_sRefundabilityOfFeeToLenderT.Refundable:
                    AddFormFieldData("sRefundabilityOfFeeToLender", "refundable");
                    break;
                case E_sRefundabilityOfFeeToLenderT.NA:
                    AddFormFieldData("sRefundabilityOfFeeToLender", "N/A (No funds collected at or prior to funding)");
                    break;
            }
            string lenderType = "";
            if (dataLoan.sIsLicensedLender) 
            {
                lenderType = "licensed mortgage lender";
            } 
            else if (dataLoan.sIsCorrespondentLender) 
            {
                lenderType = "correspondent mortgage lender";
            } 
            else if (dataLoan.sIsOtherTypeLender) 
            {
                lenderType = dataLoan.sOtherTypeLenderDesc;
            }


            //CAgentFields lenderRep = dataLoan.GetAgentOfRole(E_AgentRoleT.Lender
            AddFormFieldData("LenderType", lenderType);
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("sCommitmentEstimateDays", dataLoan.sCommitmentEstimateDays);

            if( lender.IsValid ) 
            {
                AddFormFieldData("sLenderRepNm", lender.PreparerName);
                AddFormFieldData("sLenderRepAddr0", lender.StreetAddr);
                AddFormFieldData("sLenderRepAddr1", Tools.CombineCityStateZip(lender.City, lender.State, lender.Zip));
            }
            AddFormFieldData("BorrowerName", borrowerName);

        }

	}
}
