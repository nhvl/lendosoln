using System;
using System.Collections.Specialized;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CHUD_92900_PUR_1PDF : AbstractLetterPDF
    {
        public override string PdfFile 
        {
            get { return "HUD-92900-PUR_1.pdf"; }
        }
 
        public override string Description 
        {
            get { return "Page 1"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/FHA/FHACreditAnalysisPurchase.aspx"; }
        }


        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {

            AddFormFieldData("aFHAOtherDebtPmtMCAWPrinting", dataApp.aFHAOtherDebtPmtMCAWPrinting_rep);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("aBSsn", dataApp.aBSsn);
            AddFormFieldData("aCSsn", dataApp.aCSsn);
            AddFormFieldData("sFHANonrealtyLckd", dataLoan.sFHANonrealtyLckd);
            AddFormFieldData("sFHAOtherDebtBal", dataLoan.sFHAOtherDebtBal_rep);
            AddFormFieldData("sFHADebtPmtTot", dataLoan.sFHADebtPmtTot_rep);
            AddFormFieldData("sPurchPrice6pc", dataLoan.sPurchPrice6pc_rep);
            AddFormFieldData("sFHAHousingActSection", dataLoan.sFHAHousingActSection);
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sFHAConstructionT", dataLoan.sFHAConstructionT);

            IPreparerFields underwriter = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHACreditAnalysisPurchaseUnderwriter, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            AddFormFieldData("FHACreditAnalysisPurchaseUnderwriterLicenseNumOfAgent", underwriter.LicenseNumOfAgent);

            AddFormFieldData("sLAmt", dataLoan.sLAmtCalc_rep);
            AddFormFieldData("sPresLTotPersistentHExp", dataApp.aPresTotHExp_rep);
            AddFormFieldData("sFfUfmip1003", dataLoan.sFfUfmip1003_rep);
            AddFormFieldData("sTermInYr", dataLoan.sTermInYr_rep);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sApprVal", dataLoan.sApprVal_rep);
            AddFormFieldData("sBuydownResultIR", dataLoan.sBuydownResultIR_rep);
            AddFormFieldData("sFHACcTot", dataLoan.sFHACcTot_rep);
            AddFormFieldData("sFHACcPbs", dataLoan.sFHACcPbs_rep);
            AddFormFieldData("sFHAPurchPrice", dataLoan.sFHAPurchPrice_rep);
            AddFormFieldData("sFHACcPbb", dataLoan.sFHACcPbb_rep);
            AddFormFieldData("sFHAUnadjAcquisition", dataLoan.sFHAUnadjAcquisition_rep);
            AddFormFieldData("sFHAStatutoryInvestReq", dataLoan.sFHAStatutoryInvestReq_rep);
            AddFormFieldData("sFHAHouseVal", dataLoan.sFHAHouseVal_rep);
            AddFormFieldData("sFHAReqAdj", dataLoan.sFHAReqAdj_rep);
            AddFormFieldData("sFHAMBasis", dataLoan.sFHAMBasis_rep);
            AddFormFieldData("sFHALtv", dataLoan.sFHALtv_rep);
            AddFormFieldData("sFHAMAmt", dataLoan.sFHAMAmt_rep);
            AddFormFieldData("sFHAMinDownPmt", dataLoan.sFHAMinDownPmt_rep);
            AddFormFieldData("sFHAPrepaidExp", dataLoan.sFHAPrepaidExp_rep);
            AddFormFieldData("sFHADiscountPoints", dataLoan.sFHADiscountPoints_rep);
            AddFormFieldData("sFHAImprovements", dataLoan.sFHAImprovements_rep);
            AddFormFieldData("sUfCashPd", dataLoan.sUfCashPd_rep);
            AddFormFieldData("sFHANonrealty", dataLoan.sFHANonrealty_rep);
            AddFormFieldData("sFHATotCashToClose", dataLoan.sFHATotCashToClose_rep);
            AddFormFieldData("sFHAAmtPaid", dataLoan.sFHAAmtPaid_rep);
            AddFormFieldData("sFHAGiftFundSrc", dataApp.aFHAGiftFundSrc);
            AddFormFieldData("sFHAGiftFundAmt", dataApp.aFHAGiftFundAmt_rep);
            AddFormFieldData("sFHAAssetAvail", dataApp.aFHAAssetAvail_rep);
            AddFormFieldData("sFHA2ndMSrc", dataLoan.sFHA2ndMSrc);
            AddFormFieldData("sFHA2ndMAmt", dataLoan.sFHA2ndMAmt_rep);
            AddFormFieldData("sFHACashReserves", dataLoan.sFHACashReserves_rep);
            AddFormFieldData("sFHABBaseI", dataApp.aFHABBaseI_rep);
            AddFormFieldData("sFHABOI", dataApp.aFHABOI_rep);
            AddFormFieldData("sFHACBaseI", dataApp.aFHACBaseI_rep);
            AddFormFieldData("sFHACOI", dataApp.aFHACOI_rep);
            AddFormFieldData("sFHANetRentalI", dataApp.aFHANetRentalI_rep);
            AddFormFieldData("sFHADebtInstallPmt", dataApp.aFHADebtInstallPmt_rep);
            AddFormFieldData("sFHADebtInstallBal", dataApp.aFHADebtInstallBal_rep);
            AddFormFieldData("sFHAChildSupportPmt", dataApp.aFHAChildSupportPmt_rep);
            AddFormFieldData("sFHAOtherDebtPmt", dataApp.aFHAOtherDebtPmt_rep);
            AddFormFieldData("sFHAOtherDebtBal", dataApp.aFHAOtherDebtBal_rep);

            AddFormFieldData("sFHAPro1stMPmt", dataLoan.sFHAPro1stMPmt_rep);
            AddFormFieldData("sFHAProMIns", dataLoan.sFHAProMIns_rep);
            AddFormFieldData("sFHAProHoAssocDues", dataLoan.sFHAProHoAssocDues_rep);
            AddFormFieldData("sFHAProGroundRent", dataLoan.sFHAProGroundRent_rep);
            AddFormFieldData("sFHAPro2ndFinPmt", dataLoan.sFHAPro2ndFinPmt_rep);
            AddFormFieldData("sFHAProHazIns", dataLoan.sFHAProHazIns_rep);
            AddFormFieldData("sFHAProRealETx", dataLoan.sFHAProRealETx_rep);
            AddFormFieldData("sFHAMonthlyPmt", dataLoan.sFHAMonthlyPmt_rep);
            AddFormFieldData("sFHADebtPmtTot", dataApp.aFHADebtPmtTot_rep);
            AddFormFieldData("sFHAPmtFixedTot", dataApp.aFHAPmtFixedTot_rep);
            AddFormFieldData("sFHALtv2", dataLoan.sFHALtvPurch_rep);
            AddFormFieldData("sFHAMPmtToIRatio", dataApp.aFHAMPmtToIRatio_rep);
            AddFormFieldData("sFHAFixedPmtToIRatio", dataApp.aFHAFixedPmtToIRatio_rep);
            AddFormFieldData("sFHACreditRating", dataApp.aFHACreditRating);
            AddFormFieldData("sFHARatingIAdequacy", dataApp.aFHARatingIAdequacy);
            AddFormFieldData("sFHARatingIStability", dataApp.aFHARatingIStability);
            AddFormFieldData("sFHARatingAssetAdequacy", dataApp.aFHARatingAssetAdequacy);
            AddFormFieldData("sFHABCaivrsNum", dataApp.aFHABCaivrsNum);
            AddFormFieldData("sFHACCaivrsNum", dataApp.aFHACCaivrsNum);
            AddFormFieldData("sFHABLpdGsa", dataApp.aFHABLpdGsa);
            AddFormFieldData("sFHACLpdGsa", dataApp.aFHACLpdGsa);
            AddFormFieldData("sPurchPrice", dataLoan.sPurchPrice_rep);
            AddFormFieldData("sFHASellerContribution", dataLoan.sFHASellerContribution_rep);
            AddFormFieldData("sFHAExcessContribution", dataLoan.sFHAExcessContribution_rep);
            AddFormFieldData("sFHACreditAnalysisRemarks", dataLoan.sFHACreditAnalysisRemarks);
            AddFormFieldData("sFHAGrossMonI", dataApp.aFHAGrossMonI_rep);
        }
    }

    public class CHUD_92900_PUR_2PDF : AbstractLetterPDF 
    {
        public override string PdfFile 
        {
            get { return "HUD-92900-PUR_2.pdf"; }
        }
 
        public override string Description 
        {
            get { return "Page 2"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
        }
    }


    /// <summary>
    /// Summary description for CHUD_92900_PUR_PDF.
    /// </summary>
    public class CHUD_92900_PURPDF : AbstractBatchPDF
    {
 
        public override string Description 
        {
            get { return "FHA MCAW - Purchase (HUD-92900-PUR)"; }
        }

        public override string EditLink 
        {
            get { return "/newlos/FHA/FHACreditAnalysisPurchase.aspx"; }
        }

        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            {
                return new AbstractPDFFile[] {
                                                 new CHUD_92900_PUR_1PDF(),
                                                 new CHUD_92900_PUR_2PDF()
                                             };
            }
        }


    }
    public class CHUD_92900_PUR_Combined_1PDF : AbstractLetterPDF
    {

        public override string PdfFile 
        {
            get { return "HUD-92900-PUR_1.pdf"; }
        }
 
        public override string Description 
        {
            get { return "Page 1"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/FHA/FHACreditAnalysisPurchaseCombine.aspx"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("aFHAOtherDebtPmtMCAWPrinting", dataLoan.sFHAOtherDebtPmtMCAWPrinting_rep);
            AddFormFieldData("aBNm", dataLoan.sCombinedBorNm);
            AddFormFieldData("aCNm", dataLoan.sCombinedCoborNm);
            AddFormFieldData("aBSsn", dataLoan.sCombinedBorSsn);
            AddFormFieldData("aCSsn", dataLoan.sCombinedCoborSsn);
            AddFormFieldData("sFHANonrealtyLckd", dataLoan.sFHANonrealtyLckd);
            AddFormFieldData("sFHAOtherDebtBal", dataLoan.sFHAOtherDebtBal_rep);
            AddFormFieldData("sFHADebtPmtTot", dataLoan.sFHADebtPmtTot_rep);
            AddFormFieldData("sPurchPrice6pc", dataLoan.sPurchPrice6pc_rep);
            AddFormFieldData("sFHAHousingActSection", dataLoan.sFHAHousingActSection);
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);
            AddFormFieldData("sFHAConstructionT", dataLoan.sFHAConstructionT);
            IPreparerFields underwriter = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHACreditAnalysisPurchaseUnderwriter, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            AddFormFieldData("FHACreditAnalysisPurchaseUnderwriterLicenseNumOfAgent", underwriter.LicenseNumOfAgent);

            AddFormFieldData("sLAmt", dataLoan.sLAmtCalc_rep);
            AddFormFieldData("sPresLTotPersistentHExp", dataLoan.sPresLTotHExp_rep);
            AddFormFieldData("sFfUfmip1003", dataLoan.sFfUfmip1003_rep);
            AddFormFieldData("sTermInYr", dataLoan.sTermInYr_rep);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sApprVal", dataLoan.sApprVal_rep);
            AddFormFieldData("sBuydownResultIR", dataLoan.sBuydownResultIR_rep);
            AddFormFieldData("sFHACcTot", dataLoan.sFHACcTot_rep);
            AddFormFieldData("sFHACcPbs", dataLoan.sFHACcPbs_rep);
            AddFormFieldData("sPurchPrice", dataLoan.sPurchPrice_rep);
			AddFormFieldData("sFHAPurchPrice", dataLoan.sFHAPurchPrice_rep); // OPM 24158
            AddFormFieldData("sFHACcPbb", dataLoan.sFHACcPbb_rep);
            AddFormFieldData("sFHAUnadjAcquisition", dataLoan.sFHAUnadjAcquisition_rep);
            AddFormFieldData("sFHAStatutoryInvestReq", dataLoan.sFHAStatutoryInvestReq_rep);
            AddFormFieldData("sFHAHouseVal", dataLoan.sFHAHouseVal_rep);
            AddFormFieldData("sFHAReqAdj", dataLoan.sFHAReqAdj_rep);
            AddFormFieldData("sFHAMBasis", dataLoan.sFHAMBasis_rep);
            AddFormFieldData("sFHALtv", dataLoan.sFHALtv_rep);
            AddFormFieldData("sFHAMAmt", dataLoan.sFHAMAmt_rep);
            AddFormFieldData("sFHAMinDownPmt", dataLoan.sFHAMinDownPmt_rep);
            AddFormFieldData("sFHAPrepaidExp", dataLoan.sFHAPrepaidExp_rep);
            AddFormFieldData("sFHADiscountPoints", dataLoan.sFHADiscountPoints_rep);
            AddFormFieldData("sFHAImprovements", dataLoan.sFHAImprovements_rep);
            AddFormFieldData("sUfCashPd", dataLoan.sUfCashPd_rep);
            AddFormFieldData("sFHANonrealty", dataLoan.sFHANonrealty_rep);
            AddFormFieldData("sFHATotCashToClose", dataLoan.sFHATotCashToClose_rep);
            AddFormFieldData("sFHAAmtPaid", dataLoan.sFHAAmtPaid_rep);
            AddFormFieldData("sFHAGiftFundSrc", dataLoan.sFHAGiftFundSrc);
            AddFormFieldData("sFHAGiftFundAmt", dataLoan.sFHAGiftFundAmt_rep);
            AddFormFieldData("sFHAAssetAvail", dataLoan.sFHAAssetAvail_rep);
            AddFormFieldData("sFHA2ndMSrc", dataLoan.sFHA2ndMSrc);
            AddFormFieldData("sFHA2ndMAmt", dataLoan.sFHA2ndMAmt_rep);
            AddFormFieldData("sFHACashReserves", dataLoan.sFHACashReserves_rep);
            AddFormFieldData("sFHABBaseI", dataLoan.sFHABBaseI_rep);
            AddFormFieldData("sFHABOI", dataLoan.sFHABOI_rep);
            AddFormFieldData("sFHACBaseI", dataLoan.sFHACBaseI_rep);
            AddFormFieldData("sFHACOI", dataLoan.sFHACOI_rep);
            AddFormFieldData("sFHANetRentalI", dataLoan.sFHANetRentalI_rep);
            AddFormFieldData("sFHADebtInstallPmt", dataLoan.sFHADebtInstallPmt_rep);
            AddFormFieldData("sFHADebtInstallBal", dataLoan.sFHADebtInstallBal_rep);

            AddFormFieldData("sFHAChildSupportPmt", dataLoan.sFHAChildSupportPmt_rep);
            AddFormFieldData("sFHAOtherDebtPmt", dataLoan.sFHAOtherDebtPmt_rep);
            AddFormFieldData("sFHAOtherDebtBal", dataLoan.sFHAOtherDebtBal_rep);

            AddFormFieldData("sFHAPro1stMPmt", dataLoan.sFHAPro1stMPmt_rep);
            AddFormFieldData("sFHAProMIns", dataLoan.sFHAProMIns_rep);
            AddFormFieldData("sFHAProHoAssocDues", dataLoan.sFHAProHoAssocDues_rep);
            AddFormFieldData("sFHAProGroundRent", dataLoan.sFHAProGroundRent_rep);
            AddFormFieldData("sFHAPro2ndFinPmt", dataLoan.sFHAPro2ndFinPmt_rep);
            AddFormFieldData("sFHAProHazIns", dataLoan.sFHAProHazIns_rep);
            AddFormFieldData("sFHAProRealETx", dataLoan.sFHAProRealETx_rep);
            AddFormFieldData("sFHAMonthlyPmt", dataLoan.sFHAMonthlyPmt_rep);
            AddFormFieldData("sFHADebtPmtTot", dataLoan.sFHADebtPmtTot_rep);
            AddFormFieldData("sFHAPmtFixedTot", dataLoan.sFHAPmtFixedTot_rep);
            AddFormFieldData("sFHALtv2", dataLoan.sFHALtvPurch_rep);
            AddFormFieldData("sFHAMPmtToIRatio", dataLoan.sFHAMPmtToIRatio_rep);
            AddFormFieldData("sFHAFixedPmtToIRatio", dataLoan.sFHAFixedPmtToIRatio_rep);
            AddFormFieldData("sFHACreditRating", dataLoan.sFHACreditRating);
            AddFormFieldData("sFHARatingIAdequacy", dataLoan.sFHARatingIAdequacy);
            AddFormFieldData("sFHARatingIStability", dataLoan.sFHARatingIStability);
            AddFormFieldData("sFHARatingAssetAdequacy", dataLoan.sFHARatingAssetAdequacy);
            AddFormFieldData("sFHABCaivrsNum", dataLoan.sFHABCaivrsNum);
            AddFormFieldData("sFHACCaivrsNum", dataLoan.sFHACCaivrsNum);
            AddFormFieldData("sFHABLpdGsa", dataLoan.sFHABLpdGsa);
            AddFormFieldData("sFHACLpdGsa", dataLoan.sFHACLpdGsa);
            AddFormFieldData("sPurchPrice", dataLoan.sPurchPrice_rep);
            AddFormFieldData("sFHASellerContribution", dataLoan.sFHASellerContribution_rep);
            AddFormFieldData("sFHAExcessContribution", dataLoan.sFHAExcessContribution_rep);
            AddFormFieldData("sFHACreditAnalysisRemarks", dataLoan.sFHACreditAnalysisRemarks);
            AddFormFieldData("sFHAGrossMonI", dataLoan.sFHAGrossMonI_rep);

        }
    }


    /// <summary>
    /// Summary description for CHUD_92900_PUR_PDF.
    /// </summary>
    public class CHUD_92900_PUR_CombinedPDF : AbstractBatchPDF
    {

        public override string Description 
        {
            get { return "FHA MCAW - Combined Purchase (HUD-92900-PUR)"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/FHA/FHACreditAnalysisPurchaseCombine.aspx"; }
        }
		public override bool OnlyPrintPrimary
		{
			get { return true; }
		}
        protected override AbstractPDFFile[] ChildPages 
        {
            get 
            {
                return new AbstractPDFFile[] {
                                                 new CHUD_92900_PUR_Combined_1PDF(),
                                                 new CHUD_92900_PUR_2PDF()
                                             };
            }
        }



    }
}
