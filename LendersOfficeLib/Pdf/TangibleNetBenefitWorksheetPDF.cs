﻿using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CTangibleNetBenefitWorksheetPDF : AbstractLegalPDF
    {
        public override string PdfFile
        {
            get { return "TangibleNetBenefitWorksheet.pdf"; }
        }
        public override string Description
        {
            get { return "Tangible Net Benefit Worksheet"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp)
        {
            CAgentFields loanOfficer = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("Agent_LoanOfficer_CompanyName", loanOfficer.CompanyName);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("sSpAddr_MultiLine", Tools.FormatAddress(dataLoan.sSpAddr, dataLoan.sSpCity, dataLoan.sSpState, dataLoan.sSpZip));
            AddFormFieldData("sLNm", dataLoan.sLNm);
            AddFormFieldData("sFinalLAmt", dataLoan.sFinalLAmt_rep);
            AddFormFieldData("sTerm", dataLoan.sTerm_rep);
            AddFormFieldData("sFinMethT", dataLoan.sFinMethT);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sProThisMPmt", dataLoan.sProThisMPmt_rep);
            AddFormFieldData("sLtvR", dataLoan.sLtvR_rep);
            AddFormFieldData("sQualBottomR", dataLoan.sQualBottomR_rep);
            AddFormFieldData("sLPurposeT_rep", dataLoan.sLPurposeT_rep);
        }
    }
}