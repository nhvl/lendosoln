namespace LendersOffice.Pdf
{
    using System;
    using System.Collections;
    using System.Collections.Specialized;
    using DataAccess;
    using LendersOffice.Common;

    public class CARMDisclosurePDF : AbstractLegalPDF
	{
        public override string PdfFile 
        {
            get { return "ARMDisclosure.pdf"; }
        }

        public override string Description 
        {
            get { return "ARM Program Disclosure"; }
        }
        public override string EditLink 
        {
            get { return "/newlos/Disclosure/ARMProgramDisclosure.aspx"; }
        }
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            var format = dataLoan.GetFormatTarget();
            dataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);

            AddFormFieldData("sRAdj1stCapMon", dataLoan.sRAdj1stCapMon_rep);
            AddFormFieldData("sRAdjCapMon", dataLoan.sRAdjCapMon_rep);
            AddFormFieldData("sRAdjRoundT", dataLoan.sRAdjRoundT);
            AddFormFieldData("sRAdjRoundToR", dataLoan.sRAdjRoundToR_rep);
            AddFormFieldData("sRAdjLifeCapR", dataLoan.sRAdjLifeCapR_rep);
            AddFormFieldData("sRAdj1stCapR", dataLoan.sRAdj1stCapR_rep);
            AddFormFieldData("sRAdjCapR", dataLoan.sRAdjCapR_rep);
            AddFormFieldData("aBNm", dataApp.aBNm);
            AddFormFieldData("aCNm", dataApp.aCNm);
            AddFormFieldData("sHasDemandFeature", dataLoan.sHasDemandFeature);
            AddFormFieldData("sRAdjMarginR", dataLoan.sRAdjMarginR_rep);
            AddFormFieldData("sArmIndexBasedOnVstr", dataLoan.sArmIndexBasedOnVstr);
            AddFormFieldData("sArmIndexCanBeFoundVstr", dataLoan.sArmIndexCanBeFoundVstr);
            AddFormFieldData("sArmIndexAffectInitIRBit", dataLoan.sArmIndexAffectInitIRBit);
            AddFormFieldData("sArmIndexNotifyAtLeastDaysVstr", dataLoan.sArmIndexNotifyAtLeastDaysVstr);
            AddFormFieldData("sArmIndexNotifyNotBeforeDaysVstr", dataLoan.sArmIndexNotifyNotBeforeDaysVstr);

            AddFormFieldData("sTermInYr", dataLoan.sTermInYr_rep);
            AddFormFieldData("sNoteIR", dataLoan.sNoteIR_rep);
            AddFormFieldData("sRArmFloorR", dataLoan.sRArmFloorR_rep);
            AddFormFieldData("sArmIndexEffectiveD", dataLoan.sArmIndexEffectiveD_rep);

            AddFormFieldData("sRLifeCapR", dataLoan.sRLifeCapR_rep);
            AddFormFieldData("sLNm", dataLoan.sLNm);

            // 1/19/2006 dd - OPM 3832 - To avoid data get modify in other printout, I have to create a new dataobject.
            // 1/20/2005 dd - Apply Example data. DO THIS SECTION LAST because it modify loan amount to $10,000.
            #region Setup for Amortization table work correctly in ARM Disclosure example.

            CPageData tmpDataLoan = new CPDFPrintData(dataLoan.sLId);
            tmpDataLoan.InitLoad();
            tmpDataLoan.SetFormatTarget(FormatTarget.PrintoutNormalFields);

            tmpDataLoan.sLAmtLckd = true;
            tmpDataLoan.sLAmtCalc_rep = "$10,000.00"; // Example use $10,000 as loan amount.
            tmpDataLoan.sProMInsR_rep = "0.000%"; // Reset Mortgage Insurance to 0 because Payment table rely on this.
            tmpDataLoan.sProMInsMb_rep = "$0.00";
            tmpDataLoan.sRAdjWorstIndex = true;
            tmpDataLoan.sPmtAdjCapR_rep = "0.000%";
            tmpDataLoan.sPmtAdjCapMon_rep = "0";
            tmpDataLoan.sPpmtAmt_rep = "$0.00";
            tmpDataLoan.sPpmtOneAmt_rep = "$0.00";
            #endregion
           

            AddFormFieldData("sProThisMPmt", tmpDataLoan.sProThisMPmt_rep);
            AddFormFieldData("sProThisMPmtX6", tmpDataLoan.sProThisMPmtX6_rep);
            AddFormFieldData("sWorstPmt", tmpDataLoan.sWorstPmt_rep);
            AddFormFieldData("sWorstPmtYr", tmpDataLoan.sWorstPmtYr_rep, false);

            dataLoan.SetFormatTarget(format);
        }
	}
}
