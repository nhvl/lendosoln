using System;

using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
	public class CVA_26_0285PDF : AbstractLegalPDF
	{
        public override string PdfFile 
        {
            get { return "VA-26-0285.pdf";        	 }
        }
        public override string Description 
        {
            get { return "VA Transmittal List (VA 26-0285)"; }
        }
		public override bool OnlyPrintPrimary
		{
			get { return true; }
		}
        public override string EditLink 
        {
            get { return "/newlos/VA/VATransmittalList.aspx"; }
        }
        
        protected override void ApplyData(CPageData dataLoan, CAppData dataApp) 
        {
            AddFormFieldData("sAgencyCaseNum", dataLoan.sAgencyCaseNum);

            IPreparerFields preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_0285Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("VA26_0285LenderPreparerName", preparer.PreparerName);
            AddFormFieldData("VA26_0285LenderTitle", preparer.Title);
            AddFormFieldData("VA26_0285LenderPhone", preparer.Phone);
            
            preparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.VA26_0285Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            AddFormFieldData("VA26_0285BrokerCompanyName", preparer.CompanyName);
            
        }
	}
}
