﻿using System;
using System.Collections;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Pdf
{
    public class CMaineTangibleNetBenefitWorksheetPDF : CTangibleNetBenefitWorksheetPDF
    {
        public override string Description
        {
            get { return "Maine Tangible Net Benefit Worksheet"; }
        }
    }
}